<?
//$PATH_WRT_ROOT = "../../../../"; 
include_once($intranet_root."/includes/global.php");
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libuser.php");
include_once($intranet_root."/lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($intranet_root."/includes/role_manage.php");
include_once($intranet_root."/includes/form_class_manage.php");
include_once($intranet_root."/includes/libgrouping.php");
//intranet_auth();
//intranet_opendb();

class User_Group_Transformer{
	function get_userID_from_group($CatID, $ChooseGroupID){		
		$tempList = $this->get_userArray_from_group($CatID, $ChooseGroupID);
		$idList = Array();
		foreach($tempList as $temp){
			$idList[] = $temp[0];
		}

		// debug_r($idList);
		// exit;
		return $idList;
		
	}
	
	function get_userArray_from_group($CatID, $ChooseGroupID){
		global $UserID;
		if ($CatID == "")
			return; 
		if (!is_numeric($CatID))
			$CatID = $this->catType_transform($CatID);
				
		$li = new libuser($UserID);
		$lrole = new role_manage();
		$fcm = new form_class_manage();
		$lgrouping = new libgrouping();
		//$UserID = $_SESSION["UserID"];
		// echo $UserID;
		$name_field = getNameFieldWithClassNumberByLang("a.");
		$identity = $lrole->Get_Identity_Type($UserID);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		$finalUserSet = Array();
		
		if(($CatID != "") || ($CatID != 0)){ 
			
			if($CatID == -2){
				## Non-teaching Staff ##
				$result = $fcm->Get_Non_Teaching_Staff_List();	
				$finalUserSet=array_merge($result,$finalUserSet);
			}		
			if($CatID == 5){
				$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
				$result = $li->returnArray($sql,2);		
				$finalUserSet=array_merge($result,$finalUserSet);
			}
			if($CatID == 6){
				$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '".$li->Get_Safe_Sql_Query($UserID)."'";
				$result = $li->returnArray($sql,2);
				$finalUserSet=array_merge($result,$finalUserSet);
			}
		}
		
		if($CatID != "" && sizeof($ChooseGroupID)>0)
		{
			$sql = "";
			if($CatID == -1) // teaching staff
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{			
					if($ChooseGroupID[$i] == 1)
					{
						## All Teaching Staff ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
						{
							$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.")." FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 AND all_user.UserID != '".$li->Get_Safe_Sql_Query($UserID)."')";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## Form Teacher ##
						if($identity == "Teaching")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to form teacher
						}
						if($identity == "Student")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{		
						## Class Teacher ##
						if($identity == "Teaching")
						{
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to class teacher
						}
						if($identity == "Student")
						{
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## Subject Teacher ##
						if($identity == "Teaching")
						{					
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '".$li->Get_Safe_Sql_Query($UserID)."' AND b.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."' AND c.YearTermID = '".$li->Get_Safe_Sql_Query($CurrentTermID)."') AND a.UserID != '".$li->Get_Safe_Sql_Query($UserID)."' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject teacher
						}
						if($identity == "Student")
						{					
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND a.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{					
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND a.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{
						## Subject Group Teacher ##
						if($identity == "Teaching")
						{
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND a.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject group teacher
						}
						if($identity == "Student")
						{
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND a.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND a.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}
				}
				$result = $li->returnArray($sql);
				$finalUserSet=array_merge($result,$finalUserSet);
			}
			if($CatID == 2) // student
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{
					if($ChooseGroupID[$i] == 1)
					{
						## All Student ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
						{
							$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND UserID != '$UserID' AND (ClassName != '' OR ClassName != NULL) AND (ClassNumber != '' OR ClassNumber != NULL) ORDER BY ClassName, ClassNumber, EnglishName)";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## My Form Student ##
						if($identity == "Teaching")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to form student
						}
						if($identity == "Student")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{
						## My Class Student ##
						if($identity == "Teaching")
						{
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to class student
						}
						if($identity == "Student")
						{
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## My Subject Student ##
						if($identity == "Teaching")
						{
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to Subject student
						}
						if($identity == "Student")
						{
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{		
						## My Subject Group Student ##		
						if($identity == "Teaching")
						{	
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to subject group student
						}
						if($identity == "Student")
						{	
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{	
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND b.UserID != '$UserID' AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}
				}
				$result = $li->returnArray($sql);
				$finalUserSet=array_merge($result,$finalUserSet);
			}
			if($CatID == 3) // parent
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{
					if($ChooseGroupID[$i] == 1)
					{
						## All Parents ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
						{					
							//$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE RecordType = 3 AND RecordStatus = 1 AND UserID != $UserID ORDER BY ClassName, ClassNumber, EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$all_sql = "(SELECT b.ParentID, CONCAT('(',a.ClassName,'-',a.ClassNumber,') ',a.EnglishName,'\'s Parent',' (',c.EnglishName,')') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$all_sql = "(SELECT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND b.ParentID != '$UserID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## My Form Parents ##
						if($identity == "Teaching")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$form_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to form parent
						}
						if($identity == "Student")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{
						## My Class Parents ##
						if($identity == "Teaching")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$class_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to class parent
						}
						if($identity == "Student")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## My Subject Parents ##
						if($identity == "Teaching")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject parent
						}
						if($identity == "Student")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{
						## My Subject Group Parents ##
						if($identity == "Teaching")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_group_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject group parent
						}
						if($identity == "Student")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID')) AND c.UserID != '$UserID' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}			
				}
				$result = $li->returnArray($sql);
				$finalUserSet=array_merge($result,$finalUserSet);
			}
			//echo "<div style='display:none'>";
				
			//	echo '</div>';
			if($CatID == 4) // Group
			{
				if(sizeof($ChooseGroupID)>0)
				{			
				
					$TargetGroupID = is_array($ChooseGroupID)?implode(",",IntegerSafe($ChooseGroupID)):IntegerSafe($ChooseGroupID);
					$sql = "SELECT a.UserID, ".getNameFieldByLang2("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE b.GroupID IN ($TargetGroupID) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
					$result = $li->returnArray($sql,2);
					$finalUserSet=array_merge($result,$finalUserSet);
				}
			}
		}
		return $finalUserSet;
	}

	function catType_transform($CatType){
		switch ($CatType){
			case 'T': return -1;
			case 'S': return 2;
			case 'P': return 3;
			case 'O': return 4;
		};
		return $CatType;
	}
	function get_group_name($groupPath){
		global $Lang,$i_teachingStaff,$i_nonteachingStaff,$i_identity_student,$i_identity_parent;
		if (is_numeric($groupPath)){
			$catID= $groupPath;
		}
		else{
			$prefix = substr($groupPath,0,1);
			$suffix = substr($groupPath,1);
			if (!is_numeric($prefix))
				$catID = $this->catType_transform($prefix);
			else 
				$catID=$prefix;
		}
		switch($catID){
			case -1: # Teaching staff
				switch($suffix){
					case 1:
					return $Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'];
					case 2:
					return $Lang['iMail']['FieldTitle']['ToFormTeachingStaff'];
					case 3:
					return $Lang['iMail']['FieldTitle']['ToClassTeachingStaff'];
					case 4:
					return $Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'];
					case 5:
					return $Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'];
					default:
					return $i_teachingStaff;
				};
			break;
			case -2: #non-teaching staff
			return $i_nonteachingStaff;
			case 2: # student
				switch($suffix){
					case 1:
					return $Lang['iMail']['FieldTitle']['ToIndividualsStudent'];
					case 2:
					return $Lang['iMail']['FieldTitle']['ToFormStudent'];
					case 3:
					return $Lang['iMail']['FieldTitle']['ToClassStudent'];
					case 4:
					return $Lang['iMail']['FieldTitle']['ToSubjectStudent'];
					case 5:
					return $Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'];
					default:
					return $i_identity_student;
				};
			break;
			case 3: # parent
				switch($suffix){
					case 1:
					return $Lang['iMail']['FieldTitle']['ToIndividualsParents'];
					case 2:
					return $Lang['iMail']['FieldTitle']['ToFormParents'];
					case 3:
					return $Lang['iMail']['FieldTitle']['ToClassParents'];
					case 4:
					return $Lang['iMail']['FieldTitle']['ToSubjectParents'];
					case 5:
					return $Lang['iMail']['FieldTitle']['ToSubjectGroupParents'];
					default:
					return $i_identity_parent;
				};
			break;
			case 4: # group
				$lgrouping = new libgrouping();
				if (!empty($suffix)){
					$result = $lgrouping->returnCurrentGroupsWithGroupCategory("all");
					if (count($result)>0){
						for($i=0; $i<sizeof($result); $i++){
							list($GroupID, $GroupName) = $result[$i];
							if ($GroupID == $suffix)
								return $GroupName;
						}
					}					
				}
				return $Lang['iMail']['FieldTitle']['Group'];
			break;
		
		};
		return $groupPath;
	}
}
//intranet_closedb();
?>