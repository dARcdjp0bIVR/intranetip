<?php
// page modifing by: 
$PathRelative = "../../../../";
$CurSubFunction = "iCalendar";
$CurPage = $_SERVER["REQUEST_URI"];

include_once($PathRelative."src/include/class/startup.php");

if (empty($_SESSION['SSV_USERID'])) {
	header ("Location: ".$PathRelative."login.php?CurPage=".$CurPage);
	exit;
}
AuthPage(array("Communication-Calendar-GeneralUsage"));

intranet_opendb();
include_once($PathRelative."src/include/class/icalendar.php");

$iCal = new icalendar();
//$lui = new UserInterface();

include_once($PathRelative."src/include/template/popup_header.php");

/*
if ($srcForm == "")
{
	$srcForm = "NewMailForm";
}
*/
?>
<script language="javascript">

function jRollBackStep(val)
{
	document.forma.RollBackStep.value = val;
	document.forma.submit();
}

function checkOption(obj){
    for(i=0; i<obj.length; i++){
        if(obj.options[i].value== ''){
            obj.options[i] = null;
        }
    }
}

function AddOptions(obj, type) {
        par = window.opener;
        parTable = par.document.getElementById("calViewerTable");
        noOfTr = parTable.tBodies[0].getElementsByTagName("TR");

        /*
        tableLastRowClass = noOfTr[noOfTr.length-1].className;
        if (tableLastRowClass=="" || tableLastRowClass=="tablerow1") {
                firstRowClass = "tablerow2";
        } else {
                firstRowClass = "tablerow1";
        }
        */

        x = (obj.name == "Users[]") ? "U" : "G";
        // Case of "Select parent from student"
        selectedUserType = document.forma.UserType.selectedIndex;

        if(x == "G"){
                if (document.forma.UserType.options[selectedUserType].value!="1"){
                        if(document.forma.UserType.options[selectedUserType].value=="3"){
                                x = "P";
                        } else {
                                selectedUserType2 = document.forma.UserType2.selectedIndex;

                                if (document.forma.UserType2.options[selectedUserType2].value=="0")
                                        x = "C";
                                else if (document.forma.UserType2.options[selectedUserType2].value=="1")
                                        x = "S";
                                else if (document.forma.UserType2.options[selectedUserType2].value=="2")
					 x = "E";
                        }
                }
        }
        checkOption(obj);
        i = obj.selectedIndex;

        while (i!=-1) {

                nameText = obj.options[i].text;
                if (x == "G")
                        nameText += " (<?=$i_eClass_ClassLink?>)";
                else if (x == "C")
                        nameText += " (<?=$iCalendar_ChooseViewer_Course?>)";
                else if (x == "E")
                        nameText += " (<?=$iCalendar_ChooseViewer_Group?>)";
                else if  (x == "P")
                        nameText += "<?=$iCalendar_ChooseViewer_ParentSuffix?>";

                firstNode = "<a href='#' class='imail_entry_read_link'><img src='<?=$ImagePathAbs?>imail/icon_ppl_ex.gif' w
idth='20' height='20' border='0' align='absmiddle'>";
                firstNode += nameText;
                firstNode += "</a>";
                //newName = par.document.createTextNode(nameText);

                selPermission =  "<select name='permission-" + x + obj.options[i].value + "' id='permission-" + x + obj.opt
ions[i].value + "'>";
                selPermission += "<option value='W'><?=$iCalendar_NewCalendar_SharePermissionEdit?></option>";
                selPermission += "<option value='R' selected><?=$iCalendar_NewCalendar_SharePermissionRead?></option>";
                selPermission += "</select>";

                delViewer  = "<span onclick='removeViewer(this)' class='tablelink'><a href='javascript:void(0)'>Delete</a><
/span>";
                delViewer += "<input type='hidden' value='"+x+obj.options[i].value+"' name='viewerID[]' />";

                newRow = par.document.createElement("tr");
                //newRow.className = firstRowClass;

		newCellName = par.document.createElement("td");
                newCellName.className = "cal_agenda_content_entry";
                newCellName.nowrap = true;
                newCellName.innerHTML = firstNode;

                newCellPermission = par.document.createElement("td");
                newCellPermission.className = "cal_agenda_content_entry";
                newCellPermission.innerHTML = selPermission;

                newCellDelete = par.document.createElement("td");
                newCellDelete.className = "cal_agenda_content_entry cal_agenda_content_entry_button";
                newCellDelete.innerHTML = delViewer;

                newRow.appendChild(newCellName);
                newRow.appendChild(newCellPermission);
                newRow.appendChild(newCellDelete);

                parTable.tBodies[0].appendChild(newRow);
                obj.options[i] = null;
                i = obj.selectedIndex;

                /*
                if (firstRowClass == "tablerow1") {
                        firstRowClass = "tablerow2";
                } else {
                        firstRowClass = "tablerow1";
                }
                */
        }
}

function AddRecords(TmpStr)
{
	var ParWindow = opener.window;	
	if (ParWindow)
	{		
		var ParObj2 = opener.window.document.forms["<?=$srcForm?>"].elements["<?php echo $fromSrc; ?>"];				
	}
	
	if (ParObj2.value == "")
		ParObj2.value = TmpStr;
	else	
		ParObj2.value += ";"+TmpStr;
}

function jAdd_ACTION(jParAct, jParSrc)
{
	document.forma.specialAction.value = jParAct;
	document.forma.actionLevel.value = jParSrc;
	document.forma.submit();	
}

</script>


<?php

$LoadJavascript = "";

////////////////////////////////////////////////////
//	Step1: Fixed
///////////////////////////////////////////////////
$Step1Arr = array();
$Step1Arr[] = array($Lang['email']['TeachingStaff'],0);
$Step1Arr[] = array($Lang['email']['NonTeachingStaff'],1);
//$Step1Arr[] = array("Student",2);
//$Step1Arr[] = array($Lang['email']['Subjects'],3);
//$Step1Arr[] = array($Lang['email']['Sections'],4);
//$Step1Arr[] = array($Lang['email']['Houses'],5);
$Step1Arr[] = array($Lang['email']['SchoolBasedGroup'],6);
$Step1Select = $lui->Get_Input_Select("UserType","UserType",$Step1Arr,$UserType,"size=\"4\" style=\"width: 250px\"  ");

//$ActionStep1 = "onclick=\"this.form.action='select_recipient.php#step2';jAdd_ACTION('add','TeachType')\"";
$ActionStep1 = "onclick=\"this.form.action='index_jason.php#step2';AddOptions(this.form.elements['UserType'],0);jAdd_ACTION('add','UserType')\"";

///////////////////////////////////////////////////

$IsEnding = false;
$IsEnding2 = false;
$IsEnding3 = false;
$IsEnding4 = false;
$IsEnding5 = false;

if ($UserType == "1")
{
	// Non-teaching staff	
	$ReturnArr = array();

	$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='N,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
				";
	$ReturnArr = $iCal->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}	
	$OutputArr = array();	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);		
		
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{		
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\"";
			}
			//$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
	}	
	//$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step2';jAdd_ACTION('add','step1_2')\"";
	$ActionStep2 = "onclick=\"this.form.action='index_jason.php#step2';AddOptions(this.form.elements['Users[]'],0);\"";
	
	
	$Step2Select = $lui->Get_Input_Select_multi("Users[]","Users[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
		$LoadJavascript .= "</script> ";
	}	
}
else if ($UserType == "0" || $UserType == 2)
{
// Teaching staff


/*
	$Sql =  	"
					exec Search_StaffEmail 
					@Staff_Type='T,', 
					@Roll_Group=',',					
					@School_Year =',',
					@House=',' ,
					@SubjectName=',' , 
					@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
				";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "<script language='javascript' > ";
	}
	}	
	$OutputArr = array();	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);		
		
		if (($specialAction == "add") && ($actionLevel == "step1_2"))
		{		
		if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);	
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
		}
	}	
	$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step2';jAdd_ACTION('add','step1_2')\"";

	
	$Step2Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"4\" style=\"width: 200px\" ");
	$IsEnding2 = true;
	if (($specialAction == "add") && ($actionLevel == "step1_2"))
	{
	if (count($Emails) > 0)
	{
		$LoadJavascript .= "</script> ";
	}
	}
	*/


	# Teaching Staff
	///////////////////////////////////////////////////
	//	Step2: Fixed
	///////////////////////////////////////////////////
	$Step2Arr = array();
	$Step2Arr[] = array($Lang['email']['Subjects'],0);
	$Step2Arr[] = array($Lang['email']['Sections'],1);
	//$Step2Arr[] = array($Lang['email']['Houses'],2);
	$Step2Select = $lui->Get_Input_Select("UserType2","UserType2",$Step2Arr,$UserType2,"size=\"4\" style=\"width: 250px\"");			
	//$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','TeachType')\"";
	$ActionStep2 = "onclick=\"this.form.action='index_jason.php#step3';AddOptions(this.form.elements['UserType2'],0);jAdd_ACTION('add','UserType2')\"";
	///////////////////////////////////////////////////

	////////////////////////////////////////////////////
	//	Step3: From DB
	///////////////////////////////////////////////////		
	if ($UserType2 == "0")
	{
	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);		
		}			
		$Step3Select = $lui->Get_Input_Select("UserSubject","UserSubject",$OutputArr,$UserSubject,"size=\"4\" style=\"width: 250px\" ");
		$ActionStep3 = "onclick=\"this.form.action='index_jason.php#step4';AddOptions(this.form.elements['UserSubject'],0);jAdd_ACTION('add','UserSubject')\"";
		//$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step4';jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}	
		
		if ($UserSubject != "")
		{
			$ReturnArr = array();
			if($UserType == 0){
				$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type='T,', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
						";
			} else if($UserType == 2){
				/*
                                $Sql = "        exec Search_StudentEmail
                                                        @Roll_Group = ',',
                                                        @School_Year = ',',
                                                        @House = ',',
                                                        @SubjectCode = '".$UserSubject.",',
                                                        @TTPeriod = '".$SYS_CONFIG['SchoolYear'].",'
                                                ";
                */
            }
			$ReturnArr = $iCal->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
				
				if (($specialAction == "add") && ($actionLevel == "step2_4"))
				{
					$TmpName = $ReturnArr[$i]["OfficialFullName"];					
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\"";
					}
					//$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				
			}	
			
			$Step4Select = $lui->Get_Input_Select_multi("Users[]","Users[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
			$IsEnding4 = true;
		}
		//$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','step2_4')\"";		
		$ActionStep4 = "onclick=\"this.form.action='index_jason.php#step5';AddOptions(this.form.elements['Users[]'],0);\""
;
		
		if (($specialAction == "add") && ($actionLevel == "step2_4"))
		{
			$LoadJavascript .= "</script> ";
		}	
						
		///////////////////////////////////////////////////
	}
	else if ($UserType2 == "1")
	{
		# Sections	
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $iCal->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}	
		
		$Step3Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"4\" style=\"width: 250px\" ");
		$ActionStep3 = "onclick=\"this.form.action='index_jason.php#step4';AddOptions(this.form.elements['SchoolYear'],0);jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		if ($SchoolYear != "")
		{
			$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
				if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$Roll_Group  = "";
			}			
						
			$Step4Select = $lui->Get_Input_Select("Roll_Group","Roll_Group",$OutputArr,$Roll_Group,"size=\"4\" style=\"width: 250px\" ");
			$ActionStep4 = "onclick=\"this.form.action='index_jason.php#step5';AddOptions(this.form.elements['Roll_Group'],0);jAdd_ACTION('add','Roll_Group')\"";
			//$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','Roll_Group')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}	
			
			if ($Roll_Group != "")
			{
				$ReturnArr = array();
				if($UserType == 0){
					$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type='T,', 
								@Roll_Group='".$Roll_Group.",',					
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
							";
				} else if($UserType == 2){
					/*
                        $Sql = "        exec Search_StudentEmail
                                                @Roll_Group = '".$Roll_Group.",,',
                                                @School_Year = '".$SchoolYear.",',
                                                @House = ',',
                                                @SubjectCode = ',',
                                                @TTPeriod = '".$SYS_CONFIG['SchoolYear'].",'
                                        ";
                    */
                }
				$ReturnArr = $iCal->returnArray($Sql);
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					
					if (($specialAction == "add") && ($actionLevel == "step3_5"))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\"";
						}
						//$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					
				}	
				
				$Step5Select = $lui->Get_Input_Select_multi("Users[]","Users[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
				$IsEnding5 = true;
				
				//$ActionStep5 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','step3_5')\"";
				$ActionStep5 = "onclick=\"this.form.action='index_jason.php#step5';AddOptions(this.form.elements['Users[]'],0);\"";
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step3_5"))
			{
				$LoadJavascript .= "</script> ";
			}	
						
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////
	}
	else if ($UserType2 == "2")
	{
	// Sections	? Houses	
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $iCal->returnArray($Sql);
				
		$Step3Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"4\" style=\"width: 250px\" ");		
		//$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step4';jAdd_ACTION('add','House')\"";
		$ActionStep3 = "onclick=\"this.form.action='index_jason.php#step4';AddOptions(this.form.elements['House'],0);jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step4: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{
/*
			$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
			$ReturnArr = $iCal->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)
			{
				$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
				if ($School_Year_House == $ReturnArr[$i]["School_Year"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$School_Year_House  = "";
			}			
						
			$Step4Select = $lui->Get_Input_Select("School_Year_House","School_Year_House",$OutputArr,$School_Year_House,"size=\"4\" style=\"width: 250px\" ");
			$ActionStep4 = "onclick=\"this.form.action='index.php#step5';AddOptions(this.form.elements['School_Year_House'],0);jAdd_ACTION('add','School_Year_House')\"";
			//$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','School_Year_House')\"";
*/			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}	
			
			//if ($School_Year_House != "")
			//{
				$ReturnArr = array();
                                if($UserType == 0){
					$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type='T,', 
								@Roll_Group=',',					
								@School_Year =',',
								@House='".$House.",' ,
								@SubjectName=',' , 
								@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
							";
				} else if($UserType == 2){
					/*
                        $Sql = "        exec Search_StudentEmail
                                                @Roll_Group = ',',
                                                @School_Year = '".$School_Year_House.",',
                                                @House = '".$House.",',
                                                @SubjectCode = ',',
                                                @TTPeriod = '".$SYS_CONFIG['SchoolYear'].",'
                                        ";
                    */
                }
//echo $Sql;
				$ReturnArr = $iCal->returnArray($Sql);
				$OutputArr = array();	
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"], $ReturnArr[$i]["UserID"]);
					
					if (($specialAction == "add") && ($actionLevel == "step4_4"))
					{					
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\"";
						}
						//$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi("Users[]","Users[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				//$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','step4_4')\"";
				$ActionStep4 = "onclick=\"this.form.action='index_jason.php#step5';AddOptions(this.form.elements['Users[]'],0);\"";
				
			//}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4"))
			{
				$LoadJavascript .= "</script> ";
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////

		
	}
	///////////////////////////////////////////////////		
		
}
else if ($TeachType == "2")
{
	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
		$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);		
		}			
		$Step2Select = $lui->Get_Input_Select("TeachSubject","TeachSubject",$OutputArr,$TeachSubject,"size=\"4\" style=\"width: 250px\" ");
		$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','TeachSubject')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
	
		if ($TeachSubject != "")
		{
			$Sql =  	"
							exec Search_StaffEmail 
							@Staff_Type=',', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName='".$TeachSubject.",' , 
							@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
						";
						
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
				
				if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["OfficialFullName"];					
					$TmpEmail = $ReturnArr[$i]["Email"];
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					}
					else
					{
						$TmpStr = $TmpEmail;
					}
					$TmpStr = str_replace("'","\'",$TmpStr);	
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				}
				
			}	
			
			$Step3Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','step2_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step2_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////	
}
else if ($TeachType == "3")
{
	// Sections	
		$Sql =  "exec Get_SchoolYearList ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
		}	
		
		$Step2Select = $lui->Get_Input_Select("SchoolYear","SchoolYear",$OutputArr,$SchoolYear,"size=\"4\" style=\"width: 250px\" ");
		$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','SchoolYear')\"";
	
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($SchoolYear != "")
		{
			$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
				if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$Roll_Group  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("Roll_Group","Roll_Group",$OutputArr,$Roll_Group,"size=\"4\" style=\"width: 250px\" ");
			$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step4';jAdd_ACTION('add','Roll_Group')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($Roll_Group != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group='".$Roll_Group.",',					
								@School_Year =',',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();					
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					
					if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
					{
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);				
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
					
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step4';jAdd_ACTION('add','step3_3_5')\"";
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step3_3_5"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}	
						
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////
	
}
else if ($TeachType == "4")
{
	// Sections	
		$Sql =  "exec Get_HouseList";
		$ReturnArr = $LibDB->returnArray($Sql);
				
		$Step2Select = $lui->Get_Input_Select("House","House",$ReturnArr,$House,"size=\"4\" style=\"width: 250px\" ");		
		$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step2';jAdd_ACTION('add','House')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		if ($House != "")
		{
			$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
			$ReturnArr = $LibDB->returnArray($Sql);
						
			$OutputArr = array();	
			$IsFound = false;
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
				if ($School_Year_House == $ReturnArr[$i]["School_Year"])
				{
					$IsFound = true;
				}
			}	
			if (!$IsFound)
			{
				$School_Year_House  = "";
			}			
						
			$Step3Select = $lui->Get_Input_Select("School_Year_House","School_Year_House",$OutputArr,$School_Year_House,"size=\"4\" style=\"width: 250px\" ");
			$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','School_Year_House')\"";
			
			////////////////////////////////////////////////////
			//	Step5: From DB
			///////////////////////////////////////////////////		
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "<script language='javascript' > ";
			}
			}	
			
			if ($School_Year_House != "")
			{
				$Sql =  	"
								exec Search_StaffEmail 
								@Staff_Type='T,', 
								@Roll_Group=',',					
								@School_Year ='".$School_Year_House.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
							";
				$ReturnArr = $LibDB->returnArray($Sql);
				$OutputArr = array();	
				for ($i=0;$i<count($ReturnArr);$i++)	
				{
					$OutputArr[] = array($ReturnArr[$i]["OfficialFullName"]." (".$ReturnArr[$i]["Email"].")", $ReturnArr[$i]["UserID"]);
					
					if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
					{					
					if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
					{
						$TmpName = $ReturnArr[$i]["OfficialFullName"];					
						$TmpEmail = $ReturnArr[$i]["Email"];
						if ($TmpName != "")
						{
							$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
						}
						else
						{
							$TmpStr = $TmpEmail;
						}
						$TmpStr = str_replace("'","\'",$TmpStr);				
						$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
					}
					}
				}	
				
				$Step4Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
				$IsEnding4 = true;
				
				$ActionStep4 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','step4_4_4')\"";
				
			}		
			
			if (($specialAction == "add") && ($actionLevel == "step4_4_4"))
			{
			if (count($Emails) > 0)
			{
				$LoadJavascript .= "</script> ";
			}
			}				
			///////////////////////////////////////////////////
			
		}		
		///////////////////////////////////////////////////	
	
}
else if ($TeachType == "5")
{
//School groups

	// Subjects		

		//$Sql =  "exec Get_SubjectList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
		$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$OutputArr = array();	
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$OutputArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);		
		}
		$Step2Select = $lui->Get_Input_Select("SchoolGroup","SchoolGroup",$OutputArr,$SchoolGroup,"size=\"4\" style=\"width: 250px\" ");
		$ActionStep2 = "onclick=\"this.form.action='select_recipient.php#step3';jAdd_ACTION('add','SchoolGroup')\"";
		
		////////////////////////////////////////////////////
		//	Step3: From DB
		///////////////////////////////////////////////////		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "<script language='javascript' > ";
		}
		}	
		
		if ($SchoolGroup != "")
		{
			$Sql =  	"
							SELECT
								b.UserName, b.UserEmail
							FROM
								MAIL_ADDRESS_MAPPING a,
								MAIL_ADDRESS_USER b
							WHERE
								a.MailUserID = b.MailUserID
								AND a.MailGroupID = '".$SchoolGroup."'	 
						";
			$ReturnArr = $LibDB->returnArray($Sql);
			$OutputArr = array();	
			for ($i=0;$i<count($ReturnArr);$i++)	
			{
				$OutputArr[] = array($ReturnArr[$i]["UserName"]." (".$ReturnArr[$i]["UserEmail"].")", $ReturnArr[$i]["UserID"]);
				
				if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
				{
				if (is_array($Emails) && in_array($ReturnArr[$i]["UserID"], $Emails))
				{
					$TmpName = $ReturnArr[$i]["UserName"];					
					$TmpEmail = $ReturnArr[$i]["Email"];
					if ($TmpName != "")
					{
						$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
					}
					else
					{
						$TmpStr = $TmpEmail;
					}
					$TmpStr = str_replace("'","\'",$TmpStr);	
					$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
				}
				}
				
			}	
			
			$Step3Select = $lui->Get_Input_Select_multi("Emails[]","Emails[]",$OutputArr,"","size=\"4\" style=\"width: 250px\" ");
			$IsEnding3 = true;
		}
		$ActionStep3 = "onclick=\"this.form.action='select_recipient.php#step5';jAdd_ACTION('add','step5_2_4')\"";		
		
		if (($specialAction == "add") && ($actionLevel == "step5_2_4"))
		{
		if (count($Emails) > 0)
		{
			$LoadJavascript .= "</script> ";
		}
		}	
						
		///////////////////////////////////////////////////	

	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Adding groups
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($specialAction == "add")
{
	if ($actionLevel == "TeachType")
	{		
		if ($TeachType == "1")
		{
			$Staff_Type = "N";
		}
		else
		{
			$Staff_Type = "T";
		}
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type='".$Staff_Type.",', 
						@Roll_Group=',',					
						@School_Year =',',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "TeachSubject")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House=',' ,
						@SubjectName='".$TeachSubject.",' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);

		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);	
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "SchoolYear")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$SchoolYear.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);			
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "Roll_Group")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group='".$Roll_Group.",',					
						@School_Year =',',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			$TmpStr = str_replace("'","\'",$TmpStr);			
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year =',',
						@House='".$House.",' ,
						@SubjectName=',' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
					
			$TmpStr = str_replace("'","\'",$TmpStr);	
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "School_Year_House")
	{		
		$Sql =  	"
						exec Search_StaffEmail 
						@Staff_Type=',', 
						@Roll_Group=',',					
						@School_Year ='".$School_Year_House.",',
						@House=',' ,
						@SubjectName=',' , 
						@TTPeriod= '".$SYS_CONFIG['SchoolYear'].",'
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["OfficialFullName"];					
			$TmpEmail = $ReturnArr[$i]["Email"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
						
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}	
	else if ($actionLevel == "SchoolGroup")
	{				
		$Sql =  	"
						SELECT
							b.UserName, b.UserEmail
						FROM
							MAIL_ADDRESS_MAPPING a,
							MAIL_ADDRESS_USER b
						WHERE
							a.MailUserID = b.MailUserID
							AND a.MailGroupID = '".$SchoolGroup."'	 
					";
		$ReturnArr = $LibDB->returnArray($Sql);
		
		$LoadJavascript .= "<script language='javascript' > ";		
		for ($i=0;$i<count($ReturnArr);$i++)	
		{
			$TmpName = $ReturnArr[$i]["UserName"];					
			$TmpEmail = $ReturnArr[$i]["UserEmail"];
			if ($TmpName != "")
			{
				$TmpStr = "\"".$TmpName."\" <".$TmpEmail.">";
			}
			else
			{
				$TmpStr = $TmpEmail;
			}
			
			$TmpStr = str_replace("'","\'",$TmpStr);			
			$LoadJavascript .= "AddRecords('".$TmpStr."'); \n";			
		}			
		$LoadJavascript .= "</script> ";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo $LoadJavascript;

echo $lui->Get_Div_Open("module_bulletin"," class=\"module_content\" " );
echo $lui->Get_Div_Open(""," style=\"width:99%\" " );

echo $lui->Get_Div_Open("module_content_header");				
echo $lui->Get_Div_Open("module_content_header_title");				
echo $lui->Get_HyperLink_Open("#");
echo $Lang['email']['HeaderTitle'];
echo $lui->Get_HyperLink_Close();
echo $lui->Get_Div_Close();
echo "<br />";
echo $lui->Get_Div_Close();

echo "<br style=\"clear:none\">";


////////////// DEBUG ///////////////////
if(Auth(array("MySchool-Staff"), "TARGET"))
{
//////////// END DEBUG ///////////////////

//echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_Div_Open();
//$SchoolLink .=  $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=1");
$SchoolLink .= $lui->Get_HyperLink_Open("#");
$SchoolLink .= $lui->Get_Span_Open();
$SchoolLink .= $Lang['email']['SelectRecipient'];
$SchoolLink .= $lui->Get_Span_Close();
$SchoolLink .= $lui->Get_HyperLink_Close();
/*
$PersonalLink .= $lui->Get_HyperLink_Open("imail_address_book.php?ChooseType=2");
$PersonalLink .= $lui->Get_Span_Open();
$PersonalLink .= $Lang['email']['Personal'];
$PersonalLink .= $lui->Get_Span_Close();
$PersonalLink .= $lui->Get_HyperLink_Close();
*/

echo $lui->Get_Div_Close();

//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";
/////////////////////////////////////////
////   Gen the menu list ////////////////
if($RollBackStep == "")
$RollBackStep = 5; // MAX STEP SHOW

$NatArr = array();
$NatArr[]= array($Lang['email']['SelectRecipient'], 0);

if($TeachType != "" && $RollBackStep >= 1)
{
	if($TeachType == 0)
	$NatArr[] = array($Lang['email']['TeachingStaff'], 0);
	else if($TeachType == 1)
	$NatArr[] = array($Lang['email']['NonTeachingStaff'], 1);
	else if($TeachType == 5)
	$NatArr[] = array($Lang['email']['SchoolBasedGroup'], 5);
}

if($TeachType2 != "" && $RollBackStep >= 2)
{
	if($TeachType2 == 0)
	$NatArr[] = array($Lang['email']['Subjects'], 0);
	else if($TeachType2 == 1)
	$NatArr[] = array($Lang['email']['Sections'], 1);
	else if($TeachType2 == 2)
	$NatArr[] = array($Lang['email']['Houses'], 2);
}

if($SchoolGroup != "" && $RollBackStep >= 2)
{
	$Sql =  " SELECT GroupName, MailGroupID FROM MAIL_ADDRESS_GROUP WHERE GroupType='S' ";
	$ReturnArr = $LibDB->returnArray($Sql);

	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolGroup == $ReturnArr[$i]["MailGroupID"])
		{
			$NatArr[] = array($ReturnArr[$i]["GroupName"], $ReturnArr[$i]["MailGroupID"]);
			break;
		}
	}
} // end school group

if($TeachSubject != "" && $RollBackStep >= 3)
{
	// Subject
	$Sql =  "exec Get_SubjectNameList_ByTTPeriod @TTPeriod='".$SYS_CONFIG['SchoolYear']."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
		
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($TeachSubject == $ReturnArr[$i]["Subject_Code"])
		{
			$NatArr[] = array($ReturnArr[$i]["FullName"], $ReturnArr[$i]["Subject_Code"]);
			break;
		}
	}
} // end if Teach Subject

if($House != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_HouseList";
	$ReturnArr = $LibDB->returnArray($Sql);
	
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($House == $ReturnArr[$i]["HouseCode"])
		{
			$NatArr[] = array($ReturnArr[$i]["HouseName"], $ReturnArr[$i]["HouseCode"]);
			break;
		}
	}
} // end if Hosue

if($SchoolYear != "" && $RollBackStep >= 3)
{
	$Sql =  "exec Get_SchoolYearList ";
	$ReturnArr = $LibDB->returnArray($Sql);
			
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($SchoolYear == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);	
			break;
		}	
	}
} // end if school year

if($SchoolYear != "" && $Roll_Group != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_RollGroupList_BySchoolYear @School_Year = '".$SchoolYear."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($Roll_Group == $ReturnArr[$i]["Roll_Group"])
		{
			$NatArr[] = array($ReturnArr[$i]["Roll_Group"], $ReturnArr[$i]["Roll_Group"]);		
			if ($Roll_Group == $ReturnArr[$i]["Roll_Group"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$Roll_Group  = "";
	}
} // end roll group

if($House != "" && $School_Year_House != "" && $RollBackStep >= 4)
{
	$Sql =  "exec Get_SchoolYearList_ByHouse @House = '".$House."' ";
	$ReturnArr = $LibDB->returnArray($Sql);
						
	$IsFound = false;
	for ($i=0;$i<count($ReturnArr);$i++)	
	{
		if($School_Year_House == $ReturnArr[$i]["School_Year"])
		{
			$NatArr[] = array($ReturnArr[$i]["School_Year"], $ReturnArr[$i]["School_Year"]);		
			if ($School_Year_House == $ReturnArr[$i]["School_Year"])
			{
				$IsFound = true;
			}
			break;
		}
	}	
	if (!$IsFound)
	{
		$School_Year_House  = "";
	}
} // end if House->School Year

echo $lui->Get_Div_Open("", 'class="imail_link_menu"');
echo $lui->Get_Span_Open("", 'class="imail_entry_read_link"');

for($i = 0; $i < count($NatArr); $i++)
{
	if($i%3 == 0 && $i != 0)
	$MenuList .= $lui->Get_Br();
	
	$MenuList .= $lui->Get_HyperLink_Open("#", 'onClick="jRollBackStep('.$i.')"');
	if($i == 0)
	$MenuList .= $NatArr[$i][0];
	else
	$MenuList .= " > ".$NatArr[$i][0];
	$MenuList .= $lui->Get_HyperLink_Close();
}
echo $MenuList;

echo $lui->Get_Span_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Br();

/////////////////////////////////////////////////////////////////////
if($RollBackStep < 1)
{
$TeachType = "";
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 2)
{
$TeachType2 = "";
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";
$SchoolGroup = "";

$Step3Select = "";
$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 3)
{
$TeachSubject = "";
$SchoolYear = "";
$Roll_Group = "";
$House = "";
$School_Year_House = "";

$Step4Select = "";
$Step5Select = "";
}
else if($RollBackStep < 4)
{
$Roll_Group = "";
$School_Year_House = "";
$Step5Select = "";
}


if($Step5Select != "")
{
	$StepSelect = $Step5Select;
	$ActionStep = $ActionStep5;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding5;
}
else if($Step4Select != "")
{
	$StepSelect = $Step4Select;
	$ActionStep = $ActionStep4;
	$ShowStep = "step5";
	$IsEndingMode = $IsEnding4;

}
else if($Step3Select != "")
{
	$StepSelect = $Step3Select;
	$ActionStep = $ActionStep3;
	$ShowStep = "step4";
	$IsEndingMode = $IsEnding3;
}
else if($Step2Select != "")
{
	$StepSelect = $Step2Select;
	$ActionStep = $ActionStep2;
	$ShowStep = "step3";
	$IsEndingMode = $IsEnding2;
}
else
{
	$StepSelect = $Step1Select;
	$ActionStep = $ActionStep1;
	$ShowStep = "step2";
	$IsEndingMode = $IsEnding;
}

$StepBtn .=  $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep.' ');
if (!$IsEndingMode)
{	
	$StepBtn .=  "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'index_jason.php#'.$ShowStep.'\';this.form.submit()" ');
}

$StepBtn = $lui->Get_Div_Open("form_btn").$StepBtn.$lui->Get_Div_Close();

$A_Link =  "<a name=\"".$ShowStep."\" ></a>";

//$StepSelect = "";
$Step1Select = "";
$Step2Select = "";
$Step3Select = "";
$Step4Select = "";
$Step5Select = "";

////////////////////////////////////////
				
echo $lui->Get_Div_Open("commontabs_board", "class=\"imail_mail_content\"");											
echo $lui->Get_Form_Open("forma","POST","select_recipient.php",'');

if ($StepSelect != "")
{	
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $StepSelect;
	echo $StepBtn;
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
}

if($TeachType != "")
echo $lui->Get_Input_Hidden("TeachType", "TeachType", $TeachType);
if($TeachType2 != "")
echo $lui->Get_Input_Hidden("TeachType2", "TeachType2", $TeachType2);
if($TeachSubject != "")
echo $lui->Get_Input_Hidden("TeachSubject", "TeachSubject", $TeachSubject);
if($SchoolYear != "")
echo $lui->Get_Input_Hidden("SchoolYear", "SchoolYear", $SchoolYear);
if($Roll_Group != "")
echo $lui->Get_Input_Hidden("Roll_Group", "Roll_Group", $Roll_Group);
if($House != "")
echo $lui->Get_Input_Hidden("House", "House", $House);
if($School_Year_House != "")
echo $lui->Get_Input_Hidden("School_Year_House", "School_Year_House", $School_Year_House);
if($SchoolGroup != "")
echo $lui->Get_Input_Hidden("SchoolGroup", "SchoolGroup", $SchoolGroup);

////////////////////    Show Step 1    ////////////////////////////
if ($Step1Select != "")
{
echo $lui->Get_Paragraph_Open("align=\"center\"");
echo $Step1Select;

echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep1.' ');
if (!$IsEnding)
{	
	echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient.php#step2\';this.form.submit()" ');
}
echo $lui->Get_Div_Close();

echo $lui->Get_Paragraph_Close();

echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
echo $lui->Get_Paragraph_Close();
}
////////////////////////////////////////////////////////////////

if ($Step2Select != "")
{	
	echo "<a name=\"step2\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step2Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep2.' ');
	if (!$IsEnding2)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient.php#step3\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();		
} // end if show step2

if ($Step3Select != "")
{
	echo "<a name=\"step3\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step3Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep3.' ');
	if (!$IsEnding3)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient.php#step4\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step3

if ($Step4Select != "")
{
	echo "<a name=\"step4\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step4Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep4.' ');
	if (!$IsEnding4)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
} // end if show step4

if ($Step5Select != "")
{
	echo "<a name=\"step5\" ></a>";
	echo $lui->Get_Paragraph_Open("align=\"center\"");
	echo $Step5Select;	
	echo $lui->Get_Div_Open("form_btn");
	echo $lui->Get_Input_Button("btn_add1","button",$Lang['btn_add'],'class="button_main_act" '.$ActionStep5.' ');
	if (!$IsEnding5)
	{	
		echo "&nbsp;&nbsp;".$lui->Get_Input_Button("btn_expand1","button",$Lang['btn_expand'],'class="button_main_act" onclick="this.form.action=\'select_recipient.php#step5\';this.form.submit()" ');
	}
	echo $lui->Get_Div_Close();
	echo $lui->Get_Paragraph_Close();
	echo $lui->Get_Paragraph_Open("class=\"dotline_p_narrow\"");
	echo $lui->Get_Paragraph_Close();
}

} //end check auth right

else
{
echo $lui->Get_Div_Open("commontabs");
echo $lui->Get_HyperLink_Open("#");
echo $lui->Get_Span_Open();
echo "You have no premission to select recipient";
echo $lui->Get_Span_Close();
echo $lui->Get_HyperLink_Close();
}


echo $lui->Get_Div_Open("form_btn");
echo $lui->Get_Input_Button("btn_close1","button",$Lang['btn_close_window'],'class="button_main_act" onclick="window.close()" ');
echo $lui->Get_Div_Close();

echo $lui->Get_Input_Hidden("actionLevel","actionLevel");
echo $lui->Get_Input_Hidden("specialAction","specialAction");
echo $lui->Get_Input_Hidden("fromSrc","fromSrc",$fromSrc);
echo $lui->Get_Input_Hidden("srcForm","srcForm",$srcForm);
echo $lui->Get_Input_Hidden("RollBackStep","RollBackStep"); // to roll back the last step
echo $lui->Get_Form_Close();

echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();
echo $lui->Get_Div_Close();

include_once($PathRelative."src/include/template/popup_footer.php");
?>
