<?php
// page modifing by: 
$CurSubFunction = "iCalendar";
$PATH_WRT_ROOT = "../../";
$ImagePathAbs = "/theme/image_en/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
##########################################################################################################################
intranet_auth();
intranet_opendb();

$iCal = new icalendar();

$userID = $_SESSION["UserID"];

$CalVisibleID = (isset($CalVisibleID)) ? $CalVisibleID : array();


if (count($CalVisibleID) > 0) {
	for($i=0 ; $i<count($CalVisibleID) ; $i++){
		$calID = $CalVisibleID[$i];
		$visible = (isset(${"toggleCalVisible-".$calID}) ) ? 1 : 0 ;
		
		if (is_numeric($calID)) {			
			$sql  = "UPDATE CALENDAR_CALENDAR_VIEWER SET Visible=$visible ";
			$sql .= "WHERE CalID=$calID AND UserID=$userID";
			$iCal->db_db_query($sql);
		} else if ($calID=="school") {
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value=$visible ";
			$sql .= "WHERE Setting='schoolCalVisible' AND UserID=$userID";
			$iCal->db_db_query($sql);
		} else if ($calID=="involve") {
			$sql  = "UPDATE CALENDAR_USER_PREF SET Value=IF$visible ";
			$sql .= "WHERE Setting='involveCalVisible' AND UserID=$userID";
			$iCal->db_db_query($sql);
		}
	}
}

intranet_closedb();

?>