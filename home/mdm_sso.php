<?
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


if ($plugin['eSchoolPad_MDM'] && is_file($PATH_WRT_ROOT.'plugins/mdm_config.php')) {
	include_once($PATH_WRT_ROOT."plugins/mdm_config.php");
}
else {
	No_Access_Right_Pop_Up();
	die();
}

intranet_opendb();

$luser = new libuser($_SESSION['UserID']);

$user_extra_info = array();
if($_SESSION['SSV_USER_ACCESS']['eAdmin-eSchoolPad_MDM']){
	$user_extra_info['role'] = 'admin';
}else if($luser->isTeacherStaff()){
	$user_extra_info['role'] = 'teacher';
}else if($luser->isStudent()){
	$user_extra_info['role'] = 'student';
}
$user_extra_info['email'] = $luser->UserEmail;

$user_extra_info['platforms'] = "ios,android";
$extra=base64_encode(serialize($user_extra_info));
$school_code = $ssoservice["eSchoolPad_MDM"]["schoolCode"];
$userlogin = $luser->UserLogin;

$user="$userlogin@$school_code.eclass.com.hk";
$expire='1468229902';
$key=$ssoservice["eSchoolPad_MDM"]["sharedKey"]; // the shared-key
$timestamp='1468225902';
$preauthAttriString = $user."|".$expire."|".$timestamp."|".$extra;
$preauthToken=hash_hmac("sha1",$preauthAttriString,$key);

$ssoLink = $ssoservice["eSchoolPad_MDM"]["ssoUrl"]."auth?user=$user&timestamp=$timestamp&expire=$expire&preauth=$preauthToken&extra=$extra";


intranet_closedb();
//echo $ssoLink;
header ("Location: ".$ssoLink);
?>