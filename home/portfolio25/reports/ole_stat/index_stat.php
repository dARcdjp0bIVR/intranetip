<?php

# Modifing by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-adminright.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-accessright.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
intranet_auth();
intranet_opendb();

$ldb = new libdb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();

$lpf_amr = new libpf_adminright();
$lpf_amr->ACCESS_CONTROL("REPORT","OLE_STAT","VIEW");

$lpf_acr = new libpf_accessright();
switch($lpf_acr->GET_ACCESS_RIGHT("student_info"))
{
  # Subject teacher view class
  case 3:
  # Class teacher view class
  case 2:
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $_SESSION['iPortfolio']['portfolio_user_id']);
    $class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
    $classlevel_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASSLEVEL();
    break;
  case 1:
  default:
    break;
}

# Get OLE component
# Create temporary table for Component Summary
$lpf_ole_component = new libpf_ole_component();
$t_component_arr = $lpf_ole_component->GET_COMPONENT_LIST();
for($i=0; $i<count($t_component_arr); $i++)
{
  $t_component_code = $t_component_arr[$i]['DefaultID'];
  $t_title = ($intranet_session_language == "b5") ? $t_component_arr[$i]['ChiTitle'] : $t_component_arr[$i]['EngTitle'];

  $component_selection_arr[] = array($t_component_code, $t_title);

  if($componentCode != "" && $componentCode != $t_component_code) continue;
  $component_arr[$t_component_code] = $t_title;

  $t_code_abbr = str_replace(array("[", "]"), "", $t_component_code);
  $t_fieldname = "t_".$t_code_abbr;
  
  $sql_temptable_field .= ", ".$t_fieldname." int(8) DEFAULT 0";
  $component_count_field .= ", IF(t_component_hour.".$t_fieldname." IS NULL, 0, t_component_hour.".$t_fieldname.") AS ".$t_code_abbr."Hours";
}
$component_selection_html = getSelectByArray($component_selection_arr, "name='componentCode' onChange='jCHANGE_FIELD()'", $componentCode, 1, 0, $ec_iPortfolio['all_ele'], 2);
$sql_temptable_create = "CREATE TEMPORARY TABLE tempOLEComponentSummay ";
$sql_temptable_create .= "(UserID int(11)".$sql_temptable_field.", PRIMARY KEY (UserID))";
$ldb->db_db_query($sql_temptable_create);

# Insert records into temporary table
$sql = "SELECT UserID, ELE, Hours FROM {$eclass_db}.OLE_STUDENT WHERE RecordStatus IN (2,4)";
$sql .= ($categoryID == "") ? "" : " AND Category = ".$categoryID;
$t_ole_rec_arr = $ldb->returnArray($sql);
for($i=0; $i<count($t_ole_rec_arr); $i++)
{
  $t_user_id = $t_ole_rec_arr[$i]["UserID"];
  $t_hours = $t_ole_rec_arr[$i]["Hours"];
  $t_rec_component_arr = explode(",", $t_ole_rec_arr[$i]["ELE"]);
  
  for($j=0; $j<count($t_rec_component_arr); $j++)
  {
    $t_rec_component = $t_rec_component_arr[$j];
    if($t_rec_component == "") continue;
    if(!array_key_exists($t_rec_component, $component_arr)) continue;
    
    $t_code_abbr = str_replace(array("[", "]"), "", $t_rec_component);
    $t_fieldname = "t_".$t_code_abbr;
  
    $ole_rec_arr[$t_user_id][$t_fieldname] += $t_hours;
  }
}

if(is_array($ole_rec_arr))
{
  foreach($ole_rec_arr AS $t_user_id => $t_ole_rec_arr)
  {
    $t_fieldname_arr = array_keys($t_ole_rec_arr);
    $t_hours_arr = array_values($t_ole_rec_arr);
    
    $t_fieldname_str = "UserID";
    $t_hours_str = $t_user_id;
    for($i=0; $i<count($t_fieldname_arr); $i++)
    {
      $t_fieldname_str .= ", ".$t_fieldname_arr[$i];
      $t_hours_str .= ", ".$t_hours_arr[$i];
    }
    
    $sql = "INSERT INTO tempOLEComponentSummay (".$t_fieldname_str.") VALUES (".$t_hours_str.")";
    $ldb->db_db_query($sql);
  }
}

# Get OLE Category
$lpf_ole_category = new libpf_ole_category();
$t_cat_arr = $lpf_ole_category->GET_CATEGORY_LIST();
for($i=0; $i<count($t_cat_arr); $i++)
{
  $t_id = $t_cat_arr[$i]['RecordID'];
  $t_title = ($intranet_session_language == "b5") ? $t_cat_arr[$i]['ChiTitle'] : $t_cat_arr[$i]['EngTitle'];

  $category_selection_arr[] = array($t_id, $t_title);
}
$category_selection_html = getSelectByArray($category_selection_arr, "name='categoryID' onChange='jCHANGE_FIELD()'", $categoryID, 1, 0, $ec_iPortfolio['all_category'], 2);

$lpf_fc = new libpf_formclass();
# Generate class level selection drop-down list
$t_form_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
if(is_array($classlevel_teach_arr))
{
  for($i=0; $i<count($t_form_arr); $i++)
  {
    $t_form_id = $t_form_arr[$i][0];
    if(in_array($t_form_id, $classlevel_teach_arr))
    {
      $form_arr[] = $t_form_arr[$i];
    }
  }
}
else
{
  $form_arr = $t_form_arr;
}
$year_selection_html = getSelectByArray($form_arr, "name='YearID' onChange='jCHANGE_FIELD()'", $YearID, 1, 0, $ec_iPortfolio['all_classlevel'], 2);

# Generate class selection drop-down list
$lpf_fc->SET_CLASS_VARIABLE("YearID", $YearID);
$default_ycid = "";
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
$class_name = $i_general_WholeSchool;
for($i=0; $i<count($t_class_arr); $i++)
{
  $t_yc_id = $t_class_arr[$i][0];
  $t_yc_title = $t_class_arr[$i][1];
  if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
  {
    $class_arr[] = $t_class_arr[$i];
  }
  
  if($t_yc_id == $YearClassID)
  {
    $default_ycid = $YearClassID;
  }
}
$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $default_ycid, 1, 0, $ec_iPortfolio['all_class'], 2);

if($search_name == $ec_iPortfolio['enter_student_name'])
	$search_name = "";
$cond .= ($search_name == "") ? "" : " AND (iu.ChineseName LIKE '%".$search_name."%' OR iu.EnglishName LIKE '%".$search_name."%')";
$cond .= ($YearID == "") ? "" : " AND yc.YearID = ".$YearID;
$cond .= ($default_ycid == "") ? "" : " AND yc.YearClassID = ".$default_ycid;

$overall_count_cond = ($componentCode == "") ? "" : " AND INSTR(ELE, '".$componentCode."') <> 0";
$overall_count_cond .= ($categoryID == "") ? "" : " AND Category = ".$categoryID;

if ($order=="") $order=1;
if ($field=="") $field=1;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql =  "
          SELECT
            CONCAT('<a href=\"javascript:newWindow(\'year_details.php?StudentID=', iu.UserID,'\', 94)\"  class=\"tablelink\">', ".getNameFieldByLang2("iu.").", '</a>') AS UserLink,
            yc.ClassTitleEN,
            iu.ClassNumber,
            IF(t_overall_count.ProgramCnt IS NULL, 0, t_overall_count.ProgramCnt) AS ProgramCnt,
            IF(t_overall_count.HourSum IS NULL, 0, t_overall_count.HourSum) AS HourSum
            $component_count_field,
            ".getNameFieldByLang2("iu.")." AS UserName
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          LEFT JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON  ycu.UserID = iu.UserID
          LEFT JOIN {$intranet_db}.YEAR_CLASS AS yc
            ON yc.YearClassID = ycu.YearClassID
          LEFT JOIN
            (
              SELECT
                UserID,
                count(DISTINCT ProgramID) AS ProgramCnt,
                SUM(Hours) AS HourSum
              FROM
                {$eclass_db}.OLE_STUDENT
              WHERE
                RecordStatus IN (2,4)
                $overall_count_cond
              GROUP BY
                UserID
            ) AS t_overall_count
          ON
            iu.UserID = t_overall_count.UserID
          LEFT JOIN
            tempOLEComponentSummay AS t_component_hour
          ON
            iu.UserID = t_component_hour.UserID
          WHERE
            iu.RecordType = 2 AND
            iu.RecordStatus = 1
            $cond
        ";

$LibTable->sql = $sql;
$LibTable->field_array = array("UserName", "ClassTitleEN", "ClassNumber", "ProgramCnt", "HourSum");
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 6 + count($component_arr);
$LibTable->fieldorder2 = ", ClassNumber ASC";
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 3, 3, 3);

$component_column_arr = array_fill(0, count($component_arr), 3);
array_splice($LibTable->column_array, count($LibTable->column_array), 0, $component_column_arr);

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' >".$LibTable->column(0,$i_UserName)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' >".$LibTable->column(1,$i_UserClassName)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' align='center'>".$LibTable->column(2,$i_UserClassNumber)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' align='center' class='tablebluetop seperator_left'>".$LibTable->column(3,$iPort['no_of_program'])."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' rowspan='2' align='center' class='tablebluetop seperator_left'>".$LibTable->column(4,$ec_iPortfolio['total_hours'])."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' colspan='".count($component_arr)."' align='center' class='tablegreentop tabletopnolink'>".$ec_iPortfolio['ele']."</td>\n";
$LibTable->column_list .= "</tr'>\n";
$LibTable->column_list .= "<tr class='tabletop'>\n";
# Component of OLE
foreach($component_arr AS $code => $title)
{
  if($componentCode != "" && $componentCode != $code) continue;

  $LibTable->column_list .= "<td align='center' class='tablegreentop tabletopnolink'>".$title."</td>\n";
}
$LibTable->column_list .= "</tr>\n";

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];
### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$TabMenuArr = array();
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['statistic'], 1);
/*
# Charts to be defined
if($special_feature['ole_chart'])
{
	$TabMenuArr[] = array("demo/index_stat.php", $ec_iPortfolio['chart_report'], 0);
	$TabMenuArr[] = array("demo/cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
}
*/
$TabMenuArr[] = array("index_analysis.php", $ec_iPortfolio['adv_analyze'], 0);

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">

function jCHANGE_FIELD(){
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "index_stat.php";
	document.form1.submit();
}

<?php if($special_feature['ole_chart']) { ?>
function jPOP_STAT_CHART()
{
  if(document.form1.YearClassID.value!='')
  {
    newWindow('class_stat_chart.php?YearClassID='+document.form1.YearClassID.value, 9);
  }
  else
  {
    alert('<?=$ec_warning['please_select_class']?>');
  }
<?php } ?>
}

</SCRIPT>

<FORM method='POST' name='form1' action='index_stat.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
      <?=$lpf_ui->GET_TAB_MENU($TabMenuArr)?>
			
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td>
												<?=$YearStr?>
												<?=$year_selection_html?>
												<?=$class_selection_html?>
<?php if($special_feature['ole_chart']) { ?>
												<a href="javascript:jPOP_STAT_CHART();"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/icon_resultview.gif" border="0" /></a>
<?php } ?>
											</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
										<td>
										<?=$category_selection_html?>
										<?=$component_selection_html?>
										</td>
										<td>&nbsp;</td>
										</tr>
									</table>
								</td>
								<td align="right" valign="bottom">
									<span class="tabletext">
										<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
										<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
							<?=$LibTable->displayOLEStatTable(array(3,4));?>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($LibTable->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="page_size_change" />
<input type="hidden" name="numPerPage" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
