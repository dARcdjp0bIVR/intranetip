<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-adminright.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-accessright.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-ole.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();

$lpf_amr = new libpf_adminright();
$lpf_amr->ACCESS_CONTROL("REPORT","OLE_STAT","VIEW");

$lpf_acr = new libpf_accessright();
switch($lpf_acr->GET_ACCESS_RIGHT("student_info"))
{
  # Subject teacher view class
  case 3:
  # Class teacher view class
  case 2:
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $_SESSION['iPortfolio']['portfolio_user_id']);
    $classlevel_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASSLEVEL();
    break;
  case 1:
  default:
    break;
}

$lpf_fc = new libpf_formclass();
# Generate class level selection drop-down list
$t_form_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
if(is_array($classlevel_teach_arr))
{
  for($i=0; $i<count($t_form_arr); $i++)
  {
    $t_form_id = $t_form_arr[$i][0];
    if(in_array($t_form_id, $classlevel_teach_arr))
    {
      $form_arr[] = $t_form_arr[$i];
    }
  }
}
else
{
  $form_arr = $t_form_arr;
}
$year_selection_html = getSelectByArray($form_arr, "name='YearID'", $YearID, 1, 0, $ec_iPortfolio['all_classlevel'], 2);

# Get OLE component
$lpf_ole_component = new libpf_ole_component();
$t_component_arr = $lpf_ole_component->GET_COMPONENT_LIST();
$component_check_html = "<input type=\"checkbox\" name=\"componentMaster\" id=\"componentMaster\" onClick=\"jCHECK_ALL_COMPONENT(this)\" /> <label for=\"componentMaster\">".$ec_iPortfolio['all_ele']."</label><br />\n";
for($i=0; $i<count($t_component_arr); $i++)
{
  $t_component_code = $t_component_arr[$i]['DefaultID'];
  $t_title = ($intranet_session_language == "b5") ? $t_component_arr[$i]['ChiTitle'] : $t_component_arr[$i]['EngTitle'];
  $t_id = "component_".$i;

  $component_check_html .= "<img src=\"$image_path/spacer.gif\" width=\"20\" height=\"1\" /><input type=\"checkbox\" name=\"componentCode[]\" id=\"".$t_id."\" value=\"".$t_component_code."\"> <label for=\"".$t_id."\">".$t_title."</label><br />\n";
}

$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$TabMenuArr = array();
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['statistic'], 0);
/*
# Charts to be defined
if($special_feature['ole_chart'])
{
	$TabMenuArr[] = array("demo/index_stat.php", $ec_iPortfolio['chart_report'], 0);
	$TabMenuArr[] = array("demo/cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
}
*/
$TabMenuArr[] = array("index_analysis.php", $ec_iPortfolio['adv_analyze'], 1);

$linterface->LAYOUT_START();
?>

<script language="JavaScript">

function checkform(obj){
/*
	if(obj.EndYear.value < obj.StartYear.value)
	{
		alert("<?=$ec_warning['start_end_year']?>");
		return false;
	}
*/
	if(countChecked(obj, "componentCode[]") == 0)
	{
		alert("<?=$msg_check_at_least_one?>");
		return false;
	}
	
	if(!check_positive_int(obj.Hour1, "<?=$ec_warning['please_enter_pos_integer']?>", "", 0))
		return false;

	if(obj.hour_range_type.value == "0")
	{
		if(!check_positive_int(obj.Hour2, "<?=$ec_warning['please_enter_pos_integer']?>", "", 0))
			return false;
	
		if(parseInt(obj.Hour2.value) < parseInt(obj.Hour1.value))
		{
			alert("<?=$ec_warning['start_end_hours']?>");
			obj.Hour2.focus();
			return false;
		}
	}

	return true;
}

function jDISPLAY_HOUR2()
{
	var HourRangeType = parseInt(document.getElementsByName("hour_range_type")[0].value);

	if(HourRangeType != 0)
		document.getElementById("Hour2Cell").style.display = "none";
	else
		document.getElementById("Hour2Cell").style.display = "block";
}

function jRESET_FORM()
{
  document.form1.reset();
  jDISPLAY_HOUR2();
}

function jCHECK_ALL_COMPONENT(jParcheckObj)
{
  var checked = jParcheckObj.checked;
  setChecked(checked, document.form1, "componentCode[]");
}

</script>

<FORM method='POST' name='form1' action='index_analysis_result.php' onSubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		  <?=$lpf_ui->GET_TAB_MENU($TabMenuArr)?>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td width="80%" valign="top">
									<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
											<td width="80%" valign="top">
												<?=$year_selection_html?>
											</td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_year']?></span></td>
											<td>
												<?=$StartYearStr?> <?=$profiles_to?> <?=$EndYearStr?>
											</td>
										</tr>
										<tr valign="top">
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['ele']?></span></td>
											<td>
												<?=$component_check_html?>
											</td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['hours_range']?></span></td>
											<td>
												<table border="0" cellspacing="0" cellpadding="3">
													<tr>
														<td><input name="Hour1" type="text" class="tabletext" style="width:35px" value="0"/></td>
														<td align="center">
															<select name="hour_range_type" class="formtextbox" onChange="jDISPLAY_HOUR2()">
																<option value="0"><?=$ec_iPortfolio['hours_to']?></option>
																<option value="1"><?=$ec_iPortfolio['hours_above']?></option>
																<option value="-1"><?=$ec_iPortfolio['hours_below']?></option>
															</select>
														</td>
														<td align="center" id="Hour2Cell"><input name="Hour2" type="text" class="tabletext" style="width:35px" value="0"/></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$ec_iPortfolio['view_analysis']?>" />
									<input type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="jRESET_FORM()" value="<?=$button_reset?>" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
