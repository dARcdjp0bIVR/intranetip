<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-adminright.php");
intranet_auth();
intranet_opendb();

$lpf_amr = new libpf_adminright();
$lpf_amr->ACCESS_CONTROL("SLP", "OFFICIAL_PHOTO", "UPLOAD");

$lf = new libfilesystem();

$import_file_name = $userfile_name;
if($import_file_name=="")
{
	header("Location: student_photo.php");
}

$ext = strtoupper($lf->file_ext($import_file_name));
$filename = $lf->file_name($import_file_name);

if ($ext==".ZIP")
{
	$tmp_dir = $intranet_root."/file/tmp_photo";
	$dest = $intranet_root."/file/official_photo";
	$lf->folder_new($tmp_dir);
	$lf->folder_new($dest);
	
	$success_count = 0;
	$fail_count = 0;
	if (file_exists($tmp_dir) && file_exists($dest))
	{
		rename($userfile, "{$userfile}.zip");
		$result_arr = $lf->file_unzip("{$userfile}.zip", $tmp_dir);
// 		exec("rm {$userfile}.zip");
		exec("rm ".OsCommandSafe($userfile)."zip");
		
		$handle=opendir($tmp_dir);
		while (($file = readdir($handle))!==false) 
		{
			if($file!="." && $file!="..")
			{
				$file_ext = $lf->file_ext($file);
				$file_name = $lf->file_name($file);

				if($file_ext==".JPG" || $file_ext==".jpg" || $file_ext==".jpeg" || $file_ext==".JPEG" || $file_ext==".jpe" || $file_ext==".JPE")
				{
					rename($tmp_dir."/".$file, $tmp_dir."/".$file_name.".jpg");
					chmod($tmp_dir."/".$file_name.".jpg", 0777);
					$success_count++;
				}
				else
				{
					$fail_count++;
				}
			}
		}

		chdir($tmp_dir);
// 		exec("mv *.jpg $dest");
		exec("mv *.jpg ".OsCommandSafe($dest));
		chdir($intranet_root."/file");
// 		exec("rm -r $tmp_dir");
		exec("rm -r ".OsCommandSafe($tmp_dir));
	}
	$params = "success_count=$success_count&fail_count=$fail_count";
}
else
{
	$params = "wrong_format=1";
}
intranet_closedb();

header("Location: student_photo.php?uploaded=1&$params");
?>
