<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.sunny.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once("campus_link_ui.php");
intranet_auth();
intranet_opendb();

$db =new libdb();

$headScript = "";
$newButton="";  
$newCampusLinkDialog = "";
$campusUI = new campus_link_ui();


if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1){
### script preparation
$headScript = <<<headerScript
<script language="Javascript">
	function moveUp(linkID){ 
		prevBlock = $('#CampusLinkSet-'+linkID).prev('div').get();		
		prevID = prevBlock[0].id.split("-");
		posY = $('#bubble_board_content').find('#campusLinkContent').scrollTop();
		//posY = document.getElementById("campusLinkContent").scrollTop;
		$('div#campusLinkContent').ajaxStart(
			function(){
				$('span#ajaxMsgBlock').text('$ip20_loading ...');
				$('span#ajaxMsgBlock').css("display","block");				
			});
		$('div#campusLinkContent').ajaxError(
			function(){
				$('span#ajaxMsgBlock').text('$i_CampusLink_connect_fail');
				$('span#ajaxMsgBlock').css("display","block");
				setTimeout(hiddenMsgBlock,1000);
			});
		$('div#campusLinkContent').load(serverURL,{"loadType":"move","upLinkID":linkID,"downLinkID":prevID[1],"posY":posY},function(){
			$('span#ajaxMsgBlock').text('$i_CampusLink_moveUp_success');
			$('span#ajaxMsgBlock').css("display","block");
			setTimeout(hiddenMsgBlock,1000);
		});
		
	}
	
	function moveDown(linkID){ 
		nextBlock = $('#CampusLinkSet-'+linkID).next('div').get();		
		nextID = nextBlock[0].id.split("-");
		posY = $('#bubble_board_content').find('#campusLinkContent').scrollTop();
		$('div#campusLinkContent').ajaxStart(
			function(){
				$('span#ajaxMsgBlock').text('$ip20_loading ...');
				$('span#ajaxMsgBlock').css("display","block");				
			});
		$('div#campusLinkContent').ajaxError(
			function(){
				$('span#ajaxMsgBlock').text('$i_CampusLink_connect_fail');
				$('span#ajaxMsgBlock').css("display","block");
				setTimeout(hiddenMsgBlock,1000);
			});
		$('div#campusLinkContent').load(serverURL,{"loadType":"move","upLinkID":nextID[1],"downLinkID":linkID,"posY":posY},function(){
			$('span#ajaxMsgBlock').text('$i_CampusLink_moveDown_success');
			$('span#ajaxMsgBlock').css("display","block");
			setTimeout(hiddenMsgBlock,1000);
		});
		
	}
	
	
	function Show_CompusLink_edit_box(data) {
		Hide_Return_Message();
		
		$.post(
			"/home/campus_link/Get_Link_Mangement_Box.php", 
			{ boxMode: data.mode, linkID: data.linkID },
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				//js_Init_JEdit();
				//js_Init_DND_Table();
				//$('#debugArea').html(ReturnData);
				
				// initialize the add box if the user click the add button
				//if (WithAdd == 1)
					//js_Add_Building_Row();
			}
		);
	}
	
	function newCampusSetting(obj){						
		$(obj).addClass("current");  
		$("div#CampusLinkSettingBlock").slideDown("slow");
	}
	function closeNewCampusSetting(){
		$("a#campusSettingButton").removeClass("current");
		$("div#CampusLinkSettingBlock").slideUp("fast");
	}
	function openCampusSetting(obj){
		$(obj).addClass("current");
		linkid = (obj.id).split("-");
		$('div#CampusLinkSettingBlock-'+linkid[1]).slideDown("slow");
	}
	function closeCampusSetting(linkID){
		$("a#campusSettingButton-"+linkID).removeClass("current");
		$('div#CampusLinkSettingBlock-'+linkID).slideUp("fast");
	}
	function insertNewCampusLink(){
		linktitle = document.getElementById("linkTitle_new").value; 
		linkurl = document.getElementById("linkURL_new").value;
		if (linktitle == "" || linkurl == ""){
			alert("$i_CampusLink_validate");
			return;
		}
		temp = linkurl.toLowerCase();
		if (!temp.match("^http://")&&!temp.match("^https://")&&!temp.match("^ftp://")){
			alert("$i_CampusLink_wrong_addr");
			return;
		}
		
		serverURL = "/home/campus_link/campus_link_load_content.php";
		$('div#campusLinkContent').ajaxStart(
			function(){
				$('span#ajaxMsgBlock').text('$ip20_loading ...');
				$('span#ajaxMsgBlock').css("display","block");				
			});
		$('div#campusLinkContent').ajaxError(
			function(){
				$('span#ajaxMsgBlock').text('$i_CampusLink_connect_fail');
				$('span#ajaxMsgBlock').css("display","block");
				setTimeout(hiddenMsgBlock,1000);
			});
		$('div#campusLinkContent').load(serverURL,{"loadType":"insert","title":linktitle,"url":linkurl},function(){
			$('span#ajaxMsgBlock').text('$i_con_gen_msg_add');
			$('span#ajaxMsgBlock').css("display","block");
			document.getElementById("linkTitle_new").value="";
			document.getElementById("linkURL_new").value="";
			setTimeout(hiddenMsgBlock,1000);
		});
	}
	
	function hiddenMsgBlock(){
		$('span#ajaxMsgBlock').css("display","none");
	}
	
	function deleteCampusSetting(linkID){
		y = confirm("$i_CampusLink_delete_warning:\\n\\n"+document.getElementById("campus_link-"+linkID).innerHTML);
		if (!y)
			return;
		$().ajaxStart(
			function(){
				$('span#ajaxMsgBlock').text('$ip20_loading ...');
				$('span#ajaxMsgBlock').css("display","block");
			});
		$.ajax({
			type: "POST",
			url: "/home/campus_link/campus_link_load_content.php",
			dataType: "xml",
			data: "loadType=delete&linkID="+linkID,
			success: function(xml){
				deletestatus = $(xml).find("status").text();
				if (deletestatus == "success"){
					deleteid = $(xml).find("linkID").text();
					$('span#ajaxMsgBlock').text('$i_con_gen_msg_delete');					
					$('span#ajaxMsgBlock').css("display","block");					
					$('#CampusLinkSet-'+deleteid).remove();
					linkSet = $('div[id^="CampusLinkSet-"]');
					if (linkSet.get().length == 1){
						linkSet.find('a[id^="moveUp-"]').css("display","none");
						linkSet.find('a[id^="moveDown-"]').css("display","none");
					}
					else{
						lastlinkSet = $('div[id^="CampusLinkSet-"]:last').find('a[id^="moveDown-"]').css("display","none");
					}
					setTimeout(hiddenMsgBlock,1000);					
				}
				else{
					$('span#ajaxMsgBlock').text('$i_CampusLink_delete_fail');
					$('span#ajaxMsgBlock').css("display","block");
					setTimeout(hiddenMsgBlock,1000);
				}
			},
			error: function(response){
				$('span#ajaxMsgBlock').text('$i_CampusLink_connect_fail');
				setTimeout(hiddenMsgBlock,1000);
			}
		})
	}
	function editCampusSetting(linkID){
		$().ajaxStart(
			function(){
				$('span#ajaxMsgBlock').text('$ip20_loading ...');
				$('span#ajaxMsgBlock').css("display","block");
			});
		linktitle = document.getElementById("linkTitle-"+linkID).value;
		linkurl = document.getElementById("linkURL-"+linkID).value;
		if (linktitle == "" || linkurl == ""){
			alert("$i_CampusLink_validate");
			return;
		}
		temp = linkurl.toLowerCase();
		if (!temp.match("^http://")&&!temp.match("^https://")&&!temp.match("^ftp://")){
			alert("$i_CampusLink_wrong_addr");
			return;
		}
		$.post("/home/campus_link/campus_link_load_content.php",{"loadType":"edit","linkID":linkID,"title":linktitle,"url":linkurl},function(xmlDoc){
			editstatus = $(xmlDoc).find("status").text();	
			if (editstatus == "success"){
				editid = $(xmlDoc).find("linkID").text();
				
				edittitle = $(xmlDoc).find("title").text();
				edittitle = edittitle.replace(/\\\\\\\\/g,"\\\");
				edittitle = edittitle.replace(/\\\'/g,"'");
				edittitle = edittitle.replace(/\\\"/g,'"');
				editurl = $(xmlDoc).find("url").text();
				editurl = editurl.replace(/\\\\\\\\/g,"\\\");
				editurl = editurl.replace(/\\\'/g,"'");
				editurl = editurl.replace(/\\\"/g,'"');
				
				$('span#ajaxMsgBlock').text('$i_con_gen_msg_update');
				$('span#ajaxMsgBlock').css("display","block");
				$("#campus_link-"+linkID).text(edittitle);
				document.getElementById("campus_link-"+linkID).href = editurl;
				document.getElementById("linkTitle-"+linkID).value = edittitle;
				document.getElementById("linkURL-"+linkID).value = editurl;
				closeCampusSetting(linkID);
				setTimeout(hiddenMsgBlock,1000);					
			}
			else{
				$('span#ajaxMsgBlock').text('$i_CampusLink_edit_fail');
				$('span#ajaxMsgBlock').css("display","block");
				setTimeout(hiddenMsgBlock,1000);
			}
		},"xml");
		
	}
	
</script>
headerScript;


### UI preparation
$newButton = '<br style="clear:both" />'.$campusUI->getNewCampusLinkButton();

//$newCampusLinkDialog = $campusUI->getNewCampusLinkDialogBox();


}

/*
if (trim($VisibleToValues)=="")
{
	$VisibleToValues = "NoBody";
}
*/


$header = "";
switch($loadType){
	case "whole":
//		<link href="/templates/'.$LAYOUT_SKIN.'/css/content_25.css" rel="stylesheet" type="text/css">
		$header = '
		<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
		<script type="text/javascript" src="/templates/jquery/jquery.blockUI.js"></script>
		<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

				<div class="board_title">
					<h1 style="font-size:12px">'.$i_CampusLink_title.'</h1>
					
					'.$newButton.'
					<br style="clear:both" />
				</div>
				<div class="board_title_setting" id="CampusLinkSettingBlock" style="display:none">
					<div class="form_field" style="padding:5px">'.$i_CampusLink_add.'</div>
					<p class="spacer"> </p>&nbsp;
					'.$newCampusLinkDialog.'		
				</div>
				'.$headScript.'
				<br style="clear:both">
'; 
	break;
	case "insert":
		if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1){
			$sql = "select DisplayOrder from INTRANET_CAMPUS_LINK order by DisplayOrder DESC";
			$resultSet = $db->returnArray($sql);
			if (empty($resultSet)){
				$newOrder = 0;
			}
			else{
				$newOrder = $resultSet[0]["DisplayOrder"]+1;
			}
			$linkTitle = $db->Get_Safe_Sql_Query(intranet_htmlspecialchars($title));
			$linkURL = $db->Get_Safe_Sql_Query(intranet_htmlspecialchars($url));
			$sql = "insert into INTRANET_CAMPUS_LINK(`Title`,`URL`,`DateInput`,`InputBy`,`DateModified`,`ModifiedBy`, `DisplayOrder`, `VisibleTo`) values ('".$linkTitle."','".$linkURL."',now(),'".$_SESSION["UserID"]."',now(),'".$_SESSION["UserID"]."', ".$newOrder.", '".$VisibleToValues."')";
			$result = $db->db_db_query($sql);				
		}
	break;
	case "edit":
		if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1){
			$linkTitle = $db->Get_Safe_Sql_Query(intranet_htmlspecialchars($title));
			$linkURL = $db->Get_Safe_Sql_Query(intranet_htmlspecialchars($url));
			$sql = "update INTRANET_CAMPUS_LINK set ";
			$sql .= "`Title`='".$linkTitle."',";
			$sql .= "`URL`='".$linkURL."',";
			$sql .= "`ModifiedBy`='".$_SESSION["UserID"]."',";
			$sql .= "`VisibleTo`='".$VisibleToValues."',";
			$sql .= "`DateModified`= now() ";
			$sql .= "where `LinkID` ='".$linkID."'";
			$result = $db->db_db_query($sql);	
			
			/*header("Content-Type: text/xml;");
			header("Cache-Control: no-cache");
			echo '<?xml version="1.0" encoding="utf-8"?>';
			echo '<returnResult>';
			echo '<status>'.($result==1?"success":"fail").'</status>';
			echo '<linkID>'.trim($linkID).'</linkID>';	
			echo '<title>'.intranet_htmlspecialchars($title).'</title>';		
			echo '<url>'.intranet_htmlspecialchars($url).'</url>';
			echo '</returnResult>';*/
		}
	break;
	case "delete":
		if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1){
			$sql = "select DisplayOrder from INTRANET_CAMPUS_LINK
						where LinkID ='".$linkID."'";
			$order = $db->returnVector($sql);
			$sql = "update INTRANET_CAMPUS_LINK 
					set DisplayOrder = DisplayOrder - 1
					where DisplayOrder > '".$order[0]."'
					";
			$result['update'] = $db->db_db_query($sql);
			$sql="delete from INTRANET_CAMPUS_LINK where `LinkID` ='".$linkID."'";
			$result['delete'] = $db->db_db_query($sql);
			header("Content-Type: text/xml;");
			header("Cache-Control: no-cache");
			echo '<?xml version="1.0" encoding="utf-8"?>';
			echo '<returnResult>';
			echo '<status>'.(!in_array(false,$result)?"success":"fail").'</status>';
			echo '<linkID>'.trim($linkID).'</linkID>';					
			echo '</returnResult>';
		}
	return;
	case "move":
		if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1){
			$sql = "update INTRANET_CAMPUS_LINK set DisplayOrder = DisplayOrder - 1 where LinkID = ".trim($upLinkID);
			$result["upLink"] = $db->db_db_query($sql);
			$sql = "update INTRANET_CAMPUS_LINK set DisplayOrder = DisplayOrder + 1 where LinkID = ".trim($downLinkID);
			$result["downLink"] = $db->db_db_query($sql);
		}
		
	break;
};



$sql = "select * from INTRANET_CAMPUS_LINK order by DisplayOrder";

$resultSet = $db->returnArray($sql);
$linkInfo ="";
if ($loadType=="whole")
	$linkInfo = "<div id='campusLinkContent' style='height:250px; overflow:auto'>";

$i = 0;

$libuser = new libuser($UserID);

if ($libuser->isParent())
{
	$VisibleKeyword = "PARENT";
} elseif ($libuser->isStudent())
{
	$VisibleKeyword = "STUDENT";
} elseif ($libuser->isTeacherStaff())
{
	$VisibleKeyword = ($libuser->teaching) ? "STAFF_T" : "STAFF_NT";
}

foreach($resultSet as $result)
{
	if ($i == count($resultSet)-1)
		$i = -1; #The last one
	else if (count($resultSet)== 1)
		$i = -2; #only one item
	$linkID = $result["LinkID"];
	$title = $result["Title"];
	$url = $result["URL"];
	$visibleTo = $result["VisibleTo"];

	if(trim($title)=="")
		$title = $url;
	//if ($_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1 || )
	if ($visibleTo=="ALL" || $_SESSION["SSV_USER_ACCESS"]["other-campusLink"] == 1 || (strstr($visibleTo, $VisibleKeyword) && $visibleTo!="" && $VisibleKeyword!=""))
	{
		$linkInfo .= $campusUI->getCampusLinkSet($linkID,$title,$url,$i,$visibleTo);
	}
	$i++;
};
if ($loadType=="whole")
	$linkInfo .="</div>";
$linkInfo .="
		<script language='javascript'>
		// re-initialize thickbox due to ajax
		$('#bubble_board_content').find('#campusLinkContent').scrollTop( $posY )
		tb_init('a.thickbox, area.thickbox, input.thickbox');
		</script>";

intranet_closedb();

$content = $header.$linkInfo;

echo $content;
//echo debug_r($_SESSION);
?>