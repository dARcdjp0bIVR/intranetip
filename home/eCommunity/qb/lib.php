<?php

// display contact information if the IP (in system/global.php) is not set properly
if($HTTP_SERVER_VARS["SERVER_ADDR"] != $irc_ip) 
     { die("The server configuration has been modified.<br>Please contact <a href='mailto:support@broadlearning.com'>BroadLearning Technical Services</a>."); }

session_start();

function debug($strr) {
	echo $strr . " <i>(debug)</i><br>\n";
}

/* YuEn: temporary use fixed language and scheme */
$ck_default_lang = "eng";
$ck_scheme_id = 4;

# derived system setting
$admin_url_path = "";
$admin_email_path = $webmaster;
$admin_root_path = $eclass_root;
//YuEn: IRC chat bug-fixed
$chatIP = (isset($irc_dns)) ? $irc_dns : $irc_ip;
$sys_db = $eclass_db;
$db_login = $eclass_db_login;
$db_passwd = $eclass_db_passwd;
$image_path = "$admin_url_path/images";
//$src_image_path = "$admin_url_path/src/images";
$file_path_name = "files";
$file_path = "$admin_root_path/$file_path_name";
include("$admin_root_path/src/includes/php/skins.php");
$skin_path = "$admin_url_path/system/skins/".$skins[$ck_scheme_id - 1][0];

# some maybe useful variables

//skin images
$BackTitle = "$skin_path/bar_32.gif";
$BackH200 = "$skin_path/bar_200h0.gif";

// page size
$page_size=10;
// file upload ereg string
$uploadFileEregStr = "[^a-zA-Z0-9._~ -]";
// language checking
if(!isset($ck_default_lang)) $ck_default_lang = "eng";

# essential function - Charles
function classNamingDB($cid){
	global $sys_db, $eclass_prefix;
	if ($cid==$sys_db)
		return $eclass_prefix.$sys_db;
	else
		return $eclass_prefix."c".$cid;
}

function undo_htmlspecialchars($string){
	$string=str_replace("&amp;", "&", $string);
	$string=str_replace("&quot;", "\"", $string);
	$string=str_replace("&lt;", "<", $string);
	$string=str_replace("&gt;", ">", $string);
	return $string;
}

function IsEClassModeIntranet(){
	global $eClassMode;
	return ($eClassMode == "I") ? 1 : 0;
}


// authentication 
function auth($m){
	global $ck_course_id;
	global $ck_user_id;
	global $ck_memberType;
	global $ck_login_session_id;
	global $ck_user_email;
	global $admin_url_path;
	global $HTTP_SERVER_VARS;
	// Authenticate User
	if($ck_memberType==""){
		header("Location: $admin_url_path/relogin.php");
	}else if(strstr($m , $ck_memberType)){
		if($ck_user_id=="" || $ck_course_id==""){
			header("Location: $admin_url_path/relogin.php");
		}else{
			// Update Login_session
			opendb();
			$function_name = $HTTP_SERVER_VARS["SCRIPT_NAME"];
			$sql="INSERT INTO login_usage (login_session_id, function_name, inputdate) VALUES ($ck_login_session_id, '$function_name', now())";
			mysql_db_query(classNamingDB($ck_course_id), $sql);
			$sql="UPDATE login_session SET modified = now() WHERE login_session_id=$ck_login_session_id";
			mysql_db_query(classNamingDB($ck_course_id), $sql);
			closedb();
		}
	}else{
		header("Location: $admin_url_path/access.php");
	}
}

function opendb(){
	global $db_login, $db_passwd;
	mysql_connect("localhost" , $db_login, $db_passwd);
}

function closedb(){
	mysql_close();
}

// header cache refreshing
function cache(){
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");				// Date in the past
	header("Last-Modified:".gmdate("D, d M Y H:i:s")." GMT");		// always modified
	header("Cache-Control: no-cache, must-revalidate");				// HTTP/1.1
	header("Pragma: no-cache");										// HTTP/1.0
}


function getSkinCSS($id, $new){
	global $admin_root_path;
	include("$admin_root_path/src/includes/php/skins.php");
	if($id<=(sizeof($skins)) && $id>0)
		$x = ($new==1) ? "skins/".$skins[($id-1)][0]."/style_new.css" : "skins/".$skins[($id-1)][0]."/style.css";
	return $x;
}


// browser info
if ( ereg("MSIE", getenv("HTTP_USER_AGENT")) ) 
	$browser = "MSIE";
else
	$browser = "NS";
// browser checking
$version=(int) substr($HTTP_USER_AGENT, strpos($HTTP_USER_AGENT, "/")+1, 1);
if($version < 4){
header("Location: $admin_url_path/upgrade.php");
}
$NS4 = ($browser == "NS" && $version == 4);

// UI related files

function getIEbg($id) {
	global $admin_root_path;
	include("$admin_root_path/src/includes/php/skins.php");
	if($id<=(sizeof($skins)) && $id>0)
{
		$x = "skins/".$skins[($id-1)][0]."/".$skins[($id-1)][1];
	}
	else
		$x = " ";
	return $x;
}

function head(){
	global $admin_url_path, $ck_default_lang, $ck_scheme_id, $image_path, $NS4;

	cache();
	$x  = "\n\n\n\n\n\n\n";
	$x .= "<html><head><title>eclass</title>\n";
	// Disable browser's cache if Microsoft Explorer
	$browser = getenv("HTTP_USER_AGENT");
	if(strpos($browser,"MSIE ")) $x .= "<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>\n";
	if ($ck_default_lang=="chigb")
		$x .= "<META http-equiv='Content-Type' content='text/html; charset=GB2312'>\n";
	else
		$x .= "<META http-equiv='Content-Type' content='text/html; charset=big5'>\n";
	$x .= "<link rel=stylesheet href='$admin_url_path/system/".getSkinCSS($ck_scheme_id, 1)."'>\n";
	// ONLY APPLY BACKGROUND PATTERN FOR non-NS4 browsers to avoid display problem in NS4
	$iebg = $admin_url_path."/system/".getIEbg($ck_scheme_id);
	$x .= ($NS4) ? "" : "\n<STYLE TYPE='text/css'> .bodycolor {BACKGROUND-IMAGE: url('$iebg');}</STYLE>\n";
	$x .= "<script language=JavaScript src='$admin_url_path/system/settings/lang/js/".$ck_default_lang."script.js'></script>\n";
	$x .= "<script language=JavaScript src='$admin_url_path/src/includes/js/script.js'></script>\n";
	$x .= "</head>\n\n\n\n\n";

	if (func_num_args()>0) {
		if (func_get_arg(0)=="1") {
			$x .= "<STYLE TYPE='text/css'>\n";
			$x .= "BODY {background-color: #F0FFF0; background-image:url('$iebg')}\n";
			$x .= "A:link { COLOR: black; }\n";
			$x .= "A:visited { COLOR: black;}\n";
			$x .= "A:hover {COLOR: #008800;}\n";
			$x .= "A:active { COLOR: black;}\n";
			$x .= "</STYLE>\n";	
			$x .= "<body leftmargin='5' topmargin='5' marginwidth='5' marginheight='5'>\n";
		} elseif (func_get_arg(0)=="2") {
			$x .= "<STYLE TYPE='text/css'>\n";
			$x .= "BODY {background-color: #F0FFF0; background-image:url('$iebg')}\n";
			$x .= "</STYLE>\n";	
			$x .= "<body leftmargin='5' topmargin='5' marginwidth='5' marginheight='5'>\n";
		} elseif (func_get_arg(0)=="3") {
			$x .= "<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>\n";
		} elseif (func_get_arg(0)=="4") {
			// contents, resources (frame content with status)
			$x .= "<STYLE TYPE='text/css'>\n";
			$x .= "BODY {background-color: #F0FFF0; background-image:url('$iebg')}\n";
			$x .= ".backpos {top: 11px;	left: 10px;}\n";
			$x .= ".frontpos {top: 10px; left: 10px;}\n";
			$x .= "</STYLE>\n";	
			$x .= "<body leftmargin='5' topmargin='5' marginwidth='5' marginheight='5'>\n";
		} elseif (func_get_arg(0)=="5") {
			// files (browse)
			$x .= "<STYLE TYPE='text/css'>\n";
			$x .= "BODY {background-color: #F0FFF0; background-image:url('$iebg')}\n";
			$x .= "</STYLE>\n";	
			$x .= "<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>\n";
		} elseif (func_get_arg(0)=="6") {
			// pop-up with border and status w/o icon
			$x .= "<STYLE TYPE='text/css'>\n";
			$x .= ".backpos {top: 11px;	left: 22px;}\n";
			$x .= ".frontpos {top: 10px; left: 22px;}\n";
			$x .= "</STYLE>\n";	
			$x .= "<body leftmargin='3' topmargin='0' marginwidth='3' marginheight='0'>\n";
			$x .= "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
			$x .= "<tr>\n";
			$x .= "<td width='5' align='right' background='$image_path/frame/border1_up_left.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
			$x .= "<td width='100%' background='$image_path/frame/border1_bottom.gif'><img src='$image_path/space.gif' width='1' height='5'></td>\n";
			$x .= "<td background='$image_path/frame/border1_up_right.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
			$x .= "</tr>\n";
			$x .= "<tr>\n";
			$x .= "<td width='5' background='$image_path/frame/border1_left.gif'><img src='$image_path/space.gif' width='10' height='100'></td>\n";
			$x .= "<td class='bodycolor' valign=top>\n";
		}
	} else {
		$x .= "<body leftmargin='3' topmargin='0' marginwidth='3' marginheight='0'>\n";
		$x .= "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
		$x .= "<tr>\n";
		$x .= "<td width='5' align='right' background='$image_path/frame/border1_up_left.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
		$x .= "<td width='100%' background='$image_path/frame/border1_bottom.gif'><img src='$image_path/space.gif' width='1' height='5'></td>\n";
		$x .= "<td background='$image_path/frame/border1_up_right.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
		$x .= "</tr>\n";
		$x .= "<tr>\n";
		$x .= "<td width='5' background='$image_path/frame/border1_left.gif'><img src='$image_path/space.gif' width='10' height='100'></td>\n";
		$x .= "<td class='bodycolor' valign=top>\n";
	}
		
	echo $x;
}

function getBorder2_s() {
//arguments: width, align
	global $image_path;

	$para = array("", "center");
	$numargs = func_num_args();
	for ($i=0; $i<$numargs; $i++) {
		$para[$i] = func_get_arg($i);
	}

	$x = "<table width='".$para[0]."' border='0' align='".$para[1]."' cellpadding='0' cellspacing='0'>\n";
	$x .= "<tr>\n";
	$x .= "<td width='5' align='right' ><img src='$image_path/frame/border2_up_left.gif' width='9' height='5'></td>\n";
	$x .= "<td width='100%' background='$image_path/frame/border2_bottom.gif'><img src='$image_path/space.gif' width='1' height='5'></td>\n";
	$x .= "<td><img src='$image_path/frame/border2_up_right.gif' width='9' height='5'></td>\n";
	$x .= "</tr>\n";
	$x .= "<tr>\n";
	$x .= "<td width='5' background='$image_path/frame/border2_left.gif'><img src='$image_path/space.gif' width='9' height='10'></td>\n";
	//$x .= "<td class='bodycolor'>\n";
	
	echo $x;
}

function getBorder2_e() {
	global $image_path;

	$numargs = func_num_args();

	$x = "<td background='$image_path/frame/border2_right.gif'>&nbsp;</td>\n";
	$x .= "</tr>\n";
	$x .= "<tr>\n";
	$x .= "<td><img src='$image_path/frame/border2_bt_left.gif' width='9' height='5'></td>\n";
	$x .= "<td background='$image_path/frame/border2_bottom.gif'><img src='$image_path/space.gif' width='1' height='5'></td>\n";
	$x .= "<td><img src='$image_path/frame/border2_bt_right.gif' width='9' height='5'></td>\n";
	$x .= "</tr>\n";

	if ($numargs<2) {
		$x .= "<tr>\n";
		$x .= "<td colspan='3'><img src='/images/shading.gif' width='98%' border='0' height='10'></td>\n";
		$x .= "</tr>\n";
	} else {
		$x .= "<tr><td colspan='3'>\n";
		
		$para = array("", "center");
		$numargs = func_num_args();
		for ($i=0; $i<$numargs; $i++) {
			$para[$i] = func_get_arg($i);
		}

		$x .= "<table width='100%' border='0' align='".$para[0]."' cellpadding='0' cellspacing='0'>\n";
		$x .= "<tr>\n";
		$x .= "<td width='100%' valign='top'><img src='/images/shading.gif' width='100%' height='13'></td>\n";
		$x .= "<td align='right'><img src='$image_path/frame/border2_ft_left.gif' width='27' height='25'></td>\n";
		$x .= "<td nowrap background='$image_path/frame/border2_footer.gif'>\n";
		$x .= $para[1];
		$x .= "</td><td><img src='$image_path/frame/border2_ft_right.gif' width='25' height='25'></td>\n";
		$x .= "</tr>\n";
		$x .= "<tr><td colspan='2'>&nbsp;</td><td valign='top'><img src='/images/shading.gif' width='100%' height='5'></td><td>&nbsp;</td></tr></table>\n";
		
		$x .= "</td></tr>\n";
	}
	$x .= "</table>\n";
	echo $x;
}

function foot(){
	global $image_path;
	
	$numargs = func_num_args();

	$x  = "</td>\n";
	$x .= "<td background='$image_path/frame/border1_left.gif'><img src='$image_path/space.gif' width='10' height='1'></td>\n";
	$x .= "</tr>\n";
	$x .= "<tr>\n";
	$x .= "<td id=\"bottom\" background='$image_path/frame/border1_bt_left.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
	$x .= "<td background='$image_path/frame/border1_bottom.gif'><img src='$image_path/space.gif' width='1' height='5'></td>\n";
	$x .= "<td background='$image_path/frame/border1_bt_right.gif'><img src='$image_path/space.gif' width='10' height='5'></td>\n";
	$x .= "</tr>\n";
	$x .= "</table>\n";

	// DISPLAY BOTTOM-LEFT MENU (optional)
	for ($i=0; $i<$numargs; $i++)
		$x .= func_get_arg($i);

	$x .= "</body>\n";
	$x .= "\n\n\n\n";

	$x .= "</html>\n";
	$x .= "\n\n\n\n";

	echo $x;
}


function getBottomMenu() {
// args: text, link
	global $image_path, $page_top;

	$numargs = func_num_args();
	//if ($numargs%2!=0) return;
	$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>\n";
	$x .= "<td align='right'><img src='$image_path/frame/border1_ft_left.gif' width='35' height='20'></td>\n";
	$x .= "<td background='$image_path/frame/border1_footer.gif' nowrap>&nbsp;\n";

	// construct links
	for ($i=0; $i<$numargs; $i+=2)
		$x .= (func_get_arg($i+1)!="") ? " <strong><a href='".func_get_arg($i+1)."'>".func_get_arg($i)."</a></strong> | \n" : 
			" <strong>".func_get_arg($i)."</strong> | \n";

	$x .= "<a href='#top'><img src='$image_path/icon/page_top.gif' alt='".$page_top."' border='0' align='top'></a>&nbsp;</td>\n";
	$x .= "<td width='100%'><div align='left'><img src='$image_path/frame/border1_ft_right.gif' width='20' height='20'></div></td>\n";
	$x .= "</tr></table>\n";

	return $x;
}


function getStatus() {
// args: text, link
	global $image_path, $NS4;

	$numargs = func_num_args();
	if ($numargs%2!=0)
		return;
	$x = "";
	// construct links
	for ($i=0; $i<$numargs; $i+=2) {
		
		$y .= ($y!="") ? "<span class='gtitle9'> >>> </span>" : "";
		$y .= "<span class='gtitle9'>".func_get_arg($i)."</span>\n";
		
		$x .= ($x!="") ? "<Script language='JavaScript'>gradientArrow('>>>')</Script>\n" : "";
		
		$x .= (func_get_arg($i+1)!="") ? 
			" <a href='".func_get_arg($i+1)."'><Script language='JavaScript'>gradientTitle('".func_get_arg($i)."')</Script></a> \n" : 
			"<Script language='JavaScript'>gradientTitle('".func_get_arg($i)."')</Script>\n";
	}
	$z = "";
	$z .= ($NS4) ? $x : "<span class='backpos'>".$y."</span><span class='frontpos'>".$x."</span>\n";
	return $z;
}


function displayStatusAndMenu($status, $menu) {
	global $image_path, $page_bottom;

	$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
	$x .= "<tr><td width='100%'>\n";
	$x .= $status;
	$x .= "</td>";

	if ($menu!="%&%") {
		$x .= "<td valign='top'>\n";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
		$x .= "<tr><td width='100%' align='right' valign='top'>\n";
		$x .= "<img src='$image_path/frame/border2_ft_left.gif' width='27' height='25'></td>\n";
		$x .= "<td height='5' nowrap background='$image_path/frame/border2_footer.gif'>\n";
		$x .= $menu;
		$x .= "\n<a href='#bottom'><img src='$image_path/icon/page_bottom.gif' alt='$page_bottom' hspace='3' vspace='2' border='0'></a>\n";
		$x .= "</td></tr></table></td>\n";
	}
	$x .= "</tr></table>\n";

	echo $x;
}

function displayIntroAndSearch($intro, $search) {
	global $image_path;

	$x  = "<table width='94%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
	$x .= "<tr>\n";
	$x .= "<td width='350' nowrap class='intro'>".$intro."</td>\n";
	$x .= "<td align='right'>".$search."</td>\n";
	$x .= "</tr>\n";
	$x .= "<tr><td>&nbsp;</td></tr>\n";
	$x .= "</table>\n";

	echo $x;
}

function displaySysMsg($msg) {
	global $button_remove;

	$myClass = (strstr($msg, $button_remove)) ? "notice2" : "notice1";
	$x = "<table><tr><td class='bodycolor2'>&nbsp;&nbsp;<span class='$myClass'>\n";
	$x .= $msg . "</span>\n &nbsp;&nbsp;</td></tr></table>\n";
	echo $x;
}
?>
