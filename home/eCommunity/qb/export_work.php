<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libqb.php");
include_once ("../../../includes/libgroup.php");
include_once ("../../../includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$lq = new libqb();
$lg = new libgroup($GroupID);
$lf = new libfilesystem();

if ($format != 2 && ($qb_record_score && !$lg->hasAdminQB($UserID) ))            # Not reset pack
{
    # Check the point enough or not
    $user_score = $lq->returnUserScore($UserID);
    $bucket_score = $lq->returnScoreInBucket();
    $left_score = $user_score - $bucket_score;
    if ($left_score < 0)
        $action_valid = false;
    else
        $action_valid = true;
}
else
{
    $action_valid = true;
}

if ($action_valid)
{

if ($format == 0)
{
    # Export word
    /*
         - get number of qs in bucket
         - make temp dir
           - make sub dir if num of qs != 0
         for @ qtype
           - select file location from DB
           - copy files to temp dir/subdir
         - zip to temp file
         - remove temp dir
         - redirect to temp zip file
    */

    # Retrieve data in bucket
    $num_qs = $lq->returnQtyInBucket();
    $qids = $lq->returnBucket();

    # Build directory
    $qb_tempdir = "$intranet_root/file/export/qb";
    if (!is_dir($qb_tempdir)) $lf->folder_new($qb_tempdir);
    $unique_suf = session_id()."-".time();
    $documents_tempdir = "$qb_tempdir/$unique_suf";
    if (!is_dir($documents_tempdir)) $lf->folder_new($documents_tempdir);
    for ($i=0; $i<sizeof($num_qs); $i++)
    {
         if ($num_qs[$i]!=0)
         {
             $subdir = "$documents_tempdir/".$lq->directoryNames[$i];
             $lf->folder_new($subdir);
             $originals = $lq->getOriginals($qids[$i]);
             for ($j=0; $j<sizeof($originals); $j++)
             {
                  list ($temp_id,$eng,$chi) = $originals[$j];
                  $eng_path = "$intranet_root/file/qb/g$GroupID/$eng";
                  $chi_path = "$intranet_root/file/qb/g$GroupID/$chi";
                  if (is_file($eng_path))
                  {
                      $lang_path = $eng_path;
                      $filename = $lf->file_name($lang_path);
                      $fileext = $lf->file_ext($lang_path);
                      $filename = substr($filename,0,1).$temp_id.substr($filename,1).$fileext;
                      $lf->file_copy($lang_path,"$subdir/$filename");
                  }
                  if (is_file($chi_path))
                  {
                      $lang_path = $chi_path;
                      $filename = $lf->file_name($lang_path);
                      $fileext = $lf->file_ext($lang_path);
                      $filename = substr($filename,0,1).$temp_id.substr($filename,1).$fileext;
                      $lf->file_copy($lang_path,"$subdir/$filename");
                  }

             }

         }
    }

    # After copying to $documents_tempdir, zip the directory
    $zip_filename = "$unique_suf.zip";
    $target_zip = "$qb_tempdir/$zip_filename";
    $lf->file_zip(".",$target_zip,"$qb_tempdir/$unique_suf");
    $targetURL = "$intranet_httppath/file/export/qb/$zip_filename";

    # Remove tempdir
    $lf->lfs_remove($documents_tempdir);

    # Mark Downloaded
    for ($i=0; $i<sizeof($qids); $i++)
    {
         if (sizeof($qids[$i])!=0)
         {
             $lq->markQuestionReadDownload($qids[$i]);
             $lq->AddDownloadCount($qids[$i]);
         }
    }

    # Deduct Score
    if ($qb_record_score && !$lg->hasAdminQB($UserID))
    {
        $lq->deductUserScore($UserID,$bucket_score);
    }


}
else if ($format == 1)
{
     echo "Not yet implemented <br>\n";
     $targetURL = "index.php?GroupID=$GroupID&type=$type";
}
else if ($format == 2)
{
     $lq->initBucket();
     $targetURL = "index.php?GroupID=$GroupID&type=$type&msg=7";
}
}
else
{
     $targetURL = "index.php?GroupID=$GroupID&type=$type&msg=8";
}
//$targetURL = "$intranet_httppath/file/qb/g".$lq->relatedGroupID."/$targetURL";
header("Location: $targetURL");
?>