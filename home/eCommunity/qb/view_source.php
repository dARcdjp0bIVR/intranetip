<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libqb.php");
intranet_auth();
intranet_opendb();

$lq = new libqb($qid);
$lq->markQuestionRead($qid);

if ($isEng==1)
{
    $targetURL = $lq->originalFileEng;
}
else
{
    $targetURL = $lq->originalFileChi;
}
$targetURL = "$intranet_httppath/file/qb/g".$lq->relatedGroupID."/$targetURL";
header("Location: $targetURL");
?>