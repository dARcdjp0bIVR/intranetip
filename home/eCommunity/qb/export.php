<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libqb.php");
include_once ("../../../includes/libgroup.php");
include_once ("../../../includes/libuser.php");
include_once ("../../../includes/libeclass.php");
include_once ("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$lg = new libgroup($GroupID);

$lq = new libqb();
$qs = $lq->returnQtyInBucket();
$x = "";

for ($i=0; $i<sizeof($lq->QuestionType); $i++)
{
     if ($lq->QuestionType[$i]!="")
     {
         $x .= $lq->QuestionType[$i]. " : ".$qs[$i]." <br>\n";
     }
}

if ($qb_record_score && !$lg->hasAdminQB($UserID))
{

    $user_score = $lq->returnUserScore($UserID);
    $bucket_score = $lq->returnScoreInBucket();
    $left_score = $user_score - $bucket_score;

    if ($left_score < 0)
    {
        $str_left_score = "<font color=red>$left_score</font>";
        $clear_checked = " CHECKED";
    }
    else
    {
        $str_left_score = "$left_score";
        $clear_checked = "";
    }

    $x .= "<table border=0>\n";
    $x .= "<tr><td>$i_QB_Pts_Available:</td><td>$user_score</td></tr>\n";
    $x .= "<tr><td>$i_QB_Pts_InBucket:</td><td>$bucket_score</td></tr>\n";
    $x .= "<tr><td>$i_QB_Pts_LeftAfterDownload:</td><td>$str_left_score</td></tr>\n";
    $x .= "</table>\n";

    if ($left_score < 0)
    {
        $x .= "<font color=red>$i_QB_Pts_NotEnough</font>";
    }
}


$lu = new libuser($UserID);
$eclassSelect = $lq->returnSelectEclassAsT("name=classroom", $lu->UserEmail);

include_once ("../../../templates/fileheader.php");
?>
<form action="export_work.php" method="post">
<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="700" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510">
<table border=0 width=100% cellpadding=5 cellspacing=2>
<tr><td colspan=2><?="$i_QB_QuestionBucket1<br>\n$x"?></td></tr>
<tr><td width=10% align=right></td><td>
<?php if ($left_score >= 0) { ?>
<input type=radio name=format value=0 CHECKED><?=$i_QB_DownloadAsWord?><br>
<?
if ($eclassSelect != "") {
?>
<input type=radio name=format value=1><?=$i_QB_DownloadToEclass?><br>
<?
echo $eclassSelect;
}
?><br>
<?php }   # end of if left_score != 0
?>
<input type=radio name=format value=2 <?=$clear_checked?>><?=$i_QB_ClearBucket?><br>
</td></tr>
<tr><td align=right></td><td>
<input type=image src=<?=$image_go?> border=0>
<input type=image src=<?=$image_back?> onClick="history.back(); return false;">
</td></tr>
</table>
          </td>
          <td width="240">&nbsp;</td>
        </tr>
      </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>

<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=type value=<?=$type?>>
</form>
<?
include_once ("../../../templates/filefooter.php");
?>