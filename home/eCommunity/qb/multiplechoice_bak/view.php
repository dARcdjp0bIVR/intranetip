<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libgroup.php");
include_once("../../../../includes/libqb.php");
include_once("../lib-question-mc.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../../templates/fileheader.php");

$lq = new libqb($QuestionID);
$lg = new libgroup($lq->relatedGroupID);

//************************************ START OF HTML DISPLAY ************************************
?>

<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="100%" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg1.gif">
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
      </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="714" align="center">
<?php

echo $lq->display();
?>


            </td>
            <td width="29">&nbsp;</td>
          </tr>
        </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>


<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>