<?php
if ($page_size_change!="") { setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
}

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libgroup.php");
include_once("../../../../includes/libqb.php");
//include_once("../lib-question-mc.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../menu.php");

intranet_auth();
intranet_opendb();
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../../templates/fileheader.php");
/*
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-table.php');
include_once('../../../includes/php/lib-info-question.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
include_once('../menu.php');
auth("AT");
opendb();
head();
*/

$lg = new libgroup($GroupID);
//$mc = new question_mc();


$lq = new libqb();

if ($changesub == 1)
{
    $lq->initBucket();
}

$qs = $lq->returnQtyInBucket();
$x = "";
$total_qs = 0;
for ($i=0; $i<sizeof($lq->QuestionType); $i++)
{
     if ($lq->QuestionType[$i]!="")
     {
         $x .= $lq->QuestionType[$i]. " : ".$qs[$i]." <br>\n";
         $total_qs += $qs[$i];
     }

}

$groupSelect = jumpIcon().$lq->returnSelectGroup("name=GroupID onChange=\"confirmChangeSubject(this.form)\"",$GroupID);
$qtypeSelect = $lq->returnSelectQuestionType("name=type",$type);
$lvlSelect = $lq->returnSelectLevel($GroupID,"name=level",$level,1);
$catSelect = $lq->returnSelectCategory($GroupID,"name=category",$category,1);
$diffSelect = $lq->returnSelectDifficulty($GroupID,"name=difficulty",$difficulty,1);
$langSelect = $lq->returnSelectLang("name=qlang",$qlang);
$ownerSelect = $lq->returnSelectOwner($GroupID,"name=owner",$owner,1);

$go_button = "<input type=image src=$intranet_httppath$image_go alt='$button_go'>";

// TABLE SQL
$qb_name = $intranet_session_language=="en"? "EngName": "ChiName";
$lang_suf = ($intranet_session_language == "b5"? "Chi":"Eng");
$username_field = getNameFieldByLang("e.");
if($field=="") $field = 2;
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Question$lang_suf", $username_field, "a.DateModified");
/*
$fieldname  = "Category, ";
$fieldname .= "Question, ";
$fieldname .= "IF(RecordStatus='1','$qbank_public','-'), ";
$fieldname .= "DateModified, ";
$fieldname .= "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=question_id[] value=', question_id ,'>'), ";
$fieldname .= "question_id ";
$sql  = "SELECT $fieldname FROM question WHERE qtype = '0'";
$sql .= " AND (category like '%$keyword%' or question like '%$keyword%')";
if ($category!="")
     $sql .= " AND category='$category' ";
              CONCAT(IF (a.Question$lang_suf is NULL OR a.Question$lang_suf = '','-',CONCAT(LEFT(a.Question$lang_suf,50),IF(LENGTH(a.Question$lang_suf)>50,'...','') )) ,IF(LOCATE(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2 align=absmiddle>', '') ),

                         ,CONCAT(
                                 LEFT(a.Question$lang_suf,50)
                                 ,IF(LENGTH(a.Question$lang_suf)>50
                                     ,'...'
                                     ,'')
                                 )
     */
//              IF(a.RecordStatus=0,'$i_QB_Pending', IF (a.RecordStatus=1,'$i_QB_Approved','$i_QB_Rejected') ),

$sql = "SELECT
              a.QuestionID, a.QuestionCode, IF(a.Lang=1 OR a.Lang=3,a.QuestionEng,'-'),IF(a.Lang=2 OR a.Lang=3,a.QuestionChi,'-'),b.$qb_name, c.$qb_name, d.$qb_name,
              CONCAT(
                     IF (a.Question$lang_suf is NULL OR a.Question$lang_suf = ''
                         ,CONCAT('-','===###===')
                         ,CONCAT(a.Question$lang_suf,'===###===')
                         )
                     ,IF(a.BulletinID IS NULL OR a.BulletinID = ''
                         ,' '
                         ,CONCAT('<a href=javascript:open_bulletin_msg('
                                   ,a.BulletinID
                                   ,')>".bulletinIcon("Go to bulletin")."</a>'
                         )
                     )
                     ,IF(LOCATE(';$UserID;',a.ReadFlag)=0
                         , '<img src=$image_path/new.gif border=0 hspace=2 align=absmiddle>'
                         , '')
                     ),

              IF(a.Lang = 1,CONCAT('<a title=\"$i_QB_ViewOriginalEnglish\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',1)>$i_QB_EngVer</a>'),
              IF(a.Lang=2,CONCAT('<a title=\"$i_QB_ViewOriginalChinese\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',0)>$i_QB_ChiVer</a>'),
              CONCAT('<a title=\"$i_QB_ViewOriginalEnglish\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',1)>$i_QB_EngVer</a><br><a title=\"$i_QB_ViewOriginalChinese\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',0)>$i_QB_ChiVer</a>')
              )),
              $username_field, a.DateModified,
              CONCAT('<input type=checkbox name=QuestionID[] value=',a.QuestionID,'>')
        FROM QB_QUESTION as a, QB_LEVEL as b, QB_CATEGORY as c, QB_DIFFICULTY as d, INTRANET_USER as e
        WHERE a.LevelID = b.UniqueID AND a.CategoryID = c.UniqueID AND a.DifficultyID = d.UniqueID
        AND a.RelatedGroupID = $GroupID
        AND a.OwnerIntranetID = e.UserID
        AND a.RecordStatus = 1
        AND a.QuestionType = 0
        AND (b.$qb_name LIKE '%$keyword%' OR c.$qb_name LIKE '%$keyword%'
             OR d.$qb_name LIKE '%$keyword%' OR a.QuestionEng LIKE '%$keyword%'
             OR a.QuestionChi LIKE '%$keyword%' OR a.QuestionCode LIKE '%$keyword%'
             OR $username_field LIKE '%$keyword%'
             )
        ";
if ($level != 0 && $level != "")
{
    $sql .= " AND b.UniqueID = $level";
}
if ($category != 0 && $category != "")
{
    $sql .= " AND c.UniqueID = $category";
}
if ($difficulty != 0 && $difficulty != "")
{
    $sql .= " AND d.UniqueID = $difficulty";
}
if ($owner != 0 && $owner != "")
{
    $sql .= " AND a.OwnerIntranetID = $owner";
}
if ($qlang != 0 && $qlang != "")
{
    if ($qlang == 3) $sql .= " AND a.Lang = 3";
    if ($qlang == 2) $sql .= " AND (a.Lang = 3 OR a.Lang = 2)";
    if ($qlang == 1) $sql .= " AND (a.Lang = 3 OR a.Lang = 1)";
}

echo "<!-- \n\n $sql \n -->\n";
// TABLE INFO
$li->sql = $sql;
//$li->title = $qbank_mc;
$li->no_msg = ($keyword=="") ? $i_no_record_exists_msg : $i_no_search_result_msg;
$li->page_size =
($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage
;
$li->no_col = 4+2;
$li->IsColOff = 6;


//************************************ PREPARE HTML CODES ************************************

// TABLE COLUMN
$li->column_list .= "<td class=qb_tableheader height='25' width='1' align='center'>#</td>\n";
/*
$li->column_list .= "<td background='$BackTitle' width='5%'>".$li->column(0, $i_QB_QuestionCode)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='10%'>".$li->column(1, $i_QB_Level)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='15%'>".$li->column(2, $i_QB_Category)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='15%'>".$li->column(3, $i_QB_Difficulty)."</td>\n";
*/
$li->column_list .= "<td class=qb_tableheader width='35%'>".$li->column(0, $i_QB_Question)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='15%'>$i_QB_LangVer</td>\n";
//$li->column_list .= "<td class=qb_tableheader width='10%'>".$li->column(1, $i_QB_Status)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='15%'>".$li->column(1, $i_QB_Owner)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='25%'>".$li->column(2, $i_QB_LastModified)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='1'>".$li->check("QuestionID[]")."</td>\n";

$li->column_array = array(0,0,0,0,0,0,0);

echo $JavaScripts;
?>
<script language="Javascript" src='<?=$intranet_httppath?>/templates/tooltip.js'></script>
<script language="Javascript">
function confirmChangeSubject(obj)
{
         if (<?=$total_qs?> != 0)
         {
                if (confirm('<?=$i_QB_ChangeGroupReset?>'))
                {
                    obj.changesub.value = 1;
                    obj.submit();
                }
                else
                {
                    obj.GroupID.value = <?=$GroupID?>
                }
         }
         else
         {
             obj.submit();
         }
}
</script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     //isMenu = true;
     isToolTip = true;
     </script>

<form name="form1" method="get">
<div id="ToolTip"></div>

<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="100%" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg1.gif">
        <tr>
          <td width="16">&nbsp;</td>
            <td colspan=3><?=$groupSelect?></td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
            <td width="143" class="qb_on_tab"><a class=qb_on_font href=index.php?GroupID=<?=$GroupID?>><?=$i_QB_SharingArea?></a></td>
            <td width="143" class="qb_off_tab"><a class=qb_off_font href=myquestion.php?GroupID=<?=$GroupID?>><?=$i_QB_MyQuestion?></a></td>
          <td width="464">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510">
            <table width=100% cellpadding=1 cellspacing=1>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_QuestionType?>:</td><td width="367" align="left" valign="top"><?=$qtypeSelect?></td></tr>
              <tr><td colspan=2>&nbsp;</td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Level?>:</td><td width="367" align="left" valign="top"><?=$lvlSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Category?>:</td><td width="367" align="left" valign="top"><?=$catSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Difficulty?>:</td><td width="367" align="left" valign="top"><?=$diffSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_LangSelect?>:</td><td width="367" align="left" valign="top"><?=$langSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Owner?>:</td><td width="367" align="left" valign="top"><?=$ownerSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"></td><td width="367" align="left" valign="top"><?=$go_button?></td></tr>
            </table>
          </td>
          <td width="240">
            <input name="keyword" type="text" size="15">
            <input type=image src="<?=$image_search?>" border="0"><br>
            <?="$i_QB_QuestionBucket1<br>\n$x"?>
          </td>
        </tr>
      </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="15" align="right"><img src="<?=$image_path?>/groupinfo/sbqb_cornerup1.gif"></td>
            <td width="105" align="center" valign="middle" bgcolor="#BEDAEF" class="h1"><?=$i_QB_FileList?> :
              </td>
            <td width="21"><img src="/images/groupinfo/sbqb_cornerup2.gif"></td>
            <td width="573">&nbsp;</td>
            <td width="29">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6"><img src="<?=$image_path?>/groupinfo/sbqb_inframetop.gif"></td>
          </tr>
        </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_inframetbbg.gif">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="714" align="center">
<?php

// DISPLAY TOOLBAR: new, import, export
echo $li->displayFunctionbar("1", "1", $toolbar_sharing, $functionbar_sharing);

// DISPLAY TABLE LIST CONTENT
echo $li->display();
?>


            </td>
            <td width="29">&nbsp;</td>
          </tr>
        </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_inframebottom.gif"></td>
          </tr>
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<input type="hidden" name="changesub" value=0>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage
 value="<?=$numPerPage
?>">
<input type="hidden" name="page_size_change">
</form>


<?php
//foot(menuText(1));
//echo menuText(1);
include ("../../../../templates/filefooter.php");
intranet_closedb();
?>