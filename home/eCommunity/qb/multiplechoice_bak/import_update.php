<?php 
include_once('../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../includes/php/lib-user.php');
include_once('../../../includes/php/lib-filemanager.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
auth("AT");
opendb();

$mc = new question_mc();
$fm = new fileManager($ck_course_id, 5, "");

if (trim($category)!="")
	$mc->category_set = htmlspecialchars($category);
$mc->status_set = htmlspecialchars($status);

$ext = strtoupper($fm->getFileExt($userfile_name));

if ($ext=="ZIP") {
	//unzip
	$dest = $admin_root_path."/".$file_path_name."/".classNamingDB($ck_course_id)."/tmp";
	$fm->createFolder($dest);
	$dest .= "/$ck_user_id";
	$fm->createFolder($dest);
	$dest .= "/question";
	
	$dest = OsCommandSafe($dest);	
	$userfile = OsCommandSafe($userfile);
	if (file_exists($dest))		//avoid reading old files
		exec("rm -r $dest");
	$fm->createFolder($dest);
	
	if (file_exists($dest)){
		exec("unzip $userfile -d $dest");

		$fm->permission = "AW";

		//create folder (/Media/MC)
		$folderName = "Import";
		$folderID = $fm->createFolderDB($folderName, "For storing media files extracted from import pack.", "", 5);
		$folderName = "MC";
		$folderID = $fm->createFolderDB($folderName, "For Multiple Choice.", $folderID, 5);

		//find the last id of question (using name_pend to avoid overwriting files)
		$sql = "SELECT max(question_id) FROM question";
		$row = $fm->returnArray($sql, 1);
		$folderName = "Q".($row[0][0]+1)."_".$userfile_name;
		$folderID = $fm->createFolderDB($folderName, ("Imported from ".$userfile_name), $folderID, 5);

		//copy files (including TXT, DOC)
		if ($folderID!="") {
			$lu = new libuser($ck_user_id,$ck_course_id);
			$user_name = addslashes($lu->user_name());

			$fm->isImport = true;
			$fm->dest_categoryID = 5;
			$fm->dest_courseID = $ck_course_id;
			$fm->dest_folderID = $folderID;
			$fm->user_name = $user_name;

			$fm->IO2DB($dest, $fm->dest_folderID);
		}

		//import
		$mc->path_base = $dest;
		$mc->name_prefix = "/Import/MC/$folderName/";
		$mc->importFromFiles($dest);

		exec("rm -r $dest");
	}
	//for each file, copy/import
	$msg=1;
} elseif ($ext=="TXT") {
	if (file_exists($userfile)) {
		$dataArr = file($userfile);
		$data = implode("", $dataArr);
		if ($mc->importFromText($data))
			$msg=1;
	}
} elseif ($ext=="DOC") {
	if (!$isWVinstall)
		return;
	if (file_exists($userfile)) {
		$dest = $admin_root_path."/".$file_path_name."/".classNamingDB($ck_course_id)."/tmp";
		$fm->createFolder($dest);
		$dest .= "/$ck_user_id";
		$fm->createFolder($dest);
		$dest .= "/question";
		
		$dest = OsCommandSafe($dest);	
		if (file_exists($dest))		//avoid reading old files
			exec("rm -r $dest");
		//$fm->createFolder($dest);
		mkdir($dest, 0777);

		//use WV
		$filename = substr($userfile_name, 0, strpos(strtoupper($userfile_name), ".DOC"));
		$filename = OsCommandSafe($filename);	
		copy($userfile, $dest."/".$filename.".doc");
		chdir($dest);

		//passthru("abcd");
		exec("wvHtml $filename.doc $filename.html");
		//debug("wvHtml $filename.doc $filename.html");
		passthru("ls -l");
		//debug("wvHtml --targetdir=$dest/ $dest/$filename.doc $filename.html");
	}
} else {
	$msg=5;
}

closedb();

header("Location: index.php?msg=$msg");
?>
