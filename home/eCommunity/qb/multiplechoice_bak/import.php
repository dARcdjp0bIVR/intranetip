<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libqb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
/*
include_once('../../../../../system/settings/global.php');
include_once('../../../includes/php/lib-db.php');
include_once('../../../includes/php/lib-question-mc.php');
include_once('../../../../system/settings/lang/'.$ck_default_lang.'.php');
include_once('../menu.php');
*/
intranet_auth();
intranet_opendb();

$lq = new libqb();
$qtypeSelect = $lq->returnSelectQuestionType("name=type");
$lvlSelect = $lq->returnSelectLevel($GroupID,"name=level");
$catSelect = $lq->returnSelectCategory($GroupID,"name=category");
$diffSelect = $lq->returnSelectDifficulty($GroupID,"name=difficulty");

include_once("../../../../templates/fileheader.php");
?>

<form action="import_update.php" method="post" enctype="multipart/form-data">
<?=$i_QB_ImportNotes?>
<?=$qtypeSelect?><br>
<?=$lvlSelect?><br>
<?=$catSelect?><br>
<?=$diffSelect?><br>
<input type=file name=userfile>

<input type=image src=<?=$image_import?>>
<input type=image src=<?=$image_cancel?> onClick="history.back(); return false;">


</form>
<?php
include("../../../../templates/filefooter.php");
intranet_closedb();
?>