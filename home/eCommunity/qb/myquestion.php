<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libqb.php");
include_once("lib-question-mc.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("menu.php");

intranet_auth();
intranet_opendb();

if ($type == "")
{
    $type = 0;
}
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");

$lg = new libgroup($GroupID);
$mc = new question_mc();


$lq = new libqb();
$groupSelect = jumpIcon().$lq->returnSelectGroup("name=GroupID onChange=\"this.form.submit()\"",$GroupID);
$qtypeSelect = $lq->returnSelectQuestionType("name=type",$type);
$lvlSelect = $lq->returnSelectLevel($GroupID,"name=level",$level,1);
$catSelect = $lq->returnSelectCategory($GroupID,"name=category",$category,1);
$diffSelect = $lq->returnSelectDifficulty($GroupID,"name=difficulty",$difficulty,1);

$go_button = "<input type=image src=$intranet_httppath$image_go alt='$button_go'>";

// TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$qb_name = $intranet_session_language=="en"? "EngName": "ChiName";
$lang_suf = ($intranet_session_language == "b5"? "Chi":"Eng");
$username_field = getNameFieldByLang("e.");
if($field=="") $field = 3;
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Question$lang_suf", "a.RecordStatus","a.DownloadedCount", "a.DateModified");


$sql = "SELECT
              a.QuestionID, a.QuestionCode, IF(a.Lang=1 OR a.Lang=3,a.QuestionEng,'-'),IF(a.Lang=2 OR a.Lang=3,a.QuestionChi,'-'),b.$qb_name, c.$qb_name, d.$qb_name, a.Score,
              CONCAT(
                     IF(a.Question$lang_suf is NULL OR a.Question$lang_suf = ''
                         ,'-===###==='
                         ,CONCAT(a.Question$lang_suf,'===###===')
                     )
                     ,IF(a.BulletinID IS NULL OR a.BulletinID = ''
                         ,' '
                         ,CONCAT('<a href=javascript:open_bulletin_msg('
                                   ,a.BulletinID
                                   ,')>".bulletinIcon("Go to bulletin")."</a>'
                         )
                     )
              ),
              IF(a.Lang = 1,CONCAT('<a title=\"$i_QB_DownloadEnglish\" class=functionlink href=$intranet_httppath/file/qb/g$GroupID/',a.OriginalFileEng,'>$i_QB_EngVer</a>'),
              IF(a.Lang=2,CONCAT('<a title=\"$i_QB_DownloadChinese\" class=functionlink href=$intranet_httppath/file/qb/g$GroupID/',a.OriginalFileChi,'>$i_QB_ChiVer</a>'),
              CONCAT('<a title=\"$i_QB_DownloadEnglish\" class=functionlink href=$intranet_httppath/file/qb/g$GroupID/',a.OriginalFileEng,'>$i_QB_EngVer</a><br><a title=\"$i_QB_DownloadChinese\" class=functionlink href=$intranet_httppath/file/qb/g$GroupID/',a.OriginalFileChi,'>$i_QB_ChiVer</a>')
              )),
              IF(a.RecordStatus=0,'$i_QB_Pending', IF (a.RecordStatus=1,'$i_QB_Approved','$i_QB_Rejected') ),
              a.DownloadedCount, a.DateModified,
              CONCAT('<input type=checkbox name=QuestionID[] value=',a.QuestionID,'>')
        FROM QB_QUESTION as a, QB_LEVEL as b, QB_CATEGORY as c, QB_DIFFICULTY as d, INTRANET_USER as e
        WHERE a.LevelID = b.UniqueID AND a.CategoryID = c.UniqueID AND a.DifficultyID = d.UniqueID
        AND a.OwnerIntranetID = e.UserID
        AND a.RelatedGroupID = $GroupID
        AND a.OwnerIntranetID = $UserID
        AND a.QuestionType = $type
        AND a.RecordStatus <> 1
        AND (b.$qb_name LIKE '%$keyword%' OR c.$qb_name LIKE '%$keyword%'
             OR d.$qb_name LIKE '%$keyword%' OR a.QuestionEng LIKE '%$keyword%'
             OR a.QuestionChi LIKE '%$keyword%' OR a.QuestionCode LIKE '%$keyword%'
             )
        ";
if ($level != 0 && $level != "")
{
    $sql .= " AND b.UniqueID = $level";
}
if ($category != 0 && $category != "")
{
    $sql .= " AND c.UniqueID = $category";
}
if ($difficulty != 0 && $difficulty != "")
{
    $sql .= " AND d.UniqueID = $difficulty";
}
// TABLE INFO
$li->sql = $sql;
//$li->title = $qbank_mc;
$li->no_msg = ($keyword=="") ? $i_no_record_exists_msg : $i_no_search_result_msg;
$li->page_size =
($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage
;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = 6;


//************************************ PREPARE HTML CODES ************************************

// TABLE COLUMN
$li->column_list .= "<td class=qb_tableheader height='25' width='1' align='center'>#</td>\n";
/*
$li->column_list .= "<td background='$BackTitle' width='5%'>".$li->column(0, $i_QB_QuestionCode)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='10%'>".$li->column(1, $i_QB_Level)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='15%'>".$li->column(2, $i_QB_Category)."</td>\n";
$li->column_list .= "<td background='$BackTitle' width='15%'>".$li->column(3, $i_QB_Difficulty)."</td>\n";
*/
$li->column_list .= "<td class=qb_tableheader width='40%'>".$li->column(0, $i_QB_Question)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='15%'>$i_QB_LangVer</td>\n";
$li->column_list .= "<td class=qb_tableheader width='10%'>".$li->column(1, $i_QB_Status)."</td>\n";
//$li->column_list .= "<td class=qb_tableheader width='15%'>".$li->column(2, $i_QB_Owner)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='10%'>".$li->column(2, $i_QB_DownloadedCount)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='25%'>".$li->column(3, $i_QB_LastModified)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='1'>".$li->check("QuestionID[]")."</td>\n";

$li->column_array = array(0,0,0,0,0,0,0);

echo $JavaScripts;
include_once("../tooltab.php");
?>
<script language="Javascript" src='<?=$intranet_httppath?>/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     //isMenu = true;
     isToolTip = true;
     </script>

<form name="form1" method="get">
<div id="ToolTip"></div>

<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="100%" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
      </table>
      <?=$header_tool_qb?>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg1.gif">
        <tr>
          <td width="16">&nbsp;</td>
            <td colspan=3><?=$groupSelect?></td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
            <td width="143" class="qb_off_tab"><a class=qb_off_font href=index.php?GroupID=<?=$GroupID?>><?=$i_QB_SharingArea?></a></td>
            <td width="143" class="qb_on_tab"><a class=qb_on_font href=myquestion.php?GroupID=<?=$GroupID?>><?=$i_QB_MyQuestion?></a></td>
          <td width="464">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510">
            <table width=100% cellpadding=1 cellspacing=1>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_QuestionType?>:</td><td width="367" align="left" valign="top"><?=$qtypeSelect?></td></tr>
              <tr><td colspan=2>&nbsp;</td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Level?>:</td><td width="367" align="left" valign="top"><?=$lvlSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Category?>:</td><td width="367" align="left" valign="top"><?=$catSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Difficulty?>:</td><td width="367" align="left" valign="top"><?=$diffSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"></td><td width="367" align="left" valign="top"><?=$go_button?></td></tr>
            </table>
          </td>
          <td width="240">
            <input name="keyword" type="text" size="15">
            <input type=image src="<?=$image_search?>" border="0">
          </td>
        </tr>
      </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="15" align="right"><img src="<?=$image_path?>/groupinfo/sbqb_cornerup1.gif"></td>
            <td width="105" align="center" valign="middle" bgcolor="#BEDAEF" class="h1"><?=$i_QB_FileList?> :
              </td>
            <td width="21"><img src="<?=$image_path?>/groupinfo/sbqb_cornerup2.gif"></td>
            <td width="573">&nbsp;</td>
            <td width="29">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6"><img src="<?=$image_path?>/groupinfo/sbqb_inframetop.gif"></td>
          </tr>
        </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_inframetbbg.gif">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="714" align="center">
<?php

// DISPLAY TOOLBAR: new, import, export
echo $li->displayFunctionbar("1", "1", $toolbar_myquestion, $functionbar_myquestion);

// DISPLAY TABLE LIST CONTENT
echo $li->display();
?>


            </td>
            <td width="29">&nbsp;</td>
          </tr>
        </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_inframebottom.gif"></td>
          </tr>
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
include_once ("../../../templates/filefooter.php");
intranet_closedb();
?>