<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libqb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lg = new libgroup($GroupID);

$QuestionID = (is_array($QuestionID)? $QuestionID[0]: $QuestionID);

$lq = new libqb($QuestionID);
//$qtypeSelect = $lq->returnSelectQuestionType("name=type");
$stype = $lq->QuestionType[$lq->type];
$lvlSelect = $lq->returnSelectLevel($GroupID,"name=level",$lq->levelID);
$catSelect = $lq->returnSelectCategory($GroupID,"name=category",$lq->categoryID);
$diffSelect = $lq->returnSelectDifficulty($GroupID,"name=difficulty",$lq->difficultyID);

include_once("../../../templates/fileheader.php");
?>
<form action="edit_update.php" method="post">
<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="700" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510">
<table border=0 width=100% cellpadding=5 cellspacing=2>
<tr><td colspan=2><?=$i_QB_EditNotes?></td></tr>
<tr><td width=40% align=right><?=$i_QB_QuestionType?>:</td><td><?=$stype?></td></tr>
<tr><td width=40% align=right><?=$i_QB_Level?>:</td><td><?=$lvlSelect?></td></tr>
<tr><td align=right><?=$i_QB_Category?>:</td><td><?=$catSelect?></td></tr>
<tr><td align=right><?=$i_QB_Difficulty?>:</td><td><?=$diffSelect?></td></tr>
<tr><td align=right></td><td>
<input type=image src=<?=$image_save?>>
<input type=image src=<?=$image_cancel?> onClick="history.back(); return false;">
</td></tr>
</table>
          </td>
          <td width="240">&nbsp;</td>
        </tr>
      </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>

<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=QuestionID value=<?=$QuestionID?>>
<input type=hidden name=type value=<?=$lq->type?>>
</form>
<?php
include("../../../templates/filefooter.php");
intranet_closedb();
?>