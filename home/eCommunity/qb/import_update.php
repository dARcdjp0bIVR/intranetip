<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libqb.php");
include_once ("../../../includes/libfilesystem.php");
include_once ("../../../includes/libuser.php");

intranet_auth();
intranet_opendb();


$Q_TYPE_MC = 0;
$Q_TYPE_SQ = 1;
$Q_TYPE_FillIn = 2;
$Q_TYPE_Match = 3;
$Q_TYPE_TandF = 4;
function char_replace($str)
{
         $str = str_replace("'","&#039;",$str);
         return $str;
}

function locateAnotherFile($target_fileid, $target_dir_name,$target_prefix, $dir_list, $from)
{
         $lf2 = new libfilesystem();
         for ($i=$from; $i<sizeof($dir_list); $i++)
         {
              $filepath = $dir_list[$i];
              $filename = $lf2->file_name($filepath);
              $filedir = dirname($filepath);
              $fileid = substr($filename,1);
              $prefix = substr($filename,0,1);
              if ($fileid == $target_fileid && $prefix == $target_prefix && $filedir == $target_dir_name)
              {
                  return $i;
              }
         }
         return -1;
}

# Accept only options as A. or A(161,68) full . in Chinese Word
function parseMCQuestion ($question)
{
         $normalDot = 46;
         $wholeDot1 = 161;
         $wholeDot2 = 68;
         $choiceOption = "A";
         $result = array();
         $start = 0;

         $posOption = strpos($question,$choiceOption);
         while (!($posOption===false))
         {
                $dot1 = substr($question,$posOption+1,1);
                $dot2 = substr($question,$posOption+2,1);

                if (ord($dot1)!=$normalDot && (!(ord($dot1)==$wholeDot1 && ord($dot2)==$wholeDot2)) )
                {
                    # Not a valid option
                    $posOption = strpos($question,$choiceOption,$posOption+1);
                }
                else
                {
                    # Option found
                    $extract = substr($question,$start,$posOption-$start);
                    $start = $posOption+3;
                    $result[] = $extract;
                    $choiceOption++;
                    $posOption = strpos($question,$choiceOption,$start+1);
                }
         }
         $result[] = substr($question,$start);
         return $result;
}

$lf = new libfilesystem();
$qb_infiles_path = "file/qb/infiles";
$qb_infiles_syspath = "$intranet_root/$qb_infiles_path";
if (!is_dir($qb_infiles_syspath))
{
     $lf->folder_new($qb_infiles_syspath);
}
$lq = new libqb();
$lu = new libuser($UserID);

$file_ext = $lf->file_ext($userfile_name);

$qb_dir = "$intranet_root/file/qb";
$group_dir = "$qb_dir/g$GroupID";
$folder = session_id()."-".time();
$new_folder = "$group_dir/$folder";

if (!is_dir($qb_dir))
{
     $lf->folder_new($qb_dir);
}
if (!is_dir($group_dir))
{
     $lf->folder_new($group_dir);
}

$lf->folder_new($new_folder);

if ($file_ext ==".zip")
{
    # Decompress file to new folder
    $lf->file_unzip($userfile, $new_folder);
}
else if ($file_ext == ".doc")
{
     $lf->item_copy($userfile,"$new_folder/$userfile_name");
}
else
    exit();

# Process files
$processed_list = array();
$list = $lf->return_folderlist($new_folder);
for ($i=0; $i<sizeof($list); $i++)
{
     $filepath = $list[$i];
//     echo "Processing $filepath ...<br>\n";
     $filename = $lf->file_name($filepath);
     $fileext = $lf->file_ext($filepath);
     $fileid = substr($filename,1);
     $dir_name = dirname($filepath);
     $fileid_withdir = "$dir_name/$fileid";
     $whole_file_name = basename($filepath);

//     echo "ID: $fileid_withdir <br>\n";
     if ($fileext != ".doc") continue;
     if (in_array($fileid_withdir,$processed_list)) continue;    // Already processed

//     echo "Not in processed list <br>\n";

     $prefix = substr($filename,0,1);
     if ($prefix == "e")
         $checkother = "c";
     else if ($prefix == "c")
          $checkother = "e";
     else continue;         // Wrong prefix, unknown language

     $processed_list[] = $fileid_withdir;

     $j = locateAnotherFile($fileid,$dir_name,$checkother,$list,$i+1);
//     echo "Locate another file at $j <br>\n";
     if ($j == -1)          // Single language
     {
         if ($prefix == "e")
         {
//             echo "Process single english file <br>\n";
             # English
             $lang_flag = 1;

             # Put in DB
             $lang_field = ",QuestionEng,AnswerEng,HintEng,ExplanationEng,ReferenceEng,OriginalFileEng";
             //$questionEng = $lf->readWordFileContent($filepath); //"$whole_file_name";
             $targetFolder = session_id()."-".time()."-".$lf->file_name($filepath);
             $lf->folder_new("$qb_infiles_syspath/$targetFolder");

             $questionEng = char_replace($lf->convertWordFileWImage($filepath,"$qb_infiles_path/$targetFolder","engimage.html"));
             $answerEng = "";
             $hintEng = "";
             $explanationEng = "";
             $referenceEng = "";
             $originalEng = substr($filepath, strlen($group_dir)+1);

             $questionChi = "";
             $answerChi = "";
             $hintChi = "";
             $explanationChi = "";
             $referenceChi = "";
             $originalChi = "";
         }
         else
         {
//             echo "Process single chinese file <br>\n";
             # Chinese
             $lang_flag = 2;
             $lang_field = ",QuestionChi,AnswerChi,HintChi,ExplanationChi,ReferenceChi,OriginalFileChi";
             //$questionChi = $lf->readWordFileContent($filepath); //"$whole_file_name";
             $targetFolder = session_id()."-".time()."-".$lf->file_name($filepath);
             $lf->folder_new("$qb_infiles_syspath/$targetFolder");

             $questionChi = char_replace($lf->convertWordFileWImage($filepath,"$qb_infiles_path/$targetFolder","chiimage.html"));
             $answerChi = "";
             $hintChi = "";
             $explanationChi = "";
             $referenceChi = "";
             $originalChi = substr($filepath, strlen($group_dir)+1);

             $questionEng = "";
             $answerEng = "";
             $hintEng = "";
             $explanationEng = "";
             $referenceEng = "";
             $originalEng = "";
         }
     }
     else
     {
//         echo "Process Bi-lang file <br>\n";
         $lang_flag = 3;
         $another_file = $list[$j];
         if ($prefix == "e")
         {
             $originalEng = substr($filepath, strlen($group_dir)+1);
             $originalChi = substr($another_file, strlen($group_dir)+1);
             $engFile = $filepath;
             $chiFile = $another_file;
         }
         else
         {
             $originalChi = substr($filepath, strlen($group_dir)+1);
             $originalEng = substr($another_file, strlen($group_dir)+1);
             $chiFile = $filepath;
             $engFile = $another_file;
         }

         $lang_field = ",QuestionEng,AnswerEng,HintEng,ExplanationEng,ReferenceEng,OriginalFileEng";
         //$questionEng =  $lf->readWordFileContent($engFile); //"$originalEng";
         $targetFolder = session_id()."-".time()."-".$lf->file_name($engFile);
         $lf->folder_new("$qb_infiles_syspath/$targetFolder");
         $questionEng = char_replace($lf->convertWordFileWImage($engFile,"$qb_infiles_path/$targetFolder","engimage.html"));

         $answerEng = "";
         $hintEng = "";
         $explanationEng = "";
         $referenceEng = "";

         $lang_field .= ",QuestionChi,AnswerChi,HintChi,ExplanationChi,ReferenceChi,OriginalFileChi";
         //$questionChi = $lf->readWordFileContent($chiFile); //"$originalChi";
         $targetFolder = session_id()."-".time()."-".$lf->file_name($chiFile);
         $lf->folder_new("$qb_infiles_syspath/$targetFolder");
         $questionChi = char_replace($lf->convertWordFileWImage($chiFile,"$qb_infiles_path/$targetFolder","chiimage.html"));

         $answerChi = "";
         $hintChi = "";
         $explanationChi = "";
         $referenceChi = "";

     }

     # Parse Content
     $answer_separator = "###";
     $option_separator = "===@@@===";

     $lang_values = "";
     if ($questionEng != "")
     {
         $temp = split($answer_separator,$questionEng);
         $answerEng = $temp[1];
         if ($type==$Q_TYPE_MC)
         {
             $questionEng = implode($option_separator,parseMCQuestion($temp[0]));
         }
         else
         {
             $questionEng = $temp[0];
         }
         $lang_values = ",'$questionEng','$answerEng','$hintEng','$explanationEng','$referenceEng'";
         # Originals
         $lang_values .= ",'$originalEng'";
     }
     if ($questionChi != "")
     {
         $temp = split($answer_separator,$questionChi);
         $answerChi = $temp[1];
         if ($type==$Q_TYPE_MC)
         {
             $questionChi = implode($option_separator,parseMCQuestion($temp[0]));
         }
         else
         {
             $questionChi = $temp[0];
         }
         $lang_values .= ",'$questionChi','$answerChi','$hintChi','$explanationChi','$referenceChi'";
         # Originals
         $lang_values .= ",'$originalChi'";
     }


     $ownerName = $lu->getNameForRecord();
     $fields_name = "LevelID, CategoryID, DifficultyID, QuestionType, OwnerIntranetID,OwnerName, RelatedGroupID, RecordStatus, Lang,DateInput,DateModified,ReadFlag,DownloadFlag";
     $insert_values = "$level, $category, $difficulty, $type, $UserID,'$ownerName', $GroupID, '0', $lang_flag,now(),now(),';$UserID;',';$UserID;'";
     $fields_name .= $lang_field;
     $insert_values .= $lang_values;
     $sql = "INSERT INTO QB_QUESTION ($fields_name) VALUES ($insert_values)";
//   echo "$sql <br>\n";
     $lq->db_db_query($sql);
     $db_insert_id = $lq->db_insert_id();
     $sql = "UPDATE QB_QUESTION SET QuestionCode = CONCAT('Q',RelatedGroupID,'-',$db_insert_id) WHERE QuestionID = $db_insert_id";
     $lq->db_db_query($sql);
//     echo "<br>\nQuestion updated<br>\n<br>\n";
//     echo "<hr>\n";
}

header("Location: myquestion.php?GroupID=$GroupID&type=$type&msg=1");
?>