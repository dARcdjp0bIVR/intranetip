<?php 
class libtable extends libdb{

	var $field;
	var $order;
	var $pageNo;
	var $field_array;
	var $column_array;
	var $column_list;
	var $sql;
	var $db;
	var $rs;
	var $page_size;
	var $title;
	var $no_col;
	var $no_msg;
	var $bgcolor=array("#EEEEEE", "#DDDDDD");
	var $tableColor = "#FFFFFF";
	var $sortby;
	var $checkall;
	var $clearall;
	var $total;
	var $pagename;
	var $pagePrev;
	var $pageNext;
	var $imagePath;
	var $noNumber;		//3.0 use index from 1 to n?
	var $total_row;		//3.0 select only the data inside the range and this field is the total no. of data fullfilled the query
	var $n_start;
	var $style;			//style of the table
	
	function libtable($f, $o, $p){
		global $image_path;
		$this->imagePath = $image_path;
		$f+=0;
		$o+=0;
		$p+=0;
		($f==0) ? $this->field=0 : $this->field=$f;
		($o==0) ? $this->order=0 : $this->order=$o;
		($p==0) ? $this->pageNo=1 : $this->pageNo=$p;
		if (!isset($this->style)) $this->style = 0;
		global $list_sortby, $button_check_all, $button_clear_all, $list_total, $list_page, $list_prev, $list_next;
		(trim($list_sortby) == "") ? $this->sortby="Sort By" : $this->sortby=$list_sortby;
		(trim($button_check_all) == "") ? $this->checkall="Check All" : $this->checkall=$button_check_all;
		(trim($button_clear_all) == "") ? $this->clearall="Clear All" : $this->clearall=$button_clear_all;
		(trim($list_total) == "") ? $this->total="Total" : $this->total=$list_total;
		(trim($list_page) == "") ? $this->pagename="Page" : $this->pagename=$list_page;
		(trim($list_prev) == "") ? $this->pagePrev="Prev" : $this->pagePrev=$list_prev;
		(trim($list_next) == "") ? $this->pageNext="Next" : $this->pageNext=$list_next;
	}

	function built_sql(){
		$x = $this->sql;

		//3.0 count the total no. of rows
		$y = "SELECT COUNT(*) " . stristr($x, " FROM ");
		$this->rs=$this->db_db_query($y);
		if ($this->db_num_rows()!=0){
			$row = $this->db_fetch_array();
			$this->total_row = ($this->db_num_rows()!=1) ? $this->db_num_rows() : $row[0];
			
		}
		(count($this->field_array)<=$this->field) ? $x.=" ORDER BY ".$this->field_array[0] : $x.=" ORDER BY ".$this->field_array[$this->field];
		($this->order==0) ? $x.=" DESC" : $x.=" ASC";
		$x .= " LIMIT ".$this->n_start.", ".$this->page_size;		//3.0 limit the retrieval of data which is only inside the range
		//echo $y;
		return $x;
	}

	function column($field_index, $field_name){
		$x="";
		if($this->field==$field_index){
			if($this->order==1) $x.="<a class=tableTitle href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
			if($this->order==0) $x.="<a class=tableTitle href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
		}else{
			$x.="<a class=tableTitle href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
		}
		$x.=str_replace("_", " ", $field_name);
		if($this->field==$field_index){
			if($this->order==0) $x.="<img src='$this->imagePath/icon/ds_desc.gif' hspace=2 border=0 align='absmiddle'>";
			if($this->order==1) $x.="<img src='$this->imagePath/icon/ds_asc.gif' hspace=2  border=0 align='absmiddle'>";
		}
		$x.="</a>";
		return $x;
	}

	function navigation(){
		$percentage = ($this->style==0) ? "98%" : "100%";

		if (func_num_args()==0) {
				$x  = "<table width='$percentage' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
			$x .= "<tr>\n";
			if ($this->style==0) 
				$x .= "<td width='100%' valign='top'><img src='$this->imagePath/shading.gif' width='100%' height='13'></td>\n";
			$x .= "<td align='right'><img src='$this->imagePath/frame/border2_ft_left.gif' width='27' height='25'></td>\n";
			$x .= "<td nowrap background='$this->imagePath/frame/border2_footer.gif' align='center'>\n";
			$x .= $this->record_range();
			$x .= "&nbsp; &nbsp;\n";
			$x .= $this->prev_n();
			$x .= "&nbsp;\n";
			$x .= $this->go_page();
			$x .= "&nbsp;\n";
			$x .= $this->next_n();
			$x .= "&nbsp; &nbsp;\n";
			$x .= $this->num_per_page();
			$x .= "</td><td><img src='$this->imagePath/frame/border2_ft_right.gif' width='25' height='25'></td>\n";
			$x .= "</tr>\n";
			if ($this->style==0) 
				$x .= "<tr><td colspan='2'>&nbsp;</td><td valign='top'><img src='/images/shading.gif' width='100%' height='5'></td><td>&nbsp;</td></tr>";
			$x .= "</table><br>\n";
		} else {
			$x  = "<table width='$percentage' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
			$x .= "<tr><td nowrap align='right'>\n";
			$x .= $this->record_range();
			$x .= "&nbsp;\n";
			$x .= $this->prev_n();
			$x .= $this->go_page();
			$x .= $this->next_n();
			$x .= "&nbsp;\n";
			$x .= $this->num_per_page();
			$x .= "</td></tr></table>\n";
		}

		return $x;
	}
	
	function record_range(){
		$n_title=$this->title;
		$n_end=min(($this->total_row),($this->pageNo*$this->page_size));
		$x="<span class='cal_day'>$n_title ".($this->n_start+1)." - $n_end, ".$this->total." $this->total_row</span>\n";
		return $x;
	}

	function prev_n(){
		
		$previous_icon = "<img src='$this->imagePath/icon/previous.gif' border=0 hspace=2 vspace=0 align='absmiddle'>";
		$n_page = $this->pageNo;
		$n_total = ceil($this->total_row/$this->page_size);
		$n_size = $this->page_size;
		if($n_page==1)
			$x = $previous_icon.$this->pagePrev."\n";
		else
			$x = "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
		return $x;
	}

	function next_n(){
		
		$next_icon = "<img src='$this->imagePath/icon/next.gif' border=0 hspace=2 vspace=0 align='absmiddle'>";
		$n_page = $this->pageNo;
		$n_total = ceil($this->total_row/$this->page_size);
		$n_size = $this->page_size;
		if($n_page==$n_total)
			$x = $this->pageNext.$next_icon."\n";
		else
			$x = "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
		return $x;
	}

	
	function go_page(){
		$n_page=$this->pageNo;
		$n_total=ceil($this->total_row/$this->page_size);
		/* remove "$this->pagename." from the beginning of "=" */
		$x=" <select class='inputfield' onChange='this.form.pageNo.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		for($i=1;$i<=$n_total;$i++){
			if($n_page==$i){
				$x.="<option value=$i selected>$i</option>\n";
			}else{
				$x.="<option value=$i>$i</option>\n";
			}
		}
		$x.="</select>\n";
		return $x;
	}
	
	function num_per_page(){
		global $range_all, $list_item_no;

		$x = " <select class='inputfield' name='num_per_page'  onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
		for($i=1;$i<=8;$i++){
			$j = $i * 10;
			/* since there is a problem of handling 90 or more checkbox, the max. no of items/page is set to 80
			if ($i==10) {
				$t=$range_all;
				$j=2000;
			} else 
			*/
				$t=$j;
			if($this->page_size==$j){
				$x.="<option value=$j selected>$t</option>\n";
			}else{
				$x.="<option value=$j>$t</option>\n";
			}
		}
		$x.="</select><span class='cal_day'>".$list_item_no."</span>\n";
		return $x;
	}
				
	function check($id){
		$x="<input type=checkbox name='checkmaster' onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
		return $x;
	}

	function displayFunctionbar($n, $m, $a, $b){
		$x = "";
		if(strstr($m , $n)){
			$x  = "<table width='96%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
			$x .= "<tr>\n";
			if ($a!="") {
				$x .= "<td width='25' nowrap align='right'>\n";
				$x .= "<img src='$this->imagePath/frame/border2_hd_left.gif' width='15' height='25'></td>\n";
				$x .= "<td nowrap background='$this->imagePath/frame/border2_header.gif'>".$a."</td>\n";
				$x .= "<td width='25'><img src='$this->imagePath/frame/border2_hd_right.gif' width='25' height='25'></td>\n";
			}
			$x .= "<td width='100%' height='25' align='right' nowrap>\n";
			$x .= $b;
			$x .= "&nbsp; &nbsp;</td>\n";
			$x .= "</tr>\n";
			$x .= "</table>\n";
		}
		echo $x;
	}

	function displayCell($i,$data, $cn){
		if ($data=="") $data="&nbsp;";
		switch ($this->column_array[$i]){
		case 0:
			$x = ($i==0 && $this->noNumber) ? "<td height='35' align='center' class=tableContent$cn>$data</td>\n" : "<td height='35' class=tableContent$cn>$data</td>\n";
			break;
		case 1:
			$x = "<td height='35' class=tableContent$cn>".$this->convertAllLinks(nl2br($data))."</td>\n";
			break;
		case 2:
			$x = "<td height='35' class=tableContent$cn>".stripslashes($data)."</td>\n";
			break;
		}
		return $x;
	}

	function displayColumn(){
		$x.="<tr><td nowrap background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td>\n";
		$x.=$this->column_list;
		$x.="<td nowrap background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
		return $x;
	}

	function display(){
		$i=0;
		$this->n_start=($this->pageNo-1)*$this->page_size;
		$this->rs=$this->db_db_query($this->built_sql());
		$total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
		$x.="<table width='96%' border='0' align='center' cellpadding='0' cellspacing='0'>\n";
		$x.="<tr>\n"; 
		$x.="<td width='1'><img src='$this->imagePath/frame/border2_up_left.gif' width='9' height='5'></td>\n";
		$x.="<td colspan='".$this->no_col."' nowrap background='$this->imagePath/frame/border2_bottom.gif'>\n";
		$x.="<img src='$this->imagePath/space.gif' width='1' height='5'></td>\n";
		$x.="<td width='1'><img src='$this->imagePath/frame/border2_up_right.gif' width='9' height='5'></td>\n";
		$x.="</tr>\n";
		$x.=$this->displayColumn();
		if ($this->db_num_rows()==0){
			$x.="<tr><td nowrap background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td><td class=tableContent2 align=center colspan=".$this->no_col."><br><br>".$this->no_msg."<br><br><br></td><td nowrap background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
		}else{
			//if($this->db_data_seek(($this->pageNo-1)*$this->page_size)){
			while($row = $this->db_fetch_array()) {
				$i++;
				if($i>$this->page_size) break;
				$x.="<tr><td background='$this->imagePath/frame/border2_left.gif'>&nbsp;</td>\n";
				$cn = ( $i%2 == 1) ? "1" : "2";
				$x.= ($this->noNumber) ? "" : "<td class=tableContent$cn height='35' align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
				for($j=0; $j<$total_col; $j++){
					$t=$row[$j];
					if ($j==1){
						$t=ereg_replace ("===@@@===", " ", $t);
					}
					$x.=$this->displayCell($j,$t, $cn);
				}
				$x.="<td background='$this->imagePath/frame/border2_right.gif'>&nbsp;</td></tr>\n";
			}
				//}
		}
		$x.="<tr><td><img src='$this->imagePath/frame/border2_bt_left.gif' width='9' height='5'></td>";
		$x.="<td colspan='".$this->no_col."' background='$this->imagePath/frame/border2_bottom.gif'>";
		$x.="<img src='$this->imagePath/space.gif' width='5' height='5'></td>";
		$x.="<td><img src='$this->imagePath/frame/border2_bt_right.gif' width='9' height='5'></td></tr>\n";
		if($this->db_num_rows()<>0) $x.= "<tr><td>&nbsp;</td><td colspan='".($this->no_col)."'>".$this->navigation()."</td><td>&nbsp;</td></tr>";
		$x.="</table>\n";
		if ($this->db_num_rows()==0) $x .= "<br>&nbsp;\n";
		$this->db_free_result();
		echo $x;
	}
	
	function cut_string($text, $length, $symbol = "..."){ 
		$length_text = strlen($text);
		$length_symbol = strlen($symbol); 

		if($length_text <= $length || $length_text <= $length_symbol || $length <= $length_symbol) 
			return($text); 
		else 
			return(substr($text, 0, $length - $length_symbol) . $symbol); 
	}
}

/*
updates:
number of items per page is allowed to be changed;
*/

?>