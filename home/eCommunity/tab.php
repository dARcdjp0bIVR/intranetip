<?php
//include_once("$intranet_root/includes/libqb.php");
//$lq = new libqb();

$GroupID = IntegerSafe($GroupID);

$tab_lgroup = new libgroup($GroupID);
$grp_navigation = "<b>\n";
if ($tab_lgroup->hasAdminBasicInfo($UserID))
{
    $grp_navigation .= "<a href=$intranet_httppath/home/school/settings/settings.php?GroupID=$GroupID>$i_GroupSettingsBasicInfo</a>\n";
    $delimiter = "|";
}
if ($tab_lgroup->hasAdminMemberList($UserID))
{
    $grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/members/?GroupID=$GroupID>$i_GroupSettingsMemberList</a>\n";
    $delimiter = "|";
}
if ($tab_lgroup->hasAdminInternalAnnounce($UserID) || $tab_lgroup->hasAdminAllAnnounce($UserID))
{
    $grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/announce/?GroupID=$GroupID>$i_GroupSettingsAnnouncement</a>\n";
    $delimiter = "|";
}
if ($tab_lgroup->hasAdminInternalEvent($UserID) || $tab_lgroup->hasAdminAllEvent($UserID))
{
    $grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/event/?GroupID=$GroupID>$i_GroupSettingsEvent</a>\n";
    $delimiter = "|";
}
if ($tab_lgroup->hasAdminQB($UserID) && $tab_lgroup->RecordType == 2)
{
    $grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/qb_admin/?GroupID=$GroupID>$i_QB_SubjectAdminLink</a>\n";
    $delimiter = "|";
}
if ($tab_lgroup->hasAdminInternalSurvey($UserID) || $tab_lgroup->hasAdminAllSurvey($UserID))
{
    $grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/survey/?GroupID=$GroupID>$i_GroupSettingsSurvey</a>\n";
    $delimiter = "|";
}
# Added on 26 Jan : Photo album
$grp_navigation .= "$delimiter<a href=$intranet_httppath/home/school/album/?GroupID=$GroupID>$i_GroupSettingsAlbum</a>\n";

$grp_navigation .= "</b>\n";

?>