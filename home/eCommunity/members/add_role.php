<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libgroup($GroupID);

$role = $_POST['role'];

if (trim($role)!="")
{
    $sql = "INSERT INTO INTRANET_ROLE (Title, RecordType, DateInput, DateModified) VALUES ('".htmlspecialchars(trim($role))."', '".$li->RecordType."', now(), now())";
//    debug($sql);
	$li->db_db_query($sql);

    $RoleID = $li->db_insert_id();
}

intranet_closedb();
$url = ($list ==1 ? "index.php":"add2.php");
header("Location: $url?GroupID=$GroupID&RoleID=$RoleID");
?>