<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup($GroupID);
$lu = new libgroup($GroupID);

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
//if($field=="") $field=2;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 2; break;
}
$order = ($order == 1) ? 1 : 0;

$keyword = stripslashes($keyword);
//if (b.Performance=\"\",CONCAT('&nbsp;'), CONCAT(b.Performance)) as Performance,
$sql = " Select c.Title,a.UserLogin, if(a.EnglishName=\"\",CONCAT('&nbsp;'),a.EnglishName),if(a.ChineseName=\"\", CONCAT('&nbsp;'),a.ChineseName) , if(a.ClassName=\"\",CONCAT('&nbsp;'),CONCAT(a.ClassName,a.ClassNumber)) as ClassName,  CONCAT('<input type=\"checkbox\" name=\"targetUserID[]\" value=\"', a.UserID ,'\">') from 
			INTRANET_USER AS a LEFT OUTER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID AND b.GroupID ='".$GroupID. "'
			LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID
			WHERE b.GroupID IS NOT NULL AND b.GroupID = '".$GroupID."'
			AND (
                          (a.UserLogin LIKE '%$keyword%') OR
                          (a.UserEmail LIKE '%$keyword%') OR
                          (a.EnglishName LIKE '%$keyword%') OR
                          (a.ChineseName LIKE '%$keyword%') OR
                          (a.ClassName LIKE '%$keyword%') OR
                          (a.ClassNumber LIKE '%$keyword%') OR
                          (b.Performance LIKE '%$keyword%') )
	";
# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("title","UserLogin","EnglishName","ChineseName","ClassName");
$li->sql = $sql;


//$li->IsColOff = "eCommForumSearchList";
$li->IsColOff = 2;
$li->no_msg = $i_frontpage_bulletin_no_record;

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' nowrap>".$li->column(0,$i_adminmenu_role)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' nowrap>".$li->column(1,$i_UserLogin)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'  nowrap>".$li->column(2,$i_UserEnglishName)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' nowrap>".$li->column(3,$i_UserChineseName)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' nowrap>".$li->column(4,$i_ClassNameNumber)."</td>\n";
//$li->column_list .= "<td class='tabletop tabletopnolink'  nowrap>$i_wordtemplates_performance</td>\n";
$li->column_list .= "<td width=1 class=\"tabletop tabletopnolink\">".$li->check("UserIDArr[]")."</td>\n";
$li->no_col = 7;

$AddBtn 	= "<a href=\"javascript:checkPost(document.form1,'add2.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $eComm['NewMember'] . "</a>";
$delBtn 	= "<a href=\"javascript:checkRole(document.form1,'targetUserID[]','delete.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'targetUserID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

$searchbar = "<input class=text type=text name=keyword value=\"".stripslashes($keyword)."\" size=15 maxlength=30>\n";
$searchbar .= $linterface->GET_BTN($eComm['Search'], "submit", "","button"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n";


### Button

$TypeStr= "<span class=\"ecomm_forum_title\">".$eComm['MemberList']."</span>";

$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='index.php?GroupID='+this.value\"",$GroupID);
$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['MemberSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><a href='../group/index.php?GroupID=$GroupID'><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle'></a> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_search.($intranet_session_language=="en"?" ":""). $eComm['Member'], "");
$linterface->LAYOUT_START();
?>
<script language="javascript">


</script>
<form name=form1 method=get>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                              	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td></td>
                                              	</tr>
                                          	</table>
					</td>
                                        <td align="right"><a href="setting.php?GroupID=<?=$GroupID?>" class="contenttool"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_prev_off.gif" width="11" height="10" border="0" align="absmiddle"> <?=$button_back?> </a></td>
				</tr>
				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                	<td class="tabletext" nowrap><?=$select_status?></td>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchbar?></td>
						</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$editBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>