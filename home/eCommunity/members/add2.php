<?php
/*******************************************
 *  modification log
 * 20100413 Marcus:
 * 		- added checking to exclude existng user when adding user into current group
 * *****************************************/
  
# Replace add.php in 2.0

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

# retrieve role type

$lo = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);
$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$luser = new libuser();

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,0,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
} 


         $row = $lo->returnRoleType();
         $RoleOptions .= "<select name=RoleID>\n";
         for($i=0; $i<sizeof($row); $i++)
         $RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
         $RoleOptions .= "</select>\n";

         $permitted = array(1,2,3,4);

         $li = new libgrouping();

         if($CatID > 10){
              unset($ChooseGroupID);
              $ChooseGroupID[0] = $CatID-10;
         }

         $teaching=0;
		if($CatID < 0){
		     unset($ChooseGroupID);
		     //$ChooseGroupID[0] = 0-$CatID;
		     switch($CatID){
			     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
			     case -3 : $ChooseGroupID[0]=3; $teaching=0;break;
			     
			     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
			     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
			     default:$ChooseGroupID[0]=1; $teaching=1;
			 }
		}
         
         $lgroupcat = new libgroupcategory();
		$cats = $lgroupcat->returnAllCat();
	    $x1  = ($CatID!=0 && $CatID > 0) ? "<select name=\"CatID\" onChange=\"checkOptionNone(this.form.elements['ChooseGroupID[]']);this.form.submit()\">\n" : "<select name=\"CatID\" onChange=\"this.form.submit()\"s>\n";
		$x1 .= "<option value=\"0\"></option>\n";
		for ($i=0; $i<sizeof($cats); $i++)
		{
		     list($id,$name) = $cats[$i];
		     if ($id!=0)
		     {
		         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
		     }
		}
		
		$x1 .= "<option value=0>";
		for($i = 0; $i < 20; $i++)
			$x1 .= "_";
		$x1 .= "</option>\n";
		$x1 .= "<option value=0></option>\n";
		
		
		$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
		$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
		    
		$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
		$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
				$x1 .= "<option value=0></option>\n";
				$x1 .= "</select>";
		
		 if($CatID!=0 && $CatID > 0) {
		     $row = $li->returnCategoryGroups($CatID);
		     $x2  = "<select name=ChooseGroupID[] size=5 multiple onchange='jFILTER_CHANGE(1)'>\n";
		     for($i=0; $i<sizeof($row); $i++){
		          $GroupCatID = $row[$i][0];
		          $GroupCatName = $intranet_session_language=="en" ? $row[$i][1] : $row[$i]['TitleChinese'];
		          $x2 .= "<option value=$GroupCatID";
		          for($j=0; $j<sizeof($ChooseGroupID); $j++){
		          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
		          }
		          $x2 .= ">$GroupCatName</option>\n";
		     }
		     $x2 .= "<option>";
		     for($i = 0; $i < 40; $i++)
		     $x2 .= "&nbsp;";
		     $x2 .= "</option>\n";
		     $x2 .= "</select>\n";
		}
		
		if(isset($ChooseGroupID)) {
		    if($CatID<0)
				$row = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching);
			else
		   		$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
		   	
		   	$existUser = $li->returnGroupUsersInIdentity(array($GroupID),$permitted);
		   	$existUserIDs = Get_Array_By_Key($existUser,"UserID");

		     $x3  = "<select name=ChooseUserID[] size=15 multiple>\n";
		     for($i=0; $i<sizeof($row); $i++)
		     {
		     	if(in_array($row[$i][0],$existUserIDs)) continue;
		     	$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
		     }
		     $x3 .= "<option>";
		     for($i = 0; $i < 40; $i++)
		     $x3 .= "&nbsp;";
		     $x3 .= "</option>\n";
		     $x3 .= "</select>\n";
		}
        $add_btn = $linterface->GET_ACTION_BTN($eComm['Add'], "button", " add_user(this.form)");
		$cancel_btn = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='setting.php?GroupID=".$GroupID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");	
         $step1 = getStepIcons(4,1,$i_SelectMemberNoGroupSteps);
         $step2 = getStepIcons(4,2,$i_SelectMemberNoGroupSteps);
         $step3 = getStepIcons(4,3,$i_SelectMemberNoGroupSteps);
         $step4 = getStepIcons(4,4,$i_SelectMemberNoGroupSteps);


         

$CurrentPage	= "Settings_MemberSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

	$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['MemberSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
	";
	$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
	
$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $eComm['Member'], "");
         ?>

         <script language="javascript">
         function add_role(obj){
              role_name = prompt("<?=$i_RoleNewPrompt?>", "New_Role");
              if(role_name!=null && Trim(role_name)!=""){
                   obj.role.value = role_name;
                   obj.action = "add_role.php";
                   obj.submit();
              }
         }
         
         function jFILTER_CHANGE(jChangeType)
		{
			obj = document.form1;
		
			if(jChangeType==1 && typeof(obj.ChooseGroupID)!="undefined")
				obj.ChooseGroupID.value = "";
		
			obj.action = "add2.php";
			obj.submit();
		}

         function add_user(obj){
	         if (typeof(obj.elements['ChooseUserID[]'])!="undefined" && obj.elements['ChooseUserID[]'].value!="")
	         {
		      
	         checkOption(obj.elements['ChooseUserID[]']);
              obj.action = "add_user.php";
              obj.submit();   
	         }	
	         else
	         {
		         alert('<?=$eComm['jsSelectStudent']?>');
	         
          }
         }

         function SelectAll(obj)
         {
                  for (i=0; i<obj.length; i++)
                  {
                       obj.options[i].selected = true;
                  }
         }
         </script>
<br/>
<form name=form1 action="" method=post>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_SelectMemberNoGroupSteps[1]?></span></td>
					<td><?php echo $RoleOptions; ?> <a href="javascript:add_role(document.form1)" class="tabletext"><?=newIcon2()." $button_new $i_admintitle_role"?></a></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_frontpage_campusmail_select_category; ?></span></td>
					<td><?php echo $x1; ?></td>
				</tr>
                                
                               <?php if($CatID!=0 && $CatID > 0) { ?> <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_frontpage_campusmail_select_group; ?></span></td>
					<td><?php echo $x2; ?> </td>
				</tr>
                                <tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"></span></td>					
                                        <td><!--<?php echo  $linterface->GET_BTN($i_frontpage_campusmail_expand, "button", "checkOption(this.form.elements['ChooseGroupID[]'])")?>--> </td>
                                        <!--<input type=image src=<?=$image_expand?> alt="<?php echo $i_frontpage_campusmail_expand; ?>" onClick="checkOption(this.form.elements['ChooseGroupID[]'])";>-->
				</tr><?php } ?>
				<?php if(isset($ChooseGroupID)) { ?>
                                <tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $i_frontpage_campusmail_select_user; ?></span></td>					
                                        <td><?php echo $x3; ?></td>
				</tr>
								<tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"></span></td>					
                                        <td><?php echo  $linterface->GET_BTN($eComm['select_all'], "button", "SelectAll(this.form.elements['ChooseUserID[]']); return false;")?></td>
                                        
				</tr>

				<?php } ?>				
                </table>
			</td>
                </tr>
                </table>
	</td>
</tr>

<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark"></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?=$add_btn?>
				<?=$cancel_btn?>
                		
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>

<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
<input type="hidden" name="role">
</form>

       
        

<?php
         
print $linterface->FOCUS_ON_LOAD("form1.RoleID");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>