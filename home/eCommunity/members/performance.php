<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../tab.php");

$GroupID = IntegerSafe($GroupID);
$targetUserID = IntegerSafe($targetUserID);

$li = new libgroup($GroupID);

if ($li->hasAdminMemberList($UserID))
{
         $targetUserID = (is_array($targetUserID)? $targetUserID[0]:$targetUserID);
         $lu = new libuser($targetUserID);
         $lg = new libgrouping();
         $lword = new libwordtemplates();
         $performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
         $mem_info = $li->returnMemberInfo($targetUserID);
         list ($Role,$Performance) = $mem_info;

    $toolbar .= "<input type=image src=$image_submit>";
    $toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
    $toolbar .= " <input type=image src=$image_cancel onClick=\"location='index.php?GroupID=$GroupID'; return false;\">";

         include_once("../../../templates/fileheader.php");
         ?>

<form name=form1 action="perform_update.php" method=post>
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $lgroup->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
      <tr>
            <td width="1"><img src="images/spacer.gif" width="1" height="1">
            </td>
      <td align=left width=749>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$i_GroupSettingsMemberList.displayArrow().$button_updateperformance?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" height="1"></td>
          </tr>
          <tr align="center">
            <td height=10 colspan="2"><img src="/images/spacer.gif" width="450" height="1"></td>
          </tr>
        </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td align=center>
              <table width=93% border=0 cellspacing=1 cellpadding=2>
                <tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
                <tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
                <tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
                <tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
                <tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
                <tr><td align=right><?php echo $i_ActivityName; ?>:</td><td><?=$li->Title?></td></tr>
                <tr><td align=right><?php echo $i_ActivityRole; ?>:</td><td><?=$Role?></td></tr>
                <tr><td align=right><?php echo $i_ActivityPerformance; ?>:</td><td><input type=text size=30 name=performance value='<?=$Performance?>'><?=$performancelist?></td></tr>
                <tr><td><br></td><td><?=$toolbar?></td></tr>
              </table>
            </td>
          </tr>
        </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
               </td>
             </tr>
           </table>
<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=targetUserID value=<?=$targetUserID?>>
</form>
<?php
         include_once("../../../templates/filefooter.php");
}
else
{
    header("Location: ../close.php");
}
intranet_closedb();
?>