<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libclubsenrol.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../tab.php");

$GroupID = IntegerSafe($GroupID);

$li = new libgroup($GroupID);
if ($li->hasAdminMemberList($UserID))
{

         $lclubsenrol = new libclubsenrol();
         if (!$lclubsenrol->isGroupAllowApproval($GroupID))
         {
              header ("Location: index.php?GroupID=$GroupID");
              exit();
         }
#####
# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 6;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        case 6: $field = 6; break;
        default: $field = 6; break;
}
$order = ($order == 0) ? 0 : 1;
if ($filter == "") $filter = -1;
if ($filter == -1)
{
    $conds = "";
}
else
{
    $conds = "AND a.RecordStatus = '$filter'";
}
$sql = "SELECT b.UserLogin, b.EnglishName, b.ChineseName
        ,IF(b.ClassName IS NULL OR b.ClassName = '','-',CONCAT(b.ClassName,'(',b.ClassNumber,')')) as Class
        ,a.Choice, CASE a.RecordStatus
              WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
              WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
              WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
              END
        ,a.DateModified
        ,CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
        FROM INTRANET_ENROL_GROUPSTUDENT as a LEFT OUTER JOIN
             INTRANET_USER as b ON a.StudentID = b.UserID
        WHERE a.GroupID = '$GroupID'
        AND (b.UserLogin LIKE '%$keyword%' OR
             b.EnglishName LIKE '%$keyword%' OR
             b.ChineseName LIKE '%$keyword%' OR
             b.ClassName LIKE '%$keyword%' OR
             a.Choice LIKE '%$keyword%')
             $conds
        ";
# TABLE INFO
$ltable = new libdbtable($field, $order, $pageNo);
$ltable->field_array = array("b.UserLogin","b.EnglishName", "b.ChineseName", "Class","a.Choice","a.RecordStatus","a.DateModified");
$ltable->sql = $sql;
$ltable->no_col = sizeof($ltable->field_array)+2;
$ltable->title = $i_admintitle_announcement;
$ltable->column_array = array(0,0,0,0,0,0,0);
$ltable->wrap_array = array(0,0,0,0,0,0,0);
$ltable->IsColOff = 4;
// TABLE COLUMN
$pos = 0;
$ltable->column_list .= "<td bgcolor=#FCD5AE width=15 class=title_head>#</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=90 class=title_head>".$ltable->column($pos++, $i_UserLogin)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=160 class=title_head>".$ltable->column($pos++, $i_UserEnglishName)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=110 class=title_head>".$ltable->column($pos++, $i_UserChineseName)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=80 class=title_head>".$ltable->column($pos++, $i_ClassNameNumber)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=50 class=title_head>".$ltable->column($pos++, $i_ClubsEnrollment_Priority)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=70 class=title_head>".$ltable->column($pos++, $i_ClubsEnrollment_Status)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=130 class=title_head>".$ltable->column($pos++, $i_ClubsEnrollment_LastSubmissionTime)."</td>\n";
$ltable->column_list .= "<td bgcolor=#FCD5AE width=15 class=title_head>".$ltable->check("StudentID[]")."</td>\n";

########

         $quotaDisplay = "";
         $groupQuota = $lclubsenrol->getQuota($GroupID);
         if ($groupQuota === false)
         {
             header ("Location: index.php?GroupID=$GroupID");
             exit();
         }
         $approvedCount = $lclubsenrol->getApprovedCount($GroupID);
         if ($groupQuota == 0)
         {
             $quotaDisplay .= "$i_ClubsEnrollment_GroupQuotaNo";
         }
         else
         {
             $quotaDisplay .= "$i_ClubsEnrollment_GroupQuota1 : $groupQuota";
         }
         $quotaDisplay .= "<br>\n$i_ClubsEnrollment_GroupApprovedCount : $approvedCount";

         include_once("../../../templates/fileheader.php");

         $sel_all = ($filter == -1? "SELECTED":"");
         $wai_all = ($filter == 0? "SELECTED":"");
         $rej_all = ($filter == 1? "SELECTED":"");
         $app_all = ($filter == 2? "SELECTED":"");
         $toolbar = "<SELECT name=filter onChange=this.form.submit()>\n";
         $toolbar .= "<OPTION value=-1 $sel_all>$i_status_all</OPTION>\n";
         $toolbar .= "<OPTION value=0 $wai_all>$i_ClubsEnrollment_StatusWaiting</OPTION>\n";
         $toolbar .= "<OPTION value=1 $rej_all>$i_ClubsEnrollment_StatusRejected</OPTION>\n";
         $toolbar .= "<OPTION value=2 $app_all>$i_ClubsEnrollment_StatusApproved</OPTION>\n";
         $toolbar .= "</SELECT>\n";
         $action_approve = "<a href=javascript:checkApprove(document.form1,'StudentID[]','approve.php')><img border=0 src=\"$image_path/btn_approve_$intranet_session_language.gif\"></a>";
         $action_reject = "<a href=\"javascript:checkAlert(document.form1,'StudentID[]','reject.php','$i_ClubsEnrollment_Alert_Reject')\"><img border=0 src=\"$image_path/btn_reject_$intranet_session_language.gif\"></a>";
         #$action_remove = "<a href=javascript:checkRemove(document.form1,'StudentID[]','remove.php')><img border=0 src=\"$image_delete\"></a>";

         if ($filter != 1 && $filter != 2)
         {
             $functionbar = "$action_approve $action_reject";
         }
         else
         {
             $functionbar = "";
         }

         $groupSelect .= jumpIcon().$li->getSelectAdminMember("name=GroupID onChange=this.form.submit()",$GroupID);
         $searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
         $searchbar .= "<input type=image src=\"$httppath$image_search\">\n";
         # From tab.php
         $grp_navigation .= "";
         //echo "<p>".$li->Title."</p>\n";
         include_once("../tooltab.php");
         ?>
         <SCRIPT LANGUAGE=JAVASCRIPT>
         </SCRIPT>

         <form name=form1 action="" method=get>
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
      <tr>
            <td width="1"><img src="images/spacer.gif" width="1" height="1">
            </td>
      <td align=left width=749>
      <?=$groupSelect?>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?="<a href=index.php?GroupID=$GroupID>$i_GroupSettingsMemberList</a>"?></font><?
            if ($lclubsenrol->isGroupAllowApproval($GroupID))
            {
                echo " | <font color=\"#031BAC\"> $i_ClubsEnrollment_EnrollmentList </font>";
            }
            ?></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" height="1"></td>
          </tr>
          <tr>
            <td colspan=2>
              <table width=100%>
                   <tr>
                     <td colspan=4 height=10>&nbsp;</td>
                   </tr>
                   <tr>
                     <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
                     <td width=310 valign="middle" align="left"><?=$quotaDisplay?></td>
                     <td width=400 valign=top align=right>&nbsp;</td>
                     <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
                   </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan=2>
              <table width=100%>
                   <tr>
                     <td colspan=4 height=10>&nbsp;</td>
                   </tr>
                   <tr>
                     <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
                     <td width=310 valign="middle" class="functionlink" align="left"><?=$toolbar?></td>
                     <td width=400 valign=top align=right><?=$searchbar?><br><?="$functionbar"?></td>
                     <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
                   </tr>
              </table>
            </td>
          </tr>
          <tr align="center">
            <td height=10 colspan="2"><img src="/images/spacer.gif" width="450" height="1"></td>
          </tr>
        </table>
              <?php echo $ltable->display(); ?>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
                   <tr>
                     <td colspan=3 height=10>&nbsp;
                     </td>
                   </tr>
                   <tr>
                     <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
                     <td class=decription>
                       <div align="right"><font color="#FF0000"><font color=red>*</font> - <?=$i_GroupKey?></font></div>
                     </td>
                     <td width="20" class="h1"><img src="/images/spacer.gif" width="20" height="1"></td>
                   </tr>
                 </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
               </td>
             </tr>
           </table>
         <input type=hidden name=field value="<?=$field?>">
         <input type=hidden name=order value="<?=$order?>">
         <input type=hidden name=pageNo value="<?=$pageNo?>">
         <input type=hidden name=page_size_change value="">
         <input type=hidden name=numPerPage value="<?=$li->page_size?>">
         </form>
<?php
         include_once("../../../templates/filefooter.php");
}
else
{
    header("Location: ../close.php");
}
intranet_closedb();
?>