<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$StudentID = IntegerSafe($StudentID);

$li = new libclubsenrol();
$StudentID = array_unique($StudentID);
$StudentID = array_values($StudentID);
if ($li->mode == 0 || !$li->isGroupAllowApproval($GroupID))
{
    header ("Location: index.php?GroupID=$GroupID");
}
else if ($li->mode == 1)
{
     $list = implode(",",$StudentID);
     $sql = "SELECT StudentID FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($list) AND RecordStatus = 0 AND GroupID = '$GroupID'";
     $waiting = $li->returnVector($sql);

     # Check Quota
     $counts = $li->getCounts($GroupID);
     $groupQuota = $counts[0];
     $targetSize = sizeof($waiting);
     if ($groupQuota != 0)
     {
         $approvedCount = $counts[1];
         $spaceLeft = $groupQuota - $approvedCount;
         if ($spaceLeft < $targetSize)
         {
             header ("Location: enrol.php?GroupID=$GroupID&msg=4");
             exit();
         }
     }
     if ($targetSize==0)
     {
         header("Location: enrol.php?GroupID=$GroupID&filter=$filter&msg=2");
         exit();
     }
     $waitingList = implode(",",$waiting);

     $sql = "LOCK TABLES INTRANET_ENROL_GROUPSTUDENT WRITE, INTRANET_ENROL_STUDENT WRITE,
             INTRANET_ENROL_GROUPINFO WRITE";
     $li->db_db_query($sql);
     $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 2 WHERE StudentID IN ($waitingList) AND GroupID = '$GroupID' AND RecordStatus = 0";
     $li->db_db_query($sql);
     $sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved = Approved + 1 WHERE StudentID IN ($waitingList)";
     $li->db_db_query($sql);
     $li->synApprovedCount($GroupID);
     $li->db_db_query("UNLOCK TABLES");
     $delimiter = "";
     for ($i=0; $i<sizeof($waiting); $i++)
     {
          $values .= $delimiter."(".$waiting[$i].",$GroupID)";
          $delimiter = ",";
     }
     $sql = "INSERT IGNORE INTO INTRANET_USERGROUP (UserID,GroupID) VALUES $values";
     $li->db_db_query($sql);
     $li->UpdateRole_UserGroup();
}
else
{
    $list = implode(",",$StudentID);
    $sql = "SELECT StudentID FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($list) AND RecordStatus = 0 AND GroupID = '$GroupID'";
    $waiting = $li->returnVector($sql);

    # Check Quota
    $counts = $li->getCounts($GroupID);
    $groupQuota = $counts[0];
    $targetSize = sizeof($waiting);
    if ($groupQuota != 0)
    {
        $approvedCount = $counts[1];
        $spaceLeft = $groupQuota - $approvedCount;
        if ($spaceLeft < $targetSize)
        {
            header ("Location: enrol.php?GroupID=$GroupID&msg=4");
            exit();
        }
    }
     if ($targetSize==0)
     {
         header("Location: enrol.php?GroupID=$GroupID&filter=$filter&msg=2");
         exit();
     }
    $waitingList = implode(",",$waiting);

    $sql = "LOCK TABLES INTRANET_ENROL_STUDENT WRITE, INTRANET_ENROL_GROUPSTUDENT WRITE, INTRANET_ENROL_GROUPINFO WRITE";
    $li->db_db_query($sql);

    # Divide into full list and unfull list
    $sql = "SELECT StudentID FROM INTRANET_ENROL_STUDENT WHERE Max > Approved AND StudentID IN ($waitingList)";
    $unfulled = $li->returnVector($sql);

    if (sizeof($unfulled)==0) $fulled = $waiting;
    else
    {
        for ($i=0; $i<sizeof($waiting); $i++)
        {
             if (!in_array($waiting[$i],$unfulled))
             {
                  $fulled[] = $waiting[$i];
             }
        }
    }

    # Manage Unfull list
    if (sizeof($unfulled)!=0)
    {
        $unfullList = implode(",",$unfulled);
        $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 2 WHERE StudentID IN ($unfullList) AND GroupID = '$GroupID'";
        $li->db_db_query($sql);
        $sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved = Approved + 1 WHERE StudentID IN ($unfullList)";
        $li->db_db_query($sql);
    }

    # Manage full list
    if (sizeof($fulled)!=0)
    {
        $fullList = implode(",",$fulled);
        $sql = "SELECT StudentID, MAX(Choice) FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($fullList) AND RecordStatus = 2 GROUP BY StudentID";
        $lowest = $li->returnArray($sql,2);
        $sql = "SELECT StudentID, Choice FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($fullList) AND GroupID = '$GroupID' AND RecordStatus = 0";
        $current = $li->returnArray($sql,2);
        for ($i=0; $i<sizeof($lowest); $i++)
        {
             list ($id,$choice) = $lowest[$i];
             $approved[$id] = $choice;
        }
        for ($i=0; $i<sizeof($current); $i++)
        {
             list ($id,$choice) = $current[$i];
             $original = $approved[$id];
             if ($original == "" || $original > $choice)
             {
                 # Cancel the original one and approved this one
                 $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 0 WHERE StudentID = '$id' AND Choice = '$original'";
                 $li->db_db_query($sql);
                 $sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 2 WHERE StudentID = '$id' AND GroupID = '$GroupID'";
                 $li->db_db_query($sql);
             }
             else
             {
                 # Nothing to do when the priority is lower
             }
        }

    }

    $li->synApprovedCount($GroupID);


    # Finishing
    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);

}

intranet_closedb();
header("Location: enrol.php?GroupID=$GroupID&filter=$filter&msg=2");
?>