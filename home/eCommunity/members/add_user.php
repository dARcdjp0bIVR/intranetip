<?php
# using: yat

include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libdb();

$GroupID = IntegerSafe($GroupID);

$lgroup = new libgroup($GroupID);
$CalID = $lgroup->CalID;

$AcademicYearID = $lgroup->AcademicYearID;
$ay = new academic_year($AcademicYearID);
$role = $_POST['role'];

if(isset($target))
{
	if(stristr($target,","))
		$target = explode(",",$target);
	else
		$target = array($target);
		
    //$insert_sql = "REPLACE INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified, EnrolGroupID) VALUES ";
    $insert_sql = "INSERT IGNORE INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified, EnrolGroupID) VALUES ";
	$delimiter = $some_failed = '';
	for ($i=0; $i<sizeof($target); $i++)
	{
		$targetType = substr($target[$i],0,1);
		
		$user = substr($target[$i],1);
		
		if($targetType=="U")
		{
			if(!$ay->Check_User_Existence_In_Academic_Year($user))
			{
				$some_failed = true;
				continue;
			}
          $sql .= $delimiter."($GroupID, ".$user.", $role, now(), now(), '$EnrolGroupID')";
			##############################################
			## START - add CALENDAR_CALENDAR_VIEWER
			##############################################
			$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				values 
				($CalID, ".$user.", $GroupID, 'E', 'R', '2f75e9', '1')
			";
			$li->db_db_query($cal_sql);
			##############################################
			## END - add CALENDAR_CALENDAR_VIEWER
			##############################################
			$delimiter = ',';
		  
		}
		else
		{
			$lg = new libgroup($user);
			$GroupMember = $lg->returnGroupUser();
			
			foreach($GroupMember as $k=>$d)
			{
				if(!$ay->Check_User_Existence_In_Academic_Year($d["UserID"]))
				{
					$some_failed = true;
					continue;
				}
				$sql .= $delimiter."($GroupID, ".$d["UserID"].", $role, now(), now(), '$EnrolGroupID')";
				
				##############################################
				## START - add CALENDAR_CALENDAR_VIEWER
				##############################################
				$cal_sql = "
					insert into CALENDAR_CALENDAR_VIEWER 
					(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
					values 
					($CalID, ".$d["UserID"].", $GroupID, 'E', 'R', '2f75e9', '1')
				";
				$li->db_db_query($cal_sql);
				##############################################
				## END - add CALENDAR_CALENDAR_VIEWER
				##############################################
				$delimiter = ',';	
			}
			
		}

	}
			if(!empty($sql))
			{
			    $success = $li->db_db_query($insert_sql.$sql);
	 			$li->UpdateRole_UserGroup();
	 		}
// 	 		if($success && !$some_failed)
// 			{
// 				$msg = 1;
// 			}
// 			else
// 			{
// 				$msg = 2;
// 			}


}

/*
if(sizeof($ChooseUserID) <> 0){
     $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID) VALUES ";
     for($i=0;$i<sizeof($ChooseUserID);$i++){
          $sql .= ($i==0) ? "" : ",";
          $sql .= "($GroupID, ".$ChooseUserID[$i].", $RoleID)";
     }
     $li->db_db_query($sql);
     $li->UpdateRole_UserGroup();
}
*/

intranet_closedb();
?>
<body onLoad="document.form1.submit();">
<form name=form1 action="setting.php" method="get">
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
<input type=hidden name=RoleID value="<?php echo $RoleID; ?>">
<input type=hidden name=msg value="add">
</form>
</body>