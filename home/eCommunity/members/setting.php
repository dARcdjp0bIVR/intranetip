<?php
## Using By : 
#############################################
##	Modification Log:
##	2010-10-27: Ivan
##	- Add remarks if purchased eEnrollment
##
##	2010-01-04: Max (200912311437)
##	- Setting enable forum
##	- Modified the checking of AdminAccessRight from !(substr($legroup->AdminAccessRight,7,1) to !(substr($legroup->AdminAccessRight,1,1)
#############################################
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();

$legroup = new libegroup($GroupID);

$lu = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);


$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,1,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}



$keyword = trim($keyword);
$field = ($field == "") ? 2 : $field;
$order = ($order == 1) ? 1 : 0;
$ClassNameOrder = ($order == 1)?"ASC":"DESC";
if($filter == "") $filter = 1;	
	
	//, if (b.Performance=\"\",CONCAT('&nbsp;'), CONCAT(b.Performance)) as Performance
	$sql = " Select c.Title,a.UserLogin, if(a.EnglishName=\"\",CONCAT('&nbsp;'),a.EnglishName),if(a.ChineseName=\"\", CONCAT('&nbsp;'),a.ChineseName) , if(a.ClassName=\"\",CONCAT('&nbsp;'),CONCAT(a.ClassName,'(',a.ClassNumber,')')) as ClassName, CONCAT('<input type=\"checkbox\" name=\"targetUserID[]\" value=\"', a.UserID ,'\">') from 
			INTRANET_USER AS a LEFT OUTER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID AND b.GroupID ='".$GroupID. "'
			LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID
			WHERE b.GroupID IS NOT NULL AND b.GroupID = '".$GroupID."'
			AND (
                          (a.UserLogin LIKE '%$keyword%') OR
                          (a.UserEmail LIKE '%$keyword%') OR
                          (a.EnglishName LIKE '%$keyword%') OR
                          (a.ChineseName LIKE '%$keyword%') OR
                          (a.ClassName LIKE '%$keyword%') OR
                          (a.ClassNumber LIKE '%$keyword%') OR
                          (b.Performance LIKE '%$keyword%') )
	";
	if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
	$pageSizeChangeEnabled = true;
	$li = new libdbtable2007($field, $order, $pageNo);		
	$li->field_array = array("c.title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName $ClassNameOrder, a.ClassNumber");
	$li->sql = $sql;
	//$li->IsColOff = "eCommAdminMemList";
	$li->IsColOff = "IP25_table";
	$li->column_list .= "<th class='tabletop tabletopnolink'>#</td>\n";
	$li->column_list .= "<th class=\"tabletop tabletopnolink\" nowrap>".$li->column(0, $i_adminmenu_role)."</th>\n";
	$li->column_list .= "<th class=\"tabletop tabletopnolink\" nowrap>".$li->column(1, $i_UserLogin)."</th>\n";
	$li->column_list .= "<th class=\"tabletop tabletopnolink\"  nowrap>".$li->column(2, $i_UserEnglishName)."</th>\n";
	$li->column_list .= "<th class=\"tabletop tabletopnolink\" nowrap>".$li->column(3, $i_UserChineseName)."</th>\n";
	$li->column_list .= "<th class=\"tabletop tabletopnolink\" nowrap>".$li->column(4, $i_ClassNameNumber)."</th>\n";
	//$li->column_list .= "<td class=\"tabletop tabletopnolink\"  nowrap>".$li->column(5, $i_wordtemplates_performance)."</td>\n";
	$li->column_list .= "<th width=1 class=\"tabletop tabletopnolink\">".$li->check("targetUserID[]")."</th>\n";
	//$li->no_col = 6;
	$li->no_col = sizeof($li->field_array)+2;

	
	$toolbar .= "<a href=javascript:add_role(document.form1)>".newIcon2()."$button_new $i_admintitle_role</a>";
	
	if ($legroup->RecordType != 0 && ( !$plugin['eEnrollment'] || $legroup->RecordType != 5)) 
	{
// 		$AddBtn 	= "<a href=\"javascript:checkPost(document.form1,'add2.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $eComm['NewMember'] . "</a>";
		$AddBtn 	= "<a class='new' href=\"javascript:newWindow('/home/common_choose/group_member.php?GroupID={$GroupID}',11)\">" . $eComm['NewMember'] . "</a>".toolBarSpacer();       
		$delBtn 	= "<a href=\"javascript:checkRole(document.form1,'targetUserID[]','delete.php')\" class=\"tool_delete\">" . $button_delete . "</a>";
		$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'targetUserID[]','edit.php')\" class=\"tool_edit\"> " . $button_edit . "</a>";

			$edittoolbar .= '<div class="common_table_tool">';
				$edittoolbar .= $editBtn;
				$edittoolbar .= $delBtn;
			$edittoolbar .= '</div>';
//		$edittoolbar .= '<table border="0" cellspacing="0" cellpadding="0">';
//		$edittoolbar .= '<tr>';
//			$edittoolbar .= '<td width="21"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/table_tool_01.gif" width="21" height="23" /></td>';
//			$edittoolbar .= '<td background="'.$image_path.'/'.$LAYOUT_SKIN.'/table_tool_02.gif">';
//				$edittoolbar .= '<table border="0" cellspacing="0" cellpadding="2">';
//				$edittoolbar .= '<tr>';
//					$edittoolbar .= '<td nowrap>'.$editBtn.'</td>';
//                    $edittoolbar .= '<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="5"></td>';
//                    $edittoolbar .= '<td nowrap>'.$delBtn.'</td>';
//				$edittoolbar .= '</tr>';
//					$edittoolbar .= '</table>';
//			$edittoolbar .= '</td>';
//				$edittoolbar .= '<td width="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/table_tool_03.gif" width="6" height="23" /></td>';
//		$edittoolbar .= '</tr>';
//		$edittoolbar .= '</table>';
	}
	$searchbar = '<div class="Conntent_search"><input type="text" name="keyword" value="'.$keyword.'"></div>';
//	$searchbar = "<input class=\"tabletext\" type=\"text\" name=\"keyword\" value=\"".stripslashes($keyword)."\" size=\"15\" maxlength=\"30\">\n";
//	$searchbar .= $linterface->GET_BTN($button_search, "submit")."\n";
	
	$TypeStr= "<span class=\"ecomm_forum_title\">".$eComm['MemberList']."</span>";
	
	$CurrentPage	= "Settings_MemberSettings";
	
	//$MyGroupSelection= $lu->getSelectGroupsByAdminRight(0,"name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
	$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

	$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['MemberSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
	";
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
	
	$linterface->LAYOUT_START();
	
	
	$MemberListMgmtWarning = '';
	if ($plugin['eEnrollment'] && $lu->RecordType == 5)
		$MemberListMgmtWarning = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['Group']['AdminRightArr']['MemberManagement_eEnrolRemarks']);
	
	
	?>
	<script language="javascript">
	function bulletin_view(id){
	        obj = document.form1;
	        obj.BulletinID.value = id;
	        obj.action = "message.php";
	        obj.submit();
	}
	function del(obj,element,page){
	        if(countChecked(obj,element)==0)
	                alert(globalAlertMsg2);
	        else{
	                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
	                obj.action=page;                
	                obj.method="POST";
	                obj.submit();				             
	                }
	        }
	}
	function checkRemoveThis(obj,element,page){
	        if(countChecked(obj,element)==0)
	                alert(globalAlertMsg2);
	        else{
	                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
	                obj.action=page;                
	                obj.method="POST";
	                obj.submit();				             
	                }
	        }
	}
	
	</script>
	<br />
	<form name=form1 method=get>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<div class="content_top_tool">
					<div class="Conntent_tool">
						<?=$AddBtn?>
					</div>
					<?=$searchbar?>
					<br style="clear: both;">	
				</div>
				<?=$MemberListMgmtWarning?>				
				<div class="table_board">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="bottom"></td>
							<td valign="bottom"><?=$edittoolbar?></td>
						</tr>
					</table>
					
					<?=$li->display();?>
					<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
					<input type=hidden name=order value="<?php echo $li->order; ?>">
					<input type=hidden name=field value="<?php echo $li->field; ?>">
					<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
					<input type=hidden name=numPerPage value="<?=$li->page_size?>">
					<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
					<input type=hidden name="target" value="">
					<input type=hidden name="role" value="">
				</div>
			</td>
		</tr>
	</table>
	</form> 
	
<?

$linterface->LAYOUT_STOP();
//}
//else
//{
	
    //header("Location: ../close.php");
  
//}

intranet_closedb(); 
?>