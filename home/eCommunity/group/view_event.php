<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ical_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$legroup 	= new libegroup($GroupID);

$MODULE_OBJ['title'] = $i_Events;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

echo $legroup->CalEventDisplay($ts);

intranet_closedb();
$linterface->LAYOUT_STOP();

?>