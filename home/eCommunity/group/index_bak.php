<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libannounce.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lo = new libuser($UserID);
$Groups = $lo->returnGroups();
# Limit user can only view timetable of own group
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];
if ($filter != 1 && $filter != 3) $filter = 2;

$li = new libgroup($GroupID);
$la = new libannounce();
$functionbar .= jumpIcon().$li->getSelectGroups("name=GroupID onChange=\"this.form.action='index.php'; this.form.submit()\"",$GroupID);
$filter_array = array( array(1,$i_EventAll), array(2,$i_EventUpcoming), array(3,$i_EventPast));
$filterbar = getSelectByArray($filter_array,"name=filter onChange=\"this.form.submit();\"",$filter,0,1);

include_once("../../../templates/fileheader.php");
include_once("../tooltab.php");
?>
<script language="javascript">

function moreNews(type)
{
         newWindow('<?=$intranet_httppath?>/home/moreannouncement.php?GroupID=<?=$GroupID?>&type='+type,1);
}
</script>
<form name=form1 method=get action=index.php>
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="101" class=popup_top><img src="<?=$image_path?>/groupinfo/heading_groupinfo_<?=$intranet_session_language?>.gif"></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td class=popup_topcell><font color="#FFFFFF" title="<?=$li->Title?>"><?php echo chopword($li->Title,40); ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
  <tr>
  <td width=10>&nbsp;</td>
  <td width=500 align=left><?php echo $functionbar; ?></td>
  <td width=230 align=right><?=$toolbar?></td>
  <td width=10>&nbsp;</td>
  </tr>
  <tr>
  <td width=10> </td>
  <td colspan=2>
    <table width=93% border=0 cellspacing=2 cellpadding=2>
      <tr>
        <td>
          <br><img src='<?=$image_path?>/index/announcement_bullet.gif'> <span class=h1><?=$i_AnnouncementGroup?></span><br>
          <?=$la->displayGroupAnnounceInGroup(array($GroupID))?>
        </td>
      </tr>
    </table>
    <table width=93% border=0 cellspacing=2 cellpadding=2>
      <tr>
        <td>
          <br><img src='<?=$image_path?>/index/announcement_bullet.gif'> <span class=h1><?=$i_GroupSettingsEvent?></span><br>
          <?=$filterbar?><br>
          <?=$li->displayGroupEventInGroup($filter)?>
        </td>
      </tr>
    </table>
  </td>
  <td width=10>&nbsp;</td>
  </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>


</td></tr>
</table>
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>