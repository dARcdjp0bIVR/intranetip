<?php
## Using By : 
#############################################
##	Modification Log:
##  Date 2018-10-12 Isaac
##  added isDisplayinEComunity() to check if the current group is allowed to be access in eComunity when page load
##
##	2017-07-26 Carlos
##	Cater https resource url.
##
##	2012-02-09 YatWoon
##	Improved: Keep track the last accessed group id
##
##	2011-10-12 YatWoon
##	Fixed: Failed to display default view mode which set in sharing settings
##
##	2011-08-23 YatWoon
##	Add alumni logic
##
##	2010-11-12	YatWoon
##	display club/group name according to lang session
##
##  20100901:	Marcus 
##	- fixed document.all caused js error.
##  20100114:	Marcus 
##	- hide groups which is not display in eCommunity in Selection Box
##	2010-01-04: Max (200912311437)
##	- disable forum when do not have function access right on it
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
//include_once($PATH_WRT_ROOT."includes/libcalevent{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();

$GroupID = IntegerSafe($GroupID);


if(!$GroupID)
{
	header("Location: ../index.php");
	exit;
}

$legroup 	= new libegroup($GroupID);
//$li 		= new libgroup($GroupID);
$la		 	= new libannounce();
$lu2007 	= new libuser2007($UserID);
$lb 		= new libbulletin();
$lcal		= new libcalevent2007();
$icalendar 	= new icalendar();  
	
if($lu2007->isInGroup($GroupID) && $lu2007->isDisplayinEComunity($GroupID))
{
	$CurrentPage	= "MyGroups";
	
	if($_SESSION['UserType']!=USERTYPE_ALUMNI)
		$MyGroupSelection= $legroup->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID,'',$foreComm=1);
}
else
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
//	$CurrentPage	= "OtherGroups";
//	$MyGroupSelection= $li->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID,'',$foreComm=1);
}
$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";
### keep track the last accessed Group ID
$legroup->UpdateLastAccess($GroupID);

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td width=\"50%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\" nowrap><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\" width=\"50%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\">
";

if($lu2007->isInGroup($GroupID))
	$rightMenu .= "	<a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a>";
else
	$rightMenu .= "&nbsp;";
$rightMenu .= "
				</td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu(1);
### End Right Menu ###



### Title ###
$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap> <div onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$legroup->TitleDisplay}</div></td>
			<td align='right' width='70%'>".$legroup->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
	";
$TAGS_OBJ[] = array($title,"");
	
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
// 	$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLC'];
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

$group_logo = $legroup->GroupLogo();
$announcement_table = $legroup->AnnouncementTable($la->returnGroupAnnouncement($GroupID, $legroup->IndexAnnounceNo, 1), $la->returnGroupAnnouncement($GroupID, $legroup->IndexAnnounceNo, 0), $legroup->IndexAnnounceNo);
if($legroup->CalID && $legroup->CalDisplayIndex && ($lu2007->isInGroup($GroupID) || (!$lu2007->isInGroup($GroupID) && $legroup->CalPublicStatus)) )
{
	$today = getdate();
	$calendar = "<td width=\"200\" height=\"200\" align=\"right\" valign=\"top\">
	<link href='".$http_protocol."{$eclass40_httppath}css/portal.css' rel='stylesheet' type='text/css'>

	";
	$icalendar_path ="newWindow('/home/iCalendar/index.php?Caltype=2&GroupID=$GroupID&action=CalDisable',31)";
	$calendar .="<div id='small_calendar' style='margin-left:0px'>";
		$calendar .="<div style='position:relative;left:-3px'  onclick=$icalendar_path>";
	$calendar .= $icalendar->generateMonthlySmallCalendar(date('Y'), date('m'), $GroupID, 2);
	$calendar .="</div>";
	$calendar .="</div>";
	//$calendar .= $legroup->calendar($legroup->CalID);
	$calendar .= "</td>";
}
$member_list = $legroup->member_list('all');

$share_area = $legroup->ShareArea();
### Function Access of Forum ###
# Group tools settings
# Field : FunctionAccess
# - Bit-1 (LSB) : timetable
# - Bit-2 : chat
# - Bit-3 : bulletin
# - Bit-4 : shared links
# - Bit-5 : shared files
# - Bit-6 : question bank
# - Bit-7 : Photo Album
# - Bit-8 (MSB) : Survey
# * shift by one in array... i.e. array_position = bit_position-1
define("FUNC_ACCESS_FORUM_BIT_POS", 2);	// bulletin
if ($legroup->isAccessTool(FUNC_ACCESS_FORUM_BIT_POS)) {
	$forum = $legroup->Forum($lb->returnLatestTopic($GroupID, 5));
} else {
	$forum = "&nbsp;";
}

//$DefaultViewMode = $legroup->DefaultViewMode ? $legroup->DefaultViewMode : 2;
$DefaultViewMode = $legroup->DefaultViewMode;
?>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>

<script language="javascript">
<!--
function viewfile(fileid)
{
	 with(document.form1)
	 {
		FileID.value = fileid;
		action = "../files/file_view.php";
		submit();
	 }
}


function newFile(t)
{
         with(document.form1)
         {
                action = "../files/"+t+"_new.php";
                submit();
         }
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt, ft ) 
{
	jFormObject.menutype.value=mt;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "../files/aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 
}

var callback_sharearea = 
{
	success: function (o)
	{
		jChangeContent( "share_div", o.responseText );						
	}
		
}
	
function jAJAX_SHARE_VIEW_MODE( jFormObject, t, s, ft ) 
{
	Block_Element("share_div");

	//alert(jFormObject.DisplayType.value);
	if( t==-1 )
		t = jFormObject.DisplayType.value;
	else
		jFormObject.DisplayType.value=t;
		
	jFormObject.start.value=s;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_share_area.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_sharearea);		
	
	jAJAX_SHARE_VIEW_MODE_SELECT(document.shareviewmodeform, t, ft);
	
	$("#ft_span_").attr("className","share_sub_list");
	$("#ft_span_p").attr("className","share_sub_list");
	$("#ft_span_v").attr("className","share_sub_list");
	$("#ft_span_w").attr("className","share_sub_list");
	$("#ft_span_f").attr("className","share_sub_list");
	$("#ft_span_wa").attr("className","share_sub_list");
//	document.all.ft_span_.className= "share_sub_list";
//	document.all.ft_span_p.className= "share_sub_list";
//	document.all.ft_span_v.className= "share_sub_list";
//	document.all.ft_span_w.className= "share_sub_list";
//	document.all.ft_span_f.className= "share_sub_list";
//	document.all.ft_span_wa.className= "share_sub_list";
		
	switch(ft)
	{
		case "P":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_photo.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Photo']?>";
//			document.all.ft_span_p.className= "share_sub_list_current";
			$("#ft_span_p").attr("className","share_sub_list_current");
			break;	
		case "V":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_video.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Video']?>";
//			document.all.ft_span_v.className= "share_sub_list_current";
			$("#ft_span_v").attr("className","share_sub_list_current");
			break;	
		case "W":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_website.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Website']?>";
//			document.all.ft_span_w.className= "share_sub_list_current";
			$("#ft_span_w").attr("className","share_sub_list_current");
			break;	
		case "F":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_file.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['File']?>";
//			document.all.ft_span_f.className= "share_sub_list_current";
			$("#ft_span_f").attr("className","share_sub_list_current");
			break;	
		case "WA":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_file.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['WaitingRejectList']?>";
//			document.all.ft_span_wa.className= "share_sub_list_current";
			$("#ft_span_wa").attr("className","share_sub_list_current");
			break;
		default:
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_shares.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['LatestShare']?>";
//			document.all.ft_span_.className= "share_sub_list_current";
			$("#ft_span_").attr("className","share_sub_list_current");
			break;	
	}
	
	$("#share_title").html(stitle);
//	document.all.share_title.innerHTML = stitle;

	
	
}

var callback_shareareaviewmode = 
{
	success: function (o)
	{
		jChangeContent( "shareviewmode_div", o.responseText );						
	}
}
	
function jAJAX_SHARE_VIEW_MODE_SELECT( jFormObject, t, ft )
{
	//jFormObject.DisplayType.value=t;
	jFormObject.DisplayType.value=document.shareform.DisplayType.value;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_share_area_view_mode_select.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_shareareaviewmode);		
}

function maxShareArea()
{
	document.maxshareaform.submit();
}

function maxForum()
{
	document.maxForumform.submit();
}

function viewfolder(folderid)
{
	 with(document.viewfolderform)
	 {
		FolderID.value = folderid;
		submit();
	 }
}

function bulletin_view(id){
        obj = document.bulletinform;
        obj.BulletinID.value = id;
        obj.action = "../bulletin/message.php";
        obj.submit();
}

function moreNews(type)
{
	newWindow('../../moreannouncement.php?group=<?=$GroupID?>&type='+type,1);
}

function jAJAX_MEMBER_LIST( jFormObject, tl ) 
{
	jFormObject.listtype.value=tl;
	jFormObject.GroupID.value=<?=$GroupID?>;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "../group/aj_member_list.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_memberlist);		
}

var callback_memberlist = 
{
	success: function (o)
	{
		//jChangeContent( "member_list_div", o.responseText );						
		jChangeContent( "member_list_div", o.responseText );						
	}
		
}

function jAJAX_GO_CALENDER( jFormObject, ParV, ParTS ) 
	{
		jChangeContent( "CalContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");
				
		jFormObject.v.value=ParV;
		jFormObject.ts.value=ParTS;
				
		YAHOO.util.Connect.setForm(jFormObject);
		path = "aj_calender.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_calender);				
	}
	
var callback_calender = 
	{
		success: function (o)
		{
			jChangeContent( "CalContent", o.responseText);						
		}
			
	}	
	
function view_event(ts){
        newWindow('view_event.php?ts='+ts+'&GroupID=<?=$GroupID?>',12);
}	

$().ready(function(){
	jAJAX_SHARE_VIEW_MODE(document.shareform, <?=$DefaultViewMode?>, 0, document.shareform.FileType.value)
});

//-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<!-- START GROUP LOGO //-->
			<?=$group_logo?>
			<!--END GROUP LOGO //-->
		</td>
		<td width=100%>
			<!-- START ANNOUNCEMENT TABLE //-->
			<?=$announcement_table?>
			<!--END ANNOUNCEMENT TABLE //-->
		</td>
		<!-- START CALENDAR //-->
		<?=$calendar?>
		<!-- END CALENDAR //-->
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="20%" height="100%" valign="top" rowspan="2">
			<!--START MEMEBR LIST //-->
			<?=$member_list?>
			<!-- END MEMEBR LIST //-->
		</td>
		<td valign="top">
			<!--START SHARE AREA //-->
			<?=$share_area?>
			<!-- END SHARE AREA //-->
		</td>
	</tr>
	<tr>
		<td valign="top" style=\"position:relative;\">
			<!-- START FORUM //-->
			<?=$forum?>
			<!-- END FORUM //-->
		</td>
	</tr>
</table>

<form name="form1" action="" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="">
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
<input type="hidden" name="indexpage" value="1">
</form>

<form name="shareform">
<input type="hidden" name="DisplayType" value="<?=$legroup->DefaultViewMode;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="start" value="0">
</form>

<form name="shareviewmodeform">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
</form>

<form name="maxshareaform" action="../files/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form>

<form name="maxForumform" action="../bulletin/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form>

<form name="viewfolderform" action="../files/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>

<form name="bulletinform" method="get">
<input type="hidden" name="BulletinID" value="">
<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
</form>     

<form name="memberlistform">
<input type="hidden" name="listtype" value="">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form> 

<form name="CalForm" >
<input type="hidden" name="v" value="<?=$v?>" />
<input type="hidden" name="ts" value="<?=$ts?>" />
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form>		
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>