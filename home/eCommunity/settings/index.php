<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lg = new libgroup($GroupID);
if ($lg->hasAdminBasicInfo($UserID))
{
    header("Location: settings.php?GroupID=$GroupID");
}
else if ($lg->hasAdminInternalAnnounce($UserID) || $lg->hasAdminAllAnnounce($UserID))
{
     header("Location: ../announce/settings.php?GroupID=$GroupID");
}
else if ($lg->hasAdminFiles($UserID))
{
    header("Location: ../files/settings_folder.php?GroupID=$GroupID");
}
else if ($lg->hasAdminBulletin($UserID))
{
    header("Location: ../bulletin/setting.php?GroupID=$GroupID");
}
else if ($lg->hasAdminMemberList($UserID))
{
     header("Location: ../members/setting.php?GroupID=$GroupID");
}
else # Not go to settings
{
	header("Location: ../group/?GroupID=$GroupID");
}

header("Location: ../settings/settings.php?GroupID=$GroupID");
?>