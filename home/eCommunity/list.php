<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/liborganization.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libgrouping();
$lcat = new libgroupcategory();

$content = get_file_content("$intranet_root/file/orpage.txt");
if ($content != "")
{
    $data = explode("\n",$content);
    $hideMember = ($data[0]==1);
    $hideEmail = ($data[1]==1);
    $default_cat = ($data[2]==""? 5:$data[2]);
    if ($default_cat == 0)
    {
        $default_cat = "";
    }
}
else
{
    $hideMember = false;
    $hideEmail = false;
    $default_cat = 5;
}

if (!isset($category))
{
     $category = $default_cat;
}
if ($category != "")
{
    $cats = $lcat->returnCatsForOrPage(1);
    $found = false;
    for ($i=0; $i<sizeof($cats); $i++)
    {
         if ($cats[$i][0]==$category)
         {
             $found = true;
             break;
         }
    }
    if (!$found)
    {
         $category = "";
    }
}
$cat_select = $lcat->returnSelectCategoryForOrPage("name=category onChange=\"this.form.submit();\"",true,1,$category);

#$x = $li->ListAllGroups2();
/*
for($i=1; $i<sizeof($i_GroupRoleOrganization); $i++){
     $col = ($i<3) ? 2 : 5;
     $img = $i_GroupRoleOrganization[$i];
     $groups = $li->returnCategoryGroups($i);
     $x .= "<p><table border=0 cellpadding=0 cellspacing=0>\n";
     $x .= "<tr><td>".${"i_frontpage_schoolinfo_organization_".$img."_t"}."</td></tr>\n";
     $x .= "<tr><td class=schoolinfo_organization_".$img."_m_bg align=center><br>\n";
     $x .= $li->displayOrganizationGroups($groups,$col);
     $x .= "<br><br></td></tr>\n";
     $x .= "<tr><td>".${"i_frontpage_schoolinfo_organization_".$img."_b"}."</td></tr>\n";
     $x .= "</table>\n";
}
*/
$lorg = new liborganization();
$groupsNotDisplay = $lorg->returnHideList();
$x = $li->ListAllGroupsForCat($category,$groupsNotDisplay);
$navigation = $i_frontpage_separator.$i_frontpage_schoolinfo.$i_frontpage_separator.$i_frontpage_schoolinfo_organization;
include_once("../../templates/homeheader.php");
?>
<table width="794" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp; </td>
  </tr>
  <tr>
    <td align="center">
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?php echo $i_frontpage_schoolinfo_title_organization; ?></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?php echo $i_organization_description; ?></td>
        </tr>
        <tr>
          <td height=30 width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <form name=form1 action='' method=GET>
            <table width="275" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCC7F">
                <tr>
                  <td width="16"><img src="<?=$image_path?>/organization/tab_left.gif" width="16" height="33"></td>
                  <td width="90"><img src="<?=$image_path?>/organization/tab_organization_<?=$intranet_session_language?>.gif"></td>
                  <td class=td_left_middle><?=$cat_select?></td>
                  <td width="16"><img src="<?=$image_path?>/organization/tab_right.gif" width="16" height="33"></td>
                </tr>
            </table>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align=center>
      <?php echo $x; ?>
      <br>
    </td>
  </tr>
</table>



<?php
include_once("../../templates/homefooter.php");
intranet_closedb();
?>