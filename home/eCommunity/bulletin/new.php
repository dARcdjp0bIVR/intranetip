<?php
# using: 

#############################################
##
##	Date:	2012-08-15 [YatWoon]
##			Remove "Set to Private" option
##
##	Date:	2010-06-21 [YatWoon]
##			Admin can now create forum topic in "settings" so that need to add a checking to cater which page should be back to
##
#############################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$BulletinID = IntegerSafe($BulletinID);

$linterface 	= new interface_html();
$lu2007 = new libuser2007($UserID);
$legroup 		= new libegroup($GroupID);
//$lu = new libgroup($GroupID);
$bu = new libbulletin($BulletinID, $ThreadID); 

$ForumAttStatus = $bu->CheckForumAttStatus($GroupID);
$ForumAttStatus = $ForumAttStatus[0]['ForumAttStatus'];

# Attachment Folder
# create a new folder to avoid more than 1 notice using the same folder
     //session_register("eCommBulletinAttFolder");
     $eCommBulletinAttFolder = session_id().".".time();
	 session_register_intranet("eCommBulletinAttFolder",$eCommBulletinAttFolder);
	
$li = new libfilesystem();

/*
$path = "$file_path/file/eComm/bulletin/$eCommBulletinAttFolder"."tmp";
if (!is_dir($path))
{
     $li->folder_new($path);
}
*/
if ($ForumAttStatus)
{
$attachment_list = "<select name='Attachment[]' size='4' multiple><option>";
        for($i = 0; $i < 40; $i++) 
        	$attachment_list .= "&nbsp;"; 
        $attachment_list .= "</option></select>";
$attachment_button = $linterface->GET_BTN($i_frontpage_campusmail_attach, "button","openFileWindow()");
$attachment_remove = $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])");
}
//$lgroup = new libgroup($GroupID);
$groupSelect .= $legroup->getSelectBulletinGroups("name=PostGroupID[] size=5 multiple",$GroupID);

$PAGE_NAVIGATION[] = array($eComm['NewTopic'], "");

if($lu2007->isInGroup($GroupID))
{
	$CurrentPage = "MyGroups";
	if($_SESSION['UserType']!=USERTYPE_ALUMNI)
	$MyGroupSelection= $legroup->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
	if($backto=="settings")
	{
		$CurrentPage	= "Settings_ForumSettings";
		$backURL = "setting.php";
	}
	else
	{
		$backURL = "index.php";
	}

}
else
{
//$CurrentPage = "OtherGroups";
//$MyGroupSelection= $lu->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
	header("Location: ../index.php?msg=AccessDenied");
	exit;

}



#Reply table
$ReplyTB ="<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
          
          <tr><td align=\"left\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
          <tr ><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\">".$eComm['Group']."</span></td>  
              <td>".$groupSelect."</td>  
          </tr>
          <tr ><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\">".$eComm['Title']." <span class='tabletextrequire'>*</span></span></td>  
              <td><input name=\"Subject\" type=\"text\" class=\"textboxtext2\" value=\"".$Subject."\"></td>  
          </tr>
          <tr><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\">".$eComm['Content']." <span class='tabletextrequire'>*</span></span></td>
              <td>".$linterface->GET_TEXTAREA("Message", $Message)."</td>   
          </tr>
          ";
if ($ForumAttStatus)
{
          $ReplyTB .="<tr><td width=\"30%\" ><span class=\"tabletext\">".$eComm['Attachment']."</span></td>
          	<td>".$attachment_list."</td>
          </tr>
          <tr><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\"></span></td>
          	<td>".$attachment_button."</td>
          </tr>
          <tr><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\">&nbsp;</span></td>
          	<td>".$attachment_remove."</td>
          </tr>";
      }
          /*
      		$ReplyTB .="<tr><td width=\"30%\" class=\"formfieldtitle\"><span class=\"tabletext\">". $eComm['set_private'] ." <img src=\"". $image_path ."/". $LAYOUT_SKIN ."/ecomm/icon_private.gif\" width=\"18\" height=\"18\" align=\"absmiddle\"></span></td>
          <td><input type=\"checkbox\" name=\"private\" value=\"1\" ></td></tr> ";
          */
          $ReplyTB .="</table></td>
          </tr>
          
          <tr>
			<td colspan=\"2\" align=\"left\">        
                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">
                <tr>
                	<td align=\"left\" class=\"tabletextremark\">&nbsp;&nbsp;&nbsp;".$i_general_required_field2."</td>
		</tr>
<tr>
                	<td class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\" /></td>
                </tr>
		  
          <tr>
            <td align=\"center\">
            ".
				$linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n".
                $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='". $backURL ."?GroupID=".$GroupID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
          ."</td></tr></table></td></tr></table>"; 


### Button


$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap> <div onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$legroup->TitleDisplay}</div></td>
			<td align='right' width='70%'>".$legroup->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();
?>
<script language="javascript">


function checkform(obj){
     if(countOption(obj.elements["PostGroupID[]"])==0){ alert(globalAlertMsg18); return false; }
     if(!check_text(obj.Subject, "<?php echo $i_frontpage_bulletin_alert_msg1;?>.")) return false;
     if(!check_text(obj.Message, "<?php echo $i_frontpage_bulletin_alert_msg2;?>.")) return false;
     checkOptionAll(obj.elements["Attachment[]"]);

	if(needalert)
	{
		 if(!confirm('<?=$Lang['eComm']['NotSendToFullGroup']?>'))
			return false;
	}
}

function openFileWindow()
{
        // newWindow('attach.php',10);
	var url="";
	var TargetGroup = document.getElementsByName('PostGroupID[]')[0];
	for(var i=0; i<TargetGroup.options.length;i++)
	{
		if(TargetGroup.options[i].selected)
			url+="&targetgroup[]="+TargetGroup.options[i].value;
	}
	//alert(url);
	newWindow('attach.php?GroupID=<?=$GroupID?>&folder=<?=$eCommBulletinAttFolder?>'+url,2)		
}

//if some groups exceed quota, alert user new topic will not be post
var needalert=false;
function addExceedQuotaGroup(groupname,groupid)
{
	document.form1.ExceedQuotaGroup.value=groupname;
	document.form1.ExceedQuotaGroupId.value=groupid;
	
	if(groupname)
	{
		var msg = "<?=$Lang['Group']['NotEnoughSpace']?>"+groupname;
		$("span#warnmsg").html(msg);
		$("span#warnmsg").css("display","block");
		needalert=true;
	}
	else
	{
		$("span#warnmsg").html("");
		$("span#warnmsg").css("display","none");
		needalert=false;
	}
	
}
</script>
<br>
<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'>
		<table border="0" cellpadding="3" cellspacing="0"  >
			<tr> 
				<td nowrap='nowrap'><span id="warnmsg" class="systemmsg" style='display:none'></span></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center" colspan=2>
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
      			<td width="12" valign="bottom"  width="12" height="19"></td>
      			<td width="13" ></td>
      			<td><?=$ReplyTB?></td>
      			<td width="18" ></td>
    			</tr>
    		<!--end--->
            </table>
        </td>
</tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=filesize value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=BulletinID value="">
<input type=hidden name=GroupID value="<?php echo $GroupID; ?>">
<input type="hidden" name="ExceedQuotaGroup" value="">
<input type="hidden" name="ExceedQuotaGroupId" value="">
<input type="hidden" name="backto" value="<?=$backto?>">
</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>