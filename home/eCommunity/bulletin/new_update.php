<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$Subject = htmlspecialchars($Subject);
$Message = htmlspecialchars($Message);


$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);

$li = new libuser($UserID);
$PublicStatus = $private? 0 : 1;
$ExceedQuotaGroupIdArr=explode(",",$ExceedQuotaGroupId);
for($i=0; $i<sizeof($PostGroupID); $i++)
{
	if(in_array($PostGroupID[$i],$ExceedQuotaGroupIdArr))
		continue;
	$path = "$file_path/file/eComm/bulletin/".$eCommBulletinAttFolder.$PostGroupID[$i];
	$lu = new libfilesystem();
	$command ="mv $path"."tmp $path";

	$command= OsCommandSafe($command);	
	exec($command);

	/*if($bug_tracing['eComm_bulletin_attachment_log']){
		
				 $temp_log_filepath = "$file_path/file/log_eComm_bulletin_attachment.txt";
				 $temp_time = date("Y-m-d H:i:s");
				 $temp_user = $UserID;
				 $temp_page = 'eComm_bulletin_attach_update.php';
				 $temp_action=$command;
				 $temp_content = get_file_content($temp_log_filepath);
				 $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
				 $temp_content .= $temp_logentry;
				 write_file_content($temp_content, $temp_log_filepath);
			 }*/

	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;

	while (list($key, $value) = each($files)) {
		 if(!strstr($AttachmentStr,$files[$key][0])){
				/*if($bug_tracing['eComm_bulletin_attachment_log']){
				 $temp_log_filepath = "$file_path/file/log_eComm_bulletin_attachment.txt";
				 $temp_time = date("Y-m-d H:i:s");
				 $temp_user = $UserID;
				 $temp_page = 'eComm_bulletin_attach_update.php';
				 $temp_action="remove file:".$path."/".$files[$key][0];
				 $temp_content = get_file_content($temp_log_filepath);
				 $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
				 $temp_content .= $temp_logentry;
				 write_file_content($temp_content, $temp_log_filepath);
			 }*/
			  $lu->file_remove($path."/".$files[$key][0]);
		 }
	}
	
	$Attachment = $AttachmentStr?$eCommBulletinAttFolder.$PostGroupID[$i]:"NULL";
	//$IsAttachment = (isset($Attachment)) ? 1 : 0;
	if($AttachmentStr)
	{
		$folderinfo=$lu->folder_size($path);
		$filesize=ceil($folderinfo[0]/1000);
	}
	else
		$filesize="NULL";
		
     if($li->IsGroup($PostGroupID[$i])){
          $sql = "INSERT INTO INTRANET_BULLETIN (ParentID, GroupID, UserID, UserName, UserEmail, Subject, Message, Attachment, Attsize, ReadFlag, PublicStatus, DateInput, DateModified) VALUES (0, ".$PostGroupID[$i].", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$Subject', '$Message', '$Attachment', '$filesize', ';$UserID;', '$PublicStatus', now(), now())";
          $li->db_db_query($sql);
		//debug($sql);
     }
	 
}

intranet_closedb();
$backURL = $backto=="settings" ? "setting.php":"index.php";
header("Location: ". $backURL ."?GroupID=$GroupID&msg=add");
?>