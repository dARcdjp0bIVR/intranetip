<?php
## Using By : 
#############################################
##	Modification Log:
##	2010-01-04: Max (200912311437)
##	- Add a row for setting enable forum
##	- Modified the checking of AdminAccessRight from !(substr($legroup->AdminAccessRight,2,1) to !(substr($legroup->AdminAccessRight,7,1)
##	  where 2 is the checking of internal announcement and 7 is forum(bulletin)
#############################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$legroup = new libegroup($GroupID);

### START check user access rights ###
$GroupsID = $legroup->hasAnyGroupAdminRight();

if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,7,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/index.php?GroupID=$GroupID");
	exit;
}

$bu = new libbulletin($BulletinID, $ThreadID); 
$bu->updateAttachFlag($AllowAtt,$GroupID);
$ForumAttStatus = $bu->CheckForumAttStatus($GroupID);
$ForumAttStatus = $ForumAttStatus[0]['ForumAttStatus'];

### Function Access of Forum ###
# Group tools settings
# Field : FunctionAccess
# - Bit-1 (LSB) : timetable
# - Bit-2 : chat
# - Bit-3 : bulletin
# - Bit-4 : shared links
# - Bit-5 : shared files
# - Bit-6 : question bank
# - Bit-7 : Photo Album
# - Bit-8 (MSB) : Survey
# * shift by one in array... i.e. array_position = bit_position-1
define("FUNC_ACCESS_FORUM_BIT_POS", 2);	// bulletin
define("CHECKED", " checked='checked' ");
define("UNCHECKED", "");
if ($legroup->isAccessTool(FUNC_ACCESS_FORUM_BIT_POS)) {
	$enableChecked = CHECKED;
	$disableChecked = UNCHECKED;
} else {
	$enableChecked = UNCHECKED;
	$disableChecked = CHECKED;
}
$AllowForumBtn = "
	<input type='radio' name='AllowForum' value='1' id='AllowForum1' " . $enableChecked . " /><label for='AllowForum1'>" . $i_general_yes . "</label><input type='radio' name='AllowForum' value='0' id='AllowForum0' " . $disableChecked . " /><label for='AllowForum0'>" . $i_general_no . "</label>
";


$AllowAttBtn="<input type=\"radio\" name=\"AllowAtt\" value=\"1\" id=\"AllowAtt1\" ";
$ForumAttStatus?$AllowAttBtn.="checked":"";
$AllowAttBtn.=">";
$AllowAttBtn.="<label for=\"AllowAtt1\">".$i_general_yes."</label>"; 
$AllowAttBtn.="<input type=\"radio\" name=\"AllowAtt\" value=\"0\" id=\"AllowAtt0\" ";
$ForumAttStatus?"":$AllowAttBtn.="checked";
$AllowAttBtn.=">";
$AllowAttBtn.="<label for=\"AllowAtt0\">".$i_general_no."</label> ";
//$AllowAttBtn.= $linterface->GET_BTN($button_update, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ");

 	
//if (!$AllowAtt) 
	
### END check user access rights ###

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_ForumSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";

$TAGS_OBJ[] = array($eComm['Management'],"setting.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($i_general_BasicSettings,"setting_basic.php?GroupID=$GroupID", 1);

$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;

$pstatus_str = (isset($pstatus) && $pstatus!='-1') ? " and c.PublicStatus=$pstatus " : "";	



if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

### Button




?>
<SCRIPT LANGUAGE=Javascript>
function bulletin_view(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        obj.action = "message.php";
        obj.submit();
}
function showRead(id)
{
         newWindow('read.php?BulletinIDArr='+id,1);
}

</SCRIPT>

<br />

<form name="form1" method="get" action="setting.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td>
        <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
        <tr> 
        	<td valign="top">
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<!-- Allow using forum -->
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Allow_Forum']?></span></td>
					<td class="tabletext">
						<?=$AllowForumBtn?>
					</td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Allow_Att']?></span></td>
					<td class="tabletext">
						<?=$AllowAttBtn?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
        </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
		</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>
<br />
<input type=hidden name="BulletinID" value="">
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="FuncAccessValue_Forum" id="FuncAccessValue_Forum" value="<?=FUNC_ACCESS_FORUM_BIT_POS?>"/>
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.AllowAtt1");
$linterface->LAYOUT_STOP();

?>
