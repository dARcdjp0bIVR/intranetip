<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$lo = new libuser($UserID);
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	header("Location: ../../school/");
	exit;
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../index.php");
    exit();
}

$onTop = $onTop ? 1 : 0;
$PhotoResize = $FolderType == 'P'? $_POST['PhotoResize'] : "0";
$DemensionX = $PhotoResize == 1? $_POST['DemensionX'] : "NULL";
$DemensionY = $PhotoResize == 1? $_POST['DemensionY'] : "NULL";

//$sql = "update INTRANET_FILE_FOLDER set FolderType='$FolderType', Title='$Title', PublicStatus='$PublicStatus', onTop='$onTop', PhotoResize=$PhotoResize, DemensionX=$DemensionX, DemensionY=$DemensionY where FolderID=$FolderID";
$sql = "update INTRANET_FILE_FOLDER set FolderType='$FolderType', Title='$Title', onTop='$onTop', PhotoResize='$PhotoResize', DemensionX='$DemensionX', DemensionY='$DemensionY' where FolderID='$FolderID'";
$legroup->db_db_query($sql);

$xmsg = ($legroup->db_affected_rows()<=0) ? "update_failed" : "update";

intranet_closedb();
header("Location: settings_folder.php?xmsg=$xmsg&GroupID=$GroupID&FolderType=$FolderType");

?>