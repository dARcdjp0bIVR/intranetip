<?php
##	Modifying By: 
#######################################
##	Modification Log:
##	2017-07-26 Carlos: Cater https resource url.
##	2010-03-05 Max (201003051446)
##	- get the GroupID from previous page
#######################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup($GroupID);
$lo 			= new libuser($UserID);
$lfolder 		= new libfolder($FolderID);

$lu 	= new libgroup($GroupID);
$lu2007 	= new libuser2007($UserID);
if (!$lu->isAccessFiles() or !$lu2007->isInGroup($GroupID))
{
    header("Location: ../index.php");
    intranet_closedb();
    exit();
}

$FileID = $FileID[0];

### Title ###
$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?FileType=$FileType&DisplayType=$DisplayType&GroupID='+this.value\"",$GroupID);

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu();
### Start Right Menu ###

$TypeNameStr = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_photo.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" border=\"0\"> ". $eComm['Edit_'] . $eComm['Photo'];

### File Field
for($f=1;$f<=10;$f++)
{
	$ff .= "
	<tr id=\"ff_$f\" style=\"display:". ($f==1?"inline":"none") ."\">
		<td width=\"25\" class=\"tabletext\">$f.</td>
		<td class=\"tabletext\"><input type=\"file\" name=\"myfile[]\" class=\"file\"></td>
	</tr>
	";
}

### File cell
	$lf = new libfile($FileID);
	$FileType = $lf->FileType;
	$PublicStatus = $lf->PublicStatus;
	
	$file_cell = "
		<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
		  <tr class=\"albumtabletop albumtabletoptext\">
		  	<td align=\"left\"><strong>". $lf->Title ."</strong></td>
		    </tr>
		  <tr class=\"forumtablerow\">
		    <td class=\"forumtablerow tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		      <tr>
		        <td width=\"220\" align=\"center\" valign=\"top\"><div id=\"photo_border_nolink\">
				<span>
				". $legroup->DisplayPhoto($FileID, 150) ."
				</span></div></td>
		        <td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		          <tr>
		            <td width=\"30%\" align=\"left\" class=\"tabletext\">". $eComm['Title'] ." </td>
		            <td align=\"left\"><input name=\"usertitle\" type=\"text\" class=\"textboxtext2\" value=\"". ($lf->UserTitle==""? $lf->Title : $lf->UserTitle) ."\" /></td>
		          </tr>
		          <tr>
		            <td align=\"left\" class=\"tabletext\" valign=\"top\">". $eComm['Description'] ."</td>
		            <td align=\"left\">". $linterface->GET_TEXTAREA("description", $lf->Description ) ."</td>
		          </tr>
		          <tr>
		            <td align=\"left\" class=\"tabletext\">". $eComm['move_to_album'] ." </td>
		            <td align=\"left\"><span class=\"tabletextremark\"><?=$FileType?>
		              ". $lfolder->getFolderList($GroupID, "name='FolderID'", $lf->FolderID, $FileType) ."
		            </span></td>
		          </tr>
		          <tr>
		            <td align=\"left\" class=\"tabletext\">". $eComm['remove_this_file'] ." </td>
		            <td align=\"left\"><label>
		              <input type=\"checkbox\" name=\"remove\" value=\"1\">
		            </label></td>
		          </tr>
		          <tr>
		            <td align=\"left\" class=\"tabletext\">". $eComm['set_private'] ." <img src=\"". $image_path ."/". $LAYOUT_SKIN ."/ecomm/icon_private.gif\" width=\"18\" height=\"18\" align=\"absmiddle\"></td>
		            <td align=\"left\"><input type=\"checkbox\" name=\"private\" value=\"1\" ". ($PublicStatus==1?"":"checked") ."></td>
		          </tr>
		        </table></td>
		      </tr>
		    </table></td>
		    </tr>
			</table>
			</a>
	";

?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">
function newFile(t)
{
         with(document.form1a)
         {
                action = t+"_new.php";
                submit();
         }
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 

}

</script>

<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top">
	            <br />
	            
	            <form name="form1" method="post" action="file_edit_update.php">
	            <input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
				<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
				<input type="hidden" name="FileID" value="<?=$FileID;?>">
								
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				            
				<!------------- Start edit photo details -------------//-->
				<tr>
					<td align="center">
						<!--------- Start photo edit cell ---------------//-->
						<?=$file_cell?>
						<!--------- End photo edit cell ---------------//-->
					</td>
				</tr>
				<!------------- End edit photo details -------------//-->
					
					
				<tr>
					<td class="tabletext">
						<table width="95%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td align="right">
							<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2") ?>
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)","button_cancel") ?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
              
              </table>
				</form> 
	            
	               
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	            
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_view_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
<input type="hidden" name="currentMode" value="add">
</form>

<form name="form1a" action="file_view.php" method="post">
<input type="hidden" name="DisplayType" value="<?=$DisplayType?>">
<input type="hidden" name="FileType" value="<?=$FileType?>">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="FileID" value="<?=$FileID?>">
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>