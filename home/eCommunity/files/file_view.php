<?php
// Using:

/********************** Change Log ***********************/
#
#	Date    :	2019-09-04	Bill    [2019-0815-1424-47235]
#	Details	:	add csrf token for checking
#
#	Date    :	2017-07-26	Carlos
#	Details	:	Cater https resource url.
#
#	Date    :	2011-09-17	YatWoon
#	Details	:	update layout of Waiting for Approval
#
#	Date    :	2011-05-03	YatWoon
#	Details	:	add alt / title for the icon
#
#	Date	:	2010-09-27 [Thomas]
#	Details	:	add previous and next button beside the image of the file
#				modified JS function file_btn_check()
#
#	Date    :   2010-09-06 [marcus]
#   Details :   modifed js function view(id) , use window.location = instead of newWindow, which will cause problem in IE
#
#	Date    :   2010-07-06 [Thomas]
#   Details :   use AJAX to load file image, file info and comment
#
# 	Date	:	2010-06-24 [Yuen]
#	Details	:	improved to show photos no larger than 600x500 by width or height respectively
#
# 	Date	:	2009-12-17 [Yuen]
#	Details	:	fixed the problem that owner cannot see the edit button in single view
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libdbtable.php');
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);
$FileID = IntegerSafe($FileID);

$linterface = new interface_html();
$legroup 	= new libegroup($GroupID);
$lf 		= new libfile($FileID);
$lo 		= new libuser($UserID);
$lu 		= new libgroup($GroupID);
$lu2007 	= new libuser2007($UserID);

if(!$lf->viewFileRight() )
{
	header("Location: ./index.php?GroupID=$GroupID&msg=AccessDenied");
    intranet_closedb();
    exit();
}

if($lu2007->isInGroup($GroupID))
{
	$CurrentPage = "MyGroups";
	if($_SESSION['UserType'] != USERTYPE_ALUMNI) {
	    $MyGroupSelection = $lu->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
    }
}
else
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
	
//	$CurrentPage	= "OtherGroups";
//	$MyGroupSelection= $lu->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}

// $FileType		= $lf->FileType ? $lf->FileType : $FileType;
$FileType = $lf->Approved!=1 ? "WA" : $lf->FileType;
if ($FileType=="F" && !$lu->isAccessFiles() || $FileType=="V" && !$lu->isAccessVideo() ||$FileType=="P" && !$lu->isAccessPhoto() )
{
	header("Location: ./index.php?GroupID=$GroupID&msg=AccessDenied");
    intranet_closedb();
    exit();
}

if (!$FileID && !$FolderID)
{
    header("Location: ./index.php?GroupID=$GroupID");
    intranet_closedb();
    exit();
}

if (!$FolderID)
{
	$lf 		= new libfile($FileID);
	$FolderID	= $lf->FolderID;
}
$lfolder = new libfolder($FolderID);

if(!$FileID)
{
	$FileID = $lfolder->FirstFileID();
	$lf = new libfile($FileID);
}

if(!$lu2007->isInGroup($lf->GroupID) || ($lf->Approved == 0 or $lf->Approved == -1) and $lf->UserID != $UserID)
{
    header("Location: ../index.php?GroupID=$GroupID&msg=AccessDenied");
    intranet_closedb();
    exit();
}

$DisplayType = cleanCrossSiteScriptingCode($DisplayType);

####################################################
# nevigation bar
####################################################
if($FileType=="WA")
{
	$PAGE_NAVIGATION = array(
		$eComm['WaitingApproval'], "index.php?GroupID=$GroupID&DisplayType=$DisplayType&FileType=$FileType",
		$lf->Title,
	);
}
else
{
	$PAGE_NAVIGATION = array(
		$eComm[$FileType], "index2.php?GroupID=$GroupID&DisplayType=$DisplayType&FileType=$FileType",
		$lfolder->Title, "index.php?GroupID=$GroupID&DisplayType=$DisplayType&FileType=$FileType&FolderID=$FolderID",
		$lf->Title,
	);
}

####################################################
# define file/folder type and related variables
####################################################
switch($FileType)
{
	case "F":
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_file.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['File'];
		break;
	case "P":
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_photo.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['Photo'];
		break;
	case "W":
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_website.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['Website'];
		break;
	case "V":
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_video.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['Video'];
		break;
	case "WA":
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_file.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['WaitingRejectList'];
		break;
	default:
		$TypeImage = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_shares.gif" width="20" height="20" align="absmiddle">';
		$TypeName = $eComm['LatestShare'];
		break;
}
$TypeNameStr = $TypeImage . $TypeName;

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu();
### End Right Menu ###

if($FileType!="WA")
{
	### Start Display mode icons ###
	$DisplayIcons = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";
	
	$DisplayIcons .= "<td>";
	$DisplayIcons .= "<a href=\"index.php?GroupID={$GroupID}&FileType={$FileType}&FolderID={$FolderID}\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list.gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\" onMouseOver=\"MM_swapImage('view_list','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>";
	$DisplayIcons .= "</td>";

	$DisplayIcons .= "<td>";
	$DisplayIcons .= "<a href=\"index.php?GroupID={$GroupID}&FileType={$FileType}&DisplayType=4&FolderID={$FolderID}\" onMouseOver=\"MM_swapImage('view_4','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4.gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\"></a>";
	$DisplayIcons .= "</td>";

	$DisplayIcons .= "<td>";
	$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1_on.gif\" name=\"view_1\" width=\"24\" height=\"20\" border=\"0\" id=\"view_1\">";
	$DisplayIcons .= "</td>";

	$DisplayIcons .= "</tr></table>";
	### End Display mode icons ###
}

################################
### Start File Info ###
################################
$FileInfo = "
	<tr>
	<td align=\"center\">
	<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	<tr>
	<td width=\"80\" align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['Title'] .": </span></td>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\"><strong> ". $lf->FileUserTitle() ."</strong></span>". $lf->StatusImg() ."</td>
	<td align=\"left\" class=\"share_file_title\">&nbsp;</td>
	<td align=\"right\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['CreatedBy'] .": <strong>". $lf->UserName ." </strong></span></td>
	</tr>
	<tr>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $i_FileTitle .": </span></td>
	<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $lf->Title ."</span></td>
	<td align=\"left\" class=\"share_file_title\">&nbsp;</td>
	<td align=\"right\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['CreatedDate'] .": ". $lf->DateInput ." </span></td>
	</tr>
";

if($lf->Description)
{
	$FileInfo .= "
		<tr>
		<td align=\"left\" class=\"share_file_title\" valign=\"top\"><span class=\"tabletext\">". $i_FileDescription .": </span></td>
		<td align=\"left\" class=\"share_file_title\" colspan=\"3\"><span class=\"tabletext\">". nl2br($lf->Description) ." </span></td>
		</tr>
	";
}

if($FileType=="WA") 
{
	$FileInfo .= "
		<tr>
		<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['ApprovalStatus'] .": </span></td>
		<td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". ($lf->Approved==-1 ? $eComm['Rejected'] : $eComm['WaitingApproval']) ."</span></td>
		<td align=\"left\" class=\"share_file_title\" colspan=\"2\">&nbsp;</td>
		</tr>
	";
}

$FileInfo .= "
	</table></td>
	</tr>
	</table>
	</div>
";
### End File Info ###

################################
### Start File Image ###
################################
if($FileType=="W")
{
	$url = strstr($lf->Location,$http_protocol)?$lf->Location:$http_protocol.$lf->Location."/";
	$link = "<a href=\"". $url ."\" target=\"_blank\">";
}
else
{
	$link = "<a href=\"javascript:view(". $FileID .");\">";
}

if($FileType!="WA")
{
	$prv_link = "<a herf=\"javascript:void(0);\" onClick=\"nav_viewfile('pre');\" class=\"contenttool\" style=\"cursor:pointer\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_prev.gif\" border=\"0\"></span></a>";
	$next_link = "<a herf=\"javascript:void(0);\" onClick=\"nav_viewfile('next');\" class=\"contenttool\" style=\"cursor:pointer\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_next.gif\" border=\"0\"></span></a>";
}

$file_image = "
	<tr>
	<td align=\"center\">
		<table>
		<tr>
			<td id=\"pre_file_btn\" align=\"left\" style=\"visibility:hidden\"><div id=\"btn_large\">$prv_link</div></td>
			<td><div id=\"photo_border\" align=\"center\"> <span>". $legroup->DisplayPhoto($FileID, 600, 500, 1, $link)  ."</span></div></td>
			<td id=\"next_file_btn\" align=\"right\" style=\"visibility:hidden\"><div id=\"btn_large\">$next_link</div></td>
		</tr>
		</table>
		<br>
	</td>
	</tr>
";
### End File Image ###

$FileList  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
if($lf->Approved == 1) {
    $FileList .= $legroup->DisplayFileListTable($FileID, $FolderID, $FileType);
}

#############################################
## Comment Area
#############################################

$CommentArea = "
		<tr id=\"CommentArea\">
		";
if($lf->Approved == 1)
{
	$CommentArea .= "
		<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		  <tr>
		    <td align=\"center\">

		    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		      <tr>
		        <td align=\"left\"><span class=\"tabletext\"> <strong><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $eComm['Comment'] ." (<span id=\"CommentNo\">". $lf->getCommentNumber() ."</span>) </strong></span></td>
		        <td align=\"right\"><span id=\"write_comment_str\"><a href=\"javascript:void(0);\" onClick=\"toggle_comemnt_form();\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['WriteYourcomment'] ." </a></span></td>
		      </tr>
		    </table>

		    <div id=\"write_comment_form\" style=\"display:none\">
			<form name=\"form1\" action=\"\" method=\"post\" onSubmit=\"return checkForm(this);\">
			<input type=\"hidden\" name=\"DisplayType\" value=\"". $DisplayType ."\">
			<input type=\"hidden\" name=\"FileType\" value=\"". $FileType ."\">
			<input type=\"hidden\" name=\"GroupID\" value=\"". $GroupID ."\">
			<input type=\"hidden\" name=\"FolderID\" value=\"". $FolderID ."\">
			<input type=\"hidden\" name=\"FileID\" value=\"". $FileID ."\">
			<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
			<tr>
			  <td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"><span class=\"tabletext\">". $eComm['Yourcomment'] ."</span></td>
			</tr>
			<tr>
			  <td align=\"center\">". $linterface->GET_TEXTAREA("Comment","") ."</td>
			</tr>
			<tr>
			  <td align=\"center\">
			  <input name=\"submit\" type=\"button\" class=\"formbutton\" value=\"". $button_submit ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"AddComment()\">
			  <input name=\"submit\" type=\"reset\" class=\"formbutton\" value=\"". $button_reset ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\">
			  <input name=\"cancel_btn\" type=\"button\" class=\"formbutton\" value=\"". $button_cancel ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"toggle_comemnt_form()\">
			  </td>
			</tr>
			<tr>
			  <td align=\"right\" class=\"dotline\">&nbsp;</td>
			</tr>
			</table>
			</form>
			</div>

	";

	$CommentArea .= "<div id=\"Comment_List\">".$legroup->DisplayComments($FileID)."</div>";
	$CommentArea .= "
		      </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table></td>

	";
}
$CommentArea .= "
		</tr>
	";
## End Comment Area

$file_main_content = "
	<div id=\"main_content_1\">
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">

	<tr>
		<td align=\"left\">". $linterface->GET_NAVIGATION3($PAGE_NAVIGATION) ."</td>
	</tr>
	";

if($lu->hasAdminFiles($UserID) || $lf->UserID==$UserID)
{
	$delBtn = "<a href=\"javascript:RemoveThis();\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
	if($lu->hasAdminFiles($UserID) || $lf->UserID==$UserID)	$editBtn 	= "<a href=\"javascript:clickEdit()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

	$file_main_content .= "
			<tr>
	    	<td align=\"center\">
	    	<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	    	<tr>
	    		<td align=\"right\">". $linterface->GET_SYS_MSG($xmsg, $xmsg2) ."</td>
			</tr>
			<tr>
	    		<td align=\"right\">{$editBtn} {$delBtn}</td>
			</tr>
			</table>
			</td>
			</tr>
	";
}

if($FileID)
{
	$file_main_content .= $file_image;
	$file_main_content .= $FileInfo;
	$file_main_content .= $FileList;
	$file_main_content .= $CommentArea;
}
else
{
	$file_main_content .= "<tr><td class='tabletext' align='center'><br>".$i_no_record_exists_msg."<br><br></td></tr>";
}
$file_main_content .= "</table>";

### Title ###
$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";

$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}

$linterface->LAYOUT_START();
?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">

function file_btn_check(fileid)
{
	var File_start = $('#FileStart').val();
	var File_end = $('#FileEnd').val();
	
	if(fileid == File_start)
		$('#pre_file_btn').attr('style', 'visibility:hidden');
	else
		$('#pre_file_btn').attr('style', 'visibility:visible');
	
	if(fileid == File_end)
		$('#next_file_btn').attr('style', 'visibility:hidden');
	else
		$('#next_file_btn').attr('style', 'visibility:visible');
}

function newFile(t)
{
     with(document.form1a)
     {
        action = t+"_new.php";
        submit();
     }
}

function view(id){
	window.location= "view.php?FileID="+id
    //newWindow("view.php?FileID="+id,5);
}

function viewfile(fileid)
{
	var DisplayType = $('#form1a_DisplayType').val();
	var FileType = $('#form1a_FileType').val();
	var GroupID = $('#form1a_GroupID').val();
	var FolderID = $('#form1a_FolderID').val();
	var CurrentFileID = $('#form1a_FileID').val();
	
	$('#img_'+ CurrentFileID).removeClass('current_select_item');
	$('#img_'+ fileid).addClass('current_select_item');
	
	var CurrentFileObj = document.getElementById('img_'+CurrentFileID);
	var NextFileObj    = document.getElementById('img_'+fileid);
	if(document.all){
		CurrentFileObj.setAttribute('onmouseout', function(){this.className='';});
		NextFileObj.setAttribute('onmouseout', function(){this.className='current_select_item';});
	}
	else{
		CurrentFileObj.setAttribute('onmouseout', 'this.className=\'\'');
		NextFileObj.setAttribute('onmouseout', 'this.className=\'current_select_item\'');
	}

	$('#form1a_FileID').val(fileid);
	$('#delform_FileID').val(fileid);
	
	$('#main_content_1').load(
		'file_view_ajax/ajax_main_content_1.php', 
		{'File_attr[]': [FileType, GroupID, fileid, FolderID, DisplayType]},
		function(){
			file_btn_check(fileid);
		}
	);
	
	$('#CommentArea').load(
		'file_view_ajax/CommentArea.php', 
		{'File_attr[]': [FileType, GroupID, fileid, FolderID, DisplayType]}
	);
}

function nav_viewfile(direction)
{
	var FileID_List = document.getElementById("FileID_List").elements;
	var CurrentFileID = $('#form1a_FileID').val();
	var next_FileID = 0;
	
	var AlbumWidth   = document.getElementById("albumarea").clientWidth;
	var total_len = 0;
	
	for(var i=0;i<FileID_List.length;i++)
	{
		var CurrentFile_Length = document.getElementById("img_" + FileID_List[i].value).clientWidth;
		total_len = total_len + CurrentFile_Length;
		
		if(FileID_List[i].value == CurrentFileID){
			if(direction == "pre"){
				next_FileID = FileID_List[i-1].value;
				total_len = total_len - CurrentFile_Length / 2;
				break;
			}
			else if (direction == "next"){
				var NextFile_Length = document.getElementById("img_" + FileID_List[i+1].value).clientWidth;
				next_FileID = FileID_List[i+1].value;
				total_len = total_len - NextFile_Length / 2;
				break;
			}
		}
	}
	document.getElementById("albumarea").scrollLeft = total_len - AlbumWidth / 2;
	viewfile(next_FileID);
}

function toggle_comemnt_form()
{
	if($("#write_comment_form").css("display") == "none")
	{
		$("#write_comment_form").show();
		$("#write_comment_str").html("");
	}
	else
	{
		$("#write_comment_form").hide();
		$("#write_comment_str").html("<a href=\"javascript:void(0);\" onClick=\"toggle_comemnt_form();\" class=\"contenttool\"><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"><?=$eComm['WriteYourcomment']?></a>");
	}
}

function checkForm(obj)
{
	if(!check_text(obj.Comment, "<?=$i_alert_pleasefillin.$eComm['Yourcomment']?>")) return false;

}

var callback_rightmenu =
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );
	}
}

function jAJAX_RIGHT_MENU( jFormObject, mt )
{
	jFormObject.menutype.value=mt;

	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);

	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif";
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif";
}

function RemoveThis()
{
    if(confirm("<?=$eComm['jsDeleteRecord']?>"))
    {
        document.delform.submit();
    }
}

function clickEdit()
{
	document.delform.action="file_edit.php";
	document.delform.submit();
}

function AddComment()
{
	var DisplayType = $('#form1a_DisplayType').val();
	var FileType = $('#form1a_FileType').val();
	var GroupID = $('#form1a_GroupID').val();
	var FolderID = $('#form1a_FolderID').val();
	var CurrentFileID = $('#form1a_FileID').val();
	var Comment = $('#Comment').val();
	
	var no_of_comment = parseInt($('#CommentNo').text());
	no_of_comment++;
	$('#CommentNo').text(no_of_comment);
	
	$('#Comment_List').load('comment_add_update.php', {'File_attr[]': [CurrentFileID, Comment]});
	var Comment = $('#Comment').val('');
	toggle_comemnt_form();
}

var albumareaObj;
var albumareatableObj;

$().ready(function() {
	file_btn_check($('#form1a_FileID').val());
	albumareaObj = $("div#albumarea");
	albumareatableObj = $("table#albumareatable");
	albumareaObj.width(700);
	albumareaObj.width(albumareatableObj.width()-50);
});

$(window).resize(function () {
	albumareaObj.width(albumareatableObj.width()-50);
});
</script>

<br>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="right"><?=$DisplayIcons?></td>
	              </tr>

	              <tr>
	                <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
	                  <tr>
	                    <td align="center">

	                    <?=$file_main_content?>

	                    </td>
	                  </tr>
	                </table>
	                </td>
	              </tr>
	            </table>
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>

	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>
	            </td>

	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_view_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>

<form name="form1a" action="file_view.php" method="get">
<input type="hidden" id="form1a_DisplayType" name="DisplayType" value="<?=$DisplayType?>">
<input type="hidden" id="form1a_FileType" name="FileType" value="<?=$FileType?>">
<input type="hidden" id="form1a_GroupID" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" id="form1a_FolderID" name="FolderID" value="<?=$FolderID?>">
<input type="hidden" id="form1a_FileID" name="FileID" value="<?=$FileID?>">
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>

<form name="delform" action="file_remove.php" method="get">
<input type="hidden" id="delform_DisplayType" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" id="delform_FileType" name="FileType" value="<?=$FileType;?>">
<input type="hidden" id="delform_GroupID" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" id="delform_FolderID" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" id="delform_FileID" name="FileID[]" value="<?=$FileID?>">

<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>