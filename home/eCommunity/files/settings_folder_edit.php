<?php
#using:
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lu = new libgroup($GroupID);
$lu2007 = new libuser2007($UserID); 

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
    exit();
}

$CurrentPage	= "Settings_SharingSettings";
$MyGroupSelection= $legroup->getSelectAdminShared("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

$FolderID = (is_array($FolderID)? $FolderID[0]:$FolderID);
$lfolder = new libfolder($FolderID);

if(!$lu2007->isInGroup($lfolder->GroupID))
{
	header("Location: ./?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

$FolderTypeSelect 	= "<select id='FolderType' name='FolderType' onchange='javascript:showphotosize()'>\n";
$FolderTypeSelect 	.= "<option value='P' ".(($lfolder->FolderType=='P')?"selected":"").">". $eComm['Photo'] ."</option>\n";
$FolderTypeSelect 	.= "<option value='V' ".(($lfolder->FolderType=='V')?"selected":"").">". $eComm['Video'] ."</option>\n";
$FolderTypeSelect 	.= "<option value='W' ".(($lfolder->FolderType=='W')?"selected":"").">". $eComm['Website'] ."</option>\n";
$FolderTypeSelect 	.= "<option value='F' ".(($lfolder->FolderType=='F')?"selected":"").">". $eComm['File'] ."</option>\n";
$FolderTypeSelect 	.= "</select>\n";

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eComm['EditFolder']);

?>

<script language="JavaScript">
function checkForm(obj)
{
	if(!check_text(obj.Title, "<?=$i_alert_pleasefillin?> <?=$eComm['Folder']?>")) return false;

}

function showphotosize()
{
	var folder_type = $('#FolderType').val();
	if(folder_type == 'P')
		$('#photosize').attr('style', '');
	else
		$('#photosize').attr('style', 'display:none');
}

$(document).ready(function(){
	$('#PhotoResize0').click(function(){
		$('#ResizeDemension').attr('style', 'visibility: hidden');
	})
	$('#PhotoResize1').click(function(){
		$('#ResizeDemension').attr('style', '');
	})
});
</script>

<br />   
<form name="form1" method="post" action="settings_folder_edit_update.php" onSubmit="return checkForm(this);">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" value="<?=$FolderID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
                <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
                <tr> 
                	<td valign="top">
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['FolderType']?></span></td>
							<td class="tabletext"><?=$FolderTypeSelect?></td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Folder']?></span></td>
							<td class="tabletext"><input type='text' name='Title' maxlength='255' value='<?=$lfolder->Title?>' class='textboxtext'></td>
						</tr>
						
						<? /* ?>
						<tr valign='top'>
							<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['Status']?></span> </td>
							<td class='tabletext'>
								<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" <?=$lfolder->PublicStatus==1?"checked":""?>> <label for="PublicStatus1"><?=$eComm['Public'];?></label> 
								<input type="radio" name="PublicStatus" value="0" id="PublicStatus0" <?=$lfolder->PublicStatus==0?"checked":""?>> <label for="PublicStatus0"><?=$eComm['Private'];?></label>
							</td>
						</tr>
						<? */ ?>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
							<td class="tabletext">
								<input type="checkbox" name="onTop" value="1" <?=$lfolder->onTop?"checked":""?>>
							</td>
						</tr>
						<tr valign="top" id="photosize" style="<?=$lfolder->FolderType=='P'? "" : "display:none"?>">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eComm']['PhotoSize']?> </span></td>
							<td class="tabletext">
								<input type="radio" name="PhotoResize" value="0" id="PhotoResize0"<?=$lfolder->PhotoResize == 1? "":"checked"?>> <label for="PhotoResize0"><?=$Lang['eComm']['Origanal']?></label>
								<input type="radio" name="PhotoResize" value="1" id="PhotoResize1"<?=$lfolder->PhotoResize == 1? "checked":""?>> <label for="PhotoResize1"><?=$Lang['eComm']['Resize']?></label>&nbsp;
								<span id="ResizeDemension" <?=$lfolder->PhotoResize == 1? "" : "style=\"visibility: hidden\""?>>(<input type="text" name="DemensionX" maxlength="5" value="<?=$lfolder->DemensionX?>" style="width:40px">&nbsp;x&nbsp;<input type="text" name="DemensionY" maxlength="5" value="<?=$lfolder->DemensionY?>" style="width:40px">px - <?=$Lang['eComm']['Width']?> x <?=$Lang['eComm']['Height']?>)</span>
							</td>
						</tr>
						</table>
					</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)") ?>
		</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>                        
</form>

<br />

<?php
print $linterface->FOCUS_ON_LOAD("form1.FolderType");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>