<?php
// Using:

###################################
#
#	Date:	2019-09-04	Bill    [2019-0815-1424-47235]
#           added checking before delete any files
#	        updated csrf token handling
#
#   Date:   2019-06-06 Anna
#           Added CSRF token checking.
#
#	Date:	2011-09-17	YatWoon
#			improved: del WA file, return to WA page (not FileType page)
#
###################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$csrf_token = $_POST['csrf_token']? $_POST['csrf_token'] : $_GET['csrf_token'];
$csrf_token_key = $_POST['csrf_token_key']? $_POST['csrf_token_key'] : $_GET['csrf_token_key'];
// if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
if(!verifyCsrfToken($csrf_token,$csrf_token_key)){
    header('Location: /');
    exit;
}

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FileID = IntegerSafe($FileID);

$lo = new libfilesystem();
$lg = new libgroup($GroupID);

for($i=0;$i<sizeof($FileID);$i++)
{
	$thisFileID = $FileID[$i];
    if(!$thisFileID) {
        continue;
    }

	$lf = new libfile($thisFileID);
	$FolderID = $lf->Approved==1 ? $FolderID : "";
	$FileType = $lf->Approved==1 ? $lf->FileType : "WA";
	
	// Check if user is owner or admin
	if (!($lg->hasAdminFiles($UserID) || $lf->UserID==$UserID))
	{
	    header("Location: index2.php?GroupID=$GroupID");
	    intranet_closedb();
	    exit();
	}
	
	// Delete file records
	$lo = new libfilesystem();
	if ($lg->hasAdminFiles($UserID) || $lf->UserID==$UserID)
	{
		$Folder = $intranet_root."/file/group/g".$lf->GroupID;
		$path = $Folder."/".$lf->Location;
		
		if($lf->GroupID && $lf->Location != '' && file_exists($path)) {
            $lo->lfs_remove($path);
        }

		// $lf->db_db_query("DELETE FROM INTRANET_FILE_COMMENT WHERE FileID = '$thisFileID'");
        $sql = "DELETE FROM INTRANET_FILE_COMMENT WHERE FileID = '$thisFileID'";
        $lf->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_FILE WHERE FileID = '$thisFileID'";
		$lf->db_db_query($sql) or die(mysql_error());
	}
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="index.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
</form>
</body>