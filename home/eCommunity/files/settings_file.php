<?php

######################################
#
#	Date:	2014-01-13	YatWoon	[Case#X57661]
#			Fixed: failed to sort table ordering 
#
#	Date:	2013-04-24	YatWoon
#			Hide "public/private" [Case#2013-0412-0957-26073]
#
######################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$linterface = new interface_html();
$lgroup = new libgroup($GroupID);
$legroup = new libegroup($GroupID);
$lfolder = new libfolder($FolderID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
	header("Location: ../group/?GroupID=$GroupID");
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPage	= "Settings_SharingSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);
$pstatus = $pstatus>-1 ? $pstatus : -1;
//$astatus = isset($astatus) ? $astatus : 1;

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('settings_file_new.php?GroupID=$GroupID&FileType=$FileType')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'FileID[]','settings_file_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'FileID[]','settings_file_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
$approveByn = "<a href=\"javascript:checkApprove(document.form1,'FileID[]','settings_file_batch_approval.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_approve . "</a>";

#Filtering Tags
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag 	.= "<td>";

#File Type
$searchTag 	.= "&nbsp;<select name='FileType' onChange='document.form1.FolderID.selectedIndex=0;this.form.submit();'>\n";
$searchTag 	.= "<option value=''>$i_status_all ". $eComm['FileType'] ."</option>\n"; 
$searchTag 	.= "<option value='P' ".(($FileType=='P')?"selected":"").">". $eComm['Photo'] ."</option>\n";
$searchTag 	.= "<option value='V' ".(($FileType=='V')?"selected":"").">". $eComm['Video'] ."</option>\n";
$searchTag 	.= "<option value='W' ".(($FileType=='W')?"selected":"").">". $eComm['Website'] ."</option>\n";
$searchTag 	.= "<option value='F' ".(($FileType=='F')?"selected":"").">". $eComm['File'] ."</option>\n";
$searchTag 	.= "</select>\n";

#Folder
$searchTag .= $lfolder->getFolderList($GroupID, "name='FolderID' onChange='this.form.submit();'", $FolderID, $FileType, 1, '-1', $i_status_all . " " . $eComm['Folder']);

/*
#Public/Private
$searchTag 	.= "<br>&nbsp;<select name='pstatus' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value='-1'>". $eComm['Public'] ." / " . $eComm['Private'] ."</option>\n";
$searchTag 	.= "<option value='1' ".(($pstatus==1)?"selected":"").">". $eComm['Public'] ."</option>\n";
$searchTag 	.= "<option value='0' ".(($pstatus==0 && $pstatus!="")?"selected":"").">". $eComm['Private'] ."</option>\n";
$searchTag 	.= "</select>\n";
*/

#Approval Status
$searchTag 	.= "&nbsp;<select name='astatus' onChange='this.form.submit();'>\n";
$searchTag 	.= "<option value='' ".((strlen($astatus)==0)?"selected":"").">". $i_status_all ."</option>\n";
$searchTag 	.= "<option value='1' ".(($astatus==1)?"selected":"").">". $i_status_approved ."</option>\n";
$searchTag 	.= "<option value='0' ".(($astatus==0 && strlen($astatus)==1)?"selected":"").">". $i_status_waiting ."</option>\n";
$searchTag 	.= "<option value='-1' ".(($astatus==-1)?"selected":"").">". $i_status_rejected ."</option>\n";
$searchTag 	.= "</select>\n";

#Search 
$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td valign=\"bottom\">".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</tr></table>";

### Table SQL ###
$keyword = trim($keyword);
// $order = ($order == 1) ? 0 : 1;
if($filter == "") $filter = 1;
$field = ($field != "") ? $field : 0;
// $pstatus_str = ( $pstatus > -1) ? " and a.PublicStatus=$pstatus " : "";	
/*
concat(
			if(a.PublicStatus=0, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle>'), ''),
			if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle>'), '')
		),
*/
if(strlen($astatus))
	$astatus_str = " and a.Approved=$astatus ";	

$user_field = getNameFieldWithClassNumberByLang("b.");
$FileType_str = ($FileType) ? " and a.FileType='$FileType'" : "";
$Folder_str = ($FolderID=='-1' || $FolderID=='') ? "" : " and a.FolderID='$FolderID'";
$sql = "
	select 
		if(a.onTop=1, '<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle>', ''),
		CONCAT('<a class=\"tablelink\" href=\"javascript:editFile(', a.FileID, ');\">', IF(a.UserTitle!='', a.UserTitle, a.Title), '</a>'),
		c.Title as FolderTitle,
		CASE 
			WHEN a.FileType='P' THEN '". $eComm['Photo'] ."'
			WHEN a.FileType='W' THEN '". $eComm['Website'] ."'
			WHEN a.FileType='V' THEN '". $eComm['Video'] ."'
			WHEN a.FileType='F' THEN '". $eComm['File'] ."'
		ELSE ' '
		END,
		IF (a.UserID IS NOT NULL OR a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer') as UserName,
		a.DateInput,
		case a.Approved
			WHEN '0' THEN '". $i_status_waiting ."'
			 WHEN '1' THEN '". $i_status_approved ."'
			 WHEN '-1' THEN '". $i_status_rejected ."' 
		END,
		count(d.CommentID) as countComment,
		CONCAT('<input type=\"checkbox\" name=\"FileID[]\" value=\"', a.FileID ,'\">')
	from 
		INTRANET_FILE as a
	    LEFT OUTER JOIN INTRANET_FILE_FOLDER c using (FolderID)
	    LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.UserID
	    LEFT OUTER JOIN INTRANET_FILE_COMMENT as d on a.FileID = d.FileID
	where 
		a.GroupID=$GroupID
		AND (a.Title like '%$keyword%' OR a.UserTitle like '%$keyword%')
		$FileType_str
		$astatus_str
		$Folder_str
	group by a.FileID
";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.UserTitle","a.FileType", "FolderTitle", "UserName", "a.DateInput");
$li->sql = $sql;
$li->no_col = 10;
$li->IsColOff = 2;
$li->title = $eComm['File'];

// TABLE COLUMN		
$li->column_list .= "<td class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='18'>&nbsp</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='250'>".$li->column(0, $eComm['File'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(1, $eComm['Folder'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(2, $eComm['FileType'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(3, $eComm['CreatedBy'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(4, $eComm['CreatedDate'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(6, $eComm['ApprovalStatus'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column(5, $eComm['Comment'].'#')."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("FileID[]")."</td>\n";
    

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lgroup->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="javascript">
function checkRemove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}

function editFile(fid)
{
	with(document.form1)
	{
		thisFileID.value = fid;
		action = "settings_file_edit.php";
		method = "get";
		submit();		
	}	
}
</script>

<br />

<form name="form1" method="get" action="settings_file.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="thisFileID" value="">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>

				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
												</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$approveByn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$editBtn?></td>
                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                    <td nowrap><?=$delBtn?></td>
                                    
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="page_FileType" value="<?=$FileType?>">
<input type="hidden" name="page_FolderID" value="<?=$FolderID?>">
<input type="hidden" name="page_pstatus" value="<?=$pstatus?>">
<input type="hidden" name="page_keyword" value="<?=$keyword?>">

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
