<?php

############ Change Log [Start] ##############
#
#	Date	:	2010-05-11	[YatWoon]
#				for rename filename, using "strrpos" instead of "strpos" due to some filename with .
#
############ Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$li = new libuser($UserID);
$lo = new libfilesystem();
$legroup = new libegroup($GroupID);

$file_upload_count = 10;

$FileID = array();

if($li->IsGroup($GroupID))
{
	$lg = new libgroup($GroupID);
	
	for($i=1;$i<=$file_upload_count;$i++)
	{		
		$FileLocation = $_FILES['myfile'.$i]['tmp_name'];
		if(!$FileLocation)	continue;
		
		$FileName = ${"myfile_name".$i};
		## check file format
		if($lg->AllowedVideoTypes != "ALL")
		{
			$allowedAry = explode("/",strtolower($lg->AllowedVideoTypes));
			$ext = get_file_ext($FileName);
			$ext = strtolower(substr($ext, 1, strlen($ext)-1));
			if(!in_array($ext, $allowedAry))	
			{
				$error_msg = "file_type_not_allowed";	
				continue;
			}
		}
		
		$FileSize = $_FILES['myfile'.$i]['size']/1000;
		$FLocation = session_id()."-".(time()+$i).strtolower(substr($FileName, strrpos($FileName,".")));
		
		$used = $lg->returnGroupUsedStorageQuota();
		$available = ($lg->StorageQuota)*1000 - $used;
		
		if ($available >= $FileSize)
		{
			$Folder = $intranet_root."/file/group/g".$GroupID;
			$lo->folder_new($Folder);
			$lo->lfs_copy($FileLocation, $Folder."/".$FLocation);
			$approved = $legroup->needApproval?"0":"1";
			
			$sql = "
				INSERT INTO INTRANET_FILE 
				(GroupID, UserID, UserName, UserEmail, Title, UserTitle, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, Approved) 
				VALUES 
				(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$FileName','$FileName', '$FLocation', ';$UserID;', now(), now(), '$FileSize', '$FolderID', 'V', '$approved')
			";
			
			$li->db_db_query($sql) or die(mysql_error());
			array_push($FileID, $li->db_insert_id());
		}
		else
		{
			$error_msg = "no_space";	
		}
		
	}
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="video_new_step2.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=serialize($FileID);?>">
<input type="hidden" name="error_msg" value="<?=$error_msg;?>">
</form>
</body>