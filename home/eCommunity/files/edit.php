<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libfile.php");
include("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$FileID = IntegerSafe($FileID);

$li = new libfile($FileID);
$toolbar .= "<input type=image src=$image_save>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"history.back(); this.form.method='get';this.form.action='index.php';return false;\">";

include("../../../templates/fileheader.php");
?>

<form action="edit_update.php" method="post">
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width=101 class=popup_top><?=$i_grouphead_sharedfiles?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $button_edit." ".$i_frontpage_schoolinfo_groupinfo_group_files; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
        <tr>
          <td align=center>
<table width=92% border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_FileGroup; ?>:</td><td><?php echo $li->GroupTitle(); ?></td></tr>
<tr><td align=right><?php echo $i_FileTitle; ?>:</td><td><?php echo $li->Title; ?></td></tr>
<tr><td align=right><?php echo $i_FileDescription; ?>:</td><td><textarea name=Description cols=50 rows=5 wrap=virtual><?php echo $li->Description; ?></textarea></td></tr>
<tr><td><br></td><td><?=$toolbar?></td></tr>
</table>
</td>
</tr>
</table>
<table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>

</td></tr>
</table>
<input type=hidden name=GroupID value="<?php echo $li->GroupID; ?>">
<input type=hidden name=FileID value="<?php echo $li->FileID; ?>">
</form>

<?php
include("../../../templates/filefooter.php");
intranet_closedb();
?>