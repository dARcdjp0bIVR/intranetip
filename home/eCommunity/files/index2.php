<?php

#######################################
##	Modification Log:
#	Date:	2017-07-26 Carlos
#			Cater https resource url.
#
#	Date:	2015-09-07 Omas
#			Fix: Cannot show empty folder
#
#	Date:	2012-02-09 YatWoon
#			Improved: Keep track the last accessed group id
#
#	Date:	2011-05-03	YatWoon
#			add alt / title for the icon
#
##	2010-03-05 Max (201003051446)
##	- get the GroupID from previous page
#######################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libdbtable.php');
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");

$GroupID = IntegerSafe($GroupID);

$linterface 	= new interface_html();
$legroup 		= new libegroup($GroupID);
$lfolder 		= new libfolder();

$lo = new libuser($UserID);
//$lu = new libgroup($GroupID);

//if (!$lu->isAccessFiles())
//{
//    header("Location: ../index.php");
//    intranet_closedb();
//    exit();
//}

$lu2007 	= new libuser2007($UserID);
if($lu2007->isInGroup($GroupID) && ($FileType!='W' || $legroup->isAccessLinks()) && ($FileType!='F' || $legroup->isAccessFiles()) && ($FileType!='P' || $legroup->isAccessPhoto()) && ($FileType!='V' || $legroup->isAccessVideo()))
{
	$CurrentPage	= "MyGroups";
	
	if($_SESSION['UserType']!=USERTYPE_ALUMNI)
	$MyGroupSelection= $legroup->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}
else
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
//	$CurrentPage	= "OtherGroups";
//	$MyGroupSelection= $lu->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}

### keep track the last accessed Group ID
$legroup->UpdateLastAccess($GroupID);

/*
if ($lu->hasAdminFiles($UserID))
    $adminDelete = "CONCAT('<a href=javascript:del(', FileID, ')><img src=../../../images/eraser_icon.gif alt=$button_remove hspace=10 vspace=2 border=0 align=absmiddle></a>')";
else
    $adminDelete = "''";
*/

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=4;
switch ($field)
{
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        default: $field = 4; break;
}
$order = ($order == 1) ? 1 : 0;

$li = new libdbtable2007($field, $order, $pageNo);

$TypeImage = $lfolder->TypeImg($FileType);
$TypeName = $lfolder->TypeName($FileType);
$FolderTitle = $lfolder->FolderTitle($FileType);

switch($FileType)
{
	case "P":
	$ApprovedStr = " and b.approved = 1 ";
		$FolderType_str = " and a.FolderType='P' ";
		$li->field_array = array("a.onTop");
		break;
	case "V":
		$ApprovedStr = " and b.approved = 1 ";
		$FolderType_str = " and a.FolderType='V' ";
		$li->field_array = array("a.onTop");
		break;
	case "W":
		$ApprovedStr = " and b.approved = 1 ";
		$FolderType_str = " and a.FolderType='W' ";
		$li->field_array = array("a.onTop");
		break;
	case "F":
		$ApprovedStr = " and b.approved = 1 ";
		$FolderType_str = " and (a.FolderType='F' or a.FolderType='' or a.FolderType is null) ";
		$li->field_array = array("a.onTop");
		break;
	case "WA":
		$ApprovedStr = " and b.approved <> 1 ";
		$li->field_array = array("a.onTop");
		break;				
	default:
		$ApprovedStr = " and b.approved = 1 ";
		$FolderType_str = "";
		$li->field_array = array("a.DateModified");
		break;
}
$excludedType = array();
if(!$legroup->isAccessPhoto())
	$excludedType[] = "'P'";
if(!$legroup->isAccessVideo())
	$excludedType[] = "'V'";
if(!$legroup->isAccessLinks())
	$excludedType[] = "'W'";
if(!$legroup->isAccessFiles())
	$excludedType[] = "'F'";
if(count($excludedType)>0)
	$FileType_str .= " AND b.FileType NOT IN (".implode(",",$excludedType).") ";

$li->fieldorder2 = ", a.Title ";
$TypeNameStr = $TypeImage . $TypeName;

//if($legroup->needApproval)	$Approved_str = " and b.Approved=1";
                               
# TABLE INFO
switch($DisplayType)
{
	case "4":		## thumbnail
		if ($legroup->hasAdminFiles($UserID))
		{
			$adminFileTitle = "
				CONCAT(
					'<input type=\"checkbox\" name=\"FolderID[]\" value=\"', a.FolderID ,'\">',
					'<a href=javascript:viewFolder(', a.FolderID, ') class=tablelink><strong>', a.Title, '</strong></a>', 
					CASE 
			         WHEN a.PublicStatus='0' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title=\"". $eComm['Private'] ."\" alt=\"". $eComm['Private'] ."\">')
			          ELSE ' '
			        END
				),
			";
		}
		else
		{
			$adminFileTitle = "
				CONCAT(
					'<a href=javascript:viewFolder(', a.FolderID, ') class=tablelink><strong>', a.Title, '</strong></a>', 
					CASE 
			         WHEN a.PublicStatus='0' THEN concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title=\"". $eComm['Private'] ."\" alt=\"". $eComm['Private'] ."\">')
			          ELSE ' '
			        END
				),
			";
		}
				
		$sql = "
			SELECT
				a.FolderID,
				$adminFileTitle
			    CONCAT('Created by: <strong>', a.UserName, '</strong>'),
			    CONCAT('<a href=javascript:viewFolder(', a.FolderID, ') class=tablelink>', '".$TypeImage." ', count(b.FileID), ' Files</a>')
			    
		";
		
		$li->IsColOff = "eCommShareList_4";
		//$li->field_array = array("a.onTop","a.Title");
		$li->no_col = 4;
		break;
		
	default:		### list
		$sql = "
			SELECT
				concat(
					if(a.PublicStatus=0, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title=\"". $eComm['Private'] ."\" alt=\"". $eComm['Private'] ."\">'), '&nbsp;'),
					if(a.onTop=1, concat('<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_ontop.gif border=0 align=absmiddle title=\"". $eComm['Top'] ."\" alt=\"". $eComm['Top'] ."\">'), '&nbsp;')
				),
				CONCAT('<a href=javascript:viewFolder(', a.FolderID, ') class=tablelink>', a.Title, '</a>'),
			    a.UserName,
			    count(b.FileID), 
			    CONCAT('<input type=\"checkbox\" name=\"FolderID[]\" value=\"', a.FolderID ,'\">'),
			    a.FolderID
		";
		
		$li->IsColOff = "eCommShareList_List";
		
		// TABLE COLUMN
		$li->column_list .= "<td width='38' class='albumtabletoptext' nowrap>&nbsp;</td>\n";
		$li->column_list .= "<td align='left' class='albumtabletoptext' nowrap>". $FolderTitle ."</td>\n";
		$li->column_list .= "<td align='left' class='albumtabletoptext' nowrap>". $eComm['By'] ."</td>\n";
		$li->column_list .= "<td width='35' align='left' class='albumtabletoptext' nowrap>". $eComm[$FileType] ."</td>\n";
		//$li->column_list .= "<td width='90' align='left' class='albumtabletop albumtabletoptext' nowrap>". $eComm['LastUpload'] ."</td>\n";
		if($legroup->hasAdminFiles($UserID))
		{
			$li->column_list .= "<td width=1 class='albumtabletoptext'>".$li->check("FolderID[]")."</td>\n";
			$li->no_col = 5;
		}
		else
			$li->no_col = 4;
		break;		
}		

$sql .= "
	FROM 
		INTRANET_FILE_FOLDER a 
		left outer join INTRANET_FILE b on (a.FolderID = b.FolderID $ApprovedStr $FileType_str)
	WHERE 
		a.GroupID = '$GroupID' 
		AND (a.Title like '%$keyword%')
		$FolderType_str
		/*$FileType_str*/
		$Approved_str
		/*$ApprovedStr*/
	group by 
		a.FolderID		
";

$li->sql = $sql;

### Add link
if($legroup->hasAdminFiles($UserID))
{
	$AddBtn 	= "<a href=\"javascript:newFolder()\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $eComm['Add_'].$FolderTitle . "</a>";
	$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'FolderID[]','folder_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
	$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'FolderID[]','folder_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
}

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu();
### Start Right Menu ###

### Start Display mode icons ###
$DisplayIcons = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";

$DisplayIcons .= "<td>";
if(!$DisplayType)
	$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\" >";
else
	$DisplayIcons .= "<a href=\"index2.php?GroupID={$GroupID}&FileType={$FileType}\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list.gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\" onMouseOver=\"MM_swapImage('view_list','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>";	
$DisplayIcons .= "</td>";

$DisplayIcons .= "<td>";
if($DisplayType==4)
	$DisplayIcons .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\">";
else
	$DisplayIcons .= "<a href=\"index2.php?GroupID={$GroupID}&FileType={$FileType}&DisplayType=4\" onMouseOver=\"MM_swapImage('view_4','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4.gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\"></a>";
$DisplayIcons .= "</td>";

$DisplayIcons .= "</tr></table>";
### End Display mode icons ###     


### Title ###

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$legroup->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$legroup->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();
$legroup->db_db_query("debug: ");
?>
<style>.GroupTitleLink:hover{color:#f00; cursor:pointer;}</style>
<script type="text/javascript" src="<?=$is_https?"https":"http"?>://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$is_https?"https":"http"?>://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">
function newFolder()
{
         with(document.form1)
         {
                action = "folder_new.php";
                submit();
         }
}

function newFile(t)
{
         with(document.form1)
         {
                action = t+"_new.php";
                submit();
         }
}

function viewFolder(fid)
{
         with(document.form1)
         {
                FolderID.value = fid;
	         	action = "index.php";
                submit();
         }
}

function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteFolder']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 

}
	
</script>


<br>
<form name="form1" method="get" action="">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	            <td align="right"><?=$DisplayIcons?></td>
	              <tr>
	                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                	<td align="center">
	                	<table width="95%" border="0" cellspacing="0" cellpadding="0">
	                	<tr>
	                		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
						</tr>
						</table>
						</td>
	              	</tr>
                            <br/>
	              	<tr>
	                	<td align="center">
	                	<table width="95%" border="0" cellspacing="0" cellpadding="0">
	                	<tr>
	                		<td align="left" nowrap><?=$AddBtn?></td>
                        </tr>
                            <tr></tr>
                            <tr class="table-action-bar">
	                		<td align="right" nowrap> <?=$editBtn?> <?=$delBtn?></td>
						</tr>
						</table>
						</td>
	              	</tr>

	                  <tr>
	                    <td align="center"><?=$li->display('folder');?></td>
	                  </tr>
	                </table>                
	                </td>
	              </tr>
	            </table>              
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	              
	              
	              
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_view_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>
            

<br>


<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>