<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$DisplayType = $_POST['DisplayType'];
$FileType = $_POST['FileType'];
$GroupID = $_POST['GroupID'];
$FolderID = $_POST['FolderID'];
$Title = $_POST['Title'];
$PublicStatus = $_POST['PublicStatus'];
$onTop = $_POST['onTop'];
$onTop = $onTop ? 1 : 0;
$PhotoResize = $_POST['PhotoResize'];
$DemensionX = $PhotoResize == 1? $_POST['DemensionX'] : "NULL";
$DemensionY = $PhotoResize == 1? $_POST['DemensionY'] : "NULL";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);
$PublicStatus = IntegerSafe($PublicStatus);

$lo = new libuser($UserID);
$lu = new libgroup($GroupID);
if (!$lu->hasAdminFiles($UserID))
{
    intranet_closedb();
    exit();
}

$sql = "update INTRANET_FILE_FOLDER set Title='$Title', PublicStatus='$PublicStatus', onTop='$onTop', PhotoResize='$PhotoResize', DemensionX='$DemensionX', DemensionY='$DemensionY' where FolderID='$FolderID'";
$lu->db_db_query($sql);

$xmsg = ($lu->db_affected_rows()<=0) ? "update_failed" : "update";

intranet_closedb();
header("Location: index2.php?xmsg=$xmsg&GroupID=$GroupID&FileType=$FileType&DisplayType=$DisplayType");

?>