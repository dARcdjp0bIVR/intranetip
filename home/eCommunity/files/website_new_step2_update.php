<?php
// Using:

###################################
#
#	Date:	2019-09-04	Bill    [2019-0815-1424-47235]
#	        fixed url files handling
#
###################################

$fileIDStr = $_POST['FileID']? $_POST['FileID'] : $_GET['FileID'];

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$lo = new libuser($UserID);
$lu = new libgroup($GroupID);

$DisplayType = $_POST['DisplayType'];
$FileType = $_POST['FileType'];
$GroupID = $_POST['GroupID'];
$FolderID = $_POST['FolderID'];

// $FileID = unserialize($_POST['FileID']);
$FileID = unserialize($fileIDStr);
$FileID_fin = array();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);
$FileID = IntegerSafe($FileID);

for($i=0;$i<sizeof($FileID);$i++)
{
	$this_FileID = $FileID[$i];
	$this_usertitle = $_POST['usertitle'.$FileID[$i]];
	$this_description = $_POST['description'.$FileID[$i]];
	$this_FolderID = $_POST['FolderID'.$FileID[$i]];
// 	$this_remove = $_POST['remove'.$FileID[$i]];
	$this_private = $_POST['private'.$FileID[$i]];
	$PublicStatus = $this_private? 0 : 1;
	
// 	if($this_remove==1)
// 		$sql = "delete from INTRANET_FILE where FileID=$this_FileID";
// 	else
// 	{
		$sql = "UPDATE INTRANET_FILE SET UserTitle='$this_usertitle', Description='$this_description', FolderID='$this_FolderID', PublicStatus='$PublicStatus', DateModified=now() WHERE FileID = '$this_FileID'";
		array_push($FileID_fin, $this_FileID);
// 	}
		
	$lu->db_db_query($sql) or die(mysql_error());
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="website_new_fin.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=serialize($FileID_fin);?>">
</form>
</body>