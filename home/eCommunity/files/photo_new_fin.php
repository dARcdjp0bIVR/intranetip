<?php
##	Modifying By:

#######################################
##	Modification Log:
#
#	Date:	2019-09-04	Bill    [2019-0815-1424-47235]
#	        fixed cannot display photos
#
#	Date:	2017-07-26	Carlos
#			Cater https resource url.
#
#	Date:	2011-05-03	YatWoon
#			add alt / title for the icon
#
#	Date:	2010-03-05 Max (201003051446)
#	        get the GroupID from previous page
#
#######################################

$fileIDStr = $_POST['FileID']? $_POST['FileID'] : $_GET['FileID'];

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

// $FileID = unserialize($FileID);
$FileID = unserialize($fileIDStr);
$FileID = IntegerSafe($FileID);

$linterface = new interface_html();
$legroup 	= new libegroup($GroupID);
$lo 		= new libuser($UserID);
$lfolder 	= new libfolder($FolderID);

$CurrentPage = "MyGroups";
$currentMode = "add";

$lu	= new libgroup($GroupID);
$lu2007 = new libuser2007($UserID);

if (!$lu->isAccessPhoto() or !$lu2007->isInGroup($GroupID))
{
    header("Location: ../index.php");
    intranet_closedb();
    exit();
}

$FileType = cleanCrossSiteScriptingCode($FileType);
$DisplayType = cleanCrossSiteScriptingCode($DisplayType);

### Title ###
if($_SESSION['UserType'] != USERTYPE_ALUMNI) {
    $MyGroupSelection= $lu->getSelectPhotoGroups("name='GroupID' onChange=\"window.location='?FileType=$FileType&DisplayType=$DisplayType&GroupID='+this.value\"",$GroupID);
}
$FolderListSelection = $lfolder->getFolderList($GroupID, "name='FolderID' onChange='changeFolder(this.selectedIndex);'", $FolderID, $FileType);

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";

$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{ 
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'view');\" class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['View']."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
            </table></td>
            <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['Add'] ."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
            </table>
            </td>
          </tr>
        </table></td>
        </tr>
    </table>
      <br>
";
$rightMenu .= $legroup->RightAddMenu();
### Start Right Menu ###

$TypeNameStr = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_photo_add.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" border=\"0\"> ". $eComm['Add_'] . $eComm['Photo'];

### file list
for($i=0;$i<sizeof($FileID);$i++) 
{ 
	$file_list .="
		<div id=\"photo_thumb_list\"><div id=\"photo_border\">
			<a href=\"javascript:viewfile(". $FileID[$i] .");\">". $legroup->DisplayPhoto($FileID[$i], 75) ."</a>
		</div>
		</div>
	";
}

if(!$file_list)	$error_table = "<br>".$linterface->GET_WARNING_TABLE("no_file");

$STEPS_OBJ[] = array($eComm['select_photo'], 0);
$STEPS_OBJ[] = array($eComm['input_detail_info'], 0);
$STEPS_OBJ[] = array($eComm['add_photo_finished'], 1);
?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif";
}

function newFile(t)
{
    with(document.form1a)
    {
        action = t+"_new.php";
        submit();
    }
}

function viewfile(fileid)
{
	 with(document.form1a)
	 {
		FileID.value = fileid;
		submit();
	 }

}
</script>

<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top">
	            <br />

				<table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td align="center">
                <?= $linterface->GET_STEPS($STEPS_OBJ) ?>
                <?=$error_table?>
                </td>
              </tr>
              
              <? if($file_list) {?>
              <tr>
                <td height="10" align="center"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
              </tr>
              
              <!------------- Start Photos Uploaed --------------//-->
              <tr>
                <td align="center">
                <a name="photo_uploaded_table">
                <table width="90%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="16"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_01.gif" width="16" height="16"></td>
                    <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_02a.gif" width="111" height="16"></td>
                    <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_03.gif" width="22" height="16"></td>
                  </tr>
                  <tr>
                    <td width="16" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_04.gif" width="16" height="13"></td>
                    <td bgcolor="#6fb4ba">
					
					  <table width="100%" border="0" cellspacing="0" cellpadding="3">
				  <tr>
					<td align="center" bgcolor="#e3f7f4">
					<center>
					
					<?=$file_list?>
					
					</center>
					</td>
				  </tr>
				</table>
					</td>
                    <td width="22" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_06a.gif" width="22" height="110"></td>
                  </tr>
                  <tr>
                    <td width="16"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_07.gif" width="16" height="9"></td>
                    <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_08.gif" width="16" height="9"></td>
                    <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/album/album_1_09.gif" width="22" height="9"></td>
                  </tr>
                </table>
                </a>
                  <br></td>
              </tr>
              <!------------- End Photos Uploaed --------------//-->
              <? } ?>
             </table>
	               
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	            
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_add_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        

<form name="form1a" action="file_view.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType?>">
<input type="hidden" name="FileType" value="<?=$FileType?>">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="FileID" value="<?=$FileID?>">
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
<input type="hidden" name="currentMode" value="add">
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>