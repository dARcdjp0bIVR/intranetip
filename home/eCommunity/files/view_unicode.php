<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libfile.php");
intranet_auth();
intranet_opendb();

$FileID = IntegerSafe($FileID);

$li = new libfile($FileID);
$li->UpdateReadFlag();

intranet_closedb();

/*###
  ### Test: Construct all header detail and use readfile() to output
  ### Assume output filename retrieve from the DB is UTF-8, as stimulate by iconv()
  ### Result: urlencode() must be used to convert the filename
  
$filename = "../../../file/group/g".$li->GroupID."/".$li->Location;

$size = filesize($filename);

$newFilename = urlencode(iconv("Big5", "UTF-8", $li->Title));

#echo $newFilename;

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");

header("Content-Length: $size");
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$newFilename);

@readfile($filename);

##########################*/

/*##########################

$newFilename = urlencode(iconv("Big5", "UTF-8", $li->Title));
#echo $newFilename;

##########################*/

$newFilename = $li->Location;
#$newFilename = urlencode(iconv("Big5", "UTF-8", $li->Title));
#echo $newFilename;

header("Location: ../../../file/group/g".$li->GroupID."/".$newFilename);
?>
