<?php

# being modified by: Thomas


/********************** Change Log ***********************/
#
#	Date    :   2010-07-06 [Thomas]
#   Details :   Use AJAX to load all comment
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lo 			= new libuser($UserID);
$Groups 		= $lo->returnGroups();
$GroupID 		= ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];
$lu 			= new libgroup($GroupID);
$legroup 		= new libegroup($GroupID);

$FileID      = $_POST["File_attr"][0];
$Comment      = $_POST["File_attr"][1];

$sql = "
	INSERT INTO INTRANET_FILE_COMMENT 
	(FileID, UserID, UserName, Comment, DateInput) 
	VALUES
	('$FileID', '$UserID','".addslashes($lo->NickNameClassNumber())."','$Comment', now())
";
$lu->db_db_query($sql);

echo $legroup->DisplayComments($FileID);
intranet_closedb();
?>