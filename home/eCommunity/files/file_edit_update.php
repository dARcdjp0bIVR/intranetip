<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");

intranet_auth();
intranet_opendb();

$this_FileID = $_POST['FileID'];
$this_usertitle = $_POST['usertitle'];
$this_description = $_POST['description'];
$this_FolderID = $_POST['FolderID'];
$this_remove = $_POST['remove'];
$this_private = $_POST['private'];
$PublicStatus = $this_private? 0 : 1;
$this_CoverImg = $_POST['CoverImg'];
$CoverImg = $this_CoverImg? 1 : 0;

$GroupID = IntegerSafe($GroupID);
$this_FileID = IntegerSafe($this_FileID);
$this_FolderID = IntegerSafe($this_FolderID);


$lg = new libgroup($GroupID);

$lf = new libfile($this_FileID);
$FileType = $lf->FileType;

//Check the user is owner or admin
if (!($lg->hasAdminFiles($UserID) || $lf->UserID==$UserID))
{
    header("Location: index2.php?GroupID=$GroupID");
    intranet_closedb();
    exit();
}
$DisplayType = cleanCrossSiteScriptingCode($DisplayType);
if($this_remove==1)
{
	$sql = "delete from INTRANET_FILE where FileID='$this_FileID'";
	$backpage = "index.php";
	$DisplayType = "";
}
else
{
	### if set the cover image, clear other cover image within same folder
	if($CoverImg)
	{
			$sql = "update INTRANET_FILE set CoverImg=0 where FolderID='$this_FolderID'";
			$lf->db_db_query($sql) or die(mysql_error());
	}
	
	$sql = "update INTRANET_FILE set UserTitle='$this_usertitle', Description='$this_description', FolderID='$this_FolderID', PublicStatus='$PublicStatus', DateModified=now(), CoverImg='$CoverImg' where FileID='$this_FileID'";
	$backpage = "file_view.php"; 
}
	
$lf->db_db_query($sql) or die(mysql_error());

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="<?=$backpage?>" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=$FileID;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
</form>
</body>