<?php

###############################################
#
#	Date:	2012-09-05	YatWoon
#			hide private function
#
###############################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FileID = IntegerSafe($FileID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lu = new libgroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
    exit();
}

$FileID = (is_array($FileID)? $FileID[0]: ($FileID ? $FileID : $thisFileID));
if (!$FileID)
{
    header("Location: ../group/?GroupID=$GroupID&msg=AccessDenied");
    exit();
}

$lfile = new libfile($FileID);
$lfolder = new libfolder();

$lu2007 = new libuser2007($UserID);
if(!$lu2007->isInGroup($lfile->GroupID))
{
	header("Location: ./?GroupID=$GroupID&msg=AccessDenied");
	exit;
}

### File Thumbnail ###
$FileLocation = $PATH_WRT_ROOT."file/group/g". $lfile->GroupID ."/". $lfile->Location;
$link = "<a href='javascript:view(".$FileID.")'>";
$thumb = $legroup->DisplayPhoto($FileID, '200', '', 1, $link);

$CurrentPage	= "Settings_SharingSettings";
$MyGroupSelection= $legroup->getSelectAdminShared("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eComm['Edit_'].$eComm['File']);

?>

<br />   
<form name="form1" method="post" action="settings_file_edit_update.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FileID" value="<?=$FileID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
                <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
                <tr> 
                	<td valign="top">
                		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						
                		<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['FileName']?></span></td>
							<td class="tabletext"><?=$lfile->Title?></td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['FileType']?></span></td>
							<td class="tabletext"><?=$lfile->returnFileType()?></td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Title']?></span></td>
							<td class="tabletext"><input type='text' name='Title' maxlength='255' value="<?=htmlspecialchars($lfile->UserTitle)?>" class='textboxtext'></td>
						</tr>
						
						<tr>
							<td width="30%" align="left" class="formfieldtitle" valign="top"><span class="tabletext"><?=$eComm['Description']?></span></td>
							<td align="left"><?=$linterface->GET_TEXTAREA("description", $lfile->Description )?></td>
						</tr>
						
						<tr>
							<td width="30%" align="left" class="formfieldtitle"><span class="tabletext"><?=$eComm['move_to_folder']?></span></td>
							<td align="left"><span class="tabletextremark"><?=$lfolder->getFolderList($GroupID, "name='FolderID'", $lfile->FolderID, $lfile->FileType)?></span></td>
						</tr>
						
						<? /* ?>
						<tr valign='top'>
							<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['Status']?></span> </td>
							<td class='tabletext'>
								<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" <?=$lfile->PublicStatus==1?"checked":""?>> <label for="PublicStatus1"><?=$eComm['Public'];?></label> 
								<input type="radio" name="PublicStatus" value="0" id="PublicStatus0" <?=$lfile->PublicStatus==0?"checked":""?>> <label for="PublicStatus0"><?=$eComm['Private'];?></label>
							</td>
						</tr>
						<? */ ?>
						<tr valign='top'>
							<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['ApprovalStatus']?></span> </td>
							<td class='tabletext'>
								<select name="Approved">
									<option value="0" <?=$lfile->Approved==0?"selected":""?>><?=$Lang['eComm']['WaitingForApproval']?></option>
									<option value="1" <?=$lfile->Approved==1?"selected":""?>><?=$i_status_approve?></option>
									<option value="-1" <?=$lfile->Approved==-1?"selected":""?>><?=$i_status_rejected?></option>
								</select>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
							<td class="tabletext">
								<input type="checkbox" name="onTop" value="1" <?=$lfile->onTop?"checked":""?>>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['SetAsCoverImage']?> </span></td>
							<td class="tabletext">
								<input type="checkbox" name="CoverImg" value="1" <?=$lfile->CoverImg?"checked":""?>>
							</td>
						</tr>
						
						</table>
					</td>
					<td valign="top" width="200"><?=$thumb?></td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)") ?>
		</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>                        

<input type="hidden" name="page_FileType" value="<?=$page_FileType?>">
<input type="hidden" name="page_FolderID" value="<?=$page_FolderID?>">
<input type="hidden" name="page_pstatus" value="<?=$page_pstatus?>">
<input type="hidden" name="page_keyword" value="<?=$page_keyword?>">

</form>

<br />
<script>
function view(id){
	window.location= "view.php?FileID="+id
        //newWindow("view.php?FileID="+id,5);
}
</script>
<?php
print $linterface->FOCUS_ON_LOAD("form1.Title");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>