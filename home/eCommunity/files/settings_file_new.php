<?php
# using: 

###############################################
#
#   Date:   2019-10-14  Bill    [DM#3665]
#           change file input to input fields for website
#
#	Date:	2012-09-05	YatWoon
#			hide private function
#
#	Date:	2011-12-07	YatWoon
#			Improved: display "onTop" folder first [Case#2011-0830-1331-46071]
#
###############################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lu = new libgroup($GroupID);
$li = new libdb();

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
    header("Location: ../group/?GroupID=$GroupID");
    exit();
}

$FileType = $FileType ? $FileType : "P";

$CurrentPage = "Settings_SharingSettings";
$MyGroupSelection = $legroup->getSelectAdminShared("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$TAGS_OBJ[] = array($eComm['Folder'],"settings_folder.php?GroupID=$GroupID", 0);
$TAGS_OBJ[] = array($eComm['File'],"settings_file.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);

$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->TitleDisplay."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title, "", 0);

$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eComm['New_'].$eComm['File']);
?>

<script language="javascript">
<!--
var folder = new Array();
<?
	$f_ary = array("P","V","W","F");
	for($i=0;$i<sizeof($f_ary);$i++)
	{
?>
		folder['<?=$f_ary[$i]?>'] = new Array();
		<?
		$sql = "SELECT FolderID, Title FROM INTRANET_FILE_FOLDER where GroupID='$GroupID' and FolderType='". $f_ary[$i] ."' order by onTop desc, Title";
		$temp = $li->returnArray($sql,1);
		
		for($j=0;$j<sizeof($temp);$j++)
		{
        ?>
			folder['<?=$f_ary[$i]?>'][<?=$j?>] = "<option value='<?=$temp[$j]['FolderID']?>'><?=$temp[$j]['Title']?></option>";
		<?
		}
	}
?>

function redirect(x)
{
	var temp = document.form1.FolderID;
	
	//for (m=temp.options.length-1;m>0;m--)
	/*for (i=0;i<=temp.options.length;i++)
	{
		if(temp.options[i])
			temp.options[i] = null;
	}*/
	$(temp).contents().remove();

	for (i=0;i<folder[x].length;i++)
	{
		//temp.options[i] = new Option(folder[x][i].text,folder[x][i].value);
		$(temp).append(folder[x][i]);
	}

	if(temp.options.length>0) {
        temp.options[0].selected = true;
    }

    if(x == 'W') {
        $('tr#file_row').hide();
	    $('tr#url_row').show();
    } else {
        $('tr#file_row').show();
        $('tr#url_row').hide();
    }
}

function checkForm(obj)
{
	if(!obj.FolderID.value)
	{
		alert("<?=$eComm['NoFolderSelected']?>");
		return false;	
	}
	
	if(!obj.Title.value)
	{
		alert("<?=$Lang['eComm']['PleaseInsertTitle']?>");
		return false;	
	}

	if(obj.FileType.value == 'W')
    {
        if(!obj.url.value)
        {
            alert("<?=$eComm['js_please_input_url']?>");
            return false;
        }
    }
    else
    {
        if(!Trim(obj.myfile.value))
        {
            alert("<?=$eComm['js_please_select_file']?>");
            return false;
        }
        else
        {
            var tname = obj.myfile.value;
            v = tname.substr(tname.lastIndexOf("\\")+1);
            document.getElementById('myfile_name').value = v;
        }
    }

	return true;
}
//-->
</script>

<br />   
<form name="form1" method="post" action="settings_file_new_update.php" onSubmit="return checkForm(this);" enctype="multipart/form-data">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
            <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
                <tr> 
                	<td valign="top">
                		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                	
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['FileType']?></span></td>
							<td>
								<?
								$searchTag 	= "<select name='FileType' onChange='redirect(this.value);'>\n";
								$searchTag 	.= "<option value='P' ".(($FileType=='P')?"selected":"").">". $eComm['Photo'] ."</option>\n";
								$searchTag 	.= "<option value='V' ".(($FileType=='V')?"selected":"").">". $eComm['Video'] ."</option>\n";
								$searchTag 	.= "<option value='W' ".(($FileType=='W')?"selected":"").">". $eComm['Website'] ."</option>\n";
								$searchTag 	.= "<option value='F' ".(($FileType=='F')?"selected":"").">". $eComm['File'] ."</option>\n";
								$searchTag 	.= "</select>\n";
								?>
								<?=$searchTag?>
							</td>
						</tr>
						
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Folder']?></span> <span class='tabletextrequire'>*</span></td>
							<td>
								<select name="FolderID">
								<option value="">---</option>
								</select>
							</td>
						</tr>

						<tr valign="top" id="file_row">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['File']?> <span class='tabletextrequire'>*</span></span></td>
							<td class="tabletext">
								<input type="file" class="file" name="myfile" id="myfile">
								<input type="hidden" name="myfile_name" id="myfile_name">
							</td>
						</tr>

                        <tr valign="top" id="url_row" display="none">
                            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['URL']?> <span class='tabletextrequire'>*</span></span></td>
                            <td class="tabletext">
                                <input type="text" id="url" name="url" class="textboxtext">
                            </td>
                        </tr>
						
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Title']?><span class='tabletextrequire'>*</span></span></td>
							<td class="tabletext"><input type='text' name='Title' maxlength='255' value='' class='textboxtext'></td>
						</tr>

						<tr>
							<td width="30%" align="left" class="formfieldtitle" valign="top"><span class="tabletext"><?=$eComm['Description']?></span></td>
							<td align="left"><?=$linterface->GET_TEXTAREA("description", '' )?></td>
						</tr>
						<? /* ?>
						<tr valign='top'>
							<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['Status']?></span> </td>
							<td class='tabletext'>
								<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" checked> <label for="PublicStatus1"><?=$eComm['Public'];?></label> 
								<input type="radio" name="PublicStatus" value="0" id="PublicStatus0" > <label for="PublicStatus0"><?=$eComm['Private'];?></label>
							</td>
						</tr>
						<? */ ?>
						<tr valign="top">
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
							<td class="tabletext">
								<input type="checkbox" name="onTop" value="1" >
							</td>
						</tr>
						</table>
					</td>
                </tr>
            </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
        	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
        <tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)") ?>
			</td>
		</tr>
	    </table>                                
	</td>
</tr>
</table>                        

<input type="hidden" name="page_FileType" value="<?=$page_FileType?>">
<input type="hidden" name="page_FolderID" value="<?=$page_FolderID?>">
<input type="hidden" name="page_pstatus" value="<?=$page_pstatus?>">
<input type="hidden" name="page_keyword" value="<?=$page_keyword?>">

</form>

<br />

<script language="javascript">
<!--
redirect('<?=$FileType?>');
//-->
</script>

<?php
print $linterface->FOCUS_ON_LOAD("form1.FileType");
$linterface->LAYOUT_STOP();

intranet_closedb();
?>