<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$lfolder = new libfolder($FolderID);

$MODULE_OBJ['title'] = $eComm['Add_'].$lfolder->FolderTitle($FileType);
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();

$FolderTitle = $lfolder->FolderTitle($FileType);
?>
 
<script>
function checkForm(obj)
{
	if(!check_text(obj.Title, "<?=$i_alert_pleasefillin.$FolderTitle?>")) return false;
	if(obj.PhotoResize[1].checked)
	{
		if(!check_positive_nonzero_int(obj.DemensionX, "<?=$Lang['eComm']['Width'].$Lang['eComm']['DemensionErr']?>")) return false;
		if(!check_positive_nonzero_int(obj.DemensionY, "<?=$Lang['eComm']['Height'].$Lang['eComm']['DemensionErr']?>")) return false;
	}
}

$(document).ready(function(){
	$('#PhotoResize0').click(function(){
		$('#ResizeDemension').attr('style', 'visibility: hidden');
	})
	$('#PhotoResize1').click(function(){
		$('#ResizeDemension').attr('style', '');
	})
});
</script>
 
<form name="form1" method="post" action="folder_new_update.php" onSubmit="return checkForm(this);">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
  
<tr>
	<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
					<td><br />
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr valign='top'>
							<td width='25%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$FolderTitle?></span> <span class='tabletextrequire'>*</span></td>
							<td class='tabletext'><input type='text' name='Title' maxlength='255' value='' class='textboxtext'></td>
						</tr>
						
						<tr valign='top'>
							<td width='25%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['Status']?></span> </td>
							<td class='tabletext'>
								<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" checked> <label for="PublicStatus1"><?=$eComm['Public'];?></label> 
								<input type="radio" name="PublicStatus" value="0" id="PublicStatus0"> <label for="PublicStatus0"><?=$eComm['Private'];?></label>
							</td>
						</tr>
						
						<tr valign="top">
							<td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
							<td class="tabletext">
								<input type="checkbox" name="onTop" value="1">
							</td>
						</tr>
						
						<tr valign="top">
							<td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eComm']['PhotoSize']?> </span></td>
							<td class="tabletext">
								<input type="radio" name="PhotoResize" value="0" id="PhotoResize0" checked> <label for="PhotoResize0"><?=$Lang['eComm']['Origanal']?></label><br>
								<input type="radio" name="PhotoResize" value="1" id="PhotoResize1"> <label for="PhotoResize1"><?=$Lang['eComm']['Resize']?></label>&nbsp;
								<span id="ResizeDemension" style="visibility: hidden">(<input type="text" name="DemensionX" maxlength="5" style="width:40px">&nbsp;x&nbsp;<input type="text" name="DemensionY" maxlength="5" style="width:40px">px - <?=$Lang['eComm']['Width']?> x <?=$Lang['eComm']['Height']?>)</span>
							</td>
						</tr>
						
						</table>
					</td>
				</tr>
				</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:window.close();","closeBtn") ?>
			</td>
		</tr>
				</table>                                
	</td>
</tr>

</table>     
</form>         

 
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>