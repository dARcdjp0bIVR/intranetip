<?php
##	Modifying By:
#######################################
##	Modification Log:]
#	2017-07-26 Carlos
#	- Cater https resource url.
#
#	Date:	2011-05-03	YatWoon
#			add alt / title for the icon
#
##	2010-08-04 Thomas
##  - Add Photo Resize option
##
##	2010-03-05 Max (201003051446)
##	- get the GroupID from previous page
#######################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup($GroupID);

$FolderID = $FolderID[0];

$lo = new libuser($UserID);
$Groups = $lo->returnGroups();
$GroupID = ($lo->IsGroup($GroupID)) ? $GroupID : $Groups[0][0];
$lu = new libgroup($GroupID); 
$lfolder = new libfolder($FolderID);
$lu2007 	= new libuser2007($UserID);
if (!$lu->hasAdminFiles($UserID) or !$lu2007->isInGroup($GroupID))
{
    header("Location: ../index.php");
    intranet_closedb();
    exit();
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=4;
switch ($field)
{
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        default: $field = 4; break;
}
$order = ($order == 1) ? 1 : 0;

$TypeImage = $lfolder->TypeImg($FileType);
$TypeName = $lfolder->TypeName($FileType);
$FolderTitle = $lfolder->FolderTitle($FileType);
$TypeNameStr = $TypeImage . $TypeName;

                              
### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";
$rightMenu .= $legroup->RightViewMenu();
### Start Right Menu ###

### Title ###
$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?FileType=$FileType&DisplayType=$DisplayType&GroupID='+this.value\"",$GroupID);

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eComm['Edit'.$FolderTitle]);
?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">
function checkForm(obj)
{
	if(!check_text(obj.Title, "<?=$i_alert_pleasefillin.$FolderTitle?>")) return false;
	if(obj.PhotoResize[1].checked)
	{
		if(!check_positive_nonzero_int(obj.DemensionX, "<?=$Lang['eComm']['Width'].$Lang['eComm']['DemensionErr']?>")) return false;
		if(!check_positive_nonzero_int(obj.DemensionY, "<?=$Lang['eComm']['Height'].$Lang['eComm']['DemensionErr']?>")) return false;
	}
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 

}

function newFile(t)
{
         with(document.form1)
         {
                action = t+"_new.php";
                submit();
         }
}

$(document).ready(function(){
	$('#PhotoResize0').click(function(){
		$('#ResizeDemension').attr('style', 'visibility: hidden');
	})
	$('#PhotoResize1').click(function(){
		$('#ResizeDemension').attr('style', '');
	})
});

</script>


<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top">
	            <br />
	            <form name="form1" method="post" action="folder_edit_update.php" onSubmit="return checkForm(this);">
	            <input type="hidden" name="DisplayType" value="<?=$DisplayType?>">
				<input type="hidden" name="FileType" value="<?=$FileType?>">
				<input type="hidden" name="GroupID" value="<?=$GroupID?>">
				<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
				
				<br />
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	            <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	            </tr>
	              
				<tr>
					<td colspan="2">
				                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				                <tr> 
				                	<td><br />
										<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr valign='top'>
											<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$FolderTitle?></span> <span class='tabletextrequire'>*</span></td>
											<td class='tabletext'><input type='text' name='Title' maxlength='255' value='<?=$lfolder->Title?>' class='textboxtext'></td>
										</tr>
										
										<tr valign='top'>
											<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eComm['Status']?></span> </td>
											<td class='tabletext'>
												<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" <?=$lfolder->PublicStatus ? "checked" : "";?>> <label for="PublicStatus1"><?=$eComm['Public'];?></label> 
												<input type="radio" name="PublicStatus" value="0" id="PublicStatus0" <?=$lfolder->PublicStatus ? "" : "checked";?>> <label for="PublicStatus0"><?=$eComm['Private'];?></label>
											</td>
										</tr>
										
										<tr valign="top">
											<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
											<td class="tabletext">
												<input type="checkbox" name="onTop" value="1" <?=$lfolder->onTop?"checked":""?>>
											</td>
										</tr>
										
										<tr valign="top" style="<?=$FileType=='P'? "" : "display:none"?>">
											<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eComm']['PhotoSize']?> </span></td>
											<td class="tabletext">
												<input type="radio" name="PhotoResize" value="0" id="PhotoResize0"<?=$lfolder->PhotoResize == 1? "":"checked"?>> <label for="PhotoResize0"><?=$Lang['eComm']['Origanal']?></label>
												<input type="radio" name="PhotoResize" value="1" id="PhotoResize1"<?=$lfolder->PhotoResize == 1? "checked":""?>> <label for="PhotoResize1"><?=$Lang['eComm']['Resize']?></label>&nbsp;
												<span id="ResizeDemension" <?=$lfolder->PhotoResize == 1? "" : "style=\"visibility: hidden\""?>>(<input type="text" name="DemensionX" maxlength="5" value="<?=$lfolder->DemensionX?>" style="width:40px">&nbsp;x&nbsp;<input type="text" name="DemensionY" maxlength="5" value="<?=$lfolder->DemensionY?>" style="width:40px">px - <?=$Lang['eComm']['Width']?> x <?=$Lang['eComm']['Height']?>)</span>
											</td>
										</tr>
										
										</table>
									</td>
				                </tr>
				                </table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">        
				                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				                <tr>
				                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
						</tr>
				                <tr>
				                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				                </tr>
				                <tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
		                		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.go(-1);","cancelbtn") ?>
							</td>
						</tr>
				                </table>                                
					</td>
				</tr>

	            </table>     
	            </form>         
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	            
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_view_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>

<br>

<?php
print $linterface->FOCUS_ON_LOAD("form1.Title");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>