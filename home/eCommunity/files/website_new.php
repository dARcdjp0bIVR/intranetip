<?php
##	Modifying By:

#######################################
##	Modification Log:
#
#   Date:   2019-10-14  Bill    [DM#3663]
#           fixed JS error
#
#   2019-06-06 (Anna): Added CSRF token checking. 
#	Date:	2017-07-26	Carlos
#			Cater https resource url.
#
#	Date:	2011-05-03	YatWoon
#			add alt / title for the icon
#
#	Date:	2010-12-01 [YatWoon]
#			if there is no folder created, then create a default folder
#
#	2010-03-05 Max (201003051446)
#	- get the GroupID from previous page
#
#######################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
$http_protocol = $is_https? "https://" : "http://";

$GroupID = trim($GroupID);

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);


$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup($GroupID);
$lo 			= new libuser($UserID);
$lfolder 		= new libfolder($FolderID);
$currentMode	= "add";
$FileType		= "W";

$lu 	= new libgroup($GroupID);
$lu2007 	= new libuser2007($UserID);
if (!$lu->isAccessLinks() or !$lu2007->isInGroup($GroupID))
{
    header("Location: ../index.php?msg=AccessDenied");
    intranet_closedb();
    exit();
}

### Title ###
if($_SESSION['UserType']!=USERTYPE_ALUMNI)
$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?FileType=$FileType&DisplayType=$DisplayType&GroupID='+this.value\"",$GroupID);
$folderNo = $lfolder->getFolderNumber($GroupID, $FileType);
# if there is no folder created, then create a temp folder
if(!$folderNo)
{
	$legroup->CreateTmpFolder($GroupID, $FileType, addslashes($lo->NickNameClassNumber()));	
	$folderNo = 1;
}
$FolderListSelection = $lfolder->getFolderList($GroupID, "name='FolderID'", $FolderID, $FileType);

$title = "
	<table width='100%' height='25' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' width='20%' class='contenttitle' nowrap> <span onclick=\"window.location.href='{$PATH_WRT_ROOT}home/eCommunity/group/index.php?GroupID={$GroupID}'\" class='contenttitle GroupTitleLink'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle'/>{$lu->TitleDisplay}</span></td>
			<td align='right' width='80%'>".$lu->displayStorage3()."</td>
			<td align='right' nowrap>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
// $MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$MODULE_OBJ['title'] = $legroup->TitleDisplay;
}
else
{
	$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
}
$linterface->LAYOUT_START();

### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\"><a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'view');\" class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['View']."</a></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
            </table></td>
            <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['Add'] ."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_add_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
            </table>
            </td>
          </tr>
        </table></td>
        </tr>
    </table>
      <br>
";
$rightMenu .= $legroup->RightAddMenu();
### Start Right Menu ###

$TypeNameStr = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_website_add.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" border=\"0\"> ". $eComm['Add_'] . $eComm['Website'];

### File Field
for($f=1;$f<=10;$f++)
{
	$ff .= "
	<tr id=\"ff_$f\" style=\"display:". ($f==1?"block":"none") ."\">
		<td width=\"25\" class=\"tabletext\">$f.</td>
		<td class=\"tabletext\"><input type=\"text\" id=\"url".$f."\" name=\"url".$f."\" class=\"textboxtext\"></td>
	</tr>
	";
}

$STEPS_OBJ[] = array($eComm['input_website'], 1);
$STEPS_OBJ[] = array($eComm['input_detail_info'], 0);
$STEPS_OBJ[] = array($eComm['add_website_finished'], 0);
                
if($folderNo)
{
	$info_table = $linterface->GET_STEPS($STEPS_OBJ);
}
else
{
	$info_table = $linterface->GET_WARNING_TABLE($eComm['NoFolder']['W']);
}

?>

<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$http_protocol?><?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>
<script language="JavaScript">
function newFile(t)
{
         with(document.form1a)
         {
                action = t+"_new.php";
                submit();
         }
}

function clickMore()
{
	for(var i=1;i<=10;i++)
	{
		if(eval("document.getElementById('ff_"+i+"').style.display;") == "none")
		{
			eval("document.getElementById('ff_"+i+"').style.display='block';") 
			break;
		}
	}
	
	if(i>=10)
		document.getElementById('tr_ff_more').style.display='none';
}

function checkForm(obj)
{
	var flag = 0;
	for(var i=1;i<=10;i++)
	{
		tname = eval("document.getElementById('url"+ i +"').value;");
		if(tname)	flag++;
	}
	
	if(!flag)	
	{
		alert("<?=$eComm['js_please_input_url']?>");
		return false;
	}
	
	return true;

}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt ) 
{
	jFormObject.menutype.value=mt;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 

}
</script>

<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="5" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_01.gif" width="5" height="28"></td>
	            <td height="28" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_02.gif"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
	                <tr>
	                  <td align="left"><table border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_01.gif" width="9" height="28"></td>
	                      <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_02.gif"><span class="ecomm_box_title"><?=$TypeNameStr?></span></td>
	                      <td width="9"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_title_03.gif" width="9" height="28"></td>
	                    </tr>
	                  </table></td>
	                  <td align="right" valign="top"><a href="../group/index.php?GroupID=<?=$GroupID?>"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_off.gif" name="a_emini" width="16" height="16" border="0" id="a_mini" onMouseOver="MM_swapImage('a_mini','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/btn_mini_on.gif',1)" onMouseOut="MM_swapImgRestore()" alt="<?=$Lang['eComm']['BackToHome']?>" title="<?=$Lang['eComm']['BackToHome']?>"></a></td>
	                </tr>
	              </table></td>
	            <td width="6" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_03.gif" width="6" height="28"></td>
	            <td width="130" height="28"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="130" height="10"></td>
	            <td width="13">&nbsp;</td>
	          </tr>
	          <tr>
	            <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_04.gif" width="5" height="240"></td>
	            <td valign="top">
	            <br />
	            
	            <form name="form1" method="post" action="website_new_update.php" onSubmit="return checkForm(this);">
	            <input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
				<input type="hidden" name="FileType" value="<?=$FileType;?>">
				<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
				
				<?php echo csrfTokenHtml(generateCsrfToken()); ?>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td align="center">
                
                <?=$info_table;?>                
                
                </td>
              </tr>
              
              <tr>
                <td height="10" align="center"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
              </tr>
              
              <? if($folderNo) { ?>
              <tr>
                <td class="tabletext"><?=$eComm['add_to_category']?>: 
                <?=$FolderListSelection?>  
                </td>
              </tr>
              
              <tr>
                <td><table width="95%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="120" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_01.gif" width="4" height="10"></td>
                          <td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_02.gif" width="4" height="10"></td>
                        </tr>
                        <tr>
                          <td width="4" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_03.gif" width="4" height="14"></td>
                          <td align="right" bgcolor="#addbd4"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
                            </tr>
                            <tr>
                              
                              <td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/btn_share_add_menu_bg.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" height="34"></td>
                                  <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_url.gif" width="20" height="20" border="0" align="absmiddle"> <span class="ecomm_box_title"><?=$eComm['URL']?></span> </td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_05.gif" width="4" height="4"></td>
                          <td height="4" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_menu_06.gif" width="4" height="4"></td>
                        </tr>
                    </table></td>
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="10" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_01.gif" width="10" height="10"></td>
                          <td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_02.gif" width="10" height="10"></td>
                          <td width="12" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_03.gif" width="12" height="10"></td>
                        </tr>
                        <tr>
                          <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_04.gif" width="10" height="150"></td>
                          <td valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                            
                          <?=$ff?>
                          
                            <tr id="tr_ff_more" style="display:block">
                              <td class="tabletext">&nbsp;</td>
                              <td class="tabletext"><a href="javascript:clickMore();" class="box_more"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_add_entry.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_general_more?></a></td>
                            </tr>
                            
                          </table></td>
                          <td width="12" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_06.gif" width="12" height="10"></td>
                        </tr>
                        <tr>
                          <td width="10" height="11"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_07.gif" width="10" height="11"></td>
                          <td height="11" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_08.gif" width="12" height="11"></td>
                          <td width="12" height="11"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_add_board_09.gif" width="12" height="11"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="top">&nbsp;</td>
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr>
                        <td align="right">
                        
                        <?= $linterface->GET_ACTION_BTN($button_next, "submit", "","submit2") ?>
		                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
								
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                </td>
              </tr>
              
              <? } ?>
              </table>
	            </form> 
	            
	               
	            </td>
	            <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_06.gif" width="6" height="17"></td>
	            
	            <td width="130" align="right" valign="top" bgcolor="#eff9e0" style="background-image:url(<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;">
	            <div id="div_right_menu"><?=$rightMenu?></div>	
	            </td>
	            
	            <td width="13" valign="top" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_middle_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_top_add_02.gif" width="13" height="32" id="rightmenuimg"></td>
	          </tr>
	          <tr>
	            <td width="5" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_07.gif" width="5" height="29"></td>
	            <td height="29" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08a.gif" width="19" height="29"></td>
	                <td align="right"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_08b.gif" width="261" height="29"></td>
	              </tr>
	            </table></td>
	            <td width="6" height="29"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_09.gif" width="6" height="29"></td>
	            <td width="130" height="29" align="left" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_02.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_01.gif" width="124" height="29"></td>
	            <td width="13"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/ecomm/share_board_sub_bottom_03.gif" width="13" height="29"></td>
	          </tr>
	        </table>
		</td>
	</tr>
</table>        

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="W">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
<input type="hidden" name="currentMode" value="add">
</form>

<form name="form1a" action="file_view.php" method="post">
<input type="hidden" name="DisplayType" value="<?=$DisplayType?>">
<input type="hidden" name="FileType" value="<?=$FileType?>">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="FileID" value="<?=$FileID?>">
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>