<?php
// Using:

###################################
#
#   Date:   2019-09-04  Bill    [2019-0815-1424-47235]
#           Added checking before delete any files
#           Delete related comments in db
#
###################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FileID = IntegerSafe($FileID);

$lo = new libfilesystem();
$lg = new libgroup($GroupID);
$legroup = new libegroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,9,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if (!$legroup->hasAdminFiles($UserID))
{
	header("Location: ../group/?GroupID=$GroupID");
    exit();
}

for($i=0;$i<sizeof($FileID);$i++)
{
	$thisFileID = $FileID[$i];
	if(!$thisFileID) {
		continue;
	}

	$lf = new libfile($thisFileID);
	$FileType = $lf->FileType;
	
	// Delete the file record
	$lo = new libfilesystem();
	if ($lg->hasAdminFiles($UserID) || $lf->UserID==$UserID)
	{
		$Folder = $intranet_root."/file/group/g".$lf->GroupID;
        $path = $Folder."/".$lf->Location;

        if($lf->GroupID && $lf->Location != '' && file_exists($path)) {
            $lo->lfs_remove($path);
        }
        // $lo->lfs_remove($Folder."/".$lf->Location);
	}
	
	// $lf->db_db_query("SELECT * FROM INTRANET_FILE_COMMENT WHERE FileID = '$thisFileID'");
    $sql = "DELETE FROM INTRANET_FILE_COMMENT WHERE FileID = '$thisFileID'";
    $lf->db_db_query($sql);
	
	$sql = "DELETE FROM INTRANET_FILE WHERE FileID = '$thisFileID'";
	$lf->db_db_query($sql) or die(mysql_error());
}

intranet_closedb();
header("Location: settings_file.php?xmsg=delete&GroupID=$GroupID&FileType=$page_FileType&FolderID=$page_FolderID&pstatus=$page_pstatus&keyword=$page_keyword");
?>
