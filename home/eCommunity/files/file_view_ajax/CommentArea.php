<?php

# being modified by: Thomas


/********************** Change Log ***********************/
#
#	Date    :   2010-07-06 [Thomas]
#   Details :   Use AJAX to load comments
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT.'includes/libdbtable.php');
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
//include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfile.php");
//include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$File_Type    = $_POST["File_attr"][0];
$Group_ID     = $_POST["File_attr"][1];
$File_ID      = $_POST["File_attr"][2];
$Folder_ID    = $_POST["File_attr"][3];
$Display_Type = $_POST["File_attr"][4];

$linterface 	= new interface_html();
$legroup 		= new libegroup($Group_ID);
$lf 			= new libfile($File_ID);

$File_Type		= $lf->FileType ? $lf->FileType : $File_Type;

if($lf->Approved==1)
{
	$CommentArea = "
		<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		  <tr>
		    <td align=\"center\">

		    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
		      <tr>
		        <td align=\"left\"><span class=\"tabletext\"> <strong><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $eComm['Comment'] ." (<span id=\"CommentNo\">". $lf->getCommentNumber() ."</span>) </strong></span></td>
		        <td align=\"right\"><span id=\"write_comment_str\"><a href=\"javascript:void(0);\" onClick=\"toggle_comemnt_form();\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['WriteYourcomment'] ." </a></span></td>
		      </tr>
		    </table>

		     <div id=\"write_comment_form\" style=\"display:none\">
			<form name=\"form1\" action=\"comment_add_update.php\" method=\"post\" onSubmit=\"return checkForm(this);\">
			<input type=\"hidden\" name=\"DisplayType\" value=\"". $Display_Type ."\">
			<input type=\"hidden\" name=\"FileType\" value=\"". $File_Type ."\">
			<input type=\"hidden\" name=\"GroupID\" value=\"". $Group_ID ."\">
			<input type=\"hidden\" name=\"FolderID\" value=\"". $Folder_ID ."\">
			<input type=\"hidden\" name=\"FileID\" value=\"". $File_ID ."\">
					
			<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
			<tr>
			  <td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"><span class=\"tabletext\">". $eComm['Yourcomment'] ."</span></td>
			</tr>
			<tr>
			  <td align=\"center\">". $linterface->GET_TEXTAREA("Comment","") ."</td>
			</tr>
			<tr>
			  <td align=\"center\">
			  <input name=\"submit\" type=\"button\" class=\"formbutton\" value=\"". $button_submit ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"AddComment()\">
			  <input name=\"submit\" type=\"reset\" class=\"formbutton\" value=\"". $button_reset ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\">
				<input name=\"cancel_btn\" type=\"button\" class=\"formbutton\" value=\"". $button_cancel ."\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"toggle_comemnt_form()\">
			  </td>
			</tr>
			<tr>
			  <td align=\"right\" class=\"dotline\">&nbsp;</td>
			</tr>
			
			</table></form>
			</div>

	";

	$CommentArea .= "<div id=\"Comment_List\">".$legroup->DisplayComments($File_ID)."</div>";

	$CommentArea .= "
		      </td>
		  </tr>
		  <tr>
		    <td>&nbsp;</td>
		  </tr>
		</table></td>

	";
}

intranet_closedb();

echo $CommentArea;
?>