<?php
#using:
############ Change Log [Start] ##############
#
#
############ Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfolder.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$FolderID = IntegerSafe($FolderID);

$li = new libuser($UserID);
$lf = new libfolder($FolderID);
$legroup = new libegroup($GroupID);

/*
debug_pr($_POST); 

# check ext
$RawFileName = $_FILES['myfile']['name'];
$Rawext = strtolower(get_file_ext($RawFileName));
$Rawext = substr($Rawext, 1, strlen($Rawext)-1);
debug_pr($Rawext);

# double check file extension
if(strtoupper($Rawext) == "ZIP")
{
	
	
	
	
	
}

debug_pr("aaa");

exit;

*/








$file_upload_count = 10;

$FileID = array();

if($li->IsGroup($GroupID))
{
	$lg = new libgroup($GroupID);
	
	for($i=1;$i<=$file_upload_count;$i++)
	{
		$lo = new libfilesystem();
		
		$RawFileLocation = $_FILES['myfile'.$i]['tmp_name'];
		if(!$RawFileLocation)	continue;
		
		$RawFileName = $_FILES['myfile'.$i]['name'];
		
		$Rawext = strtolower(get_file_ext($RawFileName));
		$Rawext = substr($Rawext, 1, strlen($Rawext)-1);
		
		$tmp_dir = session_id()."_".time()."_".$i;
		$tmp_src = "/tmp/".$tmp_dir;
		mkdir($tmp_src);
		
		$file_ary = array();
		
		#unzip / copy file to temp folder
		if($Rawext == 'zip')
			$lo->unzipFile($RawFileLocation, $tmp_src);
		else
		{
			$tmp_Location = session_id()."-".(time()+$i).strtolower(substr($RawFileName, strrpos($RawFileName,".")));
			$lo->lfs_copy($RawFileLocation, $tmp_src."/".$tmp_Location);
		}
		
		$temp_ary = array();
		$temp_ary = $lo->return_folderlist($tmp_src);
		
		$file_ary = array();
		$file_ary_key = 0;
			
		for($k=0;$k<sizeof($temp_ary);$k++)
		{
			$File_Name = substr($temp_ary[$k], strrpos($temp_ary[$k], "/")+1);  //without opening "/"
			$File_ext  = strtolower(get_file_ext($File_Name));
			$File_ext  = substr($File_ext, 1, strlen($File_ext)-1);
			
			if(!is_dir($temp_ary[$k]) && $File_ext != 'ini')
			{
				#Get File into and save in an array
				$file_ary[$file_ary_key]['Location'] = $temp_ary[$k];
				$file_ary[$file_ary_key]['FileName'] = ($Rawext == 'zip'? $File_Name : $RawFileName);
				$file_ary[$file_ary_key]['FileSize'] = filesize($temp_ary[$k]) / 1000;
				$file_ary_key++;
			}
		}
			
		for($j=0;$j<sizeof($file_ary);$j++)
		{
			$FileLocation = $file_ary[$j]['Location'];
			$FileName     = $file_ary[$j]['FileName'];
			$FileSize     = $file_ary[$j]['FileSize'];
			
			if($FileLocation && $FileName && $FileSize)
			{
				## check file format
				if($lg->AllowedImageTypes != "ALL")
				{
					$allowedAry = explode("/",strtolower($lg->AllowedImageTypes));
					$ext = strtolower(get_file_ext($FileName));
					$ext = substr($ext, 1, strlen($ext)-1);
					if(!in_array($ext, $allowedAry))	
					{
						$error_msg = "file_type_not_allowed";	
						continue;
					}
				}
		
				$FLocation = session_id()."-".(time()+$i).$j.strtolower(substr($FileName, strrpos($FileName,".")));

				$used = $lg->returnGroupUsedStorageQuota();
				$available = ($lg->StorageQuota)*1000 - $used;
				
				if ($available >= $FileSize)
				{
					$Folder = $intranet_root."/file/group/g".$GroupID;
					$lo->folder_new($Folder);
					
					#check if the folder need resize
					if($lf->PhotoResize == 1)
						$legroup->resize_photo($FileLocation, $Folder."/".$FLocation, $lf->DemensionX, $lf->DemensionY);
					else
						$lo->lfs_copy($FileLocation, $Folder."/".$FLocation);

					$approved = $legroup->needApproval?"0":"1";
					$save_FileName = $li->Get_Safe_Sql_Query($FileName); //remove unwanted char in Filename
			
					$sql = "
						INSERT INTO INTRANET_FILE 
						(GroupID, UserID, UserName, UserEmail, Title, UserTitle, Location, ReadFlag, DateInput, DateModified, Size, FolderID, FileType, Approved) 
						VALUES 
						(".$GroupID.", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$save_FileName', '$save_FileName', '$FLocation', ';$UserID;', now(), now(), $FileSize, $FolderID, 'P', ". ($lg->needApproval?"0":"1") .")
					";

					$li->db_db_query($sql) or die(mysql_error());
					array_push($FileID, $li->db_insert_id());
				}
				else
				{
					$error_msg = "no_space";	
				}
			}
		}
		$lo->folder_remove_recursive($tmp_src);
	}
}

intranet_closedb();
?>

<body onLoad="document.form1.submit();">
<form name="form1" action="photo_new_step2.php" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="<?=serialize($FileID);?>">
<input type="hidden" name="error_msg" value="<?=$error_msg;?>">
</form>
</body>