<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroup.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libgroup($GroupID);
$img = $i_GroupRoleOrganization[$li->RecordType];
if ($img == "") $img = "misc";
$image_lf = ${"i_schoolinfo_organization_iconleft_$img"};
$image_rt = ${"i_schoolinfo_organization_iconright_$img"};
$image_bt = ${"i_schoolinfo_organization_iconbottom_$img"."_popup"};

$content = get_file_content("$intranet_root/file/orpage.txt");
if ($content != "")
{
    $data = explode("\n",$content);
    $hideMember = ($data[0]==1);
    $hideEmail = ($data[1]==1);
}
else
{
    $hideMember = false;
    $hideEmail = false;
}


/*

$x .= "<table width=353 border=0 cellpadding=0 cellspacing=0>\n";
$x .= "<tr>\n";
$x .= "<td>".${"i_frontpage_schoolinfo_popup_".$img."_t"}."</td>\n";
$x .= "<td class=schoolinfo_popup_".$img."_t_bg><img src=../../images/space.gif border=0 width=237 height=8><br><b>".$li->Title."</b>".$li->returnImageIconForURL($li->URL)."</td>\n";
$x .= "<td><img src=../../images/frontpage/schoolinfo_popup_".$img."_tr.gif border=0 width=21 height=44></td>\n";
$x .= "</tr>\n";
if ($li->Description != "")
{
    $x .= "<tr><td colspan=3 class=schoolinfo_popup_".$img."_m_bg align=center>\n";
    $x .= "<table width=90%><tr><td align=left>".$li->Description."</td></tr></table></td></tr>\n";
}
$x .= "<tr><td colspan=3 class=schoolinfo_popup_".$img."_m_bg align=center><br>".$li->displayDirectoryUsers()."<br><br></td></tr>\n";
$x .= "<tr><td colspan=3><img src=../../images/frontpage/schoolinfo_popup_".$img."_b.gif border=0 width=353 height=17></td></tr>\n";
$x .= "</table>\n";
*/
include_once("../../templates/fileheader.php");
?>

<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="475">&nbsp;</td>
  </tr>
  <tr align="center">
    <td colspan="2">
      <table class=schoolinfo_organization_<?=$img?>_m_bg width="481" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="94"><?=$image_lf?></td>
          <td width="242" align="left" valign="top" class=h1><?=$li->returnCategoryName()?></td>
          <td width="145" class=td_corner><img src="<?=$image_path?>/organization/<?=$img?>_top_right_pop.gif" width="87" height="21"></td>
        </tr>
      </table>
      <table class=schoolinfo_organization_<?=$img?>_p_bg width="481" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td> <span class="h1"><font color="#006600"><?=intranet_wordwrap($li->Title,30,"\n",1)?></font></span> <?=$li->returnImageIconForURL($li->URL)?>
            <blockquote>
              <p><span class="body"><font color="#006600"><?=nl2br(intranet_wordwrap($li->Description,30,"\n",1))?></font></span></p>
              <table width="380" border="0" cellspacing="0" cellpadding="0" class="body">
              <tr><td>
              <?
              if (!$hideMember)
              {
                   echo $li->displayDirectoryUsers();
              }
              ?>
              </td></tr>
              </table>
            </blockquote>
          </td>
        </tr>
      </table>
      <table width="481" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><?=$image_bt?></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<?php
include_once("../../templates/filefooter.php");
intranet_closedb();
?>