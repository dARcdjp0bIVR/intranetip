<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lgroup = new libgroup($GroupID);

if ($GroupID =="" || $lgroup->hasAdminAllEvent($UserID) || $lgroup->hasAdminInternalEvent($UserID))
{

    if ($lgroup->hasAdminAllEvent($UserID))
    {
        $conds = "";
    }
    else
    {
        $conds = " AND Internal = 1";
    }

    # Grab announcement can be deleted by this user
    $originalList = implode(",", $EventID);
    $sql = "SELECT EventID FROM INTRANET_EVENT WHERE EventID IN ($originalList) AND OwnerGroupID = '$GroupID' $conds";
    $result = $lgroup->returnVector($sql);
    $list = implode(",",$result);

         $sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN ($list)";
         $lgroup->db_db_query($sql);

         $sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($list)";
         $lgroup->db_db_query($sql);

         header("Location: index.php?GroupID=$GroupID&filter=$filter&order=$order&field=$field&msg=3");


}
else
{
    header("Location: ../close.php");
}


intranet_closedb();
?>