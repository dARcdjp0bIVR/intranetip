<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libevent.php");
include("../../../includes/libgroup.php");
include("../../../includes/libgrouping.php");
include("../../../lang/lang.$intranet_session_language.php");
include("../../../templates/fileheader.php");
intranet_auth();
intranet_opendb();

$EventID = IntegerSafe($EventID);


$EventID = (is_array($EventID)? $EventID[0]: $EventID);
$li = new libevent($EventID);
$GroupID = $li->OwnerGroupID;

$lgroup = new libgroup($GroupID);
if ($GroupID =="" || $lgroup->hasAdminInternalEvent($UserID) || $lgroup->hasAdminAllEvent($UserID))
{

         $RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
         $RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";

$toolbar .= "<input type=image src=$image_save>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
//$toolbar .= " <input type=image src=$image_cancel onClick=\"history.back();\">";
$toolbar .= " <a href='../event/?GroupID=$GroupID'><img border=0 src=$image_cancel></a>";

    if ($lgroup->hasAdminAllEvent($UserID))
    {
        $lo = new libgrouping();
        //$public_row = "<tr><td align=right></td><td><input type=checkbox NAME=public value=1>$i_GroupPublicToSchool</td></tr>\n";
        $public_row = "<tr><td colspan=2>$i_EventPublicInstruction".$lo->displayEventGroupsFrontend($EventID)."</td></tr>\n";
    }

         ?>

         <script language="javascript">
         function checkform(obj){
                 if(!check_text(obj.EventDate, "<?php echo $i_alert_pleasefillin.$i_EventDate; ?>.")) return false;
                 if(!check_date(obj.EventDate, "<?php echo $i_invalid_date; ?>.")) return false;
                 if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_EventTitle; ?>.")) return false;
             <?
             if ($lgroup->hasAdminAllEvent($UserID)) {
             ?>
             checkOptionAll(obj.elements["TargetGroupID[]"]);
             <? } ?>
         }
         </script>

<form name=form1 action="edit_update.php" method=post enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $lgroup->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
      <tr>
            <td width="1"><img src="images/spacer.gif" width="1" height="1">
            </td>

      <td align=left>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"></div>
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$button_edit." ".$i_GroupSettingTabs[3]?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
          </tr>
          <tr>
          <td colspan=2 align=center>
         <table width=93% border=0 cellpadding=2 cellspacing=1>
         <tr><td align=right><?php echo $i_EventDate; ?>:</td><td><input class=text type=text name=EventDate size=10 maxlength=10 value="<?php echo $li->EventDate; ?>"> (yyyy-mm-dd)</td></tr>
         <tr><td align=right><?php echo $i_EventTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=255 value="<?php echo $li->Title; ?>"></td></tr>
         <tr><td align=right><?php echo $i_EventVenue; ?>:</td><td><input class=text type=text name=EventVenue size=20 maxlength=100 value="<?php echo $li->EventVenue; ?>"></td></tr>
         <tr><td align=right><?php echo $i_EventNature; ?>:</td><td><input class=text type=text name=EventNature size=20 maxlength=100 value="<?php echo $li->EventNature; ?>"></td></tr>
         <tr><td align=right><?php echo $i_EventDescription; ?>:</td><td><textarea name=Description cols=60 rows=10><?php echo $li->Description; ?></textarea></td></tr>
         <tr><td align=right><?php echo $i_EventRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 <?php echo $RecordStatus1; ?>> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0 <?php echo $RecordStatus0; ?>> <?php echo $i_status_pending; ?></td></tr>
         <?=$public_row?>
         <tr><td></td><td><?=$toolbar?></td></tr>
         </table>
          </td>
          </tr>
        </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
          </tr>
        </table>



    </td>
  </tr>
</table>
<input type=hidden name=EventID value="<?php echo $li->EventID; ?>">
</form>

<?php
}
else
{
    header ("Location: ../settings/?GroupID=$GroupID");
}
intranet_closedb();
include("../../../templates/filefooter.php");
?>