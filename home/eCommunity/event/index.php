<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../tab.php");
include_once("../../../templates/fileheader.php");


$GroupID = IntegerSafe($GroupID);

$lg = new libgrouping();
$lgroup = new libgroup($GroupID);

if ($lgroup->hasAdminInternalEvent($UserID) || $lgroup->hasAdminAllEvent($UserID))
{

     # TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

     $keyword = trim($keyword);
     switch ($field){
             case 0: $field = 0; break;
             case 1: $field = 1; break;
             case 2: $field = 2; break;
             case 3: $field = 3; break;
             case 4: $field = 4; break;
             case 5: $field = 5; break;
             default: $field = 0; break;
     }
     $order = ($order == 1) ? 1 : 0;
     if($filter == "") $filter = 1;
     $filter = ($filter == 1) ? 1 : 0;

if ($lgroup->hasAdminAllEvent($UserID))
{
    $conds = "";
}
else
{
    $conds = "AND a.Internal = 1";
}

     $sql  = "SELECT
                             DATE_FORMAT(a.EventDate, '%Y-%m-%d'),
                             if (a.OwnerGroupID = '$GroupID',CONCAT('<a href=edit.php?EventID[]=', a.EventID, '>', a.Title, '</a>'),a.Title),
                             a.EventVenue,
                             a.EventNature,
                             a.DateModified,
                             if (a.OwnerGroupID = '$GroupID',CONCAT('<input type=checkbox name=EventID[] value=', a.EventID ,'>'),' ')
                     FROM
                             INTRANET_EVENT as a
                     WHERE
                             (a.Title like '%$keyword%' OR a.EventDate like '%$keyword%' OR a.EventNature like '%$keyword%' OR a.EventVenue like '%$keyword%') AND
                             a.RecordStatus = '$filter' AND
                             a.RecordType = 3 AND
                             a.OwnerGroupID = '$GroupID'
                             $conds
                     ";

     # TABLE INFO
     $li = new libdbtable($field, $order, $pageNo);
     $li->field_array = array("EventDate", "Title", "EventVenue", "EventNature", "DateModified");
     $li->sql = $sql;
     $li->no_col = sizeof($li->field_array)+2;
     $li->title = $i_admintitle_event;
     $li->column_array = array(0,5,5,5,0);
     $li->wrap_array = array(0,15,15,15,0);
     $li->IsColOff = 4;

     // TABLE COLUMN
     $li->column_list .= "<td bgcolor=#FCD5AE width=20 class=title_head align=center width=1>#</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=110 class=title_head>".$li->column(0, $i_EventDate)."</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=103 class=title_head>".$li->column(1, $i_EventTitle)."</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=80 class=title_head>".$li->column(2, $i_EventVenue)."</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=80 class=title_head>".$li->column(3, $i_EventNature)."</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=110 class=title_head>".$li->column(4, $i_EventDateModified)."</td>\n";
     $li->column_list .= "<td bgcolor=#FCD5AE width=20 class=title_head>".$li->check("EventID[]")."</td>\n";

     // TABLE FUNCTION BAR
     $toolbar = "<a href=javascript:checkGet(document.form1,'new.php')>".newIcon2()."$button_new</a>";
     $functionbar  = "<a href=javascript:checkEdit(document.form1,'EventID[]','edit.php')><img src=$image_edit border=0></a>";
     $functionbar .= "<a href=javascript:checkRemove(document.form1,'EventID[]','remove.php')><img src=$image_delete border=0></a>";
     $searchbar  = "$i_EventRecordStatus:<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
     $searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
     $searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
     $searchbar .= "</select>\n";
     $searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
     $searchbar .= "<input type=image src=$image_search border=0>\n";

     # From tab.php
     $grp_navigation .= "";

     $xmsg = "";
     switch($msg){
            case 1: $xmsg = "<p> $i_con_msg_add </p>\n"; break;
            case 2: $xmsg = "<p> $i_con_msg_update </p>\n"; break;
            case 3: $xmsg = "<p> $i_con_msg_delete </p>\n"; break;
     }
     echo $xmsg;
    $groupSelect .= jumpIcon().$lgroup->getSelectAdminEvent("name=GroupID onChange=this.form.submit()",$GroupID);
include_once("../tooltab.php");

?>
<form name=form1 action="" method=get>
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $lgroup->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
      <tr>
            <td width="1"><img src="images/spacer.gif" width="1" height="1">
            </td>
      <td align=left width=749>
      <?=$groupSelect?>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$i_GroupSettingsEvent?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" height="1"></td>
          </tr>
          <tr>
            <td colspan=2>
              <table width=100%>
                <tr>
                  <td width="40"><img src="/images/spacer.gif" width="20" height="1"></td>
                  <td width="710" class="decription"><?=$i_EventAdminRight?></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr align="center">
            <td height=10 colspan="2"><img src="/images/spacer.gif" width="450" height="1"></td>
          </tr>
        </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif" height="30">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
            <td valign="middle" class="functionlink" align="left"><?=$toolbar?></td>
            <td valign="middle" align="right"><?=$searchbar?></td>
            <td width="20"><img src="images/spacer.gif" width="20" height="1"></td>
          </tr>
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
            <td></td>
            <td valign="middle" align="right"><?=$functionbar?></td>
            <td width="20"><img src="images/spacer.gif" width="20" height="1"></td>
          </tr>
        </table>
        <?=$li->display()?>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

</form>

<?php
}
else
{
    header ("Location: ../settings/?GroupID=$GroupID");
}
intranet_closedb();
include("../../../templates/filefooter.php");
?>