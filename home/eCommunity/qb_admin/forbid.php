<?php
include_once ("../../../includes/global.php");
include_once ("../../../includes/libdb.php");
include_once ("../../../includes/libgroup.php");

intranet_auth();
intranet_opendb();
$lgroup = new libgroup($GroupID);
if ($lgroup->hasAdminQB($UserID))
{

$li = new libdb();
$status_forbidden = 0;

$list = implode(",",$QuestionID);

$sql = "UPDATE QB_QUESTION SET RecordStatus = $status_forbidden WHERE QuestionID IN ($list)";
$li->db_db_query($sql);

header ("Location: index.php?GroupID=$GroupID&type=$type&msg=2");

}
else   # Not Admin, close window
{
         header ("Location: ../close.php");
}
intranet_closedb();
?>