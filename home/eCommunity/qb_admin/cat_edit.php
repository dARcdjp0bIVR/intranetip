<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libqb.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$uniqueID = is_array($uniqueID)? $uniqueID[0]:$uniqueID;

$lg = new libgroup($GroupID);

if ($lg->hasAdminQB($UserID))
{
include_once("menu.php");
include_once("../tab.php");
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");

$lq = new libqb();
$lq->retrieveData($uniqueID,$type);

// TABLE SQL
# From tab.php
$grp_navigation .= "";

$groupSelect = $lq->returnSelectAdminGroup("name=GroupID onChange=this.form.submit()",$GroupID);
$go_button = "<input type=image src=$intranet_httppath$image_go alt='$button_go'>";

?>
<form name="form1" method="post" action="cat_edit_update.php">
<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="100%" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg1.gif">
      <tr>
            <td width="1"><img src="<?=$image_path?>/spacer.gif" width="1" height="1">
            </td>

      <td colspan=3 align=left>
      <?=$groupSelect?>
      </td></tr>
          <tr>
            <td colspan=4 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="<?=$image_path?>/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
        <tr>
          <td width="16">&nbsp;</td>
            <td width="143" class="qb_on_tab"><a class=qb_off_font href=index.php?GroupID=<?=$GroupID?>><?=$i_QB_QuestionAdmin?></a></td>
            <td width="143" class="qb_off_tab"><a class=qb_on_font href=settings.php?GroupID=<?=$GroupID?>><?=$i_QB_Settings?></a></td>
          <td width="464">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="66">&nbsp;</td>
          <td width="560" align="center" valign="top">&nbsp;</td>
          <td width="140">&nbsp;</td>
        </tr>
        <tr>
          <td >&nbsp;</td>
          <td align=center>
<table width=500 cellpadding=10 cellspacing=1>
<tr><td><?=$button_edit." ".$lq->tableTitle[$type]?></td><td></td></tr>
<tr><td><?=$i_QB_EngName?>:</td><td><input type=text size=40 name=engName value='<?=$lq->engName?>'></td></tr>
<tr><td><?=$i_QB_ChiName?>:</td><td><input type=text size=40 name=chiName value='<?=$lq->chiName?>'></td></tr>
<tr><td><?=$i_QB_DisplayOrder?>:</td><td><input type=text size=5 name=displayOrder value='<?=$lq->displayOrder?>'></td></tr>
<tr><td></td><td><input type=image src="<?=$image_submit?>"></td></tr>
</tr>
</table>

          </td>
          <td>
          </td>
        </tr>
      </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<input type=hidden name=type value=<?=$type?>>
<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=uniqueID value=<?=$uniqueID?>>
</form>


<?php
include ("../../../templates/filefooter.php");
}
else   # Not Admin, close window
{
      header ("Location: ../close.php");
}
//foot(menuText(1));
//echo menuText(1);
intranet_closedb();
?>