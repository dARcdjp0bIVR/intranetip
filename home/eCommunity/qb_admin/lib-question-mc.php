<?php
class question_mc extends libdb{

     var $question, $question_html_opt, $question_attach;
     var $choiceA, $choiceA_html_opt, $choiceA_attach;
     var $choiceB, $choiceB_html_opt, $choiceB_attach;
     var $choiceC, $choiceC_html_opt, $choiceC_attach;
     var $choiceD, $choiceD_html_opt, $choiceD_attach;
     var $choiceE, $choiceE_html_opt, $choiceE_attach;
     var $category, $answer, $status;
     var $hint, $hint_html_opt, $hint_attach;
     var $explanation, $explanation_html_opt, $explanation_attach;
     var $reference, $reference_html_opt, $reference_attach;
     var $inputdate, $modified;
     var $lenCategory, $lenQuestionCol, $lenQuestionInput;
     var $lenQuestionCol, $lenChoiceCol;
     var $que_no;
     var $path_base, $media_path, $name_prefix;
     var $importFile;
     var $category_set, $status_set;
     var $destination;

     function question_mc(){
          global $ck_course_id, $NS4;
          global $intranet_db;
          $this->db = $intranet_db;//classNamingDB($ck_course_id);

          if ( $NS4 ) {
               $this->lenCategory = 20;
               $this->lenQuestionCol = 37;
               $this->lenQuestionInput = 17;
               $this->lenChoiceCol = 20;
               $this->lenChoiceInput = 10;
          }
          else {
               $this->lenCategory = 30;
               $this->lenQuestionCol = 70;
               $this->lenQuestionInput = 30;
               $this->lenChoiceCol = 36;
               $this->lenChoiceInput = 20;
          }
     }

     function returnQuestion($question_id){
          $separator = "===@@@===";
          $sql = "SELECT category, question, answer, hint, explanation, reference, inputdate, modified, status FROM question WHERE question_id = $question_id";
          $this->rs=$this->db_db_query($sql);
          if($this->rs && $this->db_num_rows()!=0){
               $row = $this->db_fetch_array();
               $this->category = $row[0];
               $question = split($separator, $row[1]);
               $this->question = $question[0];
               $this->question_html_opt = $question[1];
               $this->question_attach = $question[2];
               for ($k=1; $k<6; $k++) {
                    $z = $k * 3;
                    $this->{"choice".chr(64+$k)} =
 $question[$z];
                    $this->{"choice".chr(64+$k)."_html_opt"} = $question[$z+1];
                    $this->{"choice".chr(64+$k)."_attach"} = $question[$z+2];
               }
               $this->answer = $row[2];
               $hint = split($separator, $row[3]);
               $this->hint = $hint[0];
               $this->hint_html_opt = $hint[1];
               $this->hint_attach = $hint[2];
               $explanation = split($separator, $row[4]);
               $this->explanation = $explanation[0];
               $this->explanation_html_opt = $explanation[1];
               $this->explanation_attach = $explanation[2];
               $reference = split($separator, $row[5]);
               $this->reference = $reference[0];
               $this->reference_html_opt = $reference[1];
               $this->reference_attach = $reference[2];
               $this->inputdate = $row[6];
               $this->modified = $row[7];
               $this->status = $row[8];
               return 1;
          }else{
               return 0;
          }
          $this->db_free_result();
     }

     function displayTest($i, $question_id, $mark){
          global $qbank_category, $qbank_question, $qbank_hint, $question_mark;
          $this->returnQuestion($question_id);
          $x  = "";
          $x .= $this->displayQuestionTitle($this->question." <span class=guide>($mark $question_mark)</span>", $this->question_html_opt, $this->question_attach);
          if ($this->question_attach=="") $x .= "<br>\n";
          $x .= "<table width=100% border=0 cellpadding=0 cellspacing=0>\n";
          $x .= $this->displayAnswerChoice("A.", "A", $this->choiceA, $this->choiceA_html_opt, $this->choiceA_attach, "answer$i", 0);
          $x .= $this->displayAnswerChoice("B.", "B", $this->choiceB, $this->choiceB_html_opt, $this->choiceB_attach, "answer$i", 0);
          $x .= $this->displayAnswerChoice("C.", "C", $this->choiceC, $this->choiceC_html_opt, $this->choiceC_attach, "answer$i", 0);
          $x .= $this->displayAnswerChoice("D.", "D", $this->choiceD, $this->choiceD_html_opt, $this->choiceD_attach, "answer$i", 0);
          $x .= $this->displayAnswerChoice("E.", "E", $this->choiceE, $this->choiceE_html_opt, $this->choiceE_attach, "answer$i", 0);
          $x .= "<tr><td colspan=4>&nbsp;</td></tr>\n";
          if ($this->hint!="") $x .= "<tr><td colspan=4>".$this->displayQuestionTitle("<span class=intro>$qbank_hint: </span><span class=guide>".$this->hint."</span>", $this->hint_html_opt, $this->hint_attach)."</td></tr>\n";
          $x .= "</table>\n";
          return $x;
     }


//NEW
     function displayQuestion($question_id){
          global $qbank_category, $qbank_hint, $qbank_expl, $qbank_ref, $qbank_answer_choice;
          global $qbank_question;
          $this->returnQuestion($question_id);

          $x = "<table width='95%' border='0' cellpadding='2' cellspacing='1' align=center>\n";
          $x .= "<tr><td colspan='2'><span class='guide'>$qbank_category :</span></td></tr>\n";
          $x .= "<tr><td width='25' nowrap='nowrap'>&nbsp;</td><td width='100%'>".$this->category."</td></tr>\n";
          $x .= "</table>\n";

          $x .= "<br>\n";
          $x .= $this->displayContent($qbank_question, $this->question, $this->question_html_opt, $this->question_attach);
          $x .= "<table width=95% align=center><tr><td><span class='guide'>$qbank_answer_choice :</span></td></tr><tr><td align=right>\n";
          $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
          $x .= $this->displayAnswerChoice("A.", "A", $this->choiceA, $this->choiceA_html_opt, $this->choiceA_attach, "answer", 1);
          $x .= $this->displayAnswerChoice("B.", "B", $this->choiceB, $this->choiceB_html_opt, $this->choiceB_attach, "answer", 1);
          $x .= $this->displayAnswerChoice("C.", "C", $this->choiceC, $this->choiceC_html_opt, $this->choiceC_attach, "answer", 1);
          $x .= $this->displayAnswerChoice("D.", "D", $this->choiceD, $this->choiceD_html_opt, $this->choiceD_attach, "answer", 1);
          $x .= $this->displayAnswerChoice("E.", "E", $this->choiceE, $this->choiceE_html_opt, $this->choiceE_attach, "answer", 1);
          $x .= "</table><br>\n";
          $x .= "</td></tr></table>\n";
          $x .= $this->displayContent($qbank_hint, $this->hint, $this->hint_html_opt, $this->hint_attach);
          $x .= $this->displayContent($qbank_expl, $this->explanation, $this->explanation_html_opt, $this->explanation_attach);
          $x .= $this->displayContent($qbank_ref, $this->reference, $this->reference_html_opt, $this->reference_attach);
          $x .= "<br>\n";

          return $x;
     }


     function displayMediaFile($image_attach){
          global $admin_url_path, $eclass_root, $image_path, $list_next, $list_prev;

          if ($image_attach=="") {
               return "&nbsp";
          }

          $att_arr = split("\:", $image_attach);
          $img_total = sizeof($att_arr);
          $img_hidden = "<input type='hidden' value='1".$this->getFilePath($image_attach);
          srand((double)microtime()*1000000);
          $prefix = rand(0,10000).$img_total."N".rand(0,10000).(strlen($image_attach)%1000)."R".rand(0,100000);
          $img_hidden .= "' name='imgH_$prefix' >";

          // check if the source is from outside (ie, http://, ftp://)

          $img = "<img src=\"$image_path/space.gif\" border='0' height='60' width='80' name='imgN_$prefix'>";

          $x = "";
          $x .= "<table width='100' border='0' cellpadding='0' cellspacing='0'>\n";
          $x .= "<tr align='center' valign='top'>\n";
          $x .= "<td colspan='3' background='$image_path/control/tn_top.gif' class=small height='16'><div id='imgS_$prefix' align='center'>1/$img_total</div></td>\n";
          $x .= "</tr><tr>\n";
          $x .= "<td width='10' valign='top'><img src='$image_path/control/tn_left.gif'></td>\n";
          $x .= "<td width='80' valign=top>".$img.$img_hidden."</td>\n";
          $x .= "<td width='10' valign='top'><img src='$image_path/control/tn_right.gif'></td>\n";
          $x .= "</tr><tr>\n";
          $x .= "<td colspan='3'>";
          $x .= ($img_total>1) ? "<a href='JavaScript:changePic(document.form1.imgN_$prefix, document.form1.imgH_$prefix, \"imgS_$prefix\", 0)'><img src='$image_path/control/tn_arrowleft.gif' border='0' alt='$list_prev'></a>" : "<img src='$image_path/control/tn_noarrowleft.gif' border='0'>";
          $x .= "<img src='$image_path/control/tn_hori_line1.gif' border='0'>";
          if ($img_total>0) $x .= "<a href=\"javascript:newWin('$admin_url_path/src/assessment/questionbank/media_view.php', 'imgH_$prefix')\"><img src='$image_path/control/tn_zoom.gif' border='0'></a>";
          $x .= "<img src='$image_path/control/tn_hori_line2.gif' border='0'>";
          $x .= ($img_total>1) ? "<a href='JavaScript:changePic(document.form1.imgN_$prefix, document.form1.imgH_$prefix, \"imgS_$prefix\", 1)'><img src='$image_path/control/tn_arrowright.gif' border='0' alt='$list_next'></a>" : "<img src='$image_path/control/tn_noarrowright.gif' border='0'>";
          $x .= "</tr><tr>\n";
          $x .= "<td colspan='3' valign='top' height='10'><img src='$image_path/control/tn_bottom.gif'></td>\n";
          $x .= "</tr>\n";
          $x .= "</table>\n";
          if ($img_total>0) $x .= "<script language=javascript>changePic(document.form1.imgN_$prefix, document.form1.imgH_$prefix, \"imgS_$prefix\", 2);</script>\n";

          return $x;
     }

//NEW
     function displayContent($name, $title, $html_opt, $attach){
          if ($title=="")
               return ;
          $x = "";
          $title = ($html_opt=="1") ? $this->undo_htmlspecialchars($title) : $this->checktext($title);

          $x .= "<table width='95%' border='0' cellspacing='2' cellpadding='1' align=center>\n";
          $x .= "<tr>\n";
          $x .= "<td colspan='2'><span class='guide'>$name :</span></td>\n";
          $x .= "<td rowspan='2'>".$this->displayMediaFile($attach)."</td>\n";
          $x .= "</tr>\n";
          $x .= "<tr> \n";
          $x .= "<td width='25' nowrap='nowrap'>&nbsp;</td>\n";
          $x .= "<td width='100%' valign=top>$title</td>\n";
          $x .= "</tr>\n";
          $x .= "</table>\n";
          $x .= "<br>\n";

          return $x;
     }

     function displayQuestionTitle($title, $html_opt, $attach){
          if ($title=="")
               return ;

          $x = "";
          $title = ($html_opt=="1") ? $this->undo_htmlspecialchars($title) : $this->checktext($title);

          $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='1'>\n";
          $x .= "<tr>\n";
          $x .= "<td width='100%' valign=top>".$title."</td>\n";
          $x .= "<td>".$this->displayMediaFile($attach)."</td>\n";
          $x .= "</tr>\n";
          $x .= "</table>\n";

          return $x;
     }

     function displayMarkContent($name, $title){

          if($title<>"")
               $x .= "<tr><td><i>$name</i></td><td width='100%'>$title</td></tr>\n";
          return $x;
     }

/* YuEn: to be deleted */
     function displayQuestionChoice($name, $answer, $title, $html_opt, $attach, $fieldname, $flag){
          global $admin_url_path, $ck_course_id;
          if($title<>""){
               $title = ($html_opt=="1") ? $this->undo_htmlspecialchars($title) : $this->checktext($title);

               $image_file="$admin_url_path/file/".classNamingDB($ck_course_id)."/question/$attach";
               if($attach<>"") $attach = $this->displayImage($image_file, 70);

               $flag = ($flag == 0) ? "" : (($this->answer==$answer) ? "checked" : "");
               $x .= "<tr>";
               $x .= "<td><input type=radio name=$fieldname value=$answer $flag> $name $title</td>";
               $x .= "<td>$attach<br></td>";
               $x .= "</tr>\n";
          }
          return $x;
     }

//NEW
     function displayAnswerChoice($name, $answer, $title, $html_opt, $attach, $fieldname, $flag){
          $x = "";
          $question = ($this->question_html_opt=="1") ? $this->undo_htmlspecialchars($this->question) : $this->checktext($this->question);

          if($title<>""){
               $title = ($html_opt=="1") ? $this->undo_htmlspecialchars($title) : $this->checktext($title);
               $flag = ($flag == 0) ? "" : (($this->answer==$answer) ? "checked" : "");

               $x .= "<tr>\n";
               $x .= "<td width='40' align='right' valign='top' nowrap><input type=radio name=$fieldname value=$answer $flag>&nbsp;</td>\n";
               $x .= "<td nowrap valign='top' height=25>$name&nbsp;</td>\n";
               $x .= "<td width='100%' valign='top'>$title</td>\n";
               $x .= "<td>".$this->displayMediaFile($attach)."</td>\n";
               $x .= "</tr>\n";

          }
          return $x;
     }
     /*
     function displayMcCategory($name, $title){
          global $qbank_public;
          $x  = "";
          $x .= "<tr>\n";
          $x .= "<td align=right width=15%>$name</td>\n";
          $x .= "<td>\n";
          $x .= "<input class=text type=text name=$title size=20 maxlength=30 value=\"".$this->category."\">\n";
          $x .= $this->returnSelectCategory($title);
          $x .= "<input type=checkbox name=status value=1 ".(($this->status=='1')?"checked":"")."> $qbank_public\n";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          return $x;
     }
*/
//NEW
     function displayCategory($name, $title, $style=0){
          global $qbank_public, $import_category_guide;
          $x  = "<tr>\n";
          $asterisk = ($style==0) ? "<span class='asterisk'>*</span>" : "";
          $x .= "<td width='19%' align='right' valign='top'><span class=title>$name</span> $asterisk</td>\n";
          if ($style==0) {
               $x .= "<td width='5%'>&nbsp;</td>\n";
               $x .= "<td width='1%'>&nbsp;</td>\n";
          }
          $x .= "<td width='75%' nowrap>\n";
          $x .= "<input class='inputfield' type='text' name='$title' size='".$this->lenCategory."' maxlength='30' value='".$this->category."'>\n";
          $x .= $this->returnSelectCategory($title);
          $x .= "<input type='checkbox' name='status' value='1' ".(($this->status=='1')?"checked":"")."> $qbank_public\n";
          if ($style==1)
               $x .= "<br><span class=guide>$import_category_guide</span>";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          return $x;
     }

     function displayMcQuestion($name, $title){
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $x = "";
          $x .= "<tr>\n";
          $x .= "<td align=right width=15%>$name</td>\n";
          $x .= "<td><textarea name=".$title." cols=70 rows=3 wrap=virtual>".$value1."</textarea></td>\n";
          $x .= "</tr>\n";
          $x .= "<tr>\n";
          $x .= "<td><br></td><td>";
          $x .= "<input type=checkbox name=".$title."_html_opt value=1 ".(($value2=='1')?"checked":"")."> $html_tags_allowed ";
          $x .= "<input class=text type=text name=".$title."_attach size=20 maxlength=30 value=\"".$value3."\">";
          $x .= "<input class=button type=button value=\"$button_media\" onClick=fileAttach(this.form.".$title."_attach)><input class=button type=button value=\"$button_clear\" onClick=this.form.".$title."_attach.value=''><br>";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          return $x;
     }

     function displayQuestionInput($name, $title){
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $x  = "<tr>\n";
          $x .= "<td align='right' valign='top'><span class=title>".$name."</span> <span class='asterisk'>*</span></td>\n";
          $x .= "<td>&nbsp;</td>\n";
        $x .= "<td>&nbsp;</td>\n";
          $x .= "<td nowrap> <textarea name='".$title."' cols='".$this->lenQuestionCol."' rows='3' class='inputfield' >".$value1."</textarea><br>\n";
          $x .= "<input type='checkbox' name='".$title."_html_opt' value='1' ".(($value2=='1')?"checked":"")."> $html_tags_allowed ";
          $x .= "<img src='/images/space.gif' width='15' height='5'>\n";
          $x .= "<input name='".$title."_attach' type='text' class='inputfield' size='".$this->lenQuestionInput."' value='".$value3."'>\n";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_media\" onClick=\"fileAttach(this.form.".$title."_attach)\">";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_clear\" onClick=\"this.form.".$title."_attach.value=''\">\n";
          $x .= "<br>&nbsp;</td>\n";
          $x .= "</tr>\n";
          return $x;
     }

     function displayMc($name, $title){
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $x = "";
          $x .= "<tr>\n";
          $x .= "<td align=right width=15%>$name</td>\n";
          $x .= "<td>\n";
          $x .= "<table border=0 cellpadding=2 cellspacing=0>\n";
          $x .= "<tr>\n";
          $x .= "<td><textarea name=".$title." cols=30 rows=3 wrap=virtual>".$value1."</textarea></td>\n";
          $x .= "<td>";
          $x .= "<input class=text type=text name=".$title."_attach size=20 maxlength=30 value=\"".$value3."\"> ";
          $x .= "<input class=buttonsmall type=button value=\"$button_media\" onClick=fileAttach(this.form.".$title."_attach)><input class=buttonsmall type=button value=\"$button_clear\" onClick=this.form.".$title."_attach.value=''><br>";
          $x .= "<input type=checkbox name=".$title."_html_opt value=1 ".(($value2=='1')?"checked":"")."> $html_tags_allowed <br>";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          $x .= "</table>\n";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          return $x;
     }

//NEW
     function displayExtraField($name, $title){
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          $x  = "<tr>\n";
          $x .= "<td align='right' valign='top' class=title>".$name."&nbsp;</td>\n";
          $x .= "<td>&nbsp;</td>\n";
          $x .= "<td>&nbsp;</td>\n";
          $x .= "<td><table width='550' border='0' cellspacing='0' cellpadding='0'>\n";
          $x .= "<tr>\n";
          $x .= "<td width='240'><textarea name='".$title."' cols='".$this->lenChoiceCol."' rows='3' class='inputfield' >".$value1."</textarea></td>\n";
          $x .= "<td width='310'><input type='checkbox' name='".$title."_html_opt' value='1' ".(($value2=='1')?"checked":"")."> ".$html_tags_allowed." <br>\n";
          $x .= "<input name='".$title."_attach' type='text' class='inputfield' size='".$this->lenChoiceInput."' maxlength='30' value=\"".$value3."\">\n";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_media\" onClick=\"fileAttach(this.form.".$title."_attach)\">";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_clear\" onClick=\"this.form.".$title."_attach.value=''\"><br>&nbsp;</td>\n";
          $x .= "</tr></table>\n";
          $x .= "</td></tr>\n";
          return $x;
     }

     function displayMcChoice($name, $title, $answer){
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          $x = "";
          $x .= "<tr>\n";
          $x .= "<td align=right width=15%><input type=radio name=answer value=$answer ".(($this->answer==$answer)?"checked":"")."> $name</td>\n";
          $x .= "<td>\n";
          $x .= "<table border=0 cellpadding=2 cellspacing=0>\n";
          $x .= "<tr>\n";
          $x .= "<td><textarea name=".$title." cols=40 rows=3 wrap=virtual>".$value1."</textarea></td>\n";
          $x .= "<td>";
          $x .= "<input class=text type=text name=".$title."_attach size=20 maxlength=30 value=\"".$value3."\"> ";
          $x .= "<input class=buttonsmall type=button value=\"$button_media\" onClick=fileAttach(this.form.".$title."_attach)><input class=buttonsmall type=button value=\"$button_clear\" onClick=this.form.".$title."_attach.value=''><br>";
          $x .= "<input type=checkbox name=".$title."_html_opt value=1 ".(($value2=='1')?"checked":"")."> $html_tags_allowed <br>";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          $x .= "</table>\n";
          $x .= "</td>\n";
          $x .= "</tr>\n";
          return $x;
     }

//NEW
     function displayChoice($name, $title, $answer){
          global $plain_text, $html_tags_allowed, $button_media, $button_clear;
          $value1 = $this->{$title};
          $value2 = $this->{$title."_html_opt"};
          $value3 = $this->{$title."_attach"};
          $x  = "<tr>\n";
          $x .= "<td>&nbsp;</td>\n";
          $x .= "<td align='right'><input type='radio' name='answer' value='$answer' ".(($this->answer==$answer)?"checked":"")."> $name</td>\n";
          $x .= "<td>&nbsp;</td>\n";
          $x .= "<td><table width='550' border='0' cellspacing='0' cellpadding='0'>\n";
          $x .= "<tr>\n";
          $x .= "<td width='240'><textarea name='".$title."' cols='".$this->lenChoiceCol."' rows='3' class='inputfield' >".$value1."</textarea></td>\n";
          $x .= "<td width='310'><input type='checkbox' name='".$title."_html_opt' value='1' ".(($value2=='1')?"checked":"")."> ".$html_tags_allowed." <br>\n";
          $x .= "<input name='".$title."_attach' type='text' class='inputfield' size='".$this->lenChoiceInput."' maxlength='30' value=\"".$value3."\">\n";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_media\" onClick=\"fileAttach(this.form.".$title."_attach)\">";
          $x .= "<input class='buttonsmall' type='button' value=\"$button_clear\" onClick=\"this.form.".$title."_attach.value=''\"><br>&nbsp;</td>\n";
          $x .= "</tr></table>\n";
          $x .= "</td></tr>\n";
          return $x;
     }

     function createFolder($folderPath) {
          umask(0);
          return (file_exists($folderPath)) ? 0 : mkdir($folderPath, 0777);
     }

     function exportQuestion($question_id){
          $x = "";
          $filter = (sizeof($question_id)>0) ? "AND question_id IN (".implode(",",$question_id).")" : "";
          $separator = "===@@@===";
          $sql = "SELECT category, question, answer, hint, explanation, reference, inputdate, modified, status FROM question WHERE qtype = '0' $filter";
          $this->rs=$this->db_db_query($sql);
          $rowA = $this->returnArray($sql, 9);
          for ($i=0; $i<sizeof($rowA); $i++) {
               $row = $rowA[$i];
               $this->category = $row[0];
               $question = split($separator, $row[1]);
               $this->question = $question[0];
               $this->question_html_opt = $question[1];
               $this->question_attach = $question[2];
               for ($k=1; $k<6; $k++) {
                    $z = $k * 3;
                    $this->{"choice".chr(64+$k)} =
 $question[$z];
                    $this->{"choice".chr(64+$k)."_html_opt"} = $question[$z+1];
                    $this->{"choice".chr(64+$k)."_attach"} = $question[$z+2];
               }
               $this->answer = $row[2];
               $hint = split($separator, $row[3]);
               $this->hint = $hint[0];
               $this->hint_html_opt = $hint[1];
               $this->hint_attach = $hint[2];
               $explanation = split($separator, $row[4]);
               $this->explanation = $explanation[0];
               $this->explanation_html_opt = $explanation[1];
               $this->explanation_attach = $explanation[2];
               $reference = split($separator, $row[5]);
               $this->reference = $reference[0];
               $this->reference_html_opt = $reference[1];
               $this->reference_attach = $reference[2];
               $this->inputdate = $row[6];
               $this->modified = $row[7];
               $this->status = $row[8];
               $x .= "Category=".$this->category."\n";
               $x .= ($this->status==1) ? "Public=yes\n" : "Public=no\n";
               $x .= $this->question."\n";
               $x .= $this->buildOptionalMedia($this->question_html_opt, $this->question_attach);
               $x .= $this->answer."\n";
               $x .= "[[]]\n";
               for ($k=65; $k<70; $k++) {
                    $x .= ($this->{"choice".chr($k)}=="") ? "" : $this->{"choice".chr($k)}."\n";
                    $x .= $this->buildOptionalMedia($this->{"choice".chr($k)."_html_opt"}, $this->{"choice".chr($k)."_attach"});
               }
               $x .= ($this->hint=="") ? "" : $this->hint."\n";
               $x .= $this->buildOptionalMedia($this->hint_html_opt, $this->hint_attach);
               $x .= ($this->explanation=="") ? "" : $this->explanation."\n";
               $x .= $this->buildOptionalMedia($this->explanation_html_opt, $this->explanation_attach);
               $x .= ($this->reference=="") ? "" : $this->reference."\n";
               $x .= $this->buildOptionalMedia($this->reference_html_opt, $this->reference_attach);
               $x .= "###\n\n\n";
          }
          return $x;
     }

     function buildOptionalMedia($html_opt, $attach) {
          global $admin_root_path;

          $x = "";
          $html_opt = ($html_opt==1) ? "HTML" : "";
          if ($attach!="") {
               $attArr = split("\:", $attach);
               for ($i=0; $i<sizeof($attArr); $i++) {
                    //get file location
                    $attArr[$i] = trim($attArr[$i]);
                    $Title = basename($attArr[$i]);
                    $sLen = strlen($attArr[$i])- strlen($Title);
                    $VirPath = trim(substr($attArr[$i], 0, $sLen));
                    $Title = trim($Title);
                    if ($VirPath!="" && $VirPath!="/")
                         $VirPath = "VirPath='".$VirPath."'";
                    else
                         $VirPath = "VirPath is NULL";
                    $sql = "SELECT Location FROM eclass_file WHERE $VirPath AND Title='$Title' AND Category=5 AND IsDir<>1 ";
                    $row = $this->returnArray($sql, 1);
                    $Location = $row[0][0];
                    $filepath = $admin_root_path.$Location."/".$Title;

                    //crop file path
                    if (is_integer($pos=strpos($attArr[$i], "/MC/")))
                         $attArr[$i] = substr($attArr[$i], strpos($attArr[$i], "/", $pos+4)+1);
                    elseif (strpos($attArr[$i], "/")==0)
                         $attArr[$i] = substr($attArr[$i], 1);

                    //make folders
                    $folderArr = split("\/", $attArr[$i]);
                    $dest = $this->destination;
                    for ($k=0; $k<sizeof($folderArr)-1; $k++) {
                         $dest .=  "/". $folderArr[$k];
                         $this->createFolder($dest);
                    }

                    //copy file
                    if (file_exists($filepath))
                         copy($filepath, $dest."/".$Title);
               }
               $attach = implode(", ", $attArr);
          }
          if ($html_opt!="" && $attach!="")
               $x = "[[".$html_opt.", ".$attach."]]";
          elseif ($html_opt!="" || $attach!="")
               $x = "[[".$html_opt.$attach."]]";
          else
               $x = "[[]]";

          return $x."\n";
     }

     function undo_htmlspecialchars($string){
          $string = str_replace("&amp;", "&", $string);
          $string = str_replace("&quot;", "\"", $string);
          $string = str_replace("&lt;", "<", $string);
          $string = str_replace("&gt;", ">", $string);
          $string = str_replace("<form>", "&lt;form&gt;", $string);
          $string = str_replace("</form>", "&lt;/form&gt;", $string);
          $string = str_replace("<input", "&lt;input", $string);
          $string = str_replace("<textarea>", "&lt;textarea&gt;", $string);
          $string = str_replace("</textarea>", "&lt;/textarea&gt;", $string);
          return $string;
     }

     function checktext($string){
          return $this->convertAllLinks2(nl2br($string));
     }

     function returnScore($question_id, $answer){
          $this->returnQuestion($question_id);
          $score = ($this->answer == $answer) ? 1 : 0;
          $a = array($score,$answer);
          return $a;
     }

     function displayQuestionAnswer($name, $answer, $title, $html_opt, $attach, $yourAnswer){
          if($title<>""){
               $title = ($html_opt=="1") ? $this->undo_htmlspecialchars($title) : $this->checktext($title);

               if(strtoupper($yourAnswer)==strtoupper($answer) && strtoupper($this->answer)==strtoupper($answer)) $color = "blue";
               if(strtoupper($yourAnswer)<>strtoupper($answer) && strtoupper($this->answer)==strtoupper($answer)) $color = "green";
               if(strtoupper($yourAnswer)==strtoupper($answer) && strtoupper($this->answer)<>strtoupper($answer)) $color = "red";
               if(strtoupper($yourAnswer)<>strtoupper($answer) && strtoupper($this->answer)<>strtoupper($answer)) $color = "black";

               $x .= "<tr>\n";
               $x .= "<td nowrap valign='top' width=15>&nbsp;</td>\n";
               $x .= "<td height=25 width='100%' valign=top><font color=$color>$name $title</font></td>\n";
               $x .= "<td>".$this->displayMediaFile($attach)."</td>\n";
               $x .= "</tr>\n";
          }
          return $x;
     }

     function showMark($mark, $score){
          global $question_mark, $image_path;
          if($score==0) $x = "&nbsp; <img src=$image_path/cross.gif align=absmiddle border=0 hspace=2><span class=intro>($score $question_mark)</span>\n";
          if($score==$mark) $x = "&nbsp; <img src=$image_path/tick.gif align=absmiddle border=0 hspace=2>($score $question_mark)\n";
          if($score>0 && $score<$mark) $x = "&nbsp; <img src=$image_path/tick_cross.gif align=absmiddle border=0 hspace=2><span class=intro>($score/$mark $question_mark)</span>\n";
          return $x;
     }

     function questionResult($i, $question_id, $mark, $score, $answer, $comment, $isMark){
          global $qbank_category, $qbank_question, $qbank_hint, $qbank_expl, $qbank_ref, $gradebook_comment, $gradebook_score;
          $x  = "";
          if($this->returnQuestion($question_id)){

               $x .= $this->displayQuestionTitle($this->question.$this->showMark($mark, $score), $this->question_html_opt, $this->question_attach);
               if ($this->question_attach=="")
                    $x .= "<br>";
               $x .= "<table width='100%' border=0 cellpadding=0 cellspacing=0>\n";
               $x .= $this->displayQuestionAnswer("A.", "A", $this->choiceA, $this->choiceA_html_opt, $this->choiceA_attach, $answer);
               $x .= $this->displayQuestionAnswer("B.", "B", $this->choiceB, $this->choiceB_html_opt, $this->choiceB_attach, $answer);
               $x .= $this->displayQuestionAnswer("C.", "C", $this->choiceC, $this->choiceC_html_opt, $this->choiceC_attach, $answer);
               $x .= $this->displayQuestionAnswer("D.", "D", $this->choiceD, $this->choiceD_html_opt, $this->choiceD_attach, $answer);
               $x .= $this->displayQuestionAnswer("E.", "E", $this->choiceE, $this->choiceE_html_opt, $this->choiceE_attach, $answer);
               $x .= "</table>\n";
               $x .= "<br>\n";
               if ($this->hint!="") $x .= $this->displayQuestionTitle("<span class=intro>$qbank_hint: </span><span class=guide>".$this->hint."</span>", $this->hint_html_opt, $this->hint_attach);
               if ($this->explanation!="") $x .= $this->displayQuestionTitle("<span class=intro>$qbank_expl: </span><span class=guide>".$this->explanation."</span>", $this->explanation_html_opt, $this->explanation_attach);
               if ($this->reference!="") $x .= $this->displayQuestionTitle("<span class=intro>$qbank_ref: </span><span class=guide>".$this->reference."</span>", $this->reference_html_opt, $this->reference_attach);

               if ($this->hint!="" || $this->explanation!="" || $this->reference!="")
                    $x .= "<br>\n";
               if($isMark==0){
                    $xx = $this->displayMarkContent("$gradebook_comment:", $comment);
               }else{
                    $xx = $this->displayMarkContent("$gradebook_score:", $this->returnSelectMark($score,$mark));
                    $xx .= $this->displayMarkContent("$gradebook_comment:", "<textarea name=comment[] class=inputfield cols=40 rows=3 wrap=virtual></textarea>");
               }
               if ($xx!="") {
                    $x .= "<table width='100%' border=0 cellpadding=0 cellspacing=0>\n";
                    $x .= $xx . "</table>\n";
               }
          }
          return $x;
     }

     function returnSelectMark($score,$mark){
          $x  = "";
          $x .= "<select name=score[]>";
          $x .= "<option value=$score>$score</option>";
          for($i=0; $i<=$mark; $i++){
          $x .= "<option value=$i>$i</option>";
          }
          $x .= "</select>";
          return $x;
     }

     function returnQuestionStat($title, $attempts, $total){
          global $image_path;
          $percent = ($total==0) ? 0 : round(100*$attempts/$total);
          $x .= "<tr>";
          $x .= "<td align=center>$title</td>";
          $x .= "<td align=center>$attempts</td>";
          $x .= "<td>";

          $x .= $this->genPercentageBar($percent);

          $x .= "</td>";
          $x .= "</tr>\n";
          return $x;
     }

     function displayStatistics($question_id,$sql_filter){
          global $image_path, $qbank_ans, $gradebook_attempts, $qbank_statistics, $qbank_attempts;
          $a = $b = $c = $d = $e = 0;
          $sql = "SELECT answer FROM self_test_question WHERE question_id = $question_id ".$sql_filter;
          if($sql_filter=="") $answerSelfTest = $this->returnArray($sql, 1);
          $sql = "SELECT answer FROM quiz_result_question WHERE question_id = $question_id ".$sql_filter;
          $answerQuiz = $this->returnArray($sql, 1);
          $result = array_merge($answerQuiz, $answerSelfTest);
          $total = sizeof($result);
          for($i=0;$i<$total; $i++){
               if($result[$i][0]=="A") $a++;
               if($result[$i][0]=="B") $b++;
               if($result[$i][0]=="C") $c++;
               if($result[$i][0]=="D") $d++;
               if($result[$i][0]=="E") $e++;
          }
          $x = "<hr size=1 class=hrbar><br>\n";
          $x .= "<table width=95% border=0 cellpadding=2 cellspacing=1 align=center>\n";
          $x .= "<tr><td colspan=3>$qbank_attempts: $total</td></tr>\n";
          $x .= "<tr><td colspan=3><br></td></tr>\n";
          if($total > 0){
               $x .= "<tr>\n";
               $x .= "<td align=center width=30%><b>$qbank_ans</b></td>\n";
               $x .= "<td align=center width=30%><b>$gradebook_attempts</b></td>\n";
               $x .= "<td align=center width=40%><b>$qbank_statistics</b></td>\n";
               $x .= "</tr>\n";
               if($this->choiceA<>"") $x .= $this->returnQuestionStat("A", $a, $total);
               if($this->choiceB<>"") $x .= $this->returnQuestionStat("B", $b, $total);
               if($this->choiceC<>"") $x .= $this->returnQuestionStat("C", $c, $total);
               if($this->choiceD<>"") $x .= $this->returnQuestionStat("D", $d, $total);
               if($this->choiceE<>"") $x .= $this->returnQuestionStat("E", $e, $total);
          }
          $x .= "</table>\n";
          return $x;
     }

     function returnSelectCategory($title){
          global $button_select;
          $sql = "SELECT category FROM question GROUP BY category ORDER BY category";
          $category = $this->returnArray($sql,1);
          $x .= "<select onChange='this.form.$title.value=this.value;this.options[0].selected=true;' class='inputfield'>\n";
          $x .= "<option value=\"\">- $button_select -</option>\n";
          for($i=0;$i<sizeof($category); $i++){
               $cat = $category[$i][0];
               $x .= "<option value=\"$cat\">$cat</option>\n";
          }
          $x .= "</select>\n";
          return $x;
     }

     function returnCategoryMenu($title){
          global $button_select, $links_categories;
          $sql = "SELECT category FROM question WHERE qtype='0' GROUP BY category ORDER BY category";
          $category = $this->returnArray($sql,1);
          $x = "<select name=category onChange='JavaScript:this.form.submit()' class='inputfield'>\n";
          $x .= "<option value=\"\">-- $links_categories --</option>\n";
          for($i=0;$i<sizeof($category); $i++){
               $cat = $category[$i][0];
               $x .= ($title==$cat) ? "<option value=\"$cat\" selected>$cat</option>\n" : "<option value=\"$cat\">$cat</option>\n";
          }
          $x .= "</select>\n";
          return $x;
     }


//YuEn: to be deleted
     function displayImage($image_file, $thresholdVal)
     {
          global $eclass_root, $image_path;
          $image_file_server=$eclass_root .$image_file;
          if (file_exists(
$image_file_server) ){
               $size = GetImageSize($image_file_server);
               if ($size[0]>$thresholdVal) {     $thisImageWidth=$thresholdVal;     $magImage=TRUE;     } else {     $thisImageWidth=$size[0];     }

               $x = "<img src=\"$image_file\" border=0 width=$thisImageWidth align=left hspace=10 vspace=0>";
               if ($magImage) {     $x .="<a href='#' onClick=\"newImageWindow('$image_file',$size[0],$size[1]);return false;\"><img src=$image_path/zoom.gif width=18 height=18 border=0></a>";     }
          } else { $x = "&nbsp;"
; }
          return $x;
     }

     function getFilePath($str){
          global $admin_url_path;

          $x = "";
          if($str=='')
               return "";
          $files = split("\:",stripslashes($str));
          for($i=0; $i<count($files); $i++){
               $Title = basename($files[$i]);
               $sLen = strlen($files[$i])- strlen($Title);
               $VirPath = trim(substr($files[$i], 0, $sLen));
               $Title = trim($Title);
               if ($VirPath!="" && $VirPath!="/")
                    $VirPath = "VirPath='".$VirPath."'";
               else
                    $VirPath = "VirPath is NULL";
               $sql = "SELECT CONCAT(ifnull(Location,''), '/', Title) FROM eclass_file WHERE $VirPath AND Title='$Title' AND Category=5 AND IsDir<>1 ";
               $row = $this->returnArray($sql, 1);
               $x .= "|".$admin_url_path.$row[0][0];
          }
          return $x;
     }

     function genPercentageBar($per_now) {
          global $image_path;

          /*  100% = 150pixels  */
          $ratio = 1.5;
          $per_color = round($per_now * $ratio);
          $per_space = 100*$ratio - $per_color;

          $x = "<table width='175' border='0' cellspacing='0' cellpadding='0'>\n";
          $x .= "<tr><td width='100' class='bordercolor'>\n";
          $x .= "<table width='150' border='0' cellspacing='1' cellpadding='0'>\n";
          $x .= "<tr>\n";
          if ($per_now==0)
               $x .= "<td width=".$per_space." bgcolor=white><img src='$image_path/space.gif' width='".$per_space."' height='11' border='0'></td>\n";
          elseif ($per_now==100)
               $x .= "<td width=$per_now><img src='$image_path/bar_100.gif' width='".$per_color."' height='11' border='0'></td>\n";
          else
               $x .= "<td width=$per_now><img src='$image_path/bar_100.gif' width='".$per_color."' height='11' border='0'></td>\n <td width=".$per_space." bgcolor=white><img src='$image_path/space.gif' width='".$per_space."' height='10' border='0'></td>\n";
          $x .= "</tr></table></td><td width='45' align='right'>&nbsp;$per_now%</td>\n";
          $x .= "</tr></table>\n";

          return $x;
     }

     function tidyUp($str) {
          if (!strstr($str, "[[")) {
               $ff = ($this->importFile!="") ? "in file '".$this->importFile."'" : "";
               echo "wrong input at question ".$this->que_no." $ff : [[".$str;
               die();
          }
          $tmpStr = explode("[[", $str);
          return array(trim($tmpStr[0]), trim($tmpStr[1]));          /* main content & HTML + files */
     }

     function splitHTML_media($str) {
          $strtmp = split("\,", $str);
          $isHTML = (strtoupper($strtmp[0])=="HTML") ? 1 : 0;
          $media = "";
          $myPath = ($this->path_base!="") ? $this->name_prefix.$this->media_path."/" : "/";
          for ($i=$isHTML; $i<sizeof($strtmp); $i++) {
               if (trim($strtmp[$i])!="") {
                    $file = trim($strtmp[$i]);
                    $media .= ($media!="") ? " : ".$myPath.$file : $myPath.$file;
               }
          }
          $media = str_replace("//", "/", $media);
          return array($isHTML, $media);
     }

     function getCategoryPublic($question) {
          $category = "";
          unset($status);
          if (is_integer($pos=strpos(strtoupper($question), "CATEGORY="))) {
               $pos_n = strpos($question, "\n", $pos);
               $category = trim(substr($question, $pos+9, $pos_n-$pos-9));
               $question = trim(substr($question, $pos_n+1));
          }
          if (is_integer($pos=strpos(strtoupper($question), "PUBLIC="))) {
               $pos_n = strpos($question, "\n", $pos);
               $public = strtoupper(trim(substr($question, $pos+7, $pos_n-$pos-7)));
               $status = ($public=="YES" || $public=="TRUE" || $public==1) ? 1 : 0;
               $question = trim(substr($question, $pos_n+1));
          }
          if ($category=="" && $this->category_set==""){
               $ff = ($this->importFile!="") ? "in file '".$this->importFile."'" : "";
               die("Sorry, import failed at question ".$this->que_no." $ff - missing category");
          }
          if ($category=="")
               $category = $this->category_set;
          elseif ($this->category_set=="")
               $this->category_set = $category;
          if (!isset($status))
               $status = $this->status_set;

          return array($question, $category, $status);
     }

     function importFromText($data) {
          $this->que_no = 0;
          $data = split("\###",stripslashes(htmlspecialchars($data)));
          for($i=0; $i<sizeof($data)-1; $i++){
               $this->que_no ++ ;
               $xStr = explode("]]",trim($data[$i]));

               // terminate if the total number is not match
               $lastnum=count($xStr);
               if ($lastnum<>11) {
                    $ff = ($this->importFile!="") ? "in file '".$this->importFile."'" : "";
                    die("Sorry, wrong import format at question ".$this->que_no." $ff - missing field(s)");
               }

               $tmpArr = $this->tidyUp($xStr[0]);
               $question = $tmpArr[0];
               $tmpArrB = $this->getCategoryPublic($question);
               $question = $tmpArrB[0];
               $category = $tmpArrB[1];
               $status = $tmpArrB[2];

               $question_addon =
$tmpArr[1];
               $tmpArr = $this->tidyUp($xStr[1]);
               $answer = $tmpArr[0];

               for ($j=0; $j<5; $j++) {
                    $tmpArr = $this->tidyUp($xStr[2+$j]);
                    ${"choice".chr(65+$j)} = $tmpArr[0];
                    ${"choice".chr(65+$j)."_addon"} =
$tmpArr[1];
               }

               $tmpArr = $this->tidyUp($xStr[7]);
               $hint = $tmpArr[0];
               $hint_addon =
$tmpArr[1];
               $tmpArr = $this->tidyUp($xStr[8]);
               $explanation = $tmpArr[0];
               $explanation_addon =
$tmpArr[1];
               $tmpArr = $this->tidyUp($xStr[9]);
               $reference = $tmpArr[0];
               $reference_addon =
$tmpArr[1];

               //$status = (strtoupper($public)=="PUBLIC") ? 1 : 0;
               $tmpArr = $this->splitHTML_media($question_addon);
               $question_html_opt = ($tmpArr[0]==1) ? 1 : "";
               $question_attach = $tmpArr[1];

               for ($j=0; $j<5; $j++) {
                    $tmpArr = $this->splitHTML_media(${"choice".chr(65+$j)."_addon"});
                    ${"choice".chr(65+$j)."_html_opt"} = ($tmpArr[0]==1) ? 1 : "";
                    ${"choice".chr(65+$j)."_attach"} = $tmpArr[1];
               }

               $tmpArr = $this->splitHTML_media($hint_addon);
               $hint_html_opt = ($tmpArr[0]==1) ? 1 : "";
               $hint_attach = $tmpArr[1];
               $tmpArr = $this->splitHTML_media($explanation_addon);
               $explanation_html_opt = ($tmpArr[0]==1) ? 1 : "";
               $explanation_attach = $tmpArr[1];
               $tmpArr = $this->splitHTML_media($reference_addon);
               $reference_html_opt = ($tmpArr[0]==1) ? 1 : "";
               $reference_attach = $tmpArr[1];

               $separator = "===@@@===";
               $x  = addslashes($question).$separator.$question_html_opt.$separator.$question_attach;
               $x .= $separator.addslashes($choiceA).$separator.$choiceA_html_opt.$separator.$choiceA_attach;
               $x .= $separator.addslashes($choiceB).$separator.$choiceB_html_opt.$separator.$choiceB_attach;
               $x .= $separator.addslashes($choiceC).$separator.$choiceC_html_opt.$separator.$choiceC_attach;
               $x .= $separator.addslashes($choiceD).$separator.$choiceD_html_opt.$separator.$choiceD_attach;
               $x .= $separator.addslashes($choiceE).$separator.$choiceE_html_opt.$separator.$choiceE_attach;
               $question = $x;
               $hint = addslashes($hint).$separator.$hint_html_opt.$separator.$hint_attach;
               $explanation = addslashes($explanation).$separator.$explanation_html_opt.$separator.$explanation_attach;
               $reference = addslashes($reference).$separator.$reference_html_opt.$separator.$reference_attach;

               $fieldname  = "category, question, answer, hint, explanation, ";
               $fieldname .= "reference, inputdate, modified, qtype, status";
               $fieldvalue  = "'$category', '$question', '$answer', '$hint', '$explanation', ";
               $fieldvalue .= "'$reference', now(), now(), '0', '$status'";
               $sql="INSERT INTO question ($fieldname) values ($fieldvalue)";

               $this->db_db_query($sql);
          }
          return true;
     }

     // recursively read files (DOC, TXT) and import to MC
     function importFromFiles($basedir) {
          global $ck_user_email, $ck_user_id, $ck_memberType;

          $ThisDir=array();                         //Create array for current directories contents
          if (!file_exists($basedir))
               return;
          chdir($basedir);                         //switch to the directory we wish to scan
          $current=getcwd();
          $handle=opendir(".");                    //open current directory for reading

          while ($file = readdir($handle)) {
               //Don't add special directories '..' or '.' to the list
               $ext = $this->getFileExt($file);
               $extUpper = strtoupper($ext);
               if (($file!='..') && ($file!='.')) {
                    $source = $current.'/'.$file;
                    if (is_dir($file)) {
                         array_push($ThisDir,$source);   //build an array of contents for this directory
                    } elseif ($extUpper=="DOC") {
                         //debug("DOC::".$current);
                    } elseif ($extUpper=="TXT") {
                         //debug("TXT::".getcwd());
                         $dataArr = file($source);
                         $data = implode("", $dataArr);
                         $this->media_path = str_replace($this->path_base, '', $current);
                         if (strstr($data, "###") && strstr($data, "[[")) {
                              $this->importFile = $file;
                              $this->importFromText($data);
                         }
                    }
               }
          }
          closedir($handle);
          //Loop through each directory,  run RecurseDir function on each one
          for ($i=0; $i<sizeof($ThisDir); $i++) {
               $this->importFromFiles($ThisDir[$i], $fid);
          }
          //make sure we go back to our origin
          chdir($basedir);
          //return $AllDirectories;
     }

     function getFileExt($file){
          $file = basename($file);
          $x = strrpos($file,".");
          return substr($file, $x+1);
     }

}
?>
<?
// *******************************************************************************
// Copyright Notice : COPYRIGHT (C) 1999 BY ARCHIWEB.COM.HK
/*
 Project : eClass 2.5

 Author: hpchan
 Abstracts :



 Amendment History:

 Date By     Description
 -------     ------------- ----------------------------------------------------
 20011112     Hide the Category of the question in Exercise/Exam/Survey
                 Line : 70      function displayTest
                 Line : 341      function questionResult

 20011113     Zoom Function of Assessment
                 Line : 442 (end of file)      function displayImage($image_file, $thresholdVal) added
                 Line : 113-117           function displayQuestionTitle
                 Line : 138-141           function displayQuestionChoice


*/
// *******************************************************************************
?>