<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libqb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libfilesystem.php");

intranet_opendb();

$lq = new libqb();
$lo = new libfilesystem();
$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
     header ("Location: index.php");
     exit();
}
if ($lg->hasAdminQB($UserID))
{

$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none"){          # import failed
        header("Location: import.php?GroupID=$GroupID&type=$type");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lo->file_read_csv($filepath);
                array_shift($data);                   # drop the title bar
        }


        $new_order = $lq->newDisplayOrder($GroupID,$type);
        $delimiter = "";
        $fields_value = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($engName,$chiName,$dorder) = $data[$i];
             if ($dorder == "")
             {
                 $dorder = $new_order+1;
                 $new_order = $dorder;
             }
             else
             {
                 $new_order = $dorder;
             }
             $fields_value .= "$delimiter ('$engName','$chiName','$dorder','$GroupID')";
             $delimiter = ",";
        }

        $sql = "INSERT IGNORE INTO ".$lq->settingTable[$type]." (EngName,ChiName,DisplayOrder,RelatedGroupID) VALUES $fields_value";
        $lq->db_db_query($sql);
        header("Location: settings_cat.php?GroupID=$GroupID");
}

}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();

?>