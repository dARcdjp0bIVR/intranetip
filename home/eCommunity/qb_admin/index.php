<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libqb.php");
include_once("lib-question-mc.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
if ($type=="") $type = 0;
include_once("menu.php");
include_once("../tab.php");
$body_tags = "bgcolor=#FFFFFF text=#000000 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0";
include_once("../../../templates/fileheader.php");

$lgrouping = new libgrouping();
$lg = new libgroup($GroupID);

if ($lg->hasAdminQB($UserID))
{

$mc = new question_mc();


$lq = new libqb();
$qtypeSelect = $lq->returnSelectQuestionType("name=type onChange=\"this.form.submit()\"",$type);
$lvlSelect = $lq->returnSelectLevel($GroupID,"name=level",$level,1);
$catSelect = $lq->returnSelectCategory($GroupID,"name=category",$category,1);
$diffSelect = $lq->returnSelectDifficulty($GroupID,"name=difficulty",$difficulty,1);
$langSelect = $lq->returnSelectLang("name=qlang",$qlang);
$ownerSelect = $lq->returnSelectOwner($GroupID,"name=owner",$owner,1);
$statusSelect = $lq->returnSelectStatus("name=status",$status);

//$go_button = "<input type=submit value='Go' class=button>";

// TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$qb_name = $intranet_session_language=="en"? "EngName": "ChiName";
$lang_suf = ($intranet_session_language == "b5"? "Chi":"Eng");
$username_field = getNameFieldByLang("e.");
if($field=="") $field = 4;
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Question$lang_suf", $username_field,"a.RecordStatus", "a.DownloadedCount","a.DateModified");



$sql = "SELECT
              a.QuestionID, a.QuestionCode, IF(a.Lang=1 OR a.Lang=3,a.QuestionEng,'-'),IF(a.Lang=2 OR a.Lang=3,a.QuestionChi,'-'),b.$qb_name, c.$qb_name, d.$qb_name, a.Score,
              CONCAT(
                     IF (a.Question$lang_suf is NULL OR a.Question$lang_suf = ''
                         ,'-===###==='
                         ,CONCAT(a.Question$lang_suf,'===###===')
                         )
                     ,IF(a.BulletinID IS NULL OR a.BulletinID = ''
                         ,' '
                         ,CONCAT('<a href=javascript:open_bulletin_msg('
                                   ,a.BulletinID
                                   ,')>".bulletinIcon("Go to bulletin")."</a>'
                         )
                     )
                     ,IF(LOCATE(';$UserID;',a.ReadFlag)=0
                         , '<img src=$image_path/new.gif border=0 hspace=2 align=absmiddle>'
                         , '')
                     ),
              IF(a.Lang = 1,CONCAT('<a title=\"$i_QB_ViewOriginalEnglish\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',1)>$i_QB_EngVer</a>'),
              IF(a.Lang=2,CONCAT('<a title=\"$i_QB_ViewOriginalChinese\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',0)>$i_QB_ChiVer</a>'),
              CONCAT('<a title=\"$i_QB_ViewOriginalEnglish\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',1)>$i_QB_EngVer</a><br><a title=\"$i_QB_ViewOriginalChinese\" class=functionlink href=javascript:qb_open_source_document(',a.QuestionID,',0)>$i_QB_ChiVer</a>')
              )),
              IF(e.UserID IS NULL,CONCAT('<I>',a.OwnerName,'</I>'),$username_field),
              IF(a.RecordStatus=0,'$i_QB_Pending', IF (a.RecordStatus=1,'$i_QB_Approved','$i_QB_Rejected') ),
              a.DownloadedCount, a.DateModified,
              CONCAT('<input type=checkbox name=QuestionID[] value=',a.QuestionID,'>')
        FROM QB_QUESTION as a LEFT OUTER JOIN QB_LEVEL as b ON a.LevelID = b.UniqueID
             LEFT OUTER JOIN QB_CATEGORY as c ON a.CategoryID = c.UniqueID
             LEFT OUTER JOIN QB_DIFFICULTY as d ON a.DifficultyID = d.UniqueID
             LEFT OUTER JOIN INTRANET_USER as e ON a.OwnerIntranetID = e.UserID
        WHERE a.RelatedGroupID = $GroupID
        AND a.QuestionType = $type
        AND (b.$qb_name LIKE '%$keyword%' OR c.$qb_name LIKE '%$keyword%'
             OR d.$qb_name LIKE '%$keyword%' OR a.QuestionEng LIKE '%$keyword%'
             OR a.QuestionChi LIKE '%$keyword%' OR a.QuestionCode LIKE '%$keyword%'
             OR $username_field LIKE '%$keyword%'
             )
        ";
if ($level != 0 && $level != "")
{
    $sql .= " AND b.UniqueID = $level";
}
if ($category != 0 && $category != "")
{
    $sql .= " AND c.UniqueID = $category";
}
if ($difficulty != 0 && $difficulty != "")
{
    $sql .= " AND d.UniqueID = $difficulty";
}
if ($owner != 0 && $owner != "")
{
    $sql .= " AND a.OwnerIntranetID = $owner";
}
if ($qlang != 0 && $qlang != "")
{
    if ($qlang == 3) $sql .= " AND a.Lang = 3";
    if ($qlang == 2) $sql .= " AND (a.Lang = 3 OR a.Lang = 2)";
    if ($qlang == 1) $sql .= " AND (a.Lang = 3 OR a.Lang = 1)";
}
if ($status != 0 && $status != "")
{
    $sql .= " AND a.RecordStatus = ". ($status-1);
}

// TABLE INFO
$li->sql = $sql;
//$li->title = $qbank_mc;
$li->no_msg = ($keyword=="") ? $i_no_record_exists_msg : $i_no_search_result_msg;
$li->page_size =
($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage
;
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = 6;


//************************************ PREPARE HTML CODES ************************************

// TABLE COLUMN
$li->column_list .= "<td class=qb_tableheader height='25' width='1' align='center'>#</td>\n";
$li->column_list .= "<td class=qb_tableheader width='31%'>".$li->column(0, $i_QB_Question)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='10%'>$i_QB_LangVer</td>\n";
$li->column_list .= "<td class=qb_tableheader width='13%'>".$li->column(1, $i_QB_Owner)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='8%'>".$li->column(2, $i_QB_Status)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='13%'>".$li->column(3, $i_QB_DownloadedCount)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='25%'>".$li->column(4, $i_QB_LastModified)."</td>\n";
$li->column_list .= "<td class=qb_tableheader width='1'>".$li->check("QuestionID[]")."</td>\n";

$li->column_array = array(0,0,0,0,0,0,0);

# From tab.php
$grp_navigation .= "";

$groupSelect = $lq->returnSelectAdminGroup("name=GroupID onChange=this.form.submit()",$GroupID);
$go_button = "<input type=image src=$intranet_httppath$image_go alt='$button_go'>";

echo $JavaScripts;
include_once("../tooltab.php");
?>
<script language="Javascript" src='<?=$intranet_httppath?>/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     //isMenu = true;
     isToolTip = true;
     </script>

<form name="form1" method="get">
<div id="ToolTip"></div>

<table width="791" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="766">

      <table width="766" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_questionbank?></td>
          <td width="8"><img src="<?=$image_path?>/groupinfo/popup_barleft.gif"></td>
          <td width="100%" class=popup_topcell><font color="#FFFFFF"><?=$lg->Title?></font></td>
          <td width="20"><img src="<?=$image_path?>/groupinfo/popup_barright3.gif"></td>
        </tr>
      </table>
      <?=$header_tool_qb?>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg1.gif">
      <tr>
            <td width="1"><img src="<?=$image_path?>/spacer.gif" width="1" height="1">
            </td>

      <td colspan=3 align=left>
      <?=$groupSelect?>
      </td></tr>
          <tr>
            <td colspan=4 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="<?=$image_path?>/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
        <tr>
          <td width="16">&nbsp;</td>
            <td width="143" class="qb_on_tab"><a class=qb_on_font href=index.php?GroupID=<?=$GroupID?>><?=$i_QB_QuestionAdmin?></a></td>
            <td width="143" class="qb_off_tab"><a class=qb_off_font href=settings.php?GroupID=<?=$GroupID?>><?=$i_QB_Settings?></a></td>
          <td width="464">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4"><img src="<?=$image_path?>/groupinfo/sbqb_frametop.gif"></td>
        </tr>
      </table>
      <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510" align="right" valign="top">&nbsp;</td>
          <td width="240">&nbsp;</td>
        </tr>
        <tr>
          <td width="16">&nbsp;</td>
          <td width="510">
            <table width=100% cellpadding=1 cellspacing=1>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_QuestionType?>:</td><td width="367" align="left" valign="top"><?=$qtypeSelect?></td></tr>
              <tr><td colspan=2>&nbsp;</td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Level?>:</td><td width="367" align="left" valign="top"><?=$lvlSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Category?>:</td><td width="367" align="left" valign="top"><?=$catSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Difficulty?>:</td><td width="367" align="left" valign="top"><?=$diffSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_LangSelect?>:</td><td width="367" align="left" valign="top"><?=$langSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Owner?>:</td><td width="367" align="left" valign="top"><?=$ownerSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"><?=$i_QB_Status?>:</td><td width="367" align="left" valign="top"><?=$statusSelect?></td></tr>
              <tr><td width=143 align="right" valign="top"></td><td width="367" align="left" valign="top"><?=$go_button?></td></tr>
            </table>
          </td>
          <td width="240">
            <input name="keyword" type="text" size="15">
            <input type=image src="<?=$image_search?>" border="0">
          </td>
        </tr>
      </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_tbbg2.gif" class="body">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="15" align="right"><img src="<?=$image_path?>/groupinfo/sbqb_cornerup1.gif"></td>
            <td width="105" align="center" valign="middle" bgcolor="#BEDAEF" class="h1"><?=$i_QB_FileList?> :
              </td>
            <td width="21"><img src="<?=$image_path?>/groupinfo/sbqb_cornerup2.gif"></td>
            <td width="573">&nbsp;</td>
            <td width="29">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6"><img src="<?=$image_path?>/groupinfo/sbqb_inframetop.gif"></td>
          </tr>
        </table>
        <table width="766" border="0" cellpadding="0" cellspacing="0" background="<?=$image_path?>/groupinfo/sbqb_inframetbbg.gif">
          <tr>
            <td width="23">&nbsp;</td>
            <td width="714" align="center">
<?php

// DISPLAY TOOLBAR: new, import, export
echo $li->displayFunctionbar("1", "1", $toolbar_admin, $functionbar_admin);

// DISPLAY TABLE LIST CONTENT
echo $li->display();
?>


            </td>
            <td width="29">&nbsp;</td>
          </tr>
        </table>
        <table width="766" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_inframebottom.gif"></td>
          </tr>
          <tr>
            <td><img src="<?=$image_path?>/groupinfo/sbqb_framebottom.gif"></td>
          </tr>
        </table>

    </td>
  </tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=rejmsg value="">
</form>


<?php
include ("../../../templates/filefooter.php");
}
else   # Not Admin, close window
{
         header ("Location: ../close.php");
}
//foot(menuText(1));
//echo menuText(1);
intranet_closedb();
?>