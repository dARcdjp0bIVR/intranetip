<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libqb.php");
include_once("../../../includes/libgroup.php");

intranet_opendb();

$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
    header("Location: ../close.php");
}
if ($lg->hasAdminQB($UserID))
{
$lq = new libqb();
$remove_list = implode(",",$uniqueID);

if (in_array($target,$uniqueID))
{
    header("Location: settings_cat.php?signal=2&GroupID=$GroupID");
    exit();
}

$IDLink = $lq->questionLink[$type];
if ($target != 0)
{
    $sql = "UPDATE QB_QUESTION SET $IDLink = $target WHERE $IDLink IN ($remove_list)";
}
else
{
    $sql = "DELETE FROM QB_QUESTION WHERE $IDLink IN ($remove_list)";
}
$lq->db_db_query($sql);

$sql = "DELETE FROM ".$lq->settingTable[$type]." WHERE UniqueID IN ($remove_list) AND RelatedGroupID = $GroupID";
$lq->db_db_query($sql);

header("Location: settings_cat.php?signal=1&GroupID=$GroupID");
}
else
{
    header ("Location: ../close.php");
}
?>