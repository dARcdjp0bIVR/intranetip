<?php

/* top-right menu */
function menuIcon($type) {
     global $image_path, $qbank_mc, $qbank_tf, $qbank_sq, $qbank_fitb, $qbank_match;

     $menubar = "";

     $menubar .= ($type==1) ? "<img src='$image_path/icon/assessment/multi.gif' alt='".$qbank_mc."' name='MC' hspace='5' border='0'>\n" : "<a href='../multiplechoice/index.php' onMouseOut='MM_swapImgRestore()' onMouseOver='MM_swapImage(\"MC\",\"\",\"$image_path/icon/assessment/multi_on.gif\",1)'>\n <img src='$image_path/icon/assessment/multi_off.gif' alt='".$qbank_mc."' name='MC' hspace='5' border='0'></a>\n";

     $menubar .= ($type==2) ? "<img src='$image_path/icon/assessment/tnfalse.gif' alt='$qbank_tf' name='TF' hspace='5' border='0'>\n" : "<a href='../truefalse/index.php' onMouseOut='MM_swapImgRestore()' onMouseOver='MM_swapImage(\"TF\",\"\",\"$image_path/icon/assessment/tnfalse_on.gif\",1)'>\n  <img src='$image_path/icon/assessment/tnfalse_off.gif' alt='$qbank_tf' name='TF' hspace='5' border='0'></a>\n";

     $menubar .= ($type==3) ? "<img src='$image_path/icon/assessment/short_question.gif' alt='$qbank_sq' name='SQ' hspace='5' border='0'>\n" : "<a href='../shortquestion/index.php' onMouseOut='MM_swapImgRestore()' onMouseOver='MM_swapImage(\"SQ\",\"\",\"$image_path/icon/assessment/short_question_on.gif\",1)'>\n <img src='$image_path/icon/assessment/short_question_off.gif' alt='$qbank_sq' name='SQ' hspace='5' border='0'></a>\n";

     $menubar .= ($type==4) ? "<img src='$image_path/icon/assessment/fitb.gif' alt='$qbank_fitb' name='FITB' hspace='5' border='0'>\n" : "<a href='../fillintheblank/index.php' onMouseOut='MM_swapImgRestore()' onMouseOver='MM_swapImage(\"FITB\",\"\",\"$image_path/icon/assessment/fitb_on.gif\",1)'>\n <img src='$image_path/icon/assessment/fitb_off.gif' alt='$qbank_fitb' name='FITB' hspace='5' border='0'></a>\n";

     $menubar .= ($type==5) ? "<img src='$image_path/icon/assessment/matching.gif' alt='$qbank_match' name='MAT' hspace='5' border='0'>\n" : "<a href='../matching/index.php' onMouseOut='MM_swapImgRestore()' onMouseOver='MM_swapImage(\"MAT\",\"\",\"$image_path/icon/assessment/matching_on.gif\",1)'>\n <img src='$image_path/icon/assessment/matching_off.gif' alt='$qbank_match' name='MAT' hspace='5' border='0'></a>\n";

     $pimage = "";
     if ($type!=1) $pimage .= "'$image_path/icon/assessment/multi_on.gif'";
     if ($type!=2) $pimage .= ($pimage!="") ? ", '$image_path/icon/assessment/tnfalse_on.gif'" : "'$image_path/icon/assessment/tnfalse_on.gif'";
     if ($type!=3) $pimage .= ", '$image_path/icon/assessment/short_question_on.gif'";
     if ($type!=4) $pimage .= ", '$image_path/icon/assessment/fitb_on.gif'";
     if ($type!=5) $pimage .= ", '$image_path/icon/assessment/matching_on.gif'";

     $menubar .= "<Script Language='JavaScript'>\n";
     $menubar .= "MM_preloadImages($pimage);\n";
     $menubar .= "</Script>\n";

     return $menubar;
}

/* bottom-left menu */
function menuText($type) {
     global $qbank_mc, $qbank_tf, $qbank_sq, $qbank_fitb, $qbank_match;

     $link1 = ($type==1) ? "" : "../multiplechoice/index.php";
     $link2 = ($type==2) ? "" : "../truefalse/index.php";
     $link3 = ($type==3) ? "" : "../shortquestion/index.php";
     $link4 = ($type==4) ? "" : "../fillintheblank/index.php";
     $link5 = ($type==5) ? "" : "../matching/index.php";

     $bottommenu = getBottomMenu($qbank_mc, "$link1", $qbank_tf, "$link2", $qbank_sq, "$link3", $qbank_fitb, "$link4", $qbank_match, "$link5");

     return $bottommenu;
}


function undo_htmlspecialchars($string)
{
         $string = str_replace("&amp;", "&", $string);
         $string = str_replace("&quot;", "\"", $string);
         $string = str_replace("&lt;", "<", $string);
         $string = str_replace("&gt;", ">", $string);
         $string = str_replace("<form>", "&lt;form&gt;", $string);
         $string = str_replace("</form>", "&lt;/form&gt;", $string);
         $string = str_replace("<input", "&lt;input", $string);
         $string = str_replace("<textarea>", "&lt;textarea&gt;", $string);
         $string = str_replace("</textarea>", "&lt;/textarea&gt;", $string);
         return $string;
}


function getQuestion($question, $answer)
{
         $separator = "===@@@===";
         $x = "";
         $y = "";
         $tmpArr = split($separator, $question);
         $tmpAns = split($separator, $answer);
         $sizeArr = max((sizeof($tmpArr)-2), sizeof($tmpAns));
         $question_html_opt = $tmpArr[sizeof($tmpArr)-2];

         for ($i=0; $i<$sizeArr; $i++)
         {
              $y .= " ".$tmpArr[$i];
              $x .= ($question_html_opt==1) ? " ".undo_htmlspecialchars($tmpArr[$i]) : " ".$tmpArr[$i];
              $ans = $tmpAns[$i];
              $y .= " ".$ans;
              $x .= " <u>".$ans."</u>";
         }
         return array($x, $y);
}


/* TOOLBAR : New, Import, Export */
$toolbar = "";
$toolbar .= "<a href='javascript:checkNew(\"import.php?GroupID=$GroupID\")'>\n"
              . ImportIcon2().$button_import."</a>\n";
$toolbar .= "<img src='$image_path/space.gif' width='2' height='1' border='0'>\n";
$toolbar .= "<a href=javascript:checkPost(document.form1,'export.php')>"
              .ExportIcon2().$button_export."</a>\n";

$importlink = "<a href='javascript:checkNew(\"import.php?GroupID=$GroupID\")'>\n"
              . ImportIcon2().$button_import."</a>\n";
$exportlink = "<a href=javascript:checkPost(document.form1,'export.php')>"
              .ExportIcon2().$button_export."</a>\n";
$spacer = "<img src='$image_path/space.gif' width='2' height='1' border='0'>\n";

$action_edit = "<input type=image src=\"$image_edit\" name='edit' onClick=\"checkEdit(this.form,'QuestionID[]','edit.php');return false;\">\n";
$action_delete = "<input type=image src=\"$image_delete\" onClick=\"checkRemove(this.form,'QuestionID[]','remove.php');return false;\">\n";

$action_authorize = "";
if ($qb_record_score)
{
    $action_authorize = $i_QB_AwardPts;
    $score_available = $qb_score_typearray[$type];
    $score_select = "<SELECT name=pts>\n";
    for ($i=0; $i<sizeof($score_available); $i++)
    {
         $score = $score_available[$i];
         $score_select .= "<OPTION value=$score>$score $i_QB_Pts</OPTION>\n";
    }
    $score_select .= "</SELECT>\n";
    $action_authorize .= " $score_select";
}
$action_authorize .= "<input type=image src=\"$intranet_httppath$image_authorize\" name='authorize' onClick=\"checkAlert(this.form,'QuestionID[]','authorize.php','$i_QB_Alert_Authorize');return false;\">\n";
$action_forbid = "<input type=image src=\"$intranet_httppath$image_forbid\" name='forbid' onClick=\"checkAlert(this.form,'QuestionID[]','forbid.php','$i_QB_Alert_Forbid');return false;\">\n";
$action_reject = "<input type=image src=\"$intranet_httppath$image_reject\" name='reject' onClick=\"checkPrompt(this.form,'QuestionID[]','reject.php','$i_QB_Prompt_Reject',this.form.rejmsg,'');return false;\">\n";

$markAllReadLink = "<a href=\"javascript:AlertPost(document.form1,'markAllRead.php','$i_QB_ConfirmAllRead')\">"
                     .$i_QB_MarkAllRead."</a>\n";
$markAllReadDownloadLink = "<a href=\"javascript:AlertPost(document.form1,'markAllReadDownload.php','$i_QB_ConfirmAllDownload')\">"
                     .$i_QB_MarkAllReadDownload."</a>\n";

$action_addToBucket = "<a href=\"javascript:checkAlert(document.form1,'QuestionID[]','addtobucket.php','$i_QB_ConfirmAddToBucket')\">"
                        .$i_QB_AddToBucket."</a>\n";
$action_addAllToBucket = "<a href=\"javascript:AlertPost(document.form1,'addalltobucket.php','$i_QB_ConfirmAddAllToBucket')\">"
                           .$i_QB_AddAllToBucket."</a>\n";
$action_clearBucket = "<a href=\"javascript:AlertPost(document.form1,'clearbucket.php','$i_QB_ConfirmClearBucket')\">"
                        .$i_QB_ClearBucket."</a>\n";

$view_all_link = "<a href=\"javascript:viewAll($GroupID)\">$i_QB_ViewAll</a>\n";

$toolbar_sharing = "$exportlink $markAllReadLink $markAllReadDownloadLink $action_addToBucket $action_addAllToBucket $action_clearBucket";
$functionbar_sharing = "";

$toolbar_myquestion = "$importlink";
$functionbar_myquestion = "$action_edit$spacer$action_delete";

#$toolbar_admin = "$view_all_link";
$functionbar_admin = "$action_forbid$spacer$action_reject$spacer$action_edit$spacer$action_delete<br>$action_authorize";

// By using different settings/styles, Netscape and IE can have a very similar appearance.

if ( $NS4 ) {
     $lenSearch = 11;
     $posm = '';
} else {
     $lenSearch = 16;
     $posm = 'align="absmiddle"';
}

/* SEARCH AREA */
$searchbar  = "<input name='keyword' type='text' id='keyword' maxlength=256 class='inputfield' \n"
               . "value='".stripslashes($keyword)."' onFocus='this.form.pageNo.value=1;' size='".$lenSearch."'>\n"
               . "<input class='button' type='submit' value='".$button_search."'>\n";

$JavaScripts = "";
//$JavaScripts = "<script language='JavaScript' src='../../../includes/js/imgMenu.js'></script>\n";
//$JavaScripts .= "<script language='JavaScript' src='../../../includes/js/tooltip.js' type='text/javascript'></script>\n";
$JavaScripts .= "<style type='text/css'>\n";
$JavaScripts .= "#ToolTip{position:absolute; top: 0px; left: 0px; z-index:1; visibility:hidden;}\n";
$JavaScripts .= "</style>\n\n";
$JavaScripts .= "<script language='JavaScript'>\n";
$JavaScripts .= "isToolTip = true;\n";
$JavaScripts .= "function tipsNow(text)\n";
$JavaScripts .= "{\n";
$JavaScripts .= "if (document.readyState=='complete')\n";
$JavaScripts .= "  {\n";
$JavaScripts .= "     tt= \"<table border='0' cellspacing='0' cellpadding='1'>\";\n";
$JavaScripts .= "     tt += \"<tr><td class='tipborder'><table width='100%' border='0' cellspacing='0' cellpadding='3'>\";\n";
$JavaScripts .= "     tt += \"<tr><td class='tipbg' valign='top'><font size='-2'>\"+text+\"</font></td></tr>\";\n";
$JavaScripts .= "     tt += \"</table></td></tr></table>\";\n";
$JavaScripts .= "     showTip('ToolTip', tt);\n";
$JavaScripts .= "  }\n";
$JavaScripts .= "}\n";
$JavaScripts .= "function view(id){
\n";
$JavaScripts .= "     url='view.php?QuestionID='+id;
\n";
$JavaScripts .= "     newWindow(url,1);
\n";
$JavaScripts .= "}\n";
$JavaScripts .= "function viewAll(id){
\n";
$JavaScripts .= "     url='printall.php?GroupID='+id;
\n";
$JavaScripts .= "     newWindow(url,1);
\n";
$JavaScripts .= "}\n";
$JavaScripts .= "</script>\n";


?>