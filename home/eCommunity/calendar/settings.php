<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$linterface = new interface_html();
$legroup = new libegroup($GroupID);
$lu = new libgroup($GroupID);

$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || !(substr($legroup->AdminAccessRight,1,1) || $legroup->AdminAccessRight=='ALL'))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

$CurrentPage	= "Settings_CalendarSettings";
$MyGroupSelection= $legroup->getSelectAdminBasicInfo("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

### Title ###
$title = "
		<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' nowrap> <span class='contenttitle'>". $eComm['CalendarSettings'] ."</span></td>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";

$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$CalID = $legroup->CalID;

function selectionList($arr){
	for ($i=0; $i<sizeof($arr); $i++)
	{
		$rx .= "<option value=\"".$arr[$i][0]."\">".$arr[$i][1]."</option>\n";
	}
	return $rx;
}

$AdminUser = $legroup->getCalAdminUser();
if(!empty($AdminUser))
{

	$sql = "select UserID, CONCAT(EnglishName, ' (', ChineseName, ')') from INTRANET_USER where UserID IN ($AdminUser) ORDER BY EnglishName, ChineseName ";
	$row = $legroup->returnArray($sql, 2);
	$AdminSelectionList = selectionList($row);
}

# get group member with Read permission only
$sql = "
	select 
		a.UserID,
		CONCAT(b.EnglishName, ' (', b.ChineseName, ')')
	from 
		CALENDAR_CALENDAR_VIEWER as a
		left join INTRANET_USER as b on (b.UserID=a.UserID)
	where 
		a.CalID='$CalID'
		and a.Access='R'
		and b.EnglishName <>''
	ORDER BY 
		b.EnglishName, b.ChineseName
";
$row = $legroup->returnArray($sql, 2);
$TeachingStaffList = selectionList($row);

?>

<script language="javascript">
function checkform(obj){
	var target_obj = eval("obj.elements['target[]']");
	checkOption(target_obj);
	for(i=0; i<target_obj.length; i++)
	{
		target_obj.options[i].selected = true;
	}
}
</script>

<br />   

<form name="form1" method="post" action="settings_update.php">
<input type="hidden" name="GroupID" value="<?=$GroupID?>">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' ><?=$linterface->GET_SYS_MSG($msg,$xmsg);?></td>
</tr>

<? if(!$legroup->CalID) { ?>
	<tr>
		<td class="tabletext" align="center"><?=$eComm['NoCalendar']?></td>
	</tr>	
	<tr>
		<td>        
	                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	                <tr>
	                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
				<td align="center" colspan="2">
				<input type="hidden" name="createCal" value="1">
				<?= $linterface->GET_ACTION_BTN($eComm['CreateCalendar'], "submit") ?>
			</td>
			</tr>
		    </table>                                
		</td>
	</tr>
<? } else {?>
	
	<tr>
		<td>
	                <table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	                <tr> 
	                	<td valign="top">
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Group']?></span></td>
						<td class="tabletext"><?=$legroup->Title?></td>
					</tr>
					
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayAtGroupIndex']?> </span></td>
						<td class="tabletext">
							<input type="radio" name="CalDisplayIndex" value="1" id="display1" <?=$legroup->CalDisplayIndex?"checked":""?>> <label for="display1"><?=$i_general_yes?></label> 
							<input type="radio" name="CalDisplayIndex" value="0" id="display0" <?=$legroup->CalDisplayIndex?"":"checked"?>> <label for="display0"><?=$i_general_no?></label>
						</td>
					</tr>
					
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['Status']?> </span></td>
						<td class="tabletext">
							<input type="radio" name="CalPublicStatus" value="1" id="status1" <?=$legroup->CalPublicStatus?"checked":""?>> <label for="status1"><?=$eComm['Public']?></label> 
							<input type="radio" name="CalPublicStatus" value="0" id="status0" <?=$legroup->CalPublicStatus?"":"checked"?>> <label for="status0"><?=$eComm['Private']?></label>
						</td>
					</tr>
		
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eReportCard["AdminUser"]?> </span></td>
						<td class="tabletext">
							<table border="0" width="100%">
							<tr>
								<td valign="top">
									<select name="target[]" class='inputfield' size=7 multiple>
						        	<?=$AdminSelectionList?>
						                <? if(sizeof($AdminSelectionList)==0) {?>
						                <option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
						                <? } ?>
						        	</select>
					        	</td>
					        	<td align=center nowrap width="10%" valign="top">
					        		<?= $linterface->GET_BTN($eComm['AddTo'], "button", "checkOptionTransfer(this.form.elements['source[]'],this.form.elements['target[]']);return false;") ?>
					        		<br><br>
					        		<?= $linterface->GET_BTN($eComm['DeleteTo'], "button", "checkOptionTransfer(this.form.elements['target[]'],this.form.elements['source[]']);return false;") ?>
								</td>
								<td width="50%" valign="top">
							        	<select class='inputfield' name="source[]" size=7 multiple>
							        	<?=$TeachingStaffList?>
							                <? if(sizeof($TeachingStaffList)==0) {?>
							        	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
							                <? } ?>
							        	</select>
								</td>
								</tr>
								</table>
						</td>
					</tr>
					
					
					
				</table>
				</td>
							
	                </tr>
	                </table>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
	                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	                <tr>
	                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
				<td align="center" colspan="2">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "checkform(this.form);","submit2") ?>
		        <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
			</tr>
		    </table>                                
		</td>
	</tr>


<? 
print $linterface->FOCUS_ON_LOAD("form1.CalDisplayIndex[0]");
} ?>

</table>                        
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>