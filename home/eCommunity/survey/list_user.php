<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$SurveyID = IntegerSafe($SurveyID);

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);
if ($lsurvey->RecordType == 1)
{
    header("Location: result.php?SurveyID=$SurveyID");
    exit();
}

$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     if ($GroupID != "")
     {
         $userlist = $lsurvey->returnUserListByGroup($GroupID);
     }
     else if ($RecordType != "")
     {
          $userlist = $lsurvey->returnUserListByUserType($RecordType);
     }
     else if ($ClassName != "")
     {
          $userlist = $lsurvey->returnUserListByClassName($ClassName);
     }
     else
     {
         $userlist = $lsurvey->returnUserList();
     }
     if (sizeof($userlist)==0)
     {
         $x = "<font color=red>$i_Survey_NoSurveyForCriteria</font><br>\n";
         $x .= "<a href=javascript:history.back()><img src=$image_back border=0></a>";
     }
     else
     {
         $x = "<ol>\n";
         for ($i=0; $i<sizeof($userlist); $i++)
         {
              list($id,$name) = $userlist[$i];
              $x .= "<li><a class=list_link href=view_answer.php?SurveyID=$SurveyID&viewUser=$id>$name</a></li>\n";
         }
         $x .= "</ol>\n";
     }
     include_once("../../../templates/fileheader.php");

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_UserList; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=left>
<blockquote>
<?=$x?>
</blockquote>
            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>
<?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>