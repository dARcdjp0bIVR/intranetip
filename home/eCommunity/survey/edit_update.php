<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$li = new libsurvey($SurveyID);
$editable = $li->isSurveyEditable();
$ownerGroupID = $li->OwnerGroupID;
$lg = new libgroup($ownerGroupID);
$lu = new libuser($UserID);

if ($ownerGroupID == "")
{
    $gvalid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $gvalid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $li->Internal == 1)
    {
         $gvalid = true;
    }
    else
    {
        $gvalid = false;
    }
}

if ($gvalid)
{
    $GroupID = $ownerGroupID;


    $Title = intranet_htmlspecialchars(trim($Title));
    $Description = intranet_htmlspecialchars(trim($Description));
    $StartDate = intranet_htmlspecialchars(trim($StartDate));
    $EndDate = intranet_htmlspecialchars(trim($EndDate));
    $qStr = intranet_htmlspecialchars(trim($qStr));

    if (sizeof($TargetGroupID)!=0)
    {
        $TargetGroupID = array_unique($TargetGroupID);
        $TargetGroupID = array_values($TargetGroupID);
    }

    $startStamp = strtotime($StartDate);
    $endStamp = strtotime($EndDate);

    if (compareDate($startStamp,$endStamp)>0)   // start > end
    {
        $valid = false;
    }
    else
    {
        $valid = true;
    }

    if ($valid)
    {
        if ($editable)
        {
            $internal = 0;
            if ($lg->hasAdminAllSurvey($UserID))
            {
                if (sizeof($TargetGroupID)==1 && $TargetGroupID[0]==$GroupID)
                {
                    $internal = 1;
                }
            }
            else
            {
                $internal = 1;
            }
        }

        $username = $lu->getNameForRecord();

        $fieldname  = "Title = '$Title', ";
        $fieldname .= "Description = '$Description', ";
        $fieldname .= "DateStart = '$StartDate', ";
        $fieldname .= "DateEnd = '$EndDate',";
        $fieldname .= "RecordStatus = '$RecordStatus', ";
        $fieldname .= "PosterID = '$UserID',";
        $fieldname .= "PosterName = '$username',";
        if ($editable)
        {
            $fieldname .= "Question = '$qStr',";
            $fieldname .= "Internal = '$internal',";
            $fieldname .= "RecordType = '$isAnonymous',";
			$fieldname .= "AllFieldsReq = '$AllFieldsReq',";
        }
        $fieldname .= "DateModified = now()";

        $sql = "UPDATE INTRANET_SURVEY SET $fieldname WHERE SurveyID = '$SurveyID'";
        $li->db_db_query($sql);

        if ($editable)         # Change group target
        {
            # Remove all group first
            $sql = "DELETE FROM INTRANET_GROUPSURVEY WHERE SurveyID = '$SurveyID'";
            $li->db_db_query($sql);

            if ($internal == 1)
            {
                # New record added back for this group only
                $sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES ($GroupID, $SurveyID)";
                $li->db_db_query($sql);
            }
            else
            {
                if (sizeof($TargetGroupID)!= 0)
                {
                    $sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES ";
                    $delimiter = "";
                    for ($i=0; $i<sizeof($TargetGroupID); $i++)
                    {
                         $sql .= "$delimiter (".$TargetGroupID[$i].", $SurveyID)";
                         $delimiter = ",";
                    }
                    $li->db_db_query($sql);
                }
            }
         }
         header("Location: index.php?GroupID=$GroupID&filter=$RecordStatus&msg=2");
    }
    else
    {
        header ("Location: edit.php?SurveyID[]=$SurveyID&invalid=1");
    }
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();

?>