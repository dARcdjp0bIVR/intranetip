<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);

$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     include_once("../../../templates/fileheader.php");
     $queString = $lsurvey->Question;
     $queString = str_replace('"','&quot;',$queString);

     $poster = $lsurvey->returnPosterName();
     $ownerGroup = $lsurvey->returnOwnerGroup();
     $targetGroups = $lsurvey->returnTargetGroups();

     if ($ownerGroup != "")
     {
         $poster = "$poster<br>\n$ownerGroup";
     }
     if (sizeof($targetGroups)==0)
     {
         $target = "$i_general_WholeSchool";
     }
     else
     {
         $target = implode(", ",$targetGroups);
     }

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_Overall; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=center><br>
<table width=95% cellspacing=3 cellpadding=3>
<tr><td width=30%><?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td></tr>
<tr><td width=30%><?=$i_general_enddate?></td><td><?=$lsurvey->DateEnd?></td></tr>
<tr><td width=30%><?=$i_general_title?></td><td><?=$lsurvey->Title?></td></tr>
<tr><td width=30%><?=$i_general_description?></td><td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td></tr>
<tr><td width=30%><?=$i_Survey_Poster?></td><td><?=$poster?></td></tr>
<tr><td width=30%><?=$i_general_TargetGroup?></td><td><?=$target?></td></tr>
<tr><td colspan=2>
<?=$i_Survey_Overall?><br>
<hr width=95% align=center>

<script language="javascript" src="/templates/forms/form_view.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>
<script language="Javascript">
<?=returnFormStatWords()?>
var myQue = "<?=$lsurvey->Question?>";
var myAns = new Array();
<?=$lsurvey->returnAnswer()?>

var stats= new Statistics();
stats.qString = myQue;
stats.answer = myAns;
stats.analysisData();
document.write(stats.getStats());
</SCRIPT>
<hr width=95% align=center>
<a href=javascript:window.print()><img alt='<?=$button_print?>' src="<?=$image_path?>/btn_print_<?=$intranet_session_language?>.gif" border=0></a>
<a href=javascript:window.close()><img alt='<?=$button_close?>' src="<?=$image_closewindow?>" border=0></a>
</td></tr>
</table>

            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>

<?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>