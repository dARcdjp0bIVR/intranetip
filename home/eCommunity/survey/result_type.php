<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$SurveyID = IntegerSafe($SurveyID);

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);

$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     include_once("../../../templates/fileheader.php");
     $targetGroups = $lsurvey->returnTargetGroups();
     if (sizeof($targetGroups)==0)
     {
         $target = "$i_general_WholeSchool";
     }
     else
     {
         $target = implode(", ",$targetGroups);
     }

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_PleaseSelectType; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=left>
<blockquote>
<ol>
<li><a class=functionlink_new href=result.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_Overall?></a></li>
<? if (sizeof($targetGroups)!=0) { ?>
<li><a class=functionlink_new href=list_group.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perGroup?></a></li>
<? } ?>
<li><a class=functionlink_new href=list_id.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perIdentity?></a></li>
<li><a class=functionlink_new href=list_class.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perClass?></a></li>
<? if ($lsurvey->RecordType != 1) { ?>
<li><a class=functionlink_new href=list_user.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perUser?></a></li>
<? } ?>
</ol>
</blockquote>
            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>
<?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>