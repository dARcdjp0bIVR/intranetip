<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);

if ($lsurvey->RecordType == 1)
{
    header("Location: result.php?SurveyID=$SurveyID");
    exit();
}

$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     include_once("../../../templates/fileheader.php");
     $poster = $lsurvey->returnPosterName();
     $ownerGroup = $lsurvey->returnOwnerGroup();
     $targetGroups = $lsurvey->returnTargetGroups();

     if ($ownerGroup != "")
     {
         $poster = "$poster<br>\n$ownerGroup";
     }
     if (sizeof($targetGroups)==0)
     {
         $target = "$i_general_WholeSchool";
     }
     else
     {
         $target = implode(",",$targetGroups);
     }

     $targetGroupArray = $lsurvey->returnTargetGroupsArray();
     # Group-based
     if (sizeof($targetGroupArray)!=0)
     {
         $delim = "";
         $group_list = "";
         for ($i=0; $i<sizeof($targetGroupArray); $i++)
         {
              list($id,$title) = $targetGroupArray[$i];
              $group_list .= "$delim $id";
              $delim = ",";
         }
         /*
         $sql = "SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ($group_list)";
         $userids = $lsurvey->returnVector($sql);
         */
         $name_field = getNameFieldWithClassNumberByLang("a.");
         /*
         $sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a
                 LEFT OUTER JOIN INTRANET_USERGROUP as b ON a.UserID = b.UserID AND b.GroupID IN ($group_list)
                 LEFT OUTER JOIN INTRANET_SURVEYANSWER as c ON a.UserID = c.UserID
                 WHERE c.UserID IS NULL
                 ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
                 */
         $sql = "SELECT a.UserID, $name_field FROM INTRANET_USERGROUP as b
                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                 LEFT OUTER JOIN INTRANET_SURVEYANSWER as c ON a.UserID = c.UserID AND c.SurveyID = '$SurveyID'
                 WHERE c.UserID IS NULL AND b.GroupID ".($group_list==""?"=''":"IN ($group_list) ")."
                 ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
#                 echo "<!-- $sql -->";
         $result = $lsurvey->returnArray($sql,2);
     }
     else  # School-based
     {
         $name_field = getNameFieldWithClassNumberByLang("a.");
         $sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a
                 LEFT OUTER JOIN INTRANET_SURVEYANSWER as c ON a.UserID = c.UserID AND c.SurveyID = '$SurveyID'
                 WHERE c.UserID IS NULL
                 ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
         $result = $lsurvey->returnArray($sql,2);
     }
     $display = "<table align=center width=70% border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>\n";
     for ($i=0; $i<sizeof($result); $i++)
     {
          list ($id, $name) = $result[$i];
          $display .= "<tr><td>$name</td></tr>\n";
     }
     $display .= "</table>\n";

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_UnfillList; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=center>

<table width=95% cellspacing=3 cellpadding=3>
<tr><td width=30%><?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td></tr>
<tr><td width=30%><?=$i_general_enddate?></td><td><?=$lsurvey->DateEnd?></td></tr>
<tr><td width=30%><?=$i_general_title?></td><td><?=$lsurvey->Title?></td></tr>
<tr><td width=30%><?=$i_general_description?></td><td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td></tr>
<tr><td width=30%><?=$i_Survey_Poster?></td><td><?=$poster?></td></tr>
<tr><td width=30%><?=$i_general_TargetGroup?></td><td><?=$target?></td></tr>
<tr><td colspan=2>
<? if (sizeof($result)!=0) { ?>
<hr width=95%>
<B><?=$i_Survey_UnfillList?></B><br>
<a class=functionlink_new href=result.php?SurveyID=<?=$SurveyID?>>(<?=$i_Survey_Overall?>)</a>
<br><br>
<?=$display?>
<? }
else
{
echo $i_Survey_AllFilled;
}
?>
<hr width=95%>
<a href=javascript:window.print()><img alt='<?=$button_print?>' src="<?=$image_path?>/btn_print_<?=$intranet_session_language?>.gif" border=0></a>
<a href=javascript:window.close()><img alt='<?=$button_close?>' src="<?=$image_closewindow?>" border=0></a>
</td></tr>
</table>

            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>

<?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>