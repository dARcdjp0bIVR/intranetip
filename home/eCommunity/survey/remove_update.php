<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$lg = new libgroup($GroupID);
if ($GroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
        $internalOnly = false;
    }
    else if ($lg->hasAdminInternalSurvey($UserID))
    {
         $valid = true;
         $internalOnly = true;
    }
    else
    {
        $valid = false;
    }
}
if ($valid)
{
    # Select which can be deleted
    $conds = ($internalOnly? " AND Internal = 1":"");
    $olist = implode(",",$SurveyID);
    $sql = "SELECT SurveyID FROM INTRANET_SURVEY WHERE SurveyID IN ($olist) AND OwnerGroupID = '$GroupID' $conds";
    $result = $lg->returnVector($sql);
    if (sizeof($result)!=0)
    {
        $list = implode(",",$result);
        $sql = "DELETE FROM INTRANET_SURVEY WHERE SurveyID IN ($list)";
        $lg->db_db_query($sql);
        $sql = "DELETE FROM INTRANET_SURVEYANSWER WHERE SurveyID IN ($list)";
        $lg->db_db_query($sql);
    }
    header("Location: index.php?GroupID=$GroupID&msg=3");
}
else
{
    header("Location: ../close.php");
}


intranet_closedb();
?>