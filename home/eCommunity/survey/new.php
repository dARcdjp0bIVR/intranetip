<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
include_once("../tab.php");

$GroupID = IntegerSafe($GroupID);

$li = new libgroup($GroupID);
if ($li->hasAdminInternalSurvey($UserID) || $li->hasAdminAllSurvey($UserID))
{
    include_once("../../../templates/fileheader.php");
     if ($t!="")
         echo "<p> $i_con_msg_date_start_wrong </p>\n";

    # From tab.php
    $grp_navigation .= "";
    $toolbar .= "<input type=image src=$image_submit>";
    $toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
    $toolbar .= " <input type=image src=$image_cancel onClick=\"location='index.php?GroupID=$GroupID'; return false;\">";
    if ($li->hasAdminAllSurvey($UserID))
    {
        include_once("../../../includes/libgrouping.php");
        $lgrouping = new libgrouping();
        //$public_row = "<tr><td align=right></td><td><input type=checkbox NAME=public value=1>$i_GroupPublicToSchool</td></tr>\n";
        $public_row = "<tr><td colspan=2>$i_Survey_PublicInstruction".$lgrouping->displaySurveyGroupsFrontend()."</td></tr>\n";
    }

    ?>
<script language="javascript">
function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}
     function checkform(obj){
             if(!check_text(obj.StartDate, "<?php echo $i_alert_pleasefillin.$i_general_startdate; ?>.")) return false;
             if(!check_date(obj.StartDate, "<?php echo $i_invalid_date; ?>.")) return false;
             if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$i_general_enddate; ?>.")) return false;
             if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>.")) return false;
             if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_general_title; ?>.")) return false;
             if(compareDate(obj.EndDate.value,obj.StartDate.value)<0) {alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); return false;}
             <?
             if ($li->hasAdminAllSurvey($UserID))
             {
             ?>
             checkOptionAll(obj.elements["TargetGroupID[]"]);
             <? } ?>
     }

     </script>

<form name=form1 action="new_update.php" method=post enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
        <tr>
            <td width="1"><img src="/images/spacer.gif" width="1" height="1">
            </td>
      <td align=left>
      </td></tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$button_new." ".$i_GroupSettingsSurvey?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
          </tr>
          <tr>
          <td colspan=2 align=center>
            <table width=93% border=0 cellpadding=2 cellspacing=1>
              <tr><td align=right><?php echo $i_general_startdate; ?>:</td><td><input class=text type=text name=StartDate size=10 maxlength=10 value="<?php echo ($ad==""?date("Y-m-d"):$ad); ?>"> (yyyy-mm-dd)</td></tr>
              <tr><td align=right><?php echo $i_general_enddate; ?>:</td><td><input class=text type=text name=EndDate size=10 maxlength=10 value="<?php echo ($ed==""?date("Y-m-d"):$ed); ?>"> (yyyy-mm-dd)</td></tr>
              <tr><td align=right><?php echo $i_general_title; ?>:</td><td><input class=text type=text name=Title value="<?=$t?>" size=30 maxlength=100></td></tr>
              <tr><td align=right><?php echo $i_general_description; ?>:</td><td><textarea name=Description cols=60 rows=10><?=$d?></textarea></td></tr>
              <tr><td align=right><?php echo $i_general_status; ?>:</td><td><input type=radio name=RecordStatus value=1 CHECKED> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=2> <?php echo $i_status_pending; ?> <input type=radio name=RecordStatus value=3> <?php echo $i_status_template; ?></td></tr>
              <tr><td align=right>&nbsp;</td><td><input type=checkbox name=isAnonymous value=1> <?="$i_Survey_Anonymous ($i_Survey_Anonymous_Description)"?></td></tr>
                          <tr>
                                <td align=right>&nbsp;</td>
                                <td><input name=AllFieldsReq type=checkbox value="1"> <?=$i_Survey_AllRequire2Fill?></td>
                          </tr>
              <tr>
                <td align=right nowrap><?=$i_Form_ConstructForm?>:</td>
                <td>
                  <a href="javascript:newWindow('editform.php?GroupID=<?=$GroupID?>',1)"><img border=0 src="<?=$image_path?>/btn_conduct_survey_<?=$intranet_session_language?>.gif">&nbsp;<a href=javascript:newWindow('preview.php',11)>[<?="$button_preview"?>]</a>
                </td>
              </tr>
              <?=$public_row?>
              <tr>
                <td></td>
                <td>
                  <?=$toolbar?>
                </td>
              </tr>
            </table>



          </td>
          </tr>
        </table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type=hidden name="qStr" value="">
<input type=hidden name="aStr" value="">
<input type=hidden name=GroupID value=<?=$GroupID?>>
</form>
     <?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../settings/?GroupID=$GroupID");
}
intranet_closedb();
?>