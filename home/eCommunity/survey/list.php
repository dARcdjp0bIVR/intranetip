<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$lu = new libuser($UserID);
$lsurvey = new libsurvey();
$x = $lsurvey->displaySurvey();

$navigation =  $i_frontpage_separator.$i_Survey;

include_once("../../../templates/homeheader.php");
?>
<SCRIPT LANGUAGE=Javascript>
function openSurvey(id)
{
         newWindow('/home/school/survey/fill.php?SurveyID='+id,1);
}
</SCRIPT>
<table width="794" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25">&nbsp;</td>
    <td width="769" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td><img src="<?=$image_path?>/survey/title_<?=$intranet_session_language?>.gif"></td>
  </tr>
  <tr>
    <td width="25">&nbsp;</td>
    <td width="769" colspan="2">&nbsp;</td>
  </tr>
</table>
<table width="794" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
      <table width="719" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="48"><img src="<?=$image_path?>/survey/frame_top_left.gif" width="48" height="80"></td>
          <td width="577"><img src="<?=$image_path?>/survey/frame_top_middle.gif" width="577" height="80"></td>
          <td width="94"><img src="<?=$image_path?>/survey/frame_top_right.gif" width="94" height="80"></td>
        </tr>
        <tr>
          <td class=survey_list_bg_left>&nbsp;</td>
          <td align="center" valign="top">
          <?=$x?>
          </td>
          <td align="left" valign="top" class=survey_list_bg_right><img src="<?=$image_path?>/survey/pencil.gif" width="94" height="279"></td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/survey/frame_bottom_left.gif" width="48" height="49"></td>
          <td class=survey_list_bg_bottom>&nbsp;</td>
          <td><img src="<?=$image_path?>/survey/frame_bottom_right.gif" width="94" height="49"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="794" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<?php
include_once("../../../templates/homefooter.php");
intranet_closedb();
?>