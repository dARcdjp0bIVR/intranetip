<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libsurvey.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$SurveyID = IntegerSafe($SurveyID);

$lsurvey = new libsurvey($SurveyID);
$ownerGroupID = $lsurvey->OwnerGroupID;
$lg = new libgroup($ownerGroupID);

if ($ownerGroupID == "")
{
    $valid = false;
}
else
{
    if ($lg->hasAdminAllSurvey($UserID))
    {
        $valid = true;
    }
    else if ($lg->hasAdminInternalSurvey($UserID) && $lsurvey->Internal == 1)
    {
         $valid = true;
    }
    else
    {
        $valid = false;
    }
}

if ($valid)
{
     $x = "<ol>\n";
     for ($i=1; $i<sizeof($i_identity_array); $i++)
     {
          $name = $i_identity_array[$i];
          $x .= "<li><a class=list_link href=result_id.php?SurveyID=$SurveyID&RecordType=$i>$name</a></li>\n";
     }
     $x .= "</ol>\n";
     include_once("../../../templates/fileheader.php");

?>
<table width="580" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><img src=<?=$image_path?>/survey/heading_survey_<?=$intranet_session_language?>.gif></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=500 class=popup_topcell><font color="#FFFFFF"><?php echo $i_Survey_IdentityList; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
        <table width="553" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg.gif">
          <tr>
            <td align=left>
<blockquote>
<?=$x?>
</blockquote>
            </td>
          </tr>
        </table>
      <table width="553" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom.gif"></td>
        </tr>
      </table>

    </td>
  </tr>

</table>
<?php
     include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>