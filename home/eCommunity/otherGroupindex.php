<?php
# using: yat

############## Change Log Start ###############
#
#	Date:	2011-08-11 YatWoon
#			Remove incorrect description
#
#	Date:	2010-04-15 YatWoon
#			Hidden icon list
#	
############## Change Log End ###############

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
//include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/libqb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libcycle.php");
//include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "OtherGroups";

$lu2007 = new libuser2007($UserID);
$legroup 		= new libegroup();

$SchoolYear=Get_Current_Academic_Year_ID();

### Title ###
$TAGS_OBJ[] = array($eComm['MyGroups'],"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE="Javascript">
function openGroupPage(id){
         newWindow('/home/school/group/index.php?GroupID='+id,10);
}
function openSurvey(id)
{
         newWindow('/home/school/survey/fill.php?SurveyID='+id,1);
}
</SCRIPT>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="8">
        	<? /* ?>
		<tr>
                	<td colspan="2" class="tabletext">
                                <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                <tr class="tablerow2">
                                	<td class="tabletext"><?=$i_GroupPageDescription?></td>
        			</tr>
                        </table>
                        </td>
		</tr>
		<? */ ?>
<? /* ?>
                <tr>
                	<td colspan="2" align="right">
                                <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                <td align='center' valign='middle'><span class='tabletext'><?=$i_general_icon_list?></span><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/icon_arrow.gif' width='11' height='11' align='absmiddle'> </td>
                                <td align='right'>
                                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                        <tr> 
                                        <td width='4'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/iconlist_left.gif' width='4' height='18'></td>
                                        <td bgcolor='#fafafa'><table border='0' cellpadding='0' cellspacing='0' class='iconlist'>
                                        <tr>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_annou.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_announcement?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_event.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_event?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_timetable.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_timetable?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_chatroom.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_chat?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_forum.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_bulletin?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_link.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_links?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_doc.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_files?>&nbsp;</td>
                                        <td align='center'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/icon_list/icon_groupsetting.gif' width='18' height='16' border='0' align='absmiddle'>&nbsp;<?=$i_frontpage_schoolinfo_groupinfo_group_settings?>&nbsp;</td>
                                        <td align='center'>&nbsp;</td>
                                        </tr>
                                        </table></td>
                                        <td width='4'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/eclass/iconlist_right.gif' width='4' height='18'></td>
                                        </tr>
                                        </table>
        			</td>
                                </tr>
                                </table>
			</td>
		</tr>
		<? */ ?>
                <tr>
                	<td colspan="2" class="tabletext"><?=$lu2007->displayGroupPage2(1,$SchoolYear);?></td>
		</tr>
                
                </table>
	</td>
</tr>
</table>        
<br />
 
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
