<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

include_once("../tab.php");
$parent_id = $_REQUEST['album_id'];
$targetArray = $_GET['AlbumID'];

$GroupID = IntegerSafe($GroupID);
$parent_id = IntegerSafe($parent_id);
$targetArray = IntegerSafe($targetArray);
$album_id = IntegerSafe($album_id);

# Get original record
$album_id = (is_array($targetArray)?$targetArray[0]:$targetArray);

$la = new libalbum($album_id);
$la->db = $intranet_db;
$checkbox_checked_access[$la->returnAccessType()] = "checked";
$AllowedAccessType = $la->returnAlowedAccessType($GroupID);

$li = new libgroup($GroupID);
$lo = new libgrouping();
$Groups = $lo->returnAdminGroups($UserID);

# From tab.php
$grp_navigation .= "";
$toolbar .= "<input type=image src=$image_submit>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"javascript:history.back(); return false;\">";	

include_once("../../../templates/fileheader.php");	

?>
<script language="javascript">
function checkform(obj){
	var accesstype;
	for (i=0; i<obj.AccessType.length; i++) {
		if (obj.AccessType[i].checked == true) {
			accesstype = obj.AccessType[i].value;
		}
	}	      
	 
	if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;  
	if (accesstype == 2) {
	    document.form1.action = "edit_album.php";
	}
        
}
</script>

<form name="form1" action="new_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class=popup_top><?=$i_grouphead_groupsetting?></td>
					<td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
					<td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
					<td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
					<td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="1"><img src="/images/spacer.gif" width="1" height="1"></td>
					<td align=left></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
					<td width="730" class="h1"><font color="#031BAC"><?=$button_new." ".$i_GroupSettingTabs[4]?></font></td>
				</tr>
				<tr align="center">
					<td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>					
				</tr>				
				<tr>
          			<td colspan=2 align=center>
            			<table width=93% border=0 cellpadding=2 cellspacing=1>
              				<tr>
              					<td align=right nowrap><?php echo $i_general_DisplayOrder; ?>:</td><td><input class=text type=text name=DisplayOrder size=10 maxlength=10 value="<?=$la->returnDisplayOrder()?>"></td>
              				</tr>
              				<tr>
              					<td align=right nowrap><?php echo $i_AnnouncementTitle; ?>:</td><td><input class=text type=text name=Title value="<?=$la->returnAlbumName()?>" size=30 maxlength=100></td>
              				</tr>
              				<tr>
              					<td align=right><?php echo $i_AlbumDescription; ?>:</td><td><textarea name=Description cols=60 rows=10><?=$la->returnDescription()?></textarea></td>
              				</tr>
              				<tr>
              					<td align=right><?php echo $i_AnnouncementRecordStatus; ?>:</td>
              					<td>
              						<input type=radio name=AccessType value=1 <?=$checkbox_checked_access[1]?>> <?php echo $i_AlbumInternalGroup; ?><br>
              					<? 
              						if ($AllowedAccessType != 1)
              						{
	              						?>  
	              							<input type=radio name=AccessType value=2 <?=$checkbox_checked_access[2]?>> <?php echo $i_AlbumSelectedGroupsUsers; ?><br>            					              					
              								<input type=radio name=AccessType value=3 <?=$checkbox_checked_access[3]?>> <?php echo $i_AlbumIntranet; ?><br>	              								
              							<?
          							}
          							if ($AllowedAccessType == 3)
          							{
	          							?>          							
              								<input type=radio name=AccessType value=4 <?=$checkbox_checked_access[4]?>> <?php echo $i_AlbumInternet; ?><br>
              							<?
          							}
              					?>	              						
              					</td>	
              				</tr>              				
							<tr>
								<td colspan=2><br></td>
							</tr>
							<tr>
								<td></td>
								<td><?=$toolbar?></td>
							</tr>
              			</table>
              		</td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="parent_id" value="<?=$parent_id?>">
<input type="hidden" name="album_id" value="<?=$album_id?>">
<input type="hidden" name="group_id" value="<?=$GroupID?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>