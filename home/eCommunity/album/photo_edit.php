<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$album_id = $_GET['album_id'];
//$album_name = $_GET['album_name'];
$ItemID = $_GET['ItemID'];
$photo_id= $ItemID[0];

$GroupID = IntegerSafe($GroupID);
$album_id = IntegerSafe($album_id);


$li = new libalbum($album_id);
$li->db = $intranet_db;
$li->photo_limit = array(300, 300);
$li->loadPhoto($photo_id);

$lg = new libgroup($GroupID);
$lo = new libgrouping();
$Groups = $lo->returnAdminGroups($UserID);

# From tab.php
$grp_navigation .= "";
$toolbar .= "<input type=image src=$image_submit>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"javascript:history.back(); return false;\">";	

include_once("../tab.php");
include_once("../../../templates/fileheader.php");	
?>

<script language="javascript">
function viewPhoto(pid){
	newWindow('photo_view.php?album_id=<?=$album_id?>&photo_id='+pid, 16);
	return ;
}
</script>

<form name="form1" method="post" action="photo_edit_update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class=popup_top><?=$i_grouphead_groupsetting?></td>
					<td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
					<td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
					<td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
					<td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="1"><img src="/images/spacer.gif" width="1" height="1"></td>
					<td align=left></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
					<td width="730" class="h1"><font color="#031BAC"><?=$button_new." ".$i_GroupSettingTabs[4]?></font></td>
				</tr>
				<tr align="center">
					<td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>					
				</tr>				
				<tr>
          			<td colspan=2 align=center><br>
            			<table width=93% border=0 cellpadding=2 cellspacing=1>
              				<tr>
              					<td align=right nowrap><?=$i_AlbumPhotoFile?>:</td>
              					<td><?=$li->returnPhotoImg()?></td>
              				</tr>
              				<tr>
              					<td align=right nowrap><?=$i_general_description?>:</td>
              					<td><input type="text" name="photo_description" value="<?=$li->returnPhotoDescription(1)?>" size=45 maxlength="255"></td>
              				</tr>
							<tr>
								<td colspan=2><br></td>
							</tr>
							<tr>
								<td></td>
								<td><?=$toolbar?></td>
							</tr>
              			</table>
              		</td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type=hidden name="photo_id" value="<?=$photo_id?>">
<input type=hidden name="album_id" value="<?=$album_id?>">
<input type=hidden name="group_id" value="<?=$GroupID?>">
<input type=hidden name="count" value="<?=$count?>">
</form>

<?php
intranet_closedb();
include_once("../../../templates/filefooter.php");
?>