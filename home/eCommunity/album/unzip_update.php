<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$group_id = IntegerSafe($group_id);
$album_id = IntegerSafe($album_id);

$zip_file_name = htmlentities(trim($userfile_name));
$file_ext = strtoupper(strrchr($zip_file_name, "."));

if ($file_ext != ".ZIP")
{
	# Support zip file only
	header("Location: photo_add_batch.php?GroupID=$group_id");
	exit();
}
else
{	
	$li = new libdb();
	$lf = new libfilesystem(); 
	
	$la = new libalbum($album_id);		
	$la->db = $li->db;
	$album_path = $la->getAlbumPathFile($album_id);	
#	$dest = $file_path.$album_path;
		
	$temp_path = $file_path.$album_path."tmp";
	
	$temp_path= OsCommandSafe($temp_path);	
	$lf->folder_new($temp_path);
	if (file_exists($temp_path))
	{
		$result_arr = $lf->unzipFile($userfile, $temp_path);
		$folder_size = $lf->folder_size($temp_path);				
		$la->IO2DB($temp_path, $album_path);
		
		exec("rm -r \"$temp_path\"");		
	}
	
	$la->updatePhotoOrder();
	$la->updateAlbumDate();
	$la->updateThumbnail();

	
	header("Location: list_photo.php?album_id=$album_id&GroupID=$group_id");
	
	/*	
	for($i=1; $i<sizeof($result); $i++)
	{		
		$data = explode("/", $result[$i]);
		$filename = $data[sizeof($data)-1];
		
		$is_exist_file = (file_exists($dest.$filename) && trim($filename)!="");
		if ($is_exist_file)
		{			
			$filesize = ceil(filesize($dest.$filename)/1000);
			$la->addPhoto2Album($album_id, "", addslashes($filename), "", $album_path, $filesize);
			$la->updateStorageQuota($filesize);				
		}
	}
	*/
	
}

	

?>
