<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libalbum.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$album_id = IntegerSafe($album_id);
$parent_id = IntegerSafe($parent_id);

$lg = new libgroup($GroupID);
include_once("../tab.php");
include_once("../../../templates/fileheader.php");	

$lo = new libgrouping();
$Groups = $lo->returnAdminGroups($UserID);
$li = new libalbum($album_id);
$li->db = $intranet_db;
$user_list = $li->returnUserAllowedList();
$list = $li->returnAlbumList();

# From tab.php
$grp_navigation .= "";
$toolbar .= "<input type=image src=$image_submit>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"location='javascript:history.go(-2)'; return false;\">";	
    
$Title = htmlspecialchars(trim($Title));
$Description = htmlspecialchars(trim($Description));
$DisplayOrder = htmlspecialchars(trim($DisplayOrder));
$album_id = htmlspecialchars(trim($album_id));
$parent_id = htmlspecialchars(trim($parent_id));

?>

<script language="javascript">
function checkform(obj){
        checkOptionAll(obj.elements["GroupID[]"]);      
        checkOptionAll(obj.elements["child[]"]); 
}
</script>

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">

<blockquote>
<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class=popup_top><?=$i_grouphead_groupsetting?></td>
					<td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
					<td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $li->Title; ?></font></td>
					<td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
					<td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="1"><img src="/images/spacer.gif" width="1" height="1"></td>
					<td align=left></td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
					<td width="730" class="h1"><font color="#031BAC"><?=$button_new." ".$i_GroupSettingTabs[4]?></font></td>
				</tr>
				<tr align="center">
					<td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
				</tr>
				<tr>
          			<td colspan=2 align=center>
						<table width=500 border=0 cellpadding=5 cellspacing=0>
							<tr>
								<td align="right"><?=$i_admintitle_group?>:</td>
								<td><?php echo $lo->displayPhotoalbumGroups($album_id); ?></td>
							</tr>
							<tr>
								<td align="right"><?=$i_AlbumUserGroup?>:</td>
								<td>
									<table width=95% border=0 cellspacing=1 cellpadding=1>
										<tr>
											<td width=1>
												<select name=child[] size=4 multiple>
												<?php 
													for($i = 0; $i < sizeof($user_list); $i++) { 		
														echo "<option value={$user_list[$i][1]}>{$user_list[$i][0]}</option>\n";
													} 
													echo "<option>";
													for ($i=sizeof($user_list); $i<40; $i++) {
														echo "&nbsp;";
													}
													echo "</option>";		
												?>
												</select>		
											</td>
											<td>
												<a href="javascript:newWindow('choose/index.php?fieldname=child[]',2)"><img src="/images/admin/button/s_btn_select_std_<?=$intranet_session_language?>.gif" border="0"></a>
												<br>
												<a href="javascript:checkOptionRemove(document.form1.elements['child[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
											</td>
										</tr>
										<tr>
											<td colspan=2><br></td>
										</tr>
										<tr>
											<td></td>
											<td><?=$toolbar?></td>
										</tr>
									</table>
								</td>
							</tr>			
						</table>
              		</td>
				</tr>
			</table>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<input type="hidden" name="Title" value="<?=$Title?>">
<input type="hidden" name="Description" value="<?=$Description?>">
<input type="hidden" name="DisplayOrder" value="<?=$DisplayOrder?>">
<input type="hidden" name="AccessType" value="2">
<input type="hidden" name="parent_id" value="<?=$parent_id?>">
<input type="hidden" name="album_id" value="<?=$album_id?>">
<input type="hidden" name="group_id" value="<?=$group_id?>">

</form>

<?php
intranet_closedb();
     include_once("../../../templates/filefooter.php");
?>