<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$album_id = IntegerSafe($album_id);

$li = new libalbum();
$li->db = $intranet_db;
$li->swapPhotos($ItemID[0], $ItemID[1]);

intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count&GroupID=$GroupID");
?>