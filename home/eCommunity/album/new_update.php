<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libgrouping();
$valid = 1;
$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$DisplayOrder = intranet_htmlspecialchars(trim($DisplayOrder));
$AccessType = intranet_htmlspecialchars(trim($AccessType));
$ParentID = IntegerSafe(trim($parent_id));
$child = $_REQUEST['child'];
$UserAllowedList = "";
$GroupAllowedList = "";

if ($valid)
{         
    if ($AccessType == 2) {
		# Handle Group and User Permission
		$UserList = Array();
		$GroupList = Array();
		for ($i=0; $i<sizeof($child); $i++) {
			array_push($UserList, $child[$i]);
		}		
		for ($i=0; $i<sizeof($GroupID); $i++) {			
			array_push($GroupList, $GroupID[$i]);
		}
		$UserList = array_unique($UserList);
		$GroupList = array_unique($GroupList);
		$UserAllowedList = implode(',', $UserList);		
		$GroupAllowedList = implode(',', $GroupList);		
 	}

 	
	if (empty($album_id)) {
 		# Insert into database
 		$sql = "INSERT INTO INTRANET_PHOTO_ALBUM (ParentID, IsLeafNote, AlbumName, Description, DisplayOrder, OwnerGroupID, LastChangeByUserID, AccessType, UserAllowedList, GroupAllowedList, NumberOfItems, DateInput, DateModified) VALUES ('$ParentID', '1', '$Title', '$Description', '$DisplayOrder', '$group_id', '$UserID', '$AccessType', '$UserAllowedList', '$GroupAllowedList', 0, Now(), Now())";
 		$li->db_db_query($sql);
 		$AlbumID = $li->db_insert_id();
 		
 	   	$folder = session_id()."_a";
	 	$path = "";
	     
	/*
	     $sql = "INSERT INTO INTRANET_ANNOUNCEMENT (Title, Description, AnnouncementDate, RecordStatus, DateInput, DateModified, EndDate) VALUES ('$Title', '$Description', '$AnnouncementDate', '$RecordStatus', now(), now(),'$EndDate')";
	     $li->db_db_query($sql);
	     $AnnouncementID = $li->db_insert_id();
	
	     for($i=0; $i<sizeof($GroupID); $i++){
	             $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$GroupID[$i].", $AnnouncementID)";
	             $li->db_db_query($sql);
	     }
	*/
	
		$sql = "SELECT FilePath FROM INTRANET_PHOTO_ALBUM WHERE AlbumID='$ParentID'";
		$ParentPath = $li->returnVector($sql);	
	    $path = "$file_path/file/album/$ParentPath[0]$folder$AlbumID/";
	    $filepath = "$ParentPath[0]$folder$AlbumID/";
	    
	    # Craete folder if not exists
	    $lf = new libfilesystem();  
		$lf->folder_new($path);	
  
		# Update path in DB
		$sql = "UPDATE INTRANET_PHOTO_ALBUM SET FilePath = '".$filepath."' WHERE AlbumID='$AlbumID'";   
		$li->db_db_query($sql);  
	
		# Update leafnote, NumberOfItems of Parent in DB
		$sql = "UPDATE INTRANET_PHOTO_ALBUM SET IsLeafNote=0, NumberOfItems=NumberOfItems+1 WHERE AlbumID='$ParentID'";
		$li->db_db_query($sql);	
	} else {
		# Update album		
		$sql = "UPDATE INTRANET_PHOTO_ALBUM SET AlbumName='$Title', Description='$Description', DisplayOrder='$DisplayOrder', AccessType='$AccessType', UserAllowedList='$UserAllowedList', GroupAllowedList='$GroupAllowedList', LastChangeByUserID='$UserID' WHERE AlbumID='$album_id'";
		$li->db_db_query($sql);		
	}	
	
	intranet_closedb();
	header("Location: index.php?album_id=$ParentID&GroupID=$group_id");	
}
else
{
    header("Location: new.php?t=$Title&d=$Description&a=$AccessType&album_id=$album_id&GroupID=$group_id");
}
?>