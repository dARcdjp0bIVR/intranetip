<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);
$album_id = IntegerSafe($album_id);


include_once("../tab.php");

$lgroup = new libgroup($GroupID);

if ($lgroup->isAccessPhotoAlbum())
{
    $isAlbumAdmin = $lgroup->hasAdminPhotoAlbum($UserID);
    $isAdmin = $isAlbumAdmin;
    
   # $deleteIcon = ($isAdmin ? ",'<a href=javascript:del(', IA.AlbumID, ')><img src=../../../images/eraser_icon.gif alt=$button_remove hspace=20 vspace=2 border=0 align=absmiddle></a>'" : "");
   
    
	# TABLE SQL
	if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
	$pageSizeChangeEnabled = true;

	$keyword = trim($keyword);
	
	if ($field=="") $field=1;
	switch ($field){        
	        case 1: $field = 1; break;
	        case 2: $field = 2; break;
	        case 3: $field = 3; break;
	        case 4: $field = 4; break;
	        case 5: $field = 5; break;
	        default: $field = 1; break;
	}
	$order = ($order == 1) ? 1 : 0;
	
	if($filter == "") $filter = 1;
	
	$parentID = (!isset($album_id) || $album_id == "") ? 0 : $album_id;
	$cond = (empty($album_id)) ? " AND IA.ParentID=0 " : " AND IA.ParentID='$album_id'";

	#$filter = ($filter == 1) ? 1 : 0;
	/*
	if ($lgroup->hasAdminPhotoAlbum($UserID))
	{
	    $conds = "";
	}
	else
	{
	    $conds = "AND a.Internal = 1";
	}
	*/
	$user_field = getNameFieldWithClassNumberByLang("c.");
	
	$sql  = "SELECT				
	               CONCAT(IF(IA.isLeafNote=0, CONCAT('<a class=tableContentLink href=\"index.php?album_id=', IA.AlbumID, '\">', IA.AlbumName, '</a>'), CONCAT('<a class=tableContentLink href=\"list_photo.php?GroupID=', $GroupID, '&album_id=', IA.AlbumID, '&count=', IA.NumberOfItems,'\">')), '<img src=\"/includes/imagethumbnail.php?image=',IP.Path, IP.FileName, '\" width=100 border=0 >'),
	               IF(IA.isLeafNote=0, CONCAT('<a class=tableContentLink href=\"index.php?GroupID=',$GroupID,'&album_id=', IA.AlbumID, '\">', IA.AlbumName, '</a>'), CONCAT('<a class=tableContentLink href=\"list_photo.php?GroupID=', $GroupID,'&album_id=', IA.AlbumID, '&count=', IA.NumberOfItems,'\">', IA.AlbumName, '</a>')),
	               IA.Description,IA.NumberOfItems,
	               DATE_FORMAT(IA.DateModified, '%Y-%m-%d<br>%H:%i:%s'),
	               CONCAT('<input type=checkbox name=AlbumID[] value=', IA.AlbumID ,'>')
	          FROM
	               INTRANET_PHOTO_ALBUM AS IA LEFT JOIN INTRANET_PHOTO_ITEM AS IP ON IA.ThumbnailID=IP.ItemID
	          WHERE
	               (IA.AlbumName like '%$keyword%' OR IA.Description like '%$keyword%')          
	               $cond
	               AND IA.OwnerGroupID = '$GroupID'
	               GROUP BY IA.AlbumID
	          ";  
	          
	/*
	if ($special_feature['announcement_approval'] && $filter == 1)
	{
	    $table_ext = "LEFT OUTER JOIN INTRANET_ANNOUNCEMENT_APPROVAL as approval ON a.AnnouncementID = approval.AnnouncementID";
	    $field_ext = ",IF(approval.AnnouncementID IS NULL,'$i_AdminJob_Announcement_NoApproval','$i_AdminJob_Announcement_NeedApproval')";
	}
	
	
	
	
	# For Group-based administration
	$sql  = "SELECT
	                        DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
	                        DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
	                        if (a.OwnerGroupID = $GroupID,CONCAT('<a href=edit.php?GroupID=$GroupID&AnnouncementID[]=', a.AnnouncementID, '>', a.Title, '</a> ',' <a href=javascript:showRead(',a.AnnouncementID,')><img src=$image_path/icon_viewstatics.gif border=0 alt=\"$i_AnnouncementViewReadStatus\"></a>'),a.Title),
	                        IF (a.UserID IS NOT NULL AND a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer') as Username
	                        $field_ext
	                        ,a.DateModified,
	                        if (a.OwnerGroupID = $GroupID, CONCAT('<input type=checkbox name=AnnouncementID[] value=', a.AnnouncementID ,'>'),' ')
	                FROM
	                        INTRANET_ANNOUNCEMENT as a LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
	                        $table_ext
	
	                WHERE
	                        (a.Title like '%$keyword%') AND
	                        a.RecordStatus = $filter AND
	                        a.OwnerGroupID = $GroupID
	                        $conds
	                ";
	/*
	# For User-based administration
	$sql  = "SELECT
	                        DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
	                        DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
	                        if (a.UserID = $UserID,CONCAT('<a href=edit.php?GroupID=$GroupID&AnnouncementID[]=', a.AnnouncementID, '>', a.Title, '</a>'),a.Title),
	                        IF (a.UserID IS NOT NULL AND a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer') as Username,
	                        a.DateModified,
	                        if (a.UserID = $UserID, CONCAT('<input type=checkbox name=AnnouncementID[] value=', a.AnnouncementID ,'>'),' ')
	                FROM
	                        INTRANET_ANNOUNCEMENT as a LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
	                WHERE
	                        (a.Title like '%$keyword%') AND
	                        a.RecordStatus = $filter AND
	                        a.OwnerGroupID = $GroupID
	                        $conds
	                ";
	*/
	
	
	# TABLE INFO
	$li = new libdbtable($field, $order, $pageNo);
	$li->field_array = array("IP.FileName", "IA.AlbumName", "IA.Description", "IA.NumberOfItems", "IA.DateModified");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$title_width = 270;
/*
	if ($special_feature['announcement_approval'] && $filter == 1)
	{
	    $li->no_col++;
	    $title_width -= 50;
	}
	*/
	$li->title = $i_admintitle_im_photoalbum;
	$li->column_array = array(0,0,5,0,0,0);
	$li->wrap_array = array(0,0,20,0,0,0);
	$li->IsColOff = 4;
	
	// TABLE COLUMN
	$li->column_list .= "<td bgcolor=#FCD5AE width=30 class=title_head>#</td>\n";
	$li->column_list .= "<td bgcolor=#FCD5AE width=90 class=title_head>".$li->column(0, "")."</td>\n";
	$li->column_list .= "<td bgcolor=#FCD5AE width=90 class=title_head>".$li->column(1, $i_AlbumName)."</td>\n";
	$li->column_list .= "<td bgcolor=#FCD5AE width=$title_width class=title_head>".$li->column(2, $i_AlbumDescription)."</td>\n";
	$li->column_list .= "<td bgcolor=#FCD5AE width=90 class=title_head>".$li->column(3, $i_AlbumNumberOfItems)."</td>\n";
	#if ($special_feature['announcement_approval'] && $filter == 1)
	#{
	#$li->column_list .= "<td bgcolor=#FCD5AE width=50 class=title_head>".$li->column(4, $i_AdminJob_Announcement_NeedApproval)."</td>\n";
	$li->column_list .= "<td bgcolor=#FCD5AE width=150 class=title_head>".$li->column(5, $i_AnnouncementDateModified)."</td>\n";
	#}
	#else
	#{
	#$li->column_list .= "<td bgcolor=#FCD5AE width=90 class=title_head>".$li->column(4, $i_AnnouncementDateModified)."</td>\n";
	#}
	$li->column_list .= "<td bgcolor=#FCD5AE width=30 class=title_head>".$li->check("AlbumID[]")."</td>\n";


	// TABLE FUNCTION BAR
	if ($isAdmin)
	{
		$toolbar = "<a href=\"javascript:checkGet(document.form1, 'new.php?GroupID=$GroupID')\">".newFolderIcon()."$button_new_album</a>";
		#$toolbar = "<a href=javascript:checkGet(document.form1,'new.php')>".newIcon2()."$button_new</a>";
		$functionbar  = "<a href=javascript:checkEdit(document.form1,'AlbumID[]','edit.php')><img src=$image_edit border=0></a>";
		$functionbar .= "<a href=javascript:checkRemove(document.form1,'AlbumID[]','remove.php')><img src=$image_delete border=0></a>";
	}	
	$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= "<input type=image src=$image_search border=0>\n";

$xmsg = "";
switch($msg){
     case 1: $xmsg = "<p> $i_con_msg_add </p>\n"; break;
     case 2: $xmsg = "<p> $i_con_msg_update </p>\n"; break;
     case 3: $xmsg = "<p> $i_con_msg_delete </p>\n"; break;
}

echo $xmsg;

    $groupSelect .= jumpIcon().$lgroup->getSelectAdminAnnounce("name=GroupID onChange=this.form.submit()",$GroupID);
# From tab.php
$grp_navigation .= "";
include_once("../../../templates/fileheader.php");
include_once("../tooltab.php");

?>
<SCRIPT LANGUAGE=Javascript>
function showRead(id)
{
         newWindow('read.php?AnnouncementID='+id,6);
}
</SCRIPT>
<form name=form1 action="" method=get>
<table width="750" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class=popup_top><?=$i_grouphead_groupsetting?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td width=100% class=popup_topcell><font color="#FFFFFF"><?php echo $lgroup->Title; ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <?=$header_tool?>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
		  <tr>
            <td width="1"><img src="images/spacer.gif" width="1" height="1"></td>
      		<td align=left width=749><?=$groupSelect?></td>
      	  </tr>
          <tr>
            <td colspan=2 align="right">
              <table width="390" border="0" cellspacing="0" cellpadding="0" class="functionlink">
                <tr>
                  <td align="right" bgcolor="#EDDA5C">
                    <div align="center"><?=$grp_navigation?></div>
                  </td>
                  <td width="12"><img src="/images/spacer.gif" width="12" height="1"></td>
                </tr>
              </table>
              <br>
            </td>
          </tr>
      </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1">
            </td>
            <td width="730" class="h1"><font color="#031BAC"><?=$i_GroupSettingsAlbum?></font></td>
          </tr>
          <tr align="center">
            <td colspan="2"><img src="/images/line.gif" height="1"></td>
          </tr>
          <tr>
            <td colspan=2>
              <table width=100%>
                <tr>
                  <td width="40"><img src="/images/spacer.gif" width="20" height="1"></td>
                  <td width="710" class="decription"><?=$i_AnnouncementAdminRight?></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr align="center">
            <td height=10 colspan="2"><img src="/images/spacer.gif" width="450" height="1"></td>
          </tr>
        </table>
        <table width="750" border="0" cellspacing="0" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif" height="30">
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
            <td valign="middle" class="functionlink" align="left"><?=$toolbar?></td>
            <td valign="middle" align="right"><?="$i_AnnouncementRecordStatus: $searchbar"?></td>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
          </tr>
          <tr>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
            <td></td>
            <td valign="middle" align="right"><?=$functionbar?></td>
            <td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
          </tr>
        </table>
        <?=$li->display()?>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type=hidden name=album_id value="<?=$album_id?>">
<input type=hidden name=parent_id value="<?=$parentID?>">
<input type=hidden name=count value="<?=$count?>">

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../settings/?GroupID=$GroupID");
}
intranet_closedb();
?>