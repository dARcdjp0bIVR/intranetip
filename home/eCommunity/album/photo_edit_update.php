<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$album_id = trim($_POST['album_id']);
$photo_id = trim($_POST['photo_id']);

$album_id = IntegerSafe($album_id);
$photo_id = IntegerSafe($photo_id);
$group_id = IntegerSafe($group_id);
$count = IntegerSafe($count);

$li = new libalbum();
$li->updatePhoto($photo_id, $photo_description);


intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count&GroupID=$group_id");
?>