<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libadminjob.php");
include_once("../../../includes/libalbum.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);


include_once("../tab.php");

$li = new libgroup($GroupID);
$lo = new libgrouping();
$la = new libalbum();

$x = $li->getSelectPhotoGroups("name=PostGroupID[] size=5 multiple",$GroupID);
$toolbar .= "<input type=image src=$image_submit>";
$toolbar .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$toolbar .= " <input type=image src=$image_cancel onClick=\"history.back(); this.form.method='get';this.form.action='index.php';return false;\">";

$total_photo = 10;

for ($i=0; $i<$total_photo; $i++)
{
	$photo_input .= ($i>0) ? "<tr><td width=\"20\"><img src=\"/images/spacer.gif\" width=\"20\" height=\"1\"></td></tr>\n" : "";
	$photo_input .= "<tr><td width=\"20\"><img src=\"/images/spacer.gif\" width=\"20\" height=\"1\"></td>\n";
	$photo_input .= "<td>".($i+1)."&nbsp;.</td>\n";
	$photo_input .= "<td nowrap>".$i_select_file.":</td>\n";
	$photo_input .= "<td><input class=file type=file name='userfile[]' size=43><input type=hidden name='userfile_hidden[]'></td>\n";
	$photo_input .= "</tr><tr>\n";
	$photo_input .= "<td width=\"20\"><img src=\"/images/spacer.gif\" width=\"20\" height=\"1\"></td>\n";
	$photo_input .= "<td width=\"20\"><img src=\"/images/spacer.gif\" width=\"20\" height=\"1\"></td>\n";
	$photo_input .= "<td nowrap>".$i_general_description.":</td>\n";
	$photo_input .= "<td><input type='text' name='photo_description[]' value='' size=52 maxlength='255'></td>\n";
	$photo_input .= "</tr>\n";
}

include_once("../../../templates/fileheader.php");
?>
<script language="JavaScript">
function Big5FileUploadHandler(obj) 
{			
	var len = obj.elements["userfile[]"][0].value;	
<!--	alert(len);	-->
	for (var i=0; i<10; i++)
	{		
		if (typeof(obj.elements["userfile[]"][i])!='undefined')
		{				
			if (obj.elements["userfile[]"][i].value != '')
			{		
				var Ary = obj.elements["userfile[]"][i].value.split('\\');	
				obj.elements["userfile_hidden[]"][i].value = Ary[Ary.length-1];				
			}
		}
	}		
	return true;
}
</script>
<script language="javascript">
function checkform(obj){

	var len=obj.elements.length;
	var i=0;
	var flag=0;

	for( i=0 ; i<len; i++) 
	{
		if (obj.elements[i].name=="userfile[]" && obj.elements[i].value != "")
			flag = 1;		
	}

	if(flag == 0)
	{
		alert("<?php echo $jr_warning['album_photo_added']; ?>.");
		return false;
	}
	Big5FileUploadHandler(obj);
}
</script>

<form name="form1" method="post" action="photo_update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
      		<table width="750" border="0" cellspacing="0" cellpadding="0">
        		<tr>
					<td width="101" class=popup_top><?=$i_grouphead_groupsetting?></td>
					<td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
					<td class=popup_topcell><font color="#FFFFFF"><?=$i_frontpage_bulletin_postnew?></font></td>
					<td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
					<td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
				</tr>
			</table>	
			<table width="750" border="0" cellspacing="0" cellpadding="0" background="<?=$image_path?>/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="1"><img src="/images/spacer.gif" width="1" height="1"></td>
					<td align=left></td>
				</tr>
			</table>					
			<table width="750" border="0" cellspacing="2" cellpadding="0" background="/images/groupinfo/popup_tbbg_large.gif">
				<tr>
					<td width="20"><img src="/images/spacer.gif" width="20" height="1"></td>
					<td width="730" class="h1"><font color="#031BAC"><?=$button_new." ".$i_GroupSettingTabs[4]?></font></td>
				</tr>
				<tr align="center">
					<td colspan="2"><img src="/images/line.gif" width="616" height="1"></td>
				</tr>				
 				<tr>
 					<td colspan=2 align=center><br>
            			<table width=93% border=0 cellpadding=2 cellspacing=1>
							<?=$photo_input; ?>	
							<tr>
								<td colspan=3><br></td>
							</tr> 										
							<tr>
								<td></td>
								<td></td>
								<td><?=$toolbar?></td>
							</tr>
 						</table>
 					</td>
 				</tr>				
 			</table>		
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name=group_id value="<?php echo $GroupID; ?>">
<input type="hidden" name="album_id" value="<?=$album_id?>">
<input type=hidden name=pageNo value="<?php $pageNo; ?>">
<input type=hidden name=order value="<?php $order; ?>">
<input type=hidden name=field value="<?php $field; ?>">
</form>

<?php
intranet_closedb();
include("../../../templates/filefooter.php");
?>