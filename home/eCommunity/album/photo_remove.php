<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libalbum.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$album_id = $_POST['album_id'];
$order = $_POST['order'];
$pageNo = $_POST['pageNo'];
$field = $_POST['field'];
$ItemID = $_POST['ItemID'];

$album_id = IntegerSafe($album_id);
$ItemID = IntegerSafe($ItemID);
$GroupID = IntegerSafe($GroupID);
$count = IntegerSafe($count);


$lf = new libfilesystem(); 
$li = new libalbum($album_id);
$li->db = $intranet_db;
$size = sizeof($photo_ids);

$photo_ids = (is_array($ItemID)) ? implode(",", $ItemID) : $ItemID;
$li->removePhotos($photo_ids, $album_id);
$li->updateFirstPhoto($album_id);
$count = $li->getNumberOfItems();
$li->updateAlbumDate();
# Set Thumbnail Back
$li->updateThumbnail();

intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count&GroupID=$GroupID");
//header("Location: index_album.php?album_id=$album_id&msg=3");
?>