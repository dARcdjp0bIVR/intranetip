<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libgrouping();
$GroupID = (isset($GroupID)) ? $GroupID : $li->returnFirstGroup();
$lg = new libgroup($GroupID);

if ($lg->isAccessTimetable())
{

if (!$lg->hasAdminTimetable($UserID))
{
     header ("Location: index.php?GroupID=$GroupID");
     exit();
}
// if (!$li->isGroupAdmin($UserID, $GroupID)) header("Location: index.php");
include_once("../../../templates/fileheader.php");


// TABLE FUNCTION BAR
# $toolbar .= "<a href=javascript:checkGet(document.form1,'import.php')>".importIcon()."$button_import</a>\n".toolBarSpacer();
# $toolbar .= "<a href=javascript:checkGet(document.form1,'export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();
/*
$functionbar .= "<select name=GroupID onChange=\"this.form.method='get';this.form.action='import.php';this.form.submit();\">\n";
$functionbar .= $li->displayAdminGroupsSelection($GroupID);
$functionbar .= "</select>\n";
*/
$functionbar2 .= "<input type=image src=$image_submit>";
$functionbar2 .= " <input type=image src=$image_reset onClick=' this.form.reset(); return false;'>";
$functionbar2 .= " <input type=image src=$image_cancel onClick=\"location='index.php?GroupID='+this.form.GroupID.value; return false;\">";
?>

<form action=import_update.php method=post enctype="multipart/form-data">
<table width="750" border="0" cellspacing="0" cellpadding="0">
<tr><td>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="101" class=popup_top><?=$i_grouphead_timetable?></td>
          <td width="8"><img src="/images/groupinfo/popup_barleft.gif"></td>
          <td class=popup_topcell><font color="#FFFFFF" title="<?=$lg->Title?>"><?php echo chopword($lg->Title,40); ?></font></td>
          <td width="10"><img src="/images/groupinfo/popup_barright.gif"></td>
          <td width="12"><img src="/images/groupinfo/popup_barright2.gif"></td>
        </tr>
      </table>
      <table width="750" border="0" cellspacing="0" cellpadding="10" background="/images/groupinfo/popup_tbbg_large.gif">
<tr><td width=20></td>
<td></td>
</tr>
<tr>
<td width=20></td>
<td>
<p><input class=file type=file name=userfile size=25>
<p><input type=radio name=type value="0" checked><?=$i_Timetable_type0?><br>
<p><input type=radio name=type value="1"><?=$i_Timetable_type1?><br>
<p><?php echo $functionbar2; ?>
</td></tr></table>
      <table width="750" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/images/groupinfo/popup_bottom_large.gif"></td>
        </tr>
      </table>

</td></tr>
</table>
<input type=hidden name=GroupID value=<?=$GroupID?>>
</form>

<?php
  include_once("../../../templates/filefooter.php");
}
else
{
    header ("Location: ../close.php");
}
intranet_closedb();
?>