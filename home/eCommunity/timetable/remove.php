<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

$li = new libfilesystem();
$lg = new libgrouping();
$lgroup = new libgroup($GroupID);
if(isset($GroupID) && $lgroup->hasAdminTimetable($UserID)){
     $url = "/file/timetable/g".$GroupID."_timetable.csv";
     $li->lfs_remove($intranet_root.$url);
     $url = "/file/timetable/g".$GroupID."_timetable_special.csv";
     $li->lfs_remove($intranet_root.$url);
     $msg = 3;
}
header("Location: index.php?GroupID=$GroupID&msg=$msg");
?>