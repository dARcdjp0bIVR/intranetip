<?php
# Using:

############ Change Log [Start] ############
#
#   Date:   2018-11-15  Bill    [2018-1105-1041-02206]
#           fixed: email title display html specialchars
#
#	Date:	2016-11-14	Villa
#			update with the change of UI
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2011-04-04	YatWoon
#			change email notification subject & content data 
#
# - 2010-09-30 Marcus
#	add Get_Safe_Sql_Query to Description.
#
#	Date:	2010-08-24 YatWoon
#			hide Public/Private
#
# - 2010-04-13 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
############ Change Log [End] ############

$PATH_WRT_ROOT = "../../../";
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libannounce.php");
include_once("../../../includes/libemail.php");
include_once("../../../includes/libwebmail.php");
include_once("../../../includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once("../../../lang/email.php");

intranet_auth();
intranet_opendb();

$AnnouncementID = IntegerSafe($AnnouncementID);
$GroupID = IntegerSafe($GroupID);

if($publicdisplay) {
	unset($GroupID);
}

$la = new libannounce($AnnouncementID);
$originalStatus = $la->RecordStatus;

$agid = $la->OwnerGroupID;
$lg = new libgroup($agid);

if ($agid == "")
{
    $gvalid = false;
}
else
{
    if ($lg->hasAdminAllAnnounce($UserID))
    {
        $gvalid = true;
    }
    else if ($lg->hasAdminInternalAnnounce($UserID) && $la->Internal == 1)
    {
         $gvalid = true;
    }
    else
    {
        $gvalid = false;
    }
}

if ($gvalid)
{
        $li = new libgrouping();
        $TargetGroupID = $GroupID;
        $GroupID = $agid;

        $Title = intranet_htmlspecialchars(trim($Title));
        $pushMsgTitle = standardizeFormPostValue($_POST['Title']);
//          $Description = intranet_htmlspecialchars(trim($Description));
        $AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
        $EndDate = intranet_htmlspecialchars(trim($EndDate));
         
		$PublicStatus = $la->PublicStatus;		# unchange (due to public status is hidden)
		
        $startStamp = strtotime($AnnouncementDate);
        $endStamp = strtotime($EndDate);
        $onTop = $onTop ? 1 : 0;

         if (compareDate($startStamp,$endStamp)>0)   // start > end
         {
             $valid = false;
         }
         /*
         else if (compareDate($startStamp,time())<0)      // start < now
         {
              $valid = false;
         }*/
         else
         {
             $valid = true;
         }

         if ($valid)
         {
	         $lf = new libfilesystem();
	          
             $internal = 0;
             if ($lg->hasAdminAllAnnounce($UserID))
             {
                 if (sizeof($TargetGroupID)==1 && $TargetGroupID[0]==$GroupID)
                 {
                     $internal = 1;
                 }
             }
             else
             {
                 $internal = 1;
             }
             # ------------------------------

             if ($MakeNew == 1)              # Edit rejected record
             {
	            ## Update description if user upload image with Flash upload (fck) [Start]
// 				$Description2 = stripslashes($Description);
//				$Description2 = stripslashes(trim($Description));
//				$Description_updated = ($lf->copy_fck_flash_image_upload($newAnnouncementID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolNews'])); 
// 				$Description = intranet_htmlspecialchars(trim($Description_updated));
				if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
				{
					if ($Description==strip_tags($Description))
					{
						$Description = nl2br($Description);
					}
				}
				$Description2 = stripslashes(htmlspecialchars_decode($Description));
				$Description_updated = ($lf->copy_fck_flash_image_upload($AnnouncementID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolNews'])); 
				$Description_updated = trim(intranet_htmlspecialchars($Description_updated));

				$Description = intranet_htmlspecialchars(trim($Description_updated));
				## Update description if user upload image with Flash upload (fck) [End]
				
                 $sql = "INSERT INTO INTRANET_ANNOUNCEMENT
                         (Title, Description, AnnouncementDate, RecordType, RecordStatus, DateInput, DateModified, EndDate, OwnerGroupID, UserID, Internal, ReadFlag,PosterName, PublicStatus, onTop) VALUES
                         ('$Title', '".$li->Get_Safe_Sql_Query($Description)."', '$AnnouncementDate', '$publicdisplay', '$RecordStatus', now(), now(),'$EndDate', $GroupID, $UserID, '$internal',';$UserID;','$postername', $PublicStatus, $onTop)";
                 $li->db_db_query($sql);
                 $newAnnouncementID = $li->db_insert_id();
             }
             else
             {
	            ## Update description if user upload image with Flash upload (fck) [Start]
// 			    $Description2 = stripslashes($Description);
				if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
				{
					if ($Description==strip_tags($Description))
					{
						$Description = nl2br($Description);
					}
				}
				$Description2 = stripslashes(trim($Description));
				$Description_updated = ($lf->copy_fck_flash_image_upload($AnnouncementID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolNews'])); 
				$Description = intranet_htmlspecialchars(trim($Description_updated));
				## Update description if user upload image with Flash upload (fck) [End]
				
                 $fieldname  = "Title = '$Title', ";
                 $fieldname .= "Description = '".$li->Get_Safe_Sql_Query($Description)."', ";
                 $fieldname .= "AnnouncementDate = '$AnnouncementDate', ";
                 $fieldname .= "EndDate = '$EndDate',";
				 $fieldname .= "RecordType = '$publicdisplay', ";
                 $fieldname .= "RecordStatus = '$RecordStatus', ";
//                  $fieldname .= "PublicStatus = '$PublicStatus', ";
                 $fieldname .= "onTop = '$onTop', ";
//               $fieldname .= "Internal = '$internal',";
                 $fieldname .= "UserID = '$UserID',";
                 $fieldname .= "DateModified = now()";
                 $sql = "UPDATE INTRANET_ANNOUNCEMENT SET $fieldname WHERE AnnouncementID = '$AnnouncementID'";
                 
                 $li->db_db_query($sql);
             }
# ---------------------------------
              # Remove all group announcement first
              $sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
              $li->db_db_query($sql);

              if ($internal == 1)
              {
                  # New record added back for this group only
                  $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ($GroupID, $AnnouncementID)";
                  $li->db_db_query($sql);
              }
              else
              {
                  if (sizeof($TargetGroupID)!= 0)
                  {
                      $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ";
                      $delimiter = "";
                      for ($i=0; $i<sizeof($TargetGroupID); $i++)
                      {
                           $sql .= "$delimiter (".$TargetGroupID[$i].", $AnnouncementID)";
                           $delimiter = ",";
                      }
                      $li->db_db_query($sql);
                  }
              }
/*
              $isSchoolAnnouncement = $la->isSchoolAnnouncement();
              if ($public==1 && $isSchoolAnnouncement == 0)
              {
                  $sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = $AnnouncementID";
                  $li->db_db_query($sql);
              }
              if ($public != 1 && $isSchoolAnnouncement == 1)
              {
                  $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ($agid,$AnnouncementID)";
                  $li->db_db_query($sql);
              }
              */
/*
              $sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
              $li->db_db_query($sql);

              for($i=0; $i<sizeof($GroupID); $i++){
                      $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$GroupID[$i].", $AnnouncementID)";
                      $li->db_db_query($sql);
              }
*/

           # --------------------------
           # Process attachments
			  # files to delete
			  //$file2delete = explode(":",$deleted_files);
			  
			  $hasAttachment = false;
			  $targetFolder = session_id()."_a";
              if ($MakeNew == 1)
              {
                  $folder = session_id()."_a";
                  $sql = "SELECT Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
                  $result = $li->returnArray($sql,1);
                  if($result[0][0]==""){
	                  $hasAttachment = false;
	              }else{
		               $hasAttachment = true;
                       $attachmentPath = "$file_path/file/announcement/".$result[0][0].$AnnouncementID."/";
		          }
                  $newpath = "$file_path/file/announcement/$folder$newAnnouncementID/";
                  
				  # if there exists attachments(files) in the current announcement (directory)
                  if($hasAttachment)
                  {
                      # copy all files from current directory to new announcement directory
	                  $lf->lfs_copy($attachmentPath,$newpath);
                      
	                  # delete files marked by user from new announcement directory
                      for ($i=0; $i<sizeof($file2delete); $i++)
                      {
                           if (urldecode($file2delete[$i])!="")
                           {
                               $target = $newpath.urldecode($file2delete[$i]);
                               $lf->lfs_remove($target);
                           }
                      }
                      # remove the new directory if it is empty 

                      $size = $lf->folder_size($newpath);

                      if ($size[1]==0)
                      {
                          $lf->folder_remove($newpath);
                          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = NULL WHERE AnnouncementID = '$newAnnouncementID'";
                          $li->db_db_query($sql);
            	          $hasAttachment = false;
                      }
                      else
                      {
                          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = '$newAnnouncementID'";
                          $li->db_db_query($sql);
                      }
                  } // end if(hasAttachment)
                  
                  # Handle announcement require approval
                  if ($special_feature['announcement_approval'] && $RecordStatus == 2)
                  {
                      # Get Admin UserName first
                      $name_field = getNameFieldWithClassNumberForRecord("");
                      $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$AdminUserID'";
                      $temp = $li->returnVector($sql);
                      $username = $temp[0];
                      $sql = "INSERT INTO INTRANET_ANNOUNCEMENT_APPROVAL (AnnouncementID, UserID, ApprovalUserName, RecordStatus, DateInput, DateModified)
                              VALUES ($newAnnouncementID, $AdminUserID, '$username', 2, now(),now())";
                      $li->db_db_query($sql);
                  }
              }
              else
              {
	              $sql = "SELECT Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
                  $result = $li->returnArray($sql,1);
                  if($result[0][0]=="") {
	                 $hasAttachment = false;
	              }
                  else{
	                   $hasAttachment=true;
	                   $targetFolder = $result[0][0];
	              }
                  $attachmentPath = "$file_path/file/announcement/".$targetFolder.$AnnouncementID."/";
				  
                  # if there exists files to delete AND the attachment directory is exists	
                  if (sizeof($file2delete) != 0 && $hasAttachment)
                  {
                      for ($i=0; $i<sizeof($file2delete); $i++)
                      {
                           if (urldecode($file2delete[$i])!="")
                           {
                              $target = $attachmentPath.urldecode($file2delete[$i]);
                           	  $lf->lfs_remove($target);
                           }
	                  }
	                  # remove the dir if it is empty
                      $size = $lf->folder_size($attachmentPath);
                      if ($size[1]==0)
                      {
                          $lf->folder_remove($attachmentPath);
                          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = NULL WHERE AnnouncementID = '$AnnouncementID'";
                          $li->db_db_query($sql);
                          $hasAttachment = false;
                      }
                  }
                  # Handle announcement require approval
                  if ($special_feature['announcement_approval'] && $RecordStatus == 2)
                  {
                      # Get Admin UserName first
                      $name_field = getNameFieldWithClassNumberForRecord("");
                      $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$AdminUserID'";
                      $temp = $li->returnVector($sql);
                      $username = $temp[0];
                      if ($originalStatus==2)       # Waiting
                      {
                          $sql = "UPDATE INTRANET_ANNOUNCEMENT_APPROVAL SET UserID = '$AdminUserID', ApprovalUserName = '$username', Remark = '', RecordStatus = 2, DateModified = now() WHERE AnnouncementID = '$AnnouncementID' AND UserID != '$AdminUserID'";
                          $li->db_db_query($sql);
                      }
                      else     # Pending/published
                      {
                          $sql = "INSERT INTO INTRANET_ANNOUNCEMENT_APPROVAL (AnnouncementID, UserID, ApprovalUserName, RecordStatus, DateInput, DateModified)
                                  VALUES ($AnnouncementID, $AdminUserID, '$username', 2, now(),now())";
                          $li->db_db_query($sql);
                      }
                  }
              }
         
	     # num of newly attached files
	     $attachment_size=$attachment_size==""?0:$attachment_size;	
	     
         # Upload Files
	     $update_db = false;
	     $attachmentPath = $MakeNew==1?$newpath:$attachmentPath;
	     $announce_id = $MakeNew==1?$newAnnouncementID:$AnnouncementID;
	     for ($i=1; $i<=$attachment_size; $i++)
	     {
	          $key = "filea$i";
	          $loc = ${"filea$i"};
	          $file = stripslashes(${"hidden_userfile_name$i"});
	          #$file = ${"filea$i"."_name"};
	          $des = "$attachmentPath/$file";
	          if ($loc == "none" || $loc=="")
	          {
	              // do nothing
	          }
	          else
	          {

	             if (strpos($file,"."==0))
	             {
	                 // do nothing
	             }
	             else
	             {
	                   if (!$hasAttachment)
	                   {
	                        //$lf->folder_new("$file_path/file/announcement");
	                        $lf->folder_new ($attachmentPath);
	                        $hasAttachment = true;
	                        $update_db = true;
	                   }
	                   $lf->lfs_copy($loc, $des);
	             }
	          }
	     }
	     # Update attachment in DB
	     if ($update_db)
	     {
	          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$targetFolder' WHERE AnnouncementID = '$announce_id'";
	          $li->db_db_query($sql);
	     }
# ----------------------------------

              # Call sendmail function
              if($email_alert==1 && $RecordStatus==1)
              {
                $public = ($internal!=1 && sizeof($TargetGroupID)==0);
				$type = (sizeof($GroupID)==0? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
				
				if (sizeof($GroupID)>0)
				{
					$emailTargetGroups = ($internal==1? array($GroupID): $TargetGroupID);
					$Groups = $li->returnGroupNames($emailTargetGroups);
				}
				
				include_once($PATH_WRT_ROOT."includes/libegroup.php");
				$legroup = new libegroup();
				list($mailSubject, $mailBody) = $legroup->returnEmailNotificationData($AnnouncementDate, $pushMsgTitle, $type, $Groups);
	            
                $lu = new libwebmail();
                
				if($public) {
					$UserIDs = $li->returnUserIDWithCondition();
				} else {
					$UserIDs = $li->returnUserIDByGroup($emailTargetGroups);
				}
				$receiver = array_unique($UserIDs);
				$lu->sendModuleMail($UserIDs,$mailSubject,$mailBody);
              }
              
              header("Location: settings.php?GroupID=$GroupID&filter=$RecordStatus&msg=update");
         }
         else
         {
             header ("Location: edit.php?AnnouncementID[]=$AnnouncementID&invalid=1");
         }
}
else
{
    header ("Location: ../close.php");
}

intranet_closedb();
?>