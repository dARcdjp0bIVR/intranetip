<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libannounce.php");
include("../../../includes/libgrouping.php");
include("../../../templates/fileheader.php");
include("../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$AnnouncementID = IntegerSafe($AnnouncementID);

#$Title = stripslashes($Title);
#$Description = stripslashes($Description);

$Title = intranet_htmlspecialchars(trim(stripslashes($Title)));
$Description = intranet_htmlspecialchars(trim(stripslashes($Description)));
$AnnouncementDate = intranet_htmlspecialchars(trim(stripslashes($AnnouncementDate)));
$EndDate = intranet_htmlspecialchars(trim(stripslashes($EndDate)));
$aid = $AnnouncementID;

$la = new libannounce($aid);
$agid = $la->OwnerGroupID;
if ($agid == "")
{
    $valid = false;
}
else
{
    $lg = new libgrouping();
    if ($lg->isGroupAdmin($UserID,$agid))
        $valid = true;
    else $valid = false;
}
if ($valid)
{
$no_file = 5;


echo generateFileUploadNameHandler("form1","filea1","hidden_userfile_name1"
,"filea2","hidden_userfile_name2"
,"filea3","hidden_userfile_name3"
,"filea4","hidden_userfile_name4"
,"filea5","hidden_userfile_name5"
);

?>

<form name=form1 action="attach_update.php" method="post" enctype="multipart/form-data" onsubmit="return Big5FileUploadHandler();">
<table width=90% border=0 cellpadding=0 cellspacing=0 bgcolor=#EFF7DE>
<tr><td><?php echo $i_AnnouncementNewAttachment; ?></td></tr>
<tr>
<td align=center>
<br>
<table width=90% border=0 cellpadding=5 cellspacing=1>
<tr>
<td align=center>
<?php for($i=1;$i<=$no_file;$i++){ ?>
<input class=file type=file name="filea<?php echo $i; ?>" size=20>
<input type=hidden name="hidden_userfile_name<?=$i?>"><br>
<?php } ?>
</td>
</tr>
</table>
<br>
<input class=submit type=submit value="<?php echo $button_upload; ?>"><input class=button type=button value="<?php echo $button_close; ?>" onClick=self.close()></td></tr>
</td>
</tr>
<tr height=10><td></td></tr>
</table>
<input type=hidden name=aid value="<?php echo $aid; ?>">
<input type=hidden name=Title value="<?=$Title?> ">
<input type=hidden name=Description value="<?=$Description?>">
<input type=hidden name=AnnouncementDate value="<?=$AnnouncementDate?>">
<input type=hidden name=EndDate value="<?=$EndDate?>">
</form>

<?php
}
else
{?>
<SCRIPT LANGUAGE=JAVASCRIPT>

par = opener.window;
par.close();
self.close();
</SCRIPT>

<?php
}
intranet_closedb();
include("../../../templates/filefooter.php");
?>