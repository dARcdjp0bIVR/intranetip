<?php
# using: yat

############## Change Log
#	Date:	2016-11-14	Villa
#			- updated the UI
#
#	Date:	2014-07-11	Bill
#			- add flag to check target radio button
#
#	Date:	2012-07-18	YatWoon
#			- use GET_DATE_PICKER for date selection
#			- according to the School news setting to set the default end date
#
#	Date:	2011-08-22	YAtWoon
#			update permission checking for "Seek for approval" [Case: 2011-0815-1010-00066]
#
#	Date:	2010-08-24 YatWoon
#			hide Public/Private
#
#	Date:	2010-08-12	YatWoon
#			change the description from textarea to fck editor (with upload image with Flash function)
#
######################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");
//include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");

intranet_opendb();

$GroupID = IntegerSafe($GroupID);


$linterface 	= new interface_html();
//$lgroup = new libgroup($GroupID);
$legroup = new libegroup($GroupID);
$lo = new libgrouping();
$lannounce	= new libannounce();
$lfilesystem = new libfilesystem();
 
$GroupsID = $legroup->hasAnyGroupAdminRight();
if(!sizeof($GroupsID))
{
	header("Location: ../group/?GroupID=$GroupID");
	exit;
}

$lschoolnews = new libschoolnews();
$today = date('Y-m-d');
$temp = strtotime($lschoolnews->defaultNumDays.' day',  strtotime($today));
$preEndDate = date('Y-m-d',$temp);

if(!$GroupID)	
{
	$GroupID = $GroupsID[0];
	$legroup = new libegroup($GroupID);
}

if(!in_array($GroupID, $GroupsID) || (!( $legroup->hasAdminAllAnnounce($UserID) || substr($legroup->AdminAccessRight,2,1) || $legroup->AdminAccessRight=='ALL')))
{
	header("Location: ../settings/index.php?GroupID=$GroupID");
	exit;
}


// $hasAccessRight = $lannounce->hasAccessRight($UserID);
// if(!$hasAccessRight)
// {
// 	header ("Location: /");
// 	intranet_closedb();
// 	exit();
// }

$CurrentPage	= "Settings_AnnouncementSettings";
$MyGroupSelection= $legroup->getSelectAdminAnnounce("name='SelectGroupID' onChange=\"window.location='?GroupID='+this.value\"", $GroupID);

if ($legroup->hasAdminAllAnnounce($UserID) && $legroup->AnnounceAllowed) 
{
    $public_row = '
		<!-- Groups -->
		<div id="groupSelectionBox" style="display:none;">
			<table width="90%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td valign="top">
						'.$i_admintitle_group.'
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						'.$lo->displayAnnouncementGroups(0,1,0,$GroupID).'
					</td>
				</tr>
			</table>
		</div>
	';
}				
    
$no_file = 1;

### Title ###
$TAGS_OBJ[] = array($eComm['Management'],"settings.php?GroupID=$GroupID", 1);
$TAGS_OBJ[] = array($i_general_BasicSettings,"bsettings.php?GroupID=$GroupID", 0);
$title = "
	<table width='100%' border='0' cellpadding='0' cellspacing='0' valign=\"bottom\">
		<tr>
			<td valign='bottom' align='right' width='100%'>". $legroup->displayStorage3() ."</td>
			<td valign='bottom' align='right' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_back_group.gif' border='0' align='absmiddle' onclick='window.location.href=\"../group/index.php?GroupID=$GroupID\"' title='".$lu->Title."' class='GroupTitleLink'> {$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ_RIGHT[] = array($title,"",0);
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_adminmenu_announcement, "");
?>

<script language="javascript">
var no_of_upload_file = <? echo $no_file==""?5:$no_file;?>;
$(function(){
	if(!$('#publicdisplay').attr("checked")){
		$('div#email_alert1_div').show();
	}else{
		<?php if($sys_custom['schoolNews']['enableSendingEmailToAllUser']){?>
		$('div#email_alert1_div').show();
		<?php }else{?>
		$('div#email_alert1_div').hide();
		$('input#email_alert1').removeAttr("checked");
		<?php }?>
	}
});
function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}

function checkOption1(obj){
        for(i=0; i<obj.length; i++){
                if(!parseInt(obj.options[i].value)){
                        obj.options[i] = null;
                }
        }
}

function checkform(obj)
{
		var box = document.getElementById("GroupID");
 		if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
 		if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
 		if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$i_EndDate; ?>.")) return false;
 		if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>.")) return false;
		if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
		
		if(compareDate(obj.EndDate.value,obj.AnnouncementDate.value)<0) 
		{
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		
		var today = new Date();
		var todayStr = today.getFullYear()+"-"+((today.getMonth()+1)<10?"0":"")+(today.getMonth()+1)+"-"+today.getDate();
		if(compareDate(obj.AnnouncementDate.value,todayStr)<0) 
		{
			alert ("<?=$Lang['Group']['WarnStratDayPassed']?>"); 
			return false;
		}

		
		<? if ($legroup->hasAdminAllAnnounce($UserID) && $legroup->AnnounceAllowed) { ?>
			
				
				checkOption1(box);
				checkOptionAll(box);
/*				if(box.options.length<=0)
				{
					alert("<?=$Lang['Group']['WarnAddGroup']?>");
					return false;
				}
*/
		<? } ?>
		return Big5FileUploadHandler();
}

function Big5FileUploadHandler() {
		 attach_size = parseInt(document.form1.attachment_size.value);
		 for(i=0;i<=attach_size;i++){
			 	objFile = eval('document.form1.filea'+i);
			 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
			 	if(objFile!=null && objFile.value!='' && objUserFile!=null){
				 	var Ary = objFile.value.split('\\');
				 	objUserFile.value = Ary[Ary.length-1];
				}
		 }
		 return true;
	}

function add_field()
{
	     objTable = document.getElementById("upload_files");
	     attach_size = parseInt(document.form1.attachment_size.value)+1;
		 row = objTable.insertRow(-1);
	     cell = row.insertCell(0);
	     x='<input class=file type=file name="filea'+attach_size+'">';
	     x+='<input type=hidden name="hidden_userfile_name'+attach_size+'">';
	     cell.innerHTML = x;
		 document.form1.attachment_size.value = attach_size;
}

function SwitchPublic(isPublic)
{
	if(isPublic)
	{
		$("#AdminUserID").show();
		$("#GroupID").attr("disabled","disabled");
		$("#groupSelectionBox").hide();
		<?php if($sys_custom['schoolNews']['enableSendingEmailToAllUser']){?>
		$('div#email_alert1_div').show();
		<?php }else{?>
		$('input#email_alert1').removeAttr("checked");
		$('div#email_alert1_div').hide();
		<?php }?>
	}
	else
	{
		$("#AdminUserID").hide();
		$("#groupSelectionBox").show();	
		$("#GroupID").attr("disabled","");
		$('div#email_alert1_div').show();
	}
}

function SwitchStatus(isPublish){
	if(isPublish){
		$('input#email_alert1').removeAttr("disabled");
		
	}else{
		$('input#email_alert1').attr("disabled","disabled");
	}
}
function ClickApprove(){
	$('input#email_alert1').removeAttr("disabled");
}
</script>

<br />   
<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementDate?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$linterface->GET_DATE_PICKER("AnnouncementDate", $preStartDate)?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementEndDate?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$linterface->GET_DATE_PICKER("EndDate", $preEndDate)?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementTitle?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="Title" type="text" class="textboxtext" maxlength="100" value="<?=$t?>"/></td>
				</tr>
                                
                                <tr valign="top"> 
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementDescription?> </span></td>					
                                        <td>
                                        <? /* ?><textarea name="Description" cols="60" rows=10><?=$d?></textarea><? */ ?>
			  <?
					include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
					$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash");
					$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
					$objHtmlEditor->Create();
						?>
                                        <?//=$linterface->GET_TEXTAREA("Description",$d);?>
                                  </td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementAttachment?> </span></td>
					<td>
                            <table border="0" id="upload_files" cellpadding="0" cellspacing="0">
							<?php 
								for($i=1;$i<=$no_file;$i++){ ?>
									<tr><td><input class="file" type="file" name="filea<?php echo $i; ?>">
									<input type="hidden" name="hidden_userfile_name<?=$i?>"></td></tr>
							<?php } ?>
							</table>
                            <input type=button value=" + " onClick="add_field()">
                                        </td>
				</tr>
				<? /* ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['PublicStatus']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="PublicStatus" value="1" id="PublicStatus1" checked><label for="PublicStatus1"><?=$eComm['Public']?></label>
						<input type="radio" name="PublicStatus" value="0" id="PublicStatus0"><label for="PublicStatus0"><?=$eComm['Private']?></label>
					</td>
				</tr>
                <? */ ?>   
                
<!--                 Target -->
  <?if($legroup->hasAdminAllAnnounce($UserID)){?>
				 <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=	$Lang['SchoolNews']['target']?> </span></td>
					<td class="tabletext">
					
		              <input type="radio" name="publicdisplay" id="publicdisplay" onClick="SwitchPublic(this.checked)" value="1" CHECKED> <label for="publicdisplay"><?=$i_Payment_All_Users?></label>
		               <input type="radio" name="publicdisplay" id="specific" onClick="SwitchPublic(publicdisplay.checked)" value="0" > <label for="specific"><?=$Lang['SchoolNews']['SpecificGroup']?></label>
		             
		              <table>
						 <tr>
		              		<td colspan='2'>
							<?=$public_row?>
							</td>
						</tr>
					</table>
					</td>
					
				</tr>
				 <?}?>
<!--                 Status        -->
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_AnnouncementRecordStatus?> </span></td>
					<td class="tabletext">
                	<input type="radio" name="RecordStatus" value="1" CHECKED id="RecordStatus1" onClick="SwitchStatus(RecordStatus1.checked)"> <label for="RecordStatus1"><?php echo $i_status_publish; ?></label> <input type="radio" name="RecordStatus" value="0" id="RecordStatus0" onClick="SwitchStatus(RecordStatus1.checked)"> <label for="RecordStatus0"><?php echo $i_status_pending; ?></label>
					<?
		              //if ($special_feature['announcement_approval'] && $_SESSION['SSV_USER_ACCESS']['other-schoolNews']  && $legroup->hasAdminAllAnnounce($UserID))
		              if ($special_feature['announcement_approval'] && $legroup->hasAdminAllAnnounce($UserID))
		              {
		                 // $approveUsers = $ladminjob->returnAnnouncementAdminUsers();
		                  $approveUsers = $lannounce->returnAnnouncementAdminUsers();
		                  
		                  if (sizeof($approveUsers)!=0)
		                  {
		                      $select_approval = getSelectByArray($approveUsers,"name=AdminUserID  onchange='$(\"#RecordStatus2\").attr(\"checked\",\"true\")'","",1,1);
		              ?>
		              <br><span id='AdminUserID'><input type="radio" name="RecordStatus" value="2" 
		              <?php if($sys_custom['plkyls']['eCommunity']['AnnouncementDefault']) echo CHECKED;?> id="RecordStatus2" onClick="ClickApprove()">
		              <label for="RecordStatus2"><?=$Lang['eComm']['SelectApproval']?></label> <?=$select_approval?></span>
		              <?
		                  }
		              }
		              ?>
		              <br />
		              <div id="email_alert1_div">
		              <input type="checkbox" name="email_alert" value="1" id="email_alert1"> <label for="email_alert1"><?php echo $i_AnnouncementAlert; ?></label><br/>
		            </div>
					</td>
	              </tr>
	            
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eComm['DisplayOnTop']?> </span></td>
					<td class="tabletext">
						<input type="checkbox" name="onTop" value="1">
					</td>
				</tr>
				
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>

<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
                		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.go(-1)","cancelbtn") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>

<input type="hidden" name="CurrGroupID" value="<?=$GroupID?>">
<input type="hidden" name="attachment_size" value="<? echo $no_file==""?5:$no_file;?>">
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.AnnouncementDate");
$linterface->LAYOUT_STOP();
?>
