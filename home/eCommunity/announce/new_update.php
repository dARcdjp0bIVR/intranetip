<?php
# Using: 

############ Change Log [Start] ############
#
#   Date:   2018-11-15  Bill    [2018-1105-1041-02206]
#           fixed: email title display html specialchars
#
#	Date:	2016-11-14	Villa
#			update with the change of UI
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2011-04-04	YatWoon
#			change email notification subject & content data 
#
# - 2010-09-30 Marcus
#	add Get_Safe_Sql_Query to Description.
#
#	Date:	2010-08-24 YatWoon
#			hide Public/Private in form and set $PublicStatus = 1
#
# - 2010-8-12 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
# -	2009-12-11 YatWoon
# 	Temporary add checking in Send Email part, if client has external email, then send to external email (temp intranet_user email), 
#	otherwise, send to internal email 
#	P.S. Later need enhance the function for send email part
#
########### Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."lang/email.php");

intranet_auth();
intranet_opendb();

$CurrGroupID = IntegerSafe($CurrGroupID);

$li = new libgrouping();
$lg = new libgroup($CurrGroupID);

if($publicdisplay) {
	unset($GroupID);
}
if ($lg->hasAdminInternalAnnounce($UserID) || $lg->hasAdminAllAnnounce($UserID))
{
    $internal = 0;
    if ($lg->hasAdminAllAnnounce($UserID))
    {
        if (sizeof($GroupID)==1 && $GroupID[0]==$CurrGroupID)
        {
            $internal = 1;
        }
    }
    else
    {
        $internal = 1;
    }

    $Title = intranet_htmlspecialchars(trim($Title));
    $pushMsgTitle = standardizeFormPostValue($_POST['Title']);
    if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
	{
		if ($Description==strip_tags($Description))
		{
			$Description = nl2br($Description);
		}
	}
    $Description = intranet_htmlspecialchars(trim($Description));
     
    $AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
    $EndDate = intranet_htmlspecialchars(trim($EndDate));
	$PublicStatus = 1;
	
    $startStamp = strtotime($AnnouncementDate);
    $endStamp = strtotime($EndDate);
	$onTop = $onTop ? 1 : 0;
	
     if (compareDate($startStamp,$endStamp)>0)   // start > end
     {
         $valid = false;
     }
     else if (compareDate($startStamp,time())<0)      // start < now
     {
          $valid = false;
     }
     else
     {
         $valid = true;
     }

     if ($valid)
     {
	     $lf = new libfilesystem();
	     
          # Set attachment
          $folder = session_id()."_a";

          $name_field = getNameFieldForRecord("");
          $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$UserID'";
          $temp = $li->returnVector($sql);
          $postername = $temp[0];

          $sql = "INSERT INTO INTRANET_ANNOUNCEMENT
                  (Title, Description, AnnouncementDate, RecordStatus, DateInput, DateModified, EndDate, OwnerGroupID, UserID, Internal, ReadFlag,PosterName, PublicStatus, onTop) VALUES
                  ('$Title', '$Description', '$AnnouncementDate', '$RecordStatus', now(), now(),'$EndDate', '$CurrGroupID', '$UserID', '$internal',';$UserID;','$postername', '$PublicStatus', '$onTop')";
          $li->db_db_query($sql);
          $AnnouncementID = $li->db_insert_id();
							//debug(sizeof($GroupID));
		
			## Update description if user upload image with Flash upload (fck) [Start]
			$Description2 = stripslashes(htmlspecialchars_decode($Description));
			$Description_updated = ($lf->copy_fck_flash_image_upload($AnnouncementID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolNews'])); 
			$Description_updated = trim(intranet_htmlspecialchars($Description_updated));

			
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET Description = '".$li->Get_Safe_Sql_Query($Description_updated)."' WHERE AnnouncementID = '$AnnouncementID'";
	 		$li->db_db_query($sql);
			## Update description if user upload image with Flash upload (fck) [End]
		
          if ($internal != 1 && sizeof($GroupID)!=0)
          {
              $group_announce_field = "";
              $delimiter = "";
              for ($i=0; $i<sizeof($GroupID); $i++)
              {
                   $group_announce_field .= "$delimiter(".$GroupID[$i].",$AnnouncementID)";
                   $delimiter = ",";
              }
              //$sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$CurrGroupID.", $AnnouncementID)";
              $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES $group_announce_field";
              $li->db_db_query($sql);
          }
          else if ($internal == 1)
          {
               $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ($CurrGroupID,$AnnouncementID)";
               $li->db_db_query($sql);
			   //debug($sql);
          }

          $path = "$file_path/file/announcement/$folder$AnnouncementID";
          
          # Copy files
          $hasAttachment = false;
		  $attachment_size = $attachment_size==''?5:$attachment_size;
          for ($i=1; $i<=$attachment_size; $i++)
          {
               $key = "filea$i";
               $loc = ${"filea$i"};
               
               #$file = ${"filea$i"."_name"};
               $file = stripslashes(${"hidden_userfile_name$i"});
               $des = "$path/$file";
               if ($loc == "none" || trim($loc) == "")
               {
                   // do nothing
               }
               else
               {
                  if (strpos($file,"."==0))
                  {
                       // do nothing
                  }
                  else
                  {
                        if (!$hasAttachment)
                        {
                             //$lf->folder_new("$file_path/file/announcement");
                             $lf->folder_new ($path);
                             $hasAttachment = true;
                        }
                        $lf->lfs_copy($loc, $des);
                  }
               }
          }

          # Update attachment in DB
          if ($hasAttachment)
          {
               $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = '$AnnouncementID'";
               $li->db_db_query($sql);
          }

          # Handle announcement require approval
          if ($special_feature['announcement_approval'] && $RecordStatus == 2)
          {
              # Get Admin UserName first
              $name_field = getNameFieldWithClassNumberForRecord("");
              $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$AdminUserID'";
              $temp = $li->returnVector($sql);
              $username = $temp[0];
              $sql = "INSERT INTO INTRANET_ANNOUNCEMENT_APPROVAL (AnnouncementID, UserID, ApprovalUserName, RecordStatus, DateInput, DateModified)
                      VALUES ($AnnouncementID, $AdminUserID, '$username', 2, now(),now())";
              $li->db_db_query($sql);
          }

          # Call sendmail function
          if($email_alert==1 && $RecordStatus == 1)
          {
			$public = ($internal!=1 && sizeof($GroupID)==0);
			$type = ($public ? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
			
			if (sizeof($GroupID)>0)
			{
				$emailTargetGroups = ($internal==1? array($CurrGroupID): $GroupID);
				$Groups = $li->returnGroupNames($emailTargetGroups);
			}
			
			include_once($PATH_WRT_ROOT."includes/libegroup.php");
			$legroup = new libegroup();
			list($mailSubject, $mailBody) = $legroup->returnEmailNotificationData($AnnouncementDate, $pushMsgTitle, $type, $Groups);
			
			include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			$lu = new libwebmail();
			
			if($public) {
				$UserIDs = $li->returnUserIDWithCondition();
			} else {
				$UserIDs = $li->returnUserIDByGroup($emailTargetGroups);
			}
			$receiver = array_unique($UserIDs);
			$lu->sendModuleMail($UserIDs,$mailSubject,$mailBody);
          }
          
          header("Location: settings.php?filter=$RecordStatus&msg=add&GroupID=$CurrGroupID");
     }
     else
     {
         header("Location: new.php?GroupID=$CurrGroupID&t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate");
     }
}
else
{
    header ("Location: ../close.php");
}

intranet_closedb();
?>