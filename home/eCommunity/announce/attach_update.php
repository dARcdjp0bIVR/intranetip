<?php
include("../../../includes/global.php");
include("../../../includes/libfilesystem.php");
include("../../../includes/libfiletable.php");
include("../../../includes/libdb.php");
include("../../../includes/libannounce.php");
include("../../../includes/libgrouping.php");
include("../../../lang/lang.$intranet_session_language.php");
$body_tags = "onload=auto()";
include("../../../templates/fileheader.php");
intranet_auth();
intranet_opendb();

$aid = IntegerSafe($aid);

$la = new libannounce($aid);
$agid = $la->OwnerGroupID;
if ($agid == "")
{
    $valid = false;
}
else
{
    $lg = new libgrouping();
    if ($lg->isGroupAdmin($UserID,$agid))
        $valid = true;
    else $valid = false;
}
if ($valid)
{

$li = new libfilesystem();

# Set attachment path
if ($la->Attachment == "")
{
    $folder = session_id().".a";
    $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = '$aid'";
    $la->db_db_query($sql);
}
else
    $folder = $la->Attachment;

$no_file = 5;
$path = "$file_path/file/announcement/$folder".$la->AnnouncementID;

$li->folder_new($path);
for ($i=1; $i<=5; $i++)
{
     $key = "filea$i";
     $loc = ${"filea$i"};
     #$file = ${"filea$i"."_name"};
     $file = stripslashes(${"hidden_userfile_name$i"});

     $des = "$path/$file";
     if ($loc == "none" || $loc =="")
     {} else
     {
        if (strpos($file,"."===false)){
        } else
        {
              $li->lfs_copy($loc, $des);
        }
     }
}

$queryString = "&ea=1&Title=$Title&Description=$Description&AnnouncementDate=$AnnouncementDate&EndDate=$EndDate";
/*
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

*/
?>
<script language="JavaScript1.2">
function auto()
{
         autoform.target = "intranet_popup10"; //opener.window;
         autoform.submit();
         self.close();
}
</script>
<form ACTION="edit.php" name=autoform method=post>
<input type=hidden name=ea value=1>
<input type=hidden name=AnnouncementID value="<?php echo $aid; ?>">
<input type=hidden name=Title value="<?=stripslashes($Title)?>">
<input type=hidden name=Description value="<?=stripslashes($Description)?>">
<input type=hidden name=AnnouncementDate value="<?=$AnnouncementDate?>">
<input type=hidden name=EndDate value="<?=$EndDate?>">
<input type=hidden name=GroupID value="<?=$agid?>">
</form>
<?php
}
else
{?>
<script language="JavaScript1.2">
par = opener.window;
par.close();
self.close();
</script>
<?php
}
intranet_closedb();
include("../../../templates/filefooter.php");
?>