<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$GroupID = IntegerSafe($GroupID);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "MyGroups";
$legroup 		= new libegroup();
$lu = new libgroup($GroupID);


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE SQL


if ($lu->isAccessBulletin())
{
    $isBulletinAdmin = $lu->hasAdminBulletin($UserID);
    $isAdmin = $isBulletinAdmin;
   
    //$deleteIcon = ($isAdmin? ",'<a href=javascript:del(', a.BulletinID,')><img src=../../../images/eraser_icon.gif alt=$button_remove hspace=20 vspace=2 border=0 align=absmiddle></a>'":""); 
    if ($isBulletinAdmin)
    {
		$AddBtn 	= "<a href=\"javascript:newFolder()\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $eComm['New'.$FolderTitle] . "</a>";
		$delBtn 	= "<a href=\"javascript:checkRemoveThis(document.form1,'BulletinIDArr[]','removeThread.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
		$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BulletinIDArr[]','folder_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

	}
    
    
	if($field=="") $field=2;
	switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 2; break;
	}
}


$order = ($order == 1) ? 1 : 0;
$sql = "
SELECT					if (c.PublicStatus=1 ,'&nbsp;', CONCAT('<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif\">')) ,
                        CONCAT('<a href=javascript:bulletin_view(', a.BulletinID, ') class=\"tablelink\">', a.Subject, '</a>',if(locate(';$UserID;',a.ReadFlag)=0, '<img src=$image_path/new.gif border=0 hspace=2>', '') $deleteIcon) as topic,
                        if (e.PhotoLink!=\"\", CONCAT('<img src=\"',e.PhotoLink,'\" width=\"25\" height=\"30\">'), CONCAT('<img src=\"/images/myaccount_personalinfo/samplephoto.gif\" width=\"25\" height=\"30\">')),
                        a.UserName AS author,
                        if(COUNT(d.BulletinID)=0, CONCAT(COUNT(c.BulletinID)-1), CONCAT(COUNT(c.BulletinID)-1 )),
                        DATE_FORMAT(MAX(c.DateModified), '%Y-%m-%d %H:%i') AS postdate,
                        CONCAT('<input type=\"checkbox\" name=\"BulletinIDArr[]\" value=\"', a.BulletinID ,'\">')
                FROM INTRANET_BULLETIN AS a
                LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = a.UserID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS c ON (a.BulletinID = c.BulletinID OR a.BulletinID = c.ParentID)
                LEFT OUTER JOIN INTRANET_BULLETIN AS d ON
                        (a.BulletinID = d.BulletinID OR a.BulletinID = d.ParentID) AND
                        c.BulletinID = d.BulletinID AND
                        d.ParentID<>0 AND
                        d.ReadFlag not like '%;$UserID;%'
                WHERE a.GroupID = '$GroupID' AND a.ParentID = 0
                GROUP BY a.BulletinID
                
                
";

# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("topic","author","postdate","response");
$li->sql = $sql;


$li->IsColOff = "eCommForumList";
//$li->IsColOff = "displayFormat_eCommForumList";
$li->no_msg = $i_frontpage_bulletin_no_record;


// TABLE COLUMN

$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"18\" nowrap></td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' nowrap>$eComm_Field_Topic</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"20\" nowrap></td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' nowrap>$eComm_Field_Createdby</td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"80\" nowrap>$eComm_Field_ReplyNo </td>\n";
$li->column_list .= "<td class='forumtabletop forumtabletoptext' width=\"100\" nowrap>$eComm_Field_LastUpdated</td>\n";
if ($isBulletinAdmin)
{
	$li->column_list .= "<td width=1 class=\"forumtabletop forumtabletoptext\">".$li->check("BulletinIDArr[]")."</td>\n";
	$li->no_col = 6;
}else
	$li->no_col = 5;

$searchbar = "<input class=text type=text name=keyword size=15 maxlength=30>\n";
$searchbar .= $linterface->GET_ACTION_BTN($eComm['Search'], "submit", "","button"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."\n";
$toolbar = "<a href=javascript:checkPost(document.form1,'new.php') class=\"tablelink\">".newIcon2()."$i_frontpage_bulletin_postnew</a>".toolBarSpacer();
$toolbar .= "<a href=javascript:checkPost(document.form1,'mark.php') class=\"tablelink\"><img src=../../../images/quickadd_icon.gif border=0 hspace=5 vspace=0 align=absmiddle>$i_frontpage_bulletin_markallread</a>";


### Button

$ForumIcon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_forum.gif\" width=\"25\" height=\"20\" align=\"absmiddle\" /><span class=\"ecomm_forum_title\">".$eComm['Forum']."</span>";

$MyGroupSelection= $lu->getSelectFilesGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle'><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle' /> {$lu->Title}</td>
			<td valign='bottom' align='right'>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
function bulletin_view(id){
        obj = document.form1;
        obj.BulletinID.value = id;
        obj.action = "message.php";
        obj.submit();
}
function del(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$eComm['jsDeleteRecord']?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}

</script>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="33"><img src="images/2007a/content_01_t.gif" width="13" height="33"></td>
<td height="33" valign="bottom" background="images/2007a/content_02_t.gif" class="imailpagetitle"><table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
<tr>
<td align="left">&nbsp;<span class="contenttitle"><img src="images/2007a/ecomm/icon_egroup_school.gif" width="20" height="20" align="absmiddle"> Basketball Club </span> </td>
<td align="right"><label>
  <select name="select" class="tabletext">
    <option>Basketball Club</option>
  </select>
</label></td>
</tr>
</table></td>
<td width="11" height="33"><img src="images/2007a/content_03_t.gif" width="11" height="33"></td>
</tr>


</table>

 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>