<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libuser.php");
intranet_auth();
intranet_opendb();

$Title = htmlspecialchars($Title);
$URL = htmlspecialchars($URL);
$Keyword = htmlspecialchars($Keyword);
$Description = htmlspecialchars($Description);

$GroupID = IntegerSafe($GroupID);
$PostGroupID = IntegerSafe($PostGroupID);

$li = new libuser($UserID);

for($i=0; $i<sizeof($PostGroupID); $i++){
     if($li->IsGroup($PostGroupID[$i])){
          $sql = "INSERT INTO INTRANET_LINK (GroupID, UserID, UserName, UserEmail, Title, URL, Keyword, Description, ReadFlag, VoteNum, VoteTot, DateInput, DateModified) VALUES (".$PostGroupID[$i].", $UserID, '".addslashes($li->NickNameClassNumber())."', '".$li->UserEmail."', '$Title', '$URL', '$Keyword', '$Description', ';$UserID;', 0, 0, now(), now())";
          $li->db_db_query($sql);
     }
}

intranet_closedb();
header("Location: index.php?GroupID=$GroupID");
?>