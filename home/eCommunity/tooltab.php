<?php
//include_once("$intranet_root/includes/libqb.php");
//$lq = new libqb();

$GroupID = IntegerSafe($GroupID);

$id = $GroupID;
$tooltab_lgroup = new libgroup($id);
$function = $tooltab_lgroup->FunctionAccess;
$type = $tooltab_lgroup->RecordType;
$isAdmin = $tooltab_lgroup->isGroupAdmin($UserID);
$available = returnGroupAvailableFunctions();

if ($function == "ALL")
{
    $isTimetable = ($available[0]!="");
    $isChat = ($available[1]!="");
    $isBulletin = ($available[2]!="");
    $isLink = ($available[3]!="");
    $isFile = ($available[4]!="");
    $isQB = $type == 2 && ($available[5]!="");
    $isPhoto = ($available[6]!="");
    $isSurvey = ($available[7]!="");
}
else
{
    $isTimetable = (substr($function,0,1)=='1' && $available[0]!="");
    $isChat = (substr($function,1,1)=='1' && $available[1]!="");
    $isBulletin = (substr($function,2,1)=='1' && $available[2]!="");
    $isLink = (substr($function,3,1)=='1' && $available[3]!="");
    $isFile = (substr($function,4,1)=='1' && $available[4]!="");
    $isQB = $type == 2 && (substr($function,5,1)=='1' && $available[5]!="");
    $isPhoto = (substr($function,6,1)=='1' && $available[6]!="");
    $isSurvey = (substr($function,7,1)=='1' && $available[7]!="");
}
$functionIcon = "";
if ($isTimetable)
    $functionIcon .= "<a href=\"../timetable/?GroupID=$id\"><img src=/images/icon_timetable.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_timetable'></a>\n";
if ($isChat)
    $functionIcon .= "<a href=\"../chat/?GroupID=$id\"><img src=/images/icon_chatroom.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_chat'></a>\n";
if ($isBulletin)
    $functionIcon .= "<a href=\"../bulletin/?GroupID=$id\"><img src=/images/icon_bulletin.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'></a>\n";
if ($isLink)
    $functionIcon .= "<a href=\"../links/?GroupID=$id\"><img src=/images/icon_sharedlinks.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_links'></a>\n";
if ($isFile)
    $functionIcon .= "<a href=\"../files/?GroupID=$id\"><img src=/images/icon_sharedfiles.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_files'></a>\n";
if ($isQB)
    $functionIcon .= "<a href=\"../qb/?GroupID=$id\"><img src=/images/icon_schoolbasedqb.gif border=0 alt='$i_adminmenu_plugin_qb'></a>\n";
if ($isPhoto)
    $functionIcon .= ""; # Not yet implemented
if ($isSurvey)
    $functionIcon .= ""; # Not yet implemented
if ($isAdmin)
    $functionIcon .= "<a href=\"../settings/?GroupID=$id\"><img src=/images/icon_groupsetting.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_settings'></a>\n";

$header_tool = "
      <table width=\"750\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"$image_path/groupinfo/popup_tbbg_large.gif\">
        <tr>
          <td align=right width=95%>$functionIcon
          </td>
          <td width=5%>&nbsp;</td>
        </tr>
        <tr>
          <td colspan=2 align=center><hr width=90%>
          </td>
        </tr>
      </table>
";
$header_tool_qb = "
      <table width=\"766\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"$image_path/groupinfo/sbqb_tbbg1.gif\">
        <tr>
          <td align=right width=95%>$functionIcon
          </td>
          <td width=5%>&nbsp;</td>
        </tr>
        <tr>
          <td colspan=2 align=center><hr width=90%>
          </td>
        </tr>
      </table>
";
?>