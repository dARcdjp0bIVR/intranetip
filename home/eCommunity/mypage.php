<?php
/*
 * 2017-07-26 (Carlos): Cater https resource url.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libbulletin.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libegroup.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();

$GroupID = IntegerSafe($GroupID);

$legroup 	= new libegroup($GroupID);
$li 		= new libgroup($GroupID);
$la		 	= new libannounce();
$lu2007 	= new libuser2007($UserID);
$lb 		= new libbulletin();

$GroupsID = $li->getGroupsID();
$is_https = function_exists("isSecureHttps")? isSecureHttps() : ($_SERVER["SERVER_PORT"] == 443 || $_SERVER["HTTPS"]=="on" || $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https");
if(!$GroupID)	
{
	$GroupID = $GroupsID[0][0];
	$legroup = new libegroup($GroupID);
}
$li 		= new libgroup($GroupID);
if($lu2007->isInGroup($GroupID))
{
	$CurrentPage	= "MyPage";
	$MyGroupSelection= $li->getSelectGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}
else
{
	header("Location: ../index.php?msg=AccessDenied");
	exit;
//	$CurrentPage	= "OtherGroups";
//	$MyGroupSelection= $li->getSelectOtherGroups("name='GroupID' onChange=\"window.location='?GroupID='+this.value\"",$GroupID);
}

foreach ($GroupsID as $Key=>$Value)
//$forum .= $legroup->Forum($lb->returnMyTopic($GroupsID[$Key]['GroupID'], 100));



### Start Right Menu (Latest Share / Photo / Video / Website / File) ###
$rightMenu = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td height=\"32\" align=\"right\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_01.gif\"><table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td width=\"50%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t1.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t2.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_box_title\">". $eComm['View']."</span></td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_t3.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
          <td align=\"center\" width=\"50%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
              <tr>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
                <td align=\"left\">
";
if($lu2007->isInGroup($GroupID))
	$rightMenu .= "	<a href=\"javascript:jAJAX_RIGHT_MENU(document.rightmenuform, 'add');\"  class=\"share_sub_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">". $eComm['Add'] ."</a>";
else
	$rightMenu .= "&nbsp;";
$rightMenu .= "
				</td>
                <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\" height=\"29\"></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
<br />
";

### End Right Menu ###

### Title ###
$title = "
	<table width='100%' height='28' border='0' cellpadding='0' cellspacing='0'>
		<tr>
			<td valign='bottom' align='left' class='contenttitle' nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/egroup/icon_egroup_school.gif' align='absmiddle' /> <a href='{$PATH_WRT_ROOT}home/school/group/index.php?GroupID={$GroupID}' class='contenttitle'>{$li->Title}</a></td>
			<td valign='bottom' align='right' width='100%'>".$li->displayStorage3()."</td>
			<td valign='bottom' align='right' nowra>{$MyGroupSelection}</td>
		</tr>
	</table>
";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $legroup->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();






?>

<script type="text/javascript" src="<?=$is_https?"https":"http"?>://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$is_https?"https":"http"?>://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>

<script language="javascript">
<!--
function viewfile(fileid)
{
	 with(document.form1)
	 {
		FileID.value = fileid;
		action = "../files/file_view.php";
		submit();
	 }
}


function newFile(t)
{
         with(document.form1)
         {
                action = "../files/"+t+"_new.php";
                submit();
         }
}

var callback_rightmenu = 
{
	success: function (o)
	{
		jChangeContent( "div_right_menu", o.responseText );						
	}
		
}
	
function jAJAX_RIGHT_MENU( jFormObject, mt, ft ) 
{
	jFormObject.menutype.value=mt;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "../files/aj_right_menu.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_rightmenu);		
	
	if(mt=="view")
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_view_02.gif"; 
	else
		document.getElementById('rightmenuimg').src = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/share_board_sub_top_add_02.gif"; 
}

var callback_sharearea = 
{
	success: function (o)
	{
		jChangeContent( "share_div", o.responseText );						
	}
		
}
	
function jAJAX_SHARE_VIEW_MODE( jFormObject, t, s, ft ) 
{
	jFormObject.DisplayType.value=t;
	jFormObject.start.value=s;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_share_area.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_sharearea);		
	
	jAJAX_SHARE_VIEW_MODE_SELECT(document.shareviewmodeform, t, ft);
	
	document.all.ft_span_.className= "share_sub_list";
	document.all.ft_span_p.className= "share_sub_list";
	document.all.ft_span_v.className= "share_sub_list";
	document.all.ft_span_w.className= "share_sub_list";
	document.all.ft_span_f.className= "share_sub_list";
	document.all.ft_span_wa.className= "share_sub_list";
		
	switch(ft)
	{
		case "P":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_photo.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Photo']?>";
			document.all.ft_span_p.className= "share_sub_list_current";
			break;	
		case "V":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_video.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Video']?>";
			document.all.ft_span_v.className= "share_sub_list_current";
			break;	
		case "W":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_website.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['Website']?>";
			document.all.ft_span_w.className= "share_sub_list_current";
			break;	
		case "F":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_file.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['File']?>";
			document.all.ft_span_f.className= "share_sub_list_current";
			break;	
		case "WA":
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_file.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['WaitingRejectList']?>";
			document.all.ft_span_wa.className= "share_sub_list_current";
			break;
		default:
			stitle = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/icon_shares.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <?=$eComm['LatestShare']?>";
			document.all.ft_span_.className= "share_sub_list_current";
			break;	
	}
	
	document.all.share_title.innerHTML = stitle;

	
	
}

var callback_shareareaviewmode = 
{
	success: function (o)
	{
		jChangeContent( "shareviewmode_div", o.responseText );						
	}
}
	
function jAJAX_SHARE_VIEW_MODE_SELECT( jFormObject, t, ft )
{
	jFormObject.DisplayType.value=t;
	jFormObject.FileType.value=ft;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "aj_share_area_view_mode_select.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_shareareaviewmode);		
}

function maxShareArea()
{
	document.maxshareaform.submit();
}

function maxForum()
{
	document.maxForumform.submit();
}

function viewfolder(folderid)
{
	 with(document.viewfolderform)
	 {
		FolderID.value = folderid;
		submit();
	 }
}

function bulletin_view(id){
        obj = document.bulletinform;
        obj.BulletinID.value = id;
        obj.action = "../bulletin/message.php";
        obj.submit();
}

function moreNews(type)
{
	newWindow('../../moreannouncement.php?group=<?=$GroupID?>&type='+type,1);
}

//-->
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
  <td>
  
	<!--------- START TOP ROW ---------//-->
	<table width="100%" height="200" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="245" align="left" valign="top">
		<!--------- START GROUP LOGO ---------//-->
		<?=$group_logo?>
		<!--------- END GROUP LOGO ---------//-->
		</td>
		
		<td valign="top">
		<!--------- START ANNOUNCEMENT TABLE ---------//-->
		<?=$announcement_table?>
		<!--------- END ANNOUNCEMENT TABLE ---------//-->
		</td>
		
		<td width="200" height="200" align="right" valign="top">
		<!--------- START CALENDAR ---------//-->
		<?=$calendar?>
		<!--------- END CALENDAR ---------//-->
		</td>
	</tr>
	</table>
	<!--------- END TOP ROW ---------//-->
	
	
	<!--------- START MAIN ROW ---------//-->
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td width="180" valign="top">
        <!--------- START MEMEBR LIST ---------//-->
        <?=$member_list?>
        <!--------- END MEMEBR LIST ---------//-->
        </td>
        
        <td align="left" valign="top">
        <!--------- START SHARE AREA ---------//-->
        <?=$share_area?>
        <!--------- END SHARE AREA ---------//-->
        
          <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10">
          
          <!--------- START FORUM ---------//-->
          <?=$forum?>
          <!--------- END FORUM ---------//-->
          
          
          </td>
      </tr>
    </table>
    <!--------- START MAIN ROW ---------//-->
	
	
	
</td>
</tr>
</table>

<form name="form1" action="" method="get">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="<?=$FolderID;?>">
<input type="hidden" name="FileID" value="">
</form>

<form name="rightmenuform">
<input type="hidden" name="menutype" value="view">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
<input type="hidden" name="indexpage" value="1">
</form>

<form name="shareform">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="start" value="0">
</form>

<form name="shareviewmodeform">
<input type="hidden" name="DisplayType" value="<?=$DisplayType;?>">
<input type="hidden" name="FileType" value="<?=$FileType;?>">
</form>

<form name="maxshareaform" action="../files/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form>

<form name="maxForumform" action="../bulletin/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
</form>

<form name="viewfolderform" action="../files/index.php" method="get">
<input type="hidden" name="GroupID" value="<?=$GroupID;?>">
<input type="hidden" name="FolderID" value="">
</form>

<form name="bulletinform" method="get">
<input type="hidden" name="BulletinID" value="">
<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
</form>      

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>