<?php
// Editing by 

/*
 * 2018-08-08 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$ts = IntegerSafe($ts);

$li = new libcalevent($ts,$v);

$MODULE_OBJ['title'] = $i_Events;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();
?>

<?=$li->displayEventByDate($ts);?>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>