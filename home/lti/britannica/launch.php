<?php
$PATH_WRT_ROOT = '../../..';

include_once $PATH_WRT_ROOT . "/includes/global.php";
include_once $PATH_WRT_ROOT . "/includes/libdb.php";
include_once $PATH_WRT_ROOT . "/includes/libuser.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/libLtiApp.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/libLtiUser.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/Britannica/libBritannicaSchoolLtiApp.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/Britannica/libBritannicaLaunchPacksScienceLtiApp.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/Britannica/libBritannicaLaunchPacksHumanitiesAndSocialSciencesLtiApp.php";
include_once $PATH_WRT_ROOT . "/includes/LTI/Britannica/libBritannicaImageQuestLtiApp.php";

intranet_auth();
intranet_opendb();

$intranetUrl = rtrim(curPageURL(false, false), '/');

$module         = $_GET['module'];
$resourceLinkId = md5($intranetUrl);
$contextId      = $resourceLinkId;
$locale         = isset($_GET['locale']) ? $_GET['locale'] : 'en';
$documentTarget = 'window';
$returnUrl      = $intranetUrl . '/home/lti/britannica/result.php';
$debug          = $_GET['debug'];

if (
    $module === libBritannicaSchoolLtiApp::MODULE &&
    $plugin['BritannicaSchool']
) {
    $app = new libBritannicaSchoolLtiApp(
        $resourceLinkId, $contextId, $britannicaConfig['consumerKey'], $britannicaConfig['consumerSecret']
    );
} elseif (
    $module === libBritannicaLaunchPacksScienceLtiApp::MODULE &&
    $plugin['BritannicaLaunchPacksScience']
) {
    $app = new libBritannicaLaunchPacksScienceLtiApp(
        $resourceLinkId, $contextId, $britannicaConfig['consumerKey'], $britannicaConfig['consumerSecret']
    );
} elseif (
    $module === libBritannicaLaunchPacksHumanitiesAndSocialSciencesLtiApp::MODULE &&
    $plugin['BritannicaLaunchPacksHumanitiesAndSocialSciences']
) {
    $app = new libBritannicaLaunchPacksHumanitiesAndSocialSciencesLtiApp(
        $resourceLinkId, $contextId, $britannicaConfig['consumerKey'], $britannicaConfig['consumerSecret']
    );
} elseif (
    $module === libBritannicaImageQuestLtiApp::MODULE &&
    $plugin['BritannicaImageQuest']
) {
    $app = new libBritannicaImageQuestLtiApp(
        $resourceLinkId, $contextId, $britannicaConfig['consumerKey'], $britannicaConfig['consumerSecret']
    );
} else {
    echo 'Invalid module, fail to launch LTI app.';
    exit;
}

$user = isset($britannicaConfig['debug']) && $britannicaConfig['debug'] ?
    libLtiUser::createDummyInstructor() :
    libLtiUser::createByLibUser(
        new libuser($_SESSION['UserID']),
        $app->getToolConsumerInfoProductFamilyCode() === 'EJ'
    );

$app->setDebug($debug);
$app->launch($user, $locale, $documentTarget, $returnUrl);

intranet_closedb();