<?php 
//using: 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$ApplyClubEnrolGroupIDAry = explode(',',$EnrolGroupIDList);
$libenroll->retrieveApplicationStatus($StudentID);
$maxStudentWanted = $libenroll->maxStudentWanted;

### Load application data in the library
$libenroll->retrieveApplicationStatus($StudentID, '', 0);

### Get clubs which can be applied
$canBeAppliedClubInfoAry = $libenroll->getGroupsAvailableForApply($StudentID, 0);
$numOfCanBeAppliedClub = count($canBeAppliedClubInfoAry);
$canBeAppliedClubEnrolGroupIdAry = Get_Array_By_Key($canBeAppliedClubInfoAry, 0);

### Get the number of Current Term Club can be applied
$CanBeAppliedCurrentTermClub = $libenroll->getGroupsAvailableForApplyInSpecifiedSemester($canBeAppliedClubEnrolGroupIdAry,"CurrentTerm");
$numOfCanBeAppliedCurrentTermClub = count($CanBeAppliedCurrentTermClub);

### Get the number of Whole Year Club can be applied
$CanBeAppliedWholeYearClub = $libenroll->getGroupsAvailableForApplyInSpecifiedSemester($canBeAppliedClubEnrolGroupIdAry,"WholeYear");
$numOfCanBeAppliedWholeYearClub = count($CanBeAppliedWholeYearClub);



### Get the enrolled Club student info
$enrolledClubInfoAry = $libenroll->Get_Student_Club_Info($StudentID);
$enrolledClubAssoAry = BuildMultiKeyAssoc($enrolledClubInfoAry, 'EnrolGroupID');
$numOfStudentEnrolledClub = count($enrolledClubAssoAry);

//$minMaxAry[$_termNum]['ApplyMin' / 'ApplyMax' / 'EnrollMax'] = data
$minMaxAry = $libenroll->getMinMaxOfStudent($StudentID, $enrolConfigAry['Club']);

$termBasedFormChecking = false;
if (isset($minMaxAry[-1])) {
    // form-based settings
    $minNumOfApp = $minMaxAry[-1]['ApplyMin'];
    $maxNumOfApp = $minMaxAry[-1]['ApplyMax'];
    $maxNumOfEnrol = $minMaxAry[-1]['EnrollMax'];
    
}
else if ($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase') {
    //	$minNumOfApp = $minMaxAry[0];
    //	$maxNumOfApp = $minMaxAry[1];
    //	$maxNumOfEnrol = $minMaxAry[2];
    $minNumOfApp = $minMaxAry[0]['ApplyMin'];
    $maxNumOfApp = $minMaxAry[0]['ApplyMax'];
    $maxNumOfEnrol = $minMaxAry[0]['EnrollMax'];

}
else if ($libenroll->quotaSettingsType=='TermBase') {
    
    $termBasedFormChecking = true;
    $minNumOfApp = array_sum(Get_Array_By_Key(array_values($minMaxAry), 'ApplyMin'));
    
    // #T102993
    // check if the Max. no. of clubs for a student to apply is greater that the num of Availbale Club in year and term respectively
    $numOfCanBeAppliedClub = 0;
    foreach($minMaxAry as $key => $arr ){
        if($key == 0){
            // whole year
            if($arr['ApplyMax'] > $numOfCanBeAppliedWholeYearClub){
                $minMaxAry[$key]['ApplyMax'] = $numOfCanBeAppliedWholeYearClub;
                //in category selection mode
                if ( isset($sel_category) ){
                    $numOfCanBeAppliedClub = $numOfCanBeAppliedWholeYearClub + $numOfCanBeAppliedClub;
                }
            }else{
                //in category selection mode
                if ( isset($sel_category) ){
                    $numOfCanBeAppliedClub = $minMaxAry[$key]['ApplyMax'] + $numOfCanBeAppliedClub;
                }
                //if whole year club can be applied is nolimited
                if(  $minMaxAry[$key]['ApplyMax'] == 0 ){
                    $numOfCanBeAppliedClub = $numOfCanBeAppliedWholeYearClub + $numOfCanBeAppliedClub;
                }
            }
        }else{
            //term
            if($arr['ApplyMax'] > $numOfCanBeAppliedCurrentTermClub){
                
                $minMaxAry[$key]['ApplyMax'] = $numOfCanBeAppliedCurrentTermClub;
                //in category selection mode
                if ( isset($sel_category) ){
                    $numOfCanBeAppliedClub = $numOfCanBeAppliedCurrentTermClub + $numOfCanBeAppliedClub;
                }
            }
            else{
                //in category selection mode
                if ( isset($sel_category) ){
                    $numOfCanBeAppliedClub = $minMaxAry[$key]['ApplyMax'] + $numOfCanBeAppliedClub;
                }
                //if current term club can be applied is nolimited
                if(  $minMaxAry[$key]['ApplyMax'] == 0 ){
                    $numOfCanBeAppliedClub = $numOfCanBeAppliedCurrentTermClub + $numOfCanBeAppliedClub;
                }
                
            }
        }
        
    }
}
// debug_pr($numOfCanBeAppliedClub);


### If there is no upper limit for the number of applied clubs or there is not enough clubs to satisfy the maximum number of application
### => the maximum number of application equals to the number of clubs which can be applied
if ($maxNumOfApp == 0 || $maxNumOfApp > $numOfCanBeAppliedClub) {
    // H56742
    //	$maxNumOfApp = $numOfCanBeAppliedClub;
    //	$maxNumOfEnrol = $numOfCanBeAppliedClub;
    $maxNumOfApp = $numOfCanBeAppliedClub + $numOfStudentEnrolledClub;
//     $maxNumOfEnrol = $numOfCanBeAppliedClub + $numOfStudentEnrolledClub;
}

if($maxNumOfEnrol == 0 || $maxNumOfEnrol > $numOfCanBeAppliedClub){
    $maxNumOfEnrol = $numOfCanBeAppliedClub + $numOfStudentEnrolledClub;
}


### If the school requires the student to apply at least 4 clubs but there are only 2 clubs available, auto adjust the minimum number of application to 2 clubs.
if ($maxNumOfApp < $minNumOfApp) {
    $minNumOfApp = $maxNumOfApp;
}
//$maxNumOfClubCanBeEnrolled = ($libenroll->EnrollMax == 0)? $numOfCanBeAppliedClub : $maxNumOfEnrol;
$maxNumOfClubCanBeEnrolled = ($maxNumOfEnrol == 0)? $numOfCanBeAppliedClub : $maxNumOfEnrol;


// // If the student has not saved the maxwant in the system, by default, use the maximum number of clubs a student can be enrolled
// if ($maxStudentWanted == '') {
//     //$maxwant = $libenroll->EnrollMax;
//     $maxStudentWanted = $maxNumOfClubCanBeEnrolled;
// }

?>
<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $Lang['eEnrolment']['EditMaxClubNumber'];?></title>
  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="../js/moment.js"></script>
  <script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
  <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
  <script type="text/javascript" src="../js/sitemapstyler.js"></script>
  <script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
  <script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
  <script type="text/javascript" src="../js/offcanvas.js"></script>
  <link rel="stylesheet" href="../css/offcanvas.css">
  <link rel="stylesheet" href="../css/eEnrolment.css">
  <script type="text/javascript" src="../js/eEnrolment.js"></script>
  <?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? } ?>
  <link rel="stylesheet" href="../css/font-awesome.min.css">
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
  </head>
<form name="form1" id="form1" method="post" enctype="multipart/form-data">
  <body>
  <div id="wrapper"> 
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top enrolment edit">
      <div id="function">
        <div class="headerIcon"><a href="javascript:history.go(-1)"><i class="fas fa-arrow-left"></i></a></div>
      </div>
      <div class="headerTitle"><?php echo $Lang['eEnrolment']['EditMaxClubNumber'];?></div>
      <div id="button">
        <div class="headerIcon invisible"><a href="#"><span class="selectedClub" id="selectedClub"></span></a><span class="clubNum">0</span></div>
        <div class="headerIcon invisible"> <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a> </div>
		<?if($sys_custom['eEnrollment']['useMinApplyNum']){ ?>
		<div class="headerIcon"><a id="maxClubSubmit" onclick="javascript:checkForm()" class="submit"><i class="fas fa-check"></i></a></div>
      	<? }else{?>
		<div class="headerIcon"><a href="javascript:submitForm()" class="submit"><i class="fas fa-check"></i></a></div>
      	<?php } ?>
      </div>
    </nav>
    <div id="content">
      <div class="maxNoOfClub fullpage col-xs-12">		  
		  <p><?php echo  str_replace("[__ClubCount__]", $EnroupGroupCount,$Lang['eEnrolment']['YouHaveTotalAppliedClubCount2']);?></p>
		  <br />
		  <form>
			  <div class="form-group">
				  <p><?php echo $i_ClubsEnrollment_MaxForOwnWill;?>︰</p>
				  <div class="input-group">
					  <div class="input-group-btn">
						  <button type="button" class="btn btn-primary" id="fa-minus"><i class="fas fa-minus"></i></button>
					  </div>
					  <?if($sys_custom['eEnrollment']['useMinApplyNum']){ ?>
					  	<input type="number" class="form-control" id="AppliedCount"  value="<?php echo $maxStudentWanted;?>" max="<?=$maxNumOfClubCanBeEnrolled?>" min="<?=$minNumOfApp?>">
					  <? }else{?>
					  <input type="number" class="form-control" id="AppliedCount"  value="<?php echo $maxStudentWanted;?>" max="<?=$maxNumOfClubCanBeEnrolled?>">
					  <?php } ?>
					  <div class="input-group-btn">
						  <button type="button" class="btn btn-primary" id="fa-plus"><i class="fas fa-plus"></i></button>
					  </div>
				  </div>
			  </div>
		  </form>
	  </div>
    </div>
  </div>
 <div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="warning" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabel"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['ErrorMaxClubTitle'];?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button>
      </div>
      <div class="modal-body">
			<span id="alertClubCountMessage"><?php echo $Lang['eEnrolment']['SelectMaxClub1']. $minNumOfApp .$Lang['eEnrolment']['SelectMaxClub2']. $maxNumOfClubCanBeEnrolled?></span>
      </div>
    </div>
  </div>
</div>
</body>
<script type="text/javascript">

jQuery(function(){
	  var addInput = '#AppliedCount'; //This is the id of the input you are changing
	  var n = document.getElementById("AppliedCount").value;
	  //Set default value to n (n = 1)
	  var maxn =  document.getElementById("AppliedCount").max;
	  var minn = document.getElementById("AppliedCount").min;

	  <?if($sys_custom['eEnrollment']['useMinApplyNum']){ ?>
	  if(n == 0 || n == ''){
	  	n = parseInt(maxn);
	  	$(addInput).val(n);
	  }else{
		
	  }
	  	
	  <? } ?>
	  
	  //On click add 1 to n
	  $('#fa-plus').on('click', function(){	
		  if(n<maxn){
			  $(addInput).val(++n);
			}	    
	  })

	  $('#fa-minus').on('click', function(){
	    //If n is bigger or equal to 1 subtract 1 from n
	    <?if($sys_custom['eEnrollment']['useMinApplyNum']){ ?>
			if (n > minn){
	    	      $(addInput).val(--n);
    	    } else {
    	      //Otherwise do nothing
    	    }
        <? }else{?>
    	    if (n >= 1) {	  
    	      $(addInput).val(--n);
    	    } else {
    	      //Otherwise do nothing
    	    }
        <? } ?>
	  });
	});

function checkForm(){
	<?if($sys_custom['eEnrollment']['useMinApplyNum']){ ?>
    	var maxn = parseInt(document.getElementById("AppliedCount").max);
    	var minn = parseInt(document.getElementById("AppliedCount").min);
    	var num = parseInt($("#AppliedCount").val());
    	if(num > maxn || num < minn){
    		$("#maxClubSubmit").attr("data-target","#warning"),
    		$("#maxClubSubmit").attr("data-toggle","modal");
    		return false;	 
    	}else{
    		$("#maxClubSubmit").attr("data-target", ""),
    		$("#maxClubSubmit").attr("data-toggle", "");
    	}
	<? } ?>
	submitForm();
}


function submitForm(){
	document.form1.action = 'parent_eEnrol_club_start.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&FinishApplication=1&EnrolGroupIDList=<?php echo $EnrolGroupIDList;?>&TimeClashGroupID=<?php echo $TimeClashGroupID;?>&WishClubCount='+ $("#AppliedCount").val();
	document.form1.method = "POST";
	document.form1.submit(); 	
}
</script>
</form>
</html>
