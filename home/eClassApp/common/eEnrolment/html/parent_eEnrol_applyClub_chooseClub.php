<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

// $CategoryIDAry = $CategoryID==''?explode(',',$CategoryIDList):$CategoryID;
$weekDayAry = $weekDay == ''? explode(',',$weekDayList):$weekDay; 
$canBeAppliedClubInfoAry = $libenroll->getGroupsAvailableForApply($StudentID);
// $StudentID = $studentId;
### Get clubs which can be applied


$numOfCanBeAppliedClub = count($canBeAppliedClubInfoAry);
$canBeAppliedClubEnrolGroupIdAry = Get_Array_By_Key($canBeAppliedClubInfoAry, 0);

$SelectedClubIDAry = array();
$SelectedClubIDAry = $BackFromOrding == '1'?explode(',',$NowEnrolGroupIDList): explode(',',$EnrolGroupIDList);


### Get the data of all clubs
$clubInfoAry = $libenroll->Get_All_Club_Info($canBeAppliedClubEnrolGroupIdAry);
$clubEnrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, array('Semester','EnrolGroupID'));

unset($clubInfoAry);


### Get the meeting dates of all clubs
$clubMeetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($clubEnrolGroupIdAry);
$clubMeetingDateAssoAry = BuildMultiKeyAssoc($clubMeetingDateInfoAry, 'EnrolGroupID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($clubMeetingDateInfoAry);

if($EnrolGroupIDList || $NowEnrolGroupIDList){
    $leaveHref = "parent_eEnrol_club_start.php?isStudentApp=$isStudentApp&StudentID=$StudentID";
}else{
    $leaveHref = "parent_eEnrol_club_apply.php?isStudentApp=$isStudentApp&StudentID=$StudentID";
}


$CategoryAry = $libenroll->GET_CATEGORY_LIST();
$CategoryAryWithKeyID = build_assoc_array($CategoryAry);
// $CategoryIDAry= Get_Array_By_Key($CategoryAry, 'CategoryID');

// $AllSemesterAry = getAllSemesterByYearID();
// $AllSemesterNameAry = 
$thisClubSemesterAry = array();
foreach($clubInfoAssoAry as $Semester=>$canApplyclubInfoAry){
    $thisClubSemesterAry[] = $Semester;
}


// $i=0;
// foreach($thisClubSemesterAry as $thisClubSemester){
//     $DisplaySemesterAry[$thisClubSemester] ='term'.$i;
//     $DisplayClassAry[$thisClubSemester] = 'showTerm'.$i;
//     $i++;
// }

$quotaSettingsAry=  $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club'],$CategoryID=0);
$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);


$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();


$AcademicYearID = Get_Current_Academic_Year_ID();
$libAcademicYear = new academic_year($AcademicYearID);
$termNameAry = $libAcademicYear->Get_Term_List(1);
$termAry[0]['YearTermID'] = 0;
$termAry[0]['YearTermName'] = $Lang['General']['WholeYear'];
$i=0;
$DisplaySemesterAry[''] = 'term'.$i;
$DisplayClassAry[''] = 'showTerm'.$i;
foreach($termNameAry as $TermID => $TermName){
    $i++;
    $termAry[$i]['YearTermID'] = $i;
    $termAry[$i]['YearTermName'] = $TermName;
    
    $DisplaySemesterAry[$TermName] ='term'.$i;
    $DisplayClassAry[$TermName] = 'showTerm'.$i;
//     $i++;
}

$numOfTerm = count($termAry);
// debug_pr($DisplaySemesterAry);

$numOfQuotaType = count($quotaTypeAry);
$minQuatoClubAry = array();
$termBasedFormChecking = false;
if($libenroll->quotaSettingsType=='TermBase'){
    $termBasedFormChecking = true;
    
    for ($i=0; $i<$numOfQuotaType; $i++) {
        $_quotaType = $quotaTypeAry[$i];
        if ($_quotaType == 'ApplyMin') {
            $_title = $Lang['eEnrolment']['MinStudentApply'];
            
            
    //         $termAry = $libenroll->getTermList(1);
    //         $numOfTerm = count($termAry);
                  
            for ($j=0; $j<$numOfTerm; $j++) {
                $_yearTermId = $termAry[$j]['YearTermID'];
                $_yearTermName = $termAry[$j]['YearTermName'];
                
                $_yearTermNum = $_yearTermId;
    //             debug_pr($_yearTermId);
                
                if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
                    continue;
                }
            
                if ($_yearTermId == 0) {
                    $_termName = $Lang['eEnrolment']['WholeYearClub'];
                    $_term = $Lang['eEnrolment']['WholeYear'];
                    $_termNum = 0;
                }
                else {
                    $_termName = $_yearTermName.''.$Lang['eEnrolment']['Club'];
                    $_term = $_yearTermName;
                    $_termNum = $j;
                }
                
                
                $_quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$_termNum];
            
                $minQuatoClubAry[] = $_quotaTypeValue.' '.$Lang['eEnrolment']['Unit'].'' .$_termName.' ';
                $thisSemesterMinClubCountAry[$_term] = $_quotaTypeValue;
            }
        }
    
        if ($_quotaType == 'ApplyMax') {
            $_title = $Lang['eEnrolment']['MaxStudentApply'];
       
    //         $termAry = $libenroll->getTermList(1);
    //         $numOfTerm = count($termAry);     
            
            for ($j=0; $j<$numOfTerm; $j++) {
                $_yearTermId = $termAry[$j]['YearTermID'];
                $_yearTermName = $termAry[$j]['YearTermName'];           
                $_yearTermNum = $_yearTermId;
                
                if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
                    continue;
                }
                
                if ($_yearTermId == 0) {
                    $_termName = $Lang['eEnrolment']['WholeYearClub'];
                    $_term = $Lang['eEnrolment']['WholeYear'];
                    $_termNum = 0;
                }
                else {
                    $_termName = $_yearTermName.' '.$Lang['eEnrolment']['Club'];
                    
                    $_term = $_yearTermName;
                    $_termNum = $j;
                }
                
                
                $_quotaTypeValue2 = $quotaSettingsAssoAry[$_quotaType][$_termNum];
             
                $maxQuatoClubAry2[] =$_quotaTypeValue2.' '.$Lang['eEnrolment']['Unit'].' '.$_termName;
                $thisSemesterMaxClubCountAry[$_term] = $_quotaTypeValue2;
            }
        }

    }
}else if($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase'){
    
    
    $minNumOfApp = $quotaSettingsAssoAry['ApplyMin'][0];
    $maxNumOfApp = $quotaSettingsAssoAry['ApplyMax'][0];
    $maxNumOfEnrol = $quotaSettingsAssoAry['EnrollMax'][0];
    
    $thisSemesterMinClubCountAry[0] = $minNumOfApp;
    $minQuatoClubAry[] = $minNumOfApp.' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club2'];
}
// debug_pr($thisSemesterMinClubCountAry);

if($sys_custom['eEnrolment']['Application']['MinApply'] ){
    $minQuatoClubAry = array();
    $custMinClubApply = 9;
    $minQuatoClubAry[] = $custMinClubApply.' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'];
}

$displayMinQuatoClub = implode(' '.$eEnrollment['and'].' ',$minQuatoClubAry);

$DisableClubIDAry = array();

$appliedClubInfoAry = $libenroll->getChoicesOfStudent($StudentID);
// $appliedClubAssoAry = BuildMultiKeyAssoc($appliedClubInfoAry, 1);
foreach((array)$appliedClubInfoAry as $AppliedClubInfo){
    if($AppliedClubInfo['1'] == '2' || $AppliedClubInfo['1'] == '1'){ 
        // in parcitiping or rejected club
        $DisableClubIDAry[] = $AppliedClubInfo['0'];
   }
}

$CategoryIDAry = array();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eEnrolment']['SelectClub'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<script type="text/javascript" src="../js/eEnrolment.js"></script>

<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? }else{ ?>
	<link rel="stylesheet" href="../css/eEnrolment.css">
<?php }?>

<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>

<form name="form1" id="form1" method="post" enctype="multipart/form-data">
<body>
<div id="wrapper">     
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment edit">
    <div id="function">
      <div class="headerIcon"><a data-toggle="modal" data-target="#leaveWarning"><i class="fas fa-arrow-left"></i></a></div>
    </div>
    <div class="headerTitle withFunction"><?php echo $Lang['eEnrolment']['SelectClub'];?></div>
    <div id="button">
      <div class="headerIcon">
      	  <a id="filterSelectedClub" onclick="javascript:filterSelectedClub();">
          	<span class="selectedClub" id="selectedClub"></span>
          </a>
      	  <span class="clubNum">0</span>
      </div>
      <div class="headerIcon">
		  <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter">
			  <span class="filter"></span>
		  </a>
	  </div>
      <div class="headerIcon">
          <a id="nextStep" onclick="javascript:checkSubmit()" class="next">
          <i class="fas fa-arrow-right"></i>
          </a>
      </div>
    </div>
	<div id="filter" class="collapse">
	<ul class="filterUL">
		<li class="filterItem">
		<a role="button" data-toggle="collapse" href="#filterCategory"  aria-expanded="false" aria-controls="filterCategory"><span><?php echo $eEnrollment['category'];?></span></a>
		<div class="collapse" id="filterCategory">
			<div class="well">
				<div class="row" >
					<ul class="col-xs-12">
					<?php 
// 					$checked = "";
// 					if(count($CategoryAry) == count($CategoryIDAry)){
					    $checked = "checked";
// 					}
					?>
						<li class="filterSubItem">
							<span><?php echo $Lang['eEnrolment']['AllCategories'];?></span>
							<div class="form-check form-check-inline">
								<label class="form-check-label">								
									<input class="form-check-input" type="checkbox" name="allCategory" id="allCategory"  onclick="(this.checked)?setChecked(1,document.form1,'CategoryID[]'):setChecked(0,document.form1,'CategoryID[]')" <?php echo $checked;?>>
									<span>&nbsp;</span>
								</label>
							</div>
						</li>
						<?php 
						foreach ($CategoryAry as $CategoryInfo){						    
						    $CategoryID = $CategoryInfo['CategoryID'];
						    $CategoryName= $CategoryInfo['CategoryName'];	
// 						    $checked = '';
// 						    if(!empty($CategoryIDAry) && in_array($CategoryID,$CategoryIDAry) ){
// 						        $checked = "checked";
// 						    }
						?>
						<li class="filterSubItem">
							<span><?php echo $CategoryName;?></span>
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" name="CategoryID[]" onclick="javascript:changeCategory();" value="<?php echo $CategoryID;?>" <?php echo $checked;?>>
									<span>&nbsp;</span>
								</label>
							</div>
						</li>						
						<?php } ?>		
					</ul>
				</div>
			</div>
		</div>
	</li>
	<li class="filterItem">
		<a role="button" data-toggle="collapse" href="#filterDate"  aria-expanded="false" aria-controls="filterDate"><?php  echo $i_CycleDay;?></a>
		<div class="collapse" id="filterDate">
			<div class="well">
				<div class="row">
					<ul class="col-xs-12">
    					<?php 
//     					$checked = "";
//     					if($weekDayAry == '7'){
    					    $checked = "checked";
//     					}
    					?>
						<li class="filterSubItem">
							<span><?php echo $Lang['eEnrolment']['AllDay'];?></span>
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" name="allDate" id="allDate"  onclick="(this.checked)?setChecked(1,document.form1,'weekDay[]'):setChecked(0,document.form1,'weekDay[]')" <?php echo $checked;?>>
									<span>&nbsp;</span>
								</label>
							</div>
						</li>
						<?php 
						for($i=1;$i<=7;$i++){ 
// 						    $checked = "";
// 						    if(!empty($weekDayAry) && in_array($i,$weekDayAry)){
// 						      $checked = "checked";
// 						    }
						?>
						    
						<li class="filterSubItem">
							<span><?php echo $Lang['eEnrolment']['WeekDay'][$i];?></span>
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" name="weekDay[]" onclick="javascript:changeCategory();" value="<?php echo $i;?>" <?php echo $checked;?>>
									<span>&nbsp;</span>
								</label>
							</div>
						</li>
						<?php }?>
					</ul>
				</div>
			</div>
		</div>
		</li>
	  </ul>
  </div>  
  </nav>

  <div id="content">
    <div class="alertMsg">
      <div class="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <span><?php echo str_replace("[__count__]",$displayMinQuatoClub,($sys_custom['eEnrolment']['Application']['MinApply']?$Lang['eEnrolment']['YouMustAppliedClub']:$Lang['eEnrolment']['YouNeedAppliedClubMin2']));?></span> </div>
    </div>
    <ul class="yearTermList"> 
      <?php 
      $i = 0;
      foreach($clubInfoAssoAry as $Semester=>$enrolClubInforAry){
          $DisplaySemesterTitle = $Semester==''?$Lang['eEnrolment']['WholeYear']:$Semester;
          $SemesterClass = $DisplaySemesterAry[$Semester];
          $ShowSemester = $DisplayClassAry[$Semester];        
      ?>
       <li class="termItem">
		<a data-toggle="collapse" href="#<?php echo $ShowSemester;?>" role="button" aria-expanded="true" aria-controls="<?php echo $ShowSemester;?>"><?php echo $DisplaySemesterTitle.' '.$Lang['eEnrolment']['Club'];?></a>
		<div class="panel-collapse collapse in" id="<?php echo $ShowSemester;?>">
     	 	<ul class="applyClub clubList">
      
      		<?php foreach ($enrolClubInforAry as $thisEnrolGroupID=>$clubInforAry){
      		//    $thisClubSemester = $clubInforAry[''];
      		    $thisClubTitle= Get_Lang_Selection($clubInforAry['TitleChinese'],$clubInforAry['Title']);

//       		    $thisClubGroupCategory= $clubInforAry['GroupCategory'];  		    
      		  
      		    $thisClubMeetingDateAry = $clubMeetingDateAssoAry[$thisEnrolGroupID];
//       		    $thisClubMeetingDateAry = $clubMeetingDateAssoAry;
      		    $thisClubCategoryName = $clubInforAry['CategoryName'] == ''? '': $clubInforAry['CategoryName'];
      		    $afterFilterMeetingDate = array();
      		    $_meetingWeekdayDisplay = '---';
      		    $thisMeetingDateAry = array();
      		    if(!empty($thisClubMeetingDateAry) ){
      		        $MeetingWeekdayAry = array();
      		        $_thisMeetingDate = array();
      		        
      		        foreach($thisClubMeetingDateAry as $ClubMeetingDateAry){
      		            
      		            $_thisMeetingDateNo = date("N", strtotime($ClubMeetingDateAry['ActivityDateStart']));
      		            $_thisMeetingDate[] = $_thisMeetingDateNo;
      		            if(!empty($weekDayAry) && in_array($_thisMeetingDateNo,$weekDayAry)){
      		                $afterFilterMeetingDate[] = $_thisMeetingDateNo;
      		            }
      		           
      		        }	         
       		         $thisMeetingDateAry= array_unique($_thisMeetingDate);
       		         asort($thisMeetingDateAry);
       		         $MeetingWeekdayAry = array();
       		         foreach($thisMeetingDateAry as $thisMeetingDate){
       		             $MeetingWeekdayAry[] = $Lang['eEnrolment']['WeekDay'][$thisMeetingDate];
       		         }
       		         $_meetingWeekdayDisplay = implode('、',$MeetingWeekdayAry);
      		    }
      		        $check = '';
      		    if(in_array($thisEnrolGroupID,$SelectedClubIDAry)){
      		            $check = 'checked';
      		    }
      		 	
      		 	if(!in_array($thisEnrolGroupID,$DisableClubIDAry)){
			      $thisClubCategoryID = $clubInforAry['GroupCategory'];
			      $CategoryIDAry[] = $thisClubCategoryID;
      		 	}
      		 ?>
      		 
		 	<li class="<?php echo $SemesterClass;?>" name="CanEnroledGroup[<?php echo $thisClubCategoryID;?>]" id="<?php echo implode(',',$thisMeetingDateAry);?>">
				<a href="parent_eEnrol_applyClubDetials.php?isStudentApp=<?=$isStudentApp?>&EnrolGroupID=<?php echo $thisEnrolGroupID;?>" class="left" >
				  <div class="clubTitle">
				  	<span><?php echo $thisClubTitle;?></span>   				  
				  </div>
				  <div class="clubInfo">
				      <span><?=$thisClubCategoryName?></span>			
					  <div class="row">							  							
						  <span class="icon"><i class="far fa-clock"></i></span><span class="date"><?php echo $_meetingWeekdayDisplay;?></span>
					  </div>
				  </div>
				</a>
			  <div class="form-check form-check-inline">
				  <label class="form-check-label">
				  <?php if(!in_array($thisEnrolGroupID,$DisableClubIDAry)){
//				      $thisClubCategoryID = $clubInforAry['GroupCategory'];
//				      $CategoryIDAry[] = $thisClubCategoryID;
				      ?>
	 				 <input class="form-check-input" type="checkbox" value="<?php echo $thisEnrolGroupID;?>" id="EnrolGroupID[<?=$Semester?>]" name= "EnrolGroupID[]" <?php echo $check;?>>				
				  <?php }?>
				 		<span>&nbsp;</span>					 
				  </label>		  
			  </div>
			</li>     		 
      		<?  
//       		    }
      		} ?>     	
	  	</ul>
        </div>
      </li>
      <?
      $i++;
      }?>	
     
     </ul>
   </div>
   <? if(count($numOfCanBeAppliedClub) == 0){?>
       <div id="content">
        <div class="noResult">
          <p class="info"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['NoResultFound'];?></p>
    	  <!--<p><img src="../../images/eEnrol_noResultBg.png" alt="noResult"></p>-->
        </div>
      </div>	    
	<?}?>
</div>

<div class="modal fade" id="leaveWarning" tabindex="-1" role="dialog" aria-labelledby="leaveWarning" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="leaveWarninglLabel"><?php echo $button_quit;?></h5>
      </div>
      <div class="modal-body"><?php echo $Lang['eEnrolment']['ExitClubApplicationWrningMessage'];?></div>
	  <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal"><?php echo $i_general_no;?></button>
        <a href="<?php echo $leaveHref;?>"><button type="button" class="btn"><?php echo $i_general_yes;?></button></a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="warningMax" tabindex="-1" role="dialog" aria-labelledby="warningMax" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabelMax"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['AlreadyAchieveMaxClubCountTitle'];?></h5>
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button> 
      </div>
      <div class="modal-body">
      <span id="alertClubMaxCountMessage">
      
      </div>
	  <div class="modal-footer" id="MaxFooter">
        <button type="button" class="btn" data-dismiss="modal"><?php echo $Lang['eEnrolment']['ReturntoEdit'];?></button>
        <a href="javascript:submitForm()"><button type="button" class="btn"><?php echo $button_submit;?></button></a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="warningExceedMax" tabindex="-1" role="dialog" aria-labelledby="warningExceedMax" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabelMax"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['ExceededMaxClubCount'];?></h5>
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button> 
      </div>
      <div class="modal-body">
      <span id="alertExceedClubMaxCountMessage">
      
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="warningMin" tabindex="-1" role="dialog" aria-labelledby="warningMin" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabelMin"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['NotAchieveMinClubCount'];?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button>
      </div>
      <div class="modal-body">
			<span id="alertClubMinCountMessage"></span>
      </div>
    </div>
  </div>
</div>
<?php 
$CategoryIDAry = array_unique($CategoryIDAry);
$CategoryIDCount = count($CategoryIDAry);
?>

<script type="text/javascript">
$( document ).ready(function() {	
  	setChecked(1,document.form1,'CategoryID[]');
  	setChecked(1,document.form1,'weekDay[]')
  	$('.applyClub input[type="checkbox"]').change();
});


$('a[rel="filter"]').on('click', function(e) {
	if($(this)[0].attributes['aria-expanded'].value == 'true'){
		document.form1.action = 'parent_eEnrol_applyClub_chooseClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=' + <?php echo $StudentID;?>; 
		document.form1.method = "POST";
		document.form1.submit(); 
	}
});

function changeCategory(){

	 var checkboxes = document.getElementsByName("CategoryID[]");	
	 var checkboxesDay = document.getElementsByName("weekDay[]");	
	 
	 for(var i=0, n=checkboxes.length;i<n;i++) {		
		 if(checkboxes[i].checked){				
			 var AbleClubs = document.getElementsByName("CanEnroledGroup["+checkboxes[i].value+"]");
			 for(var j=0, m=AbleClubs.length;j<m;j++){

				var clubdisplay = 'false';
				 weekday = AbleClubs[j].id;
			      for(var k=0, p=checkboxesDay.length;k<p;k++){
			    	  if(checkboxesDay[k].checked){	

							if(weekday.indexOf(checkboxesDay[k].value) == -1){
								clubdisplay += ',false';				
							}else{
								clubdisplay += ',true';
							}
								
				      }
				  } 

 	 			if(clubdisplay.indexOf("true") == -1){
 	 				AbleClubs[j].style.display = "none";
 	 				//AbleClubs[j].children[1].children[0].children[0].checked = ''; 	
 	 			}else{
 	 				AbleClubs[j].style.display = "block";	
 	 	 		}		
			 }
		 }else{
			 var AbleClubs = document.getElementsByName("CanEnroledGroup["+checkboxes[i].value+"]");
			 
			 for(var j=0, m=AbleClubs.length;j<m;j++){
 				 AbleClubs[j].style.display = "none";	
 				 //AbleClubs[j].children[1].children[0].children[0].checked = ''; 	 
			 }
		 }	
	 }	
	
}
function checkSubmit(){

	var returnMinMessage = '<?php echo $Lang['eEnrolment']['YouNeedChoose'];?>';

 	var returnAchieveMaxMessage = '<?php echo $Lang['eEnrolment']['YouHaveChoose'];?>';
 	var returnEqualMaxMessage = '<?php echo $Lang['eEnrolment']['YouHaveChoose'];?>';
 	
	var alertMinMessage= false;
	var alertMaxMessage= false;
// 	var equalMaxCount = false;
console.log('uhi');
	<?php 	
	if($termBasedFormChecking){
	  
    	$thisSemesterMinClubCount = 0;
    	$thisSemesterMaxClubCount = 0;
    	$i = 0;
    	foreach ($thisSemesterMinClubCountAry as $ternName => $termMinCount){
    	   
//     	    for($i=0;$i<count($thisSemesterMinClubCountAry);$i++){
//     	    $thisSemesterMinClubCount = $thisSemesterMinClubCountAry[$i];
    
//     	    $thisSemesterName = $thisClubSemesterAry[$i];
//     	    $thisSemesterName = $thisSemesterName==''?$Lang['General']['WholeYear']:$thisSemesterName;
//     	    $thisSemesterMaxClubCount= $thisSemesterMaxClubCountAry[$i];

    	    $thisSemesterMinClubCount = $termMinCount;
    
    	    $thisSemesterName = $ternName==$Lang['eEnrolment']['WholeYear'] ? '': $ternName;

    	    
    	    $thisSemesterMaxClubCount= $thisSemesterMaxClubCountAry[$ternName];
//     	    debug_pr($ternName);
    	    ?>
    	   
    	    var thisSemesterMinClubCount = <?php echo $thisSemesterMinClubCount;?>;
    
    	    var thisSemesterMaxClubCount = <?php echo $thisSemesterMaxClubCount;?>;

    	    var SelectedClubLength = document.querySelectorAll('input[id="EnrolGroupID[<?php echo $thisSemesterName;?>]"]:checked').length;

    	 	if(SelectedClubLength < thisSemesterMinClubCount ){	 
    	 		alertMinMessage = true;   	
    			returnMinMessage += '<br>'+thisSemesterMinClubCount +'<?php echo ' '.$Lang['eEnrolment']['Unit'].''.$ternName.' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'].$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['FullStop'];?>';
    		}
    
    		if(thisSemesterMaxClubCount <  SelectedClubLength && thisSemesterMaxClubCount != 0){
    			alertMaxMessage = true;
    			returnAchieveMaxMessage += SelectedClubLength +'<?php echo ' '.$Lang['eEnrolment']['Unit'].''.$ternName.' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'];?>';			
    		}
    
//     		if(thisSemesterMaxClubCount ==  SelectedClubLength && thisSemesterMaxClubCount != 0){
 //   			returnEqualMaxMessage += SelectedClubLength +'<?php echo ' '.$Lang['eEnrolment']['Unit'].''.$ternName.' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'];?>';						
//     			equalMaxCount = true;
//     		}
    	<?php 
    	   $i++;
        }
    	
       }else{ ?>
     	 var SelectedClubLength = document.querySelectorAll('input[name="EnrolGroupID[]"]:checked').length;
         var MinClubCount = <?php echo $minNumOfApp;?>;
         var MaxClubCount = <?php echo $maxNumOfApp;?>;
    
         if(SelectedClubLength < MinClubCount){
        		alertMinMessage = true;   	
    			returnMinMessage += '<br>'+MinClubCount +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'].','.$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'];?>';
          }
    
    		if((MaxClubCount  <  SelectedClubLength) && MaxClubCount != 0){
    			alertMaxMessage = true;
    			returnAchieveMaxMessage += SelectedClubLength +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'];?>,';			
    		}
    
//     		if((MaxClubCount  ==  SelectedClubLength) && MaxClubCount != 0){
 //   			returnEqualMaxMessage += SelectedClubLength +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'];?>,';						
//     			equalMaxCount = true;
//     		}



   <?php }?>

   <?php if($custMinClubApply){ ?>
   	   alertMinMessage = false;
       var MinClubCount = <?=$custMinClubApply?>;
       
       var SelectedClubLength = document.querySelectorAll('input[name="EnrolGroupID[]"]:checked').length;
    
        if(SelectedClubLength != MinClubCount){
    		alertMinMessage = true;   	
    		returnMinMessage = '<?php echo $Lang['eEnrolment']['YouNeedChoose'];?><br>'+MinClubCount +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'].','.$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'];?>';
     	}    
   <? }?>

   
	returnAchieveMaxMessage +=  ' <?php echo $Lang['eEnrolment']['AlreadyExceededMaxClubCount'].$Lang['eEnrolment']['FullStop'];?>';

//	returnEqualMaxMessage += ' <?php echo $Lang['eEnrolment']['AlreadyAchieveMaxClubCount'].$Lang['eEnrolment']['FullStop'].' '.$Lang['eEnrolment']['AreYouSureToSubmitApplication'];?>';
    
	alertMinCategoryMessage = false;
    <?php if($sys_custom['eEnrolment']['Application']['EachCategoryNeed']){ 
         ?>
            var SelectedClub = $("input[name='EnrolGroupID[]']:checked");
            var categoridAry = [];
         
        	for(var i=0, n=SelectedClub.length;i<n;i++) {				 
    			var __categoryName= SelectedClub[i].parentElement.parentElement.parentElement.getAttribute("name");	
    			__categoryName = __categoryName.substring(16);
    		
    			categoridAry.push(__categoryName.substring(0 ,__categoryName.length - 1));
    			
    		}
        	var unique = categoridAry.filter( onlyUnique ).length;
        	var categoryLength = '<?=$CategoryIDCount?>'; 

			var clubSelectedForCat2 = [];
			var element = '2';
			for(var j=0;j<categoridAry.length;j++){
				if(categoridAry[j] == element){
					clubSelectedForCat2.push(j);
				}
			}
			
			var clubSelectedForCat4 = [];
			var element = '4';
			for(var j=0;j<categoridAry.length;j++){
				if(categoridAry[j] == element){
					clubSelectedForCat4.push(j);
				}
			}
			
			var clubSelectedForCat5 = [];
			var element = '5';
			for(var j=0;j<categoridAry.length;j++){
				if(categoridAry[j] == element){
					clubSelectedForCat5.push(j);
				}
			}
			
			if(clubSelectedForCat2.length != 1){
				alertMinCategoryMessage = true;
        		returnMinCategoryMessage = '<?=str_replace("[__qty__]",1,str_replace("[__name__]",$CategoryAryWithKeyID[2],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>';
			}
			else if(clubSelectedForCat4.length != 4){
				alertMinCategoryMessage = true;
        		returnMinCategoryMessage = '<?=str_replace("[__qty__]",4,str_replace("[__name__]",$CategoryAryWithKeyID[4],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>';
			}
			else if(clubSelectedForCat5.length != 4){
				alertMinCategoryMessage = true;
        		returnMinCategoryMessage = '<?=str_replace("[__qty__]",4,str_replace("[__name__]",$CategoryAryWithKeyID[5],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>';
			}
        	else if(unique < categoryLength){
        		alertMinCategoryMessage = true;
        		returnMinCategoryMessage = '每個活動類別最少選擇一個學會';
            }
    <?php  } ?>
    


	if(alertMinMessage){
		 $("#nextStep").attr("data-target","#warningMin"),
		 $("#nextStep").attr("data-toggle","modal"),
		 document.getElementById("alertClubMinCountMessage").innerHTML =  returnMinMessage;		 

	}
	else if(alertMaxMessage){
		 $("#nextStep").attr("data-target","#warningExceedMax"),
		 $("#nextStep").attr("data-toggle","modal"),
		 document.getElementById("alertExceedClubMaxCountMessage").innerHTML =  returnAchieveMaxMessage;
	}
// 	else if(equalMaxCount){
// 		 $("#nextStep").attr("data-target","#warningMax"),
// 		 $("#nextStep").attr("data-toggle","modal"),
// 		 document.getElementById("alertClubMaxCountMessage").innerHTML =  returnEqualMaxMessage;
// 		 $("div#MaxFooter").show();	
// 	}
	else if(alertMinCategoryMessage){
		 $("#nextStep").attr("data-target","#warningMin"),
		 $("#nextStep").attr("data-toggle","modal"),
		 document.getElementById("alertClubMinCountMessage").innerHTML =  returnMinCategoryMessage;	
	}
	else{
		$("#nextStep").attr("data-target",""),
		$("#nextStep").attr("data-toggle",""),
			
		submitForm();
	}
	
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function filterSelectedClub(){
// 	var allClub = $("input[name='EnrolGroupID[]']");
	var UnSelectedClub = $("input[name='EnrolGroupID[]']:not(:checked)");

	var SelectedClub = $("input[name='EnrolGroupID[]']:checked");
	
	if(typeof document.getElementsByClassName("selectedClub active")[0] == 'undefined' ){	
		setChecked(1,document.form1,'weekDay[]');			
	} else{
		for(var i=0, n=UnSelectedClub.length;i<n;i++) {					 
			 UnSelectedClub[i].parentElement.parentElement.parentElement.style.display = "none";	
		}	

		for(var i=0, n=SelectedClub.length;i<n;i++) {				 
			SelectedClub[i].parentElement.parentElement.parentElement.style.display = "block";	
		}
			

	}	 
}


function submitForm()
{	
	document.form1.action = 'parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&CategoryIDCount=<?=$CategoryIDCount?>';
	document.form1.method = "POST";
	document.form1.submit();
	  
}
function setChecked(status,obj,id) {	
	checkboxes = document.getElementsByName(id);	
	if (status==0){
		var checkstring = '';
	}else{
		var checkstring = 'checked';
	}
	
	for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = checkstring;
	}	
	changeCategory();
}


</script>
</form>
</body>
</html>