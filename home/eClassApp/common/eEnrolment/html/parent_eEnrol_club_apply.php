<?php 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");
 

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
 
$appStart = $libenroll->AppStart." ".$libenroll->AppStartHour.":".$libenroll->AppStartMin;
$appEnd = $libenroll->AppEnd." ".$libenroll->AppEndHour.":".$libenroll->AppEndMin;
if ($libenroll->mode != 0 && $libenroll->AppStart != '' && $libenroll->AppEnd != '') {
    $appPeriodColor = ($withinEnrolStage)? 'blue' : 'red';
    $appPeriodText = $appStart.' '.$eEnrollment['to'].' '.$appEnd;
}
else {
    $appPeriodColor = '';
    $appPeriodText = $Lang['General']['EmptySymbol'];
}
// if((strtotime(date("Y-m-d H:i")) >= strtotime($appStart)) && (strtotime(date("Y-m-d H:i")) < strtotime($appEnd))){
//     $showApplication = true; 
// }


$descriptionText = ($libenroll->mode == 0 || trim($libenroll->Description)=='')? $Lang['General']['EmptySymbol'] : nl2br($libenroll->Description);
$tieBreakRuleText = ($libenroll->tiebreak == 1)? $i_ClubsEnrollment_AppTime : $i_ClubsEnrollment_Random;

// $quotaSettingsAry=  $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club'],'',ture);
// $quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);


$quotaSettingsAry=  $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club'],$CategoryID=0);
$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);


$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();

$AcademicYearID = Get_Current_Academic_Year_ID();
$libAcademicYear = new academic_year($AcademicYearID);
$termNameAry = $libAcademicYear->Get_Term_List(1);
$termAry[0]['YearTermID'] = 0;
$termAry[0]['YearTermName'] = $Lang['General']['WholeYear'];
$i=0;
foreach($termNameAry as $TermID => $TermName){
    $i++;
    $termAry[$i]['YearTermID'] = $i;
    $termAry[$i]['YearTermName'] = $TermName;
}

$numOfTerm = count($termAry);


$numOfQuotaType = count($quotaTypeAry);
$minQuatoClubAry = array();
if($libenroll->quotaSettingsType=='TermBase'){ 
    for ($i=0; $i<$numOfQuotaType; $i++) {
        $_quotaType = $quotaTypeAry[$i];
//         if ($_quotaType == 'ApplyMin') {
          
//             for ($j=0; $j<$numOfTerm; $j++) {
//                 $_yearTermId = $termAry[$j]['YearTermID'];
//                 $_yearTermName = $termAry[$j]['YearTermName'];
                
//                 $_yearTermNum = $_yearTermId;

                
//                 if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
//                     continue;
//                 }
                
//                 if ($_yearTermId == 0) {
//                     $_termName = $Lang['eEnrolment']['WholeYearClub'];
//                     $_termNum = 0;
//                 }
//                 else {
//                     $_termName = $_yearTermName.' '.$Lang['eEnrolment']['Club'];
//                     $_termNum = $j;
//                 }
                
                
//                 $_quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$_termNum];
                
//                 $minQuatoClubAry[] = $_quotaTypeValue.' '.$Lang['eEnrolment']['Unit'].' ' .$_termName.' ';
//                 $thisSemesterMinClubCountAry[] = $_quotaTypeValue;
//             }
//         }
       
        if ($_quotaType == 'ApplyMin') {
            $_title = $Lang['eEnrolment']['MinStudentApply'];

            for ($j=0; $j<$numOfTerm; $j++) {
                $_yearTermId = $termAry[$j]['YearTermID'];
                $_yearTermName = $termAry[$j]['YearTermName'];
                
                $_yearTermNum = $_yearTermId;
                //             debug_pr($_yearTermId);
                
                if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
                    continue;
                }
                
                if ($_yearTermId == 0) {
                    $_termName = $Lang['eEnrolment']['WholeYearClub'];
                    $_termNum = 0;
                }
                else {
                    $_termName = $_yearTermName.' '.$Lang['eEnrolment']['Club'];
                    $_termNum = $j;
                }
                
                
                $_quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$_termNum];
                
                $minQuatoClubAry[] = $_quotaTypeValue.' '.$Lang['eEnrolment']['Unit'].' ' .$_termName.' ';
                $thisSemesterMinClubCountAry[] = $_quotaTypeValue;
            }
        }
    }
}else if($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase'){
    
    
    $minNumOfApp = $quotaSettingsAssoAry['ApplyMin'][0];
    $maxNumOfApp = $quotaSettingsAssoAry['ApplyMax'][0];
    $maxNumOfEnrol = $quotaSettingsAssoAry['EnrollMax'][0];
    
    $thisSemesterMinClubCountAry[0] = $minNumOfApp;
    $minQuatoClubAry[] = $minNumOfApp.' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'];
}

$displayMinQuatoClub = implode('<br>',$minQuatoClubAry);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $eEnrollmentMenu['club'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<link rel="stylesheet" href="../css/eEnrolment.css">
<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? } ?>
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment">
    <div id="function"></div>
    <div class="headerTitle"><?php echo $eEnrollmentMenu['club'];?></div>
    <div id="button"></div>
  </nav>
  <div id="filter"></div>
  <div id="content">
	<div class="row">
      <div class="clubApplication fullpage col-xs-12">
		  <div class="pageTitle"><span><?php echo $i_ClubsEnrollment;?></span></div>
		  <div class="applicationPeriod"><span><?php echo $appPeriodText;?></span></div>
		  <div class="row">
			<div class="applicationLabel col-xs-12">
				<span><?php echo $Lang['General']['Instruction'];?></span>
			</div>
		  	<div class="applicationContent col-xs-12">				
				<span><?php echo $descriptionText;?></span>
			</div>
			<div class="applicationLabel col-xs-12">
				<span><?php echo $i_ClubsEnrollment_TieBreakRule;?></span>
			</div>
		  	<div class="applicationContent col-xs-12">
				<span><?php echo $tieBreakRuleText;?></span>
			</div>
			<div class="applicationLabel col-xs-12">
				<span><?php echo ($sys_custom['eEnrolment']['Application']['MinApply']?$Lang['eEnrolment']['NoOfClubMustBeApply']:$i_ClubsEnrollment_MinForStudent);?></span>
			</div>
		  	<div class="applicationContent col-xs-12">
				<span><?php echo $displayMinQuatoClub;?></span>
			</div>
		  </div>
		  <a href="parent_eEnrol_applyClub_chooseClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?=$StudentID?>" class="btn btn-primary" role="button"><?php echo $Lang['eEnrolment']['StartApply2'];?></a>
      </div>
    </div>
  </div>
</div>
</body>
</html>
