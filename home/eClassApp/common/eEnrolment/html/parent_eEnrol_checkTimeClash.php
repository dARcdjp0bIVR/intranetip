<?php 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");


intranet_auth();
intranet_opendb();


$libenroll = new libclubsenrol();

$ClashGroupIDAry = explode(',',$ClashGroupID);
// debug_pr($EnrolGroupIDListAry);
$AfterDeletedEnrolGroupIDAry = array_diff($ClashGroupIDAry,explode(' ',$deletedClubID));
$AfterDeletedEnrolGroupIDAry = array_values($AfterDeletedEnrolGroupIDAry);
// debug_pr($EnrolGroupIDAry);
$EnrolGroupIDAry = $deletedClubID == ''?$ClashGroupIDAry:$AfterDeletedEnrolGroupIDAry;


$ParentEnrolGroupIDAry =explode(',',$CurrentEnrolGroupID);
$AfterDeletedParentEnrolGroupIDAry = array_diff($ParentEnrolGroupIDAry,explode(' ',$deletedClubID));
$AfterDeletedParentEnrolGroupIDAry = array_values($AfterDeletedParentEnrolGroupIDAry);
$CurrentEnrolGroupIDAry= $deletedClubID == ''?$ParentEnrolGroupIDAry:$AfterDeletedParentEnrolGroupIDAry;

if(empty($EnrolGroupIDAry)){
    header("Location: parent_orderingSelectClub.php ");
}

// $EnrolGroupIDAry = explode(',',$ClashGroupID);
$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDAry);
$clubInfoEnrolIDAssoAry = BuildMultiKeyAssoc($clubInfoAry,'EnrolGroupID');
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, array('Semester','EnrolGroupID'));

foreach($clubInfoAssoAry as $Semester=>$canApplyclubInfoAry){
    $thisClubSemesterAry[] = $Semester;    
}

$i=0;
foreach($thisClubSemesterAry as $thisClubSemester){
    $DisplaySemesterAry[$thisClubSemester] ='term'.$i; 
    $i++;
}

$clubMeetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupIDAry);
$clubMeetingDateDateAssoAry = BuildMultiKeyAssoc($clubMeetingDateInfoAry, array('ActivityDateStart','EnrolGroupID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);

foreach($clubMeetingDateDateAssoAry as $ActivityDateStart=>$_ClubMeetingDateAry){
    $ActivityDateStartAry[] = date("Y-m-d", strtotime($ActivityDateStart));
    foreach($_ClubMeetingDateAry as $__thisEnrolGroupID=>$__thisClubMeetingDateAry){
        $clubMeetingDateAry[$__thisEnrolGroupID][] = date("Y-m-d", strtotime($__thisClubMeetingDateAry[0]['ActivityDateStart']));
        $clubMeetingTimeAry[$__thisEnrolGroupID][] = date("H:i", strtotime($__thisClubMeetingDateAry[0]['ActivityDateStart'])) .' - '.date("H:i", strtotime($__thisClubMeetingDateAry[0]['ActivityDateEnd'])) ;
        
    }
}
$ActivityDateAry= array_unique($ActivityDateStartAry);
$ActivityDateList = implode(',',$ActivityDateAry);
$UniqueActivityDateAry = explode(',',$ActivityDateList);


$InParticipiantClubIDAry= array();
$appliedClubInfoAry = $libenroll->getChoicesOfStudent($StudentID);
// $appliedClubAssoAry = BuildMultiKeyAssoc($appliedClubInfoAry, 1);
foreach((array)$appliedClubInfoAry as $AppliedClubInfo){
    if($AppliedClubInfo['1'] == '2'){
        // in parcitiping or rejected club
        $InParticipiantClubIDAry[] = $AppliedClubInfo['0'];
    }
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eEnrolment']['FindandHandleTimeClash'];?></title>
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../js/offcanvas.js"></script>
<link rel="stylesheet" href="../css/offcanvas.css">
<script type="text/javascript" src="../js/eEnrolment.js"></script>
<link rel="stylesheet" href="../css/eEnrolment.css">
<?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
<? } ?>
<!-- <script type="text/javascript" src="../js/eEnrol.js"></script> -->
<link rel="stylesheet" href="../css/font-awesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <nav id="header" class="navbar navbar-fixed-top enrolment edit">
    <div id="function"></div>
    <div class="headerTitle"><?php echo $Lang['eEnrolment']['FindandHandleTimeClash'];?></div>
    <div id="button"><div class="headerIcon"><a href="javascript:refreshParentPage()"><span class="close"></span></a></div></div> 
  </nav>
  <div id="filter" class="invisible"></div>
  <div id="content">
	<div class="row marginB10">
      <div class="timeClashCount col-xs-12">
		  <label><?php echo $Lang['eEnrolment']['EachClubTimeClashTimes'];?></label>
		  <ol type="A">
		  	<?php //for($i=0;$i<count($clubInfoAry);$i++){   
                foreach($clubInfoEnrolIDAssoAry as $thisEnrolGroupID=> $clubInfoEnrolIDAry){
                    $thisEnrolGroupID = $clubInfoEnrolIDAry['EnrolGroupID'];
                    $thisEnrolGroupTitle = Get_Lang_Selection($clubInfoEnrolIDAry['TitleChinese'],$clubInfoEnrolIDAry['Title']);
                    $thisEnrolGroupSemester=$clubInfoEnrolIDAry['Semester'];
        		    $TemClass = $DisplaySemesterAry[$thisEnrolGroupSemester];
        		    $DisplaySemesterTitle = $thisEnrolGroupSemester==''?$i_ClubsEnrollment_WholeYear:$thisEnrolGroupSemester;?>		  
    			  <li class="<?php echo $TemClass;?>">
        			  <div class="clubTitle">
            			  <span><?php echo $thisEnrolGroupTitle;?></span> 
            			  <span class="tag label"><?php echo $DisplaySemesterTitle;?></span>
        			  </div>
        			  <div class="clashTimes"><span class="times"><?php echo count($clubMeetingDateAry[$thisEnrolGroupID]);?></span> 次</div>
    			  </li>			  
 			<? }?>
		  </ol>
      </div>
    </div>
	
    <div class="row marginB10">
      <div class="timeClashDetails col-xs-12">
		  <div class="table-responsive">
			  <table class="table">
				  <thead>
					  <th><?php echo $eEnrollmentMenu['act_date_time'];?></th>
					  <?php  
					  $i = 0;
					  foreach($clubInfoEnrolIDAssoAry as $thisEnrolGroupID=> $clubInfoEnrolIDAry){
					     
					      $thisEnrolGroupTitle= Get_Lang_Selection($clubInfoEnrolIDAry['TitleChinese'],$clubInfoEnrolIDAry['Title']);?>
            				<th><?php echo $Lang['eEnrolment']['ClubTypeAry'][$i];?><br />   
            					<?php if(!in_array($thisEnrolGroupID,$InParticipiantClubIDAry)){?>         				
                				<a data-toggle="modal" data-target="#deleteWarning<?php echo $thisEnrolGroupID;?>">               				
                					  <span class="delete">               				  
                					  <i class="fas fa-trash"></i>               					  
                					  </span>               					
                				</a>
                				  <?php  }?>
            				</th>
        					<?php 
        					$i++;
    					
					  }?>
				  </thead>
				  <tbody>
					<?php 
					for($i=0;$i<count($UniqueActivityDateAry);$i++){
// 					foreach($UniqueActivityDateAry as $UniqueActivityDate){   ?>
					  <tr>
					  <td><?php echo $UniqueActivityDateAry[$i];?></td>					  
						<?php 
						foreach($clubInfoEnrolIDAssoAry as $EnrolGroupID => $clubInfoEnrolIDAry){
						    
						      $ClubMeetingDateAry = $clubMeetingDateAry[$EnrolGroupID];
						      if(in_array($UniqueActivityDateAry[$i],$ClubMeetingDateAry)){ 
					             $display = '<a data-toggle="modal" data-target="#infoDetails'.$i.'" class="infoDetails"></a>';
					         }else{
					            $display = '&nbsp;';
					         }?>					        
					    <td><?php echo $display;?></td>    
					    <?  }   ?>
			          </tr>  
					<?php   
					
					}?>		
				  </tbody>
			  </table>
		  </div>
	  </div>
    </div>
  </div>
</div>


<?php 
foreach ($clubInfoEnrolIDAssoAry as $thisEnrolGroupID=> $clubInfoEnrolIDAry){ 
    $thisEnrolGroupTitle= Get_Lang_Selection($clubInfoEnrolIDAry['TitleChinese'],$clubInfoEnrolIDAry['Title']);
    ?>
  <div class="modal fade" id="deleteWarning<?php echo $thisEnrolGroupID;?>" tabindex="-1" role="dialog" aria-labelledby="deleteWarning" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="deleteWarninglLabel"><?php echo $button_quit;?></h5>
      </div>
        <div class="modal-body"><?php echo  str_replace("[__ClubTitle__]",$thisEnrolGroupTitle,$Lang['eEnrolment']['AreYouSureToRemoveFromList']);?></div>
        <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal"><?php echo $i_general_no;?></button>
        <a href="parent_eEnrol_checkTimeClash.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&deletedClubID=<?php echo $thisEnrolGroupID;?>&ClashGroupID=<?php echo implode(',',$EnrolGroupIDAry);?>&CurrentEnrolGroupID=<?php echo implode(',',$CurrentEnrolGroupIDAry);?>&CategoryIDList=<?php echo $CategoryIDList;?>&weekDayList=<?php echo $weekDayList;?>&CategoryIDCount=<?=$CategoryIDCount?>"><button type="button" class="btn"><?php echo $i_general_yes;?></button></a>
      </div>
      </div>
	</div>
  </div>
  <? }
?>
  
  <?php  
  for($i=0;$i<count($UniqueActivityDateAry);$i++){ ?>
    <div class="modal fade" id="infoDetails<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="infoDetails<?php echo $i;?>" aria-hidden="true">
    	<div class="modal-dialog" role="document">
        	<div class="modal-content">
                <div class="modal-header">
               		<h5 class="modal-title" id="infoLabel"><?php echo $UniqueActivityDateAry[$i];?> </h5>
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    	<span aria-hidden="true">
                    		<div class="icon"></div>
                    	</span> 
                	</button>
              	</div>
                <div class="modal-body">
                  <?php 
                  foreach($clubMeetingDateAry as $EnrolGroupID=>$_ClubMeetingDateAry){ 
                      if(in_array($UniqueActivityDateAry[$i],$_ClubMeetingDateAry)){
                          $__thisMeetingPosition = array_search($UniqueActivityDateAry[$i],$_ClubMeetingDateAry);
                          $MeetingCount = $__thisMeetingPosition+1;
                          $__thisMeetingTime = $clubMeetingTimeAry[$EnrolGroupID][$__thisMeetingPosition];
                          
                          ?>
                             <span><?php echo $clubInfoEnrolIDAssoAry[$EnrolGroupID]['Title'];?><br />第<?php echo $MeetingCount;?>次活動　<?php echo $__thisMeetingTime;?></span>
                	<?php }
                   } ?>   
               
                </div>
          	</div>
  		</div>
   </div>  
  <? } ?>
<script type="text/javascript">
function refreshParentPage(){
// 	window.parent.variableName
// 	window.opener.location.reload();
	window.opener.location.href='parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&deletedGroupID=<?php echo $deletedClubID;?>&EnrolGroupIDList=<?php echo implode(',',$CurrentEnrolGroupIDAry);?>&CategoryIDList=<?php echo $CategoryIDList;?>&weekDayList=<?php echo $weekDayList;?>&CategoryIDCount=<?=$CategoryIDCount?>'
	close();
}


</script>
</body>
</html>
