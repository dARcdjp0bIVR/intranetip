<?php 
//using: 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eEnrollment_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$quotaSettingsAry=  $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club'],$CategoryID=0);
$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);

// $quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();


$AcademicYearID = Get_Current_Academic_Year_ID();
$libAcademicYear = new academic_year($AcademicYearID);
$termNameAry = $libAcademicYear->Get_Term_List(1);
$termAry[0]['YearTermID'] = 0;
$termAry[0]['YearTermName'] = $Lang['General']['WholeYear'];
$i=0;
$DisplaySemesterAry[''] ='term'.$i;
foreach($termNameAry as $TermID => $TermName){
    $i++;
    $termAry[$i]['YearTermID'] = $i;
    $termAry[$i]['YearTermName'] = $TermName;
    
    $DisplaySemesterAry[$TermName] ='term'.$i;
}

$numOfTerm = count($termAry);


// $numOfQuotaType = count($quotaTypeAry);
$minQuatoClubAry = array();
// for ($i=0; $i<$numOfQuotaType; $i++) {
//     $_quotaType = $quotaTypeAry[$i];
//     if ($_quotaType == 'ApplyMin') {
//         $_title = $Lang['eEnrolment']['MinStudentApply'];
$thisClubSemesterAry = array();
$termBasedFormChecking = false;
if($libenroll->quotaSettingsType=='TermBase'){
    $termBasedFormChecking = true;
    for ($j=0; $j<$numOfTerm; $j++) {
        $_yearTermId = $termAry[$j]['YearTermID'];
        $_yearTermName = $termAry[$j]['YearTermName'];
        
        $_yearTermNum = $_yearTermId;
        //             debug_pr($_yearTermId);
        
        if ($_yearTermId > 0 && count($thisClubSemesterAry) > 0 && !in_array($_yearTermName, $thisClubSemesterAry)) {
            continue;
        }
        
        if ($_yearTermId == 0) {
            $_termName = $Lang['eEnrolment']['WholeYear'];
            $_termNum = 0;
        }
        else {
            $_termName = $_yearTermName;
            $_termNum = $j;
        }
        
        
        $_quotaTypeValue = $quotaSettingsAssoAry['ApplyMin'][$_termNum];
        
//         $minQuatoClubAry[] = $_quotaTypeValue.''.$Lang['eEnrolment']['Unit'].''.$_termName.'';
        $thisSemesterMinClubCountAry[] = $_quotaTypeValue;
        $MinClubAry[$_termName] = $_quotaTypeValue;
    }
}else if($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase'){
    
    
    $minNumOfApp = $quotaSettingsAssoAry['ApplyMin'][0]; 
 
    $MinClubAry[$Lang['eEnrolment']['WholeYear']] = $quotaTypeValue;
    
    $thisSemesterMinClubCountAry[0] = $minNumOfApp;
    $minQuatoClubAry[] = $minNumOfApp.' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'];
}

if($sys_custom['eEnrolment']['Application']['MinApply']){
//     $minQuatoClubAry = array();
    $custMinClubApply = 9;
}


$InParticipiantClubIDAry= array();

$appliedClubInfoAry = $libenroll->getChoicesOfStudent($StudentID);
// $appliedClubAssoAry = BuildMultiKeyAssoc($appliedClubInfoAry, 1);
foreach((array)$appliedClubInfoAry as $AppliedClubInfo){
    if($AppliedClubInfo['1'] == '2'){
        // in parcitiping or rejected club
        $InParticipiantClubIDAry[] = $AppliedClubInfo['0'];
    }
}

$EnrolGroupIDListAry = explode(',',$EnrolGroupIDList);
$EnrolGroupIDListAry= array_diff($EnrolGroupIDListAry,$InParticipiantClubIDAry);

$CategoryID = $CategoryID==''?explode(',',$CategoryIDList):$CategoryID;
$weekDay= $weekDay==''?explode(',',$weekDayList):$weekDay;
// debug_pr($EnrolGroupIDListAry);
$EnrolGroupID = $EnrolGroupID==''?$EnrolGroupIDListAry:$EnrolGroupID;
$AfterDeletedEnrolGroupIDAry = array_diff($EnrolGroupIDListAry,explode(' ',$deletedGroupID));
$AfterDeletedEnrolGroupIDAry = array_values($AfterDeletedEnrolGroupIDAry);

$CurrentEnrolGroupIDAry= $deletedGroupID==''?$EnrolGroupID:$AfterDeletedEnrolGroupIDAry;


$EnrolGroupIDAry = array_merge($CurrentEnrolGroupIDAry,$InParticipiantClubIDAry);

// $StudentID = '3519';
$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDAry);
$clubEnrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, array('Semester','EnrolGroupID'));
$clubInfoEnrolGroupIDAssoAry = BuildMultiKeyAssoc($clubInfoAry, 'EnrolGroupID');


### Get the meeting dates of clubs
$clubMeetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($clubEnrolGroupIdAry);
$clubMeetingDateAssoAry = BuildMultiKeyAssoc($clubMeetingDateInfoAry, 'EnrolGroupID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($clubMeetingDateInfoAry);

foreach($clubInfoAssoAry as $Semester=>$canApplyclubInfoAry){  
    $thisClubSemesterAry[] = $Semester;     
}

$clubNo = count($EnrolGroupIDAry);
$canApplyclubInfoAry2 = array();
for($i=0;$i<$clubNo;$i++){ 
    foreach($clubInfoEnrolGroupIDAssoAry as $_enrolGroupID=>$_enrolGroupInfoAry){  
        if($EnrolGroupIDAry[$i] == $_enrolGroupID){
            $canApplyclubInfoAry2[$_enrolGroupID] = $_enrolGroupInfoAry;                     
        }
    }
}


// $i=0;
// foreach($thisClubSemesterAry as $thisClubSemester){
//     $DisplaySemesterAry[$thisClubSemester] ='term'.$i;
//     $i++;
// }

$crash_EnrolGroupID_ary = array();
$crash_EnrolGroupName = array();
// get time crash club
for($i=0;$i<count($EnrolGroupIDAry);$i++){
    $_thisEnrolGroupID = $EnrolGroupIDAry[$i];
    for ($CountCrash = 0; $CountCrash < count($EnrolGroupIDAry); $CountCrash++) {
        if ( ($libenroll->IS_CLUB_CRASH($_thisEnrolGroupID,$EnrolGroupIDAry[$CountCrash])) && ( $_thisEnrolGroupID != $EnrolGroupIDAry[$CountCrash])) {          
            $crash_EnrolGroupID_ary[] = $EnrolGroupIDAry[$CountCrash];
            $thisClashEnrolClubInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($EnrolGroupIDAry[$CountCrash]);
            $crash_EnrolGroupName[] = Get_Lang_Selection($thisClashEnrolClubInfo['TitleChinese'],$thisClashEnrolClubInfo['Title']);
        }
    }
    
}
$crash_EnrolGroupID_ary = array_unique($crash_EnrolGroupID_ary);
$CheckTimeClashing = array();
for($i=0;$i<count($crash_EnrolGroupID_ary);$i++){
    if(in_array($crash_EnrolGroupID_ary[$i],$InParticipiantClubIDAry)){
        $CheckTimeClashing[] = false;
    }else{
        $CheckTimeClashing[] = true;
    }
}
$oneEnrolChecking = in_array(1,$CheckTimeClashing)? true:false;

$crash_EnrolGroupName = array_unique($crash_EnrolGroupName);
// $crash_EnrolGroupNameDispaly = implode('」、「',$crash_EnrolGroupName);
$crash_EnrolGroupNameDispaly = implode('", "',$crash_EnrolGroupName);
//<!-- <page contentType="text/html" pageEncoding="Big5"> -->
$WarningMessage = $libenroll->oneEnrol? $Lang['eEnrolment']['OnlyApplyOneClub']:$Lang['eEnrolment']['ConsiderApplyClashClub2'];

$CategoryIDCount = $CategoryIDCount;

$IsLessThanMinClubCount = false;
if(($custMinClubApply?$custMinClubApply:$minNumOfApp) > count($canApplyclubInfoAry2) - count($InParticipiantClubIDAry)){
	$IsLessThanMinClubCount = true;
}
?>

<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
<!--   <meta http-equiv="Content-Type" content="text/html; charset=Big5"> -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $Lang['eEnrolment']['EditClubOrder'];?></title>
  <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="../js/moment.js"></script>
  <script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
  <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>
  <script type="text/javascript" src="../js/sitemapstyler.js"></script>
  <script type="text/javascript" src="../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
  <script type="text/javascript" src="../bootstrap-select/js/bootstrap-select.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../bootstrap-select/css/bootstrap-select.min.css"/>
  <script type="text/javascript" src="../js/offcanvas.js"></script>
  <link rel="stylesheet" href="../css/offcanvas.css">
  <link rel="stylesheet" href="../css/eEnrolment.css">
  <script type="text/javascript" src="../js/eEnrolment.js"></script>
  <?php if($isStudentApp){?>
    <link rel="stylesheet" href="../css/eEnrolmentStudent.css">
	<? } ?>
  <link rel="stylesheet" href="../css/font-awesome.min.css">
  
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--   <link rel="stylesheet" href="https://resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<!--   <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script> -->
<!--   <script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script> -->
  
  <script src="../js/jquery.ui.touch-punch.min.js"></script>
  <script>
//   var updateUrl="parent_orderingSelectClub.php";
   var updateUrl ="ajax_get_new_order.php";
  
  self.sendUpdatedIndex = function ($item) {
	    // increment by 1 as we're using 1 based index on server
	    var startIndex = $item.data("startindex") + 1;

	    var newIndex = $item.index() + 1;
	   
	    if (newIndex != startIndex) {
	     var cluborder = []; 
		 var itemOrder = $('.sortingClub').sortable("toArray");

		 for (var i = 0; i < itemOrder.length; i++) {
			    cluborder[i+1] = itemOrder[i];
		 }
		
	    	$.ajax({
	    	    type: "POST",
	    	    url: updateUrl,
 				dataType: "html", 
	    	    data: {
	    	    	EnrolGroupID: itemOrder
		    	    },
	    	    success:  function (data) {
					console.log(data);
					 window.location.href = 'parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&CategoryIDCount=<?=$CategoryIDCount?>&EnrolGroupIDList=' + data;

		    	 },
		    	 complete: function(){ 
		    	    
		    	   }
	        	});	   

	    }
	}	
//   $('.sortingClub').draggable();
	
  $( function() {
	    $( ".sortingClub" ).sortable({
	      connectWith: ".sortingClub",
	      handle: ".sort",
	      start: function (event, ui) {
		     
	          $(ui.item).data("startindex", ui.item.index());
	      },
	      stop: function (event, ui) {
	          self.sendUpdatedIndex(ui.item);
	      }
	      
	    });
	 
	    $( ".functionIcon" )
	      .find( ".sort" )
	} );
 
  </script>  
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
  </head>
  
<form name="form1" id="form1" method="post" enctype="multipart/form-data">
  <body>
	<div id="wrapper"> 
    <!-- Header -->
        <nav id="header" class="navbar navbar-fixed-top enrolment edit">
        <div id="function">
            <div class="headerIcon"><a href="parent_eEnrol_applyClub_chooseClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&BackFromOrding=1&CategoryIDList=<?php echo implode(',',$CategoryID);?>&weekDayList=<?php echo implode(',',$weekDay);?>&NowEnrolGroupIDList=<?php echo implode(',',$CurrentEnrolGroupIDAry);?>"><i class="fas fa-arrow-left"></i></a></div>
          </div>
        <div class="headerTitle"><?php echo $Lang['eEnrolment']['EditClubOrder'];?></div>
        <div id="button">
            <div class="headerIcon invisible"><a href="#"><span class="selectedClub" id="selectedClub"></span></a><span class="clubNum">0</span></div>
            <div class="headerIcon invisible"> <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a> </div>
            <?php  if(!$IsLessThanMinClubCount && count($crash_EnrolGroupID_ary) != '0' && $oneEnrolChecking){ ?>          
            <div class="headerIcon">  
            	<a data-toggle="modal" data-target="#warning"><i class="fas fa-arrow-right"></i></a>
            </div>
            <?php }else{ ?>
            <div class="headerIcon visible">
            <?php 
//             if($libenroll->disableCheckingNoOfClubStuWantToJoin){
//                 $url = 'parent_eEnrol_club_start.php?StudentID='.$StudentID.'&EnrolGroupIDList='.implode(',',$EnrolGroupIDAry).'&FinishApplication=1&TimeClashGroupID='.implode(',',$crash_EnrolGroupID_ary);
//             }else{
//                 $url = 'parent_setMaxNoOfClub.php?StudentID='.$StudentID.'&EnrolGroupIDList='.implode(',',$EnrolGroupIDAry).'&EnroupGroupCount='.count($EnrolGroupIDAry).'&TimeClashGroupID='.implode(',',$crash_EnrolGroupID_ary);
//             }
            ?>
               <a onclick="javascript:checkSubmit()" class="next" id="nextStep">
               <i class="fas fa-arrow-right"></i>
               </a>
           </div>            
            <? }?>
          </div>
      </nav>
     </div>
     <div id="content">
        <?php if(count($crash_EnrolGroupID_ary) != 0  && $oneEnrolChecking){?>
            <div class="alertMsg">
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <!--  
                    <span>「<?php echo $crash_EnrolGroupNameDispaly;?>」<?php echo $Lang['eEnrolment']['ClubHaveTimeClash'];?><br /><?php echo $WarningMessage;?>
                    -->
                    <span><?php echo $Lang['eEnrolment']['ClubHaveTimeClash2'];?> "<?php echo $crash_EnrolGroupNameDispaly;?>"<br /><?php echo $WarningMessage;?>
                    </span> 
                </div>
            </div>
            <?php 
            foreach($clubInfoAssoAry as $Semester=>$enrolClubInforAry){
                 
                $Semester = $Semester== ''? $eEnrollment['YearBased'] :$Semester;
                $ClubCount = count($enrolClubInforAry);
                $Display[] = $ClubCount.$Lang['eEnrolment']['CountClub'].$Semester;
            }
            ?>
            <div class="noClubSelected col-xs-12"><? echo $Lang['eEnrolment']['YouHaveChoose'];?><?php echo implode(' '.$Lang['General']['And'].' ',$Display);?><?php echo ' '.$Lang['eEnrolment']['Club2'];?>︰
            </div>
        	<a href="javascript:checkClubTimeClash()" class="btn checkTimeClash" role="button" target="_blank">
        	<span class="icon timeClash"></span> 
        	<span class="timeClash"><?php echo $Lang['eEnrolment']['CheckandHandleTimeClash'];?></span>
        	</a>     
         <? }else{ ?>
             
             <div class="noClubSelected col-xs-12"> <?php echo $Lang['eEnrolment']['YouHaveSelected'].' '.count($CurrentEnrolGroupIDAry).' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'];?>
             </div>
             
        <? } ?>
       	<ul class="applyClub sortingClub col-xs-12">
    		<?php 
    		$i= 0;
    		foreach ($canApplyclubInfoAry2 as $thisEnrolGroupID=>$clubInforAry){
    		        $i++;
    		        $SemesterTitle = $clubInforAry['Semester'];
    		        $SemesterClass = $DisplaySemesterAry[$SemesterTitle];
    		        $DisplaySemesterTitle = $SemesterTitle==''?$i_ClubsEnrollment_WholeYear:$SemesterTitle;
    		       
    		        $thisClubTitle= Get_Lang_Selection($clubInforAry['TitleChinese'],$clubInforAry['Title']);
    		        $thisClubGroupCategoryID = $clubInforAry['GroupCategory'];
    		        $thisClubGroupCategoryName = $clubInforAry['CategoryName'];
    		     
    		        $ShowSort = $libenroll->tiebreak;
    		        $thisClubMeetingDateAry = $clubMeetingDateAssoAry[$thisEnrolGroupID];
    	
    		        $_meetingWeekdayDisplay = '---';
    		        if(!empty($thisClubMeetingDateAry) ){
    		            $MeetingWeekdayAry = array();
    		            $_thisMeetingDate = array();
    		            
    		            foreach($thisClubMeetingDateAry as $ClubMeetingDateAry){	                
    		                $_thisMeetingDateNo = date("N", strtotime($ClubMeetingDateAry['ActivityDateStart']));
    		                $_thisMeetingDate[] = $_thisMeetingDateNo;		                
    		            }
 		            
    		            $thisMeetingDateAry= array_unique($_thisMeetingDate);
    		            asort($thisMeetingDateAry);
    		            $MeetingWeekdayAry = array();
    		            foreach($thisMeetingDateAry as $thisMeetingDate){
    		                $MeetingWeekdayAry[] = $Lang['eEnrolment']['WeekDay'][$thisMeetingDate];
    		            }
    		            $_meetingWeekdayDisplay = implode('、',$MeetingWeekdayAry);
    		        }
    		        
    		        if(!in_array($thisEnrolGroupID,$InParticipiantClubIDAry)){
    		      
    		?>
            <li class="<?php echo $SemesterClass;?> ui-state-default" id="<?php echo $thisEnrolGroupID;?>" name="<?=$thisClubGroupCategoryID?>"> <a href="parent_eEnrol_applyClubDetials.php?isStudentApp=<?=$isStudentApp?>&EnrolGroupID=<?php echo $thisEnrolGroupID;?>" class="left" >
              <div class="orderNum"><?php echo $i;?></div>
              <div class="clubTitle"><span><?php echo $thisClubTitle;?></span> <span class="tag label"><?php echo $DisplaySemesterTitle;?></span></div>
              <div class="clubInfo">
              <span class="date"><?=$thisClubGroupCategoryName?></span> 
              <div class="row">
               <span class="icon"><i class="far fa-clock"></i></span>
               <span class="date"><?php echo $_meetingWeekdayDisplay;?></span> 
               </div>
              	<?php 
             	if($ShowSort){ 
             	    $AppledQuato = $libenroll->getQuota($thisEnrolGroupID);
             	    $ParAry['EnrolGroupID'] =$thisEnrolGroupID;
             	    $ApplyedStudent = $libenroll->GET_APPLY_STUDENT($ParAry);
             	    
             	    $ApplyedStudentIDAry = Get_Array_By_Key($ApplyedStudent, 'UserID');
             	    $ApplyedStudentCount = in_array($StudentID,$ApplyedStudentIDAry)?count($ApplyedStudent):count($ApplyedStudent)+1;      	    
             	    ?>
             	    <div class="row"> <span class="icon ordering"></span><span class="order"><?php echo $eEnrollment['position'];?><small> (<?php echo $Lang['eEnrolment']['Quato'];?>)</small>︰<br class="break"><?php echo $ApplyedStudentCount;?><small> (<?php echo $AppledQuato;?>)</small></span> </div>
             
             	<? }?>
             	
             <?   if(in_array($thisEnrolGroupID,$crash_EnrolGroupID_ary)){          
                 ?>
                    <div class="row"><span class="icon timeClash"></span><span class="timeClash"><?php echo $eEnrollment['time_crash'];?></span> </div>
             	<?php } ?>
           </div>
              </a>
            <div class="functionIcon"> 
                <a data-toggle="modal" data-target="#deleteWarning<?php echo $thisEnrolGroupID;?>">
                    <span class="delete">
                    	<i class="fas fa-trash"></i>
                    </span>
                </a> 
            	<span class="sort" id="sortable" value="<?php echo $thisEnrolGroupID;?>"></span>
            </div>
          </li>       
          <?php 
    		}
    		} ?>
          </ul>
  	</div>
 
  <?php 
  foreach ($canApplyclubInfoAry2 as $thisEnrolGroupID=>$clubInforAry){
      $thisClubTitle= Get_Lang_Selection($clubInforAry['TitleChinese'],$clubInforAry['Title']);?>
  <div class="modal fade" id="deleteWarning<?php echo $thisEnrolGroupID;?>" tabindex="-1" role="dialog" aria-labelledby="deleteWarning" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="deleteWarninglLabel"><?php echo $button_quit;?></h5>
      </div>
        <div class="modal-body"><?php echo str_replace("[__ClubTitle__]",$thisClubTitle,$Lang['eEnrolment']['AreYouSureToRemoveFromList']);?></div>
        <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal"><?php echo $i_general_no;?></button>
        <a href="parent_orderingSelectClub.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&deletedGroupID=<?php echo $thisEnrolGroupID;?>&EnrolGroupIDList=<?php echo implode(',',$CurrentEnrolGroupIDAry);?>&CategoryIDList=<?php echo implode(',',$CategoryID);?>&weekDayList=<?php echo implode(',',$weekDay);?>&CategoryIDCount=<?=$CategoryIDCount?>"><button type="button" class="btn"><?php echo $i_general_yes;?></button></a>
      </div>
      </div>
	</div>
  </div>
  <?php 
      }
//   }
  ?>
  
  <div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="warning" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="warninglLabel"><i class="fas fa-exclamation-triangle"></i> <?php echo $eEnrollment['time_crash'];?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button>
      </div>
      <?php if($libenroll->oneEnrol && $oneEnrolChecking){
        $WarningMessageNextStep = '「'.$crash_EnrolGroupNameDispaly.'」'.$Lang['eEnrolment']['ClubHaveTimeClash'].'<br>'.$Lang['eEnrolment']['OnlyCanApplyOneClub'];
      ?>     
        <div class="modal-body"><span><?php echo $WarningMessageNextStep;?></span>
          </div>   
      <?php 
      }else{
//           $WarningMessageNextStep = $Lang['eEnrolment']['Club'].$Lang['eEnrolment']['ClubHaveTimeClash'].'<br>'.$Lang['eEnrolment']['SuretoApplyClashClub'];
          $sentence = str_replace("with", "between", $Lang['eEnrolment']['ClubHaveTimeClash2']);
          $club = str_replace("Club", "clubs", $Lang['eEnrolment']['Club']);
          $clashClub = str_replace("club", "club(s)", $Lang['eEnrolment']['SuretoApplyClashClub']);
          $WarningMessageNextStep = $sentence.$club.'<br>'.$clashClub;
      ?>
      <div class="modal-body"><span><?php echo $WarningMessageNextStep;?></span></div>
     	<div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal"><?php echo $Lang['Btn']['Back'];?></button>
        
        <a onclick="javascript:checkSubmit()"><button type="button" class="btn"><?php echo $eEnrollment['confirm'];?></button></a>
        
        </div>
      <?php }?>
    </div>
  </div>
  </div>
  
<div class="modal fade" id="warningMin" tabindex="-1" role="dialog" aria-labelledby="warningMin" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warninglLabelMin"><i class="fas fa-exclamation-triangle"></i><?php echo $Lang['eEnrolment']['NotAchieveMinClubCount'];?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">
        <div class="icon"></div>
        </span> </button>
      </div>
      <div class="modal-body">
			<span id="alertClubMinCountMessage"></span>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function checkClubTimeClash() {	
	window.open("parent_eEnrol_checkTimeClash.php?isStudentApp=<?=$isStudentApp?>&StudentID=<?php echo $StudentID;?>&ClashGroupID=<?php echo implode(',',$crash_EnrolGroupID_ary);?>&CurrentEnrolGroupID=<?php echo implode(',',$CurrentEnrolGroupIDAry);?>&CategoryIDList=<?php echo implode(',',$CategoryID);?>&weekDayList=<?php echo implode(',',$weekDay);?>&CategoryIDCount=<?=$CategoryIDCount?>","", "");
	$.ajax({
		type: "POST",
		url: "parent_eEnrol_checkTimeClash.php",
		data: "",
		success: function (xml) {
			window.location.reload();
		}
	});
}
function checkSubmit(){

	var returnMinMessage = '<?php echo $Lang['eEnrolment']['YouNeedChoose'];?>';
	var alertMinMessage= false;
  	var alertMinCategoryMessage = false;
	<?php 	
	if($termBasedFormChecking){
	foreach ((array)$MinClubAry as $semesterName => $Count){
	    $SelectedCount = 0;
	    foreach((array)$canApplyclubInfoAry2 as $_enrolGroupID=>$EnrolGroupInfo){

	        if($EnrolGroupInfo['YearTermName'] == $semesterName){       
	            $SelectedCount ++;
	        }	  	        
	   }	
	   ?>
	   
	 var thisSemesterMinClubCount = <?php echo $Count;?>;
	 var SelectedClubLength = <?=$SelectedCount?>;

	 if(SelectedClubLength < thisSemesterMinClubCount ){	 
	    alertMinMessage = true;   	
	    returnMinMessage += '<br>'+thisSemesterMinClubCount +'<?php echo ' '.$Lang['eEnrolment']['Unit'].''.$semesterName.' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'].$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['FullStop'];?>';
	 }

	<?php }	
    }else{?>
        var SelectedClubLength = <?=count($canApplyclubInfoAry2)?> - <?=count($InParticipiantClubIDAry)?>;
        var MinClubCount = <?=$minNumOfApp?>;
    
        if(SelectedClubLength < MinClubCount){
    		alertMinMessage = true;   	
    		returnMinMessage += '<br>'+MinClubCount +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'].$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'];?>';
        }

    <?php }?> 

    <?php if($custMinClubApply){ ?>
    	alertMinMessage = false;
        var SelectedClubLength = <?=count($canApplyclubInfoAry2)?> - <?=count($InParticipiantClubIDAry)?>;
        var MinClubCount = <?=$custMinClubApply?>;
    
        if(SelectedClubLength != MinClubCount){
    		alertMinMessage = true;   	
    		returnMinMessage = '<?php echo $Lang['eEnrolment']['YouNeedChoose'];?><br>'+MinClubCount +'  <?php echo ' '.$Lang['eEnrolment']['Unit'].' '.$Lang['eEnrolment']['Club'].$Lang['eEnrolment']['Comma'].$Lang['eEnrolment']['NowYouOnlyChoose'];?> '+ SelectedClubLength +' <?php echo ' '.$Lang['eEnrolment']['Unit'].$Lang['eEnrolment']['Club'];?>';
        }
    
    <? } ?>

    <?php if($sys_custom['eEnrolment']['Application']['EachCategoryNeed']){ ?>

 		var SelectedClubLength = <?=count($canApplyclubInfoAry2)?> - <?=count($InParticipiantClubIDAry)?>;
      	var categoryLength = '<?=$CategoryIDCount?>'; 

       	if(SelectedClubLength < categoryLength){
       		alertMinCategoryMessage = true;
       		returnMinCategoryMessage = '<?=$Lang['eEnrolment']['WarningMsg']['NeedSelectEachCategory']?>';
           }
   <?php  } ?>

    
	if(alertMinMessage){
		 $("#nextStep").attr("data-target","#warningMin"),
		 $("#nextStep").attr("data-toggle","modal"),
		 document.getElementById("alertClubMinCountMessage").innerHTML =  returnMinMessage;		 	 
	}
	else if(alertMinCategoryMessage){
		 $("#nextStep").attr("data-target","#warningMin"),
		 $("#nextStep").attr("data-toggle","modal"),
		 document.getElementById("alertClubMinCountMessage").innerHTML =  returnMinCategoryMessage;		 	 
	}
	else{
		submitForm();
		}
}

function submitForm()
{	
	<?php 
	if($libenroll->disableCheckingNoOfClubStuWantToJoin){
	    $url = 'parent_eEnrol_club_start.php?isStudentApp='.$isStudentApp.'&StudentID='.$StudentID.'&FinishApplication=1&EnrolGroupIDList='.implode(',',$CurrentEnrolGroupIDAry).'&TimeClashGroupID='.implode(',',$crash_EnrolGroupID_ary).'&CategoryIDCount='.$CategoryIDCount;
	}else{
	    $url = 'parent_setMaxNoOfClub.php?isStudentApp='.$isStudentApp.'&StudentID='.$StudentID.'&EnrolGroupIDList='.implode(',',$CurrentEnrolGroupIDAry).'&EnroupGroupCount='.count($CurrentEnrolGroupIDAry).'&TimeClashGroupID='.implode(',',$crash_EnrolGroupID_ary).'&CategoryIDCount='.$CategoryIDCount;
	}
	?>
	document.form1.action = '<?php echo $url;?>';
	document.form1.method = "POST";
	document.form1.submit();
	  
}
</script>
</body>
</form>
</html>
