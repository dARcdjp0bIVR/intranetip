<?php
/*
 * 2018-04-30 Cameron
 * - not need to apply convert2unicode to the whole output for ej because medical uses utf8
 *
 * 2018-04-23 Cameron
 * - create this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/cust/medical/medicalConfig.inc.php");
include_once ($PATH_WRT_ROOT . "includes/cust/common_function.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical_app_api.php");
include_once $PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLog.php";
include_once $PATH_WRT_ROOT . "includes/cust/medical/libMedicalShowInput.php";
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev1.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev2.php");

class Management extends Controller
{

    public function __construct($info = array())
    {
        parent::__construct($info);
    }

    public function __destruct()
    {
        if (isset($this->medicalAPI)) {
            $this->closeMedicalApiConnection();
        }
    }

    private function isEJ()
    {
        global $junior_mck;
        return isset($junior_mck);
    }

    public function _remap($method)
    {
        global $Lang, $intranet_session_language, $intranet_root, $PATH_WRT_ROOT;
        global $w2_cfg, $medical_cfg, $plugin, $sys_custom;
        
        $medical_api = $this->getMedicalApiConnection();
        
        $method = $this->str_replace_first("cus_", "", $method);
        $methodData = array(
            "controllerLink" => "medical/sleep/management",
            "pageLimit" => isset($_POST["cus"]["limit"]) ? $_POST["cus"]["limit"] : 10,
            "currPage" => isset($_POST["cus"]["page"]) ? $_POST["cus"]["page"] : 1
        );
        $this->infoArr["postData"]["limit"] = $methodData["pageLimit"];
        $this->infoArr["postData"]["page"] = $methodData["currPage"];
        
        $jsStatus = "success"; // success => print $method html, fail => return to orignal page (default success)
        $jsError = "";
        
        /**
         * ** Set up Access Web View required Data ***
         */
        switch ($method) {
            
            // Add Record - Step 1 (Student)
            case "edit":
                // Get Student Options when back from Step 2
                $selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
                
                if (! empty($selected_student)) {
                    $this->infoArr["UIContent"]["StudentSelection"] = $medical_api->getTargetUserDropDownOption($selected_student[0]);
                }
                // Unset POST Data
                if (isset($_POST)) {
                    if (isset($_POST["sld_student"])) {
                        unset($_POST["sld_student"]);
                        unset($this->infoArr["postData"]["formData"]["sld_student"]);
                    }
                }
                break;
            
            // Add Record - Step 2 (Record Details)
            case "edit_step2":
                // Check Empty Student
                $selected_student = $this->infoArr["postData"]["formData"]["sld_student"];
                
                if (empty($selected_student)) {
                    // Set Error Message: No Students
                    $jsError = "noSelectedStudents";
                    $jsStatus = "fail";
                } else {
                    global $sys_custom;
                    
                    // Get Summary Table Content
                    $RecordSummaryAry = array();
                    foreach ((array) $selected_student as $thisStudentID) { // one student only
                        $thisStudentInfo = $medical_api->getStudentInfoByID($thisStudentID);
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            $RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
                        }
                    }
                    $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                    
                    // get sleep status list (level1 items)
                    $studentSleepLev1 = $medical_api->getStudentSleepLev1();
                    $this->infoArr["UIContent"]["SleepStatusSelect"] = $studentSleepLev1[1];
                    $this->infoArr["UIContent"]["SleepColorSelect"] = $studentSleepLev1[0];
                    
                    // get sleep reason list (level2 items)
                    $this->infoArr["UIContent"]["SleepReasonSelect"] = $medical_api->getStudentSleepLev2('', - 1); // show "Please Select" first
                                                                                                                 
                    // get sleep frequency
                    $this->infoArr["UIContent"]["SleepFrequencySelect"] = $medical_api->getSleepFrequency($selected_frequency = '');
                    
                    // Get Sleep Default Remarks
                    $this->infoArr["UIContent"]["SleepDefaultRemarks"] = $medical_api->getDefaultSleepRemarks();
                    
                    // Get Hidden Fields
                    $student_field = "";
                    foreach ((array) $selected_student as $thisStudentID) {
                        $student_field .= "<input type=\"hidden\" name=\"sld_student[]\" value=\"" . $thisStudentID . "\">\n";
                    }
                    $this->infoArr["UIContent"]["HiddenInputField"] = $student_field;
                }
                break;
            
            // Edit Record
            case "edit_record":
                
                // get pass in parameters
                $RecordID = $this->infoArr["postData"]["formData"]["RecordID"];
                
                if (! $RecordID) {
                    // Set Error Message: No record identifier
                    $jsError = "noRecordID";
                    $jsStatus = "fail";
                } else {
                    $objStudentSleepLog = new StudentSleepLog($RecordID);
                    if (! $objStudentSleepLog->getUserID()) {
                        $jsError = "noRecordID";
                        $jsStatus = "fail";
                    } else {
                        $studentID = $objStudentSleepLog->getUserID();
                        $recordTime = $objStudentSleepLog->getRecordTime();
                        $sleepID = $objStudentSleepLog->getSleepID(); // Sleep Status
                        $frequency = $objStudentSleepLog->getFrequency();
                        $reasonID = $objStudentSleepLog->getReasonID();
                        $remarks = $objStudentSleepLog->getRemarks();
                        
                        // Get Summary Table Content
                        $RecordSummaryAry = array();
                        
                        $thisStudentInfo = $medical_api->getStudentInfoByID($studentID);
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            $RecordSummaryAry[] = "$thisStudentName ($thisClassNameNum)";
                        }
                        
                        $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                        
                        $this->infoArr["UIContent"]["SleepRecordDate"] = substr($recordTime, 0, 10);
                        
                        // Get Sleep Status
                        $studentSleepLev1 = $medical_api->getStudentSleepLev1($sleepID);
                        $this->infoArr["UIContent"]["SleepStatusSelect"] = $studentSleepLev1[1];
                        $this->infoArr["UIContent"]["SleepColorSelect"] = $studentSleepLev1[0];
                        
                        // get sleep reason list (level2 items)
                        $this->infoArr["UIContent"]["SleepReasonSelect"] = $medical_api->getStudentSleepLev2($reasonID, $sleepID); // show "Please Select" first
                        
                        $this->infoArr["UIContent"]["SleepFrequencySelect"] = $medical_api->getSleepFrequency($frequency);
                        $this->infoArr["UIContent"]["SleepRemarks"] = $remarks;
                        
                        // Get Sleep Default Remarks
                        $this->infoArr["UIContent"]["SleepDefaultRemarks"] = $medical_api->getDefaultSleepRemarks();
                        
                        // Get Hidden Fields
                        $hidden_field = "<input type=\"hidden\" name=\"sld_student[]\" value=\"" . $studentID . "\">\n";
                        $hidden_field .= "<input type=\"hidden\" id=\"RecordID\" name=\"RecordID\" value=\"" . $RecordID . "\">\n";
                        $this->infoArr["UIContent"]["HiddenInputField"] = $hidden_field;
                        
                        $this->infoArr["UIContent"]["RecordID"] = $RecordID;
                    }
                }
                break;
            
            // View Record
            case "view":
                
                // get pass in parameters
                $RecordID = $_GET["RecordID"];
                $userType = $_SESSION["UserType"];
                
                if (! $RecordID) {
                    // Set Error Message: No record identifier
                    $jsError = "noRecordID";
                    $jsStatus = "fail";
                } else {
                    $objStudentSleepLog = new StudentSleepLog($RecordID);
                    if (! $objStudentSleepLog->getUserID()) {
                        $jsError = "noRecordID";
                        $jsStatus = "fail";
                    } else {
                        $studentID = $objStudentSleepLog->getUserID();
                        $recordTime = $objStudentSleepLog->getRecordTime();
                        $sleepID = $objStudentSleepLog->getSleepID(); // Sleep Status
                        $frequency = $objStudentSleepLog->getFrequency();
                        $reasonID = $objStudentSleepLog->getReasonID();
                        $remarks = $objStudentSleepLog->getRemarks();
                        $modifiedBy = $objStudentSleepLog->getModifiedBy();
                        $modifiedOn = $objStudentSleepLog->getDateModified();
                        
                        // Get Summary Table Content
                        $RecordSummaryAry = array();
                        
                        $thisStudentInfo = $medical_api->getStudentInfoByID($studentID);
                        
                        if (! empty($thisStudentInfo)) {
                            $thisStudentName = $thisStudentInfo['Name'];
                            $thisClassNameNum = $thisStudentInfo['ClassName'] . " - " . $thisStudentInfo['ClassNumber'];
                            if ($userType == '1') {
                                $studentInfo = "$thisStudentName ($thisClassNameNum)";
                            } else {
                                $studentInfo = $thisStudentName;
                            }
                            $RecordSummaryAry[] = $studentInfo;
                        }
                        $this->infoArr["UIContent"]["UserType"] = $userType;
                        
                        $this->infoArr["UIContent"]["SummaryTable"] = implode("<br/>", $RecordSummaryAry);
                        
                        // Get Sleep Status
                        $objSleepStatus = new studentSleepLev1($sleepID);
                        $this->infoArr["UIContent"]["SleepStatusName"] = $objSleepStatus->getStatusName();
                        $this->infoArr["UIContent"]["SleepStatusColor"] = $objSleepStatus->getColor();
                        
                        $objSleepReason = new studentSleepLev2($reasonID);
                        $this->infoArr["UIContent"]["SleepReasonName"] = $objSleepReason->getReasonName();
                        $this->infoArr["UIContent"]["SleepFrequency"] = $frequency;
                        
                        // Get Sleep RecordDate
                        $this->infoArr["UIContent"]["SleepRecordDate"] = substr($recordTime, 0, 10);
                        
                        $lastModifiedInfo = $medical_api->getPersonInfo($modifiedBy);
                        
                        $lastUpdatedBy = $lastModifiedInfo['Name'] . (($lastModifiedInfo['UserType'] == USERTYPE_PARENT) ? " (" . $Lang['medical']['general']['parent'] . ")" : "");
                        
                        $this->infoArr["UIContent"]["LastUpdatedBy"] = $lastUpdatedBy;
                        
                        $this->infoArr["UIContent"]["LastUpdatedOn"] = $modifiedOn;
                        
                        $this->infoArr["UIContent"]["SleepRemarks"] = $remarks;
                        
                        if (! libMedicalShowInput::attendanceDependence('sleep')) {
                            $present = 1; // Always present if the school have not buy attendance module
                        } else {
                            $present = $medical_api->getAttendanceStatusOfUser($studentID, substr($recordTime, 0, 10));
                        }
                        
                        $show_attendance = ($present == 1) ? $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Present'] : $Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
                        $this->infoArr["UIContent"]["AttendanceStatus"] = $show_attendance;
                        
                        // Get Hidden Fields
                        $hidden_field = "<input type=\"hidden\" id=\"RecordID\" name=\"RecordID\" value=\"" . $RecordID . "\">\n";
                        
                        $this->infoArr["UIContent"]["HiddenInputField"] = $hidden_field;
                        $this->infoArr["UIContent"]["RecordID"] = $RecordID;
                    }
                }
                break;
        }
        
        // Get Web View
        $responseHTML = $this->view($methodData);
        
        // Set up defualt JSON return
        $param = array(
            "hash" => $this->infoArr["strHash"],
            "pageType" => $method,
            "respType" => "HTML",
            "content" => $responseHTML,
            "customJS" => "",
            "pData" => $this->infoArr["postData"],
            "status" => $jsStatus,
            "error" => $jsError
        );
        
        /**
         * ** Perform AJAX required Actions ***
         */
        switch ($method) {
            
            // Add Record - Step 3 - Save sleep record
            case "edit_step3":
                // POST Data (Add Record > "formData")
                $thisFormData = $this->infoArr["postData"]["formData"];
                
                $success = false;
                
                // Insert Sleep Record
                if (! empty($thisFormData)) {
                    $jsErrorMsg = $medical_api->addSleepRecord($thisFormData);
                    if (empty($jsErrorMsg)) {
                        $success = true;
                    } else {
                        $param["error"] = $jsErrorMsg;
                    }
                } else {
                    $param["error"] = "noFormData";
                }
                
                $param["status"] = $success ? "success" : "fail";
                
                break;
            
            case "edit_update":
                // POST Data (Update Record > "formData")
                $thisFormData = $this->infoArr["postData"]["formData"];
                
                $success = false;
                
                // Update Sleep Record
                if (! empty($thisFormData)) {
                    $retInfo = $medical_api->updateSleepRecord($thisFormData);
                    $jsErrorMsg = $retInfo["error"];
                    if (empty($jsErrorMsg)) {
                        $success = true;
                    } else {
                        $param["error"] = $jsErrorMsg;
                    }
                } else {
                    $param["error"] = "noFormData";
                }
                
                $param["status"] = $success ? "success" : "fail";
                $param["RecordID"] = $retInfo["RecordID"];
                
                break;
            
            // Delete Sleep
            case "delete":
                
                $RecordID = $_POST["RecordID"];
                
                $objStudentSleepLog = new StudentSleepLog($RecordID);
                $result = $objStudentSleepLog->deleteRecord($RecordID);
                $param["status"] = $result ? "success" : "fail";
                
                break;
        }
        
        // Output JSON return
        sleep(1);
        $this->outputJSON($param);
    }

    private $medicalAPI;

    private function getMedicalApiConnection()
    {
        global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language, $plugin;
        if (isset($this->medicalAPI)) {
            return $this->medicalAPI;
        } else {
            
            intranet_opendb();
            
            $medicalAPI = new libMedical_app_api();
            $medicalAPI->controllerModule = "sleep";
            $medicalAPI->setMedicalImagePath($this->infoArr["thisImagePath"]);
            
            $objMedical = new libMedical();
            if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_MANAGEMENT') || ! $plugin['medical_module']['sleep']) {
                $medicalAPI->AllowAccess = false;
            } else {
                $medicalAPI->AllowAccess = true;
            }
            
            return $this->medicalAPI = $medicalAPI;
        }
    }

    private function closeMedicalApiConnection()
    {
        intranet_closedb();
    }
}