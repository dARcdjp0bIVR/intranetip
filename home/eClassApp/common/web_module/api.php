<?php
// using: 
/*
 *  2019-02-15 Cameron
 *      - add lang file for eBooking (send email use it) [case #X155789]
 *      
 * 	2017-12-04 Cameron
 * 		- Add lang for medical
 */
include_once '../../../../'.'includes/global.php';
include_once '../../../../'.'includes/libdb.php';
include_once '../../../../'.'lang/lang.'.$_SESSION['intranet_session_language'].'.php';
// include_once '../../../../'.'lang/ebooking_lang.'.$_SESSION['intranet_session_language'].'.php';

$moduleName = explode('/',$_GET["page"]);
if($moduleName[0]=='ebooking'){
	include_once '../../../../'.'lang/ebooking_app_lang.'.$_SESSION['intranet_session_language'].'.php';
	include_once '../../../../'.'lang/ebooking_lang.' . $_SESSION['intranet_session_language']. '.php';
}
if($moduleName[0]=='medical'){
	include_once '../../../../'.'lang/cust/medical_lang.'.$_SESSION['intranet_session_language'].'.php';
}
if($intranet_version == "2.0" || isset($junior_mck)){
    include_once '../../../../'.'lang/lang.'.$intranet_session_language.'.utf8.php';
}

$token_key = "taist23657";
if (isset($_POST["id"])) {
	$thisToken = md5($_POST["id"] . " " . $token_key);
}
else {
	$thisToken = "";
}

if (isset($_SESSION['UserID']) && isset($_POST["token"]) && $_POST["token"] == $thisToken && !empty($thisToken)) {
	header('Content-Type: text/html; charset=utf-8');
	header("Access-Control-Allow-Origin: *");
	
	$info = array();
	$info["hostname"] = $_SERVER["HTTP_HOST"];
	$info["requestUri"] = $_SERVER["REQUEST_URI"];
	$info["thisUriPath"] = dirname($info["requestUri"]);
	$info["thisPath"] = dirname(__FILE__);
	$info["thisImagePath"] = "/home/eClassApp/common/web_module/assets/img";
	
	$controller_path = "./controllers/";
	$libs_path = "./libs/";
	$forceUseClass = false;
	
	include_once("../../../../includes/json.php");
	include_once($controller_path . "controller.php");
	
	$info["postData"] = $_POST;
	$info["getData"] = $_GET;
	$info["strHash"] = isset($_GET["page"]) ? $_GET["page"] : "ediscipline/goodconduct_misconduct/management/mylist";
	$info["loadType"] = isset($_POST["loadType"]) ? $_POST["loadType"] : "page";
	$info["controllerInfo"] = explode("/", $info["strHash"]);
	$clrPath = $info["controllerInfo"];
	if (count($info["controllerInfo"]) > 1) {
		array_pop($clrPath);
		$info["custLink"] = implode("/", $clrPath);
		$info["className"] = ucfirst(strtolower($clrPath[count($clrPath) - 1]));
		$info["methodName"] = "cus_" . basename($info["strHash"]);
	}


	$controller_file = $controller_path . strtolower(implode("/", $clrPath)) . ".php";
	
	if (file_exists($controller_file)) {
		include_once($controller_file);
		$pageController = new $info["className"]($info);
		if (method_exists($pageController, "_remap")) {
			$pageController->_remap($info["methodName"]);
		}
		else {
			header("HTTP/1.0 404 Not Found");
			exit;
		}
	}
	else {
		header("HTTP/1.0 404 Not Found");
		exit;
	}
}
else {
	header("HTTP/1.0 404 Not Found");
	exit;
}
?>