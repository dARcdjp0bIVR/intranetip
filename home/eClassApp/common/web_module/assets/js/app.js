/************************************************/
/* formJS controller
// Using:  
/************************************************/
var formJS = {
	vrs: {
		submitBtnObj: "a.submitBtn",
		cancelBtnObj: "a.cancelBtn",
		cusValBtnObj: "a.cusValBtn",
		ato_xhr: "",
		xhr: "",
		sldObj: "",
		glob_data: {},
		autoCompleteTimer: 0
	},
	ltr: {
		submitHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler", "listener");
			//destroy listener
			pageJS.cfn.destroyListener();
			formJS.cfn.destroyListener();
			
			var formObj = $(this).attr('rel');
			if ($("#" + formObj).length > 0) {
				var isAllowSubmit = true;
				if ($("#" + formObj).find('input.required').length > 0) {
					$("#" + formObj).find('input.required').each(function() {
						if ($(this).val() == "" || typeof $(this).val() == "undefined") {
							//console.log($(this).attr('name'), "-" + $(this).val(), "=" + typeof $(this).val());
							isAllowSubmit = false;
						}
					});
				}
				
				if ($("#" + formObj).find('select.required').length > 0) {
					$("#" + formObj).find('select.required').each(function() {
						if ($(this).find('option:selected').length == 0) {
							isAllowSubmit = false;
						}
					});
				}
				
				// for a pair of timepicker
				if ($("#" + formObj).find('input.req-fromtime').length == 1 && $("#" + formObj).find('input.req-totime').length == 1 ) {
					var fromTime = $("#" + formObj).find('input.req-fromtime').val();
					var toTime = $("#" + formObj).find('input.req-totime').val();
					var turnToSecond = function (hhmm){
						var a = hhmm.split(':');
						return (+a[0]) * 60 * 60 + (+a[1]) * 60 ;
					}
					if( turnToSecond(fromTime) >= turnToSecond(toTime)){
						isAllowSubmit = false;
						Materialize.toast(_langtxt["incorrectTime"] , 4000);
					}
				}
				
				// for a pair of datepicker
				if ($("#" + formObj).find('input.req-fromdate').length == 1 && $("#" + formObj).find('input.req-todate').length == 1 ) {
					var fromDate = $("#" + formObj).find('input.req-fromdate').val();
					var toDate = $("#" + formObj).find('input.req-todate').val();
					var dateCompare = function (from, to){
						fromDateObj = new Date(from);
						toDateObj = new Date(to);
						if(fromDateObj > toDateObj){
							// if invalid
							return true;
						}else{
							// if valid
							return false;
						}
					}
					if( dateCompare(fromDate,toDate)){
						isAllowSubmit = false;
						Materialize.toast(_langtxt["incorrectTime"] , 4000);
					}
				}
				
				if (!isAllowSubmit) {
					//do nothing
					//bind listener
					pageJS.cfn.bindListener();
					formJS.cfn.bindListener();
					//Materialize.toast('Error', 4000);
				} else {

					var successURL = $("#" + formObj).attr('data-success');
					// var formData = $("#" + formObj).serialize();
					var serialized = $("#" + formObj).serializeArray();
			        var formData = JSON.stringify(serialized);
					if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle(formData, "data", strUrl);
					var strUrl = $("#" + formObj).attr('rel');
					var xhr = formJS.vrs.xhr;
					if (xhr != "") {
						xhr.abort();
					}
					//global
					formJS.vrs.glob_data[strUrl] = formData;
					
					var form_submitdata = pageJS.vrs.data;
					form_submitdata.formData = formData
					form_submitdata.srhtxt = "formSubmit";
					form_submitdata.stepData = formJS.vrs.glob_data;
					
					Materialize.toast(_langtxt["loading"], 4000);
					formJS.vrs.xhr = $.ajax({
						url: pageJS.vrs.url + "?page=" + strUrl,
						data: form_submitdata,
						timeout: 25000,
						cache: false,
						dataType: "json",
						type: "POST",
						success: function(respdata, status, jqXHR) {
							
							$('.toast').remove();
							if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > success", "listener");
							if (typeof respdata.status !="undefined" && respdata.status == "success") {
//								formJS.vrs.glob_data[strUrl]["response"] = respdata;
								if(strUrl == successURL){
									pageJS.cfn.pageOut();
									appJS.vrs.detectHashChange = false;
									window.location.hash = successURL;
									pageJS.vrs.currHash = window.location.hash;
									pageJS.ltr.successHandler(respdata, status, jqXHR);
									appJS.vrs.detectHashChange = true;
								}								
								else if (pageJS.vrs.currHash != successURL) {
									pageJS.cfn.pageOut();
									appJS.vrs.detectHashChange = false;
									window.location.hash = successURL;
									appJS.vrs.detectHashChange = true;
									pageJS.cfn.pageLoad(successURL);
								}
							}else{
								if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > error", "listener");
								var fromModules = strUrl.substring(0, strUrl.search('/'));
								Materialize.toast(_langtxt[fromModules]["error"][respdata.error] , 4000);
								//bind listener
								pageJS.cfn.bindListener();
								formJS.cfn.bindListener();
							}
						},
						error: function(jqXHR, status, err) {
							if (debugMsgJS.vrs.allowConsole) console.log(jqXHR,status,err);
							if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.submitHandler > error", "listener");
							$('.toast').remove();
							Materialize.toast(_langtxt["errorToLoadThePage"], 4000);
							//bind listener
							pageJS.cfn.bindListener();
							formJS.cfn.bindListener();
							
							// cardobj.find('.card-content').empty().html(_langtxt["dataNotFound"]);
						}
					});
//					$(pageJS.vrs.loadingElm).attr('style','display: none;');
				}
			}
			e.preventDefault();
		},
		cancelHandler: function(e){
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.cancelHandler", "listener");
			var successURL =  $(this).attr('rel');
			
			pageJS.cfn.pageOut();
			appJS.vrs.detectHashChange = false;
			window.location.hash = successURL;
			appJS.vrs.detectHashChange = true;
			pageJS.cfn.pageLoad(successURL);
			
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.cancelHandler > success", "listener");
		},
		stdSelectedHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.stdSelectedHandler", "listener");
			var std_id = $(this).attr('id');
			var std_name = $(this).text();
			var resObj = $(formJS.vrs.sldObj).attr('rel');
			if ($('option.' + std_id).length == 0) {
				$('#' + resObj).append('<option value="' + std_id + '" class="' + std_id + '" selected>' + std_name + '</option>');
			}
			$('#' + resObj).material_select();
			$(formJS.vrs.sldObj).find('input[type="text"]').eq(0).val('');
			$(formJS.vrs.sldObj).find('.card').slideUp("fast", function() {
				$(this).remove();
			});
			e.preventDefault();
		},
		autoToggleDisplayContent: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoToggleDisplayContent", "listener");
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($("#" + strREL).length > 0) {
					var targetVal = $(this).val();
					formJS.vrs.targetToggleObj =  "#" + strREL;
				}
				if($(this).is(':checked')) {
					$(formJS.vrs.targetToggleObj).show();
				}
				else {
					$(formJS.vrs.targetToggleObj).hide();
				}
			}
		},
		autoAjaxContent: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxContent", "listener");
			var strREL = $(this).attr('rel');
			var strTAR = $(this).attr('id');
			if (typeof strREL != "undefined" && typeof strTAR != "undefined") {
				if ($("#" + strREL).length > 0) {
					formJS.vrs.sldObj =  "#" + strREL;
					var cardobj = "";
					if ($(formJS.vrs.sldObj).find('.card').length > 0) {
						$(formJS.vrs.sldObj).find('.card').remove();
					}
					$(formJS.vrs.sldObj).append('<div class="card hidden"><div class="card-content"><i class="fa fa-refresh fa-spin fa-fw"></i> ' + _langtxt["loading"] + '</div></div>');
					cardobj = $(formJS.vrs.sldObj).find('.card').eq(0);
					cardobj.hide().removeClass('hidden').delay(100).slideDown(800);
					try {
						var ato_xhr = formJS.vrs.ato_xhr;
						if (ato_xhr != "") {
							ato_xhr.abort();
						}
						if ($(this).val() == "" ) {
							$(formJS.vrs.sldObj).find('.card').remove();
						}
						else {
							/*************************/
							var strUrl = pageJS.vrs.url;
							var mydata = pageJS.vrs.data;
							mydata.srhtxt = $(this).val();
							if($('input#needphoto')) {
								mydata.needphoto = $('input#needphoto').val();
							}
							/*************************/
							var resObj = $(formJS.vrs.sldObj).attr('rel');
							$('#' + resObj).find('option:not(:selected)').remove();
							$('#' + resObj).material_select();
							if (formJS.vrs.autoCompleteTimer) {
        						window.clearTimeout(formJS.vrs.autoCompleteTimer);
        					}
						    formJS.vrs.autoCompleteTimer = window.setTimeout(function(){
						        strUrl += "?page=ajdata/common/" + strTAR;
								formJS.vrs.ato_xhr = $.ajax({
									url: strUrl,
									data: mydata,
									timeout: 3000,
									cache: false,
									dataType: "json",
									type: "post",
									success: function(respdata, status, jqXHR) {
										if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxContent > success", "listener");
										var strHTML = "";
										var hasRec = false;
										if (respdata.resultData.length > 0) {
											hasRec = true;
											$.each(respdata.resultData, function(index, data) {
												if(data.photo_class == undefined) {
													data.photo_class = '';
												}
												strHTML += "<a class='btn btn-xs blue lighten-1 waves-effect waves-orange studentlist " + data.photo_class + "' id='" + data.label + "'>" + data.name + "</a> ";
											});
										}
										cardobj.find('.card-content').empty().html(strHTML);
										if (hasRec) {
											cardobj.find('.studentlist').off('click', formJS.ltr.stdSelectedHandler);
											cardobj.find('.studentlist').on('click', formJS.ltr.stdSelectedHandler);
										}
									},
									error: function(jqXHR, status, err) {
										if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxContent > error", "listener");
										// cardobj.find('.card-content').empty().html(_langtxt["dataNotFound"]);
									}
								});
						    }, 1000);
							/*************************/
						}
					} catch(e) {
						
					}
				}
			}
		},
		autoAjaxListContent: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxListContent", "listener");
			var strREL = $(this).attr('rel');				// e.g. dropdown_opt3[n1]
			var strTAR = $(this).attr('id');				// e.g. event_level3_select[n1]
			var urlTarget = strTAR.replace(/\[/g,'');
			urlTarget = urlTarget.replace(/\]/g,'');
			urlTarget = urlTarget.replace(/n\d/g,'');
			strREL = strREL.replace(/\[/g,'\\[');
			strREL = strREL.replace(/\]/g,'\\]');
			strTAR = strTAR.replace(/\[/g,'\\[');
			strTAR = strTAR.replace(/\]/g,'\\]');
			
			if (typeof strREL != "undefined" && typeof strTAR != "undefined") {
				if ($("#" + strREL).length > 0) {			
					formJS.vrs.sldObj =  "#" + strREL;
					var resObj = $(formJS.vrs.sldObj).attr('rel');
					resObj = resObj.replace(/\[/g,'\\[');
					resObj = resObj.replace(/\]/g,'\\]');
					
					$('#' + resObj).find("option:gt(0)").remove();	// remove all options in the list except the first one
					$('#' + resObj).material_select();

					try {
						if ($(this).val() == "" ) {
							$(formJS.vrs.sldObj).find('.card').remove();
						}
						else {
							/*************************/
							var strUrl = pageJS.vrs.url;
							var mydata = pageJS.vrs.data;
							mydata.selectedVal = $(this).val();		// selected option value
							/*************************/

							if (formJS.vrs.autoCompleteTimer) {
        						window.clearTimeout(formJS.vrs.autoCompleteTimer);
        					}
						    formJS.vrs.autoCompleteTimer = window.setTimeout(function(){
						    	if (urlTarget == "event_level3_select") {
						        	strUrl += "?page=medical/student_log/management/" + urlTarget;
						        }
						        else {
						        	strUrl += "?page=" + urlTarget;		// please modify this for other modules
						        }
						        
								formJS.vrs.ato_xhr = $.ajax({
									url: strUrl,
									data: mydata,
									timeout: 3000,
									cache: false,
									dataType: "json",
									type: "post",
									success: function(respdata, status, jqXHR) {
										if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxListContent > success", "listener");
										if (respdata.resultData.length > 0) {
											$.each(respdata.resultData, function(index, data) {
												if ($('option.' + data.label).length == 0) {
													$('#' + resObj).append('<option value="' + data.label + '" class="' + data.label + '">' +  data.name + '</option>');
												}
											});
											$('#' + resObj).material_select();
										}
										else {
											//console.log('no result data');										
										}
									},
									error: function(jqXHR, status, err) {
										if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxListContent > error", "listener");
										// cardobj.find('.card-content').empty().html(_langtxt["dataNotFound"]);
									}
								});
						    }, 1000);
							/*************************/
						}
					} catch(e) {
						//console.log('try error');
					}
				}
				else {
					console.log('strREL length < 0');
				}
			}
			else {
				console.log('undefined');
			}
			
		},
		autoAjaxSelectContent: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxSelectContent", "listener");
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($("select#" + strREL).length > 0) {
					// Remove Target Selection Options
					formJS.vrs.targetObj =  "select#" + strREL;
					if ($(formJS.vrs.targetObj).find('option').length > 0) {
						$(formJS.vrs.targetObj).find('option').filter(function() {
					        return this.value > 0 || $.trim(this.value).length > 0;
					    }).remove();
					    $(formJS.vrs.targetObj).val("");
						$(formJS.vrs.targetObj).material_select();
					}
					
					// Remove Next Level Target Selection Options
					var strNextREL = $(formJS.vrs.targetObj).attr('rel');
					if(typeof strNextREL != "undefined" && $("select#" + strNextREL).length > 0) {
						formJS.vrs.targetNextObj =  "select#" + strNextREL;
						if ($(formJS.vrs.targetNextObj).find('option').length > 0) {
							$(formJS.vrs.targetNextObj).find('option').filter(function() {
					        	return this.value > 0 || $.trim(this.value).length > 0;
						    }).remove();
						};
					    $(formJS.vrs.targetNextObj).val("");
						$(formJS.vrs.targetNextObj).material_select();
					}
					
					try {
						var ato_xhr = formJS.vrs.ato_xhr;
						if (ato_xhr != "") {
							ato_xhr.abort();
						}
						if ($(this).val() == "") {
							// $(formJS.vrs.sldObj).find('.card').remove();
						}
						else {
							var strUrl = pageJS.vrs.url;
							var mydata = pageJS.vrs.data;
							mydata.val = $(this).val();
							var strMethod = $(this).attr('id');
							var strModule = $(this).attr('target-module');
							var product = '';
							
							if (strModule == 'medical_student_log') {
								product = 'medical';
								strModule = 'student_log';
							}
							else {
								product = 'ediscipline';
							}
							strModule = strModule != "" && strModule != undefined ? (strModule) : ("goodconduct_misconduct");
							strUrl += "?page=" + product + "/" + strModule + "/management/" + strMethod;

							formJS.vrs.ato_xhr = $.ajax({
								url: strUrl,
								data: mydata,
								timeout: 3000,
								cache: false,
								dataType: "json",
								type: "post",
								success: function(respdata, status, jqXHR) {
									if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxSelectContent > success", "listener");
								
									// Rebuild Target Selection Options
									if (respdata && respdata.resultData.length > 0) {
										$.each(respdata.resultData, function(index, data) {
											$(formJS.vrs.targetObj).append($("<option value=\""+data.label+"\">"+data.name+"</option>"));
										});
										//$(formJS.vrs.targetObj+' option:first-child').attr("selected", "selected");
										$(formJS.vrs.targetObj).material_select();
										
										if (strMethod == 'event_level1_select') {	// in medical student log
											$('#event_level2_select').change();
										}
									}
								},
								error: function(jqXHR, status, err) {
									if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.autoAjaxContent > error", "listener");
								}
							});
						}
					}
					catch(e) {
						// 
					}
				}
			}
		}
	},
	cfn: {
		bindListener: function() {
			formJS.cfn.destroyListener();
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.cfn.bindListener", "listener");
			
			$(formJS.vrs.submitBtnObj).on("click", formJS.ltr.submitHandler);
			$(formJS.vrs.cancelBtnObj).on("click", formJS.ltr.cancelHandler);
			
			if ($('textarea.materialize-textarea').length > 0) {
				if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> textarea.materialize-textarea", "info");
				$('textarea.materialize-textarea').trigger('autoresize');
			}
			if ($('input.autocomplete').length > 0) {
				if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> input.autocomplete", "info");
				$('input.autocomplete').on('keyup', formJS.ltr.autoAjaxContent);
			}
			if ($('select.select_ajaxload').length > 0) {
				if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> select.select_reload", "info");
				$('select.select_ajaxload').on('change', formJS.ltr.autoAjaxSelectContent);
			}
			if ($('select.select_ajaxloadlist').length > 0) {
				if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> select.select_reloadlist", "info");
				$('select.select_ajaxloadlist').on('change', formJS.ltr.autoAjaxListContent);
			}
			if ($('input.toggle_display_checkbox:checkbox').length > 0) {
				if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init >> input.toggle_display_checkbox:checkbox", "info");
				$('input.toggle_display_checkbox:checkbox').on('click', formJS.ltr.autoToggleDisplayContent);
			}
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false, // Does not change width of dropdown to that of the activator
				hover: true, // Activate on hover
				gutter: 0, // Spacing from edge
				belowOrigin: false, // Displays dropdown below the button
				alignment: 'left', // Displays dropdown with edge aligned to the left of button
				stopPropagation: false // Stops event propagation
			});
		},
		destroyListener: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.cfn.destroyListener", "warning");
			$(formJS.vrs.submitBtnObj).off("click", formJS.ltr.submitHandler);
			if ($('input.autocomplete').length > 0) {
				$('input.autocomplete').off('keyup', formJS.ltr.autoAjaxContent);
			}
			if ($('select.select_ajaxload').length > 0) {
				$('select.select_ajaxload').off('change', formJS.ltr.autoAjaxSelectContent);
			}
			if ($('input.toggle_display_checkbox:checkbox').length > 0) {
				$('input.toggle_display_checkbox:checkbox').off('click', formJS.ltr.autoToggleDisplayContent);
			}
		},
		stepSelectOption: function() {
			
		},
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("formJS.ltr.init", "info");
		}
	}
};

/************************************************/
/* pageJS controller
/************************************************/
var pageJS = {
	vrs: {
		wrapHdr: "#pageHldrWrap",
		container: ".pageHldr",
		ajaxlink: ".ajlink",
		boxlink: ".boxlink",
		modallink: ".modallink",
		loadingElm : ".loadingEm",
		actionBtn : ".actionBtn",
		filterBtn : ".filterBTN",
		currHash: "",
		xhr: "",
		data: { token: initToken, id: initID },
		url: initSvrLink + "api.php",
		lang: {
			pageNotFound: _langtxt["pageNotFound"]
		},
		pageReady: true
	},
	ltr: {
		modallinkHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.modallinkHandler", "listener");
			var strREL = $(this).attr('rel');
			$('#myModal').modal('open');
			e.preventDefault();
		},
		boxlinkHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.boxlinkHandler", "listener");
			var strREL = $(this).attr('rel');
			if ($("#" + strREL).hasClass('hidden')) {
				$("#" + strREL).hide().removeClass('hidden').slideDown("slow");
			} else {
				$("#" + strREL).slideUp("slow", function() {
					$(this).addClass('hidden');
				});
			}
			e.preventDefault();
		},
		filterBoxHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.filterBoxHandler > .filterBTN >>> " + appJS.vrs.clickBHR, "info");
			if ($("#filter").hasClass('hidden')) {
				$("#filter").hide().removeClass('hidden').slideDown("slow");
			} else {
				$("#filter").slideUp("slow", function() {
					$(this).addClass('hidden');
				});
			}
			//var dTable = $('.dTtable');
			//if (dTable != "") {
			//	dTable.draw();
			//}
			e.preventDefault();
		},
		linkHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.linkHandler", "listener");
			var strHash = "";
			if (typeof $(this).attr('rel') != "undefined" && pageJS.vrs.pageReady) {
				strHash = $(this).attr('rel');
				strHash = strHash.replace(/\#/g, '');
				if (pageJS.vrs.currHash != strHash) {
					pageJS.cfn.pageOut();
					// pageJS.cfn.pageLoad(strHash);
					appJS.vrs.detectHashChange = false;
					window.location.hash = strHash;
					appJS.vrs.detectHashChange = true;
				}
			}
			e.preventDefault();
		},
		actionBtnHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.actionBtnHandler", "listener");
			var data = $(this).data();
			data.token = initToken;
			data.id = initID 
			var url = pageJS.vrs.url
			var strUrl = pageJS.vrs.url;
			strUrl = $(this).attr('rel');
			strUrl = strUrl.replace(/\#/g, '');
			url += "?page=" + strUrl;
			var fromModules = strUrl.substring(0, strUrl.search('/'));
			switch (data.action){
				case 'delete':
					$('#myModal').modal({
						ready: function() {
							$('#myModal').find('.modal-action').off('click');
							$('#myModal').find('.modal-action').on('click',function(){
								if($(this).attr('value')=='yes'){
									$('#myModal').find('.modal-action').off('click');
									pageJS.cfn.modalConfirmCallBack(data, strUrl);
									$('#myModal').modal('close');
								}else{
									$('#myModal').modal('close');
								}
							});
						}
					});
					$('#myModal').modal('open');
					break;
				case 'filter':
					var tableObj = (fromModules+'table');
					window[tableObj].ajax.reload();
					break;
				case 'goEdit':
					break
				default:
					pageJS.cfn.modalConfirmCallBack(data, strUrl);
			}
			e.preventDefault();
		},
		successHandler: function(respdata, status, jqXHR) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.successHandler", "success");
			var respType = "HTML";
			if (typeof respdata.respType != "undefined") {
				switch (respdata.respType) {
					case "DATA":
						respType = respdata.respType;
						break;
				}
			}
			if (respType == "HTML") {
				appJS.cfn.animateCss($(pageJS.vrs.loadingElm), appJS.vrs.loadingOutEffect, 'hide');
				if (typeof respdata.hash != "undefined") {
					switch (respdata.hash) {
						default:
							if (typeof respdata.pageType != "undefined" && respdata.pageType == "list") {
								if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle(JSON.stringify(respdata.pData), "data");
								$.cookie(respdata.hash, JSON.stringify(respdata.pData));
							}
							$(pageJS.vrs.container).show().html(respdata.content);
							var loadEff = appJS.vrs.loadingInEffect;
							if (typeof respdata.loadingInEffect != "undefined") {
								loadEff = respdata.loadingInEffect;
							}
							appJS.cfn.animateCss($(pageJS.vrs.container), loadEff);
							break;
					}
				} else {
					var divClass = pageJS.vrs.container;
					$(pageJS.vrs.wrapHdr).empty().html("<div class='" + divClass.replace(".", "") + "'></div>");
					$(pageJS.vrs.container).show().html(pageJS.vrs.lang.pageNotFound);
					appJS.cfn.animateCss($(pageJS.vrs.container), appJS.vrs.loadingInEffect);
				}
			}
			appJS.cfn.bindListener();
		},
		failHandler: function(jqXHR, status, err) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.failHandler", "error");
			appJS.cfn.animateCss($(pageJS.vrs.loadingElm), appJS.vrs.loadingOutEffect, 'hide');
			var divClass = pageJS.vrs.container;
			$(pageJS.vrs.wrapHdr).empty().html("<div class='" + divClass.replace(".", "") + "'></div>");
			$(pageJS.vrs.container).show().html(pageJS.vrs.lang.pageNotFound);
			appJS.cfn.animateCss($(pageJS.vrs.container), appJS.vrs.loadingInEffect);
		}
	},
	cfn: {
		
		modalConfirmCallBack: function(data, strUrl) {
			var url = pageJS.vrs.url
			url += "?page=" + strUrl;
			$.ajax({
				url: url,
				data: data,
				type: "post",
				success: function(respdata){
					respdata = JSON.parse(respdata);
					var fromModules = strUrl.substring(0, strUrl.search('/'));
					var tableObj = (fromModules+'table');					
					Materialize.toast(_langtxt[fromModules][respdata.error] , 2000);
					window[tableObj].ajax.reload();
//					$('#myModal').modal('close');
				}
			});
		},
		bindListener: function() {
			pageJS.cfn.destroyListener();
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.cfn.bindListener", "listener");
			$(pageJS.vrs.ajaxlink).on('click', pageJS.ltr.linkHandler);
			$(pageJS.vrs.boxlink).on('click', pageJS.ltr.boxlinkHandler);
			$(pageJS.vrs.modallink).on('click', pageJS.ltr.modallinkHandler);
			$(pageJS.vrs.actionBtn).on('click', pageJS.ltr.actionBtnHandler);
			$(pageJS.vrs.filterBtn).on('click', pageJS.ltr.filterBoxHandler);
		},
		destroyListener: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.cfn.destroyListener", "warning");
			$(pageJS.vrs.ajaxlink).off('click', pageJS.ltr.linkHandler);
			$(pageJS.vrs.boxlink).off('click', pageJS.ltr.boxLinkHandler);
			$(pageJS.vrs.modallink).off('click', pageJS.ltr.modallinkHandler);
			$(pageJS.vrs.actionBtn).off('click', pageJS.ltr.actionBtnHandler);
			$(pageJS.vrs.filterBtn).off('click', pageJS.ltr.filterBoxHandler);
		},
		pageLoad: function(strHash, submitData, success, fail) {
			if (pageJS.vrs.pageReady) {
				strHash = strHash.replace(/\#/g, '');
				if (strHash == "medical/bowel/management/mylist") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php");
				}
				else if (strHash == "medical/bowel/management/mylist&Success=1") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php?Success=1");
				}
				else if (strHash == "medical/sleep/management/mylist") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php");
				}
				else if (strHash == "medical/sleep/management/mylist&Success=1") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php?Success=1");
				}
				else if (strHash == "medical/student_log/management/mylist") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php");
				}
				else if (strHash == "medical/student_log/management/mylist&Success=1") {
					window.location.replace("../../eclassApp/medicalCaring/medical_item_list.php?Success=1");
				}
				else {			
				
					if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.cfn.pageLoad > " + strHash, "function");
					var pageType = "list";
					if (strHash != "") {
						var hashArr = strHash.split("/");
						pageType = hashArr[hashArr.length - 1];
					}
					pageJS.vrs.pageReady = false;
					pageJS.cfn.destroyListener();
					var xhr = pageJS.vrs.xhr;
					if (xhr != "") {
						xhr.abort();
					}
					var strUrl = pageJS.vrs.url;
					if (strHash != "") {
						strUrl += "?page=" + strHash;
					}
					if (typeof success == "undefined" || success == "") {
						successHdr = pageJS.ltr.successHandler;
					}
					if (typeof fail == "undefined" || fail == "") {
						failHdr = pageJS.ltr.failHandler;
					}
					var cookie_strHash = strHash;
					if (cookie_strHash == "") cookie_strHash = "index";
					var cookiesData = $.cookie(cookie_strHash);
					var mydata = {};
					if (typeof cookiesData != "undefined" && cookiesData != "undefined" && cookiesData.length > 0) {
						mydata = JSON.parse(cookiesData);
						mydata.from = "cookie";
					} else {
						mydata = pageJS.vrs.data;
						mydata.from = "default";
					}
					if (typeof submitData != "undefined" && submitData != "") {
						mydata.loadType = "page";
						mydata.cus = submitData;
					}
				
//					$.cookie(cookie_strHash, JSON.stringify(mydata));
					if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle(JSON.stringify(mydata), "data");
					try {
						pageJS.vrs.currHash = strHash;
						pageJS.vrs.xhr = $.ajax({
							url: strUrl,
							data: mydata,
							timeout: 3000,
							cache: false,
							dataType: "json",
							type: "post",
							success: function(respdata, status, jqXHR) {
								successHdr(respdata, status, jqXHR);
								pageJS.vrs.pageReady = true;
							},
							error: function(jqXHR, status, err) {
								failHdr(jqXHR, status, err);
								pageJS.vrs.pageReady = true;
							}
						});
					} catch (e) {
						if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("Error", "error");
						appJS.cfn.bindListener();
						pageJS.vrs.pageReady = true;
					}
				}				
			}
		},
		pageOut: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.cfn.pageOut", "function");
			$('body').addClass("loading");
			
			if($(".child").length>0){
				//z-index handling
				var highest = 0;
				$(".child").each(function() {
				    var current = parseInt($(this).css("z-index"), 10);
				    if(current && highest < current) highest = current;
				});
				if(highest<9999){
					highest = 9999;
				}
			}
			$(pageJS.vrs.loadingElm).css('z-index',(highest+1));
			$(pageJS.vrs.loadingElm).stop().fadeIn(800);
		},
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.cfn.init", "info");
			var strHash = window.location.hash;
			pageJS.cfn.pageLoad(strHash);
		}
	}
}

/************************************************/
/* debugMsgJS controller
/************************************************/
var debugMsgJS = {
	vrs: {
		allowConsole: false
	},
	ltr: {},
	cfn: {
		dConosle: function(msg, type) {
			if (typeof msg == "undefined") msg = "";
			if (typeof type == "undefined") type = "info";
			var colorStyle = "background: #fff; color: #000";
			if (type.toLowerCase() == "error") {
				console.error(msg);
			} else if (type.toLowerCase() == "data") {
				//console.log('%c DATA :: ' + msg + " ", colorStyle);
			} else {
				switch (type.toLowerCase()) {
					case "listener":
						colorStyle = "background: Gold; color: #000";
						break;
					case "function":
						colorStyle = "background: DodgerBlue; color: #fff";
						break;
					case "danger":
						colorStyle = "background: Crimson; color: #000";
						break;
					case "warning":
						colorStyle = "background: HotPink; color: #000";
						break;
					case "success":
						colorStyle = "background: LightGreen; color: #000";
						break;
				}
				if (colorStyle != "") {
					if (typeof msg == "string") {
						msg = "[" + msg;
						msg = msg.replace("JS.", "].");
						msg = msg.replace(".cfn.", " Function > ");
						msg = msg.replace(".ltr.", " Listener > ");
					}
					console.log('%c > ' + msg + " ", colorStyle);
				} else {
					console.log(" > " + msg + " ");
				}
			}
		}
	}
};

/************************************************/
/* appJS controller
/************************************************/
var appJS = {
	vrs: {
		allowAnimate: true,
		loadingInEffect: "bounceInDown",
		loadingOutEffect: "flipOutX",
		detectHashChange: true
	},
	ltr: {
		disableHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("appJS.ltr.disableHandler", "info");
			e.preventDefault();
		},
		hashChangeHandle: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("appJS.ltr.hashChangeHandle", "listener");
			if (appJS.vrs.detectHashChange) {
				if(window.location.hash != pageJS.vrs.currHash){
					pageJS.cfn.pageOut();
					var strHash = window.location.hash;
					pageJS.cfn.pageLoad(strHash);
				}
			}
			e.preventDefault();
		}
	},
	cfn: {
		animateCss: function (obj, animationName, otherAction) {
			if (appJS.vrs.allowAnimate) {
				var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				obj.addClass('animated ' + animationName).one(animationEnd, function() {
					obj.removeClass('animated ' + animationName);
					if (typeof otherAction != "undefined") {
						switch (otherAction) {
							case "hide":
								obj.hide();
								$('body').removeClass("loading");
								break;
						}
					}
				});
			}
		},
		bindListener: function() {
			appJS.cfn.destroyListener();
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("appJS.cfn.bindListener", "listener");
			pageJS.cfn.bindListener();
			formJS.cfn.bindListener();
			$("a[href='#']").on('click', appJS.ltr.disableHandler);
			$("button").on('click', appJS.ltr.disableHandler);
			window.onhashchange = function(e) {
				appJS.ltr.hashChangeHandle(e);
			}
			if ($('select').length > 0) {
				$('select').material_select();
			}
			/********************** Custom by fl for materialize css ***************************/
			if ($("input.select-dropdown").length > 0) {
				$("input.select-dropdown").each(function() {
//					if ($(this).parent().find('.multiple-select-dropdown').length > 0) {
						$(this).on("focus", function(e) {
							$(this).blur();					
						});
//					}
				});
			}
			/********************** Custom by fl for materialize css ***************************/
			/************* By omas for testing timepicker **************/
			if ($("input.timepicker").length > 0) {
				$("input.timepicker").each(function() {
					$(this).on("focus", function(e) {
						$(this).blur();					
					});
				});
				$('input.timepicker').clockpicker({placement:'top',vibrate:false, autoclose:true,   minutestep: 5 });
			}
			/************* By omas for testing timepicker **************/
			if($('.collapsible').length > 0){
				$('.collapsible').collapsible();
			}
			if ($('.datepicker').length > 0) {
				$('.datepicker').pickadate({
					monthsFull: _langtxt["monthsFull"],
					weekdaysShort: _langtxt["weekdaysShort"],
					today: _langtxt["today"],
					clear: '',
					close: _langtxt["close"],
					format: 'yyyy/mm/dd',
					FormatSubmit: 'yyyy/mm/dd'
				});
			}
			
			$('.modal').modal();
		},
		destroyListener: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("appJS.cfn.destroyListener", "warning");
			pageJS.cfn.destroyListener();
			formJS.cfn.destroyListener();
			window.onhashchange = null;
			$("a[href='#']").off('click', appJS.ltr.disableHandler);
		},
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("appJS.cfn.init", "info");
			pageJS.cfn.init();
			formJS.cfn.init();
			appJS.cfn.bindListener();
		}
	}
};