<?php
/*
 * 	Modify by:
 * 
 * 	2018-01-05 Cameron
 * 		- create this file
 */
ob_start();
//debug_pr($this->infoArr);

global $Lang;
global $i_general_class, $i_general_name;
global $i_general_choose_student, $i_general_selected_students;
?>

<div class="fiuld-container cusView">
<!--
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$Lang['medical']['menu']['bowel']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
-->	
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<!--<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$Lang['medical']['menu']['bowel']?></a></li>-->
				<li class="active"><?=$Lang['medical']['menu']['bowel']?></li>
				<li class="active"><?=$Lang['medical']['general']['edit']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
				
					<form class="col s12" id='edit_form' rel="<?=$viewData["controllerLink"]?>/edit_update" data-success="<?=$viewData["controllerLink"]?>/view">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<div class="input-field col col-xs-12 col-sm-6">
								<div class="select-wrapper">
									<input id="date" name="date" class="datepicker" value="<?=$this->infoArr["UIContent"]["BowelRecordDate"]?>">
								</div>
								<label for="date"><?=$Lang['General']['Date']?></label>
							</div>
							
							<div class="input-field col col-xs-10 col-sm-5">
								 <select class="select_ajaxload required" id="bowel_status" name="bowel_status" onChange="changeBowelStatus()">
									<?=$this->infoArr["UIContent"]["BowelStatusSelect"]?>
								 </select>
								<label><?=$Lang['medical']['bowel']['tableHeader']['BowelCondition']?></label>
							</div>
							
							<div class="col col-xs-2 col-sm-1">
								<div class="select-wrapper">
								<label id="bowel_color" style="display: inline-block;
									margin-top:35px;
								    border: 1px solid #c0c0c0;
								    width: 17px;
								    height: 17px;
								    background-color:<?=$this->infoArr["UIContent"]["BowelStatusSelectedColor"]?>"></label>
    							</div>
							</div>

							<div class="input-field col col-xs-5 col-sm-2">
								<?=$this->infoArr["UIContent"]["BowelHourSelect"]?>
								<label><?=$Lang['medical']['bowel']['tableHeader']['BowelTime']?></label>
							</div>
							<div class="input-field col-xs-2 col-sm-2" style="margin-top:35px; text-align:center;">
								<span>:</span>
							</div>
							<div class="input-field col col-xs-5 col-sm-2">
								<?=$this->infoArr["UIContent"]["BowelMinuteSelect"]?>
							</div>
							
							<div class="input-field col col-xs-12 col-sm-6">
								<?=$this->infoArr["UIContent"]["BowelDefaultRemarks"]?>
								<div class="remark_top"><label class="remark_title"><a href="#" onClick="toggleDefaultRemarks()"><img src="/images/icon/preset_option.gif" id="posimg" border="0" alt="<?=$Lang['Btn']['Select']?>">&nbsp;<?=$Lang['medical']['App']['CopyDefaultRemarks']?></a></label></div>
							</div>

							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="remarks" name="remarks" class="materialize-textarea" data-length="120"><?=$this->infoArr["UIContent"]["BowelRemarks"]?></textarea>
								<label id="remarks_label" for="remarks" class="remark_title active"><?=$Lang['medical']['bowel']['tableHeader']['Remarks']?></label>
							</div>

							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a rel="<?=$viewData["controllerLink"]?>/view&RecordID=<?=$this->infoArr["UIContent"]["RecordID"]?>&RecordTime=<?=$this->infoArr["UIContent"]["RecordTime"]?>&UserType=<?=$_SESSION['UserType']?>" class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn'><?=$Lang['Btn']['Cancel']?></a>
									<a class='btn btn-sm waves-effect waves-orange editBtn' type="submit" rel="edit_form" name="action"><?=$Lang['medical']['general']['update']?></a> 
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.default_remarks{
	display: none;
}
</style>
<script>

function changeBowelStatus(){
	$("#bowel_color").css('background-color',$("#bowel_status option:selected").attr('data-color'));
}

function toggleDefaultRemarks(){
	$('#default_remarks').toggleClass('default_remarks');
}

function SetRemarks(obj){
	var new_val = $(obj).text();
	var org_val = $('#remarks').val();
	if (org_val.indexOf(new_val) == -1) {
		org_val = org_val + ((org_val != '') ? '\n' : '') + new_val;
		$('#remarks').val(org_val);
		$('#remarks').focus(); 
		$('#remarks_label').css('margin-top','-10px');
	} 
	toggleDefaultRemarks();
}

$(document).ready(function(){
	$('.defaultRemarkSpan > a').on('click',function(){
		$('iframe#lyrShim').hide();
	});
	
	
	// customize success handler based on standard submit listener
	$('.editBtn').off('click'); 
	$('.editBtn').on('click',function(e){

		var formObj = $(this).attr('rel');
		if ($("#" + formObj).length > 0) {
			var isAllowSubmit = true;
			if ($("#" + formObj).find('input.required').length > 0) {
				$("#" + formObj).find('input.required').each(function() {
					if ($(this).val() == "" || typeof $(this).val() == "undefined") {
						isAllowSubmit = false;
					}
				});
			}
			
			if ($("#" + formObj).find('select.required').length > 0) {
				$("#" + formObj).find('select.required').each(function() {
					if ($(this).find('option:selected').length == 0) {
						isAllowSubmit = false;
					}
				});
			}
				
			if (!isAllowSubmit) {
				//bind listener
				pageJS.cfn.bindListener();
				formJS.cfn.bindListener();
				//Materialize.toast('Error', 4000);
			} else {

				var successURL = $("#" + formObj).attr('data-success');
				var serialized = $("#" + formObj).serializeArray();
		        var formData = JSON.stringify(serialized);
				var strUrl = $("#" + formObj).attr('rel');
				var xhr = formJS.vrs.xhr;
				if (xhr != "") {
					xhr.abort();
				}
				//global
				formJS.vrs.glob_data[strUrl] = formData;
				
				var form_submitdata = pageJS.vrs.data;
				form_submitdata.formData = formData
				form_submitdata.srhtxt = "formSubmit";
				form_submitdata.stepData = formJS.vrs.glob_data;
				
				Materialize.toast(_langtxt["loading"], 4000);
				formJS.vrs.xhr = $.ajax({
					url: pageJS.vrs.url + "?page=" + strUrl,
					data: form_submitdata,
					timeout: 25000,
					cache: false,
					dataType: "json",
					type: "POST",
					success: function(respdata, status, jqXHR) {
						
						$('.toast').remove();
						if (typeof respdata.status !="undefined" && respdata.status == "success") {

							if(strUrl == successURL){
								pageJS.cfn.pageOut();
								appJS.vrs.detectHashChange = false;

								successURL += "&RecordID=" + respdata.RecordID + "&RecordTime=" + respdata.RecordTime; 								
								window.location.hash = successURL;
									
								pageJS.vrs.currHash = window.location.hash;
								pageJS.ltr.successHandler(respdata, status, jqXHR);
								appJS.vrs.detectHashChange = true;
							}								
							else if (pageJS.vrs.currHash != successURL) {
								pageJS.cfn.pageOut();
								appJS.vrs.detectHashChange = false;
								successURL += "&RecordID=" + respdata.RecordID + "&RecordTime=" + respdata.RecordTime;
								window.location.hash = successURL;
								appJS.vrs.detectHashChange = true;
								pageJS.cfn.pageLoad(successURL);
							}
						}else{
							var fromModules = strUrl.substring(0, strUrl.search('/'));
							Materialize.toast(_langtxt[fromModules]["error"][respdata.error] , 4000);
							//bind listener
							pageJS.cfn.bindListener();
							formJS.cfn.bindListener();
						}
					},
					error: function(jqXHR, status, err) {
						$('.toast').remove();
						Materialize.toast(_langtxt["errorToLoadThePage"], 4000);
						//bind listener
						pageJS.cfn.bindListener();
						formJS.cfn.bindListener();
					}
				});
			}
		}
		e.preventDefault();
		
	});		// editBtn click
		
});

</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>