<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang;
global $i_general_class, $i_general_name;
global $i_general_choose_student, $i_general_selected_students;
?>

<div class="fiuld-container cusView">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<!--<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$Lang['medical']['menu']['bowel']?></a></li>-->
				<li class="active"><?=$Lang['medical']['menu']['bowel']?></li>
				<li class="active"><?=$Lang['medical']['general']['new']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>2: <?=$Lang['medical']['general']['new']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form' rel="<?=$viewData["controllerLink"]?>/edit_step3" data-success="<?=$viewData["controllerLink"]?>/mylist&Success=1">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<div class="input-field col col-xs-12 col-sm-6">
								<div class="select-wrapper">
									<input id="date" name="date" class="datepicker" value="<?=date("Y-m-d")?>">
								</div>
								<label for="date"><?=$Lang['General']['Date']?></label>
							</div>
							
							<div class="input-field col col-xs-10 col-sm-5">
								 <select class="select_ajaxload required" id="bowel_status" name="bowel_status" onChange="changeBowelStatus()">
									<?=$this->infoArr["UIContent"]["BowelStatusSelect"]?>
								 </select>
								<label><?=$Lang['medical']['bowel']['tableHeader']['BowelCondition']?></label>
							</div>
							
							<div class="col col-xs-2 col-sm-1">
								<div class="select-wrapper">
								<label id="bowel_color" style="display: inline-block;
									margin-top:35px;
								    border: 1px solid #c0c0c0;
								    width: 17px;
								    height: 17px;
								    background-color:<?=$this->infoArr["UIContent"]["BowelStatusSelectedColor"]?>"></label>
    							</div>
							</div>

							<div class="input-field col col-xs-5 col-sm-2">
								<?=$this->infoArr["UIContent"]["BowelHourSelect"]?>
								<label><?=$Lang['medical']['bowel']['tableHeader']['BowelTime']?></label>
							</div>
							<div class="input-field col-xs-2 col-sm-2" style="margin-top:35px; text-align:center;">
								<span>:</span>
							</div>
							<div class="input-field col col-xs-5 col-sm-2">
								<?=$this->infoArr["UIContent"]["BowelMinuteSelect"]?>
							</div>
							
							<div class="input-field col col-xs-12 col-sm-6">
								<?=$this->infoArr["UIContent"]["BowelDefaultRemarks"]?>
								<div class="remark_top"><label class="remark_title"><a href="#" onClick="toggleDefaultRemarks()"><img src="/images/icon/preset_option.gif" id="posimg" border="0" alt="<?=$Lang['Btn']['Select']?>">&nbsp;<?=$Lang['medical']['App']['CopyDefaultRemarks']?></a></label></div>
							</div>

							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="remarks" name="remarks" class="materialize-textarea" data-length="120"></textarea>
								<label id="remarks_label" for="remarks" class="remark_title"><?=$Lang['medical']['bowel']['tableHeader']['Remarks']?></label>
							</div>

							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?=$viewData["controllerLink"]?>/edit"><?=$Lang['Btn']['Back']?></a>
									<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step2_form" name="action"><?=$Lang['medical']['general']['new']?></a> 
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.default_remarks{
	display: none;
}
</style>
<script>

function changeBowelStatus(){
	$("#bowel_color").css('background-color',$("#bowel_status option:selected").attr('data-color'));
}

function toggleDefaultRemarks(){
	$('#default_remarks').toggleClass('default_remarks');
}

function SetRemarks(obj){
	var new_val = $(obj).text();
	var org_val = $('#remarks').val();
	if (org_val.indexOf(new_val) == -1) {
		org_val = org_val + ((org_val != '') ? '\n' : '') + new_val;
		$('#remarks').val(org_val);
		$('#remarks').focus(); 
		$('#remarks_label').css('margin-top','-10px');
	} 
	toggleDefaultRemarks();
}

$(document).ready(function(){
	$('.defaultRemarkSpan > a').on('click',function(){
		$('iframe#lyrShim').hide();
	});
});

</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>