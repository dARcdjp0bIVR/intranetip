<?php
/*
 * Using: 
 *
 *  2018-11-08 Cameron
 *      - enable hyperlink of navigation item so that it can return to medical main page when edit
 * 
 * 2018-01-05 Cameron
 * - create this file
 *
 */
ob_start();
// debug_pr($this->infoArr);

global $Lang, $image_path, $LAYOUT_SKIN;
?>

<div class="fiuld-container cusView">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$Lang['medical']['menu']['bowel']?></a></li>
				<!-- <li class="active"><?=$Lang['medical']['menu']['bowel']?></li> -->
				<li class="active"><?=$Lang['Btn']['View']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form'
						rel="<?=$viewData["controllerLink"]?>/edit_record"
						data-success="<?=$viewData["controllerLink"]?>/edit_record">
						<div class="row">

							<div class="col col-xs-12 col-sm-6">
								<div>
									<label id="bowel_color" style="display: inline-block;
										margin-bottom:-3px;
									    border: 1px solid #c0c0c0;
									    width: 17px;
									    height: 17px;
									    background-color:<?=$this->infoArr["UIContent"]["BowelStatusColor"]?>">
									</label> <label><?=$this->infoArr["UIContent"]["BowelStatusName"]?></label>
								</div>
							</div>

						<? if ($this->infoArr["UIContent"]["UserType"] == '1'):?>
							<div class="col col-xs-12 col-sm-6 div_view">
								<div>
									<?=$this->infoArr["UIContent"]["SummaryTable"]?>
								</div>
							</div>
						<? endif;?>

							<div class="col col-xs-12 col-sm-6 div_view">
								<div>
									<img
										src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png"
										align="top"
										style="margin-left: -2px; margin-bottom: 5px; width: 20px; height: 20px;" />
									<?=$this->infoArr["UIContent"]["BowelRecordTime"]?>
								</div>
							</div>

						<? if (!empty($this->infoArr["UIContent"]["BowelRemarks"])):?>
							<div class="col col-xs-12 col-sm-6 div_view">
								<label class="remark_title"><?=$Lang['medical']['bowel']['tableHeader']['Remarks']?></label><br>
								<?=nl2br($this->infoArr["UIContent"]["BowelRemarks"])?>
							</div>
						<? endif;?>

							<div class="col col-xs-12 col-sm-6 div_view">
								<label class="remark_title"><?=$Lang['medical']['meal']['tableHeader']['AttendanceStatus']?></label><br>
								<?=$this->infoArr["UIContent"]["AttendanceStatus"]?>
							</div>

							<div class="col col-xs-12 col-sm-6 div_view">
								<label class="remark_title"><?=$Lang['medical']['meal']['tableHeader']['LastPersonConfirmed']?></label><br>
								<?=$this->infoArr["UIContent"]["LastUpdatedBy"]?>
							</div>

							<div class="col col-xs-12 col-sm-6 div_view">
								<label class="remark_title"><?=$Lang['medical']['meal']['tableHeader']['LastUpdated']?></label><br>
								<?=$this->infoArr["UIContent"]["LastUpdatedOn"]?>
							</div>

							<div class="col-xs-12 text-right div_view">
								<div class="btn-group-justified">
									<a class="btn btn-sm waves-effect waves-orange submitBtn"
										type="submit" rel="step2_form" name="edit"> <?=$Lang['Btn']['Edit']?></a>
									<a
										class="btn btn-sm waves-effect waves-orange btn_delete deleteBtn"
										type="button" name="delete" href="#myModal"
										rel="<?=$viewData["controllerLink"]?>/delete"
										data-RecordID="<?=$this->infoArr["UIContent"]["RecordID"]?>"
										data-RecordTime="<?=$this->infoArr["UIContent"]["RecordTime"]?>"> <?=$Lang['Btn']['Delete']?></a>
								</div>
							</div>
							<div class="col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
function actionButtonHandler2 (e) {
	if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.actionButtonHandler2", "listener");
	var strHash = "";
	if (typeof $(this).attr('rel') != "undefined" && pageJS.vrs.pageReady) {
		pageJS.vrs.data.RecordID = $(this).attr('data-RecordID');
		pageJS.vrs.data.RecordTime = $(this).attr('data-RecordTime');
		strHREF = $(this).attr('href');
		if(strHREF != '#myModal') {
			strHash = $(this).attr('rel');
			strHash = strHash.replace(/\#/g, '');
			if (pageJS.vrs.currHash != strHash) {
				pageJS.cfn.pageOut();
				// pageJS.cfn.pageLoad(strHash);
				appJS.vrs.detectHashChange = false;
				window.location.hash = strHash;
				appJS.vrs.detectHashChange = true;
			}
		}
	}
	e.preventDefault();
}

$(document).ready(function(){

	if ($('.deleteBtn').attr('data-RecordID') == "") {
		Materialize.toast(_langtxt["dataNotFound"] , 4000);
		//bind listener
		pageJS.cfn.bindListener();
		formJS.cfn.bindListener();
	}
	
	// Clear Existing Data
	if(pageJS.vrs.data.RecordID) {
		pageJS.vrs.data.RecordID = null;
		pageJS.vrs.data.RecordTime = null;
	}

    // Set Click Handler for action buttons
    $('.deleteBtn').off('click');
    $('.deleteBtn').on('click', actionButtonHandler2);
    
    // Set Modal Handling
	$("div#myModal").off('click', 'a.modal-action[value="yes"]');
    $("div#myModal").on('click', 'a.modal-action[value="yes"]', function (e) {
	   	if($(this).attr("disabled") != "true") {
	   		$(this).attr("disabled", "true");
		    $.ajax({
				type: "POST",
				url: pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/delete',
				data: { id: '', 
						token: initToken, 
						RecordID : pageJS.vrs.data.RecordID,
						RecordTime : pageJS.vrs.data.RecordTime
					 },
		        success: function(responseData) {
			        	// Get AJAX status
		        		responseData = jQuery.parseJSON(responseData);
		        		status = "";
		        		if(responseData) {
		        			status = responseData.status;
		        		}
		        		
		        		// Close Modal and Reload Data Table
		              	$('a.modal-close').click();
	   					$('a.modal-action[value="yes"]').attr("disabled", false);
						Materialize.toast(_langtxt["medical"]["delete_"+status], 2000);
						window.location = "<?=curPageURL(0,0)."/home/eClassApp/eclassApp/medicalCaring/medical_item_list.php?Success=1"?>";
		  		}
	       });
	   	}
	});

});

</script>
<?php

//intranet_closedb();

$responseHTML = ob_get_contents();
ob_end_clean();
?>