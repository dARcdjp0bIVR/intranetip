<?php
/*
 * 2018-04-30 Cameron
 * - fix wording for ej
 */
ob_start();
// debug_pr($this->infoArr);

global $Lang;
?>

<div class="fiuld-container cusView">
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<!--<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$Lang['medical']['menu']['bowel']?></a></li>-->
				<li class="active"><?=$Lang['medical']['menu']['bowel']?></li>
				<li class="active"><?=$Lang['medical']['general']['new']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>1: <?=$Lang['medical']['App']['ChooseStudent']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form id='step1_form' class="col s12"
						rel="<?=$viewData["controllerLink"]?>/edit_step2"
						data-success="<?=$viewData["controllerLink"]?>/edit_step2">
						<div class="row">
							<div class="input-field col col-xs-12 col-sm-6" id="dropdown_opt"
								rel="sld_student">
								<input class="autocomplete" type="text" id="autocomplete-name"
									rel='dropdown_opt'> <label for="autocomplete-name"><?=$Lang['medical']['App']['SearchStudentUsingName']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6"
								id='dropdown_opt2' rel="sld_student">
								<input class="autocomplete" type="text" id="autocomplete-class"
									rel='dropdown_opt2'> <label for="autocomplete-class"><?=$Lang['medical']['App']['SearchStudentUsingClass']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-12">
								<select id="sld_student" name="sld_student[]" readyonly
									class="required">
									<option value="" disabled selected><?=$Lang['medical']['App']['PleaseSearchStudent']?></option>
								<?=$this->infoArr["UIContent"]["StudentSelection"]?>
							</select> <label><?=$Lang['medical']['App']['SelectedStudent']?></label>
							</div>
							<div class="col-xs-12">
								<div class="btn-group-justified">
									<a rel="<?=$viewData["controllerLink"]?>/mylist"
										class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn'><?=$Lang['Btn']['Cancel']?></a>
									<a rel="step1_form"
										class='btn btn-sm waves-effect waves-orange submitBtn'
										type="submit" name="action"><?=$Lang['Btn']['Continue']?><span
										class="glyphicon glyphicon-chevron-right"></span> </a>
								</div>
							</div>
						</div>
					</form>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>