<?php
/*
 *  2019-06-19 Cameron
 *      - change $Lang['eBooking']['App']['Button']['Cancel'] to $Lang['eBooking']['App']['Button']['PrevStep']
 * 
 *  2019-03-15 Cameron
 *      - fix: should display step title according to booking type
 */

ob_start();
global $Lang;
$data = $this->infoArr['postData']['formData'];

if ($data['bookingType'] == 'item') {
    $displayStep = $Lang['eBooking']['App']['FieldTitle']['ComfirmBookingItem'];
}
else {
    $displayStep = $Lang['eBooking']['App']['FieldTitle']['ComfirmBookingRoom'];
}

//pack booking details as json and placed in hidden field
$BookingDatails = $this->outputJSON($this->infoArr["api"]['data']['BookingDetails'],false);
?>
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</div>
				<nobr><?=$Lang['eBooking']['App']['FieldTitle']['NewBooking']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="active"><?=str_replace('<--Step Num-->', '3', $Lang['eBooking']['App']['FieldTitle']['Step'])?> <?=$displayStep?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='new_step3c' rel="<?php echo $viewData["controllerLink"]; ?>/new_step4" data-success="<?php echo $viewData["controllerLink"]; ?>/new_step4">
						<?=$this->infoArr['formSelectorHTML']?>
						<div class="col-xs-12">
							<div class="btn-group-justified">
								<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?php echo $viewData["controllerLink"]; ?>/new_step2c"><?=$Lang['eBooking']['App']['Button']['PrevStep']?></a>
								<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="new_step3c" name="action"><?=$Lang['eBooking']['App']['Button']['Continue']?> <span class="glyphicon glyphicon-chevron-right"></span> </a> 
							</div>
						</div>
						<div class="input-field col s12 row">
							<input type="hidden" name="BookingDetails" id="BookingDetails" value='<?=$BookingDatails?>'>
							<input type="hidden" name="SubmitFrom" id="SubmitFrom" value="step3c">
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>