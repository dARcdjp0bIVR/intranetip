<?php
/*
 *  2019-06-27 Cameron
 *      - don't allow to choose "Appointed Period" in Booking Method if Booking Type is "Repeated" for $sys_custom['eBooking']['EverydayTimetable']
 *      
 *  2019-06-19 Cameron
 *      - change $Lang['eBooking']['App']['Button']['Cancel'] to $Lang['eBooking']['App']['Button']['RealCancel']
 */
global $Lang, $sys_custom;
ob_start();
$data = $this->infoArr['postData']['formData'];
// $eBookingSetting = $this->infoArr['eBookingSetting'];
?>
							
							

<div class="row header">
	<div class="col-xs-12">
		<h3 class="text-info">
			<div class="pull-left">
				<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</div>
			<nobr><?=$Lang['eBooking']['App']['FieldTitle']['NewBooking']?></nobr>
		</h3>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
			<li class="active"><?=str_replace('<--Step Num-->','1',$Lang['eBooking']['App']['FieldTitle']['Step']).$Lang['eBooking']['App']['FieldTitle']['SelectBookingMethod'] ?></li>
		</ol>
		<div class="card active">
			<div class="card-content teal lighten-5">
				<form class="col s12" id='new_step1a' rel='<?php echo $viewData["controllerLink"]; ?>/new_step2' data-success='<?php echo $viewData["controllerLink"]; ?>/new_step2'>
					 <?=$this->infoArr['formSelectorHTML']?>
					 <div class="col-xs-12">
					 	<div class="btn-group-justified">
							<a rel='<?php echo $viewData["controllerLink"]; ?>/list' class='ajlink btn btn-sm grey darken-1 waves-effect waves-orange'><?=$Lang['eBooking']['App']['Button']['RealCancel']?></a>
							<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="new_step1a" name="action"><?=$Lang['eBooking']['App']['Button']['Continue']?><span class="glyphicon glyphicon-chevron-right"></span> </a> 
						</div>
					</div>
				</div>
				</form>
				<div class='clearfix'></div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
<?php if ($sys_custom['eBooking']['EverydayTimetable']):?>
	var specificTimeOptionText = $('#timeMode option[value="specificTime"]').text();
	
	$('#modeSelect').on('change',function(){
		if ($(this).val() == '2') {
			$('#timeMode option[value="specificTime"]').remove();
			$('#timeMode').material_select();
		}
		else {
			$('#timeMode').append('<option value="specificTime">'+specificTimeOptionText+'</option>');
			$('#timeMode').material_select();
		}
	});        
<?php endif;?>    
});
</script>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>