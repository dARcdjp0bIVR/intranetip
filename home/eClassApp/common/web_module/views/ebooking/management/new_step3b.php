<?php
ob_start();
global $Lang;
$data = $this->infoArr['postData']['formData'];
$BookingDatails = $this->outputJSON($this->infoArr["api"]['data']['BookingDetails'],false);
?>
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</div>
				<nobr><?=$Lang['eBooking']['App']['FieldTitle']['NewBooking'] ?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li class="active"><?=str_replace('<--Step Num-->', '3', $Lang['eBooking']['App']['FieldTitle']['Step'])?><?=$Lang['eBooking']['App']['FieldTitle']['ComfirmBookingTime']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='new_step3b' rel="<?php echo $viewData["controllerLink"]; ?>/new_step4" data-success="<?php echo $viewData["controllerLink"]; ?>/new_step4">
						<?=$this->infoArr['formSelectorHTML']?>
						<div class="col-xs-12">
							<div class="btn-group-justified">
								<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?php echo $viewData["controllerLink"]; ?>/new_step2b"><?=$Lang['eBooking']['App']['Button']['Cancel']?></a>
								<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="new_step3b" name="action"><?=$Lang['eBooking']['App']['Button']['Continue'] ?><span class="glyphicon glyphicon-chevron-right"></span> </a> 
							</div>
						</div>
						<div class="input-field col s12 row">
							<input type="hidden" name="BookingDetails" id="BookingDetails" value='<?=$BookingDatails?>'>
							<input type="hidden" name="roomID" id="roomID" value='<?=$data['roomID']?>'>
							<input type="hidden" name="SubmitFrom" id="SubmitFrom" value="step3b">
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>