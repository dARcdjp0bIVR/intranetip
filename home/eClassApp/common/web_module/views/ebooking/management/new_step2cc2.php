<?php
/*
 *  2019-06-19 Cameron
 *      - change $Lang['eBooking']['App']['Button']['Cancel'] to $Lang['eBooking']['App']['Button']['PrevStep']
 */
global $Lang;
ob_start();
$data = $this->infoArr['postData']['formData'];
// $eBookingSetting = $this->infoArr['eBookingSetting'];
?>
							
							

<div class="row header">
	<div class="col-xs-12">
		<h3 class="text-info">
			<div class="pull-left">
				<a href="#" rel='<?php echo $viewData["controllerLink"]; ?>/list' class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span></a>
			</div>
			<nobr><?=$Lang['eBooking']['App']['FieldTitle']['NewBooking']?></nobr>
		</h3>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<ol class="breadcrumb">
						<li class="active"><?=str_replace('<--Step Num-->', '2', $Lang['eBooking']['App']['FieldTitle']['Step'])?> <?=$Lang['eBooking']['App']['FieldTitle']['SelectBookingPeriod']?></li>
		</ol>
		<div class="card active">
			<div class="card-content teal lighten-5">
				<form class="col s12" id='new_step2cc2' rel='<?php echo $viewData["controllerLink"]; ?>/new_step3cc' data-success='<?php echo $viewData["controllerLink"]; ?>/new_step3cc'>
					 <?=$this->infoArr['formSelectorHTML']?>
					 <div class="col-xs-12">
					 	<div class="btn-group-justified">
							<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel='<?php echo $viewData["controllerLink"]; ?>/new_step2cc1'><?=$Lang['eBooking']['App']['Button']['PrevStep']?></a>
							<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="new_step2cc2" name="action"><?=$Lang['eBooking']['App']['Button']['Continue']?><span class="glyphicon glyphicon-chevron-right"></span> </a> 
						</div>
					</div>
					<input type="hidden" name="bookingType" value='<?=$data['bookingType']?>'>
						<?php foreach ( (array)$data['dateArr'] as $_date){?>	
	 						<input type="hidden" name="date[]" value="<?=$_date?>">
	 				<?php }?>
	 				<input type="hidden" name="SubmitFrom" id="SubmitFrom" value="new_step2cc2">
				</form>
				<div class='clearfix'></div>
			</div>
		</div>
	</div>
</div>
<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>