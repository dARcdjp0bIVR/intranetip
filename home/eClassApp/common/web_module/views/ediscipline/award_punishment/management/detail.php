<?php
ob_start();
//debug_pr($this->infoArr);
//debug_pr($viewData);

global $Lang, $eDiscipline;
global $i_general_class, $i_general_name, $i_Discipline_Reason2, $i_Discipline_System_Discipline_Status, $i_Discipline_System_Subscore1, $i_Profile_PersonInCharge, $i_Homework_subject, $i_Discipline_Last_Updated, $i_Discipline_Remark;
global $i_Notice_DateStart, $i_Notice_DateEnd, $i_Notice_Title, $i_Notice_NoticeNumber, $i_Notice_Description, $i_Notice_Issuer, $i_Notice_SignStatus, $i_UserStudentName, $i_Notice_RecipientType;
global $i_Discipline_System_Award_Punishment_Reference, $i_Discipline_Reason, $i_EventDate;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<form id="filterForm" rel="/list" class="ng-pristine ng-valid">
				<h3 class="text-info">
					<div class="pull-left">
						<a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
					</div>
					<div class="pull-right">
						<? if($this->infoArr["PageData"]["EditAPAccessRight"]) { ?>
							<a href="#" rel="<?=$viewData["controllerLink"]?>/edit_record" class="ajlink btn btn-sm btn-info waves-effect waves-orange" type="submit" name="action" class="img_target"><span class="glyphicon glyphicon-pencil"></span> <span class='hidden-xs'> </span></a>
						<? } ?>
						<? if($this->infoArr["PageData"]["DeleteAPAccessRight"]) { ?>
							<a href='#myModal' rel="<?=$viewData["controllerLink"]?>/delete" class="btn btn-sm wave s-effect waves-orange"><span class="glyphicon glyphicon-trash"></span> <span class='hidden-xs'> </span></a>
						<? } ?>
					</div>
					<nobr><?=$eDiscipline['Award_and_Punishment']?></nobr>
				</h3>
				<hr>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a id="index_link" href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Award_and_Punishment']?></a></li>
				<li class="active"><?=$Lang['eDiscipline']['App']['RecordDetails']?></li>
			</ol>
			<div class="card">
				<div class="card-content grey lighten-4">
					<div class="row">
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$eDiscipline['Category_Item']?>: </div>
						<div class="col-xs-12 col-sm-9 col-lg-10 listvalue border-bottom-1px text-bold"><?=$this->infoArr["PageData"]["RecordInfo"]["ItemName"]?> </div>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_general_class?>: </div>
						<div class="col-xs-12 col-sm-9 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["ClassNameNum"]?></div>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_general_name?>: </div>
						<div class="col-xs-12 col-sm-9 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["StudentName"]?></div>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$Lang['eDiscipline']['EventDate']?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordDate"]?></div>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Discipline_Reason2?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordReceived"]?></div>
						<? if(isset($this->infoArr["PageData"]["RecordInfo"]["ConductScoreChange"])) { ?>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$eDiscipline['Conduct_Mark']?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["ConductScoreChange"]?></div>
						<? } ?>
						<? if(isset($this->infoArr["PageData"]["RecordInfo"]["SubScore1Change"])) { ?>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Discipline_System_Subscore1?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["SubScore1Change"]?></div>
						<? } ?>
						<? if(isset($this->infoArr["PageData"]["RecordInfo"]["SubScore2Change"])) { ?>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$Lang['eDiscipline']['ActivityScore']?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["SubScore2Change"]?></div>
						<? } ?>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Discipline_System_Discipline_Status?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordStatus"]?></div>
						<!--
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$eDiscipline['Action']?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordAction"]?></div>
						-->
						<? if(isset($this->infoArr["PageData"]["RecordInfo"]["RecordSubject"])) { ?>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Homework_subject?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordSubject"]?></div>
						<? } ?>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Profile_PersonInCharge?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["RecordPICs"]?></div>
						<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Discipline_Last_Updated?>: </div>
						<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["ModifiedDateTime"]?></div>
						<div class="clearfix col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Discipline_Remark?>: </div>
						<div class="col-xs-12 col-sm-6 col-lg-10 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["RecordInfo"]["Remark"]?></div>
					</div>
				</div>
			</div>
			<? if (isset($this->infoArr["PageData"]["NoticeInfo"])) { ?>
				<div class="card">
					<div class="card-content grey lighten-4">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-lg-12 listlabel">
								<h5><?=$i_Notice_Description?></h5>
							</div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_DateStart?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["DateStart"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_DateEnd?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["DateEnd"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_Title?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["Title"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_NoticeNumber?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["NoticeNumber"]?></div>
							<div class="clearfix col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_Description?>: </div>
							<div class="col-xs-12 col-sm-6 col-lg-10 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["Description"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_Issuer?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["NoticeIssuer"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Profile_PersonInCharge?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["NoticePIC"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_SignStatus?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["NoticeStatus"]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_UserStudentName?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["Recipient"][1]?></div>
							<div class="col-xs-12 col-sm-3 col-lg-2 listlabel"><?=$i_Notice_RecipientType?>: </div>
							<div class="col-xs-12 col-sm-3 col-lg-4 listvalue border-bottom-1px"><?=$this->infoArr["PageData"]["NoticeInfo"]["Recipient"][0]?></div>
						</div>
					</div>
				</div>
			<? } ?>
			<? if (isset($this->infoArr["PageData"]["RecordInfo"]["RecordRelatedConduct"])) { ?>
				<div class="card">
					<div class="card-content grey lighten-4">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-lg-12 listlabel">
								<h5><?=$eDiscipline['HISTORY']?></h5>
							</div>
							<table id="conductTable" class="dTtable table table-striped table-hover nowrap" cellspacing="0" width="100%">
								<thead>
									<tr class='' role="row">
										<th class='text-center' width='55%'><?=$i_Discipline_Reason?></th>
										<th class='text-center' width='15%'><?=$i_EventDate?></th>
										<th class='text-center' width='30%'><?=$i_Profile_PersonInCharge?></th>
									</tr>
								</thead>
								<tbody>
									<?=$this->infoArr["PageData"]["RecordInfo"]["RecordRelatedConduct"]?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
	</div>
</div>

<script>
// Set Modal Handling
$("div#myModal").off('click', 'a.modal-action[value="yes"]');
$("div#myModal").on('click', 'a.modal-action[value="yes"]', function (e) {
   	if($('a.modal-action[value="yes"]').attr("disabled") != "disabled") {
		$('a.modal-action[value="yes"]').attr("disabled", true);
	    $.ajax({
			type: "POST",
			url: pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/delete',
			data: { id: '', token: initToken, target_id : pageJS.vrs.data.targetid },
	        success: function(responseData) {
		        	// Get AJAX status
	        		responseData = jQuery.parseJSON(responseData);
	        		status = "";
	        		if(responseData) {
	        			status = responseData.status;
	        		}
	        		
	        		// Close Modal
	              	$('a.modal-close').click();
   					$('a.modal-action[value="yes"]').attr("disabled", false);
					Materialize.toast(_langtxt["ediscipline"]["delete_"+status], 2000);
					
					// Redirect to Table List if success
	        		if(status=="success") {
	        			$('a#index_link').get(0).click();
	        		}
	  		}
       });
   	}
});

// Init related Conduct Data Table
if($('table#conductTable')) {
	$('table#conductTable').DataTable({
			responsive: true,
			searching: 	false,
			paging:   	false,
			ordering: 	false,
			info:     	false
    });
}
</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>