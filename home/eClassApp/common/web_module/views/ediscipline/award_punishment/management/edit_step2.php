<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang, $eDiscipline, $iDiscipline;
global $i_Merit_Award, $i_Merit_Punishment, $i_general_class, $i_general_name, $i_Discipline_System_Conduct_Semester, $i_Discipline_System_Subscore1, $i_Profile_PersonInCharge, $i_Discipline_System_Discipline_Status, $i_Discipline_Remark, $i_Discipline_Last_Updated;
global $i_general_choose_student, $i_general_selected_students, $i_Discipline_System_Discipline_Case_Record_Attachment, $i_general_receive, $i_Subject_name;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/edit" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$eDiscipline['Award_and_Punishment']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Award_and_Punishment']?></a></li>
				<li class="active"><?=$eDiscipline['NewRecord']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>2: <?=$eDiscipline['NewRecord']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form' rel="<?=$viewData["controllerLink"]?>/edit_step3" data-success="<?=$viewData["controllerLink"]?>/edit_step3">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<? if(!empty($this->InfoArr["UIContent"]["SemesterSelect"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<?=$this->InfoArr["UIContent"]["SemesterSelect"]?>
									<label><?=$i_Discipline_System_Conduct_Semester?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<div class="select-wrapper">
									<input id="date" name="date" class="datepicker" value="<?=date("Y/m/d")?>">
								</div>
								<label for="date"><?=$Lang['eDiscipline']['EventDate']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="ap_type_select" name="ap_type_select" rel="ap_category_select" target-module="award_punishment">
									<option value="" disabled selected><?=$Lang['General']['PleaseSelect']?></option>
									<option value="1"><?=$i_Merit_Award?></option>
									<option value="-1"><?=$i_Merit_Punishment?></option>
								</select>
								<label><?=$eDiscipline['Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="ap_category_select" name="ap_category_select" rel="ap_item_select" target-module="award_punishment">
									<option value="" disabled selected>-- <?=$Lang['General']['PleaseSelect']?> --</option>
								</select>
								<label><?=$iDiscipline['Accumulative_Category']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								 <select class="select_ajaxload required" id="ap_item_select" name="ap_item_select">
									<option value="" disabled selected>-- <?=$Lang['General']['PleaseSelect']?> --</option>
								</select>
								<label><?=$iDiscipline['Accumulative_Category_Item']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6" id="merit_div">
								 <select id="ap_merit_type" name="ap_merit_type" onchange="updateMeritNumValue(this.value)">
									<?=$this->infoArr["UIContent"]["MeritTypeOption"]?>
								</select>
								<label><?=$iDiscipline['Award_Punishment_Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6" id="demerit_div" style="display:none">
								 <select id="ap_demerit_type" name="ap_demerit_type" onchange="updateMeritNumValue(this.value)">
									<?=$this->infoArr["UIContent"]["DemeritTypeOption"]?>
								</select>
								<label><?=$iDiscipline['Award_Punishment_Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								 <select id="ap_merit_num" name="ap_merit_num" onchange="updateMeritTypeValue(this.value)">
									<?=$this->infoArr["UIContent"]["MeritNumOption"]?>
								</select>
								<label><?=$i_general_receive?></label>
							</div>
							<? if(!empty($this->infoArr["UIContent"]["ConductMarkOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_conduct" name="ap_merit_conduct">
									 	<?=$this->infoArr["UIContent"]["ConductMarkOption"]?>
									</select>
									<label><?=$eDiscipline['Conduct_Mark']?></label>
								</div>
							<? } ?>
							<? if(!empty($this->infoArr["UIContent"]["StudyScoreOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_study_score" name="ap_merit_study_score">
									 	<?=$this->infoArr["UIContent"]["StudyScoreOption"]?>
									</select>
									<label><?=$i_Discipline_System_Subscore1?></label>
								</div>
							<? } ?>
							<? if(!empty($this->infoArr["UIContent"]["ActivityScoreOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_activity_score" name="ap_merit_activity_score">
									 	<?=$this->infoArr["UIContent"]["ActivityScoreOption"]?>
									</select>
									<label><?=$Lang['eDiscipline']['ActivityScore']?></label>
								</div>
							<? } ?>
                            <? if(!empty($this->infoArr["UIContent"]["SubjectNameOption"])) { ?>
                                <div class="input-field col col-xs-12 col-sm-6">
                                    <select id="ap_subject_select" name="ap_subject_select">
                                        <?=$this->infoArr["UIContent"]["SubjectNameOption"]?>
                                    </select>
                                    <label><?=$i_Subject_name?></label>
                                </div>
                            <? } ?>
							<? if ($this->infoArr["PageData"]["HiddenPICSelect"]=="") { ?>
								<div class="input-field col col-xs-12 col-sm-6" id="dropdown_opt" rel="sld_student">
									<div class="select-wrapper"></div>
									<input type="text" id="autocomplete-teacher-name" rel='dropdown_opt' class="autocomplete">
									<label for="autocomplete-name"><?=$Lang['eDiscipline']['App']['SearchPICsUsingName']?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<select multiple class="required" id="sld_student" name="pics[]" readyonly>
									<option value="" disabled selected><?=$Lang['eDiscipline']['App']['PleaseSearch']?></option>
									<?=$this->infoArr["UIContent"]["PICDefultOption"]?>
								</select>
								<label><?=$Lang['eDiscipline']['App']['SelectedPICs']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="textarea1" name="remarks" class="materialize-textarea" data-length="120"></textarea>
								<label for="textarea1"><?=$i_Discipline_Remark?></label>
							</div>
							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?=$viewData["controllerLink"]?>/edit"><?=$Lang['Btn']['Back']?></a>
									<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step2_form" name="action" onclick="updateMeritSetVal()"><?=$eDiscipline['NewRecord']?></a> 
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$("form#step2_form").on('change', 'select#ap_item_select', function (e) {
	var target_item = $('select#ap_item_select').val();
	if(target_item > 0) {
		$.ajax({
			type: "POST",
			url: pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/ap_item_select',
			data: { id: '', token: initToken, target_id : target_item },
	        success: function(responseData) {
		        	// Get Response Data & Load Preset Settings
	        		responseData = jQuery.parseJSON(responseData);
	        		if(responseData && responseData.resultData) {
	        			var item_merit_type = $('#ap_type_select').val();
	        			var item_merit_profile_type = responseData.resultData.RelatedMeritType;
	        			var item_merit_count = responseData.resultData.NumOfMerit;
	        			var item_merit_conduct = responseData.resultData.ConductScore;
	        			var item_merit_subscore = responseData.resultData.SubScore1;
	        			var item_merit_actscore = responseData.resultData.SubScore2;
	        			
	        			// Merit Type 
	        			if(item_merit_type=="1") {
	        				$('div#demerit_div').hide();
	        				$('div#merit_div').show();
	        				$('select#ap_merit_type').val(item_merit_profile_type);
    						$('select#ap_merit_type').material_select();
	        			}
	        			else {
	        				$('div#merit_div').hide();
	        				$('div#demerit_div').show();
	        				$('select#ap_demerit_type').val(item_merit_profile_type);
    						$('select#ap_demerit_type').material_select();
	        			}
	        			
	        			// Merit Number
        				$('select#ap_merit_num').val(item_merit_count);
						$('select#ap_merit_num').material_select();
						
						// Merit Conduct Score
						if($('select#ap_merit_conduct')) {
    						$('select#ap_merit_conduct').val(item_merit_conduct);
    						$('select#ap_merit_conduct').material_select();
						}
						
						// Merit Study Score
						if($('select#ap_merit_study_score')) {
    						$('select#ap_merit_study_score').val(item_merit_subscore);
    						$('select#ap_merit_study_score').material_select();
						}
						
						// Merit Activity Score
						if($('select#ap_merit_activity_score')) {
    						$('select#ap_merit_activity_score').val(item_merit_actscore);
    						$('select#ap_merit_activity_score').material_select();
						}
	        		}
	  		}
		});
	}
});

function updateMeritSetVal()
{
	// Set Receive to "0"
	if($('select#ap_merit_num')) {
		if($('div#merit_div:visible')[0] && $('select#ap_merit_type') && $('select#ap_merit_type').val()=="-999") {
			$('select#ap_merit_num').val("0.00");
			$('select#ap_merit_num').material_select();
		}
		if($('div#demerit_div:visible')[0] && $('select#ap_demerit_type') && $('select#ap_demerit_type').val()=="-999") {
			$('select#ap_merit_num').val("0.00");
			$('select#ap_merit_num').material_select();
		}
	}
	
	// Set Award/Punishment Type to "---"
	if($('select#ap_merit_num') && $('select#ap_merit_num').val()=="0.00") {
		if($('select#ap_merit_type')) {
			$('select#ap_merit_type').val("-999");
			$('select#ap_merit_type').material_select();
		}
		if($('select#ap_demerit_type')) {
			$('select#ap_demerit_type').val("-999");
			$('select#ap_demerit_type').material_select();
		}
	}
}

function updateMeritNumValue(val)
{
	if($('select#ap_merit_num') && val=="-999") {
		$('select#ap_merit_num').val("0.00");
		$('select#ap_merit_num').material_select();
	}
}

function updateMeritTypeValue(val)
{
	if($('select#ap_merit_type') && val=="0.00") {
		$('select#ap_merit_type').val("-999");
		$('select#ap_merit_type').material_select();
	}
	if($('select#ap_demerit_type') && val=="0.00") {
		$('select#ap_demerit_type').val("-999");
		$('select#ap_demerit_type').material_select();
	}
}

</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>