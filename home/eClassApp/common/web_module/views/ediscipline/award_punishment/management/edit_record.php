<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang, $eDiscipline, $iDiscipline;
global $i_Discipline_GoodConduct, $i_Discipline_Misconduct, $i_general_class, $i_general_name, $i_Discipline_System_Conduct_Semester, $i_general_receive, $i_Discipline_System_Subscore1, $i_Profile_PersonInCharge, $i_Discipline_System_Discipline_Status, $i_Discipline_Remark, $i_Discipline_Last_Updated;
global $i_general_choose_student, $i_general_selected_students, $i_Discipline_System_Discipline_Case_Record_Attachment, $i_Discipline_System_Award_Punishment_Release, $i_Subject_name, $button_update;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$eDiscipline['Award_and_Punishment']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Award_and_Punishment']?></a></li>
				<li class="active"><?=$eDiscipline['EditRecord']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form' rel="<?=$viewData["controllerLink"]?>/edit_record_update" data-success="<?=$viewData["controllerLink"]?>/mylist">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<? if(!empty($this->InfoArr["UIContent"]["SemesterSelect"]) && $this->infoArr["PageData"]["DisableDetailsFields"]=="") { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<?=$this->InfoArr["UIContent"]["SemesterSelect"]?>
									<label><?=$i_Discipline_System_Conduct_Semester?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<div class="select-wrapper">
									<input id="date" name="date" class="datepicker" value="<?=$this->infoArr["PageData"]["MeritRecord"]["RecordDate"]?>" <?=$this->infoArr["PageData"]["DisableDateField"]?>>
								</div>
								<label for="date"><?=$Lang['eDiscipline']['EventDate']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select id="ap_type_select" disabled>
									<option value="<?=$this->infoArr["PageData"]["MeritRecord"]["MeritType"]?>" disabled selected><?=$this->infoArr["PageData"]["MeritRecord"]["MeritTypeName"]?></option>
								</select>
								<label><?=$eDiscipline['Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="ap_category_select" name="ap_category_select" rel="ap_item_select" target-module="award_punishment" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									<option value="" disabled>-- <?=$Lang['General']['PleaseSelect']?> --</option>
									<?=$this->infoArr["UIContent"]["CategoryOption"]?>
								</select>
								<label><?=$iDiscipline['Accumulative_Category']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="ap_item_select" name="ap_item_select" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									<option value="" disabled>-- <?=$Lang['General']['PleaseSelect']?> --</option>
									<?=$this->infoArr["UIContent"]["ItemOption"]?>
								</select>
								<label><?=$iDiscipline['Accumulative_Category_Item']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6" id="merit_div">
								<select class="select_ajaxload" id="ap_merit_type" name="ap_merit_type" onchange="updateMeritNumValue(this.value)" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									<?=$this->infoArr["UIContent"]["ProfileTypeOption"]?>
								</select>
								<label><?=$iDiscipline['Award_Punishment_Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select id="ap_merit_num" name="ap_merit_num" onchange="updateMeritTypeValue(this.value)" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									<?=$this->infoArr["UIContent"]["ProfileCountOption"]?>
								</select>
								<label><?=$i_general_receive?></label>
							</div>
							<? if(!empty($this->infoArr["UIContent"]["ConductScoreOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_conduct" name="ap_merit_conduct" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
										<?=$this->infoArr["UIContent"]["ConductScoreOption"]?>
									</select>
									<label><?=$eDiscipline['Conduct_Mark']?></label>
								</div>
							<? } ?>
							<? if(!empty($this->infoArr["UIContent"]["StudyScoreOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_study_score" name="ap_merit_study_score" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									 	<?=$this->infoArr["UIContent"]["StudyScoreOption"]?>
									</select>
									<label><?=$i_Discipline_System_Subscore1?></label>
								</div>
							<? } ?>
							<? if(!empty($this->infoArr["UIContent"]["ActivityScoreOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<select id="ap_merit_activity_score" name="ap_merit_activity_score" <?=$this->infoArr["PageData"]["DisableDetailsFields"]?>>
									 	<?=$this->infoArr["UIContent"]["ActivityScoreOption"]?>
									</select>
									<label><?=$Lang['eDiscipline']['ActivityScore']?></label>
								</div>
							<? } ?>
                            <? if(!empty($this->infoArr["UIContent"]["SubjectNameOption"])) { ?>
                                <div class="input-field col col-xs-12 col-sm-6">
                                    <select id="ap_subject_select" name="ap_subject_select">
                                        <?=$this->infoArr["UIContent"]["SubjectNameOption"]?>
                                    </select>
                                    <label><?=$i_Subject_name?></label>
                                </div>
                            <? } ?>
							<? if($this->infoArr["PageData"]["HiddenPICSelect"]=="" && $this->infoArr["PageData"]["DisablePICField"]=="") { ?>
								<div class="input-field col col-xs-12 col-sm-6" id="dropdown_opt" rel="sld_student">
									<div class="select-wrapper"></div>
									<input type="text" id="autocomplete-teacher-name" rel='dropdown_opt' class="autocomplete">
									<label for="autocomplete-name"><?=$Lang['eDiscipline']['App']['SearchPICsUsingName']?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<select multiple class="required" id="sld_student" name="pics[]" readyonly <?=$this->infoArr["PageData"]["DisablePICField"]?>>
									<option value="" disabled selected><?=$Lang['eDiscipline']['App']['PleaseSearch']?></option>
									<?=$this->infoArr["UIContent"]["PICOption"]?>
								</select>
								<label><?=$Lang['eDiscipline']['App']['SelectedPICs']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="textarea1" name="remarks" class="materialize-textarea" data-length="120" <?=$this->infoArr["PageData"]["DisableRemarkFields"]?>><?=$this->infoArr["PageData"]["MeritRecord"]["Remark"]?></textarea>
								<label for="textarea1"><?=$i_Discipline_Remark?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select id="ap_record_status" name="ap_record_status" class="icons">
								 	<?=$this->infoArr["UIContent"]["RecordStatusOption"]?>
								</select>	
								<label><?=$i_Discipline_System_Discipline_Status?></label>
							</div>
							<? if(!empty($this->infoArr["UIContent"]["ReleaseStatusOption"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6" id="release_div">
									<select id="ap_record_release_status" name="ap_record_release_status" class="icons">
									 	<?=$this->infoArr["UIContent"]["ReleaseStatusOption"]?>
									</select>
									<label><?=$i_Discipline_System_Award_Punishment_Release?></label>
								</div>
							<? } ?>
							<div class="col-xs-12 text-right">
								<div class="btn-group-justified"> 
									<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?=$viewData["controllerLink"]?>/mylist"><?=$Lang['Btn']['Cancel']?></a>
									<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step2_form" name="action" onclick="updateMeritSetVal()"><?=$button_update?><span class="glyphicon glyphicon-chevron-right"></span> </a>
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$("form#step2_form").on('change', 'select#ap_item_select', function (e) {
	var target_item = $('select#ap_item_select').val();
	if(target_item > 0) {
	  $.ajax({
				type: "POST",
				url: pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/ap_item_select',
				data: { id: '', token: initToken, target_id : target_item },
		        success: function(responseData) {
			        	// Get AJAX status
		        		responseData = jQuery.parseJSON(responseData);
		        		if(responseData && responseData.resultData) {
		        			var item_merit_type = $('#ap_type_select').val();
		        			var item_merit_profile_type = responseData.resultData.RelatedMeritType;
		        			var item_merit_count = responseData.resultData.NumOfMerit;
		        			var item_merit_conduct = responseData.resultData.ConductScore;
		        			var item_merit_subscore = responseData.resultData.SubScore1;
		        			var item_merit_actscore = responseData.resultData.SubScore2;
		        			
		        			// Merit Type
	        				$('select#ap_merit_type').val(item_merit_profile_type);
    						$('select#ap_merit_type').material_select();
		        			
		        			// Merit Number
	        				$('select#ap_merit_num').val(item_merit_count);
							$('select#ap_merit_num').material_select();
							
							// Merit Conduct Score
							if($('select#ap_merit_conduct')) {
	    						$('select#ap_merit_conduct').val(item_merit_conduct);
	    						$('select#ap_merit_conduct').material_select();
							}
							
							// Merit Study Score
							if($('select#ap_merit_study_score')) {
	    						$('select#ap_merit_study_score').val(item_merit_subscore);
	    						$('select#ap_merit_study_score').material_select();
							}
							
							// Merit Activity Score
							if($('select#ap_merit_activity_score')) {
	    						$('select#ap_merit_activity_score').val(item_merit_actscore);
	    						$('select#ap_merit_activity_score').material_select();
							}
		  				}
		        }
	       });
	}
});

<? if(!empty($this->infoArr["UIContent"]["ReleaseStatusOption"])) { ?>
	$("form#step2_form").on('change', 'select#ap_record_status', function (e) {
		var record_status = $('select#ap_record_status').val();
		if(record_status == parseInt("<?=$this->infoArr["PageData"]["MeritRecord"]["RecordStatus"]?>")) {
			$('#release_div').show();
		}
		else {
			$('#release_div').hide();
		}
	});
<? } ?>

function updateMeritSetVal()
{
	<? if($this->infoArr["PageData"]["DisableDetailsFields"]=="") { ?>
		// Set Receive to "0"
		if($('select#ap_merit_num')) {
			if($('select#ap_merit_type') && $('select#ap_merit_type').val()=="-999") {
				$('select#ap_merit_num').val("0.00");
				$('select#ap_merit_num').material_select();
			}
		}
		
		// Set Award/Punishment Type to "---"
		if($('select#ap_merit_num') && $('select#ap_merit_num').val()=="0.00") {
			if($('select#ap_merit_type')) {
				$('select#ap_merit_type').val("-999");
				$('select#ap_merit_type').material_select();
			}
		}
	<? } ?>
}

function updateMeritNumValue(val)
{
	if($('select#ap_merit_num') && val=="-999") {
		$('select#ap_merit_num').val("0.00");
		$('select#ap_merit_num').material_select();
	}
}

function updateMeritTypeValue(val)
{
	if($('select#ap_merit_type') && val=="0.00") {
		$('select#ap_merit_type').val("-999");
		$('select#ap_merit_type').material_select();
	}
}

</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>