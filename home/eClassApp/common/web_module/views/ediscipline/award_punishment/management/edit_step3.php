<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang, $eDiscipline;
global $i_Discipline_Send_Notice_Via_ENotice, $i_Discipline_System_Discipline_Reason_For_Issue, $i_Discipline_System_Discipline_Template_Name, $i_Discipline_Additional_Info;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$eDiscipline['Award_and_Punishment']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Award_and_Punishment']?></a></li>
				<li class="active"><?=$this->infoArr["UIContent"]["PageNavigation"][0]?></li>
				<li class="active"><?=$this->infoArr["UIContent"]["PageNavigation"][1]?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id="step3_form" rel="<?=$viewData["controllerLink"]?>/edit_complete" data-success="<?=$viewData["controllerLink"]?>/mylist">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<div>
								<input type="checkbox" id="send_notice" name="send_notice" rel="set_notice_div" class="toggle_display_checkbox" value="1" <?=$this->infoArr["UIContent"]["NoticeOptions"]["DefaultSelectNotice"]?>/>
							    <label for="send_notice"><?=$eDiscipline["SendNoticeWhenReleased"]?></label>
							</div>
						</div>
						<div class="row" id="set_notice_div" <?=$this->infoArr["UIContent"]["NoticeOptions"]["DefaultShowNoticeDiv"]?>>
							<div class="input-field col col-xs-12 col-sm-6">
								 <select class="select_ajaxload required" id="notice_category" rel="notice_template" name="notice_category" target-module="award_punishment">
									<option value="" disabled selected><?=$Lang['General']['PleaseSelect']?></option>
									<?=$this->infoArr["UIContent"]["NoticeOptions"]["CategorySelectOptions"]?>
								</select>
								<label><?=$i_Discipline_System_Discipline_Reason_For_Issue?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								 <select class="select_ajaxload required" id="notice_template" name="notice_template">
									<option value="" disabled selected><?=$Lang['General']['PleaseSelect']?></option>
									<?=$this->infoArr["UIContent"]["NoticeOptions"]["NoticeSelectOptions"]?>
								</select>
								<label><?=$i_Discipline_System_Discipline_Template_Name?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-12">
								 <textarea id="additional_info" name="additional_info" class="materialize-textarea" data-length="120"></textarea>
								<label for="additional_info"><?=$i_Discipline_Additional_Info?></label>
							</div>
							<? if($this->infoArr["UIContent"]["NoticeOptions"]["DisableCheckSemdEmail"]==false) { ?>
								<div class="input-field col col-xs-12 col-sm-12">
									<input type="checkbox" id="email_notify" name="email_notify" value="1" <?=$this->infoArr["UIContent"]["NoticeOptions"]["DefaultCheckSendEmail"]?>/>
								    <label for="email_notify"><?=$Lang['eDiscipline']['EmailNotifyParent']?></label>
								</div>
							<? } ?>
						</div>
						<div class="row">
							<div class="input-field col col-xs-12 col-sm-12">&nbsp;</div>
							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step3_form" name="action"><?=$Lang['Btn']['Done']?></a>
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>