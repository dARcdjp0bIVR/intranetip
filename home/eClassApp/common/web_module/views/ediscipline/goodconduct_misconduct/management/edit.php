<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang, $eDiscipline, $sys_custom;
global $i_general_choose_student, $i_general_selected_students;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$eDiscipline['Good_Conduct_and_Misconduct']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Good_Conduct_and_Misconduct']?></a></li>
				<li class="active"><?=$eDiscipline['NewRecord']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>1: <?=$i_general_choose_student?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form id='step1_form' class="col s12" rel="<?=$viewData["controllerLink"]?>/edit_step2" data-success="<?=$viewData["controllerLink"]?>/edit_step2">
					<div class="row">
						<div class="input-field col col-xs-12 col-sm-6" id="dropdown_opt" rel="sld_student">
							<input class="autocomplete" type="text" id="autocomplete-name" rel='dropdown_opt'>
							<label for="autocomplete-name"><?=$Lang['eDiscipline']['App']['SearchStudentUsingName']?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-6" id='dropdown_opt2' rel="sld_student">
							<input class="autocomplete" type="text" id="autocomplete-class" rel='dropdown_opt2'>
							<label for="autocomplete-class"><?=$Lang['eDiscipline']['App']['SearchStudentUsingClass']?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-12">
							 <select multiple id="sld_student" name="sld_student[]" readyonly class="required">
								<option value="" disabled selected><?=$Lang['eDiscipline']['App']['PleaseSearchStudent']?></option>
								<?=$this->infoArr["UIContent"]["StudentSelection"]?>
							</select>
							<label><?=$i_general_selected_students?></label>
						</div>
						<div class="col-xs-12">
							<div class="btn-group-justified"> 
								<a rel="<?=$viewData["controllerLink"]?>/mylist" class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn'><?=$Lang['Btn']['Cancel']?></a>
								<a rel="step1_form" class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" name="action"><?=$Lang['Btn']['Continue']?><span class="glyphicon glyphicon-chevron-right"></span> </a>
							</div>
						</div>
					</div>
					<?php if($sys_custom['eDiscipline']['ShowStudentPhotoInTeacherAppSearch']) { ?>
						<input type='hidden' id='needphoto' name='needphoto' value='1'/>
					<?php } ?>
					</form>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>