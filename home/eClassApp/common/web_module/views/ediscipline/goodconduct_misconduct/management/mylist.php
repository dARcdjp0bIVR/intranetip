<?php
ob_start();
//debug_pr($_SESSION);

global $Lang, $eDiscipline;
global $i_general_class, $i_general_name, $i_Discipline_System_ItemName, $i_Profile_PersonInCharge, $i_Discipline_System_Discipline_Status, $i_Discipline_Last_Updated;
global $i_Discipline_System_Conduct_School_Year, $i_Discipline_System_Conduct_Semester;
global $token;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">	
			<h3 class="text-info">
			  	<a class='dropdown-button btn' href='#' data-activates='dropdown1' data-beloworigin="true" data-hover="false" style="width: 35px; padding-left: 0px; padding-right: 0px;">
			  		<img src="<?=$this->infoArr["thisImagePath"]?>/action_more.png" style='width: 35px;'/>
			  	</a>
			  	<ul id='dropdown1' class='dropdown-content'>
			    	<li><a href="#" rel="ediscipline/award_punishment/management/mylist" class="ajlink" style="width: auto; min-width: 0; background-color: white"><?=$eDiscipline['Award_and_Punishment']?></a></li>
			  	</ul>
				<nobr><?=$eDiscipline['Good_Conduct_and_Misconduct']?></nobr>
				<div class="pull-right">
					<? if ($this->infoArr["PageData"]["AddAPAccessRight"]) { ?>
						<a href="#" rel="<?=$viewData["controllerLink"]?>/edit" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-plus"></span> <span class="hidden-xs"> </span></a>
					<? } ?>
					<a href="#" class="btn btn-sm white-text waves-effect waves-orange filterBTN"><span class="glyphicon glyphicon-filter"></span> <span class="hidden-xs"> </span></a>
				</div>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="card hidden" id="filter">
				<div class="card-content grey lighten-4">
					<form id="filterForm" rel="<?=$viewData["controllerLink"]?>/mylist" data-success="<?=$viewData["controllerLink"]?>/mylist" class="ng-pristine ng-valid">
						<input type="hidden" name="fhidden" value="hval">
						<div class="input-field col col-xs-12 col-sm-6">
							<?=$this->infoArr["UIContent"]["SchoolYearSelection"]?>
							<label><?=$i_Discipline_System_Conduct_School_Year?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-6">
							<?=$this->infoArr["UIContent"]["SemesterSelection"]?>
							<label><?=$i_Discipline_System_Conduct_Semester?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-6">
							 <?=$this->infoArr["UIContent"]["ClassSelection"]?>
							<label><?=$i_general_class?></label>
						</div>
						<div class="input-field col col-xs-12 col-sm-6">
							<?=$this->infoArr["UIContent"]["PICSelection"]?>
							<label><?=$i_Profile_PersonInCharge?></label>
						</div>
						<div class="text-right col-xs-12">
							<div class="btn-group-justified">
								<a rel="filter" class="boxlink btn btn-sm grey darken-1 white-text"><?=$Lang['Btn']['Cancel']?></a>
								<a href="#" rel="ediscipline/management/" class="btn btn-sm teal lighten-1 white-text actionBtn" data-action="filter"><?=$Lang['Btn']['Submit']?></a>
							</div>
						</div>
						<input type="hidden" name="id" value="">
						<input type="hidden" name="token" value="<?=$token?>">
					</form>
					<div class="clearfix"></div>
				</div>
			</div>
			<table id="conductTable" class="dTtable table table-striped table-hover" cellspacing="0" width="100%" style="word-wrap:break-word;">
				<thead>
					<tr class='' role="row">
						<th class='text-center' style='white-space: nowrap;'><?=$i_Discipline_System_ItemName?></th>
						<th class='text-center'><?=$i_general_class?></th>
						<th class='text-center'><?=$i_general_name?></th>
						<th class='text-center' style='white-space: nowrap;'><?=$Lang['eDiscipline']['EventDate']?></th>
						<th class='text-center'><?=$eDiscipline['Type']?></th>
						<th class='text-center'><?=$eDiscipline['Action']?></th>
						<th class='text-center'><?=$i_Profile_PersonInCharge?></th>
						<th class='text-center' style='white-space: nowrap;'><?=$i_Discipline_Last_Updated?></th>
						<th class='text-center'><?=$i_Discipline_System_Discipline_Status?></th>
						<th class='text-center'>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		                <th>&nbsp;</th>
		            </tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<style>
.dTtable td {
	word-wrap: break-word;
}
</style>

<script>
function actonButtonHandle (e) {
	if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("pageJS.ltr.actonButtonHandle", "listener");
	var strHash = "";
	if (typeof $(this).attr('rel') != "undefined" && pageJS.vrs.pageReady) {
		pageJS.vrs.data.targetid = $('img.img_target', this).attr('data-id');
		strHREF = $(this).attr('href');
		if(strHREF != '#myModal') {
			strHash = $(this).attr('rel');
			strHash = strHash.replace(/\#/g, '');
			if (pageJS.vrs.currHash != strHash) {
				pageJS.cfn.pageOut();
				// pageJS.cfn.pageLoad(strHash);
				appJS.vrs.detectHashChange = false;
				window.location.hash = strHash;
				appJS.vrs.detectHashChange = true;
			}
		}
	}
	e.preventDefault();
}

$(document).ready( function()
{
	// Clear Existing Data
	if(pageJS.vrs.data.targetid) {
		pageJS.vrs.data.targetid = null;
	}
	
	// Build Data Table
	edisciplinetable = '';
    edisciplinetable = $('table#conductTable').DataTable( {
							dom: '<"top"f>rt<"bottom"l>p<"clear">',
	         				responsive: true,
	         				processing: true,
	         				bStateSave: true,
							columnDefs: [
					                        {	"className": "text-center", "targets": [ 8, 9 ]			},
					                        { 	"width": "8%", 				"targets": [ 4, 5, 7, 8 ]	},
					                        { 	"width": "10%",				"targets": [ 1, 2, 3, 6 ]	},
					                        { 	"width": "14%",				"targets": [ 0, 9 ]			},
					                        {	"orderable": false, 		"targets": [ 5, 8, 9 ]		}
					                    ],
	         				order:	 	[[ 3, "desc" ]],
					    	language: 	{
								            "search": _langtxt["dataTable"]["search"],
								            "lengthMenu": _langtxt["dataTable"]["recordPerPage"],
								            "info": _langtxt["dataTable"]["pageInfo"],
								            "paginate": {
								            	"next":	_langtxt["dataTable"]["next"] ,
								                "previous": _langtxt["dataTable"]["pervious"] 
								            },
								            "infoEmpty": _langtxt["dataTable"]["emptyPageInfo"],
								            "infoFiltered": _langtxt["dataTable"]["pageInfoFilter"],
								            "zeroRecords": _langtxt['dataNotFound'],
								            "loadingRecords": " "
					        			},
							ajax: 		{
								            "url" : pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/mydata',
								            "type" : "POST",
								            "data" : function () { return $('#filterForm').serializeArray(); }
										}
    });
    
    // Conduct Data Table Processing Handling
	$('table#conductTable').on( 'processing.dt', function ( e, settings, processing ) {
		if(processing) {
			$(pageJS.vrs.loadingElm).stop().fadeIn(800);
		}
		else {
			$(pageJS.vrs.loadingElm).stop().fadeOut(800);
		}
   	}).dataTable();
   
    // Set Click Handler for action buttons
    $('table#conductTable').off('click', 'a.link_target');
    $('table#conductTable').on('click', 'a.link_target', actonButtonHandle);
    // $('table#conductTable').on('click', 'img.img_target', function() { pageJS.vrs.data.targetid = $(this).attr('data-id'); });
    
    // Set Modal Handling
	$("div#myModal").off('click', 'a.modal-action[value="yes"]');
    $("div#myModal").on('click', 'a.modal-action[value="yes"]', function (e) {
	   	if($(this).attr("disabled") != "true") {
	   		$(this).attr("disabled", "true");
		    $.ajax({
				type: "POST",
				url: pageJS.vrs.url + '?page=<?=$viewData["controllerLink"]?>/delete',
				data: { id: '', token: initToken, target_id : pageJS.vrs.data.targetid },
		        success: function(responseData) {
			        	// Get AJAX status
		        		responseData = jQuery.parseJSON(responseData);
		        		status = "";
		        		if(responseData) {
		        			status = responseData.status;
		        		}
		        		
		        		// Close Modal
		              	$('a.modal-close').click();
	   					$('a.modal-action[value="yes"]').attr("disabled", false);
	   					
	   					// Reload Data Table
						edisciplinetable.ajax.reload();
						Materialize.toast(_langtxt["ediscipline"]["delete_"+status], 2000);
		  		}
	       });
	   	}
	});
	
//	ebookingtable.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
//		pageJS.cfn.bindListener();
//	} );
});
</script>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>