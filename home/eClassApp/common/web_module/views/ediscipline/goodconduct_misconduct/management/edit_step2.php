<?php
ob_start();
//debug_pr($this->infoArr);

global $Lang, $eDiscipline, $iDiscipline;
global $i_Discipline_System_Conduct_Semester, $i_Discipline_GoodConduct, $i_Discipline_Misconduct, $i_Discipline_System_Discipline_Category, $i_Discipline_Remark;
?>

<div class="fiuld-container cusView">
	<div class="row header">
		<div class="col-xs-12">
			<h3 class="text-info">
				<div class="pull-left">
					<a href="#" rel="<?=$viewData["controllerLink"]?>/edit" class="ajlink btn btn-sm white-text waves-effect waves-orange"><span class="glyphicon glyphicon-chevron-left"></span> <span class='hidden-xs'> </span></a>
				</div>
				<nobr><?=$eDiscipline['Good_Conduct_and_Misconduct']?></nobr>
			</h3>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
				<li><a href="#" rel="<?=$viewData["controllerLink"]?>/mylist" class="ajlink"><?=$eDiscipline['Good_Conduct_and_Misconduct']?></a></li>
				<li class="active"><?=$eDiscipline['NewRecord']?></li>
				<li class="active"><?=$Lang['General']['Steps']?>2: <?=$eDiscipline['NewRecord']?></li>
			</ol>
			<div class="card active">
				<div class="card-content teal lighten-5">
					<form class="col s12" id='step2_form' rel="<?=$viewData["controllerLink"]?>/edit_step3" data-success="<?=$viewData["controllerLink"]?>/edit_step3">
						<div class="row">
							<ul class="collection with-header">
						        <li class="collection-header"><h6><?=$this->infoArr["UIContent"]["SummaryTable"]?></h6></li>
						     </ul>
						</div>
						<div class="row">
							<? if(!empty($this->InfoArr["UIContent"]["SemesterSelect"])) { ?>
								<div class="input-field col col-xs-12 col-sm-6">
									<?=$this->InfoArr["UIContent"]["SemesterSelect"]?>
									<label><?=$i_Discipline_System_Conduct_Semester?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<div class="select-wrapper">
									<input id="date" name="date" class="datepicker" value="<?=date("Y/m/d")?>">
								</div>
								<label for="date"><?=$Lang['eDiscipline']['EventDate']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="gm_type_select" name="gm_type_select" rel="gm_category_select" target-module="goodconduct_misconduct">
									<option value="" disabled selected><?=$Lang['General']['PleaseSelect']?></option>
									<option value="1"><?=$i_Discipline_GoodConduct?></option>
									<option value="-1"><?=$i_Discipline_Misconduct?></option>
								</select>
								<label><?=$eDiscipline['Type']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								<select class="select_ajaxload required" id="gm_category_select" name="gm_category_select" rel="gm_item_select" target-module="goodconduct_misconduct">
									<option value="" disabled selected>-- <?=$eDiscipline['SelectConductCategory']?> --</option>
								</select>
								<label><?=$i_Discipline_System_Discipline_Category?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-6">
								 <select class="select_ajaxload required" id="gm_item_select" name="gm_item_select">
									<option value="" disabled selected>-- <?=$eDiscipline['SelectConductItem']?> --</option>
								</select>
								<label><?=$iDiscipline['Conduct_Item']?></label>
							</div>
							<? if ($this->infoArr["PageData"]["HiddenPICSelect"]=="") { ?>
								<div class="input-field col col-xs-12 col-sm-6" id="dropdown_opt" rel="sld_student">
									<div class="select-wrapper"></div>
									<input type="text" id="autocomplete-teacher-name" rel='dropdown_opt' class="autocomplete">
									<label for="autocomplete-name"><?=$Lang['eDiscipline']['App']['SearchPICsUsingName']?></label>
								</div>
							<? } ?>
							<div class="input-field col col-xs-12 col-sm-6">
								<select multiple class="required" id="sld_student" name="pics[]" readyonly>
									<option value="" disabled selected><?=$Lang['eDiscipline']['App']['PleaseSearch']?></option>
									<?=$this->infoArr["UIContent"]["PICDefultOption"]?>
								</select>
								<label><?=$Lang['eDiscipline']['App']['SelectedPICs']?></label>
							</div>
							<div class="input-field col col-xs-12 col-sm-12">
								<textarea id="textarea1" name="remarks" class="materialize-textarea" data-length="120"></textarea>
								<label for="textarea1"><?=$i_Discipline_Remark?></label>
							</div>
							<div class="col-xs-12 text-right">
								<div class="btn-group-justified">
									<a class='btn btn-sm grey darken-1 waves-effect waves-orange cancelBtn' rel="<?=$viewData["controllerLink"]?>/edit"><?=$Lang['Btn']['Back']?></a>
									<a class='btn btn-sm waves-effect waves-orange submitBtn' type="submit" rel="step2_form" name="action"><?=$eDiscipline['NewRecord']?></a> 
								</div>
							</div>
							<div class="input-field col s12 row">
								<?=$this->infoArr["UIContent"]["HiddenInputField"]?>
							</div>
						</div>
					</form>
					<div class='clearfix'></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$responseHTML = ob_get_contents();
ob_end_clean();
?>