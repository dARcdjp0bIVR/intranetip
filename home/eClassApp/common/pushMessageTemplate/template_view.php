<?php

################## Change Log [Start] ##############
#   Date:   2019-04-29 Anna
#           ADDED cleanHtmlJavascript
#
#	Date:	2015-07-24	Omas
#			add new module Message Center
#
#	Date:	2014-12-19	Omas
#			Create this file - for common use among all modules
#
################## Change Log [End] ##############
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libAppTemplate = new libeClassApp_template();

$Module = cleanCrossSiteScriptingCode($Module);
$Section = cleanCrossSiteScriptingCode($Section);
# check Access Right
$libAppTemplate->canAccess($Module,$Section);

# Init libeClassApp_template
$libAppTemplate->setModule($Module);
$libAppTemplate->setSection($Section);

# Page Setting & Left Menu
if($Module == 'StudentAttendance'){
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	$lc = new libcardstudentattend2();
	$CurrentPageArr['StudentAttendance'] = 1;
	$CurrentPage = "PageStudentAttendanceSettings_MessageTemplate";
	if($Section == 'NotSubmitDocument'){
		//sub tabs
	}

	$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
}
else if($Module == 'eNotice'){
	include_once($PATH_WRT_ROOT."includes/libnotice.php");
	$lnotice = new libnotice();
	$CurrentPageArr['eAdminNotice'] = 1;
	$CurrentPage = "PageNoticeSettings_MessageTemplate";
	if($Section == 'NoticeSummaryReport'){
		//sub tabs
	}

	$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();
}
else if($Module == 'MessageCenter'){
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	$limc = new libmessagecenter();
	$CurrentPage = "PagePushNotificationTemplate";
	
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
}
$TAGS_OBJ[] = array($Lang['SMS']['MessageTemplates']);

//$curSubTab = 'tab1';
//$subTabAry = array();
//$subTabAry[] = array('Absent prove document hand-in Status Report', 'javascript: void(0);', $curSubTab=='tab1');
//$subTabAry[] = array('tab 2', '#', $curSubTab=='tab2');		// DON'T use "#", UI will go to top
//$subTabAry[] = array('tab 3', 'javascript: void(0);', $curSubTab=='tab3');
//$htmlAry['subTab'] = $linterface->GET_SUBTAGS($subTabAry);
$xmsg = cleanHtmlJavascript($xmsg);
# Start layout
if ($xmsg == 'DeleteSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}
else if ($xmsg == 'DeleteFail'){
	$xmsg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}
else if ($xmsg == 'AddSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['AddSuccess'];
}
else if ($xmsg == 'AddFail'){
	$xmsg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
else if ($xmsg == 'UpdateSuccess'){
	$xmsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if ($xmsg == 'UpdateFail'){
	$xmsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

$linterface->LAYOUT_START($xmsg);

# toolbar
$toolbar = $linterface->GET_LNK_ADD("template_new.php?Module=".$Module."&Section=".$Section."",$button_new,"","","",0);	

# table functionbar
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'TemplateID[]','template_new.php')\" class=\"tool_edit\">{$button_edit}</a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TemplateID[]','template_new_update.php?Delete=1')\" class=\"tool_delete\">{$button_delete}</a>&nbsp;";

# db table info
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Title", "Content","DateModified");
$li->sql = $libAppTemplate->getMessageTemplateSQL();
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 'IP25_table';

// TABLE COLUMN
$li->column_list .= "<th width=1 class=tableTitle>#</th>\n";
$li->column_list .= "<th width=25% class=tableTitle>".$li->column(0, $i_SMS_TemplateName)."</th>\n";
$li->column_list .= "<th width=50% class=tableTitle>".$li->column(1, $i_SMS_TemplateContent)."</th>\n";
$li->column_list .= "<th width=25% class=tableTitle>".$li->column(2, $i_LastModified)."</th>\n";
$li->column_list .= "<th width=1 >".$li->check("TemplateID[]")."</th>\n";

?>
<script language="javascript">
	
</script>
<?=$htmlAry['subTab']?>
<div class="content_top_tool">
	<?=$toolbar?>

	<br style="clear:both" />
</div>

<form id="form1" name="form1" action="" method="POST">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom">
			<?=$selection?>
		</td>
		<td valign="bottom">
			<div class="common_table_tool">	
			<?= $functionbar ?>
			</div>
		</td>
	</tr>
	</table>
	
	<?=$li->display()?>
	
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="Module" id="Module" value="<?=$Module?>" />
	<input type="hidden" name="Section" id="Section" value="<?=$Section?>" />

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>