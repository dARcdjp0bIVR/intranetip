<?php
/*
 *  2020-01-15 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_GET['ItemID']);
$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$itemPhoto = '';

$itemDetail = $linventory->getSingleItemDetail($itemID);
if (count($itemDetail)) {
    $itemName = Get_Lang_Selection($itemDetail['NameChi'], $itemDetail['NameEng']);
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = Get_Lang_Selection($itemDetail['DescriptionChi'], $itemDetail['DescriptionEng']);
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
}


include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){
	
	adjustStocktakeDetailHeight();

	showMore();

    $(document).on('click', '#btnNotFound', function(e) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=teacherApp.ajax',
            data : {
                'action': 'updateSingleItemMissing',
                'ItemID': '<?php echo $itemID;?>',
                'LocationID': '<?php echo $locationID;?>',
                'AdminGroupID': '<?php echo $adminGroupID;?>',
                'Remark': $('#inputRemarks').val()
            },
            success: backToStocktake,
            error: show_ajax_error
        });
    });

    $(document).on('click', '#btnStocktake', function(e) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=teacherApp.ajax',
            data : {
                'action': 'updateSingleItemRemark',
                'ItemID': '<?php echo $itemID;?>',
                'Remark': $('#inputRemarks').val()
            },
            success: backToStocktake,
            error: show_ajax_error
        });
    });
    
});


function backToStocktake(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + "<?php echo $locationID;?>" + "&AdminGroupID=" + "<?php echo $adminGroupID;?>";
	}	
	else {
//		alert('Error on update missing');
	}
}

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="inventory navbar navbar-fixed-top edit">
            <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.view_single_item&ItemID=".$itemID."&LocationID=".$locationID."&AdminGroupID=".$adminGroupID;?>"><i class="fas fa-arrow-left"></i></a></div></div>
            <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['EditStockTakeItemInfo'];?></div>
            <div id="button">
            </div>
		</nav>
		<div id="content" class="stockingDetails single">
			<div class="row">
				<div class="subTitle map col-xs-12">
					<div class="title"><?php echo $itemDetail['FullLocation'];?></div>
				</div>
			</div>

			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			<div class="showMore" id="showMore"><a href="#"><?php echo $Lang['eInventory']['eClassApp']['ShowMore'];?></a></div>
			<div class="moreInfo" id="moreInfo">
    			<ul class="itemInfo">
    				<li>
    					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
    					<span class="text"><?php echo $itemDetail['ItemCode'];?></span>
    				</li>
    				<li>
    					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
    					<span class="text"><?php echo $itemDescription;?></span>
    				</li>
    				<li>
    					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
    					<span class="text"><?php echo $itemDetail['Barcode'];?></span>
    				</li>
    				<li>
    					<span class="label"><?php echo $Lang['General']['Status2'];?></span>
    					<span class="text"><?php echo $itemDetail['Status'];?></span>
    				</li>
                </ul>
            </div>
            
			<ul class="itemInfoList">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Remark;?></span>
					<div id="chNameField" class="fieldInput ">
						<input type="text" class="form-control inputField" id="inputRemarks" placeholder="" value="<?php echo $itemDetail['StocktakeRemark'];?>">
					</div>
				</li>
			</ul>
			<div id="setProcessArea" class="single">
				<div class="submitBtn col-xs-6">
					<a class="btn btn-primary" role="button" id="btnNotFound">
						<span><?php echo $Lang['eInventory']['eClassApp']['StockTakeNotFound'];?></span>
					</a>
				</div>
				<div class="submitBtn col-xs-6">
					<a class="btn btn-primary" role="button" id="btnStocktake">
						<span><?php echo $Lang['eInventory']['eClassApp']['ConfirmStockTake'];?></span>
					</a>
				</div>
			</div>
            
		</div>            
	</div>
</body>
</html>

