<?php
/*
 *  2020-02-18 Cameron
 *      - always show funding source for bulk item
 *       
 *  2020-01-24 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);

$currentLocation = $linventory->returnLocationStr($locationID);
$itemListAry = $linventory->getStocktakeNotDoneByLocation($locationID, $adminGroupID);
//debug_pr($itemListAry);
$stocktakeNotDoneByLocationAssoc = BuildMultiKeyAssoc($itemListAry, array('ItemID','GroupInCharge','FundingSourceID'));

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){
	
	adjustContentHeight();
	
    $(document).on('click', '#StocktakeBtn', function(e) {
        if ($(':input[name="ItemIDTypeFunding\[\]"]:checked').length) {
        	$('#form1').attr('action','?task=teacherApp.stocktake.stocktake_by_selection_update');
        	$('#form1').submit();
        }
        else {
			alert("<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>");
        }
    });

    
    $(document).on('click', '#NotFoundBtn', function(e) {
        e.preventDefault();
        if ($(':input[name="ItemIDTypeFunding\[\]"]:checked').length) {
        	$('#form1').attr('action','?task=teacherApp.stocktake.stocktake_by_selection_notfound_update');
        	$('#form1').submit();
        }
        else {
			alert("<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>");
        }
    });

    $(document).on('click', 'a.bulkItem', function(e) {
		e.preventDefault();
		$('#BulkItemID').val($(this).attr('data-ItemID'));
		$('#BulkFundingSourceID').val($(this).attr('data-FundingSourceID'));
		$('#ShowFundingSource').val($(this).attr('data-ShowFundingSource'));
    	$('#form1').attr('action','?task=teacherApp.stocktake.stocktake_by_selection_bulk');
    	$('#form1').submit();
		
    });
    
    $(document).on('change', ':input[name="ItemIDTypeFunding\[\]"]', function(e) {
        var nrChecked = $(':input[name="ItemIDTypeFunding\[\]"]:checked').length;
        if (nrChecked > 0) {
            $('span.itemNum').html(nrChecked);
            $('span.small').css('display','');
        }
        else {
            $('span.itemNum').html('');
            $('span.small').css('display','none');
        }
		if ($(this).is(':checked') && ($(this).attr('data-ItemType') == 2)) {
			$('#NotFoundBtn').attr('disabled', true);
			$('#NotFoundBtn').hide();
		}
		else if ($('input.bulk-item:checked').length == 0) {
			$('#NotFoundBtn').attr('disabled', false);
			$('#NotFoundBtn').show();
		}
    });
});

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
<form id="form1" name="form1" method="post">
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="navbar navbar-fixed-top inventory">
            <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.stocktake&LocationID=".$locationID."&AdminGroupID=".$adminGroupID;?>"><i class="fas fa-arrow-left"></i></a></div></div>
            <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['NotDoneStockTakeItem'];?><span class="small" style="display: none;">(<?php echo $Lang['eInventory']['eClassApp']['HaveSelected'];?> <span class="itemNum"></span> <?php echo $Lang['eInventory']['eClassApp']['Item'];?>)</span></div>
            <div id="button">
            </div>
		</nav>
		<div id="content" class="nonStocktakeList">
			<div class="row">
				<div class="subTitle map subTitleFix col-xs-12">
					<div class="title"><?php echo $currentLocation;?></div>
				</div>
			</div>
			
			<ul class="nonStocktakeList contentFixHeight">

<?php for($i=0,$iMax=count($itemListAry);$i<$iMax;$i++):?>
<?php
    $thisItem = $itemListAry[$i];
    $_itemID = $thisItem['ItemID'];
    $_itemType = $thisItem['ItemType'];
    $_itemName = $thisItem['ItemName'];
    $_barcode = $thisItem['Barcode'];
    $_recordStatus = $thisItem['RecordStatus'];
    $_showRecordStatus = $thisItem['ShowRecordStatus'];
    $_groupInChargeID = $thisItem['GroupInCharge'];
    $_fundingSourceID = $thisItem['FundingSourceID'];
    $_fundingSourceName = $thisItem['FundingSourceName'];
    $_expectedQty = $thisItem['ExpectedQty'];
    $_stocktakeQty = $thisItem['StocktakeQty'];
    $_pendingStocktakeQty = (int)$_expectedQty - (int)$_stocktakeQty;
    $_itemTypeClass = ($_itemType == ITEM_TYPE_BULK) ? ' bulk-item' : ''; 
    
?>

				<li class="item">
					<div class="itemTitle"><?php echo $_itemName;?></div>
					<div class="info"><span class="label"><?php echo $i_InventorySystem_Item_Barcode;?> : </span><?php echo $_barcode;?></div>
<?php if ($_showRecordStatus):?>
	<div class="info"><span class="label"><?php echo $i_InventorySystem_Status;?> : </span><?php echo $_recordStatus;?></div>
<?php endif;?>
<?php if ($_itemType == ITEM_TYPE_BULK):?>
	<?php 
// 	       if (count($stocktakeNotDoneByLocationAssoc[$_itemID][$_groupInChargeID]) > 1) {
// 	           $showFundingSource = 1;
// 	       }
// 	       else {
// 	           $showFundingSource = 0;
// 	       }
	?>
	<?php //if ($showFundingSource):?>
			<div class="info"><span class="label"><?php echo $i_InventorySystem_Item_Funding;?> : </span><?php echo $_fundingSourceName;?></div>
	<?php //endif;?>
					<div class="info">
						<div class="row">
							<span class="col-xs-6"><span class="label"><?php echo $Lang['eInventory']['eClassApp']['Expected'];?> : </span><?php echo $_expectedQty;?></span>
							<a href="#" class="bulkItem" data-ItemID="<?php echo $_itemID;?>" data-FundingSourceID="<?php echo $_fundingSourceID;?>" data-ShowFundingSource="<?php //echo $showFundingSource;?>">
							<span class="stocktakeQuantity col-xs-6"><span class="label"><?php echo $Lang['eInventory']['eClassApp']['StockTake'];?> : </span><?php echo $_pendingStocktakeQty;?> <i class="fas fa-pen"></i></span>
							</a>
						</div>
					</div>
<?php endif;?>										
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input<?php echo $_itemTypeClass;?>" type="checkbox" name="ItemIDTypeFunding[]" value="<?php echo $_itemID.'-'.$_itemType.'-'.$_fundingSourceID;?>" data-ItemType="<?php echo $_itemType;?>">
							<span>&nbsp;</span>
						</label>
					</div>
				</li>
<?php endfor;?>
			</ul>
			
			<div id="setProcessArea" class="single">
				<div class="submitBtn col-xs-6">
					<a class="btn btn-primary" role="button" id="NotFoundBtn">
						<span><?php echo $Lang['eInventory']['eClassApp']['StockTakeNotFoundBtn'];?></span>
					</a>
				</div>
				<div class="submitBtn col-xs-6">
					<a class="btn btn-primary" role="button" id="StocktakeBtn">
						<span><?php echo $Lang['eInventory']['eClassApp']['AddToStockTakeBtn'];?></span>
					</a>
				</div>
			</div>
						
		</div>            
	</div>
	
	<input type="hidden" name="LocationID" value="<?php echo $locationID;?>">
	<input type="hidden" name="AdminGroupID" value="<?php echo $adminGroupID;?>">
	<input type="hidden" name="BulkItemID" id="BulkItemID" value="">
	<input type="hidden" name="BulkFundingSourceID" id="BulkFundingSourceID" value="">
	<input type="hidden" name="ShowFundingSource" id="ShowFundingSource" value="">
</form>
</body>
</html>

