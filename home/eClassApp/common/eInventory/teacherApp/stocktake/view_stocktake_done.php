<?php
/*
 *  2020-03-25 Cameron
 *      - change ul class from stocktakeList to stocktakeListDetail to hide ">" symbol
 *           
 *  2020-02-20 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$openCamera = IntegerSafe($_GET['OpenCamera']);

$adminGroupAry = $linventory->returnAdminGroup($adminGroupID);
$adminGroupName = (count($adminGroupAry) == 1) ? $adminGroupAry[0]['AdminGroupName'] : '';

$stocktakeProgressAssoc = $linventory->getStocktakeProgressByAdminGroup($adminGroupID,$locationID);
$roomAssoc = $stocktakeProgressAssoc['Room'];

// each loop has one record only
foreach ((array)$roomAssoc as $buildingID => $_roomAssoc) {
    foreach ((array)$_roomAssoc as $levelID => $__roomAssoc) {
        foreach ((array)$__roomAssoc as $_locationID => $___roomAssoc) {
            $locationAssoc = $___roomAssoc;
        }
    }
}

$thisStocktakeProgress = $linventory->getStocktakeProgressOfOneLocationUI($locationAssoc);

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");

?>
    <script>

        $(document).ready(function(){
		
            adjustContentHeight();
            
        });

        
        <?php echo $linventory->getJsAjaxError();?>

    </script>


<script type="text/javascript" src="../web/js/eInventory.js"></script>
<style>
    .stocktakeListDetail a, .stocktakeListDetail a:hover {cursor: default; text-decoration: none;}
</style>

<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="inventory edit navbar navbar-fixed-top">
        <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.stocktake_progress";?>"><i class="fas fa-arrow-left"></i></a></div></div>
        <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['StockTakeDetail'];?></div>
        <div id="button"></div>
    </nav>

    <div id="content" class="stocktaking">
        <div class="row">
            <div class="subTitle manGp subTitleFix col-xs-12">
                <div class="title"><?php echo $adminGroupName;?></div>
            </div>
        </div>
        <div class="contentFixHeight">
            <div class="processDetails">
            	<?php echo $thisStocktakeProgress;?>
            </div>
            <div class="subTitle stocktake">
                <div class="title"><?php echo $i_InventorySystem['StockTakeScope'];?></div>
            </div>
            <ul class="stocktakeListDetail">
            <?php echo $linventory->getStocktakeDoneByLocationUI($locationID, $adminGroupID);?>
            </ul>
        </div>
    </div>
</div>

</body>
</html>
