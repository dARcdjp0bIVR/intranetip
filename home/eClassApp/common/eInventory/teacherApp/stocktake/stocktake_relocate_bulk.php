<?php
/*
 *  2020-03-09 Cameron
 *      - fix character encoding problem (use ascii colon instead of Chinese) in DifferentLocationRemark
 *
 *  2020-01-23 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_GET['ItemID']);
$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$stocktakeLocationID = IntegerSafe($_GET['StocktakeLocationID']);
$itemPhoto = '';

$currentLocation = $linventory->returnLocationStr($stocktakeLocationID);

$itemDetailAry = $linventory->getBulkItemDetail($itemID, $thisLocationID='', $adminGroupID);

$nrItemDetail = count($itemDetailAry);
if ($nrItemDetail) {
    $itemDetail = $itemDetailAry[0];
    $itemName = Get_Lang_Selection($itemDetail['NameChi'], $itemDetail['NameEng']);
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = Get_Lang_Selection($itemDetail['DescriptionChi'], $itemDetail['DescriptionEng']);
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
    $itemCode = $itemDetail['ItemCode'];
    $itemBarcode = $itemDetail['Barcode'];
}


include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){

	$(document).on("click", "#btnClose", function(e) {
		e.preventDefault();
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + <?php echo $stocktakeLocationID;?> + "&AdminGroupID=" + <?php echo $adminGroupID;?>;
	});

	$(document).on("click", "#btnConfirmAndScan", function(e) {
		e.preventDefault();
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + <?php echo $stocktakeLocationID;?> + "&AdminGroupID=" + <?php echo $adminGroupID;?> + "&OpenCamera=1";
	});
	
	adjustStocktakeDetailHeight();

});

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="inventory navbar navbar-fixed-top edit">
            <div id="function"></div>
            <div class="headerTitle"><?php echo $Lang['eInventory']['eClassApp']['Error']['NotOriginalLocation'];?></div>
			<div id="button">
				<div class="headerIcon"><a href="" id="btnClose"><span class="close"></span></a></div>
			</div>
		</nav>
		<div id="content" class="stockingDetails error">
			<div class="row">
				<div class="subTitle map col-xs-12">
					<div class="title"><?php echo $currentLocation;?></div>
				</div>
			</div>

			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<div class="errorMsg location">
				<span class="text"><?php echo $Lang['eInventory']['eClassApp']['DifferentLocationRemark'];?> : </span>
            <?php 
                for($i=0; $i<$nrItemDetail;$i++) {
                    echo '<p class="text">' . $itemDetailAry[$i]['FullLocation'].'</p>';
                }
            ?>				
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			
			<ul class="itemInfo">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
					<span class="text"><?php echo $itemCode;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
					<span class="text"><?php echo $itemDescription;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
					<span class="text"><?php echo $itemBarcode;?></span>
				</li>
            </ul>
            
			<div id="setProcessArea" class="single">
				<div class="submitBtn col-xs-12">
					<a class="btn btn-primary" role="button" id="btnConfirmAndScan">
						<span><?php echo $Lang['eInventory']['eClassApp']['ConfirmAndScan'];?></span>
					</a>
				</div>
			</div>
            
		</div>            
	</div>
</body>
</html>

