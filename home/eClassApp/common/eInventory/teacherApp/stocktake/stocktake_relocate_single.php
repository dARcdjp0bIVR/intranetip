<?php
/*
 *  2020-03-20 Cameron
 *  - fix: $currentLocation should show the location in where to do stocktake but not the item original location
 *  - addSingleItemSurplus should also record the stocktake location instead of original location
 *  
 *  2020-03-10 Cameron
 *  - fix character encoding problem (use ascii colon instead of Chinese)
 *  
 *  2020-01-16 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$itemID = IntegerSafe($_GET['ItemID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$stocktakeLocationID = IntegerSafe($_GET['StocktakeLocationID']);
$itemPhoto = '';

$currentLocation = $linventory->returnLocationStr($stocktakeLocationID);

$itemDetail = $linventory->getSingleItemDetail($itemID);
if (count($itemDetail)) {
    $itemName = Get_Lang_Selection($itemDetail['NameChi'], $itemDetail['NameEng']);
    $itemName = $itemName ? $itemName : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemDescription = Get_Lang_Selection($itemDetail['DescriptionChi'], $itemDetail['DescriptionEng']);
    $itemDescription = $itemDescription ? $itemDescription : $Lang['eInventory']['eClassApp']['EmptySymbol'];
    $itemPhoto = $itemDetail['PhotoName'] ? $PATH_WRT_ROOT.$itemDetail['PhotoPath'].'/'.$itemDetail['PhotoName'] : '';
}


include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");
?>

<script>
$(document).ready(function(){

	$(document).on('click', '#btnClose', function(e) {
		e.preventDefault();
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + <?php echo $stocktakeLocationID;?> + "&AdminGroupID=" + <?php echo $adminGroupID;?>;
	});
	
    $(document).on('click', '#btnConfirmAndScan', function(e) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=teacherApp.ajax',
            data : {
                'action': 'addSingleItemSurplus',
                'ItemID': '<?php echo $itemID;?>',
                'LocationID': '<?php echo $stocktakeLocationID;?>',
                'AdminGroupID': '<?php echo $adminGroupID;?>'
            },
            success: backToStocktake,
            error: show_ajax_error
        });
    });
	
	adjustStocktakeDetailHeight();

});

function backToStocktake(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		window.location.href = "?task=teacherApp.stocktake.stocktake&LocationID=" + "<?php echo $stocktakeLocationID;?>" + "&AdminGroupID=" + "<?php echo $adminGroupID;?>" + "&OpenCamera=1";
	}	
	else {
//		alert('Error on adding surplus record');
	}
}

<?php echo $linventory->getJsAjaxError();?>

</script>
<script type="text/javascript" src="../web/js/eInventory.js"></script>

<body>
    <div id="wrapper">
        <!-- Header -->
        <nav id="header" class="inventory navbar navbar-fixed-top edit">
            <div id="function"></div>
            <div class="headerTitle"><?php echo $Lang['eInventory']['eClassApp']['AddToStockTake'];?></div>
			<div id="button">
				<div class="headerIcon"><a href="" id="btnClose"><span class="close"></span></a></div>
			</div>
		</nav>
		<div id="content" class="stockingDetails error">
			<div class="row">
				<div class="subTitle map col-xs-12">
					<div class="title"><?php echo $currentLocation;?></div>
				</div>
			</div>

			<div class="itemImg">
<?php if ($itemPhoto):?>			
				<img src="<?php echo $itemPhoto;?>" alt="<?php echo $itemName;?>" />
<?php endif;?>				
			</div>
			<div class="errorMsg location">
				<span class="text"><?php echo $Lang['eInventory']['eClassApp']['DifferentLocationRemark'];?> : </span>
				<p class="text"><?php echo $itemDetail['FullLocation'];?></p>
			</div>
			<span class="itemTitle"><?php echo $itemName;?></span>
			
			<ul class="itemInfo">
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Code;?></span>
					<span class="text"><?php echo $itemDetail['ItemCode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Description;?></span>
					<span class="text"><?php echo $itemDescription;?></span>
				</li>
				<li>
					<span class="label"><?php echo $i_InventorySystem_Item_Barcode;?></span>
					<span class="text"><?php echo $itemDetail['Barcode'];?></span>
				</li>
				<li>
					<span class="label"><?php echo $Lang['General']['Status2'];?></span>
					<span class="text"><?php echo $itemDetail['Status'];?></span>
				</li>
            </ul>
            
			<div id="setProcessArea" class="single">
<!-- 			
				<div class="submitBtn col-xs-4">
					<a class="btn btn-primary second" role="button" id="btnConfirm">
						<span><?php echo $Lang['Btn']['Confirm'];?></span>
					</a>
				</div>
 -->				
				<div class="submitBtn col-xs-12">
					<a class="btn btn-primary" role="button" id="btnConfirmAndScan">
						<span><?php echo $Lang['eInventory']['eClassApp']['ConfirmAndScan'];?></span>
					</a>
				</div>
			</div>
            
		</div>            
	</div>
</body>
</html>

