<?php
/*
 * 	Using:
 *
 *  Note:   1. must use https to support barcode scan
 *          2. for iOS, version should be at least 13
 *
 *	2020-10-21 Cameron
 *		- fix: don't allow to open camera for iOS < 13 [case #B199460]
 *
 *  2020-03-24 Cameron
 *      - check if user has AdminGroupID access right or not from $_GET
 *      - show AdminGroupName for bulk item in showLastScannedItem() 
 *      
 *  2020-03-23 Cameron
 *      - don't show ">" in lastScannedItem area when last scanned item hasn't detected or number or records is more than one (bulk items with different funding source)
 *      - keep to show ">" if there's one record detected and can be click to view details 
 *  
 *  2020-03-20 Cameron
 *      - modify addStocktakeResult() to remove old item before append new item (e.g. item not found before)
 *      - relocate for signle item should pass the stocktake $adminGroupID instead of Item's AdminGroupID
 *       
 *  2020-03-17 Cameron
 *      - use error layer to show error message instead of showing in last scanned area
 *  
 *  2020-02-18 Cameron
 *      - always show funding source for bulk item
 *      - show modal box to ask user whether to do stock take or not in next location when finish stock take in current location
 *      
 *  2019-12-18 Cameron
 *      - create this file
 */


if (!$indexVar) {
    header('location: ../');
}

$linventory = $indexVar['linventory'];

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

$locationID = IntegerSafe($_GET['LocationID']);
$adminGroupID = IntegerSafe($_GET['AdminGroupID']);
$openCamera = IntegerSafe($_GET['OpenCamera']);

if (!$linventory->IS_ADMIN_USER()) {
    $adminGroupStr = $linventory->getInventoryAdminGroup();
    $adminGroupIDOfTheUserAry = explode(',',$adminGroupStr);

    if (in_array($adminGroupID,(array)$adminGroupIDOfTheUserAry)) {
        // do nothing
    }
    else if (in_array($AdminGroupID,(array)$adminGroupIDOfTheUserAry)) {
        $adminGroupID = $AdminGroupID;      // get from cookie, case when user has multiple AdminGroup access right and select one to scan
    }
    else if (count($adminGroupIDOfTheUserAry)){
        $adminGroupID = $adminGroupIDOfTheUserAry[0];       // user does not have access right to the AdminGroupID from $_GET, use the first Group that the user has access right 
    }
    else {      // not belong to any AdminGroup
        header('location: ../');
    }
}

$isValidVersion = $linventory->checkiOSVersion();

$adminGroupAry = $linventory->returnAdminGroup($adminGroupID);
$adminGroupName = (count($adminGroupAry) == 1) ? $adminGroupAry[0]['AdminGroupName'] : '';

$stocktakeProgressAssoc = $linventory->getStocktakeProgressByAdminGroup($adminGroupID,$locationID);
//debug_pr($stocktakeProgressAssoc);
$roomAssoc = $stocktakeProgressAssoc['Room'];
// debug_pr($roomAssoc);
// exit;
// each loop has one record only
foreach ((array)$roomAssoc as $buildingID => $_roomAssoc) {
    foreach ((array)$_roomAssoc as $levelID => $__roomAssoc) {
        foreach ((array)$__roomAssoc as $_locationID => $___roomAssoc) {
            $locationAssoc = $___roomAssoc;
        }
    }
}

$thisStocktakeProgress = $linventory->getStocktakeProgressOfOneLocationUI($locationAssoc);

include($PATH_WRT_ROOT."home/eClassApp/common/eInventory/header.php");

?>
    <script>

        $(document).ready(function(){
            $(document).on('click', 'div.selectItem a', function(e) {
                if (parseInt($('span#NumberIncomplete').html())>0) {
                    var locationID = $(this).attr('data-LocationID');
                    var adminGroupID = $(this).attr('data-AdminGroupID');
                    window.location.href = "?task=teacherApp.stocktake.stocktake_by_selection&LocationID=" + locationID + "&AdminGroupID=" + adminGroupID;
                }
            });

            $(document).on('click', 'ul.stocktakeList a, #lastScannedItem a', function(e) {
                var itemID = $(this).attr('data-ItemID');
                var itemType = $(this).attr('data-ItemType');   
                var locationID = $(this).attr('data-LocationID');
                if (itemType == 2) {
                	var adminGroupID = $(this).attr('data-AdminGroupID');
                	var fundingSourceID = $(this).attr('data-FundingSourceID');
                }
                else {
                	var adminGroupID = '';
                	var fundingSourceID = '';
                }
               	viewItemDetails(itemID, itemType, locationID, adminGroupID, fundingSourceID);
            });

            $(document).on('click', '#camModal .itemInfoWithoutAfter', function(e) {
				var thisClass = $(this).attr('class');
                if (thisClass.indexOf('itemInfo') != -1){
					var child = $(this).find('a[href="#"]');
					
					if (child.length == 1) {
                        var itemID = child.attr('data-ItemID');
                        var itemType = child.attr('data-ItemType');   
                        var locationID = child.attr('data-LocationID');
                        if (itemType == 2) {
                        	var adminGroupID = child.attr('data-AdminGroupID');
                        	var fundingSourceID = child.attr('data-FundingSourceID');
                        }
                        else {
                        	var adminGroupID = '';
                        	var fundingSourceID = '';
                        }
                       	viewItemDetails(itemID, itemType, locationID, adminGroupID, fundingSourceID);
					}
					else {
						return;
					}
                }
            });            

            $(document).on('keyup', '#inputBarcode', function(e) {
            	if (location.protocol == 'https:') {
            		if ($.trim($(this).val()) != '') {	
                        $('#cameraBtn').css('display','none');
                        $('#submitBtn').css('display','');
            		}
            		else {
                        $('#cameraBtn').css('display','');
                        $('#submitBtn').css('display','none');
            		}
            	}
            });

            $(document).on('click', '#submitBtn', function(e) {
                var barcode = $.trim($('#inputBarcode').val()); 
            	if (barcode != '') {
        			addStocktake(barcode, 'input');
            	}
            	else {
					alert('<?php echo $Lang['eInventory']['eClassApp']['Warning']['PleaseInputBarcode'];?>');
            	}
            });

            $(document).on('click', '#yesBtn', function(e) {
            	window.location.href = "?task=teacherApp.stocktake.stocktake_progress&ShowFilterResult=1";
            });

            $(document).on('click', '#noBtn', function(e) {
            	$('#finished').modal('hide');
            });
            
        <?php //if ($openCamera): ?>
        	//$('cameraBtn').click();		// not work        
        <?php // endif;?>
            
		<?php if ($locationAssoc['NumberIncomplete'] == 0): ?>
			$('#finished').modal('show');
		<?php endif;?>
		
            adjustContentHeight();
            
        });

        // method: (scan, input) 
        function addStocktake(barcode, method)
        {
            var adminGroupID = '<?php echo $adminGroupID;?>';

            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'checkItemByBarcode',
                    'barcode': barcode,
                    'adminGroupID': '<?php echo $adminGroupID;?>',
                    'locationID': '<?php echo $locationID;?>'
                },
                success: function(ajaxReturn){
                	if (ajaxReturn != null && ajaxReturn.success){

//                 		if (method == 'scan') {
// 							$('#lastScannedItem').html('');		// empty message first, but it'll blink the result if camera always focus on the barcode
// 						}
                    	
                    	var updateProgress = true;
                    	if (!ajaxReturn.IsItemFound) {
							if (method == 'scan') {
								var error = '<span class="errorMsg"><?php echo $Lang['eInventory']['eClassApp']['Warning']['BarcodeNotFound'];?></span>';
								$('#lastScannedItem').html('').parent().css('display','none');
								$('#errorInfo').html(error).css('display','');
							}
							else {
								alert('<?php echo $Lang['eInventory']['eClassApp']['Warning']['BarcodeNotFound'];?>');
							}
                    	}
                    	else if (ajaxReturn.ItemType == '2' && (!ajaxReturn.IsLocationFound || (!ajaxReturn.MatchBoth && ajaxReturn.IsLocationFound && ajaxReturn.IsAdminGroupFound))) {
    			            $.ajax({
    			                dataType: "json",
    			                type: "POST",
    			                url: '?task=teacherApp.ajax',
    			                data : {
    			                    'action': 'getMsgLocationName',
    			                    'locationID': ajaxReturn.LocationID
    			                },
    			                success: function(ajaxReturn2){
    			                	if (ajaxReturn2 != null && ajaxReturn2.success){
        								if (method == 'scan') {
        									var error = '<span class="errorMsg">' + ajaxReturn2.message + '</span>';
        									$('#lastScannedItem').html('').parent().css('display','none');
        									$('#errorInfo').html(error).css('display','');
        								}
        								else {
        									alert(ajaxReturn2.message);		// PleaseDoStockInThisLocation
        								}
    			                	}
    			                },
    			                error: show_ajax_error
    			            });							
						}                    	
						else if (ajaxReturn.IsAdminGroupFound) {
							// $locationID matches the location to do stocktake
							getItemByBarcode(barcode, adminGroupID, '<?php echo $locationID;?>', updateProgress, method);
						}
						else {
							adminGroupID = ajaxReturn.AdminGroupID;
    			            $.ajax({
    			                dataType: "json",
    			                type: "POST",
    			                url: '?task=teacherApp.ajax',
    			                data : {
    			                    'action': 'getMsgAdminGroupName',
    			                    'adminGroupID': adminGroupID
    			                },
    			                success: function(ajaxReturn2){
    			                	if (ajaxReturn2 != null && ajaxReturn2.success){
       			                		var confirm = window.confirm(ajaxReturn2.message);
       			                		if (confirm) {
    	   			                		// $locationID is the location to do stocktake
       			                			getItemByBarcode(barcode, adminGroupID, '<?php echo $locationID;?>', updateProgress, method);
       			                		}
    			                	}
    			                },
    			                error: show_ajax_error
    			            });
							updateProgress = false;
						}
                	}
                },
                error: show_ajax_error
            });
            
        }

        function getItemByBarcode(barcode, adminGroupID, stocktakeLocationID, updateProgress, method)
        {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'getItemByBarcode',
                    'barcode': barcode,
                    'adminGroupID': adminGroupID,
                    'locationID': stocktakeLocationID
                },
                success: function(ajaxReturn){
                	if (ajaxReturn != null && ajaxReturn.success){                    
                        if (ajaxReturn.Items.length>0) {
                   			showLastScannedItem(ajaxReturn, updateProgress, stocktakeLocationID, method);
                        }
                        else {
                            if (method == 'scan') {
    							var error = '<span class="errorMsg"><?php echo $Lang['eInventory']['eClassApp']['Warning']['NotNeedStocktake'];?></span>';
								$('#lastScannedItem').html('').parent().css('display','none');
								$('#errorInfo').html(error).css('display','');
    						}
    						else {
    							alert('<?php echo $Lang['eInventory']['eClassApp']['Warning']['NotNeedStocktake'];?>');
    						}
                        }
                	}
                },
                error: show_ajax_error
            });
        }

	    // locationID is the location to do stocktake
        function showLastScannedItem(ajaxReturn, updateProgress, stocktakeLocationID, method)
        {
            if (ajaxReturn != null && ajaxReturn.success){
                var itemLayout = '';
//                var showFundingSource = (ajaxReturn.Items.length > 1) ? 1 : 0;
				var showFundingSource = 1;
				var itemName = '';
				var doneStocktakeItem = '';
                for(var i=0; i<ajaxReturn.Items.length; i++) {
                	itemName = ajaxReturn.Items[i].ItemName;
                    if (ajaxReturn.Items[i].ItemType == '1') {
                    	if (ajaxReturn.Items[i].IsDoneStocktake == '1') {
                    		doneStocktakeItem += itemName;
							itemName += ' ['+'<?php echo $Lang['eInventory']['eClassApp']['HasDoneStocktake'];?>'+']';
                    	}
                    	itemLayout += '<a href="#" data-ItemID="'+ ajaxReturn.Items[i].ItemID +'" data-ItemType="'+ ajaxReturn.Items[i].ItemType +'" data-LocationID="'+ ajaxReturn.Items[i].LocationID +'" data-AdminGroupID="'+ ajaxReturn.Items[i].AdminGroupID +'">' + itemName + '</a>';

                    	if (ajaxReturn.Items[i].IsDoneStocktake == '0') {
                    		doStocktake(ajaxReturn.Items[i].ItemID, ajaxReturn.Items[i].ItemType, ajaxReturn.Items[i].LocationID, ajaxReturn.Items[i].AdminGroupID, updateProgress, '', stocktakeLocationID);
                    	}
                    }
                    else {
                        if (i>0) {
							itemLayout += '<br>';
                        }

                        itemName += ' (';
//                        if (ajaxReturn.Items[i].AdminGroupID != '<?php echo $adminGroupID;?>') {
// 							itemName += '' + ajaxReturn.Items[i].AdminGroupName + ' - ';
//                         }
                        
//                        if (showFundingSource == 1) {
							itemName += ajaxReturn.Items[i].FundingSourceName; 
//                        }

						itemName += ')';
                    	if (ajaxReturn.Items[i].IsDoneStocktake == '1') {
                        	if (i>0) {
                        		doneStocktakeItem += ',';
                        	}
                    		doneStocktakeItem += itemName;
							itemName += ' ['+'<?php echo $Lang['eInventory']['eClassApp']['HasDoneStocktake'];?>'+']';
                    	}
                        
                    	itemLayout += '<a href="#" data-ItemID="'+ ajaxReturn.Items[i].ItemID +'" data-ItemType="'+ ajaxReturn.Items[i].ItemType +'" data-LocationID="'+ ajaxReturn.Items[i].LocationID +'" data-AdminGroupID="'+ ajaxReturn.Items[i].AdminGroupID + '" data-FundingSourceID="'+ ajaxReturn.Items[i].FundingSourceID +'" data-ShowFundingSource="'+ showFundingSource +'">' + itemName;
                    	itemLayout += '<br><span style="font-size:smaller;">' + '<?php echo $Lang['eInventory']['eClassApp']['Info']['BulkItemQty'];?> ' + ajaxReturn.Items[i].Quantity;
                    	itemLayout += '<br>' + '<?php echo $Lang['eInventory']['eClassApp']['Info']['EditQty'];?>' + '</span>';
                    	itemLayout += '</a>';

                    	if (ajaxReturn.Items[i].IsDoneStocktake == '0') {
                    		doStocktake(ajaxReturn.Items[i].ItemID, ajaxReturn.Items[i].ItemType, ajaxReturn.Items[i].LocationID, ajaxReturn.Items[i].AdminGroupID, updateProgress, ajaxReturn.Items[i].FundingSourceID, stocktakeLocationID);
                    	}
                    }
                	
                }
                if (method == 'scan') {
                	$('#errorInfo').html('').css('display','none');
                	if (ajaxReturn.Items.length == 1) {
                		$('#camModal .itemInfoWithoutAfter').addClass('itemInfo');		// show symbol ">" if there's only one item
                	}
                	else if (ajaxReturn.Items.length > 1) {
                		$('#camModal .itemInfoWithoutAfter').removeClass('itemInfo');
                	}
					$('#lastScannedItem').html(itemLayout).parent().css('display','');
                }
                else {
					if (doneStocktakeItem != '') {
						alert('<?php echo $Lang['eInventory']['eClassApp']['Warning']['HasDoneStocktake'];?>' + ': ' + doneStocktakeItem );
					}
                }

            }

        }

        function doStocktake(itemID, itemType, itemLocationID, adminGroupID, updateProgress, fundingSourceID, stocktakeLocationID)
        {
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=teacherApp.ajax',
                data : {
                    'action': 'doStocktake',
                    'ItemID': itemID,
                    'ItemType': itemType,
                    'ItemLocationID': itemLocationID,
                    'AdminGroupID': adminGroupID,
                    'FundingSourceID': fundingSourceID,
                    'StocktakeLocationID': stocktakeLocationID,
                },
                success: function(ajaxReturn) {
                	addStocktakeResult(ajaxReturn, itemID, itemLocationID, adminGroupID, itemType, updateProgress, stocktakeLocationID);
                },
                error: show_ajax_error
            });
        }

        function addStocktakeResult(ajaxReturn, itemID, itemLocationID, adminGroupID, itemType, updateProgress, stocktakeLocationID)
        {
        	if (ajaxReturn != null && ajaxReturn.success && ajaxReturn.StocktakeResult){
                if (ajaxReturn.StocktakeResult == 'relocate') {
					if (itemType == 1) { 
                		window.location.href = "?task=teacherApp.stocktake.stocktake_relocate_single&ItemID=" + itemID + "&AdminGroupID=<?php echo $adminGroupID;?>&StocktakeLocationID=" + stocktakeLocationID;
//                		window.open("?task=teacherApp.stocktake_relocate_single&ItemID=" + itemID + "&LocationID=" + itemLocationID + "&AdminGroupID=" + adminGroupID, '_blank');
					}
					else {
						window.location.href = "?task=teacherApp.stocktake.stocktake_relocate_bulk&ItemID=" + itemID + "&LocationID=" + itemLocationID + "&AdminGroupID=" + adminGroupID + "&StocktakeLocationID=" + stocktakeLocationID;
					}
                }
                else {
                    if (updateProgress) {
                    	$('#BarcodeList').html('');
                    	$("ul.stocktakeList").find('a[data-itemid=' + itemID + ']').remove();	// remove old item (e.g. not found before)
                    	$("ul.stocktakeList").prepend(ajaxReturn.stocktakeResultLayout);
                    	$("div.processDetails").html(ajaxReturn.stocktakeProgressLayout);
                    	$("span#NumberIncomplete").html(ajaxReturn.numberIncomplete);
                    	if ($('#StocktakeInstruction').length > 0) {
                    		$('button.close').click();
                    	}
                    	if (ajaxReturn.numberIncomplete == 0) {
                    		$('#finished').modal('show');
	                	}
                    }
                }
        	}
        }

        function viewItemDetails(itemID, itemType, locationID, adminGroupID, fundingSourceID)
        {
            if (itemType == 1) {             
            	window.location.href = "?task=teacherApp.stocktake.view_single_item&ItemID=" + itemID + "&LocationID=" + locationID + "&AdminGroupID=" + "<?php echo $adminGroupID;?>";
            }
            else {
            	window.location.href = "?task=teacherApp.stocktake.view_bulk_item&ItemID=" + itemID + "&LocationID=" + locationID + "&AdminGroupID=" + adminGroupID + "&FundingSourceID=" + fundingSourceID+ "&StocktakeAdminGroupID=" + "<?php echo $adminGroupID;?>";
            }
        }
        
        <?php echo $linventory->getJsAjaxError();?>

/*
        var metas = document.getElementsByTagName('meta');
        var i;
        if (navigator.userAgent.match(/iPhone/i)) {
            for (i=0; i<metas.length; i++) {
                if (metas[i].name == "viewport") {
                    metas[i].content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                }
            }
            document.addEventListener("gesturestart", gestureStart, false);
        }
        function gestureStart() {
            for (i=0; i<metas.length; i++) {
                if (metas[i].name == "viewport") {
                    metas[i].content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";
                }
            }
        }
*/
    </script>


<script type="text/javascript" src="../web/js/eInventory.js"></script>


<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="inventory edit navbar navbar-fixed-top">
        <div id="function"><div class="headerIcon"><a href="<?php echo "?task=teacherApp.stocktake.stocktake_progress";?>"><i class="fas fa-arrow-left"></i></a></div></div>
        <div class="headerTitle withFunction"><?php echo $Lang['eInventory']['eClassApp']['StockTake'];?></div>
        <div id="button"></div>
    </nav>

    <div id="content" class="stocktaking">
        <div class="row">
            <div class="subTitle manGp subTitleFix col-xs-12">
                <div class="title"><?php echo $adminGroupName;?></div>
            </div>
        </div>
        <div class="contentFixHeight">
            <div class="processDetails">
            	<?php echo $thisStocktakeProgress;?>
            </div>
            <div class="subTitle stocktake">
                <div class="title"><?php echo $i_InventorySystem['StockTakeScope'];?></div>
            </div>
            <ul class="stocktakeList">
            <?php echo $linventory->getStocktakeDoneByLocationUI($locationID, $adminGroupID);?>
            </ul>
            
            <!-- Camera scanning modal -->
        	<div class="modal fade" id="camModal" tabindex="-1" role="dialog" aria-labelledby="camModal" aria-hidden="true">
        		<div class="modal-dialog" role="document">
        			<div class="modal-content">
        				<div class="modal-header">
        					<a class="back" data-dismiss="modal" aria-label="Close" id="modalBackBtn">
        						<i class="fas fa-arrow-left"></i>
        					</a>
        					<h5 class="modal-title" id="exampleModalLabel"><?php echo $Lang['eInventory']['eClassApp']['ScanBarcode'];?></h5>
        				</div>
        				<div class="modal-body">
         					<div><video id="camera" style="background: #fff; display: block;"></video></div>
        					<div class="note"><?php echo $Lang['eInventory']['eClassApp']['AdjustBarcodeToMatchScreen'];?></div>
        				</div>
<!--     
                <div id="sourceSelectPanel" style="display:none">
                    <label for="sourceSelect">Change video source:</label>
                    <select id="sourceSelect" style="max-width:400px">
                    </select>
                </div>
 -->
        				<a href="#" class="itemInfoWithoutAfter">
        					<div class="itemTitle" id="lastScannedItem"></div>
        				</a>
        				
        				<div id="errorInfo" class="errorInfo" style="display:none"></div>
        			</div>
        		</div>
        	</div>
 
            <div id="stocktakeArea">

                <div class="selectItem">
                    <a href="#" data-LocationID="<?php echo $locationID;?>" data-AdminGroupID="<?php echo $adminGroupID;?>">
                        <span class="icon"></span>
                        <span class="number"><span class="text" id="NumberIncomplete"><?php echo $locationAssoc['NumberIncomplete'];?><?php if ($locationAssoc['NumberIncomplete'] > 99) echo '+';?></span></span>
                    </a>
                </div>

                <div class="inputBarcode">
                    <div class="form-check form-check-inline">
                        <div id="barcodeField" class="fieldInput">
                            <input type="text" class="form-control inputField" id="inputBarcode" placeholder="<?php echo $Lang['eInventory']['eClassApp']['InputBarcode'];?>" value="">
                        </div>
                    </div>
<?php if (checkHttpsWebProtocol() && $isValidVersion):?>
                    <a class="btn cameraOn" id="cameraBtn" data-toggle="modal" data-target="#camModal" style="display:'';"></a>
                    <a class="btn submit" id="submitBtn" style="display:none;"><i class="fas fa-check"></i></a>
<?php else:?>
                    <a class="btn submit" id="submitBtn"><i class="fas fa-check"></i></a>
<?php endif;?>
                    
                </div>

            </div>
        </div>
    </div>
</div>

<!--  finish sotck tabke modal -->
<div class="modal fade" id="finished" tabindex="-1" role="dialog" aria-labelledby="finished" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modelLabel"><?php echo $Lang['eInventory']['eClassApp']['FinishStocktake'];?></h5>
			</div>
			<div class="modal-body">
				<img src="../../common/web/images/einventory/eInventory_finishedStocktake.png" alt="Stocktake finished" height="125" />
				<p><?php echo sprintf($Lang['eInventory']['eClassApp']['StockTakeNextLocation'], $locationAssoc['LocationName']);?></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal" id="noBtn"><?php echo $Lang['General']['No'];?></button>
				<a href="#"><button type="button" class="btn" id="yesBtn"><?php echo $Lang['General']['Yes'];?></button></a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="../web/js/zxing.min.js"></script>
<script type="text/javascript">
if (location.protocol == 'https:') {
	
    window.addEventListener('load', function () {
        let selectedDeviceId;
        const codeReader = new ZXing.BrowserMultiFormatReader()
//         console.log('ZXing code reader initialized')
		var cameraStatus = 'off';
        codeReader.getVideoInputDevices()
            .then((videoInputDevices) => {
                const sourceSelect = document.getElementById('sourceSelect')
                selectedDeviceId = videoInputDevices[0].deviceId
// console.log(selectedDeviceId);
// console.log(videoInputDevices);
// alert(videoInputDevices[0].label);
// alert(videoInputDevices.length);
                if (videoInputDevices.length > 1) {
                    var foundCamera = false;
                     videoInputDevices.forEach((element) => {
//                         const sourceOption = document.createElement('option')
//                         sourceOption.text = element.label
//                         sourceOption.value = element.deviceId
//                         sourceSelect.appendChild(sourceOption)
                        deviceName = element.label;
                        deviceName = deviceName.toLowerCase();
//  console.log(deviceName);
//  console.log(element.deviceId);
                        if (deviceName.indexOf('back') != -1 && !foundCamera) {
                        	selectedDeviceId = element.deviceId;
                        	foundCamera = true;
                        }

                     })
                     if (!foundCamera) {
                    	 selectedDeviceId = videoInputDevices[1].deviceId;	// use back camera as default if there're multiple
                     }
//                     sourceSelect.onchange = () => {
//                         selectedDeviceId = sourceSelect.value;
//                     };
//                     const sourceSelectPanel = document.getElementById('sourceSelectPanel')
//                     sourceSelectPanel.style.display = 'block'
                }
                document.getElementById('cameraBtn').addEventListener('click', () => {
//alert('click);                    
                    $('button.close').click();		// close hints
                    $('#camModal').css('display','block');
                    $('#lastScannedItem').html('');
                    $('#errorInfo').html('');
                    codeReader.decodeFromVideoDevice(selectedDeviceId, 'camera', (result, err) => {
                        if (result) {
//                            alert('result');
//	                        console.log(result)
//                            document.getElementById('BarcodeList').textContent = result.text
							addStocktake(result.text, 'scan');
                        }
                        if (err && !(err instanceof ZXing.NotFoundException)) {
//                            alert('error');
//                            console.error(err)
                            document.getElementById('lastScannedItem').html(err);
                        }
                    })
//                    	console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
                })
                document.getElementById('modalBackBtn').addEventListener('click', () => {
                    codeReader.reset()
//                    document.getElementById('BarcodeList').textContent = '';
                    $('#camModal').css('display','none');
//                    console.log('Reset.')
                })
            })
            .catch((err) => {
                console.error(err)
            })
    })
    
}
</script>

</body>
</html>
