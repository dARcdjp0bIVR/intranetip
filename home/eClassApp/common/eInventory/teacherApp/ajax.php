<?
/*
 * Modifying: 
 * 
 * Log
 *
 * Description: output json format data
 *
 * 2020-04-09 [Cameron]
 *      - fix garbled Chinese character when update remark field for ej php < 5.4
 *      
 * 2020-03-23 [Cameron]
 *      - fix: apply intranet_undo_htmlspecialchars to getItemByBarcode() for javascript to display alert message if the item has been done stocktake
 *      
 * 2020-03-12 [Cameron]
 *      - pass argument $showNoRecord to getItemListUI()
 *       
 * 2020-03-09 [Cameron] 
 *      - pass argument $WithOthersOpt=false to Get_Floor_Selection() so that it's consistent with that in ej
 *      - handle character encoding problem in ej
 *      
 * 2019-12-16 [Cameron] create this file
 */

$characterset = 'utf-8';

if (!$indexVar) {
    header('location: ../');
}

if (!$_SESSION["SSV_PRIVILEGE"]["eClassApp"]["eInventory"]) {
    No_Access_Right_Pop_Up();
    exit;
}

header('Content-Type: text/html; charset=' . $characterset);

if (class_exists('JSON_obj')) {
    $ljson = new JSON_obj();
}
else {
    return;
}

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5-hkscs';
    }
}
else {
    $characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$json['success'] = false;

$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];

$linventory = $indexVar['linventory'];
$llocation_ui = $indexVar['location_ui'];

switch ($action) {

    case 'getLocationByFloor':
        $floorID = IntegerSafe($_POST['floorID']);
        $locationSelection = $linventory->getRoomSelectionUI($floorID);
        $json['LocationSelection'] = $locationSelection;
        $json['success'] = true;
        break;
       
    case 'getStocktakeProgress':
        $filterBy = IntegerSafe($_POST['filterBy']);
        $adminGroupID = IntegerSafe($_POST['adminGroupID']);
        $floorID = IntegerSafe($_POST['floorID']);
        $locationID = IntegerSafe($_POST['locationID']);

        # set cookies
        $FilterBy = $filterBy;
        $AdminGroupID = $adminGroupID;
        $TargetFloor = $floorID;
        $TargetLocation = $locationID;
        $arrCookies[] = array("ck_FilterBy", "FilterBy");
        $arrCookies[] = array("ck_AdminGroupID", "AdminGroupID");
        $arrCookies[] = array("ck_TargetFloor", "TargetFloor");
        $arrCookies[] = array("ck_TargetLocation", "TargetLocation");
        updateGetCookies($arrCookies);
        
        if ($filterBy == 1) {       // by location
            $stocktakeProgress = $linventory->getStocktakeProgressByLocationUI($locationID);
        }
        else {
            $stocktakeProgress = $linventory->getStocktakeProgressByAdminGroupUI($adminGroupID);
        }
        
        $json['StocktakeProgress'] = $stocktakeProgress;
        $json['success'] = true;
        break;

    case 'checkItemByBarcode':
        $barcode = trim($_POST['barcode']);
        $adminGroupID = IntegerSafe($_POST['adminGroupID']);
        $locationID = IntegerSafe($_POST['locationID']);
        $checkAry = $linventory->checkItemAdminGroupAndLocationByBarcode($barcode, $adminGroupID, $locationID);
        $json['IsItemFound'] = $checkAry['IsItemFound'];
        $json['IsAdminGroupFound'] = $checkAry['IsAdminGroupFound'];
        $json['IsLocationFound'] = $checkAry['IsLocationFound'];
        $json['MatchBoth'] = $checkAry['MatchBoth'];
        $json['ItemType'] = $checkAry['ItemType'];
        $json['LocationID'] = $checkAry['LocationID'];
        $json['AdminGroupID'] = $checkAry['AdminGroupID'];
        $json['success'] = true;
        break;
        
    case 'getItemByBarcode':
        $barcode = trim($_POST['barcode']);
        $adminGroupID = IntegerSafe($_POST['adminGroupID']);
        $locationID = IntegerSafe($_POST['locationID']);
        $adminGroupIDAry = explode(',',$adminGroupID);
        $itemAry = $linventory->getItemByBarcode($barcode, $adminGroupIDAry, $locationID);
        if (count($itemAry)) {
            for($i=0,$iMax=count($itemAry); $i<$iMax; $i++) {
                $itemAry[$i]['ItemName'] = intranet_undo_htmlspecialchars($itemAry[$i]['ItemName']);
                $itemAry[$i]['FundingSourceName'] = intranet_undo_htmlspecialchars($itemAry[$i]['FundingSourceName']);
            }
        }
        $json['Items'] = $itemAry;
        $json['success'] = true;
        break;

    case 'doStocktake':
        $itemID = IntegerSafe($_POST['ItemID']);
        $itemType = IntegerSafe($_POST['ItemType']);
        $itemLocationID = IntegerSafe($_POST['ItemLocationID']);
        $adminGroupID = IntegerSafe($_POST['AdminGroupID']);
        $fundingSourceID = IntegerSafe($_POST['FundingSourceID']);
        $stocktakeLocationID = IntegerSafe($_POST['StocktakeLocationID']);
        $stocktakeStatus = $linventory->getStocktakeStatus($itemID, $itemType, $itemLocationID, $adminGroupID, $fundingSourceID);
        
        if ($stocktakeStatus == ITEM_STOCKTAKE_STATUS_NOTDONE) {
            if ($linventory->isItemInLocation($itemID, $itemType, $stocktakeLocationID)) {
                $stocktakeResult = $linventory->doStocktake($itemID, $itemType, $itemLocationID, $adminGroupID, $fundingSourceID);
                if ($stocktakeResult) {
                    $json['stocktakeResultLayout'] = $linventory->getStocktakeDoneByLocationUI($itemLocationID, $adminGroupID, $fundingSourceID, $itemID, $itemType);
                    
                    $stocktakeProgressAssoc = $linventory->getStocktakeProgressByAdminGroup($adminGroupID,$itemLocationID);
                    $roomAssoc = $stocktakeProgressAssoc['Room'];
                    
                    // each loop has one record only
                    foreach ((array)$roomAssoc as $buildingID => $_roomAssoc) {
                        foreach ((array)$_roomAssoc as $levelID => $__roomAssoc) {
                            foreach ((array)$__roomAssoc as $_locationID => $___roomAssoc) {
                                $locationAssoc = $___roomAssoc;
                            }
                        }
                    }
                    $thisStocktakeProgress = $linventory->getStocktakeProgressOfOneLocationUI($locationAssoc);
                    
                    $json['stocktakeProgressLayout'] = $thisStocktakeProgress;
                    $numberIncomplete = $locationAssoc['NumberIncomplete'];
                    if ($numberIncomplete > 99) {
                        $numberIncomplete .= '+';
                    }
                    $json['numberIncomplete'] = $numberIncomplete;
                }
            }
            else {  // not in location,
                $stocktakeResult = "relocate";
            }
        }
        else {
            $stocktakeResult = false;
        }
        $json['StocktakeResult'] = $stocktakeResult;
        $json['success'] = true;
        break;
        
    case 'addSingleItemSurplus':
        $itemID = IntegerSafe($_POST['ItemID']);
        $locationID = IntegerSafe($_POST['LocationID']);
        $adminGroupID = IntegerSafe($_POST['AdminGroupID']);
        $result = $linventory->addSingleItemSurplus($itemID, $locationID, $adminGroupID);
        $json['success'] = $result;
        
        break;
        
    case 'updateSingleItemMissing':
        $itemID = IntegerSafe($_POST['ItemID']);
        $locationID = IntegerSafe($_POST['LocationID']);
        $adminGroupID = IntegerSafe($_POST['AdminGroupID']);
        $remark = standardizeFormPostValue($_POST['Remark']);
        if ($junior_mck) {
            $remark = convert2unicode($remark,1,0);     // convert to big5
        }
        $result = $linventory->updateSingleItemMissing($itemID, $locationID, $adminGroupID, $remark);
        $json['success'] = $result;
        break;
        
    case 'updateSingleItemRemark':
        $itemID = IntegerSafe($_POST['ItemID']);
        $remark = standardizeFormPostValue($_POST['Remark']);
        if ($junior_mck) {
            $remark = convert2unicode($remark,1,0);     // convert to big5
        }
        $result = $linventory->updateSingleItemRemark($itemID, $remark);
        $json['success'] = $result;
        break;
        
    case 'updateBulkItemStocktake':
        $itemID = IntegerSafe($_POST['ItemID']);
        $locationID = IntegerSafe($_POST['LocationID']);
        $adminGroupID = IntegerSafe($_POST['AdminGroupID']);
        $fundingSourceID = IntegerSafe($_POST['FundingSourceID']);
        $qtyChange = IntegerSafe($_POST['QtyChange']);
        $stocktakeQty = IntegerSafe($_POST['StocktakeQty']);
        $remark = standardizeFormPostValue($_POST['Remark']);
        if ($junior_mck) {
            $remark = convert2unicode($remark,1,0);     // convert to big5
        }
        $result = $linventory->updateBulkItemStocktake($itemID, $locationID, $adminGroupID, $fundingSourceID, $recordStatus=0, $qtyChange, $stocktakeQty, $remark);
        $json['success'] = $result;
        break;
        
    case 'getSubCategory':
        $categoryID = IntegerSafe($_POST['TargetCategory']);
        $subCategoryID = IntegerSafe($_POST['TargetSubCategory']);
        $subCategoryAry = $linventory->getSubCategoryInUse($categoryID);
        $subCategorySelection = getSelectByArray($subCategoryAry, 'name="TargetSubCategory" id="TargetSubCategory" class="selectpicker"', $subCategoryID, 1, 0, $Lang['General']['All']);
        $json['subCategorySelection'] = $subCategorySelection;
        $json['success'] = true;
        break;
        
    case 'getFloorByBuilding':
        $buildingID = IntegerSafe($_POST['TargetBuilding']);
        $flooryID = IntegerSafe($_POST['TargetFloor']);
        $floorSelection = $llocation_ui->Get_Floor_Selection($buildingID, $flooryID, "TargetFloor", "", 0, 0, $Lang['SysMgr']['Location']['All']['Floor'], false, "selectpicker");
        $json['floorSelection'] = $floorSelection;
        $json['success'] = true;
        break;
        
    case 'getRoomByFloor':
        $floorID = IntegerSafe($_POST['TargetFloor']);
        $roomID = IntegerSafe($_POST['TargetRoom']);
        $roomSelection = $llocation_ui->Get_Room_Selection($floorID, $roomID, "TargetRoom", "", 0, $Lang['SysMgr']['Location']['All']['Room'], "", "selectpicker");
        $json['roomSelection'] = $roomSelection;
        $json['success'] = true;
        break;
        
    case 'getItemList':
        $offset = IntegerSafe($_POST['offset']);
        $numberOfRecord = IntegerSafe($_POST['numberOfRecord']);
        $showNoRecord = IntegerSafe($_POST['showNoRecord']);
        $filterAry = array();
        $filterAry['ItemType'] = IntegerSafe($_POST['TargetItemType']);
        $filterAry['Category'] = IntegerSafe($_POST['TargetCategory']);
        $filterAry['SubCategory'] = IntegerSafe($_POST['TargetSubCategory']);
        $filterAry['BuildingID'] = IntegerSafe($_POST['TargetBuilding']);
        $filterAry['FloorID'] = IntegerSafe($_POST['TargetFloor']);
        $filterAry['RoomID'] = IntegerSafe($_POST['TargetRoom']);
        $filterAry['Tag'] = IntegerSafe($_POST['TargetTag']);
        $filterAry['GroupID'] = IntegerSafe($_POST['TargetGroup']);
        $filterAry['FundingSourceID'] = IntegerSafe($_POST['TargetFunding']);
        $filterAry['DisplayZeroQty'] = IntegerSafe($_POST['DisplayZeroQty']);
        
        $TargetItemType = $filterAry['ItemType'];
        $TargetCategory = $filterAry['Category'];
        $TargetSubCategory = $filterAry['SubCategory'];
        $TargetBuilding = $filterAry['BuildingID'];
        $TargetFloor = $filterAry['FloorID'];
        $TargetRoom = $filterAry['RoomID'];
        $TargetTag = $filterAry['Tag'];
         $TargetGroup = $filterAry['GroupID'];
        $TargetFunding = $filterAry['FundingSourceID'];
        $DisplayZeroQty = $filterAry['DisplayZeroQty'];
        
        # set cookies
        $arrCookies[] = array("ck_eInvApp_TargetItemType", "TargetItemType");
        $arrCookies[] = array("ck_eInvApp_TargetCategory", "TargetCategory");
        $arrCookies[] = array("ck_eInvApp_TargetSubCategory", "TargetSubCategory");
        $arrCookies[] = array("ck_eInvApp_TargetBuilding", "TargetBuilding");
        $arrCookies[] = array("ck_eInvApp_TargetFloor", "TargetFloor");
        $arrCookies[] = array("ck_eInvApp_TargetRoom", "TargetRoom");
        $arrCookies[] = array("ck_eInvApp_TargetGroup", "TargetGroup");
        $arrCookies[] = array("ck_eInvApp_TargetFunding", "TargetFunding");
        $arrCookies[] = array("ck_eInvApp_TargetTag", "TargetTag");
        $arrCookies[] = array("ck_eInvApp_DisplayZeroQty", "DisplayZeroQty");
        
        updateGetCookies($arrCookies);
        
        $json['layout'] = $linventory->getItemListUI($offset, $numberOfRecord, $showNoRecord, $filterAry);
        $json['success'] = true;
        break;
        
    case 'getMsgLocationName':
        $locationID = IntegerSafe($_POST['locationID']);
        $locationIDAry = explode(',',$locationID);
        foreach((array)$locationIDAry as $_locationID) {
            $locationFullNameAry[] = $linventory->returnLocationStr($_locationID);
        }
        $locationFullName = implode(',',$locationFullNameAry);
        $message = sprintf($Lang['eInventory']['eClassApp']['Warning']['PleaseDoStockInThisLocation'], $locationFullName); 
        $json['message'] = $message;
        $json['success'] = true;
        break;
        
    case 'getMsgAdminGroupName':
        $adminGroupID = IntegerSafe($_POST['adminGroupID']);
        $adminGroupIDAry = explode(',',$adminGroupID);
        foreach((array)$adminGroupIDAry as $_adminGroupID) {
            $adminGroupNameAry[] = $linventory->returnGroupNameByGroupID($_adminGroupID);
        }
        $adminGroupName = implode(',',$adminGroupNameAry);
        $message = sprintf($Lang['eInventory']['eClassApp']['Warning']['GroupInChargeIsDifferent'], $adminGroupName);
        $json['message'] = $message;
        $json['success'] = true;
        break;
        
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
    foreach((array)$json as $k=>$v){
        if ($k != 'success') {
            $v = convert2unicode($v,true,1);
            $json[$k] = $v;
        }
    }    
}

echo $ljson->encode($json);

?>