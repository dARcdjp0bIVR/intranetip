<?php
/*
 *  Using:
 *  
 *  Purpose: interface for applying leave of not taking school bus in a period
 *
 *  2020-09-15 Cameron
 *      - show loading image when click goBack arrow
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-01-28 Cameron
 *      - only allow select 'From School' option dateType [case #Y154191]
 *      - Reason is not mandatory field
 *      
 *  2018-11-23 Cameron
 *      - pass StartDateType to isAllowedApplyLeave
 *       
 *  2018-11-05 Cameron
 *      - fix: apply intranet_htmlspecialchars for text input field
 *      
 *  2018-06-19 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$today = date('Y-m-d');
$applicationID = $_GET['ApplicationID'];
$applicationIDAry = array($applicationID);
$applicationDetailAry = $ldb->getApplyLeaveRecord($applicationIDAry);

if (count($applicationDetailAry)) {
    $applicationDetail = $applicationDetailAry[0];
}

if ($applicationDetail['RecordStatus'] != 1) {  // Record Status is not 'waiting approval', not allow to change
    $isAllowedChange = false;
}
else {
    $isAllowedApplyLeaveAry = $ldb->isAllowedApplyLeave($applicationDetail['StudentID'],$applicationDetail['StartDate'],$applicationDetail['StartDateType']);
    $isAllowedChange = $isAllowedApplyLeaveAry['isAllowed'] ? true : false;
}

include($PATH_WRT_ROOT."home/eClassApp/common/eSchoolBus/header.php");
?>

<script type="text/javascript">
jQuery(document).ready( function() {
	$('#submitButton').click(function(e) {
		e.preventDefault();
		if (checkForm()) {
			var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['ConfirmSubmit'];?>");
			if (isConfirmed) {
				$('#submitButton').attr('disabled', true);
    			$('#form1').attr('action','?task=parentApp.applyLeaveEditUpdate');
    			$('#form1').submit();
			}
		}
	});

	$('#cancelButton').click(function(e) {
		e.preventDefault();
		var isConfirmed = window.confirm("<?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplicaton'];?>");
		if (isConfirmed) {
			$('#cancelButton').attr('disabled', true);
			$('#form1').attr('action','?task=parentApp.cancelApplyLeave');
			$('#form1').submit();
		}
		
	});
	
});

$(function () {
  	$('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  	});
});

function goBackToList()
{
    var loadingImg = '<?php echo $indexVar['linterface']->Get_Ajax_Loading_Image(); ?>';
    $('#main .submission-area').html(loadingImg).siblings().remove();
    window.location.href = "?task=parentApp.index&tab=applyLeave";
}

function checkForm()
{

// 	if($('#reason').val() == '') {
//		window.alert('<?php echo $Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'];?>');
// 		form1.reason.focus();
// 		return false;
// 	}
    
    return true;	
}
</script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="javascript:goBackToList();"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Apply'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" id="form1" method="post">

        <div id='selectDateField' class='fieldInput date'>
        	<table width="100%">
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['From'];?></span></td>
			        <td width="55%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="startDate" name="startDate" value="<?php echo $applicationDetail['StartDate'];?>" disabled />
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="30%">
                		<div>
            	          	<select id='startDateType' name='startDateType' class='selectpicker' title='' disabled>
<!--            	          	
            	            	<option value="WD" <?php if ($applicationDetail['StartDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['StartDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
-->            	            	
            	            	<option value="PM" <?php if ($applicationDetail['StartDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
                
				<tr>
			        <td width="10%" class="fieldTitle"><span class="mandatoryStar">*</span><span><?php echo $Lang['General']['To'];?></span></td>
			        <td width="55%">
                        <div class="input-group date datetimepicker">            	
                        	<input type="text" class="form-control inputField" id="endDate" name="endDate" value="<?php echo $applicationDetail['EndDate'];?>" disabled/>
                        	<span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> 
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="30%">
                		<div>
            	          	<select id='endDateType' name='endDateType' class='selectpicker' title='' disabled>
<!--            	          	
            	            	<option value="WD" <?php if ($applicationDetail['EndDateType'] == 'WD') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
            	            	<option value="AM" <?php if ($applicationDetail['EndDateType'] == 'AM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
-->            	            	
            	            	<option value="PM" <?php if ($applicationDetail['EndDateType'] == 'PM') echo ' selected ';?>><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
            	          	</select>
                		</div>
                    </td>
                </tr>
            </table>
 		</div>
     		
		<div class="fieldTitle"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Reason'];?></div>
		<div id="inputPartnerField" class="fieldInput infoMissing">
			<input type="text" class="form-control inputField" id="reason" name="reason" placeholder="" value="<?php echo intranet_htmlspecialchars($applicationDetail['Reason']);?>">
		</div>		        
        
        <div class="mandatoryMsg"><?php echo $Lang['eSchoolBus']['App']['RequiredField'];?></div>

        <div class="submission-area">
            <?php if ($isAllowedChange):?>
                <button id="cancelButton" class="btn btn-danger" data-dismiss="modal" aria-label="Cancel"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'];?></button>
                <button id="submitButton" class="btn" data-dismiss="modal" aria-label="Submit"><?php echo $Lang['Btn']['Submit'];?></button>
            <?php endif;?>
        </div>

        <input type="hidden" name="ApplicationID" value="<?php echo $applicationID;?>" >
      </form>
    </div>
  </div>
</div>

</body>
</html>
