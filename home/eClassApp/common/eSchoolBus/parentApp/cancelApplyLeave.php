<?php
/*
 *  Using:
 *  
 *  Purpose: cancel leave application of taking school bus 
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-23 Cameron
 *      - should also update reason field when cancel the application by parent
 *
 *  2019-09-20 Cameron
 *      - should also update RecordStatus in INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT [case #Y170676]
 *
 *  2018-06-20 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$dataAry = array();
$conditionAry = array();

$applicationID = $_POST['ApplicationID'];

if ($applicationID) {
    $dataAry['Reason'] = standardizeFormPostValue($reason);
    $dataAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Cancelled'];
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    
    $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Waiting'];
    $conditionAry['ApplicationID'] = $applicationID;
    
    $ldb->Start_Trans();
    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,$conditionAry,false);
//debug_pr($sql);
    $resultAry[] = $ldb->db_db_query($sql);

    unset($dataAry['Reason']);
    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT',$dataAry,$conditionAry,false);
//debug_pr($sql);
    $resultAry[] = $ldb->db_db_query($sql);

    if (!in_array(false,$resultAry)) {
        $ldb->Commit_Trans();
        $returnMsgKey = 'CancelSuccess';
    }
    else {
        $ldb->RollBack_Trans();
        $returnMsgKey = 'CancelUnsuccess';
    }
}
else {
    $returnMsgKey = 'MissingApplicationID';
}

header("location: ?task=parentApp.index&tab=applyLeave&returnMsgKey=".$returnMsgKey);

//intranet_closedb();

?>
