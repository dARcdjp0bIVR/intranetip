<?php
/*
 *  Using:
 *  
 *  Purpose: save record of applying leave for not taking school bus 
 *
 *  2020-06-19 Cameron
 *      - pass argument $studentID to getAvailableTimeSlotByDateRange() [case #Y188036]
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-20 Cameron
 *      - pass $recordStatus to addApplyLeaveSlotSql() [case #Y170676]
 *
 *  2018-06-12 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$addedTimeSlot = false;

$startDate = $_POST['startDate'];
$startDateType = $_POST['startDateType'];
$endDate = $_POST['endDate'];
$endDateType = $_POST['endDateType'];
$reason = $_POST['reason'];

if (!$_SESSION['SchoolBusStudentID']) {
    $studentID = $ldb->getChild($_SESSION['UserID']);
}
else {
    $studentID = $_SESSION['SchoolBusStudentID'];
}
// print("studentID=$studentID<br>");
// exit;

if ($studentID) {
    $dataAry['StudentID']= $studentID;
    // already check if it's allowed applying leave in ajax

    $availableTimeSlotAry = $ldb->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
    $numberOfTimeSlot = count($availableTimeSlotAry);
    
    if ($numberOfTimeSlot) {
        $recordStatus = $schoolBusConfig['ApplyLeaveStatus']['Waiting'];
        $dataAry['StartDate'] = $startDate;
        $dataAry['StartDateType'] = $startDateType;
        $dataAry['EndDate'] = $endDate;
        $dataAry['EndDateType'] = $endDateType;
        $dataAry['Reason'] = standardizeFormPostValue($reason);
        $dataAry['RecordStatus'] = $recordStatus;     // waiting approval
        $dataAry['AddedFrom'] = '1';        // from eSchoolBus
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];

        $ldb->Start_Trans();
        $sql = $ldb->insert2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,array(),false);

        $resultAry[] = $ldb->db_db_query($sql);
        $applicationID = $ldb->db_insert_id();
        
        foreach((array)$availableTimeSlotAry as $_date => $_timeSlotAry) {
            foreach((array)$_timeSlotAry as $_timeSlot=>$val) {
                $timeSlotVal = $_timeSlot == 'AM' ? '1' : '2';
                $sql = $ldb->addApplyLeaveSlotSql($applicationID, $_date, $timeSlotVal, $insertIgnore=false, $recordStatus);
                $resultAry[] = $ldb->db_db_query($sql);
            }
        }
            
        if (!in_array(false,$resultAry)) {
            $ldb->Commit_Trans();
            if ($applicationID) {
                $notifyResult = $ldb->pushApplyLeaveRecordToTeacher($applicationID);
            }
            $returnMsgKey = 'AddSuccess';
        }
        else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'AddUnsuccess';
        }
    }   // time slot exist
    else {
        $returnMsgKey = 'InvalidTimeSlot';
    }
}
else {
    $returnMsgKey = 'MissingStudentID';
}

header("location: ?task=parentApp.index&tab=applyLeave&returnMsgKey=".$returnMsgKey);

?>
