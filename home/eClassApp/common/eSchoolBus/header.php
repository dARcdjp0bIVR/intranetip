<?php 
/*
 *  Using:
 *  
 *  Purpose:    header file for eSchoolBus webview
 *  
 *  Log
 *  
 *  2018-06-11 Cameron
 *      - create this file
 */
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['Title'];?></title>
<script type="text/javascript" src="../web/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../web/js/moment.js"></script>
<script type="text/javascript" src="../web/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../web/bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../web/js/sitemapstyler.js"></script>
<script type="text/javascript" src="../web/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../web/bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../web/bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../web/bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../web/js/offcanvas.js"></script>
<link rel="stylesheet" href="../web/css/offcanvas.css">
<link rel="stylesheet" href="../web/css/eSchoolBus.css">
<link rel="stylesheet" href="../web/css/font-awesome.min.css">
<script type="text/javascript" src="../web/js/eSchoolBus.js"></script>
