<?php
/*
 *  Using:
 *  
 *  Purpose: add new leave application record of not tacking school bus by teacher / admin  
 *
 *  2020-06-19 Cameron
 *      - pass argument $studentID to getAvailableTimeSlotByDateRange() [case #Y188036]
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-20 Cameron
 *      - pass $recordStatus to addApplyLeaveSlotSql() [case #Y170676]
 *
 *  2018-06-26 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$addedTimeSlot = false;

$studentID = $_POST['studentID'];
$startDate = $_POST['startDate'];
$startDateType = $_POST['startDateType'];
$endDate = $_POST['endDate'];
$endDateType = $_POST['endDateType'];
$reason = $_POST['reason'];
$remark = $_POST['remark'];

if ($studentID) {
    $dataAry['StudentID']= $studentID;

    // class teacher and admin are always allowed to apply leave

    $availableTimeSlotAry = $ldb->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
    $numberOfTimeSlot = count($availableTimeSlotAry);
    
    if ($numberOfTimeSlot) {
        $recordStatus = $schoolBusConfig['ApplyLeaveStatus']['Confirmed'];      // Approved
        $dataAry['StartDate'] = $startDate;
        $dataAry['StartDateType'] = $startDateType;
        $dataAry['EndDate'] = $endDate;
        $dataAry['EndDateType'] = $endDateType;
        $dataAry['Reason'] = standardizeFormPostValue($reason);
        $dataAry['Remark'] = standardizeFormPostValue($remark);
        $dataAry['RecordStatus'] = $recordStatus;
        $dataAry['AddedFrom'] = '1';        // from eSchoolBus
        $dataAry['ApprovedBy'] = $_SESSION['UserID'];            
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        
        $ldb->Start_Trans();
        $sql = $ldb->insert2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,array(),false);

        $resultAry[] = $ldb->db_db_query($sql);
        $applicationID = $ldb->db_insert_id();
        
        foreach((array)$availableTimeSlotAry as $_date => $_timeSlotAry) {
            foreach((array)$_timeSlotAry as $_timeSlot=>$val) {
                $timeSlotVal = $_timeSlot == 'AM' ? '1' : '2';
                $sql = $ldb->addApplyLeaveSlotSql($applicationID, $_date, $timeSlotVal, $insertIgnore=false, $recordStatus);
                $resultAry[] = $ldb->db_db_query($sql);
            }
        }
                
        if (!in_array(false,$resultAry)) {
            $ldb->Commit_Trans();
            // not need to push message to inform parent
            $returnMsgKey = 'AddSuccess';
        }
        else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'AddUnsuccess';
        }
    }   // time slot exist
    else {
        $returnMsgKey = 'InvalidTimeSlot';
    }
}
else {
    $returnMsgKey = 'MissingStudentID';
}

header("location: ?task=teacherApp.index&tab=handled&returnMsgKey=".$returnMsgKey);

?>
