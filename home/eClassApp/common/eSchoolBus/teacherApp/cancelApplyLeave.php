<?php
/*
 *  Using:
 *  
 *  Purpose: cancel leave application by class teacher / eSchool Bus admin 
 *
 *  2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 *  2019-09-20 Cameron
 *      - also update RecordStatus to INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT  [case #Y170676]
 *
 *  2018-06-22 Cameron
 *      - create this file
 */

if ($indexVar) {
    $ldb = $indexVar['db'];
}
else {
    header("location: /");
}

$resultAry = array();
$dataAry = array();
$conditionAry = array();

$applicationID = $_POST['ApplicationID'];
$remark = $_POST['remark'];

if ($applicationID) {
    $dataAry['Remark'] = standardizeFormPostValue($remark);
    $dataAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Cancelled'];
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    
    $conditionAry['ApplicationID'] = $applicationID;
    
    $ldb->Start_Trans();
    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,$conditionAry,false);
//debug_pr($sql);
    $resultAry[] = $ldb->db_db_query($sql);

    unset($dataAry['Remark']);
    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT',$dataAry,$conditionAry,false);
//debug_pr($sql);
    $resultAry[] = $ldb->db_db_query($sql);

    if (!in_array(false,$resultAry)) {
        $ldb->Commit_Trans();
        $notifyResult = $ldb->pushApplyLeaveResultToParent($applicationID);
        $returnMsgKey = 'CancelSuccess';
    }
    else {
        $ldb->RollBack_Trans();
        $returnMsgKey = 'CancelUnsuccess';
    }
}
else {
    $returnMsgKey = 'MissingApplicationID';
}

header("location: ?task=teacherApp.index&tab=handled&returnMsgKey=".$returnMsgKey);

?>
