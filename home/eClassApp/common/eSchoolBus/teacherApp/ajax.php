<?
/*
 * Log
 *
 * Description: output json format data
 *
 * 2020-06-19 Cameron
 *      - pass argument $studentID to getAvailableTimeSlotByDateRange() [case #Y188036]
 *
 * 2020-02-11 Cameron
 *      - fix: avoid direct access the page (not via parameter) by checking $indexVar
 * 
 * 2019-10-15 Cameron
 *      - avoid direct access the page (not via parameter) by checking $indexVar
 *
 * 2019-10-14 [Cameron]
 *      - add case cancelApplicationByTimeSlotID to cancel application timeslot
 *
 * 2019-03-07 [Cameron]
 *      - comment out ej ($junior_mck) fragment as it's not support yet
 *      
 * 2019-01-30 [Cameron]  
 *      - add parameter $applyLeaveStatus, $startDate and $endDate to case getHandledListForTeacher
 *      - add parameter $startDate and $endDate to case getPendingListForTeacher
 *  
 * 2018-11-05 [Cameron] not need to apply remove_dummy_chars_for_json, e.g. remark field
 *      - apply standardizeFormPostValue to text input field in case getPendingListForTeacher and getHandledListForTeacher
 * 
 * 2018-10-24 [Cameron] add paremeter isShowNoRecord to getPendingApplicationForTeacher() and getHandledApplicationForTeacher()
 * 
 * 2018-06-22 [Cameron] create this file
 */

if (!$indexVar) {
    header("location: /");
}

$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$y = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true; // whether to remove new line, carriage return, tab and back slash
if ($indexVar) {
    $linterface = $indexVar['linterface'];
    $ldb = $indexVar['db'];

    switch ($action) {

        case 'checkApplication':
            $studentID = $_POST['studentID'];
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
            $startDateType = $_POST['startDateType'];
            $endDateType = $_POST['endDateType'];
            $isTimeSlotAppliedLeave = $ldb->isTimeSlotAppliedLeave($studentID, $startDate, $endDate, $startDateType, $endDateType);

            $x = '';
            if ($isTimeSlotAppliedLeave) {
                $x = $schoolBusConfig['ApplyLeaveError']['DuplicateApplication'];
            } else {
                $availableTimeSlotAry = $ldb->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
                if (count($availableTimeSlotAry) == 0) {
                    $x = $schoolBusConfig['ApplyLeaveError']['InvalidTimeSlot'];
                }
            }
            $json['success'] = true;
            break;

        case 'getPendingListForTeacher':
            $className = standardizeFormPostValue($_POST['className']);
            $inputSearch = standardizeFormPostValue($_POST['inputSearch']);
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
            $x = $linterface->getPendingApplicationForTeacher($_POST['offset'], $_POST['numberOfRecord'], $className, $_POST['studentID'], $inputSearch, $_POST['isShowNoRecord'], $startDate, $endDate);
            $json['success'] = true;
            break;

        case 'getHandledListForTeacher':
            $className = standardizeFormPostValue($_POST['className']);
            $inputSearch = standardizeFormPostValue($_POST['inputSearch']);
            $applyLeaveStatus = $_POST['applyLeaveStatus'];
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
            $x = $linterface->getHandledApplicationForTeacher($_POST['offset'], $_POST['numberOfRecord'], $className, $_POST['studentID'], $inputSearch, $_POST['isShowNoRecord'], $applyLeaveStatus, $startDate, $endDate);
            $json['success'] = true;
            break;

        case 'getStudentListByClass':
            $className = $_POST['className'];
//        $className = ($junior_mck) ? convert2unicode($className, 1, $direction=0) : $className;		// convert utf-8 to big5 for ej
            if (!empty($className)) {
//             if ($junior_mck) {
//                 $lclass = new libclass();
//                 $data = $lclass->getStudentNameListWClassNumberByClassName($className);
//             }
//             else {
                $data = $ldb->getStudentNameListWClassNumberByClassName($className);
//            }
            } else {
                $data = $ldb->getAllStudentOfCurrentYear();
            }
            $x = getSelectByArray($data, "name='studentID' id='studentID' class='selectpicker' ", $selected = "", $all = 0, $noFirst = 0, $FirstTitle = $Lang['eSchoolBus']['TeacherApp']['AllStudent']);
            $json['success'] = true;
            break;

        case 'cancelApplicationByTimeSlotID':
            $timeSlotID = IntegerSafe($_POST['TimeSlotID']);
            $dataAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Cancelled'];
            $dataAry['ModifiedBy'] = $_SESSION['UserID'];

            $conditionAry['TimeSlotID'] = $timeSlotID;
            $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT', $dataAry, $conditionAry, false);
//debug_pr($sql);

            $ldb->Start_Trans();
            $resultAry[] = $ldb->db_db_query($sql);

            if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                $theOtherTimeSlotAry = $ldb->getTheOtherTimeSlotOfTheDate($timeSlotID);
                if (count($theOtherTimeSlotAry)) {
                    $conditionAry['TimeSlotID'] = $theOtherTimeSlotAry[0]['TimeSlotID'];
                    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT', $dataAry, $conditionAry, false);
                    $resultAry[] = $ldb->db_db_query($sql);
                }
            }

            $applicationID = $ldb->getApplicationIDByTimeSlotID($timeSlotID);
            if (!$ldb->isRecordStatusConsistent($applicationID)) {
                if ($ldb->isRecordStatusConsistent($applicationID, $detailOnly = true)) {       // update parent application status if all child status are the same but they are different to parent's
                    unset($conditionAry);
                    $conditionAry['ApplicationID'] = $applicationID;
                    $sql = $ldb->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE', $dataAry, $conditionAry, false);
                    $resultAry[] = $ldb->db_db_query($sql);
                    $notifyResult = $ldb->pushApplyLeaveResultToParent($applicationID);         // send push message
                }
            }

            if (!in_array(false, $resultAry)) {
                $ldb->Commit_Trans();
                //$notifyResult = $ldb->pushApplyLeaveResultToParent($applicationID, $timeSlotID);      // don't send push message for individual date?
                $x = $linterface->getLeaveApplicationStatusHistoryDisplay($schoolBusConfig['ApplyLeaveStatus']['Cancelled']);
                $json['success'] = true;
            } else {
                $ldb->RollBack_Trans();
            }

            break;
    }
}

// if ($remove_dummy_chars) {
//     $x = remove_dummy_chars_for_json($x);
//     $y = remove_dummy_chars_for_json($y);
// }

$json['html'] = $x;
$json['html2'] = $y;
echo $ljson->encode($json);

?>