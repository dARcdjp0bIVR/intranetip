<?
/*
 * Log
 *
 * Description: output json format data
 * 
 * 2019-07-11 [Cameron] create this file
 */
$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

if (!$indexVar) {
    header('location: ../');
}

if (class_exists('JSON_obj')) {
    $ljson = new JSON_obj();
}
else {
    return;
}

$json['success'] = false;

$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];

$linterface = $indexVar['linterface'];
$libpos = $indexVar['libpos'];

switch ($action) {
    case 'getCategoryItem':
        $categoryID = IntegerSafe($_POST['CategoryID']);
        $itemInCart = IntegerSafe($_POST['ItemInCart']);
        $json['categoryLayout'] = $linterface->getCategoryItem($categoryID, $itemInCart);
        $json['success'] = true;
        break;

    case 'addItemToCart':
        $itemID = IntegerSafe($_POST['ItemID']);
        $existInCart = $_POST['ExistInCart'];
        $qty = IntegerSafe($_POST['Qty']);
        $libItem = new POS_Item($itemID);
        $itemInfo = array();
        $itemInfo['ItemID'] = $libItem->ItemID;
        $itemInfo['ItemName'] = $libItem->ItemName;
        $itemInfo['UnitPrice'] = $libItem->UnitPrice;
        $itemInfo['PurchaseQty'] = $qty + 1;
        $itemInfo['RemainQty'] = $libItem->ItemCount;

        if ($_SESSION['ePosStudentID'] && $itemInfo['ItemID']) {
            if ($libpos->isItemInPendingCart($_SESSION['ePosStudentID'], $itemInfo['ItemID'])) {
                $result = $libpos->updatePendingCart($_SESSION['ePosStudentID'], $itemInfo['ItemID'], $itemInfo['PurchaseQty']);
            } else {
                $result = $libpos->addPendingCart($_SESSION['ePosStudentID'], array($itemInfo['ItemID']), array($itemInfo['PurchaseQty']));
            }
        }
        else {
            $result = false;
        }

        if ($result) {
            $json['cartItem'] = $linterface->getItemInShoppingCart($itemInfo);
            $json['existInCart'] = $existInCart;
            $json['itemID'] = $itemID;
            $json['success'] = true;
        }
        break;

    case 'deductCartItem':
        $itemID = IntegerSafe($_POST['ItemID']);
        $qty = IntegerSafe($_POST['Qty']);
        $purchaseQty = $qty - 1;
        if ($purchaseQty) {
            $updateResult = $libpos->updatePendingCart($_SESSION['ePosStudentID'], $itemID, $purchaseQty);
            if ($updateResult) {
                $libItem = new POS_Item($itemID);
                $itemInfo = array();
                $itemInfo['ItemID'] = $libItem->ItemID;
                $itemInfo['ItemName'] = $libItem->ItemName;
                $itemInfo['UnitPrice'] = $libItem->UnitPrice;
                $itemInfo['PurchaseQty'] = $purchaseQty;
                $itemInfo['RemainQty'] = $libItem->ItemCount;
                $json['cartItem'] = $linterface->getItemInShoppingCart($itemInfo);
                $json['existInCart'] = 'true';
            }
        }
        else {
            $deleteResult = $libpos->deletePendingCart($_SESSION['ePosStudentID'], $itemID);
            if ($deleteResult) {
                $json['removeItemFromCart'] = 'true';
            }
        }
        $json['itemID'] = $itemID;
        $json['success'] = true;
        break;

    case 'cancelCart':
        $deleteResult = $libpos->deletePendingCart($_SESSION['ePosStudentID']);
        if ($deleteResult) {
            $json['success'] = true;
        }
        break;

    case 'zoomInItemPhoto':
        $itemID = IntegerSafe($_POST['ItemID']);
        $json['photoDetail'] = $linterface->getZoomInItemPhotoDialog($itemID);;
        $json['success'] = true;
        break;

    case 'getSearchResult':
        $keyword = $_POST['Keyword'];
        $itemInCart = IntegerSafe($_POST['ItemInCart']);
        $json['searchResult'] = $linterface->getSearchResult($keyword,$itemInCart);
        $json['success'] = true;
        break;

    case 'getOrderDetails':
        $transactionLogID = IntegerSafe($_POST['TransactionLogID']);
        $json['orderDetails'] = $linterface->getOrderDetails($transactionLogID);
        $json['success'] = true;
        break;

    case 'getStudentListByClass':
        $className = $_POST['ClassName'];
        $studentList = $libpos->getStudentListByClass($className);
        $json['StudentList'] = $linterface->GET_SELECTION_BOX($studentList, 'name="StudentID" id="StudentID"', "-- ".$Lang['Btn']['Select']." --");
        $json['success'] = true;
        break;

    case 'getPosPageByStudent':
        $studentID = IntegerSafe($_POST['StudentID']);
        $_SESSION['ePosStudentID'] = $studentID;
        $json['tabScrollableContents'] = $linterface->getAllCategories();
        $pendingCart = $libpos->getPendingCart($studentID);
        $json['shoppingCart'] = $linterface->getShoppingCart($pendingCart);

        $unpickupDetailAry = $libpos->getUnpickupDetailByStudent($studentID);
        $nrUnpickup = count($unpickupDetailAry);
        if ($nrUnpickup) {
            $unpickupStyle = '';
        }
        else {
            $unpickupStyle = 'none';
        }
        $json['unpickupStyle'] = $unpickupStyle;
        $json['unpickupNumber'] = $nrUnpickup;
        $json['unpickupOrder'] = $linterface->getUnpickupOrder($unpickupDetailAry);

        $pickupDetailAry = $libpos->getPickupDetailByStudent($studentID);
        $nrPickup = count($pickupDetailAry);
        if ($nrPickup) {
            $pickupStyle = '';
        }
        else {
            $pickupStyle = 'none';
        }
        $json['pickupStyle'] = $pickupStyle;
        $json['pickupNumber'] = $nrPickup;
        $json['pickupOrder'] = $linterface->getPickupOrder($pickupDetailAry);

        $json['success'] = true;
        break;

//    case 'beforePayByDeposit':
//        $json['BeforePayByDeposit'] = $linterface->getBeforePayByDeposit($_SESSION['ePosStudentID']);
//        $json['success'] = true;
//        break;

}

echo $ljson->encode($json);

?>