<?
/*
 *  Using:
 *
 * 	Log
 *
 *  2020-01-13 Cameron
 *      - fix: use open receipt as attachment method instead of open it directly in browser because app does not support this
 *
 *  2019-11-04 Cameron
 *      - fix: allow admin to view all receipts and allow teacher to view his/her own receipt
 *
 * 	2019-07-31 Cameron
 * 		- create this file
 */

if (!$indexVar || !$intranet_root) {
    header('location: ../');
}

require_once($intranet_root."/includes/mpdf/mpdf.php");
include_once ($intranet_root . "/includes/libfilesystem.php");


$transactionLogID = IntegerSafe($_POST['TransactionLogID']);

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'], $do_not_remove_token_data=true)){
    No_Access_Right_Pop_Up();
    exit;
}

if ($transactionLogID) {

    $libpos = $indexVar['libpos'];

    if (!($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $_SESSION['OnBehalfOfStdent']) && !$libpos->isTransactionBelongToUser($transactionLogID,$_SESSION['UserID'])) {
        No_Access_Right_Pop_Up();
        exit;
    }

    $linterface = $indexVar['linterface'];
    $receipt = $linterface->getReceiptDetails($transactionLogID);
    $receiptHeader = $linterface->getReceiptHeader();
    $fileName = "receipt";

    $lf = new libfilesystem();
    $eposFolder = $file_path."/file/epos/".$_SESSION['UserID'];
    $lf->lfs_remove($eposFolder);        // remove folder of current user
    $lf->createFolder($eposFolder);
    $eposFile = $eposFolder .'/'. $transactionLogID;

    $pdf = new mPDF('', 'A4');
    $pdf->WriteHTML($receiptHeader);
    $pdf->WriteHTML("<body>");
    $pdf->WriteHTML($receipt);
    $pdf->WriteHTML("</body></html>");

//    $pdf->Output($fileName . '.pdf', 'D');        // send to browser to download, can't open in app directly, don't know why
    $pdf->Output($eposFile . '.pdf', 'F');          // therefore, save it in file directory first, then open it as attachment


    header("location: /home/download_attachment.php?target_e=". getEncryptedText($eposFile.".pdf") . '&filename_e=' . getEncryptedText($fileName.".pdf"));

}
?>
