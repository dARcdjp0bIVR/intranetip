<?php
/*
 *  Using: 
 *
 *  Purpose: show result of Alipay payment
 *
 *  2019-12-11 Cameron
 *      - create this file
 */

if (!$indexVar || !$intranet_root) {
    header('location: ../');
}

include_once($intranet_root."/includes/libalipay.php");
include_once($intranet_root."/home/eClassApp/common/ePOS/epos_config.php");


$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];

if (empty($intranet_root)) {
    return;
}

if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
    $bodyClass="studentApp";
}
elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
    $bodyClass="teacherApp";
}
else {
    $bodyClass="";  // default parentApp
}

if (!$_SESSION['ePosStudentID']) {
    $error[]['ItemName'] = 'MissingUser';
    $displayError .= $Lang['ePOS']['eClassApp']['error']['UserNotFound'];
}
else {
//    $status = IntegerSafe($_GET['status']);
//    $transactionLogID = IntegerSafe($_GET['TransactionLogID']);
    $paymentID = IntegerSafe($_GET['paymentID']);       // TempTransactionID
    $alipayTransaction = $libpos->getAlipayTransactionByID($paymentID);
    $transactionLogID = $alipayTransaction['TransactionID'];
    $status = $alipayTransaction['RecordStatus'];
    $studentID = $alipayTransaction['StudentID'];

    if ($studentID != $_SESSION['ePosStudentID']) {
        No_Access_Right_Pop_Up();
    }

    $displayError = '';
    switch ($status) {
        case 1:     // complete
            if ($transactionLogID > 0) {
                $transactionDetail = $libpos->Get_POS_Transaction_Detail($transactionLogID);
                if (count($transactionDetail)) {
                    $transactionDetail = $transactionDetail[0];
                    $_transactionDetail = array();
                    $_transactionDetail['TransactionLogID'] = $transactionLogID;
                    $_transactionDetail['OrderNumber'] = $transactionDetail['REFCode'];
                    $payFinishLayout = $linterface->getPayFinishLayout($_transactionDetail);
                }
                else {
                    $displayError .= $Lang['ePOS']['eClassApp']['error']['LostTransaction'];
                }
            }
            else {
                switch ($transactionLogID) {
                    case 0:     // Not enough balance
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['Deflict'];
                        break;
                    case -1:    // user not found
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['UserNotFound'];
                        break;
                    case -4:    // Not enough Inventory
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['OutOfStock'];
                        break;
                    case -5:    // sql error
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['DatabaseError'];
                        break;
                    case -6:    // Not item detail
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['NoItemDetail'];
                        break;
                    default:    // unknown error
                        $displayError .= $Lang['ePOS']['eClassApp']['error']['UnknownError'];
                        break;
                }
            }
            
            break;
        case 2:
            $displayError = $Lang['ePOS']['TransactionStatus']['Cancelled'];
            break;
        case -1:
            $displayError = $Lang['ePOS']['TransactionStatus']['Failed'];
            break;
    }
}


include($intranet_root."/home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript">
    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){

        $(document).on('click', 'div#btnHome', function(e) {
            window.location.href = "?";
        });

        $(document).on('click', 'div#btnOrder', function(e) {
          $.ajax({
              dataType: "json",
              type: "POST",
              url: '?task=management.ajax',
              data : {
                  'action': 'getOrderDetails',
                  'TransactionLogID': $(this).attr('data-TransactionLogID')
              },
              success: function(ajaxReturn) {
            	  $('div#paymentResult').fadeOut(250);
                  showOrderDetails(ajaxReturn, null);
              },
              error: show_ajax_error
          });
      	});

      $(document).on('click', '#orderDetalCloseBtn', function(e) {
          e.preventDefault();
          $("#orderDetails").css('display','none');
          $("#paymentResult").fadeIn(300);
      });

      $(document).on('keyup', '#tbxTopSearch', function(e) {
          var keyword = $.trim($(this).val());
          if(e.keyCode == 13 && keyword.length>0)
          {
              window.location.href = "?task=&keyword=" + encodeURIComponent(keyword);
          }
      });
        
    });

    function showOrderDetails(ajaxReturn, obj)
    {
        if (ajaxReturn != null && ajaxReturn.success){
        	if (obj != null) {
                $this = obj;
                var id = $this.parent().parent().attr('id');
                if (id != 'undefined' && id.substr(0,3) == 'blk') {
                    var len = id.length;
                    id = 'btn' + id.substr(3,len-3);
                }
                $('#' + id).click();    // toggle (hide) blkPending or blkHistory
        	}
            $('div#payBeforeLayout').fadeOut(250);
            $('div#orderDetails').html(ajaxReturn.orderDetails).fadeIn(250);
        }
    }

</script>

</head>

<body class="<?php echo $bodyClass;?>">
<div class="mainBody">

    <div id="paymentResult">
<?php
    if (empty($displayError)) {
        echo $payFinishLayout;
    }
    else {
        echo $linterface->getErrorLayout($displayError);
    }
?>
    </div>

    <div id="orderDetails" style="display: none;">

    </div>

</div>


<?php
    echo $linterface->getPowerByFooter();
    echo $linterface->getTopBarMenu($className='', $_SESSION['ePosStudentID'], $enableSelectStudent=false);
?>
</body>
</html>
