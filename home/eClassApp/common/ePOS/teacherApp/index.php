<?php
/*
 *  Using:
 *
 *  Purpose: teacher ePOS login portal
 *
 *  2019-11-04 Cameron
 *      - should use SESSION variable to remmeber selected student so that it can show the records of the student after return to this page
 *
 *  2019-08-20 Cameron
 *      - create this file
 */

if (!$indexVar) {
    header('location: ../');
}

$libpos = $indexVar['libpos'];
$linterface = $indexVar['linterface'];

$_SESSION['ePosTeacherID'] = $_SESSION['UserID'];
if (!$_SESSION['ePosTeacherID']) {
    No_Access_Right_Pop_Up();
    exit;
}

$studentID = '';    // initialize
$className = '';
if ($_SESSION['OnBehalfOfStdent']) {
//    $studentID = IntegerSafe($_POST['StudentID']);      // return from change of StudentID
//    $className = $_POST['StudentClass'];
//    if ($studentID == '') {     // still empty, check if there's pending stock in db
//        $firstStudent = $libpos->getFirstStudentInPendingCart($_SESSION['UserID']);
//        if (count($firstStudent)) {
//            $_SESSION['ePosStudentID'] = $firstStudent[0]['StudentID'];
//            $studentID = $_SESSION['ePosStudentID'];
//            $className = $firstStudent[0]['ClassTitleEN'];
//        } else {
//            $_SESSION['ePosStudentID'] = '';
//        }
//    }
//    else {
//        $_SESSION['ePosStudentID'] = $studentID;
//    }

//    if ($_SESSION['ePosStudentID']) {
//        $pendingCart = $libpos->getPendingCart($_SESSION['ePosStudentID']);
//    }
//    else {
//        $_SESSION['ePosStudentID'] = '';        // initial empty
//        $pendingCart = array();
//    }

    if (!isset($_SESSION['ePosStudentID'])) {
        $_SESSION['ePosStudentID'] = '';        // initial empty
        $pendingCart = array();
    }
    else {
        $studentID = $_SESSION['ePosStudentID'];
        $className = $libpos->getClassNameByStudentID($studentID);
    }
}
else {
    $pendingCart = $libpos->getPendingCart($_SESSION['UserID']);    // teacher self-purchase
    $_SESSION['ePosStudentID'] = $_SESSION['UserID'];
}

// return from to search
if ($_GET['keyword']) {
    $keyword = rawurldecode($_GET['keyword']);
    $itemInCart = Get_Array_By_Key($pendingCart,'ItemID');
    $searchResult = $linterface->getSearchResult($keyword,$itemInCart);
    $searchResultCss = '';
}
else {
    $searchResult = '';
    $searchResultCss = 'none';
}


include($PATH_WRT_ROOT."home/eClassApp/common/ePOS/header.php");
?>
<script type="text/javascript" src="../web/js/eposPortal.js"></script>
<script type="text/javascript">
    var isLoading = true;
    var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

    function show_ajax_error()
    {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

    jQuery(document).ready(function(){
        $(document).on('click', 'div#btnCheckout', function() {
            if ($('#StudentID').val() == '') {
                window.alert('<?php echo $Lang['General']['JS_warning']['SelectStudent'];?>');
            }
            else {
                window.form2.method = "get";
                window.location.href = "?task=management.pay_by_deposit";
            }
        });

        <?php if ($_GET['keyword']):?>
        $("#pMain").hide();
        <?php endif;?>

        $(document).on('change', '#StudentClass', function(e) {
            e.preventDefault();
            emptyStudentOrderInfo();
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '?task=management.ajax',
                data : {
                    'action': 'getStudentListByClass',
                    'ClassName': $('#StudentClass').val()
                },
                success: showStudentList,
                error: show_ajax_error
            });
        });

        $(document).on('change', 'select#StudentID', function(e) {

            e.preventDefault();
            if ($(this).val() != '') {
//                $('#form2').submit();         // this is the alternative method
                isLoading = true;
                $('#tabScrollableContents').html(loadingImg);
                $('#blkPage').fadeOut(150);
                $('#blkTray').fadeOut(150);
                loadLayoutByStudent();
            }
            else {
                emptyStudentOrderInfo();
            }
        });

<?php if ($_SESSION['ePosStudentID']): ?>
        loadLayoutByStudent();
<?php endif;?>

    });

    function loadLayoutByStudent()
    {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '?task=management.ajax',
            data : {
                'action': 'getPosPageByStudent',
                'StudentID': $('#StudentID').val()
            },
            success: refreshCurrentPage,
            error: show_ajax_error
        });
    }

    function showStudentList(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            $('#StudentList').html(ajaxReturn.StudentList);
        }
    }

    function refreshCurrentPage(ajaxReturn)
    {
        if (ajaxReturn != null && ajaxReturn.success){
            isLoading = false;
            $('#tabScrollableContents').html(ajaxReturn.tabScrollableContents);

            // show shopping cart before prodetails for highlighting product
            $('#blkTray').html(ajaxReturn.shoppingCart);
            $('#blkTray').fadeIn(150);

            // show first category items, must show shopping cart first
            $('a.tabScrollable-link:first').click();
            $('#blkPage').fadeIn(150);

            var maxHeight = window.innerHeight - 100;
            var height = 76.6 * parseInt(ajaxReturn.pickupNumber) + 47;
            if (height > maxHeight) {
                height = maxHeight;
            }
            $('#blkHistory').css({'height':height + 'px', 'min-height':'47px'});
            $('#blkHistory').html(ajaxReturn.pickupOrder);

            height = 76.6 * parseInt(ajaxReturn.unpickupNumber) + 47;
            if (height > maxHeight) {
                height = maxHeight;
            }
            $('#blkPending').css({'height':height + 'px', 'min-height':'47px'});
            $('#blkPending').html(ajaxReturn.unpickupOrder);
            $('#lblPending').css({'display':''});
            $('#lblPending').html(ajaxReturn.unpickupNumber);
        }
    }

    function emptyStudentOrderInfo()
    {
        emptyCart();
        $('#blkHistory').html('');
        $('#blkPending').html('');
        $('#lblPending').html('');
        $('#lblPending').css({'display':'none'});
        $('#blkHistory').css({'height':'47px'});
        $('#blkPending').css({'height':'47px'});
    }

</script>

</head>

<body class="teacherApp">
<div id="cHome" class="mainBody opacity0 transition">

    <article id="pMain">

        <div id="blkTabs">
            <nav id="tabScrollable" class="tabScrollable dragscroll">
                <div id="tabScrollableContents" class="tabScrollableContents">
                    <?php echo $linterface->getAllCategories();?>
                </div>
            </nav>
            <button id="tabScrollableLeftBtn" class="tabScrollable-button tabScrollable-button-left" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/></svg>
            </button>
            <button id="tabScrollableRightBtn" class="tabScrollable-button tabScrollable-button-right" type="button">
                <svg class="tabScrollable-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/></svg>
            </button>
        </div>

        <div id="blkPage" class="product-item-container">

        </div>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><article id="pSearch" style="display: <?php echo $searchResultCss;?>">
        <?php echo $searchResult;?>
    </article><!--
    don't leave space including line feed here to suit the alignment
--><aside id="blkTray">
        <?php echo $linterface->getShoppingCart($pendingCart);?>
    </aside>
    <div id="orderDetails" style="display: none;">

    </div>

</div>

<?php
echo $linterface->getPowerByFooter();
echo $linterface->getTopBarMenu($className, $studentID, $enableSelectStudent=true);
?>

<div class="dialog-container" style="display:none;">
    <?php
    echo $linterface->getConfirmRemoveItemDialog();
    echo $linterface->getConfirmCancelOrderDialog();
    echo $linterface->getZoomInItemPhotoDialog();
    ?>

</div>

</body>
</html>
