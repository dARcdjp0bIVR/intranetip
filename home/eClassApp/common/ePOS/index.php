<?php
// using :

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

// App init
if (isset($parLang)) {
    $leClassApp_init = new libeClassApp_init();
    $intranet_session_language = $leClassApp_init->getPageLang($parLang);
    session_register_intranet("intranet_session_language", $intranet_session_language);
} else {
    $intranet_session_language = $_SESSION['intranet_session_language'];
}
if ($intranet_session_language == '') {
    $intranet_session_language = 'en';
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_app_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$indexVar['posCategory'] = new POS_Category();
$indexVar['posItem'] = new POS_Item();
$indexVar['libpos'] = new libpos();
$indexVar['linterface'] = new libpos_app_ui();
$indexVar['leClassApp'] = new libeClassApp();
$indexVar['lpayment'] = new libpayment();

if (!$_SESSION['UserType']) {
    $user = new libuser($_SESSION['UserID']);
    $isParent = $user->isParent();
    $isStudent = $user->isStudent();
    $isTeacherStaff = $user->isTeacherStaff();

    if ($isParent) {
        $userType = USERTYPE_PARENT;
        $_SESSION['UserType'] = $userType;
    }
    else if ($isStudent) {
        $userType = USERTYPE_STUDENT;
        $_SESSION['UserType'] = $userType;
    }
    else if ($isTeacherStaff) {
        $userType = USERTYPE_STAFF;
        $_SESSION['UserType'] = $userType;
    }
    else {
        $userType = '';
    }
}
else {
    $userType = $_SESSION['UserType'];
}

if ($_SESSION['UserType'] == USERTYPE_STAFF) {
    if (IntegerSafe($_GET['FromService']) == 1) {
        $_SESSION['OnBehalfOfStdent'] = false;          // process pos as myself
    } else if ((IntegerSafe($_GET['FromAdmin']) == 1) && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) {
        $_SESSION['OnBehalfOfStdent'] = true;           // on behalf of student
    } else {
        // keep value that've been set
    }
}

$task = '';
if ($_POST['task'] != '') {
	$task = $_POST['task'];
}
else if ($_GET['task'] != '') {
	$task = $_GET['task'];
}

if ($task == '') {
    switch ($userType) {
        case USERTYPE_PARENT:
            $task = 'parentApp.index';
            break;
        case USERTYPE_STUDENT:
            $task = 'studentApp.index';
            break;
        case USERTYPE_STAFF:
            $task = 'teacherApp.index';
            break;
    }
}
// Prevent searching parent directories, like 'task=../../index'.
$task = str_replace("../", "", $task);
$task = str_replace('.', '/', $task);

$indexVar['libpos']->checkAppAccessRight();

### Include corresponding php files
$indexVar['taskScript'] = '';
$indexVar['taskScript'] .= $task.'.php';

if (file_exists($indexVar['taskScript'])){
	include_once($indexVar['taskScript']);
}
else {
	$indexVar['taskScript'] = $task.'/index.php';
	if (file_exists($indexVar['taskScript'])){
		include_once($indexVar['taskScript']);
	}else{
		No_Access_Right_Pop_Up();
	}
}

intranet_closedb();
?>