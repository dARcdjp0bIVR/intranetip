<?php 
/*
 *  Using:
 *  
 *  Purpose:    header file for ePOS webview
 *  
 *  Log
 *
  * 2019-12-16 Cameron
  *     - fix lang for Title
  * 
 *  2019-07-09 Cameron
 *      - create this file
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>eClassIP ePOS</title>
    <link rel="stylesheet" href="../web/css/epos.css" type="text/css">
    <link rel="stylesheet" href="../web/css/font-awesome.min.css" media="all" type="text/css">
    <link rel="stylesheet" href="../web/css/notosanstc.css" media="all" type="text/css">
    <script type="text/javascript" src="../web/js/jquery-3.2.1.min.js"></script>
    <script language="javascript" type="text/javascript" src="../web/js/lang.<?php echo $intranet_session_language;?>.js?_=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="../web/js/epos.js"></script>
    <link rel="shortcut icon" href="../web/images/favicon.png">
    <link rel="icon" type="image/png" href="../web/images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
