// JavaScript Document
/* Filter function start */
$(function() {
    $('.tapHeader .button .searchProg').click(function(){
        $('.tapHeader div#filterProg').removeClass('in');
        $('#progContent').removeClass('filterOpen');
        $('#progContent').toggleClass('searchOpen');
    });

    $('.tapHeader .button .filterProg .filter').click(function(){
        $('.tapHeader div#searchProg').removeClass('in');
        $('#progContent').removeClass('searchOpen');
        $('#progContent').toggleClass('filterOpen');
    });

    $('.tapHeader .button .searchStd').click(function(){
        $('.tapHeader div#filterStd').removeClass('in');
        $('#stdContent').removeClass('filterOpen');
        $('#stdContent').toggleClass('searchOpen');
    });

    $('.tapHeader .button .filterStd .filter').click(function(){
        $('.tapHeader div#searchStd').removeClass('in');
        $('#stdContent').removeClass('searchOpen');
        $('#stdContent').toggleClass('filterOpen');
    });

    $('#header #button .filterStd .filterStd').click(function(){
        $('#content').removeClass('searchOpen');
        $('#content').toggleClass('filterOpen');
    });

    $('#header #button .filterProg .filterProg').click(function(){
        $('#content').removeClass('searchOpen');
        $('#content').toggleClass('filterOpen');
    });

    $('#filterStdArea.tapHeader .button .searchStd').click(function(){
        $('.tapHeader div#filterStd').removeClass('in');
        $('.addStdList').removeClass('filterOpen');
        $('.addStdList').toggleClass('searchOpen');

        if ($('.addStdList').hasClass("searchOpen")) {
            setAddStdListHeight_headerOpen();
        } else if (!$('.addStdList').hasClass("searchOpen")) {
            setAddStdListHeight_headerClose();
        }
    });

    $('#filterStdArea.tapHeader .button .filterStd .filter').click(function(){
        $('.tapHeader div#searchStd').removeClass('in');
        $('.addStdList').removeClass('searchOpen');
        $('.addStdList').toggleClass('filterOpen');

        if ($('.addStdList').hasClass("filterOpen")) {
            setAddStdListHeight_headerOpen();
        } else if (!$('.addStdList').hasClass("filterOpen")) {
            setAddStdListHeight_headerClose();
        }
    });
});
/* Filter function end */

window.onload = function() {
    $('.functionBtn .btn.approve').hide();
    $('.functionBtn .btn.reject').hide();
    var $checkboxes = $('.eventStdList input[type="checkbox"]');
    var countClubNum = $checkboxes.filter(':checked').length;

    if (countClubNum > 0) {
        $('.eventStdList').addClass('edit');
        $('.functionBtn .btn.approve').show('fast');
        $('.functionBtn .btn.reject').show('fast');
    } else {
        $('.eventStdList').removeClass('edit');
        $('.functionBtn .btn.approve').hide('fast');
        $('.functionBtn .btn.reject').hide('fast');
    }
};

/* Set event student list height start */
$(document).ready(function(){
    var deviceHeight = document.documentElement.clientHeight;
    var briefInfoHeight = document.getElementsByClassName('eventBriefInfo')[0].clientHeight;
    var x = deviceHeight - briefInfoHeight - 62;
    document.getElementsByClassName('eventStdList')[0].style.maxHeight = x + 'px';
});
/* Set event student list height end */

/* Check check box onclick start */
$(document).ready(function(){
    var $checkboxes = $('.eventStdList input[type="checkbox"]');

    $checkboxes.change(function(){
        var countClubNum = $checkboxes.filter(':checked').length;
        if (countClubNum > 0) {
            $('.eventStdList').addClass('edit');
            $('.functionBtn .btn.approve').show('fast');
            $('.functionBtn .btn.reject').show('fast');
        } else {
            $('.eventStdList').removeClass('edit');
            $('.functionBtn .btn.approve').hide('fast');
            $('.functionBtn .btn.reject').hide('fast');
        }
    });
});
/* Check check box onclick end */

/* Set student's event list height start */
$(document).ready(function(){
    var deviceHeight = document.documentElement.clientHeight;
    var stdInfo = document.getElementsByClassName('stdInfo')[0].clientHeight;
    var x = deviceHeight - stdInfo - 51;
    document.getElementsByClassName('stdEventList')[0].style.maxHeight = x + 'px';
    /*eventStdListHeight = x + 'px';*/
});
/* Set student's event list height end */

/* Input field setting start */
function setAddStdListHeight_headerClose() {
    var deviceHeight = document.documentElement.clientHeight;
    var selectedAreaHeight = document.getElementById('selectedArea').clientHeight;
    var x = deviceHeight - selectedAreaHeight - 90;
    document.getElementsByClassName('addStdList')[0].style.maxHeight = x + 'px';
}
function setAddStdListHeight_headerOpen() {
    var deviceHeight = document.documentElement.clientHeight;
    var selectedAreaHeight = document.getElementById('selectedArea').clientHeight;
    var x = deviceHeight - selectedAreaHeight - 130;
    document.getElementsByClassName('addStdList')[0].style.maxHeight = x + 'px';
}

/* Input field setting start */
function hideSelectTitleField() {
    if ($('#titleSelection').val() === '自行輸入') {
        $('#selectTitleField').hide();
        $('#inputTitleField').show();
    }
}
function hideInputTitleField() {
    $('#inputTitleField').hide();
    $('#selectTitleField').show();
}
function addEndDateField() {
    $('#inputDateField').hide();
    $('#endDateFeild').show();
}
function hideSelectRoleField() {
    if ($('#roleSelection').val() === '自行輸入') {
        $('#selectRoleField').hide();
        $('#inputRoleField').show();
    }
}
function hideInputAwardsField() {
    $('#inputRoleField').hide();
    $('#selectRoleField').show();
}
function hideSelectAwardsField() {
    if ($('#awardsSelection').val() === '自行輸入') {
        $('#selectAwardsField').hide();
        $('#inputAwardsField').show();
    }
}
function hideInputAwardsField() {
    $('#inputAwardsField').hide();
    $('#selectAwardsField').show();
}
function hideSelectCommentField() {
    if ($('#commentSelection').val() === '自行輸入') {
        $('#selectCommentField').hide();
        $('#inputCommentField').show();
    }
}
function hideInputCommentField() {
    $('#inputCommentField').hide();
    $('#selectCommentField').show();
}
function hideAllowReportingInfoList() {
    $('.allowReportingInfoList').hide();
}
function showAllowReportingInfoList() {
    $('.allowReportingInfoList').show();
}


/* Input field setting end */