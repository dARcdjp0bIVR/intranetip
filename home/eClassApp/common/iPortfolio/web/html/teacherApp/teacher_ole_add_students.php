<?php
# modifing by

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");


intranet_auth();
intranet_opendb();

$programID = IntegerSafe($_GET["programID"]);
$YearID = IntegerSafe($_POST['YearID']);
$YearClassID = IntegerSafe($_POST['YearClassID']);

//year dropdown list

$lpf_fc = new libpf_formclass();
$AcademicYearID = Get_Current_Academic_Year_ID();
$libdb = new libdb();

$year_arr = $lpf_fc->GET_CLASSLEVEL_LIST();

$html_year_selection = "<select name='YearID' id='YearID' class='selectpicker' onChange='cleanClass();reload();'>";
$html_year_selection .= "<option value=''>{$Lang['eClassApp']['iPortfolio']['AllForms']}</option>";

for($i=0, $i_max=count($year_arr); $i<$i_max; $i++)
{
    $_yID = $year_arr[$i]["YearID"];
    $_yName = $year_arr[$i]["YearName"];
    $showselected = ($YearID==$_yID)?'selected':'';
    $html_year_selection .= "<option value='{$_yID}' $showselected>{$_yName}</option>";
}

$html_year_selection .= "</select>";

//yearclass dropdown list

$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassTitle 
        FROM {$intranet_db}.YEAR y
        INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID 
        WHERE yc.YearID = {$YearID} 
        AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
        ORDER BY y.Sequence, yc.Sequence";

$yc_arr = $libdb->returnArray($sql);

$html_yearclass_selection = "<select name='YearClassID' id='YearClassID' class='selectpicker' onChange='reload()'>";
$html_yearclass_selection .= "<option value=''>{$Lang['SysMgr']['FormClassMapping']['AllClass']}</option>";

for($i=0, $i_max=count($yc_arr); $i<$i_max; $i++)
{
    $_ycID = $yc_arr[$i]["YearClassID"];
    $_ycName = $yc_arr[$i]["ClassTitle"];
    $showselected = ($YearClassID==$_ycID)?'selected':'';

    $html_yearclass_selection .= "<option value='{$_ycID}' $showselected>{$_ycName}</option>";
}
$html_yearclass_selection .= "</select>";


//students list

$cond = empty($YearID) ? "" : " AND y.YearID = '".$YearID."'";
$cond .= empty($YearClassID) ? "" : " AND ycu.YearClassID = '".$YearClassID."'";

$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
    $cond .= " AND (".getNameFieldByLang2("iu.")." LIKE '%".$inputSearch."%')";
}

$sql = "SELECT 
           iu.UserID, 
           ".getNameFieldByLang2("iu.")." as StudentName,
           ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." as ClassName,
           ycu.ClassNumber
        FROM {$intranet_db}.YEAR y
           INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID
           INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON ycu.YearClassID = yc.YearClassID 
           INNER JOIN {$intranet_db}.INTRANET_USER iu ON ycu.UserID = iu.UserID 
        where 
           yc.AcademicYearID = '".$AcademicYearID."'
           $cond
        ORDER BY 
           y.Sequence, yc.Sequence, ycu.ClassNumber";

$student_arr = $libdb->returnArray($sql);

$student_array = array();
$student_name_array = array();
$student_classname_array = array();
$student_classnumber_array = array();

for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{
    $StudentNameJS = '"'.$student_arr[$i]['StudentName'].'"';
    $ClassNameJS = '"'.$student_arr[$i]['ClassName'].'"';
    $ClassNumberJS = '"'.$student_arr[$i]['ClassNumber'].'"';

    $student_array[] = $student_arr[$i]['UserID'];
    $student_name_array[] = $student_arr[$i]['StudentName'];
    $student_classname_array[] = $student_arr[$i]['ClassName'];
    $student_classnumber_array[] = $student_arr[$i]['ClassNumber'];

    $studentsList .=  "  <li class='student'>
                            <div class='stdName'>{$student_arr[$i]['StudentName']}</div>
                            <span class='classClassNo'>{$student_arr[$i]['ClassName']}-{$student_arr[$i]['ClassNumber']}</span>
                            <div class='form-check form-check-inline'>
                                <label class='form-check-label'>
                                    <input class='form-check-input' type='checkbox' id='sid_{$student_arr[$i]['UserID']}' onclick='addstudent({$student_arr[$i]['UserID']},$StudentNameJS,$ClassNameJS,$ClassNumberJS)'>
                                    <span>&nbsp;</span>
                                </label>
                            </div>
                        </li>";

}

$student_array_string = implode(";",$student_array);
$student_name_array_string = implode(";",$student_name_array);
$student_classname_array_string = implode(";",$student_classname_array);
$student_classnumber_array_string = implode(";",$student_classnumber_array);

?>


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css">
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<script>
    var array_string = '<?= $student_array_string?>' ;
    var student_array = array_string.split(';');
    var name_array_string = '<?= $student_name_array_string?>' ;
    var student_name_array = name_array_string.split(';');
    var classname_array_string = '<?= $student_classname_array_string?>' ;
    var student_classname_array = classname_array_string.split(';');
    var classnumber_array_string = '<?= $student_classnumber_array_string?>' ;
    var student_classnumber_array = classnumber_array_string.split(';');

    var selectedStudents = new Array();

    $(document).ready(function(){


    });

    function cleanClass(){
        $("#YearClassID").val("");
    }


    function addstudent(uid,studentname,classname,classnumber) {

        if( $("#sid_"+uid)[0].checked==true){

            if(!selectedStudents.includes(uid)){

                var showList = "<li id='li_"+uid+"'><div class='stdName'>"+studentname+"</div><span class='classClassNo'>"+classname+"-"+classnumber+"</span><a href='javascript:deleteStudent("+uid+")'><span class='closeBtn'><i class='fas fa-times'></i></span></a></li>";
                $("#stdAddedList").show();
                $("#stdAddedList").append(showList);
                selectedStudents.push(uid);
                $("#selectedCount").text(selectedStudents.length);

            }
        }
    }

    function deleteStudent(uid){

        var index = selectedStudents.indexOf(uid);

        if (index > -1) {

            $("#li_"+uid).remove();
            selectedStudents.splice(index, 1);
            $("#selectedCount").text(selectedStudents.length);

        }
    }

    function addallstudents(){

        if( $("#sid_all")[0].checked==true) {


            for(var i=0; i<student_array.length;i++) {

                var uid = Number(student_array[i]);
                var studentname = student_name_array[i];
                var classname = student_classname_array[i];
                var classnumber = student_classnumber_array[i];

                var index = selectedStudents.indexOf(uid);

                // alert(selectedStudents);
                // alert(uid+":"+index);
                if (index == -1) {
                    //student not selected
                    var showList = "<li id='li_"+uid+"'><div class='stdName'>"+studentname+"</div><span class='classClassNo'>"+classname+"-"+classnumber+"</span><a href='javascript:deleteStudent("+uid+")'><span class='closeBtn'><i class='fas fa-times'></i></span></a></li>";
                    $("#stdAddedList").show();
                    $("#stdAddedList").append(showList);
                    selectedStudents.push(uid);
                }
            }
            $("#selectedCount").text(selectedStudents.length);
        }
    }

    function reload(){
        document.form1.submit();
    }

    function submitToEdit() {
        var studentsString = selectedStudents.toString();
        $("#studentsString").val(studentsString);
        document.form1.action="teacher_ole_add_students2.php";
        document.form1.submit();
    }

</script>
<body onLoad="setAddStdListHeight_headerOpen()">
<form name="form1" method="POST">

<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"><div class="headerIcon"><a onclick="window.history.go(-1)"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle" class="withFunction"><?=$Lang['eClassApp']['iPortfolio']['Selected']?> <span id="selectedCount">0</span> <?=$Lang['MassMailing']['NoOfStudents']?></div>
        <div id="button">
            <div class="headerIcon"><a data-toggle="modal" data-target="#saveStdAdd"><i class="fas fa-check"></i></a></div>
        </div>
    </nav>
    <div id="content" class="addStdArea">
        <div id="selectedArea">
            <ul class='stdAddedList' id='stdAddedList' style='display: none;'></ul>
        </div>
        <nav class="navbar tapHeader" id="filterStdArea">
            <div class="button">
                <div class="headerIcon"><a class="searchStd" role="button" data-toggle="collapse" href="#searchStd" aria-expanded="false" aria-controls="search"><i class="fas fa-search"></i></a></div>
                <div class="headerIcon"><a class="filterStd" role="button" data-toggle="collapse" href="#filterStd" aria-expanded="false" aria-controls="filter"> <span class="filter"></span> </a></div>
            </div>
            <div id="searchStd" class="collapse">
                <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo stripslashes($inputSearch);?>">
            </div>
            <div class="collapse in" id="filterStd">
                <div class="grade">
                    <?=$html_year_selection?>
                </div>
                <div class="class" id="filterClassField">
                    <?=$html_yearclass_selection?>
                </div>
            </div>
        </nav>
        <ul class="addStdList filterOpen">
            <li class="student selectAll">
                <div class="stdName"><?=$Lang['SysMgr']['RoleManagement']['AllStudent']?></div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" id="sid_all" onclick="addallstudents()">
                        <span>&nbsp;</span>
                    </label>
                </div>
            </li>
            <?=$studentsList?>
        </ul>
    </div>
</div>
 <div class="modal fade" id="saveStdAdd" tabindex="-1" role="dialog" aria-labelledby="saveStdAdd" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header edit">
                <h5 class="modal-title" id="modelLabel"><?=$Lang['eClassApp']['iPortfolio']['AssignStudentToProgramme']?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">
							<div class="icon"></div>
						</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a href="teacher_ole_programme_student_list.php?programID=<?=$programID?>"><button type="button" class="btn"><?=$Lang['eClassApp']['iPortfolio']['ViewThisProgramme']?></button></a>
                <a href="javascript:submitToEdit();"><button type="button" class="btn"><?=$Lang['eClassApp']['iPortfolio']['AddCommonRecordForStudent']?></button></a>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value = "<?=$programID?>" name="programID"  id="programID">
<input type="hidden" value = "<?=$studentsString?>" name="studentsString"  id="studentsString">
</form>
</body>
</html>
