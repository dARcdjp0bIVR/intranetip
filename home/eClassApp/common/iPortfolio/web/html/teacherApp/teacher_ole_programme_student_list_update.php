<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
intranet_auth();
intranet_opendb();

$programID = IntegerSafe($_POST['programID']);
$record_id = IntegerSafe($_POST['record_id']);

$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);

if (is_array($RecordArr))
{
    $RecordList = implode($RecordArr, ",");
}
else
{
    $RecordList = $record_id;
}
if (!isset($submitStatus))
    $submitStatus = 2;

# RecordStatus = 1 (only pending record can be deleted)
//$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ApprovedBy = IF(IFNULL(ApprovedBy, 0) = 0 OR RecordStatus <> '{$approve}', '{$UserID}', ApprovedBy), RecordStatus = '$approve', ProcessDate = now(), ModifiedDate = now() WHERE RecordID IN ($RecordList)";
$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ApprovedBy = {$UserID} , RecordStatus = '$submitStatus', ProcessDate = now(), ModifiedDate = now() WHERE RecordID IN ($RecordList)";

$LibPortfolio->db_db_query($sql);

intranet_closedb();

header("Location: teacher_ole_programme_student_list.php?programID=$programID");


?>
