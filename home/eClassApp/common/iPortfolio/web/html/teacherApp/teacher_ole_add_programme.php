<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("iportfolio_default.html");

$programID = IntegerSafe($_GET["programID"]);

// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libpf_slp(); // for libwordtemplates_ipf

$CHECKED_STR = " checked = 'checked' ";
$DISPLAY_STR = " style = 'display:none;' ";

# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;

# template drop down list
for ($f=0; $f<sizeof($LibWord->file_array); $f++)
{
    $id = $LibWord->file_array[$f][0];
    unset($tempArr);
    unset($WordListArr);
    $tempArr = $LibWord->getWordList($id);
    $WordListArr[] = array("", " --- ".$button_select." --- ");
    for ($i=0; $i<sizeof($tempArr); $i++) {
        $TempTitleArray = explode("\t", $tempArr[$i]);
        $p_title = $TempTitleArray[0];
        $WordListArr[] = array(str_replace("&amp;", "&", $p_title), undo_htmlspecialchars($p_title));
    }
    $WordListSelection[] = array($id, returnSelection($WordListArr , "", "WordList", "onChange=\"document.form1.title.value = this.value \""));
}
# template drop down list

# float list arr
for ($f=0; $f<sizeof($LibWord->file_array); $f++) {

    $id = $LibWord->file_array[$f][0];

    unset($tempArr);
    $tempArr = $LibWord->getWordList($id);
    for ($i=0; $i<sizeof($tempArr); $i++) {
        $TempTitleArray = explode("\t", $tempArr[$i]);
        $p_title = $TempTitleArray[0];
        $ListArr[$id][] = str_replace("&amp;", "&", undo_htmlspecialchars($p_title));

        # preset ELE of the corresponding title
        $ListELEArr[$id][] = $TempTitleArray[1];
    }
}

if($programID!="")
{
    $DataArr = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($programID);

    if (is_array($DataArr)) {
        $startdate = $DataArr['StartDate'];
        $enddate = $DataArr['EndDate'];
        $engTitle = $DataArr['Title'];
        $chiTitle = $DataArr['TitleChi'];
        $category = $DataArr['Category'];
        $subCategoryID = $DataArr['SubCategoryID'];
        $ELE = $DataArr['ELE'];
        $organization = $DataArr['Organization'];
        $engDetails = $DataArr['Details'];
        $chiDetails = $DataArr['DetailsChi'];
        $SchoolRemarks = $DataArr['SchoolRemarks'];
        $input_date = $DataArr['InputDate'];
        $modified_date = $DataArr['ModifiedDate'];
        $period = $DataArr['Period'];
        $int_ext = $DataArr['IntExt'];
        $canJoin = $DataArr['CanJoin'];
        $canJoinStartDate = $DataArr['CanJoinStartDate'];
        $canJoinEndDate = $DataArr['CanJoinEndDate'];
        $userName = $DataArr['UserName'];
        $autoApprove = $DataArr['AUTOAPPROVE'];
        $academicYearID = $DataArr['AcademicYearID'];
        $ytID = $DataArr['YearTermID'];
        $joinableYear = $DataArr['JoinableYear'];
        $maximumHours = $DataArr['MaximumHours'];
        $defaultHours = $DataArr['DefaultHours'];
        $compulsoryFields = $DataArr['CompulsoryFields'];
        $defaultApprover = $DataArr['DefaultApprover'];
        $IsSAS = $DataArr['IsSAS'];
        $IsOutsideSchool = $DataArr['IsOutsideSchool'];
        // Added: 2015-01-12
        $SASCategory = $DataArr['SASCategory'];
        $SASPoint = $DataArr['SASPoint'];
    }

    // [2015-0324-1420-39164] get Teacher-In-Charge
    if($sys_custom['iPortfolio_ole_record_tic']){
        $teacher_PIC = $lpf->GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID($programID);
    }

    # Get all categories if record category is disabled
    $OLE_Cat = $lpf->get_OLR_Category();
    $file_array = $LibWord->setFileArr();


    if(!array_key_exists($category, $OLE_Cat))
    {
        $file_array = $LibWord->setFileArr(true);
    }

    # get ELE Array
    $ELEArr = explode(",", $ELE);
    $ELEArr = array_map("trim", $ELEArr);

} else
{
    $NewCategoryArray = $lpf->get_OLR_Category();

    $file_array = $LibWord->setFileArr();

    // get the least category id
    if (is_array($NewCategoryArray)) {
        foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
        {
            $TmpCategoryID = $CategoryID;
            break;
        }
    }

    $JScriptID = "jArr".$TmpCategoryID;
    $JELEScriptID = "jELEArr".$TmpCategoryID;
    if($ELE!="")
    {
        $ELEArr = array($ELE);
    }
}

$canJoinCheckedYes = "";
$canJoinCheckedNo = "";
$joinPeriodDisplayStatus = "";
if ($canJoin) {
    $canJoinCheckedYes = $CHECKED_STR;
} else {
    $canJoinCheckedNo = $CHECKED_STR;
    $joinOptionsDisplayStatus = $DISPLAY_STR;
}
if ($autoApprove) {
    $autoApproveCheckedYes = $CHECKED_STR;
} else {
    $autoApproveCheckedNo = $CHECKED_STR;
}

// Checking the compulsory fields
if (!empty($compulsoryFields)) {
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]) !== false) {
        $cpsry_hours_checked = $CHECKED_STR;
    }
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]) !== false) {
        $cpsry_role_checked = $CHECKED_STR;
    }
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]) !== false) {
        $cpsry_achievement_checked = $CHECKED_STR;
    }
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]) !== false) {
        $cpsry_attachment_checked = $CHECKED_STR;
    }
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]) !== false) {
        $cpsry_details_checked = $CHECKED_STR;
    }
    if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]) !== false) {
        $cpsry_approved_by_checked = $CHECKED_STR;
    }
}


//****************** Handle form *******************//

//************ Title ***********//
if($title==""){
    $titleInfoMissing = "infoMissing";
}
$titleFieldDisplay ="<span class='label mandatory'>{$ec_iPortfolio['title']}</span>
		        	 <div id='inputTitleFieldNew' class='fieldInput {$titleInfoMissing}'>
					    <input type='text' class='form-control inputField' id='englishTitle' name='englishTitle' placeholder='' value='{$engTitle}'>
					 </div>";

//************ Date ***********//
if($startdate!="" && $enddate!="")
{
    $add_link_style = "style='display: none;'";
    $enddate_table_style = "style='display: block;'";
}
else
{
    $add_link_style = "style='display: block;'";
    $enddate_table_style = "style='display: none;'";
}

if($startdate==""){
    $dateInfoMissing = "infoMissing";
}

$dateFieldDisplay ="<span class='label mandatory'>{$ec_iPortfolio['date']}</span>
                    <div id='selectDateField' class='fieldInput date {$dateInfoMissing}'>
                      <div class='input-group date datetimepicker' >
                      <input type='text' class='form-control inputField' id='startdate' name='startdate' value='{$startdate}' />
                      <span class='input-group-addon'> <i class='fa fa-calendar-o' aria-hidden='true'></i> </span> </div>
                      <div id='inputDateField' $add_link_style>
                        <a href='#' onClick='addEndDateField()'>{$ec_iPortfolio['add_enddate']}</a> </div>
                      <div id='endDateFeild' $enddate_table_style>
                        <table>
                          <tr>
                            <td><span>{$profiles_to}</span></td>
                            <td><div class='input-group date datetimepicker'>
                                <input type='text' class='form-control inputField' id='enddate' name='enddate' value='{$enddate}' />
                                <span class='input-group-addon'> <i class='fa fa-calendar-o' aria-hidden='true'></i> </span> </div></td>
                          </tr>
                        </table>
                      </div>
                    </div>";

//************ Category ***********//

//handle category default
$defaultSelectArray = array('-9',$Lang['General']['PleaseSelect'],'');   //insert this array for default select category "--"
array_unshift($file_array, $defaultSelectArray);  //insert this array for default select category "--"
$category = (!isset($category) || $category=="") ? -9 : $category;

$categoryField = $linterface->GET_SELECTION_BOX($file_array, "class='selectpicker' name ='category' id ='category' onChange=' changeSubCategory();refreshSelectpicker();' ","",$category);

if($category==-9||$category==""){
    $categoryInfoMissing = "infoMissing";
}

$categoryFieldDisplay ="<span class='label mandatory'>{$ec_iPortfolio['category']}</span>
                        <div id='selectCategoryField' class='fieldInput {$categoryInfoMissing}'>$categoryField</div>";

//************ Sub Category ***********//
$subCategoryArray = $lpf->getSubCategory($category);

array_unshift($subCategoryArray, array("-9", $Lang['General']['PleaseSelect']));
$subCategoryField = $linterface->GET_SELECTION_BOX($subCategoryArray, "class='selectpicker' id ='subCategory' name ='subCategory' onChange='refreshSelectpicker(); '","", $subCategoryID);

$subCategoryFieldDisplay ="<span class='label'>{$ec_iPortfolio['sub_category']}</span>
                               <div id='selectSubCategoryField' class='fieldInput'>$subCategoryField</div>";

//************ School Year ***********//

if($academicYearID==-9||$academicYearID==""){
    $academicYearIDInfoMissing = "infoMissing";
}

$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
array_unshift($academic_year_arr, array("-9", $Lang['General']['PleaseSelect']));
$academicYearID = (!isset($academicYearID) || $academicYearID=="") ? -9 : $academicYearID;
$ay_selection_html = $linterface->GET_SELECTION_BOX($academic_year_arr, "class='selectpicker' id ='academicYearID' name ='academicYearID' ","", $academicYearID);

$academicYearFieldDisplay ="<span class='label mandatory'>{$ec_iPortfolio['year']}</span>
                        <div id='selectCategoryField' class='fieldInput {$academicYearIDInfoMissing}'>$ay_selection_html</div>";

//************ Component ***********//

$DefaultELEArray = $lpf->GET_ELE();

foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
{
    $checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";

    $ELEList .= "<div class='item'>
	                    <div class='form-check form-check-inline'>
	                    <label class='form-check-label'>
	              	       <input class='form-check-input' type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked>
	              	       <span class='text'>".$ELE_Name."</span></label>
	                    </div>
	                 </div>";
}

$ComponentFieldDisplay ="<span class='label'>{$ec_iPortfolio['ele']}</span>
                         <div id='selectComponentField' class='fieldInput '>$ELEList</div>";


//************ Organization ***********//
$OrganizationFieldDisplay = "<span class='label'>{$ec_iPortfolio['organization']}</span>
                             <div id='inputPartnerField' class='fieldInput'>
                               <input type='text' class='form-control inputField' id='organization' name='organization' placeholder='' value='{$organization}'>
                             </div>";

//************ Details ***********//

$DetailsFieldDisplay = "<span class='label'>{$ec_iPortfolio['details']}</span>
                        <div id='inputPartnerField' class='fieldInput'>
                           <input type='text' class='form-control inputField' id='englishDetails' name='englishDetails' placeholder='' value='{$engDetails}'>
                        </div>";



//************ School Remarks ***********//

$SchoolRemarksFieldDisplay = "<span class='label'>{$ec_iPortfolio['school_remarks']}</span>
                             <div id='inputPartnerField' class='fieldInput'>
                               <input type='text' class='form-control inputField' id='SchoolRemarks' name='SchoolRemarks' placeholder='' value='{$SchoolRemarks}'>
                             </div>";

//************ Allow Participation Reporting ***********//
//************ Applicable Form(s) ***********//
//classes
$fcm = new form_class_manage();
$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_arr = array();
for($i=0; $i<count($yearClassArr); $i++)
{
    $year_arr[] = array($yearClassArr[$i]["YearID"], $yearClassArr[$i]["YearName"]);
}
$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr))));
$year_selection_html = "<div class='item'>
                           <div class='form-check form-check-inline'>
                             <label class='form-check-label'>
                               <input class='form-check-input' type='checkbox' name='yearMaster' id='yearMaster' onClick='jCHECK_ALL_YEAR(this)'>
                                   <span class='text' for='yearMaster'>".$i_general_all."</span></div></div>";

for($i=0; $i<count($year_arr); $i++)
{
    if($i==0){
        $year_selection_html .= "<table>";
    }
    if ($i%3==0) {
        $year_selection_html .= "<tr>";
    }

    $t_year_id = $year_arr[$i][0];
    $t_year_name = $year_arr[$i][1];
    $t_id = "year_".$i;

    $t_checked = (strpos($joinableYear, $t_year_id) !== false) ? $CHECKED_STR : "";

    $year_selection_html .= "<td><div class='item'><div class='form-check form-check-inline'><label class='form-check-label'><input type='checkbox' name='Year[]' id='".$t_id."' value='".$t_year_id."' ".$t_checked."><span class='text'>".$t_year_name."</span></label></div></div></td>";

    if ($i%3==2) {
        $year_selection_html .= "</tr>";
    }
    if(count($year_arr)-$i==1){
        $year_selection_html .= "</table>";
    }
}
//approver

$TeacherArray = $lpf->GET_ALL_TEACHER_IPF();
$defaultSelectArray = array('-9',$Lang['General']['PleaseSelect'],'');
array_unshift($TeacherArray, $defaultSelectArray);

$approverField = $linterface->GET_SELECTION_BOX($TeacherArray, "class='selectpicker' id ='request_approved_by' name ='request_approved_by' onChange='refreshSelectpicker();' ","",$requestApprovedBy);

$approverFieldDisplay ="<span class='label'>{$Lang['iPortfolio']['preferred_approver']}</span>
                        <div id='selectApproverField' class='fieldInput'>$approverField</div>";



//************ OEA ***********//


# refer: home/portfolio/oea/student/task/oea_new.php!!!!

$liboea_setting = new liboea_setting();
$_objOEA_ITEM = new liboea_item();
$liboea = new liboea();
$liboea_setting_search = new liboea_setting_search($liboea_setting);
$oeaAwardBearing=="N";

if ($programID!="")
{
    $IsOEAMapped = $_objOEA_ITEM->loadRecordFromOLEMapping($programID);

    $oeaAwardBearing = $_objOEA_ITEM->getOEA_AwardBearing();

    if($oeaAwardBearing == "Y") {
        $withAward = $CHECKED_STR;
    }
    else {
        $withoutAward = $CHECKED_STR;
    }

    $OEAMappingChecked = ($IsOEAMapped) ? " checked='checked' " : "";

    //handle title choose


} else
{
    $OEAMappingChecked = "";
    $withoutAward = $CHECKED_STR;
}

if($engTitle!=""){
    $liboea_setting_search->setCriteriaTitle($engTitle);
    $liboea_setting_search->getSuggestResult();
    $searchResultTitle = $liboea_setting_search->getSearchResultTitle();
    $numOfSuggestion = sizeof($searchResultTitle);

    $OEATitleFieldDisplay .= "<p>{$Lang['iPortfolio']['OEA']['OLE_Title']} : {$engTitle}<br />{$Lang['iPortfolio']['OEA']['NoOfItemsSuggested']} : {$numOfSuggestion}</p>";
    if($numOfSuggestion>0){
        $OEATitleFieldDisplay .= "<div id='matchOEATitleField' class='fieldInput'>";
        for($i=0; $i<$numOfSuggestion; $i++)
        {
            $OEATitleFieldDisplay .= "<div class='item'>
                                         <div class='form-check form-check-inline'>
                                            <label class='form-check-label'>
                                                <input class='form-check-input' name='matchOEATitleRadios' type='radio' id='ItemCode_".$searchResultTitle[$i][0]."' value='".$searchResultTitle[$i][0]."' >
                                                <span class='text'>[{$searchResultTitle[$i][0]}] {$searchResultTitle[$i][1]}</span>
                                            </label>
                                         </div>
                                      </div>";
        }

        $OEATitleFieldDisplay .= "</div>";
    }

}

?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css">
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>


<body>
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"><div class="headerIcon"><a onclick="window.history.go(-1)"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle" class="withFunction"><?=$Lang['eClassApp']['iPortfolio']['NewOLE']?></div>
        <div id="button">

        </div>
    </nav>
    <form class="applicationForm" name="form1" method="post" action="teacher_ole_add_programme_update.php" enctype="multipart/form-data"  onSubmit="return checkform(this)">

    <div id="content" class="addProgrammePage">
        <ul class="progInfoList">
            <li>
                <?php echo $titleFieldDisplay;?>
            </li>
            <li>
                <?php echo $dateFieldDisplay;?>
            </li>
            <li>
                <?php echo $categoryFieldDisplay;?>
            </li>
            <li>
                <?php echo $subCategoryFieldDisplay;?>
            </li>
            <li>
                <?php echo $academicYearFieldDisplay;?>
            </li>
            <li>
                <?php echo $ComponentFieldDisplay;?>
            </li>
            <li>
                <?php echo $OrganizationFieldDisplay;?>
            </li>
            <li>
                <?php echo $DetailsFieldDisplay;?>
            </li>
            <li>
                <?php echo $SchoolRemarksFieldDisplay;?>
            </li>
            <li>
                <span class="label"><?=$ec_iPortfolio['SLP']['AllowStudentsToJoin']?></span>
                <div id="selectAllowReportingField" class="fieldInput">
                    <div class="item">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" name="allowStudentsToJoin" type="radio" id="allowBox" value="1" onClick="showAllowReportingInfoList()" <?= $canJoinCheckedYes ?>>
                                <span class="text"><?=$ec_iPortfolio['SLP']['Yes']?></span>
                            </label>
                            <!-- hidden div 'allowReportingInfoList' if not allow student reporting -->
                            <ul class="allowReportingInfoList">
                                <li>
                                    <span class="label"><?=$ec_iPortfolio['SLP']['JoinPeriod']?></span>
                                    <table>
                                        <tr>
                                            <td><span><?=$Lang['General']['From']?></span></td>
                                            <td><div class="input-group date datetimepicker">
                                                    <input type="text" id="jp_periodStart" name="jp_periodStart" class="form-control inputField" value="<?=$canJoinStartDate?>"/>
                                                    <span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span> </div></td>
                                        </tr>
                                        <tr>
                                            <td><span><?=$Lang['General']['To']?></span></td>
                                            <td>
                                                <div class="input-group date datetimepicker">
                                                    <input type="text" id="jp_periodEnd" name="jp_periodEnd" class="form-control inputField" value="<?=$canJoinEndDate?>"/>
                                                    <span class="input-group-addon"> <i class="fa fa-calendar-o" aria-hidden="true"></i> </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <span class="label mandatory"><?=$ec_iPortfolio['SLP']['JoinableYear']?></span>
                                    <div id="selectGradeField" class="fieldInput">
                                        <?php echo $year_selection_html;?>
                                    </div>
                                </li>
                                <li>
                                    <span class="label"><?=$ec_iPortfolio['SLP']['MaximumHours']?></span>
                                    <div id="maxHourField" class="fieldInput">
                                        <input type="text" class="form-control inputField" id="maximumHours" name="maximumHours" placeholder="" value="<?=$maximumHours?>">
                                    </div>
                                </li>
                                <li>
                                    <span class="label"><?php echo $ec_iPortfolio['SLP']['DefaultHours']; ?></span>
                                    <div id="defaultHourField" class="fieldInput">
                                        <input type="text" class="form-control inputField" id="defaultHours" name="defaultHours" placeholder="" value="<?php echo $defaultHours; ?>">
                                    </div>
                                </li>
                                <li>
                                    <span class="label"><?=$ec_iPortfolio['SLP']['CompulsoryFields']?></span>
                                    <div id="selectCompulsoryField" class="fieldInput">
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="compulsoryFields[hours]" id="cpsry_hours" value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]?>" <?=$cpsry_hours_checked?>>
                                                    <span class="text"><?=$ec_iPortfolio['hours']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="compulsoryFields[role]" id="cpsry_role" value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]?>" <?=$cpsry_role_checked?>>
                                                    <span class="text"><?=$ec_iPortfolio['ole_role']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="compulsoryFields[achievement]" id="cpsry_achievement" value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]?>" <?=$cpsry_achievement_checked?>>
                                                    <span class="text"><?=$ec_iPortfolio['achievement']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="compulsoryFields[attachment]" id="cpsry_attachment" value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]?>" <?=$cpsry_attachment_checked?>>
                                                    <span class="text"><?=$ec_iPortfolio['attachment']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="compulsoryFields[approved_by]" id="cpsry_approved_by" value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]?>" <?=$cpsry_approved_by_checked?>>
                                                    <span class="text"><?=$ec_iPortfolio['approved_by']?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <?php echo $approverFieldDisplay;?>
                                </li>
                                <li>
                                    <span class="label"><?=$ec_iPortfolio['SLP']['AutoApprove']?></span>
                                    <div id="selectAutoApprovalField" class="fieldInput">
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="autoApprove" type="radio" id="autoApprovalY" value="1" <?= $autoApproveCheckedYes ?>>
                                                    <span class="text"><?=$ec_iPortfolio['SLP']['Yes']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="autoApprove" type="radio" id="autoApprovalN" value="0" <?= $autoApproveCheckedNo ?>>
                                                    <span class="text"><?=$ec_iPortfolio['SLP']['No']?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" name="allowStudentsToJoin" type="radio" id="notAllowBox" value="0" onClick="hideAllowReportingInfoList()" <?= $canJoinCheckedNo ?>>
                                <span class="text"><?=$ec_iPortfolio['SLP']['No']?></span>
                            </label>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div id="selectOEAField" class="fieldInput">
                    <div class="item">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" id="oea" name="oea" value="1" onclick="changeOeaCheckboxes()" <?=$OEAMappingChecked?>>
                                <span class="text"><?=$Lang['iPortfolio']['OEA']['PreMapping']?><small>(<?= $Lang['iPortfolio']['OEA']['PreMappingNotes'] ?>)</small></span>
                            </label>
                            <!-- hidden div 'mapToOEAList' if not map to OEA -->
                            <ul class="mapToOEAList">
                                <li>
                                    <span class="label mandatory"><?=$Lang['iPortfolio']['OEA']['Title']?></span>
                                    <div id="selectOEATitleField" class="fieldInput">
                                        <select id="OEATitleSelection" name="OEATitleSelection" class="selectpicker" title="<?=$Lang['eClassApp']['iPortfolio']['SelectOEATitlePairingMethod']?>"  onChange="modalShow()">
                                            <option value="1" ><?=$Lang['iPortfolio']['OEA']['SuggestedItemsCheck']?></option>
                                            <option value="2"><?=$Lang['iPortfolio']['OEA']['UseOleName']?></option>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Date']?></span>
                                    <span class="text auto"><?=$Lang['iPortfolio']['OLE_OEA']['Date']?></span>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Category']?></span>
                                    <span class="text auto"><?=$Lang['eClassApp']['iPortfolio']['ReadBySystem']?></span>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Role']?></span>
                                    <span class="text auto"><?=$Lang['iPortfolio']['OLE_OEA']['Role']?></span>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Achievement']?> <small>(<?=$Lang['iPortfolio']['OEA']['OEA_AwardBearing']?>)</small></span>
                                    <div id="selectAwardBearingField" class="fieldInput">
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="awardBearing" type="radio" id="awardBearingY" value="Y" <?=$withAward?>>
                                                    <span class="text"><?=$Lang['General']['Yes']?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="awardBearing" type="radio" id="awardBearingN" value="N" <?=$withoutAward?>>
                                                    <span class="text"><?=$Lang['General']['No']?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Participation']?></span>
                                    <span class="text auto"><?=$Lang['iPortfolio']['OLE_OEA']['Nature']?></span>
                                </li>
                                <li>
                                    <span class="label"><?=$Lang['iPortfolio']['OEA']['Description']?></span>
                                    <span class="text auto"><?=$Lang['iPortfolio']['OLE_OEA']['Description']?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <p class="remarks"><?=$ec_iPortfolio['mandatory_field_description']?></p>
        <div class="row submitBtn">
            <button class="btn btn-primary" ><?=$Lang['Btn']['Next']?></button>
        </div>
    </div>
    </div>
    <div class="modal fade" id="OEA" tabindex="-1" role="dialog" aria-labelledby="OEA" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header edit">
                    <h5 class="modal-title" id="modelLabel"><?=$Lang['iPortfolio']['OEA']['SuggestedItemsCheck']?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                <div class="icon"></div>
                            </span>
                    </button>
                </div>
                <div class="modal-body" id="OEATitleFieldDisplayDiv">
                   <?=$OEATitleFieldDisplay?>
                </div>
                <div class="modal-footer">
                    <a href="javascript:changeOEATitle();"><button type="button" class="btn"><?=$Lang['Btn']['Confirm']?></button></a>
                </div>
            </div>
        </div>
    </div>

<input type="hidden" name="ItemCode" id="ItemCode" value="<?=$ItemCode?>" >
<input type="hidden" name="programID" id="programID" value="<?=$programID?>" >

</form>
</body>
</html>
<script type="text/javascript">

    $(document).ready(function(){

        if($('input[name="allowStudentsToJoin"]:checked').val()==1){
            $('.allowReportingInfoList').show();
        }else{
            $('.allowReportingInfoList').hide();
        }

        if($('input[name="oea"]:checked').length==0){
            $('.mapToOEAList').hide();
        }else{
            $('.mapToOEAList').show();
        }


        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
        }).on('dp.change', function (e) {
            $(this).parent().removeClass('infoMissing');
        });

        $('.datetimepicker-time').datetimepicker({
            sideBySide: true
        });
    });

    function changeSubCategory() {

        var catID = $("select[name='category']").val();
        if (catID==null) {return false;}
        $("[id*=listContent]").hide();
        $.ajax({
           type: "GET",
           url: "ajax_get_subcategory.php",
           async: false,
           data: "ActionType=Change_Sub_Category&CategoryID="+catID+"&SubCategoryID=<?=$subCategoryID?>",
            success: function (xml) {
                $("#selectSubCategoryField").html($(xml).find("subCategorySel").text());
            }
        });
        $('.selectpicker').selectpicker('render');

    }

    function refreshSelectpicker(){
        $('.selectpicker').selectpicker('render');
    }

    function changeOeaCheckboxes() {
        if($('input[name="oea"]:checked').length==1){
            $('.mapToOEAList').show();
        }else{
            $('.mapToOEAList').hide();
        }
    }

    function Show_SuggestionList()
    {
        var ole_title = $('input#englishTitle').val();

        var ItemCode =  $('#ItemCode').val();

        if (ole_title=="")
        {
            alert("<?=$Lang['iPortfolio']['OEA']['MissingOLETitle']?>");
            document.form1.englishTitle.focus();
            return ;
        }

        var currentRecordID = $('input#currentRecordID').val();
        $.post(
            "ajax_get_suggestion_list.php",
            {
                currentRecordID: currentRecordID,
                ole_title: ole_title,
                ItemCode: ItemCode
            },
            function(ReturnData)
            {
                $('#OEATitleFieldDisplayDiv').html(ReturnData);
            }
        );
        $('#OEA').modal('show')
    }

    function modalShow(){

        if($("select[name='OEATitleSelection']").val() == 1 ){
            Show_SuggestionList()
        }
    }

    function changeOEATitle(){

        $('#ItemCode').val($('input[name="matchOEATitleRadios"]:checked').val());
        $('#OEA').modal('hide')

        // var selectTitleValue = $('input[name="matchOEATitleRadios"]:checked').val();
        // $('#showTitle_'+selectTitleValue).text();

    }

    function jCHECK_ALL_YEAR(jParcheckObj)
    {
        var checked = jParcheckObj.checked;
        $("input[name='Year[]']").each(function(){
            this.checked = checked;
        });
    }

    function checkform(formObj)
    {
        var d = new Date();
        var curr_year = d.getFullYear();



        if(formObj.englishTitle.value=="")
        {
            alert("<?=$ec_warning['title']?>");
            formObj.englishTitle.focus();
            return false;
        }

        if(formObj.startdate.value=="")
        {
            formObj.startdate.focus();
            alert("<?=$ec_warning['date']?>");
            return false;
        }
        else
        {
            if(formObj.enddate.value!="")
            {
                if(formObj.startdate.value>formObj.enddate.value)
                {
                    formObj.enddate.focus();
                    alert("<?=$w_alert['start_end_time2']?>");
                    return false;
                }
            }

            _startDate = formObj.startdate.value;
            _endDate = formObj.enddate.value;

            var startYear = "";
            if(_startDate.length > 4){
                startYear = _startDate.substring(0,4);
            }

            var endYear   = "";
            if(_endDate.length > 4){
                endYear   = _endDate.substring(0,4);
            }else{
                //if END date not set , set the end year default to current year for checking
                endYear = curr_year;
            }

            <?php //if input year more than current Year 5 year (HardCode) , alert message
            ?>
            if((startYear - curr_year > 5) || (endYear  - curr_year > 5)){
                alert("<?=$Lang['iPortfolio']['alertYearExceed']?>");
                return false;
            }
        }

        if ($("select[name='category']").val() == "-9") {
            alert("<?=$ec_warning['category']?>");
            return false;
        }

        if ($("select[name='academicYearID']").val() == "-9") {
            alert("<?=$Lang['iPortfolio']['Student']['Report']['PlsSelectAcademicYear']?>");
            return false;
        }

        if($('input[name="allowStudentsToJoin"]:checked').val()==1){

            if($('[name="Year[]"]:checked').length==0){
                alert("<?=$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']?>");
                return false;
            }
        }

        if($('input[name="oea"]:checked').length>0){

            if($('#OEATitleSelection').val()!=1&&$('#OEATitleSelection').val()!=2){
                alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['ChooseOeaProgram']?>");
                return false;
            }
        }

        return true;
    }
</script>