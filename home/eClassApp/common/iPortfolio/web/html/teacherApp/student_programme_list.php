<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$studentID = IntegerSafe($_GET["studentID"]);
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$filterSchoolYear = IntegerSafe($_GET["filterSchoolYear"]);
$filterCategory = IntegerSafe($_GET["filterCategory"]);
isset($tab)?$tab:$tab="approved_content";
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "id='filterSchoolYear' class='selectpicker' name='filterSchoolYear' onChange='document.form1.submit()' color='black'", $filterSchoolYear, 1, 0, $i_Attendance_AllYear, 2);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("id='filterCategory' class='selectpicker' name='filterCategory' onChange='document.form1.submit()' color='black'", $filterCategory, 1);

$cond = empty($filterSchoolYear) ? "" : " AND ay.AcademicYearID = '".$filterSchoolYear."'";
$cond .= empty($filterCategory) ? "" : " AND op.Category = '".$filterCategory."'";

//handle for search title
$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
    $cond .= " AND (op.Title LIKE '%".$inputSearch."%')";
}

if($status!=""&&$status!=0)
{
    $condSatus .= " AND os.RecordStatus = '".$status."'";
}

$sql_count = " SELECT os.RecordStatus, COUNT(*)         
     
              FROM
                {$eclass_db}.OLE_STUDENT AS os
              INNER JOIN
                {$eclass_db}.OLE_PROGRAM AS op
              ON
                os.ProgramID = op.ProgramID
              LEFT JOIN
                {$intranet_db}.INTRANET_USER AS iu
              ON
                op.CreatorID = iu.UserID
              LEFT JOIN
                {$intranet_db}.ACADEMIC_YEAR AS ay
              ON
                op.AcademicYearID = ay.AcademicYearID
              LEFT JOIN
                {$eclass_db}.OLE_CATEGORY AS oc
              ON
                op.Category = oc.RecordID
              WHERE
                op.IntExt = 'INT'
                AND os.UserID = '{$studentID}'
                $cond                    
            GROUP BY os.RecordStatus;";
$count_result= $libdb->returnArray($sql_count);
$pendingcount=0;
$approvedcount=0;
$rejectedcount=0;
for($i=0;$i<count($count_result);$i++){
    if($count_result[$i][RecordStatus]==1) $pendingcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==2) $approvedcount = $count_result[$i][1];
    if($count_result[$i][RecordStatus]==3) $rejectedcount = $count_result[$i][1];
}

$sql =  "
          SELECT
          	os.RecordID,
          	os.ProgramID,
            ay.AcademicYearID,
            op.Title,
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle")." as category,
            os.RecordStatus,
            DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') as ProcessDate
          FROM
            {$eclass_db}.OLE_STUDENT AS os
          INNER JOIN
            {$eclass_db}.OLE_PROGRAM AS op
          ON
            os.ProgramID = op.ProgramID
          LEFT JOIN
            {$intranet_db}.INTRANET_USER AS iu
          ON
            op.CreatorID = iu.UserID
          LEFT JOIN
            {$intranet_db}.ACADEMIC_YEAR AS ay
          ON
            op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN
            {$eclass_db}.OLE_CATEGORY AS oc
          ON
            op.Category = oc.RecordID
          WHERE
            op.IntExt = 'INT'
            AND os.UserID = '{$studentID}'
            $cond
            $condSatus
         Order By op.StartDate desc
        ";
$result = $libdb->returnArray($sql);

for($i=0;$i<count($result);$i++){
    if($result[$i][RecordStatus]==2){
        $programme_list .= "<a href='student_record_detail.php?studentID={$studentID}&recordID={$result[$i][RecordID]}&programID={$result[$i][ProgramID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                              <li class='student'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                <div class='status approved'><span class='icon'></span>{$ec_iPortfolio['approved']}</div>
                              </li></a>";
    }else if($result[$i][RecordStatus]==1){
        $programme_list .= "<a href='student_record_detail.php?studentID={$studentID}&recordID={$result[$i][RecordID]}&programID={$result[$i][ProgramID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                                <li class='student'>
                                  <div class='eventTitle'>{$result[$i][Title]}</div>
                                  <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                  <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                  <div class='status pending'><span class='icon'></span>{$ec_iPortfolio['pending']}</div>
                                </li></a>";
    }else if($result[$i][RecordStatus]==3){
        $programme_list .= "<a href='student_record_detail.php?studentID={$studentID}&recordID={$result[$i][RecordID]}&programID={$result[$i][ProgramID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}'>
                              <li class='student'>
                                <div class='eventTitle'>{$result[$i][Title]}</div>
                                <div class='eventDate'><span class='label'>{$ec_iPortfolio['date']}︰</span>{$result[$i][period]}</div>
                                <div class='eventCategory'><span class='label'>{$ec_iPortfolio['category']}︰</span>{$result[$i][category]}</div>
                                <div class='rejectedDate'><span class='label'>{$Lang['eDiscipline']['RejectedDate']}︰</span>{$result[$i][ProcessDate]}</div>
                                <div class='status rejected'><span class='icon'></span>{$ec_iPortfolio['rejected']}</div>
                              </li></a>";
    }

}

//student name and class
$sql = "SELECT 
           iu.UserID, 
           ".getNameFieldByLang2("iu.")." as StudentName,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', LPAD(iu.ClassNumber, 2, '0')) AS ClassNumber
        FROM {$intranet_db}.YEAR y
           INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID
           INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON ycu.YearClassID = yc.YearClassID 
           INNER JOIN {$intranet_db}.INTRANET_USER iu ON ycu.UserID = iu.UserID 
        where 
           yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
           and  iu.UserID = '".$studentID."'";

$student_info= $libdb->returnArray($sql);



?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/moment.js"></script>
    <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript" src="../../js/sitemapstyler.js"></script>
    <script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
    <script type="text/javascript" src="../../js/offcanvas.js"></script>
    <link rel="stylesheet" href="../../css/offcanvas.css">
    <link rel="stylesheet" href="../../css/ole_teacher.css">
    <script type="text/javascript" src="../../js/ole_teacher.js"></script>
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>
<script>

    function change_status(status){
        $('#status').val(status);
        document.form1.submit();
    }

</script>
<body>
<form name="form1" id="form1" method="GET">
<div id="wrapper">
    <!-- Header -->
    <nav id="header" class="navbar navbar-fixed-top ole edit">
        <div id="function"><div class="headerIcon"><a onclick="window.history.go(-1)"><i class="fas fa-arrow-left"></i></a></div></div>
        <div id="headerTitle" class="filterStatus withFunction btn-2">
            <select id="filterStatus" class="selectpicker" onchange="change_status(this.value)">
                <option value="0" <?php if($status==0||$status=="") echo "selected"?>><?=$ec_iPortfolio['all_status']?></option>
                <option value="1" <?php if($status==1) echo "selected"?>><?=$ec_iPortfolio['pending']?>  (<?=$pendingcount?>)</option>
                <option value="2" <?php if($status==2) echo "selected"?>><?=$ec_iPortfolio['approved']?> (<?=$approvedcount?>)</option>
                <option value="3" <?php if($status==3) echo "selected"?>><?=$ec_iPortfolio['rejected']?> (<?=$rejectedcount?>)</option>
            </select>
        </div>
        <div id="button">
            <div class="headerIcon"><a href="teacher_ole_chart.php?StudentID=<?=$studentID?>"><span class="oleGraph" id="oleGraph"></span></a></div>
            <div class="headerIcon"><a class="filterProg" role="button" data-toggle="collapse" href="#filterProg" aria-expanded="false" aria-controls="filterProg"> <span class="filterProg"></span> </a></div>
        </div>
        <div class="collapse" id="filterProg">
            <div class="schoolYear">
                <?php echo $ay_selection_html; ?>
            </div>
            <div class="category">
                <?php echo $category_selection_html; ?>
            </div>
        </div>
    </nav>
    <div id="content">
        <div class="stdInfo">
            <div class="stdName"><?=$student_info[0][StudentName]?></div>
            <span class="classClassNo"><?=$student_info[0][ClassNumber]?></span>
        </div>
        <ul class="stdEventList">
            <?=$programme_list?>
        </ul>
        <div class="functionBtn">
            <div class="btn add">
                <a href="teacher_ole_add_programme.php"><span class="icon"></span></a>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value = "<?=$status?>" name="status" id="status">
<input type="hidden" value = "<?=$studentID?>" name="studentID" id="studentID">
</form>
</body>
</html>
