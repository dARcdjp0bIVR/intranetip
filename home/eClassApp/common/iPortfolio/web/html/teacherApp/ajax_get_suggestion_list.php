<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_additional_info.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_supplementary_info.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");

intranet_auth();
intranet_opendb();

$ole_title = trim($_POST['ole_title']);
$oleProgramId = IntegerSafe($_POST['oleProgramId']);
$ItemCode = trim($_POST['ItemCode']);

if ($ole_title == '' && $oleProgramId != '') {
	// get the title by program id
	include_once($PATH_WRT_ROOT.'includes/portfolio25/slp/ole_program.php');
	$objOleProgram = new ole_program($oleProgramId);
	$ole_title = $objOleProgram->getTitle();
}

$linterface = new interface_html();

$liboea_setting = new liboea_setting();
$liboea_setting_search = new liboea_setting_search($liboea_setting);

$default_oea_category = $liboea_setting->get_OEA_Default_Category($associateAry=0);

	$liboea_setting_search->setCriteriaTitle($ole_title);
	$liboea_setting_search->getSuggestResult();
//	$oea_itemSelect = $liboea_setting_search->loadSuggestResultDisplay();
    $oea_itemSelect = $liboea_setting_search->getSearchResultTitle();
    $numOfSuggestion = sizeof($oea_itemSelect);

$OEATitleFieldDisplay .= "<p>{$Lang['iPortfolio']['OEA']['OLE_Title']} : {$ole_title}<br />{$Lang['iPortfolio']['OEA']['NoOfItemsSuggested']} : {$numOfSuggestion}</p>";
if($numOfSuggestion>0){
    $OEATitleFieldDisplay .= "<div id='matchOEATitleField' class='fieldInput'>";
    for($i=0; $i<$numOfSuggestion; $i++)
    {
        if($ItemCode==$oea_itemSelect[$i][0]){
            $showChecked = "checked=checked";
        }else{
            $showChecked = "";
        }
        $OEATitleFieldDisplay .= "<div class='item'>
                                         <div class='form-check form-check-inline'>
                                            <label class='form-check-label'>
                                                <input class='form-check-input' name='matchOEATitleRadios' type='radio' id='ItemCode_".$oea_itemSelect[$i][0]."' value='".$oea_itemSelect[$i][0]."' $showChecked>
                                                <span class='text' id='showTitle_".$oea_itemSelect[$i][0]."'>[{$oea_itemSelect[$i][0]}] {$oea_itemSelect[$i][1]}</span>
                                            </label>
                                         </div>
                                      </div>";
    }

    $OEATitleFieldDisplay .= "</div>";
}

intranet_closedb();
echo $OEATitleFieldDisplay;
?>



