<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$LibPortfolio = new libpf_slp();
$lay = new academic_year();
$filterSchoolYear = IntegerSafe($_GET["filterSchoolYear"]);
$filterCategory = IntegerSafe($_GET["filterCategory"]);
isset($tab)?$tab:$tab="approved_content";
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "id='filterSchoolYear' class='selectpicker' name='filterSchoolYear' onChange='document.form1.submit()' color='black'", $filterSchoolYear, 1, 0, $i_Attendance_AllYear, 2);
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("id='filterCategory' class='selectpicker' name='filterCategory' onChange='document.form1.submit()' color='black'", $filterCategory, 1);

$cond = empty($filterSchoolYear) ? "" : " AND ay.AcademicYearID = '".$filterSchoolYear."'";
$cond .= empty($filterCategory) ? "" : " AND op.Category = '".$filterCategory."'";

//handle for search title
$inputSearch = trim($inputSearch);
if(trim($inputSearch) !="")
{
	$cond .= " AND (op.Title LIKE '%".$inputSearch."%')";
}

$sql =  "
          SELECT
          	os.RecordID,
            ay.AcademicYearID,
            op.Title,
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle")." as category,
            os.RecordStatus,
            DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') as ProcessDate
          FROM
            {$eclass_db}.OLE_STUDENT AS os
          INNER JOIN
            {$eclass_db}.OLE_PROGRAM AS op
          ON
            os.ProgramID = op.ProgramID
          LEFT JOIN
            {$intranet_db}.INTRANET_USER AS iu
          ON
            op.CreatorID = iu.UserID
          LEFT JOIN
            {$intranet_db}.ACADEMIC_YEAR AS ay
          ON
            op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN
            {$eclass_db}.OLE_CATEGORY AS oc
          ON
            op.Category = oc.RecordID
          WHERE
            op.IntExt = 'INT'
            AND os.UserID = '{$UserID}'
            $cond
         Order By op.StartDate desc
        ";
$result = $libdb->returnArray($sql);
for($i=0;$i<count($result);$i++){
   if($result[$i][RecordStatus]==2){
   	   $content_list_approved .= "<a href='iportfolio_ole_recordDetails.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab=approved_content'><li class='event'>
		                <div class='eventTitle'>{$result[$i][Title]}</div>
		                <div class='eventDate'>{$ec_iPortfolio['date']}︰{$result[$i][period]}</div>
		                <div class='eventCategory'>{$ec_iPortfolio['category']}︰{$result[$i][category]}</div>
		              </li></a>";
   }else if($result[$i][RecordStatus]==1){
   	   $content_list_processing .= "<a href='iportfolio_ole_recordDetails.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab=profile'><li class='event'>
		                <div class='eventTitle'>{$result[$i][Title]}</div>
		                <div class='eventDate'>{$ec_iPortfolio['date']}︰{$result[$i][period]}</div>
		                <div class='eventCategory'>{$ec_iPortfolio['category']}︰{$result[$i][category]}</div>
		                </li></a>";  
   }else if($result[$i][RecordStatus]==3){
   	   $content_list_rejected .= "<a href='iportfolio_ole_recordDetails.php?recordID={$result[$i][RecordID]}&inputSearch={$inputSearch}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab=messages'><li class='event'>
		                <div class='eventTitle'>{$result[$i][Title]}</div>
		                <div class='eventDate'>{$ec_iPortfolio['date']}︰{$result[$i][period]}</div>
		                <div class='eventCategory'>{$ec_iPortfolio['category']}︰{$result[$i][category]}</div>
		                <div class='eventCategory'>{$Lang['eDiscipline']['RejectedDate']}︰{$result[$i][ProcessDate]}</div>
		                </li></a>";
   }

}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>iPortfolio OLE</title>
<script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../js/moment.js"></script>
<script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../../js/offcanvas.js"></script>
<link rel="stylesheet" href="../../css/offcanvas.css">
<link rel="stylesheet" href="../../css/ole.css">
<link rel="stylesheet" href="../../css/font-awesome.min.css">
<script type="text/javascript" src="../../js/ole.js"></script>
<script type="text/javascript">
jQuery(document).ready( function() {
	var tab = "<?=$tab?>";
	if(tab=="approved_content"){
		$('#tab_header a[href="#approved_content"]').tab('show');
		approved();
	}
	if(tab=="profile"){
		$('#tab_header a[href="#profile"]').tab('show');
		processing();
	}
	if(tab=="messages"){
		$('#tab_header a[href="#messages"]').tab('show');
		rejected();
	}
 });


$(function(){
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var activeTab = $(e.target).attr('aria-controls');
            document.form1.tab.value = activeTab;
    });
});    
</script>
</head>

<body onLoad="checkForChanges()">
<form name="form1" method="GET">
<div id="wrapper"> 
  <!-- Header -->
  <div id="header" class="approved_not_use">
    <div id="function"><a href="iportfolio_ole_newRecord.php?"><span class="applyIcon"></span></a><a href="iportfolio_ole_newRecord.php"><span class="text"><?php echo $ec_iPortfolio['SLP']['ReportOtherActivities'];?></span></a></div>
    <div id="button"> <a role="button" data-toggle="collapse" href="#search" aria-expanded="false" aria-controls="search"> <span class="searchIcon" onClick="search()"></span> </a> <a role="button" data-toggle="collapse" href="#filter" aria-expanded="false" aria-controls="filter"> <span class="filterIcon" onClick="filter()"></span> </a>
    </div>
    <div class="collapse" id="search">
      <input type="text" class="form-control" id="inputSearch" name="inputSearch" placeholder="" value="<?php echo stripslashes($inputSearch);?>">
    </div>
    <div class="collapse" id="filter">
      <?php echo $ay_selection_html; ?>
      <?php echo $category_selection_html; ?>      
    </div>
  </div>
  <div id="ole-tab" class="approved_not_use"> 
    <!-- Nav tabs -->
    <ul id="tab_header" class="nav nav-tabs radial-out" role="tablist">
      <li role="presentation" class="col-xs-4"><a href="#approved_content" aria-controls="approved_content" role="tab" data-toggle="tab" onClick="approved()"><?php echo $ec_iPortfolio['approved'];?></a></li>
      <li role="presentation" class="col-xs-4"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" onClick="processing()"><?php echo $ec_iPortfolio['pending'];?></a></li>
      <li role="presentation" class="col-xs-4"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" onClick="rejected()"><?php echo $ec_iPortfolio['rejected'];?></a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content" id="tab-content" style="width:100%">
      <div role="tabpanel" class="tab-pane active fade in" id="approved_content">
        <div id="main">         
          <div class="eventListArea">
            <ul class="eventList">			
                <?php echo $content_list_approved;?>
            </ul>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane fade in" id="profile">
		  <div class="main"> 
          <div class="eventListArea">
            <ul class="eventList schoolEvent">
               <?php echo $content_list_processing;?>
            </ul>
          </div>
        </div>
	  </div>
      <div role="tabpanel" class="tab-pane fade in" id="messages">
		  <ul class="eventList">
		    <?php echo $content_list_rejected;?>
          </ul>
	  </div>
    </div>
  </div>
</div>
<input type="hidden" name='tab' value='approved_content'>         
</form>
</body>
</html>
