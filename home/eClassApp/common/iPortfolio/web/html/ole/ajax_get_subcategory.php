<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();
$libpf_slp = new libpf_slp();

//$CategoryID = $_GET['CategoryID'];
//$SubCategoryID = $_GET['SubCategoryID'];
//$IS_CDATA = true;
//$IS_ESC_HTML = false;
//$XML = generateXML(generateSubCategorySelection($CategoryID, $SubCategoryID), $IS_CDATA, $IS_ESC_HTML);

$ActionType = $_GET['ActionType'];
switch($ActionType) {
	case "Change_Sub_Category":
		$CategoryID = $_GET['CategoryID'];
		$SubCategoryID = $_GET['SubCategoryID'];
		$IS_CDATA = true;
		$IS_ESC_HTML = false;
		$XML = generateXML(generateSubCategorySelection($CategoryID, $SubCategoryID), $IS_CDATA, $IS_ESC_HTML);
		break;
	case "Get_Program_Name_And_Ele":
		$CategoryID = $_GET['CategoryID'];
		$SubCategoryID = $_GET['SubCategoryID'];
		if (isset($CategoryID) && $CategoryID!="" && $CategoryID!="-9") {
			$XML = generateXML(getCatEle($CategoryID));
		} else {
			$XML = generateXML(getSubCatEle($SubCategoryID));			
		}		
		break;
	default:
		break;
}
header("Content-Type:   text/xml");
echo $XML;

/////////////////////////// FUNCTION FOR THIS PAGE ////////////////////////////////
/**
 * generate a selection box for subcategory by category id
 * @param int|string ParCategoryID the category id used to get the subcategory, -9 means no category is selected
 * @return string html selection box
 */
function generateSubCategorySelection($ParCategoryID=-9, $ParSelectedSubCat=-9) {
	global $PATH_WRT_ROOT,$libpf_slp;
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	$linterface = new interface_html();
	
	# construct selection box
	$returnArr = $libpf_slp->getSubCategory($ParCategoryID);
	array_unshift($returnArr, array("-9", "請選擇"));	
	$x = "";
	$x .= $linterface->GET_SELECTION_BOX($returnArr, "class='selectpicker' name ='subCategory' onChange=\" refreshSubCategory();refreshSelectpicker() \" ","", $ParSelectedSubCat);
	# put each child into an array
	$selectionBox = array("subCategorySel", $x);
	$xmlElementsArray = array();
	array_push($xmlElementsArray, $selectionBox);	
	# output the xml elements array for generating the xml file
	return $xmlElementsArray;
}

function getCatEle($ParCategoryID) {
	global $PATH_WRT_ROOT, $eclass_db, $intranet_db, $intranet_session_language;
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	$libdb = new libdb();
	
	# getting subcategory by category id
	// RECORDSTATUS
	$PUBLIC = 1;
	
	// only retrieve English Title
	$TITLE = "ENGTITLE";
	
	$sql = "
SELECT " .$TITLE. " AS TITLE, ELE
FROM " . $eclass_db . ".OLE_PROGRAM_TITLE
WHERE CategoryID = '" . $ParCategoryID . "'
AND RECORDSTATUS = '" . $PUBLIC . "'
		";

	$returnArr = $libdb->returnArray($sql);
	# prepare for xml contents
	$xmlElementsArray = array();
	$numofElements = count($returnArr);
	if ($numofElements>0) {
		for($i=0;$i<$numofElements;$i++) {
			# construct xml sub child elements
			$xmlElementTitle = sxe("title", $returnArr[$i]["TITLE"], true, true);
			$xmlElementEle = sxe("ele", $returnArr[$i]["ELE"], true, true);
			
			# put each child into an array
			$xmlElementProgNameElms = array("programNameElements", $xmlElementTitle.$xmlElementEle);
			array_push($xmlElementsArray, $xmlElementProgNameElms);			
		}
	} else {
		$ele = array("ele", "");
		array_push($xmlElementsArray, $ele);
	}
	# output the xml elements array for generating the xml file
	return $xmlElementsArray;
}

function getSubCatEle($ParSubCategoryID) {
	global $PATH_WRT_ROOT, $eclass_db, $intranet_db, $intranet_session_language;
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	$libdb = new libdb();
	
	# getting subcategory by category id
	// RECORDSTATUS
	$PUBLIC = 1;
	
	// only retrieve English Title
	$TITLE = "ENGTITLE";
	
	$sql = "
SELECT " .$TITLE. " AS TITLE, ELE
FROM " . $eclass_db . ".OLE_PROGRAM_TITLE
WHERE SubCategoryID = '" . $ParSubCategoryID . "'
AND RECORDSTATUS = '" . $PUBLIC . "'
		";
	$returnArr = $libdb->returnArray($sql);

	# prepare for xml contents
	$xmlElementsArray = array();
	$numofElements = count($returnArr);
	if ($numofElements>0) {
		for($i=0;$i<$numofElements;$i++) {
			# construct xml sub child elements
			$xmlElementTitle = sxe("title", $returnArr[$i]["TITLE"], true, true);
			$xmlElementEle = sxe("ele", $returnArr[$i]["ELE"], true, true);
			
			# put each child into an array
			$xmlElementProgNameElms = array("programNameElements", $xmlElementTitle.$xmlElementEle);
			array_push($xmlElementsArray, $xmlElementProgNameElms);			
		}
	} else {
		$ele = array("ele", "");
		array_push($xmlElementsArray, $ele);
	}

	# output the xml elements array for generating the xml file
	return $xmlElementsArray;
}

/**
 * generate the output xml
 * @param	array	ParElementsArr	the child elements for the xml output, string [0] - child tag name, string [1] - child content
 * @return	xml	the xml to output
 */
function generateXML($ParElementsArr, $ParCData=false, $ParSpcChar=false) {
	$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	$numofElements = count($ParElementsArr);
	for ($i=0;$i<$numofElements;$i++) {
		list($TAG_NAME, $CONTENT) = $ParElementsArr[$i];
		$x .= sxe($TAG_NAME, $CONTENT, $ParCData, $ParSpcChar);
	}
	$xml .= sxe("elements", $x);
	return $xml;
}

/**
 * sxe - simple xml embed with format <xx> yy </xx>
 * @param	string	ParTag	the tag name of an xml element
 * @param	string	ParValue	corresponding content for an element
 * @param	boolean	ParCData	quote the content with <![CDATA[ content ]]>	// already html format string
 * @param	boolean	ParSpcChar	escape special characters (single quote and double quotes)	// for strings to put into html
 * @return	string	xml tag
 */
function sxe($ParTag="element", $ParValue="content", $ParCData=false, $ParSpcChar=false) {
//	$quotes = array("\"","'");
//	$htmlEntities = array("&quot;","&#039;");
	return "<".$ParTag.">".($ParCData?"<![CDATA[":"").($ParSpcChar?intranet_htmlspecialchars($ParValue):$ParValue).($ParCData?"]]>":"")."</".$ParTag.">";
}
?>