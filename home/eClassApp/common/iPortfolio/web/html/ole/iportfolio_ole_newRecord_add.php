<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$record_id = IntegerSafe($_GET["recordID"]);
$programID = IntegerSafe($_GET["programID"]);
$EventType = $_GET["EventType"];
$isEdit = $_GET["isEdit"];
$isSchoolEvent = $_GET["isSchoolEvent"];
$filterSchoolYear = IntegerSafe($_GET["filterSchoolYear"]);
$filterCategory = IntegerSafe($_GET["filterCategory"]);

if($EventType=="myEvent"){
	$isMyEvent = true;
}else{
	$isMyEvent = false;	
}

intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lpf = new libpf_slp();
$linterface = new interface_html("iportfolio_default.html");
$approvalSettings = $lpf->GET_OLR_APPROVAL_SETTING_DATA2("INT");
list($ClassName, $StudentID) = $lpf->GET_STUDENT_ID($ck_user_id);

# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;

if($record_id!="")  //IE OLE_STUDENT EXIST
{
	$DataArr = $lpf->RETURN_OLE_RECORD_BY_RECORDID($record_id);
	$startdate = $DataArr["StartDate"];
	$enddate = $DataArr["EndDate"];
	$title = $DataArr["Title"];
	$category = $DataArr["Category"];
	$ELE = $DataArr["ELE"];
	$ele = explode(",",$ELE);

	$ole_role = $DataArr["Role"];
	$hours = $DataArr["Hours"];
	$organization = $DataArr["Organization"]; 
	$achievement = $DataArr["Achievement"];
	$details = $DataArr["Details"];
	$ole_file = $DataArr["Attachment"];
	$approved_by = $DataArr["ApprovedBy"];
	$remark = $DataArr["remark"];
	$process_date = $DataArr["ProcessDate"];
	$record_status = $DataArr["RecordStatus"];
	$user_id = $DataArr["UserID"];
	$int_ext = $DataArr["IntExt"];
	$programID = $DataArr["ProgramID"];
	$subCategoryID = $DataArr["SubCategoryID"];
	$maximumHours = $DataArr["MaximumHours"];
	$compulsoryFields = $DataArr['CompulsoryFields'];
	$requestApprovedBy = $DataArr['RequestApprovedBy'];
	$IsSAS = $DataArr['IsSAS'];
	$IsOutsideSchool = $DataArr['IsOutsideSchool'];

	# Get all categories if record category is disabled
	$OLE_Cat = $lpf->get_OLR_Category();
	$file_array = $LibWord->setFileArr();
	if(!array_key_exists($category, $OLE_Cat))
		$file_array = $LibWord->setFileArr(true);

	$SubmitType = $int_ext;
	
	# get ELE Array
	$ELEArr = explode(",", $ELE);
	$ELEArr = array_map("trim", $ELEArr);
// 	$tmp_arr = explode("\:", $ole_file);
	$tmp_arr = explode(":", $ole_file);
	$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$record_id;
	$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$record_id;
    $attach_count = 0;
	for ($i=0; $i<sizeof($tmp_arr); $i++)
	{
		$attach_file = $tmp_arr[$i];
		$filename = get_file_basename($attach_file);

		if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
		{
			$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
			$attachments_html .= "<div class='attachment' id='div_attachment_$attach_count'><a href=\"".$folder_url."/".$attach_file."\" id='a_$attach_count'>".$filename."</a><span class='removeIcon' onClick='deleteAttachment($attach_count)'></span></div>";
			$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\">\n";
			$attach_count ++;
		}
	}
	
	$JScriptID = "jArr".$category;
	$JELEScriptID = "jELEArr".$category;

	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_edit, "")
		);
		
	$h_navigationWord = $button_edit;
	
	
} 
else
{

	$NewCategoryArray = $lpf->get_OLR_Category();
	$file_array = $LibWord->setFileArr();
	
	// get the least category id
	foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
	{
		$TmpCategoryID = $CategoryID;
		break;
	}

	$JScriptID = "jArr".$TmpCategoryID;
	$JELEScriptID = "jELEArr".$TmpCategoryID;
	if($ELE!="")
	{
		$ELEArr = array($ELE);
	}
	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_new, "")
	);
	
	$h_navigationWord = $button_new;
}


//handle category default
$defaultSelectArray = array('-9',$Lang['General']['PleaseSelect'],'');   //insert this array for default select category "--"
array_unshift($file_array, $defaultSelectArray);  //insert this array for default select category "--"
$category = (!isset($category) || $category=="") ? -9 : $category;

# Retrieve the OLE PROGRAM
$fieldsSetStyle = 0; # default new record enable all the input

$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($programID);
$createID = $data['CreatorID'];

$luser = new libuser($createID);

$isStudent = $luser->isStudent();
$CreateIDType = ($isStudent==true)?"student":"teacher";

if($isStudent==false)
{
	$isTeacher = $luser->isTeacherStaff();
	$CreateIDType = ($isTeacher==true)?"teacher":"";
}

if (isset($record_id) && !empty($record_id)) { # student already had a record (get from OLE_STUDENT)
	$li = new libdb();
	$sql = "
	SELECT
	  PROGRAMTYPE
	FROM
	  $eclass_db.OLE_PROGRAM
	WHERE PROGRAMID = (SELECT PROGRAMID FROM $eclass_db.OLE_STUDENT WHERE RECORDID = '$record_id')
	";
	$row = $li->returnVector($sql);
	
	$fieldsSetStyle = 0;
		
	//handling old data, for some old data without "PROGRAMTYPE" (is null or empty) , for safe , set it to "T", created by teacher
	$_programType  = (trim($row[0]) == "") ? "T": trim($row[0]);

	if ($_programType  == 'T') { # if the program is initially created by teacher, disable some fields
		$fieldsSetStyle = 1;
	}

//debug_r($requestApprovedBy);

	if (is_array($data) && $CreateIDType=='teacher')
	{				
		if($requestApprovedBy!=0 && $requestApprovedBy!=-1)   // -1 means no need to approve
		{
			$DefaultApprover_IsSelectedbyTeacher=true;	
		}
	}
	
} else if ($programID != "") { # join a record created by teacher  (get from OLE_PROGRAM)
	$programID = $_GET['programID'];
	$fieldsSetStyle = 1;

	if (is_array($data)) {
		$startdate = $data['StartDate'];
		$enddate = $data['EndDate'];
		$title = $data['Title'];
		$chiTitle = $data['TitleChi'];
		$category = $data['Category'];
		$subCategoryID = $data['SubCategoryID'];
		$ele = $data['ELE'];
		$organization = $data['Organization'];
		$details = $data['Details'];
		$chiDetails = $data['DetailsChi'];
		$remark = $data['SchoolRemarks'];
		$input_date = $data['InputDate'];
		$modified_date = $data['ModifiedDate'];
		$period = $data['Period'];
		$int_ext = $data['IntExt'];
		$canJoin = $data['CanJoin'];
		$canJoinStartDate = $data['CanJoinStartDate'];
		$canJoinEndDate = $data['CanJoinEndDate'];
		$userName = $data['UserName'];
		$autoApprove = $data['AUTOAPPROVE'];
		$maximumHours = $data['MaximumHours'];
		$defaultHours = $data['DefaultHours'];
		$compulsoryFields = $data['CompulsoryFields'];
		$requestApprovedBy = $data['DefaultApprover'];
		$IsSAS = $data['IsSAS'];
		$IsOutsideSchool = $data['IsOutsideSchool'];

		if($requestApprovedBy <=0 )  {
			//do nothing 
		}else{  // if the teacher selected DefaultApprover
			$DefaultApprover_IsSelectedbyTeacher=true;
		}
		
		if($record_id=="" && $hours==''){
			$hours=$defaultHours;
		}
		$ele = explode(",", $ele);		
	}	
}
//required column related
// compulsory fields elements setting

$redStar = "mandatory";
if (!empty($compulsoryFields)) {
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]) !== false) {
		$cpsry_hours_star = $redStar;
		$cpsry_hours_jsChecking = <<<JSEND
	if (\$("input[name=hours]").val() ==0) {
		alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['hours']}");
		return false;
	}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]) !== false) {
		$cpsry_role_star = $redStar;
		$cpsry_role_jsChecking = <<<JSEND
		if ( \$("input[name=ole_role]").val()=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['ole_role']}");
			return false;
		}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]) !== false) {
		$cpsry_achievement_star = $redStar;
		$cpsry_achievement_jsChecking = <<<JSEND
		if ( \$("input[name=achievement]").val()=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['achievement']}");
			return false;
		}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]) !== false) {
		$cpsry_attachment_star = $redStar;
		$cpsry_attachment_jsChecking = <<<JSEND
		\$targetFileUpload = \$("input[name^=ole_file]");
		var allFileString = "";
		\$targetFileUpload.each(function() {
			allFileString = allFileString + $(this).val();
		});
		if ( allFileString=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['attachment']}");
			return false;
		}
JSEND;
	}
		
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]) !== false) {
		$cpsry_details_star = $redStar;
		$cpsry_details_jsChecking = "";// do nothing in this moment
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]) !== false) {
		$cpsry_approved_by_star = $redStar;
		$cpsry_approved_by_jsChecking = <<<JSEND
		if (\$("select[name=request_approved_by]").val() == -9) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['approved_by']}");
			return false;
		}
JSEND;
	}
}

//for new
$requreJSArr = array();
if(empty($programID)){
	$objIpfSetting = new iportfolio_settings();
	$requiredField = "mandatory";
	$infoMissingField = "infoMissing";
	
	// OLE
	$CompulsorySettingArr = explode(',',$objIpfSetting->findSetting($ipf_cfg["Student_OLE_CompulsoryFields"]["SettingName"]));
	foreach((array)$CompulsorySettingArr as $fieldname){
		$requiredSetting[$fieldname] = $requiredField;
		$infoMissing[$fieldname] = $infoMissingField;		
		$requreJSArr[] = $fieldname;
	}
}
#column start
# title
if($isMyEvent){

//    if($title!=""){
//    	$titleSelectDisplay = "style='display: none;'";
//    	$titleTextDisplay = "style='display: block;'";   	
//    }else{
//    	 $titleSelectDisplay = "style='display: block;'";
//    	$titleTextDisplay = "style='display: none;'";   	    	
//    }
//	$titleFieldDisplay .= " <div class='fieldTitle mandatory'>{$ec_iPortfolio['title']}</div>
//					        <div id='selectTitleField' class='fieldInput' $titleSelectDisplay>
//					        </div>
//                            <div id='inputTitleField' class='fieldInput' $titleTextDisplay>
//                                 <input type='text' class='form-control inputField returnSelection' id='inputTitle' placeholder='' value='" . intranet_htmlspecialchars($title) . "'>
//                                 <span class='returnIcon' onClick='hideInputTitleField();setTitleLayer();'></span>
//                            </div>";

    if($title==""){
    	$titleInfoMissing = "infoMissing";
    }
    $titleFieldDisplay ="<div class='fieldTitle mandatory'>{$ec_iPortfolio['title']}</div>
		        	     <div id='inputTitleFieldNew' class='fieldInput {$titleInfoMissing}'>
					        <input type='text' class='form-control inputField' id='title' name='title' placeholder='' value='{$title}'>
					     </div>";			          
}else{
	$titleFieldDisplay = "<div class='pageTitle'>".intranet_htmlspecialchars($title)."</div>";
}

# date
if($isMyEvent){
	if($startdate!="" && $enddate!="")
	{
	    $add_link_style = "style='display: none;'";
		$enddate_table_style = "style='display: block;'";
	}
	else
	{
	    $add_link_style = "style='display: block;'";
		$enddate_table_style = "style='display: none;'";
	}

    if($startdate==""){
    	$dateInfoMissing = "infoMissing";
    }
    
	$dateFieldDisplay ="<div class='fieldTitle mandatory'>{$ec_iPortfolio['date']}</div>
				        <div id='selectDateField' class='fieldInput date {$dateInfoMissing}'>
				          <div class='input-group date datetimepicker' >
				          <input type='text' class='form-control inputField' id='startdate' name='startdate' value='{$startdate}' />
				          <span class='input-group-addon'> <i class='fa fa-calendar-o' aria-hidden='true'></i> </span> </div>
				          <div id='inputDateField' $add_link_style>
				            <a href='#' onClick='addEndDateField()'>{$ec_iPortfolio['add_enddate']}</a> </div>
				          <div id='endDateFeild' $enddate_table_style>
				            <table>
				              <tr>
				                <td><span>{$profiles_to}</span></td>
				                <td><div class='input-group date datetimepicker'>
				                    <input type='text' class='form-control inputField' id='enddate' name='enddate' value='{$enddate}' />
				                    <span class='input-group-addon'> <i class='fa fa-calendar-o' aria-hidden='true'></i> </span> </div></td>
				              </tr>
				            </table>
				          </div>
				        </div>";
					          
}else{
	if($startdate=="" || $startdate=="0000-00-00")
	    $date_display = "--";
	else if($enddate!="" && $enddate != "0000-00-00")
		$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
	else
		$date_display = $startdate;
		
	$dateFieldDisplay = "<div class='fieldTitle'>{$ec_iPortfolio['date']}</div>
			              <div class='fieldInfo'>".$date_display."</div>";	
}

#category
if($isMyEvent){
    
	$categoryField = $linterface->GET_SELECTION_BOX($file_array, "class='selectpicker' name ='category' id ='category' onChange=\" changeSubCategory();refreshSelectpicker();\" ","",$category);
	
	if($category==-9||$category==""){
    	$categoryInfoMissing = "infoMissing";
    }
	
	$categoryFieldDisplay ="<div class='fieldTitle mandatory'>{$ec_iPortfolio['category']}</div>
                        <div id='selectCategoryField' class='fieldInput {$categoryInfoMissing}'>$categoryField</div>";
					          
}else{
	
	while($elements = current($file_array)) 
	{
		if ($elements[0] == $category) {
			$categoryField = $elements[1];
		}
		next($file_array);
	}
	reset($file_array);
    $categoryFieldDisplay = "<div class='fieldTitle'>{$ec_iPortfolio['category']}</div>
                             <div class='fieldInfo'>$categoryField</div>";
}

#subCategory
$subCategoryArray = $lpf->getSubCategory($category);

if($isMyEvent){ 
    array_unshift($subCategoryArray, array("-9", $Lang['General']['PleaseSelect']));	
    $subCategoryField = $linterface->GET_SELECTION_BOX($subCategoryArray, "class='selectpicker' id ='subCategory' name ='subCategory' onChange=\" refreshSubCategory();refreshSelectpicker(); \" ","", $subCategoryID);
	
	if($subCategoryID==-9||$subCategoryID==""){
    	$subCategoryInfoMissing = $infoMissing['sub_category'];
    }	
	$subCategoryFieldDisplay ="<div class='fieldTitle {$requiredSetting['sub_category']}'>{$ec_iPortfolio['sub_category']}</div>
                               <div id='selectSubCategoryField' class='fieldInput {$subCategoryInfoMissing}'>$subCategoryField</div>";
					          
}else{
	
	if ($subCategoryID==-9 || $subCategoryID=="") {
		$subCategoryFieldDisplay = "--";
	} else {
		while($elements = current($subCategoryArray)) {
			if ($elements[0] == $subCategoryID) {
				$subCategoryField = $elements[1];
			}
			next($subCategoryArray);
		}
		reset($subCategoryArray);
	}
	
	$subCategoryFieldDisplay = "<div class='fieldTitle'>{$ec_iPortfolio['sub_category']}</div>
                             <div class='fieldInfo'>$subCategoryField</div>";
}
          
#Component
$DefaultELEArray = $lpf->GET_ELE();

if($isMyEvent){ 
	foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
	{
		$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";
		
		$ELEList .= "<div class='item'>
	                    <div class='form-check form-check-inline'>
	                    <label class='form-check-label'>
	              	       <input class='form-check-input' type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked onClick=\"jsCheckMultiple(this)\" >
	              	       <span class='text'>".$ELE_Name."</span></label>
	                    </div>
	                 </div>";
	} 	

    if(empty($ELEArr)&&$requiredSetting['ele']!=""){
    	$componentInfoMissing = "checkboxInfoMissing";
    }	 
    
	$ComponentFieldDisplay ="<div class='fieldTitle {$requiredSetting['ele']}'>{$ec_iPortfolio['ele']}</div>
                               <div id='selectComponentField' class='fieldInput {$componentInfoMissing}'>$ELEList</div>";
					          
}else{
	foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
	{
		if (is_array($ele) && in_array($ELE_ID, $ele)) {
			$ELEListTemp[] .= $ELE_Name;
		} else {
			continue;
		}
	}
	if (count($ELEListTemp)<1) {
		$ELEList = "--";
	} else {
		$ELEList = implode("<br />", $ELEListTemp);
	}
	
	$ComponentFieldDisplay = "<div class='fieldTitle'>{$ec_iPortfolio['ele']}</div>
                             <div class='fieldInfo'>$ELEList</div>";
}

#organization
if($isMyEvent){ 
	if($organization==""){
    	$organizationInfoMissing = $infoMissing['organization'];
    }	
	$PartnerFieldDisplay = "<div class='fieldTitle {$requiredSetting['organization']}'>{$ec_iPortfolio['organization']}</div>
					        <div id='inputPartnerField' class='fieldInput {$organizationInfoMissing}'>
					          <input type='text' class='form-control inputField' id='organization' name='organization' placeholder='' value='{$organization}'>
					        </div>";
}else{
	
	$PartnerFieldDisplay = "<div class='fieldTitle'>{$ec_iPortfolio['organization']}</div>
	                        <div class='fieldInfo'>{$organization}</div>";
}

#hours
if (!empty($maximumHours)) {
	$h_maximumHours = "<span class='reminder'>".$ec_iPortfolio['SLP']['MaximumHours'].": ".$maximumHours."</span>";
}
if($hours==""){
    $hoursInfoMissing = $infoMissing['hours'];
}
$hoursFieldDisplay = "<div class='fieldTitle {$requiredSetting['hours']}{$cpsry_hours_star}'>{$ec_iPortfolio['hours']}</div>
                      <div id='inputHoursField' class='fieldInput {$hoursInfoMissing}'>
				        <input type='text' class='form-control inputField' id='hours' name='hours' placeholder='' value='{$hours}'>
						{$h_maximumHours}
				      </div>";

# role
//default role
$LibWord2 = new libwordtemplates_ipf(4);
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
	$RoleName = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
	$RoleList .= "<option value='{$RoleName}'>{$RoleName}</option>";
}

if($ole_role!=""){
	$roleSelectDisplay = "style='display: none;'";
	$roleTextDisplay = "style='display: block;'";   	
}else{
	$roleSelectDisplay = "style='display: block;'";
	$roleTextDisplay = "style='display: none;'";   	    	
}
if($ole_role==""){
    $roleInfoMissing = $infoMissing['ole_role'];
}
	
$roleFieldDisplay .= " <div class='fieldTitle {$requiredSetting['ole_role']}{$cpsry_role_star}'>{$ec_iPortfolio['ole_role']}</div>
				        <div id='selectRoleField' class='fieldInput {$roleInfoMissing}' $roleSelectDisplay>
				          <select id='roleSelection' name='roleSelection' class='selectpicker' title='{$Lang['General']['PleaseSelect']}' onChange='hideSelectRoleField()'>
				            <option class='inputBox' value=''>{$Lang['iPortfolio']['OEA']['SelfInput']}</option>
				            {$RoleList}
				          </select>
				        </div>
                        <div id='inputRoleField' class='fieldInput {$roleInfoMissing}' $roleTextDisplay>
                             <input type='text' class='form-control inputField returnSelection' id='ole_role' name='ole_role' placeholder='' value='{$ole_role}'>
                             <span class='returnIcon' onClick='hideInputRoleField()'></span>
                        </div>";
                        
# achievement

//default achievement
$LibWordAward = new libwordtemplates_ipf(5);
$awards_array = $LibWordAward->getWordList(0);
for ($i=0; $i<sizeof($awards_array); $i++) {
	$AwardName = str_replace("&amp;", "&", undo_htmlspecialchars($awards_array[$i]));
	$AwardList .= "<option value='{$AwardName}'>{$AwardName}</option>";
}

if($achievement!=""){
	$roleSelectDisplay = "style='display: none;'";
	$roleTextDisplay = "style='display: block;'";   	
}else{
	$roleSelectDisplay = "style='display: block;'";
	$roleTextDisplay = "style='display: none;'";   	    	
}

if($achievement==""){
    $achievementInfoMissing = $infoMissing['achievement'];
}

$achievementFieldDisplay .= " <div class='fieldTitle {$requiredSetting['achievement']}{$cpsry_achievement_star}'>{$ec_iPortfolio['achievement']}</div>
				        <div id='selectAwardsField' class='fieldInput {$achievementInfoMissing}' $roleSelectDisplay>
				          <select id='awardsSelection' name='awardsSelection' class='selectpicker' title='{$Lang['General']['PleaseSelect']}' onChange='hideSelectAwardsField()'>
				            <option class='inputBox' value=''>{$Lang['iPortfolio']['OEA']['SelfInput']}</option>
				            {$AwardList}
				          </select>
				        </div>
                        <div id='inputAwardsField' class='fieldInput {$achievementInfoMissing}' $roleTextDisplay>
                             <input type='text' class='form-control inputField returnSelection' id='achievement' name='achievement' placeholder='' value='{$achievement}'>
                             <span class='returnIcon' onClick='hideInputAwardsField()'></span>
                        </div>";
      
# attachment

$attachmentFieldDisplay = "<div class='fieldTitle {$requiredSetting['attachment']}{$cpsry_attachment_star}'>{$ec_iPortfolio['attachment']}</div>
						   <div id='addAttachmentField' class='fieldInput'>
						   	 {$attachments_html}
						   	 <div id='attachment_list'>
						   	 </div>
						     <a href='javascript:add_field();'>{$Lang['iPortfolio']['OLE']['AddCertificationFile']}</a>
						   </div>";

# details
if($details==""){
    $detailsInfoMissing = $infoMissing['details'];
}
	$detailsFieldDisplay = "<div class='fieldTitle {$requiredSetting['details']}{$cpsry_details_star}'>{$ec_iPortfolio['details']}</div>
					        <div id='inputDetailsField' class='fieldInput {$detailsInfoMissing}'>
					          <input type='text' class='form-control inputField' id='details' name='details' placeholder='' value='{$details}'>
					        </div>";
# approver

if (($approvalSettings["IsApprovalNeed"] && $approvalSettings["Elements"]["Self"]) || $DefaultApprover_IsSelectedbyTeacher==true) {

	$hiddenField_Approver = '<input type="hidden" name="request_approved_by" value="'.$requestApprovedBy.'" >';

	$TeacherArray = $lpf->GET_ALL_TEACHER_IPF();
    $defaultSelectArray = array('-9',$Lang['General']['PleaseSelect'],'');  
    array_unshift($TeacherArray, $defaultSelectArray);
		
	$approverField = $linterface->GET_SELECTION_BOX($TeacherArray, "class='selectpicker' id ='request_approved_by' name ='request_approved_by' onChange=\"refreshSelectpicker();\" ","",$requestApprovedBy);
	
	if($requestApprovedBy==-9||$requestApprovedBy==""){
    	$approverInfoMissing = $infoMissing['preferred_approver'];
    }	
    	
	$approverFieldDisplay ="<div class='fieldTitle {$requiredSetting['preferred_approver']}{$cpsry_approved_by_star}'>{$Lang['iPortfolio']['preferred_approver']}</div>
                            <div id='selectApproverField' class='fieldInput {$approverInfoMissing}'>$approverField</div>";
} 
else {
	
	# default teacher: class teacher
	if($approved_by=="")
	{
		$ClassTeacher = $lpf->GET_CLASS_TEACHER($ClassName);
		$approved_by = $ClassTeacher[0];
	}
}


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>iPortfolio OLE</title>
<script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../js/moment.js"></script>
<script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../../js/sitemapstyler.js"></script>
<script type="text/javascript" src="../../bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
<script type="text/javascript" src="../../bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../bootstrap-select/css/bootstrap-select.min.css"/>
<script type="text/javascript" src="../../js/offcanvas.js"></script>
<link rel="stylesheet" href="../../css/offcanvas.css">
<link rel="stylesheet" href="../../css/ole.css">
<link rel="stylesheet" href="../../css/font-awesome.min.css">
<script type="text/javascript" src="../../js/ole.js"></script>
</head>

<body>
<div id="wrapper"> 
  <!-- Header -->
  <div id="header" class="inner edit navbar-fixed-top">
    <div id="function"><a href="<?php if($isEdit) echo "iportfolio_ole_recordDetails.php?recordID={$recordID}&isSchoolEvent={$isSchoolEvent}&filterSchoolYear={$filterSchoolYear}&filterCategory={$filterCategory}&tab={$tab}";else echo "iportfolio_ole_newRecord.php"?>"><span class="backIcon"></span></a> <span class="text"><?php echo $Lang['iPortfolio']['OLE']['NewOLE'];?></span></div>
  </div>
  <div id="main" class="edit">
    <div class="main">
      <form class="applicationForm" name="form1" method="post" action="iportfolio_ole_newRecord_update.php" enctype="multipart/form-data"  onSubmit="return checkform(this)">
        <?php echo $titleFieldDisplay;?>
        <?php echo $dateFieldDisplay;?>
        <?php echo $categoryFieldDisplay;?>
        <?php echo $subCategoryFieldDisplay;?>      
        <?php echo $ComponentFieldDisplay;?>      
        <?php echo $PartnerFieldDisplay;?> 
        <?php echo $hoursFieldDisplay;?> 
        <?php echo $roleFieldDisplay;?> 
        <?php echo $achievementFieldDisplay;?> 
        <?php echo $attachmentFieldDisplay;?> 
        <?php echo $detailsFieldDisplay;?> 
        <?php echo $approverFieldDisplay;?>         
        <div class="mandatoryMsg"><?php echo $Lang['iPortfolio']['OLE']['mandatory']?></div>
        <div class="submission-area">
          <button class="btn btn-blue" data-dismiss="modal" aria-label="Close"><?php echo $Lang['Btn']['Submit'];?></button>
        </div>
        <input type="hidden" name="StudentID" value="<?=$StudentID?>" >
        <input type="hidden" name="RecordID" value="<?=$record_id?>" >
        <input type="hidden" name="ProgramID" value="<?=$programID?>" >
        <input type="hidden" name="DefaultApprover_IsSelectedbyTeacher" value="<?=$DefaultApprover_IsSelectedbyTeacher?>" >
        <input type="hidden" name='attachment_size' value='0'>         
        <input type="hidden" name='attachment_size_current' value="<?=$attach_count?>">      
        <input type="hidden" name="isSchoolEvent" value="<?=$isSchoolEvent?>" >  
        <input type="hidden" name="inputSearch" value="<?=$inputSearch?>" >
        <input type="hidden" name="filterSchoolYear" value="<?=$filterSchoolYear?>" >
        <input type="hidden" name="filterCategory" value="<?=$filterCategory?>" >      
        <input type="hidden" name="tab" value="<?=$tab?>" >                                                                                                                          
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function(){	

    $('#title').on('input propertychange', function() {
        if( $(this).val().length === 0 ) {
		     $(this).parent().addClass('infoMissing');
		}else{
		     $(this).parent().removeClass('infoMissing');		    	
		}
    });

	$("#category").change(function(){
        if($("select[name='category']").val() == -9 ){
		     $('#selectCategoryField').addClass('infoMissing');
	    }else{
		     $('#selectCategoryField').removeClass('infoMissing');		    	
        }
	});
	
    <?if(in_array('ele',$requreJSArr)){?>
    $(":checkbox").change(function(){
        if($('[name="ele[]"]:checked').length==0){
		     $('#selectComponentField').addClass('checkboxInfoMissing');
	    }else{
		     $('#selectComponentField').removeClass('checkboxInfoMissing');		    	
        }
	});
	<?}?>
	
	<?if(in_array('organization',$requreJSArr)){?>
	    $('#organization').on('input propertychange', function() {		
			if($("input[name=organization]").val() == ''){
			     $(this).parent().addClass('infoMissing');
			}else{
			     $(this).parent().removeClass('infoMissing');		    	
			}
		});
	<?}?>
	
	<?if(in_array('hours',$requreJSArr)){?>
		$('#hours').on('input propertychange', function() {
			if($("input[name=hours]").val() ==0){
			     $(this).parent().addClass('infoMissing');
			}else{
			     $(this).parent().removeClass('infoMissing');		    	
			}
		});
	<?}?>
	
	<?if(in_array('ole_role',$requreJSArr)){?>
		$('#ole_role').on('input propertychange', function() {
			if($("input[name=ole_role]").val() ==''){
			     $('#inputRoleField').addClass('infoMissing');
			}else{
			     $('#inputRoleField').removeClass('infoMissing');		    	
			}
		});
		$("#roleSelection").change(function(){
	        if($("select[name='roleSelection']").val() != ''){
		        $('#selectRoleField').removeClass('infoMissing');	
		    }else{
			    $('#selectRoleField').addClass('infoMissing');			     	    	
	        }
	    });
	<?}?>	
	
	<?if(in_array('achievement',$requreJSArr)){?>
		$('#achievement').on('input propertychange', function() {
			if($("input[name=achievement]").val() ==''){
			     $('#inputAwardsField').addClass('infoMissing');
			}else{
			     $('#inputAwardsField').removeClass('infoMissing');		    	
			}
		});
		$("#awardsSelection").change(function(){
	        if($("select[name='awardsSelection']").val() != ''){
		        $('#selectAwardsField').removeClass('infoMissing');	
		    }else{
			    $('#selectAwardsField').addClass('infoMissing');			     	    	
	        }
	    });
	<?}?>	
	
    <?if(in_array('details',$requreJSArr)){?>
	    $('#details').on('input propertychange', function() {		
			if($("input[name=details]").val() == ''){
			     $(this).parent().addClass('infoMissing');
			}else{
			     $(this).parent().removeClass('infoMissing');		    	
			}
		});
	<?}?>
	
	<?if(in_array('preferred_approver',$requreJSArr)){?>	
			$("#request_approved_by").change(function(){
		        if($("select[name=request_approved_by]").val() == -9){
				     $('#selectApproverField').addClass('infoMissing');
			    }else{
				     $('#selectApproverField').removeClass('infoMissing');		    	
		        }
			});
	<?}?>
});

$(function () {
  $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
  }).on('dp.change', function (e) { 
  	$(this).parent().removeClass('infoMissing');
  });
});
$('.datetimepicker-time').datetimepicker({
    sideBySide: true
});
    
function refreshSubCategory(){
   <?if(in_array('sub_category',$requreJSArr)){?>		
	    if($("select[name='subCategory']").val() == -9 ){
			$('#selectSubCategoryField').addClass('infoMissing');
		}else{
			$('#selectSubCategoryField').removeClass('infoMissing');		    	
	    }
    <?}?>
}

function changeSubCategory() {
	
	var catID = $("select[name='category']").val();
	if (catID==null) {return false;}
	$("[id*=listContent]").hide();
	$.ajax({
		type: "GET",
		url: "ajax_get_subcategory.php",
		async: false,
		data: "ActionType=Change_Sub_Category&CategoryID="+catID+"&SubCategoryID=<?=$subCategoryID?>",
		success: function (xml) {
			$("#selectSubCategoryField").html($(xml).find("subCategorySel").text());
		}
	});
    $('.selectpicker').selectpicker('render');
}

function setTitleLayer(parTitle) {
	var parTitle = parTitle || '';
	
	$("[id*=listContent]").hide();
	$("#change_preset").hide();
	$("#change_preset_loading").show();
	var catID = $("select[name='category']").val();
	var subCatID = $("select[name='subCategory']").val();
	var urlString = "ajax_get_subcategory.php";
	var dataString = "";
	var target_count = "";
	if (subCatID == null || subCatID == -9) {
		dataString = "ActionType=Get_Program_Name_And_Ele&CategoryID="+catID;
	} else {
		dataString = "ActionType=Get_Program_Name_And_Ele&SubCategoryID="+subCatID;
	}
//	alert(urlString+"?"+dataString);
	$.ajax({
		type: "GET",
		url: urlString,
		data: dataString,
		success: function (xml) {
			// parent node is <elements>
			child = $(xml).find("programNameElements");
			var titleSelectionString = "<select id='titleSelection' class='selectpicker' onChange='hideSelectTitleField()'><option class='inputBox' value='-1'><?php echo $Lang['iPortfolio']['OEA']['SelfInput'];?></option>";
			
			child.each(function() {
				var title = $(this).find("title").text();
				var ele = $(this).find("ele").text();
			
				titleSelectionString = titleSelectionString + "<option>"+title+"</option>";								
			});
			
			titleSelectionString = titleSelectionString + "</select>";
			$("#selectTitleField").html(titleSelectionString);			
		}
	});
    $('.selectpicker').selectpicker('refresh');
}

function refreshSelectpicker(){
	$('.selectpicker').selectpicker('render');
}

function jsCheckMultiple(obj)
{
<?php if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']) { ?>
	 if(countChecked(document.form1, 'ele[]') > 1)
	  {
	    alert("<?=$msg_check_at_most_one?>");
	    obj.checked = false;
	  }
<?php } ?>
}

function hideSelectTitleField() {
	if ($('#titleSelection').val() === '-9') {
        $('#selectTitleField').hide();
		$('#inputTitleField').show();
    }
}
function hideInputTitleField() {
	$('#inputTitleField').hide();
	$('#selectTitleField').show();
}

function addEndDateField() {
	$('#inputDateField').hide();
	$('#endDateFeild').show();
}

function hideSelectRoleField() {
	if ($('#roleSelection').val() == '') {
        $('#selectRoleField').hide();
		$('#inputRoleField').show();
    }
    
    $('#ole_role').val($('#roleSelection').val());    
}
function hideInputRoleField() {
	$('#inputRoleField').hide();
	$('#selectRoleField').show();
}

function hideSelectAwardsField() {
	if ($('#awardsSelection').val() == '') {
        $('#selectAwardsField').hide();
		$('#inputAwardsField').show();
    }
    
    $('#achievement').val($('#awardsSelection').val());
    
}
function hideInputAwardsField() {
	$('#inputAwardsField').hide();
	$('#selectAwardsField').show();
}

var no_of_upload_file = 1;

function add_field()
{
	var list = document.getElementById("attachment_list");
	x= '<input class="file" type="file" name="ole_file'+no_of_upload_file+'" size="40">';
	x+='<input type="hidden" name="ole_file'+no_of_upload_file+'_hidden">';
	
	$("#attachment_list").append(x);			
	no_of_upload_file++;	
	document.form1.attachment_size.value = no_of_upload_file-1;
}

function deleteAttachment(id)
{
	$("#div_attachment_"+id).hide();	
	var list = document.getElementById("attachment_list");
	x='<input type="hidden" name="is_delete'+id+'" value="1">';	
	$("#attachment_list").append(x);			
}

function checkform(formObj)
{
	var d = new Date();
	var curr_year = d.getFullYear();



	if(formObj.title.value=="")
	{		
		alert("<?=$ec_warning['title']?>");
		formObj.title.focus();
		return false;
	}

	// Check if this is a new program created by student but not joining
	if (<?= empty($programID)?1:0 ?>) {
		if(formObj.startdate.value=="")
		{
			formObj.startdate.focus();
			alert("<?=$ec_warning['date']?>");
			return false;
		}
		else
		{
			if(formObj.enddate.value!="")
			{
				if(formObj.startdate.value>formObj.enddate.value)
				{
					formObj.enddate.focus();
					alert("<?=$w_alert['start_end_time2']?>");
					return false;
				}
			}

			_startDate = formObj.startdate.value;
			_endDate = formObj.enddate.value;

			var startYear = "";
			if(_startDate.length > 4){
				startYear = _startDate.substring(0,4);
			}

			var endYear   = "";
			if(_endDate.length > 4){
				endYear   = _endDate.substring(0,4);
			}else{
				//if END date not set , set the end year default to current year for checking
				endYear = curr_year;
			}

			<?php //if input year more than current Year 5 year (HardCode) , alert message
			?>
			if((startYear - curr_year > 5) || (endYear  - curr_year > 5)){
				alert("<?=$Lang['iPortfolio']['alertYearExceed']?>");
				return false;
			}
		}
	}
	
	if ($("select[name='category']").val() == "-9") {
		alert("<?=$ec_warning['category']?>");
		return false;
	}

	// Check hours
	var hour = document.getElementsByName('hours')[0];
	if (isNaN(hour.value)) {
		hour.focus();
		alert("<?= $ec_iPortfolio['SLP']['warning_Hour'] ?>");
		return false;
	}
	if (!<?=empty($maximumHours)?'true':'false'?>
		&& Number(hour.value)><?=empty($maximumHours)?0:$maximumHours?>) {

		hour.focus();
		alert("<?= $ec_iPortfolio['SLP']['ExceedingMaxumumHours'] ?>");
		return false;
	}

<?if(empty($programID)){ ?>
	<?if(in_array('sub_category',$requreJSArr)){?>
		if($("select[name=subCategory]").val() == -9){
			alert('<?=$ec_iPortfolio['SLP']['PleaseSelect'].' '.$ec_iPortfolio['sub_category']?>');
			return false;
		}
	<?}?>
	<?if(in_array('ele',$requreJSArr)){?>

		if($('[name="ele[]"]:checked').length==0){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['ele']?>');
			return false;
		}
	<?}?>
		<?if(in_array('organization',$requreJSArr)){?>
		if($("input[name=organization]").val() == ''){
			$("input[name=organization]").focus();
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['organization']?>');
			return false;
		}
	<?}?>
	<?if(in_array('hours',$requreJSArr)){?>
		if($("input[name=hours]").val() ==0){
			$("input[name=hours]").focus();
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['hours']?>');
			return false;
		}
	<?}?>
	<?if(in_array('ole_role',$requreJSArr)){?>
		if($("input[name=ole_role]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['ole_role']?>');
			return false;
		}
	<?}?>
	<?if(in_array('achievement',$requreJSArr)){?>
		if($("input[name=achievement]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['achievement']?>');
			return false;
		}
	<?}?>
	<?if(in_array('attachment',$requreJSArr)){?>
		var targetFileUpload = $("input[name^=ole_file]");
			var allFileString = "";
			targetFileUpload.each(function() {
			allFileString = allFileString + $(this).val();
		});
		if ( allFileString=="" ) {
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['attachment']?>');
			return false;
		}
	<?}?>
	<?if(in_array('details',$requreJSArr)){?>
		if($("input[name=details]").val()==''){
			alert('<?=$ec_iPortfolio['SLP']['PleaseFillIn'].' '.$ec_iPortfolio['details']?>');
			return false;
		}
	<?}?>
	<?if(in_array('preferred_approver',$requreJSArr)){?>
		if($("select[name=request_approved_by]").val() == -9){
			alert('<?=$ec_iPortfolio['SLP']['PleaseSelect'].' '.$ec_iPortfolio['preferred_approver']?>');
			return false;
		}
	<?}?>
<?} ?>

	<?=$cpsry_hours_jsChecking?>
	<?=$cpsry_role_jsChecking?>
	<?=$cpsry_achievement_jsChecking?>
	<?=$cpsry_attachment_jsChecking?>
	<?=$cpsry_details_jsChecking?>
	<?=$cpsry_approved_by_jsChecking?>

	return true;
}

</script> 

<!-- scripts for bootstrap-select --> 
<script>
        $('.selectpicker').selectpicker();
        $('.bootstrap-select').addClass("dropdown");
        $('.bootstrap-select button').addClass("btn-secondary");
        $('.bootstrap-select .dropdown-menu li a').addClass("dropdown-item");
    </script>
</body>
</html>
