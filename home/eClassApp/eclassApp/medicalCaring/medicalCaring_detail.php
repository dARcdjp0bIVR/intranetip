<?php
/*
 *  2018-05-15 Cameron
 *      - resume this page as it's used for Parent App. (Note: Teacher App doesn't point to this page as it use single sign on mechanism (api/eClassApp/sso_intranet.php)
 *      - add sleep part
 *      
 *  2018-03-19 Cameron - cancelled on 2018-05-15
 *      - this page is replaced by home/eClassApp/common/web_module/views/medical/bowel/management/view.php 
 *                       and home/eClassApp/common/web_module/views/medical/student_log/management/view.php
 *      for consistent, so it won't process and die() immediately
 */
//die();
$PATH_WRT_ROOT = '../../../../';
if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
{
	session_start();
	include_once($PATH_WRT_ROOT."file/eclass_app.php");
	$intranet_session_language = $eclass_app_language;
}
else {
	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
	if ($parLang != '') {
		switch (strtolower($parLang)) {
			case 'en':
				$intranet_hardcode_lang = 'en';
				break;
			default:
				$intranet_hardcode_lang = 'b5';
		}
	}
}
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/medicalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/cust/common_function.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentLogLev1.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentLogLev2.php");
include_once($PATH_WRT_ROOT."lang/cust/medical_lang.$intranet_hardcode_lang.php");

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$RecordID.$delimiter.$RecordDate;

if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

//$userType = 1;
//error_log("\n\n _SESSION['UserID'] -->".print_r($_SESSION['UserID'],true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

//if($charactor==1){	// bowel
////debug_pr("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/bowel/management/view&RecordID=".$RecordID."&RecordTime=".$RecordDate." 00:00:00&UserType=".$userType);
////error_log("\n\n bowel -->".print_r("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/bowel/management/view&RecordID=".$RecordID."&RecordTime=".$RecordDate." 00:00:00&UserType=".$userType,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
//	header("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/bowel/management/view&RecordID=".$RecordID."&RecordTime=".$RecordDate." 00:00:00&UserType=".$userType);
//}
//else if($charactor==2){	// student log
////debug_pr("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/student_log/management/view&RecordID=".$RecordID."&UserType=".$userType);
////error_log("\n\n student log-->".print_r("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/student_log/management/view&RecordID=".$RecordID."&UserType=".$userType,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
//	header("location: ".curPageURL(0,0)."/home/eClassApp/common/web_module/#medical/student_log/management/view&RecordID=".$RecordID."&UserType=".$userType);
//}
//exit;

//intranet_auth();
intranet_opendb();

$ldb = new libdb();
$objMedical = new libMedical();
$AcademicYearID = Get_Current_Academic_Year_ID();
global $w2_cfg;
$RecordDate = strtotime($RecordDate);	
$filterYear = date('Y',$RecordDate);
$filterMonth = date('m',$RecordDate);    
if($charactor==1){
    $BowelTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] ."{$filterYear}_{$filterMonth}";
    $TableName =$w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"]."{$filterYear}_{$filterMonth}";
    $attendanceTableExists = hasDailyLogTableByName($TableName);
    $bowelTableExists = hasDailyLogTableByName($BowelTableName);
	if ($bowelTableExists)
	{	
		$fieldList = array();		

		if( $attendanceTableExists ){				
			// For attedance status field, 0= present and 2 = late, both mean the student @ school
			$fieldList [] = "(c.AMStatus = '0' Or c.AMStatus = '2') as Attendance";
			$attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=b.UserID and c.DayNumber=day(b.RecordTime)";
		}
		$fieldList [] = 'b.RecordID';
		$fieldList [] = 'date(b.RecordTime) as RecordDate';
		$fieldList [] = 'substring(time(b.RecordTime),1,5) as RecordTime';
		$fieldList [] = 'b.Remarks';
		$fieldList [] = 's.StatusName';
		$fieldList [] = 's.Color';
		$fieldList [] = "case when u.RecordType=".USERTYPE_PARENT." then concat(u.LastUpdatedBy,' (".$Lang['medical']['general']['parent'].")') else u.LastUpdatedBy end as LastUpdatedBy";
		$fieldList [] = 'b.DateModified as LastUpdatedOn';			
		$fieldList [] = getNameFieldByLang2('IU.').' As Name';
		$fieldList [] = $objMedical->getClassNameByLang2 ($prefix ="YC.").' as ClassName';
		
		$fieldSelected = implode(", ", $fieldList);
		$sql = "SELECT {$fieldSelected}
				FROM {$BowelTableName} b
				LEFT JOIN MEDICAL_BOWEL_STATUS s ON b.BowelID=s.StatusID
					{$attendanceJoinTable}
				LEFT JOIN (SELECT ".getNameFieldByLang2()." As LastUpdatedBy, RecordType, UserID
							FROM INTRANET_USER
						  ) u ON u.UserID=b.ModifyBy
				LEFT JOIN 
						    INTRANET_USER IU ON IU.UserID=b.UserID
				Inner Join
					YEAR_CLASS_USER YCU
				On
					IU.UserID = YCU.UserID
				Inner Join
					YEAR_CLASS YC
				On
					YC.YearClassID = YCU.YearClassID
				Inner Join
					YEAR Y
				On
				    Y.YearID = YC.YearID											  		
				WHERE b.RecordID='$RecordID' and YC.AcademicYearID = '{$AcademicYearID}'";
				
		$bowel_result = $ldb->returnResultSet($sql);	
		$time = $bowel_result[0][RecordTime];
		list($hour,$minutes) = explode(':',$time);
		if($minutes <= 29){
					$recordTime = $hour.':00-29';
				}else{
					$recordTime = $hour.':30-59';
				}
		$show_date = $bowel_result[0][RecordDate]." ".$recordTime;
		$show_remark = $bowel_result[0][Remarks];
		$show_attendance = ($bowel_result[0][Attendance]==1)? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']:$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
		$show_lastUpdatedBy = $bowel_result[0][LastUpdatedBy];
		$show_lastUpdatedOn = $bowel_result[0][LastUpdatedOn];		
		$show_name = $bowel_result[0][Name];
		$show_classname = $bowel_result[0][ClassName];
	}		
}
if($charactor==2){
	$objStudentLogLev1 = new studentLogLev1();
	$lev1Detail = $objStudentLogLev1->getActiveStatus();
	$lev1List = array();
	foreach ($lev1Detail as $lev1) {
		$lev1List[$lev1['Lev1ID']] = $lev1['Lev1Name'];
	}
	$objStudentLogLev2 = new studentLogLev2();
	$lev2Detail = $objStudentLogLev2->getActiveStatus();
	$lev2List = array();
	foreach ($lev2Detail as $lev2) {
		$lev2List[$lev2['Lev2ID']] = $lev2['Lev2Name'];
	}
	# Level 3 List
	$Lev3List = $objMedical->getLev3List();
	foreach( (array)$Lev3List as $Lev3){
		$valueArray['lev3List'][ $Lev3['Lev3ID'] ]= $Lev3['Lev3Name'];
	}
	$objLogLev3 = $objMedical->getLevel3ListByID($RecordID);
	foreach($objLogLev3  as $item){
		$Lev4List = $objMedical->getLev4List($item['Level3ID']);
		$Lev4SelectedList = $objMedical->getLev4SelectedList($RecordID, $item['Level3ID']);
		$html = '';
		$lev3_Sel .= $valueArray['lev3List'][$item['Level3ID']];
		$lev3_Sel .= " - <span class='lev4ListArea'>";
		$lev4Str = '';
		foreach( (array)$Lev4List as $Lev4){
			foreach( (array)$Lev4SelectedList as $Lev4Selected){
				if($Lev4['Lev4ID'] == $Lev4Selected['Level4ID']){
					$lev4Str .= $Lev4['Lev4Name'] . ', ';
					break;
				}
			}
		}
		$lev3_Sel .= trim($lev4Str, ', ');
		$lev3_Sel .= "</span>";
		$lev3_Sel .= "<br />";

	}
	#Attachment
	$attachmentDetailList = $objMedical->getAttachmentDetail($RecordID);
	if(count($attachmentDetailList) == 0){
		$uploadedFilesHTML = '--';
	}else{
		$uploadedFilesHTML = $objMedical->getAttachmentHTML_App($RecordID, $attachmentDetailList, $deleteBtn=false);
	}
    
	$modifyBy = getNameFieldByLang2('IU.');
	$TableName =$w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"]."{$filterYear}_{$filterMonth}";
    $attendanceTableExists = hasDailyLogTableByName($TableName);
    if( $attendanceTableExists ){				
			// For attedance status field, 0= present and 2 = late, both mean the student @ school
			$attendance = "(c.AMStatus = '0' Or c.AMStatus = '2') as Attendance";
			$attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=MSL.UserID and c.DayNumber=day(MSL.RecordTime)";
	}
	$sql = "SELECT
				MSL.RecordID,
				MSL.UserID,
				MSL.RecordTime,
				MSL.BehaviourOne,
				MSL.BehaviourTwo,
				MSL.Duration,
				MSL.Remarks,
				MSL.DateModified,
				{$modifyBy} AS ModifyBy,
				IU.RecordType AS ModifyByRecordType,
				{$attendance},".
	            getNameFieldByLang2('IUS.')." As Name,".					
                $objMedical->getClassNameByLang2 ($prefix ="YC.")." as ClassName ".		
            "FROM
				MEDICAL_STUDENT_LOG MSL
		    {$attendanceJoinTable}
			LEFT JOIN
				INTRANET_USER IU
			ON
				MSL.ModifyBy = IU.UserID
		   LEFT JOIN
					INTRANET_USER IUS
			ON
				MSL.UserID = IUS.UserID
			Inner Join
				YEAR_CLASS_USER YCU
			On
				IUS.UserID = YCU.UserID
			Inner Join
				YEAR_CLASS YC
			On
				YC.YearClassID = YCU.YearClassID
			Inner Join
				YEAR Y
			On
				Y.YearID = YC.YearID
			WHERE
				MSL.RecordID = '{$RecordID}' and YC.AcademicYearID = '{$AcademicYearID}'		
		";
	$sl_result = $ldb->returnResultSet($sql);
	$convulationIDList = $objMedical->geConvulatonID();
	$isDisplayed = ( in_array( $sl_result[0][BehaviourTwo],$convulationIDList) || $plugin['medical_module']['AlwaysShowBodyPart'])?'':'display:none;';
	$show_title = $lev1List[$sl_result[0][BehaviourOne]]." - ".$lev2List[$sl_result[0][BehaviourTwo]];
	$show_date =  $sl_result[0][RecordTime];
	$show_remark = $sl_result[0][Remarks];
	$show_attendance = ($sl_result[0][Attendance]==1)? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']:$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
	$show_lastUpdatedBy = $sl_result[0][ModifyBy];
	$show_lastUpdatedOn = $sl_result[0][DateModified];
	$show_name = $sl_result[0][Name];
	$show_classname = $sl_result[0][ClassName];
	# Duration
    $timeLasted = explode(':',$sl_result[0][Duration]);	
    $time_min = intval($timeLasted[1])+intval($timeLasted[0]*24);
	$time_second = intval($timeLasted[2]);	
	$time_min = str_pad($time_min,2,'0',STR_PAD_LEFT);
	$time_second = str_pad($time_second,2,'0',STR_PAD_LEFT);
	$show_duration = $time_min." ".$Lang['eDiscipline']['sdbnsm_baseMark_2'].$time_second." ".$Lang['medical']['studentLog']['second'];
}

// sleep
if($charactor==3){
    $sleepTableName = "MEDICAL_STUDENT_SLEEP";
    $TableName =$w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"]."{$filterYear}_{$filterMonth}";
    $attendanceTableExists = hasDailyLogTableByName($TableName);
    
    if( $attendanceTableExists ){
        // For attedance status field, 0= present and 2 = late, both mean the student @ school
        $attendance = "(c.PMStatus = '0' Or c.PMStatus = '2') as Attendance";
        $attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=s.UserID and c.DayNumber=day(s.RecordTime)";
    }
    
    $sql = "SELECT
                    s.RecordID,
                    date(s.RecordTime) AS RecordDate,
                    ss.StatusName,
                    ss.Color,
                    sr.ReasonName,
                    s.Frequency,
                    s.Remarks,".
                    getNameFieldByLang2('mu.')." AS ModifiedBy,
                    s.DateModified,
    				{$attendance},".
    	            getNameFieldByLang2('stu.')." AS Name,".					
                    $objMedical->getClassNameByLang2 ('yc.')." AS ClassName 		
            FROM
                    MEDICAL_STUDENT_SLEEP s
            LEFT JOIN
                    INTRANET_USER stu ON stu.UserID=s.UserID
            INNER JOIN
                    YEAR_CLASS_USER ycu ON ycu.UserID=stu.UserID
            INNER JOIN
                    YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
            INNER JOIN
                    YEAR y ON y.YearID=yc.YearID
            LEFT JOIN
                    MEDICAL_SLEEP_STATUS ss ON ss.StatusID=s.SleepID
            LEFT JOIN
                    MEDICAL_SLEEP_STATUS_REASON sr ON sr.ReasonID=s.ReasonID
            LEFT JOIN
                    INTRANET_USER mu ON mu.UserID=s.ModifyBy
                    {$attendanceJoinTable}
            WHERE 
                    s.RecordID='{$RecordID}' and yc.AcademicYearID = '{$AcademicYearID}'";
    
    $sleepResult = $ldb->returnResultSet($sql);

    if (count($sleepResult)) {
        $sleepResult = current($sleepResult);
    }
    $show_date = $sleepResult['RecordDate'];
    $show_sleepStatus = $sleepResult['StatusName'];
    $show_sleepColor = $sleepResult['Color'];
    $show_sleepReason = $sleepResult['ReasonName'];
    $show_sleepFrequency = $sleepResult['Frequency'];
    $show_remark = $sleepResult['Remarks'];
    $show_attendance = ($sleepResult['Attendance']==1)? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']:$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
    $show_lastUpdatedBy = $sleepResult['ModifiedBy'];
    $show_lastUpdatedOn = $sleepResult['DateModified'];
    $show_name = $sleepResult['Name'];
    $show_classname = $sleepResult['ClassName'];
}
//die();
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();

?>
<style type="text/css">
</style>
<SCRIPT LANGUAGE=Javascript>
jQuery(document).ready( function() {
	$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
});
</SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<div data-role="page" id="pageone">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    <? if($charactor==1) { ?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="padding:10px;"><span class='tabletext' style="font-size:22px;"><span class="colorBoxStyle" style="background-color:<?=$bowel_result[0][Color]?>;">&nbsp;&nbsp;&nbsp;</span>&nbsp;<strong><?=$bowel_result[0][StatusName]?></strong></span></td>
				</tr>
				<? } ?>
				<? if($charactor==2) { ?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="padding:10px;"><span class='tabletext' style="font-size:22px;"><strong><?=$show_title?></strong></span></td>
				</tr>
				<? } ?>
			    <? if($charactor==3):?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="padding:10px;"><span class='tabletext' style="font-size:22px;"><span class="colorBoxStyle" style="background-color:<?=$show_sleepColor?>;">&nbsp;&nbsp;&nbsp;</span>&nbsp;<strong><?=$show_sleepStatus?></strong></span></td>
				</tr>
				<? endif; ?>
				
				<?if($Type=="t"){?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="padding:10px;"><span class='tabletext' style="font-size:16px;"><?=$show_name?> (<?=$show_classname?>)</span></td>
				</tr>
				<? }?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style=" padding:10px;font-size:18px;">
						<table border="0" cellpadding="0" cellspacing="0" style="width:100%; font-size:16px;">
							<tr>
								<td style="text-align:left;font-size:16px;">
									<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="top" style="width:20px; height:20px;" />
									<span><font><?=$show_date?></font></span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			    <? if($charactor==3):?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div class="ui-block-a" style="width:100%;">
							<span><?=$Lang['medical']['sleep']['tableHeader']['Reason']?>:</span><br>
							<span><strong><?php echo $show_sleepReason;?></strong></span>
						</div>
					</td>
				</tr>

				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div class="ui-block-a" style="width:100%;">
							<span><?=$Lang['medical']['sleep']['tableHeader']['Frequency']?>:</span><br>
							<span><strong><?php echo $show_sleepFrequency;?></strong></span>
						</div>
					</td>
				</tr>
				<? endif; ?>
				
				<? if($show_remark!="") { ?>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">
						   <span class='tabletext2'><?=nl2br($show_remark)?></span>
						</div>
					</td>
				</tr>
				<? } ?>
				<? if($charactor==2) { ?>
				<tr valign='top' style='<?=$isDisplayed?>'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div class="ui-block-a" style="width:100%;">
							<span><?=$Lang['medical']['studentLog']['bodyParts']?>:</span><br>
							<?=$lev3_Sel?>
						</div>
					</td>
				</tr>
				<tr valign='top' style='<?=$isDisplayed?>'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
						<div class="ui-block-a" style="width:100%;">
							<span><?=$Lang['medical']['studentLog']['timelasted']?>:</span><br>
							<?=$show_duration?>
						</div>
					</td>
				</tr>
			    <? } ?>	
			</table>
		</td>
	</tr>
</table>
	<div data-role="collapsible" data-collapsed="true" style="font-size:18px">
		<h1><?=$Lang['General']['Misc']?></h1>
		<div class="ui-grid-a">
    		<div class="ui-block-a" style="width:100%;">
    			<span><strong><?=$Lang['medical']['meal']['tableHeader']['AttendanceStatus']?></strong></span>
       			<br>
       			<span><?=$show_attendance?></span>
			</div>
			<? if($charactor==2) { ?>
			<div class="ui-block-a" style="width:100%;">
			    <br>
    			<span><strong><?=$Lang['SysMgr']['Homework']['Attachment']?></strong></span>
       			<br>
       			<span><?=$uploadedFilesHTML?></span>
			</div>
			<? } ?>		
	    	<div class="ui-block-a" style="width:100%;">
	    		<br>
	       		<span><strong><?=$Lang['medical']['meal']['tableHeader']['LastPersonConfirmed']?></strong></span>
	       		<br>
	       		<span><?=$show_lastUpdatedBy?></span>
			</div>
		
			<div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$Lang['medical']['meal']['tableHeader']['LastUpdated']?></strong></span>
				<br>
				<span><?=$show_lastUpdatedOn?></span>
			</div>
		</div>
	</div>
</div>

<?php
//debug_pr($image_path);
//debug_pr($LAYOUT_SKIN);

intranet_closedb();
/*$linterface->LAYOUT_STOP();*/
?>