<?php
/*
 *  2019-09-12 Cameron
 *      - fix: should specify mine-type as "application/pdf" if the file is pdf for ios ver 12/13
 */
$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/medicalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/cust/common_function.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentLogLev1.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentLogLev2.php");
global $medical_cfg;
//intranet_auth();
intranet_opendb();

$ldb = new libdb();
$objMedical = new libMedical();

$recordID = $_GET['RecordID'];
$uploadFile_root = $medical_cfg['uploadFile_root'];

$fileDetail = $objMedical->getFileDetailInfo($recordID);
if(empty($fileDetail)){
	echo "Incorrect ID";	
	return;
	
}

$fileName = $fileDetail['OrgFileName'];
$fileType = $fileDetail['FileType'];
$folderPath = $fileDetail['FolderPath'];
$filePath = $uploadFile_root.'/'.$folderPath.'/'.$fileDetail['RenameFile'];

if( empty($filePath) || empty($fileName) ){
	echo "Empty File Path/ Name";	
	return;
}


if(!file_exists($filePath)){
	echo "File does not exist";	
	return;	
}
$fs = new libfilesystem();
$FileContent = $fs->file_read($filePath);
$FileType = $fs->getMIMEType($FileContent);
$content_type = mime_content_type($filePath);
if ($content_type != "application/pdf") {
    $content_type = "application/octet-stream";
}
output2browser($FileContent, $fileName, $content_type);

// below function can't open pdf in app, change the function above like home/download_attachement.php
//Output_To_Browser($FileContent, $fileName, $FileType);

# Dislay generated content to html, such as attachment
function Output_To_Browser($content, $filename,$content_type="")
{
	$ua = $_SERVER["HTTP_USER_AGENT"];
	
	if($content_type=="" || $content_type=="application/zip") {	# to handle IE download corrupted zip file problem
		$content_type = "application/octet-stream";
	}
	
	while (@ob_end_clean());
	header("Pragma: public");
	header("Expires: 0"); # set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: $content_type");
	header("Content-Length: ".strlen($content));
	
	# Handle UTF-8 FileName with IE And FF
	$encoded_filename = urlencode($filename);
	$encoded_filename = str_replace("+", "%20", $encoded_filename);
	if (preg_match("/MSIE/", $ua)) {
		header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
	} else  {
		header('Content-Disposition: attachment; filename="' . $filename . '"');
	} 
	
	echo $content;
}
?>