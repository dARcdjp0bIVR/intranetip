<?php 
$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/DigitalChannels/libdigitalchannels.php');

intranet_opendb();
$libdigitalchannels = new libdigitalchannels();
$libeClassApp = new libeClassApp();
$libDb = new libdb(); 
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$CommentText = $_POST["Comment"];
$UserID = $uid;
$libdigitalchannels->Add_Album_Photo_Comment($photoID,$CommentText);
$CommentTotal = $libdigitalchannels->Get_Album_Photo_Comment_Total($photoID);
$FavoriteTotal = $libdigitalchannels->Get_Album_Photo_Favorites_Total($photoID);
header('Location: '.$PATH_WRT_ROOT.'home/eClassApp/eclassApp/digitalChannel/photo_comment.php?uid='.$uid.'&token='.$token.'&ul='.$ul.'&parLang='.$parLang.'&photoID='.$photoID.'&CommentTotal='.$CommentTotal.'&FavoriteTotal='.$FavoriteTotal);
 intranet_closedb();
 exit;
?>


