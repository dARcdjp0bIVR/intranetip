<?php 
$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/DigitalChannels/libdigitalchannels.php');
$libeClassApp = new libeClassApp();
$libDB = new libdb();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}
$libdigitalchannels = new libdigitalchannels();
intranet_opendb();
if($photoType == "RecPhoto"){
	$photo = $libdigitalchannels->AppGetRecPhoto($uid,array($photoID));
}else{
	$photo = $libdigitalchannels->AppGetPhoto($uid,$albumID,$photoID);
}
$photoLink = $PATH_WRT_ROOT.$photo[0]['FilePath'];
$photoCommentTotal = $photo[0]['CommentTotal'];
$album = $libdigitalchannels->Get_Album($photo[0]['AlbumID']);
$category = $libdigitalchannels->Get_Category($album[0]['CategoryCode']);
$categoryTitle = $intranet_hardcode_lang == "en"?$category[0]['DescriptionEn']:$category[0]['DescriptionChi'];
$dateInput = date('Y-m-d h:i:s', $photo[0]['DateInput']);
$photoViewIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/parentApp/digital_channels/icon_photoAlbum_view_grey.png"."\"";
$photoLikeIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/parentApp/digital_channels/icon_photoAlbum_star_grey.png"."\"";
$photoCommentIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/parentApp/digital_channels/icon_photoAlbum_comment_grey.png"."\"";
$photoViewHref = '<a href="#"  id = "photoView" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-photo-view" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom: 15px;"></a>';
$photoLikeHref = '<a href="#"  id = "photoLike" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-photo-like" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom: 15px;"></a>';
$photoCommentHref = '<a href="#"  id = "photoComment" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline ui-btn-icon-left ui-btn-a ui-nodisc-icon" data-icon="myapp-photo-comment" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom: 15px;"></a>';

$tempAlbumTitle = $album[0]['Title'] != ""?$album[0]['Title']:$Lang['eClassApp']['DigitalChannels']['UntitledAlbum'];

$viewTotalTitle = $Lang['eClassApp']['DigitalChannels']['PhotoViews'];
if(($intranet_hardcode_lang == 'en')&& ($photo[0]['ViewTotal'] > 1)){
	$viewTotalTitle = $viewTotalTitle."s";
}
$favTotalTitle = $Lang['eClassApp']['DigitalChannels']['PhotoFavorite'];
if(($intranet_hardcode_lang == 'en')&& ($photo[0]['FavoriteTotal'] > 1)){
	$favTotalTitle = $favTotalTitle."s";
}
$commentTotalTitle = $Lang['eClassApp']['DigitalChannels']['PhotoComment'];
if(($intranet_hardcode_lang == 'en')&& ($photo[0]['CommentTotal'] > 1)){
	$commentTotalTitle = $commentTotalTitle."s";
}

 $isUserReadable = $libdigitalchannels->AppIsAlbumReadable($photo[0]['AlbumID'],$uid);
	$x = "<div id='my-wrapper'><table style='width:100%;padding-bottom:0px;background-color:white;'><tbody>";
	$x .= "<tr><td style='width:50px;height:50px;text-align: right;'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['PhotoTitle']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;' class='my-commentcolumn'>".$photo[0]['Title']."</td></tr>";
	$x .= "<tr><td style='width:50px;height:50px;text-align: right;'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['AlbumTitle']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;' class='my-commentcolumn'>".$tempAlbumTitle."</td></tr>";
	$x .= "<tr><td style='width:50px;height:50px;text-align: right;'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['Category']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;' class='my-commentcolumn'>".$categoryTitle."</td></tr>";
	if($sys_custom['eClassApp']['SFOC']){
	   $x .= "<tr><td style='width:50px;height:50px;text-align: right;'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['EventTitle']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;' class='my-commentcolumn'>".$photo[0]['EventTitle']."</td></tr>";
	   $x .= "<tr><td style='width:50px;height:50px;text-align: right;'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['EventDate']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;' class='my-commentcolumn'>".$photo[0]['EventDate']."</td></tr>";
	}
	$x .= "<tr><td style='width:50px;height:50px;text-align: right;padding-bottom:20px'><b style='color: gray;'>".$Lang['eClassApp']['DigitalChannels']['UploadDate']."</b></td><td style='width:75%;max-width:10px;padding-right: 10px;padding-left: 10px;padding-bottom:20px' class='my-commentcolumn'>".$dateInput."</td></tr>";
	$x .= "</tbody></table>";
	$x .= "<ul data-role='listview' id='defalt_class_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
	$x .= "<li style ='background-color: white !important;height: 30px;padding-left:35px;padding-bottom: 30px;padding-top: 0px;'><table><tr><td>".$photoViewHref.'</td><td style="padding-top: 7px;">&nbsp;&nbsp;'.$photo[0]['ViewTotal']."&nbsp;".$viewTotalTitle ."</td></tr></table></li>";
	if($isUserReadable){
	  $x .= "<li style ='background-color: white !important;height: 30px;padding-left:35px;padding-bottom: 30px;padding-top: 0px;'><table><tr><td>".$photoLikeHref.'</td><td style="padding-top: 7px;">&nbsp;&nbsp;'.$photo[0]['FavoriteTotal']."&nbsp;".$favTotalTitle."</td></tr></table></li>";
	  $x .= "<li style ='background-color: white !important;height: 30px;padding-left:35px;padding-bottom: 30px;padding-top: 0px;'><table><tr><td>".$photoCommentHref.'</td><td style="padding-top: 7px;">&nbsp;&nbsp;'.$photo[0]['CommentTotal']."&nbsp;".$commentTotalTitle."</td></tr></table></li>";		
	}
    $x .= "</ul></div>"; 
    	
$CommentTotal = $photo[0]['CommentTotal'];
$FavoriteTotal = $photo[0]['FavoriteTotal'];
echo $libeClassApp->getAppWebPageInitStart();
?>
	<style type="text/css">	
	.ui-icon-myapp-photo-view {
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$photoViewIconUrl?>) no-repeat !important;
	 }
	.ui-icon-myapp-photo-like {
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$photoLikeIconUrl?>) no-repeat !important;
	 }
	.ui-icon-myapp-photo-comment {
		 background-color: transparent !important;
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$photoCommentIconUrl?>) no-repeat !important;
	 }
	td.my-commentcolumn{
       margin:0 0 0 0 !important;
	   height:auto;
	   word-wrap: break-word;
    }
	</style>
	<script>
	$(document).ready( function() {
	});
	
	function get_Comment_Fav_Data(){
       return "CommentTotal=<?=$CommentTotal."&FavoriteTotal=".$FavoriteTotal?>";
    }
	</script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content" style="padding-left:0px;padding-right: 0px;padding-top: 0px;padding-bottom: 0px;">
   	        <?=$x?>   	        
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>