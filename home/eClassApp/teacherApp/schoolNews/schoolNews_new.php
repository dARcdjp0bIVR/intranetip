<?php
/*
 *	Modification Log:
 *
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 *  2018-06-22 (Philips)
 *      - added Time Selection
 *
 *	2016-09-21 (Tiffany) [ip.2.5.7.10.1]
 *		- modified groupSelectionBox mutiple select
 *	2015-09-08 (Ivan) [P84463] [ej.5.0.5.11.1]
 *		- added flag $sys_custom['eClassTeacherApp']['schoolNewsDefaultSpecificGroup'] handling 
 */

$PATH_WRT_ROOT = '../../../../';

//set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
//$leClassApp_init = new libeClassApp_init();
//$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);
//$intranet_session_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$uid = IntegerSafe($uid);
$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$linterface = new interface_html();
$libenroll = new libclubsenrol();

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$timestamp = time();
$date_time_array = getdate($timestamp);
$hours = $date_time_array[ "hours"];
$minutes = $date_time_array["minutes"];
$seconds = $date_time_array[ "seconds"];
$month = $date_time_array["mon"];
$day = $date_time_array["mday"] + 7;
$year = $date_time_array["year"];

// use mktime to recreate the unix timestamp
$timestamp =  mktime($hours, $minutes, $seconds ,$month, $day, $year);
$pre_startdate = date('Y-m-d',strtotime($today));
$pre_duedate = date('Y-m-d',$timestamp);

$lo = new libgrouping();
global $plugin;

$announcementGroups = $lo->returnSelectedCategoryGroupsFromType(0,"INTRANET_GROUPANNOUNCEMENT","AnnouncementID",0, 0);

//$i_admintitle_group
$x = '<div class="ui-field-contain" style="margin-left: 5px;margin-right: 5px;margin-top: 0px;margin-bottom: 0px;"><label for="GroupID">'.$Lang['Group']['Group'].'</label><select name="GroupID[]" id="GroupID" multiple="multiple" data-iconpos="left"  data-mini="true" data-native-menu="false">';
$showECA = 1;
$myListView = "";
for($i=0; $i<sizeof($announcementGroups); $i++){
	if ($plugin['eEnrollment'] && $showECA!=1&& $libenroll->isUsingYearTermBased && $announcementGroups[$i]['RecordType'] == 5) {
		continue;
	}
	$GroupID = $announcementGroups[$i][0];
    $Title = $announcementGroups[$i][1];
	$RecordTypeName = $announcementGroups[$i][3];
	$x .="<option value=$GroupID>($RecordTypeName) $Title</option>";
	$myListView .= "<li data-option-index='".$i."' data-icon='false' class='' role='option' aria-selected='false'><a href='#' tabindex='-1' class='ui-btn ui-checkbox-off ui-btn-icon-right'>(".$RecordTypeName.") ".$Title."</a></li>";
}
$x .= '</select></div>';

$nowChecked = false;
$scheduledChecked = true;
$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
    $sendTime = strtotime($sendTimeString);
    if ($sendTime > $nowTime) {
        $scheduledMessageChecked = "checked";
        $nowChecked = false;
        $scheduledChecked = true;
    }
    else {
        $scheduledMessageChecked = "";
        $sendTimeString = "";
    }
}
else {
    $sendTimeString = "";
}

$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
$nextTimeslotHour = date('H', $rounded_seconds);
$nextTimeslotMinute = date('i', $rounded_seconds);
if ($sendTimeString == "") {
    $selectionBoxDate = date('Y-m-d', $nowTime);
    $selectionBoxHour = $nextTimeslotHour;
    $selectionBoxMinute = $nextTimeslotMinute;
}
else {
    $selectionBoxDate = date('Y-m-d', $sendTime);
    $selectionBoxHour = date('H', $sendTime);
    $selectionBoxMinute = date('i', $sendTime);
}

$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $nowChecked=true, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");
$htmlAry['startTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("AnnouncementDate", ($ad==""?$preStartDate:$ad));
$htmlAry['endTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("EndDate", ($ed==""?$preEndDate:$ed));

if(!$sys_custom['schoolNews']['disableSendToParentAlert']) {
    $htmlAry['sendToParentAlert'] = '<span id="stu_parent_alert">
                                        <hr style="border: solid lightgrey 2px;border-radius:25px;"/>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="stu_parent_alert" id="stu_parent_alert" value="1"> <label for="stu_parent_alert">'.$Lang['SysMgr']['SchoolNews']['StudentParentAlert'].'</label>
									</span>';
}

$x_line = '';
//$x_line .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
$x_line .= "<div align='center'><input type='text' data-role='date' id='sendTime_date' name='sendtime_date' value='$selectionBoxDate' />";
$x_line .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
$x_line .= "<span style='font-size: 300%;vertical-align: top;'> : </span>";
$x_line .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15).'</div>';
$x_line .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
$x_line .= "<span id='div_push_message_date_err_msg'></span>";
$x_line .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
$htmlAry['sendTimeDateTimeDisplay'] = '<div id="specificSendTimeDiv" style='.($scheduledChecked? "":"display:none").'>';
$htmlAry['sendTimeDateTimeDisplay'] .= $x_line;
$htmlAry['sendTimeDateTimeDisplay'] .= '</div>';

echo $libeClassApp->getAppWebPageInitStart();
?>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style>
div.ui-select{
    width:auto; display: inline-block;
}
</style>
<script>
$(document).ready(function () {
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd'
    });
    var updated = "<?=$Updated?>";
    if(updated == 'Y'){
    	alert("<?=$Lang['SysMgr']['SchoolNews']['UpdateScuccess']?>");
    }else if(updated == 'N'){
    	alert("<?=$Lang['SysMgr']['SchoolNews']['UpdateUnsuccess']?>");
    }

    checkMessageAlert($('#push_message_alert'));
    groupSelection();
    
    <?php if ($sys_custom['eClassTeacherApp']['schoolNewsDefaultSpecificGroup']) { ?>
		$('input#publicdisplay').click();	
	<?php } ?>
});

function clickedSendTimeRadio(targetType) {
	if (targetType == 'now') {
		$('div#specificSendTimeDiv').hide();
	}
	else {
		$('div#specificSendTimeDiv').show();
	}
}

function submit(){
	if(checkform()){
		 document.form1.submit();	
	} 
	return false;
}

function checkform(){
	var announcementDate = document.getElementById("AnnouncementDate");
	announcementDate.value = $.datepicker.formatDate( "yy-mm-dd",new Date(announcementDate.value));
	var endDate = document.getElementById("EndDate");
	endDate.value = $.datepicker.formatDate( "yy-mm-dd",new Date(endDate.value));
	var title = document.getElementById("Title");
	if(!check_text(announcementDate, "<?php echo $Lang['SysMgr']['SchoolNews']['PleaseFillIn'].$Lang['SysMgr']['SchoolNews']['StartDate']; ?>.")) return false;
    if(!check_date(announcementDate, "<?php echo $Lang['SysMgr']['SchoolNews']['Invaliddate']; ?>.")) return false;
    if(!check_text(endDate, "<?php echo $Lang['SysMgr']['SchoolNews']['PleaseFillIn'].$Lang['SysMgr']['SchoolNews']['EndDate']; ?>.")) return false;
	if(!check_date(endDate, "<?php echo $Lang['SysMgr']['SchoolNews']['Invaliddate']; ?>.")) return false;
	if(!check_text(title, "<?php echo $Lang['SysMgr']['SchoolNews']['PleaseFillIn'].$Lang['SysMgr']['SchoolNews']['Title']; ?>.")) return false;
    if(compareDate(endDate.value,announcementDate.value)<0) 
		{
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		
		var today = new Date();
		var todayStr = today.getFullYear()+"-"+((today.getMonth()+1)<10?"0":"")+(today.getMonth()+1)+"-"+today.getDate();
		if(compareDate(announcementDate.value,todayStr)<0) 
		{
			alert ("<?=$Lang['Group']['WarnStratDayPassed']?>"); 
			return false;
		}
		
		var PublicDisplay = document.getElementById("publicdisplay");
		if(PublicDisplay != null)
		{
			var Group = document.getElementById("GroupID");
			if(PublicDisplay.checked == false){
				var checkCounter = 0;
				for(i=0; i<Group.length; i++){
                    if(Group.options[i].selected == true){
                    	checkCounter ++;
                    }
                 }
				if(checkCounter==0){
					alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
					return false;
				}
			}
		}
		
		var scheduleString = '';
   		if ($('input#sendTimeRadio_scheduled').attr('checked')) {
   	 		scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    	}
    	$('input#sendTimeString').val(scheduleString);
		
 	    document.getElementById('submit_btn').style.pointerEvents = 'none';
	    document.getElementById('submit_href').innerHTML ="<?=$Lang['General']['Submitting...']?>";
	    return true;
}


<?php global $sys_custom;?>
function groupSelection(){
	var obj = document.getElementById("publicdisplay");
	var group = document.getElementById("groupSelectionBox");
	if(obj.checked == false){
		group.style.display = "block";
		$('span#push_message_alert').show();
		
		if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
			$('input#stu_parent_alert').removeAttr('disabled');
			$('span#stu_parent_alert').show();
		}
		$('div#email_alertDiv').show();
	}
	else{
		group.style.display = "none";
		if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
			$('input#stu_parent_alert').removeAttr('checked');
			$('input#stu_parent_alert').attr('disabled', true);
			$('span#stu_parent_alert').hide();
		}
		if('<?=$sys_custom['schoolNews']['enableSendingEmailToAllUser']?>'!='1'){
//			$('#email_alert').attr('disabled',true);
			$('input#email_alert').removeAttr('checked');
			$('div#email_alertDiv').hide();
		}
	//	removeAllOption();
	}
}

function removeAllOption(){
    var Group = document.getElementById("GroupID");
	for(i=0; i<Group.length; i++){
        if(Group.options[i].selected == true){
        	Group.options[i].selected = false;
        }
     }
     var GroupIDMenu = document.getElementById("GroupID-menu");
     for(i=0; i<GroupIDMenu.length; i++){
        if(Group.options[i].selected == true){
        	Group.options[i].selected = false;
        }
     }
    document.getElementById('GroupID-button').innerHTML = '<span>&nbsp;</span><span class="ui-li-count ui-body-inherit" style="display: none;">0</span>';
	$("#GroupID-menu").empty();
	var myListView = "<?=$myListView?>";
	$('#GroupID-menu').append(myListView);   
}

function checkMessageAlert(ele){
	if($(ele)[0].checked==true){
		$('#PushMessageSetting').show();
	} else {
		$('#PushMessageSetting').hide();
	}
}

function syncIssueTimeToPushMsg(){
	var issueDate = $('#AnnouncementDate').val();
	$('#sendTime_date').val(issueDate);
}
</script>
</head>
	<body>	
	<div data-role="page">
        <form name="form1" action="new_update.php?UserID=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>" enctype="multipart/form-data" method="post">
        
          <div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">             
	         <table style="width: 100%;padding-left: 5px; background-color:#f6f6f6;">
			 <tr>
		       <td style="padding-left:5px;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['StartDate']?>
		       </td>
		       <td>					    
		          <input type="text" data-role="date" id="AnnouncementDate" name="AnnouncementDate" value="<?=$pre_startdate?>">
		       </td>
		     </tr>
		    <tr>
		       <td style="padding-left:5px;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['EndDate']?>
		       </td>
		       <td colspan="2"  style="padding-right:10px">
		          <input type="text" data-role="date" id="EndDate" name="EndDate" data-options="{'dateFormat': 'yyyy-MM-dd'}" value="<?=$pre_duedate?>">
		       </td>
		    </tr>
			<tr>
		       <td style="padding-left:5px;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['Title']?>
		       </td>
		       <td colspan="2"  style="padding-right:10px">
		          <input type="text" name="Title" id="Title"  value="<?=$pre_title?>">
		       </td>
		    </tr>
		    <tr>
		       <td style="padding-left:5px;;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['Description']?>
		       </td>
		       <td colspan="2"  id= 'xEditingArea' style="padding-right:10px"><?=$linterface->GET_TEXTAREA("Description","");?></td>
		    </tr>
		    <tr>
			<tr>
		       <td style="padding-left:5px;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['Attachment']?>
		       </td>
			   <td colspan="2" >
		           <input type="file" style="width:100%" class=file name="multiplefile[]" id="multiplefile[]" size=30 multiple>					 
			   </td>
		    </tr>
		    <tr>
		       <td style="padding-left:5px;padding-top:8px;width: 70px;" align="left" valign="top">
		          <?=$Lang['SysMgr']['SchoolNews']['Status']?>
		       </td>
			   <td colspan="2" style="padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;border-bottom-width: 0px;">
		          <input type="radio" name="RecordStatus" id="RecordStatus1" value="1" CHECKED style=" padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;"> <label for="RecordStatus1" style="background-color: #F9F9F9 !important;border-color: #F9F9F9 !important;"><?=$Lang['SysMgr']['SchoolNews']['StatusPublish']?></label>
			      <input type="radio" name="RecordStatus" id="RecordStatus0" value="0" style=" padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;"> <label for="RecordStatus0" style="background-color: #F9F9F9 !important;border-color: #F9F9F9 !important;"><?=$Lang['SysMgr']['SchoolNews']['StatusPending']?></label>
			   </td>
             </tr>
             <tr>
				<td colspan="2" align="left" valign="top">
					<input type="checkbox" name="publicdisplay" id="publicdisplay" onClick="groupSelection();" value="1" CHECKED> <label for="publicdisplay"><?=$Lang['SysMgr']['SchoolNews']['PublicAnnouncement']?></label>
				</td>
			 </tr>
			 <? if(!$special_feature['announcement_hide_email_alert'] || $plugin['eClassApp']) {?>
				<tr>
					<td colspan="2">
						<? if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
							<span id='push_message_alert' style='margin-left:20px;'>
							<input type="checkbox" name="push_message_alert" id="push_message_alert" value="1" onchange="checkMessageAlert(this)"> <label for="push_message_alert"><?=$Lang['SysMgr']['SchoolNews']['PushMessageAlert']?></label>
							<div id= 'PushMessageSetting' width="100%" style="border: 1px solid lightgrey; border-radius: 25px;padding-top:10px;">
							<span style="text-align:center; width: 100%;display: inline-block;"><?=$Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting']?></span>
							<table style="padding: 10px;width:100%;">
								<tr>
			   						<td colspan="2" style="padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;border-bottom-width: 0px;">
		          						<?php echo '<div id="sendTimeSettingDiv"><fieldset data-role="controlgroup" data-type="horizontal" align="center">'.
											$htmlAry['sendTimeNowRadio'].
											$htmlAry['sendTimeScheduledRadio'].'</fieldset>'?>
										<?=$htmlAry['sendTimeDateTimeDisplay']?>
			   						</td>
             					</tr>
             						<td colspan="2">
             							<?php echo $htmlAry['sendToParentAlert']?>
             						</td>
                                <tr>
                                </tr>
							</table>
							</div>
							</span><br>
						<? }?>
						<div id="email_alertDiv"></span>
						<? if(!$special_feature['announcement_hide_email_alert']) {?>
							<input type="checkbox" name="email_alert" id="email_alert" value="1"> <label for="email_alert"><?=$Lang['SysMgr']['SchoolNews']['EmailAlert']?></label>
						<? }?>
						<span'>(<?=$Lang['eNotice']['NotifyRemark']?>)</span></div>
					</td>
				</tr>
			 <? } ?>
			  </table>
			  
			  <!-- Groups -->
			  <div id="groupSelectionBox" style="display:none;">
		        <?=$x?>
			 </div>
           </div>
         <div data-role="footer" id= "submit_btn" data-position="fixed"  style ="background-color:white;height:70px" align="center">
           <a href="javascript:submit();" id= "submit_href" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
                 padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$Lang['ePost']['Button']['Submit']?></a>
         </div>	 
		</form>
	  </div>
	</body>
</html>

<?php
intranet_closedb();
?>