<?php
/*
 *	Modification Log:
 *	2019-11-25 (Philips) [2019-1108-1407-52054]
 *		- use $sys_custom['eClassApp']['studentPerformance_maxChangeAmount'] to modify Max Change Amount of marks
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 */

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$StudentID = IntegerSafe($StudentID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
$PerformanceRecordID = IntegerSafe($PerformanceRecordID);
### Handle SQL Injection + XSS [END]

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$libDB = new libdb(); 
$NameField = getNameFieldByLang();

$libuser = new libuser($StudentID);
$student = $libuser->User;
$student = $student[0];

$addIcon = $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_plus.png";
$minusIcon =  $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_minus.png";
$NameLangTag = Get_Lang_Selection('ChineseName', 'EnglishName');
$x = $libeClassApp->getCenterHeader($student['ClassName']."-".$student['ClassNumber']." ".$student[$NameLangTag]);

$score = 0;
$Comment = "";
$submitCond = '';
$maxAmount = $sys_custom['eClassApp']['studentPerformance_maxChangeAmount'] ? $sys_custom['eClassApp']['studentPerformance_maxChangeAmount'] : 10;
if($PerformanceRecordID != ""){
   $sql = "select Score, Comment from INTRANET_APP_PERFORMANCE_STUDENT where StudentID='".$StudentID."' and SubjectGroupID='".$SubjectGroupID."' and PerformanceRecordID='".$PerformanceRecordID."'";
   $result = $libDB->returnResultSet($sql);
   $score = $result[0]['Score'];
   $Comment = $result[0]['Comment'];
   $submitCond = '&PerformanceRecordID='.$PerformanceRecordID;
   $goBackUrl = "student_performance_detailpage.php?uid=".$uid."&token=".$token."&ul=$ul&parLang=$parLang&SubjectGroupID=$SubjectGroupID&StudentID=$StudentID&selectedSortID=$selectedSortID&ChosenTermID=$ChosenTermID&DetailTag=$DetailTag";
}else{
   $goBackUrl = "group_student_performanceList.php?uid=".$uid."&token=".$token."&ul=$ul&parLang=$parLang&SubjectGroupID=$SubjectGroupID&selectedSortID=$selectedSortID&ChosenTermID=$ChosenTermID";
}

$_personalphotoPathAry = $libuser->GET_USER_PHOTO($student['UserLogin']);
$student['PhotoLink'] = $_personalphotoPathAry[1];
$x .= '<table style="width:-webkit-calc(100% - 10px);;padding-left:10px"><tr>
       <td style="width:100px!important;background-size: 100px 100px!important;"><img style="width:100px!important;background-size: 100px 100px!important;" src='.$student['PhotoLink'].' align="left"></td>
       <td style="width:-webkit-calc(100% - 100px);padding-top:0px"><table><tr>
       <td id="performanceScore">'.$Lang['eClassApp']['StudentPerformance']['ActScore'].':</td>
       <tr><td>
       <table><tr>
       <td><a href="#" data-icon="my-minusIcon" data-position-to="window" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:70px!important;height:70px!important;background-size: 70px 70px!important;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom:0px;margin-top:0px"  onclick ="MinusScore();">
       </a></td>
       <td style="width:30px;max-width:30px;text-align:center;vertical-align:middle;font-size:1.50em"><div id="CurrentScore" name="CurrentScore" style="color:#5E9800">'.$score.'</div></td>
       <td><a href="#" data-icon="my-addIcon" data-position-to="window" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:70px!important;height:70px!important;background-size: 70px 70px!important;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom:0px;margin-top:0px"  onclick ="AddScore();">
       </a></td>
       </tr>
       </table></td>
       </tr>
       </tr>
       <tr><td>
       <input type="range" name="slider-s" id="slider-s" value="'.$score.'" min="-'.$maxAmount.'" max="'.$maxAmount.'" data-highlight="true">
       </td></tr>
       </table></td>
       </tr></table>';
$x .= '<div style="background-color:#E3E3E3;height:30px"><h1 style="text-align: left;width=100%;font-size:15px;text-shadow: none!important;margin-left: 20px;padding-top:5px">'.$Lang['eClassApp']['StudentPerformance']['Comment'].':</h1></div>';
$x .= '<div style="margin-left:10px;margin-right:10px;"><textarea rows="8" cols="50" id="Comment" name="Comment">'.$Comment.'</textarea></div>';       
$x .= '<div style="padding-left:16px;padding-right:16px"><button type="button" style ="background-color:#429DEA;padding:0 0 0 0;" onclick="submitNewScore();"><h1 style ="text-align: center;width=100%;font-size:20px;color:white;text-shadow: none!important;margin-left:0px;margin-right:0px">'.$Lang['eClassApp']['StudentPerformance']['Submit'].'</h1></button></div>';
$submitScore = 'submit.php?StudentID='.$StudentID.'&SubjectGroupID='.$SubjectGroupID.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'&ChosenTermID='.$ChosenTermID.'&selectedSortID='.$selectedSortID.$submitCond;

echo $libeClassApp->getAppWebPageInitStart();
?>

	<style type="text/css">
	 .ui-icon-my-addIcon{			
	     border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$addIcon?>) no-repeat !important;
	}
	.ui-icon-my-minusIcon{			
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$minusIcon?>) no-repeat !important;
	}
	textarea.ui-input-text { height: inherit !important}
	.ui-slider-input {
		display : none !important;
    }
    .ui-slider{
    	margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .ui-bar-inherit{
    	margin-left: 0px !important;
        margin-right: 0px !important;
    }
	</style>
	<script>
	var changeMaxAmount = <?=$maxAmount ? $maxAmount : 0?>;
	 $(document).ready( function() {	
 	    $( "#slider-s").change(function(e){
		   var slider_value=$("#slider-s").slider().val();
		   if(slider_value >= 0){
		   	  AddScore(slider_value);
		   }else{
		   	  MinusScore(slider_value);
		   }
        });	 
	});
	
	function submitNewScore(){
		var commentContent = $('#Comment').val();
		commentContent = commentContent.replace(/\s+/g, "");
		if(commentContent ==""){
			$('#Comment').val('');
		}
		var submitScore =  $('#CurrentScore').html();
		$('#Score').val(submitScore);
		document.submitScore.submit();
	}
	
	function AddScore(newScore){
	   var currentScore = 0;
	   if((newScore >= -changeMaxAmount)&&(newScore <= changeMaxAmount)){
	   	  currentScore = newScore;
	   }else{
	   	 currentScore = $('#CurrentScore').html();
	   	 currentScore++;
	   	 $("#slider-s").slider().val(currentScore);
	   	 $('#slider-s').slider('refresh');
	   }

	   if(currentScore >= 0){
	          document.getElementById("CurrentScore").style.color = "green";
	    }
	   	if(currentScore <= changeMaxAmount){
	    $('#CurrentScore').html(currentScore);
	    }
	}
	function MinusScore(newScore){
	   var currentScore = 0;
	   if((newScore >= -changeMaxAmount)&&(newScore <= changeMaxAmount)){
	   	  currentScore = newScore;
	   }else{
	   	 currentScore = $('#CurrentScore').html();
	     currentScore--;
	     $("#slider-s").slider().val(currentScore);
	   	 $('#slider-s').slider('refresh');
	   }
	   
	    if(currentScore < 0){
	          document.getElementById("CurrentScore").style.color = "red";
	    }
	    if(currentScore>=-changeMaxAmount){
	    $('#CurrentScore').html(currentScore);
	    }
	}	
	
	function goBack(){
        var url = "<?=$goBackUrl?>";
	    window.location = url;
    }
	</script>
</head>
	<body>	
		<div data-role="page" id="body" >	
		 <div data-role="content" style="padding-left: 0px;padding-right: 0px;">
		   	<form class="ui-filterable"  id = "submitScore" name = "submitScore" style = "background-color:#FFFFFF"  method="post" action="<?=$submitScore?>" enctype="multipart/form-data">
   	        <?=$x?>
   	        <input type="hidden" id="Score"  name="Score">
   	        </form>
   	        
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>