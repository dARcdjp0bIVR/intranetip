<?php
/*
 *	Modification Log:
 *
 *  2019-05-10 (Bill)
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 */

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$_SESSION['CurrentSchoolYearID'] = '';

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$ChosenTermID = IntegerSafe($ChosenTermID);
### Handle SQL Injection + XSS [END]

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$libDB = new libdb();
$subject_class_mapping = new subject_class_mapping();

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$AcademicYearID = Get_Current_Academic_Year_ID();
$academic_year = new academic_year($AcademicYearID);
$termList = $academic_year->Get_Term_List();
$ChosenTermID = $ChosenTermID !=''? $ChosenTermID : getCurrentSemesterID();

$subjectGroupNameLang = Get_Lang_Selection('SubjectGroupNameCh', 'SubjectGroupNameEn');
$Accessible_Subject_Group = $subject_class_mapping->Get_User_Accessible_Subject_Group($uid,$AcademicYearID,$ChosenTermID);

$subjectGroupAry = array();
$subjectHashGroupAry = array();
$latestModifyTimeAry = array();
$classIDAry = array();
$subjectGroupAryString = "";
if(count($Accessible_Subject_Group)>0){
    for($i=0;$i<count($Accessible_Subject_Group);$i++){
    	$tempSubjectGroupID = $Accessible_Subject_Group[$i]['SubjectGroupID'];
    	$tempClassID = $Accessible_Subject_Group[$i]['ClassID'];
    	$subjectGroupIDAry[] = $tempSubjectGroupID;
    	$classIDAry[] = $tempClassID;
    	$subjectHashGroupAry[$tempSubjectGroupID][] = $Accessible_Subject_Group[$i];
    }
    $subjectGroupAryString = implode(",", $subjectGroupIDAry);
    $classIDAryString = implode(",", $classIDAry);
    $sql = "Select 
                  X.SubjectGroupID,
                  X.Score,
                  X.DateModified,
                  c.ClassID
            From 
                  INTRANET_CLASS as c
                  LEFT JOIN INTRANET_USER as u on c.ClassName=u.ClassName
                  LEFT JOIN (select * from INTRANET_APP_PERFORMANCE_STUDENT ORDER BY DateModified DESC) AS X 
           WHERE 
                   X.SubjectGroupID IN (".$subjectGroupAryString.") AND  X.TeacherID='".$uid."' and c.ClassID IN (".$classIDAryString.") group by c.ClassID,X.SubjectGroupID";
    $latestModifyTimeAry = $libDB->returnResultSet($sql);
}

$latestModifyTimeHashAry = array();
for($j=0;$j<count($latestModifyTimeAry);$j++){
	$tempSubjectGroupID = $latestModifyTimeAry[$j]['SubjectGroupID'];
	$latestModifyTimeHashAry[$tempSubjectGroupID][] = $latestModifyTimeAry[$j];
}

foreach($subjectHashGroupAry as $key=>$value){
	$tempLastModifyTimeAry = $latestModifyTimeHashAry[$key];
	if($tempLastModifyTimeAry !=null){
		$subjectHashGroupAry[$key][0]['LatestModify'] = $tempLastModifyTimeAry[0]['DateModified'];
	}else{
		$subjectHashGroupAry[$key][0]['LatestModify'] = "";
	}
}

#HTML GENERATE
  $term_selection_body = '<table style="padding-left: 14px;"><tr><td>'.
                         '<p style="text-align:left; margin-left:5px;margin-top: 18px;margin-right:5px;" class="ui-li-text-my-title-font">'.$Lang['eClassApp']['StudentPerformance']['Term'].'</p></td>'.
                         '<td style="padding-left:6px;"><select name="select-term" id="select-term" data-native-menu="false" onchange="updateTerm(this);">';
	foreach ($termList as $key => $value){
		$isSelected = '';
		if($key==$ChosenTermID){
		$isSelected = 'selected="selected"';
		}
		$term_selection_body .= '<option value="'.$key.'" '.$isSelected.'>'.$value.'</option>';
	}
   $term_selection_body .= '</select></td></tr></table>';
   $header = '<header data-role ="header"  data-position="fixed"  style ="background-color:#F1F1F1;">
                                 '.$term_selection_body.' 
								 </header>';
   $content = "<div id='my-wrapper'><ul data-role='listview' id='term_group_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
   foreach($subjectHashGroupAry as $key=>$value){
		    $tempLastModifyTimeAry = $latestModifyTimeHashAry[$key];
		    $tempLastModifyTime = "";
		    $tempLastModifyTimeBG= "";
			if($tempLastModifyTimeAry !=null){
			   $tempLastModifyTime = $tempLastModifyTimeAry[0]['DateModified'];
			   $tempLastModifyCompareTime = date('Y-m-d', strtotime($tempLastModifyTime));
			   if(strtotime($tempLastModifyCompareTime) === strtotime('today')){
			   $tempLastModifyTimeBG = "color:#5E9800";
			   }
			}
   $content .="<li style ='background-color: white !important;'><a href='#'  id = '".$value[0]['SubjectGroupID']."'  style ='background-color: transparent !important;padding-left:20px;padding-top:3px;padding-bottom:3px' onclick = 'getGroupStrudentList(this);'>
	<p style='font-weight: bold;font-size: 15px;'>".$value[0][$subjectGroupNameLang]."</p>";
	if($tempLastModifyTime != ""){
   $content .= "<p style='font-size: 10px;".$tempLastModifyTimeBG."'>".$tempLastModifyTime."</p>";		
	}
   $content .="</a></li>"."\r\n";	
   }
   $content .= "</ul></div>";	
  $x .= $header.$content;
  
echo $libeClassApp->getAppWebPageInitStart();
?>
	<style type="text/css">
	.ui-field-contain>label{
		width:50px
	}
	</style>
	<script>
	 $(document).ready( function() {	
	});
	function updateTerm(chooseClass){
		    var chosen_term_id = chooseClass.value;		
		    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/teacher_term_groupList.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&ChosenTermID="+chosen_term_id;
		    setTimeout(function(){
		    	 window.location = url;
		    	    }, 200);
	}
	function getGroupStrudentList(chooseClass){
		    var chosen_subjectGroupID = chooseClass.id;
		    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/group_student_performanceList.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID="+chosen_subjectGroupID+"&ChosenTermID=<?=$ChosenTermID?>";
		    window.location = url;
	}
	</script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content">
   	        <?=$x?>
   	        
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>