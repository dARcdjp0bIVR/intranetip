<?php 
/*
 * Change log
 * 
 * 2019-05-10 Bill
 *      - prevent SQL Injection
 *      - apply intranet_auth()
 * 2018-06-04 Isaac - added condition to include * mark to left students.
 */

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
### Handle SQL Injection + XSS [END]

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$libDB = new libdb();
$libuser = new libuser($uid,$ul);

$AcademicYearID = Get_Current_Academic_Year_ID();
$subject_class_mapping = new subject_class_mapping();
$sql = 'SELECT ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = "'.$SubjectGroupID.'"';
$subjectGroupInfo = $libDB->returnResultSet($sql);
$groupName = Get_Lang_Selection($subjectGroupInfo[0]['ClassTitleB5'], $subjectGroupInfo[0]['ClassTitleEN']);

$sortArrayTag = array("LatestScore","TotalScore","TotalComment");
$sortArray = array();
$sortArray[1] = $Lang['eClassApp']['StudentPerformance']['Number'];$sortArray[2]= $Lang['eClassApp']['StudentPerformance']['Name'] ;$sortArray[3]= $Lang['eClassApp']['StudentPerformance'][$sortArrayTag[0]];
$sortArray[4]= $Lang['eClassApp']['StudentPerformance'][$sortArrayTag[1]];$sortArray[5]= $Lang['eClassApp']['StudentPerformance'][$sortArrayTag[2]];
$selectedSortID = $selectedSortID !=null?$selectedSortID:1;

$sortKey = '';
if($selectedSortID == 1){
//	$sortKey = ' ORDER BY u.ClassName DESC, u.ClassNumber+0 ASC';
	$sortKey = ' ORDER BY u.ClassName ASC, u.ClassNumber+0 ASC';
}else if($selectedSortID == 2){
	$sortKey = ' ORDER BY StudentName ASC';
}

$NameField = getNameFieldByLang('u.');
$ArchiveNameField = getNameFieldByLang2('au.');
		$sql = 'Select 
							stct.SubjectGroupID,
							u.UserID, 
                            u.UserLogin,
                            u.ClassName,
                            u.ClassNumber,
							CASE 
								WHEN au.UserID IS NOT NULL then CONCAT(\'<font style="color:red;">*</font>\','.$ArchiveNameField.') 
								WHEN u.RecordStatus = 3  THEN CONCAT(\'<font style="color:red;">*</font>\','.$NameField.') 
                                ELSE '.$NameField.' 
							END as StudentName 
						From 
							SUBJECT_TERM_CLASS_USER as stct 
							LEFT JOIN 
							INTRANET_USER as u 
							on stct.UserID = u.UserID 
							LEFT JOIN 
							INTRANET_ARCHIVE_USER as au 
							on stct.UserID = au.UserID 
						where 
							stct.SubjectGroupID="'.$SubjectGroupID.'"'.$sortKey;
$studentList = $libDB->returnResultSet($sql);

$studentPerformanceAry = array();
$userIDAry = array();
function Users($Students){
    return $Students['UserID'];
}
$userIDAry = array_map("Users", $studentList);
if(count($userIDAry)>0){
    $userIDs = implode(",", array_remove_empty($userIDAry));
    $sql = "Select StudentID,Score,Comment from INTRANET_APP_PERFORMANCE_STUDENT where StudentID IN (".$userIDs.") AND SubjectGroupID='".$SubjectGroupID."' ORDER BY StudentID,DateInput DESC";
    $studentPerformanceAry = $libDB->returnResultSet($sql);
}

#
$studentPerformanceCounterAry = array();
for($j=0;$j<count($studentPerformanceAry);$j++){
	$tempStudentID = $studentPerformanceAry[$j]['StudentID'];
	//if($studentPerformanceAry[$j]['Score'] != 0){
	if($studentPerformanceAry[$j]['Score'] !== null){
    	$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[1]] = $studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[1]]==null?0:$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[1]];
    	$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[1]] +=$studentPerformanceAry[$j]['Score'];	
	}
	if($studentPerformanceAry[$j]['Comment'] != null){
    	$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[2]] = $studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[2]]==null?0:$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[2]];
    	$studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[2]]++;
	}
	#set the latest score as the first not 0 one
	//if(($studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[0]]==null)&&($studentPerformanceAry[$j]['Score']!=0)){
	if(($studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[0]]==null)&&($studentPerformanceAry[$j]['Score']!==null)){
	   $studentPerformanceCounterAry[$tempStudentID][$sortArrayTag[0]] = $studentPerformanceAry[$j]['Score'];
	}
}

#Hash StudentList
$StudentHashAry = array();
$NoScoreStudentAry = array();
for($m=0;$m<count($studentList);$m++){
    $tempUserID = $studentList[$m]['UserID'];
    $StudentHashAry[$tempUserID] = $studentList[$m];
    if($studentPerformanceCounterAry[$tempUserID] == null){
        $NoScoreStudentAry[] = $tempUserID;
    }
}

#sorting List
$studentPerformanceSortIDAry = array();
switch($selectedSortID){
	case 1:#sort by number
    	foreach($StudentHashAry as $key=>$value){
    	   $studentPerformanceSortIDAry[] = $key;
    	}
    	break;
	case 2:#sort by name
    	foreach($StudentHashAry as $key=>$value){
    	   $studentPerformanceSortIDAry[] = $key;
    	}
    	break;
    case 3:#sort by latest score
          $sortable_key_array = getSortableKeyArray($studentPerformanceCounterAry,$sortArrayTag[0]);  
          $studentPerformanceSortIDAry = array_merge($sortable_key_array,$NoScoreStudentAry);
        break;
    case 4:#sort by total score
          $sortable_key_array = getSortableKeyArray($studentPerformanceCounterAry,$sortArrayTag[1]);
          $studentPerformanceSortIDAry = array_merge($sortable_key_array,$NoScoreStudentAry);
        break;
    case 5:#sort by total comment
          $sortable_key_array = getSortableKeyArray($studentPerformanceCounterAry,$sortArrayTag[2]);
          $studentPerformanceSortIDAry = array_merge($sortable_key_array,$NoScoreStudentAry);
        break;
}
	function getSortableKeyArray($Ary,$Key){
		$sortable_key_array = array();
		$sortedIdAry = array();
		foreach($Ary as $k=>$v){
			 if (is_array($v)) {
			 	$sortable_key_array[$k] = $v[$Key];
			 }	
		}
		arsort($sortable_key_array);	
		foreach($sortable_key_array as $ID=>$value){
			$sortedIdAry[] = $ID;
		}
		return $sortedIdAry;
	}
	
	$currentPageTrack = "&ChosenTermID=".$ChosenTermID."&selectedSortID=".$selectedSortID;

#HTML GENERATE
$sortSelection = $libeClassApp->getSelectionHtml($sortArray,'',$selectedSortID,'');
$header = '<header data-role ="header"  data-position="fixed"  style ="background-color:#F1F1F1;">
								 <p  style="text-align:left; margin-left:5px;margin-top: 18px;" class = "ui-li-text-my-title-font">'."&nbsp&nbsp".$groupName.'</p>
								 <fieldset  class="ui-btn-right" data-role="controlgroup" data-type="horizontal" style="right: 20px !important; top:10px !important;" >
								 <div class="ui-controlgroup-controls" style="font-size:0;!important">
								 '.$sortSelection.'
								 </div>
								 </fieldset>   
								 </header>';
$x = $header;
$x .="<div id='my-wrapper'><ul data-role='listview' data-inset='false' style = 'margin-bottom: 0px;padding-left:0px;padding-top:0px;'>";
$x .= "<li data-icon='false' style='padding-right:0px;padding-left:10px'>".
       "<table><tr>".
       "<td style ='width:45%;'>".$Lang['eClassApp']['StudentPerformance']['Student']."</td>".
       "<td style ='width:50px;padding-top:0px;' class='my-warpContent;'>".$sortArray[3]."</td>".
       "<td style ='width:35px;padding-top:0px;' class='my-warpContent;'>".$sortArray[4]."</td>".
       "<td style ='width:35px;padding-top:0px;' class='my-warpContent;'>".$sortArray[5]."</td>".
       "</tr></table></li>"."\r\n";

$positiveBG = $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_positive_score.png";
$negativeBG =  $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_negative_score.png";
$noScoreBG = $image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/studentPerformance/btn_gray.png";

for($i=0;$i<count($studentPerformanceSortIDAry);$i++)
{
	$tempIndexStudentID = $studentPerformanceSortIDAry[$i];
	#initialize the latestScore Backgroud and text color as grey
	$tempStudentNewScoreBG = 'my-noScoreBG';
	$tempStudentNewScoreTextColor = '#D6D6D6';
	#initialize the Score and comment text color as blue
	$tempStudentScoreTextColor = '';$tempStudentCommentTextColor = '';
	if($studentPerformanceCounterAry[$tempIndexStudentID]==null){
		#no comment and score case
		$tempStudentScoreTextColor = 'style="color:#D6D6D6"';
		$tempStudentCommentTextColor = 'style="color:#D6D6D6"';
		$tempStudentNewScore = '-';	
		$tempStudentTotalScore = '-';
		$tempTotalComment = '-';
	}else{
		#set latest bg and text color if have one
		if($studentPerformanceCounterAry[$tempIndexStudentID][$sortArrayTag[0]] != null){
			$tempStudentNewScore = $studentPerformanceCounterAry[$tempIndexStudentID][$sortArrayTag[0]];
			$tempStudentTotalScore = $studentPerformanceCounterAry[$tempIndexStudentID][$sortArrayTag[1]];
			//set Backgroud and text color	
			if($tempStudentNewScore>0){
				$tempStudentNewScore = '+'.$tempStudentNewScore;
				$tempStudentNewScoreBG = 'my-positiveBG';	
	            $tempStudentNewScoreTextColor = '#5E9800';	
			}else if($tempStudentNewScore<0){
				$tempStudentNewScoreBG = 'my-negativeBG';	
		        $tempStudentNewScoreTextColor = '#F80008';	
			}
		}else{
			#no score only comment
			$tempStudentScoreTextColor = 'style="color:#D6D6D6"';
			$tempStudentNewScore = '-';
			$tempStudentTotalScore = '-';
		}		
		
		if($studentPerformanceCounterAry[$tempIndexStudentID][$sortArrayTag[2]]==null){
			#set comment as - and color grey if no comment
			$tempStudentCommentTextColor = 'style="color:#D6D6D6"';
			$tempTotalComment = '-';
		}else{
		    $tempTotalComment = $studentPerformanceCounterAry[$tempIndexStudentID][$sortArrayTag[2]];
		}
    }
$tempStudentSerial = $StudentHashAry[$tempIndexStudentID]['ClassName'].'-'.$StudentHashAry[$tempIndexStudentID]['ClassNumber'];
$_personalphotoPathAry = $libuser->GET_USER_PHOTO($StudentHashAry[$tempIndexStudentID]['UserLogin']);
$tempStudentPhoto = $_personalphotoPathAry[1];
$tempStudentNewScoreHtml = '<a  href="#" data-icon="'.$tempStudentNewScoreBG.'" data-position-to="window" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:50px!important;height:50px!important;background-size: 50px 50px!important;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;margin-bottom:0px;margin-top:0px" id='.$StudentHashAry[$tempIndexStudentID]['UserID'].' onclick ="ScoreStudent(this);">
                       <p class="ui-li-text-my-center-p" style="color:'.$tempStudentNewScoreTextColor.';padding-top:5px;">'.$tempStudentNewScore.'</p></a>';
$tempStudentTotalScoreHtml = '<a href="#" id='.$StudentHashAry[$tempIndexStudentID]['UserID'].' onclick ="ShowTotalScore(this);" '.$tempStudentScoreTextColor.'>'.$tempStudentTotalScore.'</a>';
$tempStudentTotalCommentHtml = '<a href="#" id='.$StudentHashAry[$tempIndexStudentID]['UserID'].' onclick ="ShowComments(this);" '.$tempStudentCommentTextColor.'>'.$tempTotalComment.'</a>';
$x .= "<li data-icon='false' style='padding-right:0px;padding-left:10px'>".
       "<table><tr>".
       "<td style ='width:45%;'><img src=".$tempStudentPhoto." align='left'  style = 'height: 50px;!important;background-size: 50px 50px!important;'><p style='padding-left:5px'> ".$StudentHashAry[$tempIndexStudentID]['StudentName']."<br> ".$tempStudentSerial."</p></td>" .
       "<td style ='width:50px;padding-top:0px;'>".$tempStudentNewScoreHtml."</td>".
       "<td style ='width:35px;padding-top:0px;'><h3 class='ui-li-text-my-h3'>".$tempStudentTotalScoreHtml."</h3></td>".
       "<td style ='width:35px;padding-top:0px;'><h3 class='ui-li-text-my-h3'>".$tempStudentTotalCommentHtml."</h3></td>".
       "</tr></table></li>"."\r\n";	
}
$x .= "</ul></div>";
$currentBackUrl = $PATH_WRT_ROOT."home/eClassApp/teacherApp/studentPerformance/teacher_term_groupList.php?uid=".$uid."&token=".$token."&ul=".$ul."&parLang=".$parLang."&ChosenTermID=".$ChosenTermID;
$hiddenBackHtml = '<input type="hidden" id ="BackTargetLink" name="BackTargetLink" value="'.$currentBackUrl.'">';
$x .= $hiddenBackHtml;
#END HTML GENERATE 
echo $libeClassApp->getAppWebPageInitStart();
?>
	<style type="text/css">
	table { 
	  table-layout: fixed;
	  width: 100%
	}
    td {
    white-space: -o-pre-wrap; 
    word-wrap: break-word;
    white-space: pre-wrap; 
    white-space: -moz-pre-wrap; 
    white-space: -pre-wrap; 
    }
	div.my-warpContent{
    margin:0 0 0 0 !important;
	height:auto;
	word-wrap: break-word;
	}
	.my-hidenContent{
	overflow: hidden; !important;
	white-space: nowrap; !important;
	text-overflow: ellipsis;
	}
	p {
	color:black;
	font-size:1.00em !important;
	text-align: left
	vertical-align: middle;
	}
     .ui-icon-my-positiveBG{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$positiveBG?>) no-repeat !important;
	}
	.ui-icon-my-negativeBG{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$negativeBG?>) no-repeat !important;
	}
	.ui-icon-my-noScoreBG{			
	 border: 0 !important;
	 -webkit-box-shadow:none !important;
	 background: url(<?=$noScoreBG?>) no-repeat !important;
	}
	
	span.text-content span {
	  opacity: 1;
	  display: table-cell;
	  text-align: center;
	  vertical-align: middle;
	}
	.ui-li-text-my-h3{
	  font-size:1.00em !important;
	  text-align: center;
	  vertical-align: middle;
	}
	.ui-li-text-my-center-p{
	  font-size:1.00em !important;
	  text-align: center;
	  vertical-align: middle;
	}
	a {
      text-decoration:none;
    }
	</style>
	<script>
	$(document).ready(function() {		 
	});
	function updateTerms(chooseClass){
	    var chosen_sort_id = chooseClass.value;		
	    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/group_student_performanceList.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&selectedSortID="+chosen_sort_id+"&ChosenTermID=<?=$ChosenTermID?>";
	    setTimeout(function(){
	    	 window.location = url;
	    	    }, 200);
	}
	function ScoreStudent(chooseClass){
		var chosen_student_id = chooseClass.id;		
	    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/score_student_performance_page.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&StudentID="+chosen_student_id+"<?=$currentPageTrack?>";
	    window.location = url;
	}
	function ShowTotalScore(chooseClass){
		var chosen_student_id = chooseClass.id;		
	    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/student_performance_detailpage.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&StudentID="+chosen_student_id+"&DetailTag=Score<?=$currentPageTrack?>";
	    window.location = url;
	}
	function ShowComments(chooseClass){
		var chosen_student_id = chooseClass.id;		
	    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/student_performance_detailpage.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&SubjectGroupID=<?=$SubjectGroupID?>&StudentID="+chosen_student_id+"&DetailTag=Comment<?=$currentPageTrack?>";
	    window.location = url;
	}
	
	function goBack(){
		var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentPerformance/teacher_term_groupList.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&ChosenTermID=<?=$ChosenTermID?>";
		window.location = url;
	}
	</script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content">
   	        <?=$x?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>