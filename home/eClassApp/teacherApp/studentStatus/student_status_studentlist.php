<?php
/**
 * editing by:
 *
 * Change log:
 *
 *  Date:   2019-09-27  (Bill)  [2019-0924-1534-15235]
 *          fixed : display duplicated student records
 *                  > for approved apply leave request only
 */

$PATH_WRT_ROOT = '../../../../';

//include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
//$leClassApp_init = new libeClassApp_init();
//$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

// set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

intranet_auth();
intranet_opendb();

$uid = IntegerSafe($_GET['uid']);
$year_class_id = IntegerSafe($_GET['year_class_id']);

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$_displayTitleLang = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');

$libDB = new libdb();
$libuser = new libuser($uid,$ul);
$lc = new libcardstudentattend2();
$lapplyleave = new libapplyleave();

$myBackIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white.png"."\"";

### Set Date from previous page
if ($TargetDate == "") {
	$TargetDate = date('Y-m-d');
}
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$lc->createTable_LogAndConfirm($txt_year,$txt_month);

### period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM:
	  $display_period = $i_DayTypeAM;
      $link_page = "AM";
      $confirm_page="AM";
      break;
    case PROFILE_DAY_TYPE_PM: 
      $display_period = $i_DayTypePM;
      $confirm_page="PM";
      //}
      $link_page="PM";
      break;
    default : 
        if ($lc->attendance_mode == 0) {
	    	$display_period = $i_DayTypeAM;
		    $DayType = PROFILE_DAY_TYPE_AM;
		    $link_page = "AM";
		    $confirm_page="AM";
		}
    	else if ($lc->attendance_mode == 1) {
    		$display_period = $i_DayTypePM;      
    		$DayType = PROFILE_DAY_TYPE_PM;
	        $confirm_page="PM";
	        $link_page="PM";
    	}
    	else {
    		if(date("a")=="am")
			{
			    $display_period = $i_DayTypeAM;
			    $DayType = PROFILE_DAY_TYPE_AM;
			    $link_page = "AM";
			    $confirm_page="AM";		
		   	}
			else
			{
	    		$display_period = $i_DayTypePM;      
	    		$DayType = PROFILE_DAY_TYPE_PM;
		        $confirm_page="PM";
		        $link_page="PM";
			}		
		  }
	    break;
}
//debug_pr($lc->attendance_mode);
//
//debug_pr($DayType);

$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

if ($DayType == PROFILE_DAY_TYPE_AM) {
	$expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
	$status_field = "AMStatus";
	$time_field = "InSchoolTime";
	$station_field = "InSchoolStation";
	
	$IsConfirmField = "IsConfirmed";
	$ConfirmUserField = "ConfirmedUserID";
	
	$DateModifiedField = "DateModified";
	$ModifyByField = "ModifyBy";
	
	$daily_remark_field = "d.Remark as TeacherRemark ";
}
else {
	$expect_field = "IF(b.PMStatus IS NOT NULL,
                          b.PMStatus,
                          ".$lc->Get_PM_Expected_Field("b.","c.","f.").")";
    $status_field = "PMSatus";
	if ($lc->attendance_mode==1)
	{
    $time_field = "InSchoolTime";
	$station_field = "InSchoolStation";
	}
	else
	{
    $time_field = "LunchBackTime";
    $station_field = "LunchBackStation";
	}
	
	$IsConfirmField = "PMIsConfirmed";
	$ConfirmUserField = "PMConfirmedUserID";
	
	$DateModifiedField = "PMDateModified";
	$ModifyByField = "PMModifyBy";
	
	$daily_remark_field = "IF(d.RecordID IS NULL AND TRIM(d2.Remark)<>'',d2.Remark,d.Remark) as TeacherRemark ";
	$join_am_daily_remark_table = " LEFT JOIN CARD_STUDENT_DAILY_REMARK as d2 ON d2.StudentID=a.UserID AND d2.RecordDate='$TargetDate' AND d2.DayType='".PROFILE_DAY_TYPE_AM."' ";
}
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$settingAssoAry = $lapplyleave->getSettingsAry();
if($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1){
    $methodField="CONCAT(
								CASE r.HomeworkDeliverMethodID
									WHEN '-1' THEN ''
									WHEN '0' THEN r.HomeworkDeliverMethod
									ELSE ".Get_Lang_Selection('hm.Method', 'hm.MethodEng')."
								END
							) as Method,";
}else{
    $methodField = "'' as Method,";
}

$sql  = "SELECT
          	a.UserID,
          	".getNameFieldByLang("a.")." as name,
          	a.ClassNumber,
          	".$expect_field." as currentStatus,
            a.UserLogin,
            b.LeaveStatus,
            r.HomeworkDeliverMethod,
            $methodField
            r.ApprovalStatus,
            r.IsDeleted,
            r.StartDate,
            r.EndDate,
            j.OfficeRemark	
        FROM 
          INTRANET_USER AS a 
		  INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID 
		  INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$lc->CurAcademicYearID."' 
          LEFT OUTER JOIN $card_log_table_name AS b
          ON (
             a.UserID=b.UserID AND
             (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
             )
          LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$TargetDate."' AND c.DayType = '".$DayType."')
          LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID = d.StudentID AND d.RecordDate='$txt_year-$txt_month-$txt_day' AND d.DayType='$DayType') 
		  $join_am_daily_remark_table 
          LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod='".$DayType."' AND f.RecordDate='$txt_year-$txt_month-$txt_day')
          LEFT OUTER JOIN CARD_STUDENT_APPLY_LEAVE_RECORD AS r ON (a.UserID = r.StudentID  AND '{$TargetDate}'<=r.EndDate  AND '{$TargetDate}'>=r.StartDate AND r.ApprovalStatus='1')
		  LEFT OUTER JOIN APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING as hm ON (r.HomeworkDeliverMethodID = hm.MethodID)
		  LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
				ON j.StudentID = a.UserID 
					AND j.RecordDate = '".$TargetDate."' 
					AND j.DayType = '".$DayType."' 
        WHERE
          /* (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND */
          a.RecordType=2 AND
          a.RecordStatus IN (0,1,2) AND
          yc.YearClassID= '".$year_class_id."'
        ORDER BY a.ClassNumber ASC
        ";
    $result = $libDB->returnResultSet($sql);
//    debug_pr($result);die();

    $header = ' <a  href="#" data-icon="myapp-myBack" data-position-to="window" data-role="button" data-rel="back" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 10px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;""></a>';
	$header .= "<h1 style ='text-align: center;width=100%;font-size:20px;color:white;text-shadow: none!important;'>".$class_name."</h1>";
	$x = "<div id='my-wrapper'><ul data-role='listview' id='defalt_student_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
	for($i = 0;$i<count($result);$i++){
		#early leave icon
		$earlyleaveicon="";
		if($result[$i]['LeaveStatus'] == 1 || $result[$i]['LeaveStatus'] == 2){
		//	$earlyleaveicon = "<img src=\"$image_path/{$LAYOUT_SKIN}/attendance/icon_early_leave_bw.gif\" width=\"12\" height=\"12\" />";
			$earlyleaveicon= $Lang['eClassApp']['StudentStatus']['EARLYLEAVE'] ;
		}else $result[$i]['LeaveStatus'] = 0;
		
		if($result[$i]['currentStatus'] =='0'){
		    $currentStatus = $Lang['eClassApp']['StudentStatus']['ONTIME'];
		    $statusColorCode ="#7EC400";
		}else if($result[$i]['currentStatus'] =='2'){
		    $currentStatus = $Lang['eClassApp']['StudentStatus']['LATE'];
		    $statusColorCode ="#2792C1";
		}else if($result[$i]['currentStatus'] =='3'){
		    $currentStatus = $Lang['eClassApp']['StudentStatus']['OUT'];
		    $statusColorCode ="#888888";
		}else{
		    $currentStatus = $Lang['eClassApp']['StudentStatus']['UNAVAILABLE'];
		    $statusColorCode ="#D3292B";
		}
		$_personalphotoPathAry = $libuser->GET_USER_PHOTO($result[$i]['UserLogin']);
        $_personalphotoPath = $_personalphotoPathAry[1];
		$x .= "<li data-theme=''>".
                       "<a href='#'  id = '".$result[$i]['UserID']."' name = ".$result[$i]['currentStatus']."  style ='background-color: transparent !important;padding-top:20px;padding-bottom:0px;padding-left:60px;' onclick = 'getStudentDetail(".$result[$i]['UserID'].",".$result[$i]['currentStatus'].",".$result[$i]['LeaveStatus'].");'>".
                       "<table style ='width:100%;><tr>".
                       "<td style ='width:20%;'><img src=".$_personalphotoPath." style = 'height: 50px;!important;background-size: 50px 50px!important;padding-left:20px;padding-top:15px;'></td>" .
                       "<td style='width:20px!important;vertical-align:top;text-align:center;padding-top:0px'>".$result[$i]['ClassNumber']."</td>".                       
					   "<td style ='padding-top:0px;'>".$result[$i]['name']."<br>";
					   if($plugin['attendancestudent']){
					   	  $x .="<h1 style='text-shadow: none!important;color:".$statusColorCode."'>$currentStatus </h1><h1 style='text-shadow: none!important;color:#808080' >$earlyleaveicon</h1>";
					   }
					   "</td></tr>";
		if($result[$i]['ApprovalStatus']==1&&$result[$i]['IsDeleted']==0&&$result[$i]['currentStatus']==1&&$result[$i]['StartDate']!=""){
            $x .= "<tr><td></td><td style ='padding-top:0px;color:gray'>".$result[$i]['StartDate']." ~ ".$result[$i]['EndDate']."</td></tr>";
            $x .= "<tr><td></td><td style ='padding-top:0px;color:gray'>".$result[$i]['Method']."</td></tr>";
        }
        if($result[$i]['currentStatus']==1){
            $x .= "<tr><td></td><td style ='padding-top:0px;color:gray'>" . $result[$i]['OfficeRemark'] . "</td></tr>";
        }
//		if($result[$i]['StartDate']!=""){
//            $x .= "</td><td style ='padding-top:0px;color:gray'>".$result[$i]['StartDate']." ~ ".$result[$i]['EndDate']."<br>";
//            $x .= "<h1 style='text-shadow: none!important;color:gray;'>".$result[$i]['Method']."</h1><h1 style='text-shadow: none!important;color:#808080' ></h1>";
//        }
        $x .=          "</table></a></li>"."\r\n";
	}
$x .= "</ul></div>";
//die();
echo $libeClassApp->getAppWebPageInitStart();
?>
		<style type="text/css">
		.ui-icon-myapp-myBack{			
			 border: 0 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$myBackIconUrl?>) no-repeat !important;
		}
		table td { 
		  text-align:left; 
		  max-width: 100px;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        } 
		</style>
		<script>
		function getStudentDetail(userid,userstatus,leavestatus){

			window.location = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentStatus/student_status_details.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&user_id="+userid+"&leave_status="+leavestatus+"&user_status="+userstatus+"&year_class_id=<?=$year_class_id?>";
		}
//		function Back(){
//			    window.location = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentStatus/student_status_classlist.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>";
//		}	
		 </script>
</head>
	<body>	
		<div data-role="page" id="body">
		<div data-role="header"  data-position="fixed" style ='background-color:#AEAEAE;'>	
		 <?=$header?>
		 </div>				
   	        <div data-role="content">
   	         <?=$x?>

<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>