<?php 
/**
 * editing by: ivan
 * 
 * Change log:
 * 
 */
$PATH_WRT_ROOT = '../../../../';

//include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
//$leClassApp_init = new libeClassApp_init();
//
//$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
//include_once($PATH_WRT_ROOT.'includes/libteaching.php');
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$uid = IntegerSafe($_GET['uid']);
$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
    $libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$libDB = new libdb();       
$form_class_manage = new form_class_manage();
$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();  
//$libteaching = new libteaching();
$AcademicYearID = Get_Current_Academic_Year_ID();
$sql = "SELECT
					yc.YearClassID as YearClassID,
					yc.ClassTitleEN,
                    yc.ClassTitleB5,
					y.YearID as YearID,
					y.YearName as YearName
		FROM
					YEAR_CLASS_TEACHER as yct
					INNER JOIN
					YEAR_CLASS as yc
					ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcademicYearID' AND yct.UserID = '$uid')
					INNER JOIN
					YEAR as y
					ON (yc.YearID = y.YearID) order by  y.sequence, y.YearName, yc.sequence, yc.ClassTitleEN";
$result = $libDB->returnArray($sql,2);

$canViewAllStudentList = false;
$canViewOwnStudentContactList = false;
$canSendPushMessageYearClassIdAry = array();
if(count($result)<=0){
	#NonClassTeacher	
    $nonClassTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList']);
	if($nonClassTeacherClassStudentListRight){
		$canViewAllStudentList = true;
	}
	
	$canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent']);


    if(!$canSendMessageToAllClass) {
        $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp']);
    }

}else{
	##ClassTeacher
	$classTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewAllStudentList']);
	if($classTeacherClassStudentListRight){
		$canViewAllStudentList = true;
	}

	$classTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass']);
	if($classTeacherClassStudentListRight){
		$canViewAllStudentList = false;
		$canViewOwnStudentContactList = true;
	}

	$classTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']);
	if($classTeacherClassStudentListRight){
		$canViewAllStudentList = true;
	}


	$canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent']);
	if (!$canSendMessageToAllClass) {
		$canSendMessageToOwnClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent']);


        $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp']);
        if(!$canSendMessageToOwnClass) {
            $canSendMessageToOwnClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp']);
        }


		if ($canSendMessageToOwnClass) {
			$canSendPushMessageYearClassIdAry = Get_Array_By_Key($result, 'YearClassID');
		}
	}
}

if($canViewAllStudentList){
		$academicYearClassAry = $form_class_manage ->Get_Class_List_By_Academic_Year($AcademicYearID);
}else{
    if($canViewOwnStudentContactList == false) {
		$result = array();
	}
	    $academicYearClassAry = $result;
}
	    
$classID = 'YearClassID';
$className = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
$classLevelID = 'YearID';
$levelName = 'YearName';
		
if(count($academicYearClassAry)>0){
	$sortedArray = array();
	foreach ($academicYearClassAry as &$value) {
		$tempClass['ClassID'] = $value[$classID];
		$tempClass['ClassName'] = $value[$className]==null?$value[Get_Lang_Selection('ClassTitleEN', 'ClassTitleB5')]:$value[$className];
		$sortedArray[$value[$classLevelID]]['ClassAry'][] = $tempClass;
		$sortedArray[$value[$classLevelID]]['YearName'] = $value[$levelName];
	}

	$x = "<div id='my-wrapper'><ul data-role='listview' id='defalt_class_list' data-inset='false' data-split-icon='plus' style = 'margin-bottom: 0px;'>";
	foreach ($sortedArray as $grade => $value) {
		$x .= "<li data-theme='' data-role='list-divider' style ='height:15px;'><span style=''>".$value['YearName'].":</span></li>";
	    $classAry = $value['ClassAry'];
		for($cCount = 0;$cCount<count($classAry);$cCount++){
			$__yearClassId = $classAry[$cCount]['ClassID'];
			
			$__sendPushMessageIcon = '';
			if ($canSendMessageToAllClass || in_array($__yearClassId, (array)$canSendPushMessageYearClassIdAry)) {
				$__sendPushMessageIcon = " <a href='#' id = '".$classAry[$cCount]['ClassID']."' name ='".$classAry[$cCount]['ClassName']."' data-role='button' class='split-custom-button' data-icon='comment' data-rel='dialog' data-theme='c' data-iconpos='notext' onClick='newPushMessageToClass(this);'></a> ";
			}
			
			$x .= "<li data-icon='false' style ='background-color: white !important;height: 60px;'>
					<a href='#'  id = '".$classAry[$cCount]['ClassID']."' name ='".$classAry[$cCount]['ClassName']."' style ='background-color: transparent !important;padding-left:45px;padding-top:13px;' onclick='getStudentStatusList(this);'>
						<h1>".$classAry[$cCount]['ClassName']."</h1>
					</a>
					<div class='split-custom-wrapper'>
	            		<a href='#' id = '".$classAry[$cCount]['ClassID']."' name ='".$classAry[$cCount]['ClassName']."' data-role='button' class='split-custom-button' data-icon='carat-r' data-rel='dialog' data-theme='c' data-iconpos='notext' onclick='getStudentStatusList(this);'></a>
	            		".$__sendPushMessageIcon."           
	        		</div>
				</li>"."\r\n";
	     }
	}
	$x .= "</ul></div>";	
	    	
}else{
	   $x = $Lang['eClassApp']['StudentStatus']['NOACCESSRIGHT'];		
}

echo $libeClassApp->getAppWebPageInitStart();
?>
		<style type="text/css">
		</style>
		<script>
		function getStudentStatusList(chooseClass){
			    var yearClassId = chooseClass.id;
			    var className = chooseClass.name;
				window.location = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentStatus/student_status_studentlist.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&class_name="+className+"&year_class_id="+yearClassId;
		}
		
		function newPushMessageToClass(chooseClass){
			    var yearClassId = chooseClass.id;
			    var className = chooseClass.name;
				window.location = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/studentStatus/send_push_message.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&class_name="+className+"&year_class_id="+yearClassId;
		}
		
		
		</script>
		<style>
		.split-custom-wrapper {
    /* position wrapper on the right of the listitem */
    position: absolute;
    right: 0;
    top: 20%;
    height: 60%;
}

.split-custom-button {
    position: relative;
    float: right;   /* allow multiple links stacked on the right */
    height: 100%;
    margin:0 auto;
    min-width:3em;
    /* remove boxshadow and border */
    border:none;
    moz-border-radius: 0;
    webkit-border-radius: 0;
    border-radius: 0;
    moz-box-shadow: none;
    webkit-box-shadow: none;
    box-shadow: none;
    background-color: white !important;
}

.split-custom-button span.ui-btn-inner {
    /* position icons in center of listitem*/
    position: relative;
    margin-top:50%;
    margin-left:50%;
    /* compensation for icon dimensions */
    top:11px; 
    left:-12px;
    height:40%; /* stay within boundaries of list item */
}
</style>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content">
   	         <?=$x?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>