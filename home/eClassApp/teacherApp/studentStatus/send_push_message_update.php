<?php
//using by: 

##################################
# change log:
# Date:     2020-10-21  Ray
#           - Add attachedImage & send time
# Date:     2020-04-16  Ray
#			- added send to students
# Date:		2019-10-29	Philips [2019-1016-1721-36235]
#			- added $includeNoPushMessageUser
#
##################################

$PATH_WRT_ROOT = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$token = $_POST['token'];
$uid = IntegerSafe($_POST['uid']);
$ul = $_POST['ul'];
$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}
$_SESSION['UserID'] = $uid;


$luser = new libuser();
//$studentIds = split(",",standardizeFormPostValue($_POST['studentIds']));
$studentIds = $_POST['studentIds'];
$includeNoPushMessageUser = $_POST['includeNoPushMessageUser'];

$isPublic = "N";
//debug_pr($luser->getParentStudentMappingInfo($studentIds));
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentIds), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

//debug_pr($luser->getParentStudentMappingInfo($studentIds));
//debug_pr($parentStudentAssoAry);
$messageTitle = standardizeFormPostValue($_POST['MessageTitle']);
$messageContent = standardizeFormPostValue($_POST['MessageContent']);

$send_to_parents = true;
$send_to_students = false;

$send_to_parents = ($_POST['sendToParents'] == '1') ? true : false;
$send_to_students = ($_POST['sendToStudents'] == '1') ? true : false;


$sendTimeMode = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['sendTimeMode']));
$sendTimeString = standardizeFormPostValue(cleanHtmlJavascript($_POST['sendTimeString']));

### Check image file format
include_once ($PATH_WRT_ROOT."includes/libfilesystem.php");
$lfs = new libfilesystem();
$filename = $_FILES["attachedImage"]["name"];
$tmpfilepath = $_FILES["attachedImage"]["tmp_name"];
$fileext = $lfs->file_ext($filename);
// [2020-0212-1115-29235]
// if($_FILES["attachedImage"]["name"] && !(strtolower($fileext) == ".jpg" || strtolower($fileext) == ".gif" || strtolower($fileext) == ".png")){
if($_FILES["attachedImage"]["name"] && !(strtolower($fileext) == ".jpg" || strtolower($fileext) == ".jpeg" || strtolower($fileext) == ".gif" || strtolower($fileext) == ".png")){
	echo "<script>
    parent.postResult(0);
</script>";
	die();
}

if($send_to_parents) {
	$appType = $eclassAppConfig['appType']['Parent'];
	//$sendTimeMode = "";
	//$sendTimeString = "";
	$individualMessageInfoAry = array();
	$i = 0;
	foreach ($studentIds as $studentId) {
		$thisStudent = new libuser($studentId);
		$appParentIdAry = $luser->getParentUsingParentApp($studentId, $includeNoPushMessageUser);

//	debug_pr($appParentIdAry);
		$_individualMessageInfoAry = array();
		foreach ($appParentIdAry as $parentId) {
			$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
		}
		$_individualMessageInfoAry['messageTitle'] = $messageTitle;
		$tempContent = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
		$_individualMessageInfoAry['messageContent'] = $tempContent;
		$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
//debug_pr($_individualMessageInfoAry);
		$i++;
	}

//debug_pr($recipientUserIdAry);
//debug_pr($parentStudentAssoAry);
//debug_pr($individualMessageInfoAry);
### send message
	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString, '', '', '', '', false, $_FILES['attachedImage']);

}


if($send_to_students) {
	$appType = $eclassAppConfig['appType']['Student'];
	//$sendTimeMode = "";
	//$sendTimeString = "";
	$individualMessageInfoAry = array();
	$i = 0;
	foreach ($studentIds as $studentId) {
		$thisStudent = new libuser($studentId);

//	debug_pr($appParentIdAry);
		$_individualMessageInfoAry = array();
		$_individualMessageInfoAry['relatedUserIdAssoAry'][$studentId] = (array)$studentId;
		$_individualMessageInfoAry['messageTitle'] = $messageTitle;
		$tempContent = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
		$_individualMessageInfoAry['messageContent'] = $tempContent;
		$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
//debug_pr($_individualMessageInfoAry);
		$i++;
	}

//debug_pr($recipientUserIdAry);
//debug_pr($parentStudentAssoAry);
//debug_pr($individualMessageInfoAry);
### send message
	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString, '', '', '', '', false, $_FILES['attachedImage']);


}

//$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];


//$return_page = $this_page."?DayType=$DayType&TargetDate=$TargetDate&order_by_time=$order_by_time&sent=1&Msg=".urlencode($msg);

echo "<script>
    parent.postResult(".$notifyMessageId.");
</script>";
//echo $notifyMessageId; // 1 or 0
//die;

intranet_closedb();

//header("Location: $return_page");
?>