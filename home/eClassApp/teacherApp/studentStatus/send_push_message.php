<?php 
/**
 * editing by:  Ray
 * Change log:
 * 2020-10-20 Ray
 * Add attachedImage & send time
 *
 * 2020-04-16 Ray
 * added send to students
 *
 * 2019-10-29 Philips
 * include No Push Message User
 * 
 * 2017-10-27 Anna
 * added select all option 
 * 
 */
$PATH_WRT_ROOT = '../../../../';

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/libteaching.php');
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$libclass = new libclass();
$libuser = new libuser();

$uid = IntegerSafe($_GET['uid']);
$yearClassID = IntegerSafe($_GET['year_class_id']);

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
$can_send_to_parents = false;
$can_send_to_students = false;


$AcademicYearID = Get_Current_Academic_Year_ID();
$sql = "SELECT
                yc.YearClassID as YearClassID,
                yc.ClassTitleEN,
                yc.ClassTitleB5,
                y.YearID as YearID,
                y.YearName as YearName
    FROM
                YEAR_CLASS_TEACHER as yct
                INNER JOIN
                YEAR_CLASS as yc
                ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcademicYearID' AND yct.UserID = '$uid')
                INNER JOIN
                YEAR as y
                ON (yc.YearID = y.YearID) order by  y.sequence, y.YearName, yc.sequence, yc.ClassTitleEN";
$result = $libclass->returnArray($sql,2);
if(count($result)<=0) {
    $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp']);
    if($canSendMessageToAllClass) {
        $can_send_to_students = true;
    }

    $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent']);
    if($canSendMessageToAllClass) {
        $can_send_to_parents = true;
    }

} else {
    $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp']);
    if(!$canSendMessageToAllClass) {
        $canSendMessageToOwnClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp']);
        if($canSendMessageToOwnClass) {
            $canSendPushMessageYearClassIdAry = Get_Array_By_Key($result, 'YearClassID');
            if(in_array($yearClassID, (array)$canSendPushMessageYearClassIdAry)) {
                $can_send_to_students = true;
            }
        }
    } else {
        $can_send_to_students = true;
    }

    $canSendMessageToAllClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent']);
    if(!$canSendMessageToAllClass) {
        $canSendMessageToOwnClass = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent']);
        if($canSendMessageToOwnClass) {
            $canSendPushMessageYearClassIdAry = Get_Array_By_Key($result, 'YearClassID');
            if(in_array($yearClassID, (array)$canSendPushMessageYearClassIdAry)) {
                $can_send_to_parents = true;
            }
        }
    } else {
        $can_send_to_parents = true;
    }

}

if($can_send_to_parents == false && $can_send_to_students == false) {
    $libeClassApp->handleInvalidToken();
}



$className = $_REQUEST['class_name'];
$token = $_REQUEST['token'];

$targetStudents = $libclass->getStudentByClassId(array($yearClassID));
### includes User without device available
$includeNoPushMessageUser = true;
$studentWithParentUsingAppAry = $libuser->getStudentWithParentUsingParentApp($includeNoPushMessageUser);

$canSendArray = array();
$canSendStudentIds = array();
$j=0;
for ($i = 0; $i < sizeOf($targetStudents); $i++) {
	$studentId = $targetStudents[$i]['UserID'];
	$lu = new libuser($studentId);
	if (in_array($studentId, $studentWithParentUsingAppAry)) {
// 		$canSendArray[$i]['name'] = $lu->UserName();
// 		$canSendArray[$i]['classnumber'] = $targetStudents[$i]['ClassNumber'];
// 		$canSendArray[$i]['userid'] = $studentId;
		
		$canSendArray[$j]['name'] = $lu->UserName();
		$canSendArray[$j]['classnumber'] = $targetStudents[$i]['ClassNumber'];
		$canSendArray[$j]['userid'] = $studentId;
		$j++;
//		$canSendStudentIds[] = $studentId;
	}
}

$myBackIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/iMail/app_view/white/icon_outbox_white.png"."\"";
$pageTitle = "New Push Message";

$linterface = new interface_html();
$refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);

$header = ' <a  href="#" data-icon="myapp-myBack" data-position-to="window" data-role="button" data-rel="back" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 10px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"></a>';
$header .= "<h1 style ='text-align: center;width=100%;font-size:20px;color:white;text-shadow: none!important;'>".$Lang['eClassApp']['ComposeMessage']."</h1>";

$recipientList = "<div data-role='collapsible' class='custom_collapsible'>
  					<h1>".$className."</h1>
				    <fieldset data-role='controlgroup' class='custom_fieldset' id='custom_fieldset'>";


for ($j=0;$j<sizeof($canSendArray);$j++) {
	if($j==0){
		$recipientList .='<label for="checkAll">'.$Lang['Btn']['SelectAll'].'</label>
						   <input class="recipient_checkbox" type="checkbox" id="checkAll" name="checkAll" onClick="(this.checked)?clickAll(1):clickAll(0)">';

	}
	
	$recipientList .= "	<label for='".$canSendArray[$j]['userid']."'>".$canSendArray[$j]['name']." (".$className." - ".$canSendArray[$j]['classnumber'].")</label>
				    	<input class='recipient_checkbox' type='checkbox' name='studentIds[]' id='".$canSendArray[$j]['userid']."' value='".$canSendArray[$j]['userid']."' >";
}
$recipientList .= "	</fieldset>
				</div>";


$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $isChecked=true, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $isChecked=0, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");

$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
$nextTimeslotHour = date('H', $rounded_seconds);
$nextTimeslotMinute = date('i', $rounded_seconds);

$x = '';
$x .= '<input type="text" data-role="date" id="sendTime_date" name="sendTime_date" value="'.date('Y-m-d').'" data-date-format="yy-mm-dd">';
$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $nextTimeslotHour);
$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $nextTimeslotMinute, $others_tab='', $interval=15);
$htmlAry['sendTimeDateTimeDisplay'] = $x;

echo $libeClassApp->getAppWebPageInitStart();
?>
    <script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
    <script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
    <link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
    <link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
    <script src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>

 
	<style type="text/css">
	.ui-icon-myapp-myBack{			
		 border: 0 !important;
		 -webkit-box-shadow:none !important;
		 background: url(<?=$myBackIconUrl?>) no-repeat !important;
	}
	#loadingmsg {
		color: black;
		background:tranparent!important;
		position: fixed;
		top: 50%;
		left: 50%;
		z-index: 100;
		margin: -8px 0px 0px -8px;
 	}
  	#loadingover {
  		background: white;
  		z-index: 99;
  		width: 100%;
	  	height: 100%;
	  	position: fixed;
	  	top: 0;
	 	left: 0;
	 	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
	  	filter: alpha(opacity=80);
	  	-moz-opacity: 0.8;
	  	-khtml-opacity: 0.8;
	  	opacity: 0.8;
	}
	
	.ui-collapsible-content {
		padding: 0 !important;
	}
	
	.custom_fieldset {
		margin: 0 auto !important;
	}
	
	</style>
	
	<script type="text/javascript">
    $.mobile.ajaxEnabled = false;
	function checkForm() {
		var formValid = false;
		var checkedAtLeastOne = false;
		var titleFilled = false;
		var contentFilled = false;

        $('input.recipient_checkbox').each(function() {
    		if ($(this).is(":checked")) {
        		checkedAtLeastOne = true;
    		}
		});

		if ($("#MessageTitle").val().length > 0) {
			titleFilled = true;
		}
		if ($("#MessageContent").val().length > 0) {
			contentFilled = true;
		}


        if($("#sendToParents:checked").val() != '1' && $("#sendToStudents:checked").val() != '1') {
            checkedAtLeastOne = false;
        }


		if (!checkedAtLeastOne) {
			alert("<?=$Lang['eClassApp']['Warning']['NoRecipient']?>");
		} else if (!titleFilled) {
			alert("<?=$Lang['eClassApp']['Warning']['EmptyTitle']?>");
		} else if (!contentFilled) {
			alert("<?=$Lang['eClassApp']['Warning']['EmptyContent']?>");
		} else {

            var scheduleString = '';
            if ($('input#sendTimeRadio_scheduled').prop('checked')) {
                scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
            }
            $('input#sendTimeString').val(scheduleString);

            var minuteValid = false;
            if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
                minuteValid = true;
            }

            // if scheduled message => check time is past or not
            var dateNow = new Date();
            var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
            if ($('input#sendTimeRadio_scheduled').prop('checked') && minuteValid && scheduleString <= dateNowString) {
                alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
                return false;
            }

			<?php if ($sys_custom['eClassApp']['sendPushmessageWithin90Days']) {?>

            // check time is within 90 days
            var maxDate = new Date();
            maxDate.setDate(dateNow.getDate() + 91);
            var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
            if ($('input#sendTimeRadio_scheduled').prop('checked') && minuteValid && scheduleString > maxDateString) {
                alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange90']?>');
                $('input#sendTime_date').focus();
                return false;
            }
			<?php }else{ ?>
            // check time is within 30 days
            var maxDate = new Date();
            maxDate.setDate(dateNow.getDate() + 31);
            var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
            if ($('input#sendTimeRadio_scheduled').prop('checked') && minuteValid && scheduleString > maxDateString) {
                alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
                $('input#sendTime_date').focus();
                return false;
            }
			<?php } ?>

            formValid = true;
		}

		return formValid;
	}

	function clickAll(val){
//		$(".clickAll").click(function() {
			  // if(document.getElementsByName("checkAll")[0].checked) {

			   if(val == 1){
			    $("input[name='studentIds[]']").each(function() {
//				$("input[name='studentIds[]']").prop("checked",true);

			        $(this).prop("checked", true).checkboxradio('refresh');
			    });
			   } else {
			     $("input[name='studentIds[]']").each(function() {
			         $(this).prop("checked", false).checkboxradio('refresh');
			     });
			//	   $("input[name='studentIds[]']").prop("checked", false);
			   }
//			});

	}

	$(document).ready(function(){
        $("#attachmentFile").click(function(){
            var ua = window.navigator.userAgent;
            if( ua.indexOf("Android") >= 0 )
            {
                var androidversion = ua.slice(ua.indexOf("Android")+8,ua.indexOf("Android")+13);
                if((androidversion=='4.4.1')||(androidversion=='4.4.2')||(androidversion=='4.4.3')){
                    safeAlert('<?=$Lang['Gamma']['App']['DeviceNoSupportAttachment']?>');
                }
            }
        });
        var form_submited = false;
		$("#send_button").click(function() {
            if(form_submited) {
                return;
            }

			// get string before disabling the buttons. Otherwise, cannot get the input values
			var serializedFormString = $("form#form1").serialize();

			//$("#send_button").attr('disabled', true);
			//$("#MessageTitle").attr('disabled', true);
			//$("#MessageContent").attr('disabled', true);

			var formValid = checkForm();
			if (formValid) {
				var interval = setInterval(function(){
	        		$.mobile.loading('show');
	        		clearInterval(interval);
	    		},1);

                form_submited = true;
                $("#form1").submit();

                $("#send_button").attr('disabled', true);
                $("#MessageTitle").attr('disabled', true);
                $("#MessageContent").attr('disabled', true);

                /*
                $.post(
	               'send_push_message_update.php',
	                serializedFormString,
	                function(data){

	                	var interval = setInterval(function(){
					        $.mobile.loading('hide');
					        clearInterval(interval);
				    	},1);

	                	if (data > 0) {
				    		alert('<?=$Lang['eClassApp']['SendSuccess']?>');
				    		window.history.back();
				    	} else {
				    		alert('<?=$Lang['eClassApp']['SendFail']?>');
				    	}
	                }
				);
				*/
			} else {
				//$("#send_button").attr('disabled', false);
				//$("#MessageTitle").attr('disabled', false);
				//$("#MessageContent").attr('disabled', false);
			}


		});
	});

    function postResult(data) {
        var interval = setInterval(function(){
            $.mobile.loading('hide');
            clearInterval(interval);
        },1);

        if (data > 0) {
            alert('<?=$Lang['eClassApp']['SendSuccess']?>');
            window.history.back();
        } else {
            form_submited = false;
            alert('<?=$Lang['eClassApp']['SendFail']?>');
            $("#send_button").attr('disabled', false);
            $("#MessageTitle").attr('disabled', false);
            $("#MessageContent").attr('disabled', false);
        }
    }

    function clickedSendTimeRadio(targetType) {
        if (targetType == 'now') {
            $('div#specificSendTimeDiv').hide();
        }
        else {
            $('div#specificSendTimeDiv').show();
        }
    }
    function onSelectedDateOfJqueryMobileDatePicker() {
        var val = $('#sendTime_date').val();
        var d = new Date(val);
        var dateString = d.getFullYear() + '-' + str_pad((d.getMonth()+1),2) + '-' + str_pad(d.getDate(),2);
        $('#sendTime_date').val(dateString);
    }
	</script>
</head>
	<body>	
		<div data-role="page" id="body">
		 <div data-role="header"  data-position="fixed" style ='background-color:#429DEA;'>	
	     <div id="loadingmsg" style="display: none;"><br><?=$refreshIconUrl?><br></div>
	     <div id="loadingover" style="display: none;"></div>
		 <?=$header?>
		 </div>	
   	        <div data-role="content">
   	        	<form id="form1" method="post" action="send_push_message_update.php" enctype="multipart/form-data" target="post_iframe">
	   	        	<label><?=$Lang['eClassApp']['SendPushMessageToParentOfTheSelectedStudents']?>/<?=$i_identity_student?>:</label>
	   	        	<?=$recipientList?>
	   	        	
					<br />

                    <fieldset data-role="controlgroup" data-type="horizontal">
                        <?php if($can_send_to_parents) { ?>
                        <input type="checkbox" name="sendToParents" id="sendToParents" value="1" <?=(($can_send_to_students == false) ? 'checked' : '')?>>
                        <label for="sendToParents"><?=$i_identity_parent?></label>
                        <?php } ?>

                        <?php if($can_send_to_students) { ?>
                        <input type="checkbox" name="sendToStudents" id="sendToStudents" value="1" <?=(($can_send_to_parents == false) ? 'checked' : '')?>>
                        <label for="sendToStudents"><?=$i_identity_student?></label>
                        <?php } ?>
                    </fieldset>


	   	        	<label><?=$Lang['eClassApp']['Title']?>:</label>
	   	        	<input type="text" name="MessageTitle" id="MessageTitle" value="" class="ui-input-text ui-body-c" placeholder="<?=$Lang['eClassApp']['Title']?>" maxlength="128">
	   	        	<!--textarea class="custom-textarea" name="Title" id="Title" placeholder="<?=$Lang['eClassApp']['Title']?>"></textarea-->
	   	        	
	   	        	<label><?=$Lang['eClassApp']['Content']?>:</label>
					<span class="tabletextremark">(<?=$Lang['MessageCenter']['ContentMaximumRemarks']?>)</span>
	   	        	<textarea class="custom-textarea" name="MessageContent" id="MessageContent" placeholder="<?=$Lang['eClassApp']['Content']?>" maxlength="500"></textarea>
					<br />

					<?php if (!$sys_custom['MessageCenter']['HideSendingImage']) { ?>
                        <label><?=$Lang['AppNotifyMessage']['AttachImage']?>:</label>
                        <span class="tabletextremark">(<?=$Lang['MessageCenter']['ImageAttachementReminder']?>)</span>
                        <input type="file" id="attachedImageFile" name="attachedImage" accept="image/*"/>
                        <br />
					<?php } ?>

                    <label><?=$Lang['AppNotifyMessage']['SendTime']?>:</label>
                    <div>
						<?=$htmlAry['sendTimeNowRadio']?>
						<?=$htmlAry['sendTimeScheduledRadio']?>
                        <br />
                        <div id="specificSendTimeDiv" style="display:none;">
                            <fieldset data-role="controlgroup" data-type="horizontal">
								<?=$htmlAry['sendTimeDateTimeDisplay']?>
                            </fieldset>
                        </div>
                    </div>
                    <br/>

                    <input type="hidden" id="sendTimeString" name="sendTimeString" value="" />
					<input type="hidden" name="token" value="<?=$token?>"/>
					<input type="hidden" name="uid" value="<?=$uid?>"/>
					<input type="hidden" name="ul" value="<?=$ul?>"/>
					<input type="hidden" name="includeNoPushMessageUser" value="<?=$includeNoPushMessageUser?>" />
					<input type="button" id="send_button" value="<?=$Lang['eClassApp']['Send']?>">
				</form>
                <div style="display: none;">
                    <iframe name="post_iframe" width="0" height="0"></iframe>
                </div>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>