<?php 
### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!

/*
 * 2017-10-19 Ivan [ip.2.5.9.1.1]
 * Changed default personal photo path to call function by libuser.php
 */
$PATH_WRT_ROOT = '../../../../';

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/libdbtable.php');
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");

$uid = IntegerSafe($_GET['uid']);
$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	$libeClassApp->handleInvalidToken();
}

intranet_auth();
intranet_opendb();

$luser = new libuser();
$libstaffattend3 = new libstaffattend3(); 
$laccount = new libaccountmgmt();

 $_displayNameLang = Get_Lang_Selection('ChineseName', 'EnglishName');
 $_displayNameBackupLang = Get_Lang_Selection('EnglishName', 'ChineseName');
 
 $sql = "SELECT count(*) FROM CARD_STAFF_ATTENDANCE2_DAILY_LOG_".date("Y")."_".date("m");
 $result = $laccount->returnVector($sql);
 if($result[0]<=0){
 	//create a new current month's attendance table
     $libstaffattend3->Index_Page_Process();
 }
 
 $eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
 $intranetAPPTeacherListGroupSettingValue = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']);
 $sortArray[1] = $Lang['eClassApp']['Teacher'];$sortArray[2]= $Lang['eClassApp']['NonTeacher'];
	 if($intranetAPPTeacherListGroupSettingValue != 1){
	 	##Setting value 1 means Disable Group
	    $sortArray[3]= $Lang['eClassApp']['Group'];	
	 }
	 
  $TitleNameLang = Get_Lang_Selection('TitleChinese', 'Title');
  $selectedSortID = $selectedSortID !=null?$selectedSortID:1;
  if(($selectedSortID == 3)&&($selectedGroupID == null)){
			##Show Group List
		 	if($intranetAPPTeacherListGroupSettingValue == 2){
		 		##Enable All Groups
		 		$authorizedGroupAry = $libeClassApp->getAllIntranetAppTeacherListGroup();
		 	}else{
		 		##Enable specified Groups
		 		$authorizedGroupAry = $libeClassApp->getIntranetAppTeacherListEnableGroup();
		 	}	
	$show_main_table = '';
			for($i = 0;$i<count($authorizedGroupAry);$i++){
				$tempGroupTitle = $authorizedGroupAry[$i][$TitleNameLang] != null?$authorizedGroupAry[$i][$TitleNameLang]:$authorizedGroupAry[$i]['Title'];
				$show_main_table .= '<li class = "ui-li-static ui-body-inherit"><a href="#" id = "'.$authorizedGroupAry[$i]['ID'].'" onclick ="getGroupMembers(this)">'.$tempGroupTitle.'</a></li>';
			}//end loop groups
  	
  }else{
  	 if($selectedSortID == 2){
  	 	##Not Teacher List
  	 	$sql = "SELECT
			a.EnglishName,
			a.ChineseName,
			a.UserID,
			a.UserLogin,a.PersonalPhotoLink,a.MobileTelNo
		FROM 
		    INTRANET_USER as a  
		WHERE
			a.RecordType = ".TYPE_TEACHER." AND a.RecordStatus=1 AND ( a.Teaching = 1 IS NULL OR a.Teaching = 1 = '')  order by a.EnglishName";
  	  }else if($selectedSortID == 3){
  	  		 $sql = "Select Title,TitleChinese From INTRANET_GROUP where GroupID = '".$selectedGroupID."'";
		 	 $titleResult = $laccount->returnResultSet($sql);	
		 	 $groupName = $titleResult[0][$TitleNameLang] != null?$titleResult[0][$TitleNameLang]:$titleResult[0]['Title'];		 	 
		 	 ##get ALL USERS in the identified Group
		 	 $sql = "SELECT
							a.EnglishName,
							a.ChineseName,
							a.UserID,
							a.UserLogin as UserLogin,a.PersonalPhotoLink,a.MobileTelNo
						 FROM 
						    INTRANET_USER as a INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID = a.UserID Where ug.GroupID='".$selectedGroupID."' AND a.RecordType = '".USERTYPE_STAFF."' AND a.RecordStatus=1 GROUP BY a.UserID  ORDER BY a.EnglishName";
		 	 
  	  }else{
  	  ##Teacher List
  	  $sql = "SELECT
			a.EnglishName,
			a.ChineseName,
			a.UserID,
			a.UserLogin,a.PersonalPhotoLink,a.MobileTelNo
		FROM 
		    INTRANET_USER as a  
		WHERE
			a.RecordType = ".TYPE_TEACHER." AND a.RecordStatus=1 AND a.Teaching = 1  order by a.EnglishName";
  	  }
  $result = $laccount->returnResultSet($sql);
  $allTeacherUserIdAry = Get_Array_By_Key($result, 'UserID');
  
  
  // get teacher's attendance data
  if ($plugin['attendancestaff']) {
	  $sql = "SELECT
				wp.StaffID,
				GROUP_CONCAT(wp.InSchoolStatus) as InSchoolStatus,
				GROUP_CONCAT(wp.OutSchoolStatus) as OutSchoolStatus,
                GROUP_CONCAT(wp.OutTime) as OutTime,
				wp.SlotName
			FROM 
			    CARD_STAFF_ATTENDANCE2_DAILY_LOG_".date("Y")."_".date("m")." as wp
			WHERE
				wp.StaffID IN ('".implode("','", (array)$allTeacherUserIdAry)."') 
				AND wp.DayNumber='".date("d")."' 
			group by wp.StaffID";
	  $attendanceDataAry = $laccount->returnResultSet($sql);
	  $attendanceDataAssoAry = BuildMultiKeyAssoc($attendanceDataAry, 'StaffID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
  }
  
  ### Generate The HTML List 
  $phoneIconUrl = "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/teacherApp/teacherStatus/icon_phone.png"."\"";
  $emailIconUrl =  "\""."{$image_path}/{$LAYOUT_SKIN}"."/eClassApp/teacherApp/teacherStatus/icon_email.png"."\"";
  $emailReplyLink = $PATH_WRT_ROOT."home/imail_gamma/app_view/email_reply_forward_page.php?MailAction=Compose&TargetFolderName=TeacherStatus&token=$token&uid=$uid&ul=$ul&parLang=$parLang&recipientID=";
  $PersonalPhotoLinkPath = "";
  $linterface = new interface_html();
  $refreshIconUrl =$linterface->Get_Ajax_Loading_Image($noLang=true);
  $show_main_table = '<header data-role ="header" data-position="fixed" style ="background-color:#f6f6f6;border-bottom-width: 0px; z-index:999;">
								  <div id="loadingmsg" style="display: none;"><br>'.$refreshIconUrl.'<br></div>
								  <div id="loadingover" style="display: none;"></div>
								  </header>';
  for($i=0;$i<count($result);$i++){
  	$tempTeacher = $result[$i];
  	$_teacherUserId = $tempTeacher['UserID'];
//  	$_personalphotoPathAry = $luser->GET_USER_PHOTO($tempTeacher['UserLogin']);
//  	$_personalphotoPath = $_personalphotoPathAry[1];  	
	########## Personal Photo
	//$_personalphotoPath = "/images/myaccount_personalinfo/samplephoto.gif";
	$_personalphotoPath = $luser->returnDefaultPersonalPhotoPath();
	
	if ($tempTeacher['PersonalPhotoLink'] !="")
	{
	    if (is_file($intranet_root.$tempTeacher['PersonalPhotoLink']))
	    {
	        $_personalphotoPath = $tempTeacher['PersonalPhotoLink'];
	   }
	}
  	$_teacherName = $tempTeacher[$_displayNameLang]==null?$tempTeacher[$_displayNameBackupLang]:$tempTeacher[$_displayNameLang];  
  	$_mobileTelNo = $tempTeacher['MobileTelNo'];
  	$_userid = $tempTeacher['UserID']; 
  	//$_attendanceStatus = $Lang['eClassApp']['TeacherStatus_Nonarrival'];
  	$_attendanceStatus = $Lang['General']['EmptySymbol'];
  	
  	$_teacherAttendanceDataAry = $attendanceDataAssoAry[$_teacherUserId];
  	if(count($_teacherAttendanceDataAry)<=0){
  		$_attendanceStatus ='';
  	}else{
	  	if($_teacherAttendanceDataAry['SlotName'] != null){
		  	$InSchoolStatus = $_teacherAttendanceDataAry['InSchoolStatus'];
		  	$OutSchoolStatus = $_teacherAttendanceDataAry['OutSchoolStatus'];
		  	$OutTime = $_teacherAttendanceDataAry['OutTime'];
	  		$OutSchoolStatusArr = explode(",",$OutSchoolStatus);
	  		$OutSchoolStatusArrCounter = count($OutSchoolStatusArr);
	  		$InSchoolStatusArr = explode(",",$InSchoolStatus);
			$InSchoolStatusArrCounter = count($InSchoolStatusArr);
	  		if(($OutSchoolStatus != null)&&($OutSchoolStatusArr[$OutSchoolStatusArrCounter-1] != null)&&($OutSchoolStatusArrCounter==$InSchoolStatusArrCounter)&&(($OutSchoolStatusArr[$OutSchoolStatusArrCounter-1] == 3)||($OutSchoolStatusArr[$OutSchoolStatusArrCounter-1] == 5))){
	  			 $_attendanceStatus = $Lang['eClassApp']['TeacherStatus_Leave'];
	  		}else if(($OutSchoolStatus != null)&&($OutSchoolStatusArr[$OutSchoolStatusArrCounter-1] != null)&&($OutSchoolStatusArrCounter==$InSchoolStatusArrCounter)&&($OutSchoolStatusArr[$OutSchoolStatusArrCounter-1] == 0)&&($OutTime != null)){
                 $_attendanceStatus = $Lang['eClassApp']['TeacherStatus_Leave'];
	  		}else{
		  	 	if($InSchoolStatus != null){
			  	 	$InSchoolStatus = $InSchoolStatusArr[$InSchoolStatusArrCounter-1];
			  	 	if(($InSchoolStatus == 0)||($InSchoolStatus == 2)){
			  	 		$_attendanceStatus = $Lang['eClassApp']['TeacherStatus_Arrival'];
			  	 	}
		  	 	}
		  	 }
	  	}	
  	}
	  if($_attendanceStatus!=''){
	   	$tempStatus = "<td style ='width:60%;'><b>$_teacherName</b><br>$_attendanceStatus</td>";
	   }else{
	   $tempStatus = "<td style ='width:60%;'>$_teacherName</td>";
	   }
  	
  	$dialIconDisplay = "";
  	if($_mobileTelNo == null){
  		$dialIconDisplay = "display:none;";
  	}
  	
    $show_main_table .= "<li data-icon='false'>".
                       "<table><tr>" .
                       "<td style ='width:20%;'><img src=".$_personalphotoPath." style = 'height: 50px!important;background-size: 50px 50px!important;top:5px;'></td>" .
                       $tempStatus.
                       "<td style ='width:10%;'><a href='#' data-icon='myapp-phone' id = '$_mobileTelNo' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;left:5px;".$dialIconDisplay."' onclick = 'Dial(this.id);'></a></td>" .
                       "<td style ='width:10%;'>";
  	if($special_feature['imail']) {
  	    $show_main_table .= "<a href='#' data-icon='myapp-imail' id = '$_userid' data-position-to='window' data-role='button' class = 'ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon' style='width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 5px;left:5px;border:none' onclick = 'SendEmail(this.id);'></a>";
  	}
  	$show_main_table .= "</td>" .
                       "</tr></table></li>"."\r\n";
  }//end loop user List
   }//end if select group
   
    ###sorter
   $sortSelection = $libeClassApp->getSelectionHtml($sortArray,'',$selectedSortID,'','filterTeachers(this);');
   $header = '<p  style="text-align:left; margin-left:5px;margin-top: 18px;" class = "ui-li-text-my-title-font">'."&nbsp&nbsp".$groupName.'</p>'.
								 '<fieldset  class="ui-btn-right" data-role="controlgroup" data-type="horizontal" style="right: 20px !important; top:10px !important;" >
								 <div class="ui-controlgroup-controls" style="font-size:0;!important; margin-top:50px;">
								 '.$sortSelection.'
								 </div>
								 </fieldset>';
  $show_main_table ="<div id='my-wrapper'><ul data-role='listview' id='TeacherStaffInfoList' data-inset='false' data-filter='true' data-filter-placeholder='".$Lang['eClassApp']['TeacherStatus_Search']."' style = 'margin-bottom: 0px;padding-left:0px;padding-top:0px;'>".$header.$show_main_table."</ul></div>";
  echo $libeClassApp->getAppWebPageInitStart();
?>
		<style type="text/css">
		     .ui-icon-myapp-phone {
				 background-color: transparent !important;
				 border: 0 !important;
				 -webkit-box-shadow:none !important;
				 background: url(<?=$phoneIconUrl?>) no-repeat !important;
			  }
			 .ui-icon-myapp-imail{
			 background-color: transparent !important;
			 border: 1 !important;
			 -webkit-box-shadow:none !important;
			 background: url(<?=$emailIconUrl?>) no-repeat !important;
			 }
			 ul {
			    width: 100%;    
			    margin-left: 0px;
			    padding: 0px; 
			  }
			  ul li {
			    list-style-type: none;
			    border-bottom: 1px dashed gray;
			    margin-top: 10px; 
			    background-color: white!important;
			    border-bottom-width: 2px solid #f00; 
			    width:100%;
			  }
			  table { width:100%; }
			  table td { 
			  text-align:left; 
			  max-width: 45px;
              overflow: hidden;
              text-overflow: ellipsis;
              white-space: nowrap;} 
              #loadingmsg {
			  color: black;
			  background:tranparent!important;
			  position: fixed;
			  top: 50%;
			  left: 50%;
			  z-index: 100;
			  margin: -8px 0px 0px -8px;
			  }
			  #loadingover {
			  background: white;
			  z-index: 99;
			  width: 100%;
			  height: 100%;
			  position: fixed;
			  top: 0;
			  left: 0;
			  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
			  filter: alpha(opacity=80);
			  -moz-opacity: 0.8;
			  -khtml-opacity: 0.8;
			  opacity: 0.8;
			  }
			  .ui-btn, label.ui-btn{
              	border-style:hidden !important;
              }
		</style>
		<script>
	 $( document ).ready(function() {
		    $('div#body').on('click', '.ui-selectmenu-list > li', function () {
		    	var clickItem = $('#select-items').children().eq($(this).attr('data-option-index')).val();
		    	var currentItem = <?=$selectedSortID?>;
		    	
		    	if(clickItem == currentItem){
		    		<!--click current option, refresh current one-->
    			    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/teacherStatus/teacher_status.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&selectedSortID="+clickItem;
				    setTimeout(function(){
				    	 window.location = url;
				    	    }, 200);
		    	}
			});
		});
		function Dial(mobileTelNo){
		  if(mobileTelNo !=''){
		  var url = "tel:"+mobileTelNo.replace(/ /g,"");;
		  window.location.assign(url);
		  }else{
		  alert("<?=$Lang['eClassApp']['TeacherStatus_UserPhoneNo']?>");
		  }
		}
		function SendEmail(recipientID){
			var url = "<?=$emailReplyLink?>"+recipientID;
				window.location.assign(url);
			}
			
		function filterTeachers(chooseClass){
		    var chosen_sort_id = chooseClass.value;		
		    var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/teacherStatus/teacher_status.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&selectedSortID="+chosen_sort_id;
		    setTimeout(function(){
		    	 window.location = url;
		    	    }, 200);
	    }
	    
	    function getGroupMembers(group){
	    	var groupId = group.id;
	    	var url = "<?=$PATH_WRT_ROOT?>home/eClassApp/teacherApp/teacherStatus/teacher_status.php?uid=<?=$uid?>&token=<?=$token?>&ul=<?=$ul?>&parLang=<?=$parLang?>&selectedSortID=<?=$selectedSortID?>&selectedGroupID="+groupId;
		    setTimeout(function(){
		    	 window.location = url;
		    	    }, 200);
	    }
		 </script>
</head>
	<body>	
		<div data-role="page" id="body">			
   	        <div data-role="content">
   	        <?=$show_main_table?>
<?php
echo $libeClassApp->getAppWebPageInitEnd();
intranet_closedb();
?>