<?php 

$PATH_WRT_ROOT = '../../../../';
$parLang="b5";

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$intranet_hardcode_lang = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['intranet_session_language'] = $intranet_hardcode_lang;
$libeClassApp = new libeClassApp();
//$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
//if(!$isTokenValid) {
//	echo $i_general_no_access_right	;
//	exit;
//}
intranet_opendb();
$libdb = new libdb();
$linterface = new interface_html();
$libTT = new Timetable();
$libSCM = new subject_class_mapping();
$lcycleperiods = new libcycleperiods();
$AcademicYearID = Get_Current_Academic_Year_ID();
$YearTermID = getCurrentSemesterID();

$SubjectGroupInfoArr1 = $libSCM->Get_User_Accessible_Subject_Group($UserID, $AcademicYearID, '');
$SubjectGroupIDArr = Get_Array_By_Key($SubjectGroupInfoArr1, 'SubjectGroupID');	

$this_SubjectGroupID_DisplayFilterArr = $libSCM->Get_SubjectGroupID_Taken($YearTermID, "Personal", '', $UserID, '', "Student", '', $SubjectGroupIDArr);
if (count($this_SubjectGroupID_DisplayFilterArr) <= 0)
	$this_SubjectGroupID_DisplayFilterArr = array(0);

$TimetableID = $libTT->Get_Current_Timetable();
if ($TimetableID == '') {
	echo 'There is no timetable settings from school yet and therefore cannot view the timetable.';
	die();
}

# Get Timetable Info
$objTimetable = new Timetable($TimetableID);

# Get Cycle Info
$numOfCycleDays = $objTimetable->CycleDays;

# Get Timeslot Info
$timeSlotArr = $objTimetable->Get_Timeslot_List();
$numOfTimeSlot = count($timeSlotArr);
$TimeSlotIDArr = Get_Array_By_Key($timeSlotArr, 'TimeSlotID');	

# Get Current Day
$CurrentCycleDay = $libTT->Get_Current_Timetable_Cycle_Day();	

# Get Room Allocation Info (add parameters for filtering later)
$roomAllocationArr = $objTimetable->Get_Room_Allocation_List('', '', $this_SubjectGroupID_DisplayFilterArr, array());
		
# Get Subject Group Info
$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_Info($AcademicYearID, $YearTermID, $TimetableID);

# Get Location Info
$liblocation = new liblocation();
$LocationInfoArr = $liblocation->Get_Building_Floor_Room_Name_Arr($TimetableID);

# Get Timezon
$TimezoneDayArr = $lcycleperiods->array_numeric;

#Draw the table
$table = '';

$table .= '<table width="100%" id="ContentTable" style="border-collapse:collapse;" >'."\n";
	$tempTimeSlotWidth = 10;
	if ($numOfCycleDays != 0)
		$colWidth = floor((100 - $tempTimeSlotWidth) / $numOfCycleDays)."%";	// 2 is the estimated percentage of the "add cycle day" column
	else
		$colWidth = "100%";
	$timeSlotWidth = 100 - ($colWidth * $numOfCycleDays);	// re-calculate the time slot column width
		
	$table .= '<col align="left" width="'.$timeSlotWidth.'%"></col>'."\n";

	for ($i=0; $i<$numOfCycleDays; $i++) {
		$thisDay = $i + 1;
		
		$table .= '<col align="left" width="'.$colWidth.'"></col>'."\n";
	}
	
## Header
$table .= '<thead>'."\n";
	# Cycle Title Display
	$table .= '<tr>'."\n";
		$table .= '<th>&nbsp;</th>'."\n";
		
		# Cycle Day Display
		for ($i=0; $i<$numOfCycleDays; $i++) {
			// Display 1,2,3...
			$thisDay = $i + 1;
			
			$thisActionTag = '';

			$thisDayDisplay = $TimezoneDayArr[$thisDay-1];
			$CurrentCycleDayStyle = '';	
			if ($thisDay == $CurrentCycleDay) {
				$CurrentCycleDayStyle = 'color: white; background-color: #FF752A;';
			}else{
				$CurrentCycleDayStyle = 'color: gray;';				
			}
															
			$table .= '<th style="height:25px;vertical-align:center;'.$CurrentCycleDayStyle.'">'."\n";
				# Day Display
				$table .= '<span style="float:center;" >'.$thisDayDisplay.'</span>'."\n";

			$table .= '</th>'."\n";
		}

	$table .= '</tr>'."\n";
$table .= '</thead>'."\n";

# Content - Timeslot settings and Room Allocation
		$table .= '<tbody class="tbody" style="font-size:9px;">'."\n";
			# Loop Time Slot
			for ($i=0; $i<$numOfTimeSlot; $i++) {
				$thisTimeSlotID = $timeSlotArr[$i]['TimeSlotID'];
				
				// For time vaildation in the thickbox
				$nextTimeSlotID = ($i == ($numOfTimeSlot-1) )? "''" : $timeSlotArr[$i+1]['TimeSlotID'];
				
				# Display Timeslot Info
				$thisTimeSlotName = $timeSlotArr[$i]['TimeSlotName'];
				// Hour and Minute only
				$thisStartTime = substr($timeSlotArr[$i]['StartTime'], 0, 5);
				$thisEndTime = substr($timeSlotArr[$i]['EndTime'], 0, 5);
				$thisTimeDisplay = $thisStartTime;
                $thisTimeArrCount = count($roomAllocationArr[$thisTimeSlotID]);
				if($thisTimeArrCount==0){
					$rowHeight = 'height:40px;';					
				}else{
					$rowHeight = 'height:80px;';					
				}
				//Draw left time part
				$table .= '<tr style="'.$rowHeight.'">'."\n";
					$table .= '<td style="padding-top: 5px;vertical-align:top;font-size:9px;color:gray;text-align: center;" nowrap>'."\n";
						$table .= '<div>'."\n";
							# Time slot display
							$table .= '<span>'.$thisTimeDisplay.'</span>'."\n";
						$table .= '</div>'."\n";
					$table .= '</td>'."\n";
					
				if($thisTimeArrCount==0){					
				    	$table .= '<td colspan="'.$numOfCycleDays.'" class="detailBorder" style="text-align: center;color: gray;">'.$thisTimeSlotName."\n";
						$table .= '</td>'."\n";

				}else{
					# Loop Days
					for ($j=0; $j<$numOfCycleDays; $j++) {
						# Display Room Allocation Info
						$thisDay = $j + 1;
						$thisDetailTableDivID = 'DetailTableDiv_'.$thisTimeSlotID.'_'.$thisDay;
						
                        $thisRoomAllocationArr = array();
						$thisRoomAllocationArr = $roomAllocationArr[$thisTimeSlotID][$thisDay][0];

						# Subject Info
			     		$thisSubjectNameEN = $SubjectGroupInfoArr[$thisRoomAllocationArr['SubjectGroupID']]['SubjectDescEN'];
					    $thisSubjectNameB5 = $SubjectGroupInfoArr[$thisRoomAllocationArr['SubjectGroupID']]['SubjectDescB5'];
					    $thisSubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
					
						# Location Info
						$thisLocationInfoArr = $LocationInfoArr[$thisRoomAllocationArr['LocationID']];
						$thisRoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
						
						$hasclassbackground = '';
						$hasclasswordcolor = '';
						if(count($thisRoomAllocationArr)!=0){
							$hasclassbackground = 'background-color:#00BAB5;';
							$hasclasswordcolor = 'color:white;';
						}	
						
						$table .= '<td style="'.$hasclassbackground.' " class="detailBorder">'."\n";
							$table .= '<div  id="'.$thisDetailTableDivID.'">'."\n";
							$table .= '<table height="80"  class="detail" border=0 style="'.$hasclasswordcolor.'" ><tr><td style="vertical-align:top; font-size:12px;"><strong>'.$thisSubjectName.'</strong></td></tr><tr><td style="vertical-align:bottom;">'.$thisRoomName.'</td></tr></table>';
						    $table .= '</div>'."\n";
						$table .= '</td>'."\n";
					}
				}


				$table .= '</tr>'."\n";
				
			}

		$table .= '</tbody>'."\n";


				
$table .= '</table>'."\n";

//echo $libeClassApp->getAppWebPageInitStart();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no" /> -->

<script>

</script>
<style type="text/css">


#body{
   background:#ffffff;
   text-shadow: 0px 0px 0px #000000 !important;
   word-break: break-all;
}

td {
    border: 1px solid #eeeeee;
    border-collapse: collapse;
} 

th {
    border: 0;
} 

.detail td{
	border:0
}

</style>
</head>
	<body>
	
		<div data-role="page" id="body">			
   	      				
		    <form id="form1" name="form1" method="get" >		   
                 
                      <?=$table?>                                                                        
                                                                                       
			</form>
				 

	    </div>
	   
	</body>
</html>

<?php

intranet_closedb();

?>