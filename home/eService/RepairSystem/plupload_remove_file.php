<?php
// Editing by 
/*
 * 	2017-05-09 (Cameron) copy from digital archive
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();

$TargetFolder = $_REQUEST['TargetFolder'];
$FileName = $_REQUEST['FileName'];
$FileName = urldecode($FileName);

if(empty($TargetFolder) || empty($FileName)) {
	echo "0";
	intranet_closedb();
	exit;
}


$targetPath_Level1 = "/repair/";
$targetPath_Level2 = "/temp/";

$plupload_tmp_path = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath_Level1.$targetPath_Level2.$TargetFolder.'/'.$FileName);

$success = $libfilesystem->lfs_remove($plupload_tmp_path);
echo $success? "1":"0";

intranet_closedb();
?>