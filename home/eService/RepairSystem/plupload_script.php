<?php
// Editing by 
/*
 * 2016-05-09 (Cameron): Copy from digital archive. Included in new.php
 */
/*
 * Variables required: 
 * $pluploadButtonId - button id for triggering file selection dialog
 * $pluploadDropTargetId - droppable element id for drag and drop files
 * $pluploadFileListDivId - container element id for displaying uploaded file name, file size and upload status
 * $tempFolderPath - temp folder name (not the whole folder path, because it is visible in js coding) for storing uploaded files
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libfilesystem.php");
include_once($intranet_root."/includes/libinterface.php");
include_once($intranet_root."/home/eAdmin/ResourcesMgmt/RepairSystem/repair_conf.php");

$lfs = new libfilesystem();
$linterface = new interface_html();

$FileFormatAry = $repair_conf['FileFormatAry'];
$MaxFileSize = $repair_conf['MaxFileSize']; // MB
$MaxFileSizeInBytes = $MaxFileSize * 1024 * 1024; // bytes

echo $linterface->Include_Plupload_JS_CSS();
?>
<style type="text/css">
.DropFileArea
{
	width:100%;
	height:30px;
	line-height:30px;
	text-align:center;
	vertical-align:middle;
	border:2px dashed #CCCCCC;
}

.DropFileAreaDragOver 
{
	width:100%;
	height:30px;
	line-height:30px;
	text-align:center;
	vertical-align:middle;
	border: 2px dashed #00FF55;
}
</style>
<script type="text/javascript" language="JavaScript">
var TargetFolder = '<?=$tempFolderPath?>';
var AllowedFileFormats = [<?= count($FileFormatAry)>0? '"'.implode('","',$FileFormatAry).'"' : ""?>];
var MaxFileSizeInBytes = <?=$MaxFileSizeInBytes?>;

function jsDeleteTempFile(fileId)
{
	var filename = $('div#'+fileId + ' span.file_name').html();
	$.post(
		'/home/eService/RepairSystem/plupload_remove_file.php',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			$('div#'+fileId).remove();
		}
	);
}

function jsDeleteTempFileByName(filename)
{
	$.post(
		'/home/eService/RepairSystem/plupload_remove_file.php',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			
		}
	);
}

function jsIsFileUploadCompleted()
{
	var totalSelected = uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<uploader.files.length;i++) {
		if(uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	
	return totalDone == totalSelected;
}

function jsNumberOfFileUploaded()
{
	var dv = $('#<?=$pluploadFileListDivId?> div');
	return dv.length;
}

var uploader = new plupload.Uploader({
			        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
			        autostart : true,
			        browse_button : '<?=$pluploadButtonId?>',
			        drop_element : '<?=$pluploadDropTargetId?>',
			        container : '<?=$pluploadContainerId?>',
					flash_swf_url : '/templates/jquery/plupload/plupload.flash.swf',
					silverlight_xap_url : '/templates/jquery/plupload/plupload.silverlight.xap',
					url : '/home/eService/RepairSystem/plupload_process.php?TargetFolder=<?=$tempFolderPath?>',
			<? if(is_numeric($MaxFileSize) && $MaxFileSize > 0) { ?>		
					max_file_size : '<?=$MaxFileSize?>mb',
			<? } ?>
					chunk_size : '1mb',
					unique_names :false
			    });

uploader.bind('Init', function(up, params) {
    if (uploader.features.dragdrop) {
		var target = document.getElementById("<?=$pluploadDropTargetId?>");
	      
	    target.ondragover = function(event) {
	        event.dataTransfer.dropEffect = "copy";
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragenter = function() {
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragleave = function() {
	        this.className = "DropFileArea";
	    };
	      
	    target.ondrop = function(event) {
	    	event.preventDefault();
	        this.className = "DropFileArea";
	    };
	      
	    $('#<?=$pluploadDropTargetId?>').show();
      	
    }else{
    	$('#<?=$pluploadDropTargetId?>').hide();
    }
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
    $.each(files, function(i, file) {
    	var ok = true;
    	if(MaxFileSizeInBytes > 0 && file.size > MaxFileSizeInBytes) {
    		ok = false;
    	}
    	var ext = Get_File_Ext(file.name);
    	if(ext && AllowedFileFormats.length>0 && AllowedFileFormats.indexOf(ext) == -1){
    		ok = false;
    	}
    	if(ok) {    		
	        $('#<?=$pluploadFileListDivId?>').append(
	            '<div id="' + file.id + '">' +
				'<span id="img_'+file.id+'"></span>' + 
	            '<span class="file_name">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b>' +
	        	'</div>'
	        );
    	}else{
    		jsDeleteTempFileByName(file.name);
    		up.removeFile(file);
    	}
    });
	
    up.refresh(); // Reposition Flash/Silverlight
    up.start();
});

uploader.bind('UploadProgress', function(up, file) {
    $('#' + file.id + " b").html(file.percent + "%");
});

uploader.bind('UploadComplete', function(up, files) {
	$.each(files, function(i, file) {
		if( $('#' + file.id + ' a').length == 0) {
			$('#img_' + file.id).html('<img id="img_'+file.id+'" src="/file/repair/temp<?=$tempFolderPath?>/'+ file.name +'" width="100" height="100" border="0"> ');
			$('#' + file.id).append('<a href="javascript:void(0);" onclick="jsDeleteTempFile(\''+file.id+'\');"> [ <?=$Lang['Btn']['Delete']?> ]</a>');
		}
	});
});

</script>