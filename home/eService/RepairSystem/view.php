<?php
# using: 

############ Change Log Start ###############
#
#   Date    :   2020-08-02 (Cameron)
#               Modify: reorder item to: Location, Location Details, Category, Request Summary [case #X190080]
#
#	Date	:	2018-08-08	(Henry)
#				Fix: sql injection issue
#
#	Date	:	2017-11-15  (Cameron)
#				Fix: hide submit button for Processing record [case #E130900]
#					- show case number
#
#	Date	:	2017-10-11  (Cameron)
#				Fix: show left reporter as -- (case #N128259)
#
#	Date	:	2017-06-19 (Cameron)
#				Improvement: change view_photo to use load_dyn_size_thickbox_ip()
#
#	Date	:	2017-05-31 (Cameron)
#				Fix: disable button before submit to avoid adding duplicate record
#
#	Date	:	2017-05-10	(Cameron)
#				Improvement: show photo(s) of the request
#
#	Date	:	2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date	:	2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access for student
#
#	Date	:	2015-02-12	(Omas)
#				Improved : Add new field FollowUpPerson
#
#	Date	:	2014-12-02 (Omas)
#				fixed wrong display of status
#				improve when status = pending / process can both add remarks, status = pending  can cancel request
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date:	2011-01-03 [YAtWoon]
#			- Location Detail is not a must
#			- add rejected status
#
#	Date	:	2010-11-23	YatWoon
#				- allow view other's record (view only if the record is created by others)
#				- IP25 UI standard
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

if ( !isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || $_SESSION['UserType'] == USERTYPE_ALUMNI || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['RepairSystemAccessForParent']) || ($_SESSION['UserType'] == USERTYPE_STUDENT && $sys_custom['RepairSystemBanAccessForStudent']) || (isset($sys_custom['RepairSystem']['GrantedUserIDArr']) && !in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();
$name_field = getNameFieldByLang("u.");
$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");

$RecordID = IntegerSafe($RecordID);

$sql = "SELECT
			REC.CaseNumber, 
			CAT.Name, 
			IF((REC.LocationBuildingID!=0),	
				CONCAT(CONCAT(IF($buildingName IS NOT NULL, $buildingName, 'Main Building'), IF($levelName!='', CONCAT(' &gt; ', $levelName), ''))),
				LOC.LocationName) as LocationName, 
			CONCAT(IF(REC.LocationBuildingID!=0 AND $LocationName!='', $LocationName, ''), IF($LocationName!='' AND REC.DetailsLocation!='', CONCAT(IF(REC.LocationBuildingID!=0, ' &gt; ', ''), REC.DetailsLocation), REC.DetailsLocation)) as DetailLocation, 
			REC.Title, 
			REC.Content, 
			CASE(REC.RecordStatus)
				WHEN '".REPAIR_RECORD_STATUS_CANCELLED."' THEN '".$i_status_cancel."'
				WHEN '".REPAIR_RECORD_STATUS_PROCESS."' THEN '".$Lang['RepairSystem']['Processing']."'
				WHEN '".REPAIR_RECORD_STATUS_PENDING."' THEN '".$Lang['RepairSystem']['Pending']."'
				WHEN '".REPAIR_RECORD_STATUS_COMPLETED."' THEN '".$Lang['General']['Completed']."'
				WHEN '".REPAIR_RECORD_STATUS_REJECTED."' THEN '".$i_status_rejected."'
				WHEN '".REPAIR_RECORD_STATUS_ARCHIVED."' THEN '".$Lang['eDiscipline']['Archived']."'
				ELSE '--' END as RecordStatus, 
			REC.Remark,
			REC.UserID,
			REC.DateInput,
			IF($name_field IS NULL,'".$Lang['General']['EmptySymbol']."',$name_field) as user_name,
			IF(REC.FollowUpPerson IS NULL,'".$Lang['General']['EmptySymbol']."',REC.FollowUpPerson) as FollowUpPerson
		FROM 
			REPAIR_SYSTEM_RECORDS REC LEFT OUTER JOIN 
			REPAIR_SYSTEM_CATEGORY CAT ON (CAT.CategoryID=REC.CategoryID) LEFT OUTER JOIN 
			INVENTORY_LOCATION l ON (l.LocationID=REC.LocationID) LEFT JOIN 
			INVENTORY_LOCATION_LEVEL lv ON (lv.LocationLevelID=REC.LocationLevelID) LEFT JOIN 
			INVENTORY_LOCATION_BUILDING as bu on (bu.BuildingID=REC.LocationBuildingID) LEFT OUTER JOIN
			REPAIR_SYSTEM_LOCATION LOC ON (LOC.LocationID=REC.LocationID) 
			left join INTRANET_USER u on (u.UserID=REC.UserID)
		WHERE 
			REC.RecordID='".$RecordID."'";
			//echo $sql;
$result = $lrepairsystem->returnArray($sql);
list($caseNumber, $catName, $locationName, $detailsLocation, $title, $content, $recordStatus, $remark, $userid,$record_date,$user_name,$FollowUpPerson) = $result[0];
if($remark=="") $remark = $Lang['General']['EmptySymbol'];

/*
if($userid!=$UserID) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}
*/

# menu highlight setting
$CurrentPageArr['eServiceRepairSystem'] = 1;
$CurrentPage = "PageList";

# Left menu 
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['RepairSystem']['List']);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['ViewRequest']);

echo $linterface->Include_Thickbox_JS_CSS();
echo $lrepairsystem->getPhotoCSS();

?>
<script language="javascript">
<!--
function goBack() {
	window.location='index.php';
}

function goSubmit() {
	$('input.actionBtn').attr('disabled', 'disabled');
	if(document.form1.additionRemark.value.length<=0) {
		alert("<?=$i_alert_pleasefillin." ".$i_UserRemark?>");
		$('input.actionBtn').attr('disabled', '');	
		return false;
	} else {
		document.form1.flag.value = "submit";
		document.form1.action = "view_update.php";
		document.form1.submit();	
	}
}

function goCancel() {
	if(confirm("<?=$Lang['RepairSystem']['CancelRequestMsg']?>")) {
		document.form1.flag.value = "cancel";
		document.form1.action = "view_update.php";	
		document.form1.submit();	
	}
}

function view_photo(photoID) {
//	tb_show('<?=$Lang['RepairSystem']['ViewOriginalPhoto']?>','ajax_view_photo.php?PhotoID='+photoID+'&height=500&width=800');
	load_dyn_size_thickbox_ip('<?=$Lang['RepairSystem']['ViewOriginalPhoto']?>', 'onloadImageThickBox("'+photoID+'");', inlineID='', defaultHeight=400, defaultWidth=800);
}

function onloadImageThickBox(photoID) {
	$('div#TB_ajaxContent').load(
		"ajax_view_photo.php", 
		{ 
			PhotoID: photoID
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

-->
</script>


<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" method="POST">
<table class="form_table_v30">
<tr>
	<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$Lang['RepairSystem']['RequestInformation']?> -</i></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['CaseNumber']?></td>
	<td><?=$caseNumber?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestDate']?></td>
	<td><?=substr($record_date,0,10)?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['Reporter']?></td>
	<td><?=$user_name?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['Location']?></td>
	<td><?=$locationName?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['LocationDetail']?></td>
	<td><?=$detailsLocation ? $detailsLocation : $Lang['General']['EmptySymbol'] ?></td>
</tr>

<tr>
    <td class="field_title"><?=$Lang['RepairSystem']['Category']?></td>
    <td><?=$catName?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestSummary']?></td>
	<td><?=$title?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['RequestDetails']?></td>
	<td><?=nl2br($content)?></td>
</tr>

<!-- photos -->
<?=$lrepairsystem->getRepairReqPhotoUI($RecordID)?>

<tr>
	<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><br><i> - <?=$Lang['RepairSystem']['FollowupInformation']?> -</i></td>
</tr>

<tr>
	<td class="field_title"><?=$i_general_status?></td>
	<td><?=$recordStatus?> </td>
</tr>

<tr>
	<td class="field_title"><?=$i_UserRemark?></td>
	<td><?=nl2br(intranet_htmlspecialchars(intranet_undo_htmlspecialchars(intranet_undo_htmlspecialchars($remark))))?></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['FollowUpPerson']?></td>
	<td><?=$FollowUpPerson?></td>
</tr>
<? if(($recordStatus==$Lang['RepairSystem']['Processing'] || $recordStatus==$Lang['RepairSystem']['Pending'] )&& $userid == $UserID) { ?>
<tr>
	<td class="field_title"><?=$Lang['RepairSystem']['AddRemark']?></td>
	<td><?=$linterface->GET_TEXTAREA("additionRemark", "");?></td>
</tr>
<? } ?>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
		<?
		if(($recordStatus==$Lang['RepairSystem']['Processing'] || $recordStatus==$Lang['RepairSystem']['Pending']) && $userid == $UserID)
			echo $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		?>
		<?
		if($recordStatus==$Lang['RepairSystem']['Pending'] && $userid == $UserID)
			echo $linterface->GET_ACTION_BTN($Lang['RepairSystem']['CancelRequest'], "button", "goCancel()", "btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
		?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "btnBack", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
<p class="spacer"></p>
</div>

<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="flag" id="flag" value="">

</form>

<?
// print $linterface->FOCUS_ON_LOAD("form1.RecordDate");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>