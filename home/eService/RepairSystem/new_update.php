<?php
# using:  

##########################
#
#   Date    :   2020-09-29 (Cameron)
#               add case SrcFrom == 'eAdmin' [case #X190079]
#
#	Date	:	2017-12-28 (Cameron)
#				Fix bug: don't use "continue" to handle upload file for traditional method for IE compatible mode [case #N133395]
#
#	Date	:	2017-05-31 (Cameron)
#				Fix bug to handle special character in Title field
#
#	Date	:	2017-05-09 (Cameron)
#				Handle photo upload procedure
#
#	Date	:	2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date	:	2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access for student
#
#	Date:	2014-11-06	(Omas)
#	Add New column - CaseNumber
#
###########################


$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/repair_conf.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if ( !isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || $_SESSION['UserType'] == USERTYPE_ALUMNI || ($_SESSION['UserType'] == USERTYPE_PARENT && !$sys_custom['RepairSystemAccessForParent']) || ($_SESSION['UserType'] == USERTYPE_STUDENT && $sys_custom['RepairSystemBanAccessForStudent']) || (isset($sys_custom['RepairSystem']['GrantedUserIDArr']) && !in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$lrepairsystem = new librepairsystem();

if(sizeof($_POST)==0) {
    header("Location: ".(($_POST['SrcFrom'] == 'eAdmin') ? $PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/RepairSystem/index.php' : 'index.php'));
}

$Title = $lrepairsystem->getRequestSummaryTitle($RequestID);

$dataAry['CategoryID'] = $CatID;
$dataAry['LocationBuildingID'] = $buildingID;
$dataAry['LocationLevelID'] = $LocationLevelID;
$dataAry['LocationID'] = $locID;
$dataAry['DetailsLocation'] = intranet_htmlspecialchars($LocationDetail);
$dataAry['UserID'] = $UserID;
$dataAry['Title'] = $lrepairsystem->Get_Safe_Sql_Query($Title);
$dataAry['Content'] = intranet_htmlspecialchars($content);
$dataAry['CaseNumber'] = $lrepairsystem->retrieveNewCaseNumber($CatID);


$recordID = $lrepairsystem->insertRepairRecord($dataAry);

$result = array();
$returnMsg = '';
$result[] = $recordID;


// handle file directory
if (!empty($TargetFolder) || !empty($_FILES)) {	
	$libfilesystem = new libfilesystem();

	$FileFormatAry = $repair_conf['FileFormatAry'];
	
	$MaxFileSize = $repair_conf['MaxFileSize']; // MB
	$RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
	$MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;
	$RestrictFileFormat = count($FileFormatAry) > 0;
	
	# create folder
	$targetPath = "/repair/";
	$targetPath_2ndLevel = "/final/";
	$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath);
	
	if ( !file_exists($target1stPath) || !is_dir($target1stPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
	}
	
	$target2ndPath =  str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath.$targetPath_2ndLevel);
	if ( !file_exists($target2ndPath) || !is_dir($target2ndPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
	}

	$targetFullPath = $target2ndPath;
	chmod($targetFullPath, 0777);
}


// handle uploaded photos
if (!empty($TargetFolder)) {	// by plupload method
	
	$tempPath = str_replace('//', '/', $PATH_WRT_ROOT."file/repair/temp/".$TargetFolder);
	
	$tempFileAry = $libfilesystem->return_folderlist($tempPath);
	
	$numOfFiles = count($tempFileAry);
	
	for($i=0;$i<$numOfFiles;$i++) {
		
		# get file name
		$origFileName = $libfilesystem->get_file_basename($tempFileAry[$i]);
		if (empty($origFileName)) continue;

		$fileSize = filesize($tempFileAry[$i]);
		
		# Check file size
		if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
			$result[] = false;
			$returnMsg = 'FileSizeExceedLimit';
			continue;
		}
		
		# Extension
		$ext = substr($libfilesystem->file_ext($origFileName),1);
		
		# Check file format
		if($RestrictFileFormat && !in_array(strtolower($ext),$FileFormatAry)){
			$result[] = false;
			$returnMsg = 'FileTypeIsNotSupported';
			continue;
		}
		
		$timestamp = date('YmdHis');
		
		# encode file name
		$targetFileHashName = "r".$recordID."_".$timestamp.$i;
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# upload file
		$uploadResult = $libfilesystem->lfs_copy($tempFileAry[$i], $targetFile);
		if(file_exists($targetFile)){
			chmod($targetFile, 0777);
		}
		
		# save to db
		unset($dataAry);
		$dataAry['RecordID'] = $recordID;
		$dataAry['FileName'] = $origFileName;
		$dataAry['FileHashName'] = $targetFileHashName;
		$dataAry['SizeInBytes'] = $fileSize;
		$photoID = $lrepairsystem->insertRepairPhoto($dataAry);
		
	}
	
	// Do clean up
	$libfilesystem->deleteDirectory($tempPath);
	$sql = "DELETE FROM REPAIR_SYSTEM_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND Folder='".$TargetFolder."'";
	$lrepairsystem->db_db_query($sql);
	
}
else if (!empty($_FILES)){	// traditional method

//	$numOfFiles = count($_FILES['File']['name']);

	# get file name
	$tempFile = $_FILES['File']['tmp_name'];
	
	$origFileName =  $_FILES['File']['name'];
	if (!empty($origFileName)) {
	
		$fileSize = $_FILES['File']['size'];
		
		# Check file size
		if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
			$result[] = false;
			$returnMsg = 'FileSizeExceedLimit';
		}
		
		# Extension
		$ext = substr($libfilesystem->file_ext($origFileName),1);
		
		# Check file format
		if($RestrictFileFormat && !in_array(strtolower($ext),$FileFormatAry)){
			$result[] = false;
			$returnMsg = 'FileTypeIsBanned';
		}
		
		$timestamp = date('YmdHis');
		
		# encode file name
		$targetFileHashName = "r".$recordID."_".$timestamp;
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# upload file
		$uploadResult = move_uploaded_file($tempFile, $targetFile);
		
		chmod($targetFile, 0777);
		
		# save to db
		unset($dataAry);
		$dataAry['RecordID'] = $recordID;
		$dataAry['FileName'] = $origFileName;
		$dataAry['FileHashName'] = $targetFileHashName;
		$dataAry['SizeInBytes'] = $fileSize;
		$photoID = $lrepairsystem->insertRepairPhoto($dataAry);
	}	
}

intranet_closedb();

if($returnMsg != '') {
	$msg = $returnMsg;
}else if(!in_array(false,$result)){
	$msg = "AddSuccess";
}else {
	$msg = "AddUnsuccess";
}	

header("Location: ".(($_POST['SrcFrom'] == 'eAdmin') ? $PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/RepairSystem/index.php' : 'index.php')."?returnMsgKey=".$msg);

?>