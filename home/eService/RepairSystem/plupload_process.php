<?php
// Editing by 

/*
 * 	copy from digital archive 
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();

$linux_username = get_current_user(); // usually is junior on EJ, eclass if IP25

$TargetFolder = $_REQUEST['TargetFolder'];

$targetPath_Level1 = "/repair/";
$targetPath_Level2 = "/temp/";


$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath_Level1);

if ( !file_exists($target1stPath) || !is_dir($target1stPath)) {
	$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
}

$target2ndPath = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath_Level1.$targetPath_Level2);
if ( !file_exists($target2ndPath) || !is_dir($target2ndPath)) {
	$resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
	if(file_exists($target2ndPath)) {
		chmod($target2ndPath, 0777);
	}
}

$plupload_tmp_path = str_replace('//', '/', $PATH_WRT_ROOT."file/".$targetPath_Level1.$targetPath_Level2.$TargetFolder);
if(file_exists($plupload_tmp_path)) {
	chmod($plupload_tmp_path, 0777);
}

$pluploadPar['targetDir'] = $plupload_tmp_path;

include_once($PATH_WRT_ROOT."includes/plupload.php");

intranet_closedb();
?>