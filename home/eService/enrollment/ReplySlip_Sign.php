<?php
# using: 

/*
 * Change Log:
 * Date:	2017-04-24	Villa
 * 			-add ModifiedAns, CheckFrom for submiting checking 
 * Date:	2017-04-19 Villa
 * 			-Create the File - Copy from eNotice
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
$lform = new libform();
$libenroll = new libclubsenrol();
$linterface = new interface_html("popup.html");
$MulitMC_Dimeter = '#MCDIMETER#';


//Get ReplySlip Related to EnrolGroup
$ReplySlipRelationInfo = $libenroll->getReplySlipEnrolGroupRelation('',$enrolmentGroupID,$RecordType);
//Get ReplySlip Detail
$ReplySlipID = $ReplySlipRelationInfo[0]['ReplySlipID'];
$ReplySlipRelationMappingID = $ReplySlipRelationInfo[0]['ReplySlipGroupMappingID'];
$ReplySlipInfo = $libenroll->getReplySlip($ReplySlipID);
$ReplySlipInfo = $ReplySlipInfo[0];

if($ReplySlipRelationMappingID){
	//if have reply slip, get back the replied data
	$ReplySlipReplyInfo = $libenroll->getReplySlipReply('',$ReplySlipRelationMappingID,$_SESSION['UserID']);
	$ReplySlipReplyInfo = $ReplySlipReplyInfo[0];
	$ReplySlipReplyID = $ReplySlipReplyInfo['ReplySlipReplyID'];
	$ReplySlipReplyAns = $ReplySlipReplyInfo['Answer'];
	//case->MC-dimeter
	$ReplySlipReplyAns = str_replace($MulitMC_Dimeter, ',', $ReplySlipReplyAns);
	$libuser = new libuser($ReplySlipReplyInfo['ModifiedBy']);
	$ModifiedByName = Get_Lang_Selection($libuser->ChineseName, $libuser->EnglishName);
}
#SignInfoTable START
$SignInfoTable = "<table width='100%' class='form_table_v30'>";
//Title
$SignInfoTable .= "<tr>";
	$SignInfoTable .= "<td class='field_title'>".$eEnrollment['replySlip']['Title'] ."</td>";
	$SignInfoTable .= "<td>".$ReplySlipInfo['Title']."</td>";
$SignInfoTable .= "</tr>";
//Date Build
$SignInfoTable .= "<tr>";
	$SignInfoTable .= "<td class='field_title'>".$eEnrollment['replySlip']['BuildDate'] ."</td>";
	$SignInfoTable .= "<td>".$ReplySlipInfo['DateInput']."</td>";
$SignInfoTable .= "</tr>";
//Instruction
$SignInfoTable .= "<tr>";
	$SignInfoTable .= "<td class='field_title'>".$eEnrollment['replySlip']['Instruction'] ."</td>";
	$SignInfoTable .= "<td>".Get_String_Display($ReplySlipInfo['Instruction'])."</td>";
$SignInfoTable .= "</tr>";

if($ReplySlipReplyID){ //IF Signed 
	#Display $SignedInstruction
	$SignedInstruction = str_replace("##DATE##", $ReplySlipReplyInfo['DateModified'], $eEnrollment['replySlip']['SignedInstruction']);
	$SignedInstruction = str_replace("##PERSON##", $ModifiedByName, $SignedInstruction);
	$SignInfoTable .= '<tr>'.'<td colspan="2" style="color: red;">'.$SignedInstruction.'</td>'.'</tr>';
}
$SignInfoTable .= "</table>";
$SignInfoTable .= '<input type="hidden" name="qStr_ReplySlip" id="qStr_ReplySlip" value="'.$ReplySlipInfo['Detail'].'">';
$SignInfoTable .= '<input type="hidden" name="aStr_ReplySlip" id="aStr_ReplySlip" value="'.$ReplySlipReplyAns.'">';
#SignInfoTable END


// Question and Answer
#Get Must Submit Arr START
$queString = str_replace("&amp;", "&", $ReplySlipInfo['Detail']);
$queString = preg_replace('(\r\n|\n)', "<br>", $queString);
$queString = trim($queString)." ";
$qAry = $libenroll->splitQuestion($queString, true);
$aAry = $libenroll->parseAnswerStr($ansString);

// Convert Question String for display
$queString = $lform->getConvertedString($queString);

// init array for js checking
$mustSubmitArray = array();
$numOfQuestion = count($qAry);
// loop Questions
for($i=0; $i<count($qAry); $i++){
	$currentQuestion = $qAry[$i];
	$mustSubmitArray[] = $reqFillAllFields || $currentQuestion[3];
	// for MC Questions
	if($currentQuestion[0]==2 && $mustSubmitArray[$i])
	{
		$optionCount = count((array)$currentQuestion[4]);
		if(count(array_unique($currentQuestion[4])) > 1)
			$mustSubmitArray[$i] = $optionCount;
	}
}
#Get Must Submit Arr END

//Only Show IF set ReplySlip
$UpdateBtn =  $ReplySlipID?$linterface->GET_ACTION_BTN($button_update, "button", "SignReplySlip(); return false;","cancelbtn"):"";
$MODULE_OBJ['title'] = $Lang['eNotice']['ReplySlipForm'];
$linterface->LAYOUT_START();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>

<script language="Javascript">
var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';

function SignReplySlip(){
	
	if(CheckFrom()){
		$('#cancelbtn').attr("disabled","disabled");	
		// Block Document
// 		Block_Document('loading');
		
		var params = ModifiedAns();
		$.ajax({
			url: "ReplySlip_Sign_Update.php",
			type: "POST", 
			data: {
				"AnsData":params,
				"ReplySlipRelationMappingID" : '<?=$ReplySlipRelationMappingID?>',
				"ReplySlipReplyID" : '<?=$ReplySlipReplyID?>',
				"RecordType" : '<?=$RecordType?>'
			},
			success: function(){
	// 			self.close();
				setTimeout(void(0), 10000);
				location.reload();
			}
		});
	}else{
		//Do nothing and skip the update
	}
	
	function ModifiedAns(){
		var ansStr = $('form[name="answersheet"]').serialize(); //missing item if MC/ TrueFalse Question. ModifiedANS=>Handle this Problem
		var numOfQuestion = '<?=$numOfQuestion?>';
		for(var i=0;i<numOfQuestion;i++){
			var j = i + 1;
			var k = j + 1;
			var checkQuestion = ansStr.search("F"+j);
			if(checkQuestion=='-1'){
				if(j==numOfQuestion){
					ansStr = ansStr+"&F"+j+"=";
				}else{
					ansStr = ansStr.replace("F"+k,"F"+j+"=&F"+k );
				}
			}
		}
		return ansStr;
	}
	
    function CheckFrom(){
        var check = true;
		var ansStr = ModifiedAns();
		var ansStrArr = ansStr.split('&');
		for(var i=0; i<ansStrArr.length; i++){
			var ans = ansStrArr[i].split('=');
			if(MustSubmitArr[i]){
				if(ans[1].length && ans[1].trim!=''){
					
				}else{
					var j = i + 1;
					
					$('[name="F'+j+'"]').focus();
					alert("<?=$eEnrollment['replySlip']['MustFillWarning']?>");
					check = false;
					return false;
				} 
			}
		}
		if(check){
			return true;
		}else{
			return false;
		}
     }
// 	$('form[name="answersheet"]').attr('method','post');
// 	$('form[name="answersheet"]').attr('action','ReplySlip_Sign_Update.php');
// 	$('form[name="answersheet"]').submit();
}
</script>

<br />
<p>
<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="">
        <input type="hidden" name="qStr" value="">
        <input type="hidden" name="aStr" value="">
</form>
<tr>
<td>
<?=$SignInfoTable?>
</td>
</tr>
<tr>
	<td align="left">
          
                <?php if($ReplySlipRelationMappingID){?> 
                 	<script language="Javascript">
                 	//show only have SET ReplySLip
	                <?=$lform->getWordsInJS()?>
	                var DisplayQuestionNumber = '<?=$ReplySlipInfo['DisplayQuestionNumber']?>';
	                var sheet= new Answersheet();
	                var AllFieldsReq = '<?=$ReplySlipInfo['AllFieldsReq']?>';
	                // attention: MUST replace '"' to '&quot;'
	                sheet.qString= $('#qStr_ReplySlip').val();
	                sheet.qString=sheet.qString.replace(/#TAR#(([A-Z]|[a-z]|[0-9])+)#/g, '');
	                sheet.aString=$('#aStr_ReplySlip').val();
	          
	                //edit submitted application
	                sheet.mode=1;
	                // display * for Questions must be submitted
					sheet.mustSubmit=1;
	                sheet.answer=sheet.sheetArr();
	                
	                Part3 = '';
	                document.write(editPanel());
	                var MustSubmitArr = [];
	                <?php foreach((array)$mustSubmitArray as $key=>$_mustSubmitArray){?>
	               		 MustSubmitArr['<?=$key?>'] = '<?=$_mustSubmitArray?>';
	                <?php }?>
	                </script>
                <?php }else{?>
                		<div align='center'><?=$eEnrollment['replySlip']['ReplySlipNotSet']?></div>
                <?php }?>
	</td>
</tr>

<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $UpdateBtn?>
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>