<?php 
# using: Villa

/*
 * Change Log:
 * Date:	2017-04-19
 * 			-Create the File - Copy from eNotice
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$AnswerTag = '#ANS#';
$MulitMC_Dimeter = '#MCDIMETER#';

#Cater Each Answer
$AnsTemp = explode('&',$AnsData);
foreach((array)$AnsTemp as $_AnsTemp){
	$AnsTemp_1 =  explode('=',$_AnsTemp);
	$AnwserAry[$AnsTemp_1[0]][] = $AnsTemp_1[1];
}

$Answer = '';
foreach ((array)$AnwserAry as $_AnwserAry){
	$temp = implode($MulitMC_Dimeter, (array)$_AnwserAry);
	$Answer .= $AnswerTag.$temp;
	/*	Type 1: YesNo Question/ MC
	 * 	save as=> #ANS#x where x is a number
	 *  Type 2: Multi-MC
	 *  save as=> #ANS#x#DIMETER#x where x is a number 
	 *  Type 3: ShortQuestion/ LongQuester
	 *  save as=> #ANS#abc where abc is a string 
	 */ 
	
}
// $Answer= preg_replace('#\s+#','<br>',trim($Answer));
// foreach ((array)$AnsTemp_1 as $_AnsTemp_1){
// 	$Ans= explode('=',$_AnsTemp_1);
// 	$Answer .= $AnswerTag.$Ans[1];
// }
$StudentID = $_SESSION['UserID'];
$Answer = urldecode ($Answer); 
$libenroll->saveReplySlipReply($ReplySlipReplyID,$ReplySlipRelationMappingID,$StudentID,$Answer,$IsDeleted=0);
intranet_closedb();
?>