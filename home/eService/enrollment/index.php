<?php
// using: 
/*
 * 	Log
 * 
 * Date:    2020-05-25 [Tommy]
 *          fix: club "Maximum number of club(s) you want to participate" selectbox may be null when $sys_custom['eEnrollment']['useMinApplyNum'] is on
 *          modified min, max number and select value of selectbox
 * 
 * Date:    2020-05-04 [Tommy]
 *          only show enrol club to target form student in $sys_custom['eEnrollment']['setTargetForm']
 * 
 * Date:    2020-04-22 [Tommy]
 *          remove $libenroll->hasAccessRight() because this checking use for eAdmin
 * 
 * Date:    2020-04-01 [Tommy]
 *          add function using min. no of club for student to submit max. no of club student want to join
 * 
 * Date:    2020-02-17 [ Henry]
 *          Added $sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] to force student select all club
 *  
 * Date:    2019-07-24 [ Anna]
 *          Added $sys_custom['eEnrolment']['Application']['EachCategoryNeed']
 *          Added $sys_custom['eEnrolment']['Application']['MinApply']
 * 
 * Date:    2018-08-27 [Anna]
 *          Added $sys_custom['eEnrolment']['UnitRolePoint'] cust
 * 
 * Date:    2018-08-27 [Ivan] [X145096] [ip.2.5.9.10.1]:
 *          Added customization $sys_custom['eEnrolment']['ClassTeacherView'] to let class teacher enter eEnrolment
 * 
 * Date:    2018-04-30 (Anna)
 *          Added Committee recruitment
 * 
 *  Date:	2017-08-14 (Omas) #P110324 
 *  		Modified ReplySlip Related $sys_custom['eEnrolment']['ReplySlip']
 *   
 *  Date:	2017-04-19 (Villa)
 *  		Add ReplySlip Related
 *  
 *  Date:	2017-02-09 (Villa) R112786
 * 			Modified submit btn - hide or show the btn depending on enrollPersontype
 * 
 *  Date:	2016-09-07 (Villa) -#T102993 
 * 			Add checking function to hide the choice if the Max. no. of clubs for a student to apply is greater than the number of club can be applied in current term/ whole year
 * 			Add $CanBeAppliedCurrentTermClub - get number of club can be applied in current term
 * 			Add $numOfCanBeAppliedWholeYearClub - get number of club can be applied in Whole Year
 * 			
 * 	Date:	2015-12-16	(Omas) -#J84059 - SIS
 * 			added $sys_custom['eEnrolment']['hideRejectedTable'] - hide rejected table
 * 			added $sys_custom['eEnrolment']['hideNonApprovedRow'] - hide non approved row at announcement period
 * 
 *  Date:	2015-10-07	(Omas)
 * 			Modified js reloadTimeClashDisplay() change to async, ajax not yet return will disable the button
 * 			Modified js checkForm() - disable submit button immediately
 * 
 *  Date:	2015-09-25	(Omas)
 * 			Add $sys_custom['eEnrolment']['maxWantDefaultZero']
 * 
 *  Date:	2015-02-10	(Omas)
 *			Fix- check different category club time clash 
 *
 *  Date:	2014-12-16	Omas
 * 			Fixed when using term-base mode, number of clubs student can join get wrong problem when term 2,3,4..
 * 
 * 	Date:	2014-11-20	Omas
 * 			Performance Improvement: Disable some access right checking when the user is student 
 * 
 * 	Date:	2014-10-31	Omas
 * 			- rearrange the position of group category when using "category mode"
 * 
 * 	Date:	2014-10-29
 * 			- Enable the drop down list after submit => ensure $maxwant can store in DB
 * 
 * 	Date:	2014-10-14
 * 			- Add round report for SIS customization
 * 
 * 	Date:	2014-09-26 Bill
 * 			- Disable submit button to avoid double submit of records
 * 
 * 	Date:	2014-09-12 Ivan [J67808]
 * 			Fixed: check number of enrolled club by club member list instead of enrollment application now
 * 
 * 	Date:	2013-09-02 [Ivan][2013-0902-1015-36073]
 * 			Fixed: missed to cater the case that TargetSemester is empty => quota checking is failed			
 * 
 *  Date:	2013-08-12 [Cameron] Add argument CategoryTypeID to GET_CATEGORY_SEL()
 * 	Date:	2013-08-08 [Cameron]
 * 				Show clubs that are applied by counterpart in disabled option list
 * 	Date:	2013-08-02 [Cameron] 
 * 				1. Add $currentUserType to pass as argument to $libenroll_ui->Get_Club_Selection
 * 				2. Don't show clubs in the club option list if user is not target applicant
 * 				3. If ClubTargetApplicant (Apply to individual club) is not set, use Club Setting (Apply to all clubs) 
 * 					to control if club option list is enabled or not
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

if ($_GET['fromService']) {
    session_register_intranet('eEnrolment_enter_from_eService', 1);
}
else {
    session_register_intranet('eEnrolment_enter_from_eService', 0);
}

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$libenroll = new libclubsenrol('', $sel_category);
$libenroll_ui = new libclubsenrol_ui();
$libfcm = new form_class_manage();
if ($LibUser->isTeacherStaff()){
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	$haveClubMgmt = $libenroll->HAVE_CLUB_MGT();
	$haveActivityMgmt = $libenroll->HAVE_EVENT_MGT();
	$isAnyGroupAdmin = $libenroll->checkAnyIntranetGroupAdminWithUserMgmtRight();
}

$withinEnrolStage = $libenroll->WITHIN_ENROLMENT_STAGE("club");
$withinEnrolStageText = ($withinEnrolStage)? '1' : '0';
$withinProcessStage = $libenroll->WITHIN_ENROLMENT_PROCESS_STAGE();

if (isset($_GET['p'])) {
	// By $_GET method
	$lurlparahandler = new liburlparahandler('d', $_GET['p'], $enrolConfigAry['encryptionKey']);
	$paraAry = $lurlparahandler->getParaAry();
	$ChildID = $paraAry['ChildID'];
	$sel_category = $paraAry['sel_category'];
	$returnMsgKey = $paraAry['returnMsgKey'];
	
//	$ChildID = $lurlparahandler->returnParaValue('ChildID');
//	$sel_category = $lurlparahandler->returnParaValue('sel_category');
//	$returnMsgKey = $lurlparahandler->returnParaValue('returnMsgKey');

	if ($ChildID == '') {
		No_Access_Right_Pop_Up();
	}
}
else {
	// By $_POST method
	$ChildID = IntegerSafe(trim(stripslashes($_POST['ChildID'])));
	$OldChildID = IntegerSafe(trim(stripslashes($_POST['OldChildID'])));
	$sel_category = IntegerSafe(trim(stripslashes($_REQUEST['sel_category'])));
	$OldCategoryID = IntegerSafe(trim(stripslashes($_POST['OldCategoryID'])));
	
	if ($ChildID != $OldChildID) {
		$maxwant = '';
	}
	
	if ($sel_category != $OldCategoryID) {
		$maxwant = '';
	}
}
$CategoryID = ($sel_category=='')? 0 : $sel_category;

####################################################
############ Check Access Right [Start] ############
####################################################
if ($LibUser->IsStudent()) {
	$ApplicantID = $_SESSION['UserID'];
	$currentUserType = "S";
}
else if ($LibUser->IsParent()) {
	// is parent, get child ID
	$ChildArr = $LibUser->getChildrenList();
	if ($ChildID == '') {
		// get the first child by default
		$ApplicantID = $ChildArr[0]['StudentID'];
	}
	else {
		$ChildrenUserIDAry = Get_Array_By_Key($ChildArr, 'StudentID');
		
		if (!in_array($ChildID, $ChildrenUserIDAry)) {
			No_Access_Right_Pop_Up();
		}
		
		$ApplicantID = $ChildID;
	}
	
	$currentUserType = "P";
}

// Current User Type applies to those schools whose ClubTargetApplicant is set to true. e.g. SIS 
//if (!$sys_custom['eEnrolment']['ClubTargetApplicant'])
//{
//	$currentUserType = "";
//}

$canAccess = true;
// if (!$libenroll->hasAccessRight($_SESSION['UserID'])) {
// 	$canAccess = false;
// }

if (!$libenroll->isUseEnrollment()) {
	if ($libenroll->HAVE_CLUB_MGT()) {
		header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/group.php");
	}
	else if ($libenroll->HAVE_EVENT_MGT()) {
		header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/event.php");
	}
	else if ($_SESSION['UserType']==USERTYPE_STAFF) {
		header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/group.php");
	}
	else {
		$canAccess = false;
	}
}

//if (($isEnrolAdmin || $isEnrolMaster) && !$LibUser->isStudent() && !$LibUser->isParent()) {
//if ($LibUser->isTeacherStaff()) {

if ($LibUser->isTeacherStaff() && ($isEnrolAdmin || $isEnrolMaster || $haveClubMgmt || $haveActivityMgmt || $isAnyGroupAdmin)) {
	header("Location: ../../eAdmin/StudentMgmt/enrollment/");
}
else if ($LibUser->isTeacherStaff()) {
    if ($sys_custom['eEnrolment']['ClassTeacherView'] && $_SESSION["USER_BASIC_INFO"]["is_class_teacher"] && $_SESSION['eEnrolment_enter_from_eService']) {
        header("Location: ../../eAdmin/StudentMgmt/enrollment/reports/class_membership/index.php");
        die();
    }
	$canAccess = false;
}



if (!$canAccess) {
	No_Access_Right_Pop_Up();
}

####################################################
############# Check Access Right [End] #############
####################################################


####################################################
############# Page Proporties [Start] ##############
####################################################
$CurrentPage = "PageClubEnroll";
$CurrentPageArr['eServiceeEnrollment'] = 1;
$genStudentMenu = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['club_enroll'], "", 1);
if($sys_custom['eEnrolment']['CommitteeRecruitment']){
    $TAGS_OBJ[] = array( $eEnrollment['tab']['CommitteeRecruitment'], "committee_club.php", 0);
}

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
####################################################
############## Page Proporties [End] ###############
####################################################


####################################################
########## Application Settings [Start] ############
####################################################

### Children Selection
$h_childrenSelection = '';
if ($LibUser->IsParent()) {
	$h_childrenSelection .= $LibUser->Get_Children_Selection('ChildIDSel', 'ChildID', $ApplicantID, $_SESSION['UserID'], 'reloadPage();');
}

### Load application data in the library
$libenroll->retrieveApplicationStatus($ApplicantID, '', 0, $sel_category);

### Get clubs which can be applied
$canBeAppliedClubInfoAry = $libenroll->getGroupsAvailableForApply($ApplicantID, 0, $sel_category);
$numOfCanBeAppliedClub = count($canBeAppliedClubInfoAry);
$canBeAppliedClubEnrolGroupIdAry = Get_Array_By_Key($canBeAppliedClubInfoAry, 0);

$canBeAppliedClubCategoryAry =  array_unique(Get_Array_By_Key($canBeAppliedClubInfoAry, 2));
$numOfClubCategory = count($canBeAppliedClubCategoryAry);

### Get the number of Current Term Club can be applied
$CanBeAppliedCurrentTermClub = $libenroll->getGroupsAvailableForApplyInSpecifiedSemester($canBeAppliedClubEnrolGroupIdAry,"CurrentTerm");
$numOfCanBeAppliedCurrentTermClub = count($CanBeAppliedCurrentTermClub);

### Get the number of Whole Year Club can be applied
$CanBeAppliedWholeYearClub = $libenroll->getGroupsAvailableForApplyInSpecifiedSemester($canBeAppliedClubEnrolGroupIdAry,"WholeYear");
$numOfCanBeAppliedWholeYearClub = count($CanBeAppliedWholeYearClub);

### Get the data of all clubs
$clubInfoAry = $libenroll->Get_All_Club_Info();
$clubEnrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, 'EnrolGroupID');
unset($clubInfoAry);

### Get the meeting dates of all clubs
$clubMeetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($clubEnrolGroupIdAry);
$clubMeetingDateAssoAry = BuildMultiKeyAssoc($clubMeetingDateInfoAry, 'EnrolGroupID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($clubMeetingDateInfoAry);

### Get the rejected club
$rejectedClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($ApplicantID, $enrolGroupIdAry='', $enrolConfigAry['EnrolmentRecordStatus_Rejected'], $sel_category);
$numOfRejectedClub = count($rejectedClubInfoAry);

### Get the waiting club
$waitingClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($ApplicantID, $enrolGroupIdAry='', $enrolConfigAry['EnrolmentRecordStatus_Waiting'], $sel_category);
$numOfWaitingClub = count($waitingClubInfoAry);

### Get the enrolled Club student info
$enrolledClubInfoAry = $libenroll->Get_Student_Club_Info($ApplicantID);
$enrolledClubAssoAry = BuildMultiKeyAssoc($enrolledClubInfoAry, 'EnrolGroupID');
$numOfStudentEnrolledClub = count($enrolledClubAssoAry);
unset($enrolledClubInfoAry);

### Get student club selection array -> include the available clubs and the student's waiting clubs (student may waiting for some clubs which are not available to apply)
$studentClubSelectionEnrolGroupIdAry = array_values(array_unique(array_merge($canBeAppliedClubEnrolGroupIdAry, Get_Array_By_Key($waitingClubInfoAry, 'EnrolGroupID'))));


### Get the max min requirements for this student
$targetTermNumAry = array();
$targetSemester = $libenroll->targetSemester;
//2013-0902-1015-36073
if ($targetSemester > 0) {
	$termObj = new academic_year_term($targetSemester);
	$targetTermNumAry[] = $termObj->Get_Term_Number();		// target term
}
$targetTermNumAry[] = 0;								// whole year


//$minMaxAry[$_termNum]['ApplyMin' / 'ApplyMax' / 'EnrollMax'] = data
$minMaxAry = $libenroll->getMinMaxOfStudent($ApplicantID, $enrolConfigAry['Club'], $sel_category, $targetTermNumAry);
if($plugin['SIS_eEnrollment']){
	$minMaxAry = $libenroll->getMinMaxOfStudent($ApplicantID, $enrolConfigAry['Club'], $sel_category, array($libenroll->targetRound));
	$minMaxAry = array_values($minMaxAry);
}


$termBasedFormChecking = false;
if (isset($minMaxAry[-1])) {
	// form-based settings
	$minNumOfApp = $minMaxAry[-1]['ApplyMin'];
	$maxNumOfApp = $minMaxAry[-1]['ApplyMax'];
	$maxNumOfEnrol = $minMaxAry[-1]['EnrollMax'];	
	if($sys_custom['eEnrolment']['EnableEnrollMin']){
		$minNumOfEnrol = $minMaxAry[-1]['EnrollMin'];
	}
}
else if ($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase') {
//	$minNumOfApp = $minMaxAry[0];
//	$maxNumOfApp = $minMaxAry[1];
//	$maxNumOfEnrol = $minMaxAry[2];
	$minNumOfApp = $minMaxAry[0]['ApplyMin'];
	$maxNumOfApp = $minMaxAry[0]['ApplyMax'];
	$maxNumOfEnrol = $minMaxAry[0]['EnrollMax'];
	if($sys_custom['eEnrolment']['EnableEnrollMin']){
		$minNumOfEnrol = $minMaxAry[0]['EnrollMin'];
	}
}
else if ($libenroll->quotaSettingsType=='TermBase') {
	
	$termBasedFormChecking = true;
	$minNumOfApp = array_sum(Get_Array_By_Key(array_values($minMaxAry), 'ApplyMin'));
	
	// #T102993  	
	// check if the Max. no. of clubs for a student to apply is greater that the num of Availbale Club in year and term respectively
	$numOfCanBeAppliedClub = 0;
	foreach($minMaxAry as $key => $arr ){
		if($key == 0){
			// whole year
			if($arr['ApplyMax'] > $numOfCanBeAppliedWholeYearClub){
				$minMaxAry[$key]['ApplyMax'] = $numOfCanBeAppliedWholeYearClub;
				//in category selection mode
				if ( isset($sel_category) ){
					$numOfCanBeAppliedClub = $numOfCanBeAppliedWholeYearClub + $numOfCanBeAppliedClub;
				}
			}else{
				//in category selection mode
				if ( isset($sel_category) ){
					$numOfCanBeAppliedClub = $minMaxAry[$key]['ApplyMax'] + $numOfCanBeAppliedClub;
				}
				//if whole year club can be applied is nolimited
				if(  $minMaxAry[$key]['ApplyMax'] == 0 ){
					$numOfCanBeAppliedClub = $numOfCanBeAppliedWholeYearClub + $numOfCanBeAppliedClub;
				}
			}
		}else{
			//term
			if($arr['ApplyMax'] > $numOfCanBeAppliedCurrentTermClub){
				
				$minMaxAry[$key]['ApplyMax'] = $numOfCanBeAppliedCurrentTermClub;
				//in category selection mode
				if ( isset($sel_category) ){
					$numOfCanBeAppliedClub = $numOfCanBeAppliedCurrentTermClub + $numOfCanBeAppliedClub;
				}
			}
			else{
				//in category selection mode
				if ( isset($sel_category) ){
					$numOfCanBeAppliedClub = $minMaxAry[$key]['ApplyMax'] + $numOfCanBeAppliedClub;
				}
				//if current term club can be applied is nolimited
				if(  $minMaxAry[$key]['ApplyMax'] == 0 ){
					$numOfCanBeAppliedClub = $numOfCanBeAppliedCurrentTermClub + $numOfCanBeAppliedClub;
				}
							
			}
		}
		
	}
	
	$tempApplyMaxAry = Get_Array_By_Key(array_values($minMaxAry), 'ApplyMax');
	if(in_array('0',$tempApplyMaxAry)){
		$maxNumOfApp = '0';
	}
	else{
		$maxNumOfApp = array_sum($tempApplyMaxAry);
	} 
	
	$maxNumOfEnrol = array_sum(Get_Array_By_Key(array_values($minMaxAry), 'EnrollMax'));
	if($sys_custom['eEnrolment']['EnableEnrollMin']){
		$minNumOfEnrol = array_sum(Get_Array_By_Key(array_values($minMaxAry), 'EnrollMin'));
	}
	// #T102993  here by omas temp fix some problem , but not totally solve the problem
	// if max can apply < can 

	if($maxNumOfApp < $maxNumOfEnrol){
		$maxNumOfEnrol = $maxNumOfApp;
	}
	
	$html['jsApplyMinVarInitialize'] = '';
	$html['jsApplyMinVarInitialize'] .= 'var applyMinAry = new Array();'."\n";
	foreach((array)$minMaxAry as $_termNum => $_quotaInfoAry) {
		$html['jsApplyMinVarInitialize'] .= 'applyMinAry['.$_termNum.'] = \''.$_quotaInfoAry['ApplyMin'].'\';'."\n";
	}
	
	$html['jsApplyMaxVarInitialize'] = '';
	$html['jsApplyMaxVarInitialize'] .= 'var applyMaxAry = new Array();'."\n";
	foreach((array)$minMaxAry as $_termNum => $_quotaInfoAry) {
		$html['jsApplyMaxVarInitialize'] .= 'applyMaxAry['.$_termNum.'] = \''.$_quotaInfoAry['ApplyMax'].'\';'."\n";
	}
	
	$termAry = $libfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());
	$numOfTerm = count($termAry);

	$x = '';
	$x .= 'var applyMinWarningAry = new Array();'."\n";
	foreach((array)$minMaxAry as $_termNum => $_quotaInfoAry) {
		
		if ($_termNum == 0) {
			$x .= 'applyMinWarningAry[0] = \''.str_replace('<!--TermName-->', $eEnrollment['WholeYear'], str_replace('<!--NumOfClub-->', $minMaxAry[0]['ApplyMin'], $Lang['eEnrolment']['jaWarning']['TermMinimumApplication'])).'\';'."\n";
		}
		else {
			$_termNameEn = $termAry[$_termNum-1]['YearTermNameEN'];
			$_termNameCh = $termAry[$_termNum-1]['YearTermNameB5'];
			$_termNameName = Get_Lang_Selection($_termNameCh, $_termNameEn);
			
			$_applyMin = $minMaxAry[$_termNum]['ApplyMin'];
			
			$_warningMsg = $Lang['eEnrolment']['jaWarning']['TermMinimumApplication'];
			$_warningMsg = str_replace('<!--TermName-->', $_termNameName, $_warningMsg);
			$_warningMsg = str_replace('<!--NumOfClub-->', $_applyMin, $_warningMsg);
			
			$x .= 'applyMinWarningAry['.$_termNum.'] = \''.$_warningMsg.'\';'."\n";
		}
	}
	$html['jsApplyMinWarningAry'] = $x;
	
	
	$x = '';
	$x .= 'var applyMaxWarningAry = new Array();'."\n";
	foreach((array)$minMaxAry as $_termNum => $_quotaInfoAry) {
		
		if ($_termNum == 0) {
			$x .= 'applyMaxWarningAry[0] = \''.str_replace('<!--TermName-->', $eEnrollment['WholeYear'], str_replace('<!--NumOfClub-->', $minMaxAry[0]['ApplyMax'], $Lang['eEnrolment']['jaWarning']['TermMaximumApplication'])).'\';'."\n";
		}
		else {
			$_termNameEn = $termAry[$_termNum-1]['YearTermNameEN'];
			$_termNameCh = $termAry[$_termNum-1]['YearTermNameB5'];
			$_termNameName = Get_Lang_Selection($_termNameCh, $_termNameEn);
			
			$_applyMin = $minMaxAry[$_termNum]['ApplyMax'];
			
			$_warningMsg = $Lang['eEnrolment']['jaWarning']['TermMaximumApplication'];
			$_warningMsg = str_replace('<!--TermName-->', $_termNameName, $_warningMsg);
			$_warningMsg = str_replace('<!--NumOfClub-->', $_applyMin, $_warningMsg);
			
			$x .= 'applyMaxWarningAry['.$_termNum.'] = \''.$_warningMsg.'\';'."\n";
		}
	}
	$html['jsApplyMaxWarningAry'] = $x;
}

### If there is no upper limit for the number of applied clubs or there is not enough clubs to satisfy the maximum number of application
### => the maximum number of application equals to the number of clubs which can be applied
if ($maxNumOfApp == 0 || $maxNumOfApp > $numOfCanBeAppliedClub) {
	// H56742
//	$maxNumOfApp = $numOfCanBeAppliedClub;
//	$maxNumOfEnrol = $numOfCanBeAppliedClub;
 	$maxNumOfApp = $numOfCanBeAppliedClub + $numOfStudentEnrolledClub;
}

if(!$sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] && ($maxNumOfApp == 0 || $maxNumOfEnrol > $numOfCanBeAppliedClub)){
    $maxNumOfEnrol = $numOfCanBeAppliedClub + $numOfStudentEnrolledClub;
}

### If the school requires the student to apply at least 4 clubs but there are only 2 clubs available, auto adjust the minimum number of application to 2 clubs.
if ($maxNumOfApp < $minNumOfApp || $sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] && $minNumOfEnrol <= 0) {
    $minNumOfApp = $maxNumOfApp;
}


//$maxNumOfClubCanBeEnrolled = ($libenroll->EnrollMax == 0)? $numOfCanBeAppliedClub : $maxNumOfEnrol;
$maxNumOfClubCanBeEnrolled = ($maxNumOfEnrol == 0)? $numOfCanBeAppliedClub : $maxNumOfEnrol;

if($sys_custom['eEnrolment']['Application']['MinApply']){
	$maxNumOfClubCanBeEnrolled = 9;
}

### Get the maxwant of the student
// Use the value from the selection if there is a value, otherwise, use the saved value in the system
if($sys_custom['eEnrolment']['maxWantDefaultZero']){
	$maxwant = ($libenroll->maxStudentWanted=='') ? 0 : $libenroll->maxStudentWanted;
}
else{
	// Normal workflow !!!!!
	$maxwant = ($maxwant=='') ? $libenroll->maxStudentWanted : $maxwant;
	// If the student has not saved the maxwant in the system, by default, use the maximum number of clubs a student can be enrolled
	if ($maxwant == '') {
		//$maxwant = $libenroll->EnrollMax;
		$maxwant = $maxNumOfClubCanBeEnrolled;
	}
}
// If there are not enough clubs to fulfill student's maxwant, change the student maxwant to the maximum number of clubs a student can be enrolled
if ($maxwant > $maxNumOfEnrol && $maxNumOfEnrol != 0) {
		//$maxwant = $libenroll->EnrollMax;
		$maxwant = $maxNumOfClubCanBeEnrolled;
}

### Get the number of display row
$appliedClubInfoAry = $libenroll->applications;
if($plugin['SIS_eEnrollment']){
	$appliedClubInfoAry = $libenroll->getRoundApplication();
}

$appliedClubEnrolGroupIdAry = Get_Array_By_Key($appliedClubInfoAry, 0);
$appliedClubAssoAry = BuildMultiKeyAssoc($appliedClubInfoAry, 3); // 3 => choice , $application[$choice] = array($id,$status,$name,$choice,$applyDate);
$numOfAppliedClub = count($appliedClubAssoAry);
unset($appliedClubInfoAry);


# Get the maximum priority of the chosen Clubs
if ($numOfAppliedClub > 0) {
	$maxChoice = max(array_keys($appliedClubAssoAry));
}
else {
	// no application yet
	$maxChoice = 0;
}
//$maxNumOfApp=2;
//$maxChoice=2;
# if after the enrolment period, show all the enrol clubs info even number of clubs applied is greater then the school limit

if ($withinEnrolStage){
	$maxNumOfDisplayRow = $maxNumOfApp;
}
else {
	$maxNumOfDisplayRow = max($maxNumOfApp, $maxChoice);
}

if($sys_custom['eEnrolment']['Application']['MinApply']){
	$maxNumOfDisplayRow = 9;
}

# disable all selection box if the number of clubs enrolled by the student reached the school limit
//2014-0912-1002-36194
//$numOfApprovedClub = $libenroll->GET_STUDENT_NUMBER_OF_APPROVED_CLUB($ApplicantID, $sel_category);
$numOfApprovedClub = $libenroll->GET_STUDENT_NUMBER_OF_ENROLLED_CLUB($ApplicantID, $sel_category);


### Determine selection disable or not
$disableSelection = ($withinEnrolStage)? false : true;

//if ($withinEnrolStage && $numOfApprovedClub >= $libenroll->EnrollMax && $libenroll->EnrollMax!=0) {
if ($withinEnrolStage && $numOfApprovedClub >= $maxNumOfClubCanBeEnrolled && $maxNumOfClubCanBeEnrolled!=0) {
	$disableSelection = true;
	$warn_no_more_enrolment = $eEnrollment['warn_no_more_enrolment'];
}

### If ClubTargetApplicant (Apply to individual club) is not set, use Club Setting (Apply to all clubs) to control if club option list is enabled or not 
if (!$sys_custom['eEnrolment']['ClubTargetApplicant'])
{
	if ($libenroll->enrollPersontype == $enrolConfigAry['Settings_PersonType']['Student'] && !$LibUser->IsStudent()) {
		$disableSelection = true;
	}
	else if ($libenroll->enrollPersontype == $enrolConfigAry['Settings_PersonType']['Parent'] && !$LibUser->IsParent()) {
		$disableSelection = true;
	}
}

####################################################
########### Application Settings [End] #############
####################################################


####################################################
############## Warning Msg [Start] #################
####################################################
$warningMsgAry = array();
$warningMsgAry[] = $eEnrollment['front']['time_conflict_warning'];
if ($warn_no_more_enrolment != '') {
	$warningMsgAry[] = $warn_no_more_enrolment;
}

$h_warningMsg = '';
if ($libenroll->oneEnrol == 1) {
	$h_warningMsg .= $linterface->Get_Warning_Message_Box('', $warningMsgAry);
	$h_warningMsg .= '<br style="clear:both;" />';
}
####################################################
############### Warning Msg [End] ##################
####################################################


####################################################
###### Children Selection For Parent [Start] #######
####################################################
$h_childSel = '';
if ($LibUser->IsParent()) {
	$h_childSel .= '<div>'."\n";
		$h_childSel .= '<table class="form_table_v30">'."\n";
			### Children Selection
			$h_childSel .= '<tr>'."\n";
				$h_childSel .= '<td class="field_title">'.$Lang['eEnrolment']['Children'].'</td>'."\n";
				$h_childSel .= '<td>'.$h_childrenSelection.'</td>'."\n";
			$h_childSel .= '</tr>'."\n";
		$h_childSel .= '</table>'."\n";
	$h_childSel .= '</div>'."\n";
	$h_childSel .= '<br />'."\n";
}
####################################################
####### Children Selection For Parent [End] ########
####################################################

####################################################
############ Header Info Table [Start] #############
####################################################
$appStart = $libenroll->AppStart." ".$libenroll->AppStartHour.":".$libenroll->AppStartMin;
$appEnd = $libenroll->AppEnd." ".$libenroll->AppEndHour.":".$libenroll->AppEndMin;
if ($libenroll->mode != 0 && $libenroll->AppStart != '' && $libenroll->AppEnd != '') {
	$appPeriodColor = ($withinEnrolStage)? 'blue' : 'red';
	$appPeriodText = $appStart.' '.$eEnrollment['to'].' '.$appEnd;
}
else {
	$appPeriodColor = '';
	$appPeriodText = $Lang['General']['EmptySymbol'];
}
$descriptionText = ($libenroll->mode == 0 || trim($libenroll->Description)=='')? $Lang['General']['EmptySymbol'] : nl2br($libenroll->Description);
$tieBreakRuleText = ($libenroll->tiebreak == 1)? $i_ClubsEnrollment_AppTime : $i_ClubsEnrollment_Random;
$minNumOfAppText = ($minNumOfApp==='' || $minNumOfApp===null)? $Lang['General']['EmptySymbol'] : $minNumOfApp;

// 2012-0905-0925-06073
//$h_maxNoOfClubWantToJoinSelection = $linterface->Get_Number_Selection('maxwant', $minNumOfApp, $maxNumOfClubCanBeEnrolled, $maxwant, 'changedMaxWantSelection(this.value);', $noFirst=1, $isAll=0, $FirstTitle='', $disableSelection);
if($sys_custom['eEnrollment']['useMinApplyNum']){
    //use min enrol as first value in selection
    $h_maxNoOfClubWantToJoinSelection = $linterface->Get_Number_Selection('maxwant', ($minNumOfApp > $maxNumOfClubCanBeEnrolled)? $minNumOfEnrol : $minNumOfApp, $maxNumOfClubCanBeEnrolled, $maxwant, 'changedMaxWantSelection(this.value);', $noFirst=1, $isAll=0, $FirstTitle='');
}else{
    $h_maxNoOfClubWantToJoinSelection = $linterface->Get_Number_Selection('maxwant', $sys_custom['eEnrolment']['EnableEnrollMin']?$minNumOfEnrol:0, $maxNumOfClubCanBeEnrolled, $maxwant, 'changedMaxWantSelection(this.value);', $noFirst=1, $isAll=0, $FirstTitle='', $disableSelection);
}
# Category-related settings
$categorySelection = '';
if($libenroll->UseCategorySetting) {
	if ($sys_custom['eEnrolment']['CategoryType']) {
		$categoryTypeID = array(1);	// Club
	}
	else {
		$categoryTypeID = 0;
	}
	
	$categorySelection .= $libenroll->GET_CATEGORY_SEL($sel_category, 1, "reloadPage();", $Lang['eEnrolment']['PleaseSelectCategory'], 0, $categoryTypeID);
}

$h_headerInfo = '';
$h_headerInfo .= '<div style="float:left;">'.$linterface->GET_NAVIGATION2_IP25($Lang['eEnrolment']['EnrollmentInformation']).'</div>'."\n";
$h_headerInfo .= '<br style="clear:both;">'."\n";
$h_headerInfo .= '<div>'."\n";
	$h_headerInfo .= '<table class="form_table_v30">'."\n";
		### Application Period
		$h_headerInfo .= '<tr>'."\n";
			$h_headerInfo .= '<td class="field_title">'.$eEnrollment['app_period'].'</td>'."\n";
			$h_headerInfo .= '<td><span style="color:'.$appPeriodColor.';">'.$appPeriodText.'</span></td>'."\n";
		$h_headerInfo .= '</tr>'."\n";
		
		### Application Instruction
		$h_headerInfo .= '<tr>'."\n";
			$h_headerInfo .= '<td class="field_title">'.$Lang['General']['Instruction'].'</td>'."\n";
			$h_headerInfo .= '<td>'.$descriptionText.'</td>'."\n";
		$h_headerInfo .= '</tr>'."\n";
		
		### Lottery Priority
		$h_headerInfo .= '<tr>'."\n";
			$h_headerInfo .= '<td class="field_title">'.$i_ClubsEnrollment_TieBreakRule.'</td>'."\n";
			$h_headerInfo .= '<td>'.$tieBreakRuleText.'</td>'."\n";
		$h_headerInfo .= '</tr>'."\n";
		
		### Minimum Need to apply
		if ($libenroll->quotaSettingsType=='YearBase' || $libenroll->quotaSettingsType=='RoundBase') {
			$h_headerInfo .= '<tr>'."\n";
				$h_headerInfo .= '<td class="field_title">'.(($sys_custom['eEnrolment']['Application']['MinApply'] || $sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] && $minNumOfEnrol <= 0)?$eEnrollment['NoOfClubMustBeApply']:$eEnrollment['MinNoOfClubNeedToApply']).'</td>'."\n";
				$h_headerInfo .= '<td>'.$minNumOfAppText.'</td>'."\n";
			$h_headerInfo .= '</tr>'."\n";
		}
		else if ($libenroll->quotaSettingsType=='TermBase') {
			$h_headerInfo .= '<tr>'."\n";
				$h_headerInfo .= '<td class="field_title">'.(($sys_custom['eEnrolment']['Application']['MinApply'] || $sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] && $minNumOfEnrol <= 0)?$eEnrollment['NoOfClubMustBeApply']:$eEnrollment['MinNoOfClubNeedToApply']).'</td>'."\n";
				$h_headerInfo .= '<td>'.$libenroll_ui->Get_Enrollment_Application_Quota_Settings_Table($enrolConfigAry['Club'], $CategoryID, $ViewMode=true, $QuotaTypeAry=array('ApplyMin'), $libenroll->targetSemester, $WithQuotaTypeCol=false, $ApplicantID).'</td>'."\n";
			$h_headerInfo .= '</tr>'."\n";
		}
		
		if($libenroll->UseCategorySetting) {
			$h_headerInfo .= '<tr>'."\n";
				$h_headerInfo .= '<td class="field_title">'.$Lang['eEnrolment']['ClubCategory'].'</td>'."\n";
				$h_headerInfo .= '<td>'.$categorySelection.'</td>'."\n";
			$h_headerInfo .= '</tr>'."\n";
		}
		
		### Maximum number of club student want to join
		if($sys_custom['eEnrolment']['Application']['MinApply']){
			$h_headerInfo .= '<tr style="visibility:hidden">'."\n";
		}
		else{
			$h_headerInfo .= '<tr>'."\n";
		}
			$h_headerInfo .= '<td class="field_title">'.$eEnrollment['MaxNoOfClubWantToParticipate'].'</td>'."\n";
			$h_headerInfo .= '<td>'."\n";
				$h_headerInfo .= '<span>'.$h_maxNoOfClubWantToJoinSelection.'</span>'."\n";
				$h_headerInfo .= '<br />'."\n";
				$h_headerInfo .= '<span id="maxWantZeroWarningSpan" class="tabletextrequire" style="display:none;">'.$Lang['eEnrolment']['MaxWantZeroWarningMsg'].'</span>';
			$h_headerInfo .= '</td>'."\n";
		$h_headerInfo .= '</tr>'."\n";
		
// 	$h_headerInfo .= '</table>'."\n";
// $h_headerInfo .= '</div>'."\n";
		
####################################################
############# Header Info Table [End] ##############
####################################################


####################################################
######## Student Extra Info Table [Start] ##########
####################################################
$h_studentExtraInfo = '';
if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
	$h_studentExtraInfo .= $libenroll_ui->Get_Student_Extra_Info_UI($enrolConfigAry['Club'], $ApplicantID);
	$h_studentExtraInfo .= '<br style="clear:both;" />';
}
####################################################
######### Student Extra Info Table [End] ###########
####################################################

if($sys_custom['eEnrollment']['setTargetForm']){
    $form = $LibUser->Get_User_Studying_Form($_SESSION["UserID"], $AcademicYearID);
    $classlvl = $libenroll->settingTargetForm;
    $classlvlArr = explode(",",$classlvl);
    
    // check student in target form
    if(in_array($form[0]["YearName"], $classlvlArr)){
        $isTarget = 1;
    }else{
        $isTarget = 0;
        
        if($classlvl == ""){
            $NotTargetMsg = "<p>".$Lang['eEnrolment']['SignUpWarnMsg']."<p>";
        }else{
            $NotTargetMsg = "<p>".str_replace(array("[=TargetForm=]", ","), array($classlvl, ", "), $Lang['eEnrolment']['SignUpTargetForm'])."</p>";
        }
        
        $instructionBox = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $NotTargetMsg);
        $h_headerInfo = '<table class="form_table_v30">'."\n";
        $h_headerInfo .= '<tr>'.$instructionBox.'</tr>';
        
        $h_warningMsg = "";
    }
    
    if($withinProcessStage){
        $instructionBox = "";
    }
}

####################################################
###### Apply and Rejected Club Table [Start] #######
####################################################
$h_withinProcessStageWarning = '';
$h_applyClubTable = '';
$h_rejectedClubTable = '';
if ($withinProcessStage) {
	$h_withinProcessStageWarning .= '<span class="tabletextrequire">'.$libenroll_ui->Get_Processing_Enrolment_Message().'</span>';
}
else if($libenroll->UseCategorySetting && $sel_category=='') {
	// do nth
}
else {
	### Club Application
	$numOfCol = 0;
	$h_applyClubTable .= '<div style="float:left;">'.$linterface->GET_NAVIGATION2($Lang['eEnrolment']['ClubApplication']).'</div>';
	$h_applyClubTable .= '<br style="clear:both;" />'."\n";
	$h_applyClubTable .= '<div>'."\n";
		$h_applyClubTable .= '<table class="common_table_list_v30 edit_table_list_v30">'."\n";
			$h_applyClubTable .= '<thead>'."\n";
				$h_applyClubTable .= '<tr>'."\n";
					$h_applyClubTable .= '<th style="width:50px;">'.$Lang['eEnrolment']['Priority'].'</th>'."\n";
					$numOfCol++;
					
					//$h_applyClubTable .= '<th>'.$eEnrollment['club_name'].'</th>'."\n";
					//$numOfCol++;
					
					//$h_applyClubTable .= '<th style="width:25%;">'."\n";
					$h_applyClubTable .= '<th>'."\n";
						$h_applyClubTable .= $eEnrollment['club_name'].' / '.$eEnrollment['add_activity']['act_time'];
						$h_applyClubTable .= ' <span id="viewAllDateBtnSpan" style="display:none;">[<a class="tablelink" href="javascript:void(0);" onclick="showAllDateDetails();">'.$Lang['eEnrolment']['ViewAll'].'</a>]</span>';
						$h_applyClubTable .= ' <span id="hideAllDateBtnSpan">[<a class="tablelink" href="javascript:void(0);" onclick="hideAllDateDetails();">'.$Lang['eEnrolment']['HideAll'].'</a>]</span>';
					$h_applyClubTable .= '</th>'."\n";
					$numOfCol++;
					
					$h_applyClubTable .= '<th style="width:7%;">'.$eEnrollment['PaymentAmount'].'</th>'."\n";
					$numOfCol++;
					
					$h_applyClubTable .= '<th style="width:7%;">'.$eEnrollment['status'].'</th>'."\n";
					$numOfCol++;
					
					if ($libenroll->tiebreak == 1) {
						$h_applyClubTable .= '<th style="width:7%;">'.$eEnrollment['position'].'</th>'."\n";
						$numOfCol++;
					}
					
					$h_applyClubTable .= '<th style="width:18%;">'.$i_ClubsEnrollment_LastSubmissionTime.'</th>'."\n";
					$numOfCol++;
					
					$h_applyClubTable .= '<th style="width:10%;">'.$eEnrollment['role'].'</th>'."\n";
					$numOfCol++;
					
					$h_applyClubTable .= '<th style="width:10%;">'.$eEnrollment['attendence_record'].'</th>'."\n";
					$numOfCol++;
					
					if ($sys_custom['eEnrollment']['RequestStudentInputClubRank']) {
						$h_applyClubTable .= '<th style="width:8%;">'.$eEnrollment['ClubEventRank'].'</th>'."\n";
						$numOfCol++;
					}
					
					if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
						$h_applyClubTable .= '<th style="width:20%;">'.$Lang['eEnrolment']['ApplicantInformation'].'</th>'."\n";
						$numOfCol++;
					}
					if($LibUser->IsStudent()&&$sys_custom['eEnrolment']['ReplySlip']){
						$h_applyClubTable .= '<th style="width:10%;">'.$eEnrollment['replySlip']['replySlip'].'</th>'."\n";
					}
					$numOfCol++;
					
				$h_applyClubTable .= '</tr>'."\n";
			$h_applyClubTable .= '</thead>'."\n";
			
			$h_applyClubTable .= '<tbody>'."\n";
				if ($maxNumOfDisplayRow==0) {
					$h_applyClubTable .= '<tr><td colspan="'.$numOfCol.'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
				}
				else {
				    $PointTotal = 0;
			
					for ($i=0; $i<$maxNumOfDisplayRow; $i++) {
						$_choiceNumber = $i + 1;
						
						### Club Name
						$_appliedClubInfoAry = $appliedClubAssoAry[$_choiceNumber];
						if (empty($_appliedClubInfoAry)) {
							// not applied
							$_appliedEnrolGroupId = '';
							$_appliedStatus = '';
							$_appliedClubName = '';
							$_appliedDateTime = '';
							$_applicantInfo = '';
						}
						else {
							// applied
							$_appliedEnrolGroupId = $_appliedClubInfoAry[0];
							$_appliedStatus = $_appliedClubInfoAry[1];
							$_appliedClubName = $clubInfoAssoAry[$_appliedEnrolGroupId]['TitleWithSemester'];
							$_appliedDateTime = $_appliedClubInfoAry[4];
							$_applicantInfo = $_appliedClubInfoAry[5];
						}
						$_roleTitle = $enrolledClubAssoAry[$_appliedEnrolGroupId]['RoleTitle'];
						
						
						$rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($_appliedEnrolGroupId,$_roleTitle);
						$thisRolePoint = "";
						if($rolePointArr[0]['Point'] != null){
						    $thisRolePoint= $rolePointArr[0]['Point'];
						}	
						
						$PointTotal += $thisRolePoint;
						$_id = 'choice'.$_choiceNumber;
						$_name = 'choice'.$_choiceNumber;
						
						$_categoryName = 'CategoryChoice'.$_choiceNumber;
						$_clubNameDisplay = '';
						$categoryID = $clubInfoAssoAry[$_appliedEnrolGroupId]['GroupCategory'];
						if ($_appliedStatus == $enrolConfigAry['EnrolmentRecordStatus_Approved']) {
							// approved => display club name
						    $categoryName = $clubInfoAssoAry[$_appliedEnrolGroupId]['CategoryName']==''? '': ' ('.$clubInfoAssoAry[$_appliedEnrolGroupId]['CategoryName'].')';
						    $canEnrolledCategoryIDAry[] = $categoryID;
						    $_clubNameDisplay .= Get_Lang_Selection($clubInfoAssoAry[$_appliedEnrolGroupId]['TitleChineseWithSemester'], $clubInfoAssoAry[$_appliedEnrolGroupId]['TitleWithSemester']).$categoryName;
							$_clubNameDisplay .= '<input type="hidden" id="'.$_id.'" name="'.$_name.'" value="'.$_appliedEnrolGroupId.'" class="selectedClubField" />';
							$_clubNameDisplay .= '<input type="hidden" id="'.$_categoryName.'" name="'.$_categoryName.'" value="'.$categoryID.'" class="selectedClubCategory" />';
							
						}
						else {
							// waiting or not applied for this priority yet
							//$_displayEnrolGroupIdAry = array_values(array_diff($canBeAppliedClubEnrolGroupIdAry, $appliedClubEnrolGroupIdAry));
							//$_displayEnrolGroupIdAry[] = $_appliedEnrolGroupId;
							/////>>>>>
							if ($sys_custom['eEnrolment']['ClubTargetApplicant']) {
								if (($clubInfoAssoAry[$_appliedEnrolGroupId]['ApplyUserType'] == $currentUserType) || (!$_appliedEnrolGroupId))
								{
									$_clubNameDisplay .= $libenroll_ui->Get_Club_Selection($_id, $_name, $_appliedEnrolGroupId, $sel_category, $IsAll=0, $NoFirst=0, 'changedClubSelection(this.id, this.value, '.$_choiceNumber.');', $AcademicYearID='', $studentClubSelectionEnrolGroupIdAry, $disableSelection, 'clubSel selectedClubField',false,$currentUserType);
								}
								else
								{
									$_clubNameDisplay .= $libenroll_ui->Get_Club_Selection("Disp".$_id, $_name, $_appliedEnrolGroupId, $sel_category, $IsAll=0, $NoFirst=0, 'changedClubSelection(this.id, this.value, '.$_choiceNumber.');', $AcademicYearID='', $studentClubSelectionEnrolGroupIdAry, true, '',false);
									$_clubNameDisplay .= '<input type="hidden" id="'.$_id.'" name="'.$_name.'" value="'.$_appliedEnrolGroupId.'" class="selectedClubField" />';
								}								
							}
							else
							{
								$_clubNameDisplay .= $libenroll_ui->Get_Club_Selection($_id, $_name, $_appliedEnrolGroupId, $sel_category, $IsAll=0, $NoFirst=0, 'changedClubSelection(this.id, this.value, '.$_choiceNumber.');', $AcademicYearID='', $studentClubSelectionEnrolGroupIdAry, $disableSelection, 'clubSel selectedClubField',false);								
							
// 								$_clubNameDisplay .= '<input type="hidden" id="'.$_categoryName.'" name="'.$_categoryName.'" value="'.$categoryID.'" class="selectedClubCategory" />';
								
							}
							/////<<<<<
							
						}
						if ($_appliedEnrolGroupId == '') {
							// do nth
							$_displayDetailsIcon = 'display:none;';
						}
						else {
							 $_displayDetailsIcon = 'display:inline;';
						}
						$_clubNameDisplay .= '&nbsp;';
						$_clubNameDisplay .= '<a id="viewClubDetailsLink_'.$_choiceNumber.'" href="javascript:void(0)" onclick="viewClubDetails('.$_choiceNumber.');" style="'.$_displayDetailsIcon.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" align="absmiddle" border="0"></a>';		
						
						### Meeting Date
						$_meetingDateDisplay = $libenroll_ui->Get_Student_Application_Meeting_Date_Display($ApplicantID, $_appliedEnrolGroupId, $_choiceNumber, $clubInfoAssoAry[$_appliedEnrolGroupId], $clubMeetingDateAssoAry[$_appliedEnrolGroupId]);
						
						### Fee
						$_feeDisplay = $libenroll_ui->Get_Student_Application_Fee_Display($ApplicantID, $_appliedEnrolGroupId, $clubInfoAssoAry[$_appliedEnrolGroupId]);
						
						### Status
						$_statusDisplay = $libenroll_ui->Get_Student_Application_Status_Display($ApplicantID, $_appliedEnrolGroupId, $_appliedStatus);
						
						### Position
						$_positionDisplay = $libenroll_ui->Get_Student_Application_Position_Display($ApplicantID, $_appliedEnrolGroupId);
						
						### Application Date Time
						$_appliedDateTimeDisplay = $libenroll_ui->Get_Student_Application_Application_Date_Time_Display($ApplicantID, $_appliedEnrolGroupId, $_appliedDateTime);
						
						### Role
						$_roleDisplay = $libenroll_ui->Get_Student_Application_Role_Display($ApplicantID, $_appliedEnrolGroupId, $_roleTitle);
						
						### Attendance
						$_attendanceDisplay = $libenroll_ui->Get_Student_Application_Attendance_Display($ApplicantID, $_appliedEnrolGroupId, $_choiceNumber, $_appliedStatus);
						
						### Rank
						$_rankDisplay = '';
						if($sys_custom['eEnrollment']['RequestStudentInputClubRank']) {
							$_rankRequired = $clubInfoAssoAry[$_appliedEnrolGroupId]['RankRequired'];
							if ($_rankRequired) {
								$_disabled = ($disableSelection)? 'disabled' : '';
								$_rankDisplay .= '<input id="SessionRank'.$_choiceNumber.'" name="SessionRank'.$_choiceNumber.'" size="2" maxlength="2" '.$_disabled.' />';
							}
							else {
								$_rankDisplay .= $Lang['General']['EmptySymbol'];
							}
						}
						
						### Applicant Info
						$_applicantInfoDisplay = '';
						if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
							$_applicantInfoDisplay = $libenroll_ui->Get_Student_Application_Applicant_Info_Display($ApplicantID, $_appliedEnrolGroupId, $_choiceNumber, $disableSelection);
						}
						
						#### SIS cust #J84059 ####
						if($sys_custom['eEnrolment']['hideNonApprovedRow'] && !$withinProcessStage && !$withinEnrolStage){
							if($_appliedStatus == $enrolConfigAry['EnrolmentRecordStatus_Approved']){
								$class = ' class="approvedRow"';
							}
							else{
								$class = ' class="hideRow"';								
							}
						}
						else{
							$class = '';
						}
						
						### ReplySlip #
						if($sys_custom['eEnrolment']['ReplySlip']){
							$replySlipDisplay = '--';
							if($_appliedEnrolGroupId !='' ){
								$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$_appliedEnrolGroupId);
									
								if(!empty($ReplySlipRelation)){
									$replySlipDisplay = '<a href="javasrcipt:void(0);" onclick="OnloadReplySlip('.$_appliedEnrolGroupId.')">'.'<img src="/images/2009a/eOffice/icon_resultview.gif" border="0" align="absmiddle">'.'</a>';
								}
							}
						}
						
						### Row display
						$h_applyClubTable .= '<tr'.$class.'>'."\n";
							$h_applyClubTable .= '<td rowspan="2">'.$_choiceNumber.'</td>'."\n";
							$h_applyClubTable .= '<td colspan="'.($numOfCol - 1).'">'.$_clubNameDisplay.'</td>'."\n";
						$h_applyClubTable .= '</tr>'."\n";
						$h_applyClubTable .= '<tr'.$class.'>'."\n";
							$h_applyClubTable .= '<td><div id="meetingDateDiv_'.$_choiceNumber.'">'.$_meetingDateDisplay.'</div></td>'."\n";
							$h_applyClubTable .= '<td><div id="feeDiv_'.$_choiceNumber.'">'.$_feeDisplay.'</div></td>'."\n";
							$h_applyClubTable .= '<td>'."\n";
								$h_applyClubTable .= '<div id="statusDiv_'.$_choiceNumber.'">'.$_statusDisplay.'</div>'."\n";
								$h_applyClubTable .= '<div id="timeClashDiv_'.$_choiceNumber.'" class="timeClashDiv tabletextrequire" style="display:none;">'.$eEnrollment['time_crash'].'</div>'."\n";
							$h_applyClubTable .= '</td>'."\n";
							if ($libenroll->tiebreak == 1) {
								$h_applyClubTable .= '<td><div id="positionDiv_'.$_choiceNumber.'">'.$_positionDisplay.'</div></td>'."\n";
							}
							$h_applyClubTable .= '<td><div id="appliedDateTimeDiv_'.$_choiceNumber.'">'.$_appliedDateTimeDisplay.'</div></td>'."\n";
							$h_applyClubTable .= '<td><div id="roleTitleDiv_'.$_choiceNumber.'">'.$_roleDisplay.'</div></td>'."\n";
							$h_applyClubTable .= '<td><div id="attendanceDiv_'.$_choiceNumber.'">'.$_attendanceDisplay.'</div></td>'."\n";
							if ($sys_custom['eEnrollment']['RequestStudentInputClubRank']) {
								$h_applyClubTable .= '<td>'.$_rankDisplay.'</td>'."\n";
							}
							if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
								$h_applyClubTable .= '<td><div id="applicantInfoDiv_'.$_choiceNumber.'">'.$_applicantInfoDisplay.'</div></td>'."\n";
							}
							if($LibUser->IsStudent() && $sys_custom['eEnrolment']['ReplySlip']){
								$h_applyClubTable .= '<td><div id="replySlipDiv_'.$_choiceNumber.'">'.$replySlipDisplay.'</div></td>';
							}
						$h_applyClubTable .= '</tr>'."\n";
					}
				}
			$h_applyClubTable .= '</tbody>'."\n";
		$h_applyClubTable .= '</table>'."\n";
	$h_applyClubTable .= '</div>'."\n";

	### Club Rejected
	if ($numOfRejectedClub > 0) {
		$h_rejectedClubTable .= '<div style="float:left;">'.$linterface->GET_NAVIGATION2($Lang['eEnrolment']['ClubRejected']).'</div>';
		$h_rejectedClubTable .= '<br style="clear:both;" />'."\n";
		$h_rejectedClubTable .= '<div>'."\n";
			$h_rejectedClubTable .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
				$h_rejectedClubTable .= '<thead>'."\n";
					$h_rejectedClubTable .= '<tr>'."\n";
						$h_rejectedClubTable .= '<th style="width:20px;">#</th>'."\n";
						$h_rejectedClubTable .= '<th style="width:55%;">'.$eEnrollment['club_name'].'</th>'."\n";
						//$h_rejectedClubTable .= '<th style="width:25%;">'.$Lang['eEnrolment']['ClubCategory'].'</th>'."\n";
						$h_rejectedClubTable .= '<th style="width:45%;">'.$eEnrollment['last_modified'].'</th>'."\n";
					$h_rejectedClubTable .= '</tr>'."\n";
				$h_rejectedClubTable .= '</thead>'."\n";
				
				$h_rejectedClubTable .= '<tbody>'."\n";
				if ($numOfRejectedClub == 0) {
					$h_rejectedClubTable .= '<tr><td colspan="4" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
				}
				else {
					for ($i=0; $i<$numOfRejectedClub; $i++) {
						$_title = $rejectedClubInfoAry[$i]['Title'];
						$_category = $rejectedClubInfoAry[$i]['CategoryName'];
						$_lastModified = $rejectedClubInfoAry[$i]['DateModified'];
						
						$h_rejectedClubTable .= '<tr>'."\n";
							$h_rejectedClubTable .= '<td>'.($i + 1).'</td>'."\n";
							$h_rejectedClubTable .= '<td>'.$_title.'</td>'."\n";
							//$h_rejectedClubTable .= '<td>'.$_category.'</td>'."\n";
							$h_rejectedClubTable .= '<td>'.$_lastModified.'</td>'."\n";
						$h_rejectedClubTable .= '</tr>'."\n";
					}
				}
				$h_rejectedClubTable .= '</tbody>'."\n";
			$h_rejectedClubTable .= '</table>'."\n";
		$h_rejectedClubTable .= '</div>'."\n";
	}
}
####################################################
####### Apply and Rejected Club Table [End] ########
####################################################

if($sys_custom['eEnrolment']['UnitRolePoint']){
    $h_headerInfo .= '<tr>'."\n";
    $h_headerInfo .= '<td class="field_title">'.$Lang['eEnrolment']['SysRport']['StudentECATotalPoint'].'</td>'."\n";
    $h_headerInfo .= '<td>'.$PointTotal.'</td>'."\n";
    $h_headerInfo .= '</tr>'."\n";
}

$h_headerInfo .= '</table>'."\n";
$h_headerInfo .= '</div>'."\n";

####################################################
################ Buttons [Start] ###################
####################################################
if ($libenroll->UseCategorySetting && $sel_category=='') {
	// hide the submit button if not chosen category
}
else if ($withinProcessStage) {
	// hide the submit button if within the processing period
}
else if ($withinEnrolStage) {
	$h_submitBtn = '';
	if($LibUser->RecordType=='3' && $libenroll->enrollPersontype =='1'){
		$h_submitBtn = $linterface->GET_ACTION_BTN($Lang['eEnrolment']['SubmitApplication'], "button", "checkForm();", "Btn_EnrolFormSubmit");
	}
	if($LibUser->RecordType=='2' && $libenroll->enrollPersontype =='0'){
		$h_submitBtn = $linterface->GET_ACTION_BTN($Lang['eEnrolment']['SubmitApplication'], "button", "checkForm();", "Btn_EnrolFormSubmit");
	}
	if($sys_custom['eEnrollment']['setTargetForm'] && !$isTarget){
	    $h_submitBtn = "";
	}
// 	$h_submitBtn = $linterface->GET_ACTION_BTN($Lang['eEnrolment']['SubmitApplication'], "button", "checkForm();", "Btn_EnrolFormSubmit");
}
####################################################
################# Buttons [End] ####################
####################################################


####################################################
############# Hidden Fields [Start] ################
####################################################
$h_hiddenField = '';

// store target applicant UserID
$h_hiddenField .= $linterface->GET_HIDDEN_INPUT('ApplicantID', 'ApplicantID', $ApplicantID);

// record old child for parent
// if the parent has changed the child selection => reset the maxwant value
$h_hiddenField .= $linterface->GET_HIDDEN_INPUT('OldChildID', 'OldChildID', $ChildID);

// if the user has changed the category selection => reset the maxwant value
$h_hiddenField .= $linterface->GET_HIDDEN_INPUT('OldCategoryID', 'OldCategoryID', $sel_category);

// Maximum number of club for update page use
$h_hiddenField .= $linterface->GET_HIDDEN_INPUT('max_clubs', 'max_clubs', $maxNumOfDisplayRow);

$h_hiddenField .= $linterface->GET_HIDDEN_INPUT('isTarget', 'isTarget', $isTarget);
####################################################
############## Hidden Fields [End] #################
####################################################

#### SIS cust #J84059 #### 
if( $sys_custom['eEnrolment']['hideRejectedTable']){
	$h_rejectedClubTable = '';
}


##### #######
if($sys_custom['eEnrolment']['Application']['MinApply']){
    $custMinApplyClub = 9;
}

if($sys_custom['eEnrolment']['Application']['EachCategoryNeed']){
	$CategoryAry = $libenroll->GET_CATEGORY_LIST();
	$CategoryAryWithKeyID = build_assoc_array($CategoryAry);
}



?>
<script language="javascript">
var numOfDisplayedRow = '<?=$maxNumOfDisplayRow?>';
var withinEnrolStageText = '<?=$withinEnrolStageText?>';
<?=$html['jsApplyMinVarInitialize']?>
<?=$html['jsApplyMaxVarInitialize']?>
<?=$html['jsApplyMinWarningAry']?>
<?=$html['jsApplyMaxWarningAry']?>

$(document).ready( function() {
	<?if($sys_custom['eEnrolment']['hideNonApprovedRow'] && !$withinProcessStage && !$withinEnrolStage){?>
		$('.hideRow').hide();
	<?}?>
		
	Block_Element('clubInfoDiv');
	
	changedMaxWantSelection($('#maxwant').val());
	
	// initial the option disable status
	var i;
	for (i=0; i<<?=$maxNumOfDisplayRow?>; i++) {
		var _choiceNumber = i + 1;
		var _enrolGroupId = $('#choice' + _choiceNumber).val();
		
		if (_enrolGroupId == '' || _enrolGroupId == null) {
			// do nth
		}
		else {
			var _selectionId = 'choice' + _choiceNumber;
			$('select.clubSel[id!="' + _selectionId + '"] > option').each( function() {
				if ($(this).val() == _enrolGroupId) {
					$(this).attr('disabled', true);
				}
			});
		}
	}

	// record the original selection value so as to enable back the options if the user un-enroll a club
	$('select.clubSel').each( function() {
		$(this).data('oldValue', $(this).val());
	});
	
	// time clash display checking
	var isTarget = document.getElementById("isTarget").value;
	if(isTarget != ""){
		if(isTarget == 0){
		for (var i=0; i<numOfDisplayedRow; i++) {
			var _choiceNumber = i + 1;
			var _enrolGroupId = $('#choice' + _choiceNumber).val();

			var _selectionId = 'choice' + _choiceNumber;
			$('select.clubSel[id!="' + _selectionId + '"]').each( function() {
					$(this).attr('disabled', true);
			});
		}
			$('#Btn_EnrolFormSubmit').attr('disabled', true);
		}else{
			reloadTimeClashDisplay();
		}
	}
	
	UnBlock_Element('clubInfoDiv');
	
});

function getEnrolGroupIdByChoiceNumber(choiceNumber) {
	return $('#choice' + choiceNumber).val();
}

function getApplicantId() {
	return $('#ApplicantID').val();
}

function changedMaxWantSelection(targetMaxWant) {
	if (targetMaxWant == 0) {
		if (withinEnrolStageText == '1') {
			$('span#maxWantZeroWarningSpan').show();
		}
		
		if ($('input.selectedClubField').length == 0) {
			$('div#clubInfoDiv').hide();
		}
	}
	else {
		$('span#maxWantZeroWarningSpan').hide();
		$('div#clubInfoDiv').show();
	}
}

function changedClubSelection(changedSelId, selectedEnrolGroupId, choiceNumber) {
	Block_Element('clubInfoDiv');

	// enable the original club option for all drop down lists
	var originalClubEnrolGroupId = $('#' + changedSelId).data('oldValue');
	$('select.clubSel').each( function() {
		$(this).find('option[value="' + originalClubEnrolGroupId + '"]').attr('disabled', '');
	});
	
	// store the current selection value for later use
	$('#' + changedSelId).data('oldValue', selectedEnrolGroupId);
	
	if (selectedEnrolGroupId == '') {
		// hide the view club details icon
		$('#viewClubDetailsLink_' + choiceNumber).hide();
	}
	else {
		// disable the club option in all other drop down lists
		$('select.clubSel[id!="' + changedSelId + '"]').each( function() {
			$(this).find('option[value="' + selectedEnrolGroupId + '"]').attr('disabled', 'disabled');
		});
		
		// show the view club details icon
		$('#viewClubDetailsLink_' + choiceNumber).show();
	}
	
	// reload the selected club data row
	reloadChoiceRowInfo(choiceNumber);
	
	// reload time clash display
	reloadTimeClashDisplay();
	
	UnBlock_Element('clubInfoDiv');
}

function ShowDateDetail(choiceNumber) {
	$('#DateDetailDiv' + choiceNumber).show();
	$('#ViewDetailLink' + choiceNumber).hide();
	$('#HideDetailLink' + choiceNumber).show();	
}

function HideDateDetail(choiceNumber) {	
	$('#DateDetailDiv' + choiceNumber).hide();
	$('#ViewDetailLink' + choiceNumber).show();
	$('#HideDetailLink' + choiceNumber).hide();	
}

function showAllDateDetails() {
	$('.showDateDetailsBtn').click();
	
	$('span#hideAllDateBtnSpan').show();
	$('span#viewAllDateBtnSpan').hide();
}

function hideAllDateDetails() {
	$('.hideDateDetailsBtn').click();
	
	$('span#hideAllDateBtnSpan').hide();
	$('span#viewAllDateBtnSpan').show();
}

function viewClubDetails(choiceNumber) {
	var targetEnrolGroupId = getEnrolGroupIdByChoiceNumber(choiceNumber);
	var applicantId = getApplicantId();
	newWindow('group_info.php?EnrolGroupID=' + targetEnrolGroupId + '&applicantId=' + applicantId, 9);
}

function viewAttendance(choiceNumber) {
	var targetEnrolGroupId = getEnrolGroupIdByChoiceNumber(choiceNumber);
	var applicantId = getApplicantId();
	window.location = '../../eAdmin/StudentMgmt/enrollment/club_attendance_mgt.php?EnrolGroupID=' + targetEnrolGroupId + '&applicantId=' + applicantId + '&action=view';
}

function reloadChoiceRowInfo(choiceNumber) {
	var targetEnrolGroupId = getEnrolGroupIdByChoiceNumber(choiceNumber);
	var applicantId = getApplicantId();
	
	var loadingImgHtml = getAjaxLoadingMsg(false);
	var divIdAry = new Array();
	divIdAry[divIdAry.length] = 'meetingDateDiv_';
	divIdAry[divIdAry.length] = 'feeDiv_';
	divIdAry[divIdAry.length] = 'statusDiv_';
	divIdAry[divIdAry.length] = 'positionDiv_';
	divIdAry[divIdAry.length] = 'appliedDateTimeDiv_';
	divIdAry[divIdAry.length] = 'roleTitleDiv_';
	divIdAry[divIdAry.length] = 'attendanceDiv_';
	
	<? if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) { ?>
	divIdAry[divIdAry.length] = 'applicantInfoDiv_';
	<? } ?>
	<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
	divIdAry[divIdAry.length] = 'replySlipDiv_';
	<?php }?>
	var numOfDiv = divIdAry.length;
	var i;
	for (i=0; i<numOfDiv; i++) {
		var _divId = divIdAry[i];
		$('div#' + _divId + choiceNumber).html(loadingImgHtml);
	}
	
	if (targetEnrolGroupId == '') {
		for (i=0; i<numOfDiv; i++) {
			var _divId = divIdAry[i];
			$('div#' + _divId + choiceNumber).html('<?=$Lang['General']['EmptySymbol']?>');
		}
	}
	else {
		$.post(
			"ajax_reload.php", 
			{ 
				ReloadAction: 'studentChosenClubInfoRowData',
				enrolGroupId: targetEnrolGroupId,
				applicantId: applicantId,
				choiceNumber: choiceNumber
			},
			function(ReturnData) {
				var dataAry = ReturnData.split('<?=$enrolConfigAry['ajaxDataSeparator']?>');
				
				for (i=0; i<numOfDiv; i++) {
					var _divId = divIdAry[i];
					$('div#' + _divId + choiceNumber).html(dataAry[i]);
				}
			}
		);
	}
}

function reloadTimeClashDisplay() {
	var applicantId = getApplicantId();
	var selectedEnrolGroupIdAry = new Array();
	$('.selectedClubField').each( function () {
		selectedEnrolGroupIdAry[selectedEnrolGroupIdAry.length] = $(this).val();
	});
	var selectedEnrolGroupIdText = selectedEnrolGroupIdAry.join(',');
	
	$('#Btn_EnrolFormSubmit').attr('disabled','disabled');
	$.ajax({
				type: 'POST',
				url: "ajax_reload.php",
				async: false,
				data: {
						"ReloadAction"		:	"timeClashDisplay",
						enrolGroupIdText 	:	selectedEnrolGroupIdText,
						applicantId 	:	applicantId
						},
				success: function(ReturnData){
					var clashedChoiceNumberAry = ReturnData.split(',');
		
					var i;
					for (i=0; i<numOfDisplayedRow; i++) {
						var _choiceNumber = i + 1;
						
						if (jQuery.inArray(_choiceNumber.toString(), clashedChoiceNumberAry) == -1) {
							$('div#timeClashDiv_' + _choiceNumber).hide();
						}
						else {
							$('div#timeClashDiv_' + _choiceNumber).show();
						}
					}
					$('#Btn_EnrolFormSubmit').removeAttr('disabled');
				}
			});
}

function saveStudentExtraInfo() {
	var canSubmit = true;
	
	var emergencyPhone = $.trim($('input#EmergencyPhoneTb').val());
	if (emergencyPhone == '') {
		$('div#emergencyPhoneWarningDiv').show();
		$('input#EmergencyPhoneTb').focus();
		canSubmit = false;
	}
	else {
		$('div#emergencyPhoneWarningDiv').hide();
	}
	
	if (canSubmit) {
		$.ajax({
			url:      	"ajax_update.php",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&Action=saveStudentExtraInfo&RecordType=<?=$enrolConfigAry['Club']?>',
			success:  	function(data){
							var returnMsg = (data=='1')? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							Scroll_To_Top();
							Get_Return_Message(returnMsg);
					 	}
  		});
	}
}

function checkForm() {
	var canSubmit = true;
	$('#Btn_EnrolFormSubmit').attr("disabled",true);
	
	var oneClubOnly = '<?= $libenroll->oneEnrol ?>';
	if (oneClubOnly == '') {
		oneClubOnly = 0;
	}
	
	// check time clash
	var numOfTimeClash = $('div.timeClashDiv:visible').length;
	if (oneClubOnly == 1 && numOfTimeClash > 0) {
		alert('<?= $eEnrollment['js_enrol_one_only'] ?>');
		canSubmit = false;
//		return false;
	}
	
	
	// check if applied minimum number of club
	<? if ($termBasedFormChecking) { ?>
		var countApplicationAry = new Array();
		$.each(applyMinAry, function(_termNum, _applyMin) {
			countApplicationAry[_termNum] = 0;
		});
		$('.selectedClubField').each( function () {
			if ($(this).val() != '') {
				var _selId = $(this).attr('id');
				var _termNumber = $('#' + _selId + ' option:selected').attr('termnumber');
				countApplicationAry[_termNumber]++;
			}
		});
		
		$.each(applyMinAry, function(_termNum, _applyMin) { 
			 _termNum = parseInt(_termNum);
			 _applyMin = parseInt(_applyMin);
			 _appledNum = parseInt(countApplicationAry[_termNum]);
			 
			 if (_appledNum < _applyMin) {
			 	alert(applyMinWarningAry[_termNum]);
			 	canSubmit = false;
//			 	return false;
			 }
		});
		if (!canSubmit) {
			canSubmit = false;
//			return false;
		}
	<? } else if(!$sys_custom['eEnrolment']['Application']['MinApply']){ ?>
		var minNumOfApp = '<?=$minNumOfApp?>';
		var numOfApp = 0;
		$('.selectedClubField').each( function () {
			if ($(this).val() != '') {
				numOfApp++;
			}
		});
		if (parseInt(numOfApp) < parseInt(minNumOfApp)) {
			alert('<?= str_replace("<!--NoOfClub-->", $minNumOfApp, ($sys_custom['eEnrolment']['Application']['SelectAllClubChoice'] && $minNumOfEnrol <= 0?$Lang['eEnrolment']['WarningMsg']['NeedApplyClub']:$eEnrollment['front']['wish_enroll_min']))?>');
			canSubmit = false;
//			return false;
		}
	<? } ?>
	
	
	// check if applied maximum number of club
	<? if ($termBasedFormChecking) { ?>
		$.each(applyMaxAry, function(_termNum, _applyMax) { 
			 _termNum = parseInt(_termNum);
			 _applyMax = parseInt(_applyMax);
			 _appledNum = parseInt(countApplicationAry[_termNum]);
			 
			 if (_applyMax != 0 && _appledNum > _applyMax) {
			 	alert(applyMaxWarningAry[_termNum]);
			 	canSubmit = false;
//			 	return false;
			 }
		});
		if (!canSubmit) {
			canSubmit = false;
//			return false;
		}		
	<? } ?>
	
	
	// check if applied same club
	var appliedEnrolGroupIdAry = new Array();
	$('.selectedClubField').each( function () {
		var _enrolGroupId = $(this).val();
		if (_enrolGroupId == '') {
			return true;	// continue
		}
		
		if (jQuery.inArray(_enrolGroupId.toString(), appliedEnrolGroupIdAry) == -1) {
			appliedEnrolGroupIdAry[appliedEnrolGroupIdAry.length] = _enrolGroupId;
		}
		else {
			alert('<?=$Lang['eEnrolment']['jsWarning']['CannotApplySameClub']?>');
			canSubmit = false;
			return false;	// break the loop
		}
	});

    <?php if($sys_custom['eEnrolment']['Application']['MinApply']){ ?>
        var minNumOfApp = '<?=$custMinApplyClub?>';
        var numOfApp = 0;
        $('.selectedClubField').each( function () {
        	if ($(this).val() != '') {
        		numOfApp++;
        	}
        });

		if (parseInt(numOfApp) < parseInt(minNumOfApp)) {
			alert('<?= str_replace("<!--NoOfClub-->", $custMinApplyClub, $eEnrollment['front']['wish_enroll_min'])?>');
			canSubmit = false;
//			return false;
		}     
    <? }?>


    <?php if($sys_custom['eEnrolment']['Application']['EachCategoryNeed']){ ?>
        var selectedClubCategoryIDAry = [];
        $('.selectedClubCategory').each( function () {
        	var selectedClubCategoryID = $(this).val();
        	if (selectedClubCategoryID != '') {
        		selectedClubCategoryIDAry.push(selectedClubCategoryID);
        	}
        });
     
        $('.selectedClubField').each( function () {
        	if($(this)[0].tagName == 'SELECT'){
        		var optionClubCategoryID  = $(this)[0].options[$(this)[0].selectedIndex].id;
            	if (optionClubCategoryID != '') {
            		selectedClubCategoryIDAry.push(optionClubCategoryID);
            	}
			} 

        });

        var selectedClubLength = selectedClubCategoryIDAry.filter( onlyUnique ).length;
		var numOfClubCategory = '<?=$numOfClubCategory?>';
		
		var clubSelectedForCat2 = [];
		var element = '2';
		for(var j=0;j<selectedClubCategoryIDAry.length;j++){
			if(selectedClubCategoryIDAry[j] == element){
				clubSelectedForCat2.push(j);
			}
		}
		
		var clubSelectedForCat4 = [];
		var element = '4';
		for(var j=0;j<selectedClubCategoryIDAry.length;j++){
			if(selectedClubCategoryIDAry[j] == element){
				clubSelectedForCat4.push(j);
			}
		}
		
		var clubSelectedForCat5 = [];
		var element = '5';
		for(var j=0;j<selectedClubCategoryIDAry.length;j++){
			if(selectedClubCategoryIDAry[j] == element){
				clubSelectedForCat5.push(j);
			}
		}
		
		if(clubSelectedForCat2.length != 1){
			canSubmit = false;
    		alert('<?=str_replace("[__qty__]",1,str_replace("[__name__]",$CategoryAryWithKeyID[2],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>');
		}
		else if(clubSelectedForCat4.length != 4){
			canSubmit = false;
    		alert('<?=str_replace("[__qty__]",4,str_replace("[__name__]",$CategoryAryWithKeyID[4],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>');
		}
		else if(clubSelectedForCat5.length != 4){
			canSubmit = false;
    		alert('<?=str_replace("[__qty__]",4,str_replace("[__name__]",$CategoryAryWithKeyID[5],$Lang['eEnrolment']['WarningMsg']['NeedSelectTheCategory']))?>');
		}
		else if(selectedClubLength< numOfClubCategory){		
			 canSubmit = false;
			 alert('<?=$Lang['eEnrolment']['WarningMsg']['NeedSelectEachCategory']?>');
		}   
    <? }?>
	
	if (canSubmit) {
		// prevent double click
		$('#Btn_EnrolFormSubmit').attr("disabled",true);
		// Ensure maxwant value can store in DB
		document.getElementById('maxwant').disabled = false;
		$('form#form1').attr('action', 'enroll_update.php').submit();
	}
	else{
		$('#Btn_EnrolFormSubmit').removeAttr("disabled");
		return false;
	}
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function reloadPage() {
	$('form#form1').attr('action', 'index.php').submit();
}

function OnloadReplySlip(enrolmentGroupID){
	newWindow('ReplySlip_Sign.php?enrolmentGroupID='+enrolmentGroupID,10);
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$h_childSel?>
	
	<?=$h_studentExtraInfo?>
	
	<?=$h_warningMsg?>
		
	<div><?=$h_headerInfo?></div>
	<br style="clear:both;" />
	
	<?=$h_withinProcessStageWarning?>
	<div id="clubInfoDiv"><?=$h_applyClubTable?></div>
	<br style="clear:both;" />
	
	<div><?=$h_rejectedClubTable?></div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30"><?=$h_submitBtn?></div>
	<br style="clear:both;" />
	
	<?=$h_hiddenField?>
</form>
<br />
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>