<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();

$libenroll_ui = new libclubsenrol_ui();
$libfcm = new form_class_manage();

$canAccess = false;
if ($LibUser->IsStudent()) {
    $ApplicantID = $_SESSION['UserID'];
    $canAccess = true;
    $currentUserType = "S";
}

// if (!$libenroll->isUseEnrollment()) {
//     if ($libenroll->HAVE_CLUB_MGT()) {
//         header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/group.php");
//     }
//     else if ($libenroll->HAVE_EVENT_MGT()) {
//         header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/event.php");
//     }
//     else if ($_SESSION['UserType']==USERTYPE_STAFF) {
//         header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/group.php");
//     }
//     else {
//         $canAccess = false;
//     }
// }

if (!$canAccess || !$sys_custom['eEnrolment']['CommitteeRecruitment']) {
    No_Access_Right_Pop_Up();
}

if ($plugin['eEnrollment']) {
    include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
    $libenroll = new libclubsenrol();
    if (!$libenroll->isUseEnrollment()) {
        die("You have no priviledge to access this page.");
    }
    if ((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) && !$LibUser->isStudent() && !$LibUser->isParent())
        header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
        $CurrentPage = "PageClubEnroll";
        $CurrentPageArr['eServiceeEnrolment'] = 1;
        $genStudentMenu = 1;
        $MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
        
        if ($libenroll->hasAccessRight($_SESSION['UserID']))
        {
            include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
            
            $linterface = new interface_html();
            $TAGS_OBJ[] = array($eEnrollmentMenu['club_enroll'], "index.php", 0);         
            $TAGS_OBJ[] = array( $eEnrollment['tab']['CommitteeRecruitment'], "committee_club.php", 1);

            $linterface->LAYOUT_START();


            
            if ($libenroll->Committee_Recruitmen_End== "") $disabled = "";

            $AcademicYearID = $libenroll->CommitteeRecruitmentAcademicYearID;
            $ClubInfo = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category, '', '', $OrderBy, $OrderDesc, $ApplyUserType='', $Round);
            $numOfClub = count($ClubInfo);
            $AllowOnlineRegisterClubNum = 0;
            for ($i=0; $i<$numOfClub; $i++)
            {
                $thisOnlineRegisterStatus = $ClubInfo[$i]['UseOnlineRegistration'];
                if($thisOnlineRegisterStatus == '1'){
                    $AllowOnlineRegisterClubNum++;
                }
            }
         
            $CommitteeApplyMax =  $libenroll->CommitteeApplyMax;
            if($CommitteeApplyMax =='0' ||$CommitteeApplyMax ==''){
                $CommitteeApplyMax = $AllowOnlineRegisterClubNum;
            }
            //        $max_numbers = array(array(0, 20));
            for ($i=1; $i<=$CommitteeApplyMax; $i++) {
                $max_numbers[] = array($i,$i);
            }
           
            
            ###########################################################
            ################### Header Table [Start] ##################
            ###########################################################
            $CommitteeStart = $libenroll->Committee_Recruitmen_Start." ".$libenroll->Committee_Recruitmen_Start_Hour.":".$libenroll->Committee_Recruitmen_Start_Min;
            $CommitteeEnd = $libenroll->Committee_Recruitmen_End." ".$libenroll->Committee_Recruitmen_End_Hour.":".$libenroll->Committee_Recruitmen_End_Min;
            (date("Y-m-d H:i") >= $CommitteeStart && date("Y-m-d H:i")<=$CommitteeEnd) ? $disabled = "" : $disabled = "disabled";
                
            if ($libenroll->Committee_Recruitmen_Start!= '' && $libenroll->Committee_Recruitmen_End!= '') {
                $appPeriodText = $CommitteeStart.' '.$eEnrollment['to'].' '.$CommitteeEnd;
             }
             else {
                  $appPeriodText = $Lang['General']['EmptySymbol'];
             }

             $toolsBar = "
    				<div class=\"common_table_tool\">
    				    <a href=\"javascript:submitForm(document.form1,'committee_club_update.php?AcademicYearID=$AcademicYearID','')"."\" class=\"tool_approve\">".  $Lang['eEnrolment']['CommitteeRecruitment']['ChangeStatus']."</a>
       		        </div>
			     ";
             if($disabled){
                 $toolsBar = "";
              }
             $x = '';
               
             $x .= '<div>'."\n";
                 $x .= '<table class="form_table_v30">'."\n";
                    ### Application Period
                    $x .= '<tr>'."\n";
                        $x .= '<td class="field_title">'."\n";
                        $x .= $Lang['eEnrolment']['CommitteeRecruitment']['ClubYear']."\n";
                        $x .= '<br />'."\n";
                        $x .= '</td>'."\n";
                        $x .= '<td><span>'.getAcademicYearByAcademicYearID($AcademicYearID).'</span></td>'."\n";
                    $x .= '</tr>'."\n";
                    ### Application Period
                    $x .= '<tr>'."\n";
                        $x .= '<td class="field_title">'."\n";
                            $x .= $eEnrollment['app_period']."\n";
                        $x .= '<br />'."\n";
    //                     $x .= '<span class="tabletextremark">('.$Lang['eEnrolment']['ReferenceEnrolmentPeriodRemarks'].')</span>'."\n";
                        $x .= '</td>'."\n";
                        $x .= '<td><span>'.$appPeriodText.'</span></td>'."\n";
                    $x .= '</tr>'."\n";
                
                    $x .= '</table>'."\n";
               $x .= '</div>'."\n";
             $htmlAry['headerInfo'] = $x;
             ###########################################################
             #################### Header Table [End] ###################
             ###########################################################
?>
<script>
function submitForm(obj,page)
{
	var selectedOption = $('select#CommitteeApplyPriority').map(function(){
		return this.value === ""?null:this.value;
    }).get();


	var minClubNum = Math.max.apply(Math, selectedOption);
	
	
	var selectedOptionArray = selectedOption.sort();
	 
	
	var selectedOptionDuplicate = [];
	for (var i = 0; i < selectedOptionArray.length - 1; i++) {
	    if (selectedOptionArray[i + 1] == selectedOptionArray[i]) {
	    	selectedOptionDuplicate.push(selectedOptionArray[i]);
	    }
	}


	if(selectedOptionDuplicate != 0)
    	alert('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['CannotDuplicatePriority'];?>');
    else if(selectedOption.length != minClubNum && selectedOption.length>0 ){
    	alert('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['CannotLeaveSpacePriority'];?>');       
        }
    else{	   
	    if(confirm('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmChangeStatus'];?>')){
	        obj.action = page;
	        obj.method = "POST";
	        obj.submit();
	        return;
        }
//     	}
//     	else if(action == 'cancel')
// 	    { 
//	    	if(confirm('<?php echo $Lang['eEnrolment']['CommitteeRecruitment']['ConfirmCancelApply'];?>')){
// 	            obj.action = page;
// 	            obj.method = "POST";
// 	            obj.submit();
// 	            return;
//             }
//     	}
    }
}

</script>


<form name="form1" action="event_index.php" method="POST" id="form1">
<br/>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        	<td>
        		<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" >
        			<tr><td><?=$htmlAry['headerInfo']?></td></tr>
        			 <tr><td valign="bottom"><?=$toolsBar?></td></tr>
        		</table>
        	</td>
        </tr>
        
        <tr><td>
        	<table class="common_table_list">
                <thead>
                	<tr>
                		<th class="num_check" width="5%">#</th>                		
                		<th width="15%"><? echo $Lang['eEnrolment']['ClubCode'];?></th>
                		<th width="30%"><? echo $i_ClubsEnrollment_ClubName;?></th>
                		<th width="15%"><? echo $Lang['General']['Term'];?></th>
                		<th width="15%"><? echo $eEnrollment['add_activity']['act_category'];?></th>
                		<th width="10%"><? echo $Lang['General']['Status'];?></th>
                		<th width="10%"><? echo $Lang['eEnrolment']['Priority'];?></th>
                	</tr>
                </thead>
        
        	
            	<? 
            	$displayedRecord = 0;
       	
            	for ($i=0; $i<$numOfClub; $i++)
            	{
            	    $thisClubInfoArr = $ClubInfo[$i];
            	    $EnrolGroupID = $ClubInfo[$i]['EnrolGroupID'];
            	    
            	    $this_TitleDisplay = Get_Lang_Selection($ClubInfo[$i]['TitleChinese'],$ClubInfo[$i]['Title']);
            	    
            	   
            	    # Allow Online Registration
            	    $thisOnlineRegisterStatus = $ClubInfo[$i]['UseOnlineRegistration'];
            	   
            	    # Semester Display
            	    $SemesterDisplay = $ClubInfo[$i]['YearTermName'];
            	    
            	    # Category Display
            	    $CategoryDisplay = ($ClubInfo[$i]['CategoryName']=="") ? "-" : $ClubInfo[$i]['CategoryName'];
            	    
            	    
            	 
            	    
            	    if ($thisOnlineRegisterStatus== '1') // only show allow online register club
            	    {	   
            	        $CommitteeRecrutmentInfoAry =  $libenroll->Get_Committee_Member_Info($EnrolGroupID,$_SESSION['UserID']);
            	     
            	        $ApplyStatus =  $CommitteeRecrutmentInfoAry[0]['ApplyStatus'];
            	      
            	        $RecordStatus = $CommitteeRecrutmentInfoAry[0]['RecordStatus'];
            	        
            	        if($RecordStatus == '1'){
            	            $displayStatus = $Lang['General']['Approved'];
            	        }else if($RecordStatus == '0'){
            	            $displayStatus = $Lang['General']['Rejected'];
            	        }else{
            	            $displayStatus = $ApplyStatus=='1'?$Lang['eEnrolment']['CommitteeRecruitment']['Applied']:'-';
            	        }
            	        
            	        $unableChange = $RecordStatus == '1' || $RecordStatus == '0' ? "disabled":"";
            	        $displayedRecord++;
            	        $ClubPriority= $CommitteeRecrutmentInfoAry[0]['Priority'];
            	 //       $ApplySelectionBox  = getSelectByArray($max_numbers, 'id="CommitteeApplyPriority[]" name="CommitteeApplyPriority['.$EnrolGroupID.']" class="ApplyMaxSel"', $ClubPriority);
            	        $ApplySelectionBox  = getSelectByArray($max_numbers, 'id="CommitteeApplyPriority" name="CommitteeApplyPriority['.$EnrolGroupID.']" class="ApplyMaxSel" '.$unableChange, $ClubPriority);
            	        
            	        
          //  	        <input type="checkbox" name="EnrolGroupID[]" value="$EnrolGroupID" <?php echo $disabled;>
            	        ?>
            		     <tr class="tablerow<?= ((($displayedRecord-1) % 2) + 1)?>">
            		     	 <td class="tabletext"> <?=$displayedRecord?> </td>
            		     	<td class="tabletext"> <?=($ClubInfo[$i]['GroupCode']?$ClubInfo[$i]['GroupCode']:'-')?> </td>
            		     	 <!--Club Name -->
            			     <td class="tabletext"><?=$this_TitleDisplay?></td>
            	     
            			     <!--Semester -->
            			     <td class="tabletext"><?=$SemesterDisplay?></td>
            			     
            			     <!--Category -->
            			     <td class="tabletext"><?=$CategoryDisplay?></td>			     
            			    	
            			     <td class="tabletext"><?=$displayStatus?></td>				 
            			     <!--Checkbox -->
            			     <td align="center" valign="top">
            			     		<?php echo $ApplySelectionBox;?>
            				
            			     </td>
            			     			     
            		     </tr>
            	     <?
                 	}
                
            	}
        	
            	if ($displayedRecord== 0) {
            		print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"9\">".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
            	}
            	
        	   ?>
        	</table>
        	<br>
        </td></tr>    
    </table>
</form>
<script>


</script>
    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>