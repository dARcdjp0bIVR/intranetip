<?php
//Using:  

/********************************
 * Date:    2020-04-22 (Tommy) remove $libenroll->hasAccessRight() because it uses for eAdmin checking
 * Date:    2018-09-06 (Anna) change 15 to ApplyUserType in $EventArr
 * Date:	2017-04-24 P110324 (Villa) Add entry for signing ReplySlip
 * Date:	2016-11-30[R99066] (villa) Modified $TempStr add <a>hide all and view all to hide/show the time slots
 * 			Add HideDateDetail() hide/show function 
 * Date:	2015-11-02 (Omas) Fix oneEnrol disabled still clash with club #Z88303  
 * Date:	2013-08-12 (Cameron) Add argument CategoryTypeID to GET_CATEGORY_SEL() 
 * 
 * Date:	2013-02-08 (Rita) 
 * Details: change 'Enroll' to 'To Enroll' for survey form customization	
 * 
 * Date: 	2012-12-12 (Rita)
 * Details:	Add activity survey form 
 ********************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."home/admin/enrollment/templang.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

$ApplicantID = $UserID;
if ($ChildID != "") {
	$ApplicantID = $ChildID;
}
$ApplicantID = IntegerSafe($ApplicantID);
$sel_category = IntegerSafe($sel_category);

$isUserValid = false;
if ($LibUser->IsStudent()) {
	if ($ApplicantID == $_SESSION['UserID']) {
		$isUserValid = true;
	}
}
else if ($LibUser->IsParent()) {
	// is parent, get child ID
	$ChildArr = $LibUser->getChildrenList();
	
	$ChildrenUserIDAry = Get_Array_By_Key($ChildArr, 'StudentID');
	if ($ApplicantID != '' && ($ApplicantID == $_SESSION['UserID'] || in_array($ApplicantID, $ChildrenUserIDAry))) {
		$isUserValid = true;
	}
}
if (!$isUserValid) {
	No_Access_Right_Pop_Up();
}


if ($plugin['eEnrollment']) {
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol('',$sel_category);
// 	if (!$libenroll->isUseEnrollment()) {
// 		die("You have no priviledge to access this page.");
// 	}
	if ($libenroll->isUseEnrollment()) {
	    
	if ((($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) && !$LibUser->isStudent() && !$LibUser->isParent())
		header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");	
	$CurrentPage = "PageEventEnroll";
	$CurrentPageArr['eServiceeEnrolment'] = 1;
	$genStudentMenu = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
//     if ($libenroll->hasAccessRight($_SESSION['UserID']))
//     {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['event_enroll'], "", 1);

        $linterface->LAYOUT_START();
        
        # activity enrolment settings
//        $Instruction = $libenroll->Event_Description;
//		$InstructionArr = explode("\n", $Instruction);
//		$InstructionRow = "<tr><td colspan=\"3\" align=\"center\" class=\"tabletext\">";
//		for ($i=0; $i<sizeof($InstructionArr) ; $i++)
//			$InstructionRow .= $InstructionArr[$i]."<br />\n";
//		$InstructionRow .= "</td></tr>";
		
		$AppStart = $libenroll->Event_AppStart." ".$libenroll->Event_AppStartHour.":".$libenroll->Event_AppStartMin;
		$AppEnd = $libenroll->Event_AppEnd." ".$libenroll->Event_AppEndHour.":".$libenroll->Event_AppEndMin;
		(date("Y-m-d H:i") <= $AppEnd) ? $disabled = "" : $disabled = "disabled";
		if ($libenroll->Event_AppEnd == "") $disabled = "";
	
		if ( (($libenroll->Event_enrollPersontype == 1)&&($LibUser->IsStudent())) ||
			 (($libenroll->Event_enrollPersontype == 0)&&($LibUser->IsParent())) )
			 $disabled = "disabled";
			 
//		if (($libenroll->Event_AppStart != "")&&($libenroll->Event_AppEnd != "")) 
//		{
//			$app_period_str = str_replace("<!--AppStart-->", $AppStart, $eEnrollment['front']['app_period']);
//			$app_period_str = str_replace("<!--AppEnd-->", $AppEnd, $app_period_str);
//		}
		
		if ($LibUser->isParent()) {
//         	$UserType = 'P';	# This will change the $_SESSION['UserType']
			$ChildArr = $LibUser->getChildrenList();
			if (sizeof($ChildArr) > 0) {
				$ChildSel .= "<select name=\"ChildID\" id=\"ChildID\" onChange=\"document.form1.submit();\">";
				$ChildSel .= "<option value=\"\"> - ".$button_select." - </option>";
				if ($ChildID == "") {
					$ChildID = $ChildArr[0][0];
					$ApplicantID = $ChildID;
				}
				while (list($key, $value) = each ($ChildArr)) {			
					$ChildSel .= "<option value=\"".$ChildArr[$key][0]."\"";
					if ($ChildID == $ChildArr[$key][0]) $ChildSel .= " selected ";
					$ChildSel .= ">&nbsp;".$ChildArr[$key][1]."&nbsp;</option>\n";
				}
				$ChildSel .= "</select>";	
			}
		}
		else if ($LibUser->isStudent())
		{
// 			$UserType = 'S';		# This will change the $_SESSION['UserType']
		}
		
		//$LibUser = new libuser($UserID);
        $EventArr = $libenroll->GET_AVAILABLE_EVENTINFO_LIST($sel_category, $ApplicantID);
        
        //$TimeConflictArr = $libenroll->GEN_EVENT_TIME_CONFLICT_ARR($EventArr);
                
		##########################################################################################
		// selection box
		$minmax = $libenroll->getMinMaxOfStudent($ApplicantID, "activity");
		$min = $minmax[0];
		$max = $minmax[1];
		$totalEvent = sizeof($EventArr);
		if ($max == 0 || $max > sizeof($EventArr)) $max = $totalEvent;     # 0 means no limit
		if ($max < $min) $min = $max;
		$y = "";
		$libenroll->retrieveApplicationStatus($ApplicantID, "activity",0,$sql_category);
		$MaxToCheck = (isset($_GET['maxwant'])) ? $_GET['maxwant'] : $libenroll->maxStudentWanted;
		
		# updated on 9 Sept - use "Default max. no. of activities to be enrolled by each student" to determine the max selection of student
		$MaxToCheck = ($MaxToCheck > $libenroll->Event_EnrollMax && $libenroll->Event_EnrollMax != 0)? $libenroll->Event_EnrollMax : $MaxToCheck;
		# handle No Limit for enrol max case - show total number of Event
		$maxOptions = ($libenroll->Event_EnrollMax == 0)? $totalEvent : $libenroll->Event_EnrollMax;

		//for ($i=$min; $i<=$max; $i++)
		for ($i=$min; $i<=$maxOptions; $i++)
		{
		     $chk_str = ($MaxToCheck == $i)? "SELECTED":"";
		     $y .= "<option value=\"$i\" $chk_str>$i</option>\n";
		}
		$studentWillSelection = "<select id=\"maxwant\" name=\"maxwant\" $disabled>$y</select>\n";
		
		if(trim($disabled)=='')
			$StudentWillSaveBtn = $linterface->Get_Small_Btn($Lang['Btn']['Save'],"button","js_Update_Student_Will();");
		/*
		$isPastEventArr = array();
		for ($i = 0; $i < sizeof($EventArr); $i++) {	
			$thisEnrolEventID = $EventArr[$i][0];
		
			if (date("Y-m-d H:i", strtotime($EventArr[$i][14])) < date("Y-m-d H:i"))
			{
				$isPastEventArr[$thisEnrolEventID] = 1;
			}
			else
			{
				$isPastEventArr[$thisEnrolEventID] = 0;
			}
		}
		*/
		##########################################################################################

		//$LibUser = new libuser($UserID);        
        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");   
        if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("",$eEnrollment['default_school_quota_exceeded']);
        
        if($libenroll->Event_UseCategorySetting)
        {
        	$CatSelectTitle = $Lang['General']['PleaseSelect'];
        }
        else
        {
        	$CatSelectTitle =  $eEnrollment['all']; 
        }
        
		if ($sys_custom['eEnrolment']['CategoryType']) {
			$categoryTypeID = array(2);	// Activity
		}
		else {
			$categoryTypeID = 0;
		}
        
        $CatSelect = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "window.location='event_index.php?sel_category='+this.value ", $CatSelectTitle, 0, $categoryTypeID); 
        
        
        ###########################################################
        ################### Header Table [Start] ##################
        ########################################################### 
        $appStart = $libenroll->Event_AppStart." ".$libenroll->Event_AppStartHour.":".$libenroll->Event_AppStartMin;
		$appEnd = $libenroll->Event_AppEnd." ".$libenroll->Event_AppEndHour.":".$libenroll->Event_AppEndMin;
		if ($libenroll->Event_mode != 0 && $libenroll->Event_AppStart != '' && $libenroll->Event_AppEnd != '') {
			$appPeriodText = $appStart.' '.$eEnrollment['to'].' '.$appEnd;
		}
		else {
			$appPeriodText = $Lang['General']['EmptySymbol'];
		}
		$descriptionText = ($libenroll->Event_mode == 0 || trim($libenroll->Event_Description)=='')? $Lang['General']['EmptySymbol'] : nl2br($libenroll->Event_Description);
        
        $x = '';
		$x .= '<div style="float:left;">'.$linterface->GET_NAVIGATION2_IP25($Lang['eEnrolment']['EnrollmentInformation']).'</div>'."\n";
		$x .= '<br style="clear:both;">'."\n";
		$x .= '<div>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				### Application Period
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $eEnrollment['app_period']."\n";
						$x .= '<br />'."\n";
						$x .= '<span class="tabletextremark">('.$Lang['eEnrolment']['ReferenceEnrolmentPeriodRemarks'].')</span>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td><span>'.$appPeriodText.'</span></td>'."\n";
				$x .= '</tr>'."\n";
				
				### Application Instruction
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['Instruction'].'</td>'."\n";
					$x .= '<td>'.$descriptionText.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				if ($libenroll->Event_UseCategorySetting) {
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$eEnrollment['add_activity']['act_category'].'</td>'."\n";
						$x .= '<td>'.$CatSelect.'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		$htmlAry['headerInfo'] = $x;
		###########################################################
        #################### Header Table [End] ###################
        ###########################################################

?>
<script>
function OnloadReplySlip(enrolmentGroupID){
	newWindow('ReplySlip_Sign.php?enrolmentGroupID='+enrolmentGroupID+'&RecordType=2',10);
}
function showList(EnrolGroupID) {
	$("tr.exrow"+EnrolGroupID).show();
	$("tr.viewexrow"+EnrolGroupID).hide();
}
</script>


<form name="form1" action="event_index.php" method="POST" id="form1">
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
	<table border="0" cellspacing="0" cellpadding="1" align="center" width="95%">
		<tr>
			<td align="left"><?= $ChildSel ?></td>
		</tr>
		<tr>
			<td align="right"><?= $SysMsg ?></td>
		</tr>
	</table>
</td></tr>

<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="1" align="center" width="95%">
			<tr><td><?=$htmlAry['headerInfo']?></td></tr>
			
			<tr>
				<td nowrap align="center" class="tabletext">
					<? if ($libenroll->Event_oneEnrol == 1) { ?>
						<table border="0" cellspacing="0" cellpadding="1" align="center">
							<tr>
								<td class="tabletext">
									<?= $eEnrollment['front']['time_conflict_warning']?>
								</td>
							</tr>
						</table>
					<? } ?>
				</td>
			</tr>

			<tr>
				<td nowrap align="center" class="tabletext">
					<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
						<tr><td height="10"><td></tr>
						<tr><td class="dotline" colspan="10"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
						<tr><td height="3"><td></tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
	</td>
</tr>

<?if($sel_category || !$libenroll->Event_UseCategorySetting){?>
<tr>
	<td>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">			
			<tr>
				<td nowrap align="center" class="tabletext">
					<table border="0" cellspacing="0" cellpadding="1" align="center">
						<tr>
							<td class="tabletext">
								<?= str_replace("<!--NoOfActivity-->", $libenroll->Event_defaultMin, $eEnrollment['front']['wish_enroll_min_event'])?>
							</td>
							<td class="tabletext">
								<?= $eEnrollment['front']['wish_enroll_front']?>
							</td>
							<td class="tabletext">
								<?= $studentWillSelection?>
							</td>
							<td class="tabletext">
								<?= $eEnrollment['front']['wish_enroll_end_event']?>
								<?= $StudentWillSaveBtn ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>

		</table>
	</td>
</tr>

<tr><td>
<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tableorangetop">
		<td class="tableTitle" nowrap><span class="tabletopnolink"><?= $eEnrollment['add_activity']['act_name']?></span></td>
		<td class="tableTitle" width="30%"><span class="tabletopnolink"><?= $eEnrollment['add_activity']['act_time']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['add_activity']['act_quota']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$i_ClubsEnrollment_TieBreakRule?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$Lang['eEnrolment']['ApplicationDeadline']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['position']?></span></td>
		
		<?php if($libenroll->enableActivitySurveyFormRight()){?>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$Lang['eSurvey']['SurveyForm']?></span></td>
		<?}?>
		
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['status']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['role']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['attendence_record']?></span></td>
		<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['replySlip']['replySlip']?></span></td>
		<?php }?>	
	</tr>
	<? # added by henry on 26/8/2009
		$groupList = $libenroll->getClubListByUserID($ApplicantID);
		$enrolledEnrolEventIdAry = Get_Array_By_Key($libenroll->Get_Student_Activity_Info($ApplicantID), 'EnrolEventID');
		# end of addition
	?>
	<? for ($i = 0; $i < sizeof($EventArr); $i++) { ?>
	<?
	//debug_pr($EventArr[$i]);
	if(in_array($EventArr[$i]['EnrolGroupID'], $groupList) || $EventArr[$i]['EnrolGroupID']==0  || $EventArr[$i]['EnrolGroupID']=='' || in_array($EventArr[$i]['EnrolEventID'], $enrolledEnrolEventIdAry)) {	# added by henry
		$thisEnrolEventID = $EventArr[$i]['EnrolEventID'];
		$thisQuota = $EventArr[$i]['Quota'];
		
		$DataArr['EnrolEventID'] = $thisEnrolEventID;
		$DataArr['StudentID'] = $ApplicantID;
		$tempMark = false;
		
		$isStudentEnrolledEvent = $libenroll->IS_STUDNET_ENROLLED_EVENT($DataArr);
		$isStudentAppliedEvent = $libenroll->IS_STUDNET_APPLIED_EVENT($DataArr);
		$thisEnrolStatusArr = $libenroll->Get_Student_Event_Enroll_Status($thisEnrolEventID, $ApplicantID);
		$thisEnrolStatus = $thisEnrolStatusArr[0]['RecordStatus'];
		
		$TempEnrollStatus = '';
		$applied = false;
		
		if ($sys_custom['eEnrolment']['hide_club_if_quota_full'] && $isStudentAppliedEvent==false)
		{
			$numOfApproved = count($libenroll->GET_ENROLLED_EVENTSTUDENT($thisEnrolEventID, 2));
			$numOfWaiting = count($libenroll->GET_ENROLLED_EVENTSTUDENT($thisEnrolEventID, 0));
			
			if ($thisQuota!=0 && ($numOfApproved + $numOfWaiting) >= $thisQuota)
				continue;
		}
		
		$_applyEndTimeDisplay = date('Y-m-d H:i', strtotime($EventArr[$i]['ApplyEndTime']));
		if ($EventArr[$i]['ApplyStartTime'] <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") <=$EventArr[$i]['ApplyEndTime']) { 
			$withinEnrolPeriod = true;
		}
		else {
			$withinEnrolPeriod = false;
		}	
			
		if (
			$isStudentEnrolledEvent
			||
			$isStudentAppliedEvent
			||
			(
				(
// 					( ($EventArr[$i][15] == "S")&&($_SESSION['UserType']==USERTYPE_STUDENT) )||
// 					( ($EventArr[$i][15] == "P")&&($_SESSION['UserType']==USERTYPE_PARENT) )||
// 					( ($EventArr[$i][15] == "T")&&($isStudentEnrolledEvent) )||
// 					( ($EventArr[$i][15] == "P")&&($isStudentEnrolledEvent) )
				    ( ($EventArr[$i]['ApplyUserType'] == "S")&&($_SESSION['UserType']==USERTYPE_STUDENT) )||
				    ( ($EventArr[$i]['ApplyUserType'] == "P")&&($_SESSION['UserType']==USERTYPE_PARENT) )||
				    ( ($EventArr[$i]['ApplyUserType'] == "T")&&($isStudentEnrolledEvent) )||
				    ( ($EventArr[$i]['ApplyUserType'] == "P")&&($isStudentEnrolledEvent) )
				) 
				&&
				$withinEnrolPeriod
			)
		)
		{
			$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EventArr[$i][0]);
			if ($libenroll->enableUserJoinDateRange()) {
				
				if (count($DateArr) > 0) {
					if ($libenroll->enableUserJoinDateRange()) {
						$enrolAvailArr = $libenroll->Get_Enrol_AvailDate($EventArr[$i][0], array($ApplicantID), "Activity");
						if (count($enrolAvailArr) > 0) {
							foreach ($DateArr as $kk => $vv) {
								$checkStart = strtotime(date("Y-m-d", strtotime($vv["ActivityDateStart"])));
								if (empty($enrolAvailArr[$ApplicantID]["EnrolAvailiableDateStart"])) $availStart = "";
								else $availStart = strtotime(date("Y-m-d", strtotime($enrolAvailArr[$ApplicantID]["EnrolAvailiableDateStart"])));
								if (empty($enrolAvailArr[$ApplicantID]["EnrolAvailiableDateEnd"])) $availEnd = "";
								else $availEnd = strtotime(date("Y-m-d", strtotime($enrolAvailArr[$ApplicantID]["EnrolAvailiableDateEnd"])));
								
								if ((empty($availStart) || $checkStart >= $availStart) && (empty($availEnd) || $checkStart <= $availEnd)) {
									
								} else {
									unset($DateArr[$kk]);
								}
							}
							if (count($DateArr) > 0) {
								$DateArr = array_values($DateArr);
							}
						}
					}
				}
			}
			
			$TempStr = "<a id='HideDateDetail_{$i}' href='javascript:void(0);' onclick='HideDateDetail({$i},1);'>[".$Lang['eEnrolment']['HideAll']."]</a>";
			$TempStr .= "<a id='ShowDateDetail_{$i}' href='javascript:void(0);' onclick='HideDateDetail({$i},0);' style='display: none '>[".$Lang['eEnrolment']['ViewAll']."]</a>";
			$TempStr .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" id=\"timeTable_{$i}\">";
			
			for ($j = 0; $j < sizeof($DateArr); $j++) {
				//if (($j % 2) == 0) $TempStr .= "<tr class=\"tabletext\">";
				$displaynone=($j>=5 )?' style="display:none" ':'';
				
				$TempStr .= "<tr class=\"tabletext exrow$thisEnrolEventID\" $displaynone>";
				$TempStr .= "<td class=\"tabletext\">".date("Y-m-d", strtotime($DateArr[$j][2]))." (".date("D", strtotime($DateArr[$j][2])).") ".date("H:i", strtotime($DateArr[$j][2]))."-".date("H:i", strtotime($DateArr[$j][3]))."</td>";
				$TempStr .= "</tr>";
				//if (($j % 2) == 1) $TempStr .= "</tr>";
			}
			if($j>5)
				$TempStr .= "<tr class=\"tabletext viewexrow$thisEnrolEventID\"><td>[<a class='tablelink' href='javascript:showList(\"$thisEnrolEventID\");'>".$Lang["eEnrolment"]["ShowEntireList"]."</a>]</td></tr>";
			
			$TempStr .= "</table>";
			
			
			# Survey Form Customization - Obtain Survey Question Info
			if($libenroll->enableActivitySurveyFormRight()){
				$eventInfoArr = $libenroll->GET_EVENTINFO($thisEnrolEventID);
				$surveyQuestionInfoArr = $eventInfoArr['Question'];
			}
			
			if ($EventArr[$i][2] >= $EventArr[$i][1]) {	//Approved >= Quota
				// quota display
				if ($EventArr[$i][1] == 0) {
					// normal for apply
					
										
					if($libenroll->enableActivitySurveyFormRight()){
						//$userAnswer = $libenroll->returnUserSurveyAnswerInfo($UserID, $thisEnrolEventID);

						if($surveyQuestionInfoArr){
						//	$EnrollStatus = "<a href='javascript:void(0)' onclick='openSurvey($thisEnrolEventID, $ApplicantID);' class='tablelink'>" . $Lang['General']['To'] . ' ' .$eEnrollment['front']['enroll']. "</a>";
						
							$EnrollStatus = $linterface->GET_ACTION_BTN($Lang['General']['To'] . ' ' .$eEnrollment['front']['enroll'], "Button", "openSurvey($thisEnrolEventID, $ApplicantID, 'toEnrol');");	
							
						}else{
											
							$EnrollStatus = $linterface->GET_ACTION_BTN($Lang['General']['To'] . ' ' .$eEnrollment['front']['enroll'], "Button", "js_AlertSubmitSurvey('".$ApplicantID."', '".$thisEnrolEventID."');");	
						}	
							
					}else{
						// 20170926 Omas
						if($sys_custom['eEnrolment']['ReplySlip']){
							$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$thisEnrolEventID,2);
							$onloadReplySlip = "";
							if(!empty($ReplySlipRelation)){
								$onloadReplySlip = "OnloadReplySlip('".$EventArr[$i][0]."');";
							}
							$EnrollStatus = $linterface->GET_ACTION_BTN($eEnrollment['front']['enroll'], "submit", $onloadReplySlip."document.form1.action='event_add_update.php'; document.getElementById('EnrolEventID').value='".$EventArr[$i][0]."'");
						}else{
							$EnrollStatus = $linterface->GET_ACTION_BTN($eEnrollment['front']['enroll'], "submit", "document.form1.action='event_add_update.php'; document.getElementById('EnrolEventID').value='".$EventArr[$i][0]."'");
						}	
					}
				} else {
					$EnrollStatus = $eEnrollment['front']['quota_warning'];
				}				
			} else {
				$tempMark = true;
			}
			
			# Survey from customization
			if($libenroll->enableActivitySurveyFormRight()){
			
				if($surveyQuestionInfoArr){
					$activitySurveyForm = "<a href='javascript:void(0)' onclick='openSurvey($thisEnrolEventID, $ApplicantID, \"View\");' class='tablelink'>" . $Lang['StudentRegistry']['View']. "</a>";
				}else{
					$activitySurveyForm = $Lang['General']['EmptySymbol'];
				}
			}
			
			
			$AttendanceBtn = "---";
			if ($isStudentEnrolledEvent) {
				// enrolled event
				$EnrollStatus = $eEnrollment['front']['enrolled'];
				$applied = true;
				$thisEnrolEventID = $DataArr['EnrolEventID'];
				$AttendanceBtn = "<a href=\"".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/event_attendance_mgt.php?EnrolEventID=$thisEnrolEventID&action=view&applicantId=$ApplicantID\" class=\"tablelink\">".$button_view."</a>";
			} else if ($isStudentAppliedEvent) {
				// applied event
				//$EnrollStatus = $eEnrollment['front']['applied'];				
				$TempEnrollStatus = $eEnrollment['front']['applied'];
				// preform delete
				if ($thisEnrolStatus == 1){
					$EnrollStatus = '<font color="red">'.$i_ClubsEnrollment_StatusRejected.'</font>';
				}else{
					$EnrollStatus = $eEnrollment['front']['applied']."<br>".$linterface->GET_ACTION_BTN($button_cancel, "button", "if (confirm('".$eEnrollment['js_confirm_cancel']."')) { document.form1.action='event_remove_update.php'; document.getElementById('EnrolEventID').value='".$EventArr[$i][0]."'; document.form1.submit(); } ");	
				}
			} else if ($tempMark) {
				if (
					(!((date("Y-m-d H:i", strtotime($EventArr[$i][13])) <= date("Y-m-d H:i"))&&
					  (date("Y-m-d H:i", strtotime($EventArr[$i][14])) > date("Y-m-d H:i")))) &&
					  !($EventArr[$i][13] == $EventArr[$i][14])
					) {
					$EnrollStatus = "---";
				} else {
					// normal for apply
					if($sys_custom['eEnrolment']['ReplySlip']){
						$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$thisEnrolEventID,2);
						$onloadReplySlip = '';
						if(!empty($ReplySlipRelation)){
							$onloadReplySlip = "OnloadReplySlip('".$EventArr[$i][0]."');";
						}
						$EnrollStatus = $linterface->GET_ACTION_BTN($eEnrollment['front']['enroll'], "submit", $onloadReplySlip."document.form1.action='event_add_update.php'; document.getElementById('EnrolEventID').value='".$EventArr[$i][0]."'");
					}else{
						$EnrollStatus = $linterface->GET_ACTION_BTN($eEnrollment['front']['enroll'], "submit", "document.form1.action='event_add_update.php'; document.getElementById('EnrolEventID').value='".$EventArr[$i][0]."'");
					}
				}
			}
			
			// get Role 
			$RolePar["StudentID"] = $ApplicantID;
			$RolePar["EnrolEventID"] = $EventArr[$i]['EnrolEventID'];
			$Role = $libenroll->GET_EVENT_STUDENT_ROLETITLE($RolePar);
			
						
			// time conflict
			if (($TempEnrollStatus != $eEnrollment['front']['applied'])||(!$applied)) {
				for ($j = 0; $j < sizeof($EventArr); $j++) {
					$DataArr2['EnrolEventID'] = $EventArr[$j][0];
					$checkConflictEnrolEventID = $DataArr2['EnrolEventID'];
					
					$DataArr2['StudentID'] = $ApplicantID;
					if ( ($libenroll->IS_EVENT_CRASH($EventArr[$i][0], $EventArr[$j][0])) &&
						 ($EventArr[$i][0] != $EventArr[$j][0]) && 
						 ($libenroll->IS_STUDNET_APPLIED_EVENT($DataArr2, $ExcludeRejected=1)) &&
						 ($libenroll->WITHIN_ENROLMENT_STAGE("activity")) && ($libenroll->Event_oneEnrol)
						) {
						$EnrollStatus = "<font color=\"red\">".$eEnrollment['crash_activity']."</font>";
						continue;
					}
					/*
					if ( ($libenroll->IS_EVENT_CRASH($EventArr[$i][0], $EventArr[$j][0])) &&
						 ($EventArr[$i][0] != $EventArr[$j][0]) &&
						 (!$libenroll->IS_STUDNET_ENROLLED_EVENT($DataArr)) &&
						 ($libenroll->IS_STUDNET_ENROLLED_EVENT($DataArr2))
						) {
						$EnrollStatus = $eEnrollment['conflict_warning'];
						continue;
					}
					*/
				}
				
				
				// get student's enrolled group list, check for any time crash
				#######
				$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($ApplicantID);
				for ($j = 0; $j < sizeof($StudentClub); $j++) {
					if  ($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$j][0], $EventArr[$i][0])
						 && $libenroll->Event_oneEnrol == 1
						)
					{
						$EnrollStatus = "<font color=\"red\">".$eEnrollment['crash_group']."</font>";
						continue;
					}
				}
				#######
			}

			$quota = $EventArr[$i][1];			
			if ($quota == 0) $quota = $i_ClubsEnrollment_NoLimit;
			if ($EventArr[$i][16] == 0) {
				$tieb = $i_ClubsEnrollment_Random;
				$position = $i_notapplicable;
			} else {
				$tieb = $i_ClubsEnrollment_AppTime;
				$position = $libenroll->GET_STUDENT_EVENT_POSITION($EventArr[$i][0], $ApplicantID);
			}
			
			### ReplySlip #
			if($sys_custom['eEnrolment']['ReplySlip']){
				$replySlipDisplay = Get_String_Display('');
				$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$thisEnrolEventID,2);
				if(!empty($ReplySlipRelation)){
					$replySlipDisplay = '<a href="javasrcipt:void(0);" onclick="OnloadReplySlip('.$thisEnrolEventID.')">'.'<img src="/images/2009a/eOffice/icon_resultview.gif" border="0" align="absmiddle">'.'</a>';
				}
			}
			
	?>
	<tr class="tableorangerow<?= (($displayCount % 2) + 1)?>">
		<td valign="top"><a href="javascript:void(0)" onClick="javascript:newWindow('event_info.php?EnrolEventID=<?= $EventArr[$i][0]?>&applicantId=<?=$ApplicantID?>', 9)" class="tablelink"><?= $EventArr[$i][3]?></a></td>
		<td valign="top"><span class="tabletext"><?= $TempStr?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $quota?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $tieb ?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $_applyEndTimeDisplay ?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $position ?></span></td>
		
		<?php if($libenroll->enableActivitySurveyFormRight()){?>
		<td valign="top" align="center"><span class="tabletext"><?= $activitySurveyForm?></span></td>
		<?}?>
		
		<td valign="top" align="center"><span class="tabletext"><?= $EnrollStatus?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $Role?></span></td>
		<td valign="top" align="center"><span class="tabletext"><?= $AttendanceBtn?></span></td>
		<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
		<td valign="top" align="center"><span class="tabletext"><?= $replySlipDisplay?></span></td>
		<?php }?>
	</tr>
<?

			$displayCount++;
		}
	}	# added by henry
	}
	
	if ($displayCount == 0) {
		print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"9\">".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	
	?>
</table>
<br>

</td></tr>
<tr><td>

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
</td></tr>

</table>
<br/>
<br/>
</td></tr>
<?}?>
</table>
<input type="hidden" id="EnrolEventID" name="EnrolEventID" value="">
<input type="hidden" id="ApplicantID" name="ApplicantID" value="<?= $ApplicantID?>">
</form>
<script>
function js_Update_Student_Will()
{
	var maxwant = $("#maxwant").val();
	var Category = $("#sel_category").val();
	$.post(
		"ajax_update.php",
		{
			Action:"SaveStudentWill",
			maxwant:maxwant,
			Category:Category
		},
		function(ReturnMsg){
			Get_Return_Message(ReturnMsg);
		}
	);
}
function HideDateDetail(ID,onOff){
	if(onOff){
		$('#ShowDateDetail_'+ID).show();
		$('#HideDateDetail_'+ID).hide();
		$('#timeTable_'+ID).hide();
	}else{
		$('#HideDateDetail_'+ID).show();
		$('#timeTable_'+ID).show();
		$('#ShowDateDetail_'+ID).hide();
	}
}
<?php if($libenroll->enableActivitySurveyFormRight()){?>
function openSurvey(id, ApplicantID, enrolType)
{
     //newWindow('fill.php?EnrolEventID='+id+'&RecordType=A');
    
      newWindow('fill.php?RecordType=A&EnrolEventID='+id+'&ApplicantID='+ApplicantID +'&enrolType='+enrolType);
    
}

function js_AlertSubmitSurvey(userID, EnrolEventID){
	
	$.post(
		"ajax_reload.php",
		{
			ReloadAction:"reloadSurverySubmissionStatus",
			userID:userID,
			EnrolEventID:EnrolEventID
		},
		function(ReturnMsg){
		
			if(ReturnMsg == 'NotYetFilled'){
				alert('<?php echo $Lang['General']['PleaseFillIn'] . ' ' . $Lang['eSurvey']['SurveyForm']?>');
		
			}else if (ReturnMsg == 'Filled'|| ReturnMsg == 'NotFormForFill'){
			
				$('#form1').attr("action", 'event_add_update.php');
				$('#EnrolEventID').val(EnrolEventID);
				$('#form1').submit();
			
			}
			
		}
	);
	
	

}


<?}?>

</script>
    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>