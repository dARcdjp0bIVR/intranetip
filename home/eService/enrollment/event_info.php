<?php
//using: 
/*
 * 
 * Date:    2020-04-22 [Tommy]
 *          remove $libenroll->hasAccessRight() because this checking use for eAdmin
 * 
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."home/admin/enrollment/templang.php");
intranet_auth();
intranet_opendb();

$CurrentPageArr['eServiceeEnrolment'] = 1;


if ($plugin['eEnrollment'])
{
	$EnrolEventID = IntegerSafe($EnrolEventID);
	$applicantId = IntegerSafe($applicantId);
	
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$EnrollEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);
	$MODULE_OBJ['title'] = $EnrollEventArr[3].$eEnrollment['front']['information'];
//     if ($libenroll->hasAccessRight($_SESSION['UserID']))
    if($libenroll->isUseEnrollment())
    {
    	$applicableEnrolEventIdAry = array();
		$canBeAppliedActivityInfoAry = $libenroll->GET_AVAILABLE_EVENTINFO_LIST('', $applicantId);
		$canBeAppliedActivityIdAry = Get_Array_By_Key($canBeAppliedActivityInfoAry, '0');
		
		$enrolledActivityIdAry = Get_Array_By_Key($libenroll->Get_Student_Activity_Info($applicantId), 'EnrolEventID');
		$applicableEnrolEventIdAry = array_merge($canBeAppliedActivityIdAry, $enrolledActivityIdAry);
		
		if (!in_array($EnrolEventID, (array)$applicableEnrolEventIdAry)) {
			No_Access_Right_Pop_Up();
		}
    	
    	
	    $linterface = new interface_html("popup.html");

	    ################################################################

// if ($libenroll->hasAccessRight($_SESSION['UserID']))
if($libenroll->isUseEnrollment())
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

################################################################

$linterface->LAYOUT_START();

$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$GroupArr = $libenroll->GET_EVENTCLASSLEVEL($EnrollEventArr[0]);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = false;
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {		
			 $checked = true;
			 break;
		}
	}
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr><td class=\"tabletext\">";
	if ($checked) {
		$ClassLvlChk .= $comma.$ClassLvlArr[$i][1];
		$comma = ", ";
	}
	if (($i % 10) == 9) $ClassLvlChk .= "</td></tr>";
}
$ClassLvlChk .= "</table>";

$event_details = (strip_tags($EnrollEventArr[4])==$EnrollEventArr[4]) ? str_replace("\n","<br />",$EnrollEventArr[4]) : $EnrollEventArr[4];

?>

<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $eEnrollment['add_activity']['act_name']?></td>
		<td class="tabletext"><?= $EnrollEventArr[3]?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_content']?></td>
		<td class="tabletext"><?= $event_details ?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_target']?> </td>
		<td class="tabletext"><?= $ClassLvlChk?>
			
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_age']?></td>
		<td class="tabletext">
			<? if (($EnrollEventArr[5] > 0)||($EnrollEventArr[6] > 0)) { ?>
				<?= $EnrollEventArr[6]?>
				-
				<?= $EnrollEventArr[5]?>&nbsp;<?= $eEnrollment['add_activity']['age']?>
			<? } else { ?>
				<?= $eEnrollment['no_limit']?>
			<? } ?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_gender']?></td>
		<td class="tabletext">
			<?
				if ($EnrollEventArr[7] == "M") {
					print $eEnrollment['male'];
				} else if ($EnrollEventArr[7] == "F") {
					print $eEnrollment['female']; 
				} else {
					print $eEnrollment['all'];
				}
			?>
		</td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_quota']?></td>
		<td class="tabletext"><? ($EnrollEventArr[1] == 0) ? print $i_ClubsEnrollment_NoLimit : print $EnrollEventArr[1]; ?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['app_period']?></td>
		<td class="tabletext">
			<?= date("Y-m-d H:i", strtotime($EnrollEventArr[13]))?>
			&nbsp;<?= $eEnrollment['to']?>&nbsp;
			<?= date("Y-m-d H:i", strtotime($EnrollEventArr[14]))?>
		</td>
	</tr>
	
	<?
		if (($EnrollEventArr[8] != "")||($EnrollEventArr[9] != "")||($EnrollEventArr[10] != "")||
			($EnrollEventArr[11] != "")||($EnrollEventArr[12] != ""))  {
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['attachment']?></td>
		<td class="tabletext">
		<? if ($EnrollEventArr[8] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr[8]?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr[8])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr[9] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr[9]?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr[9])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr[10] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr[10]?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr[10])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr[11] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr[11]?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr[11])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr[12] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr[12]?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr[12])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr['AttachmentLink6'] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr['AttachmentLink6']?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr['AttachmentLink6'])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr['AttachmentLink7'] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr['AttachmentLink7']?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr['AttachmentLink7'])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr['AttachmentLink8'] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr['AttachmentLink8']?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr['AttachmentLink8'])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr['AttachmentLink9'] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr['AttachmentLink9']?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr['AttachmentLink9'])?></a><br/>
		<? } ?>
		<? if ($EnrollEventArr['AttachmentLink10'] != "")  { ?>
			<a href="<?= "/file/".$EnrollEventArr['AttachmentLink10']?>" target="_blank" class="tablelink"><?= basename($EnrollEventArr['AttachmentLink10'])?></a><br/>
		<? } ?>
		</td>
	</tr>
	<?
		}
	?>
	
</table>
<br>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>