<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

switch($Action)
{
	case "SaveStudentWill":
		$ParInfo['StudentID'] = $_SESSION['UserID'];
		$ParInfo['maxwant'] = IntegerSafe($_REQUEST['maxwant']);
		$ParInfo['Category'] = IntegerSafe($_REQUEST['Category']);
	
		$success = $libenroll->Update_Enrol_Student_Max_Event($ParInfo);
		if($success)
			echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	break;
}

intranet_closedb();

?>