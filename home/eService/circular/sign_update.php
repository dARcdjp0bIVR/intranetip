<?php

#############################################
#
#   Date:   2019-04-30  Bill
#           prevent SQL Injection
#
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#
#############################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || $_SESSION['UserType']!=USERTYPE_STAFF)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CircularID = IntegerSafe($CircularID);
$actualTargetUserID = IntegerSafe($actualTargetUserID);

$val = $_POST["test"];
$val = IntegerSafe($val);
### Handle SQL Injection + XSS [END]

$MODULE_OBJ['title'] = $i_Circular_Circular;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

if ($actualTargetUserID == "")
{
    header("Location: sign.php?CircularID=$CircularID");
    exit();
}

$lform = new libform();
$lcircular = new libcircular($CircularID);

# Check admin level
$isAdmin = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        $isAdmin = true;
    }
}
$isAdmin = $isAdmin && ($adminMode==1);

# Check Reply involved
if (!$lcircular->retrieveReply($actualTargetUserID))
{
     header("Location: $intranet_httppath/close.php");
     intranet_closedb();
     exit();
}

$temp_que = str_replace("&amp;", "&", $lcircular->Question);
$queString = $lform->getConvertedString($temp_que);
$queString = trim($queString)." ";

# Check View/Edit Mode
if ($isAdmin)
{
    $js_edit_mode = ($lcircular->isHelpSignAllow);
}
else
{
    $sign_allowed = false;
    $time_allowed = false;
    if ($lcircular->isReplySigned())
    {
        if ($lcircular->isResignAllow)
        {
            $signed_allowed = true;
        }
    }
    else
    {
        $signed_allowed = true;
    }

    $today = date('Y-m-d');
    if (compareDate($today,$lcircular->DateEnd) > 0)
    {
        if ($lcircular->isLateSignAllow)
        {
            $time_allowed = true;
        }
    }
    else
    {
        $time_allowed = true;
    }
    $js_edit_mode = ($signed_allowed && $time_allowed);
}

## $ansString = $lform->getConvertedString($lcircular->answer);
$ansString = $lcircular->answer;
$ansString = str_replace("&amp;", "&", $ansString);
$ansString = $lform->getConvertedString($ansString);
$ansString .= trim($ansString)." ";

$targetType = array("",
                    $i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$lu = new libuser($actualTargetUserID);

$valid = false;
if ($js_edit_mode)
{
    if ($actualTargetUserID == "")
    {
        // do nothing
    }
    else
    {
        $valid = true;
        
        $t_ansString = intranet_htmlspecialchars(trim($aStr));
        $type = ($isAdmin? 2: 1);
		
		$star = 0;
		// $val = $_POST["test"];
		// Sign and Close
		if ($val < 3)
		{
			// Sign and Star
			if($val == 2)
			{
				$star = 1;
				?>
				<script language="javascript">
    				self.close();
    				window.opener.location.reload();
				</script>
				<?	
			}
			
		    # Update
			$sql = "UPDATE INTRANET_CIRCULAR_REPLY SET Answer = '$t_ansString',
                RecordType = '$type',RecordStatus = 2,SignerID=$UserID,DateModified=now(),HasStar = $star
                WHERE CircularID = $CircularID AND UserID = $actualTargetUserID";
			$lcircular->db_db_query($sql);
		}
    }
    
    $lcircular->synchronizeSignedCount();
    
    $lcircular->retrieveReply($actualTargetUserID);
    $ansString = str_replace("&amp;", "&", $lcircular->answer);
	$ansString = $lform->getConvertedString($ansString);
	$ansString .= trim($ansString)." ";
}
else
{
    // Unstar
	// $val = $_POST["test"];
	if($val == 3)
	{
		$star = 0;
	}
	// Add Star
	else if($val == 4)
	{
		$star = 1;
	}
	
	# Update
	$sql = "UPDATE INTRANET_CIRCULAR_REPLY SET HasStar = $star
				WHERE CircularID = $CircularID AND UserID = $actualTargetUserID";
	$lcircular->db_db_query($sql);
	?>
	
	<script language="javascript">
		self.close();
		window.opener.location.reload();
	</script>
	<?
	die();
}

if ((!$valid) && ($val < 3))
{
    header("Location: /close.php");
    exit();
}

$attachment = $lcircular->displayAttachment();

# Sign and Close
//$val = $_POST["test"];
if($val == 1)
{
	echo "<script language=\"JavaScript\">\n";
	echo "window.opener.location.reload();\n";
	echo "self.close();\n";
	echo "</script>"; 
	die();
}

//echo $sql;
//echo $val;
?>

<script language="javascript">
if(window.opener!=null) {
	window.opener.location.reload();
}

</script>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_DateStart?></span>
					</td>
					<td class='tabletext'><?=$lcircular->DateStart?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_DateEnd?></span>
					</td>
					<td class='tabletext'><?=$lcircular->DateEnd?></td>
				
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_Title?></span>
					</td>
					<td class='tabletext'><?=$lcircular->Title?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_Description?></span>
					</td>
					<td class='tabletext'><?=htmlspecialchars_decode($lcircular->Description)?></td>
				</tr>
				<? if ($attachment != "") { ?>
    				<tr valign='top'>
    					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
    						<span class='tabletext'><?=$i_Circular_Attachment?></span>
    					</td>
    					<td class='tabletext'><?=$attachment?></td>
    				</tr>
				<? } ?>
				<?
				$issuer = $lcircular->returnIssuerName();
				?>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_Issuer?></span>
					</td>
					<td class='tabletext'><?=$issuer?></td>
				</tr>
				<?
				if (!$lcircular->isReplySigned())
				{
					$statusString = "$i_Circular_Unsigned";
				}
				else
				{
					$signer = $lu->getNameWithClassNumber($lcircular->signerID);
					$signby = ($lcircular->replyType==1? $i_Circular_Signer:$i_Circular_Editor);
					$statusString = "$i_Circular_Signed <I>($signby $signer $i_Circular_At ".$lcircular->signedTime.")</I>";
				}
				?>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
						<span class='tabletext'><?=$i_Circular_SignStatus?></span>
					</td>
					<td class='tabletext'><?=$statusString?></td>
				</tr>
				<? if ($isAdmin) { ?>
    				<tr valign='top'>
    					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
    						<span class='tabletext'><?=$i_Circular_Recipient?></span>
    					</td>
    					<td class='tabletext'><?=$lu->getNameWithClassNumber($actualTargetUserID)?></td>
    				</tr>
    				<tr valign='top'>
    					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
    						<span class='tabletext'><?=$i_Circular_RecipientType?></span>
    					</td>
    					<td class='tabletext'><?=$targetType[$lcircular->RecordType]?></td>
    				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr valign='top'>
					<td class='eNoticereplytitle' align='center'><?=$i_Circular_ReplySlip?></td>
				</tr>
				<tr valign='top'>
					<td>
						<script language="javascript" src="/templates/forms/form_view.js"></script>
							<form name="ansForm" method="post" action="update.php">
							<input type=hidden name="qStr" value="">
							<input type=hidden name="aStr" value="">
						</form>
						<script language="Javascript">
							background_image = "/images/layer_bg.gif";
							myQue="<?=$queString?>";
							myAns="<?=$ansString?>";
							document.write(viewForm(myQue, myAns));
						</script>
					</td>
				</tr>
				<tr valign='top'>
						<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<form>
				<tr valign='top'>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_print, "button", "window.print();","submit2") ?>
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
					</td>
				</tr>
				
				<input type=hidden name=qStr value="<?=$queString?>">
				<input type=hidden name=aStr value="<?=$ansString?>">
				<input type=hidden name=CircularID value="<?=$CircularID?>">
				<input type=hidden name=actualTargetUserID value="<?=$actualTargetUserID?>">
				</form>
			</table>
		</td>
	</tr>
</table>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>