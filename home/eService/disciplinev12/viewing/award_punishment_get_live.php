<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=BIG5");

$RecordID = IntegerSafe($RecordID);

$ldiscipline = new libdisciplinev12();

if($flag=='detention') {
	$tempLayer = $ldiscipline->getDetentionLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='waived') {
	$tempLayer = $ldiscipline->getWaivedInfo($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='record_remark') {
	$tempLayer = $ldiscipline->getRecordRemarkLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='history') {
	$tempLayer = $ldiscipline->getAwardPunishHistoryDetailPage($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
}

echo $tempLayer;

intranet_closedb();

?>