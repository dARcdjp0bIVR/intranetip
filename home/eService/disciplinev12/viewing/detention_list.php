<?php
// Modifying by: 

###########################################################################
#
#	Date	:	2017-09-25 (Bill)	[2017-0922-1600-51206]
#				exclude deleted student detention records
#
###########################################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);

$ldiscipline = new libdisciplinev12();

# Check Access
if($_SESSION['UserType']!=2&&$_SESSION['UserType']!=3) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Detention-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser -> getChildrenList();
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID'  onchange='document.form1.submit()'", $TargetStudentID, 1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0]; 
			
			if(empty($TargetStudentID))
				$StudentIDStr = implode(",",$StudentIDAry);
			else
				$StudentIDStr = $TargetStudentID;
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[] = $ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];		
		}
	}
	else
	{
		$StudentIDAry[]=$UserID;
		$StudentIDStr = $UserID;
	}

$CurrentPage = "Viewing_Detention";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);

$linterface = new interface_html();
$SysMsg = $linterface->GET_SYS_MSG("$msg");

/*
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New")) {
	$menuBar1 = $linterface->GET_LNK_NEW("new.php", $button_new);
	$menuBar2 = $linterface->GET_LNK_IMPORT("import.php", $button_import);
	$menuBar = "<td>{$menuBar1}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
	$menuBar .= "<td>{$menuBar2}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
}
$menuBar3 = $linterface->GET_LNK_EXPORT("student_export.php?targetClass=$targetClass&targetStudent=$targetStudent&targetStatus=$targetStatus&searchString=$searchString", $button_export);
$menuBar .= "<td>{$menuBar3}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
if($sys_custom['Discipline_Detention_Export_Semester'])
{
	$menuBar4 = $linterface->GET_LNK_IMPORT("student_export_semester.php?targetClass=$targetClass&targetStudent=$targetStudent&targetStatus=$targetStatus&searchString=$searchString", $button_export." ".$i_SettingsSemester);
	$menuBar .= "<td>".$menuBar4."</td>";
}
*/

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

// Get Distinct Class
$AllClass = $ldiscipline->getClassWithSession();

if (!isset($field)) $field = 4;		// default: sort by request date
if (!isset($order)) $order = 0;		// default: desc order

$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");

$conds = "";
$conds .= " AND SES.StudentID IN (".$StudentIDStr.")";
$conds .= " AND SES.RecordStatus = 1 ";

/*if ($targetClass != "All_Classes" && $targetClass != "") {
	$conds .= " AND USR.ClassName = '$targetClass'";
}*/

if ($targetStudent == "Assigned") {
	$conds .= " AND SES.DetentionID IS NOT NULL";
}
else if ($targetStudent == "Not_Assigned") {
	$conds .= " AND SES.DetentionID IS NULL";
}

if ($targetStatus == "Detented") {
	$conds .= " AND SES.AttendanceStatus = 'PRE'";
}
else if ($targetStatus == "Absent") {
	$conds .= " AND SES.AttendanceStatus = 'ABS'";
}

$searchString = addslashes(trim($searchString));
if ($searchString != "") {
	$conds .= " AND (CONCAT(USR.ClassName, '-', USR.ClassNumber) LIKE '%$searchString%' OR $name_field LIKE '%$searchString%' OR SES.Reason LIKE '%$searchString%' OR SES.Remark LIKE '%$searchString%')";
}

if($rid!='')
{
	$conds .= " and SES.RecordID in ($rid)";
}

$sql = "SELECT
					CONCAT(USR.ClassName, '-', USR.ClassNumber) AS Class,
					$name_field AS Student,
					SES.Reason,
					IFNULL(SES.Remark, '') AS Remark,
					SUBSTRING(SES.DateInput,1,10) AS DateInput,
					SES.DetentionID,
					SES.AttendanceStatus,
					NULL,
					SES.StudentID,
					CONCAT(D.DetentionDate, D.StartTime, D.EndTime, D.Location) AS TempForSort,
					SES.RecordID
				FROM
					DISCIPLINE_DETENTION_STUDENT_SESSION SES
					LEFT JOIN DISCIPLINE_DETENTION_SESSION D ON (D.DetentionID=SES.DetentionID),
					INTRANET_USER USR
				WHERE
					SES.StudentID = USR.UserID".$conds;

$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "SES.DateInput", "TempForSort", "AttendanceStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionStudentRecordInStudentView";
$li->AllFormsData = $i_Discipline_All_Forms;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
$li->column_list .= "<td width='9%'>".$li->column($pos++, $i_Discipline_Remark)."</td>\n";
$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_Discipline_Request_Date)."</td>\n";
$li->column_list .= "<td width='41%'>".$li->column($pos++, $i_Discipline_Arranged_Session)."</td>\n";
$li->column_list .= "<td width='6%'>".$i_Discipline_Attendance."</td>\n";

# filters
/*$select_class = "<SELECT name=\"targetClass\" onChange=\"this.form.submit()\">\n";
$select_class .= "<OPTION value='All_Classes' ".($targetClass=="All_Classes"?"SELECTED":"").">$i_Discipline_All_Classes</OPTION>\n";
for ($i=0; $i<sizeof($AllClass); $i++) {
	$ClassName = $AllClass[$i];
	$select_class .= "<OPTION value='$ClassName' ".($targetClass==$ClassName?"SELECTED":"").">$ClassName</OPTION>\n";
}
$select_class .= "</SELECT>\n";
*/
$select_student = "<SELECT name=\"targetStudent\" onChange=\"this.form.submit()\">\n";
$select_student .= "<OPTION value='All_Students' ".($targetStudent=="All_Students"?"SELECTED":"").">$i_Discipline_All_Arrangement</OPTION>\n";
$select_student .= "<OPTION value='Assigned' ".($targetStudent=="Assigned"?"SELECTED":"").">$i_Discipline_Assigned</OPTION>\n";
$select_student .= "<OPTION value='Not_Assigned' ".($targetStudent=="Not_Assigned"?"SELECTED":"").">$i_Discipline_Not_Assigned</OPTION>\n";
$select_student .= "</SELECT>\n";

$select_status = "<SELECT name=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Detented' ".($targetStatus=="Detented"?"SELECTED":"").">$i_Discipline_Detented</OPTION>\n";
$select_status .= "<OPTION value='Absent' ".($targetStatus=="Absent"?"SELECTED":"").">$i_Discipline_Absent</OPTION>\n";
$select_status .= "</SELECT>\n";

$searchbar = "$select_class $select_student $select_status";

$linterface->LAYOUT_START();
?>

<br />

<script type="text/javascript">
function checkForm() {
	var tmpObj = document.getElementById('searchString');
	if(tmpObj.value=="") {
		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");
		tmpObj.focus();
		return false;
	}
	return true;
}
	
function setClickID(id) {
	document.getElementById('clickID').value = id;	
}

var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition(0,0);
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "/home/eAdmin/StudentMgmt/disciplinev12/management/detention/ajax_content.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	
	return pos_value;
}

function DisplayPosition(le, to){
	var moveLeft = (le != "undefined") ? parseInt(le) : 0;
	var moveTop = (to != "undefined") ? parseInt(to) : 0;

  	document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10+moveLeft;
  	document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop') + moveTop;
  	document.getElementById('ref_list').style.visibility='visible';
}

function Hide_Window(pos){
  	document.getElementById(pos).style.visibility='hidden';
}
</script>
	
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<form name="form1" method="post" onSubmit="return checkForm();">
	<tr><td align="right" colspan="3"><?=$SysMsg?>&nbsp;</td></tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<?=$ChildrenFilter?>
				</tr>
			</table>
		</td>
		<td align="right">
			<input name="searchString" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($searchString))?>" />
			<?=$linterface->GET_BTN($button_search, "submit", "");?>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30" width="75%"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Student_Detention_List_Instruction_Msg) ?></td>
					<td align="right" valign="middle" class="thumb_list"><a href="detention.php"><?=$i_Discipline_Calendar?></a>  | <span><?=$i_Discipline_List_View?></span></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $searchbar; ?></td>
				</tr>
			</table>
		</td>
		<!--<td align="right" valign="bottom">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:arrangeCheck();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/> <?=$i_Discipline_Arrange?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:cancelArrangeCheck();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/> <?=$i_Discipline_Cancel_Arrangement?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:checkRemove(document.form1,'RecordID[]','delete_update.php')">
										<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_delete?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:checkEdit(document.form1,'RecordID[]','edit.php')">
										<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit?></a>
								</td>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>-->
	</tr>
</table>

<?php echo $li->display(); ?>

<br />
<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="cpage" value="srec" />
<input type="hidden" name="clickID" id="clickID" value="" />
<input type="hidden" name="task" id="task" value="" />
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>