<?php
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Assign Memory
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

# Access Right Checking
$dataAry = $ldiscipline->ClassTeacherConductAccessRightChecking();

# Get Teaching Class Students
$studentIDAry = $dataAry[1];
if(sizeof($studentIDAry) > 0) {
	$StudentIDStr = implode(",", $studentIDAry);
}

# Get Base Mark
$BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');

# SQL Condition
$extraCriteria = "";
$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$ClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";

# JOIN Condition
$conds_year = ($AcademicYearID=='0')? "": " AND (con_bal.AcademicYearID='$AcademicYearID')";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? "": " AND (con_bal.IsAnnual = '1')";

# Fields
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

# Build SQL
$sql = "SELECT 
			$clsName as ClassName, 
			".getNameFieldByLang("iu.")." as UserName,
			1 as Current_Mark,
			ROUND(SUM(con_adj.AdjustMark)) as AdjustMark,
			1 as Adjusted_Mark,
			1 as GradeChar,
			DATE_FORMAT(MAX(con_adj.DateInput), '%Y-%m-%d') as LastUpdate,	
			iu.UserID,
			'$Year' as Year, 
			'".$Semester."'as Semester,
			1 as DateInput,
			COUNT(con_adj.AdjustMark),
			ycu.ClassNumber
		FROM
			INTRANET_USER iu
		LEFT OUTER JOIN 
			DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal on (iu.UserID = con_bal.StudentID $conds_semester $conds_year)
		LEFT OUTER JOIN 
			DISCIPLINE_CONDUCT_ADJUSTMENT con_adj on (iu.UserID = con_adj.studentID)
		LEFT OUTER JOIN 
			YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
		LEFT OUTER JOIN 
			YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE 
			1 $extraCriteria AND iu.UserID IN (".$StudentIDStr.")  AND iu.RecordType=2 AND iu.RecordStatus IN (0,1,2) AND yc.YearClassID != 0 AND yc.AcademicYearID = '$AcademicYearID'
		GROUP BY
			iu.UserID
		ORDER BY 
			$clsName, ycu.ClassNumber ";
$row = $ldiscipline->returnArray($sql,9);

# Get Data
$ExportArr = array();
for($i=0; $i<sizeof($row); $i++)
{
    if ($YearTermID=="" || $YearTermID==0) {
         $cond = " AND IsAnnual = 1";
         $cond2 = "";
    }
    else {
	     $cond = " AND YearTermID = '$YearTermID'";
	     $cond2 = " AND YearTermID = '$YearTermID'";
    }
    
    # Get Student Conduct Score
    $sql = "SELECT 
				ConductScore, GradeChar, Semester 
			FROM
				DISCIPLINE_STUDENT_CONDUCT_BALANCE 
			WHERE 
     			StudentID = ".$row[$i]['UserID']." and AcademicYearID = '".$AcademicYearID."' $cond";
    $tmpArr = $ldiscipline->returnArray($sql,1);
    if(sizeof($tmpArr)>0) {
 		$ConductScore = $tmpArr[0]['ConductScore'];
 		$Grade = $tmpArr[0]['GradeChar'];
	}
	else {
		$ConductScore = $BaseConductScore;
		$Grade = "";
	}
	
	# Get Conduct Score Adjustment
	if($ldiscipline->retriveConductMarkCalculationMethod() == 0)
	{
		# Display "Total Gain" & "Total Deduct"
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
			# Total Gain 
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType = 1 AND ReleaseStatus = 1 AND RecordStatus = 1 AND StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond2";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalGain = $tmpResult[0];
	    	
	    	# Total Deduct 
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType = -1 AND ReleaseStatus = 1 AND RecordStatus = 1 AND StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond2";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalDeduct = $tmpResult[0];
	    }
	    
		$sql = "SELECT AdjustMark FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond";
	}
	else
	{
		# Display "Total Gain" & "Total Deduct"
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
			# Total Gain
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType = 1 AND ReleaseStatus = 1 AND RecordStatus = 1 AND StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."'";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalGain = $tmpResult[0];
	    	
	    	# Total Deduct
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType = -1 and ReleaseStatus = 1 AND RecordStatus = 1 AND StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."'";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalDeduct = $tmpResult[0];
	    }
	    
		$TermStartDate = getStartDateOfAcademicYear($AcademicYearID, $YearTermID);
		$sub_sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."' AND TermStart <= '$TermStartDate'";
		$sql = "SELECT AdjustMark FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." and AcademicYearID = '".$AcademicYearID."' AND YearTermID IN ($sub_sql)";
	}
	$tmpArr = $ldiscipline->returnArray($sql);
	
	if(sizeof($tmpArr)>0) {
		$score = 0;
		for($y=0; $y<sizeof($tmpArr); $y++) {
			$score += $tmpArr[$y]['AdjustMark'];
		}
    }
	else {
		$score = '--';	
	}
	
	if($Grade=='') {
		$Grade = '--'; 
	}
		
	if(sizeof($tmpArr)==0 || $row[$i]['AdjustMark']=='--' || trim($row[$i]['AdjustMark']=='')) {
		$LastUpdate='--';
	}
	else {
		$LastUpdate = $ldiscipline->convertDatetoDays($row[$i]['LastUpdate']);
    }
    
 	$a = 0;
 	$ExportArr[$i][$a] = $row[$i]['ClassName'];	$a++;			// Class Name
 	$ExportArr[$i][$a] = $row[$i]['ClassNumber'];	$a++;		// Class Number
	$ExportArr[$i][$a] = $row[$i]['UserName'];	$a++;			// Student Name
	$ExportArr[$i][$a] = $ConductScore;	$a++;					// Conduct Score
	
	if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) { 
		$ExportArr[$i][$a] = $totalGain ? $totalGain : '-';	$a++; 		// Total Gain
		$ExportArr[$i][$a] = $totalDeduct ? $totalDeduct : '-';	$a++; 	// Total Deduct
		
	}
	$ExportArr[$i][$a] = $score; $a++;							// Adjustment
	$ExportArr[$i][$a] = $ConductScore + $score; $a++; 			// Adjusted Conduct Mark
	$ExportArr[$i][$a] = $Grade; $a++; 							// Conduct Grade
	$ExportArr[$i][$a] = $LastUpdate; $a++; 					// Last Update
	
}

# Column Title
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated']);
if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
	$exportColumn = array_merge($exportColumn, array($Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalGain'], $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalDeduct']));
}
$exportColumn = array_merge($exportColumn, array($Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted'], $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark'], $i_Discipline_System_Report_ClassReport_Grade,$i_Discipline_System_Discipline_Conduct_Last_Updated));

# Get Export Content
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output CSV File
$filename = 'conduct_mark.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>