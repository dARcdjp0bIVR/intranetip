<?php
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();

# Access Right Checking
$ldiscipline->ClassTeacherConductAccessRightChecking();

# Menu Highlight
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Management_ConductMark";

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# Navigation
$navigationAry[] = array($i_Discipline_System_Access_Right_Conduct_Mark, 'index.php');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

# CSV Sample
$csvFile = "conduct_mark.csv";

# CSV Data Column
$DataColumnTitleArr = array($i_ClassName, $Lang['General']['ClassNumber'], $i_general_Year, $Lang['General']['Semester'], $i_Discipline_System_Discipline_Conduct_Mark_Adjustment, $i_Discipline_PIC, $Lang['General']['Reason']);
$DataColumnPropertyArr = array(1, 1, 1, 1, 1, 1, 1);
$DataColumnRemarksArr = array("", "", "", "", "", '<span class="tabletextremark">('.$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField']['6'].')</span>');
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

# Start Layout
$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?=$htmlAry['navigation']?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr><td colspan="2">
		<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr><td>
		  		<table width="1%" class="form_table_v30">			
					<tr>
						 <td valign="top" nowrap="nowrap" class="field_title" ><?=$linterface->RequiredSymbol().$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td>
					  	 <td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				  	</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
						<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
							<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($csvFile)?>" target="_blank">
							<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
							<?=$i_general_clickheredownloadsample?>
						</td>   
					</tr>
					<tr>
						<td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>					
				  		<td class="tabletext"><?=$DataColumn?></td>
		  			</tr>
		      	</table>
			</td></tr>  
			<tr>
				<td class="tabletext"><?= $linterface->MandatoryField(); ?></td>
			</tr>
			<tr>
				<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
		</table>
	</td></tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php?Semester=$RedirectYearTermID&ClassID=$RedirectClassID'") ?>
		</td>
	</tr>
</table>

<input type="hidden" name="RedirectYearTermID" id="RedirectYearTermID" value="<?php echo $RedirectYearTermID; ?>" />
<input type="hidden" name="RedirectClassID" id="RedirectClassID" value="<?php echo $RedirectClassID; ?>" />
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>