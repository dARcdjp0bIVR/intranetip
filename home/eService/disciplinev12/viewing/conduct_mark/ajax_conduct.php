<?
# Using by : 

#####################################################
#
#	Date	:	2017-08-09	(Bill)	[2017-0321-1643-50240]
#				Copy from admin folder 
#
#####################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

if($sys_custom['eDiscipline']['yy3']) {
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

# Get Conduct Mark Calculation Method
$ConductMarkCalculationMethod = $ldisciplinev12->retriveConductMarkCalculationMethod();

# Get Year Term
$yearID = ($ajaxYear!="") ? $ajaxYear : $Year;
$YearTermID = $ajaxSemester;

# Year Term Condition
$cond = " cond_adj.StudentID = $ajaxStudentID and cond_adj.AcademicYearID = '$yearID' ";
if($YearTermID=="" || $YearTermID==0) {
	$cond .=" and cond_adj.Isannual = 1";
}
else {
	$cond .=" and cond_adj.YearTermID = '$YearTermID'";
}

# Delete Adjustment
if($ajaxType=="Remove_Adjust") {
	if($ajaxSemester=="") {
		$conds = " AND IsAnnual = 1";
	}
	else {
		$conds = " AND YearTermID = '$ajaxSemester'";
	}
	
	# DELETE [DISCIPLINE_CONDUCT_ADJUSTMENT]
	$sql = "DELETE FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE RecordID = '$ajaxRecordID' AND StudentID = '$ajaxStudentID' AND AcademicYearID = '$ajaxYear' $conds";
	$result = $ldisciplinev12->db_db_query($sql);
	if($result) {
		echo 1;
		die();
	}
}
# Get Adjustment Records
else if($ajaxType=="Adjust")
{
	# Get Previous Adjustment
	$sql = "SELECT
				cond_adj.Reason, 
				".getNameFieldByLang("iu.")." as PIC, 
				cond_adj.AdjustMark, 
				DATE_FORMAT(cond_adj.DateInput,'%Y-%m-%d') as DateInput, 
				cond_adj.RecordID, 
				cond_adj.StudentID,
				cond_adj.PICID
			FROM 
				DISCIPLINE_CONDUCT_ADJUSTMENT as cond_adj 
			LEFT JOIN 
				INTRANET_USER as iu on iu.UserID = cond_adj.PICID
			WHERE 
				$cond 
			ORDER BY 
				cond_adj.DateModified DESC";
	$resultArr = $ldisciplinev12->returnArray($sql);	
	
	# Adjustment Table
	$data = '	<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
				<tr class="tabletop">
					<td align="center" valign="middle">#</td>
					<td>'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
					<td align="left">'.$i_Discipline_PIC.'</td>
					<td>'.$eDiscipline["AdjustmentDate"].'</td>
					<td align="left">'.$i_Discipline_Reason.'</td>
					<td>&nbsp</td>
				</tr>';
	
	if(sizeof($resultArr) > 0) {
		for($a=0; $a<sizeof($resultArr); $a++)
		{
			$data .= '	<tr class="tablerow1">
							<td>'.($a+1).'</td>
						 	<td>'.$resultArr[$a]['AdjustMark'].'</td>
						 	<td>'.$resultArr[$a]['PIC'].'</td>
						 	<td>'.$resultArr[$a]['DateInput'].'</td>
							<td>'.$resultArr[$a]['Reason'].'</td>';
			if($resultArr[$a]['PICID'] == $UserID) {
				$data .= ' 	<td><div class="table_row_tool"><a href="javascript:confirmDeleteAdjust(\''.$resultArr[$a]['RecordID'].'\',\''.$resultArr[$a]['StudentID'].'\',\''.$Year.'\',\''.$Semester.'\')" class="delete_dim">&nbsp;</a></div></td>';
			}
			else {
				$data .= ' 	<td>&nbsp</td>';
			}
			$data .= '	</tr>';
		}
	}
	else {
		$data .= "<tr><td colspan='5' align='center'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['NoRecordAtThisTerm']."</td></tr>";
	}
}
# Get Merit Records
else if ($ajaxType=='Merit')
{
	# Get Student Merits
	$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID, "", "", $yearID, $YearTermID);
	
	# Check if any Misconduct
	$withGMrecords = false;
	if($sys_custom['eDiscipline']['GMConductMark']){
		$StudentGMArr = $ldisciplinev12->retrieveStudentACCUConductScoreChangeRecord($ajaxStudentID, $yearID, $YearTermID);
		$withGMrecords = count((array)$StudentGMArr) > 0;
	}
	
	# Demerit Table
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
	if(!$sys_custom['eDiscipline']['GMConductMark'] || ($sys_custom['eDiscipline']['GMConductMark'] && sizeof($StudentMeritArr) > 0))
	{
		if($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords) { 
			$data .= '	<tr class="tablerow3"><td colspan="5">'.$eDiscipline['Award_and_Punishment'].'</td></tr>';
		}
		$data .= '	<tr class="tabletop">
						<td align="center" valign="middle">#</td>
						<td align="left">'.$i_Discipline_Reason.'</td>
						<td align="left">'.$i_Discipline_PIC.'</td>
						<td >'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
						<td >'.$Lang['eDiscipline']['EventDate'].'</td>
					</tr>';
					
		for($a=0; $a<sizeof($StudentMeritArr); $a++)
		{
			$data .= '	<tr class="tablerow1">
							<td>'.($a+1).'</td>
						 	<td>'.$StudentMeritArr[$a]['Remark'].'</td>
						 	<td>'.$StudentMeritArr[$a]['PIC'].'</td>';
			if (
				$sys_custom['eDiscipline']['yy3'] && 
				($StudentMeritArr[$a]['OverflowConductMark']=='1' || $StudentMeritArr[$a]['OverflowMeritItemScore']=='1'  || $StudentMeritArr[$a]['OverflowTagScore']=='1') && 
				$StudentMeritArr[$a]['ScoreChange']==0 
			) {
				$data .= '	<td>'.$ldiscipline_ui->displayMaskedMark($StudentMeritArr[$a]['ScoreChange']).'</td>';
			}
			else {
				$data .= '	<td>'.$StudentMeritArr[$a]['ScoreChange'].'</td>';
			}
			$data .= '		<td>'.$StudentMeritArr[$a]['RecordDate'].'</td>
						</tr>';
		}
	}
	
	# Misconduct Table
	if($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords)
	{
		if(($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords && sizeof($StudentMeritArr) > 0)) {
			$data .= '	<tr class="tablerow1"><td colspan="5">&nbsp;</td></tr>';
		}
		$data .= '	<tr class="tablerow3"><td colspan="5">'.$eDiscipline['Good_Conduct_and_Misconduct'].'</td></tr>
				 	<tr class="tabletop">
						<td align="center" valign="middle">#</td>
						<td align="left">'.$i_Discipline_Reason.'</td>
						<td align="left">'.$i_Discipline_PIC.'</td>
						<td >'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
						<td >'.$Lang['eDiscipline']['EventDate'].'</td>
				 	</tr>';
		
		for($a=0; $a<sizeof($StudentGMArr); $a++)
		{
			$data .= '	<tr class="tablerow1">
						 	<td>'.($a+1).'</td>
						 	<td>'.$StudentGMArr[$a]['RecordName'].'</td>
						 	<td>'.$StudentGMArr[$a]['PICName'].'</td>
							<td>'.$StudentGMArr[$a]['GMConductScoreChange'].'</td>
							<td>'.$StudentGMArr[$a]['RecordDate'].'</td>
						</tr>';
		}
	}
}

$data .= '	</table>';

# Ajax Layer
$div_layer = '<div id="'.$targetDivID.'" style="position:absolute; width:380px; z-index:1; ">
		 		<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr><td height="19">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
								<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
								<td width="19" height="19"><a href="javascript:Hide_Window(\''.$targetDivID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
							</tr>
						</table>
					</td></tr>
					<tr><td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
								<td align="left" bgcolor="#FFFFF7">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  			<tr><td height="150" align="left" valign="top">'.$data.'</td></tr>
										<tr><td>
											<br>
											<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
									      		<tr><td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td></tr>
								     		</table>
										</td></tr>
									</table>
								</td>
								<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
						  	</tr>
						  	<tr>
								<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
								<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
								<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
						  	</tr>
						</table>
					</td></tr>
				</table>
			</div>';
echo $div_layer;
?>