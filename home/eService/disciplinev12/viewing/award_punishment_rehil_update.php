<?php
// Modifying by: 
/*
* 	Date	:	2015-07-23 (Bill)	[2015-0611-1642-26164]
* 	Detail	:	Create File
*/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eDiscipline']['CSCProbation']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$ldiscipline = new libdisciplinev12();

// Get Post data
$id = IntegerSafe($id);
$teacher_id = IntegerSafe($FollowUpTeacher);
$reh_info = nl2br($reh_info);

// Update merit record
if($id!="" && $teacher_id!="" && $reh_info!= ""){
	$sql = "UPDATE DISCIPLINE_MERIT_RECORD 
				SET RehabilPIC='$teacher_id', RehabilReason='$reh_info', RehabilStatus='0', RehabilSubmitDate=now(), ModifiedBy='".$_SESSION['UserID']."', DateModified=now() 
			WHERE RecordID='$id'";
	$success = $ldiscipline->db_db_query($sql);
}

$msg = $success? "update" : "update_failed";
header("Location: award_punishment.php?msg=".$msg);

intranet_closedb();

?>