<?php
// Modifying by: 

###########################################################################
#
#	Date	:	2017-09-25 (Bill)	[2017-0922-1600-51206]
#				exclude deleted student detention records
#
#	Date	:	2014-02-27 (YatWoon)
#				add student checking, is the student selected is belongs to that parent?
#
###########################################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$cmonth = IntegerSafe($cmonth);
$DetentionID = IntegerSafe($DetentionID);

$ldiscipline = new libdisciplinev12();

# Check access right (View page)
if($_SESSION['UserType']!=2&&$_SESSION['UserType']!=3) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Detention-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);
		$ChildrenAry = $luser -> getChildrenList();
		
		if(!empty($TargetStudentID))
		{
			#################################################
			# Check the child is belongs to parent or not
			#################################################
			$valid_child = false;
			foreach($ChildrenAry as $k => $d)
			{
				if($d['StudentID'] == $TargetStudentID) $valid_child = true;	
			}
			if(!$valid_child)
			{
				$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID'  onchange='document.FormCalendar.submit()'", $TargetStudentID, 1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0]; 

			if(empty($TargetStudentID))
				$StudentIDStr = implode(",",$StudentIDAry);
			else
				$StudentIDStr = $TargetStudentID;
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[] = $ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];		
		}
		
		# Gen Student Name Arr ($StudentNameArr[$StudentID]= $StudentName)
		foreach($ChildrenAry as $Child)
		{
			list($ChildStudentID,$ChildStudentName) = $Child;
			$StudentNameArr[$ChildStudentID] = $ChildStudentName;
		}
	}
	else
	{
		$StudentIDAry[]=$UserID;
		$StudentIDStr = $UserID;
	}

$li = new libdbtable();

$CurrentPage = "Viewing_Detention";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);

$linterface = new interface_html();
$SysMsg = $linterface->GET_SYS_MSG("$msg");

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

// Previous month and next month
if ($CurrentMonth == 1) {
	$PreviousStr = ($CurrentYear-1)."12";
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
}
else if ($CurrentMonth == 12) {
	$PreviousStr = $CurrentYear.($CurrentMonth-1);
	$NextStr = ($CurrentYear+1)."01";
}
else {
	$PreviousStr = $CurrentYear.str_pad(($CurrentMonth-1), 2, "0", STR_PAD_LEFT);
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
}

// Empty box on first week
if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 0) {	// sun
	$FirstWeekday = 2;
	$PreEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 1) {	// mon
	$FirstWeekday = 1;
	$PreEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 2) {	// tue
	$FirstWeekday = 1;
	$PreEmptyBox = 1;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 3) {	// wed
	$FirstWeekday = 1;
	$PreEmptyBox = 2;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 4) {	// thu
	$FirstWeekday = 1;
	$PreEmptyBox = 3;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 5) {	// fri
	$FirstWeekday = 1;
	$PreEmptyBox = 4;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 6) {	// sat
	$FirstWeekday = 1;
	$PreEmptyBox = $ldiscipline->Detention_Sat ? 5 : 0;
};

// Empty box on last week
if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 0) {	// sun
	$PostEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 1) {	// mon
	$PostEmptyBox = 4;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 2) {	// tue
	$PostEmptyBox = 3;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 3) {	// wed
	$PostEmptyBox = 2;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 4) {	// thu
	$PostEmptyBox = 1;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 5) {	// fri
	$PostEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 6) {	// sat
	$PostEmptyBox = 0;
};

# Select current user record only
$conds = " AND b.StudentID IN (".$StudentIDStr.") ";

$sql = "SELECT 
			a.DetentionID, DetentionDate, StartTime, EndTime, Location, AttendanceStatus, RecordID, StudentID
		FROM 
			DISCIPLINE_DETENTION_SESSION a 
		LEFT JOIN 
			DISCIPLINE_DETENTION_STUDENT_SESSION b ON (a.DetentionID = b.DetentionID)
		WHERE 
			MONTH(DetentionDate) = '$CurrentMonth' AND YEAR(DetentionDate) = '$CurrentYear' AND RecordStatus = 1 
			$conds
		ORDER BY 
			DetentionDate, StartTime, EndTime, Location";
$result = $ldiscipline->returnArray($sql, 7);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDRec[], $DetentionDateRec[], $StartTimeRec[], $EndTimeRec[], $LocationRec[], $AttendanceStatus[], $RecordIDRec[],$DetetionStudentID[]) = $result[$i];
}

// Get the names of PIC for current month
$result = $ldiscipline->getDetentionPICByYearMonth($CurrentYear, $CurrentMonth);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDPICRec[], $PICRec[], $NameRec[]) = $result[$i];
}

for ($i=0; $i<sizeof($DetentionIDRec); $i++) {
	$DisplayDetentionID[$DetentionDateRec[$i]][] = $DetentionIDRec[$i];
	$DisplayPeriod[$DetentionDateRec[$i]][] = substr($StartTimeRec[$i],0,5)."-".substr($EndTimeRec[$i],0,5);
	$DisplayLocation[$DetentionDateRec[$i]][] = intranet_htmlspecialchars($LocationRec[$i]);
	$DisplayAttendanceStatus[$DetentionDateRec[$i]][] = ($AttendanceStatus[$i]== NULL?"--":($AttendanceStatus[$i]=="PRE"?$i_Discipline_Present:$i_Discipline_Absent));
	$DisplayRecordID[$DetentionDateRec[$i]][] = $RecordIDRec[$i]; 
	if($_SESSION['UserType']==3)
		$DisplayStudentName[$DetentionDateRec[$i]][] = $StudentNameArr[$DetetionStudentID[$i]];
	
	$TmpPIC = array();
	for ($j=0; $j<sizeof($DetentionIDPICRec); $j++) {
		if ($DetentionIDRec[$i] == $DetentionIDPICRec[$j]) {
			$TmpPIC[] = $NameRec[$j];
		}
	}
	$DisplayPIC[$DetentionDateRec[$i]][] = implode(", ", $TmpPIC);
}

# Layer
$layer_html = "<div id=\"ref_list\" style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";

$linterface->LAYOUT_START();
?>

<script language="javascript">
	function changeMonth(targetMonth){
		obj = document.getElementById('cmonth');
		obj.value = targetMonth;
		document.FormCalendar.submit();
	}
	
	function checkForm(){
		for(var i=1;i<=<?=sizeof($student)?>;i++){
			if (document.getElementById('TextReason'+i).value=='') {
				alert('<?=$i_alert_pleasefillin.$i_Discipline_Reason?>');
				return false;
			}
		}
		document.getElementById('submit_flag').value='YES';
		return true;
	}
	
	// remark layer
	var ClickID = '';
	var callback_show_ref_list = {
		success: function ( o )
		{
			var tmp_str = o.responseText;
			document.getElementById('ref_list').innerHTML = tmp_str;
			DisplayPosition();
		}
	}
	
	function show_ref_list(recordID,click)
	{
		ClickID = click;
		document.getElementById('recID').value = recordID;
		
		obj = document.form1;
		YAHOO.util.Connect.setForm(obj);
		
		var path = "ajax_gm.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
	}
	
	function Hide_Window(pos) {
	  document.getElementById(pos).style.visibility='hidden';
	}

	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		
		while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
		
		return pos_value;
	}
	
	function DisplayPosition(){
		document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
		document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop') -20;
		document.getElementById('ref_list').style.visibility='visible';
	}
</script>

<br />
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<form name="FormCalendar" method="post" onsubmit="return checkForm();" action="detention.php">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><?=$ChildrenFilter?></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30" width="75%"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Student_Detention_Calentar_Instruction_Msg) ?></td>
					<td align="right" valign="middle" class="thumb_list"><span><?=$i_Discipline_Calendar?></span> | <a href="detention_list.php"><?=$i_Discipline_List_View?></a></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td width="70%" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<span class="icalendar_title">
							<a href="javascript:changeMonth('<?=$PreviousStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Previous?>"></a>
<?=date("F", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?> <?=date("Y", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?>
							<a href="javascript:changeMonth('<?=$NextStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Next?>"></a>
						</span>
					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#d2d2d2">
				<tr>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[1]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[2]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[3]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[4]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[5]?></td>
					<? if($ldiscipline->Detention_Sat) {?>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[6]?></td>
					<? } ?>
				</tr>
				<tr height="140">
<?
	for ($i=0; $i<$PreEmptyBox; $i++) {
?>
					<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
<?
	}
	$FirstWeekFlag = true;
	for ($j=$FirstWeekday; $j<=$NoOfDays; $j++) {
		if ((date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == 1) && !$FirstWeekFlag) {
?>
				<tr height="140">
<?
		}

		if($ldiscipline->Detention_Sat)
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0);
		else
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0) && (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 6);
		if ($if_condition) 
		{
?>
					<td valign="top" bgcolor="#FFFFFF"<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"background-color:#FCFFD7\"":""?>><strong<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"color:#FFFFFF; background-color:#D97200; padding:2px \"":""?>><?=$j?></strong><br>
<?
			$TmpDate = $CurrentYear."-".$CurrentMonth."-".str_pad($j, 2, "0", STR_PAD_LEFT);
			$TmpArray = $DisplayPeriod[$TmpDate];
			if (is_array($TmpArray)) {
				foreach ($TmpArray as $TmpKey => $TmpValue) {
					if ($DisplayAttendanceStatus[$TmpDate][$TmpKey] == $i_Discipline_Absent) {
						$TableClass = "detent_absent";
						//$AssignFlag = false;
					}
					else if ($TmpDate < $CurrentDay) {
						$TableClass = "detent_null";
						//$AssignFlag = true;
					}
					else {
						$TableClass = "detent_future";
					}

						$ImgSrcEclass = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_eclass.gif";
						$ImgSrcTeacher = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_teacher.gif";
						$ImgSrcTime = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_time.gif";
						$ImgSrcTakeAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_takeattendance.gif";
						$imgSrcReason = $image_path."/".$LAYOUT_SKIN."/icon_remark.gif";
						$ImgSrcStudent = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_student.gif";
?>
						<table width="100%" border="0" cellpadding="2" cellspacing="0" class="<?=$TableClass?>">
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcStudent?>" width="20" height="20" border="0" align="absmiddle">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td nowrap><?=$DisplayStudentName[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcEclass?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Location?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td nowrap><?=$DisplayLocation[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcTeacher?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Duty_Teacher?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td nowrap><?=$DisplayPIC[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcTime?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Time?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td><?=$DisplayPeriod[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcTakeAttend?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Attended?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td><?=$DisplayAttendanceStatus[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$imgSrcReason?>" width="20" height="20" border="0" align="absmiddle" title="<?=$eDiscipline['Detention_Reason']?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td><a class="tablelink" href="javascript:;" onclick="show_ref_list('<?=$DisplayRecordID[$TmpDate][$TmpKey]?>','cl_click<?=$j?>')"><?=$eDiscipline['Detention_Reason']?></a><span id="cl_click<?=$j?>">&nbsp;</span></td>
										</tr>
									</table>
								</td>
							</tr>
<?
?>
						</table>
<?
				}
			}
?>
					</td>
<?
		}
		if (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == ($ldiscipline->Detention_Sat ? 6 : 5)) {
			$FirstWeekFlag = false;
?>
				</tr>
<?
		}
	}
	for ($i=0; $i<$PostEmptyBox; $i++) {
?>
					<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
<?
		if ($i==$PostEmptyBox-1) {
?>
				</tr>
<?
		}
	}
?>
			</table>
		</td>
	</tr>
</table>

<?=$layer_html?>

<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
<input type="hidden" id="DetentionID" name="DetentionID">
</form>

<form id="form1" name="form1" action="POST">
	<input type="hidden" id="recID" name="recID"/>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>