<?php
# Modifying by : 

###### Change Log [Start] ######
#
#	Date	:	2017-06-28 (Bill)
#				added Column Study Score and Activity Score 
#				added filter Tag Type 	($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'])
#				fixed losing search string when change filter
#
#	Date	:	2016-07-04 (Bill)	[2016-0526-1018-39096]
#				add Class and PIC drop down list for teacher
#
#	Date:		2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added remarks for prefix style of deleted teacher account
#
#	Date	:	2015-07-31 (Bill)	[DM#2879]
#				fixed search box miss " after searching
#
# 	Date	:	2015-07-28 (Bill)	[2015-0611-1642-26164]
# 				display "Rehabilitation Program" in column "Action"
#
#	Date	:	2015-06-29 (Bill)	[2015-0629-1054-29066]
#				fixed filters - Merit Type and Term not work when using keyword search
#
#	Date	:	2014-06-06 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added record type button [Generate from GM]
#
#	Date	:	2014-02-27 (YatWoon)
#				add student checking, is the student selected is belongs to that parent?
#
#	Date	:	2010-09-30 (Henry)
#				display "Sign Notice" in action column rather than "Send Notice"
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	allow Staff (Teacher) can view eService > eDiscipline
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


intranet_auth();
intranet_opendb();

$TargetStudentID = IntegerSafe($TargetStudentID);
$SchoolYear = IntegerSafe($SchoolYear);
$approved = IntegerSafe($approved);
$waived = IntegerSafe($waived);
$MeritType = IntegerSafe($MeritType);
$SchoolYear2 = IntegerSafe($SchoolYear2);
$waived2 = IntegerSafe($waived2);
$approved2 = IntegerSafe($approved2);
$waitApproval2 = IntegerSafe($waitApproval2);
$released2 = IntegerSafe($released2);
$rejected2 = IntegerSafe($rejected2);
$MeritType2 = IntegerSafe($MeritType2);
$id = IntegerSafe($id);

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

if($ldiscipline->Display_ConductMarkInAP) {
	$sortEventDate = 6;			# Event Date is the 6th SQL column
} else {
	$sortEventDate = 5;			# Event Date is the 5th SQL column
}

if ($page_size_change == 1)	# change "num per page"
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
} else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
} else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}


# Check access right (View page)
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3 && $_SESSION['UserType']!=1)
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-View") && $_SESSION['UserType']!=1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$currentYearID = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;

if($_SESSION['UserType']==1) {	# check teacher access right
	if(!$ldiscipline->checkTeacherAccessEserviceRight($UserID)) {	
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
	}
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$recordType = ($MeritType!="1" && $MeritType != "-1" && $MeritType != "-2") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments."</a>";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$recordType .= " | ";
	$recordType .= ($MeritType == "-2" ? $Lang['eDiscipline']['GenerateFromGM'] : '<a href="javascript:void(0);" onclick="reloadForm(\'-2\');">'.$Lang['eDiscipline']['GenerateFromGM'].'</a>');
}

###### Initial StudentIDAry ######
	#### Gen ChildrenFilter
	$StudentIdAry = Array();
	if($_SESSION['UserType']==3)	# parent view
	{
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser($UserID);


		$ChildrenAry = $luser -> getChildrenList();
		
		if(!empty($TargetStudentID))
		{
			#################################################
			# Check the child is belongs to parent or not
			#################################################
			$valid_child = false;
			foreach($ChildrenAry as $k => $d)
			{
				if($d['StudentID'] == $TargetStudentID) $valid_child = true;	
			}
			if(!$valid_child)
			{
				$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		if(count($ChildrenAry)>1)
		{
			$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='TargetStudentID' id='TargetStudentID'  onchange='document.form1.submit()'", $TargetStudentID, 1);
			for($i=0; $i<count($ChildrenAry); $i++)
				$StudentIDAry[$i] = $ChildrenAry[$i][0]; 

			if(empty($TargetStudentID))
				$StudentIDStr = implode(",",$StudentIDAry);
			else
				$StudentIDStr = $TargetStudentID;
		}
		else
		{
			$ChildrenFilter = $ChildrenAry[0][1];
			$StudentIDAry[]=$ChildrenAry[0][0];
			$StudentIDStr = $ChildrenAry[0][0];		
			
		}
	}
	else if($_SESSION['UserType']==2)	# student view
	{
		$StudentIDAry[]=$UserID;
		$StudentIDStr = $UserID;
	} 
	else # teacher view
	{
		$studentIDAry = array();
		$studentIDAry2 = array();
		
		$studentIDAry = $ldiscipline->getStudentListByClassTeacherID("", $UserID, $currentYearID);
		$studentIDAry2 = $ldiscipline->getStudentListBySubjectTeacherID("", $UserID, $currentYearID);
		
		$newStudentAry = array_merge($studentIDAry, $studentIDAry2);
		$newStudentAry = array_unique($newStudentAry);
		
		if(sizeof($newStudentAry)>0)
			$StudentIDStr = implode(',', $newStudentAry);
		else 
			$StudentIDStr = "''";
	}
	

######### Conditions #########
$conds = "";
$extraTable = "";

// [2016-0526-1018-39096] Teacher only
if($_SESSION['UserType']==1)
{
	# Class #
	if ($targetClass != '' && $targetClass!="0")
	{
	    if(is_numeric($targetClass)) {
	    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
	    	$temp = $ldiscipline->returnVector($sql);
	    	$conds .= (sizeof($temp)>0) ? " AND g.YearClassID IN (".implode(',', $temp).")" : ""; 
		}
	    else {
	    	$conds .= " AND g.YearClassID=".substr($targetClass,2);
	    }
	}
}

# MeritType #
if($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $MeritType == "-2"){
	$conds .= " AND a.fromConductRecords='1' ";
}
else if ($MeritType != '' && $MeritType != 0) {
    $conds .= " AND a.MeritType = $MeritType";
}
else {
	$conds .= " AND (a.MeritType = 1 or a.MeritType=-1 or a.MeritType IS NULL)";
	$MeritType = 0;	
}

$yearName = $ldiscipline->getAcademicYearNameByYearID($SchoolYear);

# Year Menu #
$yearSelected = 0;
$currentYearInfo = Get_Current_Academic_Year_ID();

$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;
$yearArr = $ldiscipline->getAPSchoolYear($SchoolYear);

$selectSchoolYear = "<select name='SchoolYear' id='SchoolYear' onChange='document.form1.semester.value=\"\";reloadForm($MeritType)'>";
/*
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
*/
for($i=0; $i<sizeof($yearArr); $i++) {
	$selectSchoolYear .= "<option value='".$yearArr[$i][0]."'";
	if($SchoolYear==$yearArr[$i][0]) {
		$selectSchoolYear .= " SELECTED";
	}
	if(Get_Current_Academic_Year_ID()==$yearArr[$i][0]) {
		$yearSelected = 1;
	}
	$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
}
if($yearSelected==0) {
	$selectSchoolYear .= "<option value='".Get_Current_Academic_Year_ID()."'";
	$selectSchoolYear .= (Get_Current_Academic_Year_ID()==$SchoolYear) ? " selected" : "";
	$selectSchoolYear .= ">".$ldiscipline->getAcademicYearNameByYearID(Get_Current_Academic_Year_ID())."</option>";
}

$selectSchoolYear .= "</select>";

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = '$SchoolYear' AND g.AcademicYearID='$SchoolYear'";	
}
//$extraConds = " AND g.AcademicYearID = a.AcademicYearID";

# Semester Menu #
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$currentYearID' ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);
$SemesterMenu = "<select name='semester' id='semester' onChange='reloadForm($MeritType)'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$SemesterMenu .= "<option value='$id'";
	$SemesterMenu .= ($semester==$id) ? " selected" : "";
	$SemesterMenu .= ">$semName</option>";
}
$SemesterMenu .= "</select>";

if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND a.YearTermID = '$semester'";	
}

// [2016-0526-1018-39096] Teacher only
if($_SESSION['UserType']==1)
{
	# Class Menu #
	$select_class = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($MeritType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
	
	# PIC Menu # 
	if($semester != '' && $semester != 'WholeYear') $thisSemester = $semester;
	$PICArray = $ldiscipline->getPICInArray("DISCIPLINE_MERIT_RECORD", $currentYearID, $thisSemester);
	if(sizeof($PICArray)==0)
		$PICArray = array();
	$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"reloadForm($MeritType)\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");
	
	# PIC Condition # 
	if($pic != "") 
		$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";

	// [2015-0120-1200-33164] # Merit Item Tag Type #
	if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function']) {
		$itemTagArray = array();
		$itemTagArray[] = array("1", $eDiscipline['Conduct_Mark']);
		$itemTagArray[] = array("2", $i_Discipline_System_Subscore1);
		$ItemTagSelection = getSelectByArray($itemTagArray, " name=\"itemTag\" onChange=\"reloadForm($MeritType)\"", $itemTag, 0, 0, "".$i_Discipline_System_Award_Punishment_All_Records."");
		
		if($itemTag > 0) {
			$extraTable .= " LEFT JOIN DISCIPLINE_AP_ITEM_TAG ait ON (a.ItemID = ait.APItemID AND ait.TagID = '$itemTag') ";
			$conds .= " AND ait.TagID = '$itemTag' ";
		}
	}
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}

# Approved #
if($approved == 1) {
	$approvedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED.")";
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED.")";
}

# Unreleased #
if($unreleased == 1) {
	$unreleasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL)";
}

# Released #
if($released == 1) {
	$releasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED.")";
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED.")";	
}


#Only released records are shown to students / parent
if($_SESSION['UserType']!=1) {
	$conds2 .= ($conds2=="") ? "(" : " AND ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Locked #
/*
if($locked == 1) {
	$lockedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}
*/

$conds2 = ($conds2 != "") ? " AND ".$conds2.")" : "";

##############

$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

# $access_level = $ldiscipline->access_level;
$li = new libdbtable2007($field, $order, $pageNo);

$const_status_pending = 0;

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Award\'>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Punishment\'>";
$pendingImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif width=20 height=20 align=absmiddle title=\'".$i_Discipline_System_Award_Punishment_Pending."\' border=0>";
$rejectImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif align=absmiddle title=Rejected border=0>";
$releasedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif width=20 height=20 align=absmiddle title=\'".$i_Discipline_Released_To_Student."\' border=0>";
$approvedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif width=20 height=20 align=absmiddle title=Approved border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";

// [2015-0611-1642-26164]
$RehabilDisplay = $sys_custom['eDiscipline']['CSCProbation']? " ,a.RehabilPIC" : "";

$SubScoreDisplay = $ldiscipline->UseSubScore? " a.SubScore1Change, " : "";
$SubScoreDisplay = $ldiscipline->UseActScore? " a.SubScore1Change, a.SubScore2Change, " : $SubScoreDisplay;

if($intranet_session_language=="en")
	{
		$meritStr = "CONCAT(a.ProfileMeritCount,' ',
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END, '(s)')";
	}
	else
	{
		$meritStr = "CONCAT(
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END,
					cast(a.ProfileMeritCount as char), '". $Lang['eDiscipline']['Times'] ."', ' '
					)";
	}
	
if($s != "") {		# filter by Searching

	$s = addslashes(trim($s));
	
	$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum, 
			$student_namefield as EnglishName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,";
	if($ldiscipline->Display_ConductMarkInAP) {			
			$sql .= "a.ConductScoreChange,";
	}
	$sql .= $SubScoreDisplay;
	$sql .= "
			if(a.ItemID=0, 
			CONCAT('<a href=\"javascript:go_detail(',a.RecordID,')\">--</a>'),
			CONCAT('<a title=\"',REPLACE(c.ItemCode,'\"','&quot;'),' - ',REPLACE(c.ItemName,'\"','&quot;'),'\" href=\"javascript:go_detail(',a.RecordID,')\">',c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'),'</a>')
			) as item,
			a.RecordDate,
			PICID, 
			a.fromConductRecords as reference,
			a.TemplateID as action,
			LEFT(a.DateModified,10),
			a.ReleaseStatus as status,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
			a.WaivedBy, 
			a.RecordStatus,
			a.RecordID,
			a.CaseID,
			a.NoticeID,
			b.UserID,
			a.Remark
			$RehabilDisplay
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
					LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
					LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
					$extraTable
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID='$SchoolYear'
					AND (
						g.ClassTitleEN like '%$s%' 
						OR g.ClassTitleB5 like '%$s%'
						OR f.ClassNumber like '%$s%'
						OR $student_namefield like '%$s%'
						OR c.ItemCode like '%$s%'
						OR c.ItemName like '%$s%'
						OR a.RecordDate like '%$s%'
					)
					AND (a.MeritType=1 OR a.MeritType=-1)
					AND a.AcademicYearID='$SchoolYear'
					$conds
	";

//	$layerIDArr = $ldiscipline->loadLayerDetail($s, $conds);
	
} 
else {				# filter by option
	$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum, 
			$student_namefield as EnglishName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,";
	if($ldiscipline->Display_ConductMarkInAP) {			
			$sql .= "a.ConductScoreChange,";
	}
	$sql .= $SubScoreDisplay;
	$sql .= "
			if(a.ItemID=0, 
			CONCAT('<a href=\"javascript:go_detail(',a.RecordID,')\">--</a>'),
			CONCAT('<a title=\"',REPLACE(c.ItemCode,'\"','&quot;'),' - ',REPLACE(c.ItemName,'\"','&quot;'),'\" href=\"javascript:go_detail(',a.RecordID,')\">',c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'),'</a>')
			) as item,
			a.RecordDate,
			PICID, 
			a.fromConductRecords as reference,
			a.TemplateID as action,
			LEFT(a.DateModified,10),
			a.ReleaseStatus as status,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
			a.WaivedBy, 
			a.RecordStatus,
			a.RecordID,
			a.CaseID,
			a.NoticeID,
			b.UserID,
			a.Remark
			$RehabilDisplay
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
					LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID )
					LEFT OUTER JOIN ACADEMIC_YEAR as h ON (h.AcademicYearID=g.AcademicYearID)
					$extraTable			
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID='$SchoolYear'
					$conds
	";
}

//show user record only
$conds2.= " AND a.StudentID IN (".$StudentIDStr.") ";
$sql .= ($conds2 != "") ? " ".$conds2 : "";

//$li->field_array = array("ClassNameNum", "b.EnglishName", "meritImg", "record", "item", "a.RecordDate", "PICID", "reference", "action", "a.DateModified", "status");
$li->field_array[] = "concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))";
$li->field_array[] = "b.EnglishName";
$li->field_array[] = "meritImg";
$li->field_array[] = "record";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->field_array[] = "ConductScoreChange";
}
if($ldiscipline->UseSubScore) {
	$li->field_array[] = "SubScore1Change";
}
if($ldiscipline->UseActScore) {
	$li->field_array[] = "SubScore2Change";
}
$li->field_array[] = "item";
$li->field_array[] = "a.RecordDate";
$li->field_array[] = "PICID";
$li->field_array[] = "reference";
$li->field_array[] = "action";
$li->field_array[] = "a.DateModified";
$li->field_array[] = "status";

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_array[] = "0";
}
if($ldiscipline->UseSubScore) {
	$li->column_array[] = "0";
}
if($ldiscipline->UseActScore) {
	$li->column_array[] = "0";
}
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->wrap_array[] = "0";
}
if($ldiscipline->UseSubScore) {
	$li->wrap_array[] = "0";
}
if($ldiscipline->UseActScore) {
	$li->wrap_array[] = "0";
}

$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $i_Discipline_Reason2)."</td>\n";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $eDiscipline['Conduct_Mark'])."</td>\n";
}
if($ldiscipline->UseSubScore) {
	$li->column_list .= "<td width='60' nowrap>".$li->column($pos++, $i_Discipline_System_Subscore1)."</td>\n";
}
if($ldiscipline->UseActScore) {
	$li->column_list .= "<td width='60' nowrap>".$li->column($pos++, $Lang['eDiscipline']['ActivityScore'])."</td>\n";
}
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_Item_Code_Item_Name)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_RecordDate)."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$i_Profile_PersonInCharge."</td>\n";$pos++;
//$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_Award_Punishment_Reference)."</td>\n";
//$li->column_list .= "<td>".$li->column($pos++, $eDiscipline["Action"])."</td>\n";
$li->column_list .= "<td width='80' nowrap>$i_Discipline_System_Award_Punishment_Reference</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td>".$i_Discipline_System_Discipline_Status."</td>\n";
/*
$categoryItemLocation = 4;
$PICLocation = 6;
$historyLocation = 7;
$actionLocation = 8;
$checkboxLocation = 11;
$StatusLocation = 12;
$recordIDLocation = 13;
$caseIDLocation = 14;
$waivedByLocation = 10;
$noticeIDLocation = 16;
$remarkLocation = 18;
*/
$extraLocation = 0;
$MeritCountLocation = 3;
if($ldiscipline->Display_ConductMarkInAP) {	
	$conductMarkLocation = 4;
	if($ldiscipline->UseSubScore) {
		$SubScoreLocation = 5;
		$extraLocation++;
	}
	if($ldiscipline->UseActScore) {
		$SubScore2Location = 6;
		$extraLocation++;
	}
	$categoryItemLocation = 5 + $extraLocation;
	$PICLocation = 7 + $extraLocation;
	$historyLocation = 8 + $extraLocation;
	$actionLocation = 9 + $extraLocation;
	$waivedByLocation = 11 + $extraLocation;
	$checkboxLocation = 12 + $extraLocation;
	$StatusLocation = 14 + $extraLocation;
	$recordIDLocation = 15 + $extraLocation;
	$caseIDLocation = 16 + $extraLocation;
	$noticeIDLocation = 17 + $extraLocation;
	$remarkLocation = 19 + $extraLocation;
	// [2015-0611-1642-26164] Rehabilitation PIC
	if($sys_custom['eDiscipline']['CSCProbation']){
		$RehabilLocation = 20 + $extraLocation;
	}
	else {
		$RehabilLocation = 99;	# no use
	}
}
else {
	if($ldiscipline->UseSubScore) {
		$SubScoreLocation = 4;
		$extraLocation++;
	}
	if($ldiscipline->UseActScore) {
		$SubScore2Location = 5;
		$extraLocation++;
	}
	$categoryItemLocation = 4 + $extraLocation;
	$PICLocation = 6 + $extraLocation;
	$historyLocation = 7 + $extraLocation;
	$actionLocation = 8 + $extraLocation;
	$waivedByLocation = 10 + $extraLocation;
	$checkboxLocation = 11 + $extraLocation;
	$StatusLocation = 13 + $extraLocation;
	$recordIDLocation = 14 + $extraLocation;
	$caseIDLocation = 15 + $extraLocation;
	$noticeIDLocation = 16 + $extraLocation;
	$remarkLocation = 18 + $extraLocation;
	// [2015-0611-1642-26164] Rehabilitation PIC
	if($sys_custom['eDiscipline']['CSCProbation']){
		$RehabilLocation = 19 + $extraLocation;
	}
	else {
		$RehabilLocation = 99;	# no use
	}
}
######## end of table content #########
#######################################

# menu highlight setting
$CurrentPageArr['eServiceeDisciplinev12'] = 1;
$CurrentPage = "Viewing_AwardPunishment";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment'], "");

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
    
}

function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function reloadForm(val) {
	document.form1.MeritType.value = val;
	//document.form1.s.value = "";
	document.form1.s.value = document.form1.text.value;
	document.form1.pageNo.value = 1;
//	alert(document.document.form1.approved.value);
	document.form1.submit();	
}

function goSearch(obj) {
	/*
	if(document.form1.text.value=="") {
		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");	
		document.form1.text.focus();
		return false;
	}
	else {
	*/
		document.form1.s.value = document.form1.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
		return false;
	//}
}


function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if (type=='redeem')
		{
			redeemCheck();
			return;
		}
		document.form1.action = url;
		document.form1.submit();
	}
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
	
}


function checkCR(evt) {

	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	

}

//document.onkeypress = checkCR;

//-->
</script>
<script language="javascript">
<!--
var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];

function showResult(str,flag)
{
	
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "award_punishment_get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}  else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).style.zIndex = "1";
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e, moveX ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {		// IE
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {				// other browsers
		//tempX = e.pageX;
		//tempY = e.pageY;
		tempX = e.pageX + window.pageXOffset;
		tempY = e.pageY + window.pageYOffset;
	}
	
	if(moveX != undefined)
		tempX = tempX - moveX
	else 
		if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

//-->
</script>

<script language="javascript">
<!--
// Start of AJAX 
var callback = {
    success: function ( o )
    {
		if (o.responseText != "")
		{
			alert(o.responseText);
			return false;
		}
		else
		{
			document.form1.action = 'redeem_step1.php';
			document.form1.submit();
		}
	}
}

// Main
function redeemCheck()
{
	obj = document.form1;
    YAHOO.util.Connect.setForm(obj);
	
    var path = "redeem_check_aj.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

// End of AJAX
//-->
</script>
<script language="javascript">
<!--
function go_detail(id)
{
	document.form1.id.value = id;
	document.form1.action="award_punishment_detail.php";
	document.form1.submit();
}
//-->
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<form name="form1" method="post" action="" onSubmit="return goSearch('document.form1')">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$ChildrenFilter?>
					</td>
					<td align="right"><input name="text" type="text" class="formtextbox" value="<?=intranet_htmlspecialchars(stripslashes(stripslashes($s)))?>">
						<?=$linterface->GET_BTN($button_find, "submit");?>
					</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td height="30">
												<?=$selectSchoolYear?>
												<?=$SemesterMenu?>
												<?=$select_class?>
												<?=$picSelection?>
												<?=$ItemTagSelection?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$recordType?></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
												<br style="clear:both">
												<div id="status_option" class="selectbox_layer" style="width:200px">
													<table width="100%" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td>
																<? if($_SESSION['UserType']==1) { ?>
																<input type="checkbox" name="waitApproval" id="waitApproval" value="1" <?=$waitApprovalCheck?>>
																<label for="waitApproval"><?=$i_Discipline_System_Award_Punishment_Pending?></label><br>
																<? } ?>
																<input type="checkbox" name="approved" id="approved" value="1" <?=$approvedChecked?>>
																<label for="approved"><?=$i_Discipline_System_Award_Punishment_Approved?></label><br>
																<? if($_SESSION['UserType']==1) { ?>
																<input type="checkbox" name="rejected" id="rejected" value="1" <?=$rejectedChecked?>>
																<label for="rejected"><?=$i_Discipline_System_Award_Punishment_Rejected?></label><br>
																<? } ?>
															</td>
														</tr>
														<? if($_SESSION['UserType']==1) { ?>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>	
														<tr>
															<td>
																<input type="checkbox" name="released" id="released" value="1" <?=$releasedChecked?>>
																<label for="released"><?=$i_Discipline_System_Award_Punishment_Released?></label><br>
																<input type="checkbox" name="unreleased" id="unreleased" value="1" <?=$unreleasedChecked?>>
																<label for="unreleased"><?=$i_Discipline_System_Award_Punishment_UnReleased?></label><br>
															</td>
														</tr>
														<? } ?>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>	
														<tr>													<tr>
															<td><input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label></td>
														</tr>
<!--
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="locked"  id="locked" value="1" <?=$lockedChecked?>>
																<label for="locked"><?=$i_Discipline_System_Award_Punishment_Locked?></label> </td>
														</tr>
//-->
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td align="center" class="tabletext">
																<?= $linterface->GET_BTN($button_view, "submit", "javascript:reloadForm($MeritType)")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();")?>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$li->displayFormat_Split_PIC("Award_Punishment",$PICLocation,$StatusLocation,$checkboxLocation,$actionLocation,$recordIDLocation,"{$image_path}/{$LAYOUT_SKIN}",$historyLocation, $caseIDLocation, $waivedByLocation, $noticeIDLocation, "", $categoryItemLocation, $remarkLocation, "", $MeritCountLocation, $conductMarkLocation, $Lang['eDiscipline']['SignNotice'], "", $RehabilLocation, "", "", $SubScoreLocation, $SubScore2Location)?>
					</td>
				</tr>
			</table><br>
			<div width="100%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
			
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="MeritType" value="<?=$MeritType ?>">
			<input type="hidden" name="s" value="<?=stripslashes(stripslashes($s))?>"/>
			<input type="hidden" name="clickID" value=""/>
			<input type="hidden" name="id" value=""/>
			<input type="hidden" name="fromPPC" value="<?=$fromPPC?>"/>

			
			<input type="hidden" name="SchoolYear2" value="<?=$SchoolYear?>"/>
			<input type="hidden" name="semester2" value="<?=$semester?>"/>
			<input type="hidden" name="targetClass2" value="<?=$targetClass?>"/>
			<input type="hidden" name="waived2" value="<?=$waived?>"/>
			<input type="hidden" name="approved2" value="<?=$approved?>"/>
			<input type="hidden" name="waitApproval2" value="<?=$waitApproval?>"/>
			<input type="hidden" name="released2" value="<?=$released?>"/>
			<input type="hidden" name="rejected2" value="<?=$rejected?>"/>
			<input type="hidden" name="MeritType2" value="<?=$MeritType ?>">
			
			<input type="hidden" name="back_page" value="index.php"/>
			
			</form>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
