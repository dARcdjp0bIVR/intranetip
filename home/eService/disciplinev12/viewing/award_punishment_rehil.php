<?php
// Modifying by:
/*
* 	Date	:	2019-05-24  Bill
* 	Detail	:	apply download_attachment.php
* 	Date	:	2015-10-26 (Bill)	[2015-0611-1642-26164]
* 	Detail	:	not allow apply for CWC Rehabilitation Scheme if any punishment recorsd uner probation
* 	Date	:	2015-07-23 (Bill)	[2015-0611-1642-26164]
* 	Detail	:	Copy from award_punishment_detail.php
*/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

// Get Post data
$TargetStudentID = IntegerSafe($TargetStudentID);
$SchoolYear = IntegerSafe($SchoolYear);
$approved = IntegerSafe($approved);
$waived = IntegerSafe($waived);
$MeritType = IntegerSafe($MeritType);
$SchoolYear2 = IntegerSafe($SchoolYear2);
$waived2 = IntegerSafe($waived2);
$approved2 = IntegerSafe($approved2);
$waitApproval2 = IntegerSafe($waitApproval2);
$released2 = IntegerSafe($released2);
$rejected2 = IntegerSafe($rejected2);
$MeritType2 = IntegerSafe($MeritType2);
$id = IntegerSafe($id);

$ldiscipline = new libdisciplinev12();

# Check access right (View page)
if($_SESSION['UserType']!=1 && $_SESSION['UserType']!=2){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if(!$ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-View") && $_SESSION['UserType']!=1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if(!$sys_custom['eDiscipline']['CSCProbation']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($id);
$data = $datainfo[0];

# check teacher access right
if($_SESSION['UserType']==1){
	if(!$ldiscipline->checkTeacherAccessEserviceRight($UserID, $data['AcademicYearID'])) {	
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
	}
}
# check student access right
else if($_SESSION['UserType']==2){
	if($data['StudentID']!=$UserID){
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
		exit;
	}
}

# check if already set follow-up teacher
if(isset($data['RehabilPIC'])){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Build html content
$linterface = new interface_html();

# Menu highlight
$CurrentPage = "Viewing_AwardPunishment";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "award_punishment.php");
$PAGE_NAVIGATION[] = array($eDiscipline["RecordDetails"], "");
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['CWCRehabil']['Apply'], "");

if(empty($datainfo)){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$data = $datainfo[0];
$modified_by = $data['ModifiedBy'];
$isAcc = $data['fromConductRecords'];

# last modify
$lu = new libuser($modified_by);
if ($intranet_session_language == "en"){
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " " . $iDiscipline['By'] . " " . $lu->UserName();
}
else{
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " (" .$iDiscipline['By'] . $lu->UserName() . ")";
}

# student info
$stu = new libuser($data['StudentID']);
$stu_class = $stu->ClassName;
$stu_classnumber = $stu->ClassNumber;
$stu_name = $stu_class . ($stu_classnumber ? "-".$stu_classnumber: "") . " " . $stu->UserName();

// PIC
$PIC = $data['PICID'];
$namefield = getNameFieldWithLoginByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
$array_PIC = $ldiscipline->returnArray($sql);
$PICAry = array();
foreach($array_PIC as $k=>$d)
	$PICAry[] = $d[1];
$PICname = empty($PICAry) ? "---" : implode("<br>",$PICAry);

// Attachment
if($data['Attachment']!="") {
	$path = "$file_path/file/disciplinev12/award_punishment/".$data['Attachment']."/";
	$attachment_list = $ldiscipline->getAttachmentArray($path, '');
	if(sizeof($attachment_list) > 0) {
		$attachmentHTML = "";
		for($i=0; $i<sizeof($attachment_list); $i++) {
			list($displayFilename) = $attachment_list[$i];
			// $linkFilename = str_replace(" ","%20", $displayFilename);
			// $displayFilename = "<a href=/file/disciplinev12/award_punishment/".$data['Attachment']."/".$linkFilename." target=\"_blank\">$displayFilename</a>";
			
			$url = $path.$displayFilename;
			$displayFilename = "<a href='/home/download_attachment.php?target_e=".getEncryptedText($url)."' target=\"_blank\">$displayFilename</a>";
			$attachmentHTML .= ($attachmentHTML=="") ? $displayFilename : ", ".$displayFilename;
		}
		$attachmentHTML = "[".$attachmentHTML."]";
	}
} else {
	$attachmentHTML = "---";	
}

# Merit
$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']);
// Award - redirect
if($data['MeritType']==1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
// Punishment
else {
	$thisicon = "icon_demerit";
	$AwardPunishment = $i_Merit_Punishment;
	$IncrementDecrement = $i_Discipline_System_general_decrement;
	
	# check if any punishment records under probation
	list($approve_probation, $probation_records) = $ldiscipline->CHECK_STUDENT_CWC_PROBATION_RECORD($UserID, $data["YearTermID"], 0);
	if($approve_probation) {
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

// Ap record from gm
if($data['fromConductRecords'])
{
	$conduct_records = $ldiscipline->RETRIEVE_GROUPED_CONDUCT_RECORD($id);
	
	$Reference = "<td class='detail_box'>". $eDiscipline["HISTORY"] ."<br>";
	
	$Reference .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
		$Reference .= "<tr class='tabletop'>
							<td width='15' align='center' valign='middle'>#</td>
							<td width='20' align='left'>&nbsp;</td>
							<td align='left'>". $i_Merit_Reason ."</td>";
		if($sys_custom['eDiscipline_GM_Times']) {
			$Reference .= "<td>{$Lang['eDiscipline']['Times']}</td>";	
		}
		$Reference .= "
							<td>". $i_EventDate ."</td>
							<td>". $i_Discipline_PIC ."</a></td>
						</tr>";

		for($i=0;$i<sizeof($conduct_records);$i++)
		{
			$thisIcon = ($conduct_records[$i]['RecordType']==1) ? "icon_gd_conduct.gif":"icon_misconduct.gif";
			$thisCatID = $conduct_records[$i]['CategoryID'];
			$thisItemID = $conduct_records[$i]['ItemID'];
			$thisRecordDate = substr($conduct_records[$i]['RecordDate'],0,10);
			$record_reason = $ldiscipline->RETURN_CONDUCT_REASON($thisItemID, $thisCatID);
			$thisGmCount = (floor($conduct_records[$i]['GMCount'])==$conduct_records[$i]['GMCount']) ? floor($conduct_records[$i]['GMCount']) : round($conduct_records[$i]['GMCount'],1);
			
			$thisPIC = $ldiscipline->RETRIEVE_CONDUCT_PIC($conduct_records[$i]['RecordID']);
			$thisPIC = $thisPIC ? $thisPIC : "---";
			
			$Reference .= "<tr class='row_approved'>
								<td align='center' valign='top'>". ($i+1)."</td>
								<td align='left' valign='top' class='tabletext'><img src='". $image_path."/".$LAYOUT_SKIN ."/ediscipline/". $thisIcon ."' width='20' height='20'></td>
								<td valign='top' class='row_approved'>". $record_reason ."</td>";
			if($sys_custom['eDiscipline_GM_Times']) {
				$Reference .= "<td>{$thisGmCount}</td>";	
			}
			$Reference .= "
								<td valign='top'>". $thisRecordDate ."</td>
								<td valign='top'>". $thisPIC ."</td>
							</tr>";
		}
		$Reference .= "</table>";
	
	
	$Reference .= "</td>";
}

// Case
if($data['CaseID'])
{
	$case_info = $ldiscipline->RETRIEVE_CASE_INFO($data['CaseID']);
	$Reference = "<td>". $i_Discipline_System_Discipline_Case_Record_Case ." ". $case_info[0]['CaseNumber'] ."</td>";
}

$Reference = $Reference ? $Reference :"<td>---</td>";

// Detention
$TempAllForms = $ldiscipline->getAllFormString();
$DetentionList = $ldiscipline->getDetentionListByDemeritID($data['RecordID']);
for ($i=0; $i<sizeof($DetentionList); $i++) {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $DetentionList[$i];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	$TmpAttendanceStatus = $ldiscipline->getAttendanceStatusByStudentIDDetentionID($data['StudentID'], $TmpDetentionID);
	if ($TmpAttendanceStatus[0] == "PRE") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
	} else if ($TmpAttendanceStatus[0] == "ABS") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
	} else {
		$TmpAttendanceStatus = $i_Discipline_Not_Yet;
	}
	$DetentionOutputRow .= "<tr class=\"row_approved\">\n";
	$DetentionOutputRow .= "<td valign=\"top\">".($i+1)."</td>\n";
	if ($TmpDetentionID == "")
	{
		$DetentionOutputRow .= "<td valign=\"top\">".$i_Discipline_System_Not_Yet_Assigned."</td>\n";
	}
	else
	{
		$DetentionOutputRow .= "<td valign=\"top\">$TmpDetentionDate | $TmpLocation | $TmpForm | $TmpPIC</td>\n";
	}
	$DetentionOutputRow .= "<td valign=\"top\"><span class=\"tabletextremark\">$TmpAttendanceStatus</span></td>\n";
	$DetentionOutputRow .= "</tr>\n";
}
if ($DetentionOutputRow != "") {
	$DetentionOutputPre .= "<tr><td>".$eDiscipline['Detention']."</td></tr>\n";
	$DetentionOutputPre .= "<tr><td style=\"border:1px solid #CCCCCC\">\n";
	$DetentionOutputPre .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
	$DetentionOutputPre .= "<tr class=\"tabletop\">\n";
	$DetentionOutputPre .= "<td width=\"15\" align=\"center\">#</td>\n";
	$DetentionOutputPre .= "<td>$i_Discipline_Arranged_Session</td>\n";
	$DetentionOutputPre .= "<td>$i_Discipline_Attendance</td>\n";
	$DetentionOutputPre .= "</tr>\n";
	$DetentionOutputPost = "</table></td></tr>\n";
	$DetentionOutputData = $DetentionOutputPre.$DetentionOutputRow.$DetentionOutputPost;
}
if ($DetentionOutputData) {
	$DetentionButtonLabel = $eDiscipline["EditDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Edit';document.form1.submit();";
} else {
	$DetentionButtonLabel = $eDiscipline["AddDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Add';document.form1.submit();";
}

// eNotice
$lc = new libucc($data['NoticeID']);
$result = $lc->getNoticeDetails();
if ($data['NoticeID']) {
	$eNoticePath = "<a href=\"javascript:;\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."&StudentID=".$data['StudentID']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
	$actionContent = $eNoticePath;
} else if ($data['TemplateID']) {
	$actionContent = $eDiscipline["SendNotice"];
} else if ($DetentionOutputData == ""){
	$actionContent = $i_Discipline_No_Action;
}

# Start layout
$linterface->LAYOUT_START();

// reason item
//$ItemText = stripslashes(intranet_htmlspecialchars($data['ItemText']))
$ItemText = intranet_htmlspecialchars($data['ItemText']);

//$receive = $data['ProfileMeritCount']>0 ? $data['ProfileMeritCount']." ".$thisMeritType: "(".$i_general_na.")";
$receive = $ldiscipline->returnDeMeritStringWithNumber($data['ProfileMeritCount'], $thisMeritType);
?>

<script language="javascript">
<!--
function formSubmit(){
	var teacher_id = $('#FollowUpTeacher').val();
	var reh_info = $('#reh_info').val();
	
	if(teacher_id==""){
		alert('<?=$Lang['eDiscipline']['CWCRehabil']['PleaseSelectPIC']?>');
		return false;
	}
	if(reh_info==""){
		alert('<?=$Lang['eDiscipline']['CWCRehabil']['PleaseFillinReason']?>');
		return false;
	}
	
	document.form1.submit();
}
//-->
</script>

<br />
<form name="form1" method="POST" action="award_punishment_rehil_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$linterface->GET_SYS_MSG($msg)?></td>
			</tr>
			<tr>
				<td align='right'><?=$last_update?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_identity_student?></td>
			<td valign="top" class="tablerow2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_stu_ind.gif" width="20" height="20" border="0" align="absmiddle"><?=$stu_name?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
			<td width="80%" valign="top"><?=$data['Year']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
			<td valign="top"><?=$data['Semester']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["EventDate"]?></td>
			<td valign="top"><?=$data['RecordDate']?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?></td>
			<td valign="top"><?=$PICname?></td>
		</tr>
		<? if($data['Subject']) {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Homework_subject?></td>
			<td><?=nl2br($data['Subject'])?></td>
		</tr>
		<? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
			<td valign="top"><?=$attachmentHTML?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_general_remark?></td>
			<td><?=nl2br($data['Remark'])?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["RecordDetails"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Type"] ?></td>
			<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/<?=$thisicon?>.gif" width="20" height="20" border="0" align="absmiddle"> <?=$AwardPunishment?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Category_Item"]?></td>
			<td><?=$ItemText?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Reason2?></span></td>
			<td><?=$receive?></td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?> (<?=$IncrementDecrement?>)</td>
			<td><label for="grading_passfail" class="tabletext"></label>
					<label for="grading_passfail" class="tabletext"><?=$data['ConductScoreChange']?></label></td>
		</tr>
		<? if($ldiscipline->UseSubScore) {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?> <?=$IncrementDecrement?></td>
			<td><label for="grading_passfail" class="tabletext"></label>
					<label for="grading_passfail" class="tabletext"><?= (($data['SubScore1Change'])? $data['SubScore1Change'] : 0) ?></label></td>
		</tr>
		<? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Award_Punishment_Reference?></td>
			<?=$Reference?>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$Lang['eDiscipline']['CWCRehabil']['ApplicationContent']?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['CWCRehabil']['FollowUpTeacher']?></td>
			<td><?=$lu->getSelectTeacherStaff(" name='FollowUpTeacher' id='FollowUpTeacher'", $FollowUpTeacher)?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['CWCRehabil']['RehabilReason']?></td>
			<td><textarea id="reh_info" name="reh_info" rows="5" cols="80"></textarea></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "<?=$image_path?>/<?=$LAYOUT_SKIN?>" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "formSubmit()")?>
				&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "document.form1.action='award_punishment_detail.php'; document.form1.submit();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="id" value="<?=$id?>" />

</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
