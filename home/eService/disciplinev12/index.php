<?php
# modifying by : Bill

/*
 * 	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
 * 	Detail	:	hide GM related content ($sys_custom['eDiscipline']['HideAllGMReport'])
 * 
*	Date	:	2017-06-28 (Bill)	 [2015-0120-1200-33164]
*	Detail	:	display Study & Activity Score
*
*	Date	:	2017-06-01 (Bill)	 [2017-0518-1433-40207]
*	Detail	:	fixed incorrect ap count if empty SemesterID
* 
*	Date	:	2014-02-27 (YatWoon)
*	Detail	:	add student checking, is the student selected is belongs to that parent?
*
*	Date	:	2011-01-06 (Henry Chow)
*	Detail 	:	add GM Summary Table
*
*	Date	:	2010-06-11 (Henry)
*	Detail 	:	allow Staff (Teacher) can view eService > eDiscipline
*/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$student_id = IntegerSafe($student_id);
$SchoolYear = IntegerSafe($SchoolYear);
$semester = IntegerSafe($semester);

$ldiscipline = new libdisciplinev12();
$studentAccessRightInGroup = $ldiscipline->retrieveUserAccessRightInGroup($UserID);

#################################
# CHECK student/parent can access overview or not
#################################
if(!($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Overview-VIEW") && ($_SESSION['UserType'] == 2 || $_SESSION['UserType'] == 3 )))
{
	if($ldiscipline->CHECK_SECTION_ACCESS("Discipline-VIEWING") && ($_SESSION['UserType'] == 2 || $_SESSION['UserType'] == 3 ))
	{
		if($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-GoodConduct_Misconduct-VIEW") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/viewing/goodconduct_misconduct.php");
		else if($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-VIEW") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/viewing/award_punishment.php");
		else if($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-Detention-VIEW") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/viewing/detention.php");
		else if($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-StudentReport-VIEW") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/viewing/student_report.php");
		else if($ldiscipline->CHECK_ACCESS("Discipline-VIEWING-PersonalReport-VIEW") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/viewing/personal_check.php");
	}
	else if($ldiscipline->CHECK_SECTION_ACCESS("Discipline-RANKING") && ($_SESSION['UserType'] == 2 || $_SESSION['UserType'] == 3) )
	{
		if($ldiscipline->CHECK_ACCESS("Discipline-RANKING-GoodConduct_Ranking-View") && ($_SESSION['UserType'] == 2|| $_SESSION['UserType'] == 3) )
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/ranking/goodconduct_misconduct_ranking.php");
		else if($ldiscipline->CHECK_ACCESS("Discipline-RANKING-Award_Ranking-View") && ($_SESSION['UserType'] == 2 || $_SESSION['UserType'] == 3 ))
			header("Location: ". $PATH_WRT_ROOT ."home/eService/disciplinev12/ranking/award_punishment_ranking.php");
	}
	else
	{
		//$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	}
}

$linterface = new interface_html();
$lsp = new libstudentprofile();
		
$StudentIdAry = Array();
if($_SESSION['UserType']==1) {			# Teacher View
	header("Location: viewing/goodconduct_misconduct.php");	
} 
else if($_SESSION['UserType']==3)		# Parent View
{
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser($UserID);
	$ChildrenAry = $luser->getActiveChildrenList();
	
	if(!empty($student_id))
	{
		#################################################
		# Check the child is belongs to parent or not
		#################################################
		$valid_child = false;
		foreach($ChildrenAry as $k => $d)
		{
			if($d['StudentID'] == $student_id) $valid_child = true;	
		}
		if(!$valid_child)
		{
			$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
	
	if(count($ChildrenAry)>1)		# Parent with more than 1 child
	{
		if($student_id=='' || $student_id=='0') {
			for($i=0; $i<count($ChildrenAry); $i++)
				$studentIDAry[$i] = $ChildrenAry[$i][0]; 
		}
		else {
			$studentIDAry[] = $student_id;	
		}
		$ChildrenFilter =  getSelectByArray($ChildrenAry, " name='student_id' id='student_id'  onchange='document.form1.submit()'", $student_id, 1);
	}
	else							# Parent with only 1 child
	{
		$studentIDAry[] = $ChildrenAry[0][0];
		$ChildrenFilter = "<input type='hidden' name='student_id' id='studnet_id' value='".$ChildrenAry[0][0]."'>";
	}
}
else									# Student View
{
	$studentIDAry[] = $_SESSION['UserID'];
	$ChildrenFilter = "<input type='hidden' name='student_id' id='studnet_id' value='".$_SESSION['UserID']."'>";
}

$sql = "SELECT 
			DISTINCT yc.AcademicYearID, ".Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN')."
		FROM 
			YEAR_CLASS_USER ycu LEFT OUTER JOIN
			YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID) LEFT OUTER JOIN
			ACADEMIC_YEAR ay ON (yc.AcademicYearID=ay.AcademicYearID)
		WHERE 
			ycu.UserID IN (".implode(',',$studentIDAry).")
		ORDER BY ay.Sequence
		";
$studentInYear = $ldiscipline->returnArray($sql,2);

//$yearArr = $ldiscipline->returnAllYearsSelectionArray();
$SchoolYear = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;

$semArr = getSemesters($SchoolYear);
$semesterArr[0][] = '0';
$semesterArr[0][] = $i_Discipline_System_Award_Punishment_Whole_Year;

$i = 1;
if(sizeof($semArr)) {
	foreach($semArr as $key=>$val) {
		$semesterArr[$i][] = $key;
		$semesterArr[$i][] = $val;
		$i++;
	}
}

// [2017-0518-1433-40207] Default Current Semester if empty SemesterID
$semester = ($semester=="" && $semester!="0") ? getCurrentSemesterID() : $semester;

$CurrentSemester = ($semester=="") ? getCurrentSemesterID() : $semester;
$semesterSelect = $linterface->GET_SELECTION_BOX($semesterArr, 'id="semester" name="semester" onChange="form1.submit()"', "", $CurrentSemester);

# only display the year which has student records
$yearSelect = $linterface->GET_SELECTION_BOX($studentInYear, 'id="SchoolYear" name="SchoolYear" onChange="goURL(this.value)"', "", $SchoolYear);

$semesterValue = ($semester!='' && $semester!=0) ? $semester : "";
$startDate = getStartDateOfAcademicYear($SchoolYear, $semesterValue);
$endDate = getEndDateOfAcademicYear($SchoolYear, $semesterValue);

### AP Record
# Generate table content Merit/Demerit (Converted Data)
/*
$tableContent = "";
foreach($studentIDAry as $student) {
	$record = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($student, $startDate, $endDate);

	$tableContent .= "";
}
*/
$result = $ldiscipline->getStudentOverview($SchoolYear, $CurrentSemester, $studentIDAry);

$BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');
$BaseStudyScore = $ldiscipline->getStudyScoreRule('baseMark');
$BaseActivityScore = $ldiscipline->getActivityScoreRule('baseMark');

# Table Top
$studentTableTop .= "	<tr class='tablebluetop'>";
if($_SESSION['UserType']==3) {
	$studentTableTop .= "		<td valign='top' class='tabletop'>$i_ClassName</td>";	
	$studentTableTop .= "		<td valign='top' class='tabletop' align='center'>$i_ClassNumber</td>";	
	$studentTableTop .= "		<td valign='top' class='tabletop' align='center'>$i_UserStudentName</td>";	
}
if (!$lsp->is_merit_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_Merit</td>";
if (!$lsp->is_min_merit_disabled)	
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_MinorCredit</td>";
if (!$lsp->is_maj_merit_disabled)	
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_MajorCredit</td>";
if (!$lsp->is_sup_merit_disabled)	
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_SuperCredit</td>";
if (!$lsp->is_ult_merit_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_UltraCredit</td>";
if (!$lsp->is_warning_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_Warning</td>";
if (!$lsp->is_black_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_BlackMark</td>";
if (!$lsp->is_min_demer_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_MinorDemerit</td>";
if (!$lsp->is_maj_demer_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_MajorDemerit</td>";
if (!$lsp->is_sup_demer_disabled)
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_SuperDemerit</td>";
if (!$lsp->is_ult_demer_disabled)	
	$studentTableTop .= "		<td valign='top' class='tabletopnolink' align='center'>$i_Merit_UltraDemerit</td>";
//if($ldiscipline->Display_ConductMarkInAP)
	$studentTableTop .= "		<td valign='top' class='tablegreentop tabletopnolink seperator_left' align='center'>$i_Discipline_System_ConductScore</td>";
if($ldiscipline->UseSubScore) 
	$studentTableTop .= "		<td valign='top' class='tablegreentop tabletopnolink' align='center'>$i_Discipline_System_Subscore1</td>";
if($ldiscipline->UseActScore) 
	$studentTableTop .= "		<td valign='top' class='tablegreentop tabletopnolink' align='center'>".$Lang['eDiscipline']['ActivityScore']."</td>";
$studentTableTop .= "	</tr>";

$studentTable .= "<table width='98%' cellpadding='4' cellspacing='0' border='0'>";
$studentTable .= 			$studentTableTop;

# Table Content
$k = 0;
foreach($studentIDAry as $id)
{
	$css = ($k%2==0) ? "":"2";
	$css2 = ($k%2==0) ? 1 : 2;
	
	# Student Info
	$studentInfo = $ldiscipline->getStudentNameByID($id);
	
	# Conduct Score
	$conductScore = "";
	if($CurrentSemester=='' || $CurrentSemester=='0') {
		$cond = " AND IsAnnual = 1";
	}
	else {
		$cond = " AND YearTermID=".$CurrentSemester;
	}
	$sql = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE 
				WHERE StudentID = '$id' and AcademicYearID = '$SchoolYear' $cond ";
	$tempArr = $ldiscipline->returnVector($sql);
	
	## Get Conduct Mark Calculation Method
	$ConductMarkCalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();
	if($ConductMarkCalculationMethod == 0)
	{
		$conductScore = ($tempArr[0]!='') ? $tempArr[0] : $BaseConductScore;
		
		$adjustMark = $ldiscipline->getSemesterConductScoreAdjustByStudentID($id, $SchoolYear, $CurrentSemester);
		$conductScore = $conductScore + $adjustMark;
	}
	else
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE StudentID = '$id' AND AcademicYearID = '$SchoolYear'";
		$newRecord = $ldiscipline->returnVector($sql);
		
		if($newRecord[0] == 0) {
			$conductScore = $ldiscipline->getConductMarkRule("baseMark");	# base mark
		}
		else {
			if($semesterValue == ""){
				$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' ORDER BY TermStart DESC LIMIT 0,1";
				$FinalSemester = $ldiscipline->returnVector($sql);
				$semesterValue = $FinalSemester[0];
			}
			$TermStartDate = getStartDateOfAcademicYear($SchoolYear,$semesterValue);
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";			
			$result = $ldiscipline->returnArray($sql,1);
			
			if(sizeof($result)>0){
				for($i=0; $i<sizeof($result); $i++){
					list($TermID) = $result[$i];
					
					# get the latest conduct balance
					$sql = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE StudentID = '$id' AND YearTermID = '$TermID'";
					$ConductMarkBalance = $ldiscipline->returnVector($sql);
			
					if($ConductMarkBalance[0] != "")
					{
						$conductScore = $ConductMarkBalance[0];
						break;
					}
				}
			}
			if($conductScore == ""){
				$conductScore = $BaseConductScore;
			}
			
			if(sizeof($result)>0){
				for($i=0; $i<sizeof($result); $i++){
					list($TermID) = $result[$i];
					$sql = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = '$id' AND AcademicYearID = '$SchoolYear' AND YearTermID = '$TermID'";
					$adjustMark = $ldiscipline->returnVector($sql);
					$final_adjustMark = $final_adjustMark+$adjustMark[0];
				}
			}
			$conductScore = $conductScore+$final_adjustMark;
		}
	}
	
	# Study Score
	if($ldiscipline->UseSubScore) 
	{
		$studyScore = "";
		$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE 
					WHERE StudentID = '$id' and AcademicYearID = '$SchoolYear' $cond ";
		$tempArr = $ldiscipline->returnVector($sql);
		
		## Get Study Score Calculation Method
		$StudyScoreCalculationMethod = $ldiscipline->retriveStudyScoreCalculationMethod();
		if($StudyScoreCalculationMethod == 0)
		{
			$studyScore = ($tempArr[0]!='') ? $tempArr[0] : $BaseStudyScore;
		}
		else
		{
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE StudentID = '$id' AND AcademicYearID = '$SchoolYear'";
			$newRecord = $ldiscipline->returnVector($sql);
			
			if($newRecord[0] == 0) {
				$studyScore = $ldiscipline->getStudyScoreRule("baseMark");		# base mark
			}
			else {
				if($semesterValue == "") {
					$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' ORDER BY TermStart DESC LIMIT 0,1";
					$FinalSemester = $ldiscipline->returnVector($sql);
					$semesterValue = $FinalSemester[0];
				}
				$TermStartDate = getStartDateOfAcademicYear($SchoolYear, $semesterValue);
				$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";			
				$result = $ldiscipline->returnArray($sql,1);
				
				if(sizeof($result)>0)
				{
					for($i=0; $i<sizeof($result); $i++) {
						list($TermID) = $result[$i];
						
						# Get latest Study Score Balance
						$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE StudentID = '$id' AND YearTermID = '$TermID'";
						$StudyScoreBalance = $ldiscipline->returnVector($sql);
						if($StudyScoreBalance[0] != "")
						{
							$studyScore = $StudyScoreBalance[0];
							break;
						}
					}
				}
				if($studyScore == ""){
					$studyScore = $BaseStudyScore;
				}
			}
		}
	}
	
	# Activity Score
	if($ldiscipline->UseActScore) 
	{
		$activityScore = "";
		$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_ACTSCORE_BALANCE 
					WHERE StudentID = '$id' and AcademicYearID = '$SchoolYear' $cond ";
		$tempArr = $ldiscipline->returnVector($sql);
		
		## Get Activity Score Calculation Method
		$ActivityScoreCalculationMethod = $ldiscipline->retriveActivityScoreCalculationMethod();
		if($ActivityScoreCalculationMethod == 0)
		{
			$activityScore = ($tempArr[0]!='') ? $tempArr[0] : $BaseActivityScore;
		}
		else
		{
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_ACTSCORE_BALANCE WHERE StudentID = '$id' AND AcademicYearID = '$SchoolYear'";
			$newRecord = $ldiscipline->returnVector($sql);
			
			if($newRecord[0] == 0) {
				$activityScore = $ldiscipline->getActivityScoreRule("baseMark");		# base mark
			}
			else {
				if($semesterValue == "") {
					$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' ORDER BY TermStart DESC LIMIT 0,1";
					$FinalSemester = $ldiscipline->returnVector($sql);
					$semesterValue = $FinalSemester[0];
				}
				$TermStartDate = getStartDateOfAcademicYear($SchoolYear,$semesterValue);
				$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$SchoolYear' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";			
				$result = $ldiscipline->returnArray($sql,1);
				
				if(sizeof($result)>0)
				{
					for($i=0; $i<sizeof($result); $i++) {
						list($TermID) = $result[$i];
						
						# get latest Activity Score Balance
						$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_ACTSCORE_BALANCE WHERE StudentID = '$id' AND YearTermID = '$TermID'";
						$ActivityScoreBalance = $ldiscipline->returnVector($sql);
				
						if($ActivityScoreBalance[0] != "")
						{
							$activityScore = $ActivityScoreBalance[0];
							break;
						}
					}
				}
				if($activityScore == ""){
					$activityScore = $BaseActivityScore;
				}
			}
		}
	}
	
	if($semester=="0")
		$yearTermID = "";
	else 
		$yearTermID = $semester;
	
	# Date Range
	$ts = time();
	if ($startdate == "")
	{
	    //$startdate = date('Y-m-d',getStartOfAcademicYear($ts));
	    $startdate = getStartDateOfAcademicYear($SchoolYear, $yearTermID);
	    $startdate = substr($startdate,0,10);
	}
	if ($enddate == "")
	{
	    //$enddate = date('Y-m-d',getEndOfAcademicYear($ts));
	    $enddate = getEndDateOfAcademicYear($SchoolYear, $yearTermID);
	    $enddate = substr($enddate,0,10);
	}
	$additionConds = " AND a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;
	
	# Get data from DB
	$result = $ldiscipline->retrieveStudentMeritTypeCountByDate($id, $startdate, $enddate, $additionConds);

	# Prepare Table Content 
	$studentTable .= "			<tr>";
	if($_SESSION['UserType']==3) {
		$studentTable .= "			<td class='tablerow$css2'>".$studentInfo[0][2]."</td>";
		$studentTable .= "			<td class='tablerow$css2' align='center'>".$studentInfo[0][3]."</td>";
		$studentTable .= "			<td class='tablerow$css2' align='center'>".$studentInfo[0][1]."</td>";
	}
	if (!$lsp->is_merit_disabled)
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['1']!='')?$result['1']:0)."</td>";
	if (!$lsp->is_min_merit_disabled)	
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['2']!='')?$result['2']:0)."</td>";
	if (!$lsp->is_maj_merit_disabled)	
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['3']!='')?$result['3']:0)."</td>";
	if (!$lsp->is_sup_merit_disabled)	
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['4']!='')?$result['4']:0)."</td>";
	if (!$lsp->is_ult_merit_disabled)
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['5']!='')?$result['5']:0)."</td>";
	if (!$lsp->is_warning_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['0']!='')?$result['0']:0)."</td>";
	if (!$lsp->is_black_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['-1']!='')?$result['-1']:0)."</td>";
	if (!$lsp->is_min_demer_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['-2']!='')?$result['-2']:0)."</td>";
	if (!$lsp->is_maj_demer_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['-3']!='')?$result['-3']:0)."</td>";
	if (!$lsp->is_sup_demer_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['-4']!='')?$result['-4']:0)."</td>";
	if (!$lsp->is_ult_demer_disabled) 
		$studentTable .= "			<td class='tabletext tablerow_total$css' align='center'>".(($result['-5']!='')?$result['-5']:0)."</td>";
	//if($ldiscipline->Display_ConductMarkInAP)
		$studentTable .= "			<td class='tablerow$css2 tabletext' align='center'>$conductScore</td>";
	if($ldiscipline->UseSubScore) 
		$studentTable .= "			<td class='tablerow$css2 tabletext' align='center'>$studyScore</td>";
	if($ldiscipline->UseActScore) 
		$studentTable .= "			<td class='tablerow$css2 tabletext' align='center'>$activityScore</td>";
	$studentTable .= "			</tr>";
	$k++;
}
	$studentTable .= "</table>";
	$studentTable .= "<br>";

if(!$sys_custom['eDiscipline']['HideAllGMReport'])
{
### GM Table
//$conds = " AND RecordStatus=1";
$category = $ldiscipline->getAllGmCategory($conds);
$gm_record = $ldiscipline->returnGmRecordAmount($studentIDAry, $SchoolYear, $CurrentSemester);
//debug_pr($category);
//$max = 0;

$c = array();
for($i=0; $i<sizeof($category); $i++) {
	list($catID, $catName, $mType, $cat_status) = $category[$i];
	if($mType=="") $mType = -1;
	$c[$mType] = (!isset($c[$mType]) || $c[$mType]==0) ? 1 : $c[$mType]+1;
	//$max = ($max<$c[$mType]) ? $c[$mType] : $max;
}
//echo $max;

$data = array();
for($i=0; $i<sizeof($gm_record); $i++) {
	list($cat, $studentid, $count, $mType, $item_disabled) = $gm_record[$i];
	$data[$mType][$cat][$studentid][0] += $count;	
	$data[$mType][$cat][$studentid][1] = $item_disabled ? 1 : $item_disabled;
}

$gm_table = '
	<table width="98%" cellpadding=4 cellspacing=0>
		<tr class="tablebluetop">
			<td class="tabletop" width="12%">'.$eDiscipline["Type"].'</td>
			<td valign="top" class="tabletop" width="25%">'.$i_Discipline_System_Discipline_Category.'</td>';
foreach($studentIDAry as $id) {
	# Student Info
	$studentInfo = $ldiscipline->getStudentNameByID($id);
	$gm_table .= '<td valign="top" class="tabletopnolink" align="center">'.(($_SESSION['UserType']==3) ? ($studentInfo[0][2].'-'.$studentInfo[0][3].' '.$studentInfo[0][1]) : $list_total).'</td>';
}

$GM = array("1"=>$i_Discipline_GoodConduct, "-1"=>$i_Discipline_Misconduct);

if(sizeof($c)>0)
{
	$meritArray = array();
	$p = 0;
	foreach($c as $mType => $max) {
		$css3 = $p%2 ? 2 : 1;
		
		if(!isset($meritArray[$mType])) { 
			$meritArray[] = $mType;
			$gm_table .= '<tr><td rowspan="'.($c[$mType]+1).'" valign="top" class="tablerow'.$css3.'">'.$GM[$mType].'</td></tr>';
		}
		
		for($i=0; $i<sizeof($category); $i++) {
			$css = ($i%2==0) ? "":"2";
			$css2 = ($i%2==0) ? 1 : 2;
			
			list($catID, $catName, $type, $cat_status) = $category[$i];	
			
			if($mType==$type) {
				$status = ($cat_status==DISCIPLINE_DELETED_RECORD || $cat_status==DISCIPLINE_CONDUCT_CATEGORY_DRAFT) ? "<span class='tabletextrequire'>*</span>" : "";
				$gm_table .= '
					</tr>
					<tr>
						<td class="tablerow'.$css2.'">'.$status.$catName.'</td>';
				foreach($studentIDAry as $id) {
					$amt = (!isset($data[$mType][$catID][$id][0]) || $data[$mType][$catID][$id][0]=="") ? 0 : $data[$mType][$catID][$id][0];
					$asterisk = (!isset($data[$mType][$catID][$id][1]) || $data[$mType][$catID][$id][1]==0) ? "" : "<span class='tabletextrequire'>^</span>";
					$gm_table .= '<td class="tabletext tablerow_total'.$css.'" align="center">'.$asterisk.$amt.'</td>';
				}		
				$gm_table .= '</tr>';
			}
		}
		$p++;
	}
	
	$remark = '<span class="tabletextremark"><span class="tabletextrequire">*</span> '.$Lang['eDiscipline']['CategoryIsDeletedDisabled'].'</span><br>';
	$remark .= '<span class="tabletextremark"><span class="tabletextrequire">^</span> '.$Lang['eDiscipline']['ItemIsDeletedDisabled'].'</span>';
}
else {
	$colspan = 2 + sizeof($studentIDAry);
	$gm_table .= '<tr><td colspan="'.$colspan.'" align="center" height="40">'.$i_no_record_exists_msg.'</td></tr>';	
}

$gm_table .= '

	</table>		
		';
$gm_table .= $remark;
}

# Top Menu
$CurrentPage = "Overview";
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
function goURL(url) {
	document.form1.semester.selectedIndex = 0;
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="index.php">
<br />

<table align="center" width="98%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext">
			<?=$ChildrenFilter." ".$yearSelect." ".$semesterSelect?>
		</td>
	</tr>
</table>
<br>

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
	<span class="sectiontitle"><?=$eDiscipline['Award_and_Punishment']?></span>
<?=$studentTable?>
<!--
<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center"><?=$i_Sports_Select_Menu?> </td>
	</tr>
</table>
<br />
//-->

<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
	<span class="sectiontitle"><?=$eDiscipline['Good_Conduct_and_Misconduct']?></span>
<?=$gm_table?>
<? } ?>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>