<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
//include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
//$lstudentprofile = new libstudentprofile();
//$lclass = new libclass();

# Check Access Right
if($_SESSION['UserType']!=2 && $_SESSION['UserType']!=3){ // only student / parent can view
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$ldiscipline->CHECK_ACCESS("Discipline-RANKING-GoodConduct_Ranking-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$StudentIdAry = Array();
if($_SESSION['UserType']==3)		# parent view
{
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser($UserID);

	$ChildrenAry = $luser -> getActiveChildrenList();
	
	if(count($ChildrenAry)>1)		# parent with more than 1 child
	{
		if($student_id=='' || $student_id=='0') {
			for($i=0; $i<count($ChildrenAry); $i++)
				$studentIDAry[$i] = $ChildrenAry[$i][0]; 
		} else {
			$studentIDAry[] = $student_id;	
		}

	}
	else							# parent with only 1 child
	{
		$studentIDAry[] = $ChildrenAry[0][0];
		
	}
}
else		# student view
{
	$studentIDAry[] = $_SESSION['UserID'];
}


#get User Information
foreach($studentIDAry as $sid) {
	$stdName = $ldiscipline->getStudentNameByID($sid);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	$classID = $ldiscipline->getClassIDByClassName($className);
	
	# get student class level
	$sql = " SELECT 
				b.YearID, 
				b.YearName 
			FROM 
				YEAR_CLASS a 
				LEFT JOIN YEAR b ON a.YearID = b.YearID 
			WHERE 
				a.YearClassID=$classID";
	$classlevel = $ldiscipline->returnArray($sql,2);
	list ($ClassLevelID, $LevelName) = $classlevel[0];
	
	$meritTitle = $i_Discipline_GoodConduct;
	
	$date = getCurrentAcademicYear()." ".$i_Discipline_System_Award_Punishment_Whole_Year;
	//$conds .= " AND a.Year='".getCurrentAcademicYear()."'";
	$conds .= " AND a.AcademicYearID=".Get_Current_academic_Year_ID();
	
	$tempConds = "";
	
	$groupBy = "ycu.UserID";
	
	
	# table bottom
	$tableBottom = "</table>";
	
	$showStudentName = ($_SESSION['UserType']==3) ? "$std_name &gt; " : "";

		for($i=0; $i<2; $i++) {
		
			# table Top
			$tableTop = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
			$tableTop .= "<tr class='tablebluetop'>";
			$tableTop .= "<td class='tabletopnolink' width='10%'>{$iDiscipline['Rank']}</td>";
			$tableTop .= ($i == 1?"<td class='tabletopnolink' width='20%'>{$i_Discipline_Class} </td>":"");
			$tableTop .= "<td class='tabletopnolink' width='50%'>{$i_Discipline_Student} </td>";
			$tableTop .= "<td class='tabletopnolink' width='20%'>{$i_Discipline_No_of_Records}</td>";
			$tableTop .= "</tr>";
	
			# table no record
			$tableNoContent = $tableTop;
			$tableNoContent .= "<tr class='tablebluerow1'>";
			$tableNoContent .= "<td class='tabletext' colspan='4' height='40' align='center'>{$i_no_record_exists_msg}</td>";
			$tableNoContent .= "</tr>";
			$tableNoContent .= $tableBottom;
			
			$tempConds = " AND a.RecordType = 1";
			$tempConds .= ($i == 0) ? " AND yc.YearClassID = $classID " : " AND y.YearID = $ClassLevelID";
	
			$temp = $ldiscipline->getGMRankingData('type', $record_type[$i], $conds.$tempConds, $groupBy);
			list($lvlName, $countTotal, $thisClassName, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
						
			if($intranet_session_language == "en") 
				$tableContent .= "<br>$showStudentName".($i == 0? ($eDiscipline['GoodconductRanking']." in ".$className):($eDiscipline['GoodconductRanking']." in ".$LevelName)) ;
			else if($intranet_session_language == "b5")
				$tableContent .= "<br>$showStudentName".($i == 0? ($className.$eDiscipline['Class'].$eDiscipline['GoodconductRanking']):($LevelName.$eDiscipline['Form'].$eDiscipline['GoodconductRanking'])) ;
	
				
			if(sizeof($temp)>0) {
				$tableContent .= $tableTop;
				for($j=0; $j<sizeof($temp) && $j<10; $j++) {
					
					$showName = $temp[$j][8]; 
					$k = $j+1;
					$css = ($j%2)+1;
					$tableContent .= "<tr class='tablebluerow{$css}'>";
					$tableContent .= "<td class='tabletext'>{$k}</td>";
					$tableContent .= ($i == 1?"<td>{$temp[$j][2]}</td>":"");
					$tableContent .= "<td>{$showName}</td>";
					$tableContent .= "<td>{$temp[$j][1]}</td>";
					$tableContent .= "</tr>";
				}
				$tableContent .= $tableBottom;
			} else {
				$tableContent .= $tableNoContent;	
			}
		}
		$tableContent .= "<br>";
}
$CurrentPageArr['eServiceeDisciplinev12'] = 1;

# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['GoodconductRanking'], "");
$CurrentPage = "Ranking_GoodConduct";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td>
						<table border="0" cellspacing="5" cellpadding="5" align="center">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$date?><?=$eDiscipline['GoodconductRanking']?> </td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
<input type="hidden" name="ItemText1" id="ItemText1" />
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
