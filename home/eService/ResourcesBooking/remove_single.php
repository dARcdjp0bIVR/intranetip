<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
intranet_opendb();
auth_resource();

$failed = false;
$lb = new librb();
$lb->retrieveBooking($bid);
if ($lb->AppliedUserID != $UserID)
{
    $failed = true;
    header("Location: index.php");
}
else
{
    $bookings = $lb->retrieveBookingBatchIDs();
    $list = implode(",", $bookings);
    $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = 1, LastAction = now() WHERE BookingID IN ($list)";
    $lb->db_db_query($sql);
}
intranet_closedb();
header("Location: index.php?signal=5");
?>