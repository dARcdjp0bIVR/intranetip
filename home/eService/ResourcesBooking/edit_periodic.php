<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libpb.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_resource();

$lb = new librb();
$lp = new libpb();
$lp->retrievePeriodicBooking($pid);
$lr = new libresource($lp->ResourceID);
$lcycleperiods = new libcycleperiods();

$daysBefore = $lr->DaysBefore;

$lbatch = new libbatch($lr->TimeSlotBatchID);

$old_start = $lp->BookingStartDate;
$old_end = $lp->BookingEndDate;
$old_status = $lp->RecordStatus;

$old_start_stamp = strtotime($old_start);
$old_end_stamp = strtotime($old_end);

$old_start_month = date('m',$old_start_stamp);
$old_start_year = date('Y',$old_start_stamp);
$old_end_month = date('m',$old_end_stamp);
$old_end_year = date('Y',$old_end_stamp);

$type_checked = array("","","","");
$type_checked[$lp->RecordType] = "CHECKED";


switch ($lp->RecordType)
{
        case 1: $value1 = $lp->TypeValues; break;
        case 2: $value2 = $lp->TypeValues; break;
        case 3: $value3 = $lp->TypeValues; break;
}

$rb_image_path = "/images/resourcesbooking";

if ($old_status == 0)
{
    $min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
    if ($min_date > $old_start_stamp)
    {
        $min_date_s = $old_start;
    }
    else
    {
        $min_date_s = date('Y-m-d',$min_date);
    }
}
else if ($old_status == 4)
{
     $min_date_s = $old_start;
}
else
{
    header("Location: index.php?signal=4");
    exit();
}
$start_alert = "$i_BookingWrongPeriodicDateEdit $min_date_s $i_BookingWrongPeriodicDateEdit2";
$end_alert = "";

$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator.$i_BookingEditPeriodic;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>
     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--

          var min_date = '<?=$min_date_s?>';

          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   if (compareDate(dateValue,min_date) < 0)
                   {
                       alert("<?=$start_alert?>")
                   }
                   else
                   {
                       document.forms['form1'].start.value = dateValue;
                       endCal.currentMonth = month-1;
                       endCal.currentYear = year;
                       if (document.forms['form1'].end.value == "")
                           document.forms['form1'].end.value = dateValue;
                   }
          }
          function calendarCallback2(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }

                   dateValue =year + '-' + month + '-' + date;
                   document.forms['form1'].end.value = dateValue;
          }
     // -->
     </script>

<script language="javascript">
function checkform(obj){
     var min_date = '<?=$min_date_s?>';
     var old_end = '<?=$old_end?>';
     if(countChecked(obj,"slots[]")==0) { alert(globalAlertMsg2); return false; }
     if (!check_date(obj.start,"<?php echo $i_invalid_date; ?>")) return false;
     if (!check_date(obj.end,"<?php echo $i_invalid_date; ?>")) return false;
     <?
     if ($old_status != 4)
     {
     ?>
     if (obj.recordType[1].checked) {if (check_positive_nonzero_int(obj.value1,"<?=$i_Booking_sepdayswrong?>")) return false;}
     /*
     if (obj.recordType[2].checked) {if (obj.value2.value == -1){ alert("<?=$i_Booking_PlsWeekDays?>"); return false;}}
     if (obj.recordType[3].checked) {if (obj.value3.value == -1){ alert("<?=$i_Booking_PlsCycleDays?>"); return false;}}
     */
     <?
     }
     else
     {
     ?>
     if (compareDate(obj.end.value,old_end) > 0) {alert("<?="$i_BookingWrongPeriodicEndDate $old_end $i_BookingWrongPeriodicEndDate2"?>"); return false;}
     <?
     }
     ?>
     if (compareDate(min_date,obj.start.value) > 0) {alert("<?=$start_alert?>"); return false;}
     if (compareDate(obj.start.value,obj.end.value) > 0) {alert("<?=$i_Booking_EndDateWrong?>"); return false;}
     return true;
}
</script>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<form name=form1 action="edit_periodic_update.php" method=post ONSUBMIT="return checkform(this)">
<?php
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"><img src='$rb_image_path/icon_periodic.gif'> $i_BookingEditPeriodic </font>","");
echo $lb->getTopPaper();
echo $lr->displayItem();
?><table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingPeriodicType?> :</td>
            <td width="579" align="left" valign="top">
            <?
              if ($lp->RecordStatus == 0)
              {
              ?>
              <input type="radio" name="recordType" value="0" <?=$type_checked[0]?>>
              <?=$i_BookingType_EveryDay?><br>
              <input type="radio" name="recordType" value="1" <?=$type_checked[1]?>>
              <?=$i_BookingType_EverySeparate?> <input type="text" name="value1" size="5" maxlength="2" value=<?=$value1?>> <?=$i_BookingType_Days?>
              <br><input type="radio" name="recordType" value="2" <?=$type_checked[2]?> ONCHANGE="this.form.recordType[2].checked=true">
              <?="$i_BookingType_Every $i_BookingType_Weekday"?>
              <select name="value2">
                <?php

                for ($i=0; $i<sizeof($i_DayType0); $i++)
                {

                     echo "<option value=$i";
                     if ($value2 != "" && $i == $value2) echo " SELECTED";
                     echo ">".$i_DayType0[$i]."</OPTION>\n";
                }
                ?>
              </select>
              <br>
              <?php
                $cycles_array = $lcycleperiods->getCycleNamesByDateRange($lp->BookingStartDate,$lp->BookingEndDate);
                if (sizeof($cycles_array)==0)
                {}
                else
                {
                    $select_cycle = getSelectByArray($cycles_array, "name=value3 ONCHANGE=\"this.form.recordType[3].checked=true\"",$value3,1,1);
              ?>
              <input type="radio" name="recordType" value="3" <?=$type_checked[3]?>>
              <?="$i_BookingType_Every $i_BookingType_Cycleday"?>
              <?=$select_cycle?>
              <?
              }
              ?>

              <?php
              /*
                $cid = get_file_content("$intranet_root/file/cycle.txt");
                if ($cid == "" || $cid == 0)
                {}
                else
                {
              ?>
              <input type="radio" name="recordType" value="3" <?=$type_checked[3]?>>
              <?="$i_BookingType_Every $i_BookingType_Cycleday"?>
              <select name="value3">
                <option>��<?="$i_BookingType_PleaseSelect $i_BookingType_Cycleday"?>��</option>
                <?php
                switch ($cid)
                {
                        case 1:
                             $target = ${"i_DayType1"};
                             $days = 6;
                             break;
                        case 2:
                             $target = ${"i_DayType1"};
                             $days = 7;
                             break;
                        case 3:
                             $target = ${"i_DayType1"};
                             $days = 8;
                             break;
                        case 4:
                             $target = ${"i_DayType2"};
                             $days = 6;
                             break;
                        case 5:
                             $target = ${"i_DayType2"};
                             $days = 7;
                             break;
                        case 6:
                             $target = ${"i_DayType2"};
                             $days = 8;
                             break;
                        case 7:
                             $target = ${"i_DayType3"};
                             $days = 6;
                             break;
                        case 8:
                             $target = ${"i_DayType3"};
                             $days = 7;
                             break;
                        case 9:
                             $target = ${"i_DayType3"};
                             $days = 8;
                             break;
                }
                for ($i=0; $i<$days; $i++)
                {
                     echo "<option value=$i";
                     if ($value3 != "" && $i == $value3) echo " SELECTED";
                     echo ">".$target[$i]."</OPTION>\n";
                }
                ?>
              </select>
              <?
              }*/
            }
            else if ($lp->RecordStatus == 4)
            {
                 if ($lp->RecordType != 3)
                 {
                     $type_str = $lb->returnTypeString($lp->RecordType, $lp->TypeValues);
                 }
                 else
                 {
                     $type_str = $lcycleperiods->returnCriteriaString($lp->TypeValues,$lp->BookingStartDate,$lp->BookingEndDate);
                 }
                 echo $type_str;

                #echo $lb->returnTypeString($lp->RecordType, $lp->TypeValues);
            }
              ?>

            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingStartDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=pid value=<?=$pid?>>
            <input type=text name=start length=80 value="<?=$lp->BookingStartDate?>">
     <script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
         startCal.setCurrentMonth(<?=$old_start_month?>-1);
         startCal.setCurrentYear(<?=$old_start_year?>);
    //-->
    </script>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingEndDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=text name=end length=80 value="<?=$lp->BookingEndDate?>">
     <script language="JavaScript" type="text/javascript">
    <!--
         endCal = new dynCalendar('endCal', 'calendarCallback2', '/templates/calendar/images/');
         endCal.setCurrentMonth(<?=$old_end_month?>-1);
         endCal.setCurrentYear(<?=$old_end_year?>);
    //-->
    </script>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingStatus?> :</td>
            <td width="579" align="left" valign="top">
            <?=$i_BookingStatusArray[$lp->RecordStatus]?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingTimeSlots?> :</td>
            <td width="579" align="left" valign="top">
            <?=$lp->listEditPeriodicTimeSlots()?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <?php
               if ($lbatch->Description != "")
               {
          ?>
          <tr>
            <td colspan=2 align=center valign="top">
<table width="300" border="1" cellspacing="0" cellpadding="5" bordercolor="#B9C7FF" bgcolor="#DDE6FF" class="body">
                <tr>
                  <td>
                    <p><?=$i_Batch_Description?> :</p>
                    <?=$lbatch->Description?>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <?
               }
          ?>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">Notes :</td>
            <td width="579" align="left" valign="top">
              <textarea name="Remark" cols="50" rows="5"><?=$lp->Remark?></textarea>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top"><input TYPE=image src="<?=$image_submit?>">
              <img src="<?=$image_cancel?>" onClick="history.back()"> </td>
          </tr>
</table>
<?
echo $lb->getLargeEndPaper();
echo $lb->getLargeEndBoard();
?>
</form>
</td>
</tr>
</table>

<?php
echo $lb->pageTitle();

include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>