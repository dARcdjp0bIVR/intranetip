<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
intranet_opendb();
auth_resource();

$rejected = 5;
$lb = new libdb();

$sql = "UPDATE INTRANET_BOOKING_RECORD SET UserCleared = 1 WHERE UserID = $UserID AND RecordStatus = $rejected AND BookingDate >= CURDATE()";
$lb->db_db_query($sql);

$sql = "UPDATE INTRANET_PERIODIC_BOOKING SET UserCleared = 1 WHERE UserID = $UserID AND RecordStatus = $rejected AND BookingEndDate >= CURDATE()";
$lb->db_db_query($sql);

intranet_closedb();
header("Location: index.php?signal=8");
?>