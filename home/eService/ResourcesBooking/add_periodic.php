<?php
// Editing by 
/*
 * 2019-06-05 (Carlos): To prevent cross-site scripting, applied escape_double_quotes() in elements value, applied intranet_htmlspecialchars() to output data.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
auth_resource();

$lb = new librb();
$lr = new libresource($rid);
$lbatch = new libbatch($lr->TimeSlotBatchID);
$lcycleperiods = new libcycleperiods();

echo $lb->pageTitle();

$daysBefore = $lr->DaysBefore;
$min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
$min_date_s = date('Y-m-d',$min_date);

$alert_msg = ($daysBefore==0? "$i_Booking_TodayOrLater":"$i_Booking_TooLate1 $daysBefore $i_Booking_TooLate2");

$cid = get_file_content("$intranet_root/file/cycle.txt");

$rb_image_path = "$image_path/resourcesbooking";

$form_string = ($start==""||$end=="")?
"<form name=form1 action=\"add_periodic.php\" method=GET>":
"<form name=form1 action=\"add_periodic_update.php\" method=post ONSUBMIT=\"return checkform(this)\">";


$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator."<a href=javascript:history.back()>".$button_new."</a>".$i_frontpage_separator.$i_BookingNewPeriodic;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>
	<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     #ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     isMenu = true;
     //isToolTip = true;
     isCheckHeight = false;
     </script>
     <div id="ToolMenu"></div>
     <div id="ToolTip"></div>
     
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>
     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--

          var min_date = '<?=$min_date_s?>';

          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {

                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   if (compareDate(dateValue,min_date) < 0)
                   {
                       alert("<?=$alert_msg?>")
                   }
                   else
                   {
                       document.forms['form1'].start.value = dateValue;
                       endCal.currentMonth = month-1;
                       endCal.currentYear = year;
                       if (document.forms['form1'].end.value == "")
                           document.forms['form1'].end.value = dateValue;
                   }
          }
          function calendarCallback2(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }

                   dateValue =year + '-' + month + '-' + date;
                   if (compareDate(dateValue,min_date) < 0)
                   {
                       alert("<?=$alert_msg?>")
                   }
                   else document.forms['form1'].end.value = dateValue;
          }
     // -->
     </script>

<script language="javascript">
function checkform(obj){
     var min_date = '<?=$min_date_s?>';
     if(countChecked(obj,"slots[]")==0) { alert(globalAlertMsg2); return false; }
     if (obj.recordType[1].checked) {if (!check_positive_nonzero_int(obj.value1,"<?=$i_Booking_sepdayswrong?>")) return false;}
/*
     if (obj.recordType[2].checked) {if (obj.value2.value == -1){ alert("<?=$i_Booking_PlsWeekDays?>"); return false;}}

     if (obj.recordType.length>3)
     {
         if (obj.recordType[3].checked) {if (obj.value3.value == -1){ alert("<?=$i_Booking_PlsCycleDays?>"); return false;}}
     }
*/
     if (!check_date(obj.start,"<?php echo $i_invalid_date; ?>")) return false;
     if (!check_date(obj.end,"<?php echo $i_invalid_date; ?>")) return false;
     if (compareDate(min_date,obj.start.value) > 0) {alert("<?=$alert_msg?>"); return false;}
     if (compareDate(obj.start.value,obj.end.value) > 0) {alert("<?=$i_Booking_EndDateWrong?>"); return false;}
     return true;
}
</script>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<?=$form_string?>
<?php
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"><img src='$rb_image_path/icon_periodic.gif'> $i_BookingNewPeriodic </font>","");
echo $lb->getTopPaper();
?>
<table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
<tr><td width=50></td><td><?=getStepIcons(3,3,$i_BookingAddStep)?></td>
<tr><td height=10 colspan=2></td></tr>
</tr>
</table>
<?
echo $lr->displayItem();
?><table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
<? if ($start == "" || $end == "")  { ?>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingStartDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=rid value="<?=escape_double_quotes($rid)?>">
            <input type=text name=start length=80 value="<?=escape_double_quotes($min_date_s)?>"> (yyyy-mm-dd)
            <?=$i_Booking_DateSelectOr?>
     <script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingEndDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=text name=end length=80 value=""> (yyyy-mm-dd)
            <?=$i_Booking_DateSelectOr?>
     <script language="JavaScript" type="text/javascript">
    <!--
         endCal = new dynCalendar('endCal', 'calendarCallback2', '/templates/calendar/images/');
    //-->
    </script>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
<? } else  {?>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingStartDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=rid value="<?=escape_double_quotes($rid)?>">
            <input type=hidden name=start value="<?=escape_double_quotes($start)?>"><?=intranet_htmlspecialchars($start)?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingEndDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=end value="<?=escape_double_quotes($end)?>"><?=intranet_htmlspecialchars($end)?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingPeriodicType?> :</td>
            <td width="579" align="left" valign="top">
              <input type="radio" name="recordType" value="0" checked>
              <?=$i_BookingType_EveryDay?><br>
              <input type="radio" name="recordType" value="1">
              <?=$i_BookingType_EverySeparate?> <input type="text" name="value1" size="5" maxlength="2" ONFOCUS="this.form.recordType[1].checked=true" > <?=$i_BookingType_Days?>
              <br><input type="radio" name="recordType" value="2">
              <?="$i_BookingType_Every $i_BookingType_Weekday"?>
              <select name="value2" ONCHANGE="this.form.recordType[2].checked=true">
                <?php
                for ($i=0; $i<sizeof($i_DayType0); $i++)
                {
                     echo "<option value=$i>".$i_DayType0[$i]."</OPTION>\n";
                }
                ?>
              </select>
              <br>
              <?php
                $cycles_array = $lcycleperiods->getCycleNamesByDateRange($start,$end);
                if (sizeof($cycles_array)==0)
                {}
                else
                {
                    $select_cycle = getSelectByArray($cycles_array, "name=value3 ONCHANGE=\"this.form.recordType[3].checked=true\"","",1,1);
              ?>
              <input type="radio" name="recordType" value="3">
              <?="$i_BookingType_Every $i_BookingType_Cycleday"?>
              <?=$select_cycle?>
              <?
              }
              ?>

            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingTimeSlots?> :</td>
            <td width="579" align="left" valign="top">
            <?=$lb->listPeriodicTimeSlots($rid)?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <?php
               if ($lbatch->Description != "")
               {
          ?>
          <tr>
            <td colspan=2 align=center valign="top">
<table width="300" border="1" cellspacing="0" cellpadding="5" bordercolor="#B9C7FF" bgcolor="#DDE6FF" class="body">
                <tr>
                  <td>
                    <p><?=$i_Batch_Description?> :</p>
                    <?=intranet_htmlspecialchars($lbatch->Description)?>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <?
               }
          ?>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"><?=$i_BookingNotes?> :</td>
            <td width="579" align="left" valign="top">
              <textarea name="Remark" cols="50" rows="5"></textarea>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
<? } ?>

          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top"><input TYPE=image src="<?=$image_submit?>">
              <img src="<?=$image_cancel?>" onClick="history.back()"> </td>
          </tr>
</table>
<?
echo $lb->getLargeEndPaper();
echo $lb->getLargeEndBoard();
?>
</form>
</td>
</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>