<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();
auth_resource();

$lb = new librb();

echo $lb->pageTitle();

if (!$lb->hasRightToReserve($UserID))
{
     header("Location: index.php");
     exit();
}

$rb_image_path = "$image_path/resourcesbooking";
?>
<script language="Javascript" src='/templates/brwsniff.js'></script>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     isMenu = true;
     //isToolTip = true;
     isCheckHeight = false;
     </script>
     <div id="ToolMenu"></div>
     <div id="ToolTip"></div>
<?php
$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator.$button_new;
$preload_images[] = "$rb_image_path/$image_single_b";
$preload_images[] = "$rb_image_path/$image_periodic_b";
$body_tags = "onMouseMove=\"overhere()\"";
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>

<script language="javascript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function checkform(obj){
     if(countChecked(obj,"BookingPeriod[]")==0) { alert(globalAlertMsg2); return false; }
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
// -->

</script>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<?php
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"> $button_new </font>","");
echo $lb->getTopPaper();
?>

      <table width="729" border="0" cellspacing="0" cellpadding="0" background="<?=$rb_image_path?>/framebglightblue.gif">
        <tr>
          <td width="40">&nbsp;</td>
          <td><img src="<?=$i_BookingItemList?>"></td>
        </tr>
      </table>
      <table width="729" border="0" cellspacing="0" cellpadding="10" background="<?=$rb_image_path?>/framebglightblue.gif" class="body">
        <tr>
          <td width="40">&nbsp;</td>
          <td width="639"><?=$i_BookingAddRule?></td>
          <td width="50">&nbsp;</td>
        </tr>
        <tr>
          <td width="40">&nbsp;</td>
          <td width="639"><?=getStepIcons(3,1,$i_BookingAddStep)?></td>
          <td width="50">&nbsp;</td>
        </tr>
      </table>
<?
echo $lb->displayAvailableItems();
echo $lb->getLargeEndPaper();
echo $lb->getLargeEndBoard();
?>

</td>
</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>