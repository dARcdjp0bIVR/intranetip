<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$li = new libdb();
$Remark = htmlspecialchars(trim($Remark));
$RecordStatus = 4; # Pending
for($i=0; $i<sizeof($BookingPeriod); $i++){
     $TimeRange = split(":", $BookingPeriod[$i]);
     $DateStart = date("Y-m-d H:i:s", ($ts+$TimeRange[0]));
     $DateEnd = date("Y-m-d H:i:s", ($ts+$TimeRange[1]));
     $sql = "SELECT COUNT(BookingID) FROM INTRANET_BOOKING WHERE ResourceID = $ResourceID AND DateStart = '$DateStart' AND DateEnd = '$DateEnd' AND ( RecordStatus IN (0,2,3) OR (RecordStatus=4 AND UserID=$UserID) )";
     $count = $li->returnVector($sql);
     if ($count[0]!=0) continue;
     $sql = "INSERT INTO INTRANET_BOOKING (ResourceID, UserID, DateStart, DateEnd, Remark, RecordStatus, DateInput, DateModified) VALUES ($ResourceID, $UserID, '$DateStart', '$DateEnd', '$Remark', '$RecordStatus', now(), now())";
     $li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?ts=$ts&msg=1");

?>