<?php
/*
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
intranet_auth();
intranet_opendb();

$rid = IntegerSafe($rid);
$li = new libresource($rid);
$Remark = intranet_htmlspecialchars(stripslashes(trim($Remark)));
$start = intranet_htmlspecialchars(trim($start));
$end = intranet_htmlspecialchars(trim($end));
$value1 = intranet_htmlspecialchars(trim($value1));

$RecordStatus = 0; # Pending
$timeSlotsList = implode(",", $slots);
# Check date
# Start date >= curdate() && Start date <= due date
$start_stamp = strtotime($start);
$end_stamp = strtotime($end);

$daysBefore = $li->DaysBefore;
$daysBefore += 0;
$min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
$msg = 0;

#$lc = new libcycle();
$lcycleperiods = new libcycleperiods();

if (compareDate($start_stamp,$min_date) < 0 || compareDate($end_stamp,$start_stamp)<0)
{
    $success = 0;
    $msg = 6;
}
else
{
    $target = array(0);
    $temp = $start_stamp;
    $count = 0;

    if ($recordType == 0)
    {
        while ($temp <= $end_stamp)
        {
               $target[$count] = date('Y-m-d',$temp);
               $count++;
               $temp += 86400;
        }
        $typeValue = 0;
    }
    else if ($recordType == 1)
    {
        if (!is_numeric($value1) || $value1 < 1) {
             header ("Location: index.php?signal=6");
             exit();
        }
        while ($temp <= $end_stamp)
        {
               $target[$count] = date('Y-m-d',$temp);
               $count++;
               $temp += 86400*($value1);
        }
        $typeValue = $value1;
    }
    else if ($recordType == 2)
    {
        // Weekdays
        while (date("w",$temp) != $value2) $temp += 86400;
        while ($temp <= $end_stamp)
        {
               $target[$count] = date('Y-m-d',$temp);
               $count++;
               $temp += 86400*7;
        }
        $typeValue = $value2;
    }
    else if ($recordType == 3)
    {
        // Cycle Days
        $targetCycle = $value3;
        $dates_array = $lcycleperiods->getDatesForCycleDay($start,$end,$targetCycle);
        $target = $dates_array;
        $typeValue = $value3;
    }

    if ($target[0] == 0)
    {
        $success = 0;
        $msg = 6;
    }
    else
    {
        $dates_str = "'".implode("','",$target)."'";

        # Check availability
        /*
        $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
        $username_field = ($chi? "ChineseName": "EnglishName");
        */
        $username_field = getNameFieldByLang("b.");

        $fields_name = "a.BookingDate, c.Title, c.TimeRange, CONCAT($username_field,if (b.ClassNumber IS NOT NULL,CONCAT(' - ',b.ClassName,' (',b.ClassNumber,')'),''))";
        $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d";
        $join_conds = "a.UserID = b.UserID AND c.SlotSeq = a.TimeSlot AND a.ResourceID = d.ResourceID AND d.TimeSlotBatchID = c.BatchID";
        $conds = "a.RecordStatus IN (2,3,4) AND d.ResourceID = '$rid' AND a.BookingDate IN ($dates_str) AND a.TimeSlot IN ($timeSlotsList)";
        $sql = "SELECT $fields_name FROM $db_tables WHERE $join_conds AND $conds ORDER BY a.BookingDate ASC, a.TimeSlot ASC";
        $reserved = $li->returnArray($sql,4);
        $users_list = "";
        for ($i=0; $i<sizeof($target); $i++)
             $names[$i] = "Available";
        for ($i=0; $i<sizeof($reserved); $i++)
        {
             list ($bdate,$slotname,$slottime,$username) = $reserved[$i];
             $slotname = intranet_wordwrap($slotname,20," ",1);
             $slottime = intranet_wordwrap($slottime,20," ",1);
             $username = intranet_wordwrap($username,20," ",1);
             $users_list .= "$delimiter $slotname $slottime ($username)";
             if ($reserved[$i+1][0]==$bdate)
             {
                 $delimiter = ",<br>";
                 continue;
             }
             $pos = search_element ($bdate,$target);
             if ($pos != -1)
             {
                 $names[$pos] = $users_list;
                 $users_list = "";
                 $delimiter = "";
             }
        }
    }
}
if ($msg != 0)
{
    header("Location: index.php?signal=$msg");
}
else
{
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
    ?>
    <script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/brwsniff.js"></script>
    <?
    include_once($PATH_WRT_ROOT."templates/homeheader.php");

    ?>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<form name=form1 action=add_periodic_update_confirm.php method=post>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<?php
$lb = new librb();
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"><img src='$rb_image_path/icon_periodic.gif'> $i_BookingPeriodicConfirm </font>","");
echo $lb->getTopPaper();
$lr = new libresource($rid);
$item_details = $lr->displayItem();
if ($recordType != 3)
    $typeStr = $lb->returnTypeString($recordType,${"value$recordType"});
else $typeStr = $lcycleperiods->returnCriteriaString($value3,$start,$end);
$sql = "SELECT CONCAT(a.Title,' ', a.TimeRange) FROM INTRANET_SLOT as a, INTRANET_RESOURCE as b WHERE a.BatchID = b.TimeSlotBatchID AND b.ResourceID = '$rid' AND a.SlotSeq IN ($timeSlotsList) ORDER BY a.SlotSeq";
$target_slotnames = $li->returnVector($sql);

?>
        <table width="729" border="0" cellspacing="0" cellpadding="10" background="/images/resourcesbooking/framebglightblue.gif" class="body" height="50">
          <tr>
            <td width="20">&nbsp;</td>
            <td width="709">
              <p><?=$i_BookingPeriodic_confirmMsg1?></p>
              </td>
          </tr>
        </table><?=$item_details?>
<table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingPeriodicType?> :</td>
            <td width="579" align="left" valign="top"><?=$typeStr?></td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingStartDate?> :</td>
            <td width="579" align="left" valign="top">
            <?=$start?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingEndDate?> :</td>
            <td width="579" align="left" valign="top">
            <?=$end?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingTimeSlots?> :</td>
            <td width="579" align="left" valign="top">
            <?=implode("<br>",$target_slotnames)?>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">Notes :</td>
            <td width="579" align="left" valign="top"><?=intranet_wordwrap($Remark,40,"\n",1)?>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr align="center">
            <td colspan="2" valign="top">
              <table width="500" border="1" cellspacing="0" cellpadding="5" bordercolor="#B9C7FF" bgcolor="#DDE6FF" class="body">
                <tr>
                  <td>
                    <p><?=$i_BookingPeriodic_confirmMsg2?></p>
                    <table width="350" border="0" cellspacing="5" cellpadding="0" align="center" class="body">
                      <tr>
                        <td width=150><i><?=$i_BookingDate?></i></td>
                        <td width=200><i><?=$i_BookingStatus?></i></td>
                      </tr>
                      <?php
                      $cycleDays = $lcycleperiods->getCycleInfoByDateArray($target);
                      $available_count = 0;

                      for ($i=0; $i<sizeof($target); $i++)
                      {
                           $tempDate = $target[$i];
                           if ($names[$i]=="Available")
                           {
                               $text = "<font color=\"#000099\">".$i_BookingAvailable."</font>
                                  <input type=hidden name=dates[] value='$tempDate'>";
                               $available_count++;
                           }
                           else
                           {
                               $text = "<font color=\"#FF0000\">$i_BookingUnavailable</font><br><font color=#AF1919><i>".$names[$i]."</i></font>";
                           }
                           $array = $cycleDays[$tempDate];
                           if (is_array($array))
                           {
                               list ($txtEng, $txtChi, $txtShort) = $array;
                               if ($intranet_session_language=="en")
                               {
                                   if ($txtEng != "")
                                       $name = $txtEng;
                                   else if ($txtChi != "")
                                        $name = $txtChi;
                               }
                               else
                               {
                                   if ($txtChi != "")
                                       $name = $txtChi;
                                   else if ($txtEng != "")
                                        $name = $txtEng;
                               }
                               $name = ($name==""? $txtShort:$name);
                               $cycleNum = ($name!=""? "($name)" :"");
                           }
                           else
                           {
                               $cycleNum = "";
                           }
                           echo "<tr>
                                  <td>$tempDate $cycleNum</td>
                                  <td>$text</td>
                                  </tr>
                                  ";
                      }
                      ?>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          </tr>
          <tr align="center">
            <td colspan="2" valign="top"><img src="/images/line.gif" width="616" height="1"></td>
          </tr>
        </table>
        <table width="729" border="0" cellspacing="0" cellpadding="10" background="/images/resourcesbooking/framebglightblue.gif" class="body" height="50">
          <tr>
            <td width="20">&nbsp;</td>
            <td width="709">
              <p><?=($available_count==0?$i_BookingPeriodic_NoMatch:$i_BookingPeriodic_confirmMsg3)?></p>
            </td>
          </tr>
        </table>
        <table width="729" border="0" cellspacing="0" cellpadding="0" background="/images/resourcesbooking/framebglightblue.gif">
          <tr>
            <td width="150">&nbsp;</td>
            <td width="579"><?=($available_count==0?"":"<input type=image border=0 src=\"$image_ok\">")?><a href=javascript:history.back()><img border=0 src="<?=$image_back?>"></a><a href=index.php><img border=0 src="<?=$image_cancel?>"></a></td>
          </tr>
        </table>
<?
        echo $lb->getLargeEndPaper();
        echo $lb->getLargeEndBoard();
        ?><br>
        <input type=hidden name=rid value=<?=$rid?>>
        <input type=hidden name=recordType value=<?=$recordType?>>
        <input type=hidden name=typeValue value="<?=$typeValue?>">
        <input type=hidden name=start value="<?=$start?>">
        <input type=hidden name=end value="<?=$end?>">
        <input type=hidden name=Remark value="<?=cleanHtmlJavascript($Remark)?>">
        <input type=hidden name=timeSlotsList value="<?=$timeSlotsList?>">
		<?php echo csrfTokenHtml(generateCsrfToken());?>

</form>
<?php
      include_once($PATH_WRT_ROOT."templates/homefooter.php");
}
intranet_closedb();


function search_element($needle, $target_array)
{
         for ($i=0; $i<sizeof($target_array); $i++)
         {
              if ($needle == $target_array[$i])
                  return $i;
         }
         return -1;
}
?>