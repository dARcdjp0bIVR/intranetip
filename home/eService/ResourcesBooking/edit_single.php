<?php
/*
 *  2019-05-13 Cameron
 *       - apply IntegerSafe() to $bid
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_resource();


$bid = IntegerSafe($bid);

$lb = new librb();
$lb->retrieveBooking($bid);
$status = $lb->RecordStatus;
if ($status != 0 && $status != 4)
    header("Location: index.php");
else
{
$rid = $lb->ResourceID;
$lr = new libresource($rid);
$lbatch = new libbatch($lr->TimeSlotBatchID);

$rb_image_path = "/images/resourcesbooking";

$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator.$i_BookingEditSingle;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>

<script language="javascript">
function checkform(obj){
     if(countChecked(obj,"slots[]")==0) { alert(globalAlertMsg2); return false; }
     return true;
}
</script>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<form name=form1 action="edit_single_update.php" method=post ONSUBMIT="return checkform(this)">
<?php
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"><img src='$rb_image_path/icon_single.gif'> $i_BookingEditSingle </font>","");
echo $lb->getTopPaper();
echo $lr->displayItem();
?><table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=bid value=<?=$bid?>>
            <?=$lb->BookingDate?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"><?=$i_BookingStatus?> :</td>
            <td width="579" align="left" valign="top">
            <?=$i_BookingStatusArray[$lb->RecordStatus]?>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingTimeSlots?> :</td>
            <td width="579" align="left" valign="top">
            <?=$lb->listEditTimeSlots()?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <?php
               if ($lbatch->Description != "")
               {
          ?>
          <tr>
            <td colspan=2 align=center valign="top">
<table width="300" border="1" cellspacing="0" cellpadding="5" bordercolor="#B9C7FF" bgcolor="#DDE6FF" class="body">
                <tr>
                  <td>
                    <p><?=$i_Batch_Description?> :</p>
                    <?=$lbatch->Description?>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <?
               }
          ?>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"><?=$i_BookingRemark?> :</td>
            <td width="579" align="left" valign="top">
              <textarea name="Remark" cols="50" rows="5"><?=$lb->Remark?></textarea>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top"><input TYPE=image src="<?=$image_submit?>">
              <img src="<?=$image_cancel?>" onClick="history.back()"> </td>
          </tr>
</table>
<?
echo $lb->getLargeEndPaper();
echo $lb->getLargeEndBoard();
?>
</form>
</td>
</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
}
echo $lb->pageTitle();

intranet_closedb();
?>