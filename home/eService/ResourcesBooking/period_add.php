<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycle.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libresource($ResourceID);
$lc = new libcycle();

$navigation = $i_frontpage_separator."<a class=home href=javascript:history.back()>$i_frontpage_rb</a>".$i_frontpage_separator.$button_add;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>

<script language="javascript">
function checkform(obj){
     if(countChecked(obj,"BookingPeriod[]")==0) { alert(globalAlertMsg2); return false; }

}
</script>

<?php echo $i_frontpage_rb_title_add; ?>
<blockquote>
<p>
<table border=0 cellpadding=0 cellspacing=0>
<tr><td colspan=3><?php echo $i_frontpage_rb_title_form; ?></td></tr>
<tr>
<td><img src=../../images/frontpage/myinfo_bg_tl.gif border=0 width=12 height=12></td>
<td class=myinfo_bg_tm><img src=../../images/space.gif border=0 width=13 height=12></td>
<td><img src=../../images/frontpage/myinfo_bg_tr.gif border=0 width=23 height=12></td>
</tr>
<tr>
<td class=myinfo_bg_ml><img src=../../images/space.gif border=0 width=12 height=12></td>
<td class=myinfo_bg_mm>

<form action=add_update.php method=post onSubmit="return checkform(this);">
<?php echo $li->display(); ?>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td><?php echo $i_ResourceDateStart; ?>:</td><td><input size=10 MAXLENGTH=10 type=text name=startdate value="<?php echo date("Y-m-d", $ts); ?>"></td></tr>
<tr><td><?php echo $i_ResourceDateEnd; ?>:</td><td><input size=10 MAXLENGTH=10 type=text name=enddate value="<?php echo date("Y-m-d", $ts+604800); ?>"></td></tr>
<tr><td><?php echo $i_ResourcePeriodType; ?>:</td><td>
<?php echo $lc->getSelectDaysCycleDays("name=dayType");?>
</td></tr>
<tr><td><?php echo $i_frontpage_rb_period; ?>:</td><td><?php echo $li->displayBookingCheck($ts, $UserID); ?></td></tr>
<tr><td><?php echo $i_frontpage_rb_note; ?>:</td><td><textarea name=Remark cols=30 rows=5></textarea></td></tr>
<tr><td width=30%><br></td><td><input type=image src=<?php echo $i_frontpage_image_submit; ?> border=0> <a href=javascript:onClick=history.back()><?php echo $i_frontpage_button_cancel; ?></a></td></tr>
</table>
<input type=hidden name=ResourceID value="<?php echo $ResourceID; ?>">
<input type=hidden name=ts value="<?php echo $ts; ?>">
</form>

</td>
<td class=myinfo_bg_mr><img src=../../images/space.gif border=0 width=23 height=12></td>
</tr>
<tr>
<td><img src=../../images/frontpage/myinfo_bg_bl.gif border=0 width=12 height=23></td>
<td class=myinfo_bg_bm><img src=../../images/space.gif border=0 width=13 height=23></td>
<td><img src=../../images/frontpage/myinfo_bg_br.gif border=0 width=23 height=23></td>
</tr>
</table>
</blockquote>

<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>