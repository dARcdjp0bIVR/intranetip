<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_resource();

$lb = new librb();
$lr = new libresource();

echo $lb->pageTitle();

if ($ts == "") $ts = time();
$lbatch = new libbatch();

$filter_cat = $lr->getSelectCats("name=cat onChange=\"this.form.submit();\"",$cat,$lbatch->BatchID,false);
$filter_item = $lr->getSelectItems("name=item onChange=\"this.form.submit();\"",$item,$cat,$lbatch->BatchID,false);

$rb_image_path = "/images/resourcesbooking";

$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator.$i_frontpage_menu_resourcebooking_reserved;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>
<form name=form1 method=get>
<table width=794 border=0 cellpadding=0 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align=center>
      <table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td><?php echo $i_frontpage_rb_title_reserved; ?></td>
          <td align="right" valign="bottom">
				<?if($intranet_version=="2.5") {?>
				<br><br><a class="functionlink" href="./"><?=$i_Community_Community_GoTo?><?=$intranet_session_language=="en"?" ":""?><?=$i_frontpage_menu_resourcebooking_myrecords?></a>
				<? } ?>
		  </td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <br>
      <table width="780" border="0" cellpadding="0" cellspacing="0" background="/images/resourcesbooking/framebg2.gif">
        <tr>
            <td colspan="4"><img src="/images/resourcesbooking/frametop2.gif"></td>
        </tr>
        <tr>
          <td width="30">&nbsp;</td>
            <td width="400" align="left" valign="middle" class="body"><?=$i_Booking_filter?> :
              <?=$filter_cat?>
              |
              <?=$filter_item?>
            <td width="325" align="right" valign="top" class="functionlink">
             <span>
              <a class="functionlink" href=reserved.php?ts=<?=$ts?>><img border=0 src="/images/resourcesbooking/icon_timeslot_o.gif"><font color=gray><?=$i_Booking_ViewSchool?></font></a>
             </span>|
             <span>
              <a class="functionlink" href=otherrecords.php?ts=<?=$ts?>><img border=0 src="/images/resourcesbooking/icon_specialtimeslot.gif"><?=$i_Booking_ViewOther?></a>
             </span>
          <td width="25">&nbsp;</td>
        </tr>
        <tr>
          <td colspan=4>&nbsp;</td>
        </tr>
      </table>
      <table width="780" border="0" cellpadding="0" cellspacing="0" background="/images/resourcesbooking/framebg2.gif">
          <tr>
            <td align="center">
            <?=($item==""? $lb->displayReservedOther($ts,$cat,$item): $lb->displayReservedStandard($ts,$cat,$item) )?>
            </td>
          </tr>
      </table>
      <table width="780" border="0" cellpadding="0" cellspacing="0" background="/images/resourcesbooking/framebg2.gif">
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
      </table>
      <table width="780" border="0" cellpadding="0" cellspacing="0" background="/images/resourcesbooking/framebg2.gif">
        <tr>
          <td align="center">
            <a href="javascript:document.form1.ts.value=<?=$ts-604800?>; document.form1.submit()"><?=$image_prevweek?></a><a href="javascript:document.form1.ts.value=<?=$ts+604800?>; document.form1.submit()"><?=$image_nextweek?></a>
          </td>
        </tr>
      </table>
      <table width="780" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><img src="/images/resourcesbooking/framebottom2.gif"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type=hidden name=ts value=<?=$ts?>>
</form>
<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>