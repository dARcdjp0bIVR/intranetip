<?php

# using: 

###################################################
#
#   Date:   2019-06-04 Henry
#           - added CSRF protection
#
#   Date:   2019-05-13 Cameron
#           - fix potential sql injection problem by enclosing var with apostrophe
#
#	Date:	2011-11-08	YatWoon
#			Fixed: the calendar ignore to display the booking status if the item/room "Minimum days in advance for booking" is set.
#
###################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
auth_resource();

$lb = new librb();
$lr = new libresource($rid);

echo $lb->pageTitle();

$daysBefore = $lr->DaysBefore;
$min_date = mktime(0,0,0,date("m"),(date("d")+$daysBefore),date("Y"));
$min_date_s = date('Y-m-d',$min_date);
$max_date = mktime(0,0,0,date("m")+12,(date("d")+$daysBefore),date("Y"));
$max_date_s = date('Y-m-d',$max_date);

$sql = "SELECT count(SlotID) FROM INTRANET_SLOT WHERE BatchID = '".$lr->TimeSlotBatchID."'";
$res = $lr->returnVector($sql);
$count = $res[0];

// $sql = "SELECT DATE_FORMAT(BookingDate,'%Y%m%d') , IF(COUNT(BookingID)>=$count,2,1) FROM INTRANET_BOOKING_RECORD WHERE RecordStatus IN (2,3,4) AND ResourceID = $rid AND BookingDate >='$min_date_s' AND BookingDate <= '$max_date_s' GROUP BY BookingDate";
$sql = "SELECT DATE_FORMAT(BookingDate,'%Y%m%d') , IF(COUNT(BookingID)>=$count,2,1) FROM INTRANET_BOOKING_RECORD WHERE RecordStatus IN (2,3,4) AND ResourceID = '$rid' AND BookingDate >=CURDATE() AND BookingDate <= '$max_date_s' GROUP BY BookingDate";
$status_array = $lr->returnArray($sql,2);

$legend = "<table width=100% border=0><tr><td align=center class=dynCalendar_full>$i_BookingLegendFull</td></tr><tr><td align=center class=dynCalendar_half>$i_BookingLegendHalf</td></tr><tr><td align=center class=dynCalendar_free>$i_BookingLegendFree</td></tr>";

$alert_msg = ($daysBefore==0? "$i_Booking_TodayOrLater":"$i_Booking_TooLate1 $daysBefore $i_Booking_TooLate2");

$rb_image_path = "/images/resourcesbooking";
$navigation = $i_frontpage_separator."<a href=/home/resource/>$i_frontpage_rb</a>".$i_frontpage_separator."<a href=add.php>".$button_new."</a>".$i_frontpage_separator.$i_BookingNewSingle;
echo '<script language="Javascript" src=\'/templates/brwsniff.js\'></script>';
include_once($PATH_WRT_ROOT."templates/homeheader.php");
?>
	<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     #ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     isMenu = true;
     //isToolTip = true;
     isCheckHeight = false;
     </script>
     <div id="ToolMenu"></div>
     <div id="ToolTip"></div>
     
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
         <?
         for ($i=0; $i<sizeof($status_array); $i++)
         {
              list($d, $s) = $status_array[$i];
              echo "date_array[$d] = $s;\n";
         }
         ?>

     </script>
     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--

          var min_date = '<?=$min_date_s?>';

          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   if (compareDate(dateValue,min_date) < 0)
                   {
                       alert("<?=$alert_msg?>")
                   }
                   else
                   {
                       document.forms['add'].bdate.value = dateValue;
                       document.forms['add'].submit();
                   }
          }
     // -->
     </script>
<script language="javascript">
function checkform(obj){
     var dateChosen = <?=($bdate==""? "false":"true")?>;
     if (dateChosen)
     {
         if(countChecked(obj,"slots[]")==0) { alert(globalAlertMsg2); return false; }
     }
     else
     {
         if (!check_date(obj.bdate,"<?php echo $i_invalid_date; ?>")) return false;
         var min_date = '<?=$min_date_s?>';
         if (compareDate(min_date,obj.bdate.value) > 0) {alert("<?=$alert_msg?>"); return false;}
     }
     return true;
}

</script>
<table width=794 border=0 cellpadding=0 cellspacing=0>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
<tr>
          <td width="25">&nbsp;</td>
<td><?php echo $i_frontpage_rb_title; ?></td>
</tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
</table>
<table width=794 border=0 cellpadding=5 cellspacing=0>
<tr>
<td align=center>
<form name=add action="" method=get ONSUBMIT="return checkform(this)">
<?php
echo $lb->getLargeTitleBar($i_BookingMyRecords, "<font color=\"#FF6600\"><img src='$rb_image_path/icon_single.gif'> $i_BookingNewSingle </font>","");
echo $lb->getTopPaper();
?>
<table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
<tr><td width=50></td><td><?=getStepIcons(3,3,$i_BookingAddStep)?></td>
<tr><td height=10 colspan=2></td></tr>
</tr>
</table>
<?
echo $lr->displayItem();
?><table width="729" border="0" cellspacing="0" cellpadding="2" background="/images/resourcesbooking/framebglightblue.gif" class="body">
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingDate?> :</td>
            <td width="579" align="left" valign="top">
            <input type=hidden name=rid value=<?=$rid?>>
            <?php
              if ($bdate=="")
              {
            ?>
            <input type=text name=bdate length=80 value="<?=$bdate?>"> (yyyy-mm-dd)
            <?=$i_Booking_DateSelectOr?>
            <?
              }
              else
              {
                  $lcycleperiods = new libcycleperiods();
                  $cycle = $lcycleperiods->getCycleDayStringByDate($bdate);
                  $scycle = ($cycle==""?"":"($cycle)");
                  /*
                  $cycle = $lc->getCycleDayFromDate($bdate);
                  $i_DayType = ${"i_DayType".$lc->CycleType};
                  $scycle = ($cycle==-1? "": "(".$i_DayType[$cycle].")");
                  */
                  echo "<input type=hidden name=bdate value=\"$bdate\">$bdate $scycle";
              }
            ?>
     <script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
         startCal.setLegend('<?=$legend?>');
         startCal.differentDisplay = true;
    //-->
    </script>
            <?php
              if ($bdate=="")
              {
            ?>
            &nbsp;<input type=image src=<?=$image_viewSlots?> alt='<?=$i_BookingRetrieveTimeSlots?>'>
            <? } ?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
<?php
   if ($bdate != "")
   {
          $lbatch = new libbatch($lr->TimeSlotBatchID);
   ?>
          <tr>
            <td width="150" align="right" valign="top"> <?=$i_BookingTimeSlots?> :</td>
            <td width="579" align="left" valign="top">
            <?=$lb->listTimeSlots($rid,$bdate)?>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
          <?php
               if ($lbatch->Description != "")
               {
          ?>
          <tr>
            <td colspan=2 align=center valign="top">
<table width="300" border="1" cellspacing="0" cellpadding="5" bordercolor="#B9C7FF" bgcolor="#DDE6FF" class="body">
                <tr>
                  <td>
                    <p><?=$i_Batch_Description?> :</p>
                    <?=$lbatch->Description?>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <?
               }
          ?>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top"><?=$i_BookingNotes?> :</td>
            <td width="579" align="left" valign="top">
              <textarea name="Remark" cols="50" rows="5"></textarea>
            </td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td width="150" align="right" valign="top">&nbsp;</td>
            <td width="579" align="left"s valign="top"><input TYPE=image src="<?=$image_submit?>" onClick="this.form.action='add_single_update.php'; this.form.method='post'">
              <a href=add.php><img src="<?=$image_cancel?>" border=0></a> </td>
          </tr>
   <?php
   }
   ?>
</table>
<?
echo $lb->getLargeEndPaper();
echo $lb->getLargeEndBoard();
echo csrfTokenHtml(generateCsrfToken());
?>
</form>
</td>
</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."templates/homefooter.php");
intranet_closedb();
?>