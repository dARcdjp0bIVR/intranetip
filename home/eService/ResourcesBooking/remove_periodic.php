<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpb.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
intranet_opendb();
auth_resource();

$status_cancelled = 1;

$failed = false;
$lp = new libpb();
$lp->retrievePeriodicBooking($pid);
if ($lp->AppliedUserID != $UserID)
{
    $failed = true;
    header("Location: index.php");
}
else
{
    $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = $status_cancelled WHERE PeriodicBookingID = $pid";
    $lp->db_db_query($sql);
    $sql = "UPDATE INTRANET_PERIODIC_BOOKING SET RecordStatus = $status_cancelled WHERE PeriodicBookingID = $pid";
    $lp->db_db_query($sql);
}
intranet_closedb();
header("Location: index.php?signal=5");
?>