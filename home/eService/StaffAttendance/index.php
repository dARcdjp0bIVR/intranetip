<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) 
	|| ($_SESSION['UserType'] != USERTYPE_STAFF)) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if($sys_custom['StaffAttendance']['DailyAttendanceRecord']){
	intranet_closedb();
	header("Location: daily_attendance_record.php");
	exit();
}else if($sys_custom['StaffAttendance']['DoctorCertificate']){
	intranet_closedb();
	header("Location: doctor_certificate.php");
	exit();
}else{
	intranet_closedb();
	header ("Location: /");
	exit();
}

intranet_closedb();
?>