<form name="form1" id="form1" method="POST" action="?forminfo<?=$eFormConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>
		<?=$htmlAry['maxRow']?>
		
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
	eClassFormJS.cfn.init();
});

function goBack() {
	window.location.href = "?task=forminfo/list";
}

function goSearch() {
	$('form#form1').submit();
}

function goEdit(){
	checkEdit(document.form1,'groupIdAry[]','?task=forminfo<?=$eFormConfig['taskSeparator']?>edit');
}
function goDelete(){
	checkRemove(document.form1,'groupIdAry[]','?task=forminfo<?=$eFormConfig['taskSeparator']?>delete');
}

function goNewForm() {
	window.location.href = "?task=forminfo<?=$eFormConfig['taskSeparator']?>edit";
}

var eClassFormJS = {
	vrs: {
		formBlrContainer: ".thickboxBTN",
	},
	ltr: {
		disableHandler: function(e) {
			e.preventDefault();
		},
		formBlrClickHandler: function(e) {
			e.preventDefault();
			var url = $(this).attr('rel');
			var title = $(this).attr('title');
			var extraparams = title;
			eClassFormJS.cfn.openFormBuilder(url, extraparams);
		},
	},
	cfn: {
		init: function() {
			if ($(eClassFormJS.vrs.formBlrContainer).length > 0) {
				$(eClassFormJS.vrs.formBlrContainer).bind('click', eClassFormJS.ltr.formBlrClickHandler);
			}
			$('a.linkdisable').bind('click', eClassFormJS.ltr.disableHandler);
			$(window).bind('hashchange', eClassFormJS.ltr.disableHandler);
			var strHash = window.location.hash;
			if (typeof strHash != "undefined" && strHash != "") {
				strHash = strHash.replace("#", "");
				alert(strHash);
				window.location.hash = "";
			}
		},
		openFormBuilder: function(url, extraparams) {
			var isUse = 1;
			var defaultHeight = "";
			var defaultWidth = "";
			var tbType = 1;
			var callBackFunc = "updateFromThickbox";
			var inlineID = "";
			show_dyn_thickbox_ip(extraparams, url, extraparams, isUse, defaultHeight, defaultWidth, tbType);
		}
	}	
};

</script>