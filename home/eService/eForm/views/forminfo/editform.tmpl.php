<?php 
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
?>
<script type="text/javascript">
	var langtxt = [];
<?php
if (count($Lang["eForm"]["jsMsg"]) > 0) {
	foreach ($Lang["eForm"]["jsMsg"]as $jsLangIndex => $jsLangVal) {
?>
		langtxt["<?php echo $jsLangIndex; ?>"] = "<?php echo $jsLangVal; ?>";
<?php
	}
}
?>
</script>
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/css/jquery.alerts.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="/templates/eForm/assets/datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/eForm/assets/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/templates/eForm/assets/css/eform_viewer.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css?t=<?php echo time(); ?>" type="text/css" />

<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery.alerts.js"></script>
<script src="/templates/eForm/assets/fancybox/jquery.fancybox.min.js"></script>
<script src="/templates/eForm/assets/datetimepicker/js/moment-with-locales.min.js"></script>
<script src="/templates/eForm/assets/datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="/templates/eForm/assets/js/eform_viewer.js?t=<?php echo time(); ?>"></script>

<div class='form-useredit form-style-2'>
	<div class='form-wrap'>
<?php
echo $htmlAry['formBody'];
?>
	</div>
</div><iframe width='1%' height='1px' name="submithdr" frameborder='0'></iframe>
<script type="text/javascript">
	function saveSuccess() {
		if (userformJS.vrs.submitWithDraft) {
			jAlert("<?php echo $Lang["eForm"]["jsMsg"]["SaveAsDraftSuccess"]; ?>", "<?php echo $Lang["eForm"]["jsMsg"]["AlertDialog"]; ?>", function() {
				window.location.href = "?task=forminfo/viewform&fid=<?php echo $_GET["fid"]; ?>&gid=<?php echo $_GET["gid"]; ?>&ctk=<?php echo $_GET["ctk"]; ?>";
			});
		} else {
			jAlert("<?php echo $Lang["eForm"]["jsMsg"]["SubmitSuccess"]; ?>", "<?php echo $Lang["eForm"]["jsMsg"]["AlertDialog"]; ?>", function() {
				window.location.href = "?task=forminfo/viewform&fid=<?php echo $_GET["fid"]; ?>&gid=<?php echo $_GET["gid"]; ?>&ctk=<?php echo $_GET["ctk"]; ?>";
			});
		}
		
	}

	function saveFail() {
		jAlert("Fail");
		userformJS.cfn.hideLoading();
	}
	
	function goBack() {
		window.location.href = "?task=forminfo/list";
	}
	(function ($) {
	    $.fn.serializeFormJSON = function () {
	        var o = {};
	        var a = this.serializeArray();
	        $.each(a, function () {
	            if (o[this.name]) {
	                if (!o[this.name].push) {
	                    o[this.name] = [o[this.name]];
	                }
	                o[this.name].push(this.value || '');
	            } else {
	                o[this.name] = this.value || '';
	            }
	        });
	        return o;
	    };
	})(jQuery);
var userformJS = {
	vrs: {
		container: "#userform",
		submitButton: "input.submitBTN",
		saveAsDraftButton: "input.saveBTN",
		cancelButton: "input.cancelBTN",
		loadingElm: ".loadingContainer",
		strURL: "?task=forminfo/editform_update",
		formdata : {
			fid: "<?php echo $formInfo["FormID"]; ?>",
			gid: "<?php echo $formInfo["GridInfo"]["GridID"]; ?>",
			ctk: "<?php echo $indexVar["FormInfo"]->getToken($formInfo["FormID"] . "_" . $formInfo["GridInfo"]["GridID"]); ?>"
		},
		xhr: "",
		submitWithDraft: false
	},
	ltr: {
		disableHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.ltr.disableHandler", "listener");
			e.preventDefault();
		},
		submitHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.ltr.submitHandler", "listener");
			if (eformJS.cfn.checkForm()) {
				userformJS.vrs.submitWithDraft = false;
				$('input[name="saveAsDraft"]').eq(0).val('FS');
				$(userformJS.vrs.container).unbind('submit', userformJS.ltr.disableHandler);
				$(userformJS.vrs.container).submit();
				userformJS.cfn.showLoading();
			} else {
				userformJS.cfn.hideLoading();
			}
			e.preventDefault();
		},
		saveAsDraftHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.ltr.saveAsDraftHandler", "listener");
			$('input[name="saveAsDraft"]').eq(0).val('SD');
			userformJS.vrs.submitWithDraft = true;
			$(userformJS.vrs.container).unbind('submit', userformJS.ltr.disableHandler);
			$(userformJS.vrs.container).submit();
			userformJS.cfn.showLoading();
			e.preventDefault();
		},
		cancelHandler: function(e) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.ltr.cancelHandler", "listener");
			goBack();
			e.preventDefault();
		}
	},
	cfn: {
		showLoading: function(scrolto) {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.cfn.showLoading", "info");
			$("input, textarea, select").prop('disabled', true);
			$("#popup_ok").prop('disabled', false);
			$(userformJS.vrs.loadingElm).eq(0).fadeIn(800, function() {
				if (typeof scrolto != "undefined" && scrolto != false && typeof $(userformJS.vrs.loadingElm) != "undefined") {
					$('html, body').animate({
					    scrollTop: $(userformJS.vrs.loadingElm).offset().top - 100
					}, 1000);
				}
			});
		},
		hideLoading: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.cfn.hideLoading", "info");
			$("input, textarea, select").prop('disabled', false); 
			$(userformJS.vrs.loadingElm).eq(0).fadeOut(400);
		},
		init: function() {
			if (debugMsgJS.vrs.allowConsole) debugMsgJS.cfn.dConosle("userformJS.cfn.init", "info");
			$('#FormSchemeJSON').val($('#initJSON').html());
			$(userformJS.vrs.container).bind('submit', userformJS.ltr.disableHandler);
			$(userformJS.vrs.submitButton).eq(0).bind('click', userformJS.ltr.submitHandler);
			$(userformJS.vrs.saveAsDraftButton).eq(0).bind('click', userformJS.ltr.saveAsDraftHandler);
			$(userformJS.vrs.cancelButton).eq(0).bind('click', userformJS.ltr.cancelHandler);
			// $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true}); 
		}
	}
}
$(document).ready(function() {
	userformJS.cfn.init();
});
</script>