<?php
// using : 

############ Change Log [Start]
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#   Date:   2019-05-27  Bill    [2019-0522-1421-59206]
#           fixed cannot access if  1) class teacher of more than 1 classes +   2) any classes that is not included in current notice 
#           fixed cannot view data in tableview.php if class name with whitespaces
#
#   Date:   2019-04-30  Bill
#           prevent SQL Injection + Cross-site Scripting
#
#	Date:	2019-04-15 Carlos
#			For Payment Notice, modified to render statistics result with PHP class eNoticePaymentReplySlip.php
#
#	Date:	2017-01-18 	Bill	[DM#3121]
#			Handle display special characters in answer
#
#	Date: 	2016-07-15	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-04-21	Bill	
#			fixed: can show correct classname and stat records if teacher teaches only one class
#
#	Date:	2015-04-15	Bill	
#			allow eNotice PIC to access [2015-0323-1602-46073]
#
#	Date:	2014-12-23	Bill	[#B73042]
#			pass $type to tableview.php for Own Class data
#
#	Date:	2014-09-08	YatWoon
#			display "--" instead of NULL 
#
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#
#	Date:	2010-08-04	YatWoon
#			update layout to IP25 standard
#
############ Change Log [End] 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#   $type (1: view own class)
################################

### Handle SQL Injection + XSS [START]
$NoticeID = integerSafe($NoticeID);
$all = integerSafe($all);
$type = integerSafe($type);
### Handle SQL Injection + XSS [END]

$MODULE_OBJ['title'] = $i_Notice_ViewStat;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$queString = $lform->getConvertedString($lnotice->Question);
$lclass = new libclass();

$lu = new libuser($UserID);

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $lu->isTeacherStaff())
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

//if (!$lu->isTeacherStaff())
if (!$lu->isTeacherStaff() || !$allowTeacherAccess)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
    exit;
}

$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

// Fixed: use $class_str to prevent cannot display classname and stat result
$class_str = $class;

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - allow eNotice PIC to access
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
//if (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))
if (!$lnotice->hasViewRight() && !$isStaffView && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))
{
    //$class = $lnotice->returnClassTeacher($UserID);
    $class = $lclass->returnHeadingClass($UserID, 1);
    if ($class=="")
    {
        include_once($PATH_WRT_ROOT."includes/libaccessright.php");
        $laccessright = new libaccessright();
        $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
        exit;
    }
    
    // Fixed: build $class_str
	if(is_array($class))
	{
		$class_str = "";
	    $dest = "";
	    for($i=0;$i<sizeof($class);$i++)
	    {
	    	$class_str .= $dest.$class[$i][0];
	    	$dest = ",";
	    }
	} else {
		$class_str = $class;
	}
}

$hasEditRight = $lnotice->hasFullRightForClass($class);
if ($lnotice->RecordType == 4 && !$class)
{
    $class = "";
    $class_str = "";
    $all = 1; 
    $data = $lnotice->returnAllResult();
}
else
{
    // Check if Class Name is valid
    //if(($class == '' || $class == 'NULL') || (!$lnotice->hasViewRight() && !$lnotice->staffview && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))) {
    if(($class == '' || $class == 'NULL') || (!$lnotice->hasViewRight() && !$isStaffView && $lnotice->IssuerUserID != $UserID && !$lnotice->isNoticePIC($NoticeID))) {
        // do nothing
    }
    else {
        $noticeClassList = $lnotice->returnClassList();
        $noticeClassList = array_values(array_filter($noticeClassList));
        
        $hasValidClass = false;
        $class_ary = explode(',', $class);
        foreach((array)$class_ary as $class_name_str) {
            if(in_array($class_name_str, (array)$noticeClassList)) {
                $hasValidClass = true;
                break;
            }
        }
        
        if(!$hasValidClass) {
            include_once($PATH_WRT_ROOT."includes/libaccessright.php");
            $laccessright = new libaccessright();
            $laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
            exit;
        }
    }
    
    if ($class != "" && !$all)
    {
    	//$data = $lnotice->returnResultOfClass($class);
        $data = $lnotice->returnResultOfClass($class_str);
    }
    else
    {
        $data = $lnotice->returnAllResult();
        $all = 1;
    }
}

if (sizeof($data)==0)
{
    $x .= "<div class='no_record_find'>$i_Notice_NoReplyAtThisMoment</div>";
    $viewNeeded = false;
}
else
{
	// [DM#3121] handle special characters in data
	for ($j=0; $j<sizeof($data); $j++)
	{
		if ($data[$j]) {
	    	$data[$j] = intranet_undo_htmlspecialchars($data[$j]);
		}
	}
	
    $x = $lform->returnAnswerInJS($data);
    $viewNeeded = true;
}

//$class_display = $class=="NULL" ? "--" : $class;
$class_display = $class=="NULL" ? "--" : $class_str;

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";
?>

<form name="print_form" action="stat_print_preview.php" method="post" target="_blank">
<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="class" value="<?=$class_str?>">
<input type="hidden" name="all" value="<?=$all?>">
</form>

<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td align="right"> 
		<a href="tableview.php?type=<?=$type?>&NoticeID=<?=$NoticeID?>&class=<?=urlencode($class_str)?>&all=<?=$all?>" class="tablelink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/icon_viewreply.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Notice_TableViewReply?></a>
	</td>
</tr>
</table>                		

<table class="form_table_v30">
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_NoticeNumber?></td>
	<td><?=$lnotice->NoticeNumber?></td>
</tr>

<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Title?></td>
	<td><?=$lnotice->Title?></td>
</tr>

 <? if ($attachment != "") { ?>
    <tr valign='top'>
    	<td class='field_title'><?=$i_Notice_Attachment?></td>
    	<td><?=$attachment?></td>
    </tr>
<? } ?>

<? if ($all != 1) {?>
    <tr valign='top'>
    	<td class='field_title'><?=$i_ClassName?></td>
    	<td><?=$class_display?></td>
    </tr>
<? } else {?>
    <tr valign='top'>
    	<td class='field_title'><?=$i_Notice_RecipientType?></td>
    	<td>
        	<? switch($lnotice->RecordType)
        	{
        		case 2:	echo $i_Notice_RecipientTypeLevel; 	break;
        		case 3:	echo $i_Notice_RecipientTypeClass; 	break;
        		case 4:	echo $i_Notice_RecipientTypeIndividual; 	break;
        	}
        	?>
    	</td>
    </tr>
<? } ?>
</table>

<?php
if($is_payment_notice){
	if($viewNeeded){
		echo $eNoticePaymentReplySlip->printStatisticsView($queString, $data, $lnotice->DisplayQuestionNumber==1, $isPreview=false);
	}else{
		echo $x;
	}
}else{
?>

                        <? if ($viewNeeded) { ?>
                        	<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
                        		<script language="javascript" src="/templates/forms/eNoticePayment_form_view.js"></script>
                        	<? } else { ?>
                        		<script language="javascript" src="/templates/forms/form_view.js"></script>
                        	<? } ?>
                        	<form name="ansForm" method="post" action="update.php">
                                        <input type=hidden name="qStr" value="">
                                        <input type=hidden name="aStr" value="">
                                </form>
                        
                                <script language="Javascript">
                                
                                <?=returnFormStatWords()?>
                                var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                w_total_no = "<?=$i_Notice_NumberOfSigned?>";
                                w_show_details = "<?=$i_Form_ShowDetails?>";
                                w_hide_details = "<?=$i_Form_HideDetails?>";
                                w_view_details_img = "<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif' border='0'>";
                                var myQue = "<?=$queString?>";
								myQue="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                var myAns = new Array();
                        <? } ?>
                        
                                <?=$x?>
                        
                        <? if ($viewNeeded) { ?>
                                var stats= new Statistics();
                                stats.qString = myQue;
                                stats.answer = myAns;
                                stats.analysisData();
		document.write(stats.getStats());
                                </SCRIPT>
                        <? } ?>
<?php } ?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<? if($viewNeeded) { ?>
	<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.print_form.submit();") ?>
	<? } ?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
<p class="spacer"></p>
</div>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>