<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();


$NoticeID = IntegerSafe($NoticeID);
$lnotice = new libnotice($NoticeID);
$luser = new libuser();

$libeClassApp = new libeClassApp();

$is_notice_signed = $lnotice->isTargetNoticeSigned($NoticeID,$StudentID);
$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";

$result = false;
if($is_notice_signed && $is_payment_notice){
	$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
	$arrItemIDs = $lnotice->returnVector($sql);
	if(sizeof($arrItemIDs)>0) {
		$targetItemID = implode(",", $arrItemIDs);

		$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$StudentID' AND ItemID IN ($targetItemID)";
		$NoOfItemNeedToPaid = $lnotice->returnVector($sql);

		$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$StudentID' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
		$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);

		if ($NoOfItemNeedToPaid[0] == 0 || $NoOfItemPaidSuccessfully[0] == 0) {

			$sql = "UPDATE INTRANET_NOTICE_REPLY SET RecordStatus='0' WHERE NoticeID='$NoticeID' AND StudentID='$StudentID'";
			$result = $lnotice->db_db_query($sql);

			$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo(array($StudentID), '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;

			list($pushmessage_subject, $pushmessage_body) = $lnotice->returnPushMessageData($lnotice->DateStart,$lnotice->DateEnd,$lnotice->Title,$lnotice->NoticeNumber);

			$isPublic = "N";
			$appType = $eclassAppConfig['appType']['Parent'];
			$sendTimeMode = 'now';
			$sendTimeString = '';
			$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
		}
	}
}


intranet_closedb();

if($result)
	echo $Lang['eNotice']['ResignNoticeSuccess'];
else
	echo $Lang['eNotice']['ResignNoticeFail'];
