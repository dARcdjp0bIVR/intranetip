<?php
// Modifying by : 

######################
#   Date:   2020-07-14  Ray
#           added $sys_custom['ePayment']['FPSUseQRCode']
#
#	Date:	2020-01-02	Philips [2019-1125-1703-32207]
#			Allow Admin to sign payment notice either passed deadline or not
#
#	Date:	2019-11-12  Carlos
#			Check student subsidy identity with both notice start date and end date.
#	Date:	2019-11-01  (Philips)	[2019-1016-1125-40260]
#			added back button
#	Date:	2019-10-29  Carlos
#			(1) Handle new question type [multiple choices with input quantity].
#			(2) When eWallet is enabled would always add students into payment items even amount is $0.
#	Date	2019-09-30  Carlos
#			Fixed fail to clean previous answers generated payment items issue due to changed final anwsers do not involve any payment item. 
#	Date:	2019-09-16  Carlos
#			Update payment item amount in case of change anwser if reuse the same payment item student record.
#	Date:	2019-08-21  Carlos
#			Record payment item original amount and subsidy identity to payment item student record for reporting usage.
#	Date:	2019-07-29  Carlos
#			Handle admin modify reply slip answer, no payment would be performed, just change answer.
#			And display the modify answer records at status section.
#	Date:	2019-07-26  Carlos
#			If using eWallet to pay, and enabled [Pay At School], admin/teacher can sign on behalf of parent with [Pay At School] method to sign the reply slip. 
#	Date:	2019-06-06 Carlos
#			Handle pay at school - only sign the replyslip and generate payment item student records for paying later at ePayment.
#	Date:	2019-05-21 	Carlos
#			Can only sign when the notice is active/distributed.
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#
#   Date:   2019-01-25  (Bill)  [2019-0118-1125-41206]
#           Improved: Description - display text content from FCKEditor
#
#	Date:	2019-01-09  (Carlos)
#			Double check the calculated total amount is enough to pay with remaining balance. 
#	Date:	2018-09-28	(Bill)	[2018-0809-0935-01276]
#			Support Sign New Payment Question Type - Must Pay
#
#	Date:	2018-08-31	Carlos - Added flag $sys_custom['PaymentNoticeDoNotCheckBalance'] to ignore checking balance.
#	Date:	2018-03-13 	Carlos - Initialize payment account record if ePayment module is lately installed and record not exist.
#	Date:	2018-03-08	Carlos - $sys_custom['PaymentNoticeAlwaysGenerateItemStudents'] always generate payment item student records even amount is $0.
#	Date:	2018-01-17  Carlos - Skip checking balance if enabled $sys_custom['ePayment']['TNG'].
#
#	Date:	2015-09-15	Bill	[2015-0914-1442-30073]
# 			Allow parent to sign payment notice even student has not enough balance - Debit later only
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	2014-09-22 (Carlos): Only pay those items with choice that pay amount > 0
#
#	Date:	2014-02-28	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#	$StudentID
################################

$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();
$Json = new JSON_obj();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$lu = new libuser($UserID);
$valid = true;

// Check Notice Deadline
$today = time();
$deadline = strtotime($lnotice->DateEnd);
$PassDeadline = ($today > $deadline);

$use_payment_system = $lpayment->isEWalletDirectPayEnabled(); 
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;

$pay_at_school = $_POST['NoticePaymentMethod'] == 'PayAtSchool';
$can_sign_by_admin = false;

# check whether user has enough balance to pay for the item(s)
$result = ($lpayment->checkBalance($StudentID,1));
$currentBalance = str_replace(",","",$result[0]);

$student_subsidy_identity_id = '';
$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
if(count($student_subsidy_identity_records)>0){
	$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
}

$is_distributed_notice = $lnotice->RecordStatus == DISTRIBUTED_NOTICE_RECORD;

// [2015-0914-1442-30073] Debit directly - redirect to eNoticePayment_sign.php
if(!$is_distributed_notice || ($lnotice->DebitMethod != 2 && (!$do_not_check_balance && $currentBalance < $thisAmountTotal))) {
	intranet_closedb();
	
	header("Location: eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID&error=1");
	exit;
}
// [2015-0914-1442-30073] Debit later - allow submit & display message
else if($lnotice->DebitMethod == 2 && (!$do_not_check_balance && $currentBalance < $thisAmountTotal)){
	$msg = $Lang['eNotice']['NotEnoughBalance'];
}

//$lpayment->Start_Trans();

if ($lu->isStudent())
{
    $valid = false;
}
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();

$is_admin_modify_answer = $isTeacher && isset($_POST['modify_answer_btn']);

if ($StudentID!="")
{
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            $editAllowed = true;
        }
    }
    else # Teacher
    {
    	if(strtoupper($lnotice->Module) == "PAYMENT")
    	{
    		$editAllowed = false;
    		// [2015-0917-1053-13066] Setting: Allow eNotice admin to sign payment notice for parents - true
    		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && $lnotice->isAllowAdminToSignPaymentNotice && $StudentID!="undefined"){
    			### 2020-01-02 Philips [2019-1125-1703-32207] - Allow Admin sign either passed deadline or not
//     			if(!$PassDeadline){
    				$editAllowed = true;
    				if($use_payment_system && $pay_at_school){
    					$can_sign_by_admin = true;
    				}
//     			}
    		}
    	}else
    	{
		    if($lnotice->IsModule)
		    {
				$editAllowed = true;    
		    }
		    else
		    {
	        	$editAllowed = $lnotice->isEditAllowed($StudentID);
	    	}
    	}
    }
}
else
{
	$valid = false;
	/*
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
    */
    
}

if($is_admin_modify_answer){
	$editAllowed = true;
}

############################################################
#	Check student belongs to this notice?
############################################################
if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
{
	$valid = false;
}

if(!$valid)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$use_direct_pay_fps_qrcode = false;
$use_hsbc_fps = false;
//$use_payment_system = false;
if ($use_payment_system) {
    if ($lpayment->isFPSDirectPayEnabled() && $sys_custom['ePayment']['FPSUseQRCode']) {
        $use_hsbc_fps = $lpayment->isFPSDirectPayEnabled();
        $multi_merchant_ids = $lpayment->returnPaymentNoticeMultiMerchantAccountID($NoticeID);
        $merchants = $lpayment->getMerchantAccounts(array('ServiceProvider' => 'FPS', 'AccountID' => $multi_merchant_ids, 'RecordStatus' => 1));
        if (count($merchants) > 0) {
            $use_direct_pay_fps_qrcode = true;
        }
    }
}


$generate_item_students_for_zero_amount = $sys_custom['PaymentNoticeAlwaysGenerateItemStudents'] || $use_payment_system;

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

if ($editAllowed)
{
    	// initialize payment account record if ePayment module is lately installed
    	$sql = "INSERT IGNORE INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$StudentID',0)";
		$lnotice->db_db_query($sql);
    	
    	//$sql = "SELECT RecordStatus FROM INTRANET_NOTICE_REPLY WHERE NoticeID = $NoticeID AND StudentID = $StudentID";
    	//$isNoticeReplied = $lnotice->returnVector($sql);
    	if(!$lnotice->isTargetNoticeSigned($NoticeID,$StudentID))
    	{
    		## for Payment Notice, if no reply before, start processing.
    		## Otherwise, no need to process again.

	        $ansString = intranet_htmlspecialchars(trim($aStr));
	        $sql = "SELECT Question, DebitMethod FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
	        $arrQuestion = $lnotice->returnArray($sql);
	        $qStr = $arrQuestion[0][0];
	        $debitMethod = $arrQuestion[0][1];

	        /*
	        $arrPaymentItemDetails = $lnotice->returnPaymentItemDetails($qStr);

	        $arrAnswer = explode("#ANS#",$ansString);
			array_shift($arrAnswer);

			$notice_array = $lnotice->splitQuestion($qStr);
			$thisTotalAmount = 0;

			if(sizeof($arrPaymentItemDetails)>0)
			{
				foreach($arrPaymentItemDetails as $key=>$details)
				{
					$seletected_answer = $arrAnswer[$key-1];
					$payment_type = $notice_array[$key-1][0];
					switch($payment_type)
					{
					    case 1:
					    case 12:
							$PaymentItemName = $details['PaymentItem'];
							$PaymentItemID = $details['PaymentItemID'];
							$PaymentCategroyID = $details['PaymentCategory'];
							$PaymentAmount = $details['PaymentAmount'];
							$NonPayItem = $details['NonPayItem'];
							//debug_pr($details);
							
							if($NonPayItem) break;
							if($PaymentAmount > 0 || $generate_item_students_for_zero_amount){
								if($seletected_answer != "")
								{
									if($seletected_answer == "0"){
										//$arrPaymentProcess[$key-1]['PaymentItemID'] = $PaymentItemID;
										//$arrPaymentProcess[$key-1]['PaymentAmount'] = $PaymentAmount;
										$arrPaymentProcess[] = $PaymentItemID;
										$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
										$thisTotalAmount += $PaymentAmount;
									}
								}
							}
							//echo $thisTotalAmount.'(1)'; exit;
							break;
						case 2:
							if($details['PaymentAmount'][$seletected_answer]!=0)
							{
								$PaymentItemName = $details['PaymentItem'];
								$PaymentItemID = $details['PaymentItemID'];
								$PaymentCategroyID = $details['PaymentCategory'];
								$PaymentAmount = $details['PaymentAmount'][$seletected_answer];
								$NonPayItem = $details['NonPayItem'];
								if($NonPayItem) break;
								if($PaymentAmount > 0 || $generate_item_students_for_zero_amount){
									//$arrPaymentProcess[$key-1]['PaymentItemID'][] = $PaymentItemID;
									//$arrPaymentProcess[$key-1]['PaymentAmount'][] = $PaymentAmount;
									$arrPaymentProcess[] = $PaymentItemID;
									$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
									$thisTotalAmount += $PaymentAmount;
								}
							}
							//echo $thisTotalAmount.'(2)'; exit;
							break;
						case 3:
						
							$final_ans = explode(",",$seletected_answer);
							
							if(sizeof($final_ans)>0)
							{
								for($i=0; $i<sizeof($final_ans); $i++)
								{
									$PaymentItemName = $details['PaymentItem'][$final_ans[$i]];
									$PaymentAmount = $details['PaymentAmount'][$final_ans[$i]];
									$PaymentItemID = $details['PaymentItemID'][$final_ans[$i]];
									if($PaymentAmount != 0 || $generate_item_students_for_zero_amount)
									{
										$arrPaymentProcess[] = $PaymentItemID;
										$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
										$thisTotalAmount += $PaymentAmount;
									}
								}
							}
							//echo $thisTotalAmount.'(3)'; exit;
							break;
						case 4:
							break;
						case 5:
							break;
						case 6:
							break;
						case 7:
							break;
						case 8:
							break;
						case 9:					
							$PaymentItemName = $details['PaymentItem'][$seletected_answer];
							$PaymentAmount = $details['PaymentAmount'][$seletected_answer];
							$PaymentItemID = $details['PaymentItemID'][$seletected_answer];
							
							//$arrPaymentProcess[$key-1]['PaymentItemID'] = $PaymentItemID;
							//$arrPaymentProcess[$key-1]['PaymentAmount'] = $PaymentAmount;
							if($PaymentAmount != 0 || $generate_item_students_for_zero_amount)
							{
								$arrPaymentProcess[] = $PaymentItemID;
								$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
								$thisTotalAmount += $PaymentAmount;
							}
							//echo $thisTotalAmount.'(9)'; exit;
							break;
					}
				}
			}
			
			*/

			$thisTotalAmount = 0;
			$arrPaymentProcess = array();
			$arrPaymentAmount = array();
			$questions = $eNoticePaymentReplySlip->parseQuestionString($qStr);
			$questions = $eNoticePaymentReplySlip->applySubsidySettingsToQuestions($questions,$StudentID,$student_subsidy_identity_id);
			$questions_with_answers = $eNoticePaymentReplySlip->parseAnswerString($aStr, $questions);
			$all_payment_item_ids = $eNoticePaymentReplySlip->getAllPaymentItemID($qStr);
			for($i=0;$i<count($questions_with_answers);$i++){
				$q = $questions_with_answers[$i];
				$ans = $questions_with_answers[$i]['Answer'];
				if(!$eNoticePaymentReplySlip->isPaymentTypeQuestion($q['Type'])){
					// skip processing non-payment questions
					continue;
				}

				$tmp_payment_item_id = '';
				$tmp_amount = 0;
				$tmp_original_amount = 0;

				if(is_array($ans)){
					if(count($ans)==0){
						// empty answer, must be skipped question
						continue;
					}
					foreach($ans as $tmp_ans){
						if($q['Type'] == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY){
							$tmp_ans_index = $tmp_ans['Option'];
							$tmp_ans_quantity = $tmp_ans['Quantity'];
						}else{
							$tmp_ans_index = $tmp_ans;
							$tmp_ans_quantity = 1;
						}
						$tmp_payment_item_id = $q['Options'][$tmp_ans_index]['PaymentItemID'];
						$tmp_amount = $q['Options'][$tmp_ans_index]['Amount'];
						$tmp_original_amount = $q['Options'][$tmp_ans_index]['OriginalAmount'];
						if($tmp_amount != 0 || $generate_item_students_for_zero_amount){
							$arrPaymentProcess[] = $tmp_payment_item_id;
							$arrPaymentAmount[$tmp_payment_item_id]['Amount'] = $tmp_amount * $tmp_ans_quantity;
							$arrPaymentAmount[$tmp_payment_item_id]['OriginalAmount'] = $tmp_original_amount * $tmp_ans_quantity;
							$thisTotalAmount += $tmp_amount * $tmp_ans_quantity;
						}
					}
				}else if($ans != ''){
					if(isset($q['Options'])){
						$tmp_amount = $q['Options'][$ans]['Amount'];
						$tmp_original_amount = $q['Options'][$ans]['OriginalAmount'];
					}else{
						$tmp_amount = $q['Amount'];
						$tmp_original_amount = $q['OriginalAmount'];
					}
					if(isset($q['PaymentItemID'])){
						if($q['Type'] != QTYPE_WHETHER_TO_PAY || ($q['Type'] == QTYPE_WHETHER_TO_PAY && $ans == '0')){
							$tmp_payment_item_id = $q['PaymentItemID'];
						}
					}else{
						$tmp_payment_item_id = $q['Options'][$ans]['PaymentItemID'];
					}
					if($tmp_payment_item_id != '' && ($tmp_amount != 0 || $generate_item_students_for_zero_amount)){
						$arrPaymentProcess[] = $tmp_payment_item_id;
						$arrPaymentAmount[$tmp_payment_item_id]['Amount'] = $tmp_amount;
						$arrPaymentAmount[$tmp_payment_item_id]['OriginalAmount'] = $tmp_original_amount;
						$thisTotalAmount += $tmp_amount;
					}
				}
			}

			// do not just believe the submitted value $thisAmountTotal(which is calculated by js) from frontend, double check the calculated total amount to pay again
			if($lnotice->DebitMethod != 2 && (!$do_not_check_balance && $currentBalance < $thisTotalAmount)) {
				intranet_closedb();
				header("Location: eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID&error=1");
				exit;
			}

			/*
			if(count($all_payment_item_ids)>0){
				// try to remove the previous generated payment item student records that are failed to pay 
				$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$all_payment_item_ids)."') AND StudentID = '$StudentID' AND RecordStatus <> '1'";
				$lpayment->db_db_query($sql);
			}
			*/
			$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$all_payment_item_ids)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
			$existing_payment_ids = $lpayment->returnVector($sql);

			//debug_pr($arrPaymentProcess); exit;
			//echo $thisTotalAmount; exit;
			//debug_pr($_POST);

			if(!$pay_at_school && $use_payment_system && $use_direct_pay_fps_qrcode && sizeof($arrPaymentProcess)>0) {
				include_once($intranet_root."/includes/json.php");
				$jsonObj = new JSON_obj();

				// prepare the item student records
				$payment_id_ary = array();
				foreach($arrPaymentProcess as $cur_paymentItemID)
				{
					$tmp_payment_id = '';
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID'";
					$tmp_records = $lpayment->returnArray($sql);
					if(count($tmp_records)>0){
						$tmp_payment_id = $tmp_records[0]['PaymentID'];
						$payment_id_ary[] = $tmp_records[0]['PaymentID'];
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount='".$arrPaymentAmount[$cur_paymentItemID]['Amount']."' WHERE PaymentID='$tmp_payment_id' AND ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID' ";
						$lpayment->db_db_query($sql);

					}else{
						$sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,RecordType,RecordStatus,DateInput,DateModified)
								VALUES ('$cur_paymentItemID','$StudentID','".$arrPaymentAmount[$cur_paymentItemID]['Amount']."','0','0',NOW(),NOW())";
						$insert_success = $lpayment->db_db_query($sql);
						if($insert_success){
							$inserted_payment_id = $lpayment->db_insert_id();
							$tmp_payment_id = $inserted_payment_id;
							$payment_id_ary[] = $inserted_payment_id;
						}

					}
					if($tmp_payment_id != '' && $tmp_payment_id > 0){
						// record original amount and subsidy identity to payment record
						$is_subsidized = ($arrPaymentAmount[$cur_paymentItemID]['OriginalAmount'] - $arrPaymentAmount[$cur_paymentItemID]['Amount']) > 0 && $student_subsidy_identity_id != '';
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET OriginalAmount='".$arrPaymentAmount[$cur_paymentItemID]['OriginalAmount']."' ".($is_subsidized?",SubsidyIdentityID='$student_subsidy_identity_id'":",SubsidyIdentityID=NULL")." WHERE PaymentID='$tmp_payment_id'";
						$lpayment->db_db_query($sql);
					}
				}

				// clean up those payment item student records that not in use anymore
				if(count($payment_id_ary)>=0 && count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($payment_id_ary)>0 ? array_values(array_diff($existing_payment_ids,$payment_id_ary)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}

				if($thisTotalAmount > 0) // only need to pay by eWallet when greater than $0
				{
					$_SESSION['ENOTICE_FPS_QRCODE_PAYEMNT_REQUEST_OBJECT_URL'] = '';
					$_SESSION['ENOTICE_FPS_QRCODE_PAYMENT_DATE_INPUT'] = '';
					$NoticePaymentMethod = '';
				    if($use_hsbc_fps) {
						$NoticePaymentMethod = 'FPS';
						$requestData = array('StudentID'=>$StudentID,'ServiceProvider' => 'FPS', 'PaymentID' => $payment_id_ary[0], 'PayerUserID' => $UserID, 'Amount' => $thisTotalAmount, 'PaymentTitle' => $lnotice->Title, 'NoticeID' => $NoticeID, 'NoticePaymentID' => implode(',', $payment_id_ary), 'NoticeReplyAnswer' => $ansString);
						$returnJsonString = $lpayment->getTngQrcode($requestData);
						$decodedJsonData = $jsonObj->decode($returnJsonString);
					}
//var_dump($requestData);
//var_dump($returnJsonString);
//var_dump($decodedJsonData);
//die;
					if(($use_hsbc_fps && isset($decodedJsonData['MethodResult']) && isset($decodedJsonData['MethodResult']['qrCode']) && $decodedJsonData['MethodResult']['qrCode']!='' && isset($decodedJsonData['MethodResult']['paymentRequestObjectUrl']) && $decodedJsonData['MethodResult']['paymentRequestObjectUrl']!='')
					) {
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET NoticePaymentMethod='".$lpayment->Get_Safe_Sql_Query($NoticePaymentMethod)."' WHERE PaymentID IN (".implode(",",$payment_id_ary).")";
						$lpayment->db_db_query($sql);

						$type = ($isParent? 1: 2);

						$sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
				                RecordType = '$type', SignerID = '$UserID', DateModified = now()
				                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
						$temp = $lnotice->db_db_query($sql);

						@session_start();
						$_SESSION['ENOTICE_FPS_QRCODE_PAYEMNT_REQUEST_OBJECT_URL'] = $decodedJsonData['MethodResult']['paymentRequestObjectUrl'];
						$_SESSION['ENOTICE_FPS_QRCODE_PAYMENT_DATE_INPUT'] = $decodedJsonData['MethodResult']['dateInput'];
						header("Location: eNoticePayment_fps_qrcode.php?NoticeID=$NoticeID&StudentID=$StudentID");
					}else{
						//show alert
						intranet_closedb();
						if(is_array($decodedJsonData['ErrorCode'])) {
							$enotice_payment_error = $decodedJsonData['MethodResult']['errorCode'];
						} else {
							$enotice_payment_error = $decodedJsonData['ErrorCode'];
						}
						header("Location: eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID&enotice_payment_error=$enotice_payment_error");
					}
					exit;
				}
			}

			if($can_sign_by_admin && $pay_at_school){
				// prepare the item student records
				$payment_id_ary = array();
				foreach($arrPaymentProcess as $cur_paymentItemID)
				{
					$tmp_payment_id = '';
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$cur_paymentItemID' AND StudentID='$StudentID'";
					$tmp_records = $lpayment->returnArray($sql);
					if(count($tmp_records)>0){
						$tmp_payment_id = $tmp_records[0]['PaymentID'];
						$payment_id_ary[] = $tmp_records[0]['PaymentID'];
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount='".$arrPaymentAmount[$cur_paymentItemID]['Amount']."' WHERE PaymentID='$tmp_payment_id' AND ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID' ";
						$lpayment->db_db_query($sql);
					}else{
						$sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,RecordType,RecordStatus,DateInput,DateModified)
								VALUES ('$cur_paymentItemID','$StudentID','".$arrPaymentAmount[$cur_paymentItemID]['Amount']."','0','0',NOW(),NOW())";
						$insert_success = $lpayment->db_db_query($sql);
						if($insert_success){
							$inserted_payment_id = $lpayment->db_insert_id();
							$tmp_payment_id = $inserted_payment_id;
							$payment_id_ary[] = $inserted_payment_id;
						}
						//$lpayment->Commit_Trans();
					}
					if($tmp_payment_id != '' && $tmp_payment_id > 0){
						// record original amount and subsidy identity to payment record
						$is_subsidized = ($arrPaymentAmount[$cur_paymentItemID]['OriginalAmount'] - $arrPaymentAmount[$cur_paymentItemID]['Amount']) > 0 && $student_subsidy_identity_id != '';
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET OriginalAmount='".$arrPaymentAmount[$cur_paymentItemID]['OriginalAmount']."' ".($is_subsidized?",SubsidyIdentityID='$student_subsidy_identity_id'":",SubsidyIdentityID=NULL")." WHERE PaymentID='$tmp_payment_id'";
						$lpayment->db_db_query($sql);
					}
				}
				if(count($payment_id_ary)){
					// Mark down the payment method in payment item student records
					$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET NoticePaymentMethod='".$lpayment->Get_Safe_Sql_Query($NoticePaymentMethod)."' WHERE PaymentID IN (".implode(",",$payment_id_ary).")";
					$lpayment->db_db_query($sql);
				}
				
				// clean up those payment item student records that not in use anymore
				if(count($payment_id_ary)>=0 && count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($payment_id_ary)>0 ? array_values(array_diff($existing_payment_ids,$payment_id_ary)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}
			}else if(sizeof($arrPaymentProcess)>0)
			{	
				$result = array();
				$paid_item_ids = array();
				foreach($arrPaymentProcess as $key=>$paymentItemID)
				{
					$paid_item_ids[] = $paymentItemID;
					$targetPaymentItemID = $paymentItemID;
					$targetPaymentAmount = $arrPaymentAmount[$paymentItemID]['Amount'];
					//echo $targetPaymentAmount.'}<br>';
					$result[] = $lpayment->Process_Notice_Payment_Item($targetPaymentItemID,$StudentID,$targetPaymentAmount,$lnotice->DebitMethod,true);				
				}
				
				// find the paid payment items to exclude
				$paid_payment_ids = array();
				if(count($paid_item_ids)>0)
				{
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$paid_item_ids)."') AND StudentID='$StudentID' AND RecordStatus='1'";
					$paid_payment_ids = $lpayment->returnVector($sql);
				}
				// clean previous answer(s) generated payment items, exclude the paid payment items
				if(count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($paid_payment_ids)>0 ? array_values(array_diff($existing_payment_ids,$paid_payment_ids)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}
				/*
				if(!in_array(false,$result))
				{
					$lpayment->Commit_Trans();
				}
				else
				{
					$lpayment->RollBack_Trans();
				}
				*/
			}else if(count($existing_payment_ids)>0){ // final answers no payment items involved, clean all previous generated non-paid payment items
				$payment_ids_to_remove = $existing_payment_ids;
				if(count($payment_ids_to_remove)>0){
					$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
					$lpayment->db_db_query($sql);
				}
			}
			
	        $type = ($isParent? 1: 2);
	
	        # Update
	        $sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
	                RecordType = '$type',RecordStatus = 2,SignerID='$UserID',DateModified=now() ".($NoticePaymentMethod!=''?",NoticePaymentMethod='".$lnotice->Get_Safe_Sql_Query($NoticePaymentMethod)."' ":"")." 
	                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
	        $temp = $lnotice->db_db_query($sql);
	        /*
	        if($temp) {
	        	$lpayment->Commit_Trans();
	        } else {
	        	$lpayment->RollBack_Trans();
	        }
	        */
	    }else if($is_admin_modify_answer){
	    	$ansString = intranet_htmlspecialchars(trim($aStr));
	    	$lnotice->retrieveReply($StudentID);
    		$oldAnsString = $lnotice->answer;
    		$lnotice->upsertReplySlipModifyAnswerRecord(array('NoticeID'=>$NoticeID,'StudentID'=>$StudentID,'OldAnswer'=>$oldAnsString,'NewAnswer'=>$ansString));
	    	$sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString' 
	                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
	        $temp = $lnotice->db_db_query($sql);
	        /*
	        if($temp) {
	        	$lpayment->Commit_Trans();
	        } else {
	        	$lpayment->RollBack_Trans();
	        }
	        */
	    }
    
    $lnotice->retrieveReply($StudentID);
    $ansString = $lnotice->answer;
    $ansString =  str_replace("&amp;", "&", $ansString);
	$ansString = $lform->getConvertedString($ansString);
	$ansString.=trim($ansString)." ";
	$lnotice->retrieveAmount($StudentID);
	//$lnotice->retrieveAmountWithNonPayItem($StudentID);
	if($lnotice->countAmount != "")
		$countAmt = implode(',',$lnotice->countAmount);
	if($lnotice->AmountValue != "")
		$amountValue = implode(',',$lnotice->AmountValue);
		
		
}


$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();

$result = $lpayment->checkBalance($StudentID,1);

$linterface->LAYOUT_START();
?>

<br />   
<table align='right' width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td align='right'><?=$linterface->GET_SYS_MSG("",$msg)?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_DateStart?></span></td>
					<td class='tabletext'><?=$lnotice->DateStart?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_DateEnd?></span></td>
					<td class='tabletext'><?=$lnotice->DateEnd?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Title?></span></td>
					<td class='tabletext'><?=$lnotice->Title?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Description?></span></td>
					<? if(strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApplyEditor) { ?>
                		<td class='tabletext'><?=htmlspecialchars_decode($lnotice->getNoticeContent($StudentID))?></td>
            		<? } else { ?>
                		<td class='tabletext'><?=nl2br(intranet_convertAllLinks($lnotice->Description))?></td>
            		<? } ?>
				</tr>
                                
                                <? if ($attachment != "") { ?>
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Attachment?></span></td>
					<td class='tabletext'><?=$attachment?></td>
				</tr>
                                <? } ?>
                                
                                <? if ($isTeacher) {
        				$issuer = $lnotice->returnIssuerName();
        			?>
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Issuer?></span></td>
					<td class='tabletext'><?=$issuer?></td>
				</tr>
                        	<? } ?>

			 <?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>      	
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Profile_PersonInCharge?></span></td>
					<td class='tabletext'><?=$pics?></td>
				</tr>
			<?php } ?>
                        	
                        	<? if ($editAllowed) {
                                        if ($lnotice->replyStatus != 2)
                                        {
                                            $statusString = "$i_Notice_Unsigned";
                                        }
                                        else
                                        {
                                            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                                            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                                            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                                        }
                                        
                                        $modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'StudentID'=>$StudentID,'GetModifierUserInfo'=>true));
                                        if(count($modify_answer_records) > 0){
	                                        foreach($modify_answer_records as $modify_answer_record){
												$statusString .= "<br />".str_replace(array('<!--USER_NAME-->','<!--DATETIME-->'),array($modify_answer_record['ModifierUserName'],$modify_answer_record['InputDate']),$Lang['eNotice']['AnswerModifiedByAt']);
											}
                                        }
				?>
                        	<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_SignStatus?></span></td>
					<td class='tabletext'><?=$statusString?></td>
				</tr>
                        
                        	<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_UserStudentName?></span></td>
					<td class='tabletext'><?=$lu->getNameWithClassNumber($StudentID)?></td>
				</tr>
                        
				<? } ?>
                        
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_RecipientType?></span></td>
					<td class='tabletext'><?=$targetType[$lnotice->RecordType]?></td>
				</tr>
                                
                                <!-- Break -->
                                <tr valign='top'>
					<td class='tabletext' colspan="2">&nbsp;</td>
				</tr>
                                
                                
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eNoticereplytitle' align='center'><?=$i_Notice_ReplySlip?></td>
				</tr>
                                
				<tr valign='top'>
					<td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <tr>
                                        	<td class='tabletext' align='center'><?=$i_Notice_ReplySlip_Signed?></td>
					</tr>
					
					<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString = str_replace("&lt;br&gt;","<br>",$queString);
						$result = $lpayment->checkBalance($StudentID,1);
						$finalBalance = str_replace(",","",$result[0]);
						?>
                                        <tr>
                                        	<td>
                                                
												<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
												<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
												
                                                <form name="ansForm" method="post" action="update.php">
                                                        <input type=hidden name="qStr" value="">
                                                        <input type=hidden name="aStr" value="">
                                                </form>
                								<div id="ReplySlipContainer"></div>
                                                <script type="text/javascript" language="Javascript">     
                                                $(document).ready(function(){
                                                var options = {
													"QuestionString":"<?=$queString?>",
													"AnswerString":"<?=$ansString?>",
													"AccountBalance":<?=$finalBalance?>,
													"DoNotShowBalance":1,
													"PaymentCategoryAry":[],
													"TemplatesAry":[],
													"DisplayQuestionNumber":1,
													"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
													"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
													<?php
													$words = $eNoticePaymentReplySlip->getRequiredWords();
													foreach($words as $key => $value){
														echo '"'.$key.'":"'.$value.'",'."\n";
													}
													?>
													"Debug":false
												};
												var sheet = new Answersheet(options);
												sheet.renderPreviewPanel('ReplySlipContainer');
                                                });  
                                                </SCRIPT>
                                                
                                        	</td>
					</tr>
					<? } ?>
					<? if($debitMethod==2 && !$use_payment_system) { ?>
					<tr>
						<td class="tabletext"><?=$Lang['eNotice']['AccountBalance']?> : $<?=$result[0]?></td>
					</tr>
					<? } ?>
                                        </table>
                                        </td>
				</tr>

				<form>
                    <?php if(!$use_payment_system){ ?>    
                        <tr>
                			<td colspan="2"><?=$i_Payment_PrintPage_Outstanding_AccountBalance." : $".$result[0]?>
                			</td>
                		</tr>
                	<?php } ?>
                      		<tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr>
                                <?$result = $lpayment->checkBalance($StudentID);?>
                		
                        <tr>
                			<td align="center" colspan="2">
                                <?= $linterface->GET_ACTION_BTN($button_print, "button", "window.print();","submit2"); ?>
                				<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close();","cancelbtn"); ?>
                				<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back()");?>
                			</td>
                		</tr>
                                
                                <input type="hidden" name="qStr" value="<?=$queString?>">
                                <input type="hidden" name="aStr" value="<?=$ansString?>">
                                <input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
                                <input type="hidden" name="StudentID" value="<?=$StudentID?>">
                                </form>
                                
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>

</table>                        
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>