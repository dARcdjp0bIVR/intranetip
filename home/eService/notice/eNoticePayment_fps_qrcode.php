<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();


$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);
$url = $_SESSION['ENOTICE_FPS_QRCODE_PAYEMNT_REQUEST_OBJECT_URL'];
if($url == '') {
	intranet_closedb();
	header("Location: eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID&enotice_payment_error=0");
	exit;
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
$responseJson = curl_exec($ch);
curl_close($ch);

if($responseJson == false) {
	intranet_closedb();
	header("Location: eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID&enotice_payment_error=0");
	exit;
}

$jsonObj = new JSON_obj();
$decodedJsonData = $jsonObj->decode($responseJson);
$qrcode = $decodedJsonData['payload'];
$date_input = $_SESSION['ENOTICE_FPS_QRCODE_PAYMENT_DATE_INPUT'];
$lnotice = new libnotice($NoticeID);


$item_title = $lnotice->Title;

$lu = new libuser($UserID);
$userInfo = $lu->returnUser($StudentID);
$qrcode_title = $userInfo[0]['ClassName'].'-'.$userInfo[0]['ClassNumber'];

$back_url = "eNoticePayment_sign.php?NoticeID=$NoticeID&StudentID=$StudentID";

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

echo $linterface->INCLUDE_JS_CSS();
?>

<script type="text/javascript" language="javascript" src="/templates/qrcodegen.js"></script>

<style>
    #qrcode_div div{ margin-bottom: 10px; }
    #qrcode_div .btn_back { width: 80%; padding: 5px; }
</style>

<div id="qrcode_div" style="text-align: center; padding-top: 10px;">
	<div style=""><?=$date_input?></div>
	<div style="font-weight: bold; font-size: 20px;"><?=htmlspecialchars($qrcode_title);?></div>
	<div style="font-weight: bold; font-size: 20px;"><?=htmlspecialchars($item_title);?></div>
	<canvas xmlns="http://www.w3.org/1999/xhtml" id="qrcode-canvas" style="" width="232" height="232"></canvas>
    <div>
        <input type="button" class="btn_back" value="<?=$Lang['Btn']['Back']?>" onclick="ButtonBack();" />
    </div>
</div>
<script type="text/javascript">
    function ButtonBack()
    {
        window.location = '<?=$back_url?>';
    }
    function getInputErrorCorrectionLevel() {
        //return qrcodegen.QrCode.Ecc.MEDIUM;
        //return qrcodegen.QrCode.Ecc.QUARTILE;
        //return qrcodegen.QrCode.Ecc.HIGH;
        return qrcodegen.QrCode.Ecc.LOW;
    }
    var canvas = document.getElementById("qrcode-canvas");
    var ecl = getInputErrorCorrectionLevel();
    var text = "<?=$qrcode?>";
    var segs = qrcodegen.QrSegment.makeSegments(text);
    var minVer = 1;
    var maxVer = 40;
    var mask = -1;
    var boostEcc = true;
    var qr = qrcodegen.QrCode.encodeSegments(segs, ecl, minVer, maxVer, mask, boostEcc);
    var scale = 6;
    var border = 4;
    qr.drawCanvas(scale, border, canvas);
</script>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
