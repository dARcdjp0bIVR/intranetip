<?php
# using: 

##### Change Log [Start] #####
#
#   Date:   2019-03-20 (Bill)   [2019-0313-1725-12066]
#           support print multiple notice on same page  ($sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'])
#
#	Date:	2015-04-28	Bill
#			set $notice_content to class "tabletext", add style to ensure table header default align center
# 
#	Date:	2013-05-15	YatWoon (yy3 customization)
#			- Add print times ($print_times)
#			- Add notice remark besides print button
#
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#
#	Date	:	2010-11-18 (Henry Chow)
#				change the loation of "Page Break" from top to bottom (below the footer)
#
##### Change Log [End] #####

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$status = IntegerSafe($status);

$linterface = new interface_html();
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();

$lform = new libform();

if($NoticeIDStr)
{
	$NoticeIDStr = substr($NoticeIDStr, 0,-1);
	$result = explode(",",$NoticeIDStr);
}
else
{
	# retrieve NoticeID
	//$sign_status_cond = $sign_status ? "b.RecordType is not null" : "b.RecordType is NULL";
	
	if($sign_status!=-1 && $sign_status!="")
	{
		$sign_status_cond = " and " . ($sign_status ? "b.RecordType is not null" : "b.RecordType is NULL");
	}
	if($print_status!=-1 && $print_status!="")
	{
		$print_status_cond = " and " . ($print_status ? "b.PrintDate is not null" : "b.PrintDate is NULL");
	}

	if ($status != "")
		$recordstatus_cond = " and a.RecordStatus='$status'";

	$sql = "select 
		a.NoticeID
		from 
			INTRANET_NOTICE as a 
			left join INTRANET_NOTICE_REPLY as b on (b.NoticeID = a.NoticeID)
		where
			a.DateStart >= '$StartDate' and a.DateStart <= '$EndDate'
			$recordstatus_cond
			and a.Module = '$thisModule'
			$sign_status_cond
			$print_status_cond
		";
	$result = $lnotice->returnVector($sql);
}


if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature'] && $updateOption==2) {
	$lnotice->updateSignatureFooter($result, $option);
}

if($sys_custom['eDiscipline']['yy3'])
{
	$notice_remark = $ldiscipline->NoticeRemark;
}

# need check access right
$valid = true;
$editAllowed = true;

?>
<style>
	th { text-align: center; }
</style>

<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>
<script language="Javascript">
var replyslip = '<?=$i_Notice_ReplySlip?>';
</script>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$notice_remark?> <?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<?
$count = 0;
$print_id = "";
foreach($result as $k=>$id)
{
	$lnotice = new libnotice($id);
	$print_id .= $id.",";
	$StudentID = str_replace("U","",$lnotice->RecipientID);
	$lu = new libuser($StudentID);

	if ($editAllowed)
	{
		$lnotice->retrieveReply($StudentID);
	 	$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
		$ansString = $lform->getConvertedString($temp_ans);
		$ansString.=trim($ansString)." ";
	}

	$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
	$attachment = $lnotice->displayAttachment();
	
	$print_times = $ldiscipline->Notice_Copies ? $ldiscipline->Notice_Copies : 1;
	
	for($p=1; $p<=$print_times;$p++)
	{
		$count++;
	 	if($count < sizeof($result) * $print_times) {
	 	    $breakStyle = "style='page-break-after:always'";
	 	    
	 	    if($extra_print_notice_num > 1 && $sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'] > 1 && 
	 	             ($count % $sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'] > 0)) {
	 	        $breakStyle = "";
	 	    }
	 	} else {
	 		$breakStyle = "";
	 	}
	
		$notice_content = $lnotice->Description;
		?>
		<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
			<tr><td class="tabletext"><?=$notice_content?></td></tr>
		</table>
		
		<? 
		### check include reply slip or not
		$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
		?>
		
		<? if($hasContent) {?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
			</tr>
		</table>
		<? } ?>
		
		<? if($hasContent) {?>
		<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0" >	
		<!-- Relpy Slip -->
			<tr valign='top'>
				<td colspan="2" class='eSportprinttitle' align='center'><b><?=$i_Notice_ReplySlip?></b></td>
			</tr>
		
		<? if($lnotice->ReplySlipContent) {?>
		<!-- Content Base Start //-->
			<tr valign='top'>
				<td colspan="2">
				<?=$lnotice->ReplySlipContent?>
				</td>
			</tr>
		<? } else { 
			$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
		$queString = $lform->getConvertedString($temp_que);
		$queString =trim($queString)." ";
		?>
			<tr valign='top'>
				<td colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
								<form name="ansForm" method="post" action="update.php">
								    <input type=hidden name="qStr" value="">
								    <input type=hidden name="aStr" value="">
								</form>
							
								<script language="Javascript">
								
								<?=$lform->getWordsInJS()?>
								var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
								var sheet= new Answersheet();
								// attention: MUST replace '"' to '&quot;'
								sheet.qString="<?=$queString?>";
								sheet.aString="<?=$ansString?>";
								//edit submitted application
								sheet.mode=1;
								sheet.answer=sheet.sheetArr();
								Part3 = '';
								document.write(editPanel());
								</script>
								
								<SCRIPT LANGUAGE=javascript>
								function copyback()
								{
								         finish();
								         document.form1.qStr.value = document.ansForm.qStr.value;
								         document.form1.aStr.value = document.ansForm.aStr.value;
								}
								</SCRIPT>
							
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
						<? } ?>
		</table>				
						<? } ?>
		
						
		<? if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) { ?>
		<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
			<tr><td>
				<?=$lnotice->footer;?>
			</td></tr>
		</table>	
		<? } ?>
					
	                <div <?=$breakStyle?>>&nbsp;</div>
<? 
	}
}
$print_id = substr($print_id, 0, strlen($print_id)-1);

$update_sql = "update INTRANET_NOTICE_REPLY set PrintDate = now() where NoticeID in ($print_id)";
$lnotice->db_db_query($update_sql);

 ?>                

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
