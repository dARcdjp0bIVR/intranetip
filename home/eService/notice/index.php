<?php
// Modifying by:

############ Change Log Start ###############
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated column display checking
#
#   Date:   2019-07-23  Bill    [2019-0715-0929-00235]
#           Improved: add My Class Notice to notice type drop down list
#
#	Date:	2016-09-05	Ivan [A102645] [ip.2.5.7.10.1]
#			Fixed: missing table header title if school using Approval function and teaching viewing in eService
#
#	Date:	2016-06-06	Kenneth	
#			Edit date logic: compare datetime instead of date only
#
#	Date:	2016-04-07	Bill
#			Fixed: display header Signed/Total for current user with Issue Right
#
#	Date:	2015-12-11	Bill	[2015-0916-1431-55175]
#			Improved: add sorting to DateStart, DateEnd, NoticeNumber
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Display PICs of eNotice
#
#	Date:	2015-04-16	Omas
#			Improved: SQL will not include empty keywords when no keywords is passing in
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			Improved: add In-charge Notice to notice type drop down list
#					  add eNotice PIC logic to table display
#
#	Date:	2015-03-06	(Omas)
#			Fixed: Show notice that not reach start date
#
#	Date:	2014-11-26	(Omas)
#			Improved: New filtering - Sign status
#					  All Notice for parent and student view updated UI to follow past/current notice
#
#	Date:	2014-09-30	(Bill)
#			Improved: allow staff to view all students' replay when "staffview" is turned on
#
#	Date:	2013-01-31	(YatWoon)
#			Improved: use "signerid" to detect the notice is signed or not [Case#2013-0123-1122-59066]
#
#	Date:	2012-04-24	(YatWoon)
#			Fixed: incorrect sql query (customization related) for skss
# 
#	Date:	2012-02-16	(YatWoon)
#			Improved: add "Signed By", "Signed At" column for student view - current notice
#
#	Date:	2011-06-02	(YatWoon)
#			Improved: Search keyword > include notice content
#
#	Date: 2010-09-03 (Henry Chow)
#	Details : allow parent/student can view notice when select "All school notices" in eService
#
#	Date:	2010-08-05	YatWoon
#			change to IP25 standard
#
#	Date:	2010-02-03	YatWoon
#			update "Year" select option, only display the year which includes any notice.
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$noticeType
#	$year
#	$month
#	$NoticeID
################################
$noticeType = IntegerSafe($noticeType);
$year = IntegerSafe($year);
$month = IntegerSafe($month);
$NoticeID = IntegerSafe($NoticeID);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['eServiceNotice'] = 1;
$CurrentPage = "PageNotice";

$lnotice = new libnotice();
$lclass = new libclass();
$TargetType = "P";
$linterface = new interface_html();
$CurrentPage = "PageNotice";

if ($status != 2 && $status != 3) $status = 1;

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$pos = 0;
// [2015-0916-1431-55175] support sorting using DateEnd and NoticeNumber
//$li->field_array = array("a.DateStart");
$li->field_array = array("a.DateStart","a.DateEnd","a.NoticeNumber");
$li->fieldorder2 = ", a.NoticeID DESC";

## select Year
$filterbar = "";
$currentyear = date('Y');

$array_year = $lnotice->retrieveNoticeYears();
if(empty($array_year))	$array_year[] = $currentyear;

$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Years;
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' onChange='document.form1.submit();'",$year,1,0, $firstname) . "&nbsp;";

##filtering 
if ($_SESSION['UserType']!=USERTYPE_STAFF){
	$noticeType = $noticeType == "" ? "1" : $noticeType;
	$signStatus = $signStatus == "" ? "0" : $signStatus;
	
	## Select Sign status
	$signStatusSelect = "";
	$signStatusSelect = "<SELECT name='signStatus' onChange='this.form.submit()'>\n";
	$signStatusSelect.= "<OPTION value=0 ".($signStatus==0? "SELECTED":"").">".$Lang['eNotice']['AllSignStatus']."</OPTION>\n";
	$signStatusSelect.= "<OPTION value=1 ".($signStatus==1? "SELECTED":"").">".$Lang['eNotice']['Not Signed']."</OPTION>\n";
	$signStatusSelect.= "<OPTION value=2 ".($signStatus==2? "SELECTED":"").">".$Lang['eNotice']['Signed']."</OPTION>\n";
	$signStatusSelect.= "</SELECT>&nbsp;\n";
	
	## select of All Notices / Issued Notice
	$noticeTypeSelect = "";
	$noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
	$noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">".$Lang['eNotice']['AllNoticeType']."</OPTION>\n";
	$noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">".$Lang['eNotice']['UnexpiredNotice']."</OPTION>\n";
	$noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">".$Lang['eNotice']['ExpiredNotice']."</OPTION>\n";
	$noticeTypeSelect.= "</SELECT>&nbsp;\n";
}else{
	$signStatusSelect = "";
}

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Months;
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' onChange='document.form1.submit();'",$month,1,0,$firstname) . "&nbsp;";

$viewRight = $lnotice->hasNormalRight();
$fullRight = $lnotice->hasFullRight();
$hasIssueRight = $lnotice->hasIssueRight();
// [2015-0323-1602-46073] - check if user is eNotice PIC
$isPIC = $lnotice->isNoticePIC();

$keyword = convertKeyword($keyword);

if($sys_custom['skss']['StudentAwardReport']) {
	//$extraCond = " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";	
	$extraCond = " AND (a.Module!='DISCIPLINE_STUDENTAWARDREPORT' or a.Module is NULL)";	
}


$x = "";
        if ($_SESSION['UserType']==USERTYPE_STAFF)
        {
            $class = $lclass->returnHeadingClass($UserID, 1);
            // $isClassTeacher = ($class!="");
            $isClassTeacher = empty($class)? 0 : 1;

	        ## select of All Notices / Issued Notice / In-charge Notice
	        $noticeTypeSelect = "";
	        $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
	        $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_AllNotice</OPTION>\n";
	        if ($lnotice->hasIssueRight()) {
	            $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_MyNotice</OPTION>\n";
	        }
	  		// [2015-0323-1602-46073] - add option to show In-charge Notice
	        if($isPIC) {
	            $noticeTypeSelect.= "<OPTION value=3 ".($noticeType==3? "SELECTED":"").">".$Lang['eNotice']['InChargeNotice']."</OPTION>\n";
	        }
	        // [2019-0715-0929-00235] - add option to show notice of teaching classes
            if ($isClassTeacher && $status == 1) {
                $noticeTypeSelect.= "<OPTION value=4 ".($noticeType==4? "SELECTED":"").">".$Lang['eNotice']['MyClassNotice']."</OPTION>\n";
            }
	        $noticeTypeSelect.= "</SELECT>&nbsp;\n";
        
            if($noticeType && $noticeType != 4)	# Issued Notice / In-charge Notice
            {
				// [2015-0323-1602-46073] - return sql for showing In-charge Notice only if $noticeType == 3
                $li->sql = $lnotice->returnMyNotice($year, $month, $status, 1, $keyword, $TargetType, 1, $noticeType==3);
                //$li->no_col = 7;
                $li->no_col = 8;
                $li->IsColOff = "eNoticedisplayMyNotice";

                // TABLE COLUMN
                //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
				$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
				$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
				$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                $li->column_list .= "<th>". $Lang['eNotice']['ViewNotice'] ."</th>\n";
                $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                $li->column_list .= "<th>".$i_Notice_Type."</th>\n";
                $li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Total."</th>\n";
                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
            }
            else		# All Notices
            {
                // $class = $lclass->returnHeadingClass($UserID, 1);
				// $isClassTeacher = ($class!="");

                $li->sql = $lnotice->returnNoticeListTeacherView($status,$year,$month,1, $keyword, "ALL", $TargetType,1, $noticeType==4);
                // [2015-0428-1214-11207] increase no of column
                //$li->no_col = 7;
                $li->no_col = 8;
                $li->IsColOff = "displayTeacherView";

					$tabletop_css = "tablebluetop";					
					//$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
					//$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
					//$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
					$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
					$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
					$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
					$li->column_list .= "<th>". $Lang['eNotice']['ViewNotice'] ."</th>\n";
					$li->column_list .= "<th>".$i_Notice_Title."</th>\n";
					$li->column_list .= "<th>".$i_Notice_Issuer."</th>\n";	
					// [2015-0428-1214-11207] display table header - PIC
					$li->column_list .= "<th>".$i_Profile_PersonInCharge."</th>\n";

					// [2020-0604-1821-16170]
                    //if($lnotice->needApproval) {
					if($lnotice->needApproval || $lnotice->payment_needApproval) {
						$li->column_list .= "<th>". $Lang['eNotice']['ApprovedBy'] ."</th>\n";
						$li->no_col++;
					}
					
					if ($isClassTeacher && $status == 1)
					{
                        $li->column_list .= "<th>".$i_Notice_ViewOwnClass."</th>\n";
                        $li->no_col++;
					}

                // [2020-0604-1821-16170]
				// [2015-0323-1602-46073] - display table header "Signed/Total" if user has view right or is eNotice PIC
                // [2016-04-07 updated] - display table header "Signed/Total" if user 
                //									1. has view right
                //									2. has issue right
                //									3. is an eNotice PIC
                // if (($viewRight || $lnotice->staffview || $isPIC) && $status == 1)
                //if (($viewRight || $hasIssueRight || $isPIC || $lnotice->staffview) && $status == 1)
                if (($viewRight || $hasIssueRight || $isPIC || $lnotice->staffview || $lnotice->payment_staffview) && $status == 1)
                {
                        $li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Total."</th>\n";
                        $li->no_col++;
                }
                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";

            }
       }
        else if ($_SESSION['UserType']==USERTYPE_PARENT)
        {
        	//$noticeType = $noticeType == "" ? "0" : $noticeType;
        	
            # Grab children ids
             $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
             $children = $lnotice->returnVector($sql);
             if (sizeof($children)==0)
             {
                 include_once($PATH_WRT_ROOT."includes/libaccessright.php");
				$laccessright = new libaccessright();
				$laccessright->NO_ACCESS_RIGHT_REDIRECT();
				exit;
                 }

             $child_list = implode(",",$children);
             switch($noticeType)
             {
                case 0:                        ## Current
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         
                         if ($signStatus == 0){
                         	$signStatus_conds = "";	
                         }else if($signStatus == 1){
                         	$signStatus_conds = " AND c.SignerID IS NULL ";
                         }else if($signStatus == 2){
                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                         }	
                         if($keyword != ''){
                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
                         }
                         else{
                         	$keyword_conds = "";
                         }
                         
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified,
                                        c.SignerID
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) 
									   AND c.StudentID IN ($child_list) AND a.RecordStatus = 1
								 	   AND a.DateStart <= NOW() AND a.DateEnd >= NOW()
                                       $conds and a.TargetType='$TargetType' 
                                       AND a.IsDeleted=0
										$extraCond
										$signStatus_conds
										$keyword_conds
                                       ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentView";

                        // TABLE COLUMN
                        //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_StudentName."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                        break;
                case 1:                        ## All School Notice
//                        $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword, $child_list, $TargetType,1);
//                        $li->no_col = 5;
//                        $li->IsColOff = "displayAllNotice";
//                        
//                        // TABLE COLUMN
//                        $li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
//                        $li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
//                        $li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
//                        $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
//                        $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";


						$name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         
                         if ($signStatus == 0){
                         	$signStatus_conds = "";	
                         }else if($signStatus == 1){
                         	$signStatus_conds = " AND c.SignerID IS NULL ";
                         }else if($signStatus == 2){
                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                         }
                         
                         if($keyword != ''){
                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
                         }
                         else{
                         	$keyword_conds = "";
                         }	
                         
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified,
                                        c.SignerID
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list) AND a.RecordStatus = 1
								 AND 	1
                                       $conds and a.TargetType='$TargetType' 
                                       AND a.IsDeleted=0 AND a.DateStart <= NOW()
										$extraCond
										$signStatus_conds
										$keyword_conds
                                       ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentHistory";

                        // TABLE COLUMN
                        //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_StudentName."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                        break;
                case 2:                        ## History
                        # Grab children ids
                         $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
                         $children = $lnotice->returnVector($sql);
                         if (sizeof($children)==0) return "";

                         $child_list = implode(",",$children);
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         
                         if ($signStatus == 0){
                         	$signStatus_conds = "";	
                         }else if($signStatus == 1){
                         	$signStatus_conds = " AND c.SignerID IS NULL ";
                         }else if($signStatus == 2){
                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                         }	
                         
                         if($keyword != ''){
                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
                         }
                         else{
                         	$keyword_conds = "";
                         }
                         
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified,
                                        c.SignerID
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list)
                                        AND a.RecordStatus = 1
										$keyword_conds
                                       AND a.DateEnd < NOW()
                                       $conds 
                                       AND a.IsDeleted=0 and a.TargetType='$TargetType' $extraCond
										$signStatus_conds
								";
						
                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentHistory";

                        // TABLE COLUMN
                        //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                        //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_StudentName."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                        $li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                        break;
             }

//             ## select of All Notices / Issued Notice
//                $noticeTypeSelect = "";
//                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
//                $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
//                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$Lang['eNotice']['UnexpiredNotice']</OPTION>\n";
//                //if ($lnotice->showAllEnabled)
//                        //$noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
//                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$Lang['eNotice']['ExpiredNotice']</OPTION>\n";
//                $noticeTypeSelect.= "</SELECT>&nbsp;\n";
               
        }
        else if ($_SESSION['UserType']==USERTYPE_STUDENT)
        {
                switch($noticeType)
                {
                        case 0:                ## Current
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }
                                 
                                 if ($signStatus == 0){
		                         	$signStatus_conds = "";	
		                         }else if($signStatus == 1){
		                         	$signStatus_conds = " AND c.SignerID IS NULL ";
		                         }else if($signStatus == 2){
		                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                                 }
                                 
                                 if($keyword != ''){
		                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
		                         }
		                         else{
		                         	$keyword_conds = "";
		                         }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $name_field2 = getNameFieldWithClassNumberByLang("d.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                                a.RecordType, c.RecordType, c.RecordStatus,
                                                IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        		c.DateModified
                                         FROM INTRANET_NOTICE_REPLY as c
                                              LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                              LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                              LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                         WHERE 
                                         		('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' 
                                         		AND a.RecordStatus = 1
									           	$keyword_conds
                                               	AND a.DateStart <= NOW() AND a.DateEnd >= NOW() 
                                               	$conds 
                                               	AND a.IsDeleted=0 and a.TargetType='$TargetType' $extraCond
												$signStatus_conds	
										";

                                $li->sql = $sql;
                                $li->no_col = 5;
                                $li->IsColOff = "displayStudentView";

                                // TABLE COLUMN
                                //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                       		 	$li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                                break;
                        case 1:                ## All
//                                //$x .= $lnotice->displayAllNotice($year,$month);
//                                $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword, $UserID, $TargetType,1);
//                                $li->no_col = 5;
//                                $li->IsColOff = "displayAllNotice";
//
//                                // TABLE COLUMN
//                                $li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
//                                $li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
//                                $li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
//                                $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
//                                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";

                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }
                                 
                                 if ($signStatus == 0){
		                         	$signStatus_conds = "";	
		                         }else if($signStatus == 1){
		                         	$signStatus_conds = " AND c.SignerID IS NULL ";
		                         }else if($signStatus == 2){
		                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                                 }
                                 
                                 if($keyword != ''){
		                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
		                         }
		                         else{
		                         	$keyword_conds = "";
		                         }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $name_field2 = getNameFieldWithClassNumberByLang("d.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                                a.RecordType, c.RecordType, c.RecordStatus,
                                                IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        		c.DateModified
                                         FROM INTRANET_NOTICE_REPLY as c
                                              LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                              LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                              LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                         WHERE 
                                         		('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' 
                                         		AND a.RecordStatus = 1
									           	$keyword_conds
                                               	$conds 
                                               	AND a.DateStart <= NOW() AND a.IsDeleted=0 and a.TargetType='$TargetType' $extraCond
												$signStatus_conds
										";

                                $li->sql = $sql;
                                $li->no_col = 5;
                                $li->IsColOff = "displayStudentHistory";

                                // TABLE COLUMN
                                //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                       		 	$li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                                break;
                        case 2:                ## History
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }
								
								 if ($signStatus == 0){
		                         	$signStatus_conds = "";	
		                         }else if($signStatus == 1){
		                         	$signStatus_conds = " AND c.SignerID IS NULL ";
		                         }else if($signStatus == 2){
		                        	 $signStatus_conds = " AND c.SignerID IS NOT NULL ";
                                 }
                                 
                                 if($keyword != ''){
		                         	$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
		                         }
		                         else{
		                         	$keyword_conds = "";
		                         }
								
                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field),
                                        c.DateModified
		                                FROM INTRANET_NOTICE_REPLY as c
		                                    LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
		                                    LEFT OUTER JOIN INTRANET_USER as b ON c.SignerID = b.UserID
		                                WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' AND a.DateEnd < NOW() $conds 
											$keyword_conds
											AND a.RecordStatus=1 
											AND a.IsDeleted=0 and a.TargetType='$TargetType' $extraCond
											$signStatus_conds
										";

                                 $li->sql = $sql;
                                $li->no_col = 7;
                                $li->IsColOff = "displayStudentHistory";

                                // TABLE COLUMN
                                //$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_DateEnd."</th>\n";
                                //$li->column_list .= "<th>".$i_Notice_NoticeNumber."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
								$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_Title."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_SignerNoColon."</th>\n";
                                $li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";

                                break;
                }

//             ## select of All Notices / Issued Notice
//                $noticeTypeSelect = "";
//                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
//                $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
//                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$Lang['eNotice']['UnexpiredNotice']</OPTION>\n";
////                if ($lnotice->showAllEnabled)
////                        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
//                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$Lang['eNotice']['ExpiredNotice']</OPTION>\n";
//                $noticeTypeSelect.= "</SELECT>&nbsp;\n";

        }
        else $x .= "";

### Button
/*
$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</td></table>";
*/
$searchTag = "<input name=\"keyword\" id=\"searchText\" type=\"text\" value=\"$keyword\" onkeyup=\"Check_Go_Search(event);\"/>";


### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function viewNotice(id,studentID)
{
       newWindow('./sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewPaymentNotice(id,studentID)
{
       newWindow('./eNoticePayment_sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewResult(id)
{
         newWindow('./result.php?NoticeID='+id,10);
}
function sign(id,studentID)
{
         newWindow('./sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function signPaymentNotice(id,studentID)
{
         newWindow('./eNoticePayment_sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewReply(id,studentID)
{
         newWindow('./view.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewPaymentReply(id,studentID)
{
         newWindow('./eNoticePayment_view.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewNoticeClass(id)
{
         newWindow('./tableview.php?type=1&NoticeID='+id,10);
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
</SCRIPT>

<br />
<form name="form1" method="get" action="index.php">

<!-- function & search bar //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2">
		<div class="content_top_tool">
				<div class="Conntent_search"><?=$searchTag?></div>
			<br style="clear:both" />
		</div>
	</td>
</tr>               

<tr>
	<td valign="bottom"><div class="table_filter"><?=$noticeTypeSelect?><?=$signStatusSelect?><?=$status_select?><?=$select_year?><?=$select_month?></div></td>
</tr>


<tr>
<td colspan="2">
	<?=$li->display($TargetType);?>
</td>
</tr>

<? if(($_SESSION['UserType']==USERTYPE_PARENT || $_SESSION['UserType']==USERTYPE_STUDENT) && $noticeType!=0) {?>
<tr>
	<td>* <?=$Lang['eNotice']['LateSubmission']?></td>
</tr>
<? } ?>

</table>
<br />


<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

