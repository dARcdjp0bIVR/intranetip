<?php
// Modifying by : 

####### Change Log [Start] #######
#
#   Date:   2020-11-09 Ray
#           add resign notice
#
#   Date:   2020-10-14  Bill    [2020-0929-0952-27235]
#           hide Approve / Reject btn, to fix multiple submit
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-07-07 Ray
#           Add fps qrcode
#
#	Date:	2020-01-02	Philips [2019-1125-1703-32207]
#			Allow Admin to sign payment notice either passed deadline or not
#
#	Date:	2019-12-05	Philips
#			remove $sys_custom['eNotice']['PreviewShowTargetDetails'], show target details in general
#
#	Date:	2019-11-12  Carlos
#			Check student subsidy identity with both notice start date and end date.
#
#	Date:	2019-10-17 Philips [2019-1016-1125-40260]
#			improved: add back button for teacher
#
#	Date:	2019-07-29  Carlos
#			Added [Modify reply slip answer] button for admin to change reply slip answer, no payment would be performed, just modify answer.
#			And display the modify answer records at status section.
#
#	Date:	2019-07-26  Carlos
#			If using eWallet to pay, and enabled [Pay At School], admin/teacher can sign on behalf of parent with [Pay At School] method to sign the reply slip. 
#
#   Date:   2019-06-26  Bill    [2019-0621-0947-35276]
#           Show details of Target Type when preview eNotice    ($sys_custom['eNotice']['PreviewShowTargetDetails'])
#
#	Date:	2019-06-06	Carlos
#			Added [Pay at school] button to sign the replyslip first but pay later.
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#
#   Date:   2019-03-26 (Bill)   [2019-0131-1137-13207]
#           hide sign button    ($sys_custom['eNotice']['HideSignButton'])
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - display text content from FCKEditor
#
#   Date:   2018-10-18 (Bill)   [2018-0920-1542-19235]
#           Not allow sign notice if using TNG & Alipay
#
#	Date:	2018-08-31 (Carlos)
#			Added flag $sys_custom['PaymentNoticeDoNotCheckBalance'] to ignore checking balance.
#
#   Date:   2018-07-05 (Bill)   [DM#3447]
#           modified js calculateAmountTotal(), use parseFloat() and toFixed() to handle total amount calculation
#
#	Date:	2017-02-01 (Isaac)
#			modifided position for notice number field from below the titile to above the title. 
#           fixed consistancy of ui with parent and student notice.
#
#	Date:	2018-01-24	(Carlos) Count and display total amount for selected items that want to pay for.
#	Date:	2018-01-17  (Carlos) Skip checking balance if enabled $sys_custom['ePayment']['TNG'].
#
#	Date:	2017-05-09	(Bill)	[2017-0428-1605-58206]
#			fixed: handle draft problem in admin view (display draft input for parent only)
#
#	Date:	2016-11-03	Villa	[U107012]
#			Add Notice number to the view page
#
#	Date:	2016-10-17	Bill	[2016-1013-1541-17214]
#			Improved: display edit button for approval users
#
#	Date:	2016-09-09	Bill	[2016-0907-1616-07236]
#			fixed: direct use timestamp for deadline checking
#
#	Date:	2016-07-11	Kenneth
#			Improvement: approval and reject action for unapproved notice
#
#	Date:	2015-09-18	Bill	[2015-0917-1053-13066]
#			Improved: Allow eNotice admin to sign payment notice for parents - controlled by settings
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2014-04-14	YatWoon
#			Revised the $valid checking [Case#J61206]
#
#	Date:	2014-02-28	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#	Date	:	2010-09-27 Henry Chow
#				remove all formatting of hidden field "amountBalance"
#
######## Change Log [End] ########

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#	$StudentID
################################

$NoticeID = IntegerSafe($NoticeID);
//$StudentID = IntegerSafe($StudentID);

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$Json = new JSON_obj();
$lu = new libuser($UserID);

$valid = true;
$view_only = false;

// Check Notice Deadline
$today = time();
$deadline = strtotime($lnotice->DateEnd);

// [2016-0907-1616-07236] direct compare timestamp
//$PassDeadline = compareDate($today,$deadline)>0;
$PassDeadline = ($today > $deadline);

$allow_late_sign = $lnotice->isLateSignAllow == 1;

if ($lu->isStudent())
{
    //$valid = false;
    $view_only = true;
}

$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";
$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;

$can_pay_at_school = $lpayment->Settings['PaymentNoticeCanPayAtSchool'] == 1;

$can_sign_by_admin = false;

$modify_answer_records = array();

$show_re_sign = false;
if ($StudentID!="")
{
	$is_notice_signed = $lnotice->isTargetNoticeSigned($NoticeID,$StudentID);
	if($is_notice_signed && $is_payment_notice){
		$modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'StudentID'=>$StudentID,'GetModifierUserInfo'=>true));

		$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
		$arrItemIDs = $lnotice->returnVector($sql);
		if(sizeof($arrItemIDs)>0) {
			$targetItemID = implode(",", $arrItemIDs);

			$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$StudentID' AND ItemID IN ($targetItemID)";
			$NoOfItemNeedToPaid = $lnotice->returnVector($sql);

			$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$StudentID' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
			$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);

			if ($NoOfItemNeedToPaid[0] == 0 || $NoOfItemPaidSuccessfully[0] == 0) {
				$show_re_sign = true;
			}
		}
	}
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            //$today = time();
            //$deadline = strtotime($lnotice->DateEnd);
            //if (compareDate($today,$deadline)>0)
            if ($PassDeadline)
            {
                //$valid = false;
                $view_only = true;
            }
            else
                $editAllowed = true;
        }
    }
    else # Teacher
    {
    	if(strtoupper($lnotice->Module) == "PAYMENT")
    	{
    		$editAllowed = false;
    		// [2015-0917-1053-13066] Setting: Allow eNotice admin to sign payment notice for parents - true
    		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && $lnotice->isAllowAdminToSignPaymentNotice && $StudentID!="undefined"){
    			### 2020-01-02 Philips [2019-1125-1703-32207] - Allow Admin sign either passed deadline or not
//     			if(!$PassDeadline){
    				$editAllowed = true;
    				if($use_payment_system && $can_pay_at_school){
    					$can_sign_by_admin = true;
    				}
//     			}
    		}
    	}
    	else
    	{
		    if($lnotice->IsModule)
		    {
				$editAllowed = true;    
		    }
		    else
		    {
	        	$editAllowed = $lnotice->isEditAllowed($StudentID);
	    	}
	    }
    }
}
else
{
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
}

if ($editAllowed)
{
    if ($StudentID == "")
    {
        $valid = false;
    }
}

if($view_only)
{
	header("Location: eNoticePayment_view.php?StudentID=$StudentID&NoticeID=$NoticeID");
    exit();
}

############################################################
#	Check student belongs to this notice?
############################################################

if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
{
	$valid = false;
}

if(!$valid && !$isTeacher)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$use_direct_pay_fps_qrcode = false;
//$use_payment_system = false;
if ($use_payment_system) {
    if ($lpayment->isFPSDirectPayEnabled() && $sys_custom['ePayment']['FPSUseQRCode']) {
        $multi_merchant_ids = $lpayment->returnPaymentNoticeMultiMerchantAccountID($NoticeID);
        $merchants = $lpayment->getMerchantAccounts(array('ServiceProvider' => 'FPS', 'AccountID' => $multi_merchant_ids, 'RecordStatus' => 1));
        if (count($merchants) > 0) {
            $use_direct_pay_fps_qrcode = true;

            $fps_payment_under_processing = false;
            if($use_payment_system && $isParent && $lnotice->signerID && $lnotice->replyStatus != '2'){
                $fps_payment_under_processing = true;
            }
            if(!$editAllowed && $fps_payment_under_processing && !$afterDeadline) {
                $editAllowed = true;
            }
        }
    }
}



	// [2015-0428-1214-11207] get PICs of eNotice
	if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
		$pics = "";
		$noticePICs = $lnotice->returnNoticePICNames();
		if(count($noticePICs) > 0){
			$delim = "";
			for($picCount=0; $picCount<count($noticePICs); $picCount++){
				$pics .= $delim.$noticePICs[$picCount]['UserName'];
				$delim = ", ";
		  	}
		}
	}
	
	$lnotice->retrieveReply($StudentID, $isParent);
 	$temp_ans = str_replace("&amp;", "&", $lnotice->answer);
	$ansString = $lform->getConvertedString($temp_ans);
	$ansString .= trim($ansString)." ";
	
/*
if (!$valid)
{
    header("Location: view.php?StudentID=$StudentID&NoticeID=$NoticeID");
    exit();
}
*/

$result = ($lpayment->checkBalance($StudentID,0));
$account_balance = $lpayment->getExportAmountFormat($result[0]);

$student_subsidy_identity_id = '';
if($StudentID != '' && $StudentID > 0)
{
	$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
	if(count($student_subsidy_identity_records)>0){
		$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
	}
}

$targetType = array("", $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$targetRecipientDisplay = $targetType[$lnotice->RecordType];

// [2019-0621-0947-35276] Show details of Target Type when preview eNotice
if($isTeacher && (!isset($StudentID) || $StudentID == "" || $StudentID == '0'))
{
     $targetRecipient = $lnotice->returnRecipientNames();
     $targetRecipientDisplay = $targetRecipient[0];
     if($targetRecipient[1] != '')
     {
         $targetRecipientDisplay .= "<br/>";
         $targetRecipientDisplay .= "[".$targetRecipient[1]."]";
     }
}

# modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module=='')
{
	$attachment = $lnotice->displayAttachment();
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT")
		$attachment = $lnotice->displayAttachment();
	else
		$attachment = $lnotice->displayModuleAttachment();
}

// [2019-0118-1125-41206] display text content from FCKEditor
if(strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApplyEditor) {
    $this_Description = htmlspecialchars_decode($lnotice->getNoticeContent($StudentID));
}
else if($lnotice->IsModule) {
	$this_Description = nl2br($lnotice->Description);
}
else {
    $this_Description = nl2br(intranet_convertAllLinks($lnotice->Description));
}

/*
if($plugin['Disciplinev12']) {
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	$templateVar = $ldiscipline->TemplateVariable();
	
	$newDescription = $this_Description;
	
	if(is_array($templateVar))
	{
		foreach($templateVar as $Key=>$Value)
		{
			$newDescription = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" />", $Value, $newDescription);	
			$newDescription = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" alt=\"\" />", $Value, $newDescription);	
			$newDescription = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2007a/discipline/".$Key.".gif\" />", $Value, $newDescription);				
			$newDescription = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\" alt=\"\" />", $Value, $newDescription);				
		} 
	}
}
*/

if($error == 1) {
	$msg = $Lang['ePayment']['NotEnoughBalance'];	
}

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice2;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

// [2020-0604-1821-16170]
# [2016-1013-1541-17214] check if approval user can edit notice or not
//if(!$editAllowed && strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isApprovalUser() && $lnotice->RecordStatus>=4)
if(!$editAllowed && strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApprovalUser() && $lnotice->RecordStatus>=4)
{
	$link = "../../eAdmin/StudentMgmt/notice/payment_notice/paymentNotice_edit.php?NoticeID=".$NoticeID;
	$edit_btn = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "EditNotice('$link');" );
}

echo $linterface->INCLUDE_JS_CSS();
?>

<style>
.approval_div{
	border: 1px #ffffaa solid;
	background: #ffffb3;
	padding: 0px 20px 10px 20px; 
	text-align:center;
}
</style>
<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<SCRIPT LANGUAGE=Javascript>
var need2checkform = true;

function checkform(obj)
{
	var formAllFilled = sheet.validateFillInForm();
	if (!need2checkform || formAllFilled)
	{
	    //return true;	// payment notice need to check form
	}
	else
	{
		alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
	    return false;
	}
	 
	<? if($lnotice->DebitMethod==1 && !$do_not_check_balance) { ?>
	var AccountBalance = document.getElementById('amountBalance').value.replace(',','');
	//alert(document.getElementById('thisAmountTotal').value+"|"+AccountBalance);
	if(parseInt(document.getElementById('thisAmountTotal').value) > parseInt(AccountBalance)) {
		alert("<?=$Lang['eNotice']['NotEnoughBalance']?>");  
		return false;
	}
	else {
	<? } ?>
        if (!confirm('<?=$i_Notice_Alert_Sign?>'))
        {
        	return false;
        }
        else
        {
         	goSaveDraft(0);
			document.form1.action = "eNoticePayment_sign_update.php";
			document.form1.target = "_self";
			$("#submit2").removeClass("formbutton_v30").addClass("formbutton_disable_v30").attr("disabled", true);
			return true;
		}
	<? if($lnotice->DebitMethod==1 && !$do_not_check_balance) { ?>
	}
	<? } ?>
}

function click_print()
{
	with(document.form1)
	{
    	action = "eNoticePayment_view_print_preview.php";
        target = "_blank";
        document.form1.submit();
	}
}
function calculateAmountTotal()
{
	var selected_rbs = $(document.answersheet).find('input[type=radio]:checked');
	var selected_cbs = $(document.answersheet).find('input[type=checkbox]:checked');
	var total_to_pay = 0;
	for(var i=0;i<selected_rbs.length;i++){
		var data_amount = $(selected_rbs[i]).attr('data-amount');
		if(data_amount){
			//total_to_pay += parseInt(data_amount);
			total_to_pay += parseFloat(data_amount);
		}
	}
	for(var i=0;i<selected_cbs.length;i++){
		var data_amount = $(selected_cbs[i]).attr('data-amount');
		if(data_amount){
			//total_to_pay += parseInt(data_amount);
			total_to_pay += parseFloat(data_amount);
		}
	}
	var container = $('#TotalAmountToPay');
	if(container.length > 0){
		//container.html(total_to_pay);
		container.html(total_to_pay.toFixed(2));
	}
}

function updateAmountTotal(id, oldAmt, amt, type)
{
	if(isNaN(oldAmt)) oldAmt = 0;
//	alert(oldAmt+'//'+amt);
	
	if(type!=3) { // not checkbox option ("3" is checkbox options)
		document.getElementById('thisAmountTotal').value = parseFloat(document.getElementById('thisAmountTotal').value) + parseFloat(amt) - oldAmt;
		document.getElementById('thisItemAmount'+id).value = amt;
	}
	else {		// checkbox option
		document.getElementById('thisAmountTotal').value = parseFloat(document.getElementById('thisAmountTotal').value) + parseFloat(amt);
		document.getElementById('thisItemAmount'+id).value = parseFloat(document.getElementById('thisItemAmount'+id).value) + parseFloat(eval(amt));
	}
	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
	
	calculateAmountTotal();
}

function goSaveDraft(displayAlert) {
	paymentNoticeAjax = GetXmlHttpObject();
	if (paymentNoticeAjax == null)
	{
		alert (errAjax);
		return;
	} 
	copyback();
	    
	var url = 'ajax_paymentNotice_saveDraft.php';
	var PoseValue = Get_Form_Values(document.getElementById("form1"));
	paymentNoticeAjax.onreadystatechange = function() {
		if (paymentNoticeAjax.readyState == 4) {
			if(displayAlert)
				alert(paymentNoticeAjax.responseText);
			//Thick_Box_Init();
		}
	};
	
	paymentNoticeAjax.open("POST", url, true);
	paymentNoticeAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	paymentNoticeAjax.send(PoseValue);
}

function EditNotice(url)
{
	window.opener.location=url;
	window.close();
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function sendApproveAjax(approve_state)
{
	if(!confirm('<?=$Lang['eNotice']['Warning']['ConfirmChangingStatus']  ?>')){
		return;
	}

    $('#approval_div_radio').hide();
	$('#approval_div h4').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');

	$.ajax({
		url: "ajax_approve_notice.php",
		type: "POST",
		data: { 
			approve_state : approve_state,
			noticeID: '<?=$NoticeID ?>'  
			},
		dataType: "html",
		success: function(response){
			if(response=='APPROVED'){
				approved();
				opener.location.reload();
			}
			else if(response=='REJECTED'){
				rejected();
				opener.location.reload();
			}
		}
	});
}

var approvedOrRejected = false;
function approved(){
	$('#approval_div_radio').hide();
	$('#approval_div h4').html('<?=$Lang['General']['Approved'] ?>');
}

function rejected(){
	$('#approval_div_radio').hide();
	$('#approval_div h4').html('<?=$Lang['General']['Rejected'] ?>');
}

$(document).ready(function(){
	<?php
	if($enotice_payment_error != '') {

		if(isset($Lang['ePayment']['PaymentFailOrCanceled_'.$enotice_payment_error])) {
			echo "alert('".$Lang['ePayment']['PaymentFailOrCanceled_'.$enotice_payment_error]."');";
		} else {
			echo "alert('".$Lang['ePayment']['PaymentFailOrCanceled_other']." [".$enotice_payment_error."]');";
		}
	}
	?>


	calculateAmountTotal();
});

var submited_resign_notice = false;
function resignNotice() {
    if(submited_resign_notice) {
        return;
    }

    paymentNoticeAjax = GetXmlHttpObject();
    if (paymentNoticeAjax == null)
    {
        alert (errAjax);
        return;
    }

    if(confirm('<?=$Lang['eNotice']['ResignNoticeConfirm']?>') == false) {
        return;
    }

    $("#resign_notice").removeClass("formbutton_v30").addClass("formbutton_disable_v30").attr("disabled", true);
    submited_resign_notice = true;
    var url = 'ajax_paymentNotice_resignNotice.php';
    var PoseValue = Get_Form_Values(document.getElementById("form1"));
    paymentNoticeAjax.onreadystatechange = function() {
        if (paymentNoticeAjax.readyState == 4) {
            alert(paymentNoticeAjax.responseText);
            location.reload();
        }
    };

    paymentNoticeAjax.open("POST", url, true);
    paymentNoticeAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    paymentNoticeAjax.send(PoseValue);
}
</SCRIPT>

<br />   
<table align='right' width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td align='right'><?=$linterface->GET_SYS_MSG("",$msg)?></td>
	</tr>
</table>

<?php if ($editAllowed && !$lnotice->isTargetNoticeSigned($NoticeID,$StudentID) && $use_payment_system && !$can_sign_by_admin) { ?>
	<?= $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['ePayment']['MustSignNoticeInApp']); ?>
<?php  } ?>

<div id="spanTemp"></div>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table class="form_table_v30">
                                
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_DateStart?></span></td>
					<td class='tabletext'><?=$lnotice->DateStart?></td>
				</tr>
                                
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_DateEnd?></span></td>
					<td class='tabletext'><?=$lnotice->DateEnd?></td>
				</tr>
               	<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_NoticeNumber?></span></td>
					<td class='tabletext'><?=$lnotice->NoticeNumber?></td>
				</tr>
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_Title?></span></td>
					<td class='tabletext'><?=$lnotice->Title?></td>
				</tr>
						    
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_Description?></span></td>
					<td class='tabletext'><?=$this_Description?></td>
				</tr>
				
			<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></span></td>
                    <? if($lnotice->DebitMethod == 1){ ?>
						<td class='tabletext'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly']?></td>
					<? } else { ?>
						<td class='tabletext'><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater']?></td>
					<? } ?>
				</tr>
			<? } ?>
                
            <? if ($attachment != "") { ?>
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_Attachment?></span></td>
					<td class='tabletext'><?=$attachment?></td>
				</tr>
            <? } ?>
                
            <? if ($isTeacher) {
    				$issuer = $lnotice->returnIssuerName();
			?>
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_Issuer?></span></td>
					<td class='tabletext'><?=$issuer?></td>
				</tr>
        	<? } ?>
        	
			<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Profile_PersonInCharge?></span></td>
					<td class='tabletext'><?=$pics?></td>
				</tr>
            <?php } ?>
                        	
                        	<? if ($editAllowed) {
                                        if ($lnotice->replyStatus != 2)
                                        {
                                            $statusString = "$i_Notice_Unsigned";
                                        }
                                        else
                                        {
                                            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                                            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                                            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                                        }
                                        
                                        if(count($modify_answer_records) > 0){
	                                        foreach($modify_answer_records as $modify_answer_record){
												$statusString .= "<br />".str_replace(array('<!--USER_NAME-->','<!--DATETIME-->'),array($modify_answer_record['ModifierUserName'],$modify_answer_record['InputDate']),$Lang['eNotice']['AnswerModifiedByAt']);
											}
                                        }
                                        
				?>
                        	<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_SignStatus?></span></td>
					<td class='tabletext'><?=$statusString?></td>
				</tr>
                        
                        	<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_UserStudentName?></span></td>
					<td class='tabletext'><?=$lu->getNameWithClassNumber($StudentID)?></td>
				</tr>
                        
				<? } ?>
                                
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'><?=$i_Notice_RecipientType?></span></td>
					<!-- <td class='tabletext'><?=$targetType[$lnotice->RecordType]?></td> -->
					<td class='tabletext'><?=$targetRecipientDisplay?></td>
				</tr>
				
				</table>
                        </td>
		</tr>
                </table>
	<td>
</tr>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>
<? if($hasContent) {?>
<tr>
	<td colspan="2">      
					<!-- Break -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
                                        </tr>
                                        </table>
	</td>
</tr>
<? } ?>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />                                
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <? if($hasContent) {?>
                                <!-- Relpy Slip -->
                                <tr valign='top'>
								<td colspan="2" class='eNoticereplytitle' align='center'><?=$i_Notice_ReplySlip?></td>
							</tr>

				
				<? if($lnotice->ReplySlipContent) {?>
				<!-- Content Base Start //-->
					<tr valign='top'>
						<td colspan="2">
						<?=$lnotice->ReplySlipContent?>
						</td>
					</tr>
				<? } else {
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString =trim($queString)." ";
// 						$queString = str_replace("&lt;br&gt;","<br>",$queString);
						
					?>
					<!-- Question Base Start //-->
					<tr valign='top'>
					<td colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                        	<td>
                                <form id="ansForm" name="ansForm" method="post" action="update.php">
                                        <input type="hidden" name="qStr" value="<?=$queString?>" />
                                        <input type="hidden" name="aStr" value="<?=$ansString?>" />
                                </form>
                                <div id="ReplySlipContainer"></div>
                                <script type="text/javascript" language="javascript">
                                var options = {
									"QuestionString":document.ansForm.qStr.value,
									"AnswerString":document.ansForm.aStr.value,
									"AccountBalance":<?=$account_balance?>,
									"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
									"ExternalAmountTotalFieldID":"thisAmountTotal",
									"PaymentCategoryAry":[],
									"TemplatesAry":[],
									"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber==1?1:0?>,
									"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
									"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
									<?php
									$words = $eNoticePaymentReplySlip->getRequiredWords();
									foreach($words as $key => $value){
										echo '"'.$key.'":"'.$value.'",'."\n";
									}
									?>
									"Debug":false
								};
								var sheet = new Answersheet(options);
								                                
                                function copyback()
                                {
                                	//finish();
                                    document.ansForm.aStr.value = sheet.constructAnswerString();
                                    document.form1.qStr.value = document.ansForm.qStr.value;
                                    document.form1.aStr.value = document.ansForm.aStr.value;
                                }
                                
                                $(document).ready(function(){
									sheet.renderFillInPanel('ReplySlipContainer');
                                });
                                </script>
                        	</td>
					</tr>
                    </table>
                    </td>
				</tr>
				<!-- Question Base End //-->
				<? } ?>
				<? } ?>

				<form name="form1" id="form1" action="eNoticePayment_sign_update.php" method="post" onSubmit="return checkform(this)">
        				<?php if($sys_custom['eNotice']['HideSignButton']) {
        				    // do not show sign instruction for some schools
        				} else {
        				    //if ($editAllowed && !$lnotice->ReplySlipContent)
                            if ($editAllowed && $hasContent && (!$use_payment_system || $can_sign_by_admin || $use_direct_pay_fps_qrcode) )
                            { ?>
    							<tr>
                            		<td colspan="2" align="left" class="tabletext">&nbsp;&nbsp;&nbsp;<?=($lnotice->replyStatus==2? "":$i_Notice_SignInstruction)?></td>
            					</tr>
                            <? } ?>
						<? } ?>
                        
                        
                        <?php
		                if(!$is_notice_signed && $is_payment_notice && !$sys_custom['eClassApp']['eNoticeHideSignButton'] && !$ViewOnly && $editAllowed && $use_payment_system && $can_pay_at_school && $can_sign_by_admin){
		                	echo '<tr>';
		                   	echo 	'<td class="dotline" colspan="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>';
		               		echo '</tr>';
		               		
		                	echo '<tr>';
		                		echo '<td align="left" class="tabletext" colspan="2">'.$Lang['eNotice']['PaymentNoticePaymentMethod'].'</td>';
		                	echo '</tr>';
		                	echo '<tr>';
		                		echo '<td align="left" class="tabletext" colspan="2">';
                        		if($can_pay_at_school){
                        			echo $linterface->Get_Radio_Button("NoticePaymentMethod_PayAtSchool", "NoticePaymentMethod", 'PayAtSchool', 1, "", $Lang['eNotice']['PaymentNoticePayAtSchool'], "$('#NoticePaymentMethod').val(this.value);");
                        		}
								echo '</td>';
		                	echo '</tr>';
		                }
		                ?>
                        
                        
                  		<tr>
                        	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                        </tr>
                        <tr>
                			<td align="center" colspan="2">
    			                <?php if(!$editAllowed && $lnotice->isPaymentNoticeApprovalUser() && $lnotice->RecordStatus>=4) { ?>
    								<div id="approval_div" class="approval_div">
    								
    								<h4><?=$Lang['eNotice']['ChangeApprovalStatus'] ?></h4>
    									<div id="approval_div_radio">
    										<input type="button" onclick="sendApproveAjax(1);" value="<?= $Lang['Btn']['Approve'] ?>" class="formbutton_v30 print_hide">
    										<?php if($lnotice->RecordStatus!=5){ ?>
    										<input type="button" onclick="sendApproveAjax(0);" value="<?= $Lang['Btn']['Reject'] ?>" class="formbutton_alert print_hide">
    										<?php } ?>
    									</div>
    								</div>
    							<?php } ?>
                    			
                                <?php if($show_re_sign) {
                                    echo $linterface->GET_ACTION_BTN($Lang['eNotice']['ResignNotice'], "button", "resignNotice()","resign_notice");
                                } ?>

            					<?php if($sys_custom['eNotice']['HideSignButton']) {
                            		// do not show sign button for some schools
            					} else {
                            	   if ($editAllowed && !$lnotice->ReplySlipContent && (!$use_payment_system || $can_sign_by_admin || $use_direct_pay_fps_qrcode)) {
                            			if(!$lnotice->isTargetNoticeSigned($NoticeID,$StudentID)) {
                            				echo $linterface->GET_ACTION_BTN($i_Notice_Sign, "submit", "copyback();","submit2")."&nbsp;";
                            				if(!$use_payment_system || $use_direct_pay_fps_qrcode){
                            					echo $linterface->GET_ACTION_BTN($button_save_draft, "button", "goSaveDraft(1)","save_draft");
                            				}
                            			}
                            		}
                                    
                            		if ($editAllowed && $lnotice->ReplySlipContent && (!$use_payment_system || $use_direct_pay_fps_qrcode)) {
                               			if(!$lnotice->isTargetNoticeSigned($NoticeID,$StudentID)) {
                               				echo $linterface->GET_ACTION_BTN($i_Notice_Sign, "submit");
                               				echo $linterface->GET_ACTION_BTN($button_save_draft, "button", "goSaveDraft(1)","save_draft");
                               			}
                               		}
            					} ?>
                                
                                <?php 
                                if($is_payment_notice && $is_notice_signed && $isTeacher && !$lnotice->ReplySlipContent){
                                	echo $linterface->GET_ACTION_BTN($Lang['eNotice']['ModifyReplySlipAnswer'], "submit", "copyback();","modify_answer_btn")."&nbsp;";
                                }
                                ?>
                                
                				<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print();","cancelbtn") ?>
                				<?= $edit_btn ?>
                                <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
                                <?php  if($isTeacher&&$_GET['StudentID']!=""&&$_GET['StudentID']!='undefined'){
									echo $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back()");
								}?>
                			</td>
                		</tr>
                     	
                        <input type="hidden" name="thisAmountTotal" id="thisAmountTotal" value="<? if($lnotice->TempItemAmount) echo $lnotice->TempItemAmount; else echo 0; ?>">
                        <?
                        $result = ($lpayment->checkBalance($StudentID,1));
                        $accBal = str_replace(",","",$result[0]);
                        ?>
                        <input type="hidden" name="amountBalance" id="amountBalance" value="<?=$accBal?>">
                        <input type="hidden" name="qStr" value="">
                        <input type="hidden" name="aStr" value="">
                        <input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
                        <input type="hidden" name="StudentID" value="<?=$StudentID?>">
                </form>
				</table>
			</td>
				
                </tr>
                </table>
	</td>
</tr>
</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>