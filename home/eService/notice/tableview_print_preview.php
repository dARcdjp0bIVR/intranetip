<?php
//using :
/*
 * Change Log [Start] 
 * ***************************************************************************
 *
 *  	Date    :   2020-09-07  Bill    [2020-0604-1821-16170]
 * 			        updated access right checking
 *
 *      Date    :   2020-07-31  Bill    [2020-0522-1550-16315]
 *                  show "Student Subsidy Identity Settings" col only for payment notice
 *
 *		Date	:	2020-03-12	YatWoon
 *					Add $special_feature['eNotice']['DisableSubsidyIdentityName'] flag checking [#F180463]
 *					Hide Student Subsidy Identity Name
 *
 *      Date     :  2019-12-11  Ray
 *                  Added - Student Subsidy Identity Name
 *
 * 		Date	:	2019-08-01  Carlos
 * 					For Payment Notice, added [Last modify answer person] and [Last modify answer time].
 *
 * 		Date	:	2019-06-14  Carlos
 *					Added [Pay at school] column.
 *					Added filter to filter out [Unpaid]/[Pay at school] records.
 * 		Date	:	2019-05-29  Carlos
 *					Added flag $sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat'] to do not display signed but failed to pay signer data.
 *					Deprecated the old flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] which is reverse logic of the new flag.
 *
 *  	Date	:	2019-05-17  Carlos
 *					Added flag $sys_custom['PaymentNoticeDisplayFailPaySignerInStat'] to display signed but failed to pay students.
 *
 * 		Date	:	2019-04-10	Carlos
 * 					Cater more question types for Payment Notice.
 *
 *		Date	:	2019-02-26  Carlos
 *					Cater display of signer for Payment Notice that failed to pay but signed, display as unsigned.
 *
 *      Date    :   2018-05-11  Isaac   [C139325]
 *                  Added page-break to each class name table starting from the second one to seperate each class for print
 *
 * 		Date	:	2017-11-03	Bill	[2017-1102-1332-42207]
 * 					fixed: All Students > Answer Display - Question Type: One payment from a few categories
 *
 * 		Date	:	2017-01-18 	Bill	[DM#3121]
 * 					Handle display special characters in answer
 *
 * 		Date	:	2016-03-10 [Bill]	[2016-0301-1447-14206]
 * 					for All Students, display Class Name & Class Number
 *
 * 		Date:	:	2015-11-30 [Kenneth] [2015-1016-1125-38207]
 * 					Show option name in UI and print mode ($sys_custom['eNotice']['showOptionNumber'] != true)
 *
 * 		Date	:	2015-04-14	Bill
 * 					allow eNotice PIC to access [2015-0323-1602-46073]
 *
 * 		Date	:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
 * 					Added to show auth code logic for admin view
 *
 * 		Date	:	2014-12-23 Bill
 * 					ensure when $type = 1, only Own Class data can be shown [2014-1217-1446-17207]
 *
 * 		Date	:	2014-09-30 Bill
 * 					allow access when "staffview" is turned on
 *
 *		Date	:	2014-09-08 YatWoon
 * 					display "--" instead of NULL
 *		Date	:	2014-02-26 YatWoon
 *					improved: cater 1 staff with multiple class [Case#U58796]
 *
 *		Date	:	2011-03-09	YatWoond
 *					Fixed: missing classname for "Applicable students" type
 *
 *		Date	:	2010-10-26 [YatWoon]
 *					Missing class name and class number in the print table
 *
 * 		Date	:	2010-09-16 (Ronald)
 * 		Details :	add a checking "if ($questions[$i][0]!=6)" to check if the question type is 6 or not.
 * 					if question type==6, then no need to show
 * ***************************************************************************
 * Change Log [End]
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
################################
$NoticeID = IntegerSafe($NoticeID);


$linterface = new interface_html();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);
$lclass = new libclass();
$lpayment = new libpayment();

$stype = $type;

if (!$lu->isTeacherStaff())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

// [2020-0604-1821-16170]
// [2015-0323-1602-46073] - Target Type - not Applicable students -> add checking if user is not current eNotice PIC
$isStaffView = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->payment_staffview : $lnotice->staffview;
//if ($type==1 || (!$lnotice->hasViewRight() && $lnotice->IssuerUserID != $UserID && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID)))
if ($type==1 || (!$lnotice->hasViewRight() && $lnotice->IssuerUserID != $UserID && !$isStaffView && !$lnotice->isNoticePIC($NoticeID)))
{
     $class = $lclass->returnHeadingClass($UserID,1);
     if(is_array($class))
     {
     	$class_str = "";
     	$dest = "";
     	for($i=0;$i<sizeof($class);$i++)
     	{
     		$class_str .= $dest . $class[$i][0];
     		$dest = ",";
     	}
     }
     $class = $class_str;
     
     if ($class=="")
     {
         include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
		exit;
     }
}

$is_payment_notice = strtoupper($lnotice->Module) == "PAYMENT";

$payment_status = in_array($payment_status,array('','-1','-2'))? $payment_status : '';
$only_show_unpaid = $payment_status == '-1';
$only_show_payatschool = $payment_status == '-2';

if($is_payment_notice){
	$modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'GetModifierUserInfo'=>true));
	$studentIdToModifyAnswerRecords = BuildMultiKeyAssoc($modify_answer_records,'StudentID');
}


$questions = $lnotice->splitQuestion($lnotice->Question);

$signedCount = 0;
$totalCount = 0;
$singleTable = false;

if ($lnotice->RecordType == 4 && $type != 1)
{
    $class = "";
    $all = 1;
    $data = $lnotice->returnTableViewAll();
    $singleTable = true;
}
else
{
    if ($class != "")
    {
        $data = $lnotice->returnTableViewByClass($class);
        $singleTable = true;
    }
    else $singleTable = false;
}

$class = $class=="NULL" ? "--" : $class;

if ($singleTable)
{
    $totalCount = sizeof($data);
    if (sizeof($data)!=0)
    {
         
        $x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
		$x .= "<tr>";
		
		$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_ClassName ."</td>";
		$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_ClassNumber ."</td>";
        $x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_UserStudentName ."</td>";

        // [2020-0522-1550-16315]
		//if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
        if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
		{
			$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $Lang['ePayment']['SubsidyIdentityName'] ."</td>";
		}
        for ($i=0; $i<sizeof($questions); $i++) {
        	if (!in_array($questions[$i][0],array(6,16)))
        	{
        		######################
                #	30-11-2015	Kenneth
                #	Replace Option 1, Option 2.... to question itself
                ######################
        		if($sys_custom['eNotice']['showOptionNumber']){
        			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $i_Notice_Option . ($i+1) ."</td>";
        		}
        		else{
        			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $questions[$i][1]."</td>";
        		}
        	}
        } 
        $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $i_Notice_Signer .($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerPerson'].")":"")."</td>";
        $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $i_Notice_SignedAt .($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerTime'].")":"")."</td>";
        
        if ($sys_custom['eClassApp']['authCode']) {
        	$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['SignedByAuthCode'] ."</td>";
        }
        
        if(strtoupper($lnotice->Module) == "PAYMENT")
		{
        	$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid'] ."</td>";
        	$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully'] ."</td>";
        	$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['PaymentNoticePayAtSchool'] ."</td>";
        }
        $x .= "<tr>";
        
        for ($i=0; $i<sizeof($data); $i++)
        {
			list ($sid,$name,$classNo,$type,$status,$answer,$signer,$last, $this_class, $websamsRegNo,$signerAuthCode,$studentUserLogin,$student_status,$notice_payment_method) = $data[$i];
			
			$display_signer = $signer;
			
            //$class_display = $class ? $class : $this_class;
            $class_display = $this_class;
            if ($status == 2)
            {
				$css = "attendancepresent";
                $signedCount++;
            }
            else
            {
				$css = "attendancepresent";
                $last = "$i_Notice_Unsigned";
                if($sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat']){
                	$display_signer = "";
                }
            }
			
			$display_current_row = true;
            $y = '';
			
             $y .= "<tr class=$css><td class='eSporttdborder eSportprinttext'>$class_display</td>";
			 $y .= "<td class='eSporttdborder eSportprinttext'>$classNo</td>";
             $y .= "<td class='eSporttdborder eSportprinttext'>$name</td>";

            // [2020-0522-1550-16315]
			//if(!$special_feature['eNotice']['DisableSubsidyIdentityName'])
            if($is_payment_notice && !$special_feature['eNotice']['DisableSubsidyIdentityName'])
			{
				$student_subsidy_name = '';
				$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
				if(count($student_subsidy_identity_records)>0){
					$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
					$student_subsidy_identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$student_subsidy_identity_id));
					if(count($student_subsidy_identity_records)>0) {
						$student_subsidy_name = $student_subsidy_identity_records[0]['IdentityName'];
					}
				}
				$y .= "<td class='eSporttdborder eSportprinttext'>$student_subsidy_name</td>";
			}

             for ($j=0; $j<sizeof($questions); $j++)
             {
                  //if ($questions[$j][0]!=6)
                  //$x .= "<td class='eSporttdborder eSportprinttext'>".$answer[$j]." &nbsp;</td>";
                  /*if ($questions[$j][0]==9)
            	  {
            	 		// [DM#3121] handle special characters
            	 		//$x .= "<td class='eSporttdborder eSportprinttext'>".$questions[$j][2][$answer[$j]]." &nbsp;</td>";
            	 		$x .= "<td class='eSporttdborder eSportprinttext'>".intranet_undo_htmlspecialchars($questions[$j][2][$answer[$j]])." &nbsp;</td>";
	              }
	              else */ if (!in_array($questions[$j][0],array(6,16)))
	              {
            	 		// [DM#3121] handle special characters
	            	 	//$x .= "<td class='eSporttdborder eSportprinttext'>".$answer[$j]." &nbsp;</td>";
						$y .= "<td class='eSporttdborder eSportprinttext'>".intranet_undo_htmlspecialchars($answer[$j])." &nbsp;</td>";
		 		  }
				  else
				  {
				  		// do nothing
				  }
             }
             $modify_person = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['ModifierUserName'].')' : '';
             $modify_time = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['InputDate'].')' : '';
             $y .= "<td class='eSporttdborder eSportprinttext'>$display_signer".$modify_person."&nbsp;</td><td class='eSporttdborder eSportprinttext'>$last".$modify_time."</td>";
             
             if ($sys_custom['eClassApp']['authCode']) {
             	 $signerAuthCode = ($signerAuthCode=='')? '&nbsp;' : $signerAuthCode;
             	 $y .= "<td class='eSporttdborder eSportprinttext'>$signerAuthCode</td>";
             }
             
             	if(strtoupper($lnotice->Module) == "PAYMENT")
				{
					$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
					$arrItemIDs = $lnotice->returnVector($sql);
					if(sizeof($arrItemIDs)>0){
						$targetItemID = implode(",",$arrItemIDs);
					}
					
					$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID)";
					$NoOfItemNeedToPaid = $lnotice->returnVector($sql);
					
					$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
					$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);
					
					if(sizeof($answer)>0)
					{
						if($only_show_unpaid)
	            		{ 
	            			if($NoOfItemNeedToPaid[0] > 0 && $NoOfItemPaidSuccessfully[0] < $NoOfItemNeedToPaid[0]){
	            				$display_current_row = true;
	            			}else{
	            				$display_current_row = false;
	            			}
	            		}
						
						$y .= "<td class='eSporttdborder eSportprinttext'>".$NoOfItemNeedToPaid[0]."</td>";
						$y .= "<td class='eSporttdborder eSportprinttext'>".$NoOfItemPaidSuccessfully[0]."</td>";
						$y .= "<td class='eSporttdborder eSportprinttext'>".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
					}
					else
					{
						if($only_show_unpaid){
	            			$display_current_row = false;
	            		}
						
						$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
						$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
						$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
					}
					
					if($only_show_payatschool){
	            		$display_current_row = $notice_payment_method == 'PayAtSchool';
	            	}
			}
            $y .= "</tr>\n";
             
            if($display_current_row){
            	$x .= $y;
            }
        }
        
        $x .= "</table>\n";
    }
    else
    {
        $NoMsg = ($all==1? $i_Notice_NoStudent:$i_Notice_NotForThisClass);
        $x .= "<table width='100%' border='0' cellpadding='1' cellspacing='1'>\n";
        $x .= "<tr><td align='center' class='tabletext'>$NoMsg</td></tr>\n";
        $x .= "</table>\n";
    }
}
else
{
    $classes = $lnotice->returnTableViewGroupByClass();
    for ($pos=0; $pos<sizeof($classes); $pos++)
    {
    	list($classname,$data) = $classes[$pos];
        $totalCount += sizeof($data);
        if (sizeof($data)!=0)
        {
         	$classname_display = ($classname=="NULL" || $classname=="0" || $classname=="") ? "--":$classname;
         	$printBreak = ($pos==0) ? '':'style="page-break-before:always"';
             $x .= "
             	<table width='90%' ".$printBreak." border='0' align='center' cellpadding='0' cellspacing='0'>
             	<tr>
             		<td class='eSportprinttext'>$classname_display</td>
             	</tr>
             	</table>
             	";
            
            $x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
			$x .= "<tr>";
			
			$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_ClassName ."</td>";
			$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_ClassNumber ."</td>";
            //$x .= "<tr><td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName</td>";
            $x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $i_UserStudentName ."</td>";
			$x .= "<td valign='middle' align='center' class='eSporttdborder eSportprinttabletitle'>". $Lang['ePayment']['SubsidyIdentityName'] ."</td>";
            for ($i=0; $i<sizeof($questions); $i++)
            {
             	if (!in_array($questions[$i][0],array(6,16)))
                {
             		$tooltipQ = makeTooltip($questions[$i][1]);
             		
	        		######################
	                #	30-11-2015	Kenneth
	                #	Replace Option 1, Option 2.... to question itself
	                ######################
	        		if($sys_custom['eNotice']['showOptionNumber']){
	        			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Notice_Option ".($i+1)."</td>";
	        		}
	        		else{
	        			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $questions[$i][1]."</td>";
	        		}
             	}
            }
            $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Notice_Signer".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerPerson'].")":"")."</td><td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Notice_SignedAt".($is_payment_notice?"<br />(".$Lang['eNotice']['LastModifyAnswerTime'].")":"")."</td>";
         	if(strtoupper($lnotice->Module) == "PAYMENT")
			{
    			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid'] ."</td>";
    			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully'] ."</td>";
    			$x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>". $Lang['eNotice']['PaymentNoticePayAtSchool'] ."</td>";
    		}
            $x .= "</tr>\n";
            
            for ($i=0; $i<sizeof($data); $i++)
            {
            	//list ($sid,$name,$type,$status,$answer,$signer,$last) = $data[$i];
                list ($sid,$name,$type,$status,$answer,$signer,$last,$class_num, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $data[$i];
                
                $display_signer = $signer;
                if ($status == 2)
                {
                	$css = "signed";
                    $signedCount++;
                }
                else
                {
                	$css = "unsigned";
                	if($sys_custom['PaymentNoticeDoNotDisplayFailPaySignerInStat']){
                		$display_signer = "";
                	}
                    $last = "$i_Notice_Unsigned";
                }
				
				$display_current_row = true;
                $y = '';
				
                $y .= "<tr class='$css'>";
                $y .= "<td class='eSporttdborder eSportprinttext'>$classname_display</td>";
                $y .= "<td class='eSporttdborder eSportprinttext'>$class_num</td>";
                //$x .= "<tr class='$css'><td class='eSporttdborder eSportprinttext'>$name</td>";
                $y .= "<td class='eSporttdborder eSportprinttext'>$name</td>";

				$student_subsidy_name = '';
				$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
				if(count($student_subsidy_identity_records)>0){
					$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
					$student_subsidy_identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$student_subsidy_identity_id));
					if(count($student_subsidy_identity_records)>0) {
						$student_subsidy_name = $student_subsidy_identity_records[0]['IdentityName'];
					}
				}
				$y .= "<td class='eSporttdborder eSportprinttext'>$student_subsidy_name</td>";

                for ($j=0; $j<sizeof($questions); $j++)
                {
                	// [2017-1102-1332-42207] Answer Display - Question Type: One payment from a few categories
	            	/* if ($questions[$j][0]==9)
	            	{
	            	 	$x .= "<td class='eSporttdborder eSportprinttext'>".intranet_undo_htmlspecialchars($questions[$j][2][$answer[$j]])." &nbsp;</td>";
		            }
                    else */ if (!in_array($questions[$j][0],array(6,16)))
                    {
            	 		// [DM#3121] handle special characters
                       	//$x .= "<td class='eSporttdborder eSportprinttext'>".$answer[$j]." &nbsp;</td>";
                        $y .= "<td class='eSporttdborder eSportprinttext'>".intranet_undo_htmlspecialchars($answer[$j])." &nbsp;</td>";
                    }
                }
                $modify_person = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['ModifierUserName'].')' : '';
            	$modify_time = $is_payment_notice && isset($studentIdToModifyAnswerRecords[$sid])? '<br />('.$studentIdToModifyAnswerRecords[$sid]['InputDate'].')' : '';
                $y .= "<td class='eSporttdborder eSportprinttext'>$display_signer".$modify_person."&nbsp;</td><td class='eSporttdborder eSportprinttext'>$last".$modify_time."</td>";
                	if(strtoupper($lnotice->Module) == "PAYMENT")
					{
						$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
						$arrItemIDs = $lnotice->returnVector($sql);
						if(sizeof($arrItemIDs)>0){
							$targetItemID = implode(",",$arrItemIDs);
						}
						
						$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID)";
						$NoOfItemNeedToPaid = $lnotice->returnVector($sql);
						
						$sql = "SELECT COUNT(*) FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE STUDENTID = '$sid' AND ItemID IN ($targetItemID) AND RecordStatus = 1";
						$NoOfItemPaidSuccessfully = $lnotice->returnVector($sql);
						
						if(sizeof($answer)>0)
						{
							if($only_show_unpaid)
		            		{ 
		            			if($NoOfItemNeedToPaid[0] > 0 && $NoOfItemPaidSuccessfully[0] < $NoOfItemNeedToPaid[0]){
		            				$display_current_row = true;
		            			}else{
		            				$display_current_row = false;
		            			}
		            		}
							
							$y .= "<td class='eSporttdborder eSportprinttext'>".$NoOfItemNeedToPaid[0]."</td>";
							$y .= "<td class='eSporttdborder eSportprinttext'>".$NoOfItemPaidSuccessfully[0]."</td>";
							$y .= "<td class='eSporttdborder eSportprinttext'>".($notice_payment_method=='PayAtSchool'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
						}
						else
						{
							if($only_show_unpaid){
	            				$display_current_row = false;
	            			}
							
							$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
							$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
							$y .= "<td class='eSporttdborder eSportprinttext'> - </td>";
						}
						
						if($only_show_payatschool){
	            			$display_current_row = $notice_payment_method == 'PayAtSchool';
	            		}
					}
                	$y .= "</tr>\n";
                	
                	if($display_current_row){
            			$x .= $y;
            		}
			}
            $x .= "</table>\n";
		}
        else
        {
        	$NoMsg = ($all==1? $i_Notice_NoStudent:$i_Notice_NotForThisClass);
            $x .= "<table width='100%' border='0' cellpadding='1' cellspacing='1'>\n";
            $x .= "<tr><td align='center' class='eSporttdborder eSportprinttext'>$NoMsg</td></tr>\n";
            $x .= "</table>\n";
        }
        $x .= "<br>\n";
    }
}
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ElectronicNotice2?><br /><?=$i_Notice_ViewResult?></b></td>
	</tr>
</table>        

<table width="90%" border="0" cellpaddinh="3" cellspacing="0" align="center">
<tr><td class="eSportprinttitle"><?=$i_Notice_NoticeNumber?> : <?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
<? if ($all != 1) {?>
<tr><td class="eSportprinttitle"><?=$i_ClassName?> : <?=$class?></td></tr>
<? } ?>
<tr><td class="eSportprinttitle"><?="$i_Notice_Signed/$i_Notice_Total"?> : <?="$signedCount/$totalCount"?></td></tr>
</table>

<?=$x ?>


<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
