<?php
# modifying : 

######################
#	Date:	2019-07-29 Carlos
#			Display the modify answer records at status section.
#
#	Date:	2019-05-15 Carlos
#			Improved reply slip with new js library /templates/forms/eNoticePayment_replyslip.js
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - display text content from FCKEditor
#
#	Date:	2018-01-17	Carlos	$sys_custom['ePayment']['TNG'] Hide balance.
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2014-03-03	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();
 
################################
# Pass in:
#	$NoticeID
#	$StudentID
################################
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

/*
if(strtoupper($lnotice->Module) == "PAYMENT"){
	echo "HERE";
	header("location : ePaymentNotice_view.php?NoticeID=$NoticeID&StudentID=$StudentID");
	die;
}
*/

$lu = new libuser($UserID);
$valid = true;
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
$isStudent = $lu->isStudent();

if (!isset($StudentID) || $StudentID == "")
{
     $valid = false;
}
else
{
    if ($isStudent)
    {
        $StudentID = $UserID;
    }
    else if ($isParent)
    {
         $children = $lu->getChildren();
         if (!in_array($StudentID,$children))
         {
              $valid = false;
         }
    }
    else if ($isTeacher)
    {
         $valid = $lnotice->hasViewRightForStudent($StudentID);
    }
}

############################################################
#	Check student belongs to this notice?
############################################################
if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
{
	$valid = false;
}


if (!$lnotice->showAllEnabled && !$valid)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;

if ($valid)
{
    $lnotice->retrieveReply($StudentID);
    $ansString = $lnotice->answer;
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
if($lnotice->ModuleID=='')
{
	$attachment = $lnotice->displayAttachment();
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT")
		$attachment = $lnotice->displayAttachment();
	else
		$attachment = $lnotice->displayModuleAttachment();
}

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE=Javascript>
function updateAmountTotal(id, amt, type) {
	if(type!=3) { // not checkbox option 
		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt) - parseInt(document.getElementById('thisItemAmount'+id).value);
		document.getElementById('thisItemAmount'+id).value = amt;
	} else {
		document.getElementById('thisAmountTotal').value = parseInt(document.getElementById('thisAmountTotal').value) + parseInt(amt);
		document.getElementById('thisItemAmount'+id).value = parseInt(document.getElementById('thisItemAmount'+id).value) + parseInt(eval(amt));
	}
	//document.getElementById('spanTemp').innerHTML = document.getElementById('thisAmountTotal').value;
}
</SCRIPT>

<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_DateStart?></span></td>
					<td class='tabletext'><?=$lnotice->DateStart?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_DateEnd?></span></td>
					<td class='tabletext'><?=$lnotice->DateEnd?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Title?></span></td>
					<td class='tabletext'><?=$lnotice->Title?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Description?></span></td>
					<? if(strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApplyEditor) { ?>
                		<td class='tabletext'><?=htmlspecialchars_decode($lnotice->getNoticeContent($StudentID))?></td>
            		<? } else { ?>
                		<td class='tabletext'><?=nl2br(intranet_convertAllLinks($lnotice->Description))?></td>
            		<? } ?>
				</tr>
                                
                                <? if ($attachment != "") { ?>
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Attachment?></span></td>
					<td class='tabletext'><?=$attachment?></td>
				</tr>
                                <? } ?>
                                
                                <? if ($isTeacher) {
        				$issuer = $lnotice->returnIssuerName();
        			?>
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Issuer?></span></td>
					<td class='tabletext'><?=$issuer?></td>
				</tr>
                        	<? } ?>
                        	
			 <?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Profile_PersonInCharge?></span></td>
					<td class='tabletext'><?=$pics?></td>
				</tr>
			<?php } ?>
				
                                <?
                                if ($valid)
                                {
                                        if ($lnotice->replyStatus != 2)
                                        {
                                            $statusString = "$i_Notice_Unsigned";
                                        }
                                        else
                                        {
                                            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
                                            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
                                            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
                                        }
                                        
                                        $modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'StudentID'=>$StudentID,'GetModifierUserInfo'=>true));
                                        if(count($modify_answer_records) > 0){
	                                        foreach($modify_answer_records as $modify_answer_record){
												$statusString .= "<br />".str_replace(array('<!--USER_NAME-->','<!--DATETIME-->'),array($modify_answer_record['ModifierUserName'],$modify_answer_record['InputDate']),$Lang['eNotice']['AnswerModifiedByAt']);
											}
                                        }
                                ?>
                                	<tr valign='top'>
        					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_SignStatus?></span></td>
        					<td class='tabletext'><?=$statusString?></td>
					</tr>
                                	<tr valign='top'>
        					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_UserStudentName?></span></td>
        					<td class='tabletext'><?=$lu->getNameWithClassNumber($StudentID)?></td>
					</tr>
                                <? } ?>
                                
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_RecipientType?></span></td>
					<td class='tabletext'><?=$targetType[$lnotice->RecordType]?></td>
				</tr>
				</table>
                        </td>
		</tr>
                </table>
	<td>
</tr>

<tr>
	<td colspan="2">      
					<!-- Break -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
                                        </tr>
                                        </table>
	</td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />                                
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eNoticereplytitle' align='center'><?=$i_Notice_ReplySlip?></td>
				</tr>
				
				<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						//$queString = $lform->getConvertedString($lnotice->Question);
						
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString =trim($queString)." ";
						
						
						$result = $lpayment->checkBalance($StudentID,1);
						$finalBalance = $lpayment->getExportAmountFormat(str_replace(",","",$result[0]));
						
						$student_subsidy_identity_id = '';
						if($StudentID != '' && $StudentID > 0)
						{
							$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'DateWithinEffectiveDates'=>$lnotice->DateStart));
							if(count($student_subsidy_identity_records)>0){
								$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
							}
						}
					?>          
                                <tr>
                                	<td colspan="2">
                                        <? if ($lnotice->replyStatus == 2) { ?>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                  <tr>
        											<td align="center" class="tabletext"><?=$i_Notice_ReplySlip_Signed?></td>
                                                  </tr>
                                                  <tr>
                                                  	<td align="left">
                                                
                                                		<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
														<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <div id="ReplySlipContainer"></div>
                                                        <script type="text/javascript" language="Javascript">                                                       
                                                        $(document).ready(function(){
		                                                var options = {
															"QuestionString":"<?=$queString?>",
															"AnswerString":"<?=$ansString?>",
															"AccountBalance":<?=$finalBalance?>,
															"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
															"PaymentCategoryAry":[],
															"TemplatesAry":[],
															"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber?1:0?>,
															"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
															"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
															<?php
															$words = $eNoticePaymentReplySlip->getRequiredWords();
															foreach($words as $key => $value){
																echo '"'.$key.'":"'.$value.'",'."\n";
															}
															?>
															"Debug":false
														};
														var sheet = new Answersheet(options);
														sheet.renderPreviewPanel('ReplySlipContainer');
		                                                });  
                                                        </SCRIPT>
                                                	</td>
        										  </tr>
                                                </table>1
                                	<? } else { ?>
                                        
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tr>
                                                	<td align=left>
                                                
                                                        <script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
														<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
                                                        
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <div id="ReplySlipContainer"></div>
                                                        <script type="text/javascript" language="Javascript">
                                                        $(document).ready(function(){
                                                       		var options = {
																"QuestionString":"<?=$queString?>",
																"AnswerString":"<?=$ansString?>",
																"AccountBalance":<?=$finalBalance?>,
																"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
																"PaymentCategoryAry":[],
																"TemplatesAry":[],
																"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber?1:0?>,
																"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
																"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
																<?php
																$words = $eNoticePaymentReplySlip->getRequiredWords();
																foreach($words as $key => $value){
																	echo '"'.$key.'":"'.$value.'",'."\n";
																}
																?>
																"Debug":false
															};
															var sheet = new Answersheet(options);
															sheet.renderFillInPanel('ReplySlipContainer');
						                                });
                                                        </script>
                                                	</td>
                                                </tr>
                                                </table>
                                        <? } ?>
                                        </td>
                                </tr>

				<tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <form name="form1" action="eNoticePayment_view_print_preview.php" method="post" target="_blank"> 
                                <tr>
                			<td align="center" colspan="2">
                				<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.form1.submit()","cancelbtn") ?>
								<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
                			</td>
                		</tr>
                				<input type="hidden" name="thisAmountTotal" id="thisAmountTotal" value="0">
                                <input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
                                <input type="hidden" name="StudentID" value="<?=$StudentID?>">
                                </form>
                    <? } ?>
				</table>
				
			</td>
                </tr>
                </table>
	</td>
</tr>

</table>



<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>