<?php
# using: 

######################################### !!! IMPORTANT !!! #############################################
###																									  ###
###   		     if upload this file to client site before ip.2.5.7.7.1, please find Bill first.	  ###
###																									  ###
######################################### !!! IMPORTANT !!! #############################################

#####################
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table and reduce the width of the title td 
#
#	Date: 	2016-07-15	Bill
#			remove syntax from UI
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-11-16	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#####################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

##################################
# Pass in variable: 
#	$NoticeID
#	$StudentID
##################################
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);


$linterface = new interface_html();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);
$valid = true;
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
$isStudent = $lu->isStudent();
if (!isset($StudentID) || $StudentID == "")
{
     $valid = false;
}
else
{
    if ($isStudent)
    {
        $StudentID = $UserID;
    }
    else if ($isParent)
    {
         $children = $lu->getChildren();
         if (!in_array($StudentID,$children))
         {
              $valid = false;
         }
    }
    else if ($isTeacher)
    {
         $valid = $lnotice->hasViewRightForStudent($StudentID);
    }
}

if (!$lnotice->showAllEnabled && !$valid)
{
     header("Location: ../school/close.php");
}

if ($valid)
{
    $lnotice->retrieveReply($StudentID);
     $ansString = str_replace("\r\n", "<br>",$lnotice->answer);
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();


?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ElectronicNotice2?></b></td>
	</tr>
</table>        

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_DateStart?> : <?=$lnotice->DateStart?></td></tr>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_DateEnd?> : <?=$lnotice->DateEnd?></td></tr>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
<!--<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_Description?> : <?="";//intranet_undo_htmlspecialchars($lnotice->Description)?></td></tr>-->
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_Description?> : <?=intranet_undo_htmlspecialchars($lnotice->getNoticeContent($StudentID))?></td></tr>
<? if ($isTeacher) {
	$issuer = $lnotice->returnIssuerName();
?>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_Issuer?> : <?=$issuer?></td></tr>
<? } ?>
<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Profile_PersonInCharge?> : <?=$pics?></td></tr>
<?php } ?>
<?
if ($valid)
{
	if ($lnotice->replyStatus != 2)
	{
		$statusString = "$i_Notice_Unsigned";
	}
	else
	{
		$signer = $lu->getNameWithClassNumber($lnotice->signerID);
		$signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
		$statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
	}
?>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_SignStatus?> : <?=$statusString?></td></tr>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_UserStudentName?> : <?=$lu->getNameWithClassNumber($StudentID)?></td></tr>
<? } ?>
<tr><td class="eSportprinttitle" style="width:15%"><?=$i_Notice_RecipientType?> : <?=$targetType[$lnotice->RecordType]?></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
</tr>
</table>

<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eSportprinttitle' align='center'><b><?=$Lang['eNotice']['ReplySlipForm']?></b></td>
				</tr>
                  
					<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$queString = $lform->getConvertedString($lnotice->Question);
					?>              
                                <tr valign='top'>
					<td colspan="2">
                                        <? if ($lnotice->replyStatus == 2) { ?>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                  <tr>
        						<td align="center" class="tabletext"><?=$i_Notice_ReplySlip_Signed?></td>
                                                  </tr>
                                                  <tr>
                                                  	<td align="left">
                                                
                                                	<script language="javascript" src="/templates/forms/form_view.js"></script>
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        
                                                        <script language="Javascript">
                                                        var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                        myQue="<?=$queString?>";
                                                       	myQue="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                        myAns="<?=$ansString?>";
                                                        Part3 = '';
                                                        document.write(viewForm(myQue, myAns));
                                                        </SCRIPT>
                                                	</td>
        					</tr>
                                                </table>
                                	<? } else { ?>
                                        
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tr>
                                                	<td align=left>
                                                
                                                        <script language="javascript" src="/templates/forms/layer.js"></script>
                                                        <script language="javascript" src="/templates/forms/form_edit.js"></script>
                                                        
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <script language="Javascript">
                                                        
                                                        <?=$lform->getWordsInJS()?>
                                                        var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';
                                                        var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
                                                        var sheet= new Answersheet();
                                                        // attention: MUST replace '"' to '&quot;'
                                                        sheet.qString="<?=$queString?>";
                                                       	sheet.qString="<?= preg_replace("/#TAR#(([A-Z]|[a-z]|[0-9])+)#/", "", $queString) ?>";
                                                        sheet.aString="<?=$ansString?>";
                                                        //edit submitted application
                                                        sheet.mode=1;
                                                        sheet.answer=sheet.sheetArr();
                                                        Part3 = '';
                                                        document.write(editPanel());
                                                        </script>
                                                	</td>
                                                </tr>
                                                </table>
                                        <? } ?>
                                        </td>
				</tr>
				<? } ?>
				</table>
			</td>
                </tr>
                </table>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();

?>
