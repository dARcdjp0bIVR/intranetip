<?php
# modifying : 

######################
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#	Date:	2019-07-29 Carlos
#			Display the modify answer records at status section.
#
#	Date:	2019-05-15 Carlos
#			Improved reply slip with new js library /templates/forms/eNoticePayment_replyslip.js
#
#   Date:   2019-01-25  (Bill)  [2019-0118-1125-41206]
#           Improved: Description - display text content from FCKEditor
#
#   Date:   2018-06-21 Isaac
#           expended width of the notice table and reduce the width of the title td
#
#   Date:	2017-02-01 (Isaac)
#           Added attachment field
#
# 	Date:	2018-02-01 (Isaac)
#			Fixed ui consistancy.
#			Added noitice number field.
#
#	Date:	2018-01-17	Carlos	$sys_custom['ePayment']['TNG'] Hide balance.
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2015-04-15	Bill
#			Improved: show details to eNotice PIC [2015-0323-1602-46073]
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#	$StudentID
################################
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);


$linterface = new interface_html();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$valid = true;
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
$isStudent = $lu->isStudent();
if (!isset($StudentID) || $StudentID == "")
{
     $valid = false;
}
else
{
    if ($isStudent)
    {
        $StudentID = $UserID;
    }
    else if ($isParent)
    {
         $children = $lu->getChildren();
         if (!in_array($StudentID,$children))
         {
              $valid = false;
         }
    }
    else if ($isTeacher)
    {
    	 // [2015-0323-1602-46073] show details to eNotice PIC
        //$valid = $lnotice->hasViewRightForStudent($StudentID) || $lnotice->staffview || $lnotice->isNoticePIC($NoticeID);
        $valid = $lnotice->hasViewRightForStudent($StudentID) || $lnotice->payment_staffview || $lnotice->isNoticePIC($NoticeID);
    }
}

if (!$lnotice->showAllEnabled && !$valid)
{
	/*
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
	*/
	if($StudentID == ''){
		$StudentID = 0;
	}
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;

if ($valid)
{
    $lnotice->retrieveReply($StudentID);
    $ansString = $lnotice->answer;
    $lnotice->retrieveAmount($StudentID);
	//$lnotice->retrieveAmountWithNonPayItem($StudentID); 
	if($lnotice->countAmount != "")
		$countAmt = implode(',',$lnotice->countAmount);
	if($lnotice->AmountValue != "")	
		$amountValue = implode(',',$lnotice->AmountValue);
		
}

//echo $countAmt.'/'.$amountValue;

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();


$result = $lpayment->checkBalance($StudentID,1);
$finalBalance = $lpayment->getExportAmountFormat(str_replace(",","",$result[0]));

$student_subsidy_identity_id = '';
if($StudentID != '' && $StudentID > 0)
{
	$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'DateWithinEffectiveDates'=>$lnotice->DateStart));
	if(count($student_subsidy_identity_records)>0){
		$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
	}
}

?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<!-- <table width='100%' align='center' border=0> 
	<tr> 
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ElectronicNotice2?></b></td>
 	</tr> 
 </table>   -->     

<table class="form_table_v30"  align="center" style="margin:auto auto; width:100%">
<tr><td class="field_title" style="width:15%"><?=$i_Notice_DateStart?> </td><td><?=$lnotice->DateStart?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_DateEnd?></td><td><?=$lnotice->DateEnd?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_NoticeNumber?></td><td><?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_Title?></td><td><?=$lnotice->Title?></td></tr>
<? if(strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApplyEditor) { ?>
	<tr><td class="field_title" style="width:15%"><?=$i_Notice_Description?></td><td><?=htmlspecialchars_decode($lnotice->getNoticeContent($StudentID))?></td></tr>
<? } else { ?>
	<tr><td class="field_title" style="width:15%"><?=$i_Notice_Description?></td><td><?=nl2br(intranet_convertAllLinks($lnotice->Description))?></td></tr>
<? } ?>
<? if(strtoupper($lnotice->Module) == "PAYMENT"){ ?>
<tr><td class="field_title" style="width:15%"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></td><td><? echo ($lnotice->DebitMethod == 1) ? $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly'] : $Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLater'];?></td></tr>
<? } ?>
<? if ($attachment != "") { ?>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_Attachment?></td><td  ><?=$attachment?></td></tr>
<? } ?>
<? if ($isTeacher) {
	$issuer = $lnotice->returnIssuerName();
?>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_Issuer?></td><td><?=$issuer?></td></tr>
<? } ?>
<?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher){ ?>
<tr><td class="field_title" style="width:15%"><?=$i_Profile_PersonInCharge?></td><td><?=$pics?></td></tr>
<?php } ?>
<?
if ($valid)
{
	if ($lnotice->replyStatus != 2)
	{
		$statusString = "$i_Notice_Unsigned";
	}
	else
	{
		$signer = $lu->getNameWithClassNumber($lnotice->signerID);
		$signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
		$statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
	}
	
	$modify_answer_records = $lnotice->getReplySlipModifyAnswerRecords(array('NoticeID'=>$NoticeID,'StudentID'=>$StudentID,'GetModifierUserInfo'=>true));
	if(count($modify_answer_records) > 0){
	    foreach($modify_answer_records as $modify_answer_record){
			$statusString .= "<br />".str_replace(array('<!--USER_NAME-->','<!--DATETIME-->'),array($modify_answer_record['ModifierUserName'],$modify_answer_record['InputDate']),$Lang['eNotice']['AnswerModifiedByAt']);
		}
	}
?>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_SignStatus?></td><td><?=$statusString?></td></tr>
<tr><td class="field_title" style="width:15%"><?=$i_UserStudentName?></td><td><?=$lu->getNameWithClassNumber($StudentID)?></td></tr>
<? } ?>
<tr><td class="field_title" style="width:15%"><?=$i_Notice_RecipientType?></td><td><?=$targetType[$lnotice->RecordType]?></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align='center'><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/print_scissors.gif"></td>
</tr>
</table>

<table align="center" width="90%" border="0" cellpadding="0" cellspacing="0">
                                <!-- Relpy Slip -->
                                <tr valign='top'>
					<td colspan="2" class='eSportprinttitle' align='center'><b><?=$i_Notice_ReplySlip?></b></td>
				</tr>
                  
					<? if($lnotice->ReplySlipContent) {?>
					<!-- Content Base Start //-->
						<tr valign='top'>
							<td colspan="2">
							<?=$lnotice->ReplySlipContent?>
							</td>
						</tr>
					<? } else { 
						$queString = $lform->getConvertedString($lnotice->Question);
						$queString = str_replace("&lt;br&gt;","<br>",$queString);
					?>              
                                <tr valign='top'>
					<td colspan="2">
                                        <? if ($lnotice->replyStatus == 2) { ?>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                  <tr>
        						<td align="center" class="tabletext"><?=$i_Notice_ReplySlip_Signed?></td>
                                                  </tr>
                                                  <tr>
                                                  	<td align="left">
                                                
                                                		<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
														<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <div id="ReplySlipContainer"></div>
                                                        <script type="text/javascript" language="Javascript">                                                       
                                                        $(document).ready(function(){
		                                                var options = {
															"QuestionString":"<?=$queString?>",
															"AnswerString":"<?=$ansString?>",
															"AccountBalance":<?=$finalBalance?>,
															"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
															"PaymentCategoryAry":[],
															"TemplatesAry":[],
															"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber?1:0?>,
															"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
															"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
															<?php
															$words = $eNoticePaymentReplySlip->getRequiredWords();
															foreach($words as $key => $value){
																echo '"'.$key.'":"'.$value.'",'."\n";
															}
															?>
															"Debug":false
														};
														var sheet = new Answersheet(options);
														sheet.renderPreviewPanel('ReplySlipContainer');
		                                                });  
                                                        </SCRIPT>
                                                	</td>
        					</tr>
                                                </table>
                                	<? } else { ?>
                                        
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tr>
                                                	<td align=left>
                                                
                                                        <script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
														<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
                                                        
                                                        <form name="ansForm" method="post" action="update.php">
                                                                <input type=hidden name="qStr" value="">
                                                                <input type=hidden name="aStr" value="">
                                                        </form>
                                                        <div id="ReplySlipContainer"></div>
                                                        <script type="text/javascript" language="Javascript">
                                                        $(document).ready(function(){
                                                       		var options = {
																"QuestionString":"<?=$queString?>",
																"AnswerString":"<?=$ansString?>",
																"AccountBalance":<?=$finalBalance?>,
																"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
																"PaymentCategoryAry":[],
																"TemplatesAry":[],
																"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber?1:0?>,
																"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
																"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
																<?php
																$words = $eNoticePaymentReplySlip->getRequiredWords();
																foreach($words as $key => $value){
																	echo '"'.$key.'":"'.$value.'",'."\n";
																}
																?>
																"Debug":false
															};
															var sheet = new Answersheet(options);
															sheet.renderFillInPanel('ReplySlipContainer');
						                                });
                                                        </script>
                                                	</td>
                                                </tr>
                                                </table>
                                        <? } ?>
                                        </td>
				</tr>
				<? } ?>
				</table>
			</td>
                </tr>
                </table>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();

?>