<?php
// Modifying by: yat

######## Change Log [Start] ################
#
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();
$linterface         = new interface_html();

if ($lnotice->disabled || !$lu->isTeacherStaff() || !$lnotice->isDisciplineNoticeGroup())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$thisModule = strtoupper($ldiscipline->Module);
$keyword = trim($keyword);

$x = "";

if($sign_status!=-1 && $sign_status!="")
{
	$sign_status_cond = " and " . ($sign_status ? "b.RecordType is not null" : "b.RecordType is NULL");
}
if($print_status!=-1 && $print_status!="")
{
	$print_status_cond = " and " . ($print_status ? "b.PrintDate is not null" : "b.PrintDate is NULL");
}
$title = $title ? $title : -1;
if($title!=-1)
{
	$title_cond = " and a.Title='". $title ."'";
}

if ($status != "")
	$recordstatus_cond = " and a.RecordStatus=$status";

$name_field = getNameFieldWithClassNumberByLang("c.");
$signer_name_field = getNameFieldWithClassNumberByLang("s.");	
$viewIcon = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_preview\" border=0>";                     
$sql = "select 
		left(a.DateStart,10),
		$name_field as studentname,
		a.Title,
		if(b.RecordType is not null, '$i_Notice_Signed','$i_Notice_Unsigned'),
		$signer_name_field as signer_name,
		if(b.DateModified=b.DateInput,'',b.DateModified),
		if(b.PrintDate is not null, b.PrintDate, '". $Lang['eNotice']['NonPrint'] ."'),
		a.NoticeID
		from 
			INTRANET_NOTICE as a 
			left join INTRANET_NOTICE_REPLY as b on (b.NoticeID = a.NoticeID)
			left join INTRANET_USER as c on (concat('U',c.UserID) = a.RecipientID)
			left join INTRANET_USER as s on (s.UserID = b.SignerID)
		where
			a.DateStart >= '$StartDate' and a.DateStart <= '$EndDate'
			$recordstatus_cond
			and a.Module = '$thisModule'
			$sign_status_cond
			$print_status_cond
			$title_cond
			and (studentname like '%$keyword%' or a.Title like '%$keyword%' or c.EnglishName like '%$keyword%' or c.ChineseName like '%$keyword%')
		order by a.DateStart desc
		";
$result = $ldiscipline->returnArray($sql);

if(!empty($result))
{
	foreach($result as $k=>$d)
	{
		$x.="<tr>";
		for($i=0;$i<=6;$i++)
		{
			$x .= "<td>". $d[$i] ."&nbsp;</td>";
		}
		$x.="</tr>";
	}
}
else
{
	$x.="<tr>";
	$x .= "<td colspan='7' align='center'>". $Lang['General']['NoRecordFound'] ."</td>";
	$x.="</tr>";
}

intranet_closedb();
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<div class="table_board">
<table class="common_table_list">
<tr>
	<th><?=$i_Notice_DateStart?></th>
	<th><?=$i_identity_student?></th>
	<th><?=$i_Notice_Title?></th>
	<th><?=$i_Notice_Signed."/".$i_Notice_Unsigned?></th>
	<th><?=$i_Notice_Signer?></th>
	<th><?=$i_Notice_SignedAt?></th>
	<th><?=$Lang['eNotice']['PrintDate']?></th>
</tr>

<?=$x?>

</table>
</div>