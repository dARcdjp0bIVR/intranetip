<?php
// Modifying by: Bill

######## Change Log [Start] ################
#
# - 2016-09-08 (Bill)
#	fixed: Support print notice when target date range within 1 day
#
# -	2014-06-23 [YatWoon]
#	fixed: Unsigned record display signed date, due to the 2 records data diff is 1s only. [Case#J62964]
#
# - 2014-04-16 [YatWoon]
#	improved: Class teacher can access eService > eNotice > Discipline Notice if setting (ClassTeacherCanAccessDisciplineNotice) is on
#
# - 2013-02-05 [YatWoon]
#	improved: staff can sign the discipline notice [#2013-0103-0958-44148]
#			  export / print list (flag control)
#
# - 2009-12-23 [Henry Chow]
#	modified fotoer option
#
# - 2009-12-23 [YatWoon]
#	Add Title filter
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lu = new libuser($UserID);
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();

$linterface         = new interface_html();
$CurrentPageArr['eServiceNotice'] = 1;
$CurrentPage = "PageNotice_Discipline";

//if ($lnotice->disabled || !$lu->isTeacherStaff() || !$lnotice->isDisciplineNoticeGroup())
if ($lnotice->disabled || !$lu->isTeacherStaff() || 
	!($lnotice->isDisciplineNoticeGroup() || ($lnotice->ClassTeacherCanAccessDisciplineNotice && $_SESSION['USER_BASIC_INFO']['is_class_teacher']))
	)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$thisModule = strtoupper($ldiscipline->Module);
$status = $status ? $status : 1;
//$sign_status = $sign_status ? $sign_status : 0;
$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateStart");
$keyword = trim($keyword);

$x = "";

################################################################################################################################
# If user is not in batch group, only is a class teacher > then only display his class student records
################################################################################################################################
if(!$lnotice->isDisciplineNoticeGroup())
{
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	$lclass = new libclass();
	
	$class_ary = $lclass->returnHeadingClass($UserID, 1);
	$class_list = "";
	if(empty($class_ary))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;	
	}
	else
	{
		$sep = "";
		foreach($class_ary as $k=>$d)
		{
			$class_list .= $sep . "'".$d['ClassTitleEN'] . "'";
			$sep = ",";
		}
		
		$class_cond = " and c.ClassName in (". $class_list .")";
	}
}


if($sign_status!=-1 && $sign_status!="")
{
	$sign_status_cond = " and " . ($sign_status ? "b.RecordType is not null" : "b.RecordType is NULL");
}
if($print_status!=-1 && $print_status!="")
{
	$print_status_cond = " and " . ($print_status ? "b.PrintDate is not null" : "b.PrintDate is NULL");
}
$title = $title ? $title : -1;
if($title!=-1)
{
	$title_cond = " and a.Title='". $title ."'";
}

if ($status != "")
	$recordstatus_cond = " and a.RecordStatus=$status";

$name_field = getNameFieldWithClassNumberByLang("c.");
$signer_name_field = getNameFieldWithClassNumberByLang("s.");	
$viewIcon = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_preview\" border=0>";                     
$sql = "select 
		left(a.DateStart,10),
		$name_field as studentname,
		concat('<a href=\"javascript:ViewNoticePrintPreview(', a.NoticeID ,');\">". $viewIcon ."</a> ',a.Title),
		if(b.RecordType is not null, 
			concat('<a href=\"javascript:viewReply(', a.NoticeID ,',\'', c.UserID  ,'\');\">$i_Notice_Signed</a> '),
			concat('<a href=\"javascript:sign(', a.NoticeID ,',\'', c.UserID  ,'\');\">$i_Notice_Unsigned</a> ')
			),
		$signer_name_field as signer_name,
		if(b.DateModified=b.DateInput,'',b.DateModified),
		if(b.PrintDate is not null, b.PrintDate, '". $Lang['eNotice']['NonPrint'] ."'),
		a.NoticeID
		from 
			INTRANET_NOTICE as a 
			left join INTRANET_NOTICE_REPLY as b on (b.NoticeID = a.NoticeID)
			left join INTRANET_USER as c on (concat('U',c.UserID) = a.RecipientID)
			left join INTRANET_USER as s on (s.UserID = b.SignerID)
		where
			a.DateStart >= '$StartDate 00:00:00' and a.DateStart <= '$EndDate 23:59:59' 
			$recordstatus_cond
			and a.Module = '$thisModule'
			$sign_status_cond
			$print_status_cond
			$title_cond
			and (studentname like '%$keyword%' or a.Title like '%$keyword%' or c.EnglishName like '%$keyword%' or c.ChineseName like '%$keyword%')
		";
		$sql .= $class_cond;
$li->sql = $sql;

$li->no_col = 8;
$li->IsColOff = "eNoticedisplayNoticePrint";

// TABLE COLUMN
$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
$li->column_list .= "<th>".$i_identity_student."</th>\n";
$li->column_list .= "<th>".$i_Notice_Title."</th>\n";
$li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Unsigned."</th>\n";
$li->column_list .= "<th>".$i_Notice_Signer."</th>\n";
$li->column_list .= "<th>".$i_Notice_SignedAt."</th>\n";
$li->column_list .= "<th>".$Lang['eNotice']['PrintDate']."</th>\n";
$li->column_list .= "<th width=1>".$li->check("NoticeIDArr[]")."</th>\n";

### Button
$PrintBtn_All		= "<a href=\"javascript:printAll()\" class='print'>" . $button_print_all . "</a>";
$PrintBtn_Selected	= "<a href=\"javascript:printSelected()\" class='print'> " . $button_print_selected . "</a>";

if($special_feature['eDiscipline']['ExportPrintNotice']) 
{
	$export_btn			= $linterface->GET_LNK_EXPORT("javascript:doExport()",$btn_export,"","","",0);
	$print_list_btn		= $linterface->GET_LNK_PRINT("javascript:printList()",$Lang['General']['PrintList'],"","","",0);
}

### Search
// $searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
// $searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
// $searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
// $searchTag 	.= "</td></table>";
$searchTag = "<input name=\"keyword\" id=\"searchText\" type=\"text\" value=\"$searchText\" onkeyup=\"Check_Go_Search(event);\"/>";

### Filter - Status
$status_select = "<SELECT name='status' onChange=this.form.submit()>\n";
$status_select.= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_Notice_StatusPublished</OPTION>\n";
$status_select.= "<OPTION value=2 ".($status==2? "SELECTED":"").">$i_Notice_StatusSuspended</OPTION>\n";
$status_select.= "</SELECT>&nbsp;\n";

### Title ###
$TAGS_OBJ[] = array($eNotice['DisciplineNotice'],"");
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

### Filter - Signed / Unsign
$sign_status_select = "<SELECT name='sign_status' onChange=this.form.submit()>\n";
$sign_status_select.= "<OPTION value=-1 ".(strlen($sign_status)==0 || $sign_status==-1? "SELECTED":"").">". $Lang['eNotice']['AllSignStatus'] ."</OPTION>\n";
$sign_status_select.= "<OPTION value=0 ".(strlen($sign_status) && $sign_status==0? "SELECTED":"").">$i_Notice_Unsigned</OPTION>\n";
$sign_status_select.= "<OPTION value=1 ".($sign_status==1? "SELECTED":"").">$i_Notice_Signed</OPTION>\n";
$sign_status_select.= "</SELECT>&nbsp;\n";

### Filter - Print / non-print
$print_status_select = "<SELECT name='print_status' onChange=this.form.submit()>\n";
$print_status_select.= "<OPTION value=-1 ".(strlen($print_status)==0 || $print_status==-1? "SELECTED":"").">". $Lang['eNotice']['AllPrintStatus'] ."</OPTION>\n";
$print_status_select.= "<OPTION value=0 ".(strlen($print_status) && $print_status==0? "SELECTED":"").">". $Lang['eNotice']['NonPrint'] ."</OPTION>\n";
$print_status_select.= "<OPTION value=1 ".($print_status==1? "SELECTED":"").">". $Lang['eNotice']['Printed'] ."</OPTION>\n";
$print_status_select.= "</SELECT>&nbsp;\n";

### Filter - Title
$sql = "select distinct(Title) from INTRANET_NOTICE where Module='". $thisModule ."' order by Title";
$result = $li->returnArray($sql);
$title_select = "<SELECT name='title' onChange=this.form.submit()>\n";
$title_select .= "<option value='-1'>". $Lang['eNotice']['AllNoticeTitle'] ."</option>";
foreach($result as $k=>$d)
{
	if(trim($d['Title'])=="")	continue;
 	$title_select .= "<option value='". $d['Title'] ."' ". ($title==$d['Title'] ? "selected":"") .">". $d['Title'] ."</option>";
}
$title_select .= "</select>";

### Filter - Date Range
$date_select = $eNotice['Period_Start'] .": ";
//$date_select .= $linterface->GET_DATE_FIELD2("subStartSpan", "form1", "StartDate", $StartDate, 1, "StartDate") . "&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("StartDate", $StartDate) . "&nbsp;";
$date_select .= $eNotice['Period_End']."&nbsp;";
// $date_select .= $linterface->GET_DATE_FIELD2("subEndSpan", "form1", "EndDate", $EndDate, 1, "EndDate");
$date_select .= $linterface->GET_DATE_PICKER("EndDate", $EndDate);
$date_select .= $linterface->GET_BTN($button_submit, "submit");

if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) {
	/*
	for($i=0; $i<sizeof($Lang['eDiscipline']['NoticeFooterArray']); $i++) {
		$optionField .= '<tr><td class="'.(($updateOption==2) ? "" : "tabletextremark").'"><input type="checkbox" '.(($updateOption==2) ? "" : "disabled").' name="option[]" id="option'.($i+1).'" value="'.($i+1).'" '.(sizeof($option)>0 && in_array(($i+1), $option) ? "checked" : "").'><label for="option'.($i+1).'">'.$Lang['eDiscipline']['NoticeFooterArray'][$i+1][0].'</td></tr>';
	}
	*/
	foreach($Lang['eDiscipline']['NoticeFooterArray'] as $id=>$name) {
			$optionField .= '<tr><td class="'.(($updateOption==2) ? "" : "tabletextremark").'"><input type="checkbox" '.(($updateOption==2) ? "" : "disabled").' name="option[]" id="option'.($id).'" value="'.($id).'" '.(sizeof($option)>0 && in_array(($id), $option) ? "checked" : "").'><label for="option'.($id).'">'.$name[0]." / ".str_replace("<br>","",$name[1]).'</td></tr>';
		}
			
	$noticeSignature = '
		<a href="javascript:;" class="tablelink" onclick="showHideLayer(\'DisplayOptionDiv\',\'\');">'.$Lang['eDiscipline']['ManageNoticeSignature'].'</a><br>
				<div id="DisplayOptionTableDivTableDiv" style="float:right;position:relative;left:-285px;top:2px;z-index:20;">
				<table id="DisplayOptionDivTable">
				<tr>
				<td>
				<div id="DisplayOptionDiv" class="selectbox_layer" style="float:right;width:280px;left:100">
				<table id="FilterSelectionTable" width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
				<td style="text-align:right">
				<a href="javascript:;" class="tablelink" onclick="showHideLayer(\'DisplayOptionDiv\',0);"> '.$eComm['Close'].'</a>
				</td>
				</tr>
				<tr>
				<td style="text-align:left">
				<input type="radio" value="1" id="updateOption1" name="updateOption" class="DisplayOptionChkBox" onclick="changeEnable(0)" '.(($updateOption==1 || $updateOption=="") ? "checked" : "").' /><label for="updateOption1"> '.$Lang['eDiscipline']['RemainSignatureOption'].'</label></td>
				</tr>
				<tr>
				<td style="text-align:left">
				<input type="radio" value="2" id="updateOption2" name="updateOption" class="DisplayOptionChkBox" onclick="changeEnable(1)" '.(($updateOption==2) ? "checked" : "").' /><label for="updateOption2"> '.$Lang['eDiscipline']['UpdateSignatureOption'].'</label></td>
				</tr>
					'.$optionField.'
				</table>
				</div>
					
				</td>
				</tr>
				</table>
				</div>
				
	';
} else {
	$noticeSignature = "&nbsp;";
}
 
?>
<SCRIPT LANGUAGE=Javascript>
<!--//
function printAll()
{
	var c = "";
	var optionStr = "";
<?if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) {?>
	len = document.form1.elements['option[]'].length;
	var total = 0;
	var optionStr = "";
	for(i=1; i<=len; i++) {
		if(eval("document.getElementById('option"+i+"')").checked==true) {
			total++;
			optionStr += i+",";
		}
	}

	if(document.form1.updateOption[1].checked==true && total==0) {
		alert("<?=$Lang['eDiscipline']['SelectFooterWarningMsg']?>");
	} else {
		var c = document.getElementById('updateOption1').checked==true ? 1 : 2;
		optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
<?}?>
		if(confirm("<?=$Lang['eNotice']['ConfirmForPrint']?>"))
			newWindow('../module_print_all_preview.php?keyword=<?=$keyword?>&status=<?=$status?>&sign_status=<?=$sign_status?>&print_status=<?=$print_status?>&StartDate=<?=$StartDate?> 00:00:00&EndDate=<?=$EndDate?> 23:59:59&thisModule=<?=$thisModule?>'+'&updateOption='+c+'&option='+optionStr,10);
			
<?if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) {?>			
	}
<? } ?>	
}

function printSelected()
{
	var c = "";
	var optionStr = "";

	var s='';
	var x=document.getElementsByName("NoticeIDArr[]");
	for(i=0;i<x.length;i++)
	{
		if(x[i].checked)
		{
			s = s +x[i].value 	+",";
		}
	}
	
	if(s=='')
	{
		alert("<?=$i_Discipline_System_Notice_Warning_Please_Select?>");
	}
	else
	{
<?if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) {?>
		len = document.form1.elements['option[]'].length;
		var total = 0;
		var optionStr = "";
		for(i=1; i<=len; i++) {
			if(eval("document.getElementById('option"+i+"')").checked==true) {
				total++;
				optionStr += i+",";
			}
		}
		/*
		if(confirm("<?=$Lang['eNotice']['ConfirmForPrint']?>"))
			newWindow('../module_print_all_preview.php?NoticeIDStr='+s,10);
		*/
		if(document.form1.updateOption[1].checked==true && total==0) {
			alert("<?=$Lang['eDiscipline']['SelectFooterWarningMsg']?>");
		} else {
			var c = document.getElementById('updateOption1').checked==true ? 1 : 2;
			optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
<? } ?>
			if(confirm("<?=$Lang['eNotice']['ConfirmForPrint']?>"))
				newWindow('../module_print_all_preview.php?NoticeIDStr='+s+'&updateOption='+c+'&option='+optionStr,10);
<?if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) {?>					
		}
<? } ?>	
	}
	
}

function ViewNoticePrintPreview(id)
{
	if(confirm("<?=$Lang['eNotice']['ConfirmForPrint']?>"))
		newWindow('../module_print_preview.php?NoticeID='+id,10);
}

function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

		function showHideLayer(layerName, flag) {
			if(flag=="") {
				flag = (document.form1.DisplayFooter.value==0) ? 1 : 0;
			}

			if(flag==1) {
				document.getElementById(layerName).style.visibility = "visible";
				document.getElementById("DisplayFooter").value = 1;
			} else {
				document.getElementById(layerName).style.visibility = "hidden";
				document.getElementById("DisplayFooter").value = 0;
			}			
		}

		function changeEnable(flag) {
			if(flag==1) {
				disableFlag = false;
				textClass = "";
			} else {
				disableFlag = true;
				textClass = "tabletextremark";
			}

			len = document.form1.elements['option[]'].length;
			for(i=1; i<=len; i++) {
				eval("document.getElementById('option"+i+"')").disabled = disableFlag;
				eval("document.getElementById('option"+i+"')").parentNode.className = textClass;
			}
		}
		
function sign(id,studentID)
{
	newWindow('../sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}		

function viewReply(id,studentID)
{
	newWindow('../view.php?NoticeID='+id+'&StudentID='+studentID,10);
}

function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "index.php";
}

function printList()
{
	document.form1.action = "print_list.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.target = "_self";
	document.form1.action = "index.php";
}

//-->
</SCRIPT>

<form name="form1" method="post" action="index.php" onSubmit="return check_form();">

<div class="content_top_tool">
		<?=$export_btn?>
		<?=$print_list_btn?>
		<div class="Conntent_tool"><?=$PrintBtn_All?> <?=$PrintBtn_Selected?></div>
		<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear:both" />
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<tr>
    <td align="left" valign="bottom"><?=$status_select?><?=$sign_status_select?><?=$print_status_select?><?=$title_select?><br><?=$date_select?></td>
    <td align="right" valign="bottom" height="28"><?=$noticeSignature?></td>
</tr>
<tr>
    <td colspan="2">
            <?=$li->display();?>
    </td>
</tr>
</table>

<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="DisplayFooter" id="DisplayFooter" value="0" />
<input type="hidden" name="submitOptionFlag" id="submitOptionFlag" value="0" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

