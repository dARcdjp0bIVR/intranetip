<?php
// Modifying by : 

################## Change Log [Start] ##############
#
#   Date:   2020-10-14  Bill    [2020-0929-0952-27235]
#           hide Approve / Reject btn, to fix multiple submit
#
#   Date:   2020-09-15  Bill    [2020-0915-1022-58066]
#           fixed: incorrect Question Relation handling if reply slip has MC questions that not required to be answered
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-09-03  Bill    [2020-0826-1451-05073]
#           Performance Issue: use JS to handle Question Relation
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date:	2019-12-05	Philips
#			remove $sys_custom['eNotice']['PreviewShowTargetDetails'], show target details in general
#
#	Date:	2019-10-17 Philips [2019-1016-1125-40260]
#			improved: add back button for teacher
#
#   Date:   2019-06-26  Bill    [2019-0621-0947-35276]
#           Show details of Target Type when preview eNotice    ($sys_custom['eNotice']['PreviewShowTargetDetails'])
#
#   Date:   2019-06-05  (Bill)
#           add CSRF token in html form
#
#   Date:   2019-03-26 (Bill)   [2019-0131-1137-13207]
#           hide sign button    ($sys_custom['eNotice']['HideSignButton'])
#
#	Date:	2017-01-29 (Isaac)	
#			modifided position for notice number field from below the titile to above the title.
#
#	Date:	2017-01-16 (Bill)	[2017-0113-1120-22207]
#			not use htmlspecialchars_decode(), fixed some font-family not work for discipline notice
#
#	Date:	2016-11-03	Villa	[U107012]
#			Add Notice number to the view page
#
#	Date:	2016-10-17	Bill	[2016-1013-1541-17214]
#			Improved: display edit button for approval users
#
#	Date:	2016-08-11	Bill
#			fixed: cannot display error message for some must submit questions if answer is empty
#
#	Date: 	2016-07-14	Bill	[2016-0113-1514-09066]
#			Support MC questions to set interdependence
#			Added response for questions that no required to submit
#			Fixed error in IE
#
#	Date:	2016-07-11	Kenneth
#			Improvement: approval and reject action for unapproved notice
#
#	Date: 	2016-06-10	Bill	[2016-0113-1514-09066]
#			Merge logic from sign2.php, for interdependent eNotice questions
#			(Parent notice & Module notice use different method to create reply slip content)
#
#	Date:	2016-06-06	Kenneth	
#			Edit date logic: compare datetime instead of date only
#
#	Date:	2015-11-13	Bill	[2015-0615-1438-48014]
#			display * for Questions must be submitted (sheet.mustSubmit = 1)
#			perform answer checking for Questions must be submitted
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-07-27	Bill	[2015-0428-1214-11207]
# 			Display PICs to parent only $sys_custom['eNotice']['DisplayPICForParent'] = true
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Add field to display PIC
#
#	Date:	2015-04-21	Bill
#			Improved: change affected user type from users (without full right) to parents (not allow resign) [2015-0316-1603-27206]
#
#	Date:	2015-04-14	Bill
#			Improved: not allow users (without full right) to resign after deadline [2015-0316-1603-27206]
#			Improved: allow eNotice PIC to sign if Settings - "Allow PIC to edit replies for parents." - true [2015-0323-1602-46073]
#
#	Date:	2014-04-14	YatWoon
#			Revised the $valid checking [Case#J61206]
#
#	Date:	2014-02-28	YatWoon
#			Add checking for student, is the student related to this notice? 
#
#	Date:	2011-03-09	Henry Chow
#			change the attachment function when $this->Module is not null
#
#	Date:	2010-11-29	YatWoon
#			If user can edit Notice, then display "edit" button and click button redriect to edit page
#
#	Date:	2010-11-16 YatWoon
#			update "Do not allow parents to modify replies" logic, not apply this logic to FullRight Admin	
#
#	Date:	2010-08-02 YatWoon
#			update layout to IP25 standard
#			remove nl2br for the Description (due to use html-editor for input)
#
#	Date:	2010-06-02	YatWoon
#			check allow re-sign the notice or not ($lnotice->NotAllowReSign)
#
# Date:	2010-03-24	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

################################
# Pass in:
#		$NoticeID
#		$StudentID
################################
$NoticeID = IntegerSafe($NoticeID);
//$StudentID = IntegerSafe($StudentID);

$lform = new libform();
$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);

$valid = true;
$editAllowed = false;
$view_only = false;
if ($lu->isStudent())
{
    //$valid = false;
    $view_only = true;
}

$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();

if ($StudentID != "" && $StudentID != 0)
{
	// [2015-0316-1603-27206] check if eNotice after deadline or not
	$today = time();
	$deadline = strtotime($lnotice->DateEnd);
	
// 	$afterDeadline = compareDate($today,$deadline)>0;
	$afterDeadline = ($today>$deadline);
	
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            //$today = time();
            //$deadline = strtotime($lnotice->DateEnd);
            //if (compareDate($today,$deadline)>0)
            if ($afterDeadline)
            {
                if($lnotice->isLateSignAllow)
	            {
	            	$valid = true;
	            	$editAllowed = true;
	            }
	            else
	            {
                	//$valid = false;
                	$view_only = true;
                	$editAllowed = false;
                }
            }
            else
            {
                $editAllowed = true;
            }
        }
    }
    else # Teacher
    {
	    if($lnotice->IsModule)
	    {
			$editAllowed = true;    
	    }
	    else
	    {
        	$editAllowed = $lnotice->isEditAllowed($StudentID);
        	// [2015-0323-1602-46073] Allow PIC to sign if user is eNotice PIC and Settings - "Allow PIC to edit replies for parents." - true
        	if($StudentID!='undefined' && $lnotice->isNoticePIC($NoticeID) && $lnotice->isPICAllowReply)
        		$editAllowed = true;
    	}
    }
}
else
{
	//$valid = false;
	if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
    
}

if($view_only)
{
	header("Location: view.php?StudentID=$StudentID&NoticeID=$NoticeID");
    exit();
}

if ($editAllowed)
{
    if ($StudentID=="" || $StudentID==0)
    {
        $valid = false;
    }
}

############################################################
#	Check student belongs to this notice?
############################################################
if($StudentID)
{
	if(!$lnotice->isStudentInNotice($NoticeID, $StudentID))
	{
		$valid = false;
	}
}

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $isTeacher)
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

// if(!$valid && !$isTeacher)
if((!$valid && !$isTeacher) || !$allowTeacherAccess)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// [2015-0428-1214-11207] get PICs of eNotice
if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher) {
	$pics = "";
	$noticePICs = $lnotice->returnNoticePICNames();
	if(count($noticePICs) > 0){
		$pics = "";
		$delim = "";
		for($picCount=0; $picCount<count($noticePICs); $picCount++){
			$pics .= $delim.$noticePICs[$picCount]['UserName'];
			$delim = ", ";
	  	}
	}
}

$lnotice->retrieveReply($StudentID);

// [2020-0604-1821-16170]
$hasNoticeFullRight = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->hasPaymentNoticeFullRight() : $lnotice->hasSchoolNoticeFullRight();
$isApprovalUser = strtoupper($lnotice->Module) == "PAYMENT" ? $lnotice->isPaymentNoticeApprovalUser() : $lnotice->isSchoolNoticeApprovalUser();

# 2010-06-02 check allow re-sign the notice or not
// [2020-0604-1821-16170]
// [2015-0316-1603-27206] not allow user resign after deadline
// [2015-0316-1603-27206] only parent accounts affected, teachers (with edit right) can also resign after deadline
//if($lnotice->signerID && $lnotice->NotAllowReSign && !$lnotice->hasFullRight())
//if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline)) && !$lnotice->hasFullRight())
//if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline)) && $isParent && !$lnotice->hasFullRight())
if($lnotice->signerID && ($lnotice->NotAllowReSign || (!$lnotice->NotAllowReSign && $afterDeadline)) && $isParent && !$hasNoticeFullRight)
{
    $editAllowed = false;
}

$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";

$targetType = array("", $i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$targetRecipientDisplay = $targetType[$lnotice->RecordType];

// [2019-0621-0947-35276] Show details of Target Type when preview eNotice
if($isTeacher && (!isset($StudentID) || $StudentID == ""|| $StudentID == '0'))
{
    $targetRecipient = $lnotice->returnRecipientNames();
    $targetRecipientDisplay = $targetRecipient[0];
    if($targetRecipient[1] != '')
    {
        $targetRecipientDisplay .= "<br/>";
        $targetRecipientDisplay .= "[".$targetRecipient[1]."]";
    }
}

# Modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module=='')
{
	$attachment = $lnotice->displayAttachment();
}
else
{
	//$attachment = $lnotice->displayModuleAttachment();
	$attachment = $lnotice->displayAttachment();
}

// if($lnotice->IsModule)
// 	$this_Description = $lnotice->Description;
// else
// 	$this_Description = intranet_convertAllLinks($lnotice->Description);
// [2015-0416-1040-06164] use getNoticeContent() to get display content
//$this_Description = $lnotice->Description;
$this_Description = $lnotice->getNoticeContent($StudentID);

/*
if($plugin['Disciplinev12']) {
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	$templateVar = $ldiscipline->TemplateVariable();
	
	$newDescription = $this_Description;
	
	if(is_array($templateVar))
	{
		foreach($templateVar as $Key=>$Value)
		{
			$newDescription = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" />", $Value, $newDescription);	
			$newDescription = str_ireplace("<IMG src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif\" alt=\"\" />", $Value, $newDescription);	
			$newDescription = str_ireplace("<IMG alt=\"\" src=\"".$image_path."/2007a/discipline/".$Key.".gif\" />", $Value, $newDescription);				
			$newDescription = str_ireplace("<IMG src=\"".$image_path."/2007a/discipline/".$Key.".gif\" alt=\"\" />", $Value, $newDescription);				
		} 
	}
}	
*/

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice2;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$reqFillAllFields = $lnotice->AllFieldsReq;
// [2016-0113-1514-09066]
$DisplayQNum = $lnotice->DisplayQuestionNumber;

# check if user can edit the notice or not
// [2020-0604-1821-16170]
// [2016-1013-1541-17214] display edit button for approval users accessing pending notice
//if($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->hasFullRight())  
//if($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->hasFullRight() || (!$editAllowed && $lnotice->isApprovalUser() && $lnotice->RecordStatus>=4))
if($lnotice->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $hasNoticeFullRight || (!$editAllowed && $isApprovalUser && $lnotice->RecordStatus>=4))
{
	# check is parent-signed notice or student-signed notice
	
	$link = "../../eAdmin/StudentMgmt/notice/edit.php?NoticeID=".$NoticeID;
	if($lnotice->TargetType=="S")
		$link = "../../eAdmin/StudentMgmt/notice/student_notice/edit.php?NoticeID=".$NoticeID;
	
	if(!$lnotice->IsModule)
	$edit_btn = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "EditNotice('$link');" );
}
?>

<script language="javascript" src="/templates/forms/layer.js"></script>
<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
	<script language="javascript" src="/templates/forms/eNoticePayment_form_edit.js"></script>
<? } else { ?>
	<script language="javascript" src="/templates/forms/form_edit.js"></script>
<? } ?>	
                        	
<SCRIPT LANGUAGE=Javascript>
// Set true if need to fill in all fields before submit
var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';

<? if($lnotice->Module=='') { ?>
var formAllFilled  = true;

function recurReplace(exp, reby, txt) {
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
	return txt;
}
<? } ?>

function checkform(obj)
{
	// Submit Form
	document.form1.action = "sign_update.php";
	document.form1.target = "_self";
	
	<? if($lnotice->Module=='') { ?>	
		// Form Checking
		if (formAllFilled)
		{
		     //return true;
		}
		else
		{
		      alert("<?=$Lang['Notice']['AnswerSpecificQuestions']?>");
		      return false;
		}
//		else
//		{
//		      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
//		      return false;
//		}
	<? } else { ?>
		// - Allow submit
		// 1. All questions are required to be answered - false  AND  Specific questions are required to be answered - false
		//		OR
		// 2. Required fields are filled
		//if (!need2checkform || formAllFilled)
		if ((!need2checkform && !need2checkformSQ) || formAllFilled)
			 {
			      return true;
			 }
			 else if(!need2checkform && need2checkformSQ)
			 {
			      alert("<?=$Lang['Notice']['AnswerSpecificQuestions']?>");
			      return false;
			 }
			 else
			 {
			      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
			      return false;
			 }
	<? } ?>
		 
	// Confirm Update 
    if (!confirm('<?=$i_Notice_Alert_Sign?>'))
         return false;
}

function click_print()
{
	with(document.form1)
	{
// 	    alert(NoticeID.value);
	        
        <? if($lnotice->Module) {?>
    	action = "module_print_preview.php";
    	<? } else {?>
    	action = "print_preview.php";
    	<? } ?>
    	
    	target = "_blank";
        submit();
	}        
}

function EditNotice(url)
{
	window.opener.location=url;
	window.close();
}

function sendApproveAjax(approve_state)
{
	if(!confirm('<?=$Lang['eNotice']['Warning']['ConfirmChangingStatus']?>')){
		return;
	}
	
	$('#approval_div_radio').hide();
	$('#approval_div h4').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');

	$.ajax({
		url: "ajax_approve_notice.php",
		type: "POST",
		data: { 
			approve_state : approve_state,
			noticeID: '<?=$NoticeID ?>'  
		},
		dataType: "html",
		success: function(response){
			if(response=='APPROVED'){
				approved();
				opener.location.reload();
			}
			else if(response=='REJECTED'){
				rejected();
				opener.location.reload();
			}
		}
	});
}

var approvedOrRejected = false;
function approved(){
	$('#approval_div_radio').hide();
	$('#approval_div h4').html('<?=$Lang['General']['Approved'] ?>');
}
function rejected(){
	$('#approval_div_radio').hide();
	$('#approval_div h4').html('<?=$Lang['General']['Rejected'] ?>');
}
</SCRIPT>
<style>

.approval_div{
	border: 1px #ffffaa solid;
	background: #ffffb3;
	padding: 0px 20px 10px 20px; 
	text-align:center;
}

tr.disabled_q, tr.disabled_q td, tr.disabled_q td.tabletext, tr.disabled_q td span.tabletextrequire {
	color: gray;
}

</style>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
       
	<table class="form_table_v30">
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_DateStart?></td>
		<td><?=$lnotice->DateStart?></td>
	</tr>
                    
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_DateEnd?></span></td>
		<td><?=$lnotice->DateEnd?></td>
	</tr>
	
     <tr valign='top'>
		<td class="field_title"><?=$i_Notice_NoticeNumber?></td>
		<td><?=$lnotice->NoticeNumber?></td>
	</tr>
	                
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_Title?></td>
		<td><?=$lnotice->Title?></td>
	</tr>
	
                    
   <tr valign='top'>
		<td class="field_title"><?=$i_Notice_Description?></td>
		<? if($lnotice->Module == "DISCIPLINE") { ?>
			<td><?=$this_Description?></td>
		<? } else { ?>
			<td><?=htmlspecialchars_decode($this_Description)?></td>
		<? } ?>
	</tr>

	
	<? if(strtoupper($lnotice->Module) == "PAYMENT"){ ?>
	<tr valign='top'>
		<td class="field_title"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></td>
		<? if($lnotice->DebitMethod == 1){ ?>
			<td class="field_title"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly']?></td>
		<? } else { ?>
			<td class="field_title"><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater']?></td>
		<? } ?>
	</tr>
	<? } ?>
	
    <? if ($attachment != "") { ?>
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_Attachment?></td>
		<td><?=$attachment?></td>
	</tr>
    <? } ?>
                    
  	<? if ($isTeacher) {
		$issuer = $lnotice->returnIssuerName();
	?>
 	<tr valign='top'>
		<td class="field_title"><?=$i_Notice_Issuer?></td>
		<td><?=$issuer?></td>
	</tr>
	<? } ?>
	 
    <?php if(($sys_custom['eNotice']['DisplayPICForParent'] && $isParent) || $isTeacher) { ?>  	
		<tr valign='top'>
			<td class="field_title"><?=$i_Profile_PersonInCharge?></td>
			<td><?=$pics?></td>
		</tr>
	<?php } ?>
	
	<? 
	if ($editAllowed) {
        if ($lnotice->replyStatus != 2)
        {
            $statusString = "$i_Notice_Unsigned";
        }
        else
        {
            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
        }
	?>
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_SignStatus?></td>
		<td><?=$statusString?></td>
	</tr>
            
	<tr valign='top'>
		<td class="field_title"><?=$i_UserStudentName?></td>
		<td><?=$lu->getNameWithClassNumber($StudentID)?></td>
	</tr>
            
	<? } ?>
                    
    <tr valign='top'>
		<td class="field_title"><?=$i_Notice_RecipientType?></td>
		<!-- <td><?=$targetType[$lnotice->RecordType]?></td> -->
		<td><?=$targetRecipientDisplay?></td>
	</tr>
	
	<? if ($reqFillAllFields) { ?>
    	<tr>
    		<td class="field_title">&nbsp;</td>
    		<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
    	</tr>
	<? } ?>
	</table>
	<td>
</tr>

<? 
### check include reply slip or not
$hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
?>

<? if($hasContent) {?>
<tr>
	<td colspan="2">      
		<!-- Break -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
        </tr>
        </table>
	</td>
</tr>
<? } ?>
<tr>
	<td colspan="2">
        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
        <? if($hasContent) {?>
        <!-- Relpy Slip -->
            <tr valign='top'>
			<td colspan="2" class='eNoticereplytitle' align='center'><?=$Lang['eNotice']['ReplySlipForm']?></td>
			</tr>

			<? if($lnotice->ReplySlipContent) {?>
				<!-- Content Base Start //-->
					<tr valign='top'>
						<td colspan="2">
						<?=$lnotice->ReplySlipContent?>
						</td>
					</tr>
			<? } else {
					if($lnotice->Module==''){
						// Question and Answer
						$queString = str_replace("&amp;", "&", $lnotice->Question);
						$queString = preg_replace('(\r\n|\n)', "<br>", $queString);
						$queString = trim($queString)." ";
						$qAry = $lnotice->splitQuestion($queString, true);
						$aAry = $lnotice->parseAnswerStr($ansString);

						// Convert Question String for display
						$queString = $lform->getConvertedString($queString);

						// init array for js checking
						$optionArray = array();
						$optionArray[0] = array();
						$optionArray[1] = array();
						$mustSubmitArray = array();
                        $isSkipQuestionArray = array();

						// loop Questions
						for($i=0; $i<count($qAry); $i++)
						{
							$currentQuestion = $qAry[$i];
							$mustSubmitArray[] = $reqFillAllFields || $currentQuestion[3];
							$optionArray[0][] = $currentQuestion[4][0];
							$optionArray[1][] = $currentQuestion[4][1];
                            $isSkipQuestionArray[] = $mustSubmitArray[$i] && $optionArray[1][$i] != '' && ($optionArray[0][$i] != $optionArray[1][$i]);

							// for MC Questions
                            // [2020-0915-1022-58066]
                            //if($currentQuestion[0]==2 && $mustSubmitArray[$i])
							if($currentQuestion[0]==2)
							{
								$optionCount = count((array)$currentQuestion[4]);
								for($j=2; $j<$optionCount; $j++)
								{
									$optionArray[$j][$i] = $currentQuestion[4][$j];
								}

                                // [2020-0915-1022-58066] MC questions that required to be answered
                                $isSkipQuestionArray[$i] = false;
								if($mustSubmitArray[$i])
								{
                                    if(count(array_unique($currentQuestion[4])) > 1) {
                                        $mustSubmitArray[$i] = $optionCount;
                                        $isSkipQuestionArray[$i] = true;
                                    }
                                }
							}
						}

                        // [2020-0826-1451-05073] use JS to handle Question Relation
						// $qAryRelation = $lnotice->handleQuestionRelation ($mustSubmitArray, $optionArray);

						// Related Question
						// loop Questions
//						for($i=0; $i<count($qAry); $i++){
//							$needToloop = $mustSubmitArray[$i]==1 && $optionArray[1][$i]!="";
//						}
						
			?>
						<!-- Question Base Start //-->
						<tr valign='top'>
							<td colspan="2">
	                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
	                        <tr><td>
	                                <form name="ansForm" method="post" action="update.php">
	                                	<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center'>
										<tr><td>
											<?= $lnotice->returnReplySlipContent("sign", $qAry, $aAry, $reqFillAllFields, $DisplayQNum);?>
										</td></tr>
										</table>
                                        <input type=hidden name="qStr" value="<?=$queString?>">
                                        <input type=hidden name="aStr" value="">
                                	</form>
                                
	                                <SCRIPT LANGUAGE=javascript>
		                                var mustSubmitArr = new Array("", "<?= implode('","',(array)$mustSubmitArray); ?>");
		                                var option0SkipArr = new Array("", "<?= implode('","',(array)$optionArray[0]); ?>");
										var option1SkipArr = new Array("", "<?= implode('","',(array)$optionArray[1]); ?>");
                                        var isSkipQuestionArr = new Array("", "<?= implode('","',(array)$isSkipQuestionArray); ?>");
										var relateArr = {};
										var totalQNum = <?=count($qAry)?>;
										<?
										    /*
											foreach($qAryRelation as $qNum => $qOptions)
											{
												echo "relateArr[".$qNum."] = {};\n";
												foreach($qOptions as $oNum => $qr)
												{
													echo "relateArr[".$qNum."][".$oNum."] = new Array(\"".implode("\",\"",(array)$qr)."\");\n";
												}
											}
											*/

                                            foreach((array)$optionArray as $oNum => $qOptionArray)
                                            {
                                                echo "relateArr[".$oNum."] = {};\n";
                                                foreach((array)$qOptionArray as $qNum => $targetQ)
                                                {
                                                    if($targetQ == '') {
                                                        $targetQ = $optionArray[0][$qNum];
                                                    }
                                                    if($targetQ == '') {
                                                        $targetQ = $qNum + 1;
                                                    }
                                                    echo "relateArr[".$oNum."][".$qNum."] = '$targetQ';\n";
                                                }
                                            }
										?>
										var skipCheckingToQ = 0;
                                		
                                		function checkAvailableOption(i, opt)
                                		{
                                			var i = parseInt(i);
                                            var targetQ = relateArr[opt][i-1];
                                            if(targetQ != 'end') {
                                                targetQ = parseInt(targetQ);
                                            }
                                            var originTargetQ = targetQ;
                                            var isJumpQ = false;
                                            var preTextOnlyQCount = 0;

                                			//var opt = parseInt(opt);
                                			/*
                                			// Skip if no option settings 
                                			//var availableAry = relateArr[i-1][opt];
				                            var availableAry = relateArr[i-1];
				                            if(availableAry==undefined)
				                            	return;
				                            
				                            availableAry = availableAry[opt];
                                			if(availableAry==undefined){
                                				availableAry = new Array("-1");
                                			}
                                			*/

                                			for(var v=i+1; v<=totalQNum; v++)
                                			{
                                				var eleObj = eval("document.ansForm.F"+v);
                                				if (eleObj){
	                                				if (eleObj.length)
										       		{
											       		switch(eleObj[0].type){
											        		// Radio Button
											        	    case "radio":	
									                        case "checkbox":
                                                                if(v >= (targetQ + 1) && targetQ != 'end')
                                                                {
								                                    for (var p=0; p<eleObj.length; p++){
					                                					eleObj[p].disabled = false;
					                                					//eval("document.ansForm.F"+v+".disabled=false");
					                                					//$("#msg_"+v).attr('style', 'display:none');
					                                					$("td#msg_" + v).hide();
					                                					$("tr#row" + v).removeClass("disabled_q");
                                                                    }

                                                                    var isPreJumpQ = isJumpQ;
                                                                    if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                        isJumpQ = isSkipQuestionArr[v];
                                                                        if(!isJumpQ) {
                                                                            targetQ = relateArr[0][v-1];
                                                                            if(targetQ != 'end') {
                                                                                targetQ = parseInt(targetQ);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    for (var p=0; p<eleObj.length; p++){
					                                					eleObj[p].disabled = true;
					                                					eleObj[p].checked = false;
					                                					//eval("document.ansForm.F"+v+".disabled=true");
					                                					//$("#msg_"+v).attr('style', '');

					                                					$("td#msg_" + v).show();
					                                					$("tr#row" + v).addClass("disabled_q");
					                                				}
								                                }
								                                break;
											        	}
	                                				}
	                                				else
	                                				{
                                                        if(v >= (targetQ + 1) && targetQ != 'end')
                                                        {
                                                            //eleObj[p].disabled=false;
		                                					eval("document.ansForm.F" + v + ".disabled=false");
					                                		//$("#msg_"+v).attr('style', 'display:none');
					                                		$("td#msg_" + v).hide();
		                                					$("tr#row" + v).removeClass("disabled_q");

                                                            var isPreJumpQ = isJumpQ;
                                                            if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                isJumpQ = isSkipQuestionArr[v];
                                                                if(!isJumpQ) {
                                                                    targetQ = relateArr[0][v-1];
                                                                    if(targetQ != 'end') {
                                                                        targetQ = parseInt(targetQ);
                                                                    }
                                                                }
                                                            }
		                                				}
		                                				else
                                                        {
		                                					//eleObj[p].disabled=true;
		                                					eval("document.ansForm.F" + v + ".disabled=true");
		                                					eval("document.ansForm.F" + v + ".value=''");
					                                		//$("#msg_"+v).attr('style', '');
					                                		$("td#msg_" + v).show();
		                                					$("tr#row" + v).addClass("disabled_q");
		                                				}
	                                				}

                                                    preTextOnlyQCount = 0;
                                				} else {
                                                    if(v >= (targetQ + 1) && targetQ != 'end') {
                                                        preTextOnlyQCount = preTextOnlyQCount + 1;
                                                    }
                                                }
                                			}
                                		}
                                		
		                                function getAns(i)
		                                {
		                                	// get target answer
		       								var eleObj = eval("document.ansForm.F"+i);
        									
        									// init
									        var strAns = null;
									        var tempAns = null;
											var canSkipChecking = skipCheckingToQ=='end' || skipCheckingToQ > 0 && i <= skipCheckingToQ;
											var mustSubmitQ = mustSubmitArr[i];
											
											// reutn if required answer not exist
									        if (eleObj==undefined)
									        {
									            return '';
									        }
											
									        if (eleObj.length)
									        {
									        	if(eleObj[0].disabled==false)
									        	{
										        	switch(eleObj[0].type)
										        	{
										        		// Radio Button
										        	    case "radio":
										        			// check is T/F Question
										        			var isTFQ = false;
												        	if(document.getElementById("typeOf"+i))
												        	{
												        		isTFQ = document.getElementById("typeOf"+i).value==1;
												        	}
												        	
							                                for (p=0; p<eleObj.length; p++){
						                                        if (eleObj[p].checked)
						                                        {
					                                                tempAns += p;
					                                                
//					                                                // Update skipCheckingToQ for T/F Question 
//					                                                if(isTFQ && p=="0" && !canSkipChecking){
//					                                                	if (typeof option0SkipArr[i] != "undefined")
//					                                                	{
//					                                                		skipCheckingToQ = option0SkipArr[i];
//					                                                	} 
//					                                                }
//					                                                else if(isTFQ && p=="1" && !canSkipChecking){
//					                                                	if (typeof option1SkipArr[i] != "undefined")
//					                                                	{
//					                                                		skipCheckingToQ = option1SkipArr[i];
//					                                                	}
//					                                                }
						                                        }
							                                }
							                                
							                                // check empty input
							                                if ((need2checkform || mustSubmitQ>=1) && tempAns == null && !canSkipChecking)
							                                {
							                                    formAllFilled = false;
							                                    return false;
							                                }
															
							                                if (tempAns != null)
							                                {
							                                    strAns = tempAns;
							                                }
							                                break;
														
														// CheckBox
								                        case "checkbox":
							                                for (p=0; p<eleObj.length; p++) {
						                                        if (eleObj[p].checked)
						                                        {
						                                            if (tempAns != null)
						                                            {
						                                                tempAns += ","+p;
						                                            }
						                                            else
						                                            {
						                                                tempAns = p;
						                                            }
						                                        }
							                                }
							                                
							                                // check empty input
							                                if ((need2checkform || mustSubmitQ==1) && tempAns == null && !canSkipChecking)
							                                {
							                                    formAllFilled = false;
							                                    return false;
							                                }
							                                
							                                strAns = tempAns;
							                                break;
														
								                        case "text":
							                                for (p=0; p<eleObj.length; p++){
							                                	tempAns += recurReplace('"', '&quot;', eleObj.value);
							                                }
							                                
											        		// check empty input
							                                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
							                                {
							                                    formAllFilled = false;
							                                    return false;
							                                }
							                                strAns = tempAns;
							                                break;                  	
									                }
									        	}
									        	else
									        	{
									        		if(eleObj[0].type=='text')
									        		{
						                                strAns = '';
									        		}
									        	}
								        	}
								        	// Text Field
								        	else
								        	{
								        		if(eleObj.disabled==false)
								        		{
									                tempAns = recurReplace('"', '&quot;', eleObj.value);
									                
									        		// check empty input
									                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
									                {
									                    formAllFilled = false;
									                    return false;
									                }
									                strAns = tempAns;
									       		}
									       		else{
									       			strAns = '';
									       		}
								        	}
									        return strAns;
									  	}
									  	
		                                function copyback()
		                                {
			                                // reset parms
		                                	txtStr = '';
		                                	formAllFilled = true;
		                                	skipCheckingToQ = 0;
		                                	
		                                   	// loop table rows
		                                    var tableArr = $("#ContentTable")[0].tBodies[0].rows;
								            for (var x=0; x<tableArr.length; x++)
								            {
								            	// get answers
								            	temp = getAns(x+1);
								            	
								            	// return false if required question not filled
							                    if (formAllFilled==false)
							                    {
							                        return false;
							                    }
							                    
							                    // append answer to txtStr
												txtStr+="#ANS#"+temp;
								           	}
								           	
								           	// update aStr value
									        document.ansForm.aStr.value=txtStr;
			                                document.form1.qStr.value = document.ansForm.qStr.value;
			                                document.form1.aStr.value = document.ansForm.aStr.value;
		                                }
		                                
		                                function updateAllAvailableOption(){
		                               		// get T/F Question that involve skip action
		                                	var possibleTF = $('input[onclick]:checked');
		                                	if(possibleTF.length){
		                                		for(var TFCount=0; TFCount<possibleTF.length; TFCount++)
		                                		{
		                                			var currentTFQ = possibleTF[TFCount]
		                                			var TFQNum = currentTFQ.name.replace("F", "");
		                                			var TFQOption = currentTFQ.value;
		                                			
			                                		// perform to disable fields not related to answered question
													if(!currentTFQ.disabled){
		                                				checkAvailableOption(TFQNum, TFQOption);
		                                			}
		                                		}
		                                	}
		                                }
		                                
		                               	$(document).ready(function(){
			                               	// update all options
		                               		updateAllAvailableOption();
		                                });
			                  		</SCRIPT>
                            </td></tr>
                            </table>
                    		</td>
						</tr>
						<!-- Question Base End //-->
				<? } else {
						$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
						$queString = $lform->getConvertedString($temp_que);
						$queString =trim($queString)." ";
				?>
						<!-- Question Base Start //-->
						<tr valign='top'>
						<td colspan="2">
	                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
	                        <tr>
	                        	<td>
	                                <form name="ansForm" method="post" action="update.php">
	                                        <input type=hidden name="qStr" value="">
	                                        <input type=hidden name="aStr" value="">
	                                </form>
	                                <script language="Javascript">
									// Set true if need to fill in some selected fields before submit
									var need2checkformSQ = <? if(trim($queString)!="" && strpos($queString, "#MSUB#1")!==false){ echo "true"; } else { echo "false"; }  ?>;
									
	                                <?=$lform->getWordsInJS()?>
	                                
	                                var paymentOptions = "";
	                                var paymentOptIDAry = new Array();
									var paymentOptNameAry = new Array();
									var paymentItemID = ""; 
									var paymentIDAry = new Array();
									var paymentItemName = "<?=$Lang['eNotice']['PaymentItemName']?>";
									var accountBalance = "";
									var paymentTitle = '<?=$Lang['eNotice']['PaymentTitle']?>';
									var paymentCategory = '<?=$Lang['eNotice']['PaymentCategory']?>';
									var paymentAmount = '<?=$Lang['eNotice']['Amount']?>';
									var paymentDescritption = '<?=$Lang['eNotice']['Description']?>';
									var paymentPay = '<?=$Lang['eNotice']['Pay']?>';
									var paymentNotPaying = '<?=$Lang['eNotice']['NotPaying']?>';
									var paymentAccountBal = '<?=$Lang['eNotice']['AccountBalance']?>';
									var DisplayQuestionNumber = '<?=$lnotice->DisplayQuestionNumber?>';
									
	                                var sheet= new Answersheet();
	                                // attention: MUST replace '"' to '&quot;'
	                                sheet.qString="<?=$queString?>";
	                                sheet.aString="<?=$ansString?>";
	                                //edit submitted application
	                                sheet.mode=1;
					                // display * and perform answer checking for Questions must be submitted
	                                sheet.mustSubmit=1;
	                                sheet.answer=sheet.sheetArr();
	                                Part3 = '';
	                                document.write(editPanel());
	                                </script>
	                                
	                                <SCRIPT LANGUAGE=javascript>
	                                function copyback()
	                                {
	                                         finish();
	                                         document.form1.qStr.value = document.ansForm.qStr.value;
	                                         document.form1.aStr.value = document.ansForm.aStr.value;
	                                }
	                                </SCRIPT>
	                                
	                        	</td>
							</tr>
		                    </table>
	                    </td>
						</tr>
						<!-- Question Base End //-->
				<? } ?>
				<? } ?>
				<? } ?>
				<form name="form1" action="sign_update.php" method="post" onSubmit="return checkform(this)">
    				<?php if($sys_custom['eNotice']['HideSignButton']) {
    				    // do not show sign instruction for some schools
    				} else {
    				    //if ($editAllowed && !$lnotice->ReplySlipContent)
                        if ($editAllowed && $hasContent) { ?>
    						<tr>
                        		<td colspan="2" align="left" class="tabletext">&nbsp;&nbsp;&nbsp;<?=($lnotice->replyStatus==2? $i_Notice_SignInstruction2:$i_Notice_SignInstruction)?></td>
        					</tr>
                        <? } ?>
    				<? } ?>
				</table>
				
				<? if(!$editAllowed && $isApprovalUser && $lnotice->RecordStatus>=4){ ?>
					<div id="approval_div" class="approval_div">
						<h4><?=$Lang['eNotice']['ChangeApprovalStatus'] ?></h4>
						<div id="approval_div_radio">
							<input type="button" onclick="sendApproveAjax(1);" value="<?= $Lang['Btn']['Approve'] ?>" class="formbutton_v30 print_hide">
							<? if($lnotice->RecordStatus!=5){ ?>
								<input type="button" onclick="sendApproveAjax(0);" value="<?= $Lang['Btn']['Reject'] ?>" class="formbutton_alert print_hide">
							<? } ?>
						</div>
					</div>
				<? } ?>
				
				<div class="edit_bottom_v30">
					<p class="spacer"></p>
    					<?php if($sys_custom['eNotice']['HideSignButton']) {
                    		// do not show sign button for some schools
    					} else { ?>
    						<? if ($editAllowed && !$lnotice->ReplySlipContent && $hasContent) { ?>
    							<?= $linterface->GET_ACTION_BTN($i_Notice_Sign, "submit", "copyback();","submit2") ?>
    						<? } ?>
    						<? if ($editAllowed && (!$hasContent || $lnotice->ReplySlipContent)) { ?>
    							<?= $linterface->GET_ACTION_BTN($i_Notice_Sign, "submit") ?>
    						<? } ?>
    					<? } ?>
						
						<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print();","cancelbtn") ?>
						<?= $edit_btn ?>
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
						<?php  if($isTeacher&&$_GET['StudentID']!=""&&$_GET['StudentID']!='undefined') {
							echo $linterface->GET_ACTION_BTN($button_back, "button", "window.history.back()");
						}?>
					<p class="spacer"></p>
				</div>
				
				<input type="hidden" name="qStr" value="">
                <input type="hidden" name="aStr" value="">
                <input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
                <input type="hidden" name="StudentID" value="<?=$StudentID?>">
				
				<?php echo csrfTokenHtml(generateCsrfToken()); ?>
                </form>
            	
	</td>
</tr>
</table>                        
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>