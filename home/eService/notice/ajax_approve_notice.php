<?php
# using: Bill

########################################
#
#   Date:   2018-10-10  Bill    [2018-1008-1030-09073]
#           support pushmessagenotify_PIC / pushmessagenotify_ClassTeachers / $emailnotify_PIC / $emailnotify_ClassTeachers
#
########################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$libeClassApp = new libeClassApp();
$lwebmail = new libwebmail();

$NoticeID = $_POST['noticeID'];
$today = date('Y-m-d h:i:s');

$approve_state = $_POST['approve_state'];
if($approve_state==1)
{
	foreach((array)$NoticeID as $_noticeID)
	{
		###################################
		#
		#	Step 0: Get all temp push msg and email status
		#	Step 1: Notified Issuer by email and app msg
		#	Step 2: Send push msg and email
		#	Step 3: Status 4 => 1
		#
		###################################
	    
		####### Step 0 #######
		$noticeObj = new libnotice($_noticeID);
		$pushmessagenotify = $noticeObj->pushmessagenotify;
		$pushmessagenotify_Students = $noticeObj->pushmessagenotify_Students;
		$pushmessagenotify_PIC = $noticeObj->pushmessagenotify_PIC;
		$pushmessagenotify_ClassTeachers = $noticeObj->pushmessagenotify_ClassTeachers;
		$pushmessagenotifyMode = $noticeObj->pushmessagenotifyMode;
		$pushmessagenotifyTime = $noticeObj->pushmessagenotifyTime;
		$emailnotify = $noticeObj->emailnotify;
		$emailnotify_Students = $noticeObj->emailnotify_Students;
		$emailnotify_PIC = $noticeObj->emailnotify_PIC;
		$emailnotify_ClassTeachers = $noticeObj->emailnotify_ClassTeachers;
		$targetType = $noticeObj->TargetType;
		$studentsAry = $noticeObj->getNotifiedStudentList();
		
		$sql = "SELECT DISTINCT b.UserID
        		FROM INTRANET_PARENTRELATION as a
        		  LEFT OUTER JOIN INTRANET_USER as b ON a.ParentID = b.UserID
        		WHERE a.StudentID IN (".implode(',',$studentsAry).") ";
		$parentsAry = $noticeObj->returnVector($sql);
	    
		####### Step 1 #######
		$noticeObj->sendApprovalNotice();
	    
		####### Step 2 #######
		if($pushmessagenotify || $pushmessagenotify_Students || $pushmessagenotify_PIC || $pushmessagenotify_ClassTeachers || $emailnotify || $emailnotify_Students || $emailnotify_PIC || $emailnotify_ClassTeachers)
		{
		    if($pushmessagenotify || $pushmessagenotify_Students || $pushmessagenotify_PIC || $pushmessagenotify_ClassTeachers)
		    {
				// Send Push msg
				/**
				 * IMPORTANT!!!	20160603
				 * As Student App not yet be implemented in this module
				 * 	- ALL push msg will send to Parent app ONLY
				 */
				list($pushmessage_subject, $pushmessage_body) = $noticeObj->returnPushMessageData();
				if($targetType=='P' || $targetType=='S')
				{
					// for parent notice and student notice
					$isPublic = "N";
					$appType = $eclassAppConfig['appType']['Parent'];
					$appType_s = $eclassAppConfig['appType']['Student'];
					$appType_t = $eclassAppConfig['appType']['Teacher']; // For PIC & Class Teachers
					$sendTimeMode = $pushmessagenotifyMode;
					$sendTimeString = $pushmessagenotifyTime;
	                
					if($sendTimeMode=='scheduled') {
						// Check the time is expired: if yes -> Send NOW!!!!
						if(strtotime($today) >= strtotime($sendTimeString)) {
							// Time is expired
							$sendTimeMode = 'now';
							$sendTimeString = '00:00:00';
						}
					}
					
					// Logic if sendTimeMode == 'now',
					//	check is issue day is in future
					//		-> yes:  sendTimeMode -> 'scheduled' and sendTimeString -> issueDate
// 					else if($sendTimeMode=='now'){
// 						$now = date('Y-m-d h:i:s');
// 						if(strtotime($now)<strtotime($noticeObj->DateStart)){
// 							$sendTimeMode = 'scheduled';
// 							$sendTimeString = $noticeObj->DateStart;
// 						}
// 					}
                    
				    if($pushmessagenotify==1)
				    {
						if (!empty($studentsAry)) {
							$parentStudentAssoAry = BuildMultiKeyAssoc($lu->getParentStudentMappingInfo($studentsAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
						} else {
							$parentStudentAssoAry = array();
						}
						$individualMessageInfoAry = array();
						$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
						
						if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
							$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $_noticeID);
						} else {
							$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $_noticeID);
						}
				    }
				    if($pushmessagenotify_Students==1)
				    {
				        $StudentAssoAry = array();
					    foreach ($studentsAry as $studentId) {
    						$_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
    						// link the message to be related to oneself
    						$StudentAssoAry[$studentId] = array($_targetStudentId);
					    }
					    $individualMessageInfoAry = array();
						$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
                        
						if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
							$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $_noticeID);
						} else {
							$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $_noticeID);
						}
				    }
				    
				    // Send pushmessagenotify_PIC
				    // [2018-1008-1030-09073] Handle both PIC and Class Teachers - push msg notification
				    if($pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
				    {
				        $teacherAssoAry = array();
				        if($pushmessagenotify_PIC == 1)
				        {
				            $target_PIC = array();
				            $PICs = $noticeObj->returnNoticePICNames();
				            foreach($PICs as $PIC){
				                $target_PIC[] = $PIC['UserID'];
				            }
    				        if(isset($target_PIC) && sizeof($target_PIC) != 0)
    				        {
    				            $target_PIC = array_unique($target_PIC);
    				            foreach($target_PIC as $tPICID) {
    				                $tPICid = $tPICID;
    				                $tPICid = str_replace("&#160;", "", $tPICid);
    				                $tPICid = str_replace("U", "", $tPICid);
    				                
    				                //$PICAssoAry[$tPICid] = array($tPICid);
    				                $teacherAssoAry[$tPICid] = array($tPICid);
    				            }
    				        }
				        }
				        if($pushmessagenotify_ClassTeachers == 1)
				        {
				            require_once ($PATH_WRT_ROOT."includes/libclass.php");
				            $libclass = new libclass();
				            
				            $teacherArr = array();
				            foreach ($studentsAry as $student) {
				                $class = $libclass->returnCurrentClass($student);
				                $teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
				                foreach ($teachers as $teacher) {
				                    array_push($teacherArr, $teacher['UserID']);
				                }
				            }
				            $teacherArr = array_unique($teacherArr);
				            foreach ($teacherArr as $tid) {
				                $teacherAssoAry[$tid] = array($tid);
				            }
				        }
			            $individualMessageInfoAry = array();
			            $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
			            
			            if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
			                $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $_noticeID);
			            } else {
			                $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $_noticeID);
			            }
				    }
				}
			}
	        
			if($emailnotify || $emailnotify_Students || $emailnotify_PIC || $emailnotify_ClassTeachers)
			{
				// Send email
				list($email_subject, $email_body) = $noticeObj->returnEmailNotificationData();
				
				if($emailnotify && $emailnotify_Students) {
					$ToArray = array_merge($studentsAry,$parentsAry);
				} else if($emailnotify==1) {
					$ToArray = $parentsAry;
				} else if($emailnotify_Students==1) {
					$ToArray = $studentsAry;
				} else {
				    $ToArray = array();
				}
				
				// [2018-1008-1030-09073] Send emailnotify_PIC
				if($emailnotify_PIC == 1) {
				    $target_PIC = array();
				    $PICs = $noticeObj->returnNoticePICNames();
				    foreach($PICs as $PIC) {
				        $target_PIC[] = $PIC['UserID'];
				    }
				    if(sizeof($target_PIC) != 0)
				    {
				        $PICs = array();
				        $target_PIC = array_unique($target_PIC);
				        foreach($target_PIC as $tPICID){
				            $tPICid = $tPICID;
				            $tPICid = str_replace("&#160;", "", $tPICid);
				            $tPICid = str_replace("U", "", $tPICid);
				            
				            $PICs[] = $tPICid;
				        }
				        //debug_pr($PICs);
				        $ToArray = array_merge($ToArray, $PICs);
				    }
				}
				
				// [2018-1008-1030-09073] Send emailnotify_ClassTeachers
				if($emailnotify_ClassTeachers == 1) {
				    require_once($PATH_WRT_ROOT."includes/libclass.php");
				    $libclass = new libclass();
				    
				    $teacherArr = array();
				    foreach ($studentsAry as $student) {
				        $class = $libclass->returnCurrentClass($student);
				        $teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
				        foreach ($teachers as $teacher) {
				            array_push($teacherArr, $teacher['UserID']);
				        }
				    }
				    $teacherArr = array_unique($teacherArr);
				    
				    //debug_pr($teacherArr);
				    $ToArray = array_merge($ToArray, $teacherArr);
				}
				
				$lwebmail->sendModuleMail($ToArray,$email_subject,$email_body,1,'','User',true);
			}
		}
	}
	
	####### Step 3 #######
	// Update Status
	$libdb = new libdb();
	$sql = "UPDATE INTRANET_NOTICE SET
    			RecordStatus = '1',
    			ApprovedBy = '".$_SESSION['UserID']."',
    			ApprovedTime = now()
    		WHERE NoticeID IN ('".implode("','",(array)$NoticeID)."')";
	$libdb->db_db_query($sql);
	
	intranet_closedb();
	
	echo 'APPROVED';
}
else
{
	### Send Rejected Msg
	foreach((array)$NoticeID as $_noticeID) {
		$noticeObj = new libnotice($_noticeID);
		$noticeObj->sendApprovalNotice(false);
	}
	
	$libdb = new libdb();
	$sql = "UPDATE INTRANET_NOTICE SET
            	RecordStatus = '5',
            	ApprovedBy = '$UserID',
            	ApprovedTime = now()
        	WHERE NoticeID IN ('".implode("','",(array)$NoticeID)."')";
	$libdb->db_db_query($sql);
	
	intranet_closedb();
	
	echo 'REJECTED';
}
?>