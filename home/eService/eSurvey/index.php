<?php

# using:Isaac

########### Change Log #######
#   Date:   2019-03-19  Isaac
#           Added flag $sys_custom['eSurvey']['studentHideCreaterAndTargetGroup'] to hide creator
#
##############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lsurvey		= new libsurvey();
$lu = new libuser($UserID);

if($sys_custom['eSurvey']['studentHideCreaterAndTargetGroup']){
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $lu = new libuser($_SESSION["UserID"]);
    $checkIsStudentView = $lu->isStudent();
    if($checkIsStudentView)$hideCreatorAndTargetGroup = true;
}

### Title ###
$CurrentPage	= "PageNonAnswerSurvey";
$CurrentPageArr['eServiceeSurvey'] = 1;
$TAGS_OBJ[] = array($Lang['eSurvey']['NonAnswerSurvey']);
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


$sql = $lsurvey->returnSurvey_IP25(1, $hideCreatorAndTargetGroup);
 
# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;  
$li->field_array = array("StartDate", "SurveyID");
$li->no_col = 5;
if($checkIsStudentView){$li->no_col -= 1;}
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th>". $Lang['eSurvey']['StartDate']. "</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['EndDate']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['Title']."</th>\n";
if(!$checkIsStudentView){$li->column_list .= "<th>".$Lang['eSurvey']['Creator']."</th>\n";}
$li->column_list .= "<th>&nbsp;</th>\n";
?>
<SCRIPT LANGUAGE=Javascript>
function openSurvey(id, row)
{
         newWindow('fill.php?SurveyID='+id+'&row='+row,1);
}
</SCRIPT>

<br />
<form name="form1" method="get" action="index.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>&nbsp;</td>
					<td height="28" align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>
				<tr>
					<td colspan="2">
						<?=$li->display('green');?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
