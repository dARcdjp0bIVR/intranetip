<?php

# using:Isaac

########### Change Log #######
#   Date:   2019-03-19  Isaac
#           Added flag $sys_custom['eSurvey']['studentHideCreaterAndTargetGroup'] to hide creator
#
#	Date:	2011-08-29	YatWoon
#			fixed: If client hasn't set any setting in eAdmin, there is no data for variable $lsurvey->NotAllowReSign [Case#2011-0829-1040-55071]
#
#	Date:	2011-06-02	YatWoon
#			add option allow re-sign the survey or not ($lsurvey->NotAllowReSign)
#
##############################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lsurvey		= new libsurvey();
$lu = new libuser($UserID);

if($sys_custom['eSurvey']['studentHideCreaterAndTargetGroup']){
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $lu = new libuser($_SESSION["UserID"]);
    $checkIsStudentView = $lu->isStudent();
    if($checkIsStudentView)$hideCreatorAndTargetGroup = true;
}

### Title ###
$CurrentPage	= "PageAnsweredSurvey";
$CurrentPageArr['eServiceeSurvey'] = 1;
$TAGS_OBJ[] = array($Lang['eSurvey']['AnsweredSurvey']);
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

//$sql = $lsurvey->returnSurvey_IP25(1);

$namefield = getNameFieldWithClassNumberByLang("c.");

$viewSurveyAnswer = "concat('<a href=\"javascript:viewSurveyAnswer(', a.SurveyID,');\">', a.Title, '</a>')";
$openSurvey = "concat('<a href=\"javascript:openSurvey(', a.SurveyID,');\">', a.Title, '</a>')";

$NotAllowReSign = $lsurvey->NotAllowReSign ? $lsurvey->NotAllowReSign : 0;

$sql = "
		SELECT 
			DATE_FORMAT(a.DateStart,'%Y-%m-%d') as StartDate,
			DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as EndDate,
			if((a.DateEnd>=CURDATE() and not ". $NotAllowReSign ."), $openSurvey , $viewSurveyAnswer ),";
if(!$hideCreatorAndTargetGroup){
    $sql .= "IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),";
}
$sql .= "b.DateModified,
		    a.SurveyID as SurveyID
		FROM 
			INTRANET_SURVEYANSWER as b
		    LEFT JOIN INTRANET_SURVEY as a ON (a.SurveyID = b.SurveyID)
		    LEFT JOIN INTRANET_USER as c ON c.UserID = a.PosterID
		WHERE 
	    	b.UserID = ". $UserID;
if ($sys_custom['SurveySaveWithDraft']) {
	$sql .= " AND b.isDraft != '1'";
}

# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;  
$li->field_array = array("StartDate", "SurveyID");
$li->no_col = 5;
if($hideCreatorAndTargetGroup){$li->no_col -= 1;}
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th>". $Lang['eSurvey']['StartDate']. "</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['EndDate']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['Title']."</th>\n";
if(!$hideCreatorAndTargetGroup){$li->column_list .= "<th>".$Lang['eSurvey']['Creator']."</th>\n";}
$li->column_list .= "<th>".$Lang['eSurvey']['CompletedDate']."</th>\n";
?>
<SCRIPT LANGUAGE=Javascript>
function viewSurveyAnswer(id)
{
         newWindow('view_answer.php?SurveyID='+id+'&viewUser=<?=$UserID?>',1);
}
function openSurvey(id)
{
         newWindow('fill.php?SurveyID='+id,1);
}
</SCRIPT>

<br />
<form name="form1" method="get" action="completed_index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="center">
				<table width="96%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" class="tabletext">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>&nbsp;</td>
									<td height="28" align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
								</tr>
								<tr>
									<td colspan="2">
						<?=$li->display('blue');?>
					</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br /> <input type="hidden" name="order"
		value="<?php echo $li->order; ?>"> <input type="hidden" name="pageNo"
		value="<?php echo $li->pageNo; ?>" /> <input type="hidden"
		name="field" value="<?php echo $li->field; ?>" /> <input type="hidden"
		name="page_size_change" value="<?=$page_size_change?>" /> <input
		type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
