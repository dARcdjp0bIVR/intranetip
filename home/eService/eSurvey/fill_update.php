<?php
//Using: 
##################################
#
#   Date:   2020-05-22 Tommy
#           fix: reload this page will update answer to null in database 
#
#   Date:   2019-03-19  Isaac
#           Added flag $sys_custom['eSurvey']['studentHideCreaterAndTargetGroup'] to hide creator and target groups
#
#	Date:	2015-12-16	Pun
#			Fixed does not check for empty in this page
#
#	Date:	2012-05-02	YatWoon
#			add access right checking
#
#	Date:	2011-06-02	YatWoon
#			add option allow re-sign the survey or not ($lsurvey->NotAllowReSign)
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$aStr = trim($aStr);

intranet_auth();
intranet_opendb();

$lform = new libform();
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);
$lu = new libuser($UserID);

if($sys_custom['eSurvey']['studentHideCreaterAndTargetGroup']){
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $lu = new libuser($_SESSION["UserID"]);
    $checkIsStudentView = $lu->isStudent();
    if($checkIsStudentView)$hideCreatorAndTargetGroup = true;
}

# check access right
$this_target_group = $lsurvey->returnTargetGroupsArray();
if(sizeof($this_target_group) > 0)	# with target group
{
	$lg = new libgroup();
	$inGroup = 0;
	# check is group member or not
	foreach($this_target_group as $k=>$d)
	{
		list($this_group_id, $this_group_title) = $d;
		$inGroup = $inGroup || $lg->isGroupMember($UserID, $this_group_id);
	}
	
	if(!$inGroup)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$MODULE_OBJ['title'] = $i_Survey_FillSurvey;
$home_header_no_EmulateIE7=true;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
$before_isDraft = 0;
# Update survey
$allowDraftSetting = false;
if ($sys_custom['SurveySaveWithDraft']) {
	$allowDraftSetting = true;
}


if ($allowDraftSetting) {
	$sql = "SELECT Answer, isDraft FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$UserID'";
	$result = $lsurvey->returnResultSet($sql);
	if (count($result) > 0) {
		$isDraft = ($isDraft == "1") ? $isDraft : "0";
		$before_isDraft = ($result[0]["isDraft"] == "1") ? $result[0]["isDraft"] : 0;
		$answer = (!empty($result[0]["Answer"])) ? $result[0]["Answer"] : "";
	}
} else {
	$sql = "SELECT Answer FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$UserID'";
	$answer = $lsurvey->returnVector($sql);
}

if ((sizeof($answer) == 0 || empty($answer)) && $aStr!="") // $answer[0]==""
{
    $username = $lu->getNameForRecord();
    if ($allowDraftSetting) {
    	$sql = "INSERT INTO INTRANET_SURVEYANSWER (SurveyID, UserID, UserName, Answer, isDraft, DateInput, DateModified)
        	    VALUES ('$SurveyID', '$UserID', '$username', '$aStr', '$isDraft', now(),now())";
    } else {
    	$sql = "INSERT INTO INTRANET_SURVEYANSWER (SurveyID, UserID, UserName, Answer, DateInput, DateModified)
    	VALUES ($SurveyID,$UserID,'$username','$aStr',now(),now())";
    }
    $lsurvey->db_db_query($sql);
}
else
{
    if($aStr != ""){
    	if ($allowDraftSetting) {
    		if(!$lsurvey->NotAllowReSign || $before_isDraft=="1") {
    			$sql = "UPDATE INTRANET_SURVEYANSWER SET Answer='$aStr', isDraft='$isDraft', DateModified=now() where SurveyID='$SurveyID' and UserID='$UserID'";
    			$lsurvey->db_db_query($sql);
    		} else {
    			$aStr = $answer;
    		}
    	} else {
    		if(!$lsurvey->NotAllowReSign) {
    			$isDraft = 0;
    			$sql = "UPDATE INTRANET_SURVEYANSWER SET Answer='$aStr', DateModified=now() where SurveyID='$SurveyID' and UserID='$UserID'";
    			$lsurvey->db_db_query($sql);
    		} else {
    			$aStr = $answer[0];
    		}
    	}
    }else{
        if ($allowDraftSetting) {
            if(!$lsurvey->NotAllowReSign || $before_isDraft=="1") {
                $sql = "UPDATE INTRANET_SURVEYANSWER SET Answer='$answer', isDraft='$isDraft', DateModified=now() where SurveyID='$SurveyID' and UserID='$UserID'";
                $lsurvey->db_db_query($sql);
            } else {
                $aStr = $answer;
            }
        } else {
            if(!$lsurvey->NotAllowReSign) {
                $isDraft = 0;
                $sql = "UPDATE INTRANET_SURVEYANSWER SET Answer='$answer', DateModified=now() where SurveyID='$SurveyID' and UserID='$UserID'";
                $lsurvey->db_db_query($sql);
            } else {
                $aStr = $answer[0];
            }
        }
    }
	/*
	if(!$lsurvey->NotAllowReSign)
	{
		$sql = "UPDATE INTRANET_SURVEYANSWER SET Answer='$aStr', DateModified=now() where SurveyID='$SurveyID' and UserID='$UserID'";
		$lsurvey->db_db_query($sql);
	} else {
		$aStr = $answer[0];
	}*/
}

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);
$aStr = str_replace('"','&quot;',$aStr);
$aStr = str_replace("\n",'<br>',$aStr);
$aStr = str_replace("\r",'',$aStr);


$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();
$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();
$reqFillAllFields = $lsurvey->reqFillAllFields();

if ($ownerGroup != "")
{
    $poster = "$poster<br>\n$ownerGroup";
}
if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
{
    $target = $i_general_WholeSchool;
}
else
{
    $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
}

?>
<br />

<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_startdate?></td>
	<td><?=$lsurvey->DateStart?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_enddate?></td>
	<td><?=$lsurvey->DateEnd?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_title?></td>
	<td><?=$lsurvey->Title?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_description?></td>
	<td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td>
</tr>
<?php if(!$hideCreatorAndTargetGroup){?>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_Survey_Poster?></td>
	<td><?=$poster?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_TargetGroup?></td>
	<td><?=$target?></td>
</tr>
<?php }?>

<? if ($lsurvey->RecordType == 1) { ?>
<tr>
	<td >&nbsp;</td>
	<td>[<?=$i_Survey_Anonymous?>]</td>
</tr>
<? } ?>


<tr>
	<td colspan=2>
		<form name="ansForm" method="post" action="update.php">
		        <input type=hidden name="qStr" value="">
		        <input type=hidden name="isDraft" value="0">
		        <input type=hidden name="aStr" value="">
		</form>
		
		<?=$i_Survey_Result?><br/><?=$Lang['eSurvey']['Hint'] ?>
		<hr>
		<?php
		if($lsurvey->NotAllowReSign && $isDraft != "1") {
		?>
			<script language="javascript" src="/templates/forms/form_view.js"></script>
			
			<script language="Javascript">
			var replyslip = '<?=$i_Notice_ReplySlip?>';
			var order_lang = '<?=$Lang['eSurvey']['Order']?>';
			var choice_lang = '<?=$Lang['Polling']['Choice']?>';
			var hint_lang = '<?=$Lang['Polling']['Hint']?>';
			var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
			
			var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
			var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
			myQue = "<?=$queString?>";
			myAns = "<?=$aStr?>";
			document.write(viewForm(myQue, myAns));
			</SCRIPT>
		<? } else {
			
			$userAnswer = $lsurvey->returnUserAnswer($UserID);
			if($userAnswer)
			{
				$ansstr = $userAnswer[0]['Answer'];
				$temp_ans =  str_replace("&amp;", "&", $ansstr);
				$ansString = $lform->getConvertedString($temp_ans);
				$ansString =trim($ansString)." ";	
			}
			?>
			<script language="javascript" src="/templates/forms/form_edit.js"></script>
			<script language="Javascript">
			<?=$lform->getWordsInJS()?>
		
			var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
			var order_lang = '<?=$Lang['eSurvey']['Order']?>';
			var choice_lang = '<?=$Lang['Polling']['Choice']?>';
			var hint_lang = '<?=$Lang['Polling']['Hint']?>';
			var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
			var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
			var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
	
			//background_image = "/images/layer_bg.gif";
			
			var sheet= new Answersheet();
			// attention: MUST replace '"' to '&quot;'
			sheet.qString="<?=$queString?>";
			sheet.aString="<?=$ansString?>";
			//edit submitted application
			sheet.mode=1;
			sheet.answer=sheet.sheetArr();
			//sheet.templates=form_templates;
			document.write(editPanel());
			</script>
			<SCRIPT LANGUAGE=javascript>
			function copyback()
			{
				need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
				document.ansForm.isDraft.value = "0";
			 	finish();
			 	document.form1.qStr.value = document.ansForm.qStr.value;
			 	document.form1.aStr.value = document.ansForm.aStr.value;
			}

			function saveAsDraft()
			{
				need2checkform = false;
				document.ansForm.isDraft.value = "1";
				finish();
				document.form1.isDraft.value = document.ansForm.isDraft.value;
		 		document.form1.qStr.value = document.ansForm.qStr.value;
		 		document.form1.aStr.value = document.ansForm.aStr.value;
			}

			function validSubmit()
			{
				if (formAllFilled)//!need2checkform ||
				{
			    	return true;
			    } else if(!need2checkform && !orderingAllFilled){ 
				    alert("<?=$Lang['eSurvey']['fillallorderingquestion']?>");
				    return false;
				} else //return true;
				{
					alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
					return false;
			 	}
			}
			$(document).ready(function() {
				$('form input[type="text"]').bind('keyup keypress', function(e) {
					var keyCode = e.keyCode || e.which;
					if (keyCode === 13) {
						e.preventDefault();
						return false;
					}
				});
			});
			</SCRIPT>
		<? } ?>
		<hr>
</td>
</tr>
<tr>
	<td colspan="2" align="center">
	<form name=form1 action=fill_update.php method=post onsubmit="return validSubmit()">
<? if(!$lsurvey->NotAllowReSign || ($isDraft== "1" && $allowDraftSetting)) {?>
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "copyback();") ?>&nbsp;<?php
		
		if ($allowDraftSetting && $lsurvey->AllowSaveDraft == "1" && $isDraft == "1") {
			echo $linterface->GET_ACTION_BTN($Lang["eSurvey"]["SaveAsDraft"], "submit", "saveAsDraft();");
		}
?>
		<input type=hidden name=row value="<?=$row?>">
		<input type=hidden name="isDraft" value="0">
		<input type=hidden name=qStr value="">
		<input type=hidden name=aStr value="">
		<input type=hidden name=SurveyID value="<?=$SurveyID?>">
<? } ?>
<? 
	//echo $linterface->GET_BTN($button_print, "button", "window.print();")
	echo $linterface->GET_ACTION_BTN($button_print, "button", "newWindow('print.php?SurveyID=".$SurveyID."', 37);");
	echo "&nbsp;";
	echo $linterface->GET_ACTION_BTN($button_close, "button", "window.close();");
?>
	</form>
	</td>
</tr>
</table>
<? if($row) { ?>
	<script language="javascript">
	$(document).ready(function() {
	<!--
		var x=opener.document.getElementById('ContentTable').rows;
		var y=x[<?=$row?>].cells;
<?php if ($isDraft == "1") { ?>
		y[4].innerHTML = "<?=$Lang['eSurvey']['Draft']?>";
<?php } else { ?>
		y[4].innerHTML = "<?=$Lang['eSurvey']['Answered']?>";
<?php } ?>
	//-->
	});
	</script>
<? } ?>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
