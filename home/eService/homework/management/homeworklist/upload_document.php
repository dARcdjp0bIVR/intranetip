<?php
// Editing by 
/*
 * 2017-06-27 (Carlos): $sys_custom['LivingHomeopathy'] created.
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['LivingHomeopathy'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();
$lf = new libfilesystem();

$is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

function getNextFreeFolder($base_dir,$return_full_path=false)
{	
	$LIMIT = 30000;
	//$LIMIT = 5; // test value
	$absolute_target_folder = $base_dir;
	
	$subfolder_number = 0;
	$file_count = 0;
	$folder_to_find = $absolute_target_folder."/".$subfolder_number;
	$rel_folder_to_find = $subfolder_number;
	
	if(!is_dir($folder_to_find)){
		mkdir($folder_to_find,0777);
	}
	$number_of_attempt = 0;
	do
	{
		$number_of_attempt+=1;
		$cmd = "find $folder_to_find -maxdepth 1 | wc -l";
		$file_count = intval(shell_exec($cmd));
		if($file_count >= $LIMIT)
		{
			$subfolder_number += 1;
			$folder_to_find = $absolute_target_folder."/".$subfolder_number;
			$rel_folder_to_find = $subfolder_number;
			if(!is_dir($folder_to_find)){
				mkdir($folder_to_find,0777);
			}
		}else{
			break;
		}
		if($number_of_attempt > $LIMIT) break; // avoid forever loop
	}while($file_count >= $LIMIT);
	
	if($return_full_path){
		return $folder_to_find;
	}else{
		return $rel_folder_to_find;
	}
}

function buildReturnScript($success, $returnHtml)
{
	$x = '<script type="text/javascript" language="javascript">'."\n";
	$x.= 'if(window.top.studentDocumentUploaded){window.top.studentDocumentUploaded('.$success.',\''.$returnHtml.'\');}'."\n";
	$x.= '</script>'."\n";
	
	return $x;
}

$file_field_name = 'StudentDocument';
$document_size_limit = isset($sys_custom['eHomework']['HomeworkDocumentSizeLimit']) && is_numeric($sys_custom['eHomework']['HomeworkDocumentSizeLimit'])? $sys_custom['eHomework']['HomeworkDocumentSizeLimit'] : 100; // default 100MB

if(count($_FILES)==0 || $_FILES[$file_field_name]['error'] > 0 || $_POST['RecordID']=='' || $_FILES[$file_field_name]['size']> $document_size_limit*1024*1024){
	intranet_closedb();
	echo buildReturnScript(0,'error');
	exit;
}

$RecordID = IntegerSafe($_POST['RecordID']);
$handin_records = $lhomework->getHomeworkHandinListRecords(array('RecordID'=>$RecordID,'StudentID'=>$_SESSION['UserID']));

if(count($handin_records) == 0){
	intranet_closedb();
	echo buildReturnScript(0,'record not found');
	exit;
}

$handin_record = $handin_records[0];

$homework_record= $lhomework->returnRecord($handin_record['HomeworkID']);

$target_dir = $file_path.'/file/homework_document';
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$subfolder = getNextFreeFolder($target_dir);
$target_dir .= '/'.$subfolder;

$uniq_folder = md5(uniqid().time().$_SESSION['UserID']);
$subfolder .= '/'.$uniq_folder;
$target_dir .= '/'.$uniq_folder;
if(!file_exists($target_dir) || !is_dir($target_dir)){
	$lf->folder_new($target_dir);
}else{
	chmod($target_dir,0777);
}

$file_name = $_FILES[$file_field_name]["name"];
if($is_magic_quotes_active){
	$file_name = stripslashes($file_name);
}

$base_name = $lf->get_file_basename($file_name);
$save_file_path = $subfolder.'/'.$base_name;
$target_path = $target_dir.'/'.$base_name;

$copy_success = move_uploaded_file($_FILES[$file_field_name]["tmp_name"], $target_path);

if($copy_success){
	$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus='1',StudentDocument='".$lhomework->Get_Safe_Sql_Query($save_file_path)."',StudentDocumentUploadTime=NOW() WHERE RecordID='".$RecordID."'";
	$success = $lhomework->db_db_query($sql);
	
	if($success)
	{
		$download_url = getEncryptedText($file_path.'/file/homework_document/'.$save_file_path);
		$x .= '<table><tr>';
		$x .= '<td><a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($base_name).'</a></td>';
		// if not marked by teacher and not due yet, student can delete uploaded document and reupload again
		if($_SESSION['UserType'] == USERTYPE_STUDENT && $handin_record['TeacherDocument']=='' && $handin_record['TextScore']=='' && date("Y-m-d") <= $homework_record['DueDate'])
		{
			$x .= '<td><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="deleteStudentDocument('.$handin_record['RecordID'].');" title="'.$Lang['Btn']['Delete'].'"></a></span></td>';
		}
		$x .= '</tr></table>';
		$x .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',date("Y-m-d H:i:s"),$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
		
		echo buildReturnScript(1,$x);
	}else{
		echo buildReturnScript(0,'');
	}
}else{
	$lf->folder_remove($target_dir);
	echo buildReturnScript(0,'');
}

intranet_closedb();
?>