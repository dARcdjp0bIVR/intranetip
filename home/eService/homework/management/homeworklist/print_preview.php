<?php
// Modifing by 
/*
 * 2017-06-30 (Carlos): $sys_custom['LivingHomeopathy'] Display uploaded homework document, marked document and rating. 
 */		
$PATH_WRT_ROOT = "../../../../../";
$top_menu_mode = 0;


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eServiceHomework'] = 1;

$record= $lhomework->returnRecord($hid);

list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath,$handinRequired,$collectRequired,$PostUserID)=$record; 
$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);

if($AttachmentPath!=NULL){
	$displayAttachment = $lhomework->displayAttachment($hid);
}

if($sys_custom['LivingHomeopathy'] && ($_SESSION['UserType'] == USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"]) ) ){
	$handin_records = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$HomeworkID,'StudentID'=>($_SESSION['UserType'] == USERTYPE_STUDENT? $_SESSION['UserID']: $childrenID),'GetTeacherName'=>1));
	$handin_record = $handin_records[0];
	$xx = '<tr>
				<td align="left" valign="top">'.$Lang['eHomework']['HandInHomework'].' : </td>
				<td>';
	if($handin_record['StudentDocument']!='')
	{
		$file_name = substr($handin_record['StudentDocument'],strrpos($handin_record['StudentDocument'],'/')+1);
		$xx .= intranet_htmlspecialchars($file_name);
		$xx .= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$handin_record['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
	}else{
		$xx .= $Lang['eHomework']['NotSubmitted'];
	}
		$xx.= '</td>
			</tr>';
	$xx .= '<tr>
				<td align="left" valign="top">'.$Lang['eHomework']['Mark'].' : </td>
				<td align="left" valign="top">';
			if($handin_record['TeacherDocument']!=''){
				$file_name = substr($handin_record['TeacherDocument'],strrpos($handin_record['TeacherDocument'],'/')+1);
				$xx .= $file_name;
				$xx .= '<div class="tabletextremark">('.str_replace('<!--NAME-->',$handin_record['TeacherDocumentUploaderName'],str_replace('<!--DATETIME-->',$handin_record['TeacherDocumentUploadTime'],$Lang['eHomework']['MarkedRemark'])).')</div>';
			}else{
				$xx .= $Lang['eHomework']['NotMarked'];
			}		
	$xx .=		'</td>
			</tr>';
	$xx .= '<tr>
				<td align="left" valign="top">'.$Lang['eHomework']['Rating'].' : </td>
				<td>'.($handin_record['TextScore']==''? $Lang['eHomework']['NotRated'] : '<p>'.Get_String_Display(intranet_htmlspecialchars($handin_record['TextScore'])).'</p><div class="tabletextremark">('.str_replace('<!--NAME-->',$handin_record['TextScoreUpdaterName'],str_replace('<!--DATETIME-->',$handin_record['TextScoreUpdateTime'],$Lang['eHomework']['RatedRemark'])).')</div>' ).'</td>
			</tr>';
}

?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['HomeworkList']?></b></td>
	</tr>
</table>        


<table width="50%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td></td><td></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$subjectGroupName?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?> : </td><td align="left" valign="top"><?=$title?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?> : </td><td align="left" valign="top"><?=($description==""? "--": $description)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Attachment']?> : </td><td align="left" valign="top"><?=($AttachmentPath==NULL? $i_AnnouncementNoAttachment: $displayAttachment)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Workload']?> : </td><td align="left" valign="top"><?=($loading/2)." ".$Lang['SysMgr']['Homework']['Hours']?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?> : </td><td align="left" valign="top"><?=$start?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?> : </td><td align="left" valign="top"><?=$due?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['HandinRequired']?> : </td><td align="left" valign="top"><?=($handinRequired=="1"? $i_general_yes :$i_general_no)?></td></tr>
	<? if($lhomework->useHomeworkCollect) {?>
	<tr><td align="left" valign="top"><?=$i_Homework_Collected_By_Class_Teacher?> : </td><td align="left" valign="top"><?=($collectRequired=="1"? $i_general_yes :$i_general_no)?></td></tr>
	<? } ?>
	
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Poster']?> : </td><td align="left" valign="top"><?=($PosterName2==""? "--":$PosterName2)?></td></tr>
	<?=$xx?>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>