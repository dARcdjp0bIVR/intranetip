<?php
// Modifing by : 
		
$PATH_WRT_ROOT = "../../../../../";
$CurrentPageArr['eServiceHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();


# Student
if($_SESSION['UserType']==USERTYPE_STUDENT){

	$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
	$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
	
	$display = (sizeof($subject)==0)? false:true;

	if($display){
		//$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, "", false);		
		 $selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
			if(sizeof($subject)!=0){
				$allSubjects = " a.SubjectID IN (";
				for ($i=0; $i < sizeof($subject); $i++)
				{	
					list($ID)=$subject[$i];
					$allSubjects .= $ID.",";
				}
				$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
			else{
				$allSubjects ="";
			}
			

		$cond = "";
		
		/*if($subjectID=="")
			$cond .= " AND $allSubjects";
			 
		else*/ 
		//debug_pr($_POST);
		if($subjectID!="" && $subjectID!=-1)
			$cond = "a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID AND ";
		
		$cond .= " c.UserID='$UserID' ";
		
		// [DM#2934] Cannot view future homework in reports
		$cond .= " AND a.StartDate<=CURDATE() ";
		
		//echo $cond;
		$x = $lhomework->monthlyHomeworkList($ts, "", $cond,"",1,"");
		$filterbar = "$selectClass $selectedSubject $selectedSubjectGroup";
	}
}

# Parent
else if($_SESSION['UserType']==USERTYPE_PARENT){

	$children = $lhomework->getChildrenList($UserID);
	$childrenID = ($childrenID=="")? $children[0][0]:$childrenID;
	
	$selectedChildren = $lhomework->getCurrentChildren("name=\"childrenID\" onChange=\"document.getElementById('subjectID').options.selectedIndex=0; reloadForm($ts)\"", $children, $childrenID, "", false);

	$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
	//$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

	$display = (sizeof($subject)==0)? false:true;
	
	$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
	if($display){

		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			}
		else{
			$allSubjects ="";
		}
			
		$cond = "";
		
		if(isset($childrenID) && $childrenID!="") 
			$cond = " c.UserID='$childrenID' AND ";
		else if(sizeof($childAry)>0)
			$cond .= " c.UserID IN (".implode(',', $childAry).") AND ";
		
		if($subjectID!="" && $subjectID!=-1)
			$cond .= " a.SubjectID = $subjectID AND ";
			
		$cond .= " a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
		
		// [DM#2934] Cannot view future homework in reports
		$cond .= " AND a.StartDate<=CURDATE() ";

		//}
		$x = $lhomework->monthlyHomeworkList($ts, "", $cond,"",1,"");
	}
	
	$filterbar = "$selectedChildren $selectClass $selectedSubject $selectedSubjectGroup";	
}
?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['MonthlyHomeworkList']?></b></td>
	</tr>
</table>        

<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<? if($_SESSION['UserType']==USERTYPE_STAFF) { ?>
	<tr><td align="left" valign="top" width="100"><?=$i_ClassName?> : </td><td align="left" valign="top"><?=$className?></td></tr>
<? } ?>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
</table>
<br>

<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>