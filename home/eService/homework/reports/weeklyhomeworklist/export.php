<?php
// Modifing by
		
$PATH_WRT_ROOT = "../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();
$lexport = new libexporttext();

$filename = "WeeklyHomeworkList_AllSubjects.csv";

if($childrenID!=""){
	$filename = "WeeklyHomeworkList_".$childrenID."_AllSubjects.csv";
	if($subjectID!="")
		$filename = "WeeklyHomeworkList_".$childrenID."_".$subjectID.".csv";
}

else{
	if($subjectID!="")	
		$filename = "WeeklyHomeworkList_".$subjectID.".csv";
}

if($_SESSION['UserType']==USERTYPE_PARENT){
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', a.EN_DES, a.CH_DES) AS SubjectName, IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroupName FROM ASSESSMENT_SUBJECT AS a 
				LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS AS c on c.SubjectGroupID = b.SubjectGroupID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d on d.SubjectGroupID = c.SubjectGroupID
				WHERE d.UserID = $childrenID AND a.RecordID = $subjectID";
		
		$result = $lhomework->returnArray($sql,2);
		$subjectName = $result[0]['SubjectName'];
		$subjectGroupName = $result[0]['SubjectGroupName'];
	}
	
	$subject = $lhomework->getStudyingSubjectList($childrenID, "", $yearID, $yearTermID);
	
	if(sizeof($subject)!=0){
		$allSubjects = " a.SubjectID IN (";
		for ($i=0; $i < sizeof($subject); $i++)
		{	
			list($ID)=$subject[$i];
			$allSubjects .= $ID.",";
		}
		$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
		$allSubjects .= " AND";
	}
	else{
		$allSubjects ="";
	}
		
	$cond = ($subjectID=='')? "$allSubjects a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
	$data = $lhomework->exportWeeklyHomeworkList($ts, $childrenID, $cond);
}

else{
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', a.EN_DES, a.CH_DES) AS SubjectName, IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroupName FROM ASSESSMENT_SUBJECT AS a 
				LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS AS c on c.SubjectGroupID = b.SubjectGroupID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d on d.SubjectGroupID = c.SubjectGroupID
				WHERE d.UserID = $UserID AND a.RecordID = $subjectID";
		
		$result = $lhomework->returnArray($sql,2);
		$subjectName = $result[0]['SubjectName'];
		$subjectGroupName = $result[0]['SubjectGroupName'];
	}
	
	# Select Subjects
	$subject = $lhomework->getStudyingSubjectList($UserID, $yearID, $yearTermID);
		
	if(sizeof($subject)!=0){
		$allSubjects = " a.SubjectID IN (";
		for ($i=0; $i < sizeof($subject); $i++)
		{	
			list($ID)=$subject[$i];
			$allSubjects .= $ID.",";
		}
		$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
		$allSubjects .= " AND";
	}
	else{
		$allSubjects ="";
	}
	
	$cond = ($subjectID=="")? "$allSubjects a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
	$data = $lhomework->exportWeeklyHomeworkList($ts, "", $cond);
}

# Set $ts to sunday of the week
$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
$today = date('Y-m-d');

# Get Cycle days
$b = new libcycleperiods();
$cStart = date('Y-m-d',$ts);
$cEnd = date('Y-m-d',$ts+6*86400);
$cycle_days = $b->getCycleInfoByDateRange($cStart,$cEnd);


for ($i=0; $i<7; $i++)
{
  $week_ts[] = $ts+$i*86400;

}

$exportColumn = array();		
			
for ($i=0; $i<7; $i++)
{
	$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
	$day = $Lang['SysMgr']['Homework']['WeekDay'][$i]." (".$current.")";
	array_push($exportColumn, $day);
}

if(sizeof($data)!=0){
	for ($i=0; $i<7; $i++)
	{
		$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
		$m = 0;
		
		for($j=0;$j<sizeof($data);$j++){

			list($hid,$subject,$subjectgroup,$startdate,$duedate,$loading,$title,$description,$subjectGroupID,$subjectID,$AttachmentPath) = $data[$j];
							  
			if ($current == $startdate){
				if ($loading==0)
				{
					$tload = 0;
				}
				else if ($loading <= 16)
				{
					$tload = $loading/2;
				}
				else
				{
					$tload = "$i_Homework_morethan 8";
				}

				$text1 = $Lang['SysMgr']['Homework']['Topic'].": ".$title;
				$text2 = $Lang['SysMgr']['Homework']['Subject'].": ".$subject;
				$text3 = $Lang['SysMgr']['Homework']['SubjectGroup'].": ".$subjectgroup;
				$text4 = $Lang['SysMgr']['Homework']['Workload'].": $tload ".$Lang['SysMgr']['Homework']['Hours'];
				$text5 = $Lang['SysMgr']['Homework']['DueDate'].": ".$duedate;
		
				$ExportArr[$m][$i] = $text1;
				$m++;
				$ExportArr[$m][$i] = $text2;
				$m++;
				$ExportArr[$m][$i] = $text3;
				$m++;
				$ExportArr[$m][$i] = $text4;
				$m++;
				$ExportArr[$m][$i] = $text5;
				$m++;
				$ExportArr[$m][$i] = "";
				$m++;
			}
		}
	}
}

else{
	$ExportArr[0][0] = $i_no_record_exists_msg;
}

//debug_r($ExportArr);
//echo sizeof($ExportArr)."<br>";
$export_content = $lexport->GET_EXPORT_TXT2($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
