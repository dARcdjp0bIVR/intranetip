<?php
# using: yat

######### Change Log
#
#
##########################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lprofile = new libprofile();
$li = new libuser($UserID);

########## Personal Photo
$personal_photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
$personal_photo_delete = "";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $personal_photo_link = $li->PersonalPhotoLink;
        
        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) 
          $personal_photo_delete = "<a href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
    }
}
$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";

### Title ###
$MODULE_OBJ['title'] = "Account Information";
$PAGE_NAVIGATION[] = array($i_UserProfilePersonal);

$msg = $msg=="photo_delete" ? "UpdateSuccess" : $msg;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

?>

<script language="javascript">
function deletePersonalPhoto() {
	if(confirm("<?=$Lang['Personal']['DeletePersonalPhoto']?>?")){
		window.location = "../../../iaccount/account/photo_delete.php?PhotoType=personal&UserID=<?=$UserID?>&return_path=/home/eService/StudentRegistry/online_reg/parent_account.php"
	}
}

function checkform(obj){
 <? if ($li->RetrieveUserInfoSetting("CanUpdate", "Email"))
   { ?>
 	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$i_UserEmail; ?>.")) return false;
 	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
 <? } ?>
}

<? if($msg) {?>
if(window.opener!=null)
	window.opener.location.reload();
<? } ?>	
</script>
<form name="form1" enctype="multipart/form-data" method="post" action="parent_account_update.php" onsubmit="return checkform(this)">
<table class="form_table_v30">
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title" width="30%">
	    				<?=$i_UserLogin?>
	    			</td>
	    			<td class="tabletext" valign="top" width="70%"><?=$li->UserLogin?></td>
	    			<td class="tabletext" rowspan="5" valign="top" align="left" nowrap>
	    				<?=$Lang['Personal']['PersonalPhoto']?><br />
	    				<img src="<?=$personal_photo_link?>" width="100" height="130"><br><?=$personal_photo_delete?>
	    			</td>
    			</tr>

    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserEnglishName?>
	    			</td>
	    			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->EnglishName,20," ",1)?></td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserChineseName?>
	    			</td>
	    			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->ChineseName,20," ",1)?></td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserNickName?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php if($li->RetrieveUserInfoSetting("CanUpdate", "NickName")){ ?>
						<input class="textboxnum" type="text" name="NickName" size="20" maxlength="100" value="<?=$li->NickName?>">
					<?php } else { ?>
						<? echo $li->NickName; ?>
					<?php }?>
	    			</td>
    			</tr>

				<?php if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")){ ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?=$Lang['Personal']['PersonalPhoto']?>
					</td>
					<td class="tabletextremark" valign="top" colspan="2">
						<input type="file" name="personal_photo" class="file" size="50"><br /><?=$i_UserPhotoGuide?>
					</td>
				</tr> 
	    		<?php } else { ?>
              		<input type="hidden" name="personal_photo" value=""><br />
				<?php } ?>	
    			
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserGender?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
						<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Gender")){ ?>
	    				<input type="radio" name="Gender" id="GenderM" value="M" <?php echo $Gender0; ?>><label for="GenderM"><?php echo $i_gender_male; ?></label>&nbsp;&nbsp;
						<input type="radio" name="Gender" id="GenderF" value="F" <?php echo $Gender1; ?>><label for="GenderF"><?php echo $i_gender_female; ?></label>
						<?php }
						      else
						        echo $Gender0!=""?$i_gender_male:$i_gender_female;
						?>
	    			</td>
    			</tr>
    			
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserHomeTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "HomeTel"))
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"HomeTelNo\" maxlength=\"20\" value=\"$li->HomeTelNo\">";
					  else
					    echo intranet_wordwrap($li->HomeTelNo,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserOfficeTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "OfficeTel"))
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"OfficeTelNo\" maxlength=\"20\" value=\"$li->OfficeTelNo\">";
					  else
					    echo intranet_wordwrap($li->OfficeTelNo,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserMobileTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					        if($li->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
					          echo "<input class=\"textboxnum\" type=\"text\" name=\"MobileTelNo\" maxlength=\"20\" value=\"$li->MobileTelNo\">";
					        else
					          echo intranet_wordwrap($li->MobileTelNo,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserFaxNo?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "Fax"))
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"FaxNo\" maxlength=\"20\" value=\"$li->FaxNo\">";
					  else
					    echo intranet_wordwrap($li->FaxNo,20," ",1);
					?>
	    			</td>
    			</tr>
    			
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserAddress?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<? if($li->RetrieveUserInfoSetting("CanUpdate", "Address"))
							echo $linterface->GET_TEXTAREA("Address", $li->Address);
						else
							echo nl2br($li->Address);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserCountry?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "Country"))
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"Country\" maxlength=\"50\" value=\"$li->Country\">";
					  else
					    echo intranet_wordwrap($li->Country,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserURL?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "URL"))
					    echo "<input class=\"textboxtext\" type=\"text\" name=\"URL\" value=\"$li->URL\">";
					  else
					    echo $li->URL;
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserEmail?> <? if($li->RetrieveUserInfoSetting("CanUpdate", "Email")) {?><span class='tabletextrequire'>*</span><? } ?>
	    			</td>
	    			<td class="tabletext" valign="top" colspan="2">
					<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Email")){ 
						?>
					 	<input class="textboxtext" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$li->UserEmail?>">
					 	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
					<?php
					      }
					      else
					        echo $li->UserEmail;
					?>
	    			</td>
    			</tr>
    			
    			<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?=$i_UserProfileMessage?>
					</td>
					<td class="tabletext" valign="top" colspan="2">
					<?php
					      if($li->RetrieveUserInfoSetting("CanUpdate", "Message"))
					        echo $linterface->GET_TEXTAREA("Info", $li->Info);
					      else
					        echo nl2br($li->Info);
					?>		
					</td>
				</tr>
				
    		</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_save, "submit");?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "reset");?>
<p class="spacer"></p>
</div>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>