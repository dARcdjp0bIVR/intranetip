<?php
// using :

/************************ Change Log ****************************/
/*
 *  2020-05-20 Cameron
 *      - add CSRF protection to be consistent with updated file [case #B185549]
 */
/********************** end of Change Log ***********************/


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lprofile = new libprofile();

$li = new libuser($UserID);

$PasswordSafety = $li->CheckPasswordSafe();
$password_warning = $Lang['PasswordNotSafe'][$PasswordSafety];
$password_change_disabled = !$li->RetrieveUserInfoSetting("CanUpdate", "Password");

### Title ###
$MODULE_OBJ['title'] = $i_UserProfileLogin;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
?>
<script language="javascript">
function checkform(obj){
      if(!check_text(obj.OldPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_old; ?>")) return false;
      if(!check_text(obj.NewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) return false;
<?php if ($li->RetrieveUserInfoSetting("Enable", "PasswordPolicy")) { ?>
      if (obj.NewPassword.value.length<6)
		{
			alert("<?=$Lang['PasswordNotSafe'][-2]?>");
			obj.NewPassword.focus();
			return false;
		}
		var re = /[a-zA-Z]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-3]?>");
			obj.NewPassword.focus();
			return false;
		}
		re = /[0-9]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-4]?>");
			obj.NewPassword.focus();
			return false;
		}
<?php } ?>
      if(!check_text(obj.ReNewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_retype; ?>")) return false;
      
      if (obj.NewPassword.value != "" || obj.ReNewPassword.value != "")
      {
          if(obj.NewPassword.value!=obj.ReNewPassword.value){
              alert("<?php echo $i_frontpage_myinfo_password_mismatch; ?>");
              obj.NewPassword.value="";
              obj.ReNewPassword.value="";
              obj.NewPassword.focus();
              return false;
          }
      }
}
</script>


<? if (!(isset($password_change_disabled) && $password_change_disabled)){ ?>
<form name="form1" method="post" action="/home/iaccount/account/login_password_update.php" onsubmit="return checkform(this)">
<table class="form_table_v30">
<?php if ($PasswordSafety<1 && $li->RetrieveUserInfoSetting("Enable", "PasswordPolicy")) { ?>
<tr>
	<td valign="top" colspan="2" class="field_title">
		<font color="red"><?=$Lang['PasswordWarning']?><br />* <?=$password_warning?></font>
</tr>
	
<?php } ?>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_old?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletextremark" valign="top" width="70%">
		<input class="textboxnum" type="password" name="OldPassword" maxlength="20"> (<?=$i_UserProfilePwdUse?>)
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_new?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletextremark" valign="top">
		<input class="textboxnum" type="password" name="NewPassword" maxlength="20"> 
		<?php if ($li->RetrieveUserInfoSetting("Enable", "PasswordPolicy")) { ?>
			(<?=$Lang['PasswordNotSafe'][-2]?>)
		<? } ?>
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_retype?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletext" valign="top">
		<input class="textboxnum" type="password" name="ReNewPassword" maxlength="20">
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
</tr>
</table>
    	
<div align="left">
<?=$linterface->MandatoryField();?>
</div>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
<p class="spacer"></p>
</div>		

<input type="hidden" name="FromLogin" value="<?=$FromLogin?>" />
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>
<? } else {?>
<table class="form_table_v30">
<tr>
	<td class="tabletext" valign="top" align="center"><?=$i_UserProfile_Change_Notice?></td>
</tr>
</table>
<? } ?>


<?php
intranet_closedb();
if (!(isset($password_change_disabled) && $password_change_disabled))
	print $linterface->FOCUS_ON_LOAD("form1.OldPassword");	
$linterface->LAYOUT_STOP();
?>
