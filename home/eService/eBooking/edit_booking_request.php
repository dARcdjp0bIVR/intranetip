<?php
//using : 

/*************************************************
 * Date:	2018-01-11 (Ivan) [ip.2.5.9.1.1]
 * Details: update DateModified field for the records for the update SQL
 * Date: 	2013-01-29 (Rita)
 * Details:	add send cancel/delete request email - Send_Cancel_Delete_Booking_Request_Email();
 *************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');
include_once($PATH_WRT_ROOT."includes/libebooking_door_access.php");

intranet_opendb();

$liblog = new liblog();
$lebooking = new libebooking();
$lebooking_door_access = new libebooking_door_access();

$lebooking->Start_Trans();

switch ($action) {
	case "CancelBookingRequest" :
		$targetBookingID = implode(",",$BookingID);
		
		$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID)";
		$result['BOOKING_RECORD'][] = $lebooking->db_db_query($sql);

		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
		$result['ROOM_BOOKING_DETAIL'][] = $lebooking->db_db_query($sql);

		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM;
		$result['ITEM_BOOKING_DETAIL'][] = $lebooking->db_db_query($sql);
		
		if ($plugin['door_access']) {
			$result['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($BookingID);
		}
					
		## Send Cancel Email Start ##		
		//2013-0905-1052-19073
		//$result['sendCancelBookingEmail'] = $lebooking->Send_Cancel_Booking_Email_Notification($targetBookingID);		
		$lebooking->Send_Cancel_Booking_Email_Notification($targetBookingID);
		## Send Cancel Email End ##
			
		if(!in_array(false,$result)) {
			$lebooking->Commit_Trans();		
			header("Location: index_list.php?ReturnMsgKey=CancelBookingSuccess");
			exit;
		} else {
			$lebooking->RollBack_Trans();
			header("Location: index_list.php?ReturnMsgKey=CancelBookingFailed");
			exit;
		}
	break;
	
	case "DeleteBookingRequest" :
		$targetBookingID = implode(",",$BookingID);
		
		$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID)";
		$result['BOOKING_RECORD'][] = $lebooking->db_db_query($sql);
			
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
		$result['ROOM_BOOKING_DETAIL'][] = $lebooking->db_db_query($sql);
		
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM;
		$result['ITEM_BOOKING_DETAIL'][] = $lebooking->db_db_query($sql);
		
		$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0, DateModified = now() WHERE BookingID IN ($targetBookingID)";
		$result['DELETE_BOOKING_RECORD'][] = $lebooking->db_db_query($sql);
		
	
		## Delete log information - INTRANET_EBOOKING_RECORD
		$sql = "SELECT BookingID, Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Event, Remark, RelatedTo, InputBy, ModifiedBy, DateInput, DateModified FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($targetBookingID)";
		$ierInfoAry =  $lebooking->returnArray($sql);
		$numOfInfo = count($ierInfoAry);
				
		for($i=0; $i<$numOfInfo; $i++) {
			$BookingID = $ierInfoAry[$i]['BookingID'];
			$Date = $ierInfoAry[$i]['Date'];
			$StartTime = $ierInfoAry[$i]['StartTime'];
			$EndTime = $ierInfoAry[$i]['EndTime'];
			$RequestedBy = $ierInfoAry[$i]['RequestedBy'];
			$ResponsibleUID = $ierInfoAry[$i]['ResponsibleUID'];
			$Event = $ierInfoAry[$i]['Event'];
			$Remark = $ierInfoAry[$i]['Remark'];
			$RelatedTo = $ierInfoAry[$i]['RelatedTo'];
			$InputBy = $ierInfoAry[$i]['InputBy'];
			$ModifiedBy = $ierInfoAry[$i]['ModifiedBy'];
			$DateInput = $ierInfoAry[$i]['DateInput'];
			$DateModified = $ierInfoAry[$i]['DateModified'];
			$section ='Delete_eService_Booking_Record';
			$tableName = 'INTRANET_EBOOKING_RECORD';
			
			# Start inserting delete log
			$ierTmpAry = array();
			$ierTmpAry['BookingID'] = $BookingID;
			$ierTmpAry['Date'] = $Date;
			$ierTmpAry['StartTime'] = $StartTime;
			$ierTmpAry['EndTime'] = $EndTime;
			$ierTmpAry['RequestedBy'] = $RequestedBy;
			$ierTmpAry['ResponsibleUID'] = $ResponsibleUID;
			$ierTmpAry['Event'] = $Event;
			$ierTmpAry['Remark'] = $Remark;
			$ierTmpAry['RelatedTo'] = $RelatedTo;
			$ierTmpAry['InputBy'] = $InputBy;
			$ierTmpAry['ModifiedBy'] = $ModifiedBy;
			$ierTmpAry['DateInput'] = $DateInput;
			$ierTmpAry['DateModified'] = $DateModified;
			
			$ierSuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eBooking', $section, $liblog->BUILD_DETAIL($ierTmpAry), $tableName, $BookingID);

		}
		
		## Delete log information - INTRANET_EBOOKING_BOOKING_DETAILS
		$sql = "SELECT RecordID, BookingID, FacilityType, FacilityID, PIC, ProcessDate, InputBy, ModifiedBy, DateInput, DateModified FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetBookingID)";
		$ebkReqInfoAry = $lebooking->returnArray($sql);
		$numOfInfo = count($ebkReqInfoAry);
		
		for($i=0; $i<$numOfInfo; $i++) {
			$ebkReqRecordID = $ebkReqInfoAry[$i]['RecordID'];
			$ebkReqBookingID = $ebkReqInfoAry[$i]['BookingID'];;
			$ebkReqFacilityType = $ebkReqInfoAry[$i]['FacilityType'];
			$ebkReqFacilityID = $ebkReqInfoAry[$i]['FacilityID'];
			$ebkReqPIC = $ebkReqInfoAry[$i]['PIC'];
			$ebkReqProcessDate = $ebkReqInfoAry[$i]['ProcessDate'];
			$ebkReqInputBy = $ebkReqInfoAry[$i]['InputBy'];
			$ebkReqModifiedBy = $ebkReqInfoAry[$i]['ModifiedBy'];
			$ebkReqDateInput = $ebkReqInfoAry[$i]['DateInput'];
			$ebkReqDateModified = $ebkReqInfoAry[$i]['DateModified'];	
			
			$section = 'Delete_eService_Booking_Details';
			$tableName = 'INTRANET_EBOOKING_BOOKING_DETAILS';
			
			# Start inserting delete log
			$tmpAry = array();
			$tmpAry['RecordID'] = $ebkReqRecordID;
			$tmpAry['BookingID'] = $ebkReqBookingID;
			$tmpAry['FacilityType'] = $ebkReqFacilityType;
			$tmpAry['FacilityID'] = $ebkReqFacilityID;
			$tmpAry['PIC'] = $ebkReqPIC;
			$tmpAry['ProcessDate'] = $ebkReqProcessDate;
			$tmpAry['InputBy'] = $ebkReqInputBy;
			$tmpAry['ModifiedBy'] = $ebkReqModifiedBy;
			$tmpAry['DateInput'] = $ebkReqDateInput;
			$tmpAry['DateModified'] = $ebkReqDateModified;	
			$result['Log_Delete'] = $liblog->INSERT_LOG('eBooking', $section, $liblog->BUILD_DETAIL($tmpAry), $tableName, $ebkReqRecordID);
		}

		if ($plugin['door_access']) {
			$result['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($BookingID);
		}
		
	
		if(!in_array(false,$result)) {
			$lebooking->Commit_Trans();			
			header("Location: index_list.php?ReturnMsgKey=DeleteBookingSuccess");
			exit;
		} else {
			$lebooking->RollBack_Trans();
			header("Location: index_list.php?ReturnMsgKey=DeleteBookingFailed");
			exit;
		}
	break;
}

intranet_closedb();
?>