<?php
// using: 

################################################## Change Log ##################################################
##  Date	:	2019-05-02 (Cameron)
##  Details     fix: change $keyword to $Keyword when apply cleanHtmlJavascript
##
##  Date	:	2018-08-15 (Henry)
##  Details     fixed XSS
##
##  Date	:   2018-08-09 (Henry)
##	Details	:	fixed sql injection
##
##	Date	:	2015-10-12	Omas
##	Details	:	added ShowRoomBookingRecordInMonthView ,ShowItemBookingRecordInMonthView
##
##	Date	:	2015-01-23 (Omas)
##	Details	:	-add SaveDayViewSetting , ShowRoomBookingRecordInDayView
##
##	Date	: 	2012-06-27 (Rita)
##	Details :   case "ShowRoomBookingRecordInListView" - add $Keyword
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "EditBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "CancelBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "DeleteBookingRequest"

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$BookingID = IntegerSafe($BookingID);
$FacilityType = IntegerSafe($FacilityType);

$Keyword = cleanHtmlJavascript($Keyword);
$numPerPage = cleanHtmlJavascript($numPerPage);

$lebooking = new libebooking();

$li = new libebooking_ui();

switch ($task)
{
	case "Get_Booking_Calender":
		include_once($PATH_WRT_ROOT."includes/libcal.php");
		include_once($PATH_WRT_ROOT."includes/libcalevent.php");
		
		$newts = mktime(0,0,0,$month,1,$year);
		
		$newYear =date("Y",$newts);
		$newMonth = date("m",$newts);
		$selected = explode(",",$selected);

		$FacilityID = IntegerSafe($FacilityID);
		$Calendar = $li->Get_Booking_Calendar($FacilityType,$FacilityID,$newMonth,$newYear,$selected);
		
		if($Calendar)
			echo $Calendar;
	break;

	case "Get_Booking_Timetable":
	    $FacilityID = IntegerSafe($FacilityID);
	    $WeekDiff = IntegerSafe($WeekDiff);
	    $From_eAdmin = IntegerSafe($From_eAdmin);
		$Timetable = $li->Get_Booking_Timetable($FacilityType, $FacilityID, $WeekDiff, $From_eAdmin);

		if($Timetable)
			echo $Timetable;
	break;	
	
	case "LoadMultiSubCatTable":
	    $CategoryID = IntegerSafe($CategoryID);
		if($CategoryID)	
		{
			$SubCatTable = $li->Get_Item_Sub_Category_Multi_Select_Table($CategoryID);
			echo $SubCatTable;
		} 
	break;	
	
	case "LoadBookingItemsList":
	    $CategoryID = IntegerSafe($CategoryID);
		if($CategoryID&&!$SubCatIDList)
			break;
			
		if($SubCatIDList)	
			$SubCatIDArr = explode(",",$SubCatIDList);
			$SubCatIDArr = IntegerSafe($SubCatIDArr);
		$ItemTable = $li->Get_New_Booking_Items_List_Table($CategoryID,$SubCatIDArr, $Keyword);
		echo $ItemTable;
	break;
	
	case "CheckTimeCrash":
	    $DateList = cleanHtmlJavascript($DateList);
	    $BookStartHour = cleanHtmlJavascript($BookStartHour);
	    $BookStartMin = cleanHtmlJavascript($BookStartMin);
	    $BookEndHour = cleanHtmlJavascript($BookEndHour);
	    $BookEndMin = cleanHtmlJavascript($BookEndMin);
	    
		$DateArr = explode(",",$DateList);
		$StartTimeArr[] = $BookStartHour.":".$BookStartMin.":00";
		$EndTimeArr[] = $BookEndHour.":".$BookEndMin.":00";
		
		$FacilityID = IntegerSafe($FacilityID);
		$TimeSelectTable = $li->Get_Specific_Time_Range_Date_Select_Table($FacilityType, $FacilityID, $DateArr, $StartTimeArr, $EndTimeArr);
		echo $TimeSelectTable;
	break;
	
	case "LoadItemBookingLayerTable":
	    $ItemID = IntegerSafe($_REQUEST['ItemID']);
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		# Get Booking Day Beforehand
		include_once($PATH_WRT_ROOT."includes/libebooking_item.php");
		$objItem = new eBooking_Item($ItemID);
		$BookingDayBeforehand = $objItem->BookingDayBeforehand;
		
		# Get Booking Info
		$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));
		$ItemBookingDetailsTable = $li->Get_Item_Booking_Details_Table($ItemID, $DateTimeArr);
		
		# Get Available Booking Period
		$DateArr = array_keys($DateTimeArr);
		$SinceDate = $DateArr[0];
		$UntilDate = $DateArr[count($DateArr)-1];
		$ItemAvailableBookingPeriodTable = $li->Get_Available_Booking_Period_Layer_Table($FacilityType=2, $ItemID, $SinceDate, $UntilDate);
		$thisReturn = '';
		$thisReturn .= $Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDayBeforehand'].': '.$BookingDayBeforehand.' '.$Lang['eBooking']['Settings']['GeneralPermission']['DayBefore'];
		$thisReturn .= '<span class="tabletextremark"> ('.$Lang['eBooking']['Settings']['GeneralPermission']['RemarksArr']['ZeroBookingDayBeforehandMeansNoLimit'].')</span>';
		$thisReturn .= '<br />';
		$thisReturn .= '<br />';
		$thisReturn .= '<div class="edit_bottom" style="height:1px;"></div>';
		$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['eService']['FieldTitle']['ExistingBookingRecord']);
		$thisReturn .= '<br />';
		$thisReturn .= $ItemBookingDetailsTable;
		$thisReturn .= '<br />';
		$thisReturn .= '<br />';
		$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod']);
		$thisReturn .= $ItemAvailableBookingPeriodTable;
		$thisReturn .= '<br />';
		
		echo $thisReturn;
	break;
	
	case "EditBookingRequest" :
		### Only can be edit if the booking status = pending
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);
				
				if(sizeof($arrRoomResult) > 0) {
					if($arrRoomResult[0] == 0) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if($arrFacilitiesResult[0] == 0) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}

				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}
			}
			if(!in_array(-1, $result)) {
				echo 1;
			}else{
				echo -1;
			}
		}
	break;
	
	case "CancelBookingRequest" :
		### Only can be cancel if the booking status = approved
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);
				
				
				if(sizeof($arrRoomResult) > 0) {
					if(($arrRoomResult[0] == 1)||($arrRoomResult[0] == -1)) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if(($arrFacilitiesResult[0] == 1)||($arrFacilitiesResult[0] == -1)) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}
				
				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}
			}
			if(!in_array(-1, $result)) {
				echo 1;
			}else{
				echo -1;
			}
		}
	break;
	
	case "DeleteBookingRequest" :
		### Only can be edit if the booking status = pending / deleted
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);		
				
				
				# Check delete 
				if(sizeof($arrRoomResult) > 0) {
					if(($arrRoomResult[0] == 0) || ($arrRoomResult[0] == -1)) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if(($arrFacilitiesResult[0] == 0)||($arrFacilitiesResult[0] == -1)) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}
				
				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}				
			}
					
			if(!in_array(-1, $result)) {
				echo 1;	
				
			}else{
				echo -1;
			}					
		}		
	break;
	
	case "Load_Remark":
	    $FacilityID = IntegerSafe($FacilityID);
		$layer_content = $li->Get_My_Booking_Record_Detail_Layer($BookingID, $FacilityType, $FacilityID);
		echo $layer_content; 
	break;
	
	case "Load_RejectReason":
	    $FacilityID = IntegerSafe($FacilityID);
		$layer_content = $li->Get_My_Booking_Record_Reject_Reason_Layer($BookingID, $FacilityType, $FacilityID);
		echo $layer_content; 
	break;	
	
	case "ShowRoomBookingRecordInDayView":
	    echo $li->ShowRoomBookingRecordInDayView(cleanHtmlJavascript($_REQUEST['targetDate']),IntegerSafe($_REQUEST['LocationID']),IntegerSafe($_REQUEST['BookingStatus']),IntegerSafe($_REQUEST['ManagementGroupID']),1);
	break;
	
	case "ShowRoomBookingRecordInWeekView":
	    $WeekStartTimeStamp = cleanHtmlJavascript($WeekStartTimeStamp);
	    if ($LocationID != 'ALL') {
	        $LocationID = IntegerSafe($LocationID);
	    }
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
	    $BookingStatus = IntegerSafe($BookingStatus);
	    
		$table_content = $li->ShowRoomBookingRecordInWeekView($WeekStartTimeStamp, $LocationID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ShowRoomBookingRecordInMonthView":
		$Month = $_POST['targetMonth'];
		$Year = $_POST['targetYear'];
		if ($LocationID != 'ALL') {
		    $LocationID = IntegerSafe($LocationID);
		}
		$ManagementGroupID = IntegerSafe($ManagementGroupID);
		$BookingStatus = IntegerSafe($BookingStatus);
		
		$table_content = $li->ShowRoomBookingRecordInMonthView($Month,$Year, $LocationID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ChangeDisplayWeek":
	    $WeekStartTimeStamp = cleanHtmlJavascript($WeekStartTimeStamp);
		$returnValue = $li->ChangeDisplayWeek($WeekStartTimeStamp, IntegerSafe($ChangeValue));
		echo $returnValue;
	break;
	
	case "ChangeDisplayWeekByCal":
	    $WeekStartDate = cleanHtmlJavascript($WeekStartDate);
		$returnValue = $li->ChangeDisplayWeekByCal($WeekStartDate);
		echo $returnValue; 
	break;
	
	case "ShowRoomBookingDetail":
		
//		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
//		$arrRelatedTo = $li->returnVector($sql);
//		$targetRelatedTo = implode(",",$arrRelatedTo);
//		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
//		$arrTargetBookingID = $li->returnVector($sql);
//		$targetBookingID = implode(",",$arrTargetBookingID);
//		
//		$sql = "SELECT 
//					DISTINCT booking_record.BookingID,
//					booking_record.Date, 
//					booking_record.StartTime, 
//					booking_record.EndTime, 
//					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
//					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
//					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("room_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
//					room_details.BookingStatus,
//					booking_record.Remark
//				FROM
//					INTRANET_EBOOKING_RECORD AS booking_record
//					LEFT JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_details ON (booking_record.BookingID = room_details.BookingID AND room_details.FacilityType=".LIBEBOOKING_FACILITY_TYPE_ROOM.")
//					LEFT JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID AND item_details.FacilityType=".LIBEBOOKING_FACILITY_TYPE_ITEM.")
//					LEFT JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
//					LEFT JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
//					LEFT JOIN INTRANET_USER AS room_pic ON (room_details.PIC = room_pic.UserID)
//					LEFT JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
//					LEFT JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
//					LEFT JOIN INVENTORY_LOCATION AS location ON (room_details.RoomID = location.LocationID)
//					LEFT JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
//					LEFT JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
//					LEFT JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
//					LEFT JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (room_details.RoomID = room_group.LocationID)
//					LEFT JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group2 ON (room_group.GroupID = mgmt_group2.GroupID)
//				WHERE
//					booking_record.BookingID IN ($targetBookingID)";
//		$arrResult = $li->returnArray($sql,7);
//		
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
//				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status, $booking_remark) = $arrResult[$i];
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				} 
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : ".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : ".$booking_responsible."</td>";
//		$layer_content .= "</tr>";
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_PIC."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']." : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		
//		$layer_content .= "</table>";
		$layer_content = $li->ShowRoomBookingDetail($BookingID);
		echo $layer_content;
	break;
	
	case "ShowRoomBookingRecordInListView":
	    if ($LocationID != 'ALL') {
	        $LocationID = IntegerSafe($LocationID);
	    }
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
	    $BookingStatus = IntegerSafe($BookingStatus);
	    $BookingDateType = cleanHtmlJavascript($BookingDateType);
		$table_content = $li->ShowRoomBookingRecordInListView($LocationID, $ManagementGroupID, $BookingStatus, $BookingDateType, $pageNo, $order, $numPerPage, $Keyword);
		
		echo $table_content ;
		
	break;
	
	case "ShowRoomBookingRemark":
		$table_content = $li->showBookingRemark($BookingID);
		echo $table_content; 
	break;
	
	case "ShowItemBookingRecordInWeekView":
	    $WeekStartTimeStamp = cleanHtmlJavascript($WeekStartTimeStamp);
	    $CategoryID = IntegerSafe($CategoryID);
	    $SubCategoryID = IntegerSafe($SubCategoryID);
	    $ItemID = IntegerSafe($ItemID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$ManagementGroupID = IntegerSafe($ManagementGroupID);
		$BookingStatus = IntegerSafe($BookingStatus);		
		$table_content = $li->ShowItemBookingRecordInWeekView($WeekStartTimeStamp, $CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	
	case "ShowItemBookingRecordInMonthView":
	    $CategoryID = IntegerSafe($CategoryID);
	    $SubCategoryID = IntegerSafe($SubCategoryID);
	    $ItemID = IntegerSafe($ItemID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$ManagementGroupID = IntegerSafe($ManagementGroupID);
		$BookingStatus = IntegerSafe($BookingStatus);
		$Month = cleanHtmlJavascript($_POST['targetMonth']);
		$Year = IntegerSafe($_POST['targetYear']);
		
		$table_content = $li->ShowItemBookingRecordInMonthView($Month,$Year, $CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ShowItemBookingDetail":
		
//		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
//		$arrRelatedTo = $li->returnVector($sql);
//		$targetRelatedTo = implode(",",$arrRelatedTo);
//		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
//		$arrTargetBookingID = $li->returnVector($sql);
//		$targetBookingID = implode(",",$arrTargetBookingID);
//		
//		$sql = "SELECT 
//					DISTINCT booking_record.BookingID,
//					booking_record.Date, 
//					booking_record.StartTime, 
//					booking_record.EndTime, 
//					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
//					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
//					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("item_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
//					item_details.BookingStatus,
//					booking_record.Remark
//				FROM
//					INTRANET_EBOOKING_RECORD AS booking_record
//					LEFT OUTER JOIN INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID)
//					LEFT OUTER JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
//					LEFT OUTER JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
//					LEFT OUTER JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
//				WHERE
//					booking_record.BookingID IN ($targetBookingID)";
//		$arrResult = $li->returnArray($sql,7);
//		
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
//				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status, $booking_remark) = $arrResult[$i];
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				}
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : ".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : ".$booking_responsible."</td>";
//		$layer_content .= "</tr>";
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_PIC."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>Remark : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		
//		$layer_content .= "</table>";
		$layer_content = $li->ShowItemBookingDetail($BookingID);
		
		echo $layer_content;
	break;
	
	case "ShowItemBookingRecordInListView":
	    $CategoryID = IntegerSafe($CategoryID);
	    $SubCategoryID = IntegerSafe($SubCategoryID);
	    $ItemID = IntegerSafe($ItemID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$ManagementGroupID = IntegerSafe($ManagementGroupID);
		$BookingStatus = IntegerSafe($BookingStatus);
		$BookingDateType = cleanHtmlJavascript($BookingDateType);
		$table_content = $li->ShowItemBookingRecordInListView($CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus, $BookingDateType, $pageNo, $order, $numPerPage, $Keyword);
		echo $table_content ;		
	break;
	
	case "ShowFollowUpWorkInWeekView":
		$table_content .= $li->ShowFollowUpWorkInWeekView($WeekStartTimeStamp,$FollowUpGroup,$RemarkSelection);
		echo $table_content; 
	break;
	
	case "ShowFollowUpDetail":
//		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
//		$arrRelatedTo = $li->returnVector($sql);
//		$targetRelatedTo = implode(",",$arrRelatedTo);
//		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
//		$arrTargetBookingID = $li->returnVector($sql);
//		$targetBookingID = implode(",",$arrTargetBookingID);
//		
//		$sql = "SELECT 
//					DISTINCT booking_record.BookingID,
//					booking_record.Date, 
//					booking_record.StartTime, 
//					booking_record.EndTime, 
//					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
//					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
//					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("room_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
//					room_details.BookingStatus
//				FROM
//					INTRANET_EBOOKING_RECORD AS booking_record
//					LEFT OUTER JOIN INTRANET_EBOOKING_ROOM_BOOKING_DETAILS AS room_details ON (booking_record.BookingID = room_details.BookingID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID)
//					LEFT OUTER JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
//					LEFT OUTER JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
//					LEFT OUTER JOIN INTRANET_USER AS room_pic ON (room_details.PIC = room_pic.UserID)
//					LEFT OUTER JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
//					LEFT OUTER JOIN INVENTORY_LOCATION AS location ON (room_details.RoomID = location.LocationID)
//					LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
//					LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (room_details.RoomID = room_group.LocationID)
//					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group2 ON (room_group.GroupID = mgmt_group2.GroupID)
//				WHERE
//					booking_record.BookingID IN ($targetBookingID)";
//		$arrResult = $li->returnArray($sql,8);
//		
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
//				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status) = $arrResult[$i];
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				} 
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : ".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : ".$booking_responsible."</td>";
//		$layer_content .= "</tr>";
//		/*
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_PIC."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']." : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		*/
//		$layer_content .= "</table>";
		$layer_content = $li->ShowFollowUpDetail($BookingID);
		echo $layer_content;
	break;
	
	case "ShowDescription":
		if($FacilityType == 1){
		    $FacilityID = IntegerSafe($FacilityID);
			$sql = "SELECT Description FROM INVENTORY_LOCATION WHERE LocationID = '$FacilityID'";
			$arrDesc = $li->returnVector($sql);
						
			$tableContent .= '<table border="0" border="0" cellpadding="3" cellspacing="0" width="350px">';
			$tableContent .= '<tr class="tabletop"><td>'.$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Description'].'</td></tr>';
			$tableContent .= '<tr class="tablerow1"><td>'.$arrDesc[0].'</td></tr>';
			$tableContent .= '</table>';
			
			
			### Hide Layer Button
			$layerContent .= '<table cellspacing="0" cellpadding="3" border="0">'."\n";
				$layerContent .= '<tbody>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="right" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<a href="javascript:js_Hide_Information_Layer(\'DIV_DisplayInformation\')"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="left" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<div id="DetailsLayerContentDiv">'.$tableContent.'</div>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
				$layerContent .= '</tbody>'."\n";
			$layerContent .= '</table>'."\n";			
		}
		echo $layerContent;
	break;
	
	case "GetRoomAttachment":
		$layer_content = "";
		if($FacilityType==1){
		    $FacilityID = IntegerSafe($FacilityID);
			$sql = "SELECT Attachment FROM INVENTORY_LOCATION WHERE LocationID = '$FacilityID' ";
			$attachment_arr = $li->returnVector($sql);
			
			if(sizeof($attachment_arr)>0 && trim($attachment_arr[0])!=''){
				//$attachment_link = $PATH_WRT_ROOT."home/download_attachment.php?target=".$intranet_root."/file/ebooking/room/r".$FacilityID."/".urlencode($attachment_arr[0]);
				$encryptedPath = getEncryptedText($intranet_root."/file/ebooking/room/r".$FacilityID."/".$attachment_arr[0]);
				$attachment_link = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$encryptedPath;
				$layer_content = "<a href=\"".$attachment_link."\" target=\"_blank\"><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif' border='0' align=\"absmiddle\" valign=\"2\">".$attachment_arr[0]."</a>";
			}
		}
		echo $layer_content;
	break;
}

intranet_closedb();
die;
?>