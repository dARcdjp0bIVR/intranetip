<?php
// using : 
/*
 *  2019-05-02 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 *      - apply cleanHtmlJavascript to variables to avoid cross site attack
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$lebooking_ui = new libebooking_ui();
//$CurrentPageArr['eBooking'] = 1;
$isPrint = 1;
$include_JS_CSS = $lebooking_ui->initJavaScript();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

if(sizeof($BookingStatus) > 0) {
    $BookingStatus = IntegerSafe($BookingStatus);
	$BookingStatusArr = explode(",",$BookingStatus); 
	$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
	$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
	$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
	$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

	$booking_status_content = '<table border="0">';
	$booking_status_content .= '<tr><td>'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'].' : </td>';
	if($show_wait){
		$booking_status_content .= '<td><span class="tablelink" style="border:1px solid; padding:1px;" >'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'].'</td>';
	}
	if($show_approve){
		$booking_status_content .= '<td><span  style="border:1px solid; padding:1px; color: rgb(0, 153, 0);">'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'].'</td>';
	}
	$booking_status_content .= '</tr>';
	$booking_status_content .= '</table>';
}
else {
    $BookingStatus = '';
}

if ($LocationID != "ALL") {
    $LocationID = IntegerSafe($LocationID);
}
$ManagementGroupID = IntegerSafe($ManagementGroupID);

$WeekStartTimeStamp = cleanHtmlJavascript($WeekStartTimeStamp);
$print_content = $lebooking_ui->Show_Room_Booking_Week_Print($WeekStartTimeStamp,$LocationID,$ManagementGroupID,$BookingStatus);
?>
<?=$include_JS_CSS;?>

<br>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print">
			<?=$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['ReportTitle']['RoomBookingWeek'];?>
		</td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$booking_status_content;?>
		</td>
	</tr>
</table>

<br>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$print_content;?>
		</td>
	</tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>