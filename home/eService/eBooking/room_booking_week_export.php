<?php
// using : 
/*
 *  2019-05-02 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 *      - apply cleanHtmlJavascript to variables to avoid cross site attack
 *      - fix LocationID is from $_GET value
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();

$CurrentPageArr['eBooking'] = 1;

// Empty row
$Rows[] = array('');

$LocationID = $_GET['LocationID'];
$ManagementGroup = $_POST['ManagementGroup'];
$BookStatus = $_POST['BookStatus'];
$WeekStartTimeStamp = $_POST['WeekStartTimeStamp'];

// Addition information
if($LocationID == "ALL"){
	$tempRows = array($Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'],
					$Lang['SysMgr']['Location']['All']['Floor']);
} else {
    $LocationID = IntegerSafe($LocationID);
	$sql = "SELECT CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").") AS LocationName FROM INVENTORY_LOCATION_BUILDING as building INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID) WHERE building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1 AND room.LocationID = '$LocationID'";
	$arrLocationInfo = $lebooking_ui->returnVector($sql);
	
	$tempRows = array($Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'],
						$arrLocationInfo[0]);
}
$Rows[] = $tempRows;

if($ManagementGroup == "")
{
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
						$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup']);
} else {
	if($ManagementGroup == "-999"){
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup']);
	}else{
	    $ManagementGroup = IntegerSafe($ManagementGroup);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroup'";
		$arrMgmtGroupInfo = $lebooking_ui->returnVector($sql);
		
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$arrMgmtGroupInfo[0]);
	}
}
$Rows[] = $tempRows;

if(sizeof($BookStatus) > 0) {
    $BookingStatusList = IntegerSafe($BookingStatusList);
	$BookingStatusArr = explode(",",$BookingStatusList); 
	$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
	$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
	$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
	$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

	if($show_wait){
		$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'];
	}
	if($show_approve){
		$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'];
	}
	$str_booking_status = implode(" , ",$booking_status_content);
	$tempRows = array($Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'],
						$str_booking_status);
	$Rows[] = $tempRows;
}
else {
    $BookingStatusList = '';
}

// Empty row
$Rows[] = array('');

$exportColumn = array($Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['ReportTitle']['RoomBookingWeek'],'','','','','','','');

$WeekStartTimeStamp = cleanHtmlJavascript($WeekStartTimeStamp);
// debug_pr($LocationID);
// exit;
$csv_content = $lebooking_ui->Show_Room_Booking_Week_Export($WeekStartTimeStamp,$LocationID,$ManagementGroup,$BookingStatusList);

// combine additional information and main content
$final_csv_content = array_merge($Rows,$csv_content);

$filename = "room_booking_week.csv";
$export_content = $lexport->GET_EXPORT_TXT($final_csv_content, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>