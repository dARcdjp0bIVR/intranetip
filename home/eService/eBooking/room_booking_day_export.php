<?php
// using : 
############################ Change Log ###########################
##
##  Date    : 2019-05-02 (Cameron)
##          - fix potential sql injection problem by cast related variables to integer and enclosed them with apostrophe
##          - apply cleanHtmlJavascript to variables to avoid cross site attack
##
##	Date	: 2015-04-21 (Omas)
##			Create this page
##	
###################################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();

$CurrentPageArr['eBooking'] = 1;

// Empty row
$Rows[] = array('');

// Addition information
if($LocationID == "ALL" || $LocationID == ""){
	$LocationID = "ALL";
	$tempRows = array($Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'],
					$Lang['SysMgr']['Location']['All']['Floor']);
} else {
    $LocationID = IntegerSafe($LocationID);
	$sql = "SELECT CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").") AS LocationName FROM INVENTORY_LOCATION_BUILDING as building INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID) WHERE building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1 AND room.LocationID = '$LocationID'";
	$arrLocationInfo = $lebooking_ui->returnVector($sql);
	
	$tempRows = array($Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'],
						$arrLocationInfo[0]);
}
$Rows[] = $tempRows; 

if($ManagementGroupID == "")
{
	$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
						$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup']);
} else {
	if($ManagementGroupID == "-999"){
		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup']);
	}else{
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroupID'";
		$arrMgmtGroupInfo = $lebooking_ui->returnVector($sql);

		$tempRows = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
							$arrMgmtGroupInfo[0]);
	}
}
$Rows[] = $tempRows;

$BookingStatus = IntegerSafe($_REQUEST['BookingStatusList']); 
$BookingStatusArr = explode(",",$BookingStatus); 
$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

if($show_wait){
	$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'];
}
if($show_approve){
	$booking_status_content[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'];
}
$str_booking_status = implode(" , ",$booking_status_content);
$tempRows = array($Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'],
						$str_booking_status);
$Rows[] = $tempRows;

$BookingDateType = cleanHtmlJavascript($BookingDateType);
switch ($BookingDateType)
{
	case "Future":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest'];
	break;
	
	case "ThisWeek":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisWeek'];
	break;
	
	case "NextWeek":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextWeek'];
	break;
	
	case "ThisMonth":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisMonth'];
	break;
	
	case "NextMonth":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextMonth'];
	break;
	
	case "Past":
		$DateType_info = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest'];
	break;
}
$tempRows = array($Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['RequestDate'],
					$TargetDate);
$Rows[] = $tempRows;

// Empty row
$Rows[] = array('');

$exportColumn = array($Lang['eBooking']['Management']['FieldTitle']['RoomBooking'],'','','','','','','','','','','','','','');

// csv main content
$csv_content = $lebooking_ui->Show_Room_Booking_Day_Export($TargetDate,$LocationID,$ManagementGroupID,$BookingStatus,$BookingDateType);

// combine additional information and main content
$final_csv_content = array_merge($Rows,$csv_content);

$export_content = $lexport->GET_EXPORT_TXT($final_csv_content, $exportColumn);
$filename = "room_booking_day".$TargetDate.".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>