<?php
// using : 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lebooking = new libebooking();
$lebooking->Check_Page_Permission($AdminOnly=0);

$Keyword = trim(stripslashes($_POST['Keyword']));
$FacilityType = trim(stripslashes($_POST['FacilityType']));
$BookingStatusList = trim(stripslashes($_POST['BookingStatusList']));
$BookingDateStatus = trim(stripslashes($_POST['BookingDateStatus']));


$exportHeaderAry = $lebooking->Get_Booking_Record_Export_Header();
$exportDataAry = $lebooking->Get_Booking_Record_Export_Content($Keyword, $FacilityType, $BookingStatusList, $BookingDateStatus);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $exportHeaderAry, "", "\r\n", "", 0, "11", $includeLineBreak=1);

		
intranet_closedb();
$filename = "my_booking_record.csv";
$lexport->EXPORT_FILE($filename, $export_content);
?>