<?php
// using : 
/*
 *  2019-05-02 Cameron
 *  - fix potential sql injection problem by cast related variables to integer and enclosed them with apostrophe
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "eService_MyBookingRecord";
$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

if(isset($_POST['BookingID'])) {
	
	$targetBookingID = implode(",",$_POST['BookingID']);
	
	$sql = "SELECT Date FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$targetBookingID'";
	$arrBookingDate = $lebooking_ui->returnVector($sql);
	
	if(sizeof($arrBookingDate)>0){
		$DateList = $arrBookingDate[0];
	}
	
	$sql = "SELECT RoomID FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = '$targetBookingID'";
	$arrRoomBookingResult = $lebooking_ui->returnVector($sql);
	
	if(sizeof($arrRoomBookingResult) > 0) {
		$FacilityID = $arrRoomBookingResult[0];
		$FacilityType = 1;
	} else {
		$sql = "SELECT ItemID FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = '$targetBookingID'";
		$arrItemBookingResult = $lebooking_ui->returnVector($sql);
		if(sizeof($arrItemBookingResult) > 0){
			$FacilityID = implode(",",$arrItemBookingResult);
			$FacilityType = 2;
		}
	}
}

$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService=1);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$FacilityType = $FacilityType? IntegerSafe($FacilityType) : 1;
$FacilityID = $FacilityID? $FacilityID : 0;
$BookingMethod = $BookingMethod?IntegerSafe($BookingMethod):1;
$From_eAdmin=0;


echo $lebooking_ui->Get_New_Booking_Step1_UI($FacilityType, $FacilityID, $DateList, $targetBookingID,$From_eAdmin,$BookingMethod);

echo $lebooking_ui->initJavaScript();
?>

<script language="javascript">
var date = new Date();
var CurrMonth = date.getMonth()+1;
var CurrYear = date.getFullYear();
var SelectedAry = new Array();
var SkipRemoveDate = true; //Skip Remove 

function ChangeMonth(prevnext)
{
	if($("#FacilityID").val()!='')
		$("#CalendarView").show();
	
	var newMonth = parseInt(CurrMonth)+prevnext;
	
	$.post(
		"ajax_task.php",
		{
			task:"Get_Booking_Calender",
			FacilityID: $("#FacilityID").val(),
			FacilityType: <?=$FacilityType?>,
			year:CurrYear,
			month:newMonth,
			From_eAdmin:<?=$From_eAdmin?>,
			selected: SelectedAry.toString()
		},
		function (data)
		{
			if(data)
			{
				if(newMonth==13)
				{
					CurrMonth = 1;
					CurrYear += 1;
				}
				else if(newMonth==0)
				{
					CurrMonth = 12;
					CurrYear -= 1;
				}
				else
				{
					CurrMonth = newMonth;
				}

				$("div#CalendarWrapper").html(data);
				
				if(prevnext==0 && !SkipRemoveDate) // changed room/item
					removeAllDate();
					
				SkipRemoveDate = false;
					
			}
		}
	);
}

function addDate(datestr,Cycle,day)
{
	if(Cycle=='') Cycle='--';
	if($("tr#"+datestr).length==0)
	{
		var x = '';
		x +='<tr class="row_approved" id="'+datestr+'">';
			//x +='<td width="20" align="center" valign="top" nowrap>1</td>';
			x +='<td valign="top" nowrap>'+datestr+'</td>';
			x +='<td valign="top" nowrap>('+day+')</td>';
			x +='<td align="center" valign="top" nowrap>'+Cycle+'</td>';
			x +='<td valign="top" nowrap><a href="javascript:void(0); removeDate(\''+datestr+'\')"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove"></a></td>';
			x +='<input type="hidden" name="SelectedDateArr[]" value="'+datestr+'">';
		x +='</tr>';
		
		$("table#selectedTable>tbody").append(x);
	}
	showHideSelectedTable();
}

function removeDate(datestr)
{
	$("tr#"+datestr).remove();
	showHideSelectedTable();
}

function removeAllDate()
{
	$("table#selectedTable>tbody>tr:not(:first)").remove();
	showHideSelectedTable();
}

function showHideSelectedTable()
{
	if($("table#selectedTable>tbody>tr").length==1)
		$("table#selectedTable").hide();
	else
		$("table#selectedTable").show();
}

function refreshItemCalendar(ItemID,ItemName)
{
	$("input#FacilityID").val(ItemID);
	$("span#ItemName").html(ItemName);
	$("span#SelectEdit").html("<?=$Lang['eBooking']['eService']['Edit']?>");
	
	js_Load_Calendar();
}

function ConfirmChange()
{
	if($("table#selectedTable>tbody>tr").length>1)
	{	if(!confirm("<?=$Lang['eBooking']['eService']['jsMsg']['ConfirmRemoveDates']?>")) 
			return false;
	}
	return true;
}

function CheckForm()
{
	if(!$("#FacilityID").val())
	{
		warnMsg = "<?=$FacilityType==2?$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectRoom']:$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectItem']?>";
		
		alert(warnMsg);
		return false;
	}
	
	if($("table#selectedTable>tbody>tr").length==1)
	{
		alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectDate']?>");
		return false;
	}
	
	
	return true;
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('frm1');
	objForm.action = 'index_list.php';
	objForm.submit();
}

function ShowInformation(targetLayer)
{
	var p = $("#ShowInfo").position();
	var div_left = p.left;
	var div_top = p.top;
	
	$("div#"+targetLayer).css( {"left":(div_left+20)+"px", "top":(div_top)+"px"} );
	$("div#"+targetLayer).show().fadeIn();
	$("div#"+targetLayer).html("<?=$Lang['General']['Loading'];?>");
	
	$.post(
		"ajax_task.php",
		{
			task : "ShowDescription",
			FacilityType: <?=$FacilityType?>,
			FacilityID: $("#FacilityID").val()
		},
		function (data)
		{
			//$("div#"+targetLayer).css( {"left":(div_left+20)+"px", "top":(div_top)+"px"} );
			//$("div#"+targetLayer).show().fadeIn();
			$("div#"+targetLayer).html(data);
		}
	)
}

function ShowAttachment()
{
	$('#DIV_Attachment').load(
		"ajax_task.php",
		{
			task : "GetRoomAttachment",
			FacilityType: $("#FacilityType").val(),
			FacilityID: $("#FacilityID").val()
		},
		function(data){
			
		}
	);
}

function js_Hide_Information_Layer(targetLayer)
{
	$("div#"+targetLayer).hide().fadeOut();
	$("div#"+targetLayer).html('');
}

$(document).ready( function() {
	js_Load_Calendar();
	ShowAttachment();
	//Thick_Box_Init()
});

//function Thick_Box_Init(){
//	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
//}

function js_Change_Booking_Method()
{
	if(!ConfirmChange()) return false; 
	document.frm1.action=''; 
	document.frm1.submit()
}

function js_Load_Calendar()
{
	if($("#BookingMethod").val()==1)
		ChangeWeek(0);
	else
		ChangeMonth(0);
}

var WeekDiff = 0;
function ChangeWeek(prevnext)
{
	if($("#FacilityID").val()!='')
		$("#CalendarView").show();
	
	Block_Element("CalendarWrapper");
	
	NewWeek = WeekDiff+parseInt(prevnext)	

	$.post(
		"ajax_task.php",
		{
			task:"Get_Booking_Timetable",
			FacilityID: $("#FacilityID").val(),
			FacilityType: <?=$FacilityType?>,
			WeekDiff:NewWeek,
			From_eAdmin:<?=$From_eAdmin?>
		},
		function (data)
		{
			if(data)
			{
				$("#CalendarWrapper").html(data)
				WeekDiff += parseInt(prevnext)	
				js_Sync_Selected_Timeslot();
				UnBlock_Element("CalendarWrapper");
			}
		}
	);
}

function js_Check_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID, thisCheck)
{
	if(thisCheck)
		js_Add_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID);
	else
		js_Remove_Timeslot(Timestamp, TimeslotID);
}

var SelectSlot = new Object();
function js_Add_Timeslot(SelectedDate, Timestamp, TimeslotDisplay, TimeslotID)
{
	if(!SelectSlot[Timestamp])
		SelectSlot[Timestamp] = new Object();
	if(!SelectSlot[Timestamp][TimeslotID])
		SelectSlot[Timestamp][TimeslotID] = new Object();
	
	SelectSlot[Timestamp][TimeslotID].DateDisplay = SelectedDate;
	SelectSlot[Timestamp][TimeslotID].TimeslotDisplay = TimeslotDisplay;
	SelectSlot[Timestamp][TimeslotID].selected = true;
	
	js_Refresh_Selected_Slot_Table();
}

function js_Remove_Timeslot(Timestamp, TimeslotID)
{
	if(!SelectSlot[Timestamp][TimeslotID])
		return false;
	
	SelectSlot[Timestamp][TimeslotID].selected = false;
	$(".slot_"+Timestamp+"_"+TimeslotID).attr("checked","");
	
	js_Refresh_Selected_Slot_Table();
}

function js_Remove_All_Timeslot()
{
	if(confirm("<?=$Lang['eBooking']['eService']['JSWarning']['ConfirmDeleteSelectedSlot']?>"))
	{
		SelectSlot = new Object();
		$(".slot").attr("checked","");
		
		js_Refresh_Selected_Slot_Table();
	}
}

function js_Refresh_Selected_Slot_Table()
{
	var Empty = true;
	
	var html = '';
	 html += '<table width="100%" border="0" cellspacing="0" cellpadding="0" id="selectedTable">';
		html += '<tr class="tabletop">';
			html += '<td><?=$Lang['eBooking']['eService']['FieldTitle']['SelectedDate']?></td>';
			html +=  '<td><?=$Lang['SysMgr']['Timetable']['TimeSlot']?></td>';
			html +=  '<td ><a href="javascript:void(0);" onclick="js_Remove_All_Timeslot()"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove All"></a></td>';
		html +=  '</tr>';
	for(Timestamp in SelectSlot)
	{
		for(TimeslotID in SelectSlot[Timestamp]) 
		{
			if(SelectSlot[Timestamp][TimeslotID].selected==true)
			{
				html += '<tr class="row_approved"  >';
					html += '<td>'+SelectSlot[Timestamp][TimeslotID].DateDisplay+'</td>';
					html +=  '<td>'+SelectSlot[Timestamp][TimeslotID].TimeslotDisplay+'</td>';
					html +=  '<td ><a href="javascript:void(0);" onclick="js_Remove_Timeslot('+Timestamp+','+TimeslotID+')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_delete_b.gif" width="20" height="20" border="0" title="Remove"></a></td>';
				html +=  '</tr>';
				Empty = false;
			}
		}
	}
	html += '</table>';
	
	if(Empty)
		html='';
	
	$("#SelectedSlotTableDiv").html(html)
	
}

function js_Sync_Selected_Timeslot()
{
	for(Timestamp in SelectSlot)
	{
		for(TimeslotID in SelectSlot[Timestamp]) 
		{
			if(SelectSlot[Timestamp][TimeslotID].selected)
				$(".slot_"+Timestamp+"_"+TimeslotID).attr("checked","checked");
		}
	}
	
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>