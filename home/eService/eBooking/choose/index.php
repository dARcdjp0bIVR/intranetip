<?php
# using: ivan

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
					
intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['Group']['UserList'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

//$permitted[] = 2;
$permitted = array(1,2,3,4); //accept all type of users

$luser = new libuser();

$li = new libgrouping();
$lo = new libgroup($GroupID);
$lclass = new libclass();

$teaching=0;
if($CatID < 0){
     unset($ChooseGroupID);
     //$ChooseGroupID[0] = 0-$CatID;
     switch($CatID){
	     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
	     case -3 : $ChooseGroupID[0]=3; $teaching=0;break;
	     
	     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
	     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
	     default:$ChooseGroupID[0]=1; $teaching=1;
	 }
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

//2013-1025-1212-18177
//$row = $lo->returnRoleType(1);

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=\"CatID\" onChange=\"checkOptionNone(this.form.elements['ChooseGroupID[]']);this.form.submit()\">\n" : "<select name=\"CatID\" onChange=\"this.form.submit()\"s>\n";
$x1 .= "<option value=\"0\"></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";


$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
//if (in_array(3,$permitted))
$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
    
$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value=$GroupCatID";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if(isset($ChooseGroupID)) {
	if($EnrolGroupID)
		$userCond = " EnrolGroupID = '$EnrolGroupID' ";
		
	$existing_user = $lo->returnGroupUser('',$userCond);
	
	foreach((array)$existing_user as $data)
		$except_users[] = $data['UserID'];  
	
	if($CatID<0)
	{
	    if($CatID==-3 && $classname)
	    {
		    # retrieve student list first
		    $StudentList = $lclass->getClassStudentList($classname);
		    $StudentIDStr = implode(",",$StudentList);
		    $row = $luser->getParent($StudentIDStr);
// 		    debug_pr(sizeof($row));
	    }
	    else
	    {
			$row = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching);
		}
	}
	else
	{
   		$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
		}
   	
     $x3  = "<select id='ChosenUserID' name=ChooseUserID[]>\n";
     for($i=0; $i<sizeof($row); $i++)
     {
     	if(!in_array($row[$i][0],(array)$except_users))
     	$x3 .= "<option id='ChosenUserID_".$row[$i][0]."' value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     }
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
     
	$add_btn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])");
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
?>

<script language="javascript">
function AddOptions(obj){
	
	var SelectedUserID = $('select#ChosenUserID').val();
	var SelectedUserName = $('option#ChosenUserID_' + SelectedUserID).html();
	
	window.opener.UpdateResponsiblePerson(SelectedUserID, SelectedUserName);
	window.close();
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function add_role(){
     Obj = window.document.form1;
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          Obj.elements["role"].value = role_name;

          Obj.action = "../info/add_role.php";
          Obj.submit();
     }
}
</script>

<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_category?></span>
					</td>
					<td ><?=$x1?></td>
				</tr>
				<? if($CatID!=0 && $CatID > 0) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_group?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td><?=$x2?></td>
								<td>
									<!-- <?= $linterface->GET_SMALL_BTN($button_add, "button","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?> -->
									<?= $linterface->GET_SMALL_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
									<?= $linterface->GET_SMALL_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				
				<? 
				# 2009-12-22 yatwoon 
				# if Group category is "Parent", then display class filter (requested by UCCKE)
				if($CatID==-3) 
				{ 
					$ClassSelection = $lclass->getSelectClass("name='classname' onChange='this.form.submit();'", $classname, "", $i_general_all_classes);
					?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_UserParentLink_SelectClass?></span>
					</td>
					<td>
						<?=$ClassSelection?>
					</td>
				</tr>
				<? } ?>
				
				<? if(isset($ChooseGroupID)) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_user?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td valign='top'><?=$x3?></td>
								<td>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $add_btn ?>
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name=role value="">
<input type=hidden name=fieldname value="<?=$fieldname?>">
<input type=hidden name=GroupID value="<?=$GroupID?>">
<input type=hidden name=EnrolGroupID value="<?=$EnrolGroupID?>">

</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>