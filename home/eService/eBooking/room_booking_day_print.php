<?php
// using : 
############################ Change Log ###########################
##
##  Date    : 2019-05-02 (Cameron)
##          - fix potential sql injection problem by cast related variables to integer and enclosed them with apostrophe
##
##	Date	: 2015-01-23 (Omas)
##			Create this page
##	
###################################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$lebooking_ui = new libebooking_ui();
$CurrentPageArr['eBooking'] = 1;
$isPrint = 1;
$include_JS_CSS = $lebooking_ui->initJavaScript();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$location_info = '<table border="0">';
$location_info .= '<tr><td>'.$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'].' : </td>';
if($LocationID == ""){
	$location_info .= '<td><span>'.$Lang['SysMgr']['Location']['All']['Floor'].'</span></td></tr>';
} else {
    $LocationID = IntegerSafe($LocationID);
	$sql = "SELECT NameChi,NameEng from INVENTORY_LOCATION_BUILDING where BuildingID = '$LocationID'";
	$arrLocationInfo = $lebooking_ui->returnResultSet($sql);
	$location_displayName = Get_Lang_Selection($arrLocationInfo[0]['NameChi'],$arrLocationInfo[0]['NameEng']);
	$location_info .= '<td><span>'.$location_displayName.'</span></td></tr>';
}
$location_info .= "</table>";

$MgmtGroup_info = '<table border="0">';
$MgmtGroup_info .= '<tr><td>'.$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'].' : </td>';
if($ManagementGroupID == "")
{
	$MgmtGroup_info .= '<td><span>'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup'].'</span></td></tr>';
} else {
	if($ManagementGroupID == "-999"){
		$MgmtGroup_info .= '<td><span>'.$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup'].'</span></td></tr>';
	}else{
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroupID'";
		$arrMgmtGroupInfo = $lebooking_ui->returnVector($sql);
		$MgmtGroup_info .= '<td><span>'.$arrMgmtGroupInfo[0].'</span></td></tr>';
	}
}
$MgmtGroup_info .= "</table>";

$BookingStatus = IntegerSafe($BookingStatus);
if(sizeof($BookingStatus) > 0) {    
	$BookingStatusArr = explode(",",$BookingStatus); 
	$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
	$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
	$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
	$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";

	$booking_status_content = '<table border="0">';
	$booking_status_content .= '<tr><td>'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'].' : </td>';
	if($show_wait){
		$booking_status_content .= '<td><span class="tablelink" style="border:1px solid; padding:1px;" >'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'].'</td>';
	}
	if($show_approve){
		$booking_status_content .= '<td><span  style="border:1px solid; padding:1px; color: rgb(0, 153, 0);">'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'].'</td>';
	}
	$booking_status_content .= '</tr>';
	$booking_status_content .= '</table>';
}

$addition_info = $location_info.$MgmtGroup_info.$booking_status_content;

$targetDate = $_GET['targetDate'];
if (!intranet_validateDate($targetDate)) {
    $targetDate = date('Y-m-d');
}

$print_content = $lebooking_ui->ShowRoomBookingRecordInDayView($targetDate,$LocationID,$BookingStatus,$ManagementGroupID,1);

?>
<?=$include_JS_CSS;?>
<style type="text/css">

	
	.bookingStatus_1 {
	    color: #4db308;
	    border-top: 1px solid #4db308;
	    border-bottom: 1px solid #4db308;
	    border-right: 1px solid #4db308;
	    border-left: 1px solid #4db308;
	}
	
	.bookingStatus_0 {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}
	
	.bookingStatus_-1 {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}
	
</style>


<br>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print">
			<?=$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['ReportTitle']['RoomBookingWeek'];?>&nbsp;(<?=$targetDate?>)
		</td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$addition_info;?>
		</td>
	</tr>
</table>

<br>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$print_content;?>
		</td>
	</tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>