<?php
// using : 
/*
 *  2019-05-02 Cameron
 *  - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "eService_MyBookingRecord";
$linterface 	= new interface_html();
//$linventory	= new libinventory();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService=1);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
$DateList = cleanHtmlJavascript($_REQUEST['DateList']);
$RoomID = $_REQUEST['RoomID'];

//$TimeRangeArr[$index] = $StartTime.'_'.$EndTime.'_'.$TimeSlotID; (TimeSlotID is optional)
// e.g. '07:00:00_08:30:00_128' or '07:00:00_08:30:00'
if($SelectMethod == "Specific")
	$TimeRangeArr = $_REQUEST['SpecificTimeRangeArr'];
else
	$TimeRangeArr = $_REQUEST['TimeRangeArr'];
	
$DateArr = explode(',', $DateList);

list($StartTimeArr, $EndTimeArr, $StartTime_TimeSlotID_Mapping_Arr) = $lebooking_ui->Get_StartTime_EndTime_TimeSlotID_Array_By_TimeRange_Array($TimeRangeArr);

$TmpTimeArr = $lebooking_ui->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly=0, $BookingStatusArr=array(1));
$DateTimeArr = $TmpTimeArr[0];

echo $lebooking_ui->Get_New_Booking_Step3_UI($DateTimeArr, $FacilityType, $RoomID, $DateList, $StartTime_TimeSlotID_Mapping_Arr, $TimeRangeArr, $CurrentBookingID);

?>

<script language="javascript">
$(document).ready( function() {
	//MM_showHideLayers('ItemBookingDetailsLayer','','show');
});

function js_Show_Event_Title()
{
	if($("#iCalEvent").attr('checked')) {
		$("#div_iCalInfo").show();
	} else {
		$("#div_iCalInfo").hide();
	}
}

function js_Go_Back_Step2()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'new_booking_2.php';
	objForm.submit();
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'index_list.php';
	objForm.submit();
}

function js_Go_New_Room_Booking()
{
	if($("#iCalEvent").attr('checked')) {

		if($("#iCalEventTitle").val() != "") {
			var objForm = document.getElementById('form1');
			objForm.action = 'new_booking_update.php';
			objForm.submit();
		} else {
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'];?>");
			$("#iCalEventTitle").focus();
		}
	} else {
		var objForm = document.getElementById('form1');
		objForm.action = 'new_booking_update.php';
		objForm.submit();
	}
}

function UpdateResponsiblePerson(jsUserID, jsUserName)
{
	$('input#ResponsibleUID').val(jsUserID);
	$('span#ResponsibleUserNameSpan').html(jsUserName);
}

function js_Show_Item_Booking_Details(jsItemID, jsClickedObjID)
{
	js_Hide_Detail_Layer();
	js_Change_Layer_Position(jsClickedObjID);
	
	$('div#ItemBookingDetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>').load(
		"ajax_task.php", 
		{ 
			task: 'LoadItemBookingLayerTable',
			ItemID: jsItemID,
			DateTimeArr: $('input#DateTimeArr').val()
		},
		function(ReturnData)
		{
			js_Show_Detail_Layer();
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('ItemBookingDetailsLayer','','hide');
}

function js_Show_Detail_Layer()
{
	MM_showHideLayers('ItemBookingDetailsLayer','','show');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsClickedObjID) {
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') + 15;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + 10;
	
	var ObjLayer = document.getElementById('ItemBookingDetailsLayer');
	ObjLayer.style.left = posleft + "px";
	ObjLayer.style.top = postop + "px";
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>