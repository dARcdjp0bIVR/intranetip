<?php
// using : 
/*
 *  2019-05-02 Cameron
 *  - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "eService_MyBookingRecord";
$linterface 	= new interface_html();
//$linventory	= new libinventory();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);

$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService=1);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
$FacilityID = $_REQUEST['FacilityID'];
$RoomID = $_REQUEST['RoomID'];
$ItemID = IntegerSafe($_REQUEST['ItemID']);

$DateList = $_REQUEST['DateList'];
if ($DateList != '')
	$SelectedDateArr = explode(',', $DateList);			// from step3
else
	$SelectedDateArr = $_REQUEST['SelectedDateArr'];	// from step1
sort($SelectedDateArr);

if ($FacilityType == 2)	// item
	$ItemID = ($FacilityID=='')? $ItemID : $FacilityID;
else if ($FacilityType == 1)	// location
	$RoomID = ($FacilityID=='')? $RoomID : $FacilityID;

$SelectedTimeRangeArr = unserialize(rawurldecode($_REQUEST['SelectedTimeRangeArr']));

/*
$FacilityType = '2';
$SelectedDateArr[0] = '2010-04-26';
$SelectedDateArr[1] = '2010-04-27';
$SelectedDateArr[2] = '2010-04-28';
$RoomID = 91;


$BookingType = 'Item';
$SelectedDateArr[0] = '2010-04-12';
$SelectedDateArr[1] = '2010-04-13';
$SelectedDateArr[2] = '2010-04-14';
$SelectedDateArr[3] = '2010-04-15';
$SelectedDateArr[4] = '2010-04-21';
$ItemID = 7;
*/

echo $lebooking_ui->Get_New_Booking_Step2_UI($SelectedDateArr, $FacilityType, $RoomID, $ItemID, $SelectedTimeRangeArr, $CurrentBookingID);

?>

<script language="javascript">
var SelectMethod = "TimeSlot"; // TimeSlot/Specific  
$(document).ready( function() {
	
});

function js_Show_Event_Title()
{
	if($("#iCalEvent").attr('checked')) {
		$("#div_iCalInfo").show();
	} else {
		$("#div_iCalInfo").hide();
	}
}

function js_Go_Back_Step1()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'new_booking_1.php';
	objForm.submit();
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'index_list.php';
	objForm.submit();
}

function js_Go_Step3()
{
	$("input#SelectMethod").val(SelectMethod);
	if(SelectMethod=="TimeSlot")
	{
		if (js_Check_Selected_TimeSlot() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'new_booking_3.php';
			objForm.submit();
		}
	}
	else if(SelectMethod=="Specific")
	{
		if (js_Check_Selected_Specific() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'new_booking_3.php';
			objForm.submit();
		}
	}
}

function js_Go_New_Item_Booking()
{
	$("input#SelectMethod").val(SelectMethod);
	if(SelectMethod=="TimeSlot")
	{
		if (js_Check_Selected_TimeSlot() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'new_booking_update.php';
			objForm.submit();
		}
	}
	else if(SelectMethod=="Specific")
	{
		if (js_Check_Selected_Specific() == true)
		{
			var objForm = document.getElementById('form1');
			objForm.action = 'new_booking_update.php';
			objForm.submit();
		}
	}
}

function UpdateResponsiblePerson(jsUserID, jsUserName)
{
	$('input#ResponsibleUID').val(jsUserID);
	$('span#ResponsibleUserNameSpan').html(jsUserName);
}

function js_Check_Selected_TimeSlot()
{
	var jsCheckedTimeSlot = 0;
	$('input.TimeSlotChk').each( function () {
		if ($(this).attr('checked') == true)
			jsCheckedTimeSlot++;
	});
	
	if (jsCheckedTimeSlot == 0)
	{
		alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot']?>");
		$('input.TimeSlotChk').each( function () {
			$(this).focus();
			return false;
		});
		return false;
	}
	
	return true;
}

function js_Check_Selected_Specific()
{
	var jsCheckedSpecific = 0;
	jsCheckedSpecific = $('input.DateCheckBox:checked').length;
	
	if (jsCheckedSpecific == 0)
	{
		alert("<?=$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot']?>");
		return false;
	}
	else
	{
		var isFirst = 1;
		var DateListStr = '';
		$('input.DateCheckBox:checked').each(function(){
			var delimiter = isFirst==1?"":",";
			DateListStr += delimiter+$(this).val();
			isFirst = 0;
		});
		$("input#DateList").val(DateListStr);
	}
	
	return true;
}

function js_Change_To_Period_View(View)
{

	if(View=="Specific")
	{
		SelectMethod= "Specific";
		$("div#SpecificTableDiv").show();
		$("div#PeriodTableDiv").hide();
	}
	else
	{
		SelectMethod= "TimeSlot";
		$("div#SpecificTableDiv").hide();
		$("div#PeriodTableDiv").show();
	} 
}

			
function CheckTimeCrash()
{
	var SH =  $("#BookStartHour").val()
	var SM =  $("#BookStartMin").val()
	var EH =  $("#BookEndHour").val()
	var EM =  $("#BookEndMin").val()
	
	if(SH>EH || (SH==EH&&SM>=EM))
	{
		alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTime']?>");
		return false
	}
	
	$.post(
		"ajax_task.php",
		{
			task:"CheckTimeCrash",
			FacilityType:"<?=$FacilityType?>",
			FacilityID:"<?=$FacilityID?>",
			DateList: $("#DateList").val(),
			BookStartHour: $("#BookStartHour").val(),
			BookStartMin: $("#BookStartMin").val(),
			BookEndHour: $("#BookEndHour").val(),
			BookEndMin: $("#BookEndMin").val()
			
		},
		function (data)
		{
			$("div#TimeCheckList").html(data);
			$("input#SpecificTimeRangeArr").val(SH+":"+SM+":00_"+EH+":"+EM+":00");
		}
	
	);
}

			
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>