<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_eService_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$CurrentPageArr['eServiceClassDiary'] = 1;
$MODULE_OBJ['title'] = $Lang['ClassDiary']['ClassDiary'];
$libclassdiary_ui->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $libclassdiary_ui->Get_Class_Diary_Student_Parent_View_Index();

$libclassdiary_ui->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
function Get_Class_Diary_Table()
{
	var StudentID = $('#StudentID').val();
	var FromDate = $.trim($('input#FromDate').val());
	var ToDate = $.trim($('input#ToDate').val());
	var is_valid = true;
	
	if(StudentID == ''){
		return false;
	}
	
	$('#DateWarningLayer').html('').hide();
	if(FromDate=='' || !check_date_without_return_msg(document.getElementById('FromDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#FromDate').focus();
		is_valid = false;
	}
	
	if(ToDate=='' || !check_date_without_return_msg(document.getElementById('ToDate'))){
		$('#DateWarningLayer').html(jsLang['InvalidDateFormat']).show();
		$('input#ToDate').focus();
		is_valid = false;
	}
	
	if(FromDate > ToDate){
		$('#DateWarningLayer').html(jsLang['InvalidDateRange']).show();
		$('input#FromDate').focus();
		is_valid = false;
	}
	
	if(is_valid){
		Block_Element('form1');
		$('#ClassDiaryTableDiv').load(
			'ajax_load.php',
			{
				'task':'Get_Class_Diary_Student_View_Table',
				'StudentID':StudentID,
				'FromDate':FromDate,
				'ToDate':ToDate
			},
			function(data){
				UnBlock_Element('form1');
			}
		);
	}
}

$(document).ready(function(){
	Get_Class_Diary_Table();
});
</script>