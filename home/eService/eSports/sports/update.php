<?php
# editing by:
/***************************************************************************
*	modification log:
*
*   2019-05-30  Bill
*       - prevent SQL Injection + Cross-site Scripting
*	2013-09-02	Roy
*		- add trackCount and fieldCount
*
***************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
if (!$lsports->inEnrolmentPeriod())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$typeArr = explode(",", $types);
$trackCount = $_REQUEST["tc"];
$fieldCount = $_REQUEST["fc"];

### Handle SQL Injection + XSS [START]
$typeArr = IntegerSafe($typeArr);
for($i=0; $i<sizeof($typeArr); $i++)
{
    $t = $typeArr[$i];
    ${RQ_EventGroup.$t} = IntegerSafe(${RQ_EventGroup.$t});
}
$UQ_EventGroup = IntegerSafe($UQ_EventGroup);

$trackCount = IntegerSafe($trackCount);
$fieldCount = IntegerSafe($fieldCount);
### Handle SQL Injection + XSS [END]

# Clear the old records 
$sql = "DELETE FROM SPORTS_STUDENT_ENROL_EVENT WHERE StudentID = '$UserID'";
$lsports->db_db_query($sql);

$face_full_quota = 0;

# Insert Restrict Quota Event Enrolment Record(s)
for($i=0; $i<sizeof($typeArr); $i++)
{
	$t = $typeArr[$i];
	$tempArr = ${RQ_EventGroup.$t};
	
	$de = "";
	$values = "";
	for($j=0; $j<sizeof($tempArr); $j++)
	{
		$egid = $tempArr[$j];
		
		# Check the event is full or not
        $cur_enroled = $lsports->returnEventEnroledNo($egid, $UserID);
        $eventQuota = $lsports->returnEventQuota($egid);
		if($cur_enroled < $eventQuota || !$eventQuota)	
        {
//			$values .= $de."(".$UserID.", ".$egid.", now(), ".$UserID.")";
//			$de = ",";
			$values = "('".$UserID."', '".$egid."', now(), '".$UserID."')";
			
			$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
			$sql = "INSERT INTO SPORTS_STUDENT_ENROL_EVENT $fields VALUES $values";
			$lsports->db_db_query($sql);
            
			${totalCount.$t} = sizeof($tempArr);
		}
		else
		{
			$face_full_quota = 1;
		}
	}
}
// $totalCount1 = sizeof($EventGroup1);
// $totalCount2 = sizeof($EventGroup2);

###############
# Insert Unrestrict Quota Event Enrolment Record(s)
$de = "";
$values = "";
for($i=0; $i<sizeof($UQ_EventGroup); $i++)
{
	$egid = $UQ_EventGroup[$i];
	
	# Check the event is full or not
    $cur_enroled = $lsports->returnEventEnroledNo($egid, $UserID);
    $eventQuota = $lsports->returnEventQuota($egid);
	if($cur_enroled < $eventQuota || !$eventQuota)	
    {
        // $values .= $de."(".$UserID.", ".$egid.", now(), ".$UserID.")";
        // $de = ",";
		$values = "('".$UserID."', '".$egid."', now(), '".$UserID."')";
		
		$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
		$sql = "INSERT INTO SPORTS_STUDENT_ENROL_EVENT $fields VALUES $values";
		$lsports->db_db_query($sql);
    }
    else
	{
		$face_full_quota = 1;
	}
}

#####################

# Check Record Exist Or Not
$sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$UserID'";
$exist_info = $lsports->returnVector($sql);
if($exist_info[0] == 0) 
{
	# Insert New Record To SPORTS_STUDENT_ENROL_INFO
	$fields1 = "(StudentID, TrackEnrolCount, FieldEnrolCount, DateModified)";
	$values1 = "('".$UserID."', '".$trackCount."', '".$fieldCount."', now())";
	$sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO $fields1 VALUES $values1";
	$lsports->db_db_query($sql);
}
else
{
	// #Retrieve Amount of Events that the Student Have Enroled
	// $sql = "SELECT TrackEnrolCount, FieldEnrolCount FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$UserID'";
	// $temp = $lsports->returnArray($sql, 2);
	// if(sizeof($temp) != 0)
	// {
		// $trackCount = $temp[0][0];
		// $fieldCount = $temp[0][1];
	// }
	// else
	// {
		// $trackCount = 0;
		// $fieldCount = 0;
	// }

	# Update Existing Record in SPORTS_STUDENT_ENROL_INFO
	$sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', FieldEnrolCount = '$fieldCount', DateModified = now() WHERE StudentID = '$UserID'";
	$lsports->db_db_query($sql);
}
intranet_closedb();

if($face_full_quota) {
	$xmsg = "UpdatePartiallySuccess";
}
else {
	$xmsg = "UpdateSuccess";
}
header ("Location: index.php?xmsg=$xmsg");
?>