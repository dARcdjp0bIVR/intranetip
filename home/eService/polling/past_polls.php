<?php
# using: yat

#############################
#
#	Date:	2010-12-29	YatWoon
#			IP25 standard
#
#############################

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PagePollingPast"; 
$lpolling	= new libpolling();

## Check access right
if($_SESSION['UserType']==USERTYPE_PARENT)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

/*
# TABLE SQL
$GroupPollingIDs = $lpolling->db_sub_select("SELECT PollingID FROM INTRANET_GROUPPOLLING");
$sql  = "
	SELECT 
        DATE_FORMAT(DateStart, '%Y-%m-%d'),		
		DATE_FORMAT(DateEnd, '%Y-%m-%d'),
		Question,
        CONCAT('<a class=\'tablebluelink\' href=\'result.php?fr=0&PollingID=', PollingID, '\'>$button_view</a>')
	FROM 
        	INTRANET_POLLING 
	WHERE 
        	PollingID NOT IN ($GroupPollingIDs) 
                AND DateEnd < CURDATE()
";
*/
$sql = $lpolling->returnPolling_IP25(1, false);
# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("DateStart", "DateEnd", "Question");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th>".$li->column(0, $i_PollingDateStart)."</th>\n";
$li->column_list .= "<th>".$li->column(1, $i_PollingDateEnd)."</th>\n";
$li->column_list .= "<th>".$li->column(2, $i_PollingQuestion)."</th>\n";
$li->column_list .= "<th>$i_PollingResult</th>\n";

### Title ###
$TAGS_OBJ[] = array($i_frontpage_pastpoll,"");
$MODULE_OBJ = $lpolling->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<form name="form1" method="get" action="past_polls.php">

<?=$li->display("","view_table_list_v30");?>

<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
