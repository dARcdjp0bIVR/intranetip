<?php
include_once ("../../../../includes/global.php");
include_once ("../../../../includes/libdb.php");
include_once ("../../../../includes/libuser.php");
include_once ("../../../../includes/libslslib.php");
include_once ("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


$lu = new libuser($UserID);
$UserLogin = $lu->UserLogin;

# Hardcode for oid
#$UserLogin = "1a01";

$lsls = new libslslib();
$info_content = $lsls->getCheckoutList($UserLogin);

include_once ("slsheader.php");
$x = "";

for ($i=0; $i<sizeof($info_content); $i++)
{
     list ($tcode,$title,$callnum,$due) = $info_content[$i];
     //$x .= "<tr><td>$tcode</td><td>$title</td><td>$callnum</td><td>$due</td></tr>\n";
      $x .= "<tr><td>".($i+1).".</td><td>$title</td><td>$callnum</td><td>$due</td></tr>\n";
}
if ($x == "")
{
    $x = "<tr><td align=center colspan=4>$i_sls_library_norecords</td></tr>\n";
}

?>


<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20"><img src="images/library/lib_top_l.gif" width="20" height="47"></td>
    <td align="center" STYLE="background-image: url(images/library/lib_cell_t.gif)"><img src="images/library/head_checkoutlist_<?=$intranet_session_language?>.gif" width="519" height="47"></td>
    <td width="20"><img src="images/library/lib_top_r.gif" width="20" height="47"></td>
  </tr>
  <tr>
    <td STYLE="background-image: url(images/library/lib_cell_l.gif)">&nbsp;</td>
    <td align="center" bgcolor="#FBFEEE">
      <table width="80%" border="1" cellspacing="0" cellpadding="3" bordercolordark="#FAB341">
<tr>
<td>#</td>
<td><b><?=$i_sls_library_book_title?></b></td>
<td><b><?=$i_sls_library_call_number?></b></td>
<td><b><?=$i_sls_library_return_date?></b></td>
</tr>
<?=$x?>
</table>
    </td>
    <td STYLE="background-image: url(images/library/lib_cell_r.gif)">&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/library/lib_btm_l.gif" width="20" height="16"></td>
    <td STYLE="background-image: url(images/library/lib_cell_b.gif)"><img src="images/library/lib_cell_b.gif" width="10" height="16"></td>
    <td><img src="images/library/lib_btm_r.gif" width="20" height="16"></td>
  </tr>
</table>


<?
include_once ("../../../../templates/filefooter.php");
intranet_closedb();
?>