<?php

// Modifing by Siuwan
/********************** Change Log ***********************/
#
#	Date	:	2016-03-31 (Siuwan)
#	Detail	:	use session_register_intranet() and session_unregister_intranet() to solve PHP5.4 

#	Date	:	2013-11-04 (Fai)
#	Detail	:	support jumping to particular page using $JumpToPage (need to add corresponding page URL to this page!)

#	Date	:	2010-03-23 (Fai)
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
/******************* End Of Change Log *******************/


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php"); 
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($eclass_filepath.'/src/includes/php/lib-courseinfo.php');


intranet_auth();
intranet_opendb();
$luser = new libuser($UserID);

////////////////////////////////////////////////////////////////////////
// Modified by Wah, 2008-07-25
// Adding iPortfolio session variables
//////////////////////////////////////////////////////////////////////// 

$lo = new libeclass();

$IsPortfolioUser = false;

if ($luser->isParent())
{
	$special_rooms = $lo->getChildrenSpecialCourseMenu($UserID);
	for ($i=0; $i<sizeof($special_rooms); $i++)
	{
//		list($sr_user_course_id, $sr_course_name, $sr_RoomType, $sr_user_email) = $special_rooms[$i];
		$sr_user_course_id = $special_rooms[$i]['user_course_id'];
		$sr_course_name = $special_rooms[$i]['course_name'];
		$sr_RoomType = $special_rooms[$i]['RoomType'];
//		if ($sr_RoomType==4 && $header_leclass->license_iPortfolio!=0)
		if ($sr_RoomType==4)
		{
			$access2portfolio = true;
			$IsPortfolioUser = true;
		}
	}
	$row_arr = $special_rooms;
	
} 
else
{
	$row_arr = $lo->returnSpecialRooms($luser->UserEmail,4);

	if (count($row_arr) > 0)
	{
		$IsPortfolioUser = true;
	}
}
/* commented by Henry Chow n 2012-07-09
if(isset($_SESSION['intranet_iportfolio_admin']) && $_SESSION['intranet_iportfolio_admin']) {
	$IsPortfolioUser = true;
}
*/
$LibDB = new libdb();


# check if suspended
$sql = " SELECT count(UserID) FROM {$eclass_db}.PORTFOLIO_STUDENT WHERE IsSuspend = 1 AND UserID='{$UserID}'";
$row_check = $LibDB->returnVector($sql);

if ($row_check[0]==1)
{
	$IsPortfolioUser = false;
}


//$IsPortfolioUser = $_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"];

if (sizeof($row_arr)==1)
{
	# enter the special room directly
	$user_course_id = $row_arr[0][6];
}

if ($IsPortfolioUser)
{
    $LibDB->db = classNamingDB($eclass_db);

    $sql = "	SELECT
					course_id,
    	            user_id,
        	        user_email,
            	    memberType,
                	ifnull(scheme_id,1) AS scheme_id
				FROM
            		user_course
				WHERE
            		(status is null or status = '') AND
                	user_course_id='".$user_course_id."'
                	AND user_email='".$luser->UserEmail."'
			";
    $row_result = $LibDB->returnArray($sql);
    //debug_r($row_result);
    //debug($sql);
	
    if (sizeof($row_result)==0)
    {
		# try to find if the user is parent and parent is permitted to enter this course
        $sql = "	SELECT 
        				iu.UserID
					FROM
                    	{$intranet_db}.INTRANET_USER AS iu
					WHERE
                    	iu.RecordType='3' AND iu.UserEmail='".$luser->UserEmail."'
				";
		$row_parent = $LibDB->returnVector($sql);

		if (sizeof($row_parent)==1)
        {
			$parentID = $row_parent[0];
            $sql = "	SELECT
							uc.course_id,
							uc.user_id,
                            uc.user_email,
                            'P' AS memberType,
                            ifnull(uc.scheme_id,1) AS scheme_id
					   	FROM 
							{$intranet_db}.INTRANET_PARENTRELATION as a, 
							{$intranet_db}.INTRANET_USER as b, 
							{$eclass_db}.user_course AS uc, 
							{$eclass_db}.course AS c
					   	WHERE 
							a.StudentID=b.UserID 
						   	AND a.ParentID='$parentID'
                           	AND uc.user_email=b.UserEmail AND uc.user_course_id='".$user_course_id."'
                           	AND uc.course_id=c.course_id AND c.is_parent='1'
                        ";
			$row_result = $LibDB->returnArray($sql);

			if (sizeof($row_result)>0)
            {
            	$parent_user_email = $email;
			}
		}
	}
	
	
	if (sizeof($row_result)>0)
	{
        $row = $row_result[0];
        $course_id = $row["course_id"];
        $user_id = $row["user_id"];
        $user_email = $row["user_email"];
        $memberType = $row["memberType"];
        $scheme_id = $row["scheme_id"];


        // Update eClass course_visitor_count table
        $sql = "INSERT INTO course_count (host, course_id, user_id, inputdate) VALUES ('$REMOTE_ADDR', $course_id, $user_id, now())";
        $LibDB->db_db_query($sql);
        // Update last_used in user_course
        $sql = "UPDATE user_course SET lastused = now() WHERE user_course_id='".$user_course_id."'";
        $LibDB->db_db_query($sql);

        // Get course language
        $sql = "SELECT language, ReferenceID, rights_student, rights_guest FROM course WHERE course_id='".$course_id."'";

        $row = $LibDB->returnArray($sql);
        list($language, $ReferenceID, $courseStudentRights, $courseGuestRights) = $row[0];

        // Update last_used in usermaster
        $LibDB->db = classNamingDB($course_id);
        $sql = "UPDATE usermaster SET lastused = now() WHERE user_id = '".$user_id."'";
        $LibDB->db_db_query($sql);
        // Start login session in course
        $sql = "INSERT INTO login_session (user_id, host, inputdate, modified) VALUES ($user_id, '$REMOTE_ADDR', now(), now())";
        $LibDB->db_db_query($sql);
        $login_session_id = $LibDB->db_insert_id();

        session_start();

        # pre-load access rights for student and guest
        if ($memberType=="S")
        {
                $ck_course_rights = $courseStudentRights;
        } elseif ($memberType=="G")
        {
                $ck_course_rights = $courseGuestRights;
        } else
        {
                $ck_course_rights = "";
        }
        session_register_intranet("ck_course_rights",$ck_course_rights);

        //get default helper jobs for assistant
        if ($memberType=="A")
        {
                $sql = "SELECT Job FROM helper WHERE IsTA='1'";
                $result = $LibDB->returnArray($sql, 1);
                $ck_helper_job = $result[0][0];
                session_register_intranet("ck_helper_job",$ck_helper_job);
        } else
        {
                $ck_helper_id = "";
                session_unregister_intranet("ck_helper_id");
                $ck_helper_job = "";
                session_unregister_intranet("ck_helper_job");
        }

        // Set session cookies
        $ck_lang = "";
        session_unregister_intranet("ck_lang");
        //$ck_course_id = $course_id;
        session_register_intranet("ck_course_id",$course_id);
        //$ck_user_id = $user_id;
        session_register_intranet("ck_user_id",$user_id);
        //$ck_user_course_id = $user_course_id;
        session_register_intranet("ck_user_course_id",$user_course_id);
        //$ck_login_session_id = $login_session_id;
        session_register_intranet("ck_login_session_id",$login_session_id);
        //$ck_memberType = $memberType;
        session_register_intranet("ck_memberType",$memberType);
        //$ck_default_lang = $language;
        session_register_intranet("ck_default_lang",$language);
        //$ck_scheme_id = $scheme_id;
        session_register_intranet("ck_scheme_id",$scheme_id);
        //$ck_user_email = $user_email;
        session_register_intranet("ck_user_email",$user_email);
        //$ck_roleType = $memberType;
        session_register_intranet("ck_roleType",$memberType);
        //$ck_user_role = "VIEW";
		session_register_intranet("ck_user_role","VIEW");
		//$ck_current_academic_year_id = Get_Current_Academic_Year_ID();
		session_register_intranet("ck_current_academic_year_id",Get_Current_Academic_Year_ID());

        if ($intranet_user_id=="" || $intranet_user_id<=0)
        {
        	$sql = "SELECT UserID FROM {$intranet_db}.INTRANET_USER WHERE UserEmail='$user_email' ";
			$rs_intranet = $LibDB->returnVector($sql);
        	$intranet_user_id = $rs_intranet[0];
        }
        //$ck_intranet_user_id = $intranet_user_id;
        session_register_intranet("ck_intranet_user_id",$intranet_user_id);

        setcookie("ck_user_email_client", $user_email, time()+86400, "", "", 0);

        $li_ci = new courseinfo(1);
        $li_ci->registerRoomType($ck_course_id);
        $li_ci->registerUserRights($ck_user_id, $memberType);
        $li_ci->registerEquationEditorLicence($ck_course_id);
        $li_ci->registerEClassRight($memberType);

        if ($ReferenceID!="" && $ck_room_type=="ELP")
        {
                if (!strstr(strtoupper($ReferenceID), "ENG"))
                {
                        $ck_default_lang = "chib5";
                }
        }
//debug_r($_SESSION);
        if ($ck_memberType=="P")
        {
                $li_ci->registerEClassChildren($ck_course_id, $parent_user_email);
        }

        $ck_has_iportfolio = $IsPortfolioUser;
        session_register_intranet("ck_has_iportfolio",$IsPortfolioUser);
	} 
	else
	{
        // Login failure
        //header("Location: ".$PATH_WRT_ROOT."/index.php");
        session_start();
        
        //$ck_has_iportfolio = $IsPortfolioUser;
        session_register_intranet("ck_has_iportfolio",$IsPortfolioUser);
                
	}
        		
}
else
{
	session_start();
	
	//$ck_has_iportfolio = $IsPortfolioUser;
    session_register_intranet("ck_has_iportfolio",$IsPortfolioUser);	
}


////////////////////////////////////////////////////////////////////////
 
intranet_closedb();

# direct to particular page
if($sys_custom['lp_stem_learning_scheme']){
    $JumpToPage = 'LearningPortfolio';
}
if (isset($JumpToPage) && $JumpToPage!="")
{
	switch ($JumpToPage)
	{
		case "OLEApproval":
			$url_page = "/home/portfolio/teacher/management/oleIndex.php?clearCoo=1";
			break;
		case "LearningPortfolio":
			$url_page = "/home/portfolio/learning_portfolio_v2/";
			break;
	}
	
	if ($url_page!="")
	{
		header("location: $url_page");
		die();
	}
}

if($IsPortfolioUser)
{
	
	# to initilize session in real eClass course
	header("Location: login.php?uc_id=$ck_user_course_id&jumpback=IPPORTFOLIO");
	die();

	
	if($_SESSION["intranet_iportfolio_usertype"] == "TEACHER")
	{
		// if teacher role 
		header("Location: school_records.php");
	}
	else
	{
		// if student role
		header("Location: profile/student_info_student.php");
	}
}
else
{
	if (isset($stand_alone['iPortfolio']) && $stand_alone['iPortfolio'])
	{
		unset($_SESSION["UserID"]);
		unset($_SESSION["SSV_PRIVILEGE"]);
		unset($_SESSION["UserType"]);
		echo "<script type=\"text/javascript\">";
		echo "alert(\"".$Lang['iPortfolio']['standAloneWithoutAccount']."\");";
		echo "window.location=\"/templates/\";";
		echo "</script>";
		
	}
	else
	{
		header("Location: /home");
	}

}
exit;
?>
