<!-- Modify by Sandy -->
<script>
var EditFreeTextPage ={	
	
	_section_index: '<?=$section_index?>',
	_data : 	'',
	
	on_init: function(){
		if(this._section_index != ""){
			this._data = parent.StudentReportTemplateEditPage.get_free_text_data(this._section_index);			
		}		
	},
		
	FillIn: function(){
		var field = FCKeditorAPI.GetInstance('Content');				
		field.InsertHtml($("#genVariable").val());
	},
	
	onSubmit: function(){
		
		var data = FCKeditorAPI.GetInstance("Content").EditorWindow.parent.FCK.GetHTML();
		
		if(this._section_index !="")
			parent.StudentReportTemplateEditPage.update_free_text_data(this._section_index, data);
		else
			parent.StudentReportTemplateEditPage.add_free_text_data(data);
					
		parent.tb_remove();	
	}
};

$(document).ready(function() {		
	EditFreeTextPage.on_init();
});

function FCKeditor_OnComplete(EDITOR){	
	// Prevent JS error in IE if template is undefined.
	if (EditFreeTextPage._data) {
		EDITOR.SetHTML(EditFreeTextPage._data);
	}
}

</script>


<div class="edit_pop_board">
	<h1><span> <?= srt_lang('Edit Text') ?></span></h1>
    <div class="edit_pop_board_write">
      <table class="form_table" style="width:100%; margin:0 auto">        
        <tr><td>
        <div id="genVariableDiv">
        	<?= srt_lang('Auto-fill Information') ?>:
        	<?=$select?>
        	<input type="button" onClick="EditFreeTextPage.FillIn()" value="<?=srt_lang('Insert')?>">        
        </div>
        </td></tr>
        <tr>
        <td id="fck_panel"><?=$objHtmlEditor->Create()?></td>
        </tr>

        <col class="field_title" />
        <col  class="field_c" />
      </table>
         
   
    </div>
<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="submit2" type="button" class="formbutton" onclick="EditFreeTextPage.onSubmit();"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Submit') ?>" />
		<input name="submit2" type="button" class="formbutton" onclick="parent.tb_remove()" 
		onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
		<p class="spacer"></p>
	</div>
</div>

