<!-- Modify by -->
<script>
var EditHeaderPage ={	
		
	_header_data : 	parent.StudentReportTemplateEditPage.get_header_data(),
	
	on_init: function(){
		var objHeaderData = this._header_data;
		$("#chk_first_page").attr("checked",objHeaderData.is_to_show_header_on_first_page_only);
		<!-- Begin Henry Yuen (2010-04-20): header height can be set by user --> 
		$('#height').val(objHeaderData.height);
		<!-- End Henry Yuen (2010-04-20): header height can be set by user -->		
	},	
	FillIn: function(){
		var field = FCKeditorAPI.GetInstance('Content');				
		field.InsertHtml($("#genVariable").val());
	},
	onSubmit: function(){
		<!-- Begin Henry Yuen (2010-04-20): header height can be set by user --> 
		var height = $('#height').val();
		if(!(parseInt(height) >= 1 && parseInt(height) <= 100) || height.toString().search(/^-?[0-9]+$/) != 0){
			alert("<?= srt_lang('Panel Height') ?>: (1-100)");
			$('#height').focus();
			return false;
		}		
		<!-- End Henry Yuen (2010-04-20): header height can be set by user -->
		
		var objData = {
			'is_to_show_header_on_first_page_only': $('#chk_first_page:checked').length ? 1 : 0, 
			'header_template' : FCKeditorAPI.GetInstance("Content").EditorWindow.parent.FCK.GetHTML(),
			<!-- Begin Henry Yuen (2010-04-20): header height can be set by user --> 
			'height' : $('#height').val()
			<!-- End Henry Yuen (2010-04-20): header height can be set by user -->
		};		
		parent.StudentReportTemplateEditPage.update_header_data(objData);		
		parent.tb_remove();	
	}
};

$(document).ready(function() {		
	EditHeaderPage.on_init();
});

function FCKeditor_OnComplete(EDITOR){
	var objHeaderData = EditHeaderPage._header_data;
	// Prevent JS error in IE if template is undefined.
	if (objHeaderData.header_template) {
		EDITOR.SetHTML(objHeaderData.header_template);
	}
}

</script>


<div class="edit_pop_board">
	<h1><span> <?= srt_lang('Edit Header') ?> </span></h1>
    <div class="edit_pop_board_write">
      <table class="form_table" style="width:100%; margin:0 auto">
        <tr>                    
          <td ><input name="chk_first_page" type="checkbox" id="chk_first_page" /><?= srt_lang('Show on first page only') ?></td>          
        </tr>
        <!-- Begin Henry Yuen (2010-04-20): header height can be set by user -->
        <tr>                    
          <td ><?= srt_lang('Height') ?>: <input name="height" type="textbox" id="height" size="3" maxlength="3" value="30" /> mm (1-100)</td>          
        </tr>        
        <!-- End Henry Yuen (2010-04-20): header height can be set by user -->
        <tr><td>
        <div id="genVariableDiv">
        	<?= srt_lang('Auto-fill Information') ?>:
        	<?=$select?>
        	<input type="button" onClick="EditHeaderPage.FillIn()" value="<?=srt_lang('Insert')?>">        
        </div>
        </td></tr>
        <tr>
        <td id="fck_panel"><?=$objHtmlEditor->Create()?></td>
        </tr>

        <col class="field_title" />
        <col  class="field_c" />
      </table>
         
   
    </div>
<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="submit2" type="button" class="formbutton" onclick="EditHeaderPage.onSubmit();"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Submit') ?>" />
		<input name="submit2" type="button" class="formbutton" onclick="parent.tb_remove()" 
		onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
		<p class="spacer"></p>
	</div>
</div>

