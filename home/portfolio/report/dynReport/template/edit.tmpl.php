<?php 
	#// Modifying: 
	#modifications 
	#	Henry Yuen (2010-07-26): support rearrange data section ordering
	#	Henry Yuen (2010-07-25): hide edit options for sample template
	#	Henry Yuen (2010-06-30): add section description
?>


<link rel="stylesheet" href="<?=$intranet_httppath?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/thickbox.js"></script>
<script language="JavaScript" src="js/json2.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>

<script>
var StudentReportTemplateEditPage = {
	/**
	 * template_data is an object which mirrors the data in StudentReportTemplate, and
	 * contains the following properties (some of the properties may be missing):
	 * {
	 * 		'template_title': ?,
	 * 		'is_to_show_header_on_first_page_only': ?,
	 * 		'header_template': ?,
	 * 		'is_to_show_footer_on_last_page_only': ?,
	 * 		'footer_template': ?,
	 * 		'sections': [
	 * 			{
	 * 				'section_type': table/free_text/page_break,
	 * 				'free_text': { // For free text section only.
	 * 					'text_template': ?
	 * 				},
	 * 				'table': { // For table section only.
	 * 					'section_title': ?,
	 * 					'sort_by_column_number': 1/2/3/...,
	 * 					'is_ascending': ?,
	 * 					'columns': [
	 * 						{
	 * 							'title': ?,
	 * 							'width': ?
	 * 						},
	 * 						...
	 * 					],
	 * 					'data_sources': [
	 * 						{
	 * 							'name': OLE/Attendance Record/Activity/Merit/...,
	 * 							'max_num_of_records': ?,
	 * 							'fields': [
	 * 								field_name1, field_name2, ...
	 * 							]
	 * 						},
	 * 						...
	 * 					],
	 * 					// Style information.
	 * 					'border_size': ?,
	 * 					'border_color': ?,
	 * 					'num_of_records_to_show_per_row': 1/2/3,
	 * 					'section_title_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					},
	 * 					'column_title_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					},
	 * 					'row_text_style': {
	 * 						'font_family': ?,
	 * 						'font_size': ?,
	 * 						'is_bold': ?,
	 * 						'is_italic': ?,
	 * 						'is_underline': ?,
	 * 						'text_color': ?,
	 * 						'background_color': ?
	 * 					}
	 * 				}
	 * 			},
	 * 			...
	 * 		]
	 * }
	 */
	_template_data: <?= count($template_data) > 0 ? $json->encode($template_data) : '{}' ?>,
	
	/**
	 * Map from data source/field name to its display name.
	 * The key for a field is of the format "$data_source_name.$field_name".
	 */
	_data_source_and_field_display_names: <?= count($data_source_and_field_display_names = $data_source_manager->get_data_source_and_field_display_names()) > 0 ? $json->encode($data_source_and_field_display_names) : '{}' ?>,
	
	/**
	 * Map from student field name to its display name.
	 */
	_student_field_display_names: <?= count($student_field_display_names = $data_source_manager->student_info->get_field_display_names()) > 0 ? $json->encode($student_field_display_names) : '{}' ?>,
	
	/**
	 * Store data fields which can have criteria settings for each data sources
	 */
	_data_content_source_criteria_fields: <?= count($data_content_source_criteria_field_arr = $data_source_manager->get_criteria_data_sources()) > 0 ? $json->encode($data_content_source_criteria_field_arr) : '{}' ?>,
	
	/**
	 * Store the criteria selection options
	 */
	_data_content_criteria_options: <?= count($data_content_criteria_options = $data_source_manager->get_critieria_options()) > 0 ? $json->encode($data_content_criteria_options) : '{}' ?>,
	
	/**
	 * Store the criteria selection options lang
	 */
	_data_content_criteria_option_display_names: <?= count($data_content_criteria_options_display_names = $data_source_manager->get_criteria_options_display_names()) > 0 ? $json->encode($data_content_criteria_options_display_names) : '{}' ?>,
	
	/**
	 * Store the criteria condition options with lang
	 */
	_data_content_criteria_condition_options: <?= count($data_content_criteria_condition_options = $data_source_manager->get_criteria_condition_options()) > 0 ? $json->encode($data_content_criteria_condition_options) : '{}' ?>,
	
	/**
	 * Map from data source/field name to its display name.
	 */
	_statistics_option_and_field_display_names: <?= count($statistics_option_and_field_display_names = $data_source_manager->get_statistics_options_display_names()) > 0 ? $json->encode($statistics_option_and_field_display_names) : '{}' ?>,
	
	
	/**
	 * Return data fields which can have criteria settings for each data sources
	 */
	get_data_content_source_criteria_fields: function() {
		return StudentReportTemplateEditPage._data_content_source_criteria_fields;
	},
	
	/**
	 * Return the criteria selection options array
	 */
	get_data_content_criteria_options: function() {
		return StudentReportTemplateEditPage._data_content_criteria_options;
	},
	
	/**
	 * Return the display name of a criteria option.
	 */
	get_data_content_criteria_option_display_name: function(criteria_name) {
		var display_name = this._data_content_criteria_option_display_names[criteria_name];
		return display_name;
	},
	
	/**
	 * Return the criteria condition selection options array
	 */
	get_data_content_criteria_condition_options: function() {
		return StudentReportTemplateEditPage._data_content_criteria_condition_options;
	},
	
	/**
	 * Return the display name of a criteria option.
	 */
	get_statistics_option_display_name: function(statistics_type_code) {
		var display_name = this._statistics_option_and_field_display_names[statistics_type_code];
		return display_name;
	},
	
	
	/**
	 * Used by StudentReportTemplateEditHeaderPage to get the existing header data in the edit page.
	 * @return an object containing some properties from _template_data:
	 *		{
	 * 			'is_to_show_header_on_first_page_only': ?,
	 * 			'header_template': ? // Placeholders will be converted into image tags before return.
	 *		}
	 */
	get_header_data: function() {
		var data = this._get_template_data();
		
		return {
			'is_to_show_header_on_first_page_only': data.is_to_show_header_on_first_page_only,
			'header_template': this._convert_placeholder_to_image(data.header_template),
			<!-- Begin Henry Yuen (2010-04-20): header height can be set by user --> 
			'height': data.header_height
			<!-- End Henry Yuen (2010-04-20): header height can be set by user -->		
		};
	},
	
	/**
	 * Used by StudentReportTemplateEditHeaderPage to update the header data in the edit page.
	 * @param header_data Similar in structure as that returned by get_header_data().
	 */
	update_header_data: function(header_data) {
		var data = this._get_template_data();
		
		data.is_to_show_header_on_first_page_only = header_data.is_to_show_header_on_first_page_only;
		data.header_template = this._convert_image_to_placeholder(header_data.header_template);
		<!-- Begin Henry Yuen (2010-04-20): header height can be set by user --> 		
		data.header_height = header_data.height;			
		<!-- End Henry Yuen (2010-04-20): header height can be set by user -->
		
		// Update display.
		this._update_header_template_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditFooterPage to get the existing footer data in the edit page.
	 * @return an object containing some properties from _template_data:
	 *		{
	 * 			'is_to_show_footer_on_last_page_only': ?,
	 * 			'footer_template': ? // Placeholders will be converted into image tags before return.
	 *		}
	 */
	get_footer_data: function() {
		var data = this._get_template_data();
		
		return {
			'is_to_show_footer_on_last_page_only': data.is_to_show_footer_on_last_page_only,
			'footer_template': this._convert_placeholder_to_image(data.footer_template),
			<!-- Begin Henry Yuen (2010-04-20): footer height can be set by user --> 
			'height': data.footer_height
			<!-- End Henry Yuen (2010-04-20): footer height can be set by user -->
		};
	},
	
	/**
	 * Used by StudentReportTemplateEditFooterPage to update the footer data in the edit page.
	 * @param footer_data Similar in structure as that returned by get_footer_data().
	 */
	update_footer_data: function(footer_data) {
		var data = this._get_template_data();
		
		data.is_to_show_footer_on_last_page_only = footer_data.is_to_show_footer_on_last_page_only;
		data.footer_template = this._convert_image_to_placeholder(footer_data.footer_template);
		<!-- Begin Henry Yuen (2010-04-20): footer height can be set by user --> 		
		data.footer_height = footer_data.height;				
		<!-- End Henry Yuen (2010-04-20): footer height can be set by user -->		
		
		// Update display.
		this._update_footer_template_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditFreeTextPage to get some existing free text data in the edit page, given its section index.
	 * @return the template for the free text, placeholders will be converted into image tags before return.
	 * Return null on error.
	 */
	get_free_text_data: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'free_text')) return null;
		
		var free_text_data = this._get_template_data().sections[section_index].free_text;
		if (!free_text_data) {alert('There is no free text data in the section.'); return null;}
		
		// Return the template for the free text.
		return this._convert_placeholder_to_image(free_text_data.text_template);
	},
	
	/**
	 * Used by StudentReportTemplateEditFreeTextPage to add some free text data in the edit page.
	 * @param tpl Similar in format as that returned by get_free_text_data().
	 */
	add_free_text_data: function(tpl) {
		var data = this._get_template_data();
		if (!data.sections) data.sections = [];
		data.sections.push({
			section_type: 'free_text',
			'free_text': {
				text_template: this._convert_image_to_placeholder(tpl)
			}
		});
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditFreeTextPage to update some free text data in the edit page, given its section index.
	 * @param tpl Similar in format as that returned by get_free_text_data().
	 */
	update_free_text_data: function(section_index, tpl) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'free_text')) return;
		
		var section = this._get_template_data().sections[section_index];
		if (!section.free_text) section.free_text = {};
		var existing_free_text_data = section.free_text;
		
		// Update the template for the free text.
		existing_free_text_data.text_template = this._convert_image_to_placeholder(tpl);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used to delete some existing free text data in the edit page, given its section index.
	 */
	delete_free_text_data: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'free_text')) return;
		
		if (!confirm('<?= srt_lang('Are you sure you want to delete this text?') ?>')) return;
		
		// Remove the section.
		this._get_template_data().sections.splice(section_index, 1);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditTablePage to get some form data for a table in the edit page, given its section index.
	 * @return an object containing some properties from _template_data, so see _template_data for details:
	 * 		{ // For table section only.
	 * 			'section_title': ?,
	 * 			'sort_by_column_number': 1/2/3/...,
	 * 			'is_ascending': ?,
	 *			...
	 * 		}
	 * Return null on error.
	 */
	get_table_data: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'table')) return null;
		
		var table_data = this._get_template_data().sections[section_index].table;
		if (!table_data) {alert('There is no table data in the section.'); return null;}
		
		// Copy and return the table data.
		return $.extend(true, {}, table_data);
	},
	
	/**
	 * Used by StudentReportTemplateEditTablePage to add some form data for a table in the edit page.
	 * @param table_data Similar in structure as that returned by get_table_data(). No need to pass style information.
	 */
	add_table_data: function(table_data) {
		// Check parameters.
		if (!this._check_table_data(table_data)) return;
		
		// Clone the table data.
		table_data = $.extend(true, {}, table_data);
		
		// Remove data sources without name.
		table_data.data_sources = this._remove_data_sources_without_name(table_data.data_sources);
		
		var data = this._get_template_data();
		if (!data.sections) data.sections = [];
		data.sections.push({
			section_type: 'table',
			'table': $.extend(
				// Provide some default styles.
				{
					border_size: 1,
					section_title_style: {
						font_family: 'arialunicid0-chi',
						font_size: 12,
//						is_bold: true,
						text_color: '#FFFFFF',
						background_color: '#BBBBBB'
					},
					section_description_style: {
						font_family: 'arialunicid0-chi',
						font_size: 12,
//						is_bold: true,
						text_color: '#FFFFFF',
						background_color: '#BBBBBB'
					},
					column_title_style: {
						font_family: 'arialunicid0-chi',
						font_size: 10,
//						is_bold: true,
						text_color: '#FFFFFF',
						background_color: '#888888'
					},
					row_text_style: {
						font_family: 'arialunicid0-chi',
						font_size: 8,
						text_color: '#000000',
						background_color: '#FFFFFF'
					}
				},
				table_data
			)
		});
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditTablePage to update some form data for a table in the edit page, given its section index.
	 * @param table_data Similar in structure as that returned by get_table_data(). No need to pass style information, so that existing style information can be kept.
	 */
	update_table_data: function(section_index, table_data) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'table')) return;
		if (!this._check_table_data(table_data)) return;
		
		var section = this._get_template_data().sections[section_index];
		if (!section.table) section.table = {};
		var existing_table_data = section.table;
		
		// Provide default for missing properties.
		table_data = $.extend(
			{
				section_title: null,
				section_description: null,
				sort_by_column_number: 1,
				is_ascending: false,
				columns: [],
				data_sources: []
			},
			// Clone the table data.
			$.extend(true, {}, table_data)
		);
		
		// Remove data sources without name.
		table_data.data_sources = this._remove_data_sources_without_name(table_data.data_sources);
		
		// Merge columns separately to preserve column width, if no column is added/removed.
		if (existing_table_data.columns && table_data.columns && existing_table_data.columns.length == table_data.columns.length) {
			$.each(table_data.columns, function(i, val) {
				$.extend(existing_table_data.columns[i], val);
			});
			// No need to merge again.
			delete table_data.columns;
		}
		// Merge other properties, keeping style information.
		$.extend(existing_table_data, table_data);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used to delete some form data for a table in the edit page, given its section index.
	 */
	delete_table_data: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'table')) return;
		
		if (!confirm('<?= srt_lang('Are you sure you want to delete this data?') ?>')) return;
		
		// Remove the section.
		this._get_template_data().sections.splice(section_index, 1);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used by StudentReportTemplateEditTableStylePage to get some form data for the style of a table in the edit page, given its section index.
	 * @return an object containing some properties from _template_data, so see _template_data for details:
	 * 		{ // For table section only.
	 * 			'section_title_style': {
	 * 				'font_family': ?,
	 * 				'font_size': ?,
	 * 				'is_bold': ?,
	 * 				'is_italic': ?,
	 * 				'is_underline': ?,
	 * 				'text_color': ?,
	 * 				'background_color': ?
	 * 			},
	 * 			'column_title_style': {
	 * 				...
	 * 			},
	 * 			'row_text_style': {
	 * 				...
	 * 			},
	 * 			// Column width is stored with column title.
	 * 			'columns': [
	 * 				{
	 * 					'width': ?
	 * 				},
	 * 				...
	 * 			],
	 * 			// The following may not be supported.
	 * 			'border_size': ?,
	 * 			'border_color': ?,
	 * 			'num_of_records_to_show_per_row': 1/2/3
	 * 		}
	 * Return null on error.
	 */
	get_table_style_data: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'table')) return null;
		
		var table_data = this._get_template_data().sections[section_index].table;
		if (!table_data) {alert('There is no table data in the section.'); return null;}
		
		// Copy and return the table data.
		return $.extend(true, {}, table_data);
	},
	
	/**
	 * Used by StudentReportTemplateEditTableStylePage to update some form data for the style of a table in the edit page, given its section index.
	 * @param table_data Similar in structure as that returned by get_table_style_data().
	 */
	update_table_style_data: function(section_index, table_data) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'table')) return;
		if (!this._check_table_style_data(table_data)) return;
		
		var section = this._get_template_data().sections[section_index];
		if (!section.table) section.table = {};
		var existing_table_data = section.table;
		
		// Merge the styles.
		$.extend(true, existing_table_data, table_data);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used to add a page break in the edit page.
	 */
	add_page_break: function() {
		var data = this._get_template_data();
		if (!data.sections) data.sections = [];
		data.sections.push({
			section_type: 'page_break'
		});
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Used to delete a page break in the edit page, given its section index.
	 */
	delete_page_break: function(section_index) {
		// Check parameters.
		if (!this._check_section_index_and_type(section_index, 'page_break')) return;
		
		if (!confirm('<?= srt_lang('Are you sure you want to delete this page break?') ?>')) return;
		
		// Remove the section.
		this._get_template_data().sections.splice(section_index, 1);
		
		// Update display.
		this._update_section_list_view();
	},
	
	/**
	 * Call this when the template title is changed.
	 * Template title will be trimmed.
	 */
	on_template_title_changed: function(template_title_input) {
		if (!template_title_input) {
			alert('Please provide an input element for template title.');
			return;
		}
		
		var data = this._get_template_data();
		data.template_title = $.trim(template_title_input.value);
	},
	
	/**
	 * Call this on page load.
	 */
	on_init: function() {
		this._update_section_list_view();
		this._update_header_template_view();
		this._update_footer_template_view();
	},
			
	/**
	 * Call this when the submit button is clicked.
	 */
	on_submit: function(form) {			
		$('#saveButton').attr('disabled',true);
		if ($.trim(this._get_template_data().template_title) == '') {
			alert('<?= srt_lang('Please provide a template title.') ?>');
			$('#saveButton').removeAttr('disabled');
			return false;
		}
		
		$('<input type="hidden" name="template_data"/>')
			.attr('value', JSON.stringify(this._get_template_data()))
			.appendTo(form);
		form.submit();
	},
	
	/**
	 * Just get the template data. Initialize it if it is null.
	 */
	_get_template_data: function() {
		if (!this._template_data) this._template_data = {};
		return this._template_data;
	},
	
	/**
	 * Get the display name of a data source.
	 * @return the display name, or the data source name if the display name is not found.
	 */
	_get_data_source_display_name: function(data_source_name) {
		var display_name = this._data_source_and_field_display_names[data_source_name];
		return !display_name ? data_source_name : display_name;
	},
	
	/**
	 * Get the display name of a data source field.
	 * @return the display name, or the data source field name if the display name is not found.
	 */
	_get_data_source_field_display_name: function(data_source_name, field_name) {
		var display_name = this._data_source_and_field_display_names[data_source_name + '.' + field_name];
		return !display_name ? field_name : display_name;
	},
	
	/**
	 * Get the display name of a student field.
	 * @return the display name, or the field name if the display name is not found.
	 */
	_get_student_field_display_name: function(field_name) {
		var display_name = this._student_field_display_names[field_name];
		return !display_name ? field_name : display_name;
	},
	
	/**
	 * Convert placeholders (in header/footer/free text) to images, for display purpose.
	 * Assume that placeholders are of the format [[Placeholder]].
	 */
	_convert_placeholder_to_image: function(tpl) {
		if (!tpl) return tpl;
		
		var _this = this;
		return tpl.replace(/\[\[(\w+)\]\]/g, function(str, p1) {
			var field_name = p1;
			var display_name = _this._get_student_field_display_name(field_name);
			display_name = encodeURIComponent(display_name);
			return '<img src="<?=dirname($_SERVER['PHP_SELF'])?>/str2img.php?s=' + display_name + '&amp;field=' + field_name + '"/>';
		});
	},
	
	/**
	 * Convert images (in header/footer/free text) to placeholders, for storage purpose.
	 * Assume that placeholders are of the format [[Placeholder]].
	 * Reverse the operation of _convert_placeholder_to_image().
	 */
	_convert_image_to_placeholder: function(tpl) {
		if (!tpl) return tpl;
		
		return tpl.replace(/<img\s+[^>]+str2img[^>]+field=(\w+)[^>]+>/g, '[[$1]]');
	},
	
	/**
	 * Convert template style to CSS style, for display purpose.
	 * If font_size is smaller than 10, it will be set to 10 to prevent small text.
	 * @param style_data An object containing the following properties:
	 * 		{
	 * 			'font_family': ?,
	 * 			'font_size': ?,
	 * 			'is_bold': ?,
	 * 			'is_italic': ?,
	 * 			'is_underline': ?,
	 * 			'text_color': ?,
	 * 			'background_color': ?
	 * 		}
	 * @return A key/value object containing CSS style properties for use in jQuery.css().
	 */
	_convert_template_style_to_css_style: function(style_data) {
		var css = {};
		var v;
		if (!style_data) return css;
		if (v = style_data['font_family']) css["font-family"] = v;
		if (v = style_data['font_size']) css["font-size"] = v;
		if (v = style_data['is_bold']) css["font-weight"] = "bold";
		if (v = style_data['is_italic']) css["font-style"] = "italic";
		if (v = style_data['is_underline']) css["text-decoration"] = "underline";
		if (v = style_data['text_color']) css["color"] = v;
		if (v = style_data['background_color']) css["background-color"] = v;
		
		// Prevent small text during preview.
		if (css["font-size"] && css["font-size"] < 10) css["font-size"] = 10;
		
		return css;
	},
	
	/**
	 * Remove the data sources without name.
	 * @param data_sources An array of data sources from _template_data, so see _template_data for details:
	 * 		[
	 * 			{
	 * 				'name': OLE/Attendance Record/Activity/Merit/...,
	 * 				...
	 * 			},
	 * 			...
	 * 		]
	 * @return Return the data sources with name.
	 */
	_remove_data_sources_without_name: function(data_sources) {
		if (!data_sources) return [];
		
		return $.map(data_sources, function(n, i) {
			return (n && typeof n == 'object' && n.name) ? n : null;
		});
	},
	
	/**
	 * Re-display the header template.
	 */
	_update_header_template_view: function() {
		var data = this._get_template_data();
		$('#header_template_view').html(this._convert_placeholder_to_image(data.header_template));
		$('#header_template_view_none').toggle(!data.header_template);
	},
	
	/**
	 * Re-display the footer template.
	 */
	_update_footer_template_view: function() {
		var data = this._get_template_data();
		
		$('#footer_template_view').html(this._convert_placeholder_to_image(data.footer_template));
		$('#footer_template_view_none').toggle(!data.footer_template);
	},
	
	/**
	 * Re-display the section list.
	 */
	_update_section_list_view: function() {
		if (!this._template_data || !this._template_data.sections) return;
		
		var section_list = $('#section_list');
		
		// Remove the old sections.
		section_list.empty();
		
		// Add each section.
		var _this = this;
		$.each(this._get_template_data().sections, function(i, section) {
			if (section.section_type == 'table') {
				var table_data = section.table;
				if (!table_data) return;
				
				// Get the CSS styles for providing visual effect for some style settings.
				var section_title_style = _this._convert_template_style_to_css_style(table_data.section_title_style);
				var section_description_style = _this._convert_template_style_to_css_style(table_data.section_description_style);
				var column_title_style = _this._convert_template_style_to_css_style(table_data.column_title_style);
				var row_text_style = _this._convert_template_style_to_css_style(table_data.row_text_style);
				var row_statistics_style = _this._convert_template_style_to_css_style(table_data.row_statistics_style);
				
				// Add a table section - clone from a template and fill placeholders.
				// Placeholders are of the format ---PlaceHolderName---.
				var section_table = $('#section_table_template').clone().attr('id', '');
				section_table.html(
					section_table.html().replace(/---(\w+)---/g, function(str, p1) {
						switch (p1) {
							case 'section_index':
								return i;
								
							case 'section_title':
								// Escape the title.
								//return $('<div/>').text(table_data.section_title).html();
								//Fai 20120228 : line break for section Title
								if(table_data.section_title){
									  return table_data.section_title.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/gi, '$1'+ '<br />' +'$2');							  
								}else{
									return $('<div/>').text("").html();
								}
							
							case 'section_description':
								// show desc only when it is not empty
								// Eric Yip (20100719): line break in description
								// Henry Yuen (20100721): line break in description for IE
								if(table_data.section_description){									
								  //return $('<div/>').text(table_data.section_description).html().replace(/[\r\n]+/gi, "<br />");
								  //return $('<div/>').text(table_data.section_description).html().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/gi, '$1'+ '<br />' +'$2');
								  return table_data.section_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/gi, '$1'+ '<br />' +'$2');								  								  								  								  								  								 
								}
								else{
									return $('<div/>').text("").html();								
								}
								
							default:
								// Ignore unknown placeholders.
								return str;
						}
					})
				);
				// Provide visual effect for the style settings of the section title.
				section_table.find('.section_title').css(section_title_style);
				section_table.find('.section_description').css(section_description_style);
				// Add columns and data sources.
				if (table_data.columns && table_data.columns.length > 0) {
					// Need to append 'px' for border-width.
					var border_size = parseInt(table_data.border_size);
					border_size = isNaN(border_size) ? '0px' : (border_size + 'px');
					var data_source_table = section_table.find('.common_table_list')
						// Provide visual effect for the style settings of the table.
						.css({'border-width': border_size, 'border-color': table_data.border_color});
					var data_source_table_header = $('<tr/>').appendTo(data_source_table);
					// It seems that TCPDF uses a similar way to determine the column width.
					var default_column_width = (100 / table_data.columns.length) + '%';
					$.each(table_data.columns, function(i, n) {
						var th = $('<th/>')
							.text(!n.title ? '' : n.title)
							// Provide visual effect for the style settings of the column title.
							// Set the style of TH instead of TR, so that the style will not be overrided.
							.css(column_title_style)
							.appendTo(data_source_table_header);
						// Bad width may cause error in IE.
						try {
							th.attr('width', !n.width ? default_column_width : n.width);
						} catch (e) {
						}
					});
					if (table_data.data_sources && table_data.data_sources.length > 0) {
						$.each(table_data.data_sources, function(i, n) {
							var data_source_table_row = $('<tr/>').appendTo(data_source_table);
							$.each(table_data.columns, function(j, o) {
								// Support the case that a data source field may not be specified for a column.
								var data_source_name = !n || !n.name ? '' : n.name;
								var field_name = !n || !n.fields || !n.fields[j] ? '' : n.fields[j];
								var field_display_name = !field_name ? '-' : ('[' + (_this._get_data_source_field_display_name(data_source_name, field_name)) + ']');
								$('<td/>')
									.text(field_display_name)
									// Provide visual effect for the style settings of the row text.
									// Set the style of TD instead of TR, so that the style will not be overrided.
									.css(row_text_style)
									.appendTo(data_source_table_row);
							});
						});
						
						if (table_data.statistics_show == '1') {
							$.each(table_data.statistics_info, function(i, n) {
								var data_source_table_row = $('<tr/>').appendTo(data_source_table);
								$.each(table_data.columns, function(j, o) {
									// Support the case that a data source field may not be specified for a column.
									
									var __statType = n[j].statType;
									var __statInputText = n[j].statInputText;
									var __field_display_name = '';
									if (__statType == 'text') {
										__field_display_name = __statInputText;
									}
									else {
										if (__statType == '') {
											__field_display_name = '';
										}
										else {
											__field_display_name = '[' + StudentReportTemplateEditPage.get_statistics_option_display_name(__statType) + ']';
										}
									}
									
									$('<td/>')
										.text(__field_display_name)
										// Provide visual effect for the style settings of the row text.
										// Set the style of TD instead of TR, so that the style will not be overrided.
										.css(row_statistics_style)
										.appendTo(data_source_table_row);
								});
							});
						}
					}
				}
				// Append the section.
				section_table.appendTo(section_list).show();
				// Initialize thickbox.
				tb_init(section_table.find('.edit, .setting_row'));
			}
			else if (section.section_type == 'free_text') {
				var free_text_data = section.free_text;
				if (!free_text_data) return;
				
				// Add a free text section - clone from a template and fill placeholders.
				// Placeholders are of the format ---PlaceHolderName---.
				var section_free_text = $('#section_free_text_template').clone().attr('id', '');
				section_free_text.html(
					section_free_text.html().replace(/---(\w+)---/g, function(str, p1) {
						switch (p1) {
							case 'section_index':
								return i;
								
							case 'text_template':
								return _this._convert_placeholder_to_image(free_text_data.text_template);
								
							default:
								// Ignore unknown placeholders.
								return str;
						}
					})
				);
				// Append the section.
				section_free_text.appendTo(section_list).show();
				// Initialize thickbox.
				tb_init(section_free_text.find('.edit'));
			}
			else if (section.section_type == 'page_break') {
				// Add a page break section - clone from a template and fill placeholders.
				// Placeholders are of the format ---PlaceHolderName---.
				var section_page_break = $('#section_page_break_template').clone().attr('id', '');
				section_page_break.html(
					section_page_break.html().replace(/---(\w+)---/g, function(str, p1) {
						switch (p1) {
							case 'section_index':
								return i;
								
							default:
								// Ignore unknown placeholders.
								return str;
						}
					})
				);
				// Append the section.
				section_page_break.appendTo(section_list).show();
			}
		});
	},
	
	/**
	 * Check whether a section index is valid, and whether the section is of the given type.
	 * @private
	 */
	_check_section_index_and_type: function(section_index, section_type) {
		try {
			var data = this._get_template_data();
			if (!data.sections || data.sections.length == 0) throw('There is no section in the template.');
			var section = data.sections[section_index];
			if (!section) throw('The section with index "' + section_index + '" does not exist in the template.');
			if (section.section_type != section_type) throw('The section with index "' + section_index + '" is a "' + section.section_type + '" section, not a "' + section_type + '" section.');
		}
		catch (e) {
			alert(e);
			return false;
		}
		return true;
	},
	
	/**
	 * Check whether the data for a table section is valid.
	 * @private
	 */
	_check_table_data: function(table_data) {
		try {
			if (!table_data) throw('Table data is not given.');
			if (typeof table_data != 'object') throw('Table data should be an object.');
			if (!table_data.hasOwnProperty('section_title')) throw('Table data should provide a section title.');
			if (table_data.hasOwnProperty('columns') && !$.isArray(table_data.columns)) throw('Columns should be an array.');
			if (table_data.hasOwnProperty('data_sources') && !$.isArray(table_data.data_sources)) throw('Data sources should be an array.');
		}
		catch (e) {
			alert(e);
			return false;
		}
		return true;
	},
	
	/**
	 * Check whether the data for the style information of a table section is valid.
	 * @private
	 */
	_check_table_style_data: function(table_data) {
		try {
			if (!table_data) throw('Table data is not given.');
			if (typeof table_data != 'object') throw('Table data should be an object.');
			if (!table_data.hasOwnProperty('border_size')) throw('border_size is not given.');
			if (table_data.hasOwnProperty('section_title_style') && (!table_data.section_title_style || typeof table_data.section_title_style != 'object')) throw('section_title_style should be an object.');
			if (table_data.hasOwnProperty('column_title_style') && (!table_data.column_title_style || typeof table_data.column_title_style != 'object')) throw('column_title_style should be an object.');
			if (table_data.hasOwnProperty('row_text_style') && (!table_data.row_text_style || typeof table_data.row_text_style != 'object')) throw('row_text_style should be an object.');
		}
		catch (e) {
			alert(e);
			return false;
		}
		return true;
	},
	
	/*
	 *  Henry Yuen (2010-07-16): clone current template
	 */
	clone_template: function(form){		
		var data = this._get_template_data();
		//var regex = new RegExp("&?" + "template_id" + "=[^&]+");
		form.action = form.action.replace("template_id","parent_template_id");
		form.textfield.value = '(Cloned)' + form.textfield.value;		
		this.on_template_title_changed(form.textfield);		
		form.cloneButton.style.display = 'none';
		form.saveButton.style.display = 'inline';		
						
		//$("div.added_content").css("cursor","move");
		$("div.table_row_tool.row_content_tool").css("visibility","visible");		
		$("div.new_content_btn_tool").css("visibility","visible");
				
		window.scrollTo(0,0);
		
		$("#section_list").sortable("enable");									
	},	
	
	/*
	 *  Henry Yuen (2010-07-26): rearrange data section order
	 */
	 _arrange_data_section_order: function(sourceLoc, targetLoc){	 	
	 	var data = this._get_template_data();
	 	if(targetLoc > sourceLoc){ 	 	
	 		data.sections.splice(targetLoc + 1, 0, data.sections[sourceLoc]); 
			data.sections.splice(sourceLoc, 1);
	 	}
	 	else{
	 		data.sections.splice(targetLoc, 0, data.sections[sourceLoc]);
	 		data.sections.splice(sourceLoc + 1, 1);
	 	}	 						
		this._update_section_list_view();	
	 }
};

var section_list_sortable_start;
$(document).ready(function() {
	StudentReportTemplateEditPage.on_init();	
	
	// Henry Yuen (2010-07-26): support rearrange data section order
	$("#section_list").sortable({
   		start: function(event, ui) {
            section_list_sortable_start = ui.item.prevAll().length;
        },
        update: function(event, ui) {
        	var sourceLoc = section_list_sortable_start;
        	var targetLoc = ui.item.prevAll().length;
        	StudentReportTemplateEditPage._arrange_data_section_order(sourceLoc, targetLoc);        	        	
        }
	});
	$("#section_list").sortable("<?=$template_inputdate == ''? "disable": "enable"?>");
	
	if ('<?=$template_is_default?>' == '1') {
		$('input#cloneButton').click();
	}
});
</script>

<form action="index.php?task=edit_update&amp;template_id=<?= $template_id ?>" method="post">
<!-- ###### Tabs Start ######-->
<?php include 'tab_menu.tmpl.php'; ?>
<!-- ###### Tabs End ######-->
			  <!--###### Content Board Start ######-->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="main_content">
                      	<div class="navigation">
                      		<img src="<?=$intranet_httppath?>/images/2009a/nav_arrow.gif" width="15" height="15" align="absmiddle" /><a href="index.php?task=list"><?= srt_lang('Template List') ?></a><img src="<?=$intranet_httppath?>/images/2009a/nav_arrow.gif" width="15" height="15" align="absmiddle" /><?= srt_lang('New Template') ?><br />
                      	</div>
                      	<?=$h_edit_default_template_warning?>
                      	<br style="clear:both;" />
                     
                      <table class="form_table">

                        <col class="field_title" />
                        <col  class="field_c" />
                        <tr>
                          <td><span class="tabletextrequire">*</span><?= srt_lang('Template Title') ?></td>
                          <td>:</td>
                          <td ><input name="textfield" type="text" id="textfield" class="textbox_name" value="<?= htmlspecialchars($template->get_template_title()) ?>" onchange="StudentReportTemplateEditPage.on_template_title_changed(this)"/></td>
                        </tr>
                        <tr>
                          <td colspan="3"><em class="form_sep_title"> - <?= srt_lang('Report Template') ?> -</em></td>
                        </tr>

                        <tr>
                          <td colspan="3">
                           <!--###########report Content ##############-->     
                          <div class="report_template_paper"> 
                          
                          <!-- Header -->
                          <div class="template_part" > 
                              <h1> <span class="row_content">- <?= srt_lang('Header') ?> -</span>
                              		<!-- Henry Yuen (2010-07-25): hide the div for sample template-->
                              		<div class="table_row_tool row_content_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>"><a href="index.php?task=edit_header&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="edit thickbox" title="<?= srt_lang('Edit') ?>"></a></div>                              		                              	
                                    <p class="spacer"></p>
                              </h1>
                              <p class="spacer"></p> 
                             <div class="template_part_content">
                         	 	 <span class="content_none" id="header_template_view_none">&lt; <?= srt_lang('Header Here') ?> &gt;</span>
                         	 	 <div id="header_template_view"></div>
                             </div>
                          </div>
                          <br />
                          <!-- Content -->
                          <div class="template_part"> 
                              <h1> <span class="row_content">- <?= srt_lang('Data Content') ?> -</span> 
                                  <div class="table_row_tool row_content_tool"></div>
                              <p class="spacer"></p>
                              </h1>
                              <p class="spacer"></p>
                              
                               <div class="template_part_content" id="section_list">
								 <span class="content_none">&lt; <?= srt_lang('Data Content Here') ?> &gt;</span>                         	 	                       	     
                         	  </div>
                         	    <div  class="added_content" id="section_table_template" style="display:none">
                                  <fieldset>
                                  <legend><?= srt_lang('Data') ?></legend>
                                  	<!-- Henry Yuen (2010-07-25): hide the div for sample template-->                              		                                   
                                  	<!-- Henry Yuen (2010-07-25): support moving section order-->
                                    <div class="table_row_tool row_content_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>"><a href="index.php?task=edit_section_table&amp;section_index=---section_index---&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="edit " title="<?= srt_lang('Edit') ?>"></a><a href="index.php?task=edit_section_table_style&amp;section_index=---section_index---&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="setting_row " title="<?= srt_lang('Setting') ?>"></a><a href="javascript:StudentReportTemplateEditPage.delete_table_data(---section_index---)" class="delete " title="<?= srt_lang('Remove Data') ?>"></a></div>
                                    <div class="added_content_content"> <div class="section_title">---section_title---</div>
                                                                        <div class="section_description">---section_description---</div>                                               
                                        <p class="spacer"></p>
                                      <table class="common_table_list">
                                      </table>
                                    </div>
                                  </fieldset>
                       	      </div>
                       	                             	                             	      
                 	    	<div  class="added_content" id="section_free_text_template" style="display:none">
                              <fieldset>
                              <legend><?= srt_lang('Text') ?></legend>
                                <!-- Henry Yuen (2010-07-25): hide the div for sample template-->                              		                                                                       
                                <div class="table_row_tool row_content_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>"><a href="index.php?task=edit_section_free_text&amp;section_index=---section_index---&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="edit " title="<?= srt_lang('Edit') ?>"></a><a href="javascript:StudentReportTemplateEditPage.delete_free_text_data(---section_index---)" class="delete " title="<?= srt_lang('Remove Text') ?>"></a></div>
                                <div class="added_content_content">
                                  ---text_template---
                                </div>
                              </fieldset>
               	      		</div>
                         	  <div  class="added_content section_page_break" id="section_page_break_template" style="display:none"> <span class="row_content"><strong><?= srt_lang('Page Break') ?></strong></span>
                                   <!-- Henry Yuen (2010-07-25): hide the div for sample template-->
                                   <div class="table_row_tool row_content_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>"><a href="javascript:StudentReportTemplateEditPage.delete_page_break(---section_index---)" class="delete " title="<?= srt_lang('Remove Page Break') ?>"></a>
                                        <p class="spacer"></p> </div>
                         	      <p class="spacer"></p>
                       	      </div>
                       	                             	                             	                             	   
                               <div class="new_content">
                               <!-- Henry Yuen (2010-07-25): hide the div for sample template-->
								<div class="new_content_btn_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>" onclick="MM_showHideLayers('new_tool_b','','show')"><a href="javascript:void(0)"><?= srt_lang('Add') ?></a></div>								
								<div class="new_content_btn_tool_layer" id="new_tool_b" onClick="MM_showHideLayers('new_tool_b','','hide')">
								<a href="index.php?task=edit_section_table&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="thickbox"><?= srt_lang('Data') ?></a>								<!--<a href="#">import csv</a>-->	
								<a href="index.php?task=edit_section_free_text&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="thickbox"><?= srt_lang('Text') ?></a>	   
                                <a href="javascript:void(0)" onclick="StudentReportTemplateEditPage.add_page_break()" class="page_break"><?= srt_lang('Page Break') ?></a>	
								</div>
  							  </div>								
								 <p class="spacer"></p>
                        </div>
                        
                          <br />
                          <!-- Footer -->
                            <div class="template_part" > 
                              <h1> <span class="row_content">- <?= srt_lang('Footer') ?> -</span>
                                <!-- Henry Yuen (2010-07-25): hide the div for sample template-->                              		
                               <div class="table_row_tool row_content_tool" style="visibility:<?= $template_inputdate == ''? "hidden": "visible"?>"><a href="index.php?task=edit_footer&amp;KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=800" class="edit thickbox" title="<?= srt_lang('Edit') ?>"></a></div>
                           
	                            <p class="spacer"></p>
                               </h1>
                          		<p class="spacer"></p> 
                          		<div class="template_part_content">
                         	 	  <span class="content_none" id="footer_template_view_none">&lt; <?= srt_lang('Footer Here') ?> &gt;</span>
                         	 	 <div id="footer_template_view"></div>
                           		</div> 
                           		<p class="spacer"></p>
                           	 </div>
                          
                          </div>
                           <!--#########################-->                         
                           </td>
                        </tr>

                      </table>
                      <div class="edit_bottom">
                        <p class="spacer"></p>
                        <!-- Begin Henry Yuen (2010-07-16): clone template-->
                        <input id="cloneButton" name="cloneButton" type="button" class="formbutton" style="display:<?= $template_inputdate == 'new'?"none": "inline"?>"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Clone Template') ?>" onclick="StudentReportTemplateEditPage.clone_template(this.form)" />
		 				<!-- End Henry Yuen (2010-07-16): clone template-->
                        <input id="saveButton" name="saveButton" type="button" class="formbutton" style="display:<?= $template_inputdate == ''?"none": "inline"?>"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Save and Submit') ?>" onclick="StudentReportTemplateEditPage.on_submit(this.form)" />
                        <input id="submit2" name="submit2" type="button" class="formbutton" onclick="location.href='index.php?task=list'"
		onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
                        <p class="spacer"></p>
                      </div>
                                      </td>
                    </tr>
                  </table>
		    <!--###### Content Board End ######-->
</form>
