<!-- Modify by Henry Yuen-->
<!-- modifications
	Omas (2017-02-10): fix #J112780 
	Ivan (2013-01-09): added statistics coding
	Henry Yuen (2010-06-30): add section description
-->
<script>
var EditSectionPage ={
	source: [],
	_template_data: <?= count($aryData) > 0 ? $json->encode($aryData) : '{}' ?>,
	// Map from data source/field name to its display name.
	// The key for a field is of the format "$data_source_name.$field_name".
	_data_source_and_field_display_names: <?= count($data_source_and_field_display_names) > 0 ? $json->encode($data_source_and_field_display_names) : '{}' ?>,
	_section_index:"<?=$section_index?>",
	source_index:0,
	column_increment:0,
	on_init: function(){
		/*Edit mode*/
		
		if(this._section_index != ""){
			this.set_table_data();			
		}else{		
			var index = this.source_index;
			var src_opt = this.get_source_selection(index);
			
			this.add_extra_column("");
        	this.add_extra_column("");
			this.add_new_source(index, src_opt, "&nbsp;");
			
			// Update Criteria tab display -> show Criteria Div for the selected sources
			 EditSectionPage.set_criteria_table_data();
			 EditSectionPage.update_criteria_div_display();
		}
	},
	switch_tab: function(jsTargetTab) {
		var jsTargetDivID = 'tab_div_' + jsTargetTab;
		var jsTargetTabMenuID = 'tab_menu_' + jsTargetTab;
		
		$('div.tab_div').fadeOut();
		$('div#' + jsTargetDivID).fadeIn();
		
		$('li.tab_menu').removeClass('selected');
		$('li#' + jsTargetTabMenuID).addClass('selected');
	},
	get_src_id: function(id){		
		aryId = id.split('src_tr');
		return aryId[1];
	},
	set_table_data: function(){
		var index = 1;
			
		 var objEditData = parent.StudentReportTemplateEditPage.get_table_data(this._section_index);
		 $("#section_title").val(objEditData.section_title);
		 
		 <!-- Henry Yuen (2010-06-30) add section description-->
		 $("#section_description").val(objEditData.section_description);
		 
		 var num_column = ($(objEditData.columns).size());
		 
		 /*Set column title fields*/
		 var counter_here = 0;
		 $.each(objEditData.columns, function(n){
		 	EditSectionPage.add_extra_column(this.title);
		 });

		 var column_count = $(".column_title").size();
		 $.each(objEditData.data_sources, function(n){
		 	
		 	var src_opt = EditSectionPage.get_source_selection(index, this.name);
		 	
		 	
		 	EditSectionPage.add_new_source(index, src_opt, "&nbsp;", this.max_num_of_records);
		 	
		 	var field_count = 0;
		 	
		 	$.each(this.fields, function(x){
		 		field_count++;	
		 		// get_column_selection is overloaded, there may be problem if this is empty.
		 		
		 		EditSectionPage.get_column_selection(index, this == '' ? '--' : this, field_count);		 	
		 	});
		 	
		 	// Add missing fields.
		 	while (field_count < column_count) {
		 		field_count++;
		 		EditSectionPage.get_column_selection(index, "--", field_count);
		 	}
		 	
		 	// column_increment is used when adding columns.
		 	EditSectionPage.column_increment = field_count;
		 	
		 	index++;
		 });
		 

		 // statistics value
		 var showStatistics = objEditData.statistics_show;
		 if (showStatistics) {
		 	$('input#show_statistics_chk_1').attr('checked', 'checked');
		 }
		 else {
		 	$('input#show_statistics_chk_1').attr('checked', '');
		 }

		 // fix old template js error problem #J112780 
		 if (typeof objEditData.statistics_info !== 'undefined') {
			 $.each(objEditData.statistics_info, function(_rowIndex, _rowInfoAry){
			 	var _rowNum = _rowIndex + 1;
	
			 	$.each(_rowInfoAry, function(_colIndex, _statInfoAry){
			 		var _colNum = _colIndex + 1;
			 		
			 		$('select#statistics_option_sel_' + _rowNum + '_' + _colNum).val(_statInfoAry.statType);
			 		$('input#statistics_option_tb_' + _rowNum + '_' + _colNum).val(_statInfoAry.statInputText);
			 	});
			 });
			 this.changed_show_statistics_checkbox(1);
		 }
		 
		 this.update_sort_by_column(objEditData.sort_by_column_number, objEditData.sort_by_column_number2);		 
		 $("#sort_by_asc").val(objEditData.is_ascending);	
		 $("#sort_by_asc2").val(objEditData.is_ascending2);
		 
		  

		 // Update Criteria tab display -> show Criteria Div for the selected sources
		 EditSectionPage.set_criteria_table_data();
		 EditSectionPage.update_criteria_div_display();
	},
	
	set_criteria_table_data: function() {
		var objEditData, objFilterCriteria;
		var i, j;
		var jsSectionIndex = EditSectionPage._section_index;
		var jsIsNew = (jsSectionIndex==null || jsSectionIndex=='')? true : false;
		
		if (!jsIsNew) {
			// Load filter data
			objEditData = parent.StudentReportTemplateEditPage.get_table_data(jsSectionIndex);
			objFilterCriteria = objEditData.filter_criteria;
			jsIsNew = (objFilterCriteria==null)? true : false;
		}
		
		// Loop each Data Sources
		var objCriteriaDataField = parent.StudentReportTemplateEditPage.get_data_content_source_criteria_fields();
		$.each(objCriteriaDataField, function(jsThisDataSource, jsThisDataFieldArr) {
			var jsThisDataSourceWithoutSpace = EditSectionPage._get_text_without_space(jsThisDataSource);
			
			// Loop each Data Field
			$.each(jsThisDataFieldArr, function(i, jsThisDataField) {
				
				if (jsIsNew == 0) {
					// var jsThisDataFieldInfo = objFilterCriteria.OLE.Title;
					eval("var jsThisDataFieldInfo = objFilterCriteria." + jsThisDataSourceWithoutSpace + "." + jsThisDataField + ";");
				}
				
				var jsThisDataFieldSourceKey = jsThisDataSourceWithoutSpace + '###' + jsThisDataField;
				if (jsThisDataFieldSourceKey == 'OLE###CategoryName' || jsThisDataFieldSourceKey == 'OLE###ELE' || jsThisDataFieldSourceKey == 'PAKP_Outside_School###CategoryName') {
					if (jsIsNew == 0) {
						/*** If not new => load the settings into the UI ***/
												
						// Uncheck all options first
						var jsThisGlobalChkID = (jsThisDataField == 'CategoryName')? jsThisDataSourceWithoutSpace + '_CategoryChk_Global' : jsThisDataSourceWithoutSpace + '_ELEChk_Global';
						var jsThisChkClass = (jsThisDataField == 'CategoryName')? jsThisDataSourceWithoutSpace + '_CategoryChk' : jsThisDataSourceWithoutSpace + '_ELEChk';
						$('input#' + jsThisGlobalChkID).attr('checked', '');
						$('input.' + jsThisChkClass).attr('checked', '');
						
						// Check the selected options
						$.each(jsThisDataFieldInfo, function(j, jsThisSelectedOptionObj) {
							var jsThisObjRecordID = jsThisSelectedOptionObj.RecordID;
							var jsThisChkID = jsThisChkClass + '_' + jsThisObjRecordID;
							jsThisChkID = jsThisChkID.replace('[', '\\[').replace(']', '\\]');		// for escaping special chars "[" & "]" of jQuery in "xxx_ELEChk_[xx]"
							
							$('input#' + jsThisChkID).attr('checked', 'checked');
						});
					}
				}
				else {
			// Get Criteria Data
					if (jsIsNew == 0) {
						/*** If not new => load the settings into the UI ***/
						if (jsThisDataFieldInfo == null)
						{
							EditSectionPage.add_criteria_option_div(jsThisDataSource, jsThisDataField, 0, null);
						}
						else {
							var jsThisCriteriaInfoArr = jsThisDataFieldInfo.criteria_info_arr;
							var jsThisCriteriaRelationshipArr = jsThisDataFieldInfo.criteria_relationship_arr;
							
							var jsThisCriteriaLevelCounter = 0;
							$.each(jsThisCriteriaInfoArr, function (j, jsThisCriteriaInfo) {
								var jsThisRelationshipInfo = jsThisCriteriaRelationshipArr[j];
								
								if (jsThisRelationshipInfo == null) {
									// do nth
								}
								else {
									jsThisCriteriaInfo['condition'] = jsThisRelationshipInfo.condition;
								}
								
								EditSectionPage.add_criteria_option_div(jsThisDataSource, jsThisDataField, jsThisCriteriaLevelCounter, jsThisCriteriaInfo);
								jsThisCriteriaLevelCounter++;
							});
							
							// If no criteria elements => create a default one
							if (jsThisCriteriaLevelCounter == 0) {
								EditSectionPage.add_criteria_option_div(jsThisDataSource, jsThisDataField, 0, null);
							}
						}
					}
					else {
						/*** If new => add default filtering options only => No need to load settings ***/
						EditSectionPage.add_criteria_option_div(jsThisDataSource, jsThisDataField, 0, null);
					}
				}
			});		// End for $.each(jsThisDataFieldArr, function (i, jsThisDataField)
		});		// End $.each(objCriteriaDataField, function(jsThisDataSource, jsThisDataFieldArr)
	},
	
	add_criteria_option_div: function(data_source, data_field, criteria_level, settings_arr) {
		var data_source_without_space = EditSectionPage._get_text_without_space(data_source);
		var criteria_sel_value = (settings_arr == null)? '' : settings_arr['criteria'];
		var criteria_text_value = (settings_arr == null)? '' : htmlspecialchars(settings_arr['criteria_text']);
		var criteria_relationship_value = (settings_arr == null)? '' : settings_arr['condition'];
		
		// Insert this criteria section before this div
		var bottom_div_id = 'criteria_td_bottom_div_' + data_source_without_space + '_' + data_field;
		
		var main_div_id = 'data_field_criteria_main_div_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var main_div_class = (criteria_level==0)? '' : 'data_field_criteria_main_div_' + data_source_without_space + '_' + data_field;	// for deleting all div if selected "No Criteria" in the first level
		
		var tb_id = 'criteria_tb_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var tb_class = 'criteria_tb_' + data_source_without_space + '_' + data_field;
		
		var tb_display;
		if (criteria_level == 0) {
			// for first criteria level => display textbox according to the criteria selection
			tb_display = (criteria_sel_value == '' || criteria_sel_value == 'NoCriteria'  || criteria_sel_value == 'MustHaveText')? 'none' : 'block';
		}
		else {
			// for not first criteria level => always display textbox
			tb_display = 'block';
		}
		
		var condition_div_id = 'data_field_criteria_condition_div_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var condition_div_display = (criteria_sel_value == '' || criteria_sel_value == 'NoCriteria' || criteria_sel_value == 'MustHaveText')? 'none' : 'block';
		
		
		// Do not insert the div if the div alreadt exist
		if ($('div#' + main_div_id).length == 0) {
			var x = '';
			
			x += '<div id="' + main_div_id + '" class="' + main_div_class + '">';
				x += '<div>';
					x += '<span style="float:left;">';
						x += EditSectionPage.get_criteria_selection(data_source, data_field, criteria_level, criteria_sel_value);
					x += '</span>';
					x += '<span style="float:left;width:60%;">';
						x += '<input type="text" id="' + tb_id + '" class="textboxtext ' + tb_class + '" style="display:' + tb_display + ';" value="' + criteria_text_value + '" />';
					x += '</span>';
				x += '</div>';
				
				if (criteria_level == 0) {
					// support 2 levels criteria only at this time
					x += '<div id="' + condition_div_id + '" style="display:' + condition_div_display + ';">';
						x += '<br style="clear:both;" />';
						x += '<br style="clear:both;" />';
						x += EditSectionPage.get_condition_selection(data_source, data_field, criteria_level, criteria_relationship_value);
					x += '</div>';
					x += '<br style="clear:both;" />';
				}
			x += '</div>';
			
			$(x).insertBefore('div#' + bottom_div_id);
			$('input#' + tb_id).focus();
		}
	},
	
	delete_criteria_option_div: function(data_source, data_field, criteria_level) {
		var data_source_without_space = EditSectionPage._get_text_without_space(data_source);
		var target_div_id = 'data_field_criteria_main_div_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		
		$('div#' + target_div_id).remove();
		//$('div#' + target_div_id).detach();	// "detach" can keep DOM properties but need jQuery v1.4 or above
	},
	
	get_criteria_selection: function(data_source, data_field, criteria_level, selected_value) {
		var data_source_without_space = EditSectionPage._get_text_without_space(data_source);
		var criteria_option_arr = parent.StudentReportTemplateEditPage.get_data_content_criteria_options();
		var num_of_criteria_option = criteria_option_arr.length;
		
		var sel_id = 'criteria_sel_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var sel_class = 'criteria_sel_' + data_source_without_space + '_' + data_field;
		var sel_on_change = "EditSectionPage.changed_criteria_selection('" + data_source_without_space + "', '" + data_field + "', '" + criteria_level + "', this.value);";
		
		var x = '';
		var i, j;
		
		x += '<select id="' + sel_id + '" class="' + sel_class + '" onchange="' + sel_on_change + '">';
			for (i=0; i<num_of_criteria_option; i++)
			{
				var jsThisCriteriaArr = criteria_option_arr[i];
				var jsThisNumOfCriteria = jsThisCriteriaArr.length;
				
				// Only first criteria selection has the "No Criteria" option
				if (criteria_level > 0 && i == 0) {
					continue;
				}
				
				$.each(jsThisCriteriaArr, function (jsThisCriteria, jsThisCriteriaKey) {
					var jsThisCriteriaDisplay = parent.StudentReportTemplateEditPage.get_data_content_criteria_option_display_name(jsThisCriteriaKey);
					var jsThisSelected = (jsThisCriteriaKey==selected_value)? 'selected' : '';
					
					if (i == 0) {
						// No Criteria
						jsThisSelected = (selected_value=='' || selected_value==null)? 'selected' : jsThisSelected;
					}

					if (criteria_level > 0 && jsThisCriteriaKey == 'MustHaveText') {
						return true; 
					}

					x += '<option value="' + jsThisCriteriaKey + '" ' + jsThisSelected + '>' + jsThisCriteriaDisplay + '</option>';
				});
				
				if (i < num_of_criteria_option - 1) {
					x += '<option disabled>-----</option>';
				}
			}
		x += '</select>';
		
		return x;
	},
	
	get_condition_selection: function(data_source, data_field, criteria_level, selected_value) {
		if (selected_value != null) {
			selected_value = selected_value.toLowerCase();
		}
		var data_source_without_space = EditSectionPage._get_text_without_space(data_source);
		var condition_option_arr = parent.StudentReportTemplateEditPage.get_data_content_criteria_condition_options();
		
		var sel_id = 'condition_sel_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var sel_class = 'condition_sel_' + data_source_without_space + '_' + data_field;
		var sel_on_change = "EditSectionPage.changed_condition_selection('" + data_source_without_space + "', '" + data_field + "', '" + criteria_level + "', this.value);";
		
		var x = '';
		x += '<select id="' + sel_id + '" class="' + sel_class + '" onchange="' + sel_on_change + '">';
			var jsThisSelected = (selected_value==null || selected_value=='')? 'selected' : '';
			x += '<option value="" ' + jsThisSelected + '>' + '<?=Get_Selection_First_Title($Lang['Btn']['Select'])?>' + '</option>';
			
			$.each(condition_option_arr, function(jsThisKey, jsThisDisplay) {
				var jsThisSelected = (jsThisKey==selected_value)? 'selected' : '';
				x += '<option value="' + jsThisKey + '" ' + jsThisSelected + '>' + jsThisDisplay + '</option>';
			});
		x += '</select>';
		
		return x;
	},
	
	add_extra_source: function(){
		/* Get current source count */
		var src_count = parseInt($(".src_tr").size())+1;
		var src_opt = this.get_source_selection(src_count);
		var column_opt ="&nbsp;";
		this.add_new_source(src_count, src_opt, column_opt);
	},
	
	add_extra_column: function(title){
		/*Count columns existing*/
		
		//var column_count = parseInt($(".column_title").size())+1;
		EditSectionPage.column_increment =  parseInt(EditSectionPage.column_increment)+1;
		column_count = EditSectionPage.column_increment;
		var header_col = '<th class="sub_row_top" id="column_th'+column_count+'">'+
		  '<span class="col_index_display" style="float:left;">'+column_count+')</span>'+
          '<span class="row_content">'+
            '<input name="column_title'+column_count+'" type="text" class="textbox_date column_title" id="column_title'+column_count+'" value="'
              // Escape the title.
              + $('<div/>').text(!title ? '' : title).html().replace(/"/g, '&quot;')
              + '"/>'+
            '</span>'+
              '<div class="table_row_tool row_content_tool"><span class="table_row_tool"><a href="Javascript:EditSectionPage.delete_column('+column_count+');" class="delete_dim" title="<?= srt_lang('Delete') ?>"></a></span></div></th>';
              
        $("#top_col").attr("colSpan", parseInt($("#top_col").attr("colSpan"))+1);
        
        if($("#col_title_empty").length>0)
        	$(header_col).insertBefore("#col_title_empty");
        else
			$("#data_table").append(header_col);
		
		//var column_count = parseInt($(".column_title").size())		
		$(".src_tr").each(function(){
			
			EditSectionPage.column_increment = parseInt(EditSectionPage.column_increment);
			
			var id = EditSectionPage.get_src_id($(this).attr('id'));
			var column_container = EditSectionPage.add_column_container(id, EditSectionPage.column_increment);
			$(column_container).insertBefore("#src_delete"+id);
		
			// Only get column selection if source has been selected			
			if($("#src_opt"+id).val()  != ""){
				$(".col_td"+id).each(function(){
					
					var col_index = $(this).attr('id').split("col_td"+id+"_");
					col_index = col_index[1];
					
					if(($("#col_td"+id+"_"+col_index).text()).length < 10){								
						EditSectionPage.get_column_selection(id, "--", col_index);
					}
				});
			}
		});
		
		$('tr#add_new_source_tr').append('<td>&nbsp;</td>');
		
		
		// statistics row
		var statRowNum = 1;			// "1" means row 1, for future enhancement to have multiple stat rows
		var statSel = this.get_statistics_type_selection(statRowNum, column_count);
		var statInput = this.get_statistics_text_input(statRowNum, column_count, '');
		
		$('td#statistics_settings_last_td_' + statRowNum).before('<td>' + statSel + statInput + '</td>');
		this.changed_show_statistics_checkbox(1);	// initialize the selection and textbox disable status
		
		
		this.re_calculate_column_index();
		this.update_sort_by_column("", "");
	},
	update_sort_by_column: function(selected_opt, selected_opt2){
		var column_count = parseInt($(".column_title").size());
		selected_opt = (selected_opt=="")?$("#sort_by_column option:selected").val():selected_opt;
		
		var x = '<select name="sort_by_column" id="sort_by_column">';
		
		for(var i=1; i<=column_count; i++){
			extra = (i==selected_opt)? "SELECTED" : "";
			x += '<option value="'+i+'" '+extra+'><?= srt_lang('Column') ?> '+i+'</option>';
		}
		x += '</select>';
		$("#sort_by_column_td").html(x);
		

		selected_opt2 = (selected_opt2=="")?$("#sort_by_column2 option:selected").val():selected_opt2;
		
		var x = '<select name="sort_by_column2" id="sort_by_column2">';
		
		for(var i=1; i<=column_count; i++){
			extra = (i==selected_opt2)? "SELECTED" : "";
			x += '<option value="'+i+'" '+extra+'><?= srt_lang('Column') ?> '+i+'</option>';
		}
		x += '</select>';
		$("#sort_by_column_td2").html(x);
	},
	delete_source: function(index){
		if(confirm("<?= srt_lang('Are you sure you want to delete this data?') ?>")){
			$("#src_tr"+index).remove();
		}
	},
	delete_column: function(column_index){
		if(confirm("<?= srt_lang('Are you sure you want to delete this column?') ?>")){
			$("#top_col").attr("colSpan", parseInt($("#top_col").attr("colSpan"))-1);
			$("#column_th"+column_index).remove();
			$(".src_tr").each( function(){				
				var id = EditSectionPage.get_src_id($(this).attr('id'));								
				$('#col_td'+id+'_'+column_index).remove();
			});
			this.re_calculate_column_index();
			this.update_sort_by_column("", "");
		}
	},
	
	get_source_selection: function(index, selected_opt){
		
		var source = this._template_data;
		var max = source.length;
		
		/* Get Source */
		var x = "<select id='src_opt"+index+"' class='src_opt_sel' style='width:150px' onChange='EditSectionPage.get_column_selection("+index+", \"\");EditSectionPage.update_criteria_div_display();'>"+
				"<option value=''>-- <?= srt_lang('Select') ?> --</option>";
				
		$.each(source, function(i, val) {
			extra = (i==selected_opt)? "SELECTED" : "";
			var display_name = EditSectionPage._get_data_source_display_name(i);
			x += "<option value='"+i+"' "+extra+" title='" + display_name.replace(/'/g, '&#39;') + "'>"+display_name+"</option>";
		});
		
		x += "</select>";
		
		return x;
	},
	get_column_selection: function(index, selected_opt, col_index){
		
		var source = this._template_data;		
		var source_name = $("#src_opt"+index+" option:selected").val();
		
		var source_data = source[source_name];
		var max = (source_data==null)? 0 : source_data.length;
		//var col_index = $("[name=col"+index+"[]]").size();
		
		
		
		if(selected_opt ==""){			
			// APPLY TO ALL
			$("[name=col_td"+index+"[]]").each(function(){
				var ary_col_index = ($(this).attr('id')).split('col_td'+index+'_');
				col_index = ary_col_index[1];
				var x = "<select id='col_opt"+index+"_"+col_index+"' name='col"+index+"[]' class='col_opt"+index+"' onChange='EditSectionPage.update_column_title("+col_index+", this);'>"+
						"<option value=''>-- <?= srt_lang('Select') ?> --</option>";
				for(var i=0; i<max; i++){
					data = source_data[i];
					extra = (data==selected_opt)? "SELECTED" : "";
					var display_name = EditSectionPage._get_data_source_field_display_name(source_name, data);
					x += "<option value='"+data+"'  "+extra+">"+display_name+"</option>\n";
				}
				
				x += "</select>";
				$(this).html(x);
			});
		}else{			
			var x = "<select id='col_opt"+index+"_"+col_index+"' name='col"+index+"[]' class='col_opt"+index+"' onChange='EditSectionPage.update_column_title("+col_index+", this);'>"+
						"<option value=''>-- <?= srt_lang('Select') ?> --</option>";
						for(var i=0; i<max; i++){
							data = source_data[i];
							extra = (data==selected_opt)? "SELECTED" : "";
							var display_name = EditSectionPage._get_data_source_field_display_name(source_name, data);
							x += "<option value='"+data+"'  "+extra+">"+display_name+"</option>\n";
						}
		
			x += "</select>";
			
			// Apply to specific column selection box
			$('#col_td'+index+'_'+col_index).html(x);
		}
	},
	update_column_title: function(column_index, objSelect){
		if($("#column_title"+column_index).val() == "" && objSelect && objSelect.value){
			$("#column_title"+column_index).val($(':selected', objSelect).text());
		}
	},
	add_column_container: function(index, i){
		return '<td id="col_td'+index+'_'+i+'" name="col_td'+index+'[]" class="col_td'+index+'">&nbsp;</td>\n';
	},
	add_new_source: function(index, src_opt, column_opt, num_record_opt){
		num_record_opt = typeof num_record_opt == 'undefined' || num_record_opt == null ? '' : num_record_opt;
		
		var x = "";
		var selected = "SELECTED"
		var column_count = parseInt($(".column_title").size())+1;
		
		x = "<tr class='src_tr' id='src_tr"+index+"'><td>"+src_opt+"</td>\n";
		x += '<td><input name="num_record'+index+'" id="num_record'+index+'" value="' + num_record_opt + '" class="textboxtext num_record" /></td>\n';
		
		// Add containers for data source field dropdown.
		var _this = this;
		$('.column_title').each(function() {
			// Derive the column index from the ID of the column title.
			var column_index = /\d+/.exec($(this).attr('id'));
			x += _this.add_column_container(index, column_index);
		});
		
        x += '<td id="src_delete'+index+'"><span class="table_row_tool"><a href="javascript:EditSectionPage.delete_source('+index+');" class="delete_dim" title="<?= srt_lang('Delete') ?>"></a></span></td>';
        x += '</tr>';
        
        $(x).insertBefore("#add_new_source_tr");
	},
	re_calculate_column_index: function(){
		var count = 1;
		$(".col_index_display").each(function(){
			$(this).text(count+")");
			count++;
		});
	},
	onSubmit: function(){
		/*Checking*/
		if($("#section_title").val() == ""){$("#section_title").focus(); alert("<?= srt_lang('Please fill in section title.') ?>"); return; }
		
		// Check whether the column titles are given.
		// Trim the values too.
		try {
			$('.column_title').each(function() {
				var column_title = $.trim($(this).val());
				$(this).val(column_title);
				if (column_title == "") {
					$(this).focus();
					throw("<?= srt_lang('Please fill in column title.') ?>");
				}
			});
		} catch (e) {
			alert(e);
			return;
		}
		
		// Check whether the values for the number of records are integers, if given.
		// Trim the values too.
		try {
			$('.num_record').each(function() {
				var num_of_records = $.trim($(this).val());
				$(this).val(num_of_records);
				if (num_of_records && parseInt(num_of_records) != num_of_records - 0) {
					$(this).focus();
					throw('<?= srt_lang('Please enter an integer.') ?>');
				}
			});
		} catch (e) {
			alert(e);
			return;
		}
		
		
		var objCriteriaData = this.get_criteria_data();		
		var objData = {'section_title': $("#section_title").val(),
						'section_description': $("#section_description").val(),	
	  					'sort_by_column_number': $("#sort_by_column option:selected").val(),
	  					'is_ascending': $("#sort_by_asc option:selected").val(),
	  					'sort_by_column_number2': $("#sort_by_column2 option:selected").val(),
	  					'is_ascending2': $("#sort_by_asc2 option:selected").val(),
	  					'columns': $.map($('.column_title'), function(n, i) {	  						
	  						return {
	  							title: $(n).val()
	  						};
	  					}),
	  					'data_sources': $.map($(".src_tr"), function(n,i){
	  						
	  						var id = EditSectionPage.get_src_id($(n).attr('id'));
							
	  						return {
	  							name : $("#src_opt"+id+" option:selected").val(),
	  							max_num_of_records : $("#num_record"+id).val(),
	  							fields: $.map($(".col_opt"+id), function(n2, i2){
	  								return $(n2).val();
	  							})
	  						};
	  					}),
	  					statistics_show: ($('input#show_statistics_chk_1').attr('checked'))? 1 : 0,
	  					statistics_info: $.map($(".stat_tr"), function(n, i){
	  						var _rowNum = i + 1;
	  						
	  						return new Array (
	  							$.map($(".statistics_option_sel_" + _rowNum), function(n2, i2){
	  								var _statSelId = $(n2).attr('id');
	  								var _statTbId = _statSelId.replace('statistics_option_sel_', 'statistics_option_tb_');
	  						
			  						var _statType = $(n2).val();
			  						var _statInputText = $('input#' + _statTbId).val();
			  									  								
									return { 'statType': _statType, 'statInputText': _statInputText };
	  							})
	  						);
						}),
	  					'filter_criteria' : objCriteriaData
		}
		
		if(this._section_index == ""){
			parent.StudentReportTemplateEditPage.add_table_data(objData);
		}else{
			parent.StudentReportTemplateEditPage.update_table_data(this._section_index, objData);
		}
		parent.tb_remove();	
	},
	
	get_criteria_data: function() {
//		var objCriteriaData = { 	
//								'OLE': {
//									'Title': {
//										'criteria_info_arr': [
//											{
//												'criteria': 'NoCriteria',
//												'criteria_text': ''
//											},
//											{
//												'criteria': 'NoCriteria',
//												'criteria_text': ''
//											}
//										],
//										'criteria_relationship_arr': [
//											{
//												'condition': ''
//											}
//										]
//									},
//									'Role' : {
//										'criteria_info_arr': [
//											{
//												'criteria': 'TextContains',
//												'criteria_text': 'aaa'
//											},
//											{
//												'criteria': 'TextNotContain',
//												'criteria_text': 'ccc'
//											}
//										],
//										'criteria_relationship_arr': [
//											{
//												'condition': 'And'
//											}
//										]
//									},
//									'CategoryName' : [
//										{'RecordID' : 14},
//										{'RecordID' : 15},
//										{'RecordID' : 16},
//										{'RecordID' : 20},
//										{'RecordID' : 21}
//									],
//									'ELE' : [
//										{'RecordID' : 1},
//										{'RecordID' : 2},
//										{'RecordID' : 3},
//										{'RecordID' : 5},
//										{'RecordID' : 8}
//									]
//								},
//								'PAKP Outside School': {
//									...
//									...
//								}
//							};

		var objCriteriaData = {};
		$('div.filter_content_div').each( function () {
			var jsThisObjCriteriaData = {};
			var jsThisDataSource = $(this).attr('data_source');
			var jsThisDataSourceWithoutSpace = EditSectionPage._get_text_without_space(jsThisDataSource);
			
			$('tr.criteria_tr_' + jsThisDataSourceWithoutSpace).each( function () {
				var jsThisDataField = $(this).attr('data_field');
				var jsThisDataFieldSourceKey = jsThisDataSourceWithoutSpace + '###' + jsThisDataField;
				var jsThisDataFieldArr = new Array();
				
				if (jsThisDataFieldSourceKey == 'OLE###CategoryName' || jsThisDataFieldSourceKey == 'OLE###ELE' || jsThisDataFieldSourceKey == 'PAKP_Outside_School###CategoryName') 
				{
					// Get the corresponding Checkbox class for looping
					var jsThisChkClass;
					if (jsThisDataField == 'CategoryName') {
						jsThisChkClass = jsThisDataSourceWithoutSpace + '_CategoryChk';
					}
					else {
						jsThisChkClass = jsThisDataSourceWithoutSpace + '_ELEChk';
					}
					
					// Add the selected option's value in an Array
					$('input.' + jsThisChkClass + ':checked').each( function () {
						jsThisDataFieldArr[jsThisDataFieldArr.length] = {'RecordID': $(this).val()};
					});
				}
				else {
					var jsThisCriteriaArr = new Array();
					var jsThisCriteriaTextArr = new Array();
					var jsThisConditionArr = new Array();
					
					var jsThisCriteriaSelectionClass = 'criteria_sel_' + jsThisDataSourceWithoutSpace + '_' + jsThisDataField;
					$('select.' + jsThisCriteriaSelectionClass).each( function () {
						jsThisCriteriaArr[jsThisCriteriaArr.length] = $(this).val();
					});
					
					var jsThisCriteriaTextClass = 'criteria_tb_' + jsThisDataSourceWithoutSpace + '_' + jsThisDataField;
					$('input.' + jsThisCriteriaTextClass).each( function () {
						jsThisCriteriaTextArr[jsThisCriteriaTextArr.length] = addslashes(Trim($(this).val()));
					});
					
					var jsThisConditionSelectionClass = 'condition_sel_' + jsThisDataSourceWithoutSpace + '_' + jsThisDataField;
					$('select.' + jsThisConditionSelectionClass).each( function () {
						jsThisConditionArr[jsThisConditionArr.length] = $(this).val();
					});
					
					var i;
					var jsThisCriteriaInfoArr = {};
					var jsThisCriteriaRelationshipArr = {};
					var jsThisMaxOfCriteriaLevel = jsThisCriteriaArr.length;
					for (i=0; i<jsThisMaxOfCriteriaLevel; i++) {
						// jsThisCriteriaInfoArr[i] = {'criteria': 'TextContains', 'criteria_text': 'search string here'};
						eval("jsThisCriteriaInfoArr[" + i + "] = {	'criteria': '" + jsThisCriteriaArr[i] + "',	'criteria_text': '" + jsThisCriteriaTextArr[i] + "'	}; ");
						
						// Last Level should not have criteria condition
						if (i < jsThisMaxOfCriteriaLevel - 1) {
							// jsThisCriteriaRelationshipArr[i] = {'condition': 'and'};
							eval("jsThisCriteriaRelationshipArr[" + i + "] = { 'condition': '" + jsThisConditionArr[i] + "' }; ");
						}
					}
					
					jsThisDataFieldArr = $.extend(true, {}, {'criteria_info_arr': jsThisCriteriaInfoArr}, {'criteria_relationship_arr': jsThisCriteriaRelationshipArr});
				}
				
				// jsThisDataFieldArr = {'CategoryName': jsThisDataFieldArr};
				eval("jsThisDataFieldArr = {'" + jsThisDataField + "': jsThisDataFieldArr};");
				jsThisObjCriteriaData = $.extend(true, jsThisObjCriteriaData, jsThisDataFieldArr);
			});
			
			// jsThisObjCriteriaData = {'OLE': jsThisObjCriteriaData};
			eval("jsThisObjCriteriaData = {'" + jsThisDataSourceWithoutSpace + "': jsThisObjCriteriaData};");
			objCriteriaData = $.extend(true, objCriteriaData, jsThisObjCriteriaData);
		});
		
		return objCriteriaData;
	},
	
	update_criteria_div_display: function() {
		$('div.filter_div').hide();
		
		$('select.src_opt_sel').each( function() {
			var jsThisSelectedDataSource = $(this).val();
			if (jsThisSelectedDataSource == 'OLE' || jsThisSelectedDataSource=='PAKP Outside School') {
				var jsThisSelectedDataSourceWithoutSpace = EditSectionPage._get_text_without_space(jsThisSelectedDataSource);
				var jsThisSourceDivID = 'filter_div_' + jsThisSelectedDataSourceWithoutSpace;
				
				$('div#' + jsThisSourceDivID).show();
			}
		});
		
		
		var jsDisplayDivCount = 0;
		$('div.filter_div').each( function() {
			if ($(this).css('display') != 'none') {
				jsDisplayDivCount++;
			}
		});
		
		if (jsDisplayDivCount > 0) {
			$('div#no_criteria_settings_div').hide();
		}
		else if (jsDisplayDivCount == 0) {
			$('div#no_criteria_settings_div').show();
		}
	},
	
	changed_criteria_selection: function(data_source, data_field, criteria_level, selected_criteria) {
		var data_source_without_space = EditSectionPage._get_text_without_space(data_source);
		
		var jsTargetTb = 'criteria_tb_' + data_source_without_space + '_' + data_field + '_' + criteria_level;	
		var jsTargetConditionSel = 'condition_sel_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		var jsTargetConditionDiv = 'data_field_criteria_condition_div_' + data_source_without_space + '_' + data_field + '_' + criteria_level;
		
		if (selected_criteria == 'NoCriteria' || selected_criteria == 'MustHaveText') {
			$('input#' + jsTargetTb).val('').hide();		// reset textbox value
			$('select#' + jsTargetConditionSel).val('');	// reset condition selection
			$('div#' + jsTargetConditionDiv).hide();
			
			var jsTargetDivClass = 'data_field_criteria_main_div_' + data_source_without_space + '_' + data_field;
			var jsDivLevelCounter = 1;
			$('div.' + jsTargetDivClass).each( function() {
				EditSectionPage.delete_criteria_option_div(data_source, data_field, jsDivLevelCounter++);
			});
		}
		else {
			$('input#' + jsTargetTb).show().focus();
			$('div#' + jsTargetConditionDiv).show();
		}
	},
	
	changed_condition_selection: function(data_source, data_field, criteria_level, selected_condition) {
		var jsNextCriteriaLevel = parseInt(criteria_level + 1);
		
		if (selected_condition == '') {
			EditSectionPage.delete_criteria_option_div(data_source, data_field, jsNextCriteriaLevel);
		}
		else {
			EditSectionPage.add_criteria_option_div(data_source, data_field, jsNextCriteriaLevel, null);
		}
	},
	
	/**
	 *	Convert source from "PAKP Outside School" to "PAKP_Outside_School" for ElementID
	 *  @return the source key without space
	 */
	_get_text_without_space: function(data_source_name) {
		var jsWords = [' '];
		return data_source_name.replace(new RegExp(jsWords.join('|'), 'g'), '_');
	},
	
	/**
	 * Get the display name of a data source.
	 * @return the display name, or the data source name if the display name is not found.
	 */
	_get_data_source_display_name: function(data_source_name) {
		var display_name = this._data_source_and_field_display_names[data_source_name];
		return !display_name ? data_source_name : display_name;
	},
	
	/**
	 * Get the display name of a data source field.
	 * @return the display name, or the data source field name if the display name is not found.
	 */
	_get_data_source_field_display_name: function(data_source_name, field_name) {
		var display_name = this._data_source_and_field_display_names[data_source_name + '.' + field_name];
		return !display_name ? field_name : display_name;
	},
	
	changed_show_statistics_checkbox: function(rowNum) {
		var showStatistics = $('input#show_statistics_chk_' + rowNum).attr('checked');
		
		if (showStatistics) {
			$('select.statistics_option_sel_' + rowNum + ', input.statistics_option_tb_' + rowNum).attr('disabled', '');
		}
		else {
			$('select.statistics_option_sel_' + rowNum + ', input.statistics_option_tb_' + rowNum).attr('disabled', 'disabled');
		}
		
		var column_count = EditSectionPage.column_increment;
		var i;
		for (i=0; i<column_count; i++) {
			var _colNum = parseInt(i) + 1;
			
			$('select#statistics_option_sel_' + rowNum + '_' + _colNum).change();
		}
	},
	
	changedStatisticsSel: function(rowNum, colNum) {
		var statType = $('select#statistics_option_sel_' + rowNum + '_' + colNum).val();
		
		var textboxId = 'statistics_option_tb_' + rowNum + '_' + colNum;
		if (statType == 'text') {
			$('input#' + textboxId).attr('disabled', '').focus();
		}
		else {
			$('input#' + textboxId).attr('disabled', 'disabled');
			
			if (statType == '') {
				$('input#' + textboxId).val('');
			}
		}
	},
	
	get_statistics_type_selection: function(rowNum, colNum) {
		statSel = '';
		statSel += '<select id="statistics_option_sel_' + rowNum + '_' + colNum + '" name="statisticsAry[' + rowNum + '][statType][]" class="statistics_option_sel_' + rowNum + '" onchange="EditSectionPage.changedStatisticsSel(' + rowNum + ', ' + colNum + ');">';
			statSel += '<?=$h_statisticsOption?>';
		statSel += '</select>';
		
		return statSel;
	},
	
	get_statistics_text_input: function(rowNum, colNum, textValue) {
		return '<input id="statistics_option_tb_' + rowNum + '_' + colNum + '" name="statisticsAry[' + rowNum + '][statInputText][]" class="textboxtext statistics_option_tb_' + rowNum + '" value="' + textValue + '" />';
	}
};


$(document).ready(function() {	
	EditSectionPage.on_init();
});

</script>
<div class="edit_pop_board">
	<h1><span> <?= srt_lang('New Section') ?></span></h1>
	<div class="edit_pop_board_write">
		<?=$h_TabMenu?>
    	
      	<div id="tab_div_display" class="tab_div">
			<table class="form_table_v30" style="width:95%; margin:0 auto">
		      	<tr>
		        	<td width="150px"><?= srt_lang('Section Title') ?></td>
		        	<td>:</td>
		        	<!--td><input name="section_title_tmp" type="text" id="section_title_tmp" class="textboxtext"/></td-->
					<td>
						<textarea name="section_title" id="section_title" class="textboxtext" rows="2"></textarea>
					</td>
		        </tr>
		
		      	<col class="field_title" />
		      	<col class="field_c" />
	    	</table>
    	
			<!-- Henry Yuen (2010-06-30): Description -->
			<table class="form_table_v30" style="width:95%; margin:0 auto">
				<tr>
					<td width="150px"><?= srt_lang('Section Description') ?></td>
					<td>:</td>
					<!--<td ><input name="section_description" type="text" id="section_description" class="textboxtext"/></td>-->
					<td >
						<textarea name="section_description" id="section_description" class="textboxtext" rows="3"></textarea>
					</td>
				</tr>
				
				<col class="field_title" />
				<col  class="field_c" />
			</table>      
  
			<table class="common_table_list" style="width:95%" id="source_tb">
				<tr>
					<th colspan="2"><?= srt_lang('Data') ?></th>
					<th colspan="4" class="sub_row_top" id="top_col">
						<span class="row_content"><?= srt_lang('Column') ?></span>
						<div class="table_row_tool row_content_tool">
							<a href="javascript:EditSectionPage.add_extra_column('');" class="add" title="<?= srt_lang('Add Column') ?>"></a>
						</div>
					</th>             
				</tr>
				<tr id="data_table">
					<th style="width:150px"><?= srt_lang('Data') ?></th>
					<th><?= srt_lang('No. of Most Recent Record') ?></th>
					<th class="sub_row_top" id='col_title_empty'>&nbsp;</th>
					<!--
						<th class="sub_row_top"><span class="row_content"> 1)
							<input name="column_title1" type="text" class="textbox_date column_title" id="column_title1" value="11 Title here.."/>
							</span>
							<div class="table_row_tool row_content_tool"><span class="table_row_tool"><a href="#" class="delete_dim" title="Delete"></a></span></div>
						</th>
						<th class="sub_row_top"><span class="row_content"> 2)
							<input name="column_title2" type="text" class="textbox_date column_title" id="column_title2" value="22 Title here.."/>
							</span>
							<div class="table_row_tool row_content_tool"><span class="table_row_tool"><a href="#" class="delete_dim" title="Delete"></a></span></div></th>
					-->
				</tr>
   
				<tr id="add_new_source_tr">
					<td><span class="table_row_tool row_content_tool"><a href="javascript:EditSectionPage.add_extra_source();" class="add" title="<?= srt_lang('Add Data') ?>"></a></span></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr id="statistics_settings_tr_1" class="stat_tr">
					<td colspan="2">
						<input type="checkbox" id="show_statistics_chk_1" name="showStatisticsAry[1]" value="1" onclick="EditSectionPage.changed_show_statistics_checkbox(1);" />
						<label for="show_statistics_chk_1"><?=$Lang['iPortfolio']['DynamicReport']['StatisticsArr']['ShowStatistics']?></label>
					</td>
					<td id="statistics_settings_last_td_1">&nbsp;</td>
				</tr>

				<col />
				<col />
				<col />
				
				<tbody>
				</tbody>
  			</table>
  			
			<table class="form_table_v30" style="width:95%; margin:0 auto">
				<col class="field_title" />
				<col  class="field_c" />
				<tr>
					<td><?= srt_lang('Sort by') ?> 1</td>
					<td>:</td>
					<td>
						<div id="sort_by_column_td" style="float:left;padding:0 10px 0 0;">          
						</div>
						<select name="sort_by_asc" id="sort_by_asc">
							<option value="1"><?= srt_lang('Ascending') ?></option>
							<option value="0"><?= srt_lang('Descending') ?></option>
						</select>
					</td>
				</tr>
				<?php if ($sys_custom['iPf']['DynamicReport']['SortingWithTwoLevels']) { ?>
				<tr>
					<td><?= srt_lang('Sort by') ?> 2</td>
					<td>:</td>
					<td>
						<div id="sort_by_column_td2" style="float:left;padding:0 10px 0 0;">          
						</div>
						<select name="sort_by_asc2" id="sort_by_asc2">
							<option value="1"><?= srt_lang('Ascending') ?></option>
							<option value="0"><?= srt_lang('Descending') ?></option>
						</select>
					</td>
				</tr>
				<?php } ?>
			</table>
			<br />
		</div>	<!-- Close <div id="tab_div_display"> -->
		
		<div id="tab_div_filter" class="tab_div" style="display:none;" align="center">
			<?=$h_filter_div?>
		</div>	<!-- Close <div id="tab_div_filter"> -->
		
	</div> <!-- Close <div class="edit_pop_board_write"> -->
    
	<div class="edit_bottom">
		<p class="spacer"></p>
		<input name="submit2" type="button" class="formbutton" onclick="EditSectionPage.onSubmit();"
		 onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Submit') ?>" />
		<input name="submit2" type="button" class="formbutton" onclick="parent.tb_remove()" 
		onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?= srt_lang('Cancel') ?>" />
		<p class="spacer"></p>
	</div>
</div>