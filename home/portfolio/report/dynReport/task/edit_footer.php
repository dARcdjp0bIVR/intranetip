<?php
#using:

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportDataSourceManager.php");
include_once("$intranet_root/includes/portfolio25/dynReport/JSON.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						
## Get Params
$section_index = (isset($_REQUEST['section_index']))? trim($_REQUEST['section_index']) : "";

# Get Data source info
$objDataSrc = new StudentReportDataSourceManager();

$student_field_display_names = $objDataSrc->student_info->get_field_display_names();

$dirname = 'dirname';
$rawurlencode = 'rawurlencode';
$htmlspecialchars = 'htmlspecialchars';
$select = '<select name="genVariable" id="genVariable">';
foreach ($student_field_display_names as $field => $display_name) {
	$display_name = $student_field_display_names[$field];
	if (!$display_name) $display_name = $field;
	$image_tag = <<<HTMLEND
		<img src="{$dirname($_SERVER['PHP_SELF'])}/str2img.php?s={$rawurlencode($display_name)}&amp;field={$rawurlencode($field)}" />
HTMLEND;
	$select  .= <<<HTMLEND
		<option value="{$htmlspecialchars($image_tag)}">{$htmlspecialchars($display_name)}</option>
HTMLEND;
}
$select .= '</select>';

$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "320", "", "");
// Custom configuration of FCKeditor for dynamic report.
$objHtmlEditor->Config["CustomConfigurationsPath"] = dirname($_SERVER['PHP_SELF']) . "/js/myfckconfig.js";

# Begin Henry Yuen (2010-04-21): support attaching image in header
//$objHtmlEditor->ToolbarSet = 'StudentReportTemplate';
$lfilesystem = new libfilesystem();

$objHtmlEditor->ToolbarSet = 'StudentReportTemplate_withInsertImageFlash';
$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['DynamicReport'], '');
# Begin Henry Yuen (2010-04-21): support attaching image in header

$json = new Services_JSON();

// Include the template, which is relative to the index page.$linterface = new interface_html("popup4.html");
$linterface = new interface_html("popup5.html");
$linterface->LAYOUT_START();
include_once("template/edit_footer.tmpl.php");
$linterface->LAYOUT_STOP();

?>