<?php
#using: 

include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplateManager.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$template_id = $_REQUEST['template_id'];
if ($template_id) {
	// Delete a template.
	$template_manager = new StudentReportTemplateManager();
	$result = $template_manager->delete($template_id);
	$xmsg = $result ? 'delete' : 'delete_failed';
	
	# Begin Henry Yuen (2010-04-23): remove header image 
	if($result){ 
		$lf = new libfilesystem();
		$currentFolder = $lf->returnRelativePathFlashImageInsertPath($cfg['fck_image']['DynamicReport'], $template_id);
		$lf->deleteDirectory($intranet_root."/".$currentFolder);
	}		
	# End Henry Yuen (2010-04-23): remove header image 
}
else {
	$xmsg = 'delete_failed';
}

header("Location: index.php?task=list&xmsg=$xmsg");

?>
