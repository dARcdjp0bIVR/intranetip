<?php
#Using: 
#Modifications:
#	Omas	(2015-11-04): add teacher comments setting 
#	Omas	(2015-04-15): add _handleCurrentAlumniStudentBeforePrinting(),_handleCurrentAlumniStudentAfterPrinting() for catering alumni(or archived student)
#	Carlos	(2013-03-21): add release to student dynamic report record
# 	Ivan	(2012-02-17): add show / hide full mark display logic
#	Henry Yuen (2010-07-30): support printing doc format

// This script will call the following callbacks in the parent page to provide progress information.
// - on_progress_update: A percentage value will be passed to this function for showing the progress.
// - on_download_ready: After finished printing, a ticket value will be passed to this function, which may then use the task print_by_ticket to get the report.
@ini_set('memory_limit', -1);
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportPrintManager.php");

//Omas
$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

$targetType = strtolower($_REQUEST['trgType']);
$template_id = $_REQUEST['template_id'];
$targetClass = $_REQUEST['targetClass'];
$batchPrintMode = $_REQUEST['batchPrintMode'];
if($targetType == 'current'){
	$targetStudent = $_REQUEST['targetStudent'];
}
else if($targetType == 'alumni'){
	$targetStudent = $_REQUEST['alumni_StudentID'];
	$StudentArr = _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr);
}

$numOfStudent = count($targetStudent);

$print_criteria = array(
	'IssueDate' => $_REQUEST['IssueDate'],
	'AcademicYearIDs' => $_REQUEST['AcademicYearIDs'],
	'YearTermIDs' => $_REQUEST['YearTermIDs'],
	'PrintSelRec' => ($_REQUEST['slpPrintRec'] == 'sel'),
	'EmptyDataSymbol' => trim(stripslashes($_REQUEST['EmptyDataSymbol'])),
	'ShowNoDataTable' => $_REQUEST['ShowNoDataTable'],
	'NoDataTableContent' => trim(stripslashes($_REQUEST['NoDataTableContent'])),
	'PrintingDirection' => $_REQUEST['PrintingDirection'],
	'DisplayFullMark' => ($_REQUEST['DisplayFullMark'])? true : false,
	'DisplayOLEOrderOptions' => $_REQUEST['rdDisplayOLEOrderOptions'],
	'ReleaseToStudent' => ($_REQUEST['ReleaseToStudent'])? true : false,
	'ReleaseStatus' => ($_REQUEST['ReleaseStatus'])? true : false,
	'commentsAY'=> $_REQUEST['commentsAY'],
	'commentsSem'=> $_REQUEST['commentsSem']
);

set_time_limit(0);
ob_implicit_flush();
ob_end_flush();

#Henry Yuen (2010-07-30)
//$format = 'pdf';
$format = $_POST['printFormat'];
if ($format == '') {
	$format = $ipf_cfg['dynReport']['batchPrintMode']['pdf'];
}

$print_manager = new StudentReportPrintManager();
$lpf = new libportfolio();
// process release report record to students
$lpf->insertUpdateReleasedStudentDynamicReport($_REQUEST);

//if ($targetStudent && $targetStudent > 0) {
if ($numOfStudent == 1) {
	// Print report for a student.
	//$ticket = $print_manager->print_report_by_list_of_students(null, "pdf", $template_id, array($targetStudent), $print_criteria, 'print_progress_callback');
	//$ticket = $print_manager->print_report_by_list_of_students(null, $format, $template_id, array($targetStudent), $print_criteria, 'print_progress_callback');
	
	$targetStudent = $targetStudent[0];
	
	$fileName = null;
	$objUser = new libuser($targetStudent);
	if($targetType != 'alumni'){
//	if ($sys_custom['iPf']['DynamicReport']['Class&ClassNoAsFileNameForSingleStudentReport']) {
		$fileName = $objUser->ClassName.'_'.$objUser->ClassNumber.'.'.$format;
//	}
	}
	else{	
		$fileName = $objUser->StandardDisplayName.'.'.$format;
	}
	$ticket = $print_manager->print_report_by_list_of_students($fileName, $format, $template_id, array($targetStudent), $print_criteria, 'print_progress_callback');
}
else {
	// Print report for a class.
	//$ticket = $print_manager->print_report_by_class_name(null, "pdf", $template_id, $targetClass, $print_criteria, 'print_progress_callback');
	if($targetType != 'alumni'){
		$ticket = $print_manager->print_report_by_class_name(null, $format, $template_id, $targetClass, $print_criteria, 'print_progress_callback', $batchPrintMode, $targetStudent);
	}
	else{
		// alumni here
		$student_id_list = $targetStudent;
		$ticket = $print_manager->print_report_by_alumni_list(null, $format, $template_id, $print_criteria, 'print_progress_callback', $batchPrintMode, $targetStudent);
		//$ticket = $print_manager->print_report_by_alumni_list(null, $format, $template_id, $student_id_list, $print_criteria, 'print_progress_callback', null);
	}
}

function print_progress_callback($progress) {
	$percentage = floor($progress * 100);
	echo <<<HTMLEND
		<script>if (parent.on_progress_update) parent.on_progress_update($percentage);</script>
HTMLEND;
}

if($targetType == 'alumni'){
	_handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr);
}
?>
<script>if (parent.on_download_ready) parent.on_download_ready('<?= $ticket ?>');</script>
<?
exit;
?>