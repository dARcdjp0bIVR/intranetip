<?php
// editing by 
/*
 * 2013-03-22 (Carlos) : Get generate_settings from DB record and go through each record to regenerate report
 */
if(!isset($lpf)) {
	$lpf = new libportfolio2007();
}

$RecordIdAry = $_REQUEST['RecordID'];
$RecordIdAry = (array)$RecordIdAry;
if(count($RecordIdAry) == 0){
	exit;
}

$settingsAry = $lpf->getReleaseStudentDynamicReportSettings($RecordIdAry);
$setting_count = count($settingsAry);

/*
$postAry = array();
$postAry['task'] = 'print_with_progress';
$postAry['template_id'] = '87';
$postAry['targetClass'] = 'F9A';
$postAry['targetStudent'] = array(1971, 1980);
$postAry['AcademicYearIDs'] = array(12, 14, 9, 3, 8, 2, 1, 13);
$postAry['PrintingDirection'] = 'P';
$postAry['batchPrintMode'] = 'zip';
$postAry['IssueDate'] = '2013-03-19';
$postAry['EmptyDataSymbol'] = '';
$postAry['ShowNoDataTable'] = '1';
$postAry['NoDataTableContent'] = '未有紀錄。';
$postAry['slpPrintRec'] = 'all';
$postAry['rdDisplayOLEOrderOptions'] = 'studentSelected';
$postAry['DisplayFullMark'] = '1';
$postAry['printFormat'] = 'pdf';
$postAry['ReleaseToStudent'] = '1';
$postAry['ReleaseStatus'] = '0';
*/

for($i=0;$i<$setting_count;$i++) {
	
	$postAry = $settingsAry[$i];
	$postAry['task'] = 'print_with_progress';
	
	// convert from array to parameter text
	$postFields = http_build_query($postAry);
	
	
	// cookies handling
	$cookieFields = '';
	foreach ((array)$_COOKIE as $_key => $_value) {
		$cookieFields .= $_key.'='.$_value.';';
	}
	$cookieFields .= '; path=/';
	
	
	// post values
	$url = curPageURL($withQueryString=0, $withPageSuffix=1);
	session_write_close();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_COOKIE, $cookieFields);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
	$html = curl_exec($ch);
	curl_close($ch);
	
	//echo $html;

}
?>