<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."home/portfolio/profile/report/report_lib/slp_report.php");

include_once("$intranet_root/includes/portfolio25/dynReport/srt_lang.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReport_ui.php");
include_once("$intranet_root/includes/portfolio25/dynReport/StudentReportTemplate.php");
include_once("$intranet_root/includes/portfolio25/libportfolio-ui.php");
include_once("$intranet_root/includes/portfolio25/libpf-academic.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");
include_once("$intranet_root/includes/portfolio25/slp/libpf-ole.php");



// Is this feature enabled?
//if (!$plugin["ipf_dynReport"]) {
//	// Redirect to iPortfolio.
//	header("Location: $intranet_httppath/home/portfolio/index.php");
//	exit();
//}

intranet_auth();
intranet_opendb();
$task = trim($task);
$task = ($task == "") ? "report": $task;  //USER REQUEST ACTION
$templateID = intval(trim($templateID));
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_dynReport";
$CurrentPageName = $iPort['menu']['dynReport'];

$luser = new libuser($UserID);
$lpf = new libportfolio2007();

//debug_r($_SESSION);



$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("print_report");

//$lpf->ADMIN_ACCESS_PAGE();
### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

switch($task)
{
	case 'index': //List out all template
		// Depreciated - cannot be replaced by the default logic.
		include_once("task/index.php");
		break;
		
	case 'save_t':  //SAVE the template
		// Depreciated - cannot be replaced by the default logic.
		saveTemplate($templateID);
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		$task_script = "task/" . str_replace("../", "", $task) . ".php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
}

intranet_closedb();
exit();

// Only stripslashes if the magic quotes is on.
function stripslashes_deep_if_magic_quotes_is_on($value)
{
	// lib.php will do magic quotes work even if magic quote is off - #B96754 2016-06-03 
	if (true || get_magic_quotes_gpc()) {
		$value = is_array($value) ?
			array_map('stripslashes_deep_if_magic_quotes_is_on', $value) :
			stripslashes($value);
	}
	return $value;
}

// Call htmlspecialchars on all values in an array.
function htmlspecialchars_deep($value)
{
	return is_array($value) ?
		array_map('htmlspecialchars_deep', $value) :
		(isset($value) ? htmlspecialchars($value) : null);
}

//******************//
//** function list *//
//******************//
function saveTemplate()
{
//	echo "<script language=\"JavaScript\">";
//    echo 'self.location = "index.php?msg=3';
//    echo '</script>';
}
