<?php

// Modifing by Pun

$PATH_WRT_ROOT = "../../../../";

######### Include START #########
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
######### Include END #########


######### Access Right START #########
iportfolio_auth("T");
intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
######### Access Right END #########


######### Init START #########
$linterface = new interface_html();
######### Init END #########

$lreportcard = null;
$reportHTML = '';
$_printYear = '';
for($i=0, $iMax=count($reportID); $i<$iMax; $i++){
	if($_printYear != $reportYear[$i]){
		$lreportcard = new libreportcard();
		
		$lreportcard->schoolYear = $reportYear[$i];
		$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
		$lreportcard->ArchiveCSSPath = $lreportcard->Get_Archive_Report_Css_Path();
	}
	$_report = $lreportcard->Get_Archived_Report($reportID[$i], $studentID);
	$reportHTML .= '<div style="page-break-after:always;">';
	$reportHTML .= '<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">';
	$reportHTML .= $_report[0];
	$reportHTML .= '</table>';
	$reportHTML .= '</div>';
//	$reportHTML .= '<tr><td><br style="page-break-after:always;" /></td></tr>';
}



$ArchiveFolderPath = $lreportcard->ArchiveCSSPath;
$TemplateCSS_Filename = $eRCTemplateSetting['CSS'];
$TemplateCSS_Path = $ArchiveFolderPath.$TemplateCSS_Filename;

$PrintCSS_Path = $ArchiveFolderPath."print.css";
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header_erc.php");
?>
<link href="<?=$PrintCSS_Path?>" rel="stylesheet" type="text/css" />
<link href="<?=$TemplateCSS_Path?>" rel="stylesheet" type="text/css" />

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

	<?=$reportHTML?>
