<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

update_login_session();

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-alumni.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");
$li_pf->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);

//$li_pf->accessControl("merit");

$miscParameter = explode("&", $miscParameter);
for($i=0; $i<count($miscParameter); $i++)
{
	$temp = explode("=", $miscParameter[$i]);
	${$temp[0]} = $temp[1]; 
}

# Generate student selection drop-down list and get a student list
list($student_selection_html, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID, true, 1, "name='StudentID' onChange='document.form1.submit()'", $Year);

# Set default student ID, to cater change of class
# If not doing so, it will load data of student in previous page
for($i=0, $i_max=count($student_list); $i<$i_max; $i++)
{
  $_student_id = $student_list[$i]["UserID"];
  if($_student_id == $StudentID)
  {
    $default_student_id = $_student_id;
    break;
  }
}
if(empty($default_student_id))
{
  $default_student_id = $student_list[0]["UserID"];
}
$lpf_acc = new libpf_account_alumni(); 
$lpf_acc->SET_CLASS_VARIABLE("user_id", $default_student_id);
$lpf_acc->SET_STUDENT_PROPERTY();
$lpf_acc->SET_PARENT();
$student_name = $lpf_acc->GET_CLASS_VARIABLE(Get_Lang_Selection("chinese_name", "english_name"));
$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['year_list'], "school_records_alumni.php");
$MenuArr[] = array($Year, "school_records_alumni_year.php?Year={$Year}");
$MenuArr[] = array($student_name, "");
////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Year", "a.Semester", "a.AttendanceDate", "a.RecordType", "a.DayType", "a.Reason", "a.ModifiedDate");

# handle semester different problem
list($SemesterArr, $ShortSemesterArr) = $li_pf->getSemesterNameArrFromIP();
list($SemList, $List1, $List2, $List3) = $li_pf->getSemesterMatchingLists($semester, $SemesterArr);

$counting = $li_pf->getAttendanceCountingByYearSemester($StudentID, $year, $semester);
$absentCount = ($counting[1]=="") ? '0' : $counting[1];
$lateCount = ($counting[2]=="") ? '0' : $counting[2];
$earlyleaveCount = ($counting[3]=="") ? '0' : $counting[3];

if($semester!="" && $semester!=$ec_iPortfolio['whole_year'] && $SemList != "")
{	
	$conds = "AND (a.Semester = '$semester' OR a.Semester IN ({$SemList}))";
}

$sql =	"
					SELECT
						a.Year,
						if(INSTR('{$List1}', a.Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}', a.Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}', a.Semester),'".$SemesterArr[2]."',a.Semester))),
						DATE_FORMAT(a.AttendanceDate,'%Y-%m-%d'),
						CASE a.RecordType
							WHEN 1 THEN '$i_Profile_Absent'
							WHEN 2 THEN '$i_Profile_Late'
							WHEN 3 THEN '$i_Profile_EarlyLeave'
							ELSE '-'
						END,
						CASE a.DayType
							WHEN 1 THEN '$i_DayTypeWholeDay'
							WHEN 2 THEN '$i_DayTypeAM'
							WHEN 3 THEN '$i_DayTypePM'
						ELSE '-' END,
						a.Reason,
						a.ModifiedDate
					FROM
						{$eclass_db}.ATTENDANCE_STUDENT as a
					WHERE
						a.UserID = '$StudentID' AND
						a.Year = '$year'
						$conds
				";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $usertype_s;
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->page_size = 999;
$li->no_col = 8;
//$li->noNumber = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['semester'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(3, $ec_iPortfolio['date'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(4, $ec_iPortfolio['stype'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column(5, $ec_iPortfolio['period'], 1)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(6, $ec_iPortfolio['reason'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(7, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
$li->column_array = array(0,0,0,0,0,0,0);

//////////////////////////////////////////////

# define the page title and table size

//$template_table_top_right = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

$linterface = new interface_html();
$CurrentPage = "Teacher_alumniReport";
$CurrentPageName = $iPort['menu']['alumniReport'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['alumniReport'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# Alumni Year Selection
$AlumniYearSelection = $li_pf->GEN_ALUMNI_YEAR_SELECTION($Year);

# Generate class selection drop-down list
$ClassSelection = $li_pf->GEN_ALUMNI_CLASS_SELECTION($Year, $ClassName);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID, true, 1, "name='StudentID' onChange='document.form1.submit()'", $Year);

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>


<script language="JavaScript" type="text/JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	if(jParField == 'classname' && document.form1.ClassName.value == '')
		return;
	
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.FieldChanged.value = "search";
	document.form1.action = "school_records_alumni_year.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined")
		document.form1.ChooseYear.value = "";
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}
</script>

<FORM name="form1" method="POST" action="school_record_teacher_alumni.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr style="display:none">
						<td>
							<?=$AlumniYearSelection?> <?=$ClassSelection?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<!--td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="school_records_alumni.php"><?=$ec_iPortfolio['year_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="school_records_alumni_year.php?Year=<?=$Year?>"><?=$ClassName==""?$i_general_WholeSchool:$ClassName?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=($intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName'])?>
									</td-->
									<td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left" style="display:none">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$StudentSelection?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_list), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
												<td align="right">
													<table border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td nowrap background="#">&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="#" onClick="jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
															<td valign="top">
																<a href="#" onClick="jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($StudentID) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="5" cellpadding="0"  width="100%">
														<tr>
															<td class="tab_underline" nowrap>
																<div class="shadetabs">
																	<!--ul>
																		<?=$li_pf->HAS_RIGHT("merit") ? ($RecordType=="merit" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_merit']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('merit')\">".$ec_iPortfolio['title_merit']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("assessment_report") ? ($RecordType=="assessment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_academic_report']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('assessment')\">".$ec_iPortfolio['title_academic_report']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("activity") ? ($RecordType=="activity" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_activity']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('activity')\">".$ec_iPortfolio['title_activity']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("award") ? ($RecordType=="award" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_award']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('award')\">".$ec_iPortfolio['title_award']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("teacher_comment") ? ($RecordType=="comment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_teacher_comments']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('comment')\">".$ec_iPortfolio['title_teacher_comments']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("attendance") ? ($RecordType=="attendance" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_attendance']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('attendance')\">".$ec_iPortfolio['title_attendance']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("service") ? ($RecordType=="service" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['service']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('service')\">".$ec_iPortfolio['service']."</a></li>") : ""?>
																	</ul-->
																	<ul>
																		<?=$li_pf->HAS_RIGHT("merit") ? ($RecordType=="merit" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_merit']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('merit')\">".$ec_iPortfolio['title_merit']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("assessment_report") ? ($RecordType=="assessment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_academic_report']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('assessment')\">".$ec_iPortfolio['title_academic_report']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("activity") ? ($RecordType=="activity" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_activity']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('activity')\">".$ec_iPortfolio['title_activity']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("award") ? ($RecordType=="award" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_award']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('award')\">".$ec_iPortfolio['title_award']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("teacher_comment") ? ($RecordType=="comment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_teacher_comments']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('comment')\">".$ec_iPortfolio['title_teacher_comments']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("attendance") ? ($RecordType=="attendance" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_attendance']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('attendance')\">".$ec_iPortfolio['title_attendance']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("service") ? ($RecordType=="service" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['service']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('service')\">".$ec_iPortfolio['service']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("ole") ? ($RecordType=="ole" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['ole']."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('ole')\">".$ec_iPortfolio['ole']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("ole") ? ($RecordType=="plkp" ? "<li class='selected'><a href='#'><strong>".$iPort["external_ole_report"]["performance"]."</strong></a></li>":"<li><a href=\"javascript:jCHANGE_RECORD_TYPE('plkp')\">".$iPort["external_ole_report"]["performance"]."</a></li>") : ""?>
																	</ul>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="#" onClick="jCHANGE_RECORD_TYPE('attendance')"><?=$ec_iPortfolio['overall_summary']?></a>
													<img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['attendance_detail']?></span>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table cellpadding="8" cellspacing="0" border="0">
														<tr>
															<td class='tabletext' nowrap><?=$ec_iPortfolio['total_absent_count']?> : <b><?=$absentCount?></b>&nbsp;</td>
															<td class='tabletext' nowrap><?=$ec_iPortfolio['total_late_count']?> : <b><?=$lateCount?></b>&nbsp;</td>
															<td class='tabletext' nowrap><?=$ec_iPortfolio['total_earlyleave_count']?> : <b><?=$earlyleaveCount?></b>&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">
													<?= $li->displayPlain() ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


<input type="hidden" name="RecordType" value=<?=$RecordType?> />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="year" value="<?=$year?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name="page_size_change">
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
//closedb();
$li_pf->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
