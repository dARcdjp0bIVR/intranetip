<?php

// Modifing by Pun

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
intranet_auth();
intranet_opendb();

$lpf = new libpf_sturec();
$lpf->ACCESS_CONTROL("student_info");

$has_sr_access_right = $lpf->HAS_SCHOOL_RECORD_VIEW_RIGHT() ? 1 : 0;
$has_lp_access_right = (strstr($ck_user_rights, ":web:") && $lpf->HAS_RIGHT("learning_sharing")) ? 1 : 0;
$has_sbs_access_right = (strstr($ck_user_rights, ":growth:") && $lpf->HAS_RIGHT("growth_scheme")) ? 1 : 0;
$cond = "";
if(!empty($ClassName)){
  $cond .=  ($ClassName==-1)?" AND (iau.ClassName is NULL OR iau.ClassName = '')":" AND iau.ClassName = '".$ClassName."'";	
}
# Define display style
if(!isset($DisplayType) || $DisplayType == "" ) $DisplayType = "list";

if($search_name == $ec_iPortfolio['enter_student_name'])
{
	$search_name = "";
}
else
{
  $cond .=  " AND (iau.ChineseName LIKE '%".$search_name."%' OR iau.EnglishName LIKE '%".$search_name."%')";
}
	
if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$img_activated = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\">";
$img_pi = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_sr = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['heading']['student_record']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_lp = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_sbs = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_sbs.gif\" alt=\"".$iPort['menu']['school_based_scheme']."\" width=\"20\" height=\"20\" border=\"0\">";

$cond .= ($YearClassID == "") ? "" : " AND ycu.YearClassID = ".$YearClassID;
$cond .= ($Year == "") ? "" : " AND iau.YearOfLeft = '".$Year."'";

$sql =  "
          SELECT DISTINCT
            iau.YearOfLeft,
            iau.ClassName,
            iau.ClassNumber,
            '<!--StudentPhoto-->',
             CONCAT('<a href=\"javascript:jTO_INFO(', iau.UserID,')\">',".getNameFieldByLang2("iau.").",'</a>') AS UserName,
			iau.UserID,
            IF(ps.RecordID IS NULL, 0, 1) AS Activated,
            IF(ps.RecordID IS NOT NULL AND ps.IsSuspend = 1, 1, 0) AS Suspended

          FROM
            {$intranet_db}.INTRANET_ARCHIVE_USER AS iau
          INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
            ON  iau.UserID = ps.UserID
          LEFT JOIN ".$lpf->course_db.".user_config AS uc
            ON  ps.CourseUserID = uc.user_id AND
                uc.notes_published IS NOT NULL
          LEFT JOIN ".$lpf->course_db.".portfolio_tracking_last AS ptl
            ON  uc.user_id = ptl.student_id AND
                uc.web_portfolio_id = ptl.web_portfolio_id AND
                ptl.teacher_id = {$ck_user_id}
          WHERE
            1
            $cond
          GROUP BY
            iau.UserID
        ";

$LibTable->sql = $sql;
$LibTable->field_array = array("YearOfLeft", "ClassName", "ClassNumber", "UserName");
$LibTable->fieldorder2 = ", ClassNumber ASC";
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $LibTable->page_size = $numPerPage;
$LibTable->no_col = 5;//$LibTable->no_col = 9;
$LibTable->noNumber = true;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 0, 0, 0, 0, 3, 3, 3, 3);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td nowrap='nowrap' >".$LibTable->column(0,$ec_iPortfolio['year'])."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' >".$LibTable->column(1,$i_UserClassName)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap'>".$LibTable->column(2,$i_UserClassNumber)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap'>&nbsp;</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap'>".$LibTable->column(3,$ec_iPortfolio['heading']['student_name'])."</td>\n";
//$LibTable->column_list .= "<td nowrap='nowrap'>".$LibTable->column(4,$ec_iPortfolio['heading']['student_regno'])."</td>\n";
//$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['student_info']."</td>\n";
//$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['student_record']."</td>\n";
//$LibTable->column_list .= $has_lp_access_right ? "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['learning_portfolio']."</td>\n" : "<td>&nbsp;</td>";
//$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['report']."</td>\n";
// $LibTable->column_list .= $has_sbs_access_right ? "<td nowrap='nowrap' align='center'>".$iPort['menu']['school_based_scheme']."</td>\n" : "<td>&nbsp;</td>";
$LibTable->column_list .= "</tr>\n";	


//$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
//for($i=0; $i<count($t_classlevel_arr); $i++)
//{
//  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
//  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];
//
//  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
//  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
//  
//  for($j=0; $j<count($t_class_arr); $j++)
//  {
//    $t_yc_id = $t_class_arr[$j][0];
//    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
//    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
//    {
//      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
//    }
//    
//    if($t_yc_id == $YearClassID)
//    {
//      $class_name = $t_yc_title;
//    }
//  }
//}
# Alumni Year Selection
$AlumniYearSelection = $lpf->GEN_ALUMNI_YEAR_SELECTION($Year);

# Generate class selection drop-down list
$ClassSelection = $lpf->GEN_ALUMNI_CLASS_SELECTION($Year, $ClassName);

// Generate class selection with optgroup
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$lpf_ui = new libportfolio_ui();

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['year_list'], "school_records_alumni.php");
$MenuArr[] = array($Year, "");

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_alumniReport";
$CurrentPageName = $iPort['menu']['alumniReport'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['alumniReport'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
/*
	if(jParField == 'classlevel')
	{
		jCHANGE_CLASSLIST();
	}
	else if(jParField == 'thumbnail' || jParField == 'list')
	{
		document.form1.DisplayType.value = jParField;
		document.form1.action = "../../school_records_class.php";
		document.form1.submit();
	}
	else
	{
		document.form1.FieldChanged.value = jParField;
		document.form1.action = "../../school_records_class.php";
		document.form1.submit();
	}
*/
  if(jParField == 'thumbnail' || jParField == 'list')
	{
		document.form1.DisplayType.value = jParField;
	}

  if(jParField == 'year'){
  	document.form1.ClassName.value = '';
  }
  document.form1.action = "school_records_alumni_year.php";
  document.form1.submit();
}

// Change page to display student detail information
function jTO_INFO(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "student_info_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "learning_portfolio_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SR(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "school_record_teacher_alumni.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "../../profile/sbs/index.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "school_records_alumni_year.php";
	document.form1.submit();
}

/*
functions for showing / hiding photos
1. getMouseXY
2. jSHOW_PHOTO
3. jHIDE_PHOTO
*/
function getMouseXY(e) {
  var IE = document.all?true:false

  if (IE) { // grab the x-y pos.s if browser is IE
    tempX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    tempY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop - 10;
  } else {  // grab the x-y pos.s if browser is NS
    tempX = e.pageX
    tempY = e.pageY
  }  
  // catch possible negative values in NS4
  if (tempX < 0){tempX = 0}
  if (tempY < 0){tempY = 0}  
  // show the position values in the form named Show
  // in the text fields named MouseX and MouseY
  
  pos = new Array(tempX,tempY);
  return pos;
}

function jSHOW_PHOTO(e, jParPhotoURL){

	jHIDE_PHOTO();
	
	// Move Layer
	if (!e) var e = window.event;
	mousePos = getMouseXY(e);
	xValue = mousePos[0]; 
	yValue = mousePos[1];
	
	var PhotoLayer = document.getElementById("PhotoLayer"); 
	
	PhotoLayer.style.top = ( yValue + 10 ) + "px"; 
	PhotoLayer.style.left = ( xValue + 10 ) + "px";    

  PhotoLayer.innerHTML = "<img src=\""+jParPhotoURL+"\" width=\"100\" height=\"130\" class=\"photo_border\" />";
	PhotoLayer.style.visibility = 'visible';
}

function jHIDE_PHOTO(){
	if ( document.getElementById("PhotoLayer").style.visibility == 'visible') 
	{ 
		document.getElementById("PhotoLayer").style.visibility = 'hidden'; 
	}
}
</script>

<form method='POST' name='form1' action='school_records_alumni_year.php'>
  <table width="100%" border="0" cellpadding="0" cellspacing="10">
  	<tr>
  		<td>
  			<table width="100%" border="0" cellspacing="0" cellpadding="0">
  				<tr>
  				  <td><?=$AlumniYearSelection?> <?=$ClassSelection?></td>
  					<td align="right" valign="bottom" class="thumb_list">
  						<span class="tabletext">
  							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
  							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
  						</span>
  					</td>
  				</tr>
  			</table>
  			<table width="100%" border="0" cellspacing="0" cellpadding="3">
  				<tr>
  					<td>
  						<table width="100%" border="0" cellspacing="0" cellpadding="5">
  							<tr>
                  <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
  							</tr>
  						</table>
  					</td>
  					<td align="right" valign="bottom" class="thumb_list">
<?php
  switch($DisplayType) {
    case "list":
      echo "<a href=\"javascript:jCHANGE_FIELD('thumbnail')\">".$ec_iPortfolio['thumbnail']."</a>";
      echo " | ";
      echo "<span>".$ec_iPortfolio['list']."</span>";
      break;
    case "thumbnail":
      echo "<span>".$ec_iPortfolio['thumbnail']."</span>";
      echo " | ";
      echo "<a href=\"javascript:jCHANGE_FIELD('list')\">".$ec_iPortfolio['list']."</a>";
      break;
  }
?>
            </td>
  				</tr>
  			</table>
  			<table width="100%" border="0" cellspacing="0" cellpadding="0">
  			<!--
  				<tr>
<?php if($DisplayType == "thumbnail") { ?>
  					<td class="student_list_top student_list_top_thumb tabletext">
  						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" width="20" height="20" align="absmiddle"> <?=$StatusStr?>
<?php } else { ?>
  					<td align="right" class="tabletext">
  						<table width="100%" border="0" cellspacing="0" cellpadding="0">
  							<tr>
  								<td class="tabletextremark" nowrap>
  									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" width="20" height="20" align="absmiddle"> <?=$StatusStr?>
  								</td>
  							</tr>
  						</table>
<?php } ?>
    				</td>
    			</tr>
    			-->
    			<tr>
    				<td class="student_thumb_content">
  						<?=($DisplayType == "list" ? $LibTable->displayStudentList_ListView() : $LibTable->displayStudentList_ThumbnailView())?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($LibTable->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
  					</td>
  				</tr>
  			</table>
  		</td>
  	</tr>
  </table>
  <div id='PhotoLayer' style='position:absolute;height:130px;width:100px;visibility:hidden;'></div>

  <input type="hidden" name="DisplayType" value="<?=$DisplayType?>" />
  <input type="hidden" name="StudentID" />
  
  <input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
  <input type="hidden" name="order" value="<?= $order ?>">
  <input type="hidden" name="field" value="<?= $field ?>">
  <input type="hidden" name="page_size_change" />
  <input type="hidden" name="numPerPage" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>