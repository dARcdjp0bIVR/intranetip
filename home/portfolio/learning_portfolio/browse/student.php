<?php
/**
 * Change Log:
 * 2018-01-23 Pun [134389] [ip.2.5.9.3.1]
 *  - Fix cannot load content if site is https
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - Update $limit to loading $session_expiry_time
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TS");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();

//apply session login on 16 Nov 2012//
$li = new libdb();
$limit = $session_expiry_time * 60;
$sql = "SELECT UserEmail, UserID FROM INTRANET_USER WHERE SessionKey = '$eclasskey' AND UNIX_TIMESTAMP(SessionLastUpdated) + $limit > UNIX_TIMESTAMP(now())";
$rs = $li->returnArray($sql,1);
if ($rs[0][0] != "")
{
        $flag = 1;
} else
{
        $flag = 0;
        header("Location: $PATH_WRT_ROOT/templates/");
}
//---------------------------------//

list($WebPortfolioID, $StudentID) = $li_pf->GET_STUDENT_DECRYPTED_STRING($key);

//$student_url = $li_pf->GET_STUDENT_PORTFOLIO_PATH($WebPortfolioID, $StudentID);

//pass language setting from IP25
$lang_choice='';
switch($intranet_session_language) {
	case 'b5' : $lang_choice = 'chib5'; break;
	case 'en' : $lang_choice = 'eng'; break;
	case 'gb' : $lang_choice = 'chigb'; break;
	default : $lang_choice = 'eng'; break;
}
$portcol = (checkHttpsWebProtocol())? 'https' : 'http';
$student_url = "{$portcol}://{$eclass_httppath}/src/ip2portfolio/contents_student/contentframe.php?web_portfolio_id=".$WebPortfolioID."&student_user_id=".$li_pf->IP_USER_ID_TO_EC_USER_ID($StudentID)."&request_lang=".$lang_choice;
//$student_key = $li_pf->GET_STUDENT_CRYPTED_STRING($WebPortfolioID, $StudentID);

$lp = $li_pf->GET_LEARNING_PORTFOLIO($WebPortfolioID, $StudentID);
$last_update = base64_encode($lp['notes_published']);

//Update TEACHE HAS VIEW THIS PORTFOLIO
$li_pf->updateTeacherViewTime($StudentID,$WebPortfolioID,$UserID);




/*
if ($StudentID=="")
{
	$student_user_id = (($ck_memberType=="T" || $ck_memberType=="P") && $ck_student_user_id!="") ? $ck_student_user_id : $ck_user_id;
	$student_obj = $li_pf->getStudentID($student_user_id);
	$StudentID = $student_obj['UserID'];
	$ClassName = $student_obj['ClassName'];
}

$li_pf->accessControl("learning_sharing");

if ($student && isset($student) && $key=="")
{
	$user_id = $li_pf->getCourseUserID($StudentID);
	$course_id = $li_pf->getCourseID();
	$web_portfolio_id = ($selected_wid=="") ? $li->getStudentLatestWebPortfolioID($user_id) : $selected_wid;
	$key = $li->returnStudentKey($web_portfolio_id, $course_id, $user_id);

} else
{
	list($web_portfolio_id, $course_id, $user_id) = $li->getStudentKeyDecrypted($key);
}

//$web_portfolio_id = ($selected_wid=="") ? $web_portfolio_id : $selected_wid;
$li->web_portfolio_id = $web_portfolio_id;
$student_url = $li->getStudentURL($web_portfolio_id, $user_id, $course_id);
$last_update = base64_encode($li->getStudentLastUpdateTime($web_portfolio_id, $user_id, $course_id));

if ($student_url==null)
{
	head(11);
	
	if ($ck_memberType=="S" || $StudentRole==1)
	{
		// load settings
		$learning_portfolio_config_file = "$eclass_root/files/learning_portfolio_config.txt";
		$filecontent = trim(get_file_content($learning_portfolio_config_file));
		list($status, $review_type, $friend_review) = unserialize($filecontent);

		$warning_message = $ec_iPortfolio_guide['not_published_yet_your'];
		$btn_used = "<input name='button' type=button value=' ".$ec_iPortfolio_guide['btn_manage_portfolio']." ' onClick=\"self.location='../contents_student/index_scheme.php'\" class='button_g'>";

		if($status==1 && $student && isset($student))
		{
			$btn_used .= "&nbsp;<input name='button' type=button value=' ".$ec_iPortfolio['review_peer_learning_portfolio']." ' onClick=\"doPeerReview()\" class='button_g'>";
			$btn_used .= "&nbsp;<input name='button' type=button value=' ".$ec_iPortfolio['my_friend_list']." ' onClick=\"doMyFriendList()\" class='button_g'>";
		}

	} else
	{
		$warning_message = $ec_iPortfolio_guide['not_published_yet'];
		if($li_pf->hasRight("manage_student_learning_sharing")!=false)
		{
			$btn_used = "<input name='button' type=button value=\"".$ec_iPortfolio_guide['btn_manage_student_portfolio']."\" onClick='alert(\"".$ec_iPortfolio_guide['manage_student_portfolio_msg']."\");top.location=\"../contents_student/index_scheme.php?student_user_id=".$user_id."\"' class='button_g'>&nbsp;";
		}
		$btn_used .= "<input name='button' type=button value=' ".$button_close." ' onClick=\"self.close()\" class='button_g'>";
	}
*/
?>

<script language="JavaScript">
/*
function doMyFriendList() {
	top.location = "../contents_student/friend_list.php";
}

function doPeerReview() {
	top.location = "../contents_student/review_peer_list.php?review_type=<?=$review_type?>&friend_review=<?=$friend_review?>";
}
*/
</script>
<!--
<link href="<?=$admin_url_path?>/src/includes/css/style_portfolio_button.css" rel="stylesheet" type="text/css">
<br><br>
<table width="500" border="0" cellpadding="10" cellspacing="0" align="center">
<tr><td align="center" class="title"><?= $warning_message ?></td></tr>
<tr><td><hr></td></tr>
<tr><td align="center"><?= $btn_used ?></td></tr>
</table>

</body>
-->
<?php
/*
} else
{

	# log teacher's view history
	if (($ck_memberType=="T" || $ck_memberType=="A") && $ck_user_id!="")
	{
		$li->updateTeacherTracking($user_id, $web_portfolio_id, $ck_user_id);
	}
	*/
?>
<HTML>
<HEAD>
<TITLE>eClass iPortfolio</TITLE>
<script language="JavaScript">
function jOpenCommentPanel(){
	var frameObj = document.getElementById("eClassMainFrame");
	if (frameObj.cols!="*,0")
	{
		frameObj.cols = "*,0";
	} else
	{
		frameObj.cols = "*,400";
	}
}

function change_web_id(wid)
{		
	self.location.href="student.php?student=<?=$student?>&StudentID=<?=$StudentID?>&ClassName=<?=$ClassName?>&selected_wid="+wid;
}

function changeURL(){
	//var url = parent.eClassContentFrame.location.href;
	//var url_array = url.split("/");
	//var page_name = url_array[url_array.length-1];
	//parent.eClassCommentFrame.location.href = "student_comment.php?key=<?=$key?>&page_name=" + page_name;
	parent.eClassCommentFrame.location.href = "student_comment.php?key=<?=$key?>";
}
</script>
</HEAD>

<frameset rows="25, *" frameborder="NO" border="0" framespacing="0">
	<frame name="eClassTopFrame" src="student_comment_menu.php?last_update=<?=$last_update?>&uid=<?=$StudentID?>&StudentRole=<?=$StudentRole?>&ClassName=5B&student=1&selected_wid=<?=($selected_wid==''?$WebPortfolioID:$selected_wid)?>" noresize scrolling="NO">
	<frameset cols="*,0" name="eClassMainFrame" id="eClassMainFrame" frameborder="NO" border="0" framespacing="0">
		<frame name="eClassContentFrame" src="<?=$student_url?>" noresize scrolling="YES" onLoad="changeURL()">
		<frame name="eClassCommentFrame" src="student_comment.php?key=<?=$key?>&WebPortfolioID=<?=$WebPortfolioID?>" noresize>
	</frameset>
</frameset>
<?php
/*
}
*/
?>

</HTML>


<?php
intranet_closedb();
?>
