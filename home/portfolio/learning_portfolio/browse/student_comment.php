<?php
//using: Siuwan
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TSP");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
list($WebPortfolioID, $StudentID) = $li_pf->GET_STUDENT_DECRYPTED_STRING($key);

switch($FieldChanged)
{
	case "page":
		break;
	default:
		$Page = 1;
		break;
}

list($lp_comment_display, $lp_comment_list) = $li_pf->GEN_LP_COMMENT_LIST(array($PageDivision, $Page), $WebPortfolioID, $StudentID);

/*
if($page == "")
{
	$page = 1;
	$prev = 1;
	$next = 1;
}

if ($_GET['page'] != "")
{
	$page = $_GET['page'];
}

$page_size = 5;
$begin = ($page-1) * $page_size;
$end = $begin+1 * $page_size;

# restrieve comments
$li = new notes_iPortfolio();

# YuEn: temporary for demo only!
if ($demo && isset($demo))
{
	$sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType=4 ORDER BY course_id ";
	$row = $li->returnVector($sql);

	for ($i=0; $i<sizeof($row); $i++)
	{
		$course_id_check = $row[$i];
		$sql = "SELECT web_portfolio_id, user_id FROM user_config ORDER BY user_config_id ";
		$row2 = $li->returnArray($sql);

		if (sizeof($row2)>0)
		{
			$key = $li->returnStudentKey($row2[0]["web_portfolio_id"], $course_id_check, $row2[0]["user_id"]);
			break;
		}
	}
}
list($web_portfolio_id, $course_id, $user_id) = $li->getStudentKeyDecrypted($key);

$li->web_portfolio_id = $web_portfolio_id;
$li->db = classNamingDB($course_id);
$lang_used_now = $li->getRoomLanguage($course_id);
if (trim($lang_used_now)=="")
{
	$lang_used_now = "chib5";
}
$iPortfolio_image_path = "../../../images/portfolio_chib5";
$tmp_room_type = $ck_room_type;
$ck_room_type = "iPORTFOLIO";
include_once('../../../system/settings/lang/'.$lang_used_now.'.php');
$ck_room_type = $tmp_room_type;

$comment_total = $li->getCommentCount($web_portfolio_id, $user_id, $course_id);
$total_page = ceil($comment_total / $page_size);
$comment_list_html = $li->getCommentList($web_portfolio_id, $user_id, $course_id, $page_size, $begin, $key);

if ($comment_total>0)
{
	# page navigation
	if ($page != 1)
	{
		$prev = $page-1;
	}
	if ($page < $total_page)
	{
		$next = $page+1;
	} elseif ($page = $total_page)
	{
		$next = $page;
	}
	$page_nav_html = "";
	$page_nav_html .= ($page==1) ? "<img src='../../../images/icon/previous.gif' align='absmiddle' border='0' hspace='3'>".$list_prev : "<a href=\"{$PHP_SELF}?page={$prev}&key={$key}\"><img src='../../../images/icon/previous.gif' align='absmiddle' border='0' hspace='3'>{$list_prev}</a>";
	$page_nav_html .= " &nbsp; <SELECT name='page' onChange=\"javascript:go_to_page(this.options[this.selectedIndex].value, '$key', '$PHP_SELF')\">";
	for ($i=1; $i<=$total_page; $i++)
	{
		$selected_item = ($i==$page) ? "selected" : "";
		$page_nav_html .= "<option value='{$i}' $selected_item >{$i}</option>\n";
	}
	$page_nav_html .= "</SELECT> &nbsp; \n";
	$page_nav_html .= ($page==$total_page) ? $list_next."<img src='../../../images/icon/next.gif' align='absmiddle' border='0' hspace='3'>" : "<a href=\"{$PHP_SELF}?page={$next}&key={$key}\">{$list_next}<img src='../../../images/icon/next.gif' align='absmiddle' border='0' hspace='3'></a>\n";
}

head(11);
*/

include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_header.php");
?>

<style type="text/css">
body { 
  overflow-x: hidden; 
  overflow-y: auto;
}
</style>

<script language="JavaScript">
function checkform(obj){
	if(!check_text(obj.comment, "<?= $ec_warning['comment_content'] ?>")) return false;
	//if(!check_text(obj.author_name, "<?= $ec_warning['comment_author'] ?>")) return false;
	if(!confirm("<?= $ec_warning['comment_submit_confirm'] ?>")) return false;
	return true;
}

function checkRemove(jParCommentID)
{
	if(confirm(globalAlertMsg3))
	{
		document.form1.comment_id.value = jParCommentID;
		document.form1.action="student_comment_remove.php";
		document.form1.method="POST";
		document.form1.submit();
	}

}

function go_to_page(page, key, path)
{
	var url;
	url = path+"?key=" + key + "&page=" + page;
	window.location = url;
}

function submitForm(){
	if (checkform(document.form1))
	{
		document.form1.submit();
	}
}

function resetForm(){
	document.form1.reset();
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField, jParObj)
{
	if(jParField == "page")
	{
		document.form1.Page.value = jParObj.value;
		document.form1.PageDivision.value = '<?=$PageDivision?>';
	}
	else if(jParField == "division")
	{
		document.form1.Page.value = '<?=$Page?>';
		document.form1.PageDivision.value = jParObj.value;
	}
	
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "student_comment.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("PageSelection");
	var MaxValue = PageSelection[0].options[PageSelection[0].length-1].value;
	var MinValue = PageSelection[0].options[0].value;

	TargetValue = <?=$Page?> + jParShift;
	if(TargetValue > MaxValue || TargetValue < MinValue)
	{
		return;
	}
	else
	{
		document.form1.Page.value = TargetValue;
		document.form1.PageDivision.value = '<?=$PageDivision?>';
	}

	document.form1.FieldChanged.value = "page";
	document.form1.action = "student_comment.php";
	document.form1.submit();	
}

</script>

<form name="form1" action="student_comment_update.php" method="post" onSubmit="return checkform(this)">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td valign="top" style="background-color:#FFFFFF; border: 1px #000000 solid;">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="3">
													<tr>
														<td align="left" valign="top" class="tabletop"><a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_comment2.gif" width="20" height="20" border="0" align="absmiddle"></a><span class="tabletopnolink"> <?=$ec_iPortfolio['comments']?></span></td>
													</tr>
													
													<tr>
														<td height="5">
<?php if($ck_memberType != "P") { ?>
															<div border="0" cellspacing="0" cellpadding="2" id="NewButtonTable" style="display:block">
																
																<a href="#" class="contenttool" onClick="$('#NewCommentTable').slideDown();$('#NewButtonTable').hide();"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?></a>
																<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"/>
																
															</div>
<?php } ?>
															<div width="100%" border="0" cellspacing="0" cellpadding="0" id="NewCommentTable" style="display:none">
																
																<div style="width:100%" border="0" cellspacing="0" cellpadding="2">
																	<span class="tabletext"><?=$ec_iPortfolio['your_comment']?></span><br/>
																	<textarea name="comment"  rows="5" wrap="virtual" class="textboxtext"></textarea>
																</div>
														
																<div style="text-align:center">
																	<input type="button" class="formbutton" onClick="submitForm()"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_confirm?>">&nbsp;
																	<input type="button" class="formbutton" onClick="resetForm();$('#NewCommentTable').slideUp(function(){$('#NewButtonTable').show()});"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>">
																</div>

															</div>
														</td>
													</tr>
													
													<tr>
														<td class="tablebottom">
															<table width="100%" border="0" cellspacing="0" cellpadding="3">
																<tr>
																	<td align="left" class="tabletext"><?=$ec_iPortfolio['comments']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($lp_comment_list), $PageDivision, $Page)?>, <?=$ec_iPortfolio['total']?> <?=count($lp_comment_list)?></td>
																	<td align="right">
																		<table border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<table border="0" cellspacing="0" cellpadding="2">
																						<tr align="center" valign="middle">
																							<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp1" width="11" height="10" border="0" align="absmiddle" id="prevp1"></a> <span class="tabletext"> <?=$list_page?> </span></td>
																							<td class="tabletext">
																								<?=$li_pf->GEN_PAGE_SELECTION(count($lp_comment_list), $PageDivision, $Page, "name='PageSelection' onChange='jCHANGE_FIELD(\"page\", this)'")?>
																							</td>
																							<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp1" width="11" height="10" border="0" align="absmiddle" id="nextp1"></a></td>
																						</tr>
																					</table>
																				</td>
																				<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
																				<td>
																					<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
																						<tr>
																							<td><?=$i_general_EachDisplay?></td>
																							<td>
																								<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "onChange='jCHANGE_FIELD(\"division\", this)'")?>
																							</td>
																							<td><?=$i_general_PerPage?></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td><?=$lp_comment_display?></td>
													</tr>
													<tr>
														<td class="tablebottom">
															<table width="100%" border="0" cellspacing="0" cellpadding="3">
																<tr>
																	<td align="left" class="tabletext"><?=$ec_iPortfolio['comments']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($lp_comment_list), $PageDivision, $Page)?>, <?=$ec_iPortfolio['total']?> <?=count($lp_comment_list)?></td>
																	<td align="right">
																		<table border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<table border="0" cellspacing="0" cellpadding="2">
																						<tr align="center" valign="middle">
																							<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp1" width="11" height="10" border="0" align="absmiddle" id="prevp1"></a> <span class="tabletext"> <?=$list_page?> </span></td>
																							<td class="tabletext">
																								<?=$li_pf->GEN_PAGE_SELECTION(count($lp_comment_list), $PageDivision, $Page, "name='PageSelection' onChange='jCHANGE_FIELD(\"page\", this)'")?>
																							</td>
																							<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp1" width="11" height="10" border="0" align="absmiddle" id="nextp1"></a></td>
																						</tr>
																					</table>
																				</td>
																				<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
																				<td>
																					<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
																						<tr>
																							<td><?=$i_general_EachDisplay?></td>
																							<td>
																								<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "onChange='jCHANGE_FIELD(\"division\", this)'")?>
																							</td>
																							<td><?=$i_general_PerPage?></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="key" value="<?=$key?>" />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="Page" />
<input type="hidden" name="PageDivision" />
<input type="hidden" name="comment_id" />
</form>
<?php
intranet_closedb();
?>
