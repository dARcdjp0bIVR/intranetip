<?php
/*
 * Editing by
 *
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
 * 2014-02-20 (Jason)
 * 		- improve to retrieve the corrpesonding protfolio students & group students, who have the right to do, and they have published
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TSP");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($eclass_filepath.'/src/includes/php/lib-groups.php');
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

intranet_opendb();
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';

$li_pf = new libpf_lp();
$li_pf_sterec = new libpf_sturec();
$li_pf_sterec->SET_ALUMNI_COOKIE_BACKUP($ck_is_alumni);

$thisUserID = $_SESSION['UserID'];
/*
head(8);

$li = new notes_iPortfolio();
$li_pf = new portfolio();

$InValidPeriod = $li->checkNotesPeriod($selected_wid);

if($student==1 || ($ck_memberType=="T" && $StudentRole==0))
{
	$webArr = $li->getPublishedWebPortfolio($uid);
	if(sizeof($webArr)!=0)
	{
		$web_select = "<SELECT name='selected_wid' onChange='parent.change_web_id(this.value);document.form1.submit()'>";
		for($i=0; $i<sizeof($webArr); $i++)
		{
			list($wid, $title) = $webArr[$i];
			$title = controlLen($title,66);
			$selected = ($selected_wid==$wid) ? "SELECTED" : "";
			$web_select .= "<OPTION value='".$wid."' $selected>".$title."</OPTION>";
		}
		$web_select .= "</SELECT>";
	}
	if($li_pf->hasRight("manage_student_learning_sharing")!=false)
	{
		$edit_portfolio_html = ($InValidPeriod==1) ? "<a href='javascript:doEdit(1)'><img src='../../../images/icon/edit_content.gif' border='0' hspace='5' title='".$ec_iPortfolio_guide['btn_edit_portfolio']."'></a>" : "";
		$student_manage_html = "<a href='javascript:doManage(1)'><img src='../../../images/portfolio_chib5/gimg/icon_01.gif' border='0' hspace='5' title=\"".$ec_iPortfolio_guide['btn_manage_student_portfolio']."\"></a>";
	}
}
if ($uid==$ck_user_id && $student && $ck_memberType=="S")
{
	$edit_portfolio_html = ($InValidPeriod==1) ? "<a href='javascript:doEdit(0)'><img src='../../../images/icon/edit_content.gif' border='0' hspace='5' title='".$ec_iPortfolio_guide['btn_edit_portfolio']."'></a>" : "";
	$student_manage_html = "<a href='javascript:doManage(0)'><img src='../../../images/portfolio_chib5/gimg/icon_01.gif' border='0' hspace='5' title='".$ec_iPortfolio_guide['btn_manage_portfolio']."'></a>";

	// load settings
	$learning_portfolio_config_file = "$eclass_root/files/learning_portfolio_config.txt";
	$filecontent = trim(get_file_content($learning_portfolio_config_file));
	list($status, $review_type, $friend_review) = unserialize($filecontent);

	if($status==1)
	{
		$peer_review_html = "<a href='javascript:doPeerReview()'><img src='../../../images/portfolio_chib5/Icon/icon_Friendprofolio.gif' border='0' hspace='5' vspace='1' title='".$ec_iPortfolio['review_peer_learning_portfolio']."'></a>";

		$my_friend_list_html = ($friend_review=="1") ? "<a href='javascript:doMyFriendList()'><img src='../../../images/portfolio_chib5/Icon/icon_Friendlist.gif' border='0' hspace='5' vspace='1' title='".$ec_iPortfolio['my_friend_list']."'></a>" : "";
	}
}

$last_update_time_html = base64_decode($last_update);

closedb();
*/

include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_header.php");
$last_update_time_html = base64_decode($last_update);

$wu_key = base64_encode("web_portfolio_id=".$selected_wid."&student_user_id=".$li_pf->IP_USER_ID_TO_EC_USER_ID($uid));
?>

<script language="JavaScript">

function doManage(isTeacher){
	if(isTeacher==1)
		alert("<?=$ec_iPortfolio_guide['manage_student_portfolio_msg']?>");
	top.location = "../contents_student/index_scheme.php?student_user_id=<?=$uid?>";
}

function doEdit(isTeacher){
	if(isTeacher==1)
		alert("<?=$ec_iPortfolio_guide['manage_student_portfolio_msg']?>");
	top.eClassContentFrame.location = "<?=$HTTP_SSL.$eclass_httppath?>src/ip2portfolio/contents_student/contentframe.php?web_portfolio_id=<?=$selected_wid?>&student_user_id=<?=$li_pf->IP_USER_ID_TO_EC_USER_ID($uid)?>";
//	top.eClassContentFrame.location = "/home/eclass/login.php?UserID=<?=$thisUserID?>&uc_id=<?=$li_pf->GET_USER_COURSE_ID($thisUserID)?>&toIP2=1&wu_key=<?=$wu_key?>";
}

function doMyFriendList() {
	top.location = "../contents_student/friend_list.php";
}

function doPeerReview() {
	top.location = "../contents_student/review_peer_list.php?review_type=<?=$review_type?>&friend_review=<?=$friend_review?>";
}

</script>

<form name='form1' method='post' action='student_comment_menu.php'>

<!--
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="../../../images/portfolio_chib5/Icon/SliverBG.gif" style="border-bottom: dotted 1px #888888;"><font color="#999999" size="1">&nbsp;<?= $ec_iPortfolio['site_last_published_time'] . $last_update_time_html?></font></td>
	<td align="right" valign="top" background="../../../images/portfolio_chib5/Icon/SliverBG.gif" style="border-bottom: dotted 1px #888888;"><?=$web_select?>&nbsp;<?=$edit_portfolio_html?><?=$student_manage_html?><?=$peer_review_html?><?=$my_friend_list_html?><a href="javascript:parent.doOpenCommentPanel()"><img src="../../../images/portfolio_chib5/Icon/icon_WriteComment.gif" border="0" hspace="5" vspace="1" title="<?=$ec_iPortfolio['write_comment']?>"></a></td>
  </tr>
</table>
<input type='hidden' name='last_update' value='<?=$last_update?>' />
<input type='hidden' name='uid' value='<?=$uid?>' />
<input type='hidden' name='student' value='<?=$student?>' />
</form>
-->
<?
//==================Get student selection
$libdb = new libdb();
$li_pf_lp = new libpf_lp();
$lpf_fc = new libpf_formclass();
$lu = new libuser($thisUserID);
$dropdownlist = '';
$select_option = 0;
if($lu->isTeacherStaff()){

	# Check whether the student is alumni or not
	$sql = "select YearOfLeft from INTRANET_ARCHIVE_USER where UserID = '".$uid."' and RecordType = '2' ";
	$YearOfLeft = current($libdb->returnVector($sql));
	if(!empty($YearOfLeft)){
		# get Alumni student record from _ARCHIVE_USER
		list($dropdownlist, $t_student_detail_arr) = $li_pf_sterec->GEN_STUDENT_LIST_INFO($ClassName, $uid, true, 1, "name='StudentID' onChange='document.form1.submit()'", $YearOfLeft);
	}else{
		/*
		# old method to retrieve student list wrongly
		$sql = "Select
									yc.YearClassID
							From
									YEAR_CLASS_USER as ycu
									Inner Join
									YEAR_CLASS as yc
									On (ycu.YearClassID = yc.YearClassID)
							Where
									ycu.UserID = '$uid'
									And
									yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							";

		$rs = $libdb->returnArray($sql);
		$YearClassID = $rs[0][0];
		$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
		$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
		*/
		$default_student_id = "";
		## mechanism for retrieving corrpesonding students who has the right to do
		## modified by jason on 2014-2-19 ###########################
		$sql = "select ug.user_id
				from ".$li_pf_lp->course_db.".grouping_function as gf
				inner join ".$li_pf_lp->course_db.".user_group as ug on
				ug.group_id = gf.group_id
				where gf.function_id = '".$selected_wid."' and gf.function_type = 'PORTw' ";
		$group_user_id_arr = $libdb->returnVector($sql);
		if(!empty($group_user_id_arr)){
			# this portfolio has set for certain student group(s) and their portfolio has been published
			# - means: get students who belong to that selected student group of this portfolio
			$group_user_id_str = implode(",", $group_user_id_arr);
		} else {
			# this portfolio has not set for any student groups(s) and their portfolio has been published
			# - means all students who has right to join learning portfolio
			$group_user_id_str = '';
		}
		$sql = "select i.UserID
				from ".$li_pf_lp->course_db.".usermaster as u
				inner join ".$li_pf_lp->course_db.".user_config as c on
					c.user_id = u.user_id and c.web_portfolio_id = '".$selected_wid."' and c.notes_published is not null
				inner join INTRANET_USER as i on
					i.UserEmail = u.user_email
				where
					 u.memberType = 'S' ";
		$sql .= ($group_user_id_str != "") ? " and u.user_id in (".$group_user_id_str.") " : "";
		$sql .= " and (u.status is null || u.status not in ('deleted') ) ";
		$student_id_arr = $libdb->returnVector($sql);
		####### End of mechanism ###########################

		$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
	}
	if(is_array($t_student_detail_arr))
	{
	  for($i=0; $i<count($t_student_detail_arr); $i++)
	  {
	    $t_classname = $t_student_detail_arr[$i]['ClassName'];
	    $t_classnumber = $t_student_detail_arr[$i]['ClassNumber'];
	    $t_user_id = $t_student_detail_arr[$i]['UserID'];
	    $t_user_name = (!empty($t_classname)&&!empty($t_classnumber))?"(".$t_classname." - ".$t_classnumber.") ":"";
	    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);

	   	$t_key = $li_pf_lp->GET_STUDENT_CRYPTED_STRING($selected_wid,$t_user_id);
	    if($uid == $t_user_id){
	    	$default_student_id = $t_key;
	    	$select_option = $i;
	    }


	    $student_detail_arr[] = array($t_key, $t_user_name);
	  }
	}
	//DEBUG_R($student_detail_arr[0][0]);
	$dropdownlist = getSelectByArray($student_detail_arr, "id='StudentIDS' onChange='jCHANGE()'", $default_student_id, 0, 1, "", 2);
	$html_dropdownlist = "<td width=\"25\" nowrap><span class=\"tabletext\"> </span><a href=\"javascript:jCHANGE_STUDENTS('first')\" class=\"tablebottomlink\">&lt;&lt;</a></td>";
	$html_dropdownlist .= "<td width=\"15\" nowrap><span class=\"tabletext\"> </span><a href=\"javascript:jCHANGE_STUDENTS('previous')\" class=\"tablebottomlink\">&lt;</a></td>";
	$html_dropdownlist .= "<td align=\"center\" nowrap><span class=\"tabletext\">";
	$html_dropdownlist .= $dropdownlist;
	$html_dropdownlist .= "<td width=\"15\" nowrap><span class=\"tabletext\"> </span><a href=\"javascript:jCHANGE_STUDENTS('next')\" class=\"tablebottomlink\">&gt;</a></td>";
	$html_dropdownlist .= "<td width=\"15\" nowrap><span class=\"tabletext\"> </span><a href=\"javascript:jCHANGE_STUDENTS('last')\" class=\"tablebottomlink\">&gt;&gt;</a></td>";
	$html_dropdownlist .= "<td width=\"15\" nowrap> | </td>";
	$html_dropdownlist .= "<td nowrap class=\"tabletext\">".str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])."</td>";
}
//End Get student selection======================
?>

<table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#FFFFFF">
	<tr>
		<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/top_menu_bar.gif" class="tablebottom" style="border-bottom:1px #999999 solid"><a href="index_edit.htm" class="tabletoplink"></a>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width=23%><span class="tabletext"><span class="LP_publish"><?=$ec_iPortfolio['publish_date']?>:</span> <?=$last_update_time_html?> </span></td>

						<?=$html_dropdownlist?>

						<td width=23% align="right"><table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" class="LP_on_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
								<td align="right" valign="top" nowrap class="tabletext"><span class="LP_publish"><a href="#" class="tabletool" onClick="parent.jOpenCommentPanel()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_comment2.gif" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['view_comment']?></a></span> </td>
							</tr>
						</table></td>
				</tr>
			</table></td>
	</tr>
</table>

<script language="JavaScript">
//add by stanley 20100621

function jCHANGE(){
    var data_transmit = { ParUserID: <?=$UserID?>, FunctionName: 'retrieveSessionKey' };
	$.ajax({
		url		: "../../ajax/eclassAccess.php",
		type	: "POST",
		data	: data_transmit,
		success : function(data){
                    sessionKey = data;
                    if(sessionKey==''){

                    }else{
						top.location = "./student_view.php?key="+$('#StudentIDS :selected').val()+"&eclasskey="+sessionKey;
                    }
                }
	});
}
// Quick student switch
function jCHANGE_STUDENTS(selected)
{
    var data_transmit = { ParUserID: <?=$UserID?>, FunctionName: 'retrieveSessionKey' };
	$.ajax({
		url		: "../../ajax/eclassAccess.php",
		type	: "POST",
		data	: data_transmit,
		success : function(data){
                    sessionKey = data;
                    if(sessionKey==''){

                    }else if(selected == 'first'){
						if(<?=$select_option?> != 0)
							top.location = "./student_view.php?key=<?=$student_detail_arr[0][0]?>&eclasskey="+sessionKey;
					}
					else if(selected == 'previous'){
						if(<?=$select_option?> - 1 >= 0)
							top.location = "./student_view.php?key=<?=$student_detail_arr[$select_option-1][0]?>&eclasskey="+sessionKey;
					}
					else if(selected == 'next'){
						if(<?=$select_option + 1?> <= <?=count($student_detail_arr)-1?>)
							top.location = "./student_view.php?key=<?=$student_detail_arr[$select_option+1][0]?>&eclasskey="+sessionKey;
					}
					else if(selected == 'last'){
						if(<?=$select_option?> != <?=count($student_detail_arr)-1?>)
						top.location = "./student_view.php?key=<?=$student_detail_arr[count($student_detail_arr)-1][0]?>&eclasskey="+sessionKey;
					}
                }
	});
}
// end stanley
</script>
<?php
$li_pf_sterec->RELOAD_ALUMNI_COOKIE_BACKUP($ck_is_alumni);
intranet_closedb();
?>
