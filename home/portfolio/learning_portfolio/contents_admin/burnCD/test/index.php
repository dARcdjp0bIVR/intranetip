<style type="text/css">

body{
	font-size: 12px;
}
</style>
<?php




//Editing by: Stanley
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
//iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

include_once($PATH_WRT_ROOT.'includes/thirdParty/simpletest/unit_tester.php');
include_once($PATH_WRT_ROOT.'includes/thirdParty/simpletest/reporter.php');

//include_once('function.php');

$line =  file_get_contents("/home/web/eclass40/intranetIP25/home/portfolio/learning_portfolio/contents_admin/burnCD/test/testData/1.txt");

$ary = preg_split('/\n/',$line);
$objLP = new libpf_lp();
for($i =0,$i_max= sizeof($ary);$i< $i_max ; $i++){
	//loop every line in the 1.txt (a testing directory list)
	$ary3 = array();
	$error = 0;
	$_line = $ary[$i];
	$pattern = '/^##/';

	if(preg_match($pattern, $_line, $matches)){
		continue;
	}

	list($srcDir, $modalAns) = explode("\t",$_line);

	if (preg_match('/\/$/', $srcDir)) {
		//if the dir end with a "/" , remove it
		$srcDir = substr($srcDir,0,-1);
	}
	
	$srcDir2 = $srcDir.'_burnCD_tmp_src';

	if(file_exists($srcDir2) && is_dir($srcDir2)){
		$cmd = 'rm -rf '.$srcDir2;
		$cmd = OsCommandSafe($cmd);	
		debug_r($cmd);
		exec($cmd,$return);
	}
	$cmd = 'cp -R '.$srcDir.' '.$srcDir2;
	$cmd = OsCommandSafe($cmd);	
	debug_r($cmd);
	exec($cmd,$return);

	$objLP->pepareDir($srcDir2);
//	$objLP->pepareDir($srcDir);

	
}

/*
function pepareDir($srcDir){

	$copyDir = $srcDir.'_ipf_burncd_tmp';

	clearTmpDirectory($copyDir);
	
	$copyOK = makeCopyDirectory($srcDir,$copyDir);

	if($copyOK){
		$renameIdCounter = 0;

		//store pattern and replacement then use to replace the filename in HTML file
		$patternList = array();
		$replacementList = array();
		$fileStack = array();
		$orgDir = $copyDir;
		handleDir($copyDir,$orgDir,$fileStack,$renameIdCounter,$patternList,$replacementList);
	}else{
		echo "COPY ERROR !!!<br/>";
	}
}
*/

/*
Those value is pass by reference
&$renameIdCounter,
&$patternList,
&$replacementList
*/
function handleDirOLD($directory,$orgDirectory,&$fileStack,&$renameIdCounter,&$patternList,&$replacementList){
	echo 'start handleDir --><b>'.$directory.'</b><br/>';
	$result = getFileList($directory);

//	echo 'get file is '.$directory.' -- size of '.sizeof($result).'<br/>';
	$resultAnalysis = analysisFile($result,$directory);
//	$renameIdCounter = 0;

	//store pattern and replacement then use to replace the filename in HTML file
//	$patternList = array();  
//	$replacementList = array();
debug_r($orgDirectory);
	foreach($resultAnalysis as $fileName =>$details){

		$_renameRequire = $details['renameStatus'];
		$_isDir = $details['isDirectory'];
		$_urlEncodingName = $details['urlEncoding'];
		
		$_fullPath = $directory.'/'.$fileName;
		$_orgFullPath = $orgDirectory.'/'.$fileName;

//		echo $fileName.' url encode is -->'.$details['urlEncoding'].'<--<br/>';
		if($_renameRequire){
			$renameIdCounter = $renameIdCounter + 1;

			//get the new file name
			$ext = getFileExtension($fileName);  //get the original file ext
			$_newFileName = ($ext == "")? $renameIdCounter : $renameIdCounter.'.'.$ext; //append the ext to the file if needed 

//			echo "action --> rename from ".$directory.'/'.$fileName.' to '.$directory.'/'.$_newFileName.' UUURRRLLL -->'.$_urlEncodingName.'<----<br/>';
			echo "action --> rename from <b>".$fileName.'</b> to ----------><b>'.$_newFileName.'</b> UUURRRLLL -->'.$_urlEncodingName.'<----<br/>';

			//importance!! override the $_fullPath since file / directory is renamed, prepare for the next level f:handleDir
			$_fullPath = $directory.'/'.$_newFileName; 

			//record the new file name back to the fileName $resultAnalysis[$fileName]
			$resultAnalysis[$fileName]['newName'] = $_newFileName;

			rename($directory.'/'.$fileName , $directory.'/'.$_newFileName);

			//put the orginal file and new file name into the $patternList and  $replacementList for replace in HTML
			if($_isDir){
				//is directory , special handle [directory "/"];
				$patternList[] = '/'.$_urlEncodingName.'\//';
				$replacementList[] = $_newFileName.'/';
			}else{
				$patternList[] = '/'.$_urlEncodingName.'/';
				$replacementList[] = $_newFileName;
			}

		}
//		$fileStack[$orgDirectory][] = array($fileName,$details);
		$fileStack[$orgDirectory][] = array($fileName,$resultAnalysis[$fileName]);

		if($_isDir){
//			echo $fileName.' you are directory wor!!<br/>';
			//It is a directory  , go into it and redo the rename over again
			handleDir($_fullPath,$_orgFullPath,$fileStack,$renameIdCounter,$patternList,$replacementList);
		}
	}

	//since the $patternList will be update if a same file name exist in other directory, it will cause replace error. 
	//A same file replaced with a other file id in other directory.
	//So, Only do the replacment while the working directory is extractly in pattern "student_u2_wp48" only, after than empty the $patternList and $replacementList 
	if(preg_match('/student_u\d+_wp\d+$/',$directory)){

//		debug_r($patternList);
//		debug_r($orgDirectory);
		
		echo '<font color="red"> student directory '.$directory.'</font><br/>';
//debug_r($fileStack);
$patternList2 = array();
$replacementList2 = array();
foreach($fileStack as $directory=>$subDirAry){
	


	$pattern = '/^.*\/student_u\d+_wp\d+\/?(.*)$/';
	$replacement = '${1}';
	$directory2 = preg_replace($pattern,$replacement,$directory);

	$directorPart = explode('/',$directory2);


	$directory3 = "";

	$patternDirectoryDelimiter = '\/';
	$replacementDirectoryDelimiter = '/';

	if(trim($directory2) == ''){
		$patternDirectoryDelimiter = '';
		$replacementDirectoryDelimiter = '';
	}
	for($z = 0,$z_max = sizeof($directorPart);$z <$z_max;$z++){
		//$directory3 .= urlencode($directorPart[$z]).'\/';
		$directory3 .= urlencode($directorPart[$z]).$patternDirectoryDelimiter;
	}

	echo 'dir1 -->'.$directory.'<-- dir2 -->'.$directory2.'<-- url dir3 -->'.$directory3.'<--<br/>';

	for($z = 0,$z_max = sizeof($subDirAry);$z < $z_max;$z++){
		$_element = $subDirAry[$z];
//		debug_r($_element);
		$_file = $_element[0];
		$_fileDetails = $_element[1];
		$_isDirectory = $_fileDetails["isDirectory"];
		$_urlEncoding = $_fileDetails["urlEncoding"];
		$_location = $_fileDetails["location"];
		$_locationReplace = preg_replace($pattern,$replacement,$_location);
		$_newName = $_fileDetails["newName"];
		if($_isDirectory){
			//do nothing
		}else{
			$patternList2[] = '/'.$directory3.$_urlEncoding.'/';
//			$replacementList2[] = $_locationReplace.'/'.$_newName;
			$replacementList2[] = $_locationReplace.$replacementDirectoryDelimiter.$_newName;
		}
	}
	
}
debug_r($patternList2);
debug_r($replacementList2);
		//start the replace content
		foreach($resultAnalysis as $fileName =>$details){
			$ext = strtolower(trim(getFileExtension($fileName)));

			$_renameRequire = $details['renameStatus'];
			$_newName = $details['newName'];
			$_location = $details['location'];
			
			if($_renameRequire){
				$_fName = $_newName;
			}else{
				$_fName = $fileName;
			}
			$_fullPath = $_location.'/'.$_fName;
	//echo 'full path --> ['.$_fullPath.']<br/>';
	//echo 'before<br/>';
	//debug_r($patternList);

			if($ext == 'html' || $ext == 'htm'){
//				replaceContent($_fullPath,$patternList,$replacementList);
				replaceContent($_fullPath,$patternList2,$replacementList2);
			}
		}
		$patternList = array();
		$replacementList = array();
		$fileStack = array();
	}
	echo '<hr size="1"/>';
}
/*
function replaceContent($file,$patternList,$replacementList){
	echo 'i \'ve got the file '.$file.'<br/>';

//debug_r($patternList);

	if(file_exists($file)){
		$_line		=  file_get_contents($file);
		$_newLine	=  preg_replace($patternList,$replacementList,$_line);
		file_put_contents($file, $_newLine);
	}
}
*/
/*
function clearTmpDirectory($srcDir){

	if(file_exists($srcDir) && trim($srcDir) != ""){
		$cmd = 'rm -rf '.$srcDir;
		exec($cmd);
	}
}
/*
function makeCopyDirectory($srcDir,$copyDir){

	$cmd = 'cp -pir '.$srcDir.' '.$copyDir;
	exec($cmd,$fileListArr);

	if(file_exists($copyDir)){
		return true;
	}else{
		return false;
	}
}*/

/*
function analysisFile($fileList,$fileLocation){

	$returnList = array();
	$exceptionList[] = '^index';
	$exceptionList[] = '^mypage';
	$exceptionList[] = '^student_u';
	$exceptionList[] = 'iPt';
	$exceptionList[] = 'css$';
	for($f = 0,$f_max = sizeof($fileList);$f < $f_max;$f++) {

		$keep = false;

		$_file = $fileList[$f];

		for($i = 0,$i_max = sizeof($exceptionList);$i < $i_max;$i++){
			$_pattern = '/'.$exceptionList[$i].'/i';

			if(preg_match($_pattern, $_file)){
				$keep = true;

			}
		}

		$_renameStatus = ($keep) ? false : true;
		$_urlEncoding = urlencode($_file);

		$_fullPath = $fileLocation."/".$_file;
		$_isDirectory = (is_dir($_fullPath))? true: false;
		

		$returnList[$_file] = array('renameStatus'	=> $_renameStatus,
									'name'			=> $_file, 
									'urlEncoding'	=> $_urlEncoding, 
									'isDirectory'	=> $_isDirectory,
									'location'=>$fileLocation
								);		
	}
	return $returnList;
}
*/
exit();

?>


