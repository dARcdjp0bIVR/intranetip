<?
//Editing by: 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_opendb();

$li_pf = new libpf_lp();
$li_pf->ADMIN_ACCESS_PAGE();
$thisUserID = $_SESSION['UserID'];

// check iportfolio admin user
if (!$li_pf->IS_ADMIN_USER($thisUserID))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Generate a class selection drop-down list
$lpf_ui = new libportfolio_ui();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_BurningCD";
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];



### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

// load settings
$prepare_CDburning_config = "$eclass_filepath/files/prepare_CDburning_config.txt";
$filecontent = trim(get_file_content($prepare_CDburning_config));

if(empty($filecontent)){
	$parent_right = $student_right = 0;	
}
else{
	list($student_right, $parent_right) = unserialize($filecontent);
	list(,$student_right) = explode(":", $student_right); 
	list(,$parent_right) = explode(":", $parent_right); 
}


$linterface->LAYOUT_START();
?>
<script language="javascript">

</script>



<form name="form1" method="post" action="prepare_CDburning_setting_update.php">
<?php if($msg==2) echo "<p align='left'><font color='green'>".$iPort["msg"]["record_update"]."</font></p>"; ?>
<table width="80%" border="0" cellspacing="0" cellpadding="4">

<tr>
	<td width="15%" valign="top" nowrap="yes"><?=$Lang['iPortfolio']['cd_burning_setting_forstudent']?> : </td>
	<td>
		<ul>
			<input type="radio" name="student_right" id="s_1" value="1" <?=($student_right)?'CHECKED':''?>>&nbsp;<label for="s_1"><?=$Lang['iPortfolio']['allowed']?></label>
			
			<input type="radio" name="student_right" id="s_0" value="0" <?=($student_right)?'':'CHECKED'?>>&nbsp;<label for="s_0"><?=$Lang['iPortfolio']['prohibit']?></label>
		</ul>
	</td>
</tr>
<tr>
	<td width="15%" valign="top" nowrap="yes"><?=$Lang['iPortfolio']['cd_burning_setting_forparent']?> : </td>
	<td>
		<ul>
			<input type="radio" name="parent_right" id="p_1" value="1" <?=($parent_right)?'CHECKED':''?>>&nbsp;<label for="p_1"><?=$Lang['iPortfolio']['allowed']?></label>
			
			<input type="radio" name="parent_right" id="p_0" value="0" <?=($parent_right)?'':'CHECKED'?>>&nbsp;<label for="p_0"><?=$Lang['iPortfolio']['prohibit']?></label>
		</ul>
	</td>
</tr>
</table>
<br />
<table width="90%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td align="left">
		<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_submit?>" />
	</td>
</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>