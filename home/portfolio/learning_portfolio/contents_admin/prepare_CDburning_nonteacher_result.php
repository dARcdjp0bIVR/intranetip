<?php

// Modifying by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
iportfolio_auth("SP");
intranet_opendb();


switch($_SESSION['UserType'])
{
	case 2:
		$CurrentPage = "Student_CDBuring";
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$CurrentPage = "Parent_CDBuring";
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}

$li_pf = new libpf_slp();

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

//DEBUG(base64_decode($path));


// check iportfolio admin user
if (!$li_pf->IS_ADMIN_USER($thisUserID))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$year_class = new year_class($YearClassID);
$ClassName = $year_class->Get_Class_Name();

if($resultFrom=="copy")
{
	$remind_msg = $ec_iPortfolio['get_prepared_portfolio_ST'];
	$btn = "<input onClick='javascript:self.location.href=\"prepare_CDburning_download.php?YearClassID=".$YearClassID."&path=".$path."\"' name='button' type=button value='&nbsp;".$ec_iPortfolio['make_zip_download']."&nbsp;' class='formbutton'>&nbsp;";
}

$result_display = "<br /><br />";
$result_display .= "<table border='0' cellpadding='8' cellspacing='0' width='80%'>";
$result_display .= "<tr><td class='chi_content_15'>".$message."</td></tr>\n";
$result_display .= ($remind_msg!="") ? "<tr><td class='chi_content_15'>".$remind_msg."</td></tr>\n" : "";
$result_display .= "<tr><td><br /></td></tr>\n";
$result_display .= "<tr><td align='center'>".$btn."</td></tr>\n";
$result_display .= "</table>\n";





$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>

<?=$result_display ?>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
