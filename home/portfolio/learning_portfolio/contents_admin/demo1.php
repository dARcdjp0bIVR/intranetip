<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$li_pf->ADMIN_ACCESS_PAGE();
$thisUserID = $_SESSION['UserID'];

// check iportfolio admin user
if (!$li_pf->IS_ADMIN_USER($thisUserID))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Generate a class selection drop-down list
if(count($li_pf->GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST()) > 0)
{
	$PublishedLPClass = $li_pf->GEN_PUBLISH_LP_CLASS_SELECTION();
}
else
{
	$PublishedLPClass = "<i>".$no_record_msg."</i>";
	$DisableSubmit = "DISABLED";
}

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_BurningCD";
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

$luser = new libuser($thisUserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>
<script language="javascript">
function checkform(myObj){
	var copy_alert_sentence = "<?=$ec_warning['copy_portfolio']?>";
	var remove_alert_sentence = "<?=$ec_warning['remove_copied_portfolio']?>";

	if(myObj.ClassName.value == "")
	{
		alert("<?=$ec_warning['please_select_class']?>");
		return false;
	}

	for(i=0; i<myObj.work_type.length; i++)
	{
		if(myObj.work_type[i].checked)
			var type = parseInt(myObj.work_type[i].value);
	}

	alert_sentence = (type==1) ? copy_alert_sentence.replace("XXX", myObj.ClassName.value) : remove_alert_sentence.replace("XXX", myObj.ClassName.value);

	if(confirm(alert_sentence))
		return true;
	else
		return false;
}

</script>

<form name="form1" method="post" action="prepare_CDburning_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
								<td valign="top"><?=$PublishedLPClass?></td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
								<td width="80%">
									<span class="tabletext">
										<input type="radio" name="work_type" value="1" checked><?=$ec_iPortfolio['copy_web_portfolio_files']?> 
									</span>
									<span class="tabletext">
										<input type="radio" name="work_type" value="2"><?=$ec_iPortfolio['remove_copied_files']?><br />
										<label for="grading_passfail" class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="20" height="10"></label>
									</span>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">&nbsp;</td>
								<td valign="top">
									<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><?=$ec_iPortfolio['with_comment']?></span>
									</label><br/>
									<label for="schoolRecord" class="tabletext">
										<input type="checkbox" name="with_comment" id="schoolRecord" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">School Records</span>
									</label><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">

										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Merit/Demerit Comment </span>
									</label><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Academic Report </span>
									</label><br/>

									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Activity </span>
									</label><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Award </span>
									</label><br/>

									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Attendance </span>
									</label><br/>


									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">Service Record </span>
									</label><br/>


									&nbsp;&nbsp;&nbsp;&nbsp;<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">OLE </span>
									</label><br/>

									<label for="grading_passfail" class="tabletext">
										<input type="checkbox" name="with_comment" id="grading_passfail" value="1">
										<span class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">SLP</span>
									</label><br/>


								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_submit?>" <?=$DisableSubmit?> />
									<input type="button" class="formbutton" value="<?=$button_cancel?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="self.location='../../school_records.php'">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
