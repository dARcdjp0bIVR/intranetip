<?php

##### Change Log [Start] #####
#
#	Date:	2015-03-16	Omas
# 		Create this file for zip file and download file
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");



intranet_opendb();
$li_pf = new libpf_lp();
$lf = new libfilesystem();

if(!isset($filePath)){
// ajax return download link(start)

if(!isset($YearClassID) || !is_array($student)){
	exit;
}

$student_array = $li_pf->GET_STUDENTS_FOR_COPY_PORTFOLIO($YearClassID);
$student_name_array = BuildMultiKeyAssoc($student_array,'CourseUserID',array('EnglishName','WebSAMSRegNo'));

foreach((array)$student as $_studentID => $_isTarget){
	if($_isTarget){
		$TargetStudentArr[] = $_studentID;
	}
}

// common files
$includedFilesListAry = array($YearClassID.'/common_files',$YearClassID.'/images');


foreach((array)$TargetStudentArr as $_studentID){
	$_studentName = str_replace(' ','_',$student_name_array[$_studentID]['EnglishName']);
	$_studentWebSAMS = str_replace('#','',$student_name_array[$_studentID]['WebSAMSRegNo']);
	$_folderName = $_studentWebSAMS.'_'.$_studentName;
	$_studentPath = $YearClassID.'/'.$_folderName;
	$includedFilesListAry[] = $_studentPath;
}

//$array = array("YearClassID"=>$_REQUEST['YearClassID'], "student"=>$_REQUEST['student']);
//$serArray = base64_encode(serialize($array));

// go to the path
$path = "$li_pf->file_path/portfolio_2_burn/";
chdir($path);

// check temp exist
if(!file_exists('temp')){
	mkdir('temp');
}

$UserPath = 'temp/'.$UserID;

// check user folder exist
if(!file_exists($UserPath)){
	mkdir($UserPath);
}

// Create time folder 
$nowUnixTime=time(); // Get current time - unix timestamp
$targetPath = $UserPath.'/'.$nowUnixTime;
if(!file_exists($targetPath)){
	mkdir($targetPath);
}

// exec zip command
$timestamp = date('YmdHms', $nowUnixTime);
$zipFilename = 'iPortfolioCD_'.$YearClassID.'_'.$timestamp.'.zip'; 
$command = "zip -r ".$zipFilename." '".implode("' '",$includedFilesListAry)."'";
$command = str_replace('(','',$command);
$command = str_replace(')','',$command);
$command = OsCommandSafe($command);	
exec($command);

//for debug
//exec($command, $output, $return); //$lf->file_zip not work
//debug_pr($command);
//debug_pr($output);

$oldfile = $zipFilename;
$newfile = $targetPath.'/'.$oldfile;
$lf->file_rename($oldfile, $newfile);

$file = $targetPath.'/'.$zipFilename;

echo '<button type="button" class="formbutton" onclick="javascript:location.href=\'CDburning_download_perform.php?filePath='.base64_encode($file).'\'">'.$Lang['Button']['Download'].'</button>';

// ajax return download link(End)
}else{
// download file (start)
	$file = base64_decode($filePath);
	
	$path = "$li_pf->file_path/portfolio_2_burn/";
	chdir($path);
	
	if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
	}
// download file (end)
}

intranet_closedb();
?>