<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();
$UserID = $_SESSION['UserID'];

# Check for iPortfolio admin user
if (!$li_pf->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$search = array(".html", ".HTML", ".htm", ".HTM");
$fileTitle = str_replace($search, "", basename($flink));

include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_header.php");

?>

<script language=javascript>
function copyFile(obj) {
	var msg = "<?=$ec_warning['coyp_template']?>";
	if(confirm(msg)){
		obj.btn_submit.disabled=true;
		obj.action = "template_copy_update.php";
		obj.submit();
	}
	return false;
}
</script>

<form name="form1" method="post" onSubmit="return copyFile(this);">
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td>
			<table width=55% border=0 cellpadding=5 cellspacing=0 align="center">
			  <tr>
					<td align=right nowrap valign="top" class="tabletext"><?=$ec_iPortfolio['template_title']?>:</td>
					<td class="tabletext"><?=$fileTitle?></td>
			  </tr>
			  <tr>
					<td align=right nowrap valign="top" class="tabletext"><?=$ec_iPortfolio['copied_template_title']?>:</td>
					<td><input type="text" name="newTitle" /></td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center"><hr /></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="right">
					<input class="button_g" type="submit" value="<?=$button_submit?>" name="btn_submit"> 
					<input class="button_g" type="reset" value="<?=$button_reset?>">
					<input class="button_g" type="button" value="<?=$button_cancel?>" onClick="self.close()"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name='file_id' value="<?=$file_id?>">
<input type=hidden name=Page value="<?=$Page?>">
<input type=hidden name=PageDivision value="<?=$PageDivision?>">
<input type=hidden name=Field value="<?=$Field?>">
<input type=hidden name=Order value="<?=$Order?>">
</form>

<?php
include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/popup_footer.php");
intranet_closedb();
?>
