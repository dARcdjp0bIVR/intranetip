<?php

// Modifying by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_opendb();

$li_pf = new libpf_slp();
$li_pf->ADMIN_ACCESS_PAGE();
$lf = new libfilesystem();

$student_right = "s:".$student_right; 
$parent_right = "p:".$parent_right;

$setting_array = array($student_right, $parent_right);

$prepare_CDburning_config = "$eclass_filepath/files/prepare_CDburning_config.txt";

$lf->file_write(serialize($setting_array), $prepare_CDburning_config);

intranet_closedb();
header("Location: prepare_CDburning_setting.php?msg=2");
?>