<?php
// Modifying by
/*
 * 20150213 Ivan [B75449]
 * - modified max file size from 50MB to 300MB
 */ 

// Set the locale to UTF8, otherwise, escapeshellarg used in libfilesystem.php may not work correctly on UTF8 strings.
setlocale(LC_CTYPE, "UTF8", "en_US.UTF-8");

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("STP");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lf = new libfilesystem();

$cwd = getcwd();
$path = base64_decode($path);

//B75449
//$max_direct_dl_size = 50;		# in MB
$max_direct_dl_size = 300;		# in MB

$saveto = "/tmp/iPortfolioBurnCD";
if(!file_exists($saveto))
{
		$lf->folder_new($saveto);
}

# Remove existing zip
if(file_exists($path)){
		$lf->file_remove($saveto."/".basename($path).".zip");
}


# Zip prepared files
$lf->file_zip(basename($path), $saveto."/".basename($path), dirname($path));

$zipFile = $saveto."/".basename($path).".zip";
$fileSize = filesize($zipFile);
$displayFileSize = round($fileSize  / (1024*1024) , 2);

if($fileSize < $max_direct_dl_size * 1024 * 1024)
{
	# Output the zip file to browser
	output2browser(get_file_content($saveto."/".basename($path).".zip"), basename($path).".zip");
}
else
{
	chdir($cwd);
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
	
	$li_pf = new libpf_lp();

//	$btn = "<input onClick='javascript:self.location.href=\"http://".$eclass_httppath."/files/portfolio_2_burn/".$YearClassID.".zip\"' name='button' type=button value='".$i_Files_ClickToDownload."' class='formbutton'>&nbsp;";
	$btn .= "<input onClick='javascript:self.location.href=\"prepare_CDburning.php\"' name='button' type=button value='".$ec_iPortfolio['prepare_other_class_cd_burning']."' class='formbutton'>";
		
	$result_display = "<br /><br />";
	$result_display .= "<table border='0' cellpadding='8' cellspacing='0' width='80%'>";
		$result_display .= "<tr><td align='center'>因檔案太大 (~".$displayFileSize." M)，同學的學習檔案已預備至以下檔案<br/><br/><b>".$zipFile."</b><br/><br/>請登入 eClass Server 下載。<br/><br/> 如有疑問，可詢問學校的技術人員</td></tr>\n";
	$result_display .= "<tr><td align='center'><hr size = \"1\" width= \"50%\"/>".$btn."</td></tr>\n";
	$result_display .= "</table>\n";
	
	// template for teacher page
	$linterface = new interface_html();
	// set the current page title
	$CurrentPage = "Teacher_BurningCD";
	$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];
	
	$luser = new libuser($UserID);
	
	### Title ###
	$TAGS_OBJ[] = array($CurrentPageName,"");
	$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
	
	$linterface->LAYOUT_START();
	
	echo $result_display;
	
	$linterface->LAYOUT_STOP();
}
?>
