<?php
//Editing by: Stanley
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
iportfolio_auth("SP");
intranet_opendb();


switch($_SESSION['UserType'])
{
	case 2:
		$CurrentPage = "Student_BurningCD";
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$CurrentPage = "Parent_BurningCD";
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}

$li_pf = new libpf_slp();

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$loading_image = $linterface->Get_Ajax_Loading_Image();
$DisplayName = $luser->UserName();

//Get YearClassID
$CurrentAcademicYear = Get_Current_Academic_Year_ID();
$lpdb = new libdb();
$sql = "SELECT ycr.YearClassID 
FROM
{$intranet_db}.INTRANET_USER iu 
INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycr
            ON ycr.UserID = iu.UserID
INNER JOIN {$intranet_db}.YEAR_CLASS yc 
	    ON yc.YearClassID = ycr.YearClassID
WHERE iu.UserID = $StudentID AND yc.AcademicYearID = $CurrentAcademicYear";

$YearClassID = $lpdb->ReturnArray($sql);
$YearClassID = $YearClassID[0]['YearClassID'];

//if($sys_custom['iPortfolio_BurnCD_FullReport']){ //  turn on this for all school 20101202
$html_freport = "<input type=\"checkbox\" name=\"with_freport\" id=\"with_freport\" value=\"1\"><label for=\"with_freport\">{$ec_iPortfolio['with_freport']}</label><br />";
//}

?>

<?php // ===================================== Body Contents ============================= ?>
<script language="javascript">

var student_table_top = '<tr class="tabletop"></tr><tr class="tabletop"><td width="95%" class="tabletopnolink" >Name</td><td class="tabletopnolink" ><input onclick="return false;" type="checkbox" value="1" CHECKED/></td></tr>';
var loading_image = '<?=$loading_image?>';
function checkform(myObj){
	var copy_alert_sentence = "<?=$ec_warning['copy_portfolio_SP']?>";
	var remove_alert_sentence = "<?=$ec_warning['remove_copied_portfolio']?>";
	var pass = false;
	
	
		var type = parseInt($('input:radio[name=work_type]:checked').val());
	
		var alert_sentence = (type==1) ? copy_alert_sentence.replace("XXX", "<?=$DisplayName?>") : remove_alert_sentence.replace("XXX", "<?=$DisplayName?>");
	
		if(confirm(alert_sentence)){
			$('#StudentList').hide();
			$('#loading').append("<br /><br /><?=$Lang['iPortfolio']['alertLeavePage']?>");
			$('#loading').show();
			$('.formbutton').hide();
			return true;
		}
		else{
			return false;
		}
	

}

function checkall(obj){
	if($(obj).attr("checked")){
		$('.studentbox').attr('checked', true);			
	}
	else{
		$('.studentbox').attr('checked', false);
	}
	
}



</script>

<form name="form1" method="post" action="prepare_CDburning_nonteacher_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">

							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
								<td width="80%" class="tabletext">
									<input type="radio" name="work_type" id="worktype_1" value="1" checked><label for="worktype_1"><?=$ec_iPortfolio['copy_web_portfolio_files']?></label> 
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">&nbsp;</td>
								<td valign="top">
									<input type="checkbox" name="with_comment" id="with_comment" value="1"><label for="with_comment"><?=$ec_iPortfolio['with_comment']?></label><br />
									<?=$html_freport?>
									<input type="checkbox" name="with_slp" id="with_slp" value="1"><label for="with_slp"><?=$ec_iPortfolio['with_slp']?></label><br />
									<input type="checkbox" name="with_sbs" id="with_sbs" value="1"><label for="with_sbs"><?=$ec_iPortfolio['with_sbs']?></label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" border="0" cellSpacing="1" cellPadding="4" id="StudentList">
						<!--list -->
						<tr class="tabletop"></tr><tr class="tabletop"><td width="95%" class="tabletopnolink" >Name</td><td class="tabletopnolink" ><input onclick="return false;" type="checkbox" value="1" CHECKED/></td></tr>
						<tr><td><?=$DisplayName?></td><td><INPUT type="checkbox" name='student[<?=$StudentID?>]' onclick="return false;" value="1" CHECKED/></td></tr>
						</table>
						<div style="display:none" id="loading"><?=$loading_image?></div>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_submit?>" <?=$DisableSubmit?> />
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<INPUT name="YearClassID" Value="<?=$YearClassID?>" type="hidden" />
</form>

<?php // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
