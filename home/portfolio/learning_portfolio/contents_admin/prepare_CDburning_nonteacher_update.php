<?php

// Modifying by STANLEY

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("SP");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
intranet_opendb();
if($ck_memberType=='P'||$ck_memberType=='S'){
	$student[$ck_user_id]=1; //do not use value from post, use classroom user_id instead of intranet_user_id
}
$li_pf = new libpf_lp();
$li_pf2 = new libpf_lp2();

//$li_pf->ADMIN_ACCESS_PAGE();

if($work_type==1)
{
	$copy_path = $li_pf->COPY_PORTFOLIO_FILES($YearClassID, $with_comment, $with_slp, $with_sbs, $student, $with_freport);
	$li_pf2->exportPortfolios($copy_path, $li_pf->GET_STUDENTS_FOR_COPY_PORTFOLIO($YearClassID), $student);
	intranet_closedb();
	header("Location: prepare_CDburning_nonteacher_result.php?resultFrom=copy&YearClassID=".$YearClassID."&path=".base64_encode($copy_path));
}

?>
