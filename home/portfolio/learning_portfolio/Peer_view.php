<?php

// Modifing by Stanley

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($eclass_filepath.'/src/includes/php/lib-groups.php');


intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();


$lpf = new libportfolio();
$li_pf = new libpf_lp();
$StudentID = $_SESSION['UserID'];

$luser = new libuser($StudentID);


$DisplayType = "thumbnail";


$has_lp_access_right = (strstr($ck_user_rights, ":web:") && $lpf->HAS_RIGHT("learning_sharing")) ? 1 : 0;
$img_activated = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\">";
$img_lp = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\">";

if($search_name == $ec_iPortfolio['enter_student_name'])
{
	$search_name = "";
}
else
{
  $cond .=  " AND (iu.ChineseName LIKE '%".$search_name."%' OR iu.EnglishName LIKE '%".$search_name."%')";
}
if(!empty($addional_cond)){
	$cond .= " AND yc.YearClassID = {$addional_cond}";
}

// template for student page
$linterface = new interface_html("iportfolio_default2.html");
$CurrentPage = "Student_LearningPortfolio";
// set the current page title
$CurrentPageName = $iPort['menu']['peer_learning_portfolio'];

#MENU
$MenuArr = array();
$MenuArr[] = array($iPort['menu']['learning_portfolio'], "index.php");
$MenuArr[] = array($CurrentPageName, "");


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

// modify it to get useful content
$plp_list = $li_pf->GET_PEER_LEARNING_PORTFOLIO_LIST($StudentID);

$StudentID_list = "0";
if(!empty($plp_list)){
	foreach($plp_list as $value){
		$StudentID_list .= ",{$value['UserID']} ";
	}
}

# Generate error message
if(isset($result))
	$error_msg = $linterface->GET_SYS_MSG("",$ec_iPortfolio['iPortfolio_published'][$result]);

// Get Student List
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

//++++++++++++++++ START ++++++++++++

$studentHaveUpdate = 1;
$studentHaveNotUpdate  = 0;

$tmpSql = "create temporary  table temp_newLPForStudent
			(
			`StudentID` int(8)  NULL,
			`haveNew` tinyint(1) , student_publish_time datetime,
			KEY `StudentID` (`StudentID`),
			UNIQUE KEY `u_StudentID` (`StudentID`)
			)";

$lpf->db_db_query($tmpSql);

//insert record to temp table 
//only one student will have one record in table temp_newLPForStudent
//for ONE student has more than one LP, 
//If any LP 1) student_publish_time is larger than teacher_view_time or 2) student has publish the first time and teacher has not viewed , 
//it will set haveNew to $studentHaveUpdate. (ON DUPLICATE KEY UPDATE haveNew = if(haveNew < values(haveNew) , values(haveNew),haveNew))
//Teacher will see a "new" in the list
$insertCheckNewSql = "
		insert into 
			temp_newLPForStudent(StudentID,haveNew,student_publish_time)
		select 
			student_id,
			if(
				(UNIX_TIMESTAMP(student_publish_time)>UNIX_TIMESTAMP(teacher_view_time))
					or
				(student_publish_time is not null and teacher_view_time is null),
				{$studentHaveUpdate},
				{$studentHaveNotUpdate}
			) as 'haveUpdate', student_publish_time as 'time'
		from 
			".$lpf->course_db.".portfolio_tracking_last ORDER BY student_publish_time DESC 
		
		ON DUPLICATE KEY UPDATE 
		haveNew = if(haveNew < values(haveNew) , values(haveNew),haveNew)
	";
$lpf->db_db_query($insertCheckNewSql);


//Insert the record into temp db since the navigation bar counting problem
##Create the temp_peerlist
$tmpSql = "create temporary table temp_peerlist
			(
			`UserID` int(8)  NULL,
			`ClassTitle`  varchar(255), `ClassNumber`  int(8) , 
			`UserName`  varchar(255), notes_published datetime, 
			KEY `UserID` (`UserID`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$lpf->db_db_query($tmpSql);

$insertCheckNewSql = "
		INSERT INTO 
			temp_peerlist(UserID, ClassTitle, ClassNumber, UserName, notes_published)
          SELECT DISTINCT iu.UserID AS ID,
            ".libportfolio::GetYearClassTitleFieldByLang("yc.")." AS Title,
            iu.ClassNumber AS Number,
            ".getNameFieldByLang2("iu.")." AS Name, notes_published          
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          LEFT JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON  ycu.UserID = iu.UserID
          LEFT JOIN {$intranet_db}.YEAR_CLASS AS yc
            ON yc.YearClassID = ycu.YearClassID
          LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
            ON  iu.UserID = ps.UserID
          LEFT JOIN ".$lpf->course_db.".user_config AS uc
            ON  ps.CourseUserID = uc.user_id AND
                uc.notes_published IS NOT NULL       
		  LEFT JOIN ".$lpf->course_db.".web_portfolio as wp
			ON
				wp.web_portfolio_id = uc.web_portfolio_id    
          WHERE
            iu.RecordType = 2 AND
            iu.RecordStatus = 1 AND iu.UserID IN ($StudentID_list)
            $cond 
          GROUP BY
            iu.UserID 
	";

$lpf->db_db_query($insertCheckNewSql);

$orderby = (!empty($orderby))?$orderby:"UserID";

$sql = "SELECT * FROM temp_peerlist";
$LibTable->sql = $sql;
$LibTable->field_array = array($orderby);
$LibTable->order = "0";
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $LibTable->page_size = $numPerPage;
$LibTable->no_col = 10;
$LibTable->noNumber = true;


##dropdownlist

$html_orderby_list = <<<html
<select>
<option value='class'>Last Updated</option>
<option value='class'>class</option>
</select>
html;

//-- Start: SELECTION BOX FOR ORDER--//		
$valueNamePairArray = array();

$valueNamePairArray[] = array("UserName", $Lang['iPortfolio']['orderbyName']);
$valueNamePairArray[] = array("ClassTitle", $Lang['iPortfolio']['orderbyClass']);
$valueNamePairArray[] = array("notes_published", $Lang['iPortfolio']['orderbyLastPublishedDate']);

$htmlInSelectTag = " onchange='ch_order(this)' ";
$selectedValue = $orderby;
$isShowSelectAllOption = false;
$firstOptionName = "--{$Lang['iPortfolio']['orderby']}--";
//------------------------------------------
// Only when $firstOptionName is not set
// 0: -- $button_select --, 2: $i_general_NotSet, 3: $i_status_all
$firstOptionDefaultValue = 0;
//------------------------------------------
$quotingStyle = 1; // 1: value='', 2: value=\"\"
$html_orderby_list = getSelectByArray($valueNamePairArray,$htmlInSelectTag,$selectedValue,$isShowSelectAllOption,$firstOptionDefaultValue,$firstOptionName,$quotingStyle);
//-- End: SELECTION BOX --//

###
# Generate a class selection drop-down list
$lpf_ui = new libportfolio_ui();
//$t_yc_arr = $li_pf->GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST();
$t_yc_arr = $li_pf->GET_ALL_LEARNING_PORTFOLIO_CLASS_LIST();

if(count($t_yc_arr) > 0)
{
  
  for($i=0; $i<count($t_yc_arr); $i++)
  {
    $t_y_name = $t_yc_arr[$i][0];
    $t_yc_id = $t_yc_arr[$i][1];
    $t_yc_name = $t_yc_arr[$i][2];
  
    $yc_arr[$t_y_name][] = array($t_yc_id, $t_yc_name);
  }
  
  $PublishedLPClass = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($yc_arr, " onchange='ch_filter(this)'", $addional_cond, true);
}
else
{
	$PublishedLPClass = "<i>".$no_record_msg."</i>";
	$DisableSubmit = "DISABLED";
}
###

$linterface->LAYOUT_START();

?>
<script language="JavaScript">

// Change page to display student detail information
function jTO_LP(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "/home/portfolio/profile/learning_portfolio_student.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "Peer_view.php";
	document.form1.submit();
}

function ch_order(obj){
	var order = $(obj).val();
	
	$('#orderby').val(order);
	document.form1.submit();
}

function ch_filter(obj){
	var fl = $(obj).val();
	$('#addional_cond').val(fl);
	document.form1.submit();	
}

</script>
<FORM method='POST' name='form1' action='Peer_view.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_01.gif" width="17" height="37"></td>
					<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_03.gif">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_02.gif" class="page_title"><?=$CurrentPageName?></td>
							</tr>
						</table>
					</td>
					<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_04.gif" width="13" height="37"></td>
				</tr>
				  <tr>
					<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_05.gif" width="17" height="20"></td>
				  	<td valign="top" class="tab_table" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
				  	<!-- ===================================== Body Contents ============================= -->
				  		
				  		<table width="100%" border="0" cellspacing="0" cellpadding="5">
  							<tr>
                 				 <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?> <?=$PublishedLPClass?></td>
                 				 <td align="right" valign="bottom" class="thumb_list">
			  						<span class="tabletext">
			  							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
			  							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
			  						</span>
			  					</td>
  							</tr>
  							<tr>
                 				 <td>
                 				  <?=$html_orderby_list?>
			  					</td>
  							</tr>
  							
  						</table>
  						
  					<!-- ===================================== Thumbnail ============================= -->
  						
  						<table width="100%" border="0" cellspacing="0" cellpadding="0">
  													
	  						<tr>
							    <td class="student_thumb_content">
							   
							  		<?=$LibTable->displayStudentList_ThumbnailView(TRUE)?>
								    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
										<?php if ($LibTable->navigationHTML!="") { ?>
								            <tr class='tablebottom'>
								                  <td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
								            </tr>
										<?php } ?>
								      </table>
								     
							      </td>
							  </tr>
						 </table>
					<!-- =============================END of Thumbnail ============================== -->	 
				  	</td>
				  	<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_07.gif" width="13" height="37"></td>
				  </tr>	
				  <tr>
					<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_08.gif" width="17" height="17"></td>
					<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_09.gif" width="64" height="17"></td>
					<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_10.gif" width="13" height="17"></td>
				  </tr>
			</table>
		</td>
	</tr>
</table>
  <div id='PhotoLayer' style='position:absolute;height:130px;width:100px;visibility:hidden;'></div>

  <input type="hidden" name="DisplayType" value="<?=$DisplayType?>" />
  <input type="hidden" name="StudentID" />
  
  <input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
  <input type="hidden" name="order" value="<?= $order ?>">
  <input type="hidden" name="field" value="<?= $field ?>">
  <input type="hidden" name="page_size_change" />
  <input type="hidden" name="numPerPage" />
  <input type="hidden" id="addional_cond" name="addional_cond" value="<?=$addional_cond?>"/>
  <input type="hidden" id="orderby" name="orderby" value="<?=$orderby?>"/>
  
</FROM>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>