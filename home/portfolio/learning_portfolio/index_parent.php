<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("P");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

include_once($eclass_filepath.'/src/includes/php/lib-groups.php');

intranet_opendb();

$li_pf = new libpf_lp();

$StudentID = $ck_current_children_id;

# Resetting page number
switch($FieldChanged)
{
	case "ppage":
	case "page":
		break;
	case "division":
		$Page = 1;
		break;
	default:
		$Page = 1;
		$PPage = 1;
		break;
}

$luser = new libuser($StudentID);

# Set student photo in left menu
if(is_object($luser))
{
	$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
	$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);
}

// template for student page
$linterface = new interface_html("iportfolio_default3.html");
// set the current page title
$CurrentPage = "Parent_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

// modify it to get useful content
list($LeftContent, $LP_list) = $li_pf->GEN_LEARNING_PORTFOLIO_LIST(array($PageDivision, $Page), $StudentID);

# Generate error message
if(isset($result))
	$error_msg = $linterface->GET_SYS_MSG("",$ec_iPortfolio['iPortfolio_published'][$result]);

$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="/templates/2009a/js/iportfolio.js"></script>
<SCRIPT LANGUAGE="Javascript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "index_parent.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "index_parent.php";
	document.form1.submit();	
}

// Change pages
function jCHANGE_PPAGE(jParShift){
	var PPageSelection = document.getElementsByName("PPage");
	var OriginalIndex = PPageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PPageSelection[0].length)
	{
		PPageSelection[0].selectedIndex = PPageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PPageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PPageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "ppage";
	document.form1.action = "index_parent.php";
	document.form1.submit();	
}

</SCRIPT>

<FORM method='POST' name='form1' action='index_parent.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" class="tab_table" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
			<?=$error_msg?>
			<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tablegreenbottom">
				<tr>
					<td align="left" class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($LP_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($LP_list), $ec_iPortfolio['total_record'])?></td>
					<td align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="2">
										<tr align="center" valign="middle">
											<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp1" width="11" height="10" border="0" align="absmiddle" id="prevp1"></a> <span class="tabletext"> <?=$list_page?> </span></td>
											<td class="tabletext">
												<?=$li_pf->GEN_PAGE_SELECTION(count($LP_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
											</td>
											<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp1" width="11" height="10" border="0" align="absmiddle" id="nextp1"></a></td>
										</tr>
									</table>
								</td>
								<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
								<td>
									<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
										<tr>
											<td><?=$i_general_EachDisplay?></td>
											<td>
												<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'")?>
											</td>
											<td><?=$i_general_PerPage?></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<?=$LeftContent?>
		</td>
	</tr>
</table>

<input type="hidden" name="FieldChanged" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
