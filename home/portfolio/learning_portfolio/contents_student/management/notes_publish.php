<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_lp();

$StudentID = $_SESSION['UserID'];

$LP_info = $li_pf->GET_LEARNING_PORTFOLIO($WebPortfolioID, $StudentID);
$LP_pages_display = $li_pf->GEN_NOTES_FOR_PUBLISH($WebPortfolioID, $StudentID);

$luser = new libuser($StudentID);

# Set student photo in left menu
if(is_object($luser))
{
	$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
	$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);
}

// template for student page
$linterface = new interface_html("iportfolio_default3.html");
// set the current page title
$CurrentPage = "Student_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>


<script language="JavaScript">
function checkform(myObj){

	var anyChecked = false;
	for(var i=0; i<document.form1.elements.length; i++)
	{
		if(document.form1.elements[i].type == "checkbox" && document.form1.elements[i].checked)
		{
			anyChecked = true;
			break;
		}
	}

	var warn_msg = anyChecked ? "<?=$ec_warning['publish_content']?>" : "<?=$ec_warning['hide_content']?>";

	if (!confirm(warn_msg))
	{
		return false;
	}

	return true;
}

function viewPage(notes_id, page_no){
	newWindow("../viewpage.php?notes_id="+notes_id+"&pageNo="+page_no, 13);
}

// Trigger children checkboxes
function jTRIGGER_CHILDREN(jParObj, jParLevel, jParANo, jParBNo)
{
	CheckStatus = jParObj.checked;

	for(var i=0; i<document.form1.elements.length; i++)
	{
		if(document.form1.elements[i].name == jParObj.name)
		{
			if(jParLevel == "2")
			{
				if(document.form1.elements[i].a_no == jParANo)
					document.form1.elements[i].checked = CheckStatus;
			}
			else if(jParLevel == "3")
			{
				if(document.form1.elements[i].a_no == jParANo && document.form1.elements[i].b_no == jParBNo)
					document.form1.elements[i].checked = CheckStatus;
			}
		}
	}
}

function triggerChildren(obj, notes_id, total, enabled){
	var counter = total;
	var isHandling = false;
	for (var i=0; i<obj.elements.length; i++)
	{
		if (isHandling)
		{
			if (counter>0)
			{
				obj.elements[i].checked = enabled;
				counter --;
			} else
			{
				break;
			}
		}
		if (obj.elements[i].name=="notes_student_id[]" && obj.elements[i].value==notes_id)
		{
			isHandling = true;
		}
	}
}

// Trigger parent checkboxes
function triggerParents(obj, parent_notes_ids, enabled){
	if (enabled)
	{
		for (var i=0; i<obj.elements.length; i++)
		{
			if (obj.elements[i].name=="notes_student_id[]" && parent_notes_ids.indexOf("|"+obj.elements[i].value+"|")>=0)
			{
				obj.elements[i].checked = true;
			}
		}
	}
}
</script>

<? // ===================================== Body Contents ============================= ?>
<form name="form1" action="notes_publish_update.php" onSubmit="return checkform(this)">
<!-- start of programming contents -->

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" class="tab_table" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td align="center" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td class="navigation">
															<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
															<a href="../../index.php"><?=$iPort['menu']['learning_portfolio']?></a>
															<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
															<?=$LP_info['title']?>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="88%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
										<tr>
											<td>
													<?=$LP_pages_display?>
											</td>
										</tr>
										<tr>
											<td height="1" class="dotline"><img src="images/<?= $LAYOUT_SKIN ?>/10x10.gif" width="10" height="1"></td>
										</tr>
										<tr>
											<td align="right">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="tabletextremark">&nbsp;</td>
														<td align="center">
															<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_publish_iPortfolio?>" />
															<input type="reset" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_reset?>" />
															<input type="button" class="formbutton" onClick="self.location='../../index.php'"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>" />
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br />
									<br />
								</td>
							</tr>
							<tr>
								<td align="center" valign="top"><br></td>
							</tr>
						</table>
						<br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="WebPortfolioID" value="<?=$WebPortfolioID?>" />

		<!-- end of programming contents -->
</form>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
