<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

include_once($eclass_filepath.'/src/includes/php/lib-groups.php');

intranet_opendb();

$li_pf = new libpf_lp();

$StudentID = $_SESSION['UserID'];

$LP_info = $li_pf->GET_LEARNING_PORTFOLIO($WebPortfolioID, $StudentID);
$template_display = $li_pf->GEN_TEMPLATE_SELECTION_TABLE($LP_info['template_selected']);

/*
$nt = new notes_iPortfolio("", $web_portfolio_id);
$template_navigation_html = $nt->getTemplatesPageNavigation($pageNo, $web_portfolio_id);
$template_table_html = $nt->getTemplatesTable($pageNo, $web_portfolio_id);

$scheme_obj = $nt->getSchemeInfo($web_portfolio_id);
$scheme_obj = $scheme_obj[0];
*/
$luser = new libuser($StudentID);

# Set student photo in left menu
if(is_object($luser))
{
	$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
	$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);
}

// template for student page
$linterface = new interface_html("iportfolio_default3.html");
// set the current page title
$CurrentPage = "Student_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<script language="JavaScript">

function jSUBMIT_FORM(jParPublish){
	document.form1.Publish.value = jParPublish;
	
	for (i=0; i<document.form1.elements['StudentID[]'].length; i++)
	{
	    document.form1.elements['StudentID[]'].options[i].selected = true;
	}

	document.form1.submit();
}

/*
functions for select template
1. jSELECT_TEMPLATE
2. CompleteSelectTemplate
*/
function jSELECT_TEMPLATE(jParTemplate){

  xmlHttp_template = GetXmlHttpObject();
  if (xmlHttp_template == null)
  {
    alert ("Your browser does not support AJAX!");
    return;
  }

  // Modify Layer Content
  var url="../../../ajax/select_template_ajax.php";
  url=url+"?template=" + jParTemplate;
  url=url+"&WebPortfolioID=<?=$WebPortfolioID?>";
  url=url+"&StudentID=<?=$StudentID?>";
  url=url+"&sid="+Math.random();

  xmlHttp_template.onreadystatechange = CompleteSelectTemplate;
  xmlHttp_template.open("GET",url,true);
  xmlHttp_template.send(null);
}

function CompleteSelectTemplate(){
  if (xmlHttp_template.readyState==4)
  {
    var Template_HTML = xmlHttp_template.responseText;
    var TemplateLayer = document.getElementById("TemplateSelectDiv");
    TemplateLayer.innerHTML = Template_HTML;
  }
}

</script>

<form name="form1" method="post" action="notes_setting_update.php">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" class="tab_table" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_LP_06.gif">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td align="center" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td class="navigation">
															<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
															<a href="../../index.php"><?=$iPort['menu']['learning_portfolio']?></a>
															<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
															<a href="#"><?=$LP_info['title']?></a>
															<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
															<?=$i_general_settings?>
														</td>
													</tr>
												</table>
											</td>
											<td align="right">
												<span class="tabletext">
<!--
													<a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif" width="20" height="20" border="0"></a><a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0"></a><br />
-->
													<?=$ec_iPortfolio['last_update']?> : <?=$LP_info['modified']?> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"> <?=$ec_iPortfolio['publish_date']?> : <?=$LP_info['notes_published']?></span>
											</td>
										</tr>
									</table>
									<table width="88%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
													<tr>
														<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iPort["lp_skin"]?>:</span></td>
													</tr>
													<tr>
														<td align="center" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">
															<div id="TemplateSelectDiv">
																<?=$template_display?>
															</div>
														</td>
													</tr>
													<tr>
														<td valign="top" nowrap="nowrap">
															<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
																<tr>
																	<td valign="top" nowrap="nowrap"><span class="tabletext"><?=$button_public?>:</span></td>
																	<td width="80%" valign="top">
																		<span class="tabletext">
																			<input name="allow_review" type="radio" value="1" <?=($LP_info['allow_review']==1?"checked":"")?> /> <?=$i_general_yes?>
																			<input name="allow_review" type="radio" value="0" <?=($LP_info['allow_review']==0?"checked":"")?> /> <?=$i_general_no?>
																		</span>
																	</td>
																</tr>
																<tr valign="top">
																	<td valign="top" nowrap="nowrap" ><span class="tabletext"><?=$iPort["share_to"]?>:</span></td>
																	<td><label for="grading_honor" class="tabletext"></label>
																		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
																			<tr valign="top">
																				<td width="40%"><span class="tabletext"><?=$i_general_choose_student?></span></td>
																				<td width="10" nowrap="nowrap"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
																				<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$i_general_selected_students?></span></td>
																			</tr>
																			<tr>
																				<td valign="top" class="tablerow2">
																					<table width="100%" border="0" cellpadding="5" cellspacing="0">
																						<tr>
																							<td class="tabletext"><?=$i_general_from_class_group?></td>
																						</tr>
																						<tr>
																							<td class="tabletext"><input type="button" onClick="newWindow('pick_students.php?fieldname=StudentID[]', 9)" class="formsubbutton" value="<?=$button_select?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"></td>
																						</tr>
<!--
Todo: Share to group
																						<tr>
																							<td class="tabletext">or </td>
																						</tr>
																						<tr>
																							<td class="tabletext">by<strong> entering class and class number </strong></td>
																						</tr>
																						<tr>
																							<td class="tabletext"><input name="text3" type="text" class="tabletext" /></td>
																						</tr>
																						<tr>
																							<td class="tabletext">or </td>
																						</tr>
																						<tr>
																							<td class="tabletext">by <strong>entering student's name </strong></td>
																						</tr>
																						<tr>
																							<td class="tabletext"><input name="text32" type="text" class="tabletext" /></td>
																						</tr>
-->
																					</table>
																				<td valign="top" nowrap="nowrap">&nbsp;</td>
																				<td valign="top" nowrap="nowrap">
																					<table width="100%" border="0" cellpadding="3" cellspacing="0">
																						<tr>
																							<td>
																								<p>
																									<?=$li_pf->GEN_FRIEND_SELECTION_STUDENT($ck_intranet_user_id)?>
																								</p>
																							</td>
																						</tr>
																						<tr>
																							<td align="right"><input type="button" onClick="checkOptionRemove(document.form1.elements['StudentID[]'])" class="formsubbutton" value="<?=$button_remove?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<label for="grading_passfail" class="tabletext"></label>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
										</tr>
										<tr>
											<td align="right">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="right">
															<input type="button" class="formbutton" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="jSUBMIT_FORM(0)" />
															<input type="button" class="formbutton" value="<?=$button_save_and_publish_iPortfolio?>" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="jSUBMIT_FORM(1)" />
															<input type="button" class="formbutton" value="<?=$button_cancel?>" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="self.location='../../index.php'" />
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<br>
									<br>
								</td>
							</tr>
							<tr>
								<td align="center" valign="top"><br></td>
							</tr>
						</table>
						<br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="Publish" />
<input type="hidden" name="WebPortfolioID" value="<?=$WebPortfolioID?>" />
</form>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
