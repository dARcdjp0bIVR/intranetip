<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($eclass_filepath."/src/includes/php/lib-groups.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
intranet_opendb();

$ori_ck_user_id = $_SESSION["ck_user_id"];

preg_match_all("/([^\/:]*):([^\/:]*)\//U", $eclass_httppath, $arr);
$server_address = ($arr[1][0]=="") ? $_SERVER["SERVER_ADDR"] : $arr[1][0];
$server_port = ($arr[2][0]=="") ? $_SERVER["SERVER_PORT"] : $arr[2][0];      //eclass port (not intranetIP)

$li_pf = new libpf_lp();

for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{
  $t_user_id = $student_arr[$i];
  $t_course_user_id = $li_pf->IP_USER_ID_TO_EC_USER_ID($t_user_id);
  $_SESSION["ck_user_id"] = $t_course_user_id;

	if($publishAllPage)
	{
		// select all page ID of the LP of the student
		$sql = "SELECT notes_student_id ";
		$sql .= "FROM {$li_pf->course_db}.notes_student ";
		$sql .=  "WHERE web_portfolio_id = {$wp_id} AND user_id = {$t_course_user_id}";
		$notes_student_id = $li_pf->returnVector($sql);
	}
	else
	{
	  list($NotesArrL1, $NotesArrL2, $NotesArrL3) = $li_pf->GET_NOTES_FOR_PUBLISH($wp_id, $t_user_id);
	  
	  for($j=0, $j_max=count($NotesArrL1); $j<$j_max; $j++)
	  {
	    $a_no = $NotesArrL1[$j][1];
	  
	    if($NotesArrL1[$j][9] == "1")
	    {
	      $notes_student_id[] = $NotesArrL1[$j][0];
	    }
	    
	    if(count($NotesArrL2[$a_no]) > 0)
	  	{
	      for($j=0; $j<count($NotesArrL2[$a_no]); $j++)
	  		{
	  			$b_no = $NotesArrL2[$a_no][$j][2];
	  			
	  			if($NotesArrL2[$a_no][$j][9] == "1")
	        {
	          $notes_student_id[] = $NotesArrL2[$a_no][$j][0];
	        }
	        
	        if(count($NotesArrL3[$a_no][$b_no]) > 0)
	  			{
	          for($k=0; $k<count($NotesArrL3[$a_no][$b_no]); $k++)
	          {
	            if($NotesArrL3[$a_no][$b_no][$k][9] == "1")
	            {
	              $notes_student_id[] = $NotesArrL3[$a_no][$b_no][$k][0];
	            }
	          }
	  			}
	      }
	    } 
  	}
  }
  
  $LP_info = $li_pf->GET_LEARNING_PORTFOLIO($wp_id, $t_user_id);
  $allow_review = $LP_info['allow_review'];
  
  if(!empty($notes_student_id))
  {
    # build data
    $data = "ck_course_id=".$ck_course_id;
    $data .= "&ck_user_id=".$t_course_user_id;
    $data .= "&ck_room_type=".$ck_room_type;
    $data .= "&CourseUserID=".$t_course_user_id;
    $data .= "&StudentID=".$t_user_id;
    $data .= "&allow_review=".$allow_review;
    $data .= "&web_portfolio_id=".$wp_id;
    for($j=0, $j_max=count($notes_student_id); $j<$j_max; $j++)
    {
      $data .= "&notes_student_id[]=".$notes_student_id[$j];
    }
  
    # send data
    $fp = fsockopen($server_address, $server_port, $errno, $errstr, 10);
    if (!$fp) {
        echo "$errstr ($errno)<br />\n";
    } else {
      $out = "POST /inabox/src/ip2portfolio/contents_student/management/notes_republish_update.php HTTP/1.1\r\n";
      $out .= "Host: $server_address\r\n";
      $out .= "Content-type: application/x-www-form-urlencoded\r\n";
      $out .= "Content-length: ".strlen($data)."\r\n";
      $out .= "Connection: Close\r\n\r\n";
      $out .= $data."\r\n\r\n";
  
      fwrite($fp, $out);
  
      $contents = "";
      while (!feof($fp)) {
        $contents .= fread($fp, 8192);
      }
      
      list($head, $tail) = explode("\r\n\r\n", $contents);
      
      echo "=====================================<br />";
      echo $tail;
    }
    
    sleep(3);  //sleep for 2 seconds
  }

  unset($notes_student_id); 
}

$_SESSION["ck_user_id"] = $ori_ck_user_id;

intranet_closedb();
?>