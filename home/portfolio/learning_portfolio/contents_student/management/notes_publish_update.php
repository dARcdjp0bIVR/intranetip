<?php
/*
 * Editing by
 *
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';

$li_pf = new libpf_lp();

$CourseUserID = $li_pf->IP_USER_ID_TO_EC_USER_ID($_SESSION['UserID']);
$LP_info = $li_pf->GET_LEARNING_PORTFOLIO($WebPortfolioID, $_SESSION['UserID']);
$allow_review = $LP_info['allow_review'];

intranet_closedb();
?>
<html>
<body>
<form name="form1" method="post" action="<?=$HTTP_SSL.$eclass_httppath?>/src/ip2portfolio/contents_student/management/notes_publish_update.php">
	<input type="hidden" name="CourseUserID" value="<?=$CourseUserID?>" />
	<input type="hidden" name="StudentID" value="<?=$_SESSION['UserID']?>" />
	<input type="hidden" name="allow_review" value="<?=$allow_review?>" />

	<input type="hidden" name="web_portfolio_id" value="<?=$WebPortfolioID?>" />
<?php
	for($i=0; $i<count($notes_student_id); $i++)
		echo "<input type=\"hidden\" name=\"notes_student_id[]\" value=\"".$notes_student_id[$i]."\" />\n";
?>
</form>

<script language="JavaScript">
	document.form1.submit();
</script>
</body>
</html>
