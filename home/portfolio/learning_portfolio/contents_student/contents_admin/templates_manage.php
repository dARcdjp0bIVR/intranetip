<?php
//if ($page_size_change!="") { setcookie("ck_page_size", $numPerPage, 0, "", "", 0);}

/**
 * Change Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

// ck_student_user_id may make HTML editor switch to student mode, so unset it here.
unset($_SESSION['ck_student_user_id']);

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';

$li_pf = new libpf_lp();
$UserID = $_SESSION['UserID'];

# Check for iPortfolio admin user
if (!$li_pf->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

# Resetting page number
switch($FieldChanged)
{
	case "page":
	case "modify":
		break;
	case "division":
	default:
		$Page = 1;
		break;
}

list($lp_template_display, $lp_template_list) = $li_pf->GEN_LP_TEMPLATE_LIST(array($PageDivision, $Page), $Field, $Order);

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

$luser = new libuser($UserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

# Generate error message
if(isset($msg))
{
	if($msg == "delete_following_failed")
	{
		$FailedTitles = implode(",<br />",$li_pf->GET_TEMPLATE_TITLE($failedID));
		$error_msg = $linterface->GET_SYS_MSG("",${"i_con_msg_".$msg}."<br /><br />".$FailedTitles);
	}
	else
		$error_msg = $linterface->GET_SYS_MSG($msg);
}

/*
$li = new libdb();
if($order=="") $order=0;
if($field=="") $field=0;
$li = new libtable($field, $order, $pageNo);
$li->field_array = array("f.Title", "used_num", "f.IsSuspend", "f.DateModified");

$fieldname  = "CONCAT('<a class=\"link_a\" href=\"javascript:editTemplate(\'', CONCAT(IFNULL(f.VirPath, '/'), f.Title), '\')\" >',f.Title,'<a>'), ";
$fieldname .= "COUNT(n.notes_id) as used_num, ";
$fieldname .= "IF(f.IsSuspend=1, '$status_suspended', '$status_active'), ";
$fieldname .= "f.DateModified, ";
$fieldname .= "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\'file_ids[]\' value=\"', f.FileID,'\" />', if(COUNT(n.notes_id)=0, '', '*')) ";
$sql  = "SELECT $fieldname FROM eclass_file as f LEFT JOIN notes as n ON n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title) WHERE f.VirPath='/TEMPLATES/' AND f.Category='0' AND f.IsDir<>1 GROUP BY f.FileID";

// TABLE INFO
$li->sql = $sql;
$li->db = classNamingDB($ck_course_id);
$li->title = $ec_iPortfolio['template'];
$li->no_msg = $no_record_msg;
$li->page_size =
($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = 6;

$li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
$li->row_alt = array("#FFFFFF", "#EEEEEE");
$li->row_height = 40;
$li->plain_column = 1;
$li->plain_column_css = "link_a";

// TABLE COLUMN
$li->column_list .= "<td height='35' width='5%' align='center' bgcolor='#D6EDFA'  class='link_chi' style='border-bottom:2px solid #7BC4EA;'>#</td>\n";
$li->column_list .= "<td width='35%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(0, $ec_iPortfolio['weblog_title'], 1)."</td>\n";
$li->column_list .= "<td width='25%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(1, $ec_iPortfolio['number_of_portfolio_used'], 1)."</td>\n";
$li->column_list .= "<td width='10%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(2, $ec_iPortfolio['status'], 1)."</td>\n";

$li->column_list .= "<td width='20%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(3, $ec_iPortfolio['last_modified'], 1)."</td>\n";
$li->column_list .= "<td width='5%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->check("file_ids[]")."</td>\n";
$li->column_array = array(0,0,0,0,0,0);

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "main.php"),
					Array($ec_iPortfolio['manage_templates'], "")
					);
$template_width = "95%";

if($msg==7)
{
	// retreived the file title of files which could not be deleted;
	$sql = "SELECT
				f.Title
			FROM
				eclass_file as f
			WHERE
				f.FileID IN ($failedID)
			";
	$row = $li->returnVector($sql);

	$temp_string = implode(", ", $row);

	$con_msg = "<table><tr><td class='bodycolor2'>&nbsp;&nbsp;<span class='notice2'>\n";
	$con_msg .= $con_msg_del_fail."<br />&nbsp;&nbsp;&nbsp;<font color='#333355'>".$temp_string."</font></span></td></tr></table>";
}
*/

?>

<script language="javascript">

function addTemplate(){
	var url = "<?=$HTTP_SSL.$eclass_httppath?>/src/tool/misc/html_editor/editor_index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file&frManage=2&Field=<?=$Field?>&Order=<?=$Order?>";
	newWindow(url,21);
}

function previewTemplate(flink){
	newWindow("../../course/resources/files/open.php?fpath="+flink, 23);
}

function editTemplate(flink){
	newWindow("<?=$HTTP_SSL.$eclass_httppath?>/src/tool/misc/html_editor/editor_index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file&frManage=2&attachment="+flink+"&Field=<?=$Field?>&Order=<?=$Order?>", 21);
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "templates_manage.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;

	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "templates_manage.php";
	document.form1.submit();
}

// Suspend templates
function jSUSPEND_TEMPLATES(jParStatus){
	if(countChecked(document.form1,'file_ids[]') == 0)
	{
		alert("<?=$msg_check_at_least_one?>");
	}
	else
	{
		if(jParStatus == 1)
			msg = "<?=$ec_iPortfolio['suspend_template_confirm']?>";
		else
			msg = "<?=$ec_iPortfolio['active_template_confirm']?>";

		if(confirm(msg)){
			document.form1.Suspend.value = jParStatus;
			document.form1.action = "template_suspend.php";
			document.form1.submit();
		}
	}
}

// Delete templates
function jDELETE_TEMPLATES(){
	if(confirm("<?=$ec_iPortfolio['delete_template_confirm']?>")){
		document.form1.action = "template_remove.php";
		document.form1.submit();
	}
}

// Edit template
function jEDIT_TEMPLATE(){
	if(countChecked(document.form1,'file_ids[]') == 0)
		alert("<?=$msg_check_at_least_one?>");
	else if(countChecked(document.form1,'file_ids[]') > 1)
		alert("<?=$msg_check_at_most_one?>");
	else{
	  for(i=0; i<document.form1.elements.length; i++){
		  if (document.form1.elements[i].name=='file_ids[]' && document.form1.elements[i].checked)
		  {
		  	editTemplate(document.form1.elements[i].flink);
//			for B101
		  	document.form1.elements[i].checked = false;
		  }
	  }
	}
}

// Copy template
function jCOPY_TEMPLATE()
{
	if(countChecked(document.form1,'file_ids[]') == 0)
		alert("<?=$msg_check_at_least_one?>");
	else if(countChecked(document.form1,'file_ids[]') > 1)
		alert("<?=$msg_check_at_most_one?>");
	else{
	  for(i=0; i<document.form1.elements.length; i++){
		  if (document.form1.elements[i].name=='file_ids[]' && document.form1.elements[i].checked)
		  	newWindow("template_copy.php?file_id="+document.form1.elements[i].value+"&flink="+document.form1.elements[i].flink+"&Page="+document.form1.Page.value+"&PageDivision="+document.form1.PageDivision.value+"&Field=<?=$Field?>&Order=<?=$Order?>", 6);
	  }
	}
}

// Sort table by field
function jSORT_TABLE(jParField, jParOrder)
{
	document.form1.Field.value = jParField;
	document.form1.Order.value = jParOrder;

	document.form1.action = "templates_manage.php";
	document.form1.submit();
}
</script>

<? // ===================================== Body Contents ============================= ?>

<form name="form1" method="post" action="templates_manage.php">

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" bgcolor="#FFFFFF">
			<table width="98%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tab_underline">
									<div class="shadetabs">
										<ul>
											<li><a href="../../contents_admin/index_scheme.php"><strong><?=$ec_iPortfolio['portfolios']?></strong></a></li>
											<li class="selected"><a href="#"><?=$ec_iPortfolio['templates']?></a></li>
										</ul>
									</div>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$error_msg?></td>
							</tr>
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td><a href="#" class="contenttool" onClick="addTemplate()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?> </a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
										</tr>
									</table>
								</td>
								<td align="right" valign="bottom">&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="tabletextremark"><?=$ec_iPortfolio['used_template_cant_delete']?></td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap><a href="#" class="tabletool" onClick="jSUSPEND_TEMPLATES(0)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"> <?=$ec_iPortfolio['activate']?></a></td>
																	<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="#" class="tabletool" onClick="jSUSPEND_TEMPLATES(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_reject.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_deactivate?></a></td>
																	<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="#" class="tabletool" onClick="jCOPY_TEMPLATE()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_copy.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_copy?></a></td>
																	<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="#" class="tabletool" onClick="jDELETE_TEMPLATES()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_remove?></a></td>
																	<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="#" class="tabletool" onClick="jEDIT_TEMPLATE()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<?=$lp_template_display?>
								</td>
							</tr>
							<tr class="tablebottom">
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($lp_template_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($lp_template_list), $ec_iPortfolio['total_record'])?></td>
											<td align="right">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="2">
																<tr align="center" valign="middle">
																	<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp25" width="11" height="10" border="0" align="absmiddle" id="prevp25"></a> <span class="tabletext"> <?=$list_page?> </span></td>
																	<td class="tabletext">
																		<?=$li_pf->GEN_PAGE_SELECTION(count($lp_template_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
																	</td>
																	<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp25" width="11" height="10" border="0" align="absmiddle" id="nextp25"></a></td>
																</tr>
															</table>
														</td>
														<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
														<td>
															<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
																<tr>
																	<td><?=$i_general_EachDisplay?></td>
																	<td>
																		<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'", 10)?>
																	</td>
																	<td><?=$i_general_PerPage?></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="FieldChanged" />
<input type="hidden" name="Field" value="<?=$Field?>" />
<input type="hidden" name="Order" value="<?=$Order?>" />
<input type="hidden" name="Suspend" />
</form>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
