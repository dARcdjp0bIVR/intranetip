<?php

// Modifying by
/**
 * Change Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
 */

// Set the locale to UTF8, otherwise, escapeshellarg used in libfilesystem.php may not work correctly on UTF8 strings.
setlocale(LC_CTYPE, "UTF8", "en_US.UTF-8");

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lf = new libfilesystem();

$cwd = getcwd();

$max_direct_dl_size = 50;		# in MB

# Remove existing zip
$lf->file_remove($eclass_filepath."/files/portfolio_2_burn/".$YearClassID.".zip");

# Zip prepared files
$lf->file_zip($YearClassID, base64_decode($path), $eclass_filepath."/files/portfolio_2_burn");

if(filesize($eclass_filepath."/files/portfolio_2_burn/".$YearClassID.".zip") < $max_direct_dl_size * 1024 * 1024)
{
	# Output the zip file to browser
	output2browser(get_file_content($eclass_filepath."/files/portfolio_2_burn/".$YearClassID.".zip"), $YearClassID.".zip");
}
else
{
	chdir($cwd);
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");

	include_once($PATH_WRT_ROOT."includes/libpf-lp.php");

	$li_pf = new libpf_lp();

	$btn = "<input onClick='javascript:self.location.href=\"".$HTTP_SSL.$eclass_httppath."/files/portfolio_2_burn/".$YearClassID.".zip\"' name='button' type=button value='".$i_Files_ClickToDownload."' class='formbutton'>&nbsp;";
	$btn .= "<input onClick='javascript:self.location.href=\"prepare_CDburning.php\"' name='button' type=button value='".$ec_iPortfolio['prepare_other_class_cd_burning']."' class='formbutton'>";

	$result_display = "<br /><br />";
	$result_display .= "<table border='0' cellpadding='8' cellspacing='0' width='80%'>";
	$result_display .= "<tr><td align='center'>".$btn."</td></tr>\n";
	$result_display .= "</table>\n";

	// template for teacher page
	$linterface = new interface_html();
	// set the current page title
	$CurrentPage = "Teacher_BurningCD";
	$CurrentPageName = $iPort['menu']['prepare_cd_rom_burning'];

	$luser = new libuser($UserID);

	### Title ###
	$TAGS_OBJ[] = array($CurrentPageName,"");
	$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

	$linterface->LAYOUT_START();

	echo $result_display;

	$linterface->LAYOUT_STOP();
}
?>
