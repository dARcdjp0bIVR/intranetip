<?php

?>
<script language="javascript">
$(document).ready( function() {	
	// Focus the first element
	$('input.HourTb', 'form#form1').each( function() {
		$(this).focus();
		return false;
	});
});

function js_Go_Back_Registered_Event_List() {
	$('input#task', 'form#form1').val('list_applied_event');
	$('form#form1').submit();
}

function js_Go_Back_Select_Event_List() {
	$('input#task', 'form#form1').val('add_event');
	$('form#form1').submit();
}

function js_Submit() {
	var jsCanSubmit = true;
	
	$('div.WarningDiv', 'form#form1').hide();
	
	// Hours
	$('input.HourTb', 'form#form1').each( function() {
		var jsThisValue = Trim($(this).val());
		
		// Blank Checking
		if (jsThisValue == '') {
			var jsThisEventID = js_Get_EventID_By_ElementID($(this).attr('id'));
			$('div#HourBlankWarningDiv_' + jsThisEventID, 'form#form1').show();
			
			if (jsCanSubmit) {	// focus the first invalid element only
				$(this).focus();
			}
			
			jsCanSubmit = false;
		}
		
		// Positive Number Checking
		if (isNaN(jsThisValue) || parseFloat(jsThisValue) < 0) {
			var jsThisEventID = js_Get_EventID_By_ElementID($(this).attr('id'));
			$('div#HourMustBePositiveWarningDiv_' + jsThisEventID, 'form#form1').show();
			
			if (jsCanSubmit) {	// focus the first invalid element only
				$(this).focus();
			}
			
			jsCanSubmit = false;
		}
	});
	
	// Role
	$('input.RoleTb', 'form#form1').each( function() {
		var jsThisValue = Trim($(this).val());
		
		if (jsThisValue == '') {
			var jsThisEventID = js_Get_EventID_By_ElementID($(this).attr('id'));
			$('div#RoleBlankWarningDiv_' + jsThisEventID , 'form#form1').show();
			
			if (jsCanSubmit) {	// focus the first invalid element only
				$(this).focus();
			}
			
			jsCanSubmit = false;
		}
	});
	
	// Achievement
	$('textarea.AchievementTa', 'form#form1').each( function() {
		var jsThisValue = Trim($(this).val());
		
		if (jsThisValue == '') {
			var jsThisEventID = js_Get_EventID_By_ElementID($(this).attr('id'));
			$('div#AchievementBlankWarningDiv_' + jsThisEventID , 'form#form1').show();
			
			if (jsCanSubmit) {	// focus the first invalid element only
				$(this).focus();
			}
			
			jsCanSubmit = false;
		}
	});
	
	// Teacher PIC
	$('select.TeacherPICSel', 'form#form1').each( function() {
		var jsThisValue = Trim($(this).val());
		
		if (jsThisValue == '') {
			var jsThisEventID = js_Get_EventID_By_ElementID($(this).attr('id'));
			$('div#TeacherPICBlankWarningDiv_' + jsThisEventID , 'form#form1').show();
			
			if (jsCanSubmit) {	// focus the first invalid element only
				$(this).focus();
			}
			
			jsCanSubmit = false;
		}
	});
	
	if (jsCanSubmit) {
		Block_Document();
		js_Disable_Button('SubmitBtn');
		js_Disable_Button('BackBtn');
		js_Disable_Button('CancelBtn');
		
		$('input#task', 'form#form1').val('ajax');
		$('input#script', 'form#form1').val('save_event_user');
		var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
		
		$.ajax({  
			type: 'POST',
			url: 'index.php',
			data: jsSubmitString,  
			success: function(data) {
				if (data != '' && parseInt(data) > 0) {
					window.location = 'index.php?ReturnMsgKey=RecordSaveSuccess';
				}
				else {
					js_Enable_Button('SubmitBtn');
					js_Enable_Button('BackBtn');
					js_Enable_Button('CancelBtn');
					UnBlock_Document();
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordSaveUnSuccess']?>');
					Scroll_To_Top();
				}
			} 
		});
	}
}

function js_Get_EventID_By_ElementID(jsElementID) {
	var jsArr = jsElementID.split('_');
	return jsArr[1];
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php" onsubmit="return false;">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$h_toolBar?>
			<br style="clear:both" />
		</div>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_board">
		<?=$h_actionButton?>
		<?=$h_detailsTable?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$h_submitBtn?>
			<?=$h_backBtn?>
			<?=$h_cancelBtn?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="StudentID" name="StudentID" value="<?=$_SESSION['UserID']?>" />
</form>
<br/>