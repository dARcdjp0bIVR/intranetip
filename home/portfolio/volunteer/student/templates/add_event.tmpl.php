<?php

?>
<script language="javascript">
$(document).ready( function() {	

});

function js_Go_Back_Registered_Event_List() {
	$('input#task', 'form#form1').val('list_applied_event');
	$('form#form1').submit();
}

function js_Add_Event() {
	if ($('input.EventIDChk:checked', 'form#form1').length == 0) {
		alert('<?=$Lang['iPortfolio']['VolunteerArr']['jsWarningArr']['SelectEvent']?>');
	}
	else {
		$('input#task', 'form#form1').val('fill_event_details');
		$('form#form1').submit();
	}
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php" onsubmit="return false;">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<br style="clear:both" />
		</div>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_board">
		<?=$h_actionButton?>
		<?=$h_eventTable?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$h_addBtn?>
			<?=$h_cancelBtn?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
</form>
<br/>