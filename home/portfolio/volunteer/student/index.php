<?
# modifying :  

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer.php");

intranet_auth();
intranet_opendb();


//RECEIVE VARIABLE
$task		= trim($task);
$task 		= ($task == "")? "list_applied_event": $task;
$script		= trim($script);// for ajax

$lpf = new libportfolio2007();
$lpf_volunteer = new libpf_volunteer();


$linterface = new interface_html("iportfolio_default.html");
$CurrentPage = "Student_Volunteer";
$CurrentPageName = $Lang['iPortfolio']['VolunteerArr']['Volunteer'];


### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['VolunteerArr']['Volunteer']);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();


switch($task)
{
  case "ajax":
  	$task_script = "../ajax/" . str_replace("../", "", $script) . ".php";
    
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			//$linterface->LAYOUT_START();
			echo "error! task not find (task_script = $task_script)<br/>";
			//$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		$task = str_replace("../", "", $task);
		$task_script = "task/" . $task . ".php";
		$template_script = "templates/" . $task . ".tmpl.php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
?>