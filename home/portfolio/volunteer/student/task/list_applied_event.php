<?php
// Modifying:
/*
 * Modification Log:
 */

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ipf_volunteer_student_applied_event_list_page_size", "numPerPage");
$arrCookies[] = array("ck_ipf_volunteer_student_applied_event_list_page_number", "pageNo");
$arrCookies[] = array("ck_ipf_volunteer_student_applied_event_list_order", "order");
$arrCookies[] = array("ck_ipf_volunteer_student_applied_event_list_field", "field");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_ipf_volunteer_student_applied_event_list_page_size = '';
}
else {
	updateGetCookies($arrCookies);
}


### Navigation 
$NavigationArr = array();
$NavigationArr[] = array($Lang['iPortfolio']['VolunteerArr']['AppliedEvent']);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Toolbar
$h_addBtn = $linterface->GET_LNK_ADD("javascript:js_Apply_Event();", $Lang['Btn']['Add']);
$h_searchBox = $linterface->Get_Search_Box_Div('Keyword', '', 'onkeypress="js_Check_Searching(event);"');	// keyword initiated by JS


### Table Action Button
$ActionButtonArr = array();
$ActionButtonArr[] = array('edit', 'javascript:js_Edit_Applied_Event();');
$ActionButtonArr[] = array('delete', 'javascript:js_Delete_Applied_Event();');
$h_actionButton = $linterface->Get_DBTable_Action_Button_IP25($ActionButtonArr);


$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>