<?php
// Modifying By:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_event_user.php");

$RecordIDArr = IntegerSafe($_POST['RecordIDArr']);
$numOfRecord = count((array)$RecordIDArr);

$SuccessArr = array();
for ($i=0; $i<$numOfRecord; $i++) {
	$_recordID = $RecordIDArr[$i];
	$_objEventUser = new libpf_volunteer_event_user($_recordID);
	$_result = $_objEventUser->approve_record();
	
	$SuccessArr[] = ($_result > 0)? true : false;
}

echo (in_array(false, (array)$SuccessArr))? '0' : '1';
?>