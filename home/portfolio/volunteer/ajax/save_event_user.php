<?php
// Modifying By:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_event_user.php");

$StudentID = trim(urldecode(stripslashes($_POST['StudentID'])));
$StudentEventInfoArr = $_POST['StudentEventInfoArr'];

if ($ck_memberType=="S") {
	if ($StudentID != $_SESSION['UserID']) {
		echo false;
		die();
	}
}

$CurStudentEventInfoArr = $lpf_volunteer->getStudentAppliedEventInfo($StudentID);
$CurStudentEventAssoArr = BuildMultiKeyAssoc($CurStudentEventInfoArr, array('UserID', 'EventID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);


$SuccessArr = array();
foreach ((array)$StudentEventInfoArr as $_eventID => $thisStudentEventInfoArr) {
	$_recordID = $CurStudentEventAssoArr[$StudentID][$_eventID]['RecordID'];
	$_hours = $StudentEventInfoArr[$_eventID]['Hours'];
	$_role = trim(urldecode(stripslashes($StudentEventInfoArr[$_eventID]['Role'])));
	$_achivement = trim(urldecode(stripslashes($StudentEventInfoArr[$_eventID]['Achievement'])));
	$_teacherPIC = $StudentEventInfoArr[$_eventID]['TeacherPIC'];
	
	$_objEventUser = new libpf_volunteer_event_user($_recordID);
	$_objEventUser->setUserID($StudentID);
	$_objEventUser->setEventID($_eventID);
	$_objEventUser->setHours($_hours);
	$_objEventUser->setRole($_role);
	$_objEventUser->setAchievement($_achivement);
	$_objEventUser->setTeacherPIC($_teacherPIC);
	$_objEventUser->setComeFrom($lpf_volunteer->getComeFromCode_StudentInput());
	$_objEventUser->setApproveStatus($lpf_volunteer->getEventUserApproveStatusCode_Pending());
	$_objEventUser->setApprovedBy('null');
	$_objEventUser->setProcessDate('null');
			
	$_savedRecordID = $_objEventUser->save();
	$SuccessArr[] = ($_savedRecordID != '' && $_savedRecordID > 0)? true : false;
}

echo (in_array(false, (array)$SuccessArr))? '0' : '1';
?>