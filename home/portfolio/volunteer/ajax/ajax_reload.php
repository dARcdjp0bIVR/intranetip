<?php
# Get data
$action = stripslashes($_REQUEST['action']);

if ($action == 'Reload_Event_List_Teacher_View_DBTable') {
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
	
	$field = trim(stripslashes($_POST['field']));
	$order = trim(stripslashes($_POST['order']));
	$pageNo = trim(stripslashes($_POST['pageNo']));
	$Keyword = trim(stripslashes($_POST['Keyword']));
	
	$lpf_volunteer_ui = new libpf_volunteer_ui();
	echo $lpf_volunteer_ui->getEventListTeacherViewDBTable($field, $order, $pageNo, $Keyword);
}
else if ($action == 'Reload_Applied_Event_List_Student_View_DBTable') {
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
	
	$StudentID = IntegerSafe(trim(stripslashes($_POST['StudentID'])));
	$field = trim(stripslashes($_POST['field']));
	$order = trim(stripslashes($_POST['order']));
	$pageNo = trim(stripslashes($_POST['pageNo']));
	$Keyword = trim(stripslashes($_POST['Keyword']));
	
	$lpf_volunteer_ui = new libpf_volunteer_ui();
	echo $lpf_volunteer_ui->getAppliedEventListStudentViewDBTable($StudentID, $field, $order, $pageNo, $Keyword);
}
else if ($action == 'Reload_Event_Approval_List_Teacher_View_DBTable') {
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_ui.php");
	
	$field = trim(stripslashes($_POST['field']));
	$order = trim(stripslashes($_POST['order']));
	$pageNo = trim(stripslashes($_POST['pageNo']));
	$Keyword = trim(stripslashes($_POST['Keyword']));
	$ApproveStatus = trim(stripslashes($_POST['ApproveStatus']));
	$ProgramType = trim(stripslashes($_POST['ProgramType']));
	
	$lpf_volunteer_ui = new libpf_volunteer_ui();
	echo $lpf_volunteer_ui->getEventApprovalListTeacherViewDBTable($field, $order, $pageNo, $Keyword, $ApproveStatus, $ProgramType);
}
?>