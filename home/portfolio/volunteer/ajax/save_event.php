<?php
// Modifying By:
/*
 * Modification Log:
 */
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer_event.php");

$EventID = $_POST['EventID'];
$Code = trim(urldecode(stripslashes($_POST['Code'])));
$Title = trim(urldecode(stripslashes($_POST['Title'])));
$StartDate = $_POST['StartDate'];
$Organization = trim(urldecode(stripslashes($_POST['Organization'])));

$objEvent = new libpf_volunteer_event($EventID);
$objEvent->setCode($Code);
$objEvent->setTitle($Title);
$objEvent->setStartDate($StartDate);
$objEvent->setOrganization($Organization);
$objEvent->setComeFrom($lpf_volunteer->getComeFromCode_TeacherInput());

$SavedEventID = $objEvent->save();

echo ($SavedEventID > 0)? '1' : '0';
?>