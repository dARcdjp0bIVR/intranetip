<?php
## Using By : 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/volunteer/libpf_volunteer.php");


intranet_auth();
intranet_opendb();


$task = trim($task);
$task = ($task == "")? "list_event_approval": $task;

$lpf = new libpf_slp();
$lpf_volunteer = new libpf_volunteer();
$linterface = new interface_html();
$CurrentPage = "Teacher_Volunteer";

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);


switch ($task) {
	case 'list_event_approval':
		$CurrentTabIndex = IPF_CFG_VOLUNTEER_TEACHER_MGMT_APPROVAL;
		break;
	case 'list_event':
	case 'edit_event':
		$CurrentTabIndex = IPF_CFG_VOLUNTEER_TEACHER_MGMT_EVENT;
		break;
	default:
		$CurrentTabIndex = IPF_CFG_VOLUNTEER_TEACHER_MGMT_APPROVAL;
}
$TAGS_OBJ = libpf_tabmenu::getVolunteerTeacherTopTags($CurrentTabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");


switch($task){
  case "ajax":
    $task_script = "../ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		//$task_script = "task/" . str_replace("../", "", $task) . ".php";
		$task_script = "task/" . $task . ".php";
		$template_script = "templates/" . $task . ".tmpl.php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
exit();
?>