<?
echo $h_msg_row;
?>
<script language="javascript">
$(document).ready( function() {	
	$('input#Code', 'form#form1').focus();
});

function js_Go_Back() {
	window.location = 'index.php';
}

function js_Go_Submit() {
	var jsCanSubmit = true;
	$('div.WarningDiv' , 'form#form1').hide();
	
	var jsCode = Trim($('input#Code').val());
	$.post(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_validate',
			Action: 'Check_EventCode_Valid',
			InputValue: jsCode,
			ExcludeEventID: '<?=$EventID?>'
		},
		function(ReturnData) {
			if (ReturnData == "1") {
				var OtherFieldValid = js_Other_Form_Check();
				if (OtherFieldValid == false) {
					jsCanSubmit = false;
				}
			}
			else {
				$('div#CodeInUseWarningDiv' , 'form#form1').show();
				jsCanSubmit = false;
				js_Other_Form_Check();
			}
			
			if (jsCanSubmit) {
				Block_Document();
				js_Disable_Button('SubmitBtn');
				js_Disable_Button('CancelBtn');
				
				$('input#task', 'form#form1').val('ajax');
				$('input#script', 'form#form1').val('save_event');
				var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
				
				$.ajax({  
					type: 'POST',
					url: 'index.php',
					data: jsSubmitString,  
					success: function(data) {
						if (data != '' && parseInt(data) > 0) {
							window.location = 'index.php?task=list_event&ReturnMsgKey=RecordSaveSuccess';
						}
						else {
							js_Enable_Button('SubmitBtn');
							js_Enable_Button('CancelBtn');
							UnBlock_Document();
							Get_Return_Message('<?=$Lang['General']['ReturnMessage']['RecordSaveUnSuccess']?>');
							Scroll_To_Top();
						}
					} 
				});
			}
		}
	);
}

function js_Other_Form_Check() {
	var jsCanSubmit = true;
	var jsCode = Trim($('input#Code').val());
	
	if (jsCode == '') {
		$('div#CodeWarningDiv' , 'form#form1').show();
		jsCanSubmit = false;
	}
	
	if (!valid_code(jsCode)) {
		$('div#CodeMustBeginWithLetterWarningDiv' , 'form#form1').show();
		jsCanSubmit = false;
	}
	
	if (Trim($('input#Title').val()) == '') {
		$('div#TitleWarningDiv' , 'form#form1').show();
		jsCanSubmit = false;
	}
	
	if (Trim($('input#StartDate').val()) == '') {
		$('div#StartDateWarningDiv', 'form#form1').show();
		jsCanSubmit = false;
	}
	
	if ($('span#DPWL-StartDate', 'form#form1').is(':visible')) {
		jsCanSubmit = false;
	}
	
	return jsCanSubmit;
}

</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<br style="clear:both" />
		</div>
	</div>
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_board">
		<table class="form_table_v30">
			<tbody>
				<tr>
					<td class="field_title"><?=$linterface->RequiredSymbol().$Lang['General']['Code']?></td>
					<td>
						<input class="textboxtext" id="Code" name="Code" value="<?=intranet_htmlspecialchars($Code)?>" onkeypress="limitText(this, '<?=$CodeMaxLength?>')">
						<?=$linterface->Get_Form_Warning_Msg('CodeWarningDiv', $Lang['General']['JS_warning']['InputCode'], $Class='WarningDiv')?>
						<?=$linterface->Get_Form_Warning_Msg('CodeInUseWarningDiv', $Lang['General']['JS_warning']['CodeIsInUse'], $Class='WarningDiv')?>
						<?=$linterface->Get_Form_Warning_Msg('CodeMustBeginWithLetterWarningDiv', $Lang['General']['JS_warning']['CodeMustBeginWithLetter'], $Class='WarningDiv')?>
					</td>
				</tr>
				<tr>
					<td class="field_title"><?=$linterface->RequiredSymbol().$Lang['General']['Name']?></td>
					<td>
						<input class="textboxtext" id="Title" name="Title" value="<?=intranet_htmlspecialchars($Title)?>" onkeypress="limitText(this, '<?=$TitleMaxLength?>')">
						<?=$linterface->Get_Form_Warning_Msg('TitleWarningDiv', $Lang['General']['JS_warning']['InputName'], $Class='WarningDiv')?>
					</td>
				</tr>
				<tr>
					<td class="field_title"><?=$linterface->RequiredSymbol().$Lang['General']['StartDate']?></td>
					<td>
						<?=$linterface->GET_DATE_PICKER("StartDate", $StartDate)?>
						<?=$linterface->Get_Form_Warning_Msg('StartDateWarningDiv', $Lang['General']['JS_warning']['SelectDate'], $Class='WarningDiv')?>
					</td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['iPortfolio']['VolunteerArr']['PartnerOrganization']?></td>
					<td>
						<input class="textboxtext" id="Organization" name="Organization" value="<?=intranet_htmlspecialchars($Organization)?>" onkeypress="limitText(this, '<?=$OrganizationMaxLength?>')">
					</td>
				</tr> 
			</tbody>
		</table>
		<?=$linterface->MandatoryField()?>
	</div>
	<br style="clear:both" />
	
	<div class="edit_bottom">
		<p class="spacer"></p>
        <?=$h_submitBtn?>
        <?=$h_cancelBtn?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="EventID" name="EventID" value="<?=$EventID?>" />
</form>
<br/>