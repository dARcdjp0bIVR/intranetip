<?php
echo $linterface->Include_Cookies_JS_CSS();
?>
<script language="javascript">
var jsKeywordCookiesKey = "ck_ipf_volunteer_event_list_Keyword";

var arrCookies = new Array();
arrCookies[arrCookies.length] = jsKeywordCookiesKey;

var jsField = '<?=$field?>';
var jsOrder = '<?=$order?>';
var jsPageNo = '<?=$pageNo?>';
var jsKeyword = '<?=$Keyword?>';


$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	var jsTempKeyword = $.cookies.get(jsKeywordCookiesKey);
	jsTempKeyword = (jsTempKeyword==null || jsTempKeyword=='undefined')? '' : jsTempKeyword;
	$('input#Keyword', 'form#form1').val(jsTempKeyword).focus();
	
	Blind_Cookies_To_Object();
	
	js_Reload_DBTable(1);
});

function js_New_Event() {
	$('input#task').val('edit_event');
	$('form#form1').submit();
}

function js_Edit_Event() {
	checkEdit2(document.getElementById('form1'), 'EventIDArr[]', 'js_Go_Edit_Event()');
}

function js_Go_Edit_Event() {
	$('input#EventID').val($('input[name="EventIDArr\\[\\]"]:checked').val());
	$('input#task').val('edit_event');
	$('form#form1').submit();
}

function js_Delete_Event() {
	checkRemove2(document.getElementById('form1'), 'EventIDArr[]', 'js_Go_Delete_Event()');
}

function js_Go_Delete_Event() {
	$('input#task').val('ajax');
	$('input#script').val('delete_event');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: 'POST',
		url: 'index.php',
		data: jsSubmitString,  
		success: function(data) {
			if (data != '' && parseInt(data) > 0) {
				js_Reload_DBTable();
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
			}
			else {
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnSuccess']?>');
			}
			
			Scroll_To_Top();
		} 
	});
}

function js_Check_Searching(e) {
	if (Check_Pressed_Enter(e)) {
		js_Reload_DBTable();
	}
}

function js_Reload_DBTable(jsInitialize) {
	jsInitialize = jsInitialize || 0;
	if (jsInitialize == 1) { 
		// if first load of the table => follow current setting => do nth
	}
	else {
		// not intialize => changed filtering => start from first page
		jsPageNo = 1;
	}
	
	
	jsKeyword = $('input#Keyword', 'form#form1').val();
	$.cookies.set('ck_ipf_volunteer_event_list_Keyword', jsKeyword);
		
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Reload_Event_List_Teacher_View_DBTable',
			field: jsField,
			order: jsOrder,
			pageNo: jsPageNo,
			Keyword: jsKeyword
		},
		function(ReturnData) {
			
		}
	);
}
</script>

<br />
<form id="form1" name="form1" method="POST" action="index.php" onsubmit="return false;">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$h_newBtn?>
			<br style="clear:both" />
		</div>
		<?=$h_searchBox?>
	</div>
	<br style="clear:both" />
	
	<?=$h_navigation?>
	<br style="clear:both" />
	
	<div class="table_board">
		<?=$h_actionButton?>
		<div id="DBTableDiv">
			<?=$h_dbtable?>
		</div>	
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="EventID" name="EventID" value="" />
</form>
<br/>