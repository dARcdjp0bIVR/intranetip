<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

// $ck_course_id = 1274; // by eva temporaly

$li = new libdb();
$nt = new notes();
$ln = new notes_iPortfolio();
$group_permission = $ln->getGroupPermission($web_portfolio_id);
#set db 1274 temporarily because session not set
$li -> db = $eclass_prefix."c".$ck_course_id;

$notes_id = explode('\,', $ids);
for ($i=0;$i<sizeof($notes_id);$i++)
{
	$x = $notes_id[$i];

	$sql = "DELETE FROM my_notes WHERE notes_id = $x";
	$li->db_db_query($sql);

	$sql = "DELETE FROM notes_activity WHERE notes_id = $x";
	$li->db_db_query($sql);

	$sql = "DELETE FROM notes WHERE notes_id = $x";
	$li->db_db_query($sql);

	$sql = "DELETE FROM grouping_function WHERE function_type='NOTE' AND function_id = $x";
	$li->db_db_query($sql);

	# remove assignments related to the notes
	$sql = "DELETE FROM assignment WHERE worktype IN (8,9) AND parent_id=$x ";
	$li->db_db_query($sql);
}
$nt->buildStatus();

# if in used, update the notes_update_tracking table
if($group_permission>=0)
{
	$ln->updatePublishedNotesChange($web_portfolio_id);
}

intranet_closedb();
header("Location: organize.php?web_portfolio_id=$web_portfolio_id&msg=3");
?>
