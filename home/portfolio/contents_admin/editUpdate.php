<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

intranet_auth();
intranet_opendb();

// $ck_course_id = 1274; 

$title = HTMLtoDB($title);
$description = HTMLtoDB($description);
$status = htmlspecialchars($status);

$starttime = (trim($starttime)!="" && $status==1) ? "'".$starttime." $sh:$sm:00'" : "NULL";
$endtime = (trim($endtime)!="" && $status==1) ? "'".$endtime." $eh.$em:59'" : "NULL";
$url = ($linktype==1) ? HTMLtoDB($template_file) : "";
if ((isset($linktype) && $url=="") || $record_type=="WEBLOG" || $record_type=="FORM" || $record_type=="ACT" || $record_type=="AWARD")
{
	$url = "http://";
}

// update if submitted from edit.php
$li = new libdb();
if ($title!="" && $notes_id!="")
{
	$sql = "UPDATE ".$eclass_prefix."c".$ck_course_id.".notes SET title='$title', url='$url', status='$status', description='$description',
			starttime=$starttime, endtime=$endtime, modified=now() WHERE notes_id=$notes_id ";
			//debug_r($sql);
	$li->db_db_query($sql);
	
	$ln = new notes_iPortfolio();
	# if in used, update the notes_update_tracking table
	if($ln->getGroupPermission($web_portfolio_id)>=0)
	{
		$ln->updatePublishedNotesChange($web_portfolio_id);
	}
}

$li = new notes();
$li->buildStatus();

intranet_closedb();

?>

<script language="javascript">
parent.treeframe.location.reload();
parent.basefrm.location = "navbar.php?notes_id=<?=$notes_id?>";
</script>
