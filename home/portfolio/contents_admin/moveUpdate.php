<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

$msg = 6;	// default result
// $ck_course_id = 1274; // by eva temporaly
if ($operator!="" && $notes_id!="" && $web_portfolio_id!="")
{
	$li = new notes_iPortfolio();
	$idList = $li->getGroupNotes($web_portfolio_id);
	$li->notes2($idList);

	# verify the operation and get the note ($chapter) first
	list($m, $chapter) = $li->getReferenceNotes($notes_id, $operator);

	if ($m>=0 && $chapter>=0)
	{
		# get notes to be moved
		$ids = $li->getSelectTree(array($notes_id), true, "VALUES_ONLY");

		# process the operation
		if ($m==0)
		{
			$li->moveAfter($ids, $chapter);
		}
		if ($m==1)
		{
			$li->moveBelow($ids, $chapter);
		}
		if ($m==3)
		{
			$li->moveAfter($ids, $chapter);
			unset($li->documents);
			$idList = $li->getGroupNotes($web_portfolio_id);
			$li->notes2($idList);
			$ids = $li->getSelectTree(array($chapter), true, "VALUES_ONLY");
			$li->moveAfter($ids, $notes_id);
		}
		//if($m == 2) $li->swapChild($ids, $chapter);

		$li->buildStatus();
		$msg = 2;
		
		// if in used, update the notes_update_tracking table
		if($li->getGroupPermission($web_portfolio_id)>=0)
		{
			$li->updatePublishedNotesChange($web_portfolio_id);
		}
	}
}


intranet_closedb();
header("Location: organize.php?web_portfolio_id=$web_portfolio_id&msg=$msg");
?>
