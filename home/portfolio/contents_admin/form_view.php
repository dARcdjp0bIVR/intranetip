<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../../system/settings/lang/'.$ck_default_lang.'.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();
head(11);

$li = new libdb();
$sql = "SELECT assignment_id, sheettype, answersheet FROM assignment WHERE parent_id=$notes_id AND worktype=8 ";
$row = $li->returnArray($sql);
$form_obj = $row[0];

?>


<link href='../../includes/css/style_iPortfolio_purple.css' rel='stylesheet' type='text/css'>


<!--
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td colspan="3"><img src="<?=$sr_image_path?>/spacer.gif" width="10" height="10"></td>
</tr>
<tr >
<td background="<?=$sr_image_path?>/Border/dot_Line.gif" height="9" colspan="3"><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>
      <br>
-->
      
<form name="ansForm" method="post" action="update.php">
	<input type=hidden name="qStr" value="<?=$form_obj["answersheet"]?>">
	<input type=hidden name="aStr" value="">
</form>
<script language="javascript" src="../../includes/js/layer.js"></script>
<script language="javascript" src="../../includes/js/online_form_edit.js"></script>


<script language="Javascript">
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";
answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";


background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$form_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;
document.write(editPanel());

</script>

<!--
</td>
</tr>
<tr>
	<td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
</table>
-->

<form name="form1" method="post" action="phase_update.php" onSubmit="return checkform(this)">
<input type="hidden" name="phase_id" value="<?=$phase_id?>">
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>">
<input type="hidden" name="answersheet" value="<?=$form_obj["answersheet"]?>">
<input type="hidden" name="fieldname" value="answersheet">
<input type="hidden" name="formname" value="form1">
</form>

</body>
</html>

<?php
closedb();
?>