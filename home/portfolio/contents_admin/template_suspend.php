<?php
include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-filemanager.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

$li = new libdb();

$fileIDs = (is_array($file_ids)) ? implode(",", $file_ids) : $file_ids;

$sql = "UPDATE eclass_file SET IsSuspend = '$suspend' WHERE FileID IN ($fileIDs)";
$li->db_db_query($sql);

closedb();
header("Location: templates_manage.php?numPerPage=$numPerPage&pageNo=$pageNo&order=$order&field=$field&msg=2");
?>
