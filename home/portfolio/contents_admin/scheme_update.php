<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));  edit by eva because access right not set
intranet_auth();
intranet_opendb();


// define eclass C1274 as database
$eclass_c1274 = $eclass_prefix."c".$ck_course_id;

$li = new libdb();
$title 		= HTMLtoDB($title);
$instruction 	= HTMLtoDB($instruction);
$starttime 	= (trim($starttime)!="") ? "'".$starttime." $sh:$sm:00'" : "NULL";
$endtime 	= (trim($endtime)!="") ? "'".$endtime." $eh.$em:59'" : "NULL";
$creatorID 	= $_SESSION['UserID'];

if($sizeMax == "" || $sizeMax == '0')
{
	$sizeMax = 20;	
}
$sizeMax = HTMLtoDB($sizeMax)*1024;


if ($web_portfolio_id=="")
{
	$msg = 1;
	$status = 0;

	$fieldname = "title, instruction, status, starttime, deadline, inputdate, modified, folder_size ,created_by ";
	$sql  = "INSERT INTO {$eclass_c1274}.web_portfolio ($fieldname) values (";
	$sql .= "'$title', '$instruction', '$status', $starttime, $endtime, now(), now(), '$sizeMax','$creatorID' ";
	$sql .= ")";
	$li->db_db_query($sql);
	$web_portfolio_id = $li->db_insert_id();
	
} else
{
	$msg = 2;
	$fieldname  = "title = '$title', ";
	$fieldname .= "instruction = '$instruction', ";
	$fieldname .= "status = '$status', ";
	$fieldname .= "starttime = $starttime, ";
	$fieldname .= "deadline = $endtime, ";
	$fieldname .= "folder_size = $sizeMax, ";
	$fieldname .= "modified = now() ";
	$sql = "UPDATE {$eclass_c1274}.web_portfolio SET $fieldname WHERE web_portfolio_id = $web_portfolio_id ";
	$li->db_db_query($sql);
	
	//Handle existing webportfolio folder size
	$title = 'student_u%_wp'.$web_portfolio_id;
	$sql = "UPDATE {$eclass_c1274}.eclass_file SET SizeMax='$sizeMax' WHERE Title like '$title';";
	$li->db_db_query($sql);
	
}

// update user_group
$sql = "DELETE FROM {$eclass_c1274}.grouping_function WHERE function_type = 'PORTw' AND function_id = $web_portfolio_id";
$li->db_db_query($sql);
if ($web_portfolio_id!="" && $web_portfolio_id>0)
{
	for ($i = 0; $i < sizeof($target); $i++)
	{
		$group_id = $target[$i];
		$sql = "INSERT INTO {$eclass_c1274}.grouping_function (group_id, function_id, function_type) VALUES ($group_id, $web_portfolio_id, 'PORTw')";
		$li->db_db_query($sql);
	}
}

intranet_closedb();
header("Location: index_scheme.php?msg=$msg");

?>
