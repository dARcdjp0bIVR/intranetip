<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");
intranet_opendb();

$ldb = new libdb();
$lpf = new libpf_lp();
$lpf2 = new libpf_lp2($UserID, $web_portfolio_id, $UserID);
$portfolio_info = $lpf2->getPortfolioInfo();

$lpf->ACCESS_CONTROL("learning_sharing");

// Retrieve students whose LP was published
$sql = "SELECT iu.UserID ";
$sql .= "FROM {$lpf->course_db}.user_config uc ";
$sql .= "INNER JOIN {$lpf->course_db}.usermaster um ON uc.user_id = um.user_id ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON um.user_email = iu.UserEmail ";
$sql .= "WHERE uc.web_portfolio_id = {$web_portfolio_id} AND uc.notes_published IS NOT NULL";
$published_user_id_arr = $ldb->returnVector($sql);
$html_js_published_student = "[".implode(", ", $published_user_id_arr)."];\n";

// LP viewing key
for($i=0, $i_max=count($published_user_id_arr); $i<$i_max; $i++)
{
	$_published_user_id = $published_user_id_arr[$i];
	$_key = $lpf->GET_STUDENT_CRYPTED_STRING($web_portfolio_id, $_published_user_id);

	$field_value[] = "({$_published_user_id}, '{$_key}')";
}

$sql = "CREATE TEMPORARY TABLE tempLPKey(";
$sql .= "uID int(8), ";
$sql .= "lpKey varchar(255), ";
$sql .= "PRIMARY KEY (uID) ";
$sql .= ")";
$ldb->db_db_query($sql);
if(!empty($field_value))
{	
	$sql = "INSERT INTO tempLPKey ";
	$sql .= "(uID, lpKey) ";
	$sql .= "VALUES ";
	$sql .= implode(", ", $field_value);
	$ldb->db_db_query($sql);
} 

########################################################
# Table content : Start
########################################################

$lp_title = $lpf->GET_LEARNING_PORTFOLIO_TITLE($web_portfolio_id);

$sql = "SELECT group_id FROM {$lpf->course_db}.grouping_function WHERE function_type = 'PORTw' AND function_id = {$web_portfolio_id}";
$group_id_arr = $ldb->returnVector($sql);
if(count($group_id_arr) > 0)
{
  $sql = "SELECT DISTINCT user_id FROM {$lpf->course_db}.user_group WHERE group_id IN (".implode(", ", $group_id_arr).")";
  $user_id_arr = $ldb->returnVector($sql);

  $conds = "AND um.user_id IN (".((count($user_id_arr) > 0) ? implode(", ", $user_id_arr) : "''").") ";
}

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

//$href = $portfolio_info['version']>=1?"javascript:newWindow(\'/home/portfolio/learning_portfolio_beta/contents/?mode=draft&portfolio=":"javascript:openEclassWindow(\'student_view\',\'";
$href = "javascript:openEclassWindow(\'student_view\',\'";

$sql = "SELECT IFNULL(CONCAT(iu.ClassName, ' - ', iu.ClassNumber), '--'), IF(t_lpk.uID IS NULL, ".getNameFieldByLang2("iu.").", CONCAT('<a href=\"$href',t_lpk.lpKey,'\',\'".$UserID."\', 95)\" class=\"tablelink\">', ".getNameFieldByLang2("iu.").", '</a>')) AS DisplayName, IFNULL(uc.notes_published, '--'), ";
//$sql .= "CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"student_id[]\" value=\"', iu.UserID ,'\" ', IF(uc.notes_published IS NULL, 'DISABLED', '') ,'>') ";
$sql .= "IF(uc.user_config_id IS NULL, '&nbsp;<img src=\"{$image_path}/red_checkbox.gif\" />', CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"student_id[]\" value=\"', iu.UserID ,'\" >', IF(uc.notes_published IS NULL, '<span class=\"tabletextrequire\">#</span>', ''))) ";
$sql .= "FROM {$lpf->course_db}.usermaster um ";
$sql .= "LEFT JOIN {$lpf->course_db}.user_config uc ON um.user_id = uc.user_id AND uc.web_portfolio_id = {$web_portfolio_id} ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON um.user_email = iu.UserEmail ";
$sql .= "LEFT JOIN tempLPKey t_lpk ON iu.UserID = t_lpk.uID ";
$sql .= "WHERE um.memberType = 'S' ";
$sql .= "AND (um.status IS NULL OR um.status <> 'deleted') ";
$sql .= $conds;$lpf->returnArray($sql);

$LibTable->field_array = array("CONCAT(iu.ClassName, '-', LPAD(TRIM(iu.ClassNumber), 4, '0'))", getNameFieldByLang2("iu."), "uc.notes_published");
$LibTable->sql = $sql;
if ($portfolio_info['version']==0) $LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 5;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td width='5%' height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td width='30%' class=\"tabletopnolink\">".$LibTable->column(0,$i_general_class." - ".$i_ClassNumber, 1)."</td>";
$LibTable->column_list .= "<td width='30%' nowrap='nowrap' >".$LibTable->column(1,$i_general_name, 1)."</td>\n";
$LibTable->column_list .= "<td width='30%' >".$LibTable->column(2,$Lang['iPortfolio']['LastPublishDate'], 1)."</td>\n";
if ($portfolio_info['version']==0) $LibTable->column_list .= "<td width='5%' >".$LibTable->check("student_id[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

$MenuArr = array();
$MenuArr[] = array($iPort['menu']['learning_portfolio']." ".$ec_iPortfolio['list'], "index_scheme.php");
$MenuArr[] = array($lp_title, "");

# define the navigation, page title and table size
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getLpMgmtTabs($currentPageIndex);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="/templates/2009a/js/iportfolio.js"></script>
<script language="JavaScript">
var publishedStudentID = <?=$html_js_published_student?>
var preventDef =	function(event){
										event.preventDefault();
									}

function publishLP(obj,element,allPage){
  if($("input[name='student_id[]']:not(:disabled)").filter(":checked").size() == 0)
  {
		alert(globalAlertMsg2)
	}
	else if(confirm("<?=$Lang['iPortfolio']['publish_student_content']?>"))
	{
		document.form1.publishAllPage.value = allPage;
    document.form1.action = "scheme_republish.php";
    document.form1.submit();
  }
}

function handleActionBar(){
		var partialPublishable = true;
  
  	// for all students selected, check if his LP was publised before
		$("input[name='student_id[]']:not(:disabled)").filter(":checked").each(function(){
			var studentID = parseInt($(this).val());
			
			// If the student has not published, don't allow partial publish
			if($.inArray(studentID, publishedStudentID) == -1)
			{
				partialPublishable = false;
				return false;
			}
		});
		
		if(partialPublishable)
		{
			$("#partialPublish").css("color", "");
			$("#partialPublish").unbind("click", preventDef);
		}
		else
		{
			$("#partialPublish").css("color", "#CCCCCC");
			$("#partialPublish").bind("click", preventDef);
		}

}

$(document).ready(function(){
	$("input[name='student_id[]']").click(function(){
		handleActionBar();
	});
	
	$("#show_publish_type").click(function(){
		var pos = $("#publish_type_list").parent().position();
		var left = pos.left - 120;
		$("#publish_type_list").css("left", left+"px");
		$("#publish_type_list").slideDown("slow");
	});
	
	$("#hide_publish_type").click(function(){
		var pos = $("#publish_type_list").parent().position();
		var left = pos.left;
		$("#publish_type_list").slideUp("slow", function(){
			$("#publish_type_list").css("left", left+"px");
		});
	});
});
</script>

<form name="form1" method="post" action="scheme_student_list.php">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right"><?=$error_msg ?></td>
	</tr>
	<tr>
	 <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
	</tr>
	<tr>
		<td class="tab_underline" width="100%">
			<?=$lpf->GET_TAB_MENU($TabMenuArr) ?>
		</td>
	</tr>
	<tr>
  	<td align="center"><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['lpForcePublish_instruction'], "")?></td>
  </tr>
  <tr>
  	<td  width="100%" align="center">
  		<table width="100%" border="0" cellpadding="0" cellspacing="0">
  			<tr>
  				<td align="center" valign="middle">
  					<table width="98%" border="0" cellpadding="0" cellspacing="0">
					    
            <? if($portfolio_info['version']==0):?>
	      
	      <tr>
            		<td align="right" valign="bottom">
            			<table border="0" cellpadding="0" cellspacing="0">
            				<tbody>
					    
            					<tr>
            						<td width="21"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_01.gif" height="23" width="21"></td>
            						<td background="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_02.gif">
													<a href="javascript:;" id="show_publish_type" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_public.gif" width="12" height="12" border="0" align="absmiddle"><?=$button_publish_iPortfolio?></a>
													<div class="clear"></div>
													<div id="publish_type_list" style="position:absolute; width:150px; z-index:1; display:none;">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" >
															<tr>
																<td height="19">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td width="5" height="19"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_01.gif" width="5" height="19"></td>
																			<td height="19" valign="middle" background="/images/2009a/can_board_02.gif">&nbsp;</td>
																			<td width="19" height="19"><a href="javascript:;" id="hide_publish_type"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_close_off.gif" name="pre_close22" width="19" height="19" border="0" id="pre_close22" onMouseOver="MM_swapImage('pre_close22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
																		</tr>
																	</table>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_04.gif"><img src="/images/2009a/can_board_04.gif" width="5" height="19"></td>
																			<td bgcolor="#FFFFF7">
																				<table width="98%" border="0" cellpadding="0" cellspacing="3">
																					<tr>
																						<td align='left' valign='top'><a href="javascript:publishLP(document.form1,'student_id[]', 0)" id="partialPublish" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/icon_public.gif" align="absmiddle" border="0" height="12" width="12"> <?=$Lang['iPortfolio']['prev_published_content'] ?></a></td>
																					</tr>
																					<tr>
																						<td align='left' valign='top'><a href="javascript:publishLP(document.form1,'student_id[]', 1)" id="fullPublish" class="tabletool"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/icon_public.gif" align="absmiddle" border="0" height="12" width="12"> <?=$Lang['iPortfolio']['all_content'] ?></a></td>
																					</tr>
																				</table>
																			</td>
																			<td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_06.gif"><img src="/images/2009a/can_board_06.gif" width="6" height="6"></td>
																		</tr>
																		<tr>
																			<td width="5" height="6"><img src="/images/2009a/can_board_07.gif" width="5" height="6"></td>
																			<td height="6" background="/images/2009a/can_board_08.gif"><img src="/images/2009a/can_board_08.gif" width="5" height="6"></td>
																			<td width="6" height="6"><img src="/images/2009a/can_board_09.gif" width="6" height="6"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</div>
            						</td>
            						<td width="6"><img src="<?=$PATH_WRT_ROOT?>images/<?= $LAYOUT_SKIN ?>/management/table_tool_03.gif" height="23" width="6"></td>
            					</tr>
            				</tbody>
            			</table>
            		</td>
            	</tr>
	    <? endif;?>
  						<tr>
  							<td>
  								<?=$table_content?>
								</td>
							</tr>
			      </table>
			      <br />
			      <table width="98%" border="0" cellpadding="0" cellspacing="0">
			      	<col width="5%">
			      	<col width="93%">
			      	<tr>
			      		<td><img src="<?=$image_path?>/red_checkbox.gif" /></td>
			      		<td><?=$Lang['iPortfolio']['LPnotOpened']?></td>
			      	</tr>
			      	<tr>
			      		<td><span class="tabletextrequire">#</span></td>
			      		<td><?=$Lang['iPortfolio']['LPnotPublished']?></td>
			      	</tr>
			      </table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>">
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />
<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" />
<input type="hidden" name="publishAllPage" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>