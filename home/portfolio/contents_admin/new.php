<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
//include_once($PATH_WRT_ROOT."includes/libeclass40.php");


include_once($eclass_filepath."/src/includes/php/lib-filemanager.php");

include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
include_once($eclass_filepath."/src/includes/php/lib-user.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

// $ck_course_id = 1274; // by eva temporaly
# layout skin for learning portfolio management IP2.5
if($intranet_session_language=="en")
{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";
}else{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_chib5";
}
/*
if category,
	afer: root, categories and pages in first 2 levels
	below: root, categories in first level
if page,
	after: root, all categories and pages
	below: root, all categories
*/
$li = new notes_iPortfolio("", $web_portfolio_id);
$lpf = new libportfolio2007();
$idList = $li->getGroupNotes($web_portfolio_id);
$li->notes2($idList);

# Load scheme start date and deadline
$scheme_obj = $li->getSchemeInfo($web_portfolio_id);
$scheme_obj = $scheme_obj[0];

$scheme_startdatetime = $scheme_obj['starttime'];
if($scheme_startdatetime != "")
{
	list($scheme_startdate, $scheme_starttime) = explode(" ", $scheme_startdatetime);
	list($scheme_starthour, $scheme_startmin, $scheme_startsec) = explode(":", $scheme_starttime);
	
	$HasSchemeStartDate = true;
}
else
	$HasSchemeStartDate = false;

$scheme_enddatetime = $scheme_obj['deadline'];
if($scheme_enddatetime != "")
{
	list($scheme_enddate, $scheme_endtime) = explode(" ", $scheme_enddatetime);
	list($scheme_endhour, $scheme_endmin, $scheme_endsec) = explode(":", $scheme_endtime);
	
	$HasSchemeEndDate = true;
}
else
	$HasSchemeEndDate = false;
/*
$group_permission = $li->getGroupPermission($web_portfolio_id);
if ($group_permission>=0)
{
	echo "<script language='javascript'>\n self.history.back();\n </script>\n";
}
*/

# INITIALIZE DEFAULT PAGE TEMPLATES

$default_blank_page = "A_Blank_Page.html";
$default_folder = "TEMPLATES";

$fm = new fileManager($ck_course_id, 0, "");
$sql = "SELECT fileID FROM eclass_file
		WHERE title='$default_folder' AND VirPath IS NULL AND IsDir=1 AND Category=0 ";

$row = $fm->returnVector($sql);
if (($folderID=$row[0])=="")
{
	$lu = new libuser($ck_user_id, $ck_course_id);

	$fm = new fileManager($ck_course_id, 0, "");
	$fm->user_name = addslashes($lu->user_name());
	$fm->memberType = "T";
	$fm->permission = "AW";
	$folderID = $fm->createFolderDB($default_folder, "", "", 0);

	# add default templates from src/system
	$folder_src = "$admin_root_path/system/portfolio/default_page_templates";
	$fm->dest_categoryID = 0;
	$fm->dest_courseID = $ck_course_id;
	$fm->IO2DB($folder_src, $folderID);
}

$mode = ($mode=="") ? 0 : $mode;
${"type_checked_".$mode} = "checked";

$status = ($status=="") ? 0 : $status;
${"status_checked_".$status} = "checked";

# keep previous selection of locator if possible
if ($m!=="" && $chapter!="")
{
	$js_extra_function2 .= ($m==1) ? "document.form1.m[1].click();\n" : "";
	$js_extra_function2 .= "for (var i=0; i<document.form1.chapter.length; i++) {	if(document.form1.chapter.options[i].value==\"$chapter\") { document.form1.chapter.selectedIndex = i; }};\n";
}
$linterface = new interface_html("popup4.html");
$linterface->LAYOUT_START();
?>

<script language=JavaScript src="http://<?=$eclass_httppath?>/src/includes/js/layer.js"></script>
<link href='http://<?=$eclass_httppath?>/src/includes/css/style_iPortfolio_purple.css' rel='stylesheet' type='text/css'>
<script language="javascript">
isToolTip = true;
isMenu = true;

function checkform(obj){
	var isst = false;
	if(!check_text(obj.title, "<?= $ec_warning['notes_title'] ?>")) return false;
	if (typeof(obj.starttime)!="undefined")
	{
		if (obj.starttime.value!="")
		{
			if(!check_date(obj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			isst = true;
			
<?php if($HasSchemeStartDate) { ?>
			time1 = '<?=$scheme_startdate?>'+'<?=$scheme_starthour?>'+'<?=$scheme_startmin?>';
			time2 = toTimeStr(obj.starttime, obj.sh, obj.sm);

			if(time2 < time1)
			{
				alert("<?=$ec_warning['start_end_scheme_start_time']?>");
				obj.starttime.focus();
				return false;
			}
<?php } ?>
		}
	}
	if (typeof(obj.endtime)!="undefined")
	{
		if (obj.endtime.value!="")
		{
			if(!check_date(obj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(obj.starttime, obj.sh, obj.sm, obj.endtime, obj.eh, obj.em)) {
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
			
<?php if($HasSchemeEndDate) { ?>
			time1 = toTimeStr(obj.endtime, obj.eh, obj.em);
			time2 = '<?=$scheme_enddate?>'+'<?=$scheme_endhour?>'+'<?=$scheme_endmin?>';

			if(time2 < time1)
			{
				alert("<?=$ec_warning['start_end_scheme_end_time']?>");
				obj.endtime.focus();
				return false;
			}
<?php } ?>
		}
	}
	// assign web_portfolio_id obtained from tree navigation page
	document.form1.web_portfolio_id.value = parent.treeframe.form1.web_portfolio_id.value;
}

function addTemplate(){
	//var url = "../../tool/misc/html_editor/index.php?comeFrom=CC&fieldname=filelink&categoryID=0&attachment=" + charEscape(x) + "&title=" + document.form1.title.value;
	//var url = "http://<?=$eclass_httppath ?>/src/tool/misc/html_editor/index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file";
	var url = "http://<?=$eclass_httppath ?>/src/tool/misc/html_editor/editor_index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file";
	newWindow(url,21);
}
function triggerToSchedule(myValue){
	document.form1.add_to_schedule.disabled = (myValue=="");
}
function changeMode(mode){
	var obj = document.form1;
	if (obj.mode.value!=mode)
	{
		obj.action = "new.php";
		obj.mode.value = mode;
		obj.submit();
	}
}
function previewTemplate(){
	var obj = document.form1;
	if (typeof(obj.template_file)!="undefined")
	{
		var file_link = obj.template_file.options[obj.template_file.selectedIndex].value;
		newWindow("http://<?=$eclass_httppath ?>/src/course/resources/files/open.php?fpath="+file_link, 23);
	}
}
function doCancel(){
	history.back();
}
function doReset(){
<?php if ($mode==1) { ?>
	displayTable('table_method_html', '');
	displayTable('table_method_eclass', '');
<?php } ?>
	displayTable('table_period', '');

	document.form1.reset();
}


function triggerMethod(table_shown){
	displayTable('table_method_html', 'none');
	displayTable('table_method_form', 'none');
	displayTable('table_method_weblog', 'none');
	displayTable('table_method_eclass', 'none');
	eval("displayTable('table_method_"+table_shown.toLowerCase()+"', '')");
}

function editOnlineForm(){
	postInstantForm(document.form1, "../growth_admin/online_form/edit.php", "post", 26, "ec_popup26");
}

<?=$js_extra_function?>
</script>


<form name=form1 action="newUpdate.php" method="post" onSubmit="return checkform(this);">
<div id="ToolTip"></div>
<div id="ToolMenu"></div>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="7" align="left"><img src="<?=$sr_image_path?>/Border/Title_Left.gif" width="7" height="30"></td>
    <td background="<?=$sr_image_path?>/Border/Title_BG.gif" class="18Gray"><?=$button_new?></td>
    <td width="6" align="right"><img src="<?=$sr_image_path?>/Border/Title_Right.gif" width="6" height="30"></td>
  </tr>
		<tr >
          <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
  <tr>
    <td>&nbsp;</td>
    <td><br><table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td valign="top" width="50" class="13GrayP5555" nowrap><?= $notes_title ?>:</td>
          <td > <input type=text name="title" size=50 maxlength=255 value="<?=stripslashes(str_replace('"','&quot;',$title))?>">
          </td>
        </tr>
        <tr>
          <td valign="top" class="13GrayP5555" nowrap><?= $ec_iPortfolio['type'] ?>:</td>
            <td><table width="215" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="13Gray" nowrap><input type="radio" name="notes_type" id="notes_type_0" value="CAT" <?=$type_checked_0?> onClick="changeMode(0)" ><label for="notes_type_0"> <?=$ec_iPortfolio['type_category']?></label><img src="http://<?=$eclass_httppath?>/images/tree/portfolio/folder0_group.gif" border="0" align="absmiddle" hspace="2"></td>
                  <td class="13Gray" nowrap><input type="radio" name="notes_type" id="notes_type_1" value="DOC" <?=$type_checked_1?> onClick="changeMode(1)" ><label for="notes_type_1"> <?=$ec_iPortfolio['type_document']?></label><img src="http://<?=$eclass_httppath?>/images/tree/portfolio/doc_group.gif" border="0" align="absmiddle" hspace="2"></td>
                </tr>
              </table></td>
        </tr>
        <tr>
          <td valign="top"width="50" class="13GrayP5555" nowrap><?= $ec_iPortfolio['notes_guide'] ?>:</td>
          <td ><?=$linterface->getTextArea("description", "")?></td>
        </tr>
        <tr>
          <td valign="top" class="13GrayP5555" nowrap><?= $notes_position ?>:</td>
          <td height="50"><?= $li->locator(0, $mode) ?></td>
        </tr>
        <tr>
          <td valign="top"width="50" class="13GrayP5555" nowrap><?= $qbank_status ?>:</td>
          <td >
			<table width="365" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="100%" class="13Gray" nowrap>
						<input type=radio name=status id="status_1" value="1" onClick="if(this.checked){displayTable('table_period', 'block');}" checked><label for="status_1"><?= $button_public ?></label>&nbsp;&nbsp;
						<input type=radio name=status id="status_0" value="0" onClick="if(this.checked){displayTable('table_period', 'none');}"><label for="status_0"><?= $button_private ?></label>

						<table border=0 id="table_period" style="display:block;">
						<tr><td nowrap><?=$StartTime?>:</td><td nowrap>&nbsp;<input class="inputfield" type=text name="starttime" size=11 maxlength=10 value="" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?= $linterface->GET_CALENDAR("form1", "starttime"); ?> <?= returnHour('sh', $sh, 0).returnMinute('sm', $sm, 0) ?> <?= $linterface->getTipsHelp($ec_guide['learning_portfolio_time_start'])?></td></tr>
						<tr><td nowrap><?=$EndTime?>:</td><td nowrap>&nbsp;<input class="inputfield" type=text name=endtime size=11 maxlength=10 value="" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?= $linterface->GET_CALENDAR("form1", "endtime");	 ?> <?= returnHour('eh', $eh).returnMinute('em', $em) ?> <?= $linterface->getTipsHelp($ec_guide['learning_portfolio_time_end'])?></td></tr>
						</table>
				  </td>
                </tr>
              </table>
          </td>
        </tr>

<?php
if ($mode==1)
{
	$fm = new fileManager($ck_course_id, 0, "");

	if($sys_custom['iPf']['LP']['editLPForCreatedTeacherOnly']){

	if($lpf->IS_IPF_SUPERADMIN()){
	}else{

		$objEclass = new libeclass();
		$result = $objEclass->returnSpecialRooms($ck_user_email,$ipfRoomType = 4);

		if(count($result) == 1){
			$result = current($result);
			$this_eclass_use_id = $result['user_id'];
			$requestCreatorID = $this_eclass_use_id;		
		}

		//$sql = 'select user_id from '.$eclass_db.'.user_course where user_email = \''.$ck_user_email.'\' and course_id ='.$ck_course_id.' ';
		/*
		$result = $lpf->returnResultSet($sql);
		if(count($result) == 1){
			$result = current($result);
			$requestCreatorID = $result['user_id'];		
		}*/
	}
}
	//$files_arr = $fm->getFileInside("/TEMPLATES", $requestCreatorID);
	$files_arr = $fm->getLPschemeTemplate($requestCreatorID);
	$templates_html = $li->returnTemplatesSelection($files_arr);
?>
<tr>
          <td valign="top" class="13GrayP5555" nowrap><?= $ec_iPortfolio['notes_method'] ?>:</td>
          <td valign="top"><select name="method" onChange="triggerMethod(this.value)">
          	<option value="HTML"><?= $ec_iPortfolio['notes_method_html'] ?></option>
          	<!--<option value="FORM"><?= $ec_iPortfolio['notes_method_form'] ?></option>-->
          	<option value="WEBLOG"><?= $ec_iPortfolio['notes_method_weblog'] ?></option>
          	<!--<option value="ECLASS"><?= $ec_iPortfolio['notes_method_eClass'] ?></option>-->
          	
          	
          	<!-- New to display Activity from System -->
<!--
          	<option value="ACT">Activity</option>
          	<option value="AWARD">Award</option>
-->
          	
          	
          	
          </select>
          <br>
          <table id="table_method_html" border="0" cellpadding="0" cellspacing="0">
          <tr><td><?= $ec_iPortfolio['notes_template'] ?> - <input type=hidden name=linktype value="1"><?= $templates_html ?> <a href="javascript:previewTemplate()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgPreview','','<?=$sr_image_path?>/Buttons/btn_PreView2_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_PreView2_D.gif" name="imgPreview" border="0" hspace="3" align="absmiddle"></a><a href="javascript:addTemplate()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAddTemplate','','<?=$sr_image_path?>/Buttons/btn_AddTemplate_Ub.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_AddTemplate_Db.gif" name="imgAddTemplate" border="0" hspace="3" align="absmiddle"></a>
          </td></tr></table>
          <table id="table_method_form" border="0" cellpadding="0" cellspacing="0" style="display:none">
          <tr><td><input type="radio" name="sheettype" value="1" checked> <?=$ec_iPortfolio['form_display_h']?> &nbsp; <input type="radio" name="sheettype" value="0"> <?=$ec_iPortfolio['form_display_v']?>&nbsp; &nbsp;
          <a href="javascript:editOnlineForm()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAS','','<?=$sr_image_path?>/Buttons/btn_Produce_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Produce_D.gif" name="imgAS" border="0" align="absmiddle"></a></td></tr></table>
          <table id="table_method_weblog" border="0" cellpadding="0" cellspacing="0" style="display:none">
          <tr><td>&nbsp;</td></tr></table>
          <table id="table_method_eclass" border="0" cellpadding="0" cellspacing="0" style="display:none">
          <tr><td>&nbsp;</td></tr></table>
          <!--table id="table_method_act" border="0" cellpadding="0" cellspacing="0" style="display:none">
          <tr><td>&nbsp;</td></tr></table>
          <table id="table_method_award" border="0" cellpadding="0" cellspacing="0" style="display:none">
          <tr><td>&nbsp;</td></tr></table-->
          </td>
</tr>
<?php
}
?>

      </table></td>
    <td>&nbsp;</td>
  </tr>
  	  <tr>
          <td colspan="3"><br></td>
        </tr>
        <tr>
          <td background="<?=$sr_image_path?>/Border/dot_Line.gif" colspan="3" ><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td align="right"> <table width="150" border="0" cellspacing="5" cellpadding="0">
              <tr>
                  <td><input class="formbutton" type="submit" value="<?=$button_confirm?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
                  <td><input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="button" onClick="doReset()"></td>
                  <td><input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="javascript:doCancel()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
					</tr>
            </table></td>
          <td>&nbsp;</td>
        </tr>
</table>

<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" >
<input type="hidden" name="mode" value="<?=$mode?>" >
<input type="hidden" name="answersheet" value="">
<input type="hidden" name="fieldname" value="answersheet">
<input type="hidden" name="formname" value="form1">
</form>

<script language="JavaScript">
<?= $js_extra_function2 ?>
</script>

</BODY>
</HTML>
<?php
echo autoFocusField("title");
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
