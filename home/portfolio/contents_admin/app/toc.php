<?php
/*
 * Editing by 
 * 
 * Modification Log: 
 * 2013-06-3 (Jason)
 * 		- fix the problem of not showing the tree view list properly
 * 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// Set ID temporarily, since session not setting
$ck_room_type = "iPORTFOLIO";
//$eclass_root = "/home/eclass30/eclass30"; 
//$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";

$li = new notes_iPortfolio();
$li -> db = $eclass_prefix."c".$ck_course_id;
$idList = $li->getGroupNotes($web_portfolio_id);
$li->notes2($idList);
# layout skin for learning portfolio management IP2.5
if($intranet_session_language=="en")
{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";
}else{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_chib5";
}

$linterface = new interface_html("popup4.html");
$linterface->LAYOUT_START();
?>

<link href='http://<?=$eclass_httppath?>/src/includes/css/style_iPortfolio_purple.css' rel='stylesheet' type='text/css'>
<script language="javascript">
function getDoc(i){
	obj = parent.treeframe.document.form1;
	obj.notes_id.options[i].selected = true;
}
function goToPage(notes_id, pageNo){
	window.top.basefrm.location = "../navbar.php?notes_id="+notes_id+"&pageNo="+pageNo+"&web_portfolio_id=<?=$web_portfolio_id?>";
	parent.treeframe.highlightNode(pageNo);
}
</script>

<center>
<table width="85%" border=0 cellpadding=2 cellspacing=1>
<tr><td colspan="2">
<br>
<table width=100% border=0 cellpadding=2 cellspacing=1>
<tr>
	<td background="http://<?=$eclass_httppath?>/images/portfolio_eng/Border/dot_Line.gif" colspan="100%"><img src="<?=$sr_image_path?>/spacer.gif" width="25" height="9"></td>
</tr>

<?= $li->getTreeTableOfContent($notes_id) ?>
</table>

<table width=400 border=0><tr><td>&nbsp;</td></tr></table>

</td></tr></table>


</center>


<?php
if ($notes_id=="" || !isset($notes_id))
{
?>
<!--
<script language="javascript">
parent.setCookie("eBookMyNotes", false);
</script> 
-->
<?php
}
?>

</body>
</html>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
