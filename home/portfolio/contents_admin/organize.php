<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($eclass_filepath."/src/includes/php/lib-filemanager.php");
include_once($eclass_filepath."/src/includes/php/lib-notes.php");
include_once($eclass_filepath."/src/includes/php/lib-notes_portfolio.php");
//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

// Setting ID temporarily, since session ID not setting
// $ck_course_id = 1274; // by eva temporaly
//$image_path = "http://192.168.0.245:61001/images/";
$image_path = "http://".$eclass_httppath."/images/";
# layout skin for learning portfolio management IP2.5
if($intranet_session_language=="en")
{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_eng";
}else{
	$sr_image_path = "http://".$eclass_httppath."/images/portfolio_chib5";
}
//end setting

$li = new notes_iPortfolio("", $web_portfolio_id);
$idList = $li->getGroupNotes($web_portfolio_id);
$li->notes2($idList);

$linterface = new interface_html("popup4.html");
$linterface->LAYOUT_START();
?>

<link href='http://<?=$eclass_httppath?>/src/includes/css/style_iPortfolio_purple.css' rel='stylesheet' type='text/css'>
<script language="javascript">
function getDoc(i){
	obj = parent.treeframe.document.form1;
	obj.notes_id.options[i].selected = true;
}

function doAction(myType, notes_id){
	switch (myType)
	{
		case 1:
		// edit
				self.location = "edit.php?notes_id=" + notes_id;
				break;
		case 2:
		// remove
				self.location = "remove_item.php?web_portfolio_id=<?=$web_portfolio_id?>&notes_id[]=" + notes_id;
				break;
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
				self.location = "moveUpdate.php?notes_id=" + notes_id + "&web_portfolio_id=<?=$web_portfolio_id?>&operator=" + myType;
				break;
	}
}

function viewPage(notes_id, page_no){
	newWindow("../contents_student/viewpage.php?notes_id="+notes_id+"&pageNo="+page_no, 13);
}
</script>


<form name="form1" method="post" action="organize_update.php">

<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td width="7" align="left"><img src="<?=$sr_image_path?>/Border/Title_Left.gif" width="7" height="30"></td>
      <td background="<?=$sr_image_path?>/Border/Title_BG.gif" width="100%" ><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="18Gray"><?=$button_organize?></td><td align="right"><?=getSystemMessage($msg)?></td></tr></table></td>
      <td width="6" align="right"><img src="<?=$sr_image_path?>/Border/Title_Right.gif" width="6" height="30"></td>
    </tr>
    <tr>
      <td colspan="3" background="<?=$sr_image_path?>/Border/dot_Line.gif"><img src="<?=$sr_image_path?>/Border/dot_Line.gif" width="25" height="9"></td>
    </tr>
    <tr>
      <td colspan="3"><table width="100%" border=0 cellpadding=2 cellspacing=1 align="center">
	<?= $li->getTreeTableOrganize($notes_id) ?></table>

</td></tr>
</table>

<input type="hidden" name="web_portfolio_id" value="<?=$web_portfolio_id?>" />
</form>

<?php
if ($msg==2 || $msg==3)
{
?>
<script language="JavaScript">
parent.treeframe.location.reload();
</script>
<?php
}
?>

</body>
</html>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
