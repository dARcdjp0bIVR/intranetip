<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio2007();
$lpf->ACCESS_CONTROL("learning_sharing");

########################################################
# Table content : Start
########################################################

$ldb = new libdb();
$sql = "CREATE TEMPORARY TABLE {$intranet_db}.tempYearClass DEFAULT CHARSET=utf8 ";
$sql .= "SELECT ycu.UserID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassTitle, ycu.ClassNumber ";
$sql .= "FROM {$intranet_db}.YEAR_CLASS_USER ycu ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
$sql .= "WHERE yc.AcademicYearID = ".Get_Current_Academic_Year_ID();
$ldb->db_db_query($sql);

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$sql = "SELECT DISTINCT IFNULL(CONCAT(t_ycu.ClassTitle, ' - ', t_ycu.ClassNumber), '--'), ".getNameFieldByLang2("iu.")." AS DisplayName, CONCAT('<span id=\"statusCell_', iu.UserID, '\"><input type=\"hidden\" name=\"student_id[]\" value=\"', iu.UserID, '\" /></span>') ";
$sql .= "FROM {$intranet_db}.INTRANET_USER iu ";
$sql .= "LEFT JOIN {$intranet_db}.tempYearClass AS t_ycu ";
$sql .= "ON t_ycu.UserID = iu.UserID ";
$sql .= "WHERE iu.RecordType = 2 ";
$sql .= "AND iu.UserID IN (".implode(", ", $student_id).") ";

$LibTable->field_array = array("CONCAT(iu.ClassName, '-', LPAD(TRIM(iu.ClassNumber), 4, '0'))", "DisplayName", "uc.notes_published");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = 9999;
$LibTable->no_col = 4;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td width='4%' height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td width='32%' class=\"tabletopnolink\">{$i_general_class} - {$i_ClassNumber}</td>";
$LibTable->column_list .= "<td width='32%' nowrap='nowrap' >{$i_general_name}</td>\n";
$LibTable->column_list .= "<td width='32%' >{$iPort["status"]}</td>\n";
$LibTable->column_list .= "</tr>\n";

$table_content = $LibTable->displayPlain();

########################################################
# Table content : End
########################################################

$MenuArr = array();
$MenuArr[] = array($iPort['menu']['learning_portfolio']." ".$ec_iPortfolio['list'], "");
$MenuArr[] = array($ec_iPortfolio['student_list'], "");
$MenuArr[] = array($button_publish_iPortfolio, "");

# define the navigation, page title and table size
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];

$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getLpMgmtTabs($currentPageIndex);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="JavaScript">

var currentIndex = 0;
var maxIndex;
var student_id_arr = new Array();

function publishLP(student_id){
  var query = "wp_id=<?=$web_portfolio_id?>";
  query = query + "&student_arr[]=" + student_id;
  query = query + "&publishAllPage=<?=$publishAllPage?>";

  $.ajax({
    type: "POST",
    url: '/home/portfolio/learning_portfolio/contents_student/management/notes_republish.php',
    data: query,
    beforeSend: function() {
      $("#statusCell_"+student_id).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
    },
    success: function(data) {
      $("#statusCell_"+student_id).html("<?=$iPort["finished"]?>");
      
      currentIndex++;
      if(currentIndex <= maxIndex)
      {
        publishLP(student_id_arr[currentIndex]);
      }
      else
      {
        alert("<?=$Lang['iPortfolio']['publish_student_complete']?>");
        $("#backBtn").removeAttr("disabled");
        $("#leavePageWarn").hide();
        window.onbeforeunload = null;
      }
    }
  });
}

$(document).ready(function(){

	// Add an alert to prevent accidental click
	window.onbeforeunload = function(){
		return "";
	}

  $("input[name='student_id[]']").each(function(){
    student_id_arr.push($(this).val());  
  });

  maxIndex = student_id_arr.length - 1;
  publishLP(student_id_arr[currentIndex]);
});

</script>

<table border="0" cellpadding="5" cellspacing="0" width="98%">
  <tr>
	 <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
	</tr>
	<tr id="leavePageWarn">
	 <td><?=$linterface->Get_Warning_Message_Box("", $Lang['iPortfolio']['alertLeavePage']) ?></td>
	</tr>
  <tr>
    <td>
      <?=$table_content?>
    </td>
  </tr>
  <tr>
		<td height="1" class="dotline"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
  <tr>
    <td align="center"><input id="backBtn" type="button" onClick="self.location='scheme_student_list.php?web_portfolio_id=<?=$web_portfolio_id?>'" value="<?=$button_back?>" class="formbutton" DISABLED /></td>
  </tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>