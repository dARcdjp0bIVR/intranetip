<?php

// Modifing by Max

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($eclass_filepath."/src/includes/php/lib-groups.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

# Page Authentication
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));
iportfolio_auth("T");

intranet_opendb();

// $ck_course_id = 1274; //edit by eva temporarily. Reason: session not set
$lo = new libgroups(classNamingDB($ck_course_id));

# define the navigation, page title and table size
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_LearningPortfolio";
$CurrentPageName = $iPort['menu']['learning_portfolio'];
$TextAreaClassChangeEnabled = true;

$lpf = new libportfolio();
$lpf->ACCESS_CONTROL("learning_sharing");
$luser = new libuser($UserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="javascript">
function checkform(myObj){
	var isst = false;

	if (!check_text(myObj.title, "<?=$ec_warning['growth_title']?>")) return false;
	if(document.form1.sizeMax.value <= 0){ alert("<?php echo $assignments_alert_msg13; ?>"); document.form1.sizeMax.focus(); return false;}
	if (typeof(myObj.starttime)!="undefined")
	{
		if (myObj.starttime.value!="")
		{
			if(!check_date(myObj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			{
				isst = true;
			}
		}
	}
	if (typeof(myObj.endtime)!="undefined")
	{
		if (myObj.endtime.value!="")
		{
			if(!check_date(myObj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(myObj.starttime, myObj.sh, myObj.sm, myObj.endtime, myObj.eh, myObj.em)) {
					myObj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}
	checkOption(myObj.elements["target[]"]);
	for(var i=0; i<myObj.elements["target[]"].length; i++)
	{
		myObj.elements["target[]"].options[i].selected = true;
	}
	return true;
}
function doCancel(){
	self.location.href = "index_scheme.php";
}
function doReset(){
	self.location.reload();
}

function check(from,to){
	checkOption(from);
	checkOption(to);
	i = from.selectedIndex;
	while(i!=-1){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
		from.options[i] = null;
		i = from.selectedIndex;
	}
}
</script>

<form name="form1" method="post" action="scheme_update.php" onSubmit="return checkform(this)">
<table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
      <tr>
      	    <td colspan=2 class="navigation"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="middle" height="15" width="15"><?=$ec_iPortfolio['new_portfolios'] ?></td>
      </tr>
<tr>
<td width=40>&nbsp;</td>
<td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
          <td class="formfieldtitle" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_title'] ?></span><span class="tabletextrequire">*</span></td>
          <td width="80%"><input type=text name="title" class="textboxtext">
          </td>
        </tr>
        <tr>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_description'] ?></span></td>
            <td width="80%"><?=$linterface->getTextArea("instruction", "")?></td>
        </tr>
        <tr>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_phase_period'] ?></span></td>
          <td><table border=0 cellpadding="3" cellspacing="0">
				<tr><td><span class="tabletext"><?=$StartTime?></span></td><td nowrap>&nbsp;<input type=text name="starttime" size=11 maxlength=10 value="" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?=$linterface->GET_CALENDAR("form1", "starttime"); ?> <?= returnHour('sh', $sh, 0).returnMinute('sm', $sm, 0) ?></td></tr>
				<tr><td><span class="tabletext"><?=$EndTime?></span></td><td nowrap>&nbsp;<input type=text name="endtime" size=11 maxlength=10 value="" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}"><?= $linterface->GET_CALENDAR("form1", "endtime"); ?> <?= returnHour('eh', $eh).returnMinute('em', $em) ?></td></tr>
		</table>
	    </td>
        </tr>
        <tr>
			<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['iportfolio_folder_size'] ?></span></td>
        	<td><input type=text name="sizeMax" size=10 maxlength=10 value="20">&nbsp;<span class="tabletext"><?= $ec_iPortfolio['size_unit'] ?></span></td>
		</tr>
        <tr>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $ec_iPortfolio['growth_group'] ?></span></td>
          <td >
          	<table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
          	<tbody>
                 <tr valign="top"><td width="40%"><span class="tabletext"><?=$ec_iPortfolio['choose_group']?> :</span></td>
                                         <td nowrap="nowrap" width="10"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" height="10" width="10"></td>
                                         <td nowrap="nowrap" width="60%"><span class="tabletext"><?=$ec_iPortfolio['select_group']?> :</span></td>
                 </tr>

		<tr>
			<td class="tablerow2" valign="top">
				<select name="source[]" size=15 class="select_studentlist" style="width: 200px;" multiple>
					<?= $lo->getGroupsList() ?>
					<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
				</select>
			</td>
		<td align="center" nowrap="nowrap" valign="middle">

	   <input name="imgAddItem" class="formsubbutton" value="&gt;&gt;" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" type="button" onclick="javascript:check(document.form1.elements['source[]'],document.form1.elements['target[]'])">
         <br>
         <br>
       <input name="imgRemoveItem" class="formsubbutton" value="&lt;&lt;" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" type="button" onclick="javascript:check(document.form1.elements['target[]'],document.form1.elements['source[]'])">

		</td>
		<td nowrap="nowrap" valign="top">
		
		  <table border="0" cellpadding="3" cellspacing="0" width="100%">
                <tbody>
                			<tr>
                             <td>
                                  <p><span class="tablerow2">
                                                      <select name="target[]" size=15 class="select_studentlist" style="width: 200px;" multiple>
																<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
													</select>
                                     </span></p>
                               </td>
                             </tr>
                                    <tr>
                         				 <td class="tabletextremark" align="left"><?= $ec_iPortfolio_guide['group_growth'] ?></td>
                     				</tr>
              </tbody>
          </table>
		
		</td>
		</tr>
		</table>
          </td>
        </tr>
        </table>
</td>
<td>&nbsp;</td>
</tr>
  <tr>
    <td colspan="3"><br></td>
  </tr>
<tr>
		 <td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
         <td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>
	<table border=0 width=100%>
	<tbody>
	<tr>
		<td class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td>
  		<td align="right">
  				<input class="formbutton" type="submit" value="<?=$button_confirm?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
				<input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
				<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="javascript:doCancel()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"></td>
         </td>
	</tr>
	</tbody>
	</table>
	</td>
    
    <td>&nbsp;</td>
</tr>
</table>

</form>

<?php
echo autoFocusField("title");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
