<?php
include_once("../../../system/settings/global.php");
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing"));
auth("T");
include_once("$admin_root_path/src/portfolio/new_header.php");

if(strstr($ck_function_rights, "Sharing:Content"))
{
	$mContent = "<tr>
              <td><table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_g'>
                  <tr>
                    <td align='center'><a href='index_scheme.php' class='link_a'><font size='+1'><img src='$sr_image_path/gimg/control_big_btn.gif' width='38' height='30' border='0' align='absmiddle'> ".$ec_iPortfolio['learning_portfolio_content']."</font></a></td>
                  </tr>
              </table></td>
            </tr>
			";
}
if(strstr($ck_function_rights, "Sharing:Template"))
{
	$mTemplate = "<tr>
              <td><table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_g'>
                  <tr>
                    <td align='center'><a href='templates_manage.php' class='link_a'><font size='+1'><img src='$sr_image_path/gimg/control_big_btn.gif' width='38' height='30' border='0' align='absmiddle'> ".$ec_iPortfolio['manage_templates']."</font></a></td>
                  </tr>
              </table></td>
            </tr>
			";
}
if(strstr($ck_function_rights, "Sharing:PeerReview"))
{
	$mPeerReview = "<tr>
              <td><table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_g'>
                  <tr>
                    <td align='center'><a href='learning_portfolio_setting.php' class='link_a'><font size='+1'><img src='$sr_image_path/gimg/control_big_btn.gif' width='38' height='30' border='0' align='absmiddle'> ".$ec_iPortfolio['peer_review_setting']."</font></a></td>
                  </tr>
              </table></td>
            </tr>
			";
}
if(strstr($ck_function_rights, "Sharing:CDBurning"))
{
	$mCDBurning = "<tr>
              <td><table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_g'>
                  <tr>
                    <td align='center'><a href='prepare_CDburning_ClassList.php' class='link_a'><font size='+1'><img src='$sr_image_path/gimg/control_big_btn.gif' width='38' height='30' border='0' align='absmiddle'> ".$ec_iPortfolio['prepare_cd_burning']."</font></a></td>
                  </tr>
              </table></td>
            </tr>
			";
}

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "")
					);
$template_width = "600";

echo getBodyBeginning($template_pages, $template_width);

?>




<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="80%" border="0" cellpadding="0" cellspacing="0" class="table_b">
      <tr>
        <td align="center" valign="middle"><table width="95%" height="90%" border="0" cellpadding="0" cellspacing="10" >
			<?=$mContent?>
			<?=$mTemplate?>
			<?=$mPeerReview?>
			<?=$mCDBurning?>
        </table></td>
      </tr>
    </table></td>
	</tr>
</table>



<?= getPageEnding() ?>


<?php
include_once("$admin_root_path/src/portfolio/new_footer.php");
?>