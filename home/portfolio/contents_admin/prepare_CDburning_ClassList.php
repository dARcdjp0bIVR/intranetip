<?php

// Modifying by 

include_once('../../../system/settings/global.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:CDBurning") || !strstr($ck_user_rights, ":web:"));
auth("T");
include_once("$admin_root_path/src/includes/php/lib-db.php");
include_once("$admin_root_path/src/includes/php/lib-portfolio.php");
include_once("$admin_root_path/src/includes/php/lib-notes.php");
include_once("$admin_root_path/src/includes/php/lib-notes_portfolio.php");
include_once("$admin_root_path/src/includes/php/lib-filesystem.php");
include_once("$admin_root_path/src/portfolio/new_header.php");

opendb();

$li_pf = new portfolio();

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "main.php"),
					Array($ec_iPortfolio['prepare_cd_burning'], "")
				);

$template_width = "95%";

$class_table_html = $li_pf->getClassListTable("", 3);
	
echo getBodyBeginning($template_pages, $template_width);

?>
<script language="javascript">
function prepareBurning(class_name)
{
	self.location = "prepare_CDburning.php?ClassName="+class_name;
}
</script>



<br>
<?php

echo $class_table_html;

?>



<?= getPageEnding() ?>

<?php
closedb();
include_once("$admin_root_path/src/portfolio/new_footer.php");
?>
