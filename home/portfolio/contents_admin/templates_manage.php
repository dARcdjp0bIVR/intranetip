<?php
if ($page_size_change!="") { setcookie("ck_page_size", $numPerPage, 0, "", "", 0);}

include_once('../../../system/settings/global.php');
include_once('../../includes/php/lib-db.php');
include_once('../../includes/php/lib-table.php');
include_once('../../../system/settings/lang/'.$ck_default_lang.'.php');
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Template") || !strstr($ck_user_rights, ":web:"));
auth("TA");
opendb();

include_once("../new_header.php");

$li = new libdb();
if($order=="") $order=0;
if($field=="") $field=0;
$li = new libtable($field, $order, $pageNo);
$li->field_array = array("f.Title", "used_num", "f.IsSuspend", "f.DateModified");

$fieldname  = "CONCAT('<a class=\"link_a\" href=\"javascript:editTemplate(\'', CONCAT(IFNULL(f.VirPath, '/'), f.Title), '\')\" >',f.Title,'<a>'), ";
$fieldname .= "COUNT(n.notes_id) as used_num, ";
$fieldname .= "IF(f.IsSuspend=1, '$status_suspended', '$status_active'), ";
$fieldname .= "f.DateModified, ";
$fieldname .= "CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\'file_ids[]\' value=\"', f.FileID,'\" />', if(COUNT(n.notes_id)=0, '', '*')) ";
$sql  = "SELECT $fieldname FROM eclass_file as f LEFT JOIN notes as n ON n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title) WHERE f.VirPath='/TEMPLATES/' AND f.Category='0' AND f.IsDir<>1 GROUP BY f.FileID";

// TABLE INFO
$li->sql = $sql;
$li->db = classNamingDB($ck_course_id);
$li->title = $ec_iPortfolio['template'];
$li->no_msg = $no_record_msg;
$li->page_size =
($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = 6;

$li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
$li->row_alt = array("#FFFFFF", "#EEEEEE");
$li->row_height = 40;
$li->plain_column = 1;
$li->plain_column_css = "link_a";

// TABLE COLUMN
$li->column_list .= "<td height='35' width='5%' align='center' bgcolor='#D6EDFA'  class='link_chi' style='border-bottom:2px solid #7BC4EA;'>#</td>\n";
$li->column_list .= "<td width='35%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(0, $ec_iPortfolio['weblog_title'], 1)."</td>\n";
$li->column_list .= "<td width='25%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(1, $ec_iPortfolio['number_of_portfolio_used'], 1)."</td>\n";
$li->column_list .= "<td width='10%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(2, $ec_iPortfolio['status'], 1)."</td>\n";

$li->column_list .= "<td width='20%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->column(3, $ec_iPortfolio['last_modified'], 1)."</td>\n";
$li->column_list .= "<td width='5%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$li->check("file_ids[]")."</td>\n";
$li->column_array = array(0,0,0,0,0,0);

# define the navigation, page title and table size
$template_pages = Array(
					Array($ck_custom_title['web'], "main.php"),
					Array($ec_iPortfolio['manage_templates'], "")
					);
$template_width = "95%";

if($msg==7)
{
	// retreived the file title of files which could not be deleted;
	$sql = "SELECT 
				f.Title
			FROM 
				eclass_file as f 
			WHERE 
				f.FileID IN ($failedID)
			";
	$row = $li->returnVector($sql);
	
	$temp_string = implode(", ", $row);
	
	$con_msg = "<table><tr><td class='bodycolor2'>&nbsp;&nbsp;<span class='notice2'>\n";
	$con_msg .= $con_msg_del_fail."<br />&nbsp;&nbsp;&nbsp;<font color='#333355'>".$temp_string."</font></span></td></tr></table>";
}

echo getBodyBeginning($template_pages, $template_width);

?>

<script language="javascript">
var globalAlertMsg8 = "<?=$ec_iPortfolio['suspend_template_confirm']?>";
var globalAlertMsg12 = "<?=$ec_iPortfolio['active_template_confirm']?>";

function addTemplate(){
	var url = "../../tool/misc/html_editor/index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file&frManage=1";
	newWindow(url,21);
}

function previewTemplate(flink){
	newWindow("../../course/resources/files/open.php?fpath="+flink, 23);
}

function editTemplate(flink){

newWindow("../../tool/misc/html_editor/index.php?comeFrom=CC_templates&categoryID=0&fieldname=template_file&frManage=1&attachment="+flink, 21);

}
</script>



<form name="form1" method="post" action="templates_manage.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="2">
	  <?php
	  if($msg==7)
		echo $con_msg;
	  else
		echo getSystemMessage($msg, $err);
	  ?>
	  </td>
    </tr>
    <tr>
      <td class="13Gray" nowrap><a href="javascript:addTemplate()" class="link_a"><img src="<?= $sr_image_path ?>/Icon/icon_Add.gif" hspace="4" border="0" align="absmiddle" alt='<?= $button_new ?>'><?= $button_new ?></a></td>
      <td class="13Gray" nowrap align="right">
	  <a href="javascript:document.form1.suspend.value=1;checkPick(document.form1,'file_ids[]','template_suspend.php',1)" onMouseOut='MM_swapImgRestore()' onMouseOver="MM_swapImage('buttonSuspend','','<?=$sr_image_path?>/Buttons/btn_suspendTemplate_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_suspendTemplate_D.gif" name='buttonSuspend' border='0' id='buttonSuspend'></a>
	  <a href="javascript:document.form1.suspend.value=0;checkPick(document.form1,'file_ids[]','template_suspend.php',2)" onMouseOut='MM_swapImgRestore()' onMouseOver="MM_swapImage('buttonActive','','<?=$sr_image_path?>/Buttons/btn_ActiveTemplate_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_ActiveTemplate_D.gif" name='buttonActive' border='0' id='buttonActive'></a>
	  <a href="javascript:checkEdit(document.form1,'file_ids[]','template_copy.php')" onMouseOut='MM_swapImgRestore()' onMouseOver="MM_swapImage('buttonCopy','','<?=$sr_image_path?>/Buttons/btn_Copy_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Copy_D.gif" name='buttonCopy' border='0' id='buttonCopy'></a>
	  <a href="javascript:checkRemove(document.form1,'file_ids[]','template_remove.php');" onMouseOut='MM_swapImgRestore()' onMouseOver="MM_swapImage('buttonDelete','','<?=$sr_image_path?>/Buttons/btn_Delete_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Delete_D.gif" name='buttonDelete' border='0' id='buttonDelete'></a>
      </td>
    </tr>
	<tr><td colspan="2" align="right" valign="bottom" class='guide' height="30">*<?=$ec_iPortfolio['template_deletion_remind']?></td></tr>
    <tr>
      <td colspan="2" height="2"><img src="<?=$sr_image_path?>/spacer.gif" width="10" height="5"></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
		<tr>
          <td align="center" valign="middle"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          	<tr><td><?= $li->displayPlain() ?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
						<tr>
						  <td background="<?=$sr_image_path?>/insidepage/pagedownline.gif">&nbsp;</td>
						</tr>
						<?php
						  if ($li->navigationHTML!="")
						  {
						  ?>
							<tr>
							  <td align="right"><?=$li->navigation(1)?></td>
							</tr>
						  <?php
						  }
						  ?>
					</table>
						</td>
			   </tr>
			  </table>
				 </td>
		  </tr>
		</table>
          </td>
      </tr>
	</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name="page_size_change">
<input type=hidden name="suspend" />
</form>


<?= getPageEnding() ?>

<?php
closedb();
include_once("../new_footer.php");
?>
