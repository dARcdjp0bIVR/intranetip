<?php
// Modifying by

/**
 * ************************************************
 *  Date:   2020-06-29 [Bill]   [2020-0612-1004-37207]
 *          - show score if Score > 0 + No Grade + No Ranking   ($sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'])
 *  Date:   2020-05-15 [Bill]   [DM#3749]
 *          - fixed implode() related error msg
 *  Date:   2020-02-06 [Bill]   [2020-0206-1015-53073]
 *          - fixed PHP 5.4 Call-time pass-by-reference error
 *          - enabled js function jCHANGE_DISPLAY()
 * ************************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

// Others
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/json.php");

// include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

############################# Function Start  #########################################
function GEN_MISC_PARAMETER($mainSubjCode, $compSubjCode, $displayBy, $yearFilter=0)
{
  return base64_encode("MainSubjectCode={$mainSubjCode}&CompSubjectCode={$compSubjCode}&displayBy={$displayBy}&yearFilter={$yearFilter}");
}

function getLinkArr($fileExist, $configArr, $mainSubjCode, $compSubjCode, $displayBy, $yearFilter=0)
{
  global $ec_iPortfolio;

  $link_info =  array(
                  "Score" => $ec_iPortfolio['display_by_score'],
                  "Rank" => $ec_iPortfolio['display_by_rank'],
                  "StandardScore" => $ec_iPortfolio['display_by_stand_score']
                );

  $link_arr = array();
  foreach($link_info AS $code => $displayName)
  {
    if(!$fileExist || (is_array($configArr) && in_array($code, $configArr)))
    {
  	  $link_arr[] = ($code==$displayBy) ? "<span>{$displayName}</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($mainSubjCode, $compSubjCode, $code, $yearFilter)."')\" class='link_a'>{$displayName}</a>";
  	}
  }

  return $link_arr;
}
################################ End #####################################

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");

// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$lpf = new libpf_sturec();
$lpf_ui = new libportfolio2007();
$luser = new libuser($UserID);
$json = new JSON_obj();

//$lpf->accessControl("assessment_report");
switch($_SESSION['UserType'])
{
	case 2:
		$default_student_id = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		break;
	case 3:
		$default_student_id = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}
	
$params = "ClassName={$ClassName}&StudentID={$StudentID}";

//list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);
/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);

	# define the navigation, page title and table size
	$template_pages = Array(
				Array($ec_iPortfolio['assessment_report'], "index.php?displayBy=".$displayBy),
				Array($ec_iPortfolio['display_stat_report'], "")
				);
	
}
else if($ck_memberType=="P")
{
	# define the navigation, page title and table size
	$template_pages = Array(
					Array($ec_iPortfolio['student_list'], "../../school_records_children.php"),
					Array($ec_iPortfolio['assessment_report'], "index.php?ClassName=$ClassName&StudentID=$StudentID&displayBy=$displayBy"),
					Array($ec_iPortfolio['display_stat_report'], "")
				);
}
else
{
	# define the navigation
	if($ck_is_alumni)
	{
		$template_pages = Array(
					Array($ec_iPortfolio['alumni_list'], "../../school_records_alumni.php"),
					Array($ck_alumni_year, "../../school_records_alumni_year.php?my_year=$ck_alumni_year"),
					Array($ec_iPortfolio['assessment_report'], "index.php?$params&displayBy=$displayBy"),
					Array($ec_iPortfolio['display_stat_report'], "")
					);
	}
	else
	{
		$template_pages = Array(
					Array($ec_iPortfolio['class_list'], "../../school_records.php"),
					Array($ec_iPortfolio['student_list'], "../../school_records_class.php?ClassName=$ClassName"),
					Array($ec_iPortfolio['assessment_report'], "index.php?$params&displayBy=$displayBy"),
					Array($ec_iPortfolio['display_stat_report'], "")
					);
	}

}
*/

//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[2][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);

##########################################################################################
##########################################################################################
##########################################################################################
$yearTitle = ($yearFilter==0) ? $ec_iPortfolio['term'] :  $ec_iPortfolio['year'];

// Default values of parameters
$displayBy = "Score";
$yearFilter = 0;      // 0: Semester, 1: Year

// Restructure parameters
$miscParameterArr = explode("&", base64_decode($miscParameter));
for($i=0, $i_max=count($miscParameterArr); $i<$i_max; $i++)
{
	list($var_name, $var_val) = explode("=", $miscParameterArr[$i]);
	${$var_name} = $var_val;
}

# define the buttons according to the variable $displayBy
$SubjectParam = "MainSubjectCode={$MainSubjectCode}&CompSubjectCode={$CompSubjectCode}";
//$link_score = ($displayBy=="Score") ? "<span class='link_a'>".$ec_iPortfolio['display_by_score']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "Score", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";
//$link_rank = ($displayBy=="Rank") ? "<span class='link_a'>".$ec_iPortfolio['display_by_rank']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "Rank", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_rank']."</a>";
//$link_stand_score = ($displayBy=="StandardScore") ? "<span class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</span>" : "<a href=\"javascript:jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, "StandardScore", $yearFilter)."')\" class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</a>";
$school_record_config_file = "$eclass_root/files/school_record_config.txt";

$li_fs = new libfilesystem();
$config_arr = unserialize($li_fs->file_read($school_record_config_file));

$link_arr = getLinkArr(file_exists($school_record_config_file), $config_arr["academic_record_display"], $MainSubjectCode, $CompSubjectCode, $displayBy, $yearFilter);

# Create selection (by semester/ by year)
$filter_selection = "<td nowrap='nowrap'><input name='yearFilter' id='yearFilterYear' value='1' type='radio' ".($yearFilter==1?"CHECKED='CHECKED'":"")." onClick=\"jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, 1)."')\"> <label for=\"yearFilterYear\">{$ec_iPortfolio['by_year']}</label></td>\n";
$filter_selection .= "<td nowrap='nowrap' valign='top'><input name='yearFilter' id='yearFilterSem' value='0' type='radio' ".($yearFilter==0?"CHECKED='CHECKED'":"")." onClick=\"jCHANGE_DISPLAY('".GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, 0)."')\"> <label for=\"yearFilterSem\">".$ec_iPortfolio['by_semester']."</label></td>\n";

switch($displayBy)
{
  case "Score":
    //$retrieveField = "IF(Score <= 0, IF(Grade = '' OR Grade IS NULL, '', Score), TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))))";
    $retrieveField = "CASE ";
    $retrieveField .= "WHEN Score < 0 THEN '--' ";
    $retrieveField .= "WHEN Score = 0 THEN IF(Grade = '' OR Grade IS NULL, '--', Score) ";
    $retrieveField .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))) ";
    $retrieveField .= "END";
    $totalTitle = $ec_iPortfolio['overall_score'];
    
    if ($CompSubjectCode == '') {
    	$conds_subjectCmpCode = " AND (SubjectComponentCode = '' OR SubjectComponentCode IS NULL) ";
    }
    else {
    	$conds_subjectCmpCode = " AND SubjectComponentCode = '{$CompSubjectCode}' ";
    }

    //$maxScore = current($lpf->returnVector("SELECT MAX(Score) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' AND SubjectComponentCode = '{$CompSubjectCode}'"));
    $maxScore = current($lpf->returnVector("SELECT MAX(Score) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' $conds_subjectCmpCode "));
    $y_axis_Max = ceil($maxScore/100) * 100;
    $y_axis_Min = 0;
    $y_axis_Step = round($y_axis_Max / 100) * 10;

    break;

  case "Rank":
    $retrieveField = "IF(IFNULL(OrderMeritForm, 0) = 0, '--', OrderMeritForm)";
    $totalTitle = $ec_iPortfolio['overall_rank'];
    
    if ($CompSubjectCode == '') {
    	$conds_subjectCmpCode = " AND (SubjectComponentCode = '' OR SubjectComponentCode IS NULL) ";
    }
    else {
    	$conds_subjectCmpCode = " AND SubjectComponentCode = '{$CompSubjectCode}' ";
    }

    //$maxRank = current($lpf->returnVector("SELECT IF(MAX(OrderMeritFormTotal) > OrderMeritFormTotal, MAX(OrderMeritFormTotal), OrderMeritFormTotal) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' AND SubjectComponentCode = '{$CompSubjectCode}'"));
    $maxRank = current($lpf->returnVector("SELECT IF(MAX(OrderMeritFormTotal) > OrderMeritFormTotal, MAX(OrderMeritFormTotal), OrderMeritFormTotal) FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE SubjectCode = '{$MainSubjectCode}' $conds_subjectCmpCode "));
    $y_axis_Max = 1;
    $y_axis_Min = ceil($maxRank/10) * 10;
    $y_axis_Step = round($y_axis_Min / 100) * 10;

    break;

  case "StandardScore":
    $totalTitle = $ec_iPortfolio['overall_stand_score'];
    $y_axis_Max = 5;
    $y_axis_Min = -5;
    $y_axis_Step = 1;

    break;
}

// [2020-0612-1004-37207] Show Score if Score > 0 + No Grade + No Ranking
$alwaysDisplayScoreIfValid = $sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'];

$htmlNavArea = implode(" | ", (array)$link_arr);
 
//since accessment Nav Area with a speical layout
$htmlNavStyle = "align=\"right\" valign=\"middle\" class=\"thumb_list\"";
		
// Records of class history
$classHistoryArr = $lpf->getAssessmentClassHistoryArr($default_student_id);

// Prepare for:
// i) table header (year, class, semester)
// ii) partial query strings of temporary table
$yearClassStr = "";
$semStr = "";
$semCount = 0;
for($i=0, $i_max=count($classHistoryArr); $i<$i_max; $i++)
{
  $_ayID = $classHistoryArr[$i]["AcademicYearID"];
  $_lay = new academic_year($_ayID);
  $_year = $_lay->Get_Academic_Year_Name();
  $_year = empty($_year) ? $classHistoryArr[$i]["Year"] : $_year;

  $_ytID = $classHistoryArr[$i]["YearTermID"];
  $_layt = new academic_year_term($_ytID);
  $_sem = $_layt->Get_Year_Term_Name();
  $_sem = empty($_sem) ? $classHistoryArr[$i]["Semester"] : $_sem;
  $_className = $classHistoryArr[$i]["ClassName"];
  
  $ayArr[$_ayID] = array("yearname"=>$_year, "classname"=>$_className);
  $semArr[$_ayID][$_ytID] = $_sem;
}

if(is_array($ayArr))
{
  $insertAssessTableSqlArr = array();
  foreach($ayArr AS $ayID => $ayDetailArr)
  {
    $_yearName = $ayDetailArr["yearname"];
    $_className = $ayDetailArr["classname"];
  
    $yearClassStr .= "<td width='15%' colspan='".count($semArr)."' nowrap='nowrap' align='center'>{$_yearName}<br />{$_className}</td>\n";
    if(is_array($semArr))
    {
      $subjOverallRes = (array_key_exists(0, $semArr[$ayID]) || array_key_exists("", $semArr[$ayID]));
      
      foreach($semArr[$ayID] AS $ytID => $_ytName)
      {
        if($ytID == 0 || $ytID == "") continue;   // "" => annual result, put to last row of the year
        
        $assessTableField .= ", Score_{$ayID}_{$ytID} varchar(16)";
        
        // Special handling for standard score since it needs system calculation
        if($displayBy == "StandardScore")
        {
          // Subject records
          $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $default_student_id, $ayID, $ytID));
          
          $selectAssessTableSql .= ", IF(t_am.Score_{$ayID}_{$ytID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID}_{$ytID} - t_aam.Score_{$ayID}_{$ytID})/t_asdm.Score_{$ayID}_{$ytID}, 2), '--')) AS Score_{$ayID}_{$ytID}";
        }
        else
        {
          // Subject records
          //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveField, $default_student_id, $ayID, $ytID);
          $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveField, $default_student_id, $ayID, $ytID, false, $alwaysDisplayScoreIfValid);
          
          $selectAssessTableSql .= ", Score_{$ayID}_{$ytID}";
        }
      }

      $assessTableField .= ($subjOverallRes) ? ", Score_{$ayID} varchar(16)" : "";
      
      // Special handling for standard score since it needs system calculation
      if($displayBy == "StandardScore")
      {
        // Subject records
        $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $default_student_id, $ayID, ""));
        
        $selectAssessTableSql .= ($subjOverallRes) ? ", IF(t_am.Score_{$ayID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID} - t_aam.Score_{$ayID})/t_asdm.Score_{$ayID}, 2), '--')) AS Score_{$ayID}" : ", '--' AS Score_{$ayID}";
      }
      else
      {
        // Subject records
        //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveField, $default_student_id, $ayID, "");
        $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveField, $default_student_id, $ayID, "", false, $alwaysDisplayScoreIfValid);
        
        $selectAssessTableSql .= ($subjOverallRes) ? ", Score_{$ayID}" : "";
      }
    }
  }
}

// table object
// Temp table of assessment records
// Table name : tempAssessmentMark
if($displayBy == "StandardScore")
{
  // [2020-0206-1015-53073] fixed PHP 5.4 Call-time pass-by-reference error
  // $lpf->createTempAssessmentSDScoreTable(&$lpf, $assessTableField, $insertAssessTableSqlArr);
  $lpf->createTempAssessmentSDScoreTable($lpf, $assessTableField, $insertAssessTableSqlArr);

  $score_sql = "SELECT ";
  $score_sql .= substr($selectAssessTableSql, 2)." ";
  $score_sql .= "FROM tempAssessmentMark t_am ";
  $score_sql .= "INNER JOIN tempAssessmentAvgMark t_aam ON t_am.SubjectCode = t_aam.SubjectCode AND t_am.SubjectComponentCode = t_aam.SubjectComponentCode ";
  $score_sql .= "INNER JOIN tempAssessmentSDMark t_asdm ON t_am.SubjectCode = t_asdm.SubjectCode AND t_am.SubjectComponentCode = t_asdm.SubjectComponentCode ";
  $score_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
  $score_sql .= "WHERE t_am.SubjectCode = '{$MainSubjectCode}' AND t_am.SubjectComponentCode = '{$CompSubjectCode}'";
}
else
{
  // [2020-0206-1015-53073] fixed PHP 5.4 Call-time pass-by-reference error
  // $lpf->createTempAssessmentMarkTable(&$lpf, $assessTableField, $insertAssessTableSqlArr);
  $lpf->createTempAssessmentMarkTable($lpf, $assessTableField, $insertAssessTableSqlArr);

  $score_sql = "SELECT ";
  $score_sql .= substr($selectAssessTableSql, 2)." ";
  $score_sql .= "FROM tempAssessmentMark ";
  $score_sql .= "WHERE SubjectCode = '{$MainSubjectCode}' AND SubjectComponentCode = '{$CompSubjectCode}'";
}

// Temp table of subjects
// Table name : tempAssessmentSubject
// $lpf->createTempAssessmentSubjectTable(&$lpf);
// [2020-0206-1015-53073] fixed PHP 5.4 Call-time pass-by-reference error
$lpf->createTempAssessmentSubjectTable($lpf);

// Score data for chart / table
$t_score_arr = current($lpf->returnArray($score_sql));
if(is_array($t_score_arr))
{
  foreach($t_score_arr AS $key => $val)
  {
    $year_sem_arr = explode("_", $key);
  
    switch(count($year_sem_arr))
    {
      case 2:
        $ayID = $year_sem_arr[1];
        $lay = new academic_year($ayID);
        $x_legend_arr['year'][] = $lay->Get_Academic_Year_Name();

        switch($val)
        {
          case "":
            $score_arr['year'][] = "";
            $line_dot_arr['year'][] = null;
            break;
          case "--":
            $score_arr['year'][] = "--";
            $line_dot_arr['year'][] = null;
            break;
          default:
            $score_arr['year'][] = (float) $val;
            $line_dot_arr['year'][] = (float) $val;
            break;
        }
        break;

      case 3:
        $ayID = $year_sem_arr[1];
        $lay = new academic_year($ayID);
      
        $ytID = $year_sem_arr[2];
        $layt = new academic_year_term($ytID);
        $x_legend_arr['year_term'][] = $layt->Get_Year_Term_Name()."\n".$lay->Get_Academic_Year_Name();

        switch($val)
        {
          case "":
            $score_arr['year_term'][] = "";
            $line_dot_arr['year_term'][] = null;
            break;
          case "--":
            $score_arr['year_term'][] = "--";
            $line_dot_arr['year_term'][] = null;
            break;
          default:
            $score_arr['year_term'][] = (float) $val;
            $line_dot_arr['year_term'][] = (float) $val;
            break;
        }
        break;

      // Not accept numeric keys
      default:
        break;
    }
  }
}

// Subject selection
$sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
$sql .= "FROM tempAssessmentMark t_am ";
$sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID != '' AND t_am.SubjectCodeID IS NOT NULL, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
$sql .= "ORDER BY t_as.MainSubjOrder, t_as.CompSubjOrder";
$t_subject_arr = $lpf->returnArray($sql);

for($i=0, $i_max=count($t_subject_arr); $i<$i_max; $i++)
{
  $_mainSubjCode = $t_subject_arr[$i]["MainSubjCode"];
  $_compSubjCode = $t_subject_arr[$i]["CompSubjCode"];
  $_subjName = ($_compSubjCode == "") ? $t_subject_arr[$i]["SubjName"] : "&nbsp;&nbsp;&nbsp;{$t_subject_arr[$i]["SubjName"]}";
  
  $subjectArr[] = array(
                    GEN_MISC_PARAMETER($_mainSubjCode, $_compSubjCode, $displayBy, $yearFilter),
                    $_subjName
                  );
}
$default_miscParameter = GEN_MISC_PARAMETER($MainSubjectCode, $CompSubjectCode, $displayBy, $yearFilter);
$subject_selection_html = getSelectByArray($subjectArr, "onChange=\"jCHANGE_DISPLAY(this.value)\"", $default_miscParameter, 0, 1, "", 2);

// Data table
$data_table_html = "<table bgcolor=\"#cccccc\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"100%\">\n";
$data_table_html .= "<tr class=\"tabletop\">\n";
$data_table_html .= "<td class=\"tabletopnolink\" width=\"50%\">{$yearTitle}</td>\n";
$data_table_html .= "<td class=\"tabletopnolink\" align=\"center\" width=\"50%\">{$totalTitle}</td>\n";
$data_table_html .= "</tr>\n";

if($yearFilter == 0)
{
  $x_legend_arr = $x_legend_arr['year_term'];
  $x_axis_title = $ec_iPortfolio['semester'];
  $line_dot_arr = $line_dot_arr['year_term'];
  
  for($i=0, $i_max=count($score_arr['year_term']); $i<$i_max; $i++)
  {
    $_yt = str_replace("\n", " : ", $x_legend_arr[$i]);
    $_score = $score_arr['year_term'][$i];
    
    $data_table_html .= "<tr><td>{$_yt}</td><td align=\"center\">{$_score}</td></tr>";
  }
}
else
{
  $x_legend_arr = $x_legend_arr['year'];
  $x_axis_title = $ec_iPortfolio['year'];
  $line_dot_arr = $line_dot_arr['year'];
  
  for($i=0, $i_max=count($score_arr['year']); $i<$i_max; $i++)
  {
    $_ay = $x_legend_arr[$i];
    $_score = $score_arr['year'][$i];
    
    $data_table_html .= "<tr><td>{$_ay}</td><td align=\"center\">{$_score}</td></tr>";
  }
}
$data_table_html .= "</table>";

# flash chart
// a dummy title to enlarge height to display top of the chart
// $title = new title( " " );

// $x_legend = new x_legend( $x_axis_title );
// $x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

// $x = new x_axis();
// $x->set_stroke( 2 );
// $x->set_tick_height( 2 );
// $x->set_colour( '#999999' );
// $x->set_grid_colour( '#CCCCCC' );
// $x->set_labels_from_array( $x_legend_arr );

// $y = new y_axis();
// $y->set_stroke( 2 );
// $y->set_tick_length( 2 );
// $y->set_colour( '#999999' );
// $y->set_grid_colour( '#CCCCCC' );
// $y->set_range( $y_axis_Min, $y_axis_Max, $y_axis_Step );
// $y->set_offset(true);

// $line0 = new line_dot();
// $line0->set_dot_size(5);
// $line0->set_values( $line_dot_arr );
// $line0->set_colour( '#72a9db' );
// $line0->set_tooltip( $totalTitle.':#val#' );
// $line0->set_key( $totalTitle, '12' );
// $line0->set_id( 0 );
// $line0->set_visible( true );

// $tooltip = new tooltip();
// //$tooltip->set_proximity();
// $tooltip->set_hover();
// $tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#ff0000" );
//$tooltip->set_background_colour( "#ff00ff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

// $chart = new open_flash_chart();
// $chart->set_bg_colour( '#FFFFFF' );
// $chart->set_title( $title );
// $chart->set_x_legend( $x_legend );
// $chart->set_x_axis( $x );
// $chart->set_y_axis( $y );
// $chart->add_element( $line0 );
// $chart->set_tooltip( $tooltip );

$xAxis= $json->encode(str_replace("\n"," ",$x_legend_arr));
$data = implode(',', (array)$score_arr['year_term']);

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
# Generate menu tab
$availTab = libpf_tabmenu::getSchoolRecordTags("assessment_report");
$html_menu_tab = $lpf->GET_TAB_MENU($availTab);

echo $linterface->Include_HighChart_JS();
?>

<script type="text/javascript">
xAxis = '<?=$xAxis?>';
xAxis = JSON.parse(xAxis);

$(function () {
    $('#my_chart').highcharts({
        title: {
            text: ''
            // x: -20 //center
        },
//         subtitle: {
//             text: 'Source: WorldClimate.com',
//             x: -20
//         },
        xAxis: {
            categories:xAxis,
            title: {
                text: '<?= $x_axis_title = $ec_iPortfolio['semester']?>'
            },
        	plotLines: [{
//             	value: 0,
//             	width: 1,
            	color: '#CCCCCC'
       		}]
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
//         tooltip: {
//             valueSuffix: '°C'
//         },
        legend: {
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name:' ',
            data: [<?=$data?>]
        }],
        exporting: { enabled: false },
        credits: {
            enabled: false
        }
    });
});
</script>

<script langauge="JavaScript">
function jCHANGE_DISPLAY(jMiscPara){
	document.form1.miscParameter.value = jMiscPara;
	document.form1.action = "stat2.php";
	document.form1.submit();
}
</script>

<!-------------------- Flash Chart -------------------->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">
// function ofc_ready(){
// 	//alert('ofc_ready');
// }

// function open_flash_chart_data(){
// 	//alert( 'reading data' );
// 	return JSON.stringify(data);

// 	//return "{\"elements\":[{\"type\":\"line_dot\",\"values\":[4,3,4,1,0,1],\"width\":2,\"colour\":\"#00FF21\",\"tip\":\"#val#\",\"id\":\"component\"}],\"bg_colour\":\"#ffffff\",\"title\":{\"text\":\"adam\",\"style\":\"{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}\"},\"radar_axis\":{\"max\":5,\"colour\":\"#C1CBFF\",\"grid-colour\":\"#C1CBFF\",\"spoke-labels\":{\"labels\":[\"Like Green Color?\",\"Like Pink Color?\",\"Like Boy?\",\"Like Girl?\",\"Like Old Man?\",\"Like Young Woman?\"],\"colour\":\"#9F819F\"}},\"tooltip\":{\"mouse\":1}}";

	

// }

// function findSWF(movieName) {
//   if (navigator.appName.indexOf("Microsoft")!= -1) {
// 	return window[movieName];
//   } else {
// 	return document[movieName];
//   }
// }

// function setChart(id, display){
// 	//for testing
// 	var heading = document.getElementById("testHead");
// 	while(heading.hasChildNodes()){
// 		heading.removeChild(heading.firstChild);
// 	}
// 	var h = document.createTextNode(String(id));
// 	heading.appendChild(h);
// 	var content = document.getElementById("testContent");
// 	while(content.hasChildNodes()){
// 		content.removeChild(content.firstChild);
// 	}			
// 	var c = document.createTextNode(String(display));
// 	content.appendChild(c);
// }
	
//var data = <?php
//echo $chart->toPrettyString(); ?>;
</script>

<script type="text/javascript">
//swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "my_chart", "450", "340", "9.0.0");
</script>

<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tab_underline">
              <div align="center"><?=$html_menu_tab?></div>
            </td>
          </tr> 
          <tr>
            <td align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <img src="<?=$PATH_WRT_ROOT?>/images/2009a/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><a href="index.php"><?=$ec_iPortfolio['overall_summary']?></a>
                    <img src="<?=$PATH_WRT_ROOT?>/images/2009a/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><?=$ec_iPortfolio['subject_chart']?>
                  </td>
                </tr>
                <tr>
                  <td><?=$SubjectSelection?></td>
                  <td width='700' align="right" valign="middle" class="thumb_list"><?=$htmlNavArea?></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td align="center" valign="middle">
                    <div id="my_chart" style="min-width: 310px; height: 400px; margin: 0 auto">no flash?</div>
                  </td>
                
                  <td align="left" valign="top">
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                    
                      <tr align="left" valign="middle">
                        <td>&nbsp;</td>
                      </tr>
                      <tr align="left" valign="middle">
                        <td>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr class="tablebottom">
                              <td><?=$ec_iPortfolio['show_by']?></td>
                              <?=$filter_selection?>
                            </tr>
                        </table>
                        </td>
                      </tr>

                      <!-- Display Table Content -->
                      <tr align="left">
                        <td valign="top">
                          <table  border="0" cellpadding="0" cellspacing="0" class="table_p" width="100%">
						 	<tr>
								<td>
									<?=$data_table_html?>
								</td>
							</tr>
						</table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="StudentID" value="<?=$StudentID?>">
  <input type="hidden" name="displayBy" value="<?=$displayBy?>">
  <input type="hidden" name="miscParameter" />
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>