<?php
// Modifing by yuen


/** 
 ********************************************************* 
 * 
 *	2013-07-08	Yuen 
 *	- support assessments
 *
 *********************************************************/
 
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();
$linterface = new interface_html("popup.html");
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' onChange='jLOAD_SEMESTER()'", "", 0, 1, "", 2);

$CurrentPage = "SAMS_import_assessment";
$title = $ec_iPortfolio['SAMS_import_assessment'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

# testing only
//$lpf->ComputeSDMean(1, 9, 24);
//$lpf->ComputeSDMean(1, 9, 23);
//$lpf->ComputeSDMean(1, 9, NULL);

$linterface->LAYOUT_START();

?>

<script language="JavaScript">
function jGEN_CSV_TEMPLATE()
{
  if($("[name=academicYearID]").val() == "")
  {
  }
  else
  {
    newWindow('generate_csv_template.php?YearTermID='+$("select[name=YearTermID]").val(), 7);
  }
}

function jLOAD_SEMESTER()
{
  var ay_id = $("[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#ayterm_cell").html('&nbsp;');
    return;
  }
  
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id,
		async: false,
		beforeSend: function () {
		  $("#ayterm_cell").html('<img src="/images/2009a/indicator.gif" />');
    },
		success: function (msg) {
      if(msg != "")
      {
        $("#ayterm_cell").html(msg);
        $("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['overall_result']?>");
      }
		}
	});
}

$(document).ready(function(){
  jLOAD_SEMESTER();
  
  $("#form1").submit(function(){
    $("#btn_submit").attr("disabled","disabled");
    
    if($("input[name=userfile]").val() == "")
    {
      alert("No File");
      $("#btn_submit").removeAttr("disabled");
      return false;
    }
    else
    {
      return true;
    }
  });
});

</script>

<FORM name="form1" id="form1" enctype="multipart/form-data" action="import_sams_update.php" method="POST">
  <table width="500" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
	  <tr>
			<td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['year']?></td>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap'><?=$ay_selection_html?>&nbsp;</td>
      			<td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
      		</tr>
      	</table>
      </td>
		</tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['SAMS_CSV_file']?></td>
      <td class="tabletext"><input name="userfile" class="file" type="file" size="20" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="left" class="tabletext"><a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE()"><?=$ec_iPortfolio['generate_csv_template']?></a></td>
    </tr>
  </table>
  
  <br>
  <span class='tabletextremark'><?=$ec_iPortfolio['SAMS_regno_import_remind']?></span>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><a class="contenttool" href="download.php?FileName_e=<?=getEncryptedText('assessment_sams_sample.csv')?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
     
     
      <td align="right">
        <input id="btn_submit" class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" >
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
      </td>
    </tr>
  </table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
