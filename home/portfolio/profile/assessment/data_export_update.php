<?php
// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
iportfolio_auth("T");
intranet_opendb();

$lpf = new libpf_slp();
$lpf_sturec = new libpf_sturec();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$lexport = new libexporttext();

$ExportContent = $lpf->returnAssessmentExportContent($academicYearID, $YearTermID, $ClassNames, $report_type);

intranet_closedb();

// Output the file to user browser
$year_name = "_".$Year;
$sem_name = ($Semester!="") ? "_".$Semester : "";
$class_name = ($ClassName!="") ? "_".$ClassName : "";
$filename = ($report_type==0) ? "subject_assessment_{$academicYearID}_{$YearTermID}.csv" : "main_assessment_{$academicYearID}_{$YearTermID}.csv";
$filename = iconv("UTF-8", "Big5", $filename);

$lexport->EXPORT_FILE($filename, $ExportContent);
?>
