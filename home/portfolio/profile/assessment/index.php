<?php
/**
 * [Modification Log] Modifying By:
 * ************************************************
 *
 *  Date:   2020-09-14 [Bill]   [DM#3794]
 *          - handle empty grade display if score > 0
 *
 *  Date:   2020-06-29 [Bill]   [2020-0612-1004-37207]
 *          - show score if Score > 0 + No Grade + No Ranking   ($sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'])
 *
 *  Date:   2018-08-06 [Bill]   [2017-0901-1527-45265]
 *          - add float header for Academic Report
 *          - add special handling for Academic Report display - HKUGA College
 *
 * ************************************************
 */ 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

// Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_sturec();
$luser = new libuser($UserID);

function getLinkArr($fileExist, $configArr, $currentModule)
{
  global $ec_iPortfolio;

  $link_info =  array(
                  "Score" => $ec_iPortfolio['display_by_score'],
                  "Grade" => $ec_iPortfolio['display_by_grade'],
                  "ScoreGrade" => $ec_iPortfolio['display_by_score_grade'],
                  "Rank" => $ec_iPortfolio['display_by_rank'],
                  "StandardScore" => $ec_iPortfolio['display_by_stand_score']
                );

  $link_arr = array();
  foreach($link_info AS $code => $displayName)
  {
    if(!$fileExist || (is_array($configArr) && in_array($code, $configArr)))
    {
  	  $link_arr[] = ($code==$currentModule) ? "<span>{$displayName}</span>" : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('{$code}')\" class='link_a'>{$displayName}</a>";
  	}
  }
	
	return $link_arr;
}

//$lpf->accessControl("assessment_report");

switch($_SESSION['UserType'])
{
	case 2:
		$default_student_id = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		list($ClassName) = $lpf->getClassName($StudentID);
	//list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);
		break;
	case 3:
		$default_student_id = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}

$vars = "ClassName=$ClassName&StudentID=$StudentID";
//echo $ClassName, $StudentID, $ck_user_id;
/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);

	# define the navigation, page title and table size
	$template_pages = Array(
				Array($ec_iPortfolio['assessment_report'], "")
				);
}
else if($ck_memberType=="P")
{
	$template_table_top_right = $lpf->getChildrenListInfo($StudentID, $ClassName);

	# define the navigation, page title and table size
	$template_pages = Array(
				Array($ec_iPortfolio['student_list'], "../../school_records_children.php"),
				Array($ec_iPortfolio['assessment_report'], "")
				);
}
else
{
	$template_table_top_right = $lpf->getStudentListInfo($ClassName, $StudentID);
	
	# define the navigation
	if($ck_is_alumni)
	{
		$template_pages = Array(
					Array($ec_iPortfolio['alumni_list'], "../../school_records_alumni.php"),
					Array($ck_alumni_year, "../../school_records_alumni_year.php?my_year=$ck_alumni_year"),
					Array($ec_iPortfolio['assessment_report'], "")
					);
	}
	else
	{
		$template_pages = Array(
					Array($ec_iPortfolio['class_list'], "../../school_records.php"),
					Array($ec_iPortfolio['student_list'], "../../school_records_class.php?ClassName=$ClassName"),
					Array($ec_iPortfolio['assessment_report'], "")
					);
	}
}
*/

####################################
# generate assessment report
//$score_table_html = $lpf->generateAssessmentReport3($StudentID, $ClassName, $displayBy);
    $school_record_config_file = "$eclass_root/files/school_record_config.txt";
    $li_fs = new libfilesystem();
    $config_arr = unserialize($li_fs->file_read($school_record_config_file));

    if(!file_exists($school_record_config_file) || !empty($config_arr["academic_record_display"]))
    {
      // $displayBy not set = initialize
      if($displayBy=="")
      {
        // empty config arr = no config
        $displayBy = empty($config_arr["academic_record_display"]) ? "Score" : $config_arr["academic_record_display"][0];
      }
      else
      {
        // check if selected module is allowed
        $displayBy = (empty($config_arr["academic_record_display"]) || in_array($displayBy, $config_arr["academic_record_display"])) ? $displayBy : $config_arr["academic_record_display"][0];
      }
  
      $link_arr = getLinkArr(file_exists($school_record_config_file), $config_arr["academic_record_display"], $displayBy);
  
  		//$htmlResultTable = $li_pf->generateAssessmentReport3($default_student_id, $ClassName, $displayBy, "", "", "", "", "", true);
  		//$htmlResultTable .= "<br />";

        // [2020-0612-1004-37207] Show Score if Score > 0 + No Grade + No Ranking
        $alwaysDisplayScoreIfValid = $sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'];
  		
  		/******************************************************************************************/
  		/******************** New Method to display academic records ******************************/
  		/******************************************************************************************/
      // Select correct field (with correct format) to insert to tempAssessmentMark
      switch($displayBy)
      {
        case "Score":
        default:
          // If Score is 0 and grade is empty, show '--'
          //$retrieveField = "IF(Score <= 0, IF(Grade = '' OR Grade IS NULL, '--', Score), TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))))";
          $retrieveField = "CASE ";
          $retrieveField .= "WHEN Score < 0 THEN '--' ";
          $retrieveField .= "WHEN Score = 0 THEN IF(Grade = '' OR Grade IS NULL, '--', Score) ";
          $retrieveField .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))) ";
          $retrieveField .= "END";
          $retrieveFieldSubj = $retrieveField;
          if($sys_custom['iPf']['hideAcademicResultParentSubjectScore']) {
              $retrieveFieldSubj = "CASE ";
              $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
              $retrieveFieldSubj .= "WHEN Score < 0 THEN '--' ";
              $retrieveFieldSubj .= "WHEN Score = 0 THEN IF(Grade = '' OR Grade IS NULL, '--', Score) ";
              $retrieveFieldSubj .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))) ";
              $retrieveFieldSubj .= "END";
          }
          $totalTitle = $ec_iPortfolio['overall_score'];
          break;
        case "Grade":
          $retrieveField = "IF(Grade IS NULL OR Grade = '', '--', Grade)";
          $retrieveFieldSubj = $retrieveField;
          if($sys_custom['iPf']['hideAcademicResultParentSubjectScore']) {
              $retrieveFieldSubj = "CASE ";
              $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
              $retrieveFieldSubj .= "WHEN Grade IS NULL OR Grade = '' THEN '--' ";
              $retrieveFieldSubj .= "ELSE Grade ";
              $retrieveFieldSubj .= "END";
          }
          $totalTitle = $ec_iPortfolio['overall_grade'];
          break;
        case "ScoreGrade":
          //$retrieveField = "CONCAT(IF(Score <= 0 AND (Grade = '' OR Grade IS NULL), '--', Score), ' (', IF(Grade IS NULL OR Grade = '', '--', Grade), ')')";
          $retrieveField = "CASE ";
          $retrieveField .= "WHEN Score < 0 THEN CONCAT('-- (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
          $retrieveField .= "WHEN ABS(Score) = 0 THEN CONCAT(IF(Grade = '' OR Grade IS NULL, '--', Score), ' (', IF(Grade IS NULL OR Grade = '', '--', Grade), ')') ";
          // [DM#3794] Handle empty grade display
          //$retrieveField .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))), '(', Grade, ')') ";
          $retrieveField .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))), '(', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
          $retrieveField .= "END";
          $retrieveFieldSubj = $retrieveField;
          if($sys_custom['iPf']['hideAcademicResultParentSubjectScore']) {
              $retrieveFieldSubj = "CASE ";
              $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
              $retrieveFieldSubj .= "WHEN Score < 0 THEN CONCAT('-- (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
              $retrieveFieldSubj .= "WHEN ABS(Score) = 0 THEN CONCAT(IF(Grade = '' OR Grade IS NULL, '--', Score), ' (', IF(Grade IS NULL OR Grade = '', '--', Grade), ')') ";
              // [DM#3794] Handle empty grade display
              //$retrieveFieldSubj .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))), '(', Grade, ')') ";
              $retrieveFieldSubj .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))), '(', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
              $retrieveFieldSubj .= "END";
          }
          $totalTitle = $ec_iPortfolio['overall_score_grade'];
          break;
        case "Rank":
          $retrieveField = "IF(IFNULL(OrderMeritForm, 0) = 0 OR OrderMeritForm = -1, '--', OrderMeritForm)";
          $retrieveFieldSubj = $retrieveField;
          if($sys_custom['iPf']['hideAcademicResultParentSubjectScore']) {
              $retrieveFieldSubj = "CASE ";
              $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
              $retrieveFieldSubj .= "WHEN IFNULL(OrderMeritForm, 0) = 0 OR OrderMeritForm = -1 THEN '--' ";
              $retrieveFieldSubj .= "ELSE OrderMeritForm ";
              $retrieveFieldSubj .= "END";
          }
          $totalTitle = $ec_iPortfolio['overall_rank'];
          break;
        case "StandardScore":
          $totalTitle = $ec_iPortfolio['overall_stand_score'];
          break;
      }
      $htmlNavArea = implode(" | ", $link_arr);
      
      //since accessment Nav Area with a speical layout
  		$htmlNavStyle = "align=\"right\" valign=\"middle\" class=\"thumb_list\"";
  		
  		// Records of class history
      $classHistoryArr = $lpf->getAssessmentClassHistoryArr($default_student_id);
  
      // Prepare for:
      // i) table header (year, class, semester)
      // ii) partial query strings of temporary table
      $yearClassStr = "";
      $semStr = "";
      $semCount = 0;
  		for($i=0, $i_max=count($classHistoryArr); $i<$i_max; $i++)
  		{
  		  $_ayID = $classHistoryArr[$i]["AcademicYearID"];
  		  $_lay = new academic_year($_ayID);
  		  $_year = $_lay->Get_Academic_Year_Name();
  		  $_year = empty($_year) ? $classHistoryArr[$i]["Year"] : $_year;
  		  $_ytID = $classHistoryArr[$i]["YearTermID"];
  		  $_layt = new academic_year_term($_ytID);
  		  $_sem = $_layt->Get_Year_Term_Name();
  		  $_sem = empty($_sem) ? $classHistoryArr[$i]["Semester"] : $_sem;
  		  $_className = $classHistoryArr[$i]["ClassName"];
  		  
  		  $ayArr[$_ayID] = array("yearname"=>$_year, "classname"=>$_className);
  		  $semArr[$_ayID][$_ytID] = $_sem;
      }
      if(is_array($ayArr))
      {
        $insertAssessTableSqlArr = array();
        foreach($ayArr AS $ayID => $ayDetailArr)
        {
          $_yearName = $ayDetailArr["yearname"];
          $_className = $ayDetailArr["classname"];
        
          $yearClassStr .= "<td width='15%' colspan='".count($semArr[$ayID])."' nowrap='nowrap' align='center'>{$_yearName}<br />{$_className}</td>\n";
          if(is_array($semArr))
          {
            $subjOverallRes = (array_key_exists(0, $semArr[$ayID]) || array_key_exists("", $semArr[$ayID]));
            
            foreach($semArr[$ayID] AS $ytID => $_ytName)
            {
              if($ytID == 0 || $ytID == "") continue;   // "" => annual result, put to last row of the year
              
              $semStr .= "<td nowrap='nowrap' align='center'>{$_ytName}</td>\n";
              $assessTableField .= ", Score_{$ayID}_{$ytID} varchar(16)";
              
              // Special handling for standard score since it needs system calculation
              if($displayBy == "StandardScore")
              {
                // Subject records
                $hideParentSubjectDisplay = $sys_custom['iPf']['hideAcademicResultParentSubjectScore'];
                $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $default_student_id, $ayID, $ytID, $hideParentSubjectDisplay));
                // Overall record
                $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_MAIN_RECORD", "", "", "", $default_student_id, $ayID, $ytID));
                
                $selectAssessTableSql .= ", IF(t_am.Score_{$ayID}_{$ytID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID}_{$ytID} - t_aam.Score_{$ayID}_{$ytID})/t_asdm.Score_{$ayID}_{$ytID}, 2), '--'))";
              }
              else
              {
                // Subject records
                //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveFieldSubj, $default_student_id, $ayID, $ytID);
                $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveFieldSubj, $default_student_id, $ayID, $ytID, false, $alwaysDisplayScoreIfValid);
                // Overall record
                //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, $ytID);
                $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, $ytID, false, $alwaysDisplayScoreIfValid);
                
                $selectAssessTableSql .= ", IFNULL(t_am.Score_{$ayID}_{$ytID}, '--')";
              }
            }
            
            $semStr .= ($subjOverallRes) ? "<td nowrap='nowrap' align='center'>{$ec_iPortfolio['overall_result']}</td>\n" : "";
            $assessTableField .= ($subjOverallRes) ? ", Score_{$ayID} varchar(16)" : "";
            
            // Special handling for standard score since it needs system calculation
            if($displayBy == "StandardScore")
            {
              // Subject records
              $hideParentSubjectDisplay = $sys_custom['iPf']['hideAcademicResultParentSubjectScore'];
              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $default_student_id, $ayID, "", $hideParentSubjectDisplay));
              // Overall record
              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $lpf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_MAIN_RECORD", "", "", "", $default_student_id, $ayID, ""));
              
              $selectAssessTableSql .= ($subjOverallRes) ? ", IF(t_am.Score_{$ayID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID} - t_aam.Score_{$ayID})/t_asdm.Score_{$ayID}, 2), '--'))" : "'--'";
            }
            else
            {
              // Subject records
              //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveFieldSubj, $default_student_id, $ayID, "");
              $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "SubjectComponentCode", $retrieveFieldSubj, $default_student_id, $ayID, "", false, $alwaysDisplayScoreIfValid);
              // Overall record
              //$insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, "");
              $insertAssessTableSqlArr[] = $lpf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, "", false, $alwaysDisplayScoreIfValid);
              
              $selectAssessTableSql .= ($subjOverallRes) ? ", IFNULL(t_am.Score_{$ayID}, '--')" : "'--'";
            }
          }
          $semCount += count($semArr[$ayID]);
        }
      }
  
  		// table object
  		if ($order=="") $order=1;
  		if ($field=="") $field=0;
  		$li = new libpf_dbtable($field, $order, $pageNo);
  		
  		// Temp table of assessment records
  		// Table name : tempAssessmentMark (tempAssessmentAvgMark, tempAssessmentSDMark)
  		if($displayBy == "StandardScore")
  		{
  		  $lpf->createTempAssessmentSDScoreTable($li, $assessTableField, $insertAssessTableSqlArr);
  		  $table_sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
        $table_sql .= $selectAssessTableSql." ";
        $table_sql .= "FROM tempAssessmentMark t_am ";
        $table_sql .= "INNER JOIN tempAssessmentAvgMark t_aam ON t_am.SubjectCode = t_aam.SubjectCode AND t_am.SubjectComponentCode = t_aam.SubjectComponentCode ";
        $table_sql .= "INNER JOIN tempAssessmentSDMark t_asdm ON t_am.SubjectCode = t_asdm.SubjectCode AND t_am.SubjectComponentCode = t_asdm.SubjectComponentCode ";
        $table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
      }
      // Table name : tempAssessmentMark
      else
      {
  		  $lpf->createTempAssessmentMarkTable($li, $assessTableField, $insertAssessTableSqlArr);
  		  $table_sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
        $table_sql .= $selectAssessTableSql." ";
        $table_sql .= "FROM tempAssessmentMark t_am ";
        $table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
  		}
  
  		// Temp table of subjects
  		// Table name : tempAssessmentSubject
  		$lpf->createTempAssessmentSubjectTable($li);
  		$sql = "INSERT IGNORE INTO tempAssessmentSubject (MainSubjCode, CompSubjCode, SubjName, MainSubjOrder, CompSubjOrder) ";
      $sql .= "VALUES ('##Overall##', '##Overall##', '{$totalTitle}', 9999, 9999)";   // 9999 => a large number, ensure it is at the end
      $li->db_db_query($sql);
      
      
      // [2017-0901-1527-45265]
      if($sys_custom['iPf']['hideAcademicResultComponentDisplay'])
      {
          $sql = "select RecordID from {$intranet_db}.ASSESSMENT_SUBJECT where CMP_CODEID != '' ";
          if(!empty($sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry'])) {
              $sql .= " and CMP_CODEID IN ('".implode("','", (array)$sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry'])."') ";
          }
          $hideSubjectIds = $li->returnVector($sql);
          
          $sql = "delete from tempAssessmentSubject where CompSubjCodeID > 0 ";
          if(!empty($sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry']) && !empty($hideSubjectIds)) {
              $sql .= " and CompSubjCodeID not in ('".implode("', '", $hideSubjectIds)."') ";
          }
          $li->db_db_query($sql);
      }
  		
  		// TABLE INFO
  		$li->field_array = array("t_as.MainSubjOrder", "t_as.CompSubjOrder");
  		$li->sql = $table_sql;
  		$li->db = $intranet_db;
  		$li->title = $ec_iPortfolio['teacher_comment'];
  		$li->no_msg = $no_record_msg;
  		$li->page_size = 999;     // Large number to prevent page change 
  		$li->no_col = 3 + $semCount;
  		$li->noNumber = true;
  		$li->fieldorder2 = ", t_as.CompSubjOrder";
  		
  		$li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='1' class='table_b'>";
  		$li->row_alt = array("#FFFFFF", "F3F3F3");
  		//$li->row_height = 20;
  		$li->sort_link_style = "class='tbheading'";
  
  		# TABLE COLUMN
  		$li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
  		$li->column_list .= $yearClassStr;
  		$li->column_list .= "</tr>";
  		$li->column_list .= "<tr class='tabletop'>";
  		$li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
      $li->column_list .= $semStr;
  
  		//$li->column_array = array(0,3,3,3,3,0,0,0);
  		$li->column_array = array(0,0,0);
  		if($semCount > 0)
  		{
    		$li->column_array = array_merge($li->column_array, array_fill(1, $semCount, 3));
    	}
  
    	$score_table_html .= $li->displayAssessmentReport($displayBy);
    	
    	$supportFloatHeader = true;
  	}


//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[2][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("assessment_report");
$html_menu_tab = $lpf->GET_TAB_MENU($TabMenuArr);
?>

<!-- ===================================== Body Contents ============================= -->

<script language="JavaScript">
function jCHANGE_DISPLAY_TYPE(jDisType)
{
	document.form1.displayBy.value = jDisType;
	document.form1.action = "index.php";
	document.form1.submit();
}

function jVIEW_DETAIL(jParRecType, jParParam)
{
	document.form1.miscParameter.value = jParParam;
	document.form1.action = "stat2.php";
	document.form1.submit();
}
</script>

<FORM name="form1" method="POST">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="left" valign="bottom">
      <div align="left"><span class="chi_content_12">
      </span>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td><div align="center">
      
      <?= $html_menu_tab ?>
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      
      <td><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="tabletext"><?=$ec_iPortfolio['overall_summary']?></span></td>
      
      </tr>
      <tr>
      <td height="30">&nbsp;</td>
      
      <td align="right" valign="middle" class="thumb_list">
      <?= $htmlNavArea ?>
      </td>
      </tr>
      </table>
      <span class="chi_content_12"></span>
      </div>
      </td>
      </tr>
      </table>
      <span class="chi_content_12">
      </span></div></td>
    </tr>
    <tr>
      <td colspan="2"><?=$score_table_html?></td>
    </tr>
  </table>
  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="miscParameter" />
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
  <input type="hidden" name="displayBy" value="<?=$displayBy?>">
</FORM>

<?php if($supportFloatHeader) { ?>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script>$(function() {$('.table_b').floatHeader();});</script>
<?php } ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>