<?php
// Modifing by
/************************************************************
 * Eric Yip (20100811):
 *    - Alternative method to retrieve subjects
 ************************************************************/  
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

intranet_opendb();
$linterface = new interface_html("popup.html");
$lpf = new libpf_sturec();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

/*
$SubjectArr = $lpf->returnAllSubjectWithComponents();
for($i=0; $i<sizeof($SubjectArr); $i++)
{
	list($s_code, $cmp_code, $s_name, $cmp_name) = $SubjectArr[$i];
	$SubjectOption .= ($cmp_code=="") ? "<OPTION value='$s_code'>$s_name</OPTION>\n" : "<OPTION value='".$s_code."###".$cmp_code."'>$s_name&nbsp;-&nbsp;$cmp_name</OPTION>\n";
}
*/
$sql = "SELECT m.en_sname, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', '', c.en_sname), ".Get_Lang_Selection("m.CH_DES", "m.EN_DES").", ".Get_Lang_Selection("c.CH_DES", "c.EN_DES").", m.DisplayOrder, IF(c.CMP_CODEID is null or trim(c.CMP_CODEID) = '', 0, c.DisplayOrder) as cmp_displayOrder ";
$sql .= "FROM {$intranet_db}.ASSESSMENT_SUBJECT AS m ";
$sql .= "LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS c ON m.codeid = c.codeid AND c.RecordStatus = 1 ";
$sql .= "LEFT JOIN {$intranet_db}.LEARNING_CATEGORY AS lc ON m.LearningCategoryID = lc.LearningCategoryID ";
$sql .= "WHERE (m.CMP_CODEID IS NULL or TRIM(m.CMP_CODEID) = '') AND m.RecordStatus = 1 ";
$sql .= "ORDER BY lc.DisplayOrder, m.DisplayOrder, cmp_displayOrder";
$SubjectArr = $lpf->returnArray($sql);
for($i=0, $i_max=sizeof($SubjectArr); $i<$i_max; $i++)
{
	list($s_code, $cmp_code, $s_name, $cmp_name) = $SubjectArr[$i];
	$SubjectOption .= ($cmp_code=="") ? "<OPTION value='$s_code'>$s_name</OPTION>\n" : "<OPTION value='".$s_code."###".$cmp_code."'>$s_name&nbsp;-&nbsp;$cmp_name</OPTION>\n";
}

$CurrentPage = "generate_csv_template";
$title = $ec_iPortfolio['generate_csv_template'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>
<script language="javascript">
function checkform(myObj){

	checkOption(myObj.elements["target[]"]);
	for(var i=0; i<myObj.elements["target[]"].length; i++)
	{
		myObj.elements["target[]"].options[i].selected = true;
	}
	if(myObj.elements["target[]"].length==0)
	{
		alert("<?=$ec_iPortfolio['please_select_subject']?>");
		return false;
	}
	else
		return true;
}

function doReset(){
	self.location.reload();
}
</script>

<FORM action="generate_csv_template_update.php" method="POST" name="form1" onSubmit="return checkform(this);">
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td colspan="3">
        <span class="tabletextremark"><?= $ec_iPortfolio_guide['assessment_csv_remove_exp'] ?></span>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top" nowrap="nowrap" class="tabletext"><?=$ec_iPortfolio['SAMS_subject']?>:</td>
      <td>
        <table>
          <tr>
            <td width="45%"><span class="tabletext"><?=$iPort["selected_subject"]?> :</span></td>
    				<td width="10%">&nbsp;</td>
    				<td width="45%"><span class="tabletext"><?=$iPort["choose_subject"]?> :</span></td>
    			</tr>
				  <tr>
    				<td>
    					<select name="target[]" size=15 multiple style="width:100%">
    					</select>
    				</td>
    				<td align="center" nowrap>
              <p><input name="btnSelect" type="button" class="formsubbutton" value="&lt;&lt;" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onclick="javascript:check(document.form1.elements['source[]'],document.form1.elements['target[]'])"></p>
    				  <p><input name="btnUnselect" type="button" class="formsubbutton" value="&gt;&gt;" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onclick="javascript:check(document.form1.elements['target[]'],document.form1.elements['source[]'])"></p>
    					<!--<p><a href="javascript:check(document.form1.elements['source[]'],document.form1.elements['target[]'])" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgAddItem','','<?=$sr_image_path?>/Buttons/btn_Increase_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Increase_D.gif" name="imgAddItem" border="0" align="absmiddle"></a></p>-->
    					<!--<p><a href="javascript:check(document.form1.elements['target[]'],document.form1.elements['source[]'])" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgRemoveItem','','<?=$sr_image_path?>/Buttons/btn_Decreas_U.gif',1)"><img src="<?=$sr_image_path?>/Buttons/btn_Decreas_D.gif" name="imgRemoveItem" border="0" align="absmiddle"></a></p>-->
    				</td>
				    <td>
    					<select name="source[]" size=15 multiple style="width:100%">
    					<?=$SubjectOption?>
    					</select>
    				</td>
  				</tr>
  				<tr>
            <td colspan="3">
              <span class="tabletextremark"><?= $ec_iPortfolio_guide['assessment_csv_template'] ?></span>
            </td>
				  </tr>
				</table>
			</td>
    </tr>
  </table>
	
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
  	<tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
  	</tr>
  </table>  
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
  	<tr>
  		<td align="right">
  		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" name="btn_submit">
  		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">
  		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
  		</td>
  	</tr>
  </table>
  
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>" />
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>