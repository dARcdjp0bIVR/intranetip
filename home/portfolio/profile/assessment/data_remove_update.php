<?php
// Modifing by Pun

/* Change Log
 * 
 * Date: 2016-02-16 (Pun) ip2.5.7.3.1 - Erase record for ASSESSMENT_SUBJECT_SD_MEAN
 * Date: 2015-02-13 (Omas) ip2.5.6.3.1 - Erase record excluding archived student record
 *   
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_opendb();
$lpf = new libportfolio();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();
$linterface = new interface_html("popup.html");

if($IsAnnual==0)
{
	$conds .= ($Semester==$ec_iPortfolio['overall_result']) ? " AND IsAnnual = 1" : " AND Semester = '$Semester' AND IsAnnual = 0";
}
$conds .= ($ClassName!="") ? " AND ClassName = '$ClassName'" : "";

# ARCHIVE User Record not delete
$sql = "SELECT UserID from INTRANET_ARCHIVE_USER where RecordType  = 2";
$ArchivedUserAry = $lpf->returnVector($sql);
$notInArchived_conds = " AND UserID not IN('".implode("','",$ArchivedUserAry)."') ";


#### Delete records in ASSESSMENT_SUBJECT_SD_MEAN START ####
$sql = "SELECT DISTINCT AcademicYearID, YearClassID, YearTermID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Year = '$Year' $conds $notInArchived_conds";
$rs = $lpf->returnResultSet($sql);

$yearClassIdArr = array();
$yearTermIdArr = array();
foreach($rs as $r){
	$yearClassIdArr[] = $r['YearClassID'];
	$yearTermIdArr[] = ($r['YearTermID'])?$r['YearTermID']:'0';
	$AcademicYearID = $r['AcademicYearID'];
}

$yearClassIdList = implode("','", $yearClassIdArr);
$yearTermIdList = implode("','", $yearTermIdArr);
if($ClassName != ''){
	$yearClassIdFilterSQL = " AND YearClassID IN ('{$yearClassIdList}')";
}
if($Semester==$ec_iPortfolio['overall_result']){
	$isAnnualFilterSQL = " AND IsAnnual = 1";
}else{
	$isAnnualFilterSQL = " AND IsAnnual = 0 AND YearTermID IN ('{$yearTermIdList}')";
}

$sql = "DELETE FROM 
	{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
WHERE 
	AcademicYearID = '{$AcademicYearID}'
	{$yearClassIdFilterSQL}
	{$isAnnualFilterSQL}
";
$lpf->db_db_query($sql);
$total = $lpf->db_affected_rows();
#### Delete records in ASSESSMENT_SUBJECT_SD_MEAN END ####


# delete records in ASSESSMENT_STUDENT_SUBJECT_RECORD
$sql = "DELETE FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Year = '$Year' $conds $notInArchived_conds";
$lpf->db_db_query($sql);
$total += $lpf->db_affected_rows();

# delete records in ASSESSMENT_STUDENT_MAIN_RECORD
$sql = "DELETE FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD WHERE Year = '$Year' $conds $notInArchived_conds";
$lpf->db_db_query($sql);
$total += $lpf->db_affected_rows();

$CurrentPage = "remove_assessment_info";
$title = $ec_iPortfolio['remove_assessment_info'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

$display_content = "<table border='0' cellpadding='8' cellspacing='0'>";
$display_content .= "<tr><td class='tabletext'>".$ec_iPortfolio['number_of_removed_record']." : </td><td class='tabletext'><b>".$total."</b></td></tr>\n";
$display_content .= "</table>\n";

?>

<FORM enctype="multipart/form-data" action="import_regno_update.php" method="POST" name="form1">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_iPortfolio['remove_more_assessment']?>" onClick="self.location='data_remove.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="javascript:window.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
