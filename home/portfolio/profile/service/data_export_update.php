<?php
// Modifing by  : 
/*
 * Date		:	2012-05-29 (Henry Chow)
 * Detail	:	comment out $g_encoding_unicode
 * 
 * */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$lexport = new libexporttext();

$Semester = ($IsAnnual==1) ? "" : $Semester;
$ExportContent = $lpf->returnServiceExportContent($Year, $Semester, $ClassName);

intranet_closedb();

// Output the file to user browser
$year_name = "_".$Year;
$sem_name = ($Semester!="") ? "_".$Semester : "";
$class_name = ($ClassName!="") ? "_".$ClassName : "";
$filename = "service".$year_name.$sem_name.$class_name.".csv";

/*
if ($g_encoding_unicode) {
	$lexport->EXPORT_FILE($filename, $ExportContent);
} else {
	output2browser($ExportContent, $filename);
}
*/
$filename = iconv("UTF-8", "Big5", $filename);
$lexport->EXPORT_FILE($filename, $ExportContent);

?>
