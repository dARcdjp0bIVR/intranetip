<?php
// Editing by 
/*
 * 2017-07-11 (Carlos): $sys_custom['LivingHomeopathy'] created.
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

if(!$plugin['eEnrollment'] || !$sys_custom['LivingHomeopathy'] || !isset($RecordID) || $RecordID==''){
	exit;
}

intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$RecordID = IntegerSafe($RecordID);


$LibUser = new libuser($_SESSION['UserID']);

$sql = "SELECT s.*,a.eEnrolRecordID 
		FROM {$eclass_db}.ACTIVITY_STUDENT as s 
		LEFT JOIN {$intranet_db}.PROFILE_STUDENT_ACTIVITY as a ON a.UserID=s.UserID AND a.Year=s.Year AND a.Semester=s.Semester AND a.ActivityName=s.ActivityName AND a.ClassName=s.ClassName AND a.ClassNumber=s.ClassNumber 
		LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENTINFO as e ON e.EnrolEventID=a.eEnrolRecordID 
		LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_ATTENDANCE as ea ON ea.StudentID=a.UserID AND ea.EnrolEventID=e.EnrolEventID 
		LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_DATE as d ON ea.EventDateID=d.EventDateID   
		WHERE s.RecordID='$RecordID' ";
$activity_student_records = $LibUser->returnResultSet($sql);
if(count($activity_student_records)==0){
	intranet_closedb();
	exit;
}
//debug_pr($activity_student_records);
$AcademicYearID = $activity_student_records[0]['AcademicYearID'];
$EnrolGroupID = $activity_student_records[0]['eEnrolRecordID'];
$StudentID = $activity_student_records[0]['UserID'];
$AcademicYear = $activity_student_records[0]['Year'];

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();	
	
if ($libenroll->enableAttendanceScheduleDetail()) {
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui_cust.php");
	$libenroll_ui_cust = new libclubsenrol_ui_cust();
}

$GroupEnrollArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
$GroupArr = $libenroll->getGroupInfo($GroupID);
$title_display = $intranet_session_language=="en" ? $GroupArr[0]['Title'] : $GroupArr[0]['TitleChinese'];
if($title_display == '') $title_display = $activity_student_records[0]['ActivityName'];
$GroupDateArr = $libenroll->GET_ENROL_GROUP_DATE($GroupEnrollArr[0]);

if ($libenroll->enableAttendanceScheduleDetail()) {
	if (count($GroupDateArr) > 0) {
		
		foreach ($GroupDateArr as $i => $GroupDateRec) {
			$tmp = $libenroll_ui_cust->Get_Attendance_Time_Log_Records($EnrolGroupID, $GroupDateRec["GroupDateID"], array($StudentID=>$StudentID), "Club");
			$GroupDateArr[$i]["time_attend"] = $tmp[$GroupDateRec["GroupDateID"]][$StudentID]["time_attend"];
			$GroupDateArr[$i]["time_leave"] = $tmp[$GroupDateRec["GroupDateID"]][$StudentID]["time_leave"];
			if (empty($GroupDateArr[$i]["time_attend"])) $GroupDateArr[$i]["time_attend"] = "-";
			if (empty($GroupDateArr[$i]["time_leave"])) $GroupDateArr[$i]["time_leave"] = "-";
		}
		unset($tmp);
	}
}
if ($libenroll->enableUserJoinDateRange()) {
	$enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolGroupID, array($StudentID), "Club");
	if (count($GroupDateArr) > 0) {
		if (empty($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateStart"])) $AvailStart = "";
		else $AvailStart = strtotime(date("Y-m-d", strtotime($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateStart"])));

		if (empty($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateEnd"])) $AvailEnd = "";
		else $AvailEnd = strtotime(date("Y-m-d", strtotime($enrolAvailDateArr[$StudentID]["EnrolAvailiableDateEnd"])));

		foreach ($GroupDateArr as $kk => $vv) {
			$checkStart = strtotime(date("Y-m-d", strtotime($vv["ActivityDateStart"])));
			$notAvailable = true;
			if (
					(empty($AvailStart) || $checkStart >= $AvailStart)
					&& (empty($AvailEnd) || $checkStart <= $AvailEnd)
					) {
						$notAvailable = false;
					}
					if ($notAvailable) {
						unset($GroupDateArr[$kk]);
					}
		}
		$GroupDateArr = array_values($GroupDateArr);
	}
}

$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon_Remarks();

$MODULE_OBJ['title'] = $Lang['iAccount']['EnrolmentAttendanceOverview'];

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<table align="center">
<tr>
	<td class="tabletext"><?=$AcademicYear?> <?= $eEnrollment['year']?> </td>
	<td class="tabletext">&nbsp;</td>
	<td class="tabletext">
		<?= $eEnrollment['activity']?> : 
	</td>
	<td class="tabletext">
		<?=$title_display?>
	</td>
	</tr>
</table>
<br/>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tablegreentop">
		<td class="tableTitle" width="35%" nowrap align="center"><span class="tabletopnolink"><?= $eEnrollment['add_activity']['act_date']?></span></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tableTitle" width="25%" nowrap align="center"><span class="tabletopnolink"><?php echo $Lang['eEnrolment']['TimeofArrival']; ?></span></td>
		<td class="tableTitle" width="25%" nowrap align="center"><span class="tabletopnolink"><?php echo $Lang['eEnrolment']['TimeofDeparture']; ?></span></td>
<?php } ?>
		<td><span class="tabletopnolink"> </span></td>
	</tr>	
	<? for ($i = 0; $i < sizeof($GroupDateArr); $i++) { ?>
	<tr class="tablegreenrow<?= (($i % 2) + 1)?>">
		<td class="tabletext" align="center"><?= date("Y-m-d H:i", strtotime($GroupDateArr[$i][2]))?>-<?= date("H:i", strtotime($GroupDateArr[$i][3]))?></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tabletext" align="center"><?php echo $GroupDateArr[$i]["time_attend"]?></td>
		<td class="tabletext" align="center"><?php echo $GroupDateArr[$i]["time_leave"]?></td>
<?php } ?>
		<td align="center">		
		<?
			
			if (date("Y-m-d", strtotime($GroupDateArr[$i][2])) > date("Y-m-d"))
			{
				$temp = "future";
			}
			else
			{
				//$total++;
				/*
				if ($status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$StudentID,$EnrolGroupID)) {
					$temp = $status==1?"present":"exempt";
					$attend++;
				} else {
					$temp = "absent";
				}
				*/	
				
				$status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$StudentID,$EnrolGroupID);
				//if (in_array($status, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT)))
				if (in_array($status, array(ENROL_ATTENDANCE_PRESENT)))
					$attend++;
				else if (in_array($status, array(ENROL_ATTENDANCE_LATE)) && $libenroll->enableAttendanceLateStatusRight())
					$attend++;
				else if (in_array($status, array(ENROL_ATTENDANCE_EARLY_LEAVE)) && $libenroll->enableAttendanceEarlyLeaveStatusRight())
					$attend++;
					
				if (!in_array($status, array(ENROL_ATTENDANCE_EXEMPT)))
					$total++;
				
				$thisIcon = $libenroll_ui->Get_Attendance_Icon($status);
			}
		?>
		
		<? if ($temp == "future") { ?>
			<?=$Lang['General']['EmptySymbol']?>
		<? } else { ?>
			<?=$thisIcon?>
		<? } ?>
		
		</td>
	</tr>
	<? } ?>
	<tr class="tablegreenbottom">
		<td class="tabletext" nowrap align="center"><?= $eEnrollment['attendence']?></td>
<?php if ($libenroll->enableAttendanceScheduleDetail()) { ?>
		<td class="tabletext">&nbsp;</td>
		<td class="tabletext">&nbsp;</td>
<?php } ?>
		<?($total != 0) ? $attendPercentage = round(($attend / $total * 100),2) : $attendPercentage = "0"; ?>
		<td class="tabletext" align="center"><?= $attendPercentage ?>%</td>
	</tr>
</table>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr><td align="right"><?= $AttendanceIconRemarks ?></td></tr>
</table>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>