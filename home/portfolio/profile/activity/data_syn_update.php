<?php
// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();
$linterface = new interface_html("popup.html");

$Semester = ($IsAnnual==1) ? "" : $Semester;

$total = $lpf->updateActivityFromIP($Year, $Semester);


$CurrentPage = "eClass_update_activity";
$title = $ec_iPortfolio['eClass_update_activity'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

$display_content = "<table border='0' cellpadding='8' cellspacing='0'>";
$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_update_no']." : </td><td class='tabletext'><b>".$total."</b></td></tr>\n";
$display_content .= "</table>\n";

?>

	<br />
	<?= $display_content ?>
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_iPortfolio['update_more_activity']?>" onClick="self.location='data_syn.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="javascript:window.close()">
	</p>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
