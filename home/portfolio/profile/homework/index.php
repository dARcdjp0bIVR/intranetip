<?php
// Editing by 
/*
 * 2017-08-02 (Carlos): Created for $sys_custom['LivingHomeopathy']. 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if ($order=="") $order=0;
if ($field=="") $field=3;

$AcademicYearID = !isset($AcademicYearID)? Get_Current_Academic_Year_ID() : $AcademicYearID;

$fcm = new form_class_manage();

$academic_years = $fcm->Get_All_Academic_Year();

$academic_year_selection = getSelectByArray($academic_years, ' name="AcademicYearID" id="AcademicYearID" onchange="document.form1.submit();" ', $AcademicYearID, $___all=1, $___noFirst=0, $___FirstTitle=$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'], $___ParQuoteValue=1);

$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = array("Subject","SubjectGroup","hw.Title","hw.StartDate","hw.DueDate","Workload","Poster","hl.StudentDocument","hl.TeacherDocument","Rating");

$conds = "";
if($AcademicYearID != ''){
	$conds .= " AND hw.AcademicYearID='$AcademicYearID' ";
}
$sql = "SELECT  
			".Get_Lang_Selection("sbj.CH_DES","sbj.EN_DES")." as Subject,
			".Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as SubjectGroup,
			hw.Title,
			hw.StartDate, 
			hw.DueDate,
			CONCAT(hw.Loading/2,' ".$Lang['SysMgr']['Homework']['Hours']."') as Workload,
			".getNameFieldByLang2("u1.")." as Poster,
			hl.StudentDocument,
			hl.TeacherDocument,
			hl.TextScore as Rating,
			hl.StudentDocumentUploadTime,
			hl.TeacherDocumentUploadTime,
			".getNameFieldByLang2("u2.")." as TeacherDocumentUploaderName  
		FROM INTRANET_HOMEWORK as hw  
		INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST as hl ON hl.HomeworkID=hw.HomeworkID 
		LEFT JOIN ASSESSMENT_SUBJECT as sbj ON sbj.RecordID = hw.SubjectID 
		LEFT JOIN SUBJECT_TERM_CLASS as stc ON stc.SubjectGroupID = hw.ClassGroupID
		LEFT JOIN SUBJECT_TERM_CLASS_USER AS stcu ON stcu.SubjectGroupID = hw.ClassGroupID 
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=hw.PosterUserID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=hl.TeacherDocumentUploadedBy 
		WHERE hl.StudentID='".$_SESSION['UserID']."' $conds
		GROUP BY hw.HomeworkID ";

$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $Lang['ePayment']['HomeworkRecords'];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = count($li->field_array)+1;

$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0' width='100%'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";

$col = 0;	
$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width='13%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Subject'], 1)."</td>\n";
$li->column_list .= "<td width='13%'>".$li->column($col++, $Lang['SysMgr']['Homework']['SubjectGroup'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Topic'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['StartDate'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['DueDate'], 1)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")", 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Poster'], 1)."</td>\n";
$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['HandedInHomework'], 1)."</td>\n";
$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['Mark'], 1)."</td>\n";
$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['Rating'], 1)."</td>\n";
$li->column_array = array_fill(0,$col,0);


function handedinhomework_formatter($student_document, $row)
{
	global $file_path, $Lang;
	if($student_document == '') return '';
	$file_name = substr($student_document,strrpos($student_document,'/')+1);
	$download_url = getEncryptedText($file_path.'/file/homework_document/'.$student_document);
	$html = '<a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($file_name).'</a>';
	$html.= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$row['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
	return $html;
};

function marking_formatter($teacher_document, $row)
{
	global $file_path, $Lang;
	if($teacher_document == '') return '';
	$file_name = substr($teacher_document,strrpos($teacher_document,'/')+1);
	$download_url = getEncryptedText($file_path.'/file/homework_document/'.$teacher_document);
	$html = '<a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($file_name).'</a>';
	$html.= '<div class="tabletextremark">('.str_replace('<!--NAME-->',$row['TeacherDocumentUploaderName'],str_replace('<!--DATETIME-->',$row['TeacherDocumentUploadTime'],$Lang['eHomework']['MarkedRemark'])).')</div>';
	return $html;
};

$li->column_formatter = array();
$li->column_formatter[7] = handedinhomework_formatter;
$li->column_formatter[8] = marking_formatter;

$CurrentPage = "Student_SchoolRecords";

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$lpf = new libpf_slp();

$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("homework");
?>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
-->
</script>
<form name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
			</td>
          </tr> 
          <tr>
            <td>
              <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><?= $academic_year_selection?></td>                    
                  </tr>
                  <tr>
                  	<td><?= $summaryTable?></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center">
              <?php $li->displayPlain() ?>
<?php if ($li->navigationHTML!="") { ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
                <tr class="tablebottom">
                  <td class="tabletext" align="right"><?=$li->navigation(1)?></td>
                </tr>
                </table>
<?php } ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>