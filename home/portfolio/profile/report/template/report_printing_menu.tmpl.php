<?
/** [Modification Log] Modifying By: anna
 * *******************************************
 *   2019-06-26 Anna
 *   - Added DisplayRecordNum for bps cust
 *   2018-11-14 Bill    [2018-1112-0951-30235]
 *   - removed getJQuerySafeId + support IE for StudentIDSel
 * 
 *   2018-07-11 Anna
 *   - added getJQuerySafeId for  StudentIDSel
 *   
 *   2018-05-24 Anna    
 *   - added tmgss customzation  #133513
 *   
 *   2018-05-11 Bill    [DM#3414]
 *    added js checking for student selection before print / export 
 * 
 *	 2018-02-02 Anna
 *	 - added MaxPrizeSizeRow and MaxECASizeRow in twghczm_slp
 * 
 *   2017-04-10 Omas
 *   - add customField for twghwfns for tuning margin
 *   
 * 	 2016-12-07 Cameron
 *   - Add options for tmchkwc (SLP Report & Student Profile Report [JinYingReport])
 * 
 * 	 2016-11-30 Omas	
 *   - enalbed display footer (changed lang in customized lang for signature) for stpaul_PAS - #F96819 
 * 
 *   2016-11-08 Villa
 *   - Add options for #B106517
 * 
 * 	 2016-06-24 Omas
 *   - added Remarks field for css_slp - #S97804 
 * 
 *   2016-05-09: Omas
 *   - Fixed - #W95766
 *    
 *   2016-04-12: Omas
 * - Added js_Show_Report_Footer - #W94753  
 * 
 *  2016-03-21: Omas
 * - Add pdf for cchpw SLP #A90688 
 * 
 * 2015-11-10: Omas
 * - Add options for sptss SLP Report case#F78756
 * 
 * 2015-09-30: Omas
 * - Add options for twghczm SLP Report case#J78825
 * 
 * 2015-08-26: Omas
 * - Add options for twghwfns SLP Report case#C69320
 * 
 * 2015-02-26: Omas
 * - Add options for CWGC SLP Report case#L70438 
 * 
 * 2015-01-12: Bill
 * - Add options for Ng Wah SAS Report
 * 
 * 2014-10-14: Bill [F38945]
 * - Add alumni to Student Learning Profile Report (Paulinian Award Scheme)
 * 
 * 2014-07-21: Bill
 * - Hide print issue date setting for customized PKMS SLP report
 * 
 * 2013-10-22: Ivan [2013-1021-1354-34073]
 * - Hide export doc button for customized UCCKE full report
 * 
 * 2012-02-17 Ivan
 * - Added show / hide full mark display logic for SLP report
 * 
 * 2011-12-19 Ivan
 * - Added academic result display mode options for SLP report
 * 
 * 2011-11-09: Connie
 * - Added King's College - Student Learning Profile
 * 
 * 2011-01-28: Ivan
 * - Added Raimondi College Non-academic Performance Report
 * 
 * 2011-01-05: Thomas (201101051700)
 * - For PDF button, change to show() / hide()
 * 
 * 2010-08-18: Max (201008180910)
 * - Plug NPL cust from IP20
 * 
 * 2010-02-25: Max (201002181147)
 * - Add config for cnecc
 * *******************************************
 */
 
 //$IssueDateNow = ($sys_custom['iPf']['ChingChungHauPoWoon']['Report']['StudentLearningProfile']) ? date("d/m/Y") : date("Y-m-d");

?>
<script language="JavaScript">
function jCHANGE_REPORT_TYPE()
{
  var reportType = $("input[name=ReportType]:checked").val();
  jSHOW_ACADEMIC_RESULT_MODE(0);
  

//alert(reportType);
  var StudentType = 'single';
  var showYearSelection = true;
  jSHOW_REMARK(0);
  jSHOW_ISSUEDATE(1);
  jSupportCurrentStudentSelectOnly(); // default all type of report supoort with selecting current student only
  changeSelectReportTarget(1);
  jSHOW_customFieldRow(0);
  
  $('#recordTypeRow_sptss').hide();
  $("#DisplayRecordNum").hide();

  
  switch(reportType)
  {
    // Full Report
    case "1":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(0);
      jSHOW_ISSUEDATE(0);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);
      jSHOW_NUMBER_PAGE(0);
      jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
      js_Class_Selection(StudentType);
      
	  jSupportCurrentAlumniStudentSelect();
	  $("#trgType_current").attr('checked',true);	
		
	  $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
      
      <? if ($hideWordBtnForFullReport) { ?>
      	$('#btn_print_word').attr("disabled", true);
	  	$("#btn_print_word").hide();
	  <? } else { ?>
	  	$("#btn_print_word").removeAttr("disabled");
	  	$("#btn_print_word").show();
	  <? } ?>
      
      
	  document.form1.action="report_print.php";
      break;
    // Transcript
    case "2":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(0);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
      js_Class_Selection(StudentType);

	  jSupportCurrentAlumniStudentSelect();
	  $("#trgType_current").attr('checked',true);	


      $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
      $("#btn_print_word").removeAttr("disabled");
	  $("#btn_print_word").show();
	  document.form1.action="report_print.php";
      break;
    // Customized Report
    case "3":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(<?=$showAllYear?>);
      jALLOW_MULTIYEAR(0);
      jSHOW_ISSUEDATE(0);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
       js_Class_Selection(StudentType);
      
      $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
      $("#btn_print_word").removeAttr("disabled");
	  $("#btn_print_word").show();
	  document.form1.action="report_print.php";
      break;
    // SLP Report
    case "6":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(1);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(1);
      jSHOW_ADVANCESETTING(0);
      jSHOW_PRINTISSUEDATE(1);
      js_Class_Selection(StudentType);
      jSHOW_ACADEMIC_RESULT_MODE(1);
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
      jSupportCurrentAlumniStudentSelect();
	  $("#trgType_current").attr('checked',true);	

	  /*
	  //special handle refresh case in FF , FF keep last selected value after refresh
	  var studentSelectType = $("#trgType_alumni:checked").val();
	  if(studentSelectType == 'alumni'){
		  changeSelectReportTarget('2');
	  }
	  */	

      $("#btn_print_html").show();
      $("#btn_print_pdf").show();      
      $("#btn_print_word").removeAttr("disabled");
      $("#btn_print_word").show();
	  document.form1.action="report_print.php";
      break;
    case "7":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(0);
      jALLOW_MULTIYEAR(0);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(1);   
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
       
       js_Class_Selection(StudentType);
	  $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
      $("#btn_print_word").removeAttr("disabled");
	  $("#btn_print_word").show();
	  document.form1.action="report_print.php";
      break;
    // CNECC CUST
    case "9":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(0);
      jALLOW_MULTIYEAR(0);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);  
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
       
       js_Class_Selection(StudentType);
      $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
	  $("#btn_print_word").show();
	  document.form1.action="report_print.php";
      break;
    // BPS CUST
    case "10":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(0);
      jALLOW_MULTIYEAR(0);
      jSHOW_ISSUEDATE(0);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      $("#DisplayRecordNum").show();
      
       js_Class_Selection(StudentType);
      
      $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
	  $("#btn_print_word").hide();
	  document.form1.action="report_print.php";
      break;
    // NPL CUST
    case "11":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(0);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(0);
      jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
	   js_Class_Selection(StudentType);
	  
	  $("#btn_print_html").show();
      $("#btn_print_pdf").hide();
	  $("#btn_print_word").hide();
	  document.form1.action="report_print.php";
      break;
    // Raimond College
    case "12":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(1);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(0);
      jSHOW_ADVANCESETTING(1);
      jSHOW_NUMBER_PAGE(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
       js_Class_Selection(StudentType);
      
      $("#btn_print_html").show();
      $("#btn_print_pdf").hide();    
	  $("#btn_print_word").hide();
      break;
    // NPL CUST
     case "cactm":
    	$("#task").val(reportType);
   		jALLOW_MULTIYEAR(1);
//    	jSHOW_ISSUEDATE(1);
   		jSHOW_PRINTISSUEDATE(0);
        $("#btn_print_pdf").hide();
   	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
        jSHOW_NUMBER_PAGE(0);
        jSHOW_SELFACCOUNT(1);
        js_Show_Report_Header(0);
        js_Show_Report_Footer(0);
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
        
        js_Class_Selection(StudentType);
           
        $("#btn_print_html").show();
   		$("#btn_print_word").show();
   	  	jSupportCurrentAlumniStudentSelect();
   		document.form1.action="customizeReport/index.php";
     break;
     case "cactm_second":
		$("#task").val(reportType);
		 jALLOW_MULTIYEAR(1);
// 		jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
        $("#btn_print_pdf").hide();
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
        jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      	js_Show_Report_Header(0);
      	js_Show_Report_Footer(0);
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
        
        
        js_Class_Selection(StudentType);
        
        $("#btn_print_html").show();
		$("#btn_print_word").show();
	  	jSupportCurrentAlumniStudentSelect();
		document.form1.action="customizeReport/index.php";
 	 break;
     case "ghslp":
	
		$("#btn_print_html").hide();
        $("#btn_print_word").hide();
        $("#btn_print_pdf").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
        
        jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
//        jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
		jSHOW_ADVANCESETTING(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
		
		 js_Class_Selection(StudentType);
		
		$("#task").val("ghs");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "fywss_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(1);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		
		js_Class_Selection(StudentType);
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "stpaul_PAS":
		$("#task").val(reportType);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(1);
		
		js_Class_Selection(StudentType);	 
		
		// Added to support alumni records		
		jSupportCurrentAlumniStudentSelect();
	  	$("#trgType_current").attr('checked',true);	
		
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "pkms_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

        // Disable print issue date option: Bill
		jSHOW_ISSUEDATE(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(1);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		
		js_Class_Selection(StudentType);
		
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "cyma_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		$("#btn_print_word").hide();

	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();
	      
		jSHOW_ISSUEDATE(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		
		js_Class_Selection(StudentType);
		
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "cwgc_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();

	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide(); 
		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(1);
     	js_Show_Report_Header(1);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "srlc_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(1);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "srlc_slp2":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(1);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	  case "css_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_REMARK(1);
		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "twghwfns_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		jSHOW_customFieldRow(1);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "twghczm_slp":
		$("#task").val(reportType);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		$("#btn_print_word").show();
		$("#MaxPrizeSizeRow").show();
		$("#MaxECASizeRow").show();
		
		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "sptss_1":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "sptss_2":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "sptss_3":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "sptss_4":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$('#recordTypeRow_sptss').show();
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "sptss_5":
		$("#task").val(reportType);
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);
		
		$('#recordTypeRow_sptss').show();
		$("#printIssueDate").attr('disabled', true);
		showYearSelection = false;
		document.form1.action="customizeReport/index.php";
 	 break;
 	 case "mms":
		$("#task").val("mms");
		 //jALLOW_MULTIYEAR(1);
		$("#btn_print_pdf").hide();
		$("#btn_print_html").show();
		//$("#btn_print_word").show();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();

//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
		
		 js_Class_Selection(StudentType);
		
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 // Reading Scheme (Student Reading Profile)
  	 case "srp":
		$("#task").val("srp");
		 //jALLOW_MULTIYEAR(1);
		$("#btn_print_html").show();
        $("#btn_print_pdf").hide();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
		$("#btn_print_word").hide();
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
		
		 js_Class_Selection(StudentType);
		
		document.form1.action="srp/index.php";
 	 break;	 
	 
	 // Hong Kong True Light College - SLP Report
	 case "hktlc_slp":
	
		$("#btn_print_html").show();
        $("#btn_print_word").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
        
        jSHOW_ALLYEAR(0);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
//        jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
		jSHOW_ADVANCESETTING(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
		
		 js_Class_Selection(StudentType);
		
		$("#task").val("hktlc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 // King's College - Student Learning Profile
	 case "kc_slp":
	
        
        jSHOW_ALLYEAR(0);
		 jALLOW_MULTIYEAR(1);
// 		jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
        jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection(StudentType);
        
        $("#btn_print_html").show();
		
		
		$("#task").val("kc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	  // Sha Tin Methodist College - Student Learning Profile
	 case "stmc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
         js_Class_Selection('multiple');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
        
		
		
		$("#task").val("stmc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	  // Shung Tak Catholic English College - Student Learning Profile
	 case "stcec_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("stcec");
		document.form1.action="customizeReport/index.php";
		 	  // Shung Tak Catholic English College - Student Learning Profile
		break;
	 case "lcg_slp":
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(1);
     	
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		//$("#task").val("lcg_slp");
		$("#task").val(reportType);
		document.form1.action="customizeReport/index.php";
 	 break;
	 case "lcg_slp2":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(1);
     	js_Show_Report_Footer(1);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		//$("#task").val("lcg_slp");
		$("#task").val(reportType);
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "htc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("htc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "lskc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("lskc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "yttmc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(1,'10','27');
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("yttmc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "toss_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
         js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("toss");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "nwcss_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("nwcss");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	  case "ltfc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("ltfc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "ychwws_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(1,'12','30');
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
      $("#btn_print_word").removeAttr("disabled");
	  $("#btn_print_word").show();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		
		$("#task").val("ychwws");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "hwc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
		
		
		$("#task").val("hwc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "mcdhmc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
		
		
		$("#task").val("mcdhmc");
	  jSupportCurrentAlumniStudentSelect();
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	  case "spkc_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_PAGE(0);
		jSHOW_PASSMARK(1);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		$("#task").val("spkc");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "plhk_slp":
	  
//		jSHOW_ISSUEDATE(1);
		jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		jSHOW_NUMBER_RECORD(1,30);
     	jSHOW_SELFACCOUNT(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
        
        js_Class_Selection('single');
        
        $("#btn_print_pdf").hide();
        $("#btn_print_word").hide();
        $("#btn_print_html").show();
        $("#MaxPrizeSizeRow").hide();
        $("#MaxECASizeRow").hide();
		
		$("#task").val("plhk");
		document.form1.action="customizeReport/index.php";
 	 break;
 	 
 	 case "sagc_slp":
//      jSHOW_YEAR(1);
      jSHOW_ALLYEAR(1);
      jALLOW_MULTIYEAR(1);
//      jSHOW_ISSUEDATE(1);
      jSHOW_BILANG(1);
      jSHOW_ADVANCESETTING(0);
      jSHOW_PRINTISSUEDATE(1);
      js_Class_Selection(StudentType);
      jSHOW_ACADEMIC_RESULT_MODE(1);
      jSHOW_SELFACCOUNT(0);
      jSHOW_NUMBER_PAGE(0);
      js_Show_Report_Header(0);
      js_Show_Report_Footer(0);
      
      //jSupportCurrentAlumniStudentSelect();
	  $("#trgType_current").attr('checked',true);	

      $("#btn_print_html").hide();
      $("#btn_print_pdf").show();
      $("#btn_print_word").hide();
      $("#MaxPrizeSizeRow").hide();
      $("#MaxECASizeRow").hide();
      
	  document.form1.action="report_print.php";
      break;
      
 	 case "cchpw_slp":
//      jSHOW_YEAR(1);
      	jSHOW_ALLYEAR(1);
		jALLOW_MULTIYEAR(1);
//      jSHOW_ISSUEDATE(1);
		jSHOW_BILANG(0);
		jSHOW_ADVANCESETTING(0);
		jSHOW_PRINTISSUEDATE(1);
		js_Class_Selection(StudentType);
		jSHOW_ACADEMIC_RESULT_MODE(0);
		jSHOW_SELFACCOUNT(0);
		js_Show_Report_Footer(0);
		jSHOW_NUMBER_PAGE(1,'5','8');
		$("#printIssueDate").attr('checked',false);	
		
      js_Show_Report_Header(0);
      
		//jSupportCurrentAlumniStudentSelect();
		$("#trgType_current").attr('checked',true);	
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();
		$("#btn_print_html").show();
		$("#btn_print_pdf").show();
		$("#btn_print_word").hide();
		$("#task").val("cchpw");
		document.form1.action="customizeReport/index.php";
      break;
      
     case "ngwah_sas":
		jALLOW_MULTIYEAR(0);
		jALLOW_ALLYEAR(0);
		jSHOW_ISSUEDATE(0);
		jSHOW_PRINTISSUEDATE(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);	
//		showYearSelection = true; 
		
		// Added to support alumni records		
//		jSupportCurrentAlumniStudentSelect();
//	  	$("#trgType_current").attr('checked',true);	
		
		$("#btn_print_pdf").show();
		$("#btn_print_html").show();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();
		$("#task").val(reportType);
		document.form1.action="customizeReport/index.php";
 	  break;

     case "tmchkwc_slp":
		jALLOW_MULTIYEAR(0);
		jALLOW_ALLYEAR(0);
		jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);	
		showYearSelection = true; 
		
		// Added to support alumni records		
		jSupportCurrentAlumniStudentSelect();
	  	$("#trgType_current").attr('checked',true);	
		
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();
		$("#task").val(reportType);
		document.form1.action="customizeReport/index.php";
 	  break;

     case "tmchkwc_jyr":
		jALLOW_MULTIYEAR(0);
		jALLOW_ALLYEAR(0);
		jSHOW_ISSUEDATE(1);
		jSHOW_PRINTISSUEDATE(0);
	    jSHOW_BILANG(0);
        jSHOW_ADVANCESETTING(0);
		jSHOW_NUMBER_PAGE(0);
     	jSHOW_SELFACCOUNT(0);
     	js_Show_Report_Header(0);
     	js_Show_Report_Footer(0);
		js_Class_Selection(StudentType);	
		showYearSelection = true; 
		
		// Added to support alumni records		
		jSupportCurrentAlumniStudentSelect();
	  	$("#trgType_current").attr('checked',true);	
		
		$("#btn_print_pdf").show();
		$("#btn_print_html").hide();
		$("#btn_print_word").hide();
	      $("#MaxPrizeSizeRow").hide();
	      $("#MaxECASizeRow").hide();
		$("#task").val(reportType);
		document.form1.action="customizeReport/index.php";
 	  break;
     case "tmgss":
//       jSHOW_YEAR(1);
       jSHOW_ALLYEAR(0);
       jALLOW_MULTIYEAR(0);
       jSHOW_ISSUEDATE(1);
       jSHOW_BILANG(0);
       
       jSHOW_ADVANCESETTING(0);
       jSHOW_NUMBER_PAGE(0);
      	jSHOW_SELFACCOUNT(0);
       js_Show_Report_Header(0);
       js_Show_Report_Footer(0);
       $("#MaxPrizeSizeRow").hide();
       $("#MaxECASizeRow").hide();
       jSHOW_PRINTISSUEDATE(1);
       
       js_Class_Selection(StudentType);
       
       $("#btn_print_html").show();
       $("#btn_print_pdf").hide();
 	   $("#btn_print_word").show();
 	  $("#printIssueDate").attr('disabled', true);
 	   document.form1.action="report_print.php";
      break;

     case "nlp_slp1":
     case "nlp_slp2":
      
//        jSHOW_YEAR();
	   $("#task").val(reportType);
       jSHOW_ALLYEAR(0);
       jALLOW_MULTIYEAR(0);
       jSHOW_ISSUEDATE(1);
       jSHOW_BILANG(0);
	   showYearSelection = false;
       jSHOW_ADVANCESETTING(0);
       jSHOW_NUMBER_PAGE(0);
      	jSHOW_SELFACCOUNT(0);
       js_Show_Report_Header(0);
       js_Show_Report_Footer(0);
       $("#MaxPrizeSizeRow").hide();
       $("#MaxECASizeRow").hide();
   
       $("#AcademicYearSelection").hide();
       js_Class_Selection(StudentType);
       
       $("#btn_print_html").hide();
       $("#btn_print_pdf").show();
 	   $("#btn_print_word").hide();
// 	   $("#printIssueDate").attr('disabled', true);
	   jSHOW_PRINTISSUEDATE(0);
	   $('label[for="printIssueDate"]').hide();
	   <? $IssueDateNow = date("Y-m-d"); ?>
		document.getElementById("issuedate").value = "<?=$IssueDateNow?>";	
 	   document.form1.action="customizeReport/index.php";
      break;
      
  }
  $("#studentRow").hide();
  
  if (showYearSelection)
  {
  	jSHOW_YEAR(1);
  } else
  {
  	jSHOW_YEAR(0);
  }
  jSHOW_YEARTERM();
  
}

function jSHOW_customFieldRow(jParShow){
	if(jParShow)
	  {
	    $("tr#customFieldRow").show();
	  }
	  else
	  {
	    $("tr#customFieldRow").hide();
	  }
}

function jSHOW_YEAR(jParShow)
{
  if(jParShow)
  {
    $("#AcademicYearSelection").show();
  }
  else
  {
    $("#AcademicYearSelection").hide();
  }
}

function jSHOW_ALLYEAR(jParShow)
{

	if(jParShow)
	{
		$("#AYCheckMasterRow").show();
		$("#AYCheckMaster").show();
	}
	else
	{
		$("#AYCheckMasterRow").hide();
	}
}


function jALLOW_ALLYEAR(jParShow)
{

	if(jParShow)
	{
		$("#AYCheckMaster").show();
	}
	else
	{
		$("#AYCheckMaster").hide();
	}
}

function jALLOW_MULTIYEAR(jParAllow)
{
	if(jParAllow)
	{
		$("input[name='academicYearID[]']").each(function(){
			var id = $(this).attr("id");
			var name = $(this).attr("name");
			var value = $(this).attr("value");
			
			var html = '<input type="checkbox" name="'+name+'" id="'+id+'" value="'+value+'" />'; 
			$(this).after(html).remove(); // add new, then remove original input 
		});
	}
	else
	{
		$("input[name='academicYearID[]']").each(function(){
			var id = $(this).attr("id");
			var name = $(this).attr("name");
			var value = $(this).attr("value");
			
			var html = '<input type="radio" name="'+name+'" id="'+id+'" value="'+value+'" />'; 
			$(this).after(html).remove(); // add new, then remove original input 
		});
	}
}

function jSHOW_YEARTERM(){

//	var reportType = parseInt($("input[name=ReportType]:checked").val());
  var reportType = $("input[name=ReportType]:checked").val();
  var ayChecked = $("input[name='academicYearID[]']:checked").length;
  var ayID = $("input[name='academicYearID[]']:checked").val();

  var disable_yt = [3, 7, 9, 11, 12,'cactm','cactm_second','mms','ghslp','kc_slp','stmc_slp','stcec_slp','htc_slp','lskc_slp','yttmc_slp','toss_slp','nwcss_slp','ltfc_slp','hwc_slp','mcdhmc_slp','ychwws_slp','spkc_slp','plhk_slp','lcg_slp','lcg_slp2','fywss_slp','ngwah_sas','tmchkwc_slp','tmchkwc_jyr'];

  // show year term selection criteria:
  // 1. repor not support semester select (reportType not found in disable_yt)
  // 2. only one year is selected (ayChecked = 1)
  // 3. all year option is not selected (ayID != "") 
  if($.inArray(reportType, disable_yt) == -1 && ayChecked == 1 && ayID != "")
  {
    // call ajax to return year term selection only when one year is selected
		$.ajax({
			type: "GET",
			url: "../../ajax/ajax_get_yearterm_opt.php",
			data: {
				ay_id: $("input[name='academicYearID[]']:checked").val()
			},
			beforeSend: function () {
				$("#yearTermRow").find("td").eq(1).html('<img src="<?=$intranet_httppath?>/images/2009a/indicator.gif" />');
			},
			success: function (msg) {
				if(msg != "")
				{
					$("#yearTermRow").find("td").eq(1).html(msg);
					$("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['whole_year']?>");
				}
			}
		});
		$("#yearTermRow").show();
  }
  else
  {
    $("#yearTermRow").find("td").eq(1).html("");
    $("#yearTermRow").hide();
  }
}

function jSHOW_REMARK(jParShow){
	if(jParShow)
	{
		$("#printRemarks").show();
	}
	else{
		$("#printRemarks").hide();
	}
}

function jSHOW_ISSUEDATE(jParShow)
{
  if(jParShow)
	{
		$("#issueDateRow").show();
	}
	else
	{
		$("#issueDateRow").hide();
	}
	document.getElementById("issuedate").value = "";	
}

function jSHOW_PASSMARK(jParShow)
{
  if(jParShow)
	{
		$("#passMarkRow").show();
	}
	else
	{
		$("#passMarkRow").hide();
	}
}

function jSHOW_NUMBER_PAGE(jParShow,NoOfFirstPage,NoOfOtherPage)
{
   if ( NoOfFirstPage === undefined ) {
      NoOfFirstPage = '';
   }
    if ( NoOfOtherPage === undefined ) {
      NoOfOtherPage = '';
   }
	
  if(jParShow)
	{
		$("#NoOfFirstPageRow").show();
		$("#NoOfOtherPageRow").show();
		
		$("#NoOfFirstPage").val(NoOfFirstPage);
		$("#NoOfOtherPage").val(NoOfOtherPage);
	}
	else
	{
		$("#NoOfFirstPageRow").hide();
		$("#NoOfOtherPageRow").hide();
		
		$("#NoOfFirstPage").val('');
		$("#NoOfOtherPage").val('');
	}
}

function jSHOW_NUMBER_RECORD(jParShow,NoOfRecord)
{
    if ( NoOfRecord === undefined ) 
    {
      NoOfRecord = '';
   	}
	
  	if(jParShow)
	{
		$("#NoOfRecordIntRow").show();
		$("#NoOfRecordExtRow").show();

		$("#NoOfRecordIntRow").val(NoOfRecord);
		$("#NoOfRecordExtRow").val(NoOfRecord);
	}
	else
	{
		$("#NoOfRecordIntRow").hide();
		$("#NoOfRecordExtRow").hide();

		$("#NoOfRecordIntRow").val('');
		$("#NoOfRecordExtRow").val('');
	}
}

function jSHOW_PRINTISSUEDATE(jParShow){
	$('label[for="printIssueDate"]').show();
	if(jParShow){
		$("#printIssueDate").show();
	}else{
		$("#printIssueDate").hide();
	}
}
function jSHOW_BILANG(jParShow)
{
  if(jParShow)
	{
		$("#bi_lang").show();	
	}
	else
	{
		$("#bi_lang").hide();
	}
}

function jSHOW_ACADEMIC_RESULT_MODE(jParShow) {
	if (jParShow) {
		$('#academicScoreDisplayModeRow').show();
	}
	else {
		$('#academicScoreDisplayModeRow').hide();
	}
}

function jSHOW_ADVANCESETTING(jParShow)
{
  if(jParShow)
  {
    $("#advSettingRow").show();
  }
  else
  {
    $("#advSettingRow").hide();
  }
}


function jSHOW_SELFACCOUNT(jParShow){
	if(jParShow){
		$("#printSelfAccount").show();
	}else{
		$("#printSelfAccount").hide();
	}
}

function js_Show_Report_Header(jParShow){
	if(jParShow){
		$("#printReportHeader").show();
	}else{
		$("#printReportHeader").hide();
	}	
}

function js_Show_Report_Footer(jParShow){
	if(jParShow){
		$("#printReportFooter").show();
	}else{
		$("#printReportFooter").hide();
	}	
}

function jGEN_FILE_TEMPLATE()
{
<?php if($sys_custom["ccym_ole_report"]) { ?>
	var ycID = document.getElementById('YearClassID').value;

	if(ycID != "")
		window.location = 'gen_staraward_sample.php?YearClassID='+ycID;
	else
		alert("<?=$ec_warning['please_select_class']?>");
<?php } else { ?>
  window.location = 'gen_ele_desc_sample.php';
<?php } ?>
}


function jCHANGE_CLASS(jParStudentType)
{
	if($("#YearClass").val() != "")
	{
  	$.ajax({
  		type: "GET",
  		url: "ajax_get_class_student.php",
  		data: "YearClassID="+$("#YearClass").val()+"&StudentType="+jParStudentType,
  		beforeSend: function () {
		    $("#studentRow").find("td").eq(1).html('<img src="/images/2009a/indicator.gif" />');
      },
  		success: function (msg) {
        if(msg != "")
        {
          $("#studentRow").find("td").eq(1).html(msg);
          
          if(jParStudentType=='multiple')
          {
          	 js_Select_All('StudentIDSel', 1);
          }
         
        }
  		}
  	});
	
		$("#studentRow").show();
	}
	else
	{
    $("#studentRow").hide();
  }
}

function jPrintReport(jParPrintType)
{
	if($("input[name=ReportType]:checked").val() == 8)
	{
		if(!check_select(document.form1.ProgramID, globalAlertMsg18, ""))
  			return;
	}
	else
	{
		// check if any year option (include all year) is checked
		if(!$("#AcademicYearSelection").is(':hidden') && $("input[name='academicYearID[]']:checked").size() == 0)
		{
			alert(globalAlertMsg18);
			return;
		}

		var targetType = $("input[name='trgType']:checked").val();
		if(targetType.toLowerCase() == 'current'){
			// check if any class is selected
			if(!check_select(document.getElementById('YearClass'), globalAlertMsg18, ""))
			{
				$("select[id=YearClass]").focus();
				return;
			}
			// check if no student can be selected
			//else if($(getJQuerySafeId('#StudentIDSel:not([multiple]')).is(":visible"))
			else if(!$('#StudentIDSel').attr('multiple') && $('#StudentIDSel').is(":visible"))
			{
				if(document.getElementById('StudentIDSel').length <= 1){
					alert(globalAlertMsg18);
					$("select[id=YearClass]").focus();
					return;
				}
			}
			else if($('#StudentIDSel').is(":visible") && document.getElementById('StudentIDSel').length == 0)
			{
				alert(globalAlertMsg18);
				$("select[id=YearClass]").focus();
				return;
			}
		}
		else
		{
			var student_checked = $("input[id^='alumni_studentArr_']:checked").size();   
			if (student_checked == 0) 
			{
				alert(globalAlertMsg18);
				$("input[id^='alumni_studentArr_']:first").focus();
				return; 
			} 
		}
		
		if($("input[name=ReportType]:checked").val() == 6)
		{
			// check if issue date is in correct format
			if($("input[name=issuedate]").val() != "")
			{
				if(!check_date(document.form1.issuedate, "<?=$i_invalid_date?>"))
				return;
			}
			
			// check if any report lang is selected
			var lang_checked = $("input[name='slpReportLang[]']:checked").size(); 				
			if (lang_checked == 0) 
			{ 
				alert("<?=$w_alert['Invalid_Lang']?>");
				return; 
			} 
		}
	}

	if($("input[name=ReportType]:checked").val() == 'nlp_slp1' ||$("input[name=ReportType]:checked").val() == 'nlp_slp2' ){
		if( $("#issuedate").val() == '' ||  $("#issuedate").val()  == null){
				alert("<?=$Lang['General']['WarningArr']['InputDate']?>");
				return;
			}
		
	}

	var url = "report_print.php";
	document.form1.PrintType.value = jParPrintType;
	document.form1.IsZip.value = '0';
	//newWindow(url, 28);
	//document.form1.target = "ec_popup28";
	
	if($("input[name=ReportType]:checked").val()==6 && jParPrintType=='pdf' && $("select#StudentIDSel").val()=='') {
		js_Show_Export_Batch_Thickbox();
	} else if ($("input[name=ReportType]:checked").val()=='sagc_slp' && jParPrintType=='pdf' && $("select#StudentIDSel").val()==''){
		js_Show_Export_Batch_Thickbox();
	} else {
		document.form1.submit();
	}	
}

function js_Show_Export_Batch_Thickbox() {
	$('a#ThickboxHiddenLink').attr('title', '<?=$ec_iPortfolio['SLP']['ExportAsPdf']?>').click();
}

function js_Load_Export_Batch_Thickbox_Layer() {
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Reload_Export_Batch_Thickbox_Layer"
		},
		function(ReturnData) {
			$('#TB_ajaxContent').html(ReturnData);
		}
	);
}

function js_Go_Export_Batch() {
	if ($('input#PrintingMode_Zip').attr('checked')) {
		document.form1.IsZip.value = '1';
	}
	
	document.form1.submit();
	js_Hide_ThickBox();
}

function js_Class_Selection(ParStudentType)
{	
	$('td#classRowTD').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Class_Selection",
			StudentType: ParStudentType
		},
		function(ReturnData) {
			$('td#classRowTD').html(ReturnData);
		}
	);
}

$("document").ready(function(){
	// initialize select full report
	jCHANGE_REPORT_TYPE();

  $("#printIssueDate").click(function(){
    if($(this).attr('checked'))
    {
      $('#issuedate').removeAttr("disabled");
    }
    else
    {
      $('#issuedate').attr("disabled", true);
    }
  });
  
  $("#AYCheckMaster").live("click", function()				
	{
		if($(this).attr("type") == "checkbox")
		{
			var checked_status = $(this).attr("checked");
	    $("input[name='academicYearID[]']").each(function()
			{
				$(this).attr("checked", checked_status);
			});
		}
		
		jSHOW_YEARTERM();
	});
	
	$("input[name='academicYearID[]']").live("click", function(){
    jSHOW_YEARTERM();
  });
});
function changeSelectReportTarget(reportTargetType){
 /*reportTargetType = 1  , print for current student
  *reportTargetType = 2  , print for alumni student
 */
	if(reportTargetType == 1){
		$("#ipf_targetForCurrent").show();
		$("#targetForAlumni").hide();

		//reset alumni UI related item
	    $("#trgType_current").attr('checked',true);	
		$("#alumni_div").html(''); //reset HTML table , result set of "ALUMNI STUDENT"
		$("#r_alumni_studentName").val('');
		$("#r_alumni_studentWebsams").val('');

	}else{
		$("#ipf_targetForCurrent").hide();
		$("#targetForAlumni").show();
	}

}
function jSupportCurrentStudentSelectOnly(){
	$("#ipf_currentAlumniSelect").hide();
}
function jSupportCurrentAlumniStudentSelect(){
	$("#ipf_currentAlumniSelect").show();
}
function searchForAlumni(){
	var s_name = $("#r_alumni_studentName").val();
	var s_websams = $("#r_alumni_studentWebsams").val();
	
	if(s_name == '' && s_websams == ''){
		alert('<?= $iPort["msg"]["InputOneItem"]?>');
		$('#r_alumni_studentName').focus();
		return false;
	}

	$.ajax({
    url:      "searchAlumniStudent.php",
    type:     "POST",
    async:    true,
    data:     "r_alumni_studentName="+s_name+"&r_alumni_studentWebsams="+s_websams,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
					$("#alumni_div").html(data);
              }
	});


}

function checkAllAlumniCheckBox (flag){
   if (flag == true){
      $("input[id^='alumni_studentArr_']").attr('checked', true);
   }
   else{
      $("input[id^='alumni_studentArr_']").attr('checked', false);
   }
	return;
}
</script>

<FORM action="report_print.php" method="POST" id="form1" name="form1" target="_blank">
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td width="6%">&nbsp;</td>
				<td><table>
				<tr>
				<td><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['reportPrinting_instruction'], "")?></td>
				</tr>
				</table>
					<table border="0" cellspacing="0" cellpadding="5" width="100%">
					  <?=$record_type_selection?>
					  <tr id="AcademicYearSelection">
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td valign="top"><?=$ay_selection_html?></td>
						</tr>
						<tr id='yearTermRow' style='display:none'>
							<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$ec_iPortfolio['semester']?></td>
							<td valign="top" class="tabletext">
							</td>
						</tr>
						<tr id='recordTypeRow_sptss' style='display:none'>
							<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$ec_iPortfolio['record_type']?></td>
							<td valign="top" class="tabletext">
								<?=$reportTypeOption?>
							</td>
						</tr>
						<tr id='classRow'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?= $Lang['Identity']['Student']?> : </span></td>
							<td valign="top">
							<!--hr/-->
								<div id="selectStudentDiv_CurrentOnly">

								<div id="ipf_currentAlumniSelect" style="display:none;">
									<input type="radio" name="trgType" value = "current" id="trgType_current" onclick="changeSelectReportTarget('1')" checked><label for="trgType_current"><?= $iPort["CurrentStudent"]?></label>&nbsp;&nbsp;
									<input type="radio" name="trgType" value = "alumni" id="trgType_alumni" onclick="changeSelectReportTarget('2')"><label for="trgType_alumni"><?= $Lang['Identity']['Alumni']?></label>
								</div>
								
								<div id="ipf_targetForCurrent">
									<table border = "0">
										<tr>
											<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238); width:48px" nowrap="nowrap" valign="top">
												<span class="tabletext"><?=$ec_iPortfolio['class']?></span>
											</td>
											<td id='classRowTD' valign="top">
												<!--fill with ajax content with loading this page-->
											</td>
										</tr>
										<tr id='studentRow' style='display:none'>
											<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">
												<span class="tabletext"><?=$i_identity_student?></span>
											</td>
											<td><!-- content fill with ajax--></td>
										</tr>
									</table>									
								</div>

								<div id="targetForAlumni" style="display:none">
									<table border="0">
										<tr>
											<td><?=$Lang['AccountMgmt']['StudentName']?> : </td><td><input type="text" name="r_alumni_studentName" id="r_alumni_studentName"></td>
											<td><?=$ec_iPortfolio['heading']['student_regno']?> : </td><td><input type="text" name="r_alumni_studentWebsams" id="r_alumni_studentWebsams"></td>
											<td><input type="button" onClick="searchForAlumni()" value="<?=$Lang['Btn']['Search']?>"></td>
										</tr>
									</table>

									<div id="alumni_div"></div>
								</div> <!-- close targetForAlumni -->
							<!-- div id="selectStudentDiv_CurrentAnyAlumni" style="display:none;">
							</div -->

								<!--hr/-->
							</td>
						</tr>
						
						
						<tr id='NoOfFirstPageRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['NoOfFirstPage']?></span></td>
							<td>
								<input name="NoOfFirstPage" id="NoOfFirstPage" type="text" class="tabletext" value="" />
							</td>
						</tr>
						<tr id='NoOfOtherPageRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['NoOfOtherPage']?></span></td>
							<td>
								<input name="NoOfOtherPage" id="NoOfOtherPage" type="text" class="tabletext" value="" />
							</td>
						</tr>
						<tr id='NoOfRecordIntRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['NoOfRecordInt']?></span></td>
							<td>
								<input name="NoOfRecordInt" id="NoOfRecordInt" type="text" class="tabletext" value="25" />
							</td>
						</tr>
						<tr id='NoOfRecordExtRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['NoOfRecordExt']?></span></td>
							<td>
								<input name="NoOfRecordExt" id="NoOfRecordExt" type="text" class="tabletext" value="19" />
							</td>
						</tr>
						<tr id='passMarkRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['PassMark']?></span></td>
							<td>
								<input name="passMark" id="passMark" type="text" class="tabletext" value="" />
							</td>
						</tr>
						
						
						<tr id='issueDateRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['date_issue']?></span></td>
							<td>
								<input name="issuedate" id="issuedate" type="text" class="tabletext" value="<?=$IssueDateNow?>" />
								<?=$linterface->GET_CALENDAR("form1", "issuedate");?>
								<br/><div> <input type = "checkbox" name="printIssueDate" value = "1" id ="printIssueDate" checked ><label for="printIssueDate" ><?=$Lang['iPortfolio']['print_date_issue']?></label></div>
							</td>
						</tr>
						
						<tr id='MaxECASizeRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['twghczm']['MaxECASize'] ?></span></td>
							<td>
								<input name="maxECAsize" id="maxECAsize" type="number" class="tabletext" />
							</td>
						</tr>
						
						<tr id='MaxPrizeSizeRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['twghczm']['MaxPrizeSize'] ?></span></td>
							<td>
								<input name="maxPrizesize" id="maxPrizesize" type="number" class="tabletext"/>
							</td>
						</tr>
												
						<tr id='DisplayRecordNum' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['bps']['MaxRecordsSize']?></span></td>
							<td>
								<input name="DisplayRecordNum" id="DisplayRecordNum" type="number" class="tabletext" />
							</td>
						</tr>
						
						<tr id='bi_lang' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['print_slp_lang']?></span></td>
							<td>
								<input type = "checkbox" name="slpReportLang[]" value = "b5" id ="slpReportLang_B5" checked><label for="slpReportLang_B5" ><?=$Lang['iPortfolio']['print_slp_lang_b5']?></label>
								<input type = "checkbox" name="slpReportLang[]" value = "en" id ="slpReportLang_En" checked><label for="slpReportLang_En" ><?=$Lang['iPortfolio']['print_slp_lang_en']?></label>
							</td>
						</tr>
						<tr id='academicScoreDisplayModeRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['AcademicResult']?></span></td>
							<td>
								<?=$Lang['iPortfolio']['DisplayOptions']?>:
								<br />
								<!--<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['mark']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['mark']?>" /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['mark']?>"><?=$Lang['iPortfolio']['MarkOnly']?></label>-->
								<!--<br />-->
								<!--<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['grade']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['grade']?>" /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['grade']?>"><?=$Lang['iPortfolio']['GradeOnly']?></label>-->
								<!--<br />-->
								<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>" checked /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>"><?=$Lang['iPortfolio']['MarkThenGrade']?></label>
								<br />
								<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>" /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>"><?=$Lang['iPortfolio']['GradeThenMark']?></label>
								<br />
								<br />
								<?=$Lang['iPortfolio']['DisplayFullMark']?>:
								<br />
								<input type="radio" id="displayFullMark_Yes" name="displayFullMark" value="1" checked /> <label for="displayFullMark_Yes"><?=$Lang['General']['Yes']?></label>
								<input type="radio" id="displayFullMark_No" name="displayFullMark" value="0" /> <label for="displayFullMark_No"><?=$Lang['General']['No']?></label>
								<br />
								<br />
								<?=$Lang['iPortfolio']['DisplayComponentSubject']?>:
								<br />
								<input type="radio" id="displayComponentSubject_Yes" name="displayComponentSubject" value="1" /> <label for="displayComponentSubject_Yes"><?=$Lang['General']['Yes']?></label>
								<input type="radio" id="displayComponentSubject_No" name="displayComponentSubject" value="0" checked /> <label for="displayComponentSubject_No"><?=$Lang['General']['No']?></label>
							</td>
						</tr>
						<tr id='printSelfAccount' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['DisplaySelfAccount']?></span></td>
							<td>
								<input type="radio" id="displaySelfAccount_Yes" name="displaySelfAccount" value="1" checked /> <label for="displaySelfAccount_Yes"><?=$Lang['General']['Yes']?></label>
								<input type="radio" id="displaySelfAccount_No" name="displaySelfAccount" value="0" /> <label for="displaySelfAccount_No"><?=$Lang['General']['No']?></label>
							</td>	
						</tr>
						<tr id='printReportHeader' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['DisplayReportHeader']?></span></td>
							<td>
								<input type="radio" id="displayReportHeader_Yes" name="displayReportHeader" value="1" checked /> <label for="displayReportHeader_Yes"><?=$Lang['General']['Yes']?></label>
								<input type="radio" id="displayReportHeader_No" name="displayReportHeader" value="0" /> <label for="displayReportHeader_No"><?=$Lang['General']['No']?></label>
							</td>	
						</tr>
						<tr id='printReportFooter' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['DisplayReportFooter']?></span></td>
							<td>
								<input type="radio" id="displayReportFooter_Yes" name="displayReportFooter" value="1" checked /> <label for="displayReportFooter_Yes"><?=$Lang['General']['Yes']?></label>
								<input type="radio" id="displayReportFooter_No" name="displayReportFooter" value="0" /> <label for="displayReportFooter_No"><?=$Lang['General']['No']?></label>
							</td>	
						</tr>
						<tr id='printRemarks' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['General']['Remark']?></span></td>
							<td>
								<?php echo $linterface->GET_TEXTBOX_NAME('custRemarks', 'custRemarks', '')?>
							</td>	
						</tr>
						<?=$advance_setting_html?>
<!--
						<tr id='programRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext">Program</span></td>
							<td>
								<?=$volunteer_selection_html?>
							</td>
						</tr>
-->
						<?php  
						if($sys_custom['iPf']['twghwfns']['Report']['SLP']){
							$customFieldLang = Get_Lang_Selection( mb_convert_encoding('&#x5E95&#x908A','UTF-8', 'HTML-ENTITIES'), 'Bottom-margin' );
							$customFieldInput = '<input name="customField" id="customField" type="number" min="0" max="30" class="tabletext" value="20" />';
						}else{
							$customFieldLang = '';
							$customFieldInput = '';
						}
						?>
						<tr id='customFieldRow' style='display:none'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$customFieldLang?></span></td>
							<td>
								<?php echo $customFieldInput?>
							</td>
						</tr>
						<tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>&nbsp;</td>
							<td align="center">
								<?=$AcademicYearID_hiddenField_html?>
								<input type="hidden" id="PrintType" name="PrintType" value="" />
								<input type="hidden" id="IsZip" name="IsZip" value="" />
								<input type="hidden" id="task" name="task" value="" />

								<input class="formbutton" type="button" value="<?=$button_print?>" onClick="jPrintReport('html');"; name="btn_print_html" id="btn_print_html" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
								<input class="formbutton" type="button" value="<?=$ec_iPortfolio['SLP']['ExportAsWordDocument']?>" onClick="jPrintReport('word');" name="btn_print_word" id="btn_print_word" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
								<input class="formbutton" type="button" value="<?=$ec_iPortfolio['SLP']['ExportAsPdf']?>" onClick="jPrintReport('pdf');"; name="btn_print_pdf" id="btn_print_pdf" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
								<?=$h_ThickboxHiddenLink?>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</tbody>
			</td>
		</tr>
	</table>
	
</FORM>