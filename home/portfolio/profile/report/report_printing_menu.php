<?php
/** [Modification Log] Modifying By:  anna  
 * *******************************************
 * 2018-05-24: Anna
 * - Added tmgss cust #133513
 * 
 * 2018-04-13: Anna
 * - Changed twghczm report name
 * 
 * 2016-12-07: Cameron
 * - Add options for tmchkwc (SLP Report & Student Profile Report [JinYingReport])
 * 
 * 2015-11-10: Omas
 * - Add options for sptss SLP Report case#F78756
 * 
 * 2015-09-30: Omas
 * - Add options for twghczm SLP Report case#J78825  
 * 
 * 2015-08-26: Omas
 * - Add options for twghwfns SLP Report case#C69320 
 * 
 * 2015-03-12: Omas
 * - Add options for SRLC SLP Report case#K74649
 *  
 * 2015-02-26: Omas
 * - Add options for CWGC SLP Report case#L70438 
 * 
 * 2015-01-12: Bill
 * - Add customized reprot - Ng Wah Catholic Secondary School - Student Award Scheme
 * 
 * 2014-07-23: Bill
 * - Add file config checking for Pui Kui SLP Report
 *
 * 2013-12-04: Yuen
 * - Added customized SLP for CCC Fong Yun Wah Secondary School
 *
 * 
 * 2013-10-22: Ivan [2013-1021-1354-34073]
 * - Hide export doc button for customized UCCKE full report
 * 
 * 2011-12-15: fai
 * - Added Lung Cheung Government Secondary School - Student Learning Profile
 *
 * 2011-11-09: Connie
 * - Added King's College - Student Learning Profile
 * 
 * 2011-01-28: Ivan
 * - Added Raimondi College Non-academic Performance Report
 * 
 * 2010-08-18: Max (201008180910)
 * - Plug NPL cust from IP20
 * 
 * 2010-02-25: Max (201002181147)
 * - Add config for cnecc
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once("report_lib/common.php");

intranet_auth();
intranet_opendb();
$lpf = new libpf_report();
$lpf_slp = new libpf_slp();

// check access right
$lpf->ACCESS_CONTROL("print_report");

########################################################
# Report printing options: Start
########################################################
### Standard Record type row ###
$RecordTypeArr = array();
$RecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_1\" value=\"1\" onClick=\"jCHANGE_REPORT_TYPE()\" CHECKED><label for=\"type_1\">".$ec_iPortfolio['full_report']."</label></span>";
$RecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_6\" value=\"6\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_6\">".$ec_iPortfolio['student_learning_profile']."</label></span>";
if($plugin['ReadingScheme']) $RecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_srp\" value=\"srp\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_srp\">".$Lang['iPortfolio']['SRP']['StudentReadingProfile']."</label></span>";
if(strstr($ck_user_rights_ext, "assessment_report")) $RecordTypeArr[] = "<span class='tabletext'><input type=\"radio\" name=\"ReportType\" id=\"type_2\" value=\"2\" onClick=\"jCHANGE_REPORT_TYPE()\" /><label for=\"type_2\" >".$ec_iPortfolio['assessment_report']."</label></span>";

### Customized Record type row ###
if($sys_custom["portfolio_customized_report"] || $sys_custom["portfolio_customized_report_keichi"] || $sys_custom["portfolio_customized_report_lasalle"] || $sys_custom["portfolio_customized_report_ihmc"]){
  $CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_3\" value=\"3\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_3\">lll ".$ec_iPortfolio['composite_performance_report']."</label></span>";
  $showAllYear = $sys_custom["portfolio_customized_report_ihmc"] ? 1 : 0;
} 
if($sys_custom["ywgs_ole_report"])
{
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_7\" value=\"7\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_7\">OLE Student-Reported Data</label></span>";
}

#Caritas Chong Yuet Ming Secondary School - Report
if($sys_custom["ccym_ole_report"])
{
	//ReportType value = 7 although id = type_8  , value 7 use for handle the layout control
  $CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_8\" value=\"7\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_8\">".$ec_iPortfolio['ccym_ole_report']."</label></span>";
}
if($sys_custom['cnecc_SS']) {
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_9\" value=\"9\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_9\">" . $Lang["Cust_Cnecc"]["SunshineProgramReport"] . "</label></span>";
}
# Belilios Public School Customization - Non-academic Performance Record
if($sys_custom['bps_napr']) {
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/bps/libpf-slp-bps-napr-report.php");
	$reportRefNumber = 10;
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_$reportRefNumber\" value=\"$reportRefNumber\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_$reportRefNumber\">" . $Lang["Cust_Bps"]["NonAcademicPerformanceRecord"] . "</label></span>";
}
# Po Leung Kuk Ngan Po Ling College Non-academic Achievement Report
if($sys_custom["portfolio_customized_report_plknpl"])
{
	$reportRefNumber = 11;
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_$reportRefNumber\" value=\"$reportRefNumber\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_$reportRefNumber\">".$Lang['Cust_Npl']['Non-AcademicAchievementReport']."</label></span>";
	$showAllYear = 1;
}

# Po Leung Kuk Ngan Po Ling College Non-academic Achievement Report
if($sys_custom['iPf']['RaimondoCollege']['Report']['NonAcademicPerformanceReport'])
{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/raimondi_college/libpf-raimondi_college.php");
	$reportRefNumber = 12;
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_$reportRefNumber\" value=\"$reportRefNumber\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_$reportRefNumber\">".$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Title']."</label></span>";
	$showAllYear = 1;
}
if($sys_custom["ipf"]["cactm"]["report"]["OLE"]){
	//宣道中學
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_cactm\" value=\"cactm\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_cactm\">".$Lang['iPortfolio']['Cust_cactm']['OLEReport']."</label></span>";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_cactm_second\" value=\"cactm_second\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_cactm_second\">".$Lang['iPortfolio']['Cust_cactm']['OLEReport2']." </label></span>";
}
if ($sys_custom['iPf']['GoodHopeSchool']['Report']['StudentLearningProfile']) {
	// 德���學校 - Student Learning Profile
	$reportCode = 'ghslp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_ghslp']['StudentLearningProfile'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['HongKongTrueLightCollege']['Report']['StudentLearningProfileReport']) {
	// 香港�������院 - Student Learning Profile Report
	$reportCode = 'hktlc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_hktlc']['StudentLearningProfileReport'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}

if ($sys_custom['iPf']['KingsCollege']['Report']['StudentLearningProfile']) {
	// King's College - Student Learning Profile
	$reportCode = 'kc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_kc']['StudentLearningProfileReport'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['ShaTinMethodistCollege']['Report']['StudentLearningProfile']) {
	// Sha Tin Methodist College - Student Learning Profile
	$reportCode = 'stmc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_stmc']['StudentLearningProfileReport']; 
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['ShungTakCatholicEnglishCollege']['Report']['StudentLearningProfile']) {
	// Shung Tak Catholic English College - Student Learning Profile
	$reportCode = 'stcec_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_stcec']['StudentLearningProfileReport'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['HolyTrinityCollege']['Report']['StudentLearningProfile']) {
	// Holy Trinity College - Student Learning Profile
	$reportCode = 'htc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_htc']['StudentLearningProfileReport'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['LeeShauKeeCollege']['Report']['StudentLearningProfile']) {
	// Lee Shau Kee College - Student Learning Profile
	$reportCode = 'lskc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_lskc']['StudentLearningProfileReport']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['YauTzeTinMemorialCollege']['Report']['StudentLearningProfile']) {
	// Yau Tze Tin Memorial College - Student Learning Profile
	$reportCode = 'yttmc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_yttmc']['StudentLearningProfileReport']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['TakOiSecondarySchool']['Report']['StudentLearningProfile']) {
	// Tak Oi Secondary School - Student Learning Profile
	$reportCode = 'toss_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_toss']['StudentLearningProfileReport']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['NgWahCatholicSecondarySchool']['Report']['StudentLearningProfile']) {
	// Ng Wah Catholic Secondary School - Student Learning Profile
	$reportCode = 'nwcss_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['LamTaiFaiCollege']['Report']['StudentLearningProfile']) {
	// Lam Tai Fai College - Student Learning Profile
	$reportCode = 'ltfc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['YanChaiHospitalWongWhaSanSecondarySchool']['Report']['StudentLearningProfile']) {
	// Yan Chai Hospital Wong Wha San Secondary School - Student Learning Profile
	$reportCode = 'ychwws_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['HonWahCollege']['Report']['StudentLearningProfile']) {
	// Hon Wah College - Student Learning Profile
	$reportCode = 'hwc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['MaChanDuenHeyMemorialCollege']['Report']['StudentLearningProfile']) {
	// Ma Chan Duen Hey Memorial College - Student Learning Profile
	$reportCode = 'mcdhmc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."(mcdhmc)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['StewardsPooiKeiCollege']['Report']['StudentLearningProfile']) {
	// Ma Chan Duen Hey Memorial College - Student Learning Profile
	$reportCode = 'spkc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['PentecostalLamHonKwongSchool']['Report']['StudentLearningProfile']) {
	// Ma Chan Duen Hey Memorial College - Student Learning Profile
	$reportCode = 'plhk_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
/*
if ($sys_custom['iPf']['PentecostalLamHonKwongSchool']['Report']['StudentLearningProfile']) {
	// Ma Chan Duen Hey Memorial College - Student Learning Profile
	$reportCode = 'plhk_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
*/
if ($sys_custom['iPf']['StAntoniusGirlsCollege']['Report']['StudentLearningProfile']) {
	// �������女��院 St. Antonius Girls College - Student Learning Profile
	$reportCode = 'sagc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['ChingChungHauPoWoon']['Report']['StudentLearningProfile']) {
	// ��松侯寶����學Ching Chung Hau Po Woon Secondary School - Student Learning Profile
	$reportCode = 'cchpw_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']."";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay." (cchpw)</label></span>";
}
if ($sys_custom['iPf']['CCCFongYunWahSS']['Report']['StudentLearningProfile']) {
	// CCC Fong Yun Wah Secondary School - Student Learning Profile
	$reportCode = 'fywss_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (FYWSS)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']) {
	// CCC Fong Yun Wah Secondary School - Student Learning Profile
	$reportCode = 'stpaul_PAS';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (Paulinian Award Scheme)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['pkms']['Report']['SLP'] && file_exists("$eclass_root/files/portfolio_plms_slp_config.txt")) {
	// Pui Kiu Middle School - Student Learning Profile
	$reportCode = 'pkms_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (Pui Kiu)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['cyma']['Report']['SLP']) {
	// Pui Kiu Middle School - Student Learning Profile
	$reportCode = 'cyma_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (CYMA)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['cwgc']['Report']['SLP']) {
	// CHRISTIAN ALLIANCE CHENG WING GEE COLLEGE - Student Learning Profile
	$reportCode = 'cwgc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (CWGC)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['srlc']['Report']['SLP']) {
	// St Rose Of Lima's College
	$reportCode = 'srlc_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (SRLC)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['srlc']['Report']['SLP']) {
	// St Rose Of Lima's College 2
	$reportCode = 'srlc_slp2';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile']." (SRLC2)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['css']['Report']['SLP']) {
	// Creative secondary school
	$reportCode = 'css_slp';
	$reportTitleDisplay = 'Student Development Progress Report';
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['twghwfns']['Report']['SLP']) {
	// TWGHs Wong Fut Nam College
	$reportCode = 'twghwfns_slp';
	$reportTitleDisplay = $Lang['iPortfolio']['SLP_reportArr']['StudentLearningProfile'].' (TWGHWFNS)';
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['twghczm']['Report']['SLP']) {
	// CTWGH CHEN ZAO MEN COLLEGE
	$reportCode = 'twghczm_slp';
//	$reportTitleDisplay = 'Testimonial';
	$reportTitleDisplay = $Lang['iPortfolio']['TWGHCZM']['StudentLearningProfile'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
if ($sys_custom['iPf']['sptss']['Report']['SLP']) {
	// STEWARDS POOI TUN SECONDARY SCHOOL	
	$reportCode = 'sptss_1';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_sptss']['report1'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
	
	$reportCode = 'sptss_2';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_sptss']['report2'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
	
	$reportCode = 'sptss_3';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_sptss']['report3'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
	
	$reportCode = 'sptss_4';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_sptss']['report4'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
	
	$reportCode = 'sptss_5';
	$reportTitleDisplay = $Lang['iPortfolio']['Cust_sptss']['report5'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}
# To do: Wai Kiu Customarization

if($sys_custom["ipf"]["mms"]["report"]["OLE"]){
	//MARYMOUNT SECONDARY SCHOOL
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_mms\" value=\"mms\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_mms\">".$Lang['iPortfolio']['Cust_mms']['OLEReport']."</label></span>";
}

if($sys_custom["ipf"]["lcg"]["report"]["studentLearningProfile"]){
	//Lung Cheung Government Secondary School
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_lcg\" value=\"lcg_slp\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_lcg\">".$Lang['iPortfolio']['Cust_hktlc']['StudentLearningProfileReport']."</label></span>";
}
if($sys_custom["ipf"]["lcg"]["report"]["studentLearningProfile2ndReport"]){
	//Lung Cheung Government Secondary School
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_lcg2\" value=\"lcg_slp2\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_lcg2\">".$Lang['iPortfolio']['Cust_hktlc']['StudentLearningProfileReport']." 2</label></span>";
}

if($sys_custom['NgWah_SAS']){
	# Ng Wah Catholic Secondary School - Student Award Scheme
	$reportCode = "ngwah_sas";
	$reportTitleDisplay = $Lang['iPortfolio']['NgWah_SAS_Report'];
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}

if ($sys_custom['iPf']['JinYingScheme']) {
	// The Methodist Church HK WESLEY College	
	$reportCode = 'tmchkwc_slp';	// Student Learning Profile
	$reportTitleDisplay = $Lang['iPortfolio']['JinYing']['Report']['SLP']." (TMCHKWC)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
	
	$reportCode = 'tmchkwc_jyr';	// Student Profile Report (JinYingReport)
	$reportTitleDisplay = $Lang['iPortfolio']['JinYing']['Report']['JYR']." (TMCHKWC)";
	$CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\" value=\"".$reportCode."\" onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$reportTitleDisplay."</label></span>";
}

# Tuen Mun Government Secondary School  - Student Performance Record
if($sys_custom['iPf']['tmgss']['Report']['StudentPerformance']) {
    include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmgss/libpf-slp-tmgss-spr-report.php");
    $reportCode= 'tmgss';
    $CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\"value=\"".$reportCode."\"  onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">" . $Lang["Cust_Tmgss"]["StudentPerformanceRecord"]. "</label></span>";
}

# Po Leung Kuk Ngan Po Ling College- Student Learning Profile
if($sys_custom['iPf']['npl']['Report']['StudentLearningProfile']) {
//     include_once($PATH_WRT_ROOT."includes/portfolio25/customize/nlp/libpf-slp-nlp.php");
    $reportCode= 'nlp_slp1';
    $CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\"value=\"".$reportCode."\"  onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$Lang['iPortfolio']['Cust_nlp']['OLECurrentYear']."</label></span>";

    $reportCode= 'nlp_slp2';
    $CustRecordTypeArr[] = "<span class=\"tabletext\"><input type=\"radio\" name=\"ReportType\" id=\"type_".$reportCode."\"value=\"".$reportCode."\"  onClick=\"jCHANGE_REPORT_TYPE()\"><label for=\"type_".$reportCode."\">".$Lang['iPortfolio']['Cust_nlp']['OLEWholeYear']."</label></span>";
    
}

# standard report selection
$record_type_selection = "<tr>";
$record_type_selection .= "<td class=\"formfieldtitle\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$ec_iPortfolio['report_type']."</span></td>";
$record_type_selection .= "<td width=\"80%\">";
$record_type_selection .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">";
for($i = 0; $i < count($RecordTypeArr); $i++)
{
	$record_type_selection .= "<tr>";
	$record_type_selection .= "<td>".$RecordTypeArr[$i]."</td>";
	$record_type_selection .= "</tr>";
}
$record_type_selection .= "</table>";
$record_type_selection .= "</td>";
$record_type_selection .= "</tr>";

# customized report selection
//$CustRecordTypeArr = array();
if(count($CustRecordTypeArr) > 0)
{
  $record_type_selection .= "<tr>";
  $record_type_selection .= "<td class=\"formfieldtitle\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$Lang['iPortfolio']['custReport']."</span></td>";
  $record_type_selection .= "<td width=\"80%\">";
  $record_type_selection .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">";
  for($i = 0; $i < count($CustRecordTypeArr); $i++)
  {
  	$record_type_selection .= "<tr>";
  	$record_type_selection .= "<td>".$CustRecordTypeArr[$i]."</td>";
  	$record_type_selection .= "</tr>";
  }
  $record_type_selection .= "</table>";
  $record_type_selection .= "</td>";
  $record_type_selection .= "</tr>";
}

# academic year selection
//$lay = new academic_year();
//$academic_year_arr = $lay->Get_All_Year_List();
//$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID[]' id='academicYear' onChange='get_yearterm_opt()'", $academicYearID, 1, 0, $i_Attendance_AllYear, 2);
# Generate multiple year checkboxes
$libacademic_year = new academic_year();
$ay_arr = $libacademic_year->Get_All_Year_List();
$ay_selection_html = "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
$ay_selection_html .= "<tr id=\"AYCheckMasterRow\"><td><input type=\"checkbox\" name=\"academicYearID[]\" id=\"AYCheckMaster\" value=\"\" /> <label for=\"AYCheckMaster\">{$i_Attendance_AllYear}</label></td><td>&nbsp;</td><td>&nbsp;</td></tr>";
$AcademicYearID_hiddenField_html = "";
for($i=0; $i<ceil(count($ay_arr)/3)*3; $i++)
{
  if($i % 3 == 0) {
    $ay_selection_html .= "<tr>";
  }
  
  if(isset($ay_arr[$i])){
    $ay_selection_html .= "<td width=\"150\"><input type=\"checkbox\" name=\"academicYearID[]\" id=\"ay_".$ay_arr[$i][0]."\" value=\"".$ay_arr[$i][0]."\" /> <label for=\"ay_".$ay_arr[$i][0]."\">".$ay_arr[$i][1]."</label></td>";
  }
  else {
    $ay_selection_html .= "<td width=\"150\">&nbsp;</td>";
  }
  
  if($i % 3 == 2) {
    $ay_selection_html .= "</tr>";
  }
  
  if(isset($ay_arr[$i])){
  	$thisAcademicYearID = $ay_arr[$i][0];
	$AcademicYearID_hiddenField_html .='<input type="hidden" id="All_AcademicYearID_'.$thisAcademicYearID.'" name="All_AcademicYearID[]" value="'.$thisAcademicYearID.'" />';
  }
}

$ay_selection_html .= "</table>";

/******* the following class selection process is replaced with ajax (ajax_reload.php)************/
//# Class Selection
//$lpf_ui = new libportfolio_ui();
//$lpf_fc = new libpf_formclass();
//$class_arr = $lpf_fc->getClassListArray();
//
//$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' id='YearClass' onChange='jCHANGE_CLASS()'");
//# END Class Selection
/******* the above class selection process is replaced with ajax (ajax_reload.php)************/


//====
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' id='YearClass' onChange='jCHANGE_CLASS()'", "", 0, 0, "", 2);
/*
$ClassArray_temp = $lpf->getReportClassListData();
# Limit class to taught classes only, if the user is not admin
$isAllClass = ($lpf->IS_IPF_ADMIN() || strstr($ck_user_rights_ext, "print_report,"));
if(!$isAllClass)
{
  $taught_ClassList = $lpf->GET_TEACHING_CLASS_BY_RIGHTS("print_report");
  $taught_ClassList = array_merge($taught_ClassList[0], $taught_ClassList[1]);
}
if(is_array($ClassArray_temp))
{
  for($i=0; $i<count($ClassArray_temp); $i++)
  {
    if($isAllClass || in_array($ClassArray_temp[$i], $taught_ClassList))
    {
      $ClassArray[] = array($ClassArray_temp[$i], $ClassArray_temp[$i]);
    }
  }
}
$class_selection_html = getSelectByArray($ClassArray, "name='Class' id='Class' onChange='jCHANGE_CLASS()'","",0,0,"",2);
*/

# YWGS Customization
if($sys_custom["ywgs_ole_report"])
{
  $slp_record_opt_html = "<tr>";
  $slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">Template:</span></td>";
  $slp_record_opt_html .= "<td>";
  $slp_record_opt_html .= "<input name=\"slplayout\" id=\"slplayout1\" type=\"radio\" class=\"tabletext\" value=\"1\" CHECKED /> <label for=\"slplayout1\">Without PERSONAL REFLECTIONS</label><br />";
  $slp_record_opt_html .= "<input name=\"slplayout\" id=\"slplayout2\" type=\"radio\" class=\"tabletext\" value=\"2\" /> <label for=\"slplayout2\">With PERSONAL REFLECTIONS</label>";
  $slp_record_opt_html .= "</td>";
  $slp_record_opt_html .= "</tr>";
  
  $slp_record_opt_html .= "<tr>";
  $slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$Lang['iPortfolio']['SLPRecord']."</span></td>";
  $slp_record_opt_html .= "<td>";
  $slp_record_opt_html .= "<input name=\"slpRecOpt\" id=\"slpRecAll\" type=\"radio\" class=\"tabletext\" value=\"all\" CHECKED /> <label for=\"slpRecAll\">".$Lang['iPortfolio']['allRecord']."</label>";
  $slp_record_opt_html .= "<input name=\"slpRecOpt\" id=\"slpRecSel\" type=\"radio\" class=\"tabletext\" value=\"sel\" /> <label for=\"slpRecSel\">".$Lang['iPortfolio']['selectedRecord']."</label>";
  $slp_record_opt_html .= "</td>";
  $slp_record_opt_html .= "</tr>";
  
  $slp_record_opt_html .= "<tr>";
  $slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$Lang['iPortfolio']['SpaceTop']."</span></td>";
  $slp_record_opt_html .= "<td>";
  $slp_record_opt_html .= "<input name=\"SpaceTop\" type=\"text\" class=\"tabletext\" /> mm ({$Lang['iPortfolio']['default']}: 6mm)";
  $slp_record_opt_html .= "</td>";
  $slp_record_opt_html .= "</tr>";
  
  $slp_record_opt_html .= "<tr>";
  $slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$Lang['iPortfolio']['SpaceBottom']."</span></td>";
  $slp_record_opt_html .= "<td>";
  $slp_record_opt_html .= "<input name=\"SpaceBottom\" type=\"text\" class=\"tabletext\" /> mm ({$Lang['iPortfolio']['default']}: 4mm)";
  $slp_record_opt_html .= "</td>";
  $slp_record_opt_html .= "</tr>";
}

if($sys_custom["ccym_ole_report"])
{
  $slp_record_opt_html = "<tr>";
  $slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\"><span class=\"tabletext\">".$i_general_select_csv_file."</span></td>";
  $slp_record_opt_html .= "<td>";
  $slp_record_opt_html .= "<input type=file name=\"AttachFile\" id=\"AttachFile\"><br />";
  $slp_record_opt_html .= "<a href='javascript:jGEN_FILE_TEMPLATE()'>".$i_general_clickheredownloadsample."</a>";
  $slp_record_opt_html .= "</td>";
  $slp_record_opt_html .= "</tr>";
}

if($sys_custom['iPf']['RaimondoCollege']['Report']['NonAcademicPerformanceReport'])
{
	$slp_record_opt_html = "<tr>";
		$slp_record_opt_html .= "<td width=\"1\">";
			$slp_record_opt_html .= "<input name=\"raimondi_withSelfAccount\" id=\"raimondi_withSelfAccount\" type=\"checkbox\" value=\"1\" />";
		$slp_record_opt_html .= "</td>";
		$slp_record_opt_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\">";
			$slp_record_opt_html .= "<span class=\"tabletext\">".$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['WithStudentSelfAccountAndYearGrouping']."</span>";
		$slp_record_opt_html .= "</td>";
	$slp_record_opt_html .= "</tr>";
}

$advance_setting_html = "<tr id='advSettingRow' style='display:none'>";
$advance_setting_html .= "<td class=\"formfieldtitle\" style=\"border-bottom: 1px solid rgb(238, 238, 238);\" nowrap=\"nowrap\" valign=\"top\">".$Lang['iPortfolio']['advanceSetting']."</td>";
$advance_setting_html .= "<td><table width=\"100%\">".$slp_record_opt_html."</table></td>";
$advance_setting_html .= "</tr>";


if($sys_custom['iPf']['sptss']['Report']['SLP']){
	$option1 = 'Form 1 - Form 3';
	$option2 = 'Form 4 - Form 6';
	$reportTypeAssoArr = array('1'=>$option1 ,'2'=> $option2 );
	$reportTypeOption = getSelectByAssoArray($reportTypeAssoArr,' id="recordType_sptss" name="recordType_sptss"','',0,1);
}
########################################################
# Report printing options: End
########################################################

$hideWordBtnForFullReport = 0;
if ($sys_custom['uccke_full_report']) {
	$hideWordBtnForFullReport = 1;
}


########################################################
# Layout Display
########################################################
# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentReportPrinting";
$CurrentPageName = $iPort['menu']['student_report_printing'];

# Hidden Link to call thickbox for exporting batch pdf
$OnClick = 'js_Load_Export_Batch_Thickbox_Layer();';
$h_ThickboxHiddenLink = $linterface->Get_Thickbox_Div(220, 750, $ExtraClass, $Title, $OnClick, $InlineID="FakeLayer", $Content="", $DivID='', $LinkID='ThickboxHiddenLink', $ExtraTags='style="display:none;"');

### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_report_print'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();
echo $linterface->Include_JS_CSS();
include_once("template/report_printing_menu.tmpl.php");
$linterface->LAYOUT_STOP();

intranet_closedb();
?>