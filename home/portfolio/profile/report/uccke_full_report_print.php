<?php
# Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-student.php");
intranet_auth();
intranet_opendb();
//	ob_start();											
//	var_dump($_POST);
//	$tmp = ob_get_contents();					
//	ob_end_clean();
//	error_log("array -->".$tmp."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");


$li = new libdb();
$li_pf = new libpf_slp();
$lpf_acc = new libpf_account_student();

$year_start["month"] = 7;
$year_start["day"] = 15;


$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}



# Convert Student Merit Summary, copy from retrieveStudentConvertedMeritTypeCountByDate() in libdisciplinev12.php
function convertStudentMeritSummary($dataAry)
{
    $ldiscipline = new libdisciplinev12();
    # Get Promotion Method
		$PromotionMethod = $ldiscipline->getDisciplineGeneralSetting("PromotionMethod");
		$conversionType_punish = $ldiscipline->getConversionPeriod();
		$conversionType_award = $ldiscipline->getConversionPeriod('', 1);
		
    $MeritPromotionSetting_punish = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod);
    $MeritPromotionSetting_award = $ldiscipline->retrieveMeritPromotionInfo($PromotionMethod, '', 1);
    
    if($PromotionMethod == "global")
		{
      # $dataAry[year][sem][meritType] = no. of meritType (this array is for one student)
      # $MeritPromotionSetting[meritType] = array(FromMerit(meritType), ToMerit, UpgradeNum)        
      foreach($dataAry as $year=> $semMeritArr){
        foreach($semMeritArr as $sem =>$monthMeritArr){
          foreach($monthMeritArr as $month => $meritDataArr){
            foreach($meritDataArr as $meritType => $thisMeritVal){
              $conversionType = (intval($meritType) > 0) ? $conversionType_award : $conversionType_punish;
            
              # Group the number of merit/demerit according to conversion type
              switch($conversionType["global"])
              {
                case 0:
                case 1:
                case 2:
                  # 'whole' as place holder
  				        $_dataAry[$year][$sem]['whole'][$meritType] += $thisMeritVal; 
  				        break;
                case 3:
  				        $_dataAry[$year][$sem][$month][$meritType] += $thisMeritVal;
  				        break;
              }
            }
          }
        }          
      }

      # Convert the numbers according to rules
      foreach($_dataAry as $year=> $semMeritArr){
        foreach($semMeritArr as $sem =>$monthMeritArr){
          foreach($monthMeritArr as $month => $meritDataArr){
            foreach($meritDataArr as $meritType => $thisMeritVal){
  
              $MeritPromotionSetting = (intval($meritType) > 0) ? $MeritPromotionSetting_award : $MeritPromotionSetting_punish;

              $thisFromMerit = $MeritPromotionSetting[$meritType]["UpgradeFromType"];
    					$thisToMerit = $MeritPromotionSetting[$meritType]["MeritType"];
    					$thisUpgradeNum = $MeritPromotionSetting[$meritType]["UpgradeNum"];
  
              if(empty($thisMeritVal)) continue;
  
              # if no conversion rule, use original value
              if(empty($thisUpgradeNum))
              {
                $returnAry[$year][$sem][$meritType] += $thisMeritVal;
              }
              # apply conversion rule if there is one
              else
              {
                $returnAry[$year][$sem][$thisToMerit] += floor($thisMeritVal/$thisUpgradeNum);
    		        $returnAry[$year][$sem][$thisFromMerit] += fmod($thisMeritVal,$thisUpgradeNum);
              }
            }
          }
        }          
      }
		}
		else
    {
      # 20091208, not support category conversion
      /*
      foreach($MeritPromotionSetting as $thisCatID =>$thisCatMeritPromotionSetting)
			{
				foreach($thisCatMeritPromotionSetting as $thisMeritPromotionSetting)
				{
					$thisFromMerit = $thisMeritPromotionSetting["UpgradeFromType"];
					$thisToMerit = $thisMeritPromotionSetting["MeritType"];
					$thisUpgradeNum = $thisMeritPromotionSetting["UpgradeNum"];
					
					if(empty($dataAry[$thisCatID][$thisFromMerit]) || empty($thisUpgradeNum)) continue;
					
					$dataAry[$thisCatID][$thisToMerit] += floor($RawDataAry[$thisCatID][$thisFromMerit]/$thisUpgradeNum);
					$dataAry[$thisCatID][$thisFromMerit] = fmod($RawDataAry[$thisCatID][$thisFromMerit],$thisUpgradeNum);
				}
			}
			$returnAry = Array();	
			foreach ($dataAry as $thisCatMeritCount)
			{
				foreach($thisCatMeritCount as $thisMeritType =>$thisMeritCount)
				{
					$returnAry[$thisMeritType] += $thisMeritCount;
				}
			}
			
			
      */        
      $returnAry = $dataAry;
    }
    
		return $returnAry;
    
}

function returnStudentMeritSummary($parStudentID, $parYear="", $parSemester="")
{
  global $li, $li_pf, $eclass_db;
  
  $conds = $li_pf->getReportYearCondition($parYear);
  if($parSemester!="")
  {
  	$conds .= "AND (Semester = '$parSemester')";
  }

  $sql =  "
            SELECT
              Year,
              Semester,
              Month(MeritDate) AS meritMonth,
              RecordType,
              SUM(NumberOfUnit) AS unitSum
            FROM
              {$eclass_db}.MERIT_STUDENT
            WHERE
              UserID = ".$parStudentID."
              $conds
            GROUP BY
              Year, Semester, meritMonth, RecordType
            ORDER BY
    					Year DESC, Semester DESC, RecordType DESC
          ";
  $meritArr = $li->returnArray($sql);
  
  $returnArr = array();
  for($i=0; $i<count($meritArr); $i++)
  {
    list($year, $semester, $month, $type, $count) = $meritArr[$i];
		$returnArr[$year][$semester][$month][$type] = $count;
  }

  return $returnArr;
}

# Retrieve Student Attendance Summary
function returnStudentAttendanceSummary($studentid, $AcademicYearID="", $YearTermID="")
{
	global $intranet_db, $eclass_db, $ec_iPortfolio, $li_pf, $intranet_root;
  $lstudentprofile = new libstudentprofile();
  
  $conds = ($AcademicYearID == "") ? "" : "AND ay.AcademicYearID = {$AcademicYearID} ";
  $conds .= ($YearTermID == "") ? "" : "AND ayt.YearTermID = {$YearTermID} ";

	// Prepare full year-term array holding attendance data
	// 2014-0409-0903-47184: change academic year ordering
	$sql = "SELECT
				ay.YearNameEN AS Year,
				ayt.YearTermNameEN AS Semester
			FROM
				{$intranet_db}.YEAR_CLASS_USER ycu
				INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID
				INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON yc.AcademicYearID = ay.AcademicYearID
				INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM ayt ON ay.AcademicYearID = ayt.AcademicYearID
			WHERE
				ycu.UserID = '$studentid'
				$conds
			ORDER BY
				/* ay.Sequence DESC, ayt.TermStart */
				YearNameEN DESC, ayt.TermStart
			";
	$row = $li_pf->returnArray($sql);

	for($i=0, $i_max=sizeof($row); $i<$i_max; $i++)
	{
		$_year = $row[$i]["Year"];
		$_sem = $row[$i]["Semester"];
		
		// Initialize number of records for each year and sem to 0
		$result[$_year][$_sem][1] = 0;
		$result[$_year][$_sem][2] = 0;
		$result[$_year][$_sem][3] = 0;
		
		$result[$_year][$ec_iPortfolio['whole_year']][1] = 0;
		$result[$_year][$ec_iPortfolio['whole_year']][2] = 0;
		$result[$_year][$ec_iPortfolio['whole_year']][3] = 0; 
	}

	$conds = ($AcademicYearID == "") ? "" : "AND AcademicYearID = {$AcademicYearID} ";
  $conds .= ($YearTermID == "") ? "" : "AND YearTermID = {$YearTermID} ";
	# records grouping by year and semester
	$sql = "SELECT
						Year,
						Semester,
						RecordType,
						DayType
					FROM
						{$eclass_db}.ATTENDANCE_STUDENT
					WHERE
						UserID = '$studentid'
						$conds
					ORDER BY
						Year DESC, Semester ASC, RecordType, DayType DESC
					";
	$row = $li_pf->returnArray($sql);

	for($i=0; $i<sizeof($row); $i++)
	{
		list($year, $semester, $type, $day_type) = $row[$i];
		$add_value = ($type==1 && $day_type>1) ? 0.5 : 1;
		$add_value = $lstudentprofile->attendance_count_method ? $add_value : 1;

		$result[$year][$semester][$type] = $result[$year][$semester][$type] + $add_value;

		# get semester total
		$result[$year][$ec_iPortfolio['whole_year']][$type] = $result[$year][$ec_iPortfolio['whole_year']][$type] + $add_value;
	}
	return $result;
}

if (trim($alumni_StudentID)!="")
{
	$alumni_sids = $alumni_StudentID;
	$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

# User Basic
$conds = '';
if ($ForAlumni)
{
	
	$conds .= " AND iu.UserID IN ($alumni_sids)";
} else
{
	$TargetClass = explode(',',$TargetClass);
	foreach($TargetClass as &$val){
		$val = "'".$val."'";	
	}
	$conds = " AND iu.ClassName in (".implode(',',$TargetClass).") AND iu.RecordType = 2 AND iu.RecordStatus = 1 ";
	$conds .= ($StudentID=="") ? "" : " AND iu.UserID = '".$StudentID."'";
}

//$sql = "SELECT UserID, CONCAT(EnglishName, ' (', ChineseName,')'), ClassName, ClassNumber FROM {$intranet_db}.INTRANET_USER";
$sql = "SELECT iu.UserID, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM {$intranet_db}.INTRANET_USER as iu Inner Join {$eclass_db}.PORTFOLIO_STUDENT as ps ON (iu.UserID = ps.UserID)";
$sql .= " WHERE ps.IsSuspend = 0 ".$conds;
$sql .= " ORDER BY iu.ClassName, iu.ClassNumber + 0";
$user_arr = $li->returnArray($sql);

# Year
$sql = "SELECT DISTINCT Year FROM {$eclass_db}.ACTIVITY_STUDENT UNION ";
$sql .= "SELECT DISTINCT Year FROM {$eclass_db}.ATTENDANCE_STUDENT UNION ";
$sql .= "SELECT DISTINCT Year FROM {$eclass_db}.AWARD_STUDENT UNION ";
$sql .= "SELECT DISTINCT Year FROM {$eclass_db}.CONDUCT_STUDENT UNION ";
$sql .= "SELECT DISTINCT Year FROM {$eclass_db}.MERIT_STUDENT ";
$sql .= "ORDER BY Year DESC";
$year_arr = $li->returnVector($sql);

# Semester
# Eric Yip (20091202): Check for dummy date
if($Year != "0000-0001")
{
  if($Semester=="")
  {
  	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
  	$libyear = new academic_year($AcademicYearID);
  	# retrieve school semesters
//   	list($SemesterArr, $ShortSemesterArr) = $li_pf->getSemesterNameArrFromIP();
  	list($SemesterArr, $ShortSemesterArr) = $li_pf->getSemesterNameArrFromIP2($libyear->Get_Academic_Year_Name(),'', $untilNow = true);
  	
//   	sort($SemesterArr);
  }
  else
//   	$SemesterArr[] = $Semester;
  	$SemesterArr[$Year][] = $Semester;
}

# Activity
$conds = ($Year=="") ? "" : " AND Year = '".$Year."'";
$conds .= ($Semester=="") ? "" : " AND Semester = '".$Semester."'";
if ($ForAlumni)
{
	
	$conds .= " AND UserID IN ($alumni_sids)";
} else
{
	$conds .= ($StudentID=="") ? "" : " AND UserID = '".$StudentID."'";
}

$sql = "SELECT UserID, Year, 
		if((Semester='' OR  Semester IS NULL), '".$Lang['General']['WholeYear']."', Semester) as `Semester`,
		ActivityName, Role, Performance FROM {$eclass_db}.ACTIVITY_STUDENT";
$sql .= " WHERE 1".$conds;
$sql .= " ORDER BY UserID, Year DESC, Semester";
$t_activity_arr = $li->returnArray($sql);

for($i=0; $i<count($t_activity_arr); $i++)
{
	$activity_uid = $t_activity_arr[$i]["UserID"];
	$activity_year = $t_activity_arr[$i]["Year"];
	$activity_semester = $t_activity_arr[$i]["Semester"];
	// 	debug_pr($SemesterArr[$activity_year][$activity_semester]);
	if( (!in_array($activity_semester, (array)$SemesterArr[$activity_year]) && $activity_semester!= $Lang['General']['WholeYear'] )
	){
		continue;
	}
	$activity_act_name = str_replace("'", "\\'", $t_activity_arr[$i]["ActivityName"]);
	$activity_role = str_replace("'", "\\'", $t_activity_arr[$i]["Role"]);
	$activity_performance = str_replace("'", "\\'", $t_activity_arr[$i]["Performance"]);

	$activity_arr[$activity_uid][] =	array (
		$activity_year,
		$activity_semester,
		$activity_act_name,
		$activity_role,
		$activity_performance
	);
}

# Service
$conds = ($Year=="") ? "" : " AND Year = '".$Year."'";
$conds .= ($Semester=="") ? "" : " AND Semester = '".$Semester."'";

if ($ForAlumni)
{	
	$conds .= " AND UserID IN ($alumni_sids)";
} else
{
	$conds .= ($StudentID=="") ? "" : " AND UserID = '".$StudentID."'";
}
$sql = "SELECT UserID, Year, 
		if((Semester='' OR  Semester IS NULL), '".$Lang['General']['WholeYear']."', Semester) as `Semester`,
		ServiceName, Role, Performance FROM {$eclass_db}.SERVICE_STUDENT";
$sql .= " WHERE 1".$conds;
$sql .= " ORDER BY UserID, Year DESC, Semester";
$t_service_arr = $li->returnArray($sql);

for($i=0; $i<count($t_service_arr); $i++)
{
	$service_uid = $t_service_arr[$i]["UserID"];
	$service_year = $t_service_arr[$i]["Year"];
	$service_semester = $t_service_arr[$i]["Semester"];
	if( (!in_array($service_semester, (array)$SemesterArr[$service_year]) && $service_semester!= $Lang['General']['WholeYear'] )
	){
				continue;
	}
	$service_act_name = str_replace("'", "\\'", $t_service_arr[$i]["ServiceName"]);
	$service_role = str_replace("'", "\\'", $t_service_arr[$i]["Role"]);
	$service_performance = str_replace("'", "\\'", $t_service_arr[$i]["Performance"]);

	$service_arr[$service_uid][] =	array (
		$service_year,
		$service_semester,
		$service_act_name,
		$service_role,
		$service_performance
	);
}

# Award
$conds = ($Year=="") ? "" : " AND Year = '".$Year."'";
$conds .= ($Semester=="") ? "" : " AND Semester = '".$Semester."'";

if ($ForAlumni)
{
	$conds .= " AND UserID IN ($alumni_sids)";
} else
{
	$conds .= ($StudentID=="") ? "" : " AND UserID = '".$StudentID."'";
}
$sql = "SELECT UserID, Year, 
		if((Semester='' OR  Semester IS NULL), '".$Lang['General']['WholeYear']."', Semester) as `Semester`,
		 AwardName, Remark FROM {$eclass_db}.AWARD_STUDENT";
$sql .= " WHERE 1".$conds;
$sql .= " ORDER BY UserID, Year DESC, Semester";
$t_award_arr = $li->returnArray($sql);

for($i=0; $i<count($t_award_arr); $i++)
{
	$award_uid = $t_award_arr[$i]["UserID"];
	$award_year = $t_award_arr[$i]["Year"];
	$award_semester = $t_award_arr[$i]["Semester"];
	if( (!in_array($award_semester, (array)$SemesterArr[$award_year]) && $award_semester!= $Lang['General']['WholeYear'] )
	){
				continue;
	}
	$award_act_name = str_replace("'", "\\'", $t_award_arr[$i]["AwardName"]);
	$award_remark = str_replace("'", "\\'", $t_award_arr[$i]["Remark"]);

	$award_arr[$award_uid][] =	array (
		$award_year,
		$award_semester,
		$award_act_name,
		$award_remark
	);
}

# OLE
$ELEArr = $li_pf->get_ELE();

$conds = " AND RecordStatus IN (2,4)";
if($Year!="")
{
	list($start_year, $end_year) = explode("-", $Year);
	$start_year = str_pad($start_year, 4, "20", STR_PAD_LEFT);
	$end_year = str_pad($end_year, 4, "20", STR_PAD_LEFT);
	
	# Eric Yip (20091202): Check for dummy date
	if($start_year == "0000")
	{
  	$start_time = 0;
  	$end_time = 0;
  }
  else
  {
  	$start_time = mktime(0, 0, 0, $year_start["month"], $year_start["day"], $start_year);
  	$end_time = mktime(23, 59, 59, $year_start["month"], $year_start["day"]-1, $end_year);
  }
	
	$conds .= " AND UNIX_TIMESTAMP(StartDate) BETWEEN ".$start_time." AND ".$end_time;
}
if ($ForAlumni)
{
	$conds .= " AND UserID IN ($alumni_sids)";
} else
{
	$conds .= ($StudentID=="") ? "" : " AND UserID = '".$StudentID."'";
}

$sql = "SELECT UserID, StartDate, EndDate, Title, Role, ELE, Hours FROM {$eclass_db}.OLE_STUDENT";
$sql .= " WHERE 1".$conds;
$sql .= " ORDER BY UserID, StartDate DESC";
$t_ole_arr = $li->returnArray($sql);

for($i=0; $i<count($t_ole_arr); $i++)
{
	$ole_uid = $t_ole_arr[$i]["UserID"];
	$ole_date = ($t_ole_arr[$i]["StartDate"] == "0000-00-00") ? "-" : $t_ole_arr[$i]["StartDate"];
	if($t_ole_arr[$i]["EndDate"] != "0000-00-00")
		$ole_date .= " to ".$t_ole_arr[$i]["EndDate"];
	$ole_title = str_replace("'", "\'", $t_ole_arr[$i]["Title"]);
	$ole_role = $t_ole_arr[$i]["Role"];
	$t_ole_ele = explode(",", $t_ole_arr[$i]["ELE"]);
	$ole_ele = "";
	foreach($t_ole_ele AS $ele_id)
	{
		if($ELEArr[$ele_id] != "")
			$ole_ele .= $ELEArr[$ele_id].", ";
	}
	$ole_ele = substr($ole_ele, 0, -2);
	$ole_hours = $t_ole_arr[$i]["Hours"];

	$ole_arr[$ole_uid][] =	array (
																	$ole_date,
																	$ole_title,
																	$ole_role,
																	$ole_ele,
																	$ole_hours
																);
}

$user_js_str = "var user_arr = new Array(".count($user_arr).");\n";
for($k=0; $k<count($user_arr); $k++)
{
	$t_user_id = $user_arr[$k][0];
	$t_user_name = $user_arr[$k][1];
	$t_user_classname = $user_arr[$k][2];
	$t_user_classnumber = $user_arr[$k][3];
	
	# Attendance
	$t_attendance_array = returnStudentAttendanceSummary($t_user_id, $AcademicYearID, $YearTermID);
	$attendance_detail = array();
	if(is_array($t_attendance_array))
	{
		foreach($SemesterArr AS $attend_year => $attend_year_detailArr)
		{
// 			ksort($attend_year_detail, SORT_STRING);
			# Semester Record
			for($i=0; $i<count($attend_year_detailArr); $i++)
			{
					$attend_sem = $attend_year_detailArr[$i];
                    // $attend_sem_detail = $attend_year_detail[$attend_sem];

					// [2019-1206-1651-09206] Get attendance data
                    $attend_sem_detail = array();
                    if(isset($t_attendance_array[$attend_year][$attend_sem])) {
                        $attend_sem_detail = $t_attendance_array[$attend_year][$attend_sem];
                    }
				
					$attendance_detail[] = array(
						$attend_year,
						$attend_sem,
						(int) $attend_sem_detail[1] ? (int) $attend_sem_detail[1] : 0,
						(int) $attend_sem_detail[2] ? (int) $attend_sem_detail[2] : 0,
						(int) $attend_sem_detail[3] ? (int) $attend_sem_detail[3] : 0
					);
			}
/*			
      Eric Yip (20100524): CRM Ref No.: 2010-0524-1001
			# Whole Year Summary
			$attend_sem = $ec_iPortfolio['whole_year'];
			if($Semester == "")
				$attendance_detail[] = array	(
																				$attend_year,
																				$attend_sem,
																				(int) $attend_year_detail[$attend_sem][1],
																				(int) $attend_year_detail[$attend_sem][2],
																				(int) $attend_year_detail[$attend_sem][3]
																			);
*/
		}
	}

	# Merit
	//$t_merit_array = $li_pf->returnStudentMeritSummary($t_user_id, $Year, $Semester);
	// support merit conversion, 20091208
	$lpf_acc->SET_CLASS_VARIABLE("user_id", $t_user_id);
	$t_merit_array = $lpf_acc->GET_CONVERT_MERIT_SUMMARY($Year, $Semester);

	$t_merit_title_array = $i_Merit_TypeArray;
	$field_disabled_array = array(-2, -1, 3, 4, 5);
	$mtypeArr = $li_pf->returnMeritTypeAbility();

	$merit_detail = array();
	foreach($SemesterArr AS $merit_year => $merit_semArr)
	{
		for($i=0; $i<count($merit_semArr); $i++)
		{
			$merit_sem = $SemesterArr[$merit_year][$i];
			$merit_sem_detail = $t_merit_array[$merit_year][$merit_sem];
		
			$t_merit_row = array	(
				$merit_year,
				$merit_sem
			);

			foreach($mtypeArr AS $merit_code => $merit_num)
			{
				if(in_array($merit_code, $field_disabled_array)) continue;
			
				$t_merit_row[] = (int) $merit_sem_detail[$merit_code] ? (int) $merit_sem_detail[$merit_code] : 0;
			}
		
			$merit_detail[] = $t_merit_row;
		}

/*			
      Eric Yip (20100524): CRM Ref No.: 2010-0524-1001
			$merit_sem = $ec_iPortfolio['whole_year'];
			if($Semester == "")
			{
				$whole_year_merit = array	(
																		$merit_year,
																		$merit_sem
																	);
				
				foreach($mtypeArr AS $merit_code => $merit_num)
				{
					if(in_array($merit_code, $field_disabled_array)) continue;
				
					$whole_year_merit[] = (int) $merit_sem_detail[$merit_code];
				}
				
				$merit_detail[] = $whole_year_merit;
			}
*/
	}
// 	}
// 	else
// 	{
// 		if($Year == "")
// 		{
// 			for($i=0; $i<count($year_arr); $i++)
// 			{
// 				for($j=0; $j<count($SemesterArr); $j++)
// 				{
// 					$t_merit_row = array($year_arr[$i], $SemesterArr[$j]);
				
// 					foreach($mtypeArr AS $merit_code => $merit_num)
// 					{
// 						if(in_array($merit_code, $field_disabled_array)) continue;
					
// 						$t_merit_row[] = 0;
// 					}
				
// 					$merit_detail[] = $t_merit_row;
// 				}
// 			}
// 		}
// 		else
// 		{
// 			for($i=0; $i<count($SemesterArr); $i++)
// 			{
// 				$t_merit_row = array($Year, $SemesterArr[$i]);
			
// 				foreach($mtypeArr AS $merit_code => $merit_num)
// 				{
// 					if(in_array($merit_code, $field_disabled_array)) continue;
				
// 					$t_merit_row[] = 0;
// 				}
			
// 				$merit_detail[] = $t_merit_row;
// 			}
// 		}
// 	}

	$act_detail = $activity_arr[$t_user_id];
	$service_detail = $service_arr[$t_user_id];
	$award_detail = $award_arr[$t_user_id];
	$ole_detail = $ole_arr[$t_user_id];

	$user_js_str .= "user_arr['u".$t_user_id."'] = new Array();\n";
	
	# Basic Info
	$user_js_str .= "user_arr['u".$t_user_id."']['basic'] = new Array();\n";
	$user_js_str .= "user_arr['u".$t_user_id."']['basic'][0] = '".$t_user_name."'\n";
	$user_js_str .= "user_arr['u".$t_user_id."']['basic'][1] = '".$t_user_classname."'\n";
	$user_js_str .= "user_arr['u".$t_user_id."']['basic'][2] = '".$t_user_classnumber."'\n";
	
	# Attendance
	$user_js_str .= "user_arr['u".$t_user_id."']['attendance'] = new Array(".count($attendance_detail).");\n";
	for($i=0; $i<count($attendance_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['attendance'][".$i."] = new Array(".count($attendance_detail[$i]).");\n";
		for($j=0; $j<count($attendance_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['attendance'][".$i."][".$j."] = '".$attendance_detail[$i][$j]."';\n";
		}
	}
	
	# Merit
	$user_js_str .= "user_arr['u".$t_user_id."']['merit'] = new Array(".count($merit_detail).");\n";
	for($i=0; $i<count($merit_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['merit'][".$i."] = new Array(".count($merit_detail[$i]).");\n";
		for($j=0; $j<count($merit_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['merit'][".$i."][".$j."] = '".$merit_detail[$i][$j]."';\n";
		}
	}
	
	# Activity
	$user_js_str .= "user_arr['u".$t_user_id."']['activity'] = new Array(".count($act_detail).");\n";
	for($i=0; $i<count($act_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['activity'][".$i."] = new Array(".count($act_detail[$i]).");\n";
		for($j=0; $j<count($act_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['activity'][".$i."][".$j."] = '".$act_detail[$i][$j]."';\n";
		}
	}
	
	# Service
	$user_js_str .= "user_arr['u".$t_user_id."']['service'] = new Array(".count($service_detail).");\n";
	for($i=0; $i<count($service_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['service'][".$i."] = new Array(".count($service_detail[$i]).");\n";
		for($j=0; $j<count($service_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['service'][".$i."][".$j."] = '".$service_detail[$i][$j]."';\n";
		}
	}
	
	# Service
	$user_js_str .= "user_arr['u".$t_user_id."']['award'] = new Array(".count($award_detail).");\n";
	for($i=0; $i<count($award_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['award'][".$i."] = new Array(".count($award_detail[$i]).");\n";
		for($j=0; $j<count($award_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['award'][".$i."][".$j."] = '".$award_detail[$i][$j]."';\n";
		}
	}
	
	# OLE
	$user_js_str .= "user_arr['u".$t_user_id."']['ole'] = new Array(".count($ole_detail).");\n";
	for($i=0; $i<count($ole_detail); $i++)
	{
		$user_js_str .= "user_arr['u".$t_user_id."']['ole'][".$i."] = new Array(".count($ole_detail[$i]).");\n";
		for($j=0; $j<count($ole_detail[$i]); $j++)
		{
			$user_js_str .= "user_arr['u".$t_user_id."']['ole'][".$i."][".$j."] = '".$ole_detail[$i][$j]."';\n";
		}
	}
}

# Report Title
$report_title = "Student Records";
if($Year != "" && $Year != "0000-0001")
{
	$year_sem = $Year;
	$year_sem .= ($Semester != "") ? " ".$Semester : "";
	$report_title .= " (".$year_sem.")";
}

# Merit Title
$i = 0;
if(is_array($mtypeArr))
{
  foreach($mtypeArr AS $merit_code => $merit_num)
  {
  	if(!in_array($merit_code, $field_disabled_array))
  		$merit_title_array[] = $t_merit_title_array[$i];
  	
  	$i++;
  }
}

_handleCurrentAlumniStudentAfterPrinting("alumni", $AlumniStudentArr);

/*
if ($PrintType=="word")
{
	$ContentType = "application/x-msword";
  	$filename = "export_word_file".date("ymd").".doc";
	header('Pragma: public');
  	header('Expires: 0');
  	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  
  	header('Content-type: '.$ContentType);
  	header('Content-Length: '.strlen($output));
  
  	header('Content-Disposition: attachment; filename="'.$filename.'";');
  	
  	$output_html = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
} else
{*/
	$output_html = "<html>";
//}
?>

<?=$output_html?>
<head>

<style type="text/css">
/* <![CDATA[ */

.moduleTable, .moduleHead, .moduleCell
{
	border-color: #000;
	border-style: solid;
}

.moduleHead, .moduleCell
{
	font-family: "Times New Roman";
	font-size: 16px;
	margin: 0;
	padding: 2px;
	border-width: 1px 1px 1px 1px;
}

.moduleTable
{
	border-width: 0 0 0px 0px;
	border-spacing: 0;
	border-collapse: collapse;
}

.moduleCaption
{
	font-family: "Times New Roman";
	font-size: 18px;
	font-weight: "bold";
	text-align: "left";
	padding-top: 16px;
	white-space: nowrap;
}

.moduleHead
{
	text-align: "center";
}

.basicInfoCell
{
	font-family: "Times New Roman";
	font-size: 16px;
}

.reportTitleCell
{
	font-family: "Times New Roman";
	font-size: 16px;
	font-weight: "bold";
	text-align: "center";
	text-decoration: underline;
}

/* ]]> */
</style>


<script language="JavaScript">

var curPageNo = 1;
var curModuleIndex = 0;
var curUserID;
var pageWidthMax = "17cm";
var pageHeightMax = 950;
var pageBannerHeight = "3.5cm";
var reportTitle = '<?=$report_title?>';

var moduleArray = new Array();
moduleArray[0] = "attendance";
moduleArray[1] = "merit";
moduleArray[2] = "service";
moduleArray[3] = "activity";
moduleArray[4] = "award";
//moduleArray[5] = "ole";

var moduleCaptionArray = new Array();
moduleCaptionArray[0] = "Attendance Record";
moduleCaptionArray[1] = "Merit/Demerit Record";
moduleCaptionArray[2] = "Service Record";
moduleCaptionArray[3] = "Activity Record";
moduleCaptionArray[4] = "Award Record";
moduleCaptionArray[5] = "Other Learning Experiences";

var moduleHeaderArray = new Array();
moduleHeaderArray[0] = new Array("School Year", "Period", "Absent", "Late", "Early Leave");
moduleHeaderArray[1] = new Array("School Year", "Period", "<?=implode('", "', $merit_title_array)?>");
moduleHeaderArray[2] = new Array("School Year", "Period", "Service Name", "Role", "Performance");
moduleHeaderArray[3] = new Array("School Year", "Period", "Activity Name", "Role", "Performance");
moduleHeaderArray[4] = new Array("School Year", "Period", "Award", "Remarks");
moduleHeaderArray[5] = new Array("Date", "Title", "Role", "Components", "Hours");

var moduleWidthArray = new Array();
moduleWidthArray[0] = new Array("20%", "20%", "20%", "20%", "20%");
moduleWidthArray[1] = new Array("15%", "15%", "<?=implode('", "', array_fill(0, count($merit_title_array), round(70/count($merit_title_array))."%"))?>");
moduleWidthArray[2] = new Array("15%", "15%", "40%", "15%", "15%");
moduleWidthArray[3] = new Array("15%", "15%", "40%", "15%", "15%");
moduleWidthArray[4] = new Array("15%", "15%", "40%", "30%");
moduleWidthArray[5] = new Array("20%", "40%", "15%", "15%", "10%");

var moduleAlignArray = new Array();
moduleAlignArray[0] = new Array("", "", "center", "center", "center");
moduleAlignArray[1] = new Array("", "", "<?=implode('", "', array_fill(0, count($merit_title_array), "center"))?>");
moduleAlignArray[2] = new Array("", "", "", "", "");
moduleAlignArray[3] = new Array("", "", "", "", "");
moduleAlignArray[4] = new Array("", "", "", "");
moduleAlignArray[5] = new Array("", "", "", "", "center");

//////////////////// Data Source ////////////////////
<?=$user_js_str?>

function createRow(){
	// Always use first entry
	eval("var curEntry = user_arr['"+curUserID+"']['"+moduleArray[curModuleIndex]+"'][0];");

	var moduleRow = document.createElement("TR");
	for(i=0; i<curEntry.length; i++)
	{
		var rowCell = document.createElement("TD");
		rowCell.setAttribute("class","moduleCell");
		rowCell.setAttribute("align", moduleAlignArray[curModuleIndex][i]);
		var textData = (curEntry[i] == "") ? " " : curEntry[i];
		//var textnode = document.createTextNode(textData);
		//rowCell.appendChild(textnode);
		rowCell.innerHTML = textData;
		moduleRow.appendChild(rowCell);
	}
	return moduleRow;
}

function createNullRow(){
	var moduleRow = document.createElement("TR");
	var rowCell = document.createElement("TD");
	rowCell.setAttribute("class","moduleCell");
	rowCell.setAttribute("align","center");
	rowCell.colSpan = moduleHeaderArray[curModuleIndex].length;
	var textData = "<?=$i_no_record_exists_msg2?>";
	var textnode = document.createTextNode(textData);
	rowCell.appendChild(textnode);
	moduleRow.appendChild(rowCell);

	return moduleRow;
}

function isRowHeightExceed(){
	var moduleTable = document.getElementById(moduleArray[curModuleIndex]+"_table");
	var moduleTableHeight = moduleTable.clientHeight;
	
	var pageTable = document.getElementById("page"+curPageNo);
	var pageTableHeight = pageTable.clientHeight;

	return moduleTableHeight+pageTableHeight > pageHeightMax;
}

function isLastModule(){
	return moduleArray.length == curModuleIndex;
}

function isEmptyModule(){
	return moduleArray[curModuleIndex].length > 0;
}

function appendModuleRow(jParRowObj){
	var moduleTableBody = document.getElementById(moduleArray[curModuleIndex]+"_table_body");
	moduleTableBody.appendChild(jParRowObj);
}

function appendPageModule(){
	var moduleTable = document.getElementById(moduleArray[curModuleIndex]+"_table");
	var moduleTableBody = document.getElementById(moduleArray[curModuleIndex]+"_table_body");
	var pageRow = document.createElement("TR");
	var rowCell = document.createElement("TD");

	// Avoid ID duplicate in preparation area
	moduleTable.removeAttribute("id");
	moduleTableBody.removeAttribute("id");
	
	// Copy a new module table for appending
	var copy_moduleTable = moduleTable.cloneNode(true);
	rowCell.appendChild(copy_moduleTable);
	pageRow.appendChild(rowCell);
	
	var pageTableBody = document.getElementById("page"+curPageNo+"_body");
	pageTableBody.appendChild(pageRow);
	
	moduleTable.setAttribute("id",moduleArray[curModuleIndex]+"_table");
	moduleTableBody.setAttribute("id",moduleArray[curModuleIndex]+"_table_body");
	clearModuleTable();
}

// Create page table
function createPage(){
	var pageTable = document.createElement("table");
	pageTable.setAttribute("id","page"+curPageNo);
	pageTable.setAttribute("name","page"+curPageNo);
	pageTable.setAttribute("align","center");
	pageTable.style.pageBreakAfter = "always";
	
	var pageTableBody = document.createElement("tbody");
	pageTableBody.setAttribute("id","page"+curPageNo+"_body");
	pageTable.appendChild(pageTableBody);
	
	var printArea = document.getElementById("PrintableContent");
	printArea.appendChild(pageTable);
	
	createPageTopPadding();
}

function createPageTopPadding(){
	var pageTableBody = document.getElementById("page"+curPageNo+"_body");
	
	var reportHeadCell = document.createElement("TD");
	reportHeadCell.style.paddingTop = pageBannerHeight;
	reportHeadCell.innerHTML = "&nbsp;";

	var reportHeadRow = document.createElement("TR");
	reportHeadRow.appendChild(reportHeadCell);

	pageTableBody.appendChild(reportHeadRow);
}

function createReportTitleTable(){

	var textnode = document.createTextNode(reportTitle);
	
	var rowCell = document.createElement("TD");
	rowCell.setAttribute("class","reportTitleCell");
	rowCell.appendChild(textnode);
	
	var reportTitleRow = document.createElement("TR");
	reportTitleRow.appendChild(rowCell);
	
	var reportTitleTableBody = document.createElement("tbody");
	reportTitleTableBody.appendChild(reportTitleRow);

	var reportTitleTable = document.createElement("table");
	reportTitleTable.style.width = pageWidthMax;
	reportTitleTable.appendChild(reportTitleTableBody);

	var reportTitleTableCell = document.createElement("TD");
	reportTitleTableCell.appendChild(reportTitleTable);

	var reportTitleTableRow = document.createElement("TR");
	reportTitleTableRow.appendChild(reportTitleTableCell);
	
	var pageTableBody = document.getElementById("page"+curPageNo+"_body");
	pageTableBody.appendChild(reportTitleTableRow);
}

function createBasicInfoTable(){
	eval("var curUser = user_arr['"+curUserID+"']['basic'];");
	var basicInfoDesc = new Array("Student Name", "Class Name", "Class Number");
	var basicInfoWidth = new Array("60%", "20%", "20%");

	var basicInfoTable = document.createElement("table");
	var basicInfoTableBody = document.createElement("tbody");
	basicInfoTable.style.width = pageWidthMax;
	var basicInfoRow = document.createElement("TR");
	for(i=0; i<curUser.length; i++)
	{
		var rowCell = document.createElement("TD");
		rowCell.setAttribute("width",basicInfoWidth[i]);
		rowCell.setAttribute("class","basicInfoCell");
		//var textnode = document.createTextNode(basicInfoDesc[i]+": "+curUser[i]);
		//rowCell.appendChild(textnode);
		rowCell.innerHTML = basicInfoDesc[i]+": "+curUser[i];
		basicInfoRow.appendChild(rowCell);
	}
	basicInfoTableBody.appendChild(basicInfoRow);
	basicInfoTable.appendChild(basicInfoTableBody);

	var basicInfoTableRow = document.createElement("TR");
	var basicInfoTableCell = document.createElement("TD");
	basicInfoTableCell.appendChild(basicInfoTable);
	basicInfoTableRow.appendChild(basicInfoTableCell);
	
	var pageTableBody = document.getElementById("page"+curPageNo+"_body");
	pageTableBody.appendChild(basicInfoTableRow);
}

function createModuleTableHeader(){

	var headerEntry = moduleHeaderArray[curModuleIndex];
	var headerWidth = moduleWidthArray[curModuleIndex];

	var headerRow = document.createElement("TR");
	for(i=0; i<headerEntry.length; i++)
	{
		var rowCell = document.createElement("TD");
		rowCell.setAttribute("width",headerWidth[i]);
		rowCell.setAttribute("class","moduleHead");
		var textnode = document.createTextNode(headerEntry[i]);
		rowCell.appendChild(textnode);
		headerRow.appendChild(rowCell);
	}

	var moduleTableBody = document.getElementById(moduleArray[curModuleIndex]+"_table_body");
	moduleTableBody.appendChild(headerRow);
}

// Create module table
function createModuleTable(){
	var moduleTable = document.createElement("table");
	moduleTable.setAttribute("id",moduleArray[curModuleIndex]+"_table");
	moduleTable.setAttribute("name",moduleArray[curModuleIndex]+"_table");
	moduleTable.setAttribute("class","moduleTable");
	moduleTable.style.width = pageWidthMax;
	
	// Caption in thead
	var moduleTableHead = document.createElement("thead");
	moduleTableHead.setAttribute("id",moduleArray[curModuleIndex]+"_table_head");
	var moduleTableCaption = document.createElement("caption");
	moduleTableCaption.setAttribute("class", "moduleCaption");
	var captiontextnode = document.createTextNode(moduleCaptionArray[curModuleIndex]);
	moduleTableCaption.appendChild(captiontextnode);
	moduleTableHead.appendChild(moduleTableCaption);
	moduleTable.appendChild(moduleTableHead);
	
	// Data rows in tbody
	var moduleTableBody = document.createElement("tbody");
	moduleTableBody.setAttribute("id",moduleArray[curModuleIndex]+"_table_body");
	moduleTable.appendChild(moduleTableBody);
	
	var preparationArea = document.getElementById("PreparationArea");
	preparationArea.appendChild(moduleTable);
	
	createModuleTableHeader();
}

function destroyModuleTable(){
	var preparationArea = document.getElementById("PreparationArea");
	preparationArea.removeChild(preparationArea.childNodes[0]);
}

function clearModuleTable(){
	var moduleTable = document.getElementById(moduleArray[curModuleIndex]+"_table");

	while(moduleTable.rows.length > 0)
	{
		moduleTable.deleteRow(0);
	}
	
	createModuleTableHeader();
}

function removeLastPageBreak(){
	var lastPage = curPageNo - 1;
	var lastPageTable = document.getElementById("page"+lastPage);
	lastPageTable.style.pageBreakAfter = "";
}

// Remove used entry (first entry in array)
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};

</script>

</head>

<body>

<div id="PrintableContent"></div>

<div id="PreparationArea"></div>


<script language="JavaScript">
	if(pageHeightMax > 100)
	{
		for (uid in user_arr)
		{
			if(uid == "remove") continue;
			//if(uid != "u3476") continue;

			curUserID = uid;
			curModuleIndex = 0;

			createPage();
			createReportTitleTable();
			createBasicInfoTable();
		
			// Check if all modules are walked
			while(!isLastModule())
			{
				// Create table to hold data
				createModuleTable();
		
				// Check if module has records
				if(eval("user_arr['"+curUserID+"']['"+moduleArray[curModuleIndex]+"'].length > 0"))
				{
					while(eval("user_arr['"+curUserID+"']['"+moduleArray[curModuleIndex]+"'].length > 0")){
						var dataRow = createRow();
						appendModuleRow(dataRow);
	
						// Check if module table exceed page limit
						if(isRowHeightExceed())
						{
							// Remove last row inserted
							var moduleTable = document.getElementById(moduleArray[curModuleIndex]+"_table");
							var lastRowIndex = moduleTable.rows.length - 1;
							moduleTable.deleteRow(lastRowIndex);
		
							// Append module table to page table
							if(moduleTable.rows.length > 1)
								appendPageModule();
		
							// Create table to hold data
							curPageNo++;
							createPage();
						}
						else
						{
							eval("user_arr['"+curUserID+"']['"+moduleArray[curModuleIndex]+"'].remove(0);");
						}
					}
					
					var moduleTable = document.getElementById(moduleArray[curModuleIndex]+"_table");
					if(moduleTable.rows.length > 0)
					{
						appendPageModule();
					}
				}
				// Handle empty module
				else
				{
					var dataRow = createNullRow();
					appendModuleRow(dataRow);

					// Check if module table exceed page limit
					if(isRowHeightExceed())
					{
						curPageNo++;
						createPage();
					}
					
					appendPageModule();
				}
				
				// Destory temporary module table
				destroyModuleTable();
				
				// Advance to next module
				curModuleIndex++;
			}
			
			curPageNo++;
		}
		
		removeLastPageBreak();
	}
	else
	{
		alert("Please set height limit to at least 100!!");
	}
</script>

</body>
</html>
