<?php
################# Change Log [Start] #####
#
#	Date	:	2016-03-22	Omas
#				add support to output pdf format
#
################## Change Log [End] ######

ini_set('memory_limit','192M');
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cchpw/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");
include_once($PATH_WRT_ROOT."home/portfolio/profile/report/report_lib/slp_report.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

if($_POST['PrintType'] == 'pdf'){
	include_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cchpw/libpf-slp-pdf.php");
}
else{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cchpw/libpf-slp.php");
}

$lpf_slp = new libpf_slp();

$slp_report = new slp_report();

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);
$NoOfFirstPage = trim($NoOfFirstPage);
$NoOfOtherPage = trim($NoOfOtherPage);

$objReportMgr = new RptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

if (!$printIssueDate)
{
	$issuedate = "&nbsp;";
} else
{
	$issuedate = date("d/m/Y", strtotime($issuedate));
}

if($_POST['PrintType'] == 'pdf'){
	//start creating PDF
	$margin_left = 5; //mm
	$margin_right = 5;
	$margin_top = 42;
	$margin_bottom = 20;
	$margin_header = 0;
	$margin_footer = 5;
	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	// Chinese use mingliu
	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;
	$pdf->defaultheaderline = 0;
	$pdf->defaultfooterline = 0;
	
	$style = '<style>
				/*@media all {
					.page-break {display: none;}
				}
				@media print {
					.page-break {display: block; page-break-before: always;}
					html, body {width: 21cm;}
				}
				@page {size: A4 portrait; margin: 1.2cm 0 0 0;}*/
				body {font-family: "aa";color: #000000; font-size:10px; }
			
			.report_header {
	padding:3px;
	font-size:13pt;
	font-weight:bold;
	}
	
	.module_title {
	color:#FFFFFF;
	padding:3px;
	font-size:12pt;
	font-weight:bold;
	}
	
	.padding_all {
	padding:3px
	}
			table {
	    width: 100%;
	    border-top-width: 2px;
	    border-right-width: 2px;
	    border-bottom-width: 2px;
	    border-left-width: 2px;
	    border-top-color: rgb(0, 0, 0);
	    border-right-color: rgb(0, 0, 0);
	    border-bottom-color: rgb(0, 0, 0);
	    border-left-color: rgb(0, 0, 0);
	    border-spacing: 0px;
	}
			
	.table_no_page_break {
		border: 2px solid #000;
	}
			
	.module_title {
		border-bottom: 2px solid #000;
	}
			
	.table_print{
		font-size:14px;
	}
			</style>';
	
	// $style .= '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_report.css"></style>';
	// $style .= '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_table.css"></style>';
	$pdf->WriteHTML($style);
}
else{
	$html = '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_report.css"></style>';
	$html .= '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_table.css"></style>';
	$html .= '<STYLE TYPE="text/css">
				.report_header {
				padding:3px;
				font-size:13pt;
				font-weight:bold;
				font-family:Arial
				}
					
				.module_title {
				color:#FFFFFF;
				padding:3px;
				font-size:12pt;
				font-weight:bold;
				font-family:Arial
				}
					
				.padding_all {
				padding:3px
				}
				
				</STYLE>';

}
for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	if($NoOfFirstPage!=''){
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!=''){
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}
	
	$html .= $objStudentInfo->generateReport($_studentId,$academicYearID,$IntExt='INT',$Selected=false,$isMember=false, $lastStudent = (($i+1)==$i_max));

// 	$html .= $objStudentInfo->generateReport($_studentId,$academicYearID,$IntExt='INT',$Selected=false,$isMember=false, $lastStudent = (($i+1)==$i_max));
	/*
	
	$html .= $objStudentInfo->getStudentInfo_HTML();
	$html .= $objStudentInfo->getOLE_HTML($_studentId,$academicYearID,$IntExt='INT',$Selected=false);
	$html .= $objStudentInfo->getSchoolAward_HTML();
	$html .= $objStudentInfo->getSelfAccount_HTML();
	$html .= $objStudentInfo->getSignature_HTML();
	*/
	/*
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	
	if($NoOfFirstPage!='')
	{
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!='')
	{
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}

	$OLEInfoArr = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);

	### Page Content
//	$html .=$objStudentInfo->getEmptyTable('3%');
//	$html .= $objStudentInfo->getPart1_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('1%');
//	$html .= $objStudentInfo->getPart2_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
//	$html .= $objStudentInfo->getPart3_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr);	
	
//	$html .=$objStudentInfo->getPagebreakTable();
	$html .=$objStudentInfo->getEmptyTable('3%');
	
	$html .='<table width=100% height="925px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart5_HTML().'</td></tr>';
	$html .='</table>';

	$html .= $objStudentInfo->getSignature_HTML();
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getEmptyTable($Height='',$Content='Page '.$objStudentInfo->getPageNumberOfThisPage().' of <!--totalPage-->');	
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
	$thisTotalPage = $objStudentInfo->getPageNumberOfThisPage();
	$html = str_replace('<!--totalPage-->',$thisTotalPage,$html);
	*/
// 	$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
}

if($_POST['PrintType'] == 'pdf'){
	$pdf->Output('SLP.pdf', 'I');
}
else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}
// $pdf->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');

exit();
?>