<?php
################# Change Log [Start] #####
#	Date	:	2017-05-04	Omas
#				- change principal name #F116469 
#
#	Date	:	2016-11-30	Omas
#				-Hide award remarks if no awards - #F96819
#				-add using displayReportFooter to determine whether display signature & date
#
#	Date	:	2016-02-17	Omas
# 				 change issue date format - F92732 
#
#	Date	:	2015-11-09	Omas
# 				 Principal ( Mrs. Florence Yung ) change tp Acting Principal ( Mr. T. P. Ching ) - F88705 
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/stpaul/libpf-slp-pas.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$PageRowMax = 48;
$PageOneHeadOccupied = 23;
$PageTwoHeadOccupied = 9;

$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$alumni_lidb = new libdb();
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
//$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);

$objReportMgr = new ipfReportManager();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$academicYearID = Get_Current_Academic_Year_ID();
$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();


// 2016-02-17 Omas #F92732
// $issuedateStr = date("j F Y", strtotime($issuedate));
$issuedateStr = date("j M Y", strtotime($issuedate));

# Yuen - may need to filter students with license only
$studentList = $objReportMgr->getStudentIDList();

$academicYearIDOne = $academicYearID[0];

$libimport = new libimporttext();
# Personal Attributes 
$FileCSV = $PATH_WRT_ROOT."file/portfolio/fywss_pa_".$academicYearIDOne.".csv";
if (file_exists($FileCSV))
{
	$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
	$csv_header = $csv_data[0];
	for ($i=1; $i<sizeof($csv_data); $i++)
	{
		$rowObj = $csv_data[$i];
		$PersonalAttributes[trim($rowObj[0], "#")] = $rowObj;
	}
}

for ($i = 0; $i < sizeof($studentList); $i++)
{
	$PageNow = 1;
	
	$RowLeft = $PageRowMax;
	
	$_studentId = $studentList[$i];
	$alumni_Info = null;
	
	# Get Alumni Info
	if($ForAlumni){
		$sql = "Select 
					yc.AcademicYearID, yc.YearClassID, yc.ClassTitleEN, ycu.ClassNumber
				From YEAR_CLASS_USER ycu 
				Inner Join YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID 
				Inner Join ACADEMIC_YEAR ay on ay.AcademicYearID = yc.AcademicYearID  
				Where ycu.UserID = ".$_studentId." 
				Order By ay.Sequence desc";
		$alumni_Info = $alumni_lidb->returnArray($sql);
		$alumni_Info = $alumni_Info[0];
		
		# Update alumni settings
		if($alumni_Info != null){
			$objReportMgr->setYearClassID($alumni_Info['YearClassID']);
			$objReportMgr->setAcademicYearID($alumni_Info['AcademicYearID']);
			$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();
		}
	}

	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setPrintIssueDate($issuedateStr);
	
	$StudentInfoArr = $objStudentInfo->getStudentInfo();
	$StudentInfoArr = $StudentInfoArr[0];
	
	# Update alumni info
	if($ForAlumni && $alumni_Info != null){
		$StudentInfoArr["ClassName"] = $alumni_Info['ClassTitleEN'];
		$StudentInfoArr["ClassNumber"] = $alumni_Info['ClassNumber'];
	}
	
	$tmpVar = explode("-", $academicYearStartEndDisplay);
	$current_form_value = (int) $StudentInfoArr["ClassName"];
	$objStudentInfo->GetClassHistory($current_form_value);
	if (trim($tmpVar[1])!="")
	{
		$YearEnd = trim($tmpVar[1]);
		/*
		if ($current_form_value==1 || $current_form_value==4)
		{
			$YearStart = $YearEnd - 1;
		} elseif ($current_form_value==2 || $current_form_value==5)
		{
			$YearStart = $YearEnd - 2;
		} else
		{
			$YearStart = $YearEnd - 3;
		}
		*/
		$YearStart = $YearEnd - $objStudentInfo->GetNumberOfYears();
		$academicYearStartEndDisplay = $YearStart . "-" . $YearEnd;
	}

/*

	$ClassHistory = $objIPF->GET_STUDENT_CLASS_HISTORY($_studentId);


	$FormJunior = array();
	$FormSenior = array();

	# determine form and corresponding academic years
	for ($i=0; $i<sizeof($ClassHistory); $i++)
	{
		$classhistory_row = $ClassHistory[$i];
		$form_integer = (int) $classhistory_row["ClassName"];
		if ($form_integer<=3)
		{
			$FormJunior[] = trim($classhistory_row["Year"]);
		} else
		{
			$FormSenior[] = trim($classhistory_row["Year"]);
		}
	}
*/


	# get current class teacher
	
	$ClassTeacher = $objStudentInfo->GetClassTeachers($academicYearID);
	
	# get OLE
	$objStudentInfo->GetRDandAA();
	$objStudentInfo->GetOLE();
	
	$objStudentInfo->getAcademicPerformancePoint();
	$Senior_Subject_html = $objStudentInfo->displayAcademicPerformancePoint("SENIOR");
	
	$Senior_AD_html = $objStudentInfo->displayOLE("SENIOR", "[AD]");
	$Senior_PD_html = $objStudentInfo->displayOLE("SENIOR", "[PD]");
	$Senior_MCE_html = $objStudentInfo->displayOLE("SENIOR", "[MCE]");
	$Senior_CS_html = $objStudentInfo->displayOLE("SENIOR", "[CS]");
	$Senior_CE_html = $objStudentInfo->displayOLE("SENIOR", "[CE]");
	
	
	$Senior_RandD_html = $objStudentInfo->getAndDisplayRDorAA("SENIOR", "[".$objStudentInfo->ELEID["R&D"]."]");
	$Senior_AandA_html = $objStudentInfo->getAndDisplayRDorAA("SENIOR", "[".$objStudentInfo->ELEID["A&A"]."]");
	
	$JuniorRowTotal = $objStudentInfo->GetOLESubtotal("JUNIOR", "[AD]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[PD]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[MCE]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[CS]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[CE]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["R&D"]."]")
						+ $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["A&A"]."]")
						+ $objStudentInfo->getAcademicPerformancePointSubtotal("JUNIOR");
	
	if ($current_form_value>3)
	{
		# for form 4, 5, 6 students only
		$SeniorRowTotal = $objStudentInfo->GetOLESubtotal("SENIOR", "[AD]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[PD]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[MCE]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[CS]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[CE]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["R&D"]."]")
							+ $objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["A&A"]."]")
							+ $objStudentInfo->getAcademicPerformancePointSubtotal("SENIOR");
		
		$ADColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[AD]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[AD]");
		$PDColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[PD]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[PD]");
		$MCEColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[MCE]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[MCE]");
		$CSColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[CS]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[CS]");
		$CEColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[CE]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[CE]");
		$RDColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["R&D"]."]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["R&D"]."]");
		$AAColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["A&A"]."]") + $objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["A&A"]."]");
		
		$SubjectsColumn =  $objStudentInfo->getAcademicPerformancePointSubtotal("JUNIOR") +  $objStudentInfo->getAcademicPerformancePointSubtotal("SENIOR");
	
	} else
	{
		$SeniorRowTotal = 0;
		
		$ADColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[AD]") ;
		$PDColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[PD]");
		$MCEColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[MCE]");
		$CSColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[CS]") ;
		$CEColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[CE]");
		$RDColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["R&D"]."]") ;
		$AAColumn = $objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["A&A"]."]");
		
		$SubjectsColumn =  $objStudentInfo->getAcademicPerformancePointSubtotal("JUNIOR") ;
	
	}
	
	$OverallTotal = $ADColumn+$PDColumn+$MCEColumn+$CSColumn+$CEColumn+$RDColumn+$AAColumn+$SubjectsColumn;
	
	$AwardTitle = $objStudentInfo->DetermineAward($OverallTotal);
	### IF NIL will not show the whole remarks -#F96819
	if($AwardTitle != 'NIL')
	{
		$AwardDisplay = '<div class="extra_award"> Award Obtained: '.$AwardTitle.'</div>';
	}
	else
	{
		$AwardDisplay = '';
	}
	
	if(!$_POST['displayReportFooter'])
	{
		$SignatureDisplay = "";
	}
	else
	{
		$SignatureDisplay = <<<EOF
		       <!-- signature start -->
       	<div class="sign_box">
        	<div><span></span></div>
        	<h1>Principal <em>( Mr. S. W. Law )</em></h1>
        </div>
        <div class="sign_box">
        	<div><span></span></div>
        	<h1>Teacher-in-Charge <em>( Mr. T. Leung <!-- {$ClassTeacher} -->)</em></h1>
        </div>
        <div class="sign_box">
        	<div><span></span></div>
        	<h1>School Chop</h1>
        </div>
        <div class="sign_box">
        	<div><span>{$issuedateStr}</span></div>
        	<h1>Date of Issue</h1>
        </div>
       <!-- signature end -->
EOF;
	}
/*
$SUB_PAGE_HEADER = <<<EOF
<!--Student info start -->
       <div class="student_info">
        <div class="student_info_detail" id="student_name">
            <ul>
                <li><span>Name</span><em>:</em>{$StudentInfoArr["EnglishName"]} (<font face="PMingLiU">{$StudentInfoArr["ChineseName"]}</font>)</li>
                <li><span>Class</span><em>:</em>{$StudentInfoArr["ClassName"]}</li>
            </ul>
        </div>
        
        <div class="student_info_detail" id="reg_no">
            <ul>
                <li><span>Registration No.</span><em>:</em>{$StudentInfoArr["WebSAMSRegNo"]}</li>
                <li><span>Class No.</span><em>:</em>{$StudentInfoArr["ClassNumber"]}</li>
            </ul>
        </div>
      </div>
       <!--Student info end -->
EOF;
*/


$SUB_PAGE_HEADER =<<<EOF
<!--Student info start -->
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="student_info">
       <tr>
       	<td nowrap="nowrap"><b>Name</b></td><td nowrap="nowrap">: {$StudentInfoArr["EnglishName"]} (<font face="PMingLiU">{$StudentInfoArr["ChineseName"]}</font>)</td>
       	<td align="right" width="70%"><b>Registration No.</b></td><td nowrap="nowrap">: {$StudentInfoArr["WebSAMSRegNo"]}</td>
       </tr>
       <tr>
       	<td><b>Class</b></td><td>: Form {$StudentInfoArr["ClassName"]}</td>
       	<td align="right"><b>Class No.</b></td><td>: {$StudentInfoArr["ClassNumber"]}</td>
       </tr>
        
</table>
       <!--Student info end -->
EOF;


$html = <<<EOF
<div class="main_container">
        <!--header start-->
        <div class="title_header">
                <h1>
                  STUDENT LEARNING PROFILE ({$academicYearStartEndDisplay})<br />
        Paulinian Award Scheme</h1>
       </div>
       <!--header end-->
       {$SUB_PAGE_HEADER}
       <!-- Table list start-->
		<div class="record_list_table">
        	<h1><span>Part 1</span>Other Learning Experiences
            <em><font size="-1">Only the three activities with the highest marks for each area are listed below:</font></em>            
          </h1>
            
            <table border="0" cellpadding="0" cellspacing="0">
            
              <tr>
                <th>Class/ Club/ Team/ Event</th>
                <th class="col_role">Role</th>
                <th class="col_marks">Marks</th>
              </tr>
              <tr>
                <td><u>Aesthetic Development</u></td>
                <td class="col_role"></td>
                <td class="col_marks"></td>
              </tr>
              {$Senior_AD_html}
              <tr>
                <td><u>Physical  Development</u></td>
                <td class="col_role"></td>
                <td class="col_marks"></td>
              </tr>
              {$Senior_PD_html}
              <tr>
                <td><u>Moral and Civic Education </u></td>
                <td class="col_role"></td>
                <td class="col_marks"></td>
              </tr>
              {$Senior_MCE_html}
              <tr>
                <td><u>Community Service</u></td>
                <td class="col_role"></td>
                <td class="col_marks"></td>
              </tr>
              {$Senior_CS_html}
                <tr>
    		        <td><u>Career-related Experiences</u></td>
                    <td class="col_role"></td>
                    <td class="col_marks"></td>
	            </tr>
               {$Senior_CE_html}
            </table>
      </div>
       <!-- Table list end-->
       <!--page no.-->
       <div class="page_no"> Page 1 of 3</div>
       </div>
	<p class="page_break" clear="all" />
	<div class="main_container">
       
       <!-- page no. end-->
        <!-- Table list start-->
		<div class="record_list_table_page2">
        	
        <table class="record_list_RDtable" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <th class="col_title"><u>Responsibilities &amp; Duties</u></th>
            <th class="col_year">&nbsp;</th>
            <th class="col_post"></th>
            <th class="col_marks"></th>
          </tr>
            
              <tr>
                <th class="col_title">Class/ Club/ Team/ Event</th>
                <th class="col_year">Academic Year</th>
                <th class="col_post">Post Title</th>
                <th class="col_marks">Marks</th>
              </tr>
              {$Senior_RandD_html}
            </table>
      </div>
       <!-- Table list end-->   
       <!-- Table list start-->
		<div class="record_list_table_page2">
        	
        <table class="record_list_AAtable" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <th class="col_title"><u>Awards &amp; Achievements</u></th>
            <th class="col_year">&nbsp;</th>
            <th class="col_activity">&nbsp;</th>
            <th class="col_award"></th>
            <th class="col_marks"></th>
          </tr>
            
              <tr>
                <th class="col_title">Class/ Club/ Team/ Event</th>
                <th class="col_year">Academic Year</th>
                <th class="col_activity">Name of Activity</th>
                <th class="col_award">Award</th>
                <th class="col_marks">Marks</th>
              </tr>
              {$Senior_AandA_html}
            </table>
      </div>
       <!-- Table list end-->
	  
        <!--page no.-->
       <div class="page_no"> Page 2 of 3</div>
       </div>
	<p class="page_break" clear="all" />
	<div class="main_container">

       <!-- page no. end-->   
        {$SUB_PAGE_HEADER}
       <!-- Table list start-->
	  <div class="summary_list_table">
        	<h1><span>Part 2</span>Academic Performance	<em><font size="-1">The following marks are awarded to the student for being in the top 20% of the class or achieving outstanding academic performance.</font></em></h1>
            
          <table>
            
      <tr>
                <th>Academic Studies</th>
                <th class="col_marks">Marks</th>
            </tr>
             {$Senior_Subject_html}
            </table>
      </div>
       <!-- Table list end-->
        <!-- Table list start-->
		<div class="summary_list_table">
        	<h1><span>Part 3</span>Overall Results          </h1>
            
            <table>
            
              <tr>
                <th>&nbsp;</th>
                <th class="summary_top"><div><span>Aesthetic <br />Development</span></div></th>
                <th class="summary_top"><div><span>Physical <br />Development</span></div></th>
                <th class="summary_top"><div><span>Moral and Civic <br />Education</span></div></th>
                <th class="summary_top"><div><span>Community <br />Service</span></div></th>
                <th class="summary_top"><div><span>Career-related <br />Experiences</span></div></th>
                <th class="summary_top"><div><span>Responsibilities <br />&amp; Duties</span></div></th>
                <th class="summary_top"><div><span>Awards &amp; <br />Achievements</span></div></th>
                <th class="summary_top"><div><span>Academic <br />Performance</span></div></th>
                <th>&nbsp;</th>
              </tr>
              <tr>
                <td class="sub_title" >JUNIOR SECONDARY <span>(Form 1 to Form 3)</span></td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[AD]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[PD]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[MCE]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[CS]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[CE]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["R&D"]."]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("JUNIOR", "[".$objStudentInfo->ELEID["A&A"]."]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->getAcademicPerformancePointSubtotal("JUNIOR")}</td>
                <td class="col_total_marks">{$JuniorRowTotal}</td>
              </tr>
EOF;


if ($current_form_value>3) {

$html .= <<<EOF
<tr>
                <td class="sub_title" >SENIOR SECONDARY <span>(Form 4 to Form 6)</span></td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[AD]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[PD]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[MCE]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[CS]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[CE]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["R&D"]."]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->GetOLESubtotal("SENIOR", "[".$objStudentInfo->ELEID["A&A"]."]")}</td>
                <td class="col_sub_marks">{$objStudentInfo->getAcademicPerformancePointSubtotal("SENIOR")}</td>
                <td class="col_total_marks">{$SeniorRowTotal}</td>
              </tr>
EOF;

} else { 
$html .= <<<EOF
<tr>
                <td class="sub_title" >SENIOR SECONDARY <span>(Form 4 to Form 6)</span></td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_sub_marks">0</td>
                <td class="col_total_marks">0</td>
              </tr>
EOF;


}




$html .= <<<EOF
<tr>
                <td class="sub_title" >TOTAL<span>(Form 1 to Form 6)</span></td>
                <td class="col_sub_marks">{$ADColumn}</td>
                <td class="col_sub_marks">{$PDColumn}</td>
                <td class="col_sub_marks">{$MCEColumn}</td>
                <td class="col_sub_marks">{$CSColumn}</td>
                <td class="col_sub_marks">{$CEColumn}</td>
                <td class="col_sub_marks">{$RDColumn}</td>
                <td class="col_sub_marks">{$AAColumn}</td>
                <td class="col_sub_marks">{$SubjectsColumn}</td>
                <td class="col_total_marks">{$OverallTotal}</td>
              </tr>
            </table>
      </div>
      {$AwardDisplay}
       <!-- Table list end-->
	  {$SignatureDisplay}
       <!--page no.-->
       <div class="page_no"> Page 3 of 3</div>
       <!-- page no. end--> 
    </div>
    
EOF;



	$html_page_break = ($i != (sizeof($studentList) - 1) ) ? '<p class="page_break" clear="all" />' : '';
	
$html .= <<<EOF
       
       {$html_page_break}
EOF;

	$html = str_replace("[TOTAL_PAGES]", $PageNow, $html);
	$html = str_replace("[SUB_PAGE_HEADER]", $SUB_PAGE_HEADER, $html);

	$html_body .= $html; 
}


$htmlCSS =<<<CSS
		<style type="text/css">
@charset "utf-8";
/* CSS Document */

html, body { margin: 0px; padding: 0px; } 

body { font-family: "Times New Roman", Times, serif, "新細明體"; font-size:13px}

.main_container {display:block; width:600px; height:850px; margin:auto; /*border:1px solid #CCC;*/  padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}
.page_no { display:block; bottom:-10px; width:600px;  text-align: right; position: absolute; }
.page_no span { position:absolute; right:30px; bottom:0px; border-top:1px solid #000; width:200px; padding-top:3px}
.page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}
.title_header {display:block; padding-right:30px; height:60px; clear:both}
.title_header h1 {text-align:center;font-weight:bold; font-family:'Times New Roman', Times, serif; font-size:16px; }
.student_info { display:block; height:50px;font-size:14px}
.student_info_detail { display:block;}
.student_info_detail ul {margin:0; padding:0; }
.student_info_detail ul  li{list-style-type:none; margin:0; padding:0; display:block; clear:both; }
.student_info_detail ul li span { font-weight:bold}
.student_info_detail ul li em { font-style:normal; padding:0 5px; text-align:center}
#student_name ul li {padding-left:70px;}
#student_name ul li em {display:block; float:left; margin-left:-15px; width:15px; padding:0;}
#student_name ul li span {display:block; float:left; margin-left:-70px; width:65px;}
#student_name { float:left}
#reg_no { float:right;}

.record_list_table,.summary_list_table { display:block; padding-top:20px; }
.record_list_table_page2 { display:block; padding-top:2px; }
.record_list_table h1, .record_list_table_page2 h1, .summary_list_table h1 { margin:0; padding:0; font-size:16px; margin-bottom:20px;}
.record_list_table h1 span, .record_list_table_page2 h1 span, .summary_list_table h1 span{ padding-right:30px;}
.record_list_table h1 em, .record_list_table_page2 h1 em, .summary_list_table h1 em { font-size:15px; font-weight:normal; clear:both; display:block}

.record_list_table table, .record_list_table_page2 table { *border-collapse: collapse; border-spacing: 0;width: 100%; }
.record_list_table table tr td, .record_list_table_page2 table tr td { padding:1px 0 1px 0; vertical-align:top }
.record_list_table table tr th, .record_list_table_page2 table tr th { padding:1px 0 1px 0; text-align:left;}

.col_role, .record_list_table table tr th.col_role, .record_list_table_page2 table tr th.col_role { width:150px; text-align:center }
.col_marks, .record_list_table table tr th.col_marks, .record_list_table_page2 table tr th.col_marks  { width:50px; text-align:center }
/*.col_year, .record_list_table table tr th.col_year { width:100px; text-align:center }*/

.record_list_table_page2 .record_list_RDtable tr .col_title { width:258px; padding-right:6px; }
.record_list_table_page2 .record_list_RDtable tr .col_year { width:100px; }
.record_list_table_page2 .record_list_RDtable tr .col_post  { width:194px; }
.record_list_table_page2 .record_list_RDtable tr .col_marks  { width:30px; text-align:center }

.record_list_table_page2 .record_list_AAtable tr .col_title { width:186px; padding-right:6px; }
.record_list_table_page2 .record_list_AAtable tr .col_year { width:89px; padding-right:4px; }
.record_list_table_page2 .record_list_AAtable tr .col_activity  { width:184px; padding-right:6px; }
.record_list_table_page2 .record_list_AAtable tr .col_award  { width:88px; }
.record_list_table_page2 .record_list_AAtable tr .col_marks  { width:30px; text-align:center }

.record_list_table table tr.others_row td { padding-bottom:10px;}
.record_list_table table tr.subtotal_row td{ border-top:2px solid #000; padding-top:10px; font-weight:bold}
.record_list_table_page2 table tr.others_row td { padding-bottom:3px; }
.record_list_table_page2 table tr.subtotal_row td { border-top:2px solid #000; padding-top:3px; padding-bottom:1px; font-weight:bold}

.summary_list_table h1 { margin:0 }
.summary_list_table table{ *border-collapse: collapse; border-spacing: 0;width: 100%; border-left:1px solid #000; border-top:1px solid #000; }
.summary_list_table table tr td { padding:1px 6px 1px 6px;vertical-align:top; border-right:1px solid #000; border-bottom:1px solid #000; }
.summary_list_table table tr th { padding:1px 6px 1px 0px;  border-right:1px solid #000; border-bottom:1px solid #000;}
.summary_list_table table tr th.col_marks {  width:90px; text-align:center}
.summary_list_table table tr.subtotal_row td{ font-weight:bold}
.summary_list_table table tr td.sub_title{ text-align:right; font-weight:bold}
.summary_list_table table tr td.sub_title span { display:block; clear:both}
.summary_list_table table tr td.col_sub_marks { width:30px; text-align:center; padding-right:0; vertical-align:middle}
.summary_list_table table tr td.col_total_marks { width:80px; font-weight:bold; text-align:center;vertical-align:middle}
.summary_list_table table tr th.summary_top { width:30px; height:120px; position:relative}
.summary_list_table table tr th div { display:block; width:30px; margin-left:5px; height:120px; position:relative;}
.summary_list_table table tr th  div span{ display:block; position:absolute; width:115px; left:30px; bottom:5px; height:30px; text-align:left;-moz-transform-origin: left bottom;
  -webkit-transform-origin: left bottom;
  -o-transform-origin: left bottom;
  transform-origin: left bottom;
  -moz-transform:rotate(270deg);
  -webkit-transform:rotate(270deg);
  -o-transform:rotate(270deg);
  transform:rotate(270deg);}

.extra_award{ display:block; clear:both; padding:20px 0; font-weight:bold}
.sign_box { display:block; float:left; width:25%; height:200px;}
.sign_box div{ display: table; text-align:center; margin:5px; border-bottom: 1px solid #000; height:120px; width:90%;}
.sign_box div span { display:table-cell; height:110px; vertical-align:bottom} 
.sign_box h1 { display:block; text-align:center; margin:0; padding:0; font-size:13px;}
.sign_box h1 em { display:block; clear:both; font-weight:normal; font-style:normal}


</style>
CSS;


_handleCurrentAlumniStudentAfterPrinting("alumni", $AlumniStudentArr);

if($PrintType == "word"){
	$objReportMgr->outPutToWord($html_body);
}else{

$html_head =<<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> STUDENT LEARNING PROFILE (Paulinian Award Scheme) </title>

{$htmlCSS}

</head>

<body>
EOF;

	echo $html_head . $html_body . "</body></html>";

}
exit();
?>