<?php
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/pkms/libpf-slp-pkms.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

//debug_pr($_SESSION);
//debug_pr($_POST);
$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
//$StudentID = trim($StudentID);
//$issuedate  = trim($issuedate);

// Get default setting
$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
if(file_exists($portfolio__plms_report_config_file))
{
	list($r_formAllowed, $startdate, $enddate,$allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecords) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
	$r_isPrintIssueDate = trim(unserialize($r_isPrintIssueDate));
	
	// for access checking
//	$formArray = unserialize($r_formAllowed);
	
//	$LibUserStudent = new libuser($UserID);
//	$LibPortfolio = new libpf_slp();
//	$StudentLevel = $LibPortfolio->getClassLevel($LibUserStudent->ClassName);
	
	// Set issue date
	if($r_isPrintIssueDate != null && $r_isPrintIssueDate == 1){
		$issuedate = trim(unserialize($issuedate));
	} else {
		$issuedate = "";	
	}

	// stop 
//	if(!(is_array($formArray) && in_array($StudentLevel[0], $formArray))){
//		intranet_closedb();
//		header("Location: $PATH_WRT_ROOT./home/portfolio/profile/report/report_printing_menu.php");
//		exit();
//	}
	
}

if ($issuedate!="")
{
	$issuedateStr = date("d/m/Y", strtotime($issuedate));
} else
{
	$issuedateStr = "&nbsp;";
}

if (!isset($studentList))
{
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setYearClassID($YearClassID);
	
	$objReportMgr->setStudentID($StudentID);
	
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList();
}

// Include the main TCPDF library (search for installation path).
//require_once('../../../../../includes/tcpdf/config/lang/eng.php');
//require_once($PATH_WRT_ROOT.'includes/tcpdf/tcpdf.php');

// Prevent error: redeclare class TCPDF

require_once($PATH_WRT_ROOT.'includes/tcpdf/tcpdf.php');


//require_once('../../../../../includes/tcpdf60/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
    	global $PATH_WRT_ROOT;
        // Logo
        $image_file = $PATH_WRT_ROOT.'file/customization/pkms_logo.png';
        $this->Image($image_file, 14.6, 8.2, 18.7, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('arialunicid0', 'B', 14);
        // Title
        //$this->Cell(0, 15, '培 橋 中 學', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        
		// set color for background
		//$this->SetFillColor(255, 255, 255);
		// Vertical alignment
		//$this->SetLineStyle(array('width' => 9, 'cap' => 'butt', 'join' => 'miter', 'dash' => 1, 'color' => array(5, 255, 255)));
		
        $this->SetFont('arialunicid0', '', 15);
		$this->MultiCell(45, 40, '培  僑  中  學<br /><font size="9">PUI KIU MIDDLE SCHOOL</font>', 0, 'L', 0, 0, '', '', false, 0, true, true, 40, 'T');
        $this->SetFont('droidsansfallback', '', 12);
		$this->MultiCell(65, 40, '學生學習概覽<br /><font size="11" face="courier">STUDENT LEARNING PORTFOLIO</font>', 0, 'C', 0, 0, '', '', true, 0, true, true, 40, 'M');
        $this->SetFont('droidsansfallback', '', 8);
		$this->MultiCell(51, 40, '<font color="#555555">香港北角天后廟道190號<br /><font face="helvetica">190, Tin Hau Temple Rd.,<br/>North Point, Hong Kong.</font></font>', 0, 'R', 0, 1, '', '', true, 0, true, true, 40, 'B');
    }

    // Page footer
    public function Footer() {
    	
    	global $issuedateStr;
    	
        // Position at 15 mm from bottom
        $this->SetY(-18);
        // Set font
        // Page number
        //$this->Cell(0, 10, 'School Chop \r校印 / Date of Issue 派發日期 <br /> sdfd ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->SetFont('arialunicid0', '', 9);
        $this->Line(20, 283, 70, 283, "");
		$this->MultiCell(60, 40, '&nbsp;<br /><font face="courier">School Chop</font><br />校印', 0, 'C', 0, 0, '', '', false, 0, true, true, 40, 'T');
		$this->MultiCell(55, 40, '', 0, 'C', 0, 0, '', '', false, 0, true, true, 40, 'T');
        $this->SetFont('arialunicid0', '', 9);
        $this->Line(133, 283, 183, 283, "");
        // Set issue date
		$this->MultiCell(60, 40, $issuedateStr.'<br /><font face="courier">Date of Issue</font><br />派發日期', 0, 'C', 0, 0, '', '', true, 0, true, true, 40, 'M');
		//<table width="200" border="0" cellpadding="0" cellspacing="0"><tr><td style=\'border-bottom:1px black solid;\'></td></tr></table>
        //$this->SetFont('arialunicid0', 'R', 8);
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetTitle('培橋中學 SLP');
$pdf->SetSubject('Student Learning Portfolio');
$pdf->SetKeywords('培橋中學', 'eClass, iPortfolio, SLP');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "培橋中學", "培橋中學");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 40);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------



	
$academic_year_lib = new academic_year();
$ay_records = $academic_year_lib->Get_All_Year_List();
$AcademicYearArray = build_assoc_array($ay_records);
	
# loop students
for ($i = 0; $i < sizeof($studentList); $i++)
{	
	$_studentId = $studentList[$i];

	$objStudentInfo = new studentInfo($_studentId);
	//$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	//$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	
	$StudentInfoArr = $objStudentInfo->getStudentInfo();
	$StudentInfoArr = $StudentInfoArr[0];
	
	if (!isset($ELEArray))
	{
		$ELEArray = build_assoc_array($objStudentInfo->getELE());
	}
	
	if (!isset($CategoryArray))
	{
		$CategoryArray = build_assoc_array($objStudentInfo->getCategory());
	}

	// set font
	//$pdf->SetFont('dejavusans', '', 10);
	$pdf->SetFont('droidsansfallback', '', 9);
	
	// add a page
	$pdf->AddPage();
	
	
	if ($StudentInfoArr["Gender"]=="M")
	{
		$StudentInfoArr["Gender"] = "男";
	} elseif ($StudentInfoArr["Gender"]=="F")
	{
		$StudentInfoArr["Gender"] = "女";
	}

	// create some HTML content
	$html_student_info = '<table width="100%" border="0" cellpadding="5" cellspacing="0">' .
		'<tr>' .
			'<td width="16%">學生姓名 <font face="helvetica">Name</font> :</td><td width="28%">'.$StudentInfoArr["ChineseName"].' <font face="helvetica">('.$StudentInfoArr["EnglishName"].')</font></td>' .
			//'<td width="16%">學生姓名 <font face="helvetica">Name</font> :</td><td width="28%">莊雅詠  <font face="helvetica">(CHONG NGA WING)</font></td>' .
			'<td width="2%"> </td>' .
			'<td width="12%">班別 <font face="helvetica">Class</font> :</td><td width="10%"><font face="helvetica">'.$StudentInfoArr["ClassName"].' ('.$StudentInfoArr["ClassNumber"].')</font></td>' .
			'<td width="2%"> </td>' .
			'<td width="18%">學籍編號 <font face="helvetica">Reg. No.</font> :</td><td width="12%"><font face="helvetica">'.$StudentInfoArr["WebSAMSRegNo"].'</font></td>' .
		'</tr>' .
		'<tr>' .
			'<td >出生日期 <font face="helvetica">DOB</font> :</td><td ><font face="helvetica">'.$StudentInfoArr["DateOfBirth"].'</font></td>' .
			'<td > </td>' .
			'<td >性別 <font face="helvetica">Sex</font> :</td><td width="10%">'.$StudentInfoArr["Gender"].'</td>' .
			'<td > </td>' .
			'<td >身份證號碼 <font face="helvetica">HKID</font> :</td><td width="13%"><font face="helvetica">'.$StudentInfoArr["HKID"].'</font></td>' .
		'</tr>' .
		'</table>';
		
	
	
	
	
	$OLE_Records = $objStudentInfo->GetOLE();
	//debug_r($OLE_Records);die();

	$html = $html_student_info;
	
	$html .= '<br /><table width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td><font size="10" face="arialunicid0"><b>其他學習經歷 OTHER LEARNING EXPERIENCE</b></font></td></tr></table>';
			
			
	$html .= '<table width="650" border="1" cellpadding="4" cellspacing="0">' .
		'<thead><tr>' .
			'<td width="246" colspan="2" align="center">項 目 <font face="times">Programmes</font></td>' .
			'<td width="98">類 別 <font face="times">Categories</font></td>' .
			'<td width="78">學 年 <font face="times">Year</font></td>' .
			'<td width="78">角 色 <font face="times">Role</font></td>' .
			'<td width="150">成 就 <font face="times">Achievements</font></td>' .
		'</tr></thead>';
		
	$max_limit = 20;
	if (sizeof($OLE_Records)>$max_limit)
	{
		$ole_size_limit = $max_limit;
	} else
	{
		$ole_size_limit = sizeof($OLE_Records);
	}
	for ($j=0; $j<$ole_size_limit; $j++)
	{
		$number_now = $j+1;
		$OLEObj = $OLE_Records[$j];
		if (trim($OLEObj["Achievement"])=="")
		{
			$OLEObj["Achievement"] = "無獎項";
		}
		
		$ELE_IDs = explode(",",$OLEObj["ELE"]);
		$ELE_Titles = array();
		for ($jj=0; $jj<sizeof($ELE_IDs); $jj++)
		{
			$ELE_Titles[] = $ELEArray[$ELE_IDs[$jj]];
		}
		$OLEObj["ELE"] = implode(" / ", $ELE_Titles);
		
		if (trim($OLEObj["ELE"])=="")
		{
			# use category if no ELE
			$OLEObj["ELE"] = $CategoryArray[$OLEObj["Category"]];
		}
		
		$OLEObj["AcademicYearTitle"] = $AcademicYearArray[$OLEObj["AcademicYearID"]];
		
		$html .= '<tr>' .
				'<td width="39" align="center"><font face="times" size="11">'.$number_now.'</font></td>' .
				'<td width="207">'.$OLEObj["Title"].'</td>' .
				'<td width="98">'.$OLEObj["ELE"].'</td>' .
				'<td width="78"> <font face="times">'.$OLEObj["AcademicYearTitle"].'</font></td>' .
				'<td width="78">'.$OLEObj["Role"].'</td>' .
				'<td width="150">'.$OLEObj["Achievement"].'</td>' .
			'</tr>' .
			'<tr>' .
				'<td align="center"><font size="7">詳情</font><br /><font face="times" size="6">Detail</font></td>' .
				'<td colspan="5"><font size="8">'.$OLEObj["Details"].'</font></td>' .
			'</tr>';
	}
	
	if (sizeof($OLE_Records)<=0)
	{
		$html .= '<tr>' .
				'<td colspan="6" align="center">沒 有 紀 錄</td>' .
			'</tr>';
	}
		
	$html .= '</table>';
		
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');


	if ($displaySelfAccount)
	{
		// add a page
		$pdf->AddPage();
		
		
		// create some HTML content
		$html = $html_student_info;
		
		$html .= '<br /><table width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td><font size="10" face="arialunicid0"><b>個人自述 SELF REFLECTION</b></font></td></tr></table>';
		
		$SelfAccountRecord = $objStudentInfo->getSelfAccount();
		
		$SelfAccountContents = $SelfAccountRecord[0][0];
		if (trim($SelfAccountContents)=="")
		{
			$SelfAccountContents = "沒 有 紀 錄";
			$aslignment_used = " align=\"center\" ";
			$font_size = "";
		} else
		{
			$aslignment_used = "";
			$font_size = " size=\"12\" ";
		}
				
		$html .= '<table width="100%" border="1" cellpadding="5" cellspacing="0" >' .
				'<tr>' .
					'<td '.$aslignment_used.'><font '.$font_size.'>'.$SelfAccountContents.'</font></td>' .
		
				'</tr>' .
				'</table>';
				
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');
	}
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('pkms_slp.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


?>