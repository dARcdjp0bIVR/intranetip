<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lcg/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lcg/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);

$objReportMgr = new rptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


$html = '';
for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);



	$html .= '<table border = "0" width = "700" align="Center">';
	$html .= '<tr><td>';
	### Page Content
//	$html .=$objStudentInfo->getEmptyTable('3%');

	$html .= $objStudentInfo->getPart1_HTML();	
	$html .=$objStudentInfo->getEmptyTable('2%');

	$html .= $objStudentInfo->getPart2_HTML();	
	$html .=$objStudentInfo->getEmptyTable('2%');

	$html .=$objStudentInfo->getStudentSelf_Account();
	$html .=$objStudentInfo->getEmptyTable('2%');

	$html .=$objStudentInfo->getPerformanceAndAwards();
	$html .=$objStudentInfo->getEmptyTable('2%');

	$html .=$objStudentInfo->getServices();
	$html .=$objStudentInfo->getPagebreakTable();

	$html .= '<table width="100%">';
	$html .= '<tr><td height="940px">';
		$html .= $objStudentInfo->getOLE();
		$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= '</td></tr>';
// 	if($displayReportFooter){	
		$html .= '<tr><td>';
			$html .= $objStudentInfo->getFooter($displayReportFooter, $printIssueDate);
		$html .= '</td></tr>';
// 	}
	$html .= '</table>';

	$html .= '</td></tr>';
	$html .= '</table>';

	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>