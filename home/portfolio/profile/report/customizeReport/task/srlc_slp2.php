<?php
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/srlc/libpf-slp-srlc.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
require_once($PATH_WRT_ROOT.'includes/tcpdf60/tcpdf.php');

$AcademicYear = getCurrentAcademicYear();

function getDisplayScore($newArray,$__academicYearId,$_SubjectID,$_SubjectCode){
	//Display Score/Grade/---
	if($newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'] != '' &&
		$newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'] >= 0){
		$__displayScore = $newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Score'];
	}
	else{
		
		if($newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Grade'] ==""){
			$__displayScore = '---';
		}
		else{
			
			$__displayScore = $newArray[$__academicYearId][$_SubjectID][$_SubjectCode]['Grade'];
		}
	}
	return $__displayScore;
}

class MYPDF extends TCPDF {
	
	var $special_header;
	
	public function set_special_header($var){
		$this->special_header = $var;
	}
	
    //Page header
    public function Header() {
        global $PATH_WRT_ROOT,$student_header,$StudentInfoArr,$AcademicYear;
			$this->SetAlpha(0.3);
			$image_file = $PATH_WRT_ROOT.'/file/customization/srlc/srlc_bkg.jpg';
	        $this->Image($image_file, 11, 50, 190, 0, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			$this->SetAlpha(1);
			
			$this->SetFont('arialunicid0', 'b', 16);
			$this->SetY(10);
			$this->Cell(0, 0, 'St. Rose of Lima\'s College', 0, 0, 'L');
			
			$this->SetFont('arialunicid0', 'b', 12);
			$this->SetY(11);
			$this->Cell(0, 0, '        Student Learning Profile', 0, 0, 'C');
			
			$this->SetFont('arialunicid0', 'b', 12);
			$this->SetY(11);
			$this->Cell(0, 0, $AcademicYear, 0, 0, 'R');
			
			$this->SetFont('arialunicid0', '', 10);
			$this->SetY(19);
			$this->writeHTML($student_header);
			$this->Line(12, 19, 198, 19,'');
			$this->Line(12, 34, 198, 34,'');
			
			if($this->special_header){
				$this->writeHTML($this->special_header);
			}
    }

    // Page footer
    public function Footer() {
           
        $signature_footer = '
			<table>
			<table height=24"mm" width="186mm">
				<tr>
					<td colspan="2">School Chop:</td>
				</tr>
				<tr>
					<td height="6mm" colspan="2"></td>
				</tr>
				<tr>
					<td height="8mm" width="120mm"></td>
					<td height="8mm" width="66mm" align="center">_______________________________<br>Mr. Yeung Kit Man<br>Principal</td>
				</tr>
			</table>
		';
        
		$school_infoFooter .= '
			<table>
				<tr style="line-height: 200%;">
					<td width="78mm">School Address : 29 Ngan Shing Street, Shatin</td>
					<td width="56mm">Telephone Number : 2337-1867</td>
					<td width="52mm">Fax Number : 2338-0915</td>					
				</tr>
			</table>
		';
		
		
		$this->SetY(-47);
		$this->SetFont('arialunicid0', 'b', 10);
		//many space in the next row for push the word to the right becoz cant align to R
		$this->Cell(0, 0, '                                                                                                                                                                                          Page '.$this->getPageNumGroupAlias(), 0, 0, 'C');
			
		$this->SetY(-41);
		$this->SetFont('arialunicid0', 'b', 10);
		$this->writeHTML($signature_footer);
				
		$this->SetY(-17);
		$this->SetFont('arialunicid0', 'b', 10);
		$this->writeHTML($school_infoFooter);
        
        $lineStyle = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $this->Line(12, 280, 198, 280,$lineStyle);
        $this->Line(12, 255, 198, 255,$lineStyle);
        
        // Set font
//        $this->SetFont('arialunicid0', '', 8);
        //Page number
//        $this->SetY(-13);
//        $this->writeHTML('<span align="right">'.$this->getGroupPageNoFormatted().'/'.$this->getPageGroupAlias().'</span>');
        
        //$this->Cell(0, 10, $this->getGroupPageNoFormatted().'/'.$this->getPageGroupAlias(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetTitle('聖羅撒書院 SLP');
$pdf->SetSubject('Student Learning Portfolio');
$pdf->SetKeywords('聖羅撒書院', 'eClass, iPortfolio, SLP');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
////$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetHeaderMargin(0);
////$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//$pdf->SetFooterMargin(0);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$rightMargins =  12;
$leftMargins = 12;
$contentWidth = 210 - $leftMargins - $rightMargins;
$pdf->SetMargins($leftMargins, 37, $rightMargins); 
 
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 60);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

//line image
$blackline = $PATH_WRT_ROOT.'/file/customization/srlc/blackline.png';
$line = $PATH_WRT_ROOT.'/file/customization/srlc/line.png';

// ---------------------------------------------------------

$objPrinting = new objPrinting();

if(!empty($StudentID)){
	$studentList = array($StudentID);
}

if(empty($issuedate)){
	$issuedate = date("d/m/Y");
}

if (!isset($studentList))
{
	$objReportMgr = new ipfReportManager();
	
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList();

}

//$mode = 'pdf';
$objPrinting->setYearClassID($YearClassID);
$objPrinting->setStudentList($studentList);
$objPrinting->contentReady();

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(false);

foreach((array)$studentList as $_studentid){
	
	// Change student
	$objPrinting->setStudent($_studentid);
	
	############################################################################################################
	####################################### Studnent Info (Start) ##############################################
	############################################################################################################
	
	$StudentInfoArr = $objPrinting->getStudentInfo();
	$col1_width = 30;
	$col2_width = 30;
	$col3_width = 20;
	$col4_width = 25;
	$col5_width = 30;
	$col6_width = $contentWidth - $col1_width-$col2_width-$col3_width-$col4_width-$col5_width;
	
	$student_header = '
					<table width="'.$contentWidth.'mm">
						<tr>
							<td width="'.($col1_width+$col2_width+$col3_width+$col4_width).'mm" colspan="3"><b>Name in English:</b> '.$StudentInfoArr["EnglishName"].'</td>
							<td width="'.$col5_width.'mm"><b>Name in Chinese: </b></td>
							<td width="'.$col6_width.'mm">'.$StudentInfoArr["ChineseName"].'</td>
						</tr>
						<tr>
							<td width="'.$col1_width.'mm"><b>Class: </b>'.$objPrinting->Student_ClassName.'</td>
							<td width="'.$col2_width.'mm"><b>Class Number:</b></td>
							<td width="'.$col3_width.'mm">'.$StudentInfoArr["ClassNumber"].'</td>
							<td width="'.$col4_width.'mm"><b>Sex: </b>'.$StudentInfoArr["Gender"].'</td>
							<td width="'.$col5_width.'mm"><b>Date of Birth: </b></td>
 							<td width="'.$col6_width.'mm">'.$StudentInfoArr["DateOfBirth"].'</td>
						</tr>
						<tr>
							<td colspan="2"><b>HKID No.: </b>'.$StudentInfoArr["HKID"].'</td>
							<td colspan="2"><b>STRN: </b>'.$StudentInfoArr["STRN"].'</td>
							<td><b>Date of Issue:</b></td>
							<td>'.$issuedate.'</td>
						</tr>
					</table>';
	############################################################################################################
	###################################### Studnent Info (End) #################################################
	############################################################################################################
	
	############################################################################################################
	############################ Section 2 - Awards Record - Int (Start) #######################################
	############################################################################################################
	$Awards_Int_AssoArr = $objPrinting->GetOLE('INT','AWARDS');
	
	//------- calculate table column width
	$Awards_col1_width = 30;
	$Awards_col2_width = 100;
	$Awards_col3_width = 56;
	
	
	//if(count($Awards_Int_Records) == 0){
		
	//}else{
	$Awards_Int_table ='
			<table width="'.$contentWidth.'mm">
				<thead>
					<tr style="line-height: 200%;">
						<td colspan="3"><font size="12"><b>Awards and Achievements Obtained in the School</b></font></td>
					</tr>
					<tr>
						<td width="'.$Awards_col1_width.'mm"><b>Academic Year</b></td>
						<td width="'.$Awards_col2_width.'mm"><b>Programme Name</b></td>
						<td width="'.$Awards_col3_width.'mm"><b>Achievement</b></td>
					</tr>
					<tr style="line-height: 50%;">
						<td colspan="3"><img src="'.$blackline.'"></td>
					</tr>
				</thead>
				<tbody>';
	foreach((array)$Awards_Int_AssoArr as $_academicYearID => $_Awards_Int__Records){
		foreach((array)$_Awards_Int__Records as $__key => $__Awards_Int__Obj){
			
			if($__key == 0){
				$__displayAcademicYear = $__Awards_Int__Obj['AcademicYear'];
			}
			else{
				$__displayAcademicYear = '';
			}
			
			$Awards_Int_table .='			
					<tr style="line-height: 175%;">
						<td width="'.$Awards_col1_width.'mm">'.$__displayAcademicYear.'</td>
						<td width="'.$Awards_col2_width.'mm">'.$__Awards_Int__Obj['Title'].'</td>
						<td width="'.$Awards_col3_width.'mm">'.$__Awards_Int__Obj['Achievement'].'</td>
					</tr>
					<tr style="line-height: 50%;">
						<td></td>
						<td colspan="2"><img src="'.$line.'"></td>
					</tr>
					';
		}
	}

	$Awards_Int_table .='
				</tbody>
			</table>';

	$section2 = $Awards_Int_table;
	############################################################################################################
	############################# Section 2 - Awards Record - Int (End) ########################################
	############################################################################################################

	############################################################################################################
	############################### Section 4 - ECA Record - Int (Start) #######################################
	############################################################################################################
	$ECA_AssoArr = $objPrinting->GetOLE('INT','ECA');
	
	//------- calculate table column width
	$Awards_col1_width = 30;
	$Awards_col2_width = 100;
	$Awards_col3_width = 56;
	
	
	//if(count($ECA_Records) == 0){
		
	//}else{
	$ECA_table ='
			<table width="'.$contentWidth.'mm">
				<thead>
					<tr style="line-height: 200%;">
						<td colspan="3"><font size="12"><b>Responsible Post(s) and Participation of Regular Extra-curricular Activities in the School</b></font></td>
					</tr>
					<tr>
						<td width="'.$Awards_col1_width.'mm"><b>Academic Year</b></td>
						<td width="'.$Awards_col2_width.'mm"><b>Organisation(s)</b></td>
						<td width="'.$Awards_col3_width.'mm"><b>Role</b></td>
					</tr>
					<tr style="line-height: 50%;">
						<td colspan="3"><img src="'.$blackline.'"></td>
					</tr>
				</thead>
				<tbody>';
	foreach((array)$ECA_AssoArr as $_academicYearID => $_ECA__Records){
		foreach((array)$_ECA__Records as $__key => $__ECA__Obj){
			
			if($__key == 0){
				$__displayAcademicYear = $__ECA__Obj['AcademicYear'];
			}
			else{
				$__displayAcademicYear = '';
			}
			
			$ECA_table .='			
					<tr style="line-height: 175%;">
						<td width="'.$Awards_col1_width.'mm">'.$__displayAcademicYear.'</td>
						<td width="'.$Awards_col2_width.'mm">'.$__ECA__Obj['Title'].'</td>
						<td width="'.$Awards_col3_width.'mm">'.$__ECA__Obj['Role'].'</td>
					</tr>
					<tr style="line-height: 50%;">
						<td></td>
						<td colspan="2"><img src="'.$line.'"></td>
					</tr>
					';
		}
	}

	$ECA_table .='
				</tbody>
			</table>';

	$section4 = $ECA_table;
	############################################################################################################
	################################ Section 4 - ECA Record - Int (End) ########################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 5 - OLE internal (Start) ###########################################
	############################################################################################################
	$OLE_AssoArr = $objPrinting->GetOLE('INT','OLE');

	//------- calculate table column width
	$OLE_spacing = 7;
	$OLE_col1_width = 103;
	$OLE_col2_width = 43;
	$OLE_col3_width = 33;
	
	$OLE_table ='
			<table width="'.$contentWidth.'mm">
				<tr style="line-height: 200%;">
					<td colspan="4"><font size="12"><b>Other Learning Experiences in the School</b></font></td>
				</tr>
				<tr>
					<td width="'.$OLE_spacing.'mm"></td>
					<td width="'.$OLE_col1_width.'mm"><b>Name of Programme</b></td>
					<td width="'.$OLE_col2_width.'mm"><b>Date</b></td>
					<td width="'.$OLE_col3_width.'mm"><b>Role</b></td>
				</tr>
				<tr style="line-height: 50%;">
					<td colspan="4"><img src="'.$blackline.'"></td>
				</tr>
			<table>';
	$OLE_header = $OLE_table;
	foreach((array)$OLE_AssoArr as $_academicYearID => $_OLE_Records){
		$OLE_table .= '<table>';
		$OLE_table .= '<thead>';
		$OLE_table .= '<tr style="line-height: 125%;">';
		$OLE_table .= '<td></td>';
		$OLE_table .= '</tr>';
		$OLE_table .= '<tr>';
		$OLE_table .= '<td style="line-height: 75%;" colspan="4">'.$_academicYearID.'</td>';
		$OLE_table .= '</tr>';
		$OLE_table .= '<tr style="line-height: 50%;">';
		$OLE_table .= '<td colspan="4"><img src="'.$line.'"></td>';
		$OLE_table .= '</tr>';
		$OLE_table .= '</thead>';
		$OLE_table .= '<tbody>';
		foreach((array)$_OLE_Records as $__OLE_Obj){
			
			$__startDate = $__OLE_Obj['StartDate'];
			$__endDate = $__OLE_Obj['EndDate'];
			if($__startDate == '0000-00-00' || $__startDate == ''){
				$__displayDate = '--';
			}
			else if($__endDate == '0000-00-00'){
				$__displayDate = $__startDate;
			}
			else{
				$__displayDate = $__startDate.' - '.$__endDate;
			}
			
			$OLE_table .='			
					<tr>
						<td width="'.$OLE_spacing.'mm"></td>
						<td width="'.$OLE_col1_width.'mm">'.$__OLE_Obj['Title'].'</td>
						<td width="'.$OLE_col2_width.'mm">'.$__displayDate.'</td>
						<td width="'.$OLE_col3_width.'mm">'.$__OLE_Obj['Role'].'</td>
					</tr>
					<tr style="line-height: 50%;">
						<td></td>
						<td colspan="3"><img src="'.$line.'"></td>
					</tr>
					';
		}
		$OLE_table .='
				</tbody>
			</table>';
	}
	
	$section5 = $OLE_table;
	############################################################################################################
	################################ Section 5 - OLE internal (End) ############################################
	############################################################################################################
	
	############################################################################################################
	############################### Section 6 - OLE external (Start) ###########################################
	############################################################################################################
	
	$OLEE_AssoArr = $objPrinting->GetOLE('EXT','OLE');
	
	//------- calculate table column width
	$OLEE_spacing = 7;
	$OLEE_col1_width = 103;
	$OLEE_col2_width = 43;
	$OLEE_col3_width = 33;
	
	$OLEE_table ='
			<table width="'.$contentWidth.'mm">
				<tr style="line-height: 200%;">
					<td colspan="4"><font size="12"><b>Other Learning Experiences Outside School</b></font></td>
				</tr>
				<tr>
					<td width="'.$OLEE_spacing.'mm"></td>
					<td width="'.$OLEE_col1_width.'mm"><b>Name of Programme<br>Organisation(s)</b></td>
					<td width="'.$OLEE_col2_width.'mm"><b>Date</b></td>
					<td width="'.$OLEE_col3_width.'mm"><b>Role</b></td>
				</tr>
				<tr style="line-height: 50%;">
					<td colspan="4"><img src="'.$blackline.'"></td>
				</tr>
			</table>';
	$OLEE_header = $OLEE_table;
	foreach((array)$OLEE_AssoArr as $_academicYearID => $_OLEE_Records){
		$OLEE_table .= '<table>';
		$OLEE_table .= '<thead>';
		$OLEE_table .= '<tr style="line-height: 125%;">';
		$OLEE_table .= '<td></td>';
		$OLEE_table .= '</tr>';
		$OLEE_table .= '<tr style="line-height: 75%;">';
		$OLEE_table .= '<td colspan="4">'.$_academicYearID.'</td>';
		$OLEE_table .= '</tr>';
		$OLEE_table .= '<tr style="line-height: 50%;">';
		$OLEE_table .= '<td colspan="4"><img src="'.$line.'"></td>';
		$OLEE_table .= '</tr>';
		$OLEE_table .= '</thead>';
		$OLEE_table .= '<tbody>';
		foreach((array)$_OLEE_Records as $__OLEE_Obj){
			
			$__startDate = $__OLEE_Obj['StartDate'];
			$__endDate = $__OLEE_Obj['EndDate'];
			if($__startDate == '0000-00-00' || $__startDate == ''){
				$__displayDate = '--';
			}
			else if($__endDate == '0000-00-00'){
				$__displayDate = $__startDate;
			}
			else{
				$__displayDate = $__startDate.' - '.$__endDate;
			}
			
			if($__OLEE_Obj['Organization'] != ''){
				$__displayOrganization = '<br>'.$__OLEE_Obj['Organization'];
			}
			else{
				$__displayOrganization = '';
			}
			
			$OLEE_table .='			
					<tr>
						<td width="'.$OLEE_spacing.'mm"></td>
						<td width="'.$OLEE_col1_width.'mm">'.$__OLEE_Obj['Title'].$__displayOrganization.'</td>
						<td width="'.$OLEE_col2_width.'mm">'.$__displayDate.'</td>
						<td width="'.$OLEE_col3_width.'mm">'.$__OLEE_Obj['Role'].'</td>
					</tr>
					<tr style="line-height: 50%;">
						<td></td>
						<td colspan="3"><img src="'.$line.'"></td>
					</tr>
					';
		}
		$OLEE_table .='
				</tbody>
			</table>';
	}



	$section6 = $OLEE_table;
	############################################################################################################
	################################ Section 6 - OLE external (End) ############################################
	############################################################################################################
	
//	echo $section1;
//	echo $section1.$section2.$section3.$section4.$section5.$section6;
	// Start First Page Group
	$pdf->startPageGroup();
	
	$pdf->AddPage();
	$pdf->SetFont('arialunicid0', '', 10);
	$pdf->writeHTML($section2, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->AddPage();
	$pdf->writeHTML($section4, true, false, true, false, '');
	$pdf->endPage();
	
	$pdf->AddPage();
	$pdf->set_special_header($OLE_header); // for showing Title if there is page break (auto page break thead used by the year)
	$pdf->SetMargins($leftMargins, 52, $rightMargins); 
	$pdf->writeHTML($section5, true, false, true, false, '');	
	$pdf->set_special_header('');
	$pdf->SetMargins($leftMargins, 37, $rightMargins); 
	$pdf->endPage();

	$pdf->AddPage();
	$pdf->set_special_header($OLEE_header);
	$pdf->SetMargins($leftMargins, 55, $rightMargins); 
	$pdf->writeHTML($section6, true, false, true, false, '');
	$pdf->set_special_header('');
	$pdf->SetMargins($leftMargins, 37, $rightMargins); 
	$pdf->endPage();
	
	unset($StudentInfoArr);
	unset($student_header);
	unset($formAry);
	unset($assessmentReportInfo);
	unset($classHistoryInfoAry);
	unset($SchoolSubjectInfo);
	unset($assessment_report);
	unset($OLE_Records);
	unset($OLE_table);
	unset($StudentAwards);
	unset($awards_table);
	unset($OLEE_Records);
	unset($OLEE_table);
	unset($SelfAccountContents);
	unset($self_account);
	unset($section1);
	unset($section2);
	unset($section3);
	unset($section4);
	unset($section5);
}//End of a student

//Close and output PDF document
$pdf->Output('srlc_slp.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+

?>