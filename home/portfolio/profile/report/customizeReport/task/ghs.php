<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ghs/libpf-slp-ghsRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ghs/libpf-slp-ghsRptPDFGenerator_TCPDF.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ghs/libpf-slp-ghs.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);

### Basic PDF Settings
$pdf = new StudentReportPDFGenerator_TCPDF();
$left_margin = 7;
$right_margin = 7;
$header_blank_height = 46;
$footer_blank_height = 7;
$header_height = 86;
$footer_height = 30;



$objReportMgr = new ghsRptMgr();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$objReportMgr->setIssueDate($issuedate);
$issuedateStr = $objReportMgr->getIssueDate();

$studentList = $objReportMgr->getStudentIDList();

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$html = "";
for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setPrintIssueDate($issuedateStr);
	$objStudentInfo->setPrincipalName('Mr CHOW Chi Hung Paul');

	$h_reportHeader = $objStudentInfo->getReportHeader_HTML();
	$h_studentPartC = $objStudentInfo->getPartC_HTML();
	$h_studentPartD = $objStudentInfo->getPartD_HTML();
	$h_studentPartE = $objStudentInfo->getPartE_HTML();
	$h_signatureTable = $objStudentInfo->getReportSignature_HTML();
	
	$pdf->SetMargins($left_margin, PDF_MARGIN_TOP, $right_margin);
	$pdf->SetHeaderMargin($header_blank_height);
	$pdf->SetAutoPageBreak(TRUE, $footer_height);
	$pdf->SetFooterMargin($footer_height + PDF_MARGIN_FOOTER);
	
	$pdf->set_is_first_page(true); // this will be set to false in the StudentPDFGenerator_TCPDF class after calling Header()
	
	$is_to_show_header_on_first_page_only = false;
	$pdf->set_student_report_header(
		$h_reportHeader,
		$is_to_show_header_on_first_page_only,
		$header_height
	);														
	
	$is_to_show_footer_on_last_page_only = true;
	$pdf->set_student_report_footer(
		$h_signatureTable,
		$is_to_show_footer_on_last_page_only,
		$footer_height
	);
	
	// Start a new page for each student.			
	$pdf->AddPage();
	
	$html = <<<HTML
	<div width="780px">
		$h_studentPartC
		<br />
		$h_studentPartD
		<br />
		$h_studentPartE
	</div>
HTML;
	

	$pdf->writeHTML($html);
	$pdf->set_is_last_page(true);
}




if($PrintType == "pdf"){
	$report_content = $pdf->Output(null, 'S');
	$report_name = 'student_learning_profile.pdf';
	output2browser($report_content, $report_name);
}

exit();
?>