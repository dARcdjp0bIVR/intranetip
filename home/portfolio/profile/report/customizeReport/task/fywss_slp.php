<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/fywss/libpf-slp-fywss.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$PageRowMax = 48;
$PageOneHeadOccupied = 36;
$PageTwoHeadOccupied = 5;
$OddPageExtraOccupied = 5;

$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
//$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);

$objReportMgr = new ipfReportManager();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();


$issuedateStr = date("d/m/Y", strtotime($issuedate));

# Yuen - may need to filter students with license only
$studentList = $objReportMgr->getStudentIDList();


$academicYearIDOne = $academicYearID[0];


$libimport = new libimporttext();
# Personal Attributes 
$FileCSV = $PATH_WRT_ROOT."file/portfolio/fywss_pa_".$academicYearIDOne.".csv";
if (file_exists($FileCSV))
{
	$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
	$csv_header = $csv_data[0];
	for ($i=1; $i<sizeof($csv_data); $i++)
	{
		$rowObj = $csv_data[$i];
		$PersonalAttributes[trim($rowObj[0], "#")] = $rowObj;
	}
}

/* NOT FOLLOW THIS CONFIG YET!
$PersonalAttributeValues[4] = "Excellent<br />優異";
$PersonalAttributeValues[3] = "Good<br />良好";
$PersonalAttributeValues[2] = "Satisfactory<br />滿意";
$PersonalAttributeValues[1] = "Fair<br />尚可";
*/

for ($i = 0; $i < sizeof($studentList); $i++)
{
	$PageNow = 1;
	
	$RowLeft = $PageRowMax;
	
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setPrintIssueDate($issuedateStr);
	
	$StudentInfoArr = $objStudentInfo->getStudentInfo();
	$StudentInfoArr = $StudentInfoArr[0];
	
	$StudentAttributeResult = $PersonalAttributes[$StudentInfoArr["WebSAMSRegNo"]];

if(!$StudentInfoArr["ClassName"] || trim($StudentInfoArr["ClassName"]) == ""){
	$StudentClass = "--";
}
else{
	$StudentClass = $StudentInfoArr["ClassName"];
}

if ($StudentInfoArr["WebSAMSRegNo"]!="")
{
	//$BarCode = rawurlencode($StudentInfoArr["BarCode"]);
	$BarCode = rawurlencode($StudentInfoArr["WebSAMSRegNo"]);
	$BarCodeImage = '<img src="'.$PATH_WRT_ROOT.'home/eAdmin/ResourcesMgmt/eInventory/management/barcode_labels/barcode.php?barcode='.$BarCode.'&width=135&height=90" width="150"/>';
} else
{
	$BarCodeImage = "&nbsp;";
}

$ClassHistory = $objIPF->GET_STUDENT_CLASS_HISTORY($_studentId);
$AdmissionClassLevel = (int) $ClassHistory[0]["ClassName"];
if ($AdmissionClassLevel>0)
{
	$AdmissionClassLevel = "S".$AdmissionClassLevel;
} else
{
	$AdmissionClassLevel = "--";
}

$html = <<<EOF
<div class="main_container">
        <!--header start-->
		<div class="title_header">
           <!-- <h1>The Church of Christ in China Fong Yun Wah Secondary School<br />
	       <span> 中華基督教會方潤華中學</span><div class="school_logo"><img src="/file/fywss_slp_logo.jpg" width="130" height="155" />        </div></h1>
	       -->
	       <br /><br /><br /><br /><br /><br />
    	    <h2><span>Student Learning Profile</span><table><tr><td><img src="/images/spacer.gif" width="1" height"1"></td></tr></table>
        	<span>學生學習概覽</span>
        	<p><span>{$academicYearStartEndDisplay}</span></p> 
        	<div class="barcode">{$BarCodeImage}</div>
    	    </h2>
    </div>
    <br />
     <!--header end-->
       <!--Student info start -->
       <div class="student_info1">
        <div class="student_info_detail">
            <ul>
                <li><span>Name of Student 學生姓名</span><em>:</em>{$StudentInfoArr["EnglishName"]} {$StudentInfoArr["ChineseName"]}</li>
				<li><span>Date of Birth 出生日期</span><em>:</em>{$StudentInfoArr["DateOfBirth"]}</li>
                <li><span>Sex 性別</span><em>:</em>{$StudentInfoArr["Gender"]}</li>
                <li><span>Class Attended 就讀班別</span><em>:</em>{$StudentClass}</li>                            
            </ul>
        </div>
      </div>
      <div class="student_info2">
      <div class="student_info_detail">
            <ul>
                <li><span>HKID No 身份證號碼</span><em>:</em>{$StudentInfoArr["HKID"]}</li>
              <li><span>Date of Admission 入學日期</span><em>:</em>{$StudentInfoArr["AdmissionDate"]}</li>    
                <li><span>Form Admitted 入學年級</span><em>:</em>{$AdmissionClassLevel}</li>
                <li><span>Date of Issue 派發日期</span><em>:</em>{$issuedateStr}</li>                
            </ul>
        </div>
      </div>
       <!--Student info end -->
       <br />
    <br />
EOF;

# no need headers!
$SUB_PAGE_HEADER = <<<EOF
<div class="main_container">
        <!--header start-->
		<div class="title_header">
			<h1></h1>
    	    <h2><span>Student Learning Profile</span><br />
        	<span>學生學習概覽</span><br />
        	{$academicYearStartEndDisplay}<div class="barcode">{$BarCodeImage}</div>
    	    </h2>
    </div>
     <!--header end-->
       <div class="student_info1">
        <div class="student_info_detail">
            <ul>
                <li><span>Name 學生姓名</span><em>:</em>{$StudentInfoArr["EnglishName"]} ({$StudentInfoArr["ChineseName"]})</li>
            </ul>
        </div>
      </div>
      <div class="student_info2">
      <div class="student_info_detail">
            <ul>
                <li><span>HKID No 身份證號碼</span><em>:</em>{$StudentInfoArr["HKID"]}</li>
            </ul>
        </div>
      </div>
       <!--Student info end -->
       <br />
EOF;

# Sub Page Header
$SUB_PAGE_HEADER = <<<EOF
<div class="main_container">
        
       <br />
EOF;

$html .= <<<EOF
 <!-- Table list start-->
	 <br />&nbsp;<br /><div class="summary_list_table">
          <table>
            
      <tr>
                <th colspan="2"  align="center">Personal Attributes <br />個人特質</th>
                <th >Excellent<br />
                  優異</th>
        <th >Good<br />
          良好</th>
        <th >Satisfactory<br />
          滿意</th>
        <th >Fair<br />
          尚可</th>
      </tr>
EOF;

for ($j=3; $j<sizeof($csv_header); $j++)
{
	$attribute_name = $csv_header[$j];
	$tmpArr = explode("/", $attribute_name);
	$attribute_name_eng = trim($tmpArr[0]);
	$attribute_name_chi = trim($tmpArr[1]);
	
	$Grade4 = (trim($StudentAttributeResult[$j])==4) ? "&#10003;" : "&nbsp;";
	$Grade3 = (trim($StudentAttributeResult[$j])==3) ? "&#10003;" : "&nbsp;";
	$Grade2 = (trim($StudentAttributeResult[$j])==2) ? "&#10003;" : "&nbsp;";
	$Grade1 = (trim($StudentAttributeResult[$j])==1) ? "&#10003;" : "&nbsp;";
	
$html .= <<<EOF
              <tr>
                <td width="26%">{$attribute_name_eng}</td>
                <td width="18%">{$attribute_name_chi}</td>
                <td width="14%">{$Grade4}</td>
                <td width="14%">{$Grade3}</td>
                <td width="14%">{$Grade2}</td>
                <td width="14%">{$Grade1}</td>
            </tr>
EOF;
}
            
$html .= <<<EOF
            </table>
      </div>
       <!-- Table list end-->
EOF;


$RowLeft -= $PageOneHeadOccupied;


/*
$TableHeader = <<<EOF
<!-- Table list start-->
		<br />&nbsp;<div class="record_list_table">
        	<h1>A. List of Awards and Major Achievements 個人獎項及成就</h1>
A1. Issued by School 學校頒發
EOF;

$html .=  $TableHeader;

$RowLeft -= 2;


$html .= <<<EOF
<br /><br />&nbsp; A2. Issued by Other Agencies 其他機構頒發
EOF;		

*/


$TableHeader = <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
        	<h1>A. List of Awards and Major Achievements 個人獎項及成就</h1>
&nbsp;A1. Issued by School 學校頒發
      </div>
           
EOF;

$html .= $TableHeader;

$TableHeader = <<<EOF
       <!-- Table list start-->
		<div class="summary_list_table">
          <table>
            
      <tr>
        <th width="13%">Year <br />年份</th>
        <th width="45%">Activity / Competition <br />活動 / 比賽名稱</th>
        <th width="42%">Award / Achievement <br /> 獎項 / 成就</th>
      </tr>
EOF;

$html .= $TableHeader;
$RowLeft -= 4;

		$OLERecords = $objStudentInfo->getOLE("INT", null, "BySchool");


		list($PageNow, $RowLeft, $RecordHtml) = $objStudentInfo->displayOLEBySchool($OLERecords, $academicYearStartEndDisplay, $TableHeader, $PageNow, $RowLeft, $PageRowMax-$PageTwoHeadOccupied);
		$html .= $RecordHtml;
        





$TableHeader = <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
&nbsp;A2. Issued by Other Agencies 其他機構頒發
      </div>
           
EOF;

$html .= $TableHeader;

$TableHeader = <<<EOF
       <!-- Table list start-->
		<div class="summary_list_table">
          <table>
            
      <tr>
        <th width="13%">Year <br />年份</th>
        <th width="45%">Activity / Competition <br />活動 / 比賽名稱</th>
        <th width="18%">Organized By <br /> 主辦機構</th>
        <th width="24%">Award / Achievement <br /> 獎項 / 成就</th>
      </tr>
EOF;

$html .= $TableHeader;
$RowLeft -= 4;

		$OLERecords = $objStudentInfo->getOLE("INT", null, "ByOthers");


		list($PageNow, $RowLeft, $RecordHtml) = $objStudentInfo->displayOLEByOthers($OLERecords, $academicYearStartEndDisplay, $TableHeader, $PageNow, $RowLeft, $PageRowMax-$PageTwoHeadOccupied);
		$html .= $RecordHtml;



	if ($RowLeft<2)
	{
		$html .= '<!-- Table list end-->';
		//$RecordHtml .= '<br class="pageBreak" clear="all" />';
		### IMPORTANT: spaces are very important for top margin replacement!!!
		$html .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]</div>
	       </div>
	       <p class="page_break" clear="all" />';
		
		$PageNow ++;
		$RowLeft = $PageRowMax;
		if($PageNow % 2){
			$RowLeft -= $OddPageExtraOccupied;
		}
		
		# New page header
		$html .= '[SUB_PAGE_HEADER]';
		//$html .= $TableHeader;
	}


$TableHeader = <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
        	<h1>B. Other-Learning Experiences 其他學習經歷</h1>
      </div>
           
EOF;

$html .= $TableHeader;

$TableHeader = <<<EOF
       <!-- Table list start-->
		<div class="summary_list_table">
          <table>
            
      <tr>
        <th width="15%">Year <br />年份</th>
        <th width="35%">Activity / Programme <br />活動名稱</th>
        <th width="15%">Role <br /> 參與角色</th>
        <th width="20%">OLE<br />其他學習經歷範疇</th>
        <th width="15%">No. of Hour(s)<br />參與時數</th>
      </tr>
EOF;

$html .= $TableHeader;
$RowLeft -= 4;

		$OLERecords = $objStudentInfo->getOLE("INT");


		list($PageNow, $RowLeft, $RecordHtml) = $objStudentInfo->displayOLE($OLERecords, $academicYearStartEndDisplay, $TableHeader, $PageNow, $RowLeft, $PageRowMax-$PageTwoHeadOccupied);
		$html .= $RecordHtml;
     

if ($displaySelfAccount)
{

	if ($RowLeft<5)
	{
//		$html .= '</table></div><!-- Table list end-->';
		//$RecordHtml .= '<br class="pageBreak" clear="all" />';
		### IMPORTANT: spaces are very important for top margin replacement!!!
		$html .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]</div>
	       </div>
	       <p class="page_break" clear="all" />';
	       
		$PageNow ++;
		$RowLeft = $PageRowMax;
		if($PageNow % 2){
			$RowLeft -= $OddPageExtraOccupied;
		}
		
		# New page header
		$html .= '[SUB_PAGE_HEADER]';
		//$html .= $TableHeader;
	}
	

	$SelfAccount = $objStudentInfo->getSelfAccount();
	if (trim($SelfAccount[0][0])!="")
	{
		$HasRecord = true;
		$SelfAccountContents = trim($SelfAccount[0][0]);
	} else
	{
		$HasRecord = false;
		$SelfAccountContents = "&nbsp; &nbsp;NIL";
	}
	
	//$lineBreaks = mb_substr_count($SelfAccountContents, "<br");
	 
	 

	if ((countString($SelfAccountContents)/8 + 2) > $RowLeft)
	{
		# page break for self-account
		$first_page_rows = $RowLeft - 2;
		$characters = $first_page_rows * 46;
		

		$char_fullstop = array();
		if ($HasRecord)
		{
		    //D154776
		    if (mb_strlen($SelfAccountContents) < $characters) {
		        $char_fullstop[] = 0;
		    }
		    else {
		        $char_fullstop[] = (int) mb_strpos($SelfAccountContents, ".", $characters);
		        $char_fullstop[] = (int) mb_strpos($SelfAccountContents, "。", $characters);
		        $char_fullstop[] = (int) mb_strpos($SelfAccountContents, "!", $characters);
		        $char_fullstop[] = (int) mb_strpos($SelfAccountContents, "！", $characters);
		    }

			sort($char_fullstop);
			for ($ko=0; $ko<sizeof($char_fullstop); $ko++)
			{
				if ($char_fullstop[$ko]>0)
				{
					$characters = $char_fullstop[$ko] + 1;
					break;
				}
			}
		}

		
		$self_part_1 = mb_substr($SelfAccountContents, 0, $characters);
		
		$lineBreaks = mb_substr_count($self_part_1, "<br");
		
		

	
		if ($lineBreaks>3)
		{
			if ($lineBreaks<6)
			{
				$first_page_rows -= $lineBreaks + 3;
			} else
			{
				$first_page_rows -= round($lineBreaks*2/3);
			}
			$characters = $first_page_rows * 46;
			
			if ($HasRecord)
			{
			    //D154776
			    if (mb_strlen($SelfAccountContents) < $characters) {
			        $char_fullstop[] = 0;
			    }
			    else {
    				$char_fullstop[] = (int) mb_strpos($SelfAccountContents, ".", $characters);
    				$char_fullstop[] = (int) mb_strpos($SelfAccountContents, "。", $characters);
    				$char_fullstop[] = (int) mb_strpos($SelfAccountContents, "!", $characters);
    				$char_fullstop[] = (int) mb_strpos($SelfAccountContents, "！", $characters);
			    }
				
				sort($char_fullstop);
				for ($ko=0; $ko<sizeof($char_fullstop); $ko++)
				{
					if ($char_fullstop[$ko]>0)
					{
						$characters = $char_fullstop[$ko] + 1;
						break;
					}
				}
			}
			$self_part_1 = mb_substr($SelfAccountContents, 0, $characters);
			//debug(mb_substr($SelfAccountContents, 0, $characters);
		}
	
	
		$self_part_2 = mb_substr($SelfAccountContents, $characters);
		$self_part_2 = str_replace("\n", "", $self_part_2);
		$self_part_2 = ltrim($self_part_2, "<br />");
		
		if (mb_strlen($self_part_2)<50)
		{
			$self_part_1 .= "<br /><br />".$self_part_2;
			$self_part_2 = "";
		}
		
		if (trim($self_part_2)!="")
		{
		
			$TotalPage = $PageNow+1;
$html .= <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
        	<h1>C. Personal Statement 個人自述</h1>
            
<table>
              <tr>
                <td class="personal_stat">{$self_part_1}</td>
              </tr>
            </table>
      </div>
       <!-- Table list end-->

       <div class="page_no"> Page {$PageNow}/{$TotalPage}</div>
       </div>
       <p class="page_break" clear="all" />
EOF;
		$PageNow ++;
		
		$html .= $SUB_PAGE_HEADER;
$html .= <<<EOF
<!-- Table list start-->
	  <div class="record_list_table">
        	<h1>C. Personal Statement 個人自述</h1>
            
<table>
              <tr>
                <td class="personal_stat">{$self_part_2}</td>
              </tr>
            </table>
      </div>
       <!-- Table list end-->
EOF;
		} else
		{
			
$html .= <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
        	<h1>C. Personal Statement 個人自述</h1>
            
<table>
              <tr>
                <td class="personal_stat">{$self_part_1}</td>
              </tr>
            </table>
      </div>
       <!-- Table list end-->
EOF;
		}


	} else
	{
	
$html .= <<<EOF
<!-- Table list start-->
	  <br />&nbsp;<div class="record_list_table">
        	<h1>C. Personal Statement 個人自述</h1>
            
<table>
              <tr>
                <td class="personal_stat">{$SelfAccountContents}</td>
              </tr>
            </table>
      </div>
       <!-- Table list end-->
EOF;
	}
	
}
    $html .= '<div class="report_end">********************************************** End of Report **********************************************</div>';
    
	$html_page_break = ($i != (sizeof($studentList) - 1) ) ? '<p class="page_break" clear="all" />' : '';
	
$html .= <<<EOF
       <div class="page_no"> Page {$PageNow}/{$PageNow}
       <span> School Chop 校印</span></div>
       </div>
       {$html_page_break}
EOF;

	$html = str_replace("[TOTAL_PAGES]", $PageNow, $html);
	$html = str_replace("[SUB_PAGE_HEADER]", $SUB_PAGE_HEADER, $html);
	

	# add top magrin for odd number page(s)
	
	for ($si = 2; $si<$PageNow; $si++)
	{
		if ($si%2==0)
		{
			
			# IMPORTANT: spaces are very important for the replacement!!!
			
			$ToBeUpdated = '<div class="page_no"> Page '.$si.'/'.$PageNow.'</div>
	       </div>
	       <p class="page_break" clear="all" /><div class="main_container">';
	       
			$NewHtml = '<div class="page_no"> Page '.$si.'/'.$PageNow.'</div>
	       </div>
	       <p class="page_break" clear="all" /><div class="main_container"><br /><br /><br /><br /><br /><br />';
	       
	       
	       $html = str_replace($ToBeUpdated, $NewHtml, $html);
	      
       
       		# another format (different spacing)
			$ToBeUpdated = '<div class="page_no"> Page '.$si.'/'.$PageNow.'</div>
       </div>
       <p class="page_break" clear="all" /><div class="main_container">';
	       
	       
	       
	       $html = str_replace($ToBeUpdated, $NewHtml, $html);
	       
		}
       
	}
	
	# Add empty page for odd total page number report
	if($PageNow % 2 && $html != null){
		# Last Student - break after School Chop 校印
		if($html_page_break == ''){
			$html .= '<p class="page_break" clear="all" />';
		}
		$html .= '<div class="main_container"></div>';
		# Not last student - break after blank page
		# $html_page_break != '' => break after School Chop 校印
		if($html_page_break != ''){
			$html .= '<p class="page_break" clear="all" />';
		}
	}
	
	$html_body .= $html; 
}


$htmlCSS =<<<CSS
		<style type="text/css">
@charset "utf-8";
/* CSS Document */
body { font-family:  "Times New Roman"; font-size:13px; padding:0; margin:0;}

html, body { margin: 0px; padding: 0px; } 

.main_container {display:block; width:680px;  height:900px; margin:auto; /*border:1px solid #DDD;*/ padding:10px 20px; position:relative; padding-bottom:90px;}
.page_no { display:block; width:100%; bottom:-60px; padding:90px 0 0 0; text-align: center; position: absolute }
.page_no span { position:absolute; right:30px; bottom:25px; border-top:1px solid #000; width:200px; padding-top:3px}
.page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}
.report_end { display:block; clear:both; height:20px; line-height:20px; overflow:hidden; text-align:center} }
.title_header {display:block; min-height:100px; clear:both; position:relative }
.school_logo {display:block; position:absolute; left:40px; top:0; width:100px}
.school_logo img { width:100px; height:120px}
.title_header_text {  float:left; width:100%}
.title_header h1, .title_header h2 {text-align:center; font-size:13px; font-weight:normal; display:block; position:relative }
.title_header h1 span, .title_header h2 span {font-family:  "Times New Roman";}
.title_header h1 {  padding-top:30px; padding-right:30px; padding-left:130px; min-height:40px;}
.title_header h1 span { font-size:15px;letter-spacing:10px}
.title_header h2 span { font-size:15px;letter-spacing:1px}
.barcode { display:block; position:absolute; right:0; top:0; width:140px; height:65px;} 

.student_info1 { display:block; font-size:13px;float:left; width:57%}
.student_info2 { display:block; font-size:13px;float:left; width:43%}
.student_info_detail { display:block;padding-left:185px;}
.student_info_detail ul {margin:0; padding:0; }
.student_info_detail ul li{list-style-type:none; margin:0; padding:0; display:block; clear:both; padding-bottom:5px; }
.student_info_detail ul li span { display:block; float:left; margin-left:-185px; width:170px;}
.student_info_detail ul li em { font-style:normal;  text-align:center;display:block; float:left; margin-left:-15px; width:15px; padding:0;}

.record_list_table,.summary_list_table { display:block; padding-top:5px; font-weight:normal;}
.record_list_table h1, .summary_list_table h1 { margin:0; padding:2px 5px; font-size:13px;  background:#EEE; margin-bottom:2px; font-weight:normal;}
.record_list_table h1 span, .summary_list_table h1 span{ padding-right:30px; font-weight:normal; }
.record_list_table h1 em, .summary_list_table h1 em { font-size:13px;  clear:both; display:block}

.record_list_table table { *border-collapse: collapse; border-spacing: 0;width: 100%; }
.record_list_table table tr td { padding:1px 6px 1px 6px;vertical-align:top; font-weight:normal; }
.record_list_table table tr th { padding:1px 6px 1px 6px; text-align:left; font-weight:normal;} 

.col_role, .record_list_table table tr th.col_role { width:180px; text-align:center}
.col_marks, .record_list_table table tr th.col_marks  { width:40px; text-align:center}
.col_year, .record_list_table table tr th.col_year { width:100px; text-align:center}


.record_list_table table tr.others_row td { padding-bottom:10px;}
.record_list_table table tr.subtotal_row td{ border-top:2px solid #000; padding-top:10px;}
.record_list_table table tr td.personal_stat{ padding:2px 0px; font-family: "Times New Roman"; font-weight:normal; line-height:20px;}


.summary_list_table h1 { margin:0}
.summary_list_table table{ *border-collapse: collapse; border-spacing: 0;width: 100%; margin:0 auto; border-left:1px solid #333; border-top:1px solid #333; }
.summary_list_table table tr td { padding:4px 6px 4px 6px;vertical-align:top;  text-align:center; border-right:1px solid #333; border-bottom:1px solid #333; }
.summary_list_table table tr th { text-align:center; padding:1px 6px 1px 6px;  border-right:1px solid #333; border-bottom:1px solid #333; font-weight:normal;}
.summary_list_table table tr th.col_marks, .summary_list_table table tr td.col_marks {  width:90px; text-align:center}
#.summary_list_table table tr.subtotal_row td{ font-weight:normal}
.summary_list_table table tr td.sub_title{ text-align:right; font-weight:none}
.summary_list_table table tr td.sub_title span { display:block; clear:both}
.summary_list_table table tr td.col_sub_marks { width:30px; text-align:center; padding-right:0; vertical-align:middle}
.summary_list_table table tr td.col_total_marks { width:80px;  text-align:center;vertical-align:middle}
.summary_list_table table tr th.summary_top { width:30px; height:120px; position:relative}
.summary_list_table table tr th div { display:block; width:30px; margin-left:5px; height:120px; position:relative;}
.summary_list_table table tr th  div span{ display:block; position:absolute; width:115px; left:30px; bottom:5px; height:30px; text-align:left;-moz-transform-origin: left bottom;
  -webkit-transform-origin: left bottom;
  -o-transform-origin: left bottom;
  transform-origin: left bottom;
  -moz-transform:rotate(270deg);
  -webkit-transform:rotate(270deg);
  -o-transform:rotate(270deg);
  transform:rotate(270deg);}


</style>
CSS;


_handleCurrentAlumniStudentAfterPrinting("alumni", $AlumniStudentArr);

if($PrintType == "word"){
	$objReportMgr->outPutToWord($html_body);
}else{

$html_head =<<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 中華基督教會方潤華中學 SLP Report</title>

{$htmlCSS}

</head>

<body>
EOF;

	echo $html_head . $html_body . "</body></html>";

}
exit();
?>