<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/mms/libpf-slp-mmsRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/mms/libpf-slp-mms.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);
$printIssueDate = trim($printIssueDate);
$issuedate = trim($issuedate);

$objReportMgr = new mmsRptMgr();
$objReportMgr->setYearClassID($YearClassID);
$objReportMgr->setStudentID($StudentID);
$objReportMgr->setAcademicYearID($academicYearID);
//$objReportMgr->setIsPrintIssueDate($printIssueDate);
$objReportMgr->setIssueDate($issuedate);
$h_issueDate = $objReportMgr->getIssueDate();

$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplay();

$studentList = $objReportMgr->getStudentIDList();

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($academicYearID);
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setPrintIssueDate($issuedate);

	$h_studentInfo = $objStudentInfo->getStudentInfo_HTML();
	$h_studentPartA = $objStudentInfo->getPartA_HTML();	

	$pageBreakStyle = '';
	if($i != ($i_max -1) ){
		$pageBreakAttr = ' page-break-after:always; ';
	}

$html .= <<<HTML
	<div style="width:750px; {$pageBreakAttr}">
		<div align="center"><img src="/home/portfolio/profile/report/customizeReport/templates/mms/MARYMOUNT_school_title.jpg" width="500" height="50">&nbsp;</div>
		<div align="center" class="HeaderCSS">EXTRA-CURRICULAR ACTIVITIES AND AWARDS REPORT $academicYearStartEndDisplay</div>
		<table border = "0" align="center" width = "100%" class="mainTable">
		<tr><td align="center">$h_studentInfo</td></tr>
		<tr>
			<td>$h_studentPartA
				<span class="remarkCSS timesNewRoman"><b><i>* Evidence of awards / certifications / achievements listed is available for submission when required</i></b></span>
			</td>
		</tr>
		<tr>
			<td align ="center">
				<span class="ArialCSS"><b>~~  End of Report  ~~</b></span>
			</td>
		</tr>

		</table>

		<br/><br/><br/><br/><br/>
		<table border = "0" align="center" width = "70%">
			<col width= "20%" align="center" />
			<col width= "5%" align="center" />
			<col width= "30%" align="center" />
			<tr><td class="bottomline ArialCSS">$h_issueDate</td><td >&nbsp;</td><td class="bottomline">&nbsp;</td></tr>
			<tr><td>Date of Issue</td><td >&nbsp;</td><td>Parent’s/Guardian’s signature</td></tr>
		</table>
	</div>
HTML;

//	if($i != ($i_max -1) ){
////		$html .= "<p class=\"pageBreak\"></p>";
//		$html .= '<p><br class="pageBreak" clear="all" /></p>';
//	}


}

$htmlCSS =<<<CSS
		<style type="text/css">
html, body { margin: 0px; padding: 0px; }

body{
font-size: 16px;
}

br.pageBreak{
	page-break-before:always;
}


.HeaderCSS {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
}

.ArialCSS{
	font-family: Arial, Helvetica, sans-serif;
}
.remarkCSS{
	font-size: 13px;
}
.timesNewRoman {
	font-family: "Times New Roman", serif;
}
.chiSmall{
	
}
table.style1{
margin: 0em;
border-collapse: collapse;
}

td.bottomline{
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;

}
td.bottomline2{
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;

}
table.style1 td { 
		padding: .2em; border: 0px #000 solid; 
		/*FONT-SIZE: 11px;*/
} 

table.ole{
	font-family: Arial, Helvetica, sans-serif;
	border: 1px #000 solid; 
	FONT-SIZE: 13px;
}
table.ole td{
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #000000;

	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}

table.ole td.header{
	border-top-width: 1px;
}
table.section { 
	border: 2px #000 solid; 
	/*FONT-SIZE: 11px;*/
	padding: 0px; 

} 

p.pageBreak{
	page-break-after:always;
}

td.styleA {
	background-color: #cccccc; color: black;
}

table.mainTable{
//	font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	/*font-size: 11px;*/
}
table.mainTable td{padding:4px;}
</style>
CSS;


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}

?>