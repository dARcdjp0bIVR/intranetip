<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ychwws/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ychwws/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);
$NoOfFirstPage = trim($NoOfFirstPage);
$NoOfOtherPage = trim($NoOfOtherPage);

//debug_r($_POST);

//YearClassID

$objReportMgr = new RptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	
	if($NoOfFirstPage!='')
	{
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!='')
	{
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}

	$OLEInfoArr = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);

	### Page Content
//	$html .=$objStudentInfo->getEmptyTable('3%');
//	$html .= $objStudentInfo->getPart1_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('1%');
//	$html .= $objStudentInfo->getPart2_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
//	$html .= $objStudentInfo->getPart3_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr);	
	
//	$html .=$objStudentInfo->getPagebreakTable();
	$html .=$objStudentInfo->getEmptyTable('3%');
	
	$html .='<table width=100% height="925px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart5_HTML().'</td></tr>';
	$html .='</table>';

	$html .= $objStudentInfo->getSignature_HTML();
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getEmptyTable($Height='',$Content='Page '.$objStudentInfo->getPageNumberOfThisPage().' of <!--totalPage-->');	
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
	$thisTotalPage = $objStudentInfo->getPageNumberOfThisPage();
	$html = str_replace('<!--totalPage-->',$thisTotalPage,$html);
}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>