<?php
ini_set('memory_limit','128M');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/twghczm/libpf-slp-twghczm.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

//StartTimer("GLB_StartTime");

//content Ready
if(!empty($StudentID)){
	$studentList = array($StudentID);
}

$maxECAsize = $_POST['maxECAsize'];
$maxPrizesize = $_POST['maxPrizesize'];

if(empty($issuedate)){
	$issuedate = date("d/m/Y");
}
else{
	$issuedate = date("d/m/Y" , strtotime($issuedate));
}

if (!isset($studentList))
{
	// If no studentIDs retrieve from classname
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

	$studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);
}

if( !empty($studentList)){
    $academicYearID = Get_Current_Academic_Year_ID(); // need to change
    
    $objPrinting = new objPrinting(); // class from libpf-slp-css.php
    $objPrinting->setYearClassID($YearClassID);
    $objPrinting->setAcademicYear($academicYearID);
    $objPrinting->setStudentList($studentList);
    $objPrinting->contentReady();
    
    //Looping studentList
    $numStudent = count($studentList);
    $stylesheet = $objPrinting->returnStyleSheet();
    $html = '';
    
    for($y = 0; $y<$numStudent ; $y ++){
    	// Change student
    	$_studentid = $studentList[$y];
    	$objPrinting->setStudent($_studentid);
    
    	// Get All Content
    	$internalActivity_AssoArr = $objPrinting->GetOLE('INT','OLE'); //School based activity
    	$num_internalActivity = count($internalActivity_AssoArr);
    
    	$school_header_html = $objPrinting->returnSchoolHeader();
    	$student_info_html = $objPrinting->returnStudentInfo();
    	$studentAdmissionNo = $objPrinting->returnstudentAdmissionNo();
    	$remarks_html = $objPrinting->returnRemarks();
    	$student_character_html = $objPrinting->returnStudentCharacter();
//     	$firstpage_footer_html = $objPrinting->returnPageFooter(1);
    	$activities_table_html = $objPrinting->returnActivitiesTable($maxECAsize);
    	$awards_table_html = $objPrinting->returnAwardsTable($maxPrizesize);
     	$sign_table_html = $objPrinting->returnSignTable($issuedate);	
    	
    	
    	
    	if($_POST['PrintType'] == 'word'){
    		$html .= '<div style="page-break-before:always;">&nbsp;</div>';
    	}
    	$html .= '<div class="page_wrapper">';
//     	$html .= '<div style="height:95%;">';
    	if($_POST['PrintType'] == 'html'){
    		$html .= '<div class="marginDiv">&nbsp;</div>';
    	}
    	$html .= $school_header_html;
    	$html .= $student_info_html;
    	$html .= '<br/><br/>'.$remarks_html;
    	$html .= '<br/><br/><br/>'.$student_character_html;     	
    	$html .= '</div>';
    	$html .= '<div style=" text-align:center;">';
    	   $html .= $objPrinting->returnPageFooter(1);
    	$html .= '</div>';
    	if($_POST['PrintType'] == 'word'){
    	    $html .= '<div style="page-break-before:always;">&nbsp;</div>';
    	}
    	if($_POST['PrintType'] == 'html'){
    	    $html .= '<div class="secondpage">';
    	}else{
    	    $html .= '<div class="page_wrapper">';
    	}
    
//     	$html .= '<div style="height:98%;">';
    	$html .= $studentAdmissionNo;
    	$html .= $activities_table_html;
    	$html .= $awards_table_html;
    	$html .= '<br/><br/><br/>'.$sign_table_html;

    	$html .= '</div>';
     	$html .= '<div style="text-align:center;">';
     	  $html .= $objPrinting->returnPageFooter(2);
     	$html .= '</div>';
//     	$html .= '</div>';
    //	$footer = '<div class="footercenter">P.{PAGENO}/{nbpg}</div>';
    
    //	if($_POST['PrintType'] == 'word' && $y!=($numStudent-1) ){
    //		$html .= '<br>';
    //		$html .= '<span style="page-break-before:always">&nbsp;</span>';
    //	}
    }
    
    switch($_POST['PrintType']){
    	case 'html':
    	    $output = $stylesheet.$html;
    	break;
    	case 'word':
    		$report_html = $html;
    //		debug_pr($stylesheet.$html);
    //		die();
    		$ContentType = "application/x-msword";
    	  	$filename = "export_word_file".date("ymd").".doc";
    	  	$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
    	  	foreach ($tags_to_strip as $tag)
    	  	{
    	  		$report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_html);
    	  	}
    	  	
    	  	# Eric Yip (20091008): Replace official photo link
    //	  	$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/official_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
    //		$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/user_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
    //		$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/portfolio.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
    		//$output = "<HTML>\n";
    	  	$output = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
    	  	$output .= "<HEAD>\n";
    
    
    	  	$output .= returnHtmlMETA();
    	  	$output .= $stylesheet;
    	  	$output .= "<style>\n";
    	  	$output .= "div.page_wrapper{page:page_wrapper;}\n";
    
    	  	$output .= "@page page_wrapper{	margin:36.0pt 36.0pt 36.0pt 36.0pt;}\n";
    	  	$output .= "</style>\n";
    	  	$output .= "</HEAD>\n";
    	  	$output .= "<BODY LANG=\"zh-HK\">\n";
    	  	$output .= $report_html;
    	  	$output .= "</BODY>\n";
    	  	$output .= "</HTML>\n";
    	  	
    	  	header('Pragma: public');
    	  	header('Expires: 0');
    	  	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    	  
    	  	header('Content-type: '.$ContentType);
    	  	header('Content-Length: '.strlen($output));
    	  
    	  	header('Content-Disposition: attachment; filename="'.$filename.'";');
    	break;	
    }
    
    echo $output;
}
intranet_closedb();
?>