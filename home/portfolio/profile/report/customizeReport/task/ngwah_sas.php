<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

$linterface = new interface_html();
$li = new libportfolio();

# from Student Account
if($fromStudent){
	$StudentID = $_SESSION['UserID'];
	$academicYearID[0] = Get_Current_Academic_Year_ID();
	$PrintType = 'pdf';
}

# Academic Year
$academicYear = $academicYearID[0];
$targetYear = getAcademicYearByAcademicYearID($academicYearID[0]);

# Print Date
$issuedate = date("d-m-Y");

# Sql Cond - Student
$studentCond = '';
if($StudentID)
{
	$studentCond .= " AND ycu.UserID = '$StudentID' ";
}
else
{	
	# Get All Students in Class
	$student_ids = Get_Array_By_Key(GET_CLASS_TEACHER_STUDENT($YearClassID), 'UserID');
	$studentCond .= " AND ycu.UserID IN ('".implode("','", $student_ids)."') ";
}

# Student
$sql = "SELECT 
			iu.UserID,
			iu.ChineseName,
			iu.EnglishName,
			yc.ClassTitleEN,
			ycu.ClassNumber,
			yc.YearClassID
		FROM INTRANET_USER as iu 
		LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID 
		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$academicYear')
		LEFT JOIN YEAR as y ON y.YearID = yc.YearID 
		WHERE yc.AcademicYearID = '".$academicYear."' 
			$studentCond 
		GROUP BY iu.UserID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";	
		
$students = $li->returnResultSet($sql);
$student_counts = count($students);

# Class Teacher
$sql = "SELECT 
			yct.UserID, 
			CASE 
				WHEN au.UserID IS NOT NULL then au.ChineseName
				ELSE u.ChineseName
			END as TeacherName,
			YearClassID
			FROM YEAR_CLASS_TEACHER as yct 
			LEFT JOIN INTRANET_USER as u ON (yct.UserID = u.UserID)
			LEFT JOIN INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID) 
			WHERE YearClassID = '".$students[0]['YearClassID']."' ";
			
$class_teachers = $li->returnArray($sql);
$class_teacher_str = (Get_Array_By_Key($class_teachers, 'TeacherName'));
$class_teacher_str = implode(", ", $class_teacher_str);

# PDF
if($PrintType == 'pdf'){
	
//	StartTimer($variableName="init");

	require_once($PATH_WRT_ROOT.'includes/tcpdf/tcpdf.php');
		
	# Extend the TCPDF class to create custom Header and Footer
	class MYPDF extends TCPDF {
			
		public function __construct() {
			parent::__construct();
		} 
	
	    # Page header
	    public function Header() {
	    	global $PATH_WRT_ROOT;
			// no header
	    }
	
	    # Page footer
	    public function Footer() {
	    	global $PATH_WRT_ROOT;
	    	// no footer
		}
	}
	
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
//	$pdf->SetCreator(PDF_CREATOR);
//	$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
//	$pdf->SetTitle('培橋中學 SLP');
//	$pdf->SetSubject('Student Learning Portfolio');
//	$pdf->SetKeywords('培橋中學', 'eClass, iPortfolio, SLP');
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
//	$pdf->setFontSubsetting(false);

	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
	
	//set some language-dependent strings
	$pdf->setLanguageArray($l); 

//	debug_pr("Init:");
//	debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="init"));	

}

$x = '';
for($i=0; $i<$student_counts; $i++){
	$current_student = $students[$i];
	$disable_page_break = false;
	
	# Initiate
	$studentObj = new studentInfo($current_student['UserID'], $academicYear);
	
	# OLE Records
	$studentObj->getStudentSAS($set=true);
	
	# SAS Settings
	$sas_setting = $studentObj->getSASSettings('', $set=true);
	
	# Set Print Type
	$studentObj->setPrintType($PrintType);
	
	# Report Printing Selection
	switch($PrintType)
	{
		case "html":
			
			if($i == ($student_counts - 1)){
				$disable_page_break = true;
			}
			
			# Report Header
			$studentObj->setReportHeader($targetYear);
			
			# Report Student Info
			$studentObj->setReportStudent($current_student, $issuedate);
			
			# Report OLE SAS Records
			// A) 認可職銜 Elected/Appointed Post
			$studentObj->setReportRecords(1);
			// B) 獎項成就 Achievement
			$studentObj->setReportRecords(2);
			// C) 校內傑出表現 Outstanding Performances (Inside School)
			$studentObj->setReportRecords(3);
			// D) 校外傑出表現 Outstanding Performances (Outside School)
			$studentObj->setReportRecords(4);
			// E) 服務 Services
			$studentObj->setReportRecords(5);
			// F) 其他學習活動 Other Learning Activities
			$studentObj->setReportRecords(6);
			
			# Report Summary
			$studentObj->setReportSummary($current_student, $class_teacher_str, $disable_page_break);
			
			$x .= $studentObj->returnHTML();
			
		break;
		
		case "pdf":

//			StartTimer($variableName="whole");

			$pdf->AddPage();
			
//			StartTimer($variableName="header");

			$pdf->SetY(26);
			$pdf->SetFont('arialunicid0', '', 19);
			$pdf->Cell(0, 0, '天主教伍華中學', 0, 1, 'C', 0, '', 0);
			
			$pdf->SetXY(25, 35);
			$pdf->SetFont('arialunicid0', '', 15);
			$pdf->Cell(0, 0, "學生課外活動及獎勵紀錄表 $targetYear", 0, 1, 'C', 0, '', 0);
			    
			# School Logo
			$pdf->SetXY(17, 23);
			$pdf->Image($PATH_WRT_ROOT.'home/portfolio/profile/report/customizeReport/templates/ngwah/ngwah_logo.jpg', '', '', 17, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

//			debug_pr("Header:");
//			debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="header"));	

//			StartTimer($variableName="student_info");

			$pdf->SetY(49);
			$pdf->SetFont('arialunicid0', '', 11);
			
			$studentObj->emptyHtml();
			
			$studentObj->setReportStudent($current_student, $issuedate);
			$html = $studentObj->returnHTML();
			
			$pdf->writeHTML($html, true, false, true, false, '');
			
//			debug_pr("Student Info:");
//			debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="student_info"));

//			StartTimer($variableName="records");

			$studentObj->emptyHtml();
			$studentObj->resetCredit();
			
			$pdf->SetY(60);
			$pdf->SetFont('arialunicid0', '', 9.5);

			# Report OLE SAS Records
			// A) 認可職銜 Elected/Appointed Post
			$studentObj->setReportRecords(1);
			// B) 獎項成就 Achievement
			$studentObj->setReportRecords(2);
			// C) 校內傑出表現 Outstanding Performances (Inside School)
			$studentObj->setReportRecords(3);
			// D) 校外傑出表現 Outstanding Performances (Outside School)
			$studentObj->setReportRecords(4);
			// E) 服務 Services
			$studentObj->setReportRecords(5);
			// F) 其他學習活動 Other Learning Activities
			$studentObj->setReportRecords(6);
			
			$html = $studentObj->returnHTML();		
			
			$pdf->writeHTML($html, true, false, true, false, '');	
			
//			debug_pr("Record:");
//			debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="records"));

//			StartTimer($variableName="summary");

			$studentObj->emptyHtml();
			
			$pdf->SetFont('arialunicid0', '', 11);
			
			# Report Summary
			$studentObj->setReportSummary($current_student, $class_teacher_str);
			
			$html = $studentObj->returnHTML();
			
			$pdf->writeHTML($html, true, false, true, false, '');	
			
//			debug_pr("Summary:");
//			debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="summary"));

//			debug_pr("Whole Page:");
//			debug_pr(StopTimer($precision=5, $NoNumFormat=false, $variableName="whole"));

//			die();

		break;
	}
	unset($studentObj);
}

if($PrintType == 'pdf'){
	
	$pdf->lastPage();
	$pdf->Output('SAS_Report.pdf', 'I');
	
	die();
	
} else {
	
	$output = "<html>\n";
    $output .= "<head>\n";
    $output .= returnHtmlMETA()."\n";
    # compulsory use of IE10 for good page breaks 
    $output .= "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE10\" />\n";
    $output .= "</head>\n";
    $output .= "<body>\n";
    $output .= $x."\n";
    $output .= "</body>\n";
    $output .= "</html>\n";
	echo $output;
}
?>

<style type="text/css">
@charset "utf-8";

/* CSS Document */

 /* Report */
body { font-family: "Times New Roman, 新細明體"; margin:0; padding:0; }
html, body { margin: 0px; padding: 0px; } 

.main_container { margin:auto; padding:34px 38px; padding-bottom:75px; height:900px; width:680px; display:block; position:relative; }
.page_break { page-break-after:always; margin:0; padding:0; height:0; line-height:0; font-size:0; }
.page_break_headers { height:60px; position:relative; }
.report_seperator { padding-top:9px; padding-bottom:4px; }

/* Report Header */
.report_header { clear:both; margin-top:55px; margin-bottom:10px; height:75px; position:relative; vertical-align:center; }

.report_header td { vertical-align:top; }
.report_header .sch_logo_td { padding-left:10px; width:81px; align:center; }
.report_header .sch_logo_td img { height:75px; }

.report_header h2 { font-family:"Times New Roman, 新細明體"; font-size:18px; font-weight:normal; padding-top:2px; padding-bottom:4px; margin-left:-60px; position:relative; text-align:center;}
.report_header .chi_sch_name { font-size:22px; letter-spacing: 6px; margin-top:0px; margin-bottom:0px; }

/* Student Info */
.student_info { float:left; padding-bottom:2px; }
.student_info td { font-family: "Times New Roman, 新細明體"; font-size:14px; padding-bottom:2px; align:left; }

/* Record Table */
.record_table_header { clear:both; font-family: "Times New Roman, 新細明體"; font-size:13px; font-weight:bold; margin-top:4px; margin-bottom:1px; padding-top:2px; padding-bottom:2px; border-top: 1px solid #000000; border-bottom: 1px solid #000000; }
.record_table { font-family: "Times New Roman, 新細明體"; font-size:11px; } 
.record_table td { padding-bottom:4px; } 
.record_table_summary { font-family: "Times New Roman, 新細明體"; font-size:11px; padding-bottom:8px; }

/* Summary Table */
.summary_table td { font-family: "Times New Roman, 新細明體"; font-size:13px; padding-top:2px; padding-bottom:9px; }

</style>

