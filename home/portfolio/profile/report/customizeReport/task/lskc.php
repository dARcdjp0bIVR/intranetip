<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lskc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lskc/libpf-slp.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);




$objReportMgr = new RptMgr();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);

	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true);
	//$objStudentInfo->setPageNumber(2);


	$html .='<style type="text/css">
			@media print {
			   thead { display: table-header-group; }
			   tfoot { display: table-footer-group; }
			}
			</style>';
	### Page Content
	$html .= '<table width="100%" height="800px">';
	$html .= '<tr><td style="vertical-align:top">';
	$html .= $objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart2_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart3_HTML();
	
	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart4_HTML();
	$html .=$objStudentInfo->getEmptyTable('2%');
	
	$html .=$objStudentInfo->getPart5_HTML($OLEInfoArr_INT);
	$html .='</td></tr>';
	$html .='</table>';
//	$html .= $objStudentInfo->getEmptyTable('1%',$objStudentInfo->getPageNum());
	$html .= $objStudentInfo->getPagebreakTable();
	$objStudentInfo->addPageNum();
	
	$thisNextPageOLE_html = $objStudentInfo-> getNextPageOLE_html();
	
	$html .= '<table width="100%" height="830px">';
	$html .='<thead>';
	$html .='<tr height="100px">';
	$html .='<td>';
	$html .=$objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .= $objStudentInfo->getPart2_HTML();
	$html .='</td>';
	$html .='</tr>';
	$html .='</thead>';
	$html .='<tbody><tr><td style="vertical-align:top">';
	
	if($thisNextPageOLE_html!='')
	{
		$html .= '<table width="100%" >';
		$html .= '<tr><td style="vertical-align:top">';
		$html .=$thisNextPageOLE_html;
		$html .='</td></tr>';
		$html .='</table>';
		$html .=$objStudentInfo->getEmptyTable('1%');
//		$html .= $objStudentInfo->getEmptyTable('1%',$objStudentInfo->getPageNum());
//		$html .= $objStudentInfo->getPagebreakTable();
//		$objStudentInfo->addPageNum();
	}	
	

	$html .= $objStudentInfo->getPart6_HTML();
	$html .=$objStudentInfo->getEmptyTable('100px');
	$html .= $objStudentInfo->getPart7_HTML();	
	$html .='</td></tr></tbody>';
	
	$html .='<tfoot><tr><td>';
//	$html .= $objStudentInfo->getEmptyTable('1%',$objStudentInfo->getPageNum());
	$objStudentInfo->addPageNum();
	$html .='</td></tr></tfoot>';
	$html .='</table>';
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}

if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>