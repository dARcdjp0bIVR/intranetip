<?
ini_set('memory_limit','256M');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/sptss/libpf-slp-sptss.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$reportType = $_POST['recordType_sptss'];// 1 = Form 1-3 ; 2 = Form 4-6;

// issue date
if(empty($issuedate)){
	$tempIssueDate = date("Y-m-d");
	$issuedate = date("d-M-Y");
}
else{
	$tempIssueDate = date("Y-m-d" , strtotime($issuedate));
	$issuedate = date("d-M-Y", strtotime($issuedate));
}
$issueAYInfo = getAcademicYearAndYearTermByDate($tempIssueDate);
$issueAYName = $issueAYInfo['AcademicYearName'];

$task = $_POST['task'];
$task_num = substr($task,-1);

// target student
if(!empty($StudentID)){
	$studentList = array($StudentID);
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser($StudentID);
	$luser->LoadUserData($StudentID);
	$className = $luser->ClassName;
	$classNumber = $luser->ClassNumber;	
	unset($luser);
	$filename = $issueAYName.'_'.$className.'-'.$classNumber.'_'.$task_num;
}
else{
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	$lclass = new libclass();
	$className = $lclass->getClassName($_POST['YearClassID']);
	unset($lclass);
	$filename = $issueAYName.'_'.$className.'_'.$task_num;
}

if (!isset($studentList))
{
	// If no studentIDs retrieve from classname
	$objReportMgr = new ipfReportManager();
	$objReportMgr->setStudentID($StudentID);
	$objReportMgr->setYearClassID($YearClassID);
	$studentList = $objReportMgr->getStudentIDList($ActivatedOnly=true);
}
$numStudent = count($studentList);
# 2020-03-24 (Philips) [2020-0323-1201-47235] Prevent fatal error from memory shortage
if($numStudent > 20){
	ini_set('memory_limit','512M');
}
if($numStudent>0){
	$objPrinting = new objPrinting($_POST['task'],$studentList,$academicYearID,$YearTermID,$reportType); // class from libpf-slp-sptss.php
	
	//start creating PDF
	//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
	$margin_left = 19; //mm
	$margin_right = 19;
	$margin_top = 86;
	$margin_bottom = 35;
	$margin_header = 46;
	$margin_footer = 20;
	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	
	// Chinese use mingliu 
	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;
	
	// header footer style		
	$header = $objPrinting->returnHeader();
	$footer = $objPrinting->returnFooter();
	$pdf->DefHTMLFooterByName('Footer',$footer);	
	$pdf->SetHTMLFooterByName('Footer');
	$style = $objPrinting->returnStyleSheet();
	
	$numStudent = count($studentList);
	// Def Header Here
	for($y = 0; $y<$numStudent ; $y ++){
		// Change student
		$_studentid = $studentList[$y];
		$objPrinting->setStudent($_studentid);
		$stuInfoHTML = $objPrinting->returnStudentInfoTable();
		$pdf->DefHTMLHeaderByName("StudentHeader_$y",$header.$stuInfoHTML);
	}
	// Gen PDF here
	for($y = 0; $y<$numStudent ; $y ++){
	
		// Change student
		$_studentid = $studentList[$y];
		$objPrinting->setStudent($_studentid);
		$pdf->SetHTMLHeaderByName("StudentHeader_$y");
		if($y == 0){
			$pdf->WriteHTML($style);		
		}
		
		//HTML
		$stuMeritHTML = $objPrinting->returnStudent3YearMeritTable();
			
		// write into PDF
		$pdf->WriteHTML($stuMeritHTML);
	//	$pdf->WriteHTML($objPrinting->returnSpacing());
		$pdf->WriteHTML($objPrinting->returnEndSection());
		
		// Last Page no need to add pagebreak
		if($y != ($numStudent-1)){
			$pdf->SetHTMLHeaderByName("StudentHeader_".($y+1));
			$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
		}
	}
	
	$pdf->Output($filename.'.pdf', 'I');
}
?>