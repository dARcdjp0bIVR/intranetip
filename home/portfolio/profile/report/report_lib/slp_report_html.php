<?php
/** [Modification Log] Modifying By:  Isaac
 * *******************************************
 * 2017-11-1 Isaac
 * added <br clear=all style='page-break-before:always'> inside the footer to control page break in word.
 * 
 * 2017-10-30 Isaac
 * - added css "outline" for table inner borders, added thead to control table inner borders in both HTML and Word output
 * 
 * 2013-05-30 Yuen
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT (OLE, OutsideSchool, awards, self-account and performance/assessments) to improve page-break handling (IE only!)
 * 
 * 2012-02-17 Ivan
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT, added para $displayFullMark to add show / hide full mark display logic for SLP report
 * 
 * 2012-06-16 Connie
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT
 * 
 * 2012-02-09 Ivan [CRM:2012-0209-1132-12073]
 * - modified GEN_OLE_TABLE_SLP, GEN_EXTPERFORM_TABLE_SLP, align table header to center (except the first column)
 * 
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP, GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF, subject ordering follows the School Settings now
 * 
 * 2010-06-04 Max (201006041431)
 * - Add page break settings and SET_MODULE_PAGE_BREAK_SETTING()
 * *******************************************
 */
include_once($intranet_root."/includes/portfolio25/lib-portfolio_settings.php");


class slp_report_html extends slp_report{ 
		
  var $slp_style;
  
  function __construct(){
    global $lpf_slp, $sys_custom;
  
    parent::__construct();
    
		// Page Break Style
//		$page_break_style = "<STYLE TYPE='text/css'>\nP.breakhere {page-break-before: always}\n</STYLE>\n";
//		$page_break_here = "<p class='breakhere'></p>\n";
  
    # Table style
		$SLP_Style['report_header'] = array("padding:3px", "font-size:13pt", "font-weight:bold", "font-family:Arial");
		$SLP_Style['module_title'] = array("color:#FFFFFF", "padding:3px", "font-size:12pt", "font-weight:bold", "font-family:Arial");
		$SLP_Style['padding_all'] = array("padding:3px");
		
		if ($sys_custom['iPf']['SLP_Report']['DisplayBorderInHtmlMode']) {
		    #IE and Edge only
		    if((!strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') 
		        &&  !strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Opera')) || strpos($_SERVER['HTTP_USER_AGENT'], 'Edge')){
		        $SLP_Style['ole_content'] = array("border: 1px solid");
		        $SLP_Style['ole_head_sub'] = array("border: 1px solid");
		        $SLP_Style['acadamic_t_boarder'] = array("border: 1px solid");
		        $SLP_Style['award_t_boarder'] = array("border: 1px solid");
		    } else {
		    #Other broswer
		      $SLP_Style['ole_content'] = array("outline: 1px solid");
		      $SLP_Style['ole_head_sub'] = array("outline: 1px solid");
		      $SLP_Style['acadamic_t_boarder'] = array("outline: 1px solid");
		      $SLP_Style['award_t_boarder'] = array("outline: 1px solid");
		    }
		}
		
		
		# Add style for customarized report
		switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
		{
			case "LaSalle":
				$SLP_Style['report_header'] = array("padding:3px", "font-size:20pt", "font-family:Arial");
				$SLP_Style['report_subheader'] = array("padding:3px", "font-size:14pt", "font-family:Arial");
				$SLP_Style['border_table'] = array("border-style:solid", "border-color:#000000", "border-width:thin");
				$SLP_Style['cell_font_arial'] = array("font-size:10pt", "font-family:arial");
				$SLP_Style['cell_font_times'] = array("font-size:11pt", "font-family:times");
				$SLP_Style['bottom_border'] = array("border-bottom-color:#000000", "border-bottom-width:thin");

				break;
		}
		
		$SLP_style_to_class = "<STYLE TYPE='text/css'>\n";
		foreach($SLP_Style as $class_name => $class_property)
		{
			$SLP_style_to_class .= ".".$class_name." {\n".implode(";\n",$class_property)."\n}\n\n";
		}
		$SLP_style_to_class .= "</STYLE>";
		
		$this->setStyle($SLP_style_to_class);
  }

  function setStyle($ParStyleStr){
    $this->slp_style = $ParStyleStr;
  }
  
  function getStyle(){
    return $this->slp_style;
  }
  
	# Generate Student Learning Profile for Printing
	function GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($ParStudentID, $ParIssueDate="", $ParYear="",$ParPrintIssueDate ='', $IsZip=0, $academicScoreDisplayMode='', $displayFullMark=true, $displayComponentSubject=false)
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName, $eclass_root, $sys_custom, $ipf_cfg;
		
		$academicScoreDisplayMode = ($academicScoreDisplayMode=='')? $ipf_cfg['slpAcademicResultPrintMode_default'] : $academicScoreDisplayMode;
		
		$ipf_setting = new iportfolio_settings();
		$ShowSchoolImage = $ipf_setting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"]);

	    $this->SET_MODULE_ORDER();
	    $this->SET_MODULE_PAGE_BREAK_SETTING();
	    $this->SET_DETAIL_SETTINGS();
	    $this->setPrintFormat('html');
	    
	    $ReturnStr = $this->getStyle();
	    
	    
		# style to control page breaks automatically
		$ReturnStr000 .= "
<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
  .table_pb { page-break-after:auto }
  .table_pb tr    { page-break-inside:avoid; page-break-after:auto;}
  .table_pb td    { page-break-inside:avoid; page-break-after:auto;  }
  .table_pb thead { display:table-header-group }
  .table_pb tfoot { display:table-footer-group }
  
  .table_no_page_break000 { page-break-after:auto }
  .table_no_page_break000 tr    { page-break-inside:auto; page-break-after:auto;}
  .table_no_page_break000 td    { page-break-inside:auto; page-break-after:auto;}
  .table_no_page_break000 thead { display:table-header-group }
  .table_no_page_break000 tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  }
}

</style>";


		$ReturnStr .= "
<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 

.table_no_page_break000 { page-break-inside: avoid; display:block; } 

.auto_page_break { page-break-inside: avoid; display:block; } 

</style>";
		$SchooImage_html = $this->GEN_REPORT_HEADER_SLP_IMAGE($ParIssueDate,$ParPrintIssueDate);
		$sizeOfStudentID = count($ParStudentID);
		# Generate Main Content
		for($i=0; $i<$sizeOfStudentID; $i++)
		{
		    $StudentID = $ParStudentID[$i];
		    $this->setReportStudentId($StudentID);
		    
		    $SPArr = $this->GET_SPArr($StudentID);
		    $SubjArr = $this->GET_SubjArr();
		    $YearArr = $this->GET_YearArr();
		    $APArr = $this->GET_APArr($StudentID);
		    $OLEArr = $this->GET_OLEArr($StudentID);
		    $ActivityArr = $this->GET_ActivityArr($StudentID);
		    $AwardArr = $this->GET_AwardArr($StudentID);
		    $ExtPerformArr = $this->GET_ExtPerformArr($StudentID);
		    $SelfAccountArr = $this->GET_SelfAccountArr($StudentID);
		    $SubjectInfoOrderedArr = $this->GET_SubjectInfoOrderedArr();
		    $SubjectFullMarkArr = $this->GET_SubjectFullMarkArr();
		    
		    # handling the last page break problem
		    //$PAGE_BREAK_AFTER = ($i==$sizeOfStudentID-1) ? false : true;
		    $PAGE_BREAK_AFTER = false;
		    $PAGE_BREAK_BEFORE = ($i==0) ? false : true;
		    //debug($PAGE_BREAK_AFTER);
		    if($ShowSchoolImage && $SchooImage_html!='')
		    {
		        if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')){
		            $PageBreakBefore = "";
		        
		        } else {
		            $PageBreakBefore = "page-break-before:always;";		       
		        }    
		        $ReturnStr .=	($PAGE_BREAK_BEFORE) ? str_replace("StyleLater", "style=\"$PageBreakBefore\"", $SchooImage_html) : $SchooImage_html;
		        
		    }
		    else
		    {
		        $ReturnStr .=	$this->GEN_REPORT_HEADER_SLP($ParIssueDate, $SPArr['SchoolAddress'], $ParYear,$PAGE_BREAK_AFTER, $ParPrintIssueDate, $PAGE_BREAK_BEFORE);
		    }
		    
		    
		    //000$ReturnStr .= "<table cellpadding='0' border='0' cellspacing='0' width='100%' class='table_pb000' >\n";
		    
		    for($j=0; $j<count($this->controlOrder); $j++)
		    {
		        $module_name = $this->ModuleTitle[$this->controlOrder[$j]];
		        $show = $this->controlShow[$this->controlOrder[$j]];
		        
		        $isPageBreak = $this->pageBreakSetting[$this->controlOrder[$j]]["IS_SET_PAGE_BREAK"];
		        //				echo "controlOrder->".$this->controlOrder[$j].": modulename->".$module_name.": pagebreak->".$isPageBreak."<hr/>";
		        
		        $_pageBreakTag = '';
		        if ($show && $isPageBreak) {
		            $_pageBreakTag =	" clear='all' style='page-break-after:always' ";
		            $ReturnStr .=	"<div ".$_pageBreakTag."></div>";  //000
		        }
		        
		        //000$ReturnStr .=	"<tr><td valign='top'><div".$_pageBreakTag."></div>";
		        $ReturnStr .=	"<div class='auto_page_break'>";
		        switch($module_name)
		        {
		            # Student Particulars
		            case "student_particular":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP($SPArr, $this->detail_settings[$this->controlOrder[$j]]);
		                    //000$ReturnStr .= "<br/><br/>\n";
		                }
		                break;
		                
		                # Academic Performance
		            case "academic_performance":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr, $SubjectFullMarkArr, $academicScoreDisplayMode, $displayFullMark, $ParEmptySymbol=null, $displayComponentSubject);
		                    //$ReturnStr .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr);
		                    // 							$ReturnStr .= "xxxxxx\n";
		                    
		                    //000$ReturnStr .= "<br/>1<br/>2\n";
		                }
		                break;
		                
		                # OLE
		            case "ole":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_OLE_TABLE_SLP($OLEArr, $StudentID);
		                    //000$ReturnStr .= "<br/><br/>\n";
		                }
		                break;
		                
		                # Extra-curricular activity, Customization for lasalle
		            case "activity":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_ACTIVITY_TABLE_SLP($ActivityArr);
		                    //000$ReturnStr .= "<br/><br/>\n";
		                }
		                break;
		                
		                # Awards and Achievements in school
		            case "award_in_school":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP($AwardArr);
		                    //000$ReturnStr .= "<br/><br/>\n";
		                }
		                break;
		                
		                # Awards and Achievements outside school
		            case "award_outside_school":
		                if($show)
		                {
		                    //$ReturnStr .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL($StudentID, convertSystemYearToOLEYear($ParYear));
		                    $ReturnStr .= $this->GEN_EXTPERFORM_TABLE_SLP($ExtPerformArr, $StudentID);
		                    //000$ReturnStr .= "<br/><br/><br/>\n";
		                }
		                break;
		                
		                # Self Account
		            case "self_account":
		                if($show)
		                {
		                    $ReturnStr .= $this->GEN_SELF_ACCOUNT_TABLE_SLP($SelfAccountArr);
		                    //000$ReturnStr .= "<br/><br/>\n";
		                }
		                break;
		        }
		        $ReturnStr .=	"</div><br/><br/>\n";
		        //000$ReturnStr .=	"</td></tr>";
		    }
		    
		    //000$ReturnStr .=	"<tr><td>".$this->GEN_REPORT_FOOTER_SLP()."</td></tr>\n";
		    $ReturnStr .=	"<center>".$this->GEN_REPORT_FOOTER_SLP()."<br  style='page-break-before:always'>\n</center>\n";
		    //000$ReturnStr .= "</table>";
		    /* already handled in header
		     if ($i<$sizeOfStudentID-1)
		     {
		     $ReturnStr .= "<div clear='all' style='page-break-before:always'></div>\n";
		     }
		     */
		}//END OF EACH STDUENT
		
		return $ReturnStr;
	}
	
	# St.Antonious Girls College Customization
	function GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT_SAGC($ParStudentID, $ParIssueDate="", $ParYear="",$ParPrintIssueDate ='', $IsZip=0, $academicScoreDisplayMode='', $displaySelfAccount=true)
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName, $eclass_root, $sys_custom, $ipf_cfg;
		
		$ipf_setting = new iportfolio_settings();
		$ShowSchoolImage = $ipf_setting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"]);

	    $this->SET_MODULE_ORDER();
	    $this->SET_MODULE_PAGE_BREAK_SETTING();
	    $this->SET_DETAIL_SETTINGS();
	    $this->setPrintFormat('html');
	    
	    $ReturnStr = $this->getStyle();
		$SchooImage_html = $this->GEN_REPORT_HEADER_SLP_IMAGE($ParIssueDate,$ParPrintIssueDate);
		$sizeOfStudentID = count($ParStudentID);
		
		//set module display
		$this->ModuleTitle = array();
		$this->ModuleTitle[] = "student_particular";
		$this->ModuleTitle[] = "ole";
		$this->ModuleTitle[] = "award_in_school";
		$this->ModuleTitle[] = "award_outside_school";
		$this->ModuleTitle[] = "self_account";
		$this->controlOrder = array(0,1,2,3,4);
		
		# Generate Main Content
		for($i=0; $i<$sizeOfStudentID; $i++)
		{
			$StudentID = $ParStudentID[$i];
			$this->setReportStudentId($StudentID);
			
			$SPArr = $this->GET_SPArr($StudentID);
			$OLEArr = $this->GET_OLEArr($StudentID);
			$AwardArr = $this->GET_AwardArr($StudentID);
			$ExtPerformArr = $this->GET_ExtPerformArr($StudentID);
			$SelfAccountArr = $this->GET_SelfAccountArr($StudentID);
			
			# handling the last page break problem
			$PAGE_BREAK_AFTER = $i==0 ? false : true;
			
			$_pageBreakTag = '';
			if($PAGE_BREAK_AFTER){
				$_pageBreakTag = " clear='all' style='page-break-before:always' ";
			}
			
			$ReturnStr .= "<div $_pageBreakTag><table class='stdDiv'><tr><td valign='top' class='detailTD'>"; 
			
			if($ShowSchoolImage && $SchooImage_html!='')
			{
				$ReturnStr .=	$SchooImage_html;
			}
			else
			{
				$ReturnStr .=	$this->GEN_REPORT_HEADER_SLP($ParIssueDate, $SPArr['SchoolAddress'], $ParYear, $PAGE_BREAK_AFTER,$ParPrintIssueDate);
			}
		
		
			for($j=0; $j<count($this->controlOrder); $j++)
			{	
				$module_name = $this->ModuleTitle[$this->controlOrder[$j]];
				
//				$isPageBreak = $this->pageBreakSetting[$this->controlOrder[$j]]["IS_SET_PAGE_BREAK"];
//				echo "controlOrder->".$this->controlOrder[$j].": modulename->".$module_name.": pagebreak->".$isPageBreak."<hr/>";
				
				
				//$ReturnStr .=	"<tr><td valign='top'>";

				switch($module_name)
				{
					# Student Particulars
					case "student_particular":
						$ReturnStr .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP_SAGC($SPArr, $this->detail_settings[$this->controlOrder[$j]]);
						$ReturnStr .= "<br/><br/>\n";
					break;
					
					# OLE
					case "ole":
						$ReturnStr .= $this->GEN_OLE_TABLE_SLP($OLEArr, $StudentID);
						$ReturnStr .= "<br/><br/>\n";
					break;
						
					
					# Awards and Achievements in school
					case "award_in_school":
						$ReturnStr .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP($AwardArr);
						$ReturnStr .= "<br/><br/>\n";
					break;
						
					# Awards and Achievements outside school
					case "award_outside_school":
						//$ReturnStr .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL($StudentID, convertSystemYearToOLEYear($ParYear));
						$ReturnStr .= $this->GEN_EXTPERFORM_TABLE_SLP($ExtPerformArr, $StudentID);
						$ReturnStr .= "<br/><br/>\n";
					break;
											
					# Self Account
					case "self_account":
						if($displaySelfAccount){
							$ReturnStr .= $this->GEN_SELF_ACCOUNT_TABLE_SLP($SelfAccountArr);
							$ReturnStr .= "<br/><br/>\n";
						}
					break;
				}
        		//$ReturnStr .=	"</td></tr>";
			}			

			$ReturnStr .= '</td></tr>
							<tr><td class="signatureSpace">
							</td></tr>
							<tr><td class="signatureLine">
								<table width="100%" align="center">
								<tr>	
									<td style="height:24px" align="center">____________________________________</td>	
									<td align="center">____________________________________</td>
									<td align="center">____________________________________</td>
								</tr>
								<tr>	
									<td style="height:24px" align="center">Principal 校長</td>	
									<td align="center">Class Teacher 班主任</td>	
									<td align="center">School Chop 校印</td>
								</tr>
								</table>
							</td></tr>
							</table>
							</div>'; 
		//	$ReturnStr .=	$this->GEN_REPORT_FOOTER_SLP();
		}//END OF EACH STDUENT
		
    	return $ReturnStr;
	}
	
	
	
	
	
	
	# Generate OLE table
	function GEN_OLE_TABLE_SLP($ParOLEArr="", $StudentID="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp, $sys_custom;
		
		$emptyRecTable = <<<HTML
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td height='100' valign='middle' align='center'>--</td>
	</tr>
</table>		
HTML;

//debug($this->SLP_module_frame);
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParOLEArr) && !empty($ParOLEArr))
			{
        list($result, $NumRecordAllowed, $IsPrintAll) = $this->GET_PRINTABLE_OLE_RECORD($ParOLEArr, $StudentID, 0);

  		  $style_general = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "font-size=8pt;" : "";
  		  $style_detail = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "{$style_general} font-style:italic;" : "";
  		  $style_title_align_general = "text-align:center;";


		  if($sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'] != ''){			
			$style_title_align_general = "text-align:".$sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'].";";
		  }

        	$t_program_title = $this->BiLangOut("['ProgrammesWithDescription']");
		    $t_program_title .= $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? $this->BiLangOut("['SelectedRecords']","","","V","B5","EN") : "";

        $ele_arr = $lpf_slp->GET_ELE();
        $ele_code_arr = array_keys($ele_arr);
        $ele_title_arr = array_values($ele_arr);
        
        if(count($result) > 0)
        {
  				$ContentStr =	"<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' class='table_pb table_print'>\n";
	          $ContentStr .= "<thead><tr style='border-top:1px solid #555555;'>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} font-weight:bold;' width='35%'>{$t_program_title}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_align_general} font-weight:bold;' width='10%'>{$this->BiLangOut("['SchoolYear']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_align_general} font-weight:bold;' width='10%'>{$this->BiLangOut("['RoleOfParticipation']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_align_general} font-weight:bold;' width='15%'>{$this->BiLangOut("['PartnerOrganizationsIfAny']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_align_general} font-weight:bold;' width='15%'>{$this->BiLangOut("['EssentialLearningExperiences']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_align_general} font-weight:bold;' width='15%'>{$this->BiLangOut("['AchievementsIfAny']")}</td>";
	          $ContentStr .= "</tr></thead><tbody>";

  				for($i=0, $i_max=count($result); $i<$i_max; $i++)
  				{
  				  if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
  				
  				  $t_year_str = (trim($result[$i]['Year']) == "") ? "--" : $result[$i]['Year'];
  				  $t_role_str = (trim($result[$i]['Role']) == "") ? "--" : $result[$i]['Role'];
  				  $t_org_str = (trim($result[$i]['Organization']) == "") ? "--" : $result[$i]['Organization'];
  				  $t_ach_str = (trim($result[$i]['Achievement']) == "") ? "--" : $result[$i]['Achievement'];
  				
  				  $t_ele_str = str_replace($ele_code_arr, $ele_title_arr, $result[$i]['ELE']);
  				  $t_ele_str = str_replace(",", ", ", $t_ele_str);
  				  $t_ele_str = (trim($t_ele_str) == "") ? "--" : $t_ele_str;
  				  
  				  $t_remark_str = (trim($result[$i]['Remark']) == "") ? "" : $result[$i]['Remark'];
  				  $data_align = 'center';
				  if($sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'] != ''){
					$data_align = $sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'];
				  }
  					$t_remark_str_show = (trim($t_remark_str)!="") ? "<br /><span style=\"{$style_detail}\">".$t_remark_str."</span>" : "" ;
  					$ContentStr .=	"
  														<tr height='25' style='page-break-inside:avoid; page-break-after:auto;'>
  														  <td  valign='middle' class='ole_content' style='{$style_general}'>".$result[$i]['Title'].$t_remark_str_show."</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:".$data_align.";'>{$t_year_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:".$data_align.";'>{$t_role_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:".$data_align.";'>{$t_org_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:".$data_align.";'>{$t_ele_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:".$data_align.";'>{$t_ach_str}</td>
  														</tr>
  													";
  				}
  				$ContentStr .=	"</tbody></table>";
  			}
  			else
  			{
  				$ContentStr = $emptyRecTable;
  			}
			}
			else
			{
				$ContentStr = $emptyRecTable;
			}

			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['OtherLearningExperiences']"), $this->BiLangOut("['OLE_TableDescription']"), $ContentStr);
		}
		else
		{

		}
		
		return $ReturnStr;
	}
	
	# Generate OLE table
	function GEN_EXTPERFORM_TABLE_SLP($ParExtPerformArr="", $StudentID="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp, $sys_custom;
		
		$emptyRecTable = <<<HTML
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td height='100' valign='middle' align='center'>--</td>
	</tr>
</table>		
HTML;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParExtPerformArr) && !empty($ParExtPerformArr))
			{
        list($result, $NumRecordAllowed, $IsPrintAll) = $this->GET_PRINTABLE_OLE_RECORD($ParExtPerformArr, $StudentID, 1);
        
        	$style_general = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "font-size=8pt;" : "";
		    $style_detail = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "{$style_general} font-style:italic;" : "";
		    $style_title_general = "text-align:center;";
		    
		    $t_program_title = $this->BiLangOut("['ProgrammesWithDescription']");
		    $t_program_title .= $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? $this->BiLangOut("['SelectedRecords']","","","V","B5","EN") : "";

        if(count($result) > 0)
        {
         	 $ContentStr =	"<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' class='table_print table_pb'>\n";
	          $ContentStr .= "<thead><tr style='border-top:1px solid #555555;'>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} font-weight:bold;' width='50%'>{$t_program_title}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_general} font-weight:bold;' width='10%'>{$this->BiLangOut("['SchoolYear']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_general} font-weight:bold;' width='10%'>{$this->BiLangOut("['RoleOfParticipation']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_general} font-weight:bold;' width='15%'>{$this->BiLangOut("['Organization']")}</td>";
	          $ContentStr .= "<td class='ole_head_sub' style='{$style_general} {$style_title_general} font-weight:bold;' width='15%'>{$this->BiLangOut("['AchievementsIfAny']")}</td>";
	          $ContentStr .= "</tr></thead><tbody>";
	
  				for($i=0; $i<count($result); $i++)
  				{
  				  if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
  				  
  				  $t_year_str = (trim($result[$i]['Year']) == "") ? "--" : $result[$i]['Year'];
  				  $t_role_str = (trim($result[$i]['Role']) == "") ? "--" : $result[$i]['Role'];
  				  $t_org_str = (trim($result[$i]['Organization']) == "") ? "--" : $result[$i]['Organization'];
  				  $t_ach_str = (trim($result[$i]['Achievement']) == "") ? "--" : $result[$i]['Achievement'];
  				  
  				  $t_remark_str = (trim($result[$i]['Remark']) == "") ? "" : $result[$i]['Remark'];
  				  
  				  $t_remark_str_show = (trim($t_remark_str)!="") ? "<br /><span style=\"{$style_detail}\">".$t_remark_str."</span>" : "" ;
  				  				
  					$ContentStr .=	"
  														<tr height='25'>
  														  <td valign='middle' class='ole_content' style='{$style_general}'>".$result[$i]['Title'].$t_remark_str_show."</td>
  															<td  valign='middle' class='ole_content' style='{$style_general} text-align:center;'>{$t_year_str}</td>
  															<td  valign='middle' class='ole_content' style='{$style_general} text-align:center;'>{$t_role_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:center;'>{$t_org_str}</td>
  															<td valign='middle' class='ole_content' style='{$style_general} text-align:center;'>{$t_ach_str}</td>
  														</tr>
  													";

  				}
  				$ContentStr .=	"</tbody></table>";
  			}
  			else
  			{
          $ContentStr = $emptyRecTable;
        }
			}
			else
			{
				$ContentStr = $emptyRecTable;
			}

			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['Performace/Awards']"), $this->BiLangOut("['ExternalOLE_TableDescription']"), $ContentStr);
		}
		else
		{

		}
		$ReturnStr .= "* ".$this->BiLangOut("['RequiredRemark']");
		
		return $ReturnStr;
	}

}

?>