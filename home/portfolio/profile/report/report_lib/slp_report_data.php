<?php
/* Modifying: 
 * 
 * Log:
 * 2018-01-08 Omas
 * - modified GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP() - add secondary sort by AwardDate
 * 2013-03-27 YatWoon [Case#2013-0322-1618-29073]
 * - modified GET_OLE_SLP(), update the OLE display ordering sorting [Requested by CS]
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - modified SET_DATA_ARRAY, added GET_SubjectInfoOrderedArr, subject ordering follows the School Settings now
 */
include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($intranet_root."/includes/libpf-report.php");
include_once($intranet_root."/includes/libpf-sturec.php");

class slp_report_data extends libpf_report{

  var $SPArr;
  var $SubjArr;
  var $YearArr;
  var $APArr;
  var $AwardArr;
  var $ActivityArr;
  var $OLEArr;
  var $ExtPerformArr;
  var $SelfAccountArr;
  var $YearName;
  var $SubjectInfoOrderedArr;
  var $SubjectFullMarkArr;

  function SET_DATA_ARRAY($ParStudentID, $ParAcademicYearID=array(), $ParYearTermID="", $ParIncludeComponentSubject=false){
  		
  		global $sys_custom, $PATH_WRT_ROOT;
  		
  		include_once($PATH_WRT_ROOT.'includes/portfolio25/libpf-academic.php');
  		$libpf_academic = new libpf_academic();
  		
  		
		# Academic Year
		$this->YearName = "";
		if(count($ParAcademicYearID) == 1 && is_array($ParAcademicYearID))
		{
			$ayID = current($ParAcademicYearID);
			$lib_ay = new academic_year($ayID);
			$this->YearName = $lib_ay->YearNameEN;
		}
  
    # Get student particulars
		$this->SPArr = $this->GET_STUDENT_PARTICULARS_SLP($ParStudentID);
		
		# Get academic performance
		list($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr) = $this->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($ParStudentID, $ParIncludeComponentSubject);
		//list($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr) = $this->GET_ACADEMIC_PERFORMANCE_SLP($ParStudentID);
		
		$this->SubjArr = $SubjArr;
		$this->YearArr = $YearArr;
		$this->APArr = $APArr;
		$this->SubjectInfoOrderedArr = $SubjectInfoOrderedArr;
		
		# Get award and major achievements in school
    $this->AwardArr = $this->GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($ParStudentID, $ParAcademicYearID, $ParYearTermID);
//		echo "This is hte award arr -> ".debug_r($this->AwardArr)."<br>";die;
		# Get extra-curricular activity
		$this->ActivityArr = $this->GET_ACTIVITY_SLP($ParStudentID, $ParAcademicYearID, $ParYearTermID);
		
		# OLE
		$this->OLEArr = $this->GET_OLE_SLP($ParStudentID, "INT", $ParAcademicYearID, $ParYearTermID);
    	
    # External Performance
		$this->ExtPerformArr = $this->GET_OLE_SLP($ParStudentID, "EXT", $ParAcademicYearID, $ParYearTermID);

		# Get self account
		$this->SelfAccountArr = $this->GET_SELF_ACCOUNT_SLP($ParStudentID);
		
		# Get SubjectFullMark
		$this->SubjectFullMarkArr = $libpf_academic->Get_Subject_Full_Mark();
  }
  
  ########################################################################
  
  public function GET_SPArr(){
    return $this->SPArr;
  }
  
  public function GET_SubjArr(){
    return $this->SubjArr;
  }
  
  public function GET_YearArr(){
    return $this->YearArr;
  }
  
  public function GET_APArr(){
    return $this->APArr;
  }
  
  public function GET_AwardArr(){
    return $this->AwardArr;
  }
  
  public function GET_ActivityArr(){
    return $this->ActivityArr;
  }
  
  public function GET_OLEArr(){
    return $this->OLEArr;
  }
  
  public function GET_ExtPerformArr(){
    return $this->ExtPerformArr;
  }
  
  public function GET_SelfAccountArr(){
    return $this->SelfAccountArr;
  }
  
  public function GET_YearName(){
		return $this->YearName;
	}
	
	public function GET_SubjectInfoOrderedArr() {
		return $this->SubjectInfoOrderedArr;
	}
	
	public function GET_SubjectFullMarkArr() {
		return $this->SubjectFullMarkArr;
	}
  
  ########################################################################
  
	# Get awards and major achievement
	function GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($ParStudentIDArr, $ParAcademicYearID=array(), $ParYearTermID="")
	{
		global $intranet_db, $eclass_db;

		$sql =	"SELECT DISTINCT UserID, Year, AwardName, Remark, Organization ";
		$sql .= "FROM {$eclass_db}.AWARD_STUDENT ";
		$sql .= "WHERE UserID IN ('".implode("','", (array)$ParStudentIDArr)."') AND RecordType = '1' ";
		$sql .= empty($ParAcademicYearID) ? "" : "AND AcademicYearID IN (".implode(", ", $ParAcademicYearID).") ";
		$sql .= ($ParYearTermID != "") ? "AND YearTermID = {$ParYearTermID} " : "";
		$sql .=	"ORDER BY Year DESC , AwardDate desc";

		$AwardArr = $this->returnArray($sql);

		$ReturnArr = array();
		for($i=0; $i<count($AwardArr); $i++)
		{
			$ReturnArr[$AwardArr[$i]['UserID']][] = array	(
																											'Year' => $AwardArr[$i]['Year'],
																											'AwardName' => $AwardArr[$i]['AwardName'],
																											'Remark' => $AwardArr[$i]['Remark'],
																											'Organization' => $AwardArr[$i]['Organization']
																										);
		}

		return $ReturnArr;
	}
	
	# Get extra-curricular activity
	function GET_ACTIVITY_SLP($ParStudentIDArr, $ParAcademicYearID=array(), $ParYearTermID="")
	{
		global $eclass_db;
	
		$sql =	"SELECT UserID, Year, ActivityName, Role ";
		$sql .= "FROM {$eclass_db}.ACTIVITY_STUDENT ";
		$sql .= "WHERE UserID IN ('".implode("','", (array)$ParStudentIDArr)."') ";
		$sql .= empty($ParAcademicYearID) ? "" : "AND AcademicYearID IN (".implode(", ", $ParAcademicYearID).") ";
		$sql .= ($ParYearTermID != "") ? "AND YearTermID = {$ParYearTermID} " : "";
		$sql .=	"ORDER BY Year, ActivityName";
		$ActivityArr = $this->returnArray($sql);

		$ReturnArr = array();
		for($i=0; $i<count($ActivityArr); $i++)
		{
			$ReturnArr[$ActivityArr[$i]['UserID']][] = array	(
																													'Year' => $ActivityArr[$i]['Year'],
																													'ActivityName' => $ActivityArr[$i]['ActivityName'],
																													'Role' => $ActivityArr[$i]['Role']
																												);
		}
	
		return $ReturnArr;
	}
  
	# Get OLE / External Performance
	function GET_OLE_SLP($ParStudentIDArr, $ParIntExt, $ParAcademicYearID=array(), $ParYearTermID="")
	{
		global $eclass_db, $intranet_db, $lpf_slp,$sys_custom;
	
    # Check by OLE start date
    //$extra_table = empty($ParAcademicYearID) ? "" : "{$intranet_db}.ACADEMIC_YEAR AS ay, {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt, ";
    //$date_year_field = "op.StartDate,";
    //$cond = empty($ParAcademicYearID) ? "" : " AND (op.StartDate BETWEEN ayt.TermStart and ayt.TermEnd AND ayt.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID = ".$ParAcademicYearID.")";
    # Check by OLE year and term
    $date_year_field = Get_Lang_Selection("ay.YearNameB5 AS YearName,", "ay.YearNameEN AS YearName,");
	$extra_join_table = "INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay ON ay.AcademicYearID = op.AcademicYearID";
    $cond = "";
    $cond .= empty($ParAcademicYearID) ? "" : "AND op.AcademicYearID IN (".implode(", ", $ParAcademicYearID).") ";
    $cond .= empty($ParYearTermID) ? "" : "AND op.YearTermID = ".$ParYearTermID." ";
	
		$sql =	"SELECT os.RecordID, os.UserID, op.Title, {$date_year_field} os.Role, ";
		$sql .= "os.Achievement, op.ELE, op.Organization, op.Details AS Remark, os.SLPOrder ";
		$sql .= "FROM {$extra_table} {$eclass_db}.OLE_STUDENT AS os ";
		$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
		$sql .= $extra_join_table." ";
		$sql .= "WHERE ";
		$sql .= "os.UserID IN ('".implode("','", $ParStudentIDArr)."') AND ";
		$sql .= "os.RecordStatus IN (2,4) AND op.IntExt = '{$ParIntExt}' $cond ";
		//$sql .= "ORDER BY os.StartDate DESC";
		//$sql .= "ORDER BY ay.Sequence ASC , op.StartDate desc";
		$sql .= "ORDER BY op.StartDate desc";
		

		$OLEArr = $this->returnArray($sql);

		for($i=0; $i<count($OLEArr); $i++)
		{
		   if(isset($OLEArr[$i]['StartDate']))
		   {
  		 	    $t_academic_year_arr = getAcademicYearInfoAndTermInfoByDate($OLEArr[$i]['StartDate']);
  		  		$t_academic_year = $t_academic_year_arr[1];
	  		}
	  		else
	  		{
	  			$t_academic_year = $OLEArr[$i]['YearName']; 	
		    }
				
			$ReturnArr[$OLEArr[$i]['UserID']][] = array	(
                                                    "RecordID" => $OLEArr[$i]['RecordID'],
                                                    "Title" => $OLEArr[$i]['Title'],
																										"Year" => $t_academic_year,
																										"Role" => $OLEArr[$i]['Role'],
																										"ELE" => $OLEArr[$i]['ELE'],
																										"Achievement" => $OLEArr[$i]['Achievement'],
																										"Organization" => $OLEArr[$i]['Organization'],
																										"Remark" => $OLEArr[$i]['Remark'],
																									"SLPOrder" => $OLEArr[$i]['SLPOrder']
																								);
		}
		return $ReturnArr;
	}
}

?>