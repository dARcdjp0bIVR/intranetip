<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 *  2019-06-26 Anna
 *  - Added DisplayRecordNum for bps cust
 *  2019-03-01 Anna
 *  - added nlp cust
 *  
 *  2018-05-24 Anna
 *  - added tmgss cust
 *  
 *  2018-03-16 Omas
 * - set time limit to 300s
 * 
 *  2017-11-02 Isaac
 * - added broswer detection and changes for compatability of the inline vs border and support of consistance page break between each student in different broswer.
 * 
 *  2017-11-01 Isaac
 * - added border css to control outputing inner border for table in word export
 * 
 * 2017-10-30 Isaac
 * - added css and 'section1'div wrap for aord output to change the page margins
 * 
 * 2014-11-28 Pun
 * - Change all $_POST['var'] to $var
 * 
 * 2012-02-17 Ivan
 * - Added show / hide full mark display logic for SLP report
 * 
 * 2011-12-19 Ivan
 * - Added academic result display mode options for SLP report
 * 
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - fixed: subject ordering follows the School Settings now
 * 
 * 2010-08-18: Max (201008180910)
 * - Plug NPL cust from IP20
 * 
 * 2010-02-25: Max (201002181147)
 * - Add config for cnecc
 * *******************************************
 */
set_time_limit(300);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once("report_lib/common.php");
include_once("report_lib/full_report.php");
include_once("report_lib/transcript.php");
include_once("report_lib/slp_report.php");
include_once("report_lib/slp_report_data.php");
include_once("report_lib/slp_report_html.php");
include_once("report_lib/slp_report_pdf.php");

intranet_auth();
intranet_opendb();


$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

$IsZip = stripslashes($IsZip);

//////////////////////////////////////////////////////////////////
//start handle for slp , user to print the report in  bilingual //
//////////////////////////////////////////////////////////////////
//$slpReportLang -> variable from user input
for($i =0,$i_max = sizeof($slpReportLang);$i < $i_max; $i++){
	$_tmpLang = $slpReportLang[$i];
	$slpRequestLang = strtoupper($_tmpLang);
}
if(sizeof($slpReportLang)>1 && is_array($slpReportLang))
{
	//element of $slpReportLang > 1 , that mean user select with two lang --> set the lang to BI	
	$slpRequestLang = "BI";
}
$slpRequestLang = ($slpRequestLang == "")?"BI":$slpRequestLang;
//////////////////////////////////////////////////////////////////
//end of handle for slp , user to print the report in  bilingual//
//////////////////////////////////////////////////////////////////

$printIssueDate  = trim($printIssueDate);
$academicScoreDisplayMode = $academicScoreDisplayMode;
$displayFullMark = ($displayFullMark)? true : false;
$displayComponentSubject = ($displayComponentSubject)? true : false;

if($StudentID != "")
{
	$StudentArr = array($StudentID);
}
else if($YearClassID != "")
{
  # The GET_CLASS_TEACHER_STUDENT get the users from "YEAR_CLASS_USER" that does not join "YEAR_CLASS_TEACHER"
  	foreach(array($YearClassID) as $val){
	  	$t_StudentArr = GET_CLASS_TEACHER_STUDENT($val, $ActivatedOnly=true, strtolower($trgType));
	
		for($i=0,$i_max = count($t_StudentArr); $i<$i_max; $i++){
			$StudentArr[] = $t_StudentArr[$i]['UserID'];
		}
  	}
}


//echo $ReportType;die;
switch($ReportType)
{
  case 1:
  	$academicYearID = current($academicYearID);
  


    //include_once("report_lib/full_report.php");
    # UCCKE Customization
    if($sys_custom['uccke_full_report'])
    {


      # Academic Year
      $lib_ay = new academic_year($academicYearID);
      $Year = $lib_ay->YearNameEN;
      
      # Year Term
      $lib_ayt = new academic_year_term($YearTermID);
      $Semester = $lib_ayt->YearTermNameEN;
      
      # Year Class
      $Class = array();
      foreach((array)$YearClassID as $val){
	      $lib_yc = new year_class($val);
	      $Class[] = $lib_yc->ClassTitleEN;
      }
      if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0)
      {
      	$alumni_sids = implode(",", $alumni_StudentID);
      }
      
      
		header("Location: uccke_full_report_print.php?TargetClass=".implode(',',$Class)."&AcademicYearID=$academicYearID&Year=$Year&YearTermID=$YearTermID&Semester=$Semester&StudentID=$StudentID&alumni_StudentID=$alumni_sids&PrintType=$PrintType");
		  intranet_closedb();
      DIE();
    }
  
	$StudentArr = _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr);
	

    $lpf_sturec = new libpf_sturec();
    $lpf_report = new libpf_report();
    $lpf_slp = new libpf_slp();
    

	$report_html = generateFullReportPrint($StudentArr, $academicYearID, $YearTermID);

	_handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr);
	

	break;

	case 2:
		$academicYearID = current($academicYearID);
	
    //include_once("report_lib/transcript.php");
    $lib_transcript = new transcript();


	$StudentArr = _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr);

	$report_html = $lib_transcript->generateTranscriptPrint($StudentArr, $academicYearID, $YearTermID, false, $issuedate);

	_handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr);

	break;

	case 3:
		$academicYearID = current($academicYearID);

    if($sys_custom["portfolio_customized_report_keichi"])
    {
      # Academic Year
      $lib_ay = new academic_year($academicYearID);
      $Year = $lib_ay->YearNameEN;
      
      # Year Term
      $lib_ayt = new academic_year_term($YearTermID);
      $Semester = $lib_ayt->YearTermNameEN;
      
      # Year Class
      $lib_yc = new year_class($YearClassID);
      $Class = $lib_yc->ClassTitleEN;

      header("Location: http://{$eclass_httppath}src/portfolio/profile/report/customized_report_print.php?TargetClass=$Class&Year=$Year&Semester=$Semester&StudentID=$StudentID&PrintType=$PrintType");
		  intranet_closedb();
      DIE();
    }
    else if($sys_custom["portfolio_customized_report_ihmc"])
    {
      # Academic Year
      $lib_ay = new academic_year($academicYearID);
      $Year = $lib_ay->YearNameEN;
      
      # Year Term
      $lib_ayt = new academic_year_term($YearTermID);
      $Semester = $lib_ayt->YearTermNameEN;
      
      # Year Class
      $lib_yc = new year_class($YearClassID);
      $Class = $lib_yc->ClassTitleEN;

      header("Location: http://{$eclass_httppath}/src/portfolio/profile/report/customized_report_print_ihmc.php?TargetClass=$Class&Year=$Year&Semester=$Semester&StudentID=$StudentID&PrintType=$PrintType");
      intranet_closedb();
      DIE();
    }
    break;
	case 6:
		include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");
//	echo BiLangOut("['words']");
    //include_once("report_lib/slp_report.php");
    //$lpf_ui = new libportfolio2007();
    $lpf_slp = new libpf_slp();
    $academicYearID = in_array("", $academicYearID) ? array() : $academicYearID;
    
    # Academic Year
    //$lib_ay = new academic_year($academicYearID);
    //$Year = $lib_ay->YearNameEN;    
	
	$StudentArr = _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr);

    # Data retrieval
    //include_once("report_lib/slp_report_data.php");
    $lpf_slp_data = new slp_report_data();
    $lpf_slp_data->SET_DATA_ARRAY($StudentArr, $academicYearID, $YearTermID, $displayComponentSubject);
    $yearName = $lpf_slp_data->GET_YearName();
    
    # Report printing object selection
    switch($PrintType)
    {
      case "html":
      case "word":
        //include_once("report_lib/slp_report_html.php");
        $lib_slp_report = new slp_report_html();
        break;
      case "pdf":
        //include_once("report_lib/slp_report_pdf.php");
        $lib_slp_report = new slp_report_pdf();
        break;
    }
    
    # Set data to report printing object
    // if modified below coding, please modify "/includes/libpf-lp.php" function "generateReportPrint" also
    $lib_slp_report->SET_SPArr($lpf_slp_data->GET_SPArr());
    $lib_slp_report->SET_SubjArr($lpf_slp_data->GET_SubjArr());
    $lib_slp_report->SET_YearArr($lpf_slp_data->GET_YearArr());
    $lib_slp_report->SET_APArr($lpf_slp_data->GET_APArr());
    $lib_slp_report->SET_AwardArr($lpf_slp_data->GET_AwardArr());
    $lib_slp_report->SET_ActivityArr($lpf_slp_data->GET_ActivityArr());
    $lib_slp_report->SET_OLEArr($lpf_slp_data->GET_OLEArr());
    $lib_slp_report->SET_ExtPerformArr($lpf_slp_data->GET_ExtPerformArr());
    $lib_slp_report->SET_SelfAccountArr($lpf_slp_data->GET_SelfAccountArr());
    $lib_slp_report->SET_SubjectInfoOrderedArr($lpf_slp_data->GET_SubjectInfoOrderedArr());
    $lib_slp_report->SET_SubjectFullMarkArr($lpf_slp_data->GET_SubjectFullMarkArr());
    
    $lib_slp_report->setReportLang($slpRequestLang); 
    # Report printing
    $report_html = $lib_slp_report->GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($StudentArr, $issuedate, $yearName, $printIssueDate, $IsZip, $academicScoreDisplayMode, $displayFullMark, $displayComponentSubject);
	
	_handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr);

	break;
	case 7:
		$academicYearID = current($academicYearID);
    if($sys_custom["ywgs_ole_report"])
    {
      # Academic Year
      $lib_ay = new academic_year($academicYearID);
      $Year = $lib_ay->YearNameEN;
      
      # Year Term
      $lib_ayt = new academic_year_term($YearTermID);
      $Semester = $lib_ayt->YearTermNameEN;
      
      # Year Class
      $lib_yc = new year_class($YearClassID);
      $Class = $lib_yc->ClassTitleEN;
      
      # Print Selected Record
      $PrintSelRec = ($slpRecOpt == "sel") ? 1 : 0;
      $SpaceTop = ($SpaceTop == "") ? 1 : round(floatval($SpaceTop)/0.1, 0)*0.1;
      $SpaceBottom = ($SpaceBottom == "") ? 1 : round(floatval($SpaceBottom)/0.1, 0)*0.1;
      $hasSA = ($slplayout == 2) ? true : false;
      header("Location: ywgs_ole_report.php?TargetClass=$Class&Year=$Year&Semester=$Semester&StudentID=$StudentID&issuedate=$issuedate&PrintType=$PrintType&PrintSelRec=$PrintSelRec&SpaceTop=$SpaceTop&SpaceBottom=$SpaceBottom&hasSA=$hasSA");
		  intranet_closedb();
      DIE();
    }
    else if($sys_custom["ccym_ole_report"])
	  {
      # Academic Year
      $lib_ay = new academic_year($academicYearID);
      $Year = $lib_ay->YearNameEN;
      
      # Year Term
      $lib_ayt = new academic_year_term($YearTermID);
      $Semester = $lib_ayt->YearTermNameEN;
      
      # Year Class
      $lib_yc = new year_class($YearClassID);
      $Class = $lib_yc->ClassTitleEN;
	  
	    $uploads_dir = $intranet_root."/file/portfolio";
	    if(isset($_FILES['AttachFile']))
	    {
	      if ($_FILES['AttachFile']['error'] == UPLOAD_ERR_OK) {
	        $tmp_name = $_FILES["AttachFile"]["tmp_name"];
	        if(!file_exists($uploads_dir)) mkdir($uploads_dir);
	        move_uploaded_file($tmp_name, $uploads_dir."/starAward.csv");
	      }
	    }
	  
	  	header("Location: ccym_report_print.php?TargetClass=$Class&Year=$Year&Semester=$Semester&StudentID=$StudentID&issuedate=$issuedate&PrintType=$PrintType");
	  	DIE();
	  }
    break;
	# CNECC customize
	case 9:
		$academicYearID = current($academicYearID);
		header("Location: ../../customize/cnecc/report.php?printType={$PrintType}&StudentID=".implode(",",$StudentArr)."&academicYearID=".$academicYearID."&issueDate=".$issuedate);
		break;
	# BPS customize
	case 10:
		$academicYearID = current($academicYearID);
		header("Location: ../../customize/bps/report.php?printType=html&StudentID=".implode(",",$StudentArr)."&academicYearID=".$academicYearID."&YearTermID=".$YearTermID."&DisplayRecordNum=".$DisplayRecordNum);
		break;
	# NPL customize
	case 11:
		$academicYearID = current($academicYearID);
		header("Location: customized_report_print_plknpl.php?TargetClass=$YearClassID&academicYearID=$academicYearID&Semester=$Semester&StudentID=$StudentID&issuedate=$issuedate");
		break;
	# Raimondi College - Non Academic Performance Report
	case 12:
		$academicYearIDList = implode(',', (array)$academicYearID);
		$WithSelfAccount = $_REQUEST['raimondi_withSelfAccount'];
		header("Location: customized_report_print_raimondi.php?TargetClass=$YearClassID&academicYearIDList=$academicYearIDList&StudentID=$StudentID&issuedate=$issuedate&WithSelfAccount=$WithSelfAccount");
		break;
		# BPS customize
	case 'tmgss':
	    $academicYearID = current($academicYearID);
	    header("Location: ../../customize/tmgss/report.php?printType=".$PrintType."&StudentID=".implode(",",$StudentArr)."&academicYearID=".$academicYearID."&YearTermID=".$YearTermID."&DateofIssue=".$issuedate);
	    break;
// 	case 'nlp_slp1':
// 	case 'nlp_slp2':
// 	    $academicYearID = current($academicYearID);
// 	    header("Location: ../../customize/nlp/report.php?ReportType=".$ReportType."&StudentID=".implode(",",$StudentArr)."&academicYearID=".$academicYearID."&YearTermID=".$YearTermID."&DateofIssue=".$issuedate);
// 	    break;
	
	# ST ANTONIUS GIRLS COLLEGE - Customized Student Learning Profile Report	
	case 'sagc_slp':
		include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");
	    $lpf_slp = new libpf_slp();
	    $academicYearID = in_array("", $academicYearID) ? array() : $academicYearID;
	
		$StudentArr = _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr);
	
	    # Data retrieval
	    $lpf_slp_data = new slp_report_data();
	    $lpf_slp_data->SET_DATA_ARRAY($StudentArr, $academicYearID, $YearTermID, $displayComponentSubject);
	    $yearName = $lpf_slp_data->GET_YearName();
	    
	    $report_html = '';
	    
	    # Report printing object selection
	    switch($PrintType)
	    {
	      case "html":
	      //cannot be used, 22 Feb 2013
	        $lib_slp_report = new slp_report_html();
	    $report_html = <<<HTML
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
$(window).load(function(){
	rtStr = '';
	pageH = 900;
	$('.stdDiv').each(function(){
		aH = $(this).height() ;
		p = Math.ceil(aH/pageH);
		//$(this).css('border','1px solid #F00');
		//$(this).attr('height', p*pageH);
		//$(this).css('border','1px solid #F00');
		//$(this).find('.signatureSpace').css('height',$(this).find('.signatureSpace').height()+'px');
		//alert(aH+'  '+p*pageH+' '+$(this).height()+' '+$(this).find('.signatureLine').height()+' '+$(this).find('.detailTD').height());
		$(this).find('.signatureSpace').css('height',(p*pageH-aH-48)+'px');
	})
})
</script>
HTML;
	      	break;
	      case "pdf":
	        //include_once("report_lib/slp_report_pdf.php");
	        $lib_slp_report = new slp_report_pdf();
	        break;
	    }
	    
	    # Set data to report printing object
	    // if modified below coding, please modify "/includes/libpf-lp.php" function "generateReportPrint" also
	    $lib_slp_report->SET_SPArr($lpf_slp_data->GET_SPArr());
	    $lib_slp_report->SET_SubjArr($lpf_slp_data->GET_SubjArr());
	    $lib_slp_report->SET_YearArr($lpf_slp_data->GET_YearArr());
	    $lib_slp_report->SET_APArr($lpf_slp_data->GET_APArr());
	    $lib_slp_report->SET_OLEArr($lpf_slp_data->GET_OLEArr());
	    $lib_slp_report->SET_AwardArr($lpf_slp_data->GET_AwardArr());
	    $lib_slp_report->SET_ExtPerformArr($lpf_slp_data->GET_ExtPerformArr());
	    $lib_slp_report->SET_SelfAccountArr($lpf_slp_data->GET_SelfAccountArr());
	    $lib_slp_report->SET_SubjectInfoOrderedArr($lpf_slp_data->GET_SubjectInfoOrderedArr());
	    $lib_slp_report->SET_SubjectFullMarkArr($lpf_slp_data->GET_SubjectFullMarkArr());
	    
	    $lib_slp_report->setReportLang($slpRequestLang); 
	    # Report printing
	    
	    //$report_html .= $lib_slp_report->GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT_SAGC($StudentArr, $issuedate, $yearName, $printIssueDate, $IsZip, $academicScoreDisplayMode, $displayFullMark, $displaySelfAccount);
	    $report_html .= $lib_slp_report->GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT_SAGC($StudentArr, $issuedate, $yearName, $printIssueDate, $IsZip, $academicScoreDisplayMode, $displayFullMark, $displayComponentSubject);
		
		_handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr);
		break;	
		
}

switch($PrintType)
{
  # pdf case should not be entered as pdf has been outputted in previous function 
  case "pdf":
    DIE();
    break;
  case "word":
  	$ContentType = "application/x-msword";
  	$filename = "export_word_file".date("ymd").".doc";
  	$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
  	foreach ($tags_to_strip as $tag)
  	{
  		$report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_html);
  	}
  	
  	# Eric Yip (20091008): Replace official photo link
  	$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/official_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
	$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/user_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
	$report_html = preg_replace("/(src=)(['\"]?).*(\/file\/portfolio.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $report_html);
	//$output = "<HTML>\n";
  	$output = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
  	$output .= "<HEAD>\n";
  	$output .= returnHtmlMETA();
	$output .= "<style>\n";
  	$output .= "div.Section1{page:Section1;}\n";
 
   	if ($sys_custom['iPf']['SLP_Report']['DisplayBorderInHtmlMode']) {
  	$output .=  ".ole_content, .ole_head_sub, .acadamic_t_boarder{border: 1px solid; }/n";  	   
   	}
  	$output .= "@page Section1{	margin:36.0pt 36.0pt 36.0pt 36.0pt;}\n";
  	$output .= "</style>\n";
  	$output .= "</HEAD>\n";
  	$output .= "<BODY LANG=\"zh-HK\">\n";
 	$output .= "<div class=\"Section1\">\n";
  	$output .= $report_html;
  	$output .= "</div>\n";
  	$output .= "</BODY>\n";
  	$output .= "</HTML>\n";
  	
  	
  	header('Pragma: public');
  	header('Expires: 0');
  	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  
  	header('Content-type: '.$ContentType);
  	header('Content-Length: '.strlen($output));
  
  	header('Content-Disposition: attachment; filename="'.$filename.'";');

    break;
  case "html":
  	if($ReportType=='sagc_slp'){
    	$output = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">';
  	}else{
   		$output = "<html>\n";
  	}
    $output .= "<head>\n";
    $output .= returnHtmlMETA()."\n";
    $output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_report.css\" rel=\"stylesheet\" type=\"text/css\">\n";
    # compulsory use of IE10 for good page breaks 
    $output .= "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE10\" />\n";
    $output .= "</head>\n";
    $output .= "<body>\n";
    $output .= $report_html."\n";
    $output .= "</body>\n";
    $output .= "</html>\n";
    break;
}
//$objIPF->db_show_debug_log_by_query_number(1);
echo $output;
intranet_closedb();
?>