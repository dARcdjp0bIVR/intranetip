<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/raimondi_college/libpf-raimondi_college.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

$REPORT_WIDTH = 750;

$libportfolio_custom = new libpf_raimondi_college();
$libFCM = new form_class_manage();

$TargetClass = $_REQUEST["TargetClass"];
$StudentID = $_REQUEST["StudentID"];
$IssueDate = $_REQUEST["issuedate"];
$WithSelfAccount = $_REQUEST["WithSelfAccount"];
$AcademicYearIDList = $_REQUEST["academicYearIDList"];
$AcademicYearIDArr = explode(',', $AcademicYearIDList);
$AcademicYearIDArr = array_remove_empty($AcademicYearIDArr);
$AcademicYearInfoArr = $libFCM->Get_Academic_Year_List($AcademicYearIDArr);


### Get all Student Info
if ($StudentID=='')
{
	$StudentInfoArr = $libFCM->Get_Student_By_Class($TargetClass);
	$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
}	
else
{
	$StudentIDArr = array($StudentID);
}
$StudentInfoArr = $libFCM->Get_Student_Class_Info_In_AcademicYear($StudentIDArr, Get_Current_Academic_Year_ID());

### Generate Report
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");

echo $libportfolio_custom->Get_Print_Header();
echo $libportfolio_custom->Get_Non_Academic_Report_HTML($AcademicYearInfoArr, $StudentInfoArr, $WithSelfAccount, $IssueDate);


intranet_closedb();
?>
