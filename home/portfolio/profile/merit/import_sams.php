<?php
/*
 * 	Log
 * 	
 * 	Purpose: import merit / demerit
 *
 * 	Date:	2016-02-16 [Cameron]
 * 			- Add import column explanation and remarks
 *  
 * 	Date:	2016-01-28 [Cameron]
 * 			create this file
 * 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");	// authen
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

# tag information	
$CurrentPage = "SAMS_import_anp";
$title = $ec_iPortfolio['SAMS_import_anp'];
$TAGS_OBJ[] = array($title,"",0);
$MODULE_OBJ["title"] = $title;

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);

# import column explanation and remarks
$csv_format = "";
$delim = "<br>";
for($i=0, $iMax=count($Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['ANP']); $i<$iMax; $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['General']['ImportColumn']." ".numberToLetter($i+1, true)." : ".$Lang['iPortfolio']['SLP']['SchoolRecord']['ImportColumns']['ANP'][$i];
}
$csv_remarks = "";
for($i=0, $iMax=count($Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['ANP']); $i<$iMax; $i++){
	if($i!=0) $csv_remarks .= $delim;
	$csv_remarks .= $Lang['iPortfolio']['SLP']['SchoolRecord']['ImportRemarks']['ANP'][$i];
}

# import file interface
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$i_select_file.'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<input class="file" type="file" name="userfile">'."\r\n";
			$x .= '<br><br>'."\r\n";
			$x .= '<a class="tablelink" href="download.php?FileName_e='.getEncryptedText('anp_sams_sample.csv').'" target="_self">'."\r\n";
				$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\r\n";
				$x .= $import_csv['download']."\r\n";
			$x .= '</a>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";			
		$x .= '<td class="field_title">'.$Lang['General']['ImportArr']['DataColumn'].'</td>'."\r\n";
		$x .= '<td>'.$csv_format."</td>\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";		
		$x .= '<td colspan=2>'.$csv_remarks."</td>\r\n";
	$x .= '</tr>'."\r\n";		

$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goCancel() {
	self.close();
}

function goSubmit() {
	var canSubmit = true;
	
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		canSubmit = false;
		alert('<?=$Lang['General']['PleaseSelectFiles']?>');
		obj.elements["userfile"].focus();
	}
	
	if (canSubmit) {
		obj.submit();
	}
}
</script>
<form id="form1" name="form1" action="import_sams_validate.php" method="POST" enctype="multipart/form-data">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>