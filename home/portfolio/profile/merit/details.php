<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-table.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_sturec();

//$lpf->accessControl("merit");

/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $lpf->getStudentID($ck_user_id);

	# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['merit']." (".$year." - ".$semester.")", "")
					);
} 
else
{
	# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['merit']." (".$year." - ".$semester.")", "")
					);
}
*/

# Display according to user type
switch($_SESSION['UserType'])
{
	case 1:			# Teacher
		break;
	case 2:			# Student
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";

	# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['merit']." (".$year." - ".$semester.")", "")
					);
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;

$li = new libdbtable2007($field, $order, $pageNo);

# handle semester different problem
list($SemesterArr, $ShortSemesterArr) = $lpf->getSemesterNameArrFromIP();
list($SemList, $List1, $List2, $List3) = $lpf->getSemesterMatchingLists($semester, $SemesterArr);
if($semester!="" && $semester!=$ec_iPortfolio['whole_year'])
{
	//$conds = "AND (ms.Semester = '$semester' OR ms.Semester IN ({$SemList}))";
	$conds = "AND ms.Semester = '$semester'";
}

if($sys_custom['iPortfolioHideRemark']==true)
{	
	$li->field_array = array("ms.Year", "ms.Semester", "ms.RecordType", "ms.NumberOfUnit", "ms.Reason", "ms.PersonInCharge", "ms.ModifiedDate");
	$li->no_col = 8;
	$li->column_array = array(0,0,0,0,0,0,0);
	$ReasonWidth = "40%";
}
else
{
	$li->field_array = array("ms.Year", "ms.Semester", "ms.RecordType", "ms.NumberOfUnit", "ms.Reason", "ms.Remark", "ms.PersonInCharge", "ms.ModifiedDate");
	$li->no_col = 9;
	$li->column_array = array(0,0,0,0,0,0,0,0);
	$RemarkField = "if(ms.Remark IS NULL OR ms.Remark='', '--', ms.Remark) as Remark,";
	$ReasonWidth = "20%";
}
/*
$sql = "SELECT
		ms.Year,
        if(INSTR('{$List1}', ms.Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}', ms.Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}', ms.Semester),'".$SemesterArr[2]."',ms.Semester))),
        CASE ms.RecordType
                   WHEN 1 THEN '$i_Merit_Merit'
                   WHEN 2 THEN '$i_Merit_MinorCredit_unicode'
                   WHEN 3 THEN '$i_Merit_MajorCredit_unicode'
                   WHEN 4 THEN '$i_Merit_SuperCredit_unicode'
                   WHEN 5 THEN '$i_Merit_UltraCredit_unicode'
                   WHEN -1 THEN '$i_Merit_BlackMark'
                   WHEN -2 THEN '$i_Merit_MinorDemerit'
                   WHEN -3 THEN '$i_Merit_MajorDemerit'
                   WHEN -4 THEN '$i_Merit_SuperDemerit'
                   WHEN -5 THEN '$i_Merit_UltraDemerit'
        ELSE '-' END AS MeritType,
        ms.NumberOfUnit,
        ms.Reason,
        {$RemarkField}
        ms.PersonInCharge,
		ms.ModifiedDate 
	FROM 
		{$eclass_db}.MERIT_STUDENT as ms
    WHERE 
		ms.UserID = '$StudentID'
		AND ms.Year = '$year'
	$conds
";
*/
$sql = "SELECT
		    ms.Year,
        ms.Semester,
        CASE ms.RecordType
                   WHEN 1 THEN '$i_Merit_Merit'
                   WHEN 2 THEN '$i_Merit_MinorCredit_unicode'
                   WHEN 3 THEN '$i_Merit_MajorCredit_unicode'
                   WHEN 4 THEN '$i_Merit_SuperCredit_unicode'
                   WHEN 5 THEN '$i_Merit_UltraCredit_unicode'
                   WHEN -1 THEN '$i_Merit_BlackMark'
                   WHEN -2 THEN '$i_Merit_MinorDemerit'
                   WHEN -3 THEN '$i_Merit_MajorDemerit'
                   WHEN -4 THEN '$i_Merit_SuperDemerit'
                   WHEN -5 THEN '$i_Merit_UltraDemerit'
        ELSE '-' END AS MeritType,
        ms.NumberOfUnit,
        ms.Reason,
        {$RemarkField}
        ms.PersonInCharge,
		ms.ModifiedDate 
	FROM 
		{$eclass_db}.MERIT_STUDENT as ms
    WHERE 
		ms.UserID = '$StudentID'
		AND ms.Year = '$year'
	$conds
";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $usertype_s;
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
else
$li->page_size = 999;
//$li->noNumber = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['year'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['semester'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['mtype'], 1)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $ec_iPortfolio['amount'], 1)."</td>\n";
$li->column_list .= "<td width='".$ReasonWidth."'>".$li->column($pos++, $ec_iPortfolio['reason'], 1)."</td>\n";
if($sys_custom['iPortfolioHideRemark']==false) {
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $ec_iPortfolio['remark'] , 1)."</td>\n";
}
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['pic'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";

################### Function Start ##########################
function getSummary($my_summary){
	$tmp_arr = explode("SePa", stripslashes($my_summary));
	
	if (sizeof($tmp_arr)%2==0)
	{
		$size_used = sizeof($tmp_arr)/2;
		$rx = "<table cellpadding='8' cellspacing='0' border='0'><tr>\n";
		for ($i=0; $i<$size_used; $i++)
		{
			$rx .= "<td class='tabletext' nowrap>" . $tmp_arr[$i] . " : <b>" . $tmp_arr[$i+$size_used] . "</b>&nbsp;</td>\n";
		}
		$rx .= "</tr></table>\n";
	}

	return $rx;
}
################### Function End #############################

if($flag==1)
{
	$summary_merit = stripslashes($summary_merit);
	$summary_demerit = stripslashes($summary_demerit);
}

$summary_merit_html = getSummary($summary_merit);
$summary_demerit_html = getSummary($summary_demerit);

//////////////////////////////////////////////

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$luser = new libuser($UserID);

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 1);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 0);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("merit");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
-->
</script>


<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<FORM name="form1" method="GET">
                                <tr>
                                  <td valign="top">
                                        <?
                                        echo $lpf->GET_TAB_MENU($TabMenuArr);
                                        ?>
									<table width="100%"  border="0" cellspacing="5" cellpadding="0">
                                      <tr>
                                        <td><img src="<?=$PATH_WRT_ROOT?>/<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="index.php"><?=$ec_iPortfolio['overall_summary']?></a>
                                          <img src="<?=$PATH_WRT_ROOT?>/<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['merit_detail']?></span></td>
                                        </tr>
                                      <tr>
                                        	
                                      <tr>
                                        <td><?=$summary_merit_html?></td>
                                      </tr>
									   <tr>
                                        <td><?=$summary_demerit_html?></td>
                                      </tr>
                                      <tr>
                                        <td align="center">
										<?= $li->displayPlain() ?>
										</td>
                                      </tr>

                                    </table>
									</td>
                                </tr>
<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="year" value="<?=$year?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type="hidden" name="summary" value="<?=$summary?>">
<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
<input type="hidden" name="flag" value=1>
<input type=hidden name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type=hidden name="order" value="<?php echo $li->order; ?>">
<input type=hidden name="field" value="<?php echo $li->field; ?>">
<input type=hidden name="numPerPage" value="<?=$numPerPage?>">
<input type=hidden name="page_size_change" value="<?=$page_size_change?>">
<input type=hidden name="summary_merit" value="<?=$summary_merit?>">
<input type=hidden name="summary_demerit" value="<?=$summary_demerit?>">
</FORM>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
