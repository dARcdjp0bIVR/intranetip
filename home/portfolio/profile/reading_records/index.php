<?php
/*
 * 	Modifying by:
 * 
 * 	2018-01-26 Cameron
 * 		- create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

//auth("TSP");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_slp();
if ($page_size_change!="") { setcookie("ck_page_size", $numPerPage, 0, "", "", 0);}


switch($_SESSION['UserType'])
{
	case 2:
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}


$libelibrary = new elibrary();

$academic_year_list = $libelibrary->getYearClassList($StudentID);

### FOR UCCKE, default use current year if existed
if($sys_custom['ipf']['school_records']['uccke'] && !isset($_GET['ChooseYear'])){
	$currYearID = Get_Current_Academic_Year_ID();
	$yearIDList = Get_Array_By_Key($academic_year_list, 'AcademicYearID');
	if(in_array($currYearID, $yearIDList)){
		$ChooseYear = $currYearID;
	}
}

$AcademicYearSelection = getSelectByArray($academic_year_list, $tag=' name="ChooseYear" class=formtextbox onChange="document.form1.submit();"', $ChooseYear, $all=0, $noFirst=0, $FirstTitle=$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'], $ParQuoteValue=1);

$TableContent = $libelibrary->getStudentReadingSummaryUI($StudentID, $ChooseYear);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("reading_records");
?>


<script language="JavaScript" type="text/JavaScript">

function jVIEW_DETAIL(jParRecType, jParParam)
{
	switch(jParRecType)
	{
		case 'reading_records':
			window.location = "details.php?" + jParParam + "&ChooseYear=" + document.form1.ChooseYear.value;
			break;
	}
}

</script>

<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top">
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
              <table border="0" cellpadding="0" cellspacing="6" width="100%">
				<tr>
					<td><?=$AcademicYearSelection?></td>
				</tr>
			  </table>
            </td>
          </tr>
          <tr> 
            <td align="center">
              <?=$TableContent?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
