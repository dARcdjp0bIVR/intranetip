<?php
/*	
 * 	Modifying by
 * 
 * 	2018-01-26 Cameron
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath."/src/includes/php/lib-table.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

$lpf = new libpf_sturec();

# Display according to user type
switch($_SESSION['UserType'])
{
	case 1:			# Teacher
		break;
	case 2:			# Student
		$StudentID = $_SESSION['UserID'];
		$CurrentPage = "Student_SchoolRecords";
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		$CurrentPage = "Parent_SchoolRecords";
		break;
	default:
		break;
}

$para = array();
$showYearTerm = '';
if ($AcademicYearID) {
	$lib_fcm = new academic_year($AcademicYearID);
	$startDate = $lib_fcm->AcademicYearStart;
	$endDate = $lib_fcm->AcademicYearEnd;
	$yearName = $lib_fcm->Get_Academic_Year_Name();
	$showYearTerm = $yearName.' '.$Lang['General']['WholeYear'];
	$para['Scope'] = 'Year';
	$para['Terms'] = $lib_fcm->Get_Term_List(0);
}
else if ($SemesterID) {
	$lib_fcm = new academic_year_term($SemesterID);
	$startDate = $lib_fcm->TermStart;
	$endDate = $lib_fcm->TermEnd;
	$yearName = $lib_fcm->Get_Academic_Year_Name();
	$yearTermName = $lib_fcm->Get_Year_Term_Name();
	$showYearTerm = $yearName.' '.$yearTermName;
	$para['Scope'] = 'Semester';
	$para['Terms'] = $yearTermName;
}
else {
	//	exit;
}

$libelibrary = new elibrary();

$para['StudentID'] = $StudentID;
$para['StartDate'] = $startDate;
$para['EndDate'] = $endDate;
$htmlResultTable = $libelibrary->getStudentReadingDetailsUI($para);


// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("reading_records");
?>


<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<FORM name="form1" method="POST">
    <tr>
      <td valign="top">
            <?
            echo $lpf->GET_TAB_MENU($TabMenuArr);
            ?>
		<table width="100%"  border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td>
			  <img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="index.php?ChooseYear=<?=$ChooseYear?>"><?= $ec_iPortfolio['title_reading_records']?></a>
			  <img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$showYearTerm?></span>
            </tr>
          <tr>
          <tr>
            <td align="center">
			<?=$htmlResultTable?>
			</td>
          </tr>

        </table>
		</td>
    </tr>
</FORM>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
