<?php
// Modified by 

/********************** Change Log ***********************/
#
#
#       Date    :       2010-08-24      [fai]
#						Remove upload offical photo function in iPortfolio
/********************** Change Log ***********************/

//SINCE UPLOAD OFFICAL PHOTO REMOVE FROM IPORTFOLIO , EXIT THE PROGRAM IN CASE USER HAS BOOKMARK THIS PAGE
echo "<!-- Invalid Access -->";
exit();

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:Photo"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$lf = new libfilesystem();

# Resetting page number
switch($FieldChanged)
{
	case "division":
		$Page = 1;
		break;
	case "page":
	default:
		break;
}

$FolderList = $lf->return_folderlist($intranet_root."/file/official_photo");
for($i=0; $i<count($FolderList); $i++)
{
	if($lf->file_ext($FolderList[$i])==".jpg")
		$RegNoArr[] = $lf->file_name($FolderList[$i]);
		
}

list($UploadedCount, $SummaryTable) = $li_pf->GEN_UPLOADED_PHOTO_STUDENTS(array($PageDivision, $Page), $RegNoArr);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

# Generate upload/remove result message
if(isset($_GET))
{
	# Remove
	if($msg == 1)
	{
		$error_msg = $linterface->GET_SYS_MSG("", $ec_iPortfolio['photo_removed_msg']);
	}
	# Uploaded
	else if($uploaded == 1)
	{
		if($wrong_format==1)
			$error_msg = $linterface->GET_SYS_MSG("", $ec_iPortfolio['student_photo_upload_warning']);
		else
		{
			$error_msg =	"
											<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"  class=\"systemmsg\">
												<tr>
													<td><font color=green>".$ec_iPortfolio['success_uploaded_photo_count'].":</font></td>
													<td><font color=green>".$success_count."</font></td>
												</tr>
												<tr>
													<td><font color=red>".$ec_iPortfolio['failed_uploaded_photo_count'].":</font></td>
													<td><font color=red>".$fail_count."</font></td>
												</tr>
											</table>
										";
		}
	}
}

?>


<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript">
function checkform(objForm){
	if(getFileExtension(objForm.userfile) == ".zip" || getFileExtension(objForm.userfile) == ".ZIP")
	{
		return true;
	} else
	{
		alert("<?=$ec_iPortfolio['student_photo_upload_warning']?>");
		return false;
	}
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "student_official_photo.php";
document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "student_official_photo.php";
	document.form1.submit();
}

function jBACKTO_LIST(){
	document.form1.action = "../../school_records_class.php";
	document.form1.submit();
}

function jREMOVE_PHOTO(jClassName){
	if(confirm("<?=$ec_iPortfolio['photo_remove_confirm']?>"))
	{
		document.form1.ClassName.value = jClassName;
		document.form1.action = "student_photo_remove.php";
		document.form1.submit();
	}
}

</script>

<form name="form1" enctype="multipart/form-data" action="student_photo_upload_update.php" method="POST" onSubmit="return checkform(this)">
	<table width="98%" border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation">
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$ec_iPortfolio['upload_photo']?>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td align="center">
							<table border="0" cellspacing="0" cellpadding="3" style="border:dashed 1px #000000">
<?php if($error_msg != "") { ?>
								<tr>
									<td><?=$error_msg?></td>
								</tr>
<?php } ?>
								<tr>
									<td align="left">
										<span class="tabletextrequire">
											<?=$ec_iPortfolio['student_photo_upload_remind']?>
										</span>
									</td>
								</tr>
								<tr>
									<td align="center">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td><?=$i_select_file?>: </td>
												<td><input type="file" name="userfile" id="fileField"></td>
												<td><input type="submit" class="formbutton" value="<?=$button_upload?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td height="320" align="left" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="right">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td height="50" align="right" class="tabletext">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td>
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td>&nbsp;</td>
																					<td align="right" valign="bottom">&nbsp;</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<?=$SummaryTable?>
																		</td>
																	</tr>
																	<tr class="tablebottom">
																		<td>
																			<table width="100%" border="0" cellspacing="0" cellpadding="3">
																				<tr>
																					<td align="left" class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER($UploadedCount, $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", $UploadedCount, $ec_iPortfolio['total_record'])?></td>
																					<td align="right">
																						<table border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td>
																									<table border="0" cellspacing="0" cellpadding="2">
																										<tr align="center" valign="middle">
																											<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp1" width="11" height="10" border="0" align="absmiddle" id="prevp1"></a> <span class="tabletext"> <?=$list_page?> </span></td>
																											<td class="tabletext">
																												<?=$li_pf->GEN_PAGE_SELECTION($UploadedCount, $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
																											</td>
																											<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp1" width="11" height="10" border="0" align="absmiddle" id="nextp1"></a></td>
																										</tr>
																									</table>
																								</td>
																								<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
																								<td>
																									<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
																										<tr>
																											<td><?=$i_general_EachDisplay?></td>
																											<td>
																												<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'")?>
																											</td>
																											<td><?=$i_general_PerPage?></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" />
	<input type="hidden" name="RegNoHiddenList" value="<?=implode(",", $RegNoArr)?>" />
	<input type="hidden" name="FieldChanged" />
</form>

<? // ===================================== Body Contents (END) ============================= ?>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
