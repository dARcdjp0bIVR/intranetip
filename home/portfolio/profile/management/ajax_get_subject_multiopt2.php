<?php
//Using: Pun
/**
 * Change Log:
 * 2016-02-24 Pun
 * 	-	New File
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$subject_selection_html = '';

#### Get yearID for class teacher START ####
if($yearClassId && !$y_id){
	$yc = new year_class($yearClassId);
	$y_id = $yc->YearID;
	
	$subject_selection_html .= '<input type="hidden" name="yearID" value="'.$y_id.'" />';
	
	$ay_id = Get_Current_Academic_Year_ID();
}
#### Get yearID for class teacher END ####

$li_pf = new libpf_asr();
$lpf = new libportfolio();

#### Include Group Subject START ####
$includeCodeIdArr = array_keys((array)$sys_custom['iPortfolio_Group_Subject']);
#### Include Group Subject END ####

#### Remove Group Subject START ####
$removeCodeIdArr = array();
foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $codeArr){
	$removeCodeIdArr = array_merge($removeCodeIdArr, $codeArr);
}
#### Remove Group Subject END ####

$subj_arr = $li_pf->returnAllSubjectWithComponents($ay_id, $y_id, $includeCodeIdArr, $removeCodeIdArr);

#### Filter Subject START ####
if(count($filterSubject) > 0){
	$filterResultArr = array();
	foreach($subj_arr as $subj){
		$subjectID = $subj['SubjectID'];
		if(in_array($subjectID, $filterSubject)){
			$filterResultArr[] = $subj;
		}
	}
	$subj_arr = $filterResultArr;
}
#### Filter Subject END ####
?>


<table width="600" border="0" cellspacing="0" cellpadding="3" style="background: #EEEEEE">
	<tbody>
		<tr>
			<td colspan="3">
				<input type="checkbox" id="SubjCheckMaster" class="checkMaster">
				<label for="SubjCheckMaster"><?=$button_check_all ?></label>
			</td>
		</tr>
		
		<?php 
		for ($i = 0, $iMax = count($subj_arr); $i < $iMax; $i++) {
			$subjName = $subj_arr[$i]['SubjectName'];
			$subjID = $subj_arr[$i]['SubjectID'];
			if($subj_arr[$i]['SubjectComponentID']){
				$subjName .= " - {$subj_arr[$i]['SubjectComponentName']}";
				$subjID = $subj_arr[$i]['SubjectComponentID'];
			}
			
			if($i % 3 == 0){ 
		?>
			<tr>
		<?php 
			}
		?>
			
			<td width="200">
				<input type="checkbox" id="subj_<?=$i ?>" name="subjID[]" value="<?=$subjID ?>" class="subjID">
				<label for="subj_<?=$i ?>"><?=$subjName ?></label>
			</td>
			
		<?php 
			if($i % 3 == 2){ 
		?>
			</tr>
		<?php
			}
		}
		?>
		
	</tbody>
</table>





<?php
intranet_closedb();
?>