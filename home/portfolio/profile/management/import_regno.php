<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:Student"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_opendb();

$li_pf = new libportfolio();

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();
?>


<!-- ===================================== Body Contents ============================= -->

<script language="JavaScript">
function checkform(objForm){
	if (!check_text(objForm.userfile, "<?=$i_alert_pleaseselect.$ec_iPortfolio['SAMS_CSV_file']?>"))
	{
		return false;
	}
	
	var extension = getFileExtension(objForm.userfile).toUpperCase();
	if (extension != ".CSV" && extension != ".TXT")
	{
		alert("<?=$ec_guide['import_error_wrong_format']?>");
		return false;
	}
	
	return true;	
}
</script>

<FORM enctype="multipart/form-data" action="import_regno_update.php" method="POST" name="form1" onSubmit="return checkform(this)">
	<table width="420" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td align="right" nowrap><?=$ec_iPortfolio['SAMS_CSV_file']?>:</td>
			<td><input class="file" type="file" name="userfile" size="25" /></td>
		</tr>
		<tr>
			<td align="right" valign="top"><?=$ec_iPortfolio['SAMS_import_form']?>:</td>
			<td>
				<input type=radio name="format" id="format_1" value="1" CHECKED><label for="format_1"> LoginName, RegNo </label><br />
				<input type=radio name="format" id="format_2" value="2"><label for="format_2"> ClassName, ClassNumber, RegNo </label>
			</td>
		</tr>
	</table>

	<br /><br />
	<hr />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><a href="download.php?FileName=student_regno_sams_sample1.csv" target="_blank" class="tablelink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?> (1)</a></td>
			<td align="left">
				<input class="formbutton" type="submit" value="<?=$button_submit?>" />
				<input class="formbutton" type="reset" value="<?=$button_reset?>" />
				<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="self.location='../../school_records_class.php?YearClassID=<?=$YearClassID?>'" />
			</td>
		</tr>
		<tr>
			<td colspan=2><a href="download.php?FileName=student_regno_sams_sample2.csv" target="_blank" class="tablelink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?> (2)</a></td>
		</tr>
	</table>
	<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" >
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
