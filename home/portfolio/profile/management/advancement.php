<?php
// Modifing by
/**
 * Change Log:
 * 2020-07-30 Philips
 *  - use Fr instead of From
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_management_advancement_page_size", "numPerPage");
$arrCookies[] = array("ck_management_advancement_page_number", "pageNo");
$arrCookies[] = array("ck_management_advancement_page_order", "order");
$arrCookies[] = array("ck_management_advancement_page_field", "field");
$arrCookies[] = array("ck_management_advancement_YearID", "YearID");
$arrCookies[] = array("ck_management_advancement_YearClassID", "YearClassID");
$arrCookies[] = array("ck_management_advancement_FrAcademicYearID", "FrAcademicYearID");
$arrCookies[] = array("ck_management_advancement_FrYearTermID", "FrYearTermID");
$arrCookies[] = array("ck_management_advancement_FrSubjectID", "FrSubjectID");
$arrCookies[] = array("ck_management_advancement_ToAcademicYearID", "ToAcademicYearID");
$arrCookies[] = array("ck_management_advancement_ToYearTermID", "ToYearTermID");
$arrCookies[] = array("ck_management_advancement_ToSubjectID", "ToSubjectID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_management_advancement_page_size = '';
}
else 
	updateGetCookies($arrCookies);

iportfolio_auth("T");
intranet_opendb();

$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$lpf_ui = new libportfolio_ui();

########################################################
# Report printing options: Start
########################################################
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

# Form Selection
$FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);

# retrieve distinct years
$FrAcademicYearSelection = getSelectAcademicYear('FrAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'Fr\')"', $noFirst=1, $noPastYear=0, $FrAcademicYearID);
$ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'To\')"', $noFirst=1, $noPastYear=0, $ToAcademicYearID);


################################################################################
// Subject Selection
$FrSubjectSelection = $libSCM_ui->Get_Subject_Selection('FrSubjectID', $FrSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
$ToSubjectSelection = $libSCM_ui->Get_Subject_Selection('ToSubjectID', $ToSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);


################################################################################

########################################################
# Report printing options: End
########################################################


# View Button
$linterface = new interface_html();
$btn_View = $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", $onclick="js_Reload_DBTable();", $id="Btn_View");


# define the navigation, page title and table size
// template for teacher page
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# get tab menu
$TabIndex = "AcademicProgress";
$TabMenuArr = libpf_tabmenu::getAssessmentStatTags($TabIndex);
$html_tab_menu = libportfolio_ui::GET_TAB_MENU($TabMenuArr);

$linterface->LAYOUT_START();
?>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTermID = '<?=$FrYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	js_Reload_Term_Selection('Fr');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: jsCurYearID,
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 1
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Assessment_Statistics_Report_DBTable',
			YearID: jsCurYearID,
			YearClassID: $('select#YearClassID').val(),
			FrAcademicYearID: jsCurFrAcademicYearID,
			FrYearTermID: jsCurFrYearTermID,
			FrSubjectID: $('select#FrSubjectID').val(),
			ToAcademicYearID: jsCurToAcademicYearID,
			ToYearTermID: jsCurToYearTermID,
			ToSubjectID: $('select#ToSubjectID').val(),
			field: '<?=$field?>',
			order: '<?=$order?>',
			pageNo: '<?=$pageNo?>'
		},
		function(ReturnData)
		{
			
		}
	);
}

$(function(){
	$('#FrSubjectID').change(function(){
		$('#ToSubjectID').val($(this).val());
	});
});
</script>

<form id="form1" name="form1" method="POST" action="advancement.php">
	<?=$html_tab_menu ?>
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr id="level1">
				<td>
					<table border="0" cellspacing="0" cellpadding="5" width="100%">
						<tr>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238); width:30%;" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
							<td valign="top"><?=$FormSelection?></td>
						</tr>
						<tr id='classRow'>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
							<td valign="top"><div id="ClassSelectionDiv"></div></td>
						</tr>
						<tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id="level2">
				<td>
					<table border="0" cellspacing="0" cellpadding="5" width="100%">
						<tr>
							<td colspan="2"><em class="form_sep_title"> - <?=$Lang['General']['Fr']?> -</em></td>
							<td colspan="2"><em class="form_sep_title"> - <?=$Lang['General']['To']?> -</em></td>
						<tr>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td><div style="float:left;"><?=$FrAcademicYearSelection?></div><div id="FrYearTermSelectionDiv" style="float:left;"></div></td>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td><div style="float:left;"><?=$ToAcademicYearSelection?></div><div id="ToYearTermSelectionDiv" style="float:left;"><div></td>
						</tr>
						<tr>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td><?=$FrSubjectSelection?></td>
							<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td><?=$ToSubjectSelection?></td>
						</tr>
						<tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>&nbsp;</td>
							<td align="center">
								<?= $btn_View ?>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="DBTableDiv"><?=$h_DBTable?></div>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>