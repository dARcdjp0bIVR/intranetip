<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:Photo"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$lf = new libfilesystem();

#######################################################
function removeFiles($ParAll, $ParRegNoArr="")
{
	global $lf, $intranet_root;

	$dest = $intranet_root."/file/official_photo";
	if (file_exists($dest))
	{
		$handle=opendir($dest);
		if($ParAll==1)
		{
			while (($file = readdir($handle))!==false)
			{
				if($file!="." && $file!="..")
				{
					$filepath = $dest."/".$file;
					$lf->file_remove($filepath);
				}
			}
		}
		else
		{
			for($i=0; $i<sizeof($ParRegNoArr); $i++)
			{
				$RegNo = trim($ParRegNoArr[$i]);
				$filepath = $dest."/".$RegNo.".jpg";
				if(file_exists($filepath))
				{
					$lf->file_remove($filepath);
				}
			}
		}
	}
}

#######################################################

if($ClassName=="ALL")
{
	removeFiles(1);
}
else
{
	# get uploaded file
	$UploadedArr1 = $li_pf->GET_UPLOADED_PHOTO_STUDENTS_REGNO($ClassName, $RegNoHiddenList);

	if($ClassName=="UNKNOWN")
	{
		# archived
		$UploadedArr2 = $li_pf->GET_UPLOADED_PHOTO_ARCHIVE_STUDENTS_REGNO($RegNoHiddenList);

		if (is_array($UploadedArr2) && sizeof($UploadedArr2)>0)
		{
			if (is_array($UploadedArr1) && sizeof($UploadedArr1)>0)
			{
				$UploadedArr = array_merge($UploadedArr1, $UploadedArr2);
			} else
			{
				$UploadedArr = $UploadedArr2;
			}
		} else
		{
			$UploadedArr = $UploadedArr1;
		}

		$RegNoArr = explode(",", $RegNoHiddenList);

		$UploadedArr3 =  array_values(array_diff($RegNoArr, $UploadedArr));
		$UploadedArr = $UploadedArr3;
	} else
	{
		$UploadedArr = $UploadedArr1;
	}

	removeFiles(0, $UploadedArr);
}

intranet_closedb();
header("Location: student_official_photo.php?msg=1");
?>
