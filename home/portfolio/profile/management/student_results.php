<?php
// using : Pun
/**
 * Change Log: 
 * 2016-01-26 Pun
 * 	-	Added export CSV
 * 	-	Added display "Teacher"
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_management_stdprogress_page_size", "numPerPage");
$arrCookies[] = array("ck_management_advancement_page_number", "pageNo");
$arrCookies[] = array("ck_management_advancement_page_order", "order");
$arrCookies[] = array("ck_management_advancement_page_field", "field");
$arrCookies[] = array("ck_management_advancement_YearID", "YearID");
$arrCookies[] = array("ck_management_advancement_YearClassID", "YearClassID");
$arrCookies[] = array("ck_management_advancement_FromAcademicYearID", "FromAcademicYearID");
$arrCookies[] = array("ck_management_advancement_FromYearTermID", "FromYearTermID");
$arrCookies[] = array("ck_management_advancement_FromSubjectID", "FromSubjectID");
$arrCookies[] = array("ck_management_advancement_ToAcademicYearID", "ToAcademicYearID");
$arrCookies[] = array("ck_management_advancement_ToYearTermID", "ToYearTermID");
$arrCookies[] = array("ck_management_advancement_ToSubjectID", "ToSubjectID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_management_stdprogress_page_size = '';
}
else 
	updateGetCookies($arrCookies);

iportfolio_auth("T");
intranet_opendb();

$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$lpf_ui = new libportfolio_ui();
$lpf = new libportfolio();


########### Assess Right START ###########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classSelectionHTML = '<select id="YearClassID" name="YearClassID" onchange="js_Reload_Student_Selection()">';
foreach($accessRight['classTeacher'] as $yearClass){
	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$classSelectionHTML .= "<option value=\"{$yearClass['YearClassID']}\">{$name}</option>";
}
$classSelectionHTML .= '</select>';
########### Access Right END ###########


########################################################
# Report printing options: Start
########################################################
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

# Form Selection
$FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);

# retrieve distinct years
$FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'From\')"', $noFirst=1, $noPastYear=0, $FromAcademicYearID);
$ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'To\')"', $noFirst=1, $noPastYear=0, $ToAcademicYearID);


################################################################################
// Subject Selection
$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
$ToSubjectSelection = $libSCM_ui->Get_Subject_Selection('ToSubjectID', $ToSubjectID, $OnChange='', $noFirst=0, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);


################################################################################

########################################################
# Report printing options: End
########################################################


# View Button
$linterface = new interface_html();
$btn_View = $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", $onclick="js_Reload_DBTable();", $id="Btn_View");


# define the navigation, page title and table size
// template for teacher page
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");


//debug_r($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]);
# get tab menu
$TabIndex = "AssessmentResultOfStd";
$TabMenuArr = libpf_tabmenu::getAssessmentStatTags($TabIndex);
$html_tab_menu = libportfolio_ui::GET_TAB_MENU($TabMenuArr);

$linterface->LAYOUT_START();

/*
$SubjectID = 21; // for overall result
$YearIDs = array(9, 14);
//$DataType = "TERM"; //"ASSESSMENT"; 
$DataType = "ASSESSMENT";
//$TermIDs = "";
$ClassID = 367;
//$IsMain = true;
//$h_DBTable =  $li_pf->GetClassCrossYearBySubject($YearIDs, $SubjectID, $ClassID, $DataType);
 * 
 */
?>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	js_Reload_Term_Selection('From');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: "js_Reload_Student_Selection()",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}



function js_Reload_Student_Selection()
{
	$('div#StudentSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Student_Selection',		
			SelectedYearClassID: $('select#YearClassID').val(),
			SelectionID: 'StudentID',
			OnChange: "",
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			//jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}
$(function(){
	js_Reload_Student_Selection();
});

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
	if($('#StudentID').length == 0 || $('#StudentID').val() == ''){
		alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
		return;
	}
	if($('input[name="filterColumn\[\]"]:checked').length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectColumn']?>');
		return;
	}
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_get_mark_analysis.php", 
		/*{
			Action: 'STUDENT_RESULTS',
			StudentID: $('select#StudentID').val()			
		},*/
		$("#form1").serialize(),
		function(ReturnData)
		{
			
		}
	);
}


$("#filterAll").click(function(){
	if($(this).attr("checked")){
		$("#filterMark").attr("checked", "checked");
		$("#filterMarkDiff").attr("checked", "checked");
		$("#filterStandardScore").attr("checked", "checked");
		$("#filterStandardScoreDiff").attr("checked", "checked");
		$("#filterFormPosition").attr("checked", "checked");
	}else{
		$("#filterMark").removeAttr("checked");
		$("#filterMarkDiff").removeAttr("checked");
		$("#filterStandardScore").removeAttr("checked");
		$("#filterStandardScoreDiff").removeAttr("checked");
		$("#filterFormPosition").removeAttr("checked");
	}
});

function exportCSV(){
	$('#isExport').val(1);
	$('#form1').submit();
	$('#isExport').val(0);
}
</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" action="/home/portfolio/profile/management/ajax_get_mark_analysis.php" method="POST" target="_blank">
	<input type="hidden" name="Action" value="STUDENT_RESULTS" />
	<input type="hidden" id="isExport" name="isExport" value="0" />
	<?=$html_tab_menu ?>
	
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px">
		
<?php if($accessRight['admin']){ ?>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
				<td valign="top"><?=$FormSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top"><div id="ClassSelectionDiv"></div></td>
			</tr>
<?php }else{ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top">
					<?=$classSelectionHTML?>
				</td>
			</tr>
<?php } ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort['usertype_s']?></span></td>
				<td valign="top"><div id="StudentSelectionDiv"></div></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['display']?></span></td>
				<td valign="top">
					<input type="checkbox" id="filterAll" />
					<label for="filterAll"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					<input type="checkbox" id="filterTeacher" name="filterColumn[]" value="filterTeacher" />
					<label for="filterTeacher"><?=$iPort['report_col']['teacher']?></label>&nbsp;
					
					<input type="checkbox" id="filterMark" name="filterColumn[]" value="filterMark" />
					<label for="filterMark"><?=$Lang['iPortfolio']["Score"]?></label>&nbsp;
					
					<?php if($sys_custom['iPf']['Report']['AssessmentStatisticReport']['showMarksDifferent']){ ?>
					<input type="checkbox" id="filterMarkDiff" name="filterColumn[]" value="filterMarkDiff" />
					<label for="filterMarkDiff"><?=$Lang['iPortfolio']["MarkDifference"]?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterStandardScore" name="filterColumn[]" value="filterStandardScore" />
					<label for="filterStandardScore"><?=$Lang['iPortfolio']["StandardScore"]?></label>&nbsp;
					
					<input type="checkbox" id="filterStandardScoreDiff" name="filterColumn[]" value="filterStandardScoreDiff" />
					<label for="filterStandardScoreDiff"><?=$Lang['iPortfolio']["StandardScoreDifference"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPosition" name="filterColumn[]" value="filterFormPosition" />
					<label for="filterFormPosition"><?=$Lang['iPortfolio']["FormPosition"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPositionDiff" name="filterColumn[]" value="filterFormPositionDiff" />
					<label for="filterFormPositionDiff"><?=$Lang['iPortfolio']["FormPositionDifference"]?></label>&nbsp;
				</td>
			</tr>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px">
		<?= $btn_View ?>
	</div>
	
	<div id="DBTableDiv"><?=$h_DBTable?></div>
	
</form>

<script>
$('#filterAll').change(function(){
	if($(this).attr('checked')){
		$('input[name="filterColumn\[\]"]').attr('checked','checked');
	}else{
		$('input[name="filterColumn\[\]"]').removeAttr('checked');
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>