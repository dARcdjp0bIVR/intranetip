<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
$CLASSNUMBER_FIELD = getClassNumberField('ycu.');

if(strstr($SubjectCode, "###")==true)
{
	$SubjectArr = explode("###", $SubjectCode);
	if(sizeof($SubjectArr)>0)
	{
		$SubjectCode = $SubjectArr[0];
		$SubjectComponentCode = $SubjectArr[1];		
	}
}

//$Semester = $_GET['Semester'];
/*
if($Form!="")
{	
	$ClassName = $Form;
}
*/
////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array($CLASSNUMBER_FIELD, "StudentName", "ASSR.Score", "ASSR.Grade", "ASSR.OrderMeritForm");

$conds = ($SubjectComponentCode=="") ? " AND (ASSR.SubjectComponentCode IS NULL OR ASSR.SubjectComponentCode = '')" : " AND ASSR.SubjectComponentCode = '$SubjectComponentCode'";
if($Semester!="")
{
	$conds .= ($Semester==$ec_iPortfolio['overall']|| $Semester==$ec_iPortfolio['overall_result']) ? " AND ASSR.IsAnnual = 1" : " AND ASSR.IsAnnual = 0 AND ASSR.Semester = '$Semester'";
}
//$ec_iPortfolio['overall_result']

############################################

$sql =	"
					SELECT
						$CLASSNUMBER_FIELD as ClassNumber,
						if(iu.UserID IS NULL, CONCAT(iau.EnglishName, '<br>', iau.ChineseName), CONCAT(iu.EnglishName, '<br>', iu.ChineseName)) AS StudentName,
						ASSR.Score,
						ASSR.Grade,
						ASSR.OrderMeritForm
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as ASSR
					LEFT JOIN
						{$intranet_db}.INTRANET_USER as iu
					ON
						ASSR.UserID = iu.UserID
					LEFT JOIN
						{$intranet_db}.INTRANET_ARCHIVE_USER as iau
					ON
						ASSR.UserID = iau.UserID
					inner join {$intranet_db}.YEAR_CLASS_USER as ycu on ASSR.UserID = ycu.UserID
					inner join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					inner join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					inner join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
						and ASSR.Year = ay.YearNameEN
					WHERE
						ASSR.Year = '$Year' AND
						$CLASSNAME_FIELD = '$ClassName' AND
						ASSR.SubjectCode = '$SubjectCode'
						$conds
				";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $usertype_s;
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
//$li->page_size = 999;
$li->no_col = 6;
//$li->noNumber = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table_b'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</span></td>\n";
$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['number'], 1)."</td>\n";
$li->column_list .= "<td width='35%'>".$li->column(2, $usertype_s, 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(3, $ec_iPortfolio['score'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['grade'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['rank'], 1)."</td>\n";
$li->column_array = array(0,0,0,0,0,0);

$pageSizeChangeEnabled = true;
//////////////////////////////////////////////
# Setting of the Selection Boxes
// retrieve all subjects
$SubjectArray = $li_pf->returnAllSubjectWithComponents();
if($SubjectCode=="")
{
	for($i=0; $i<sizeof($SubjectArray); $i++)
	{
		if($SubjectArray[$i]["have_score"]==1)
		{
			$SubjectCode = $SubjectArray[$i][0];
			$SubjectComponentCode = $SubjectArray[$i][1];
			break;
		}
	}
}
else
{
	$temp = explode("###", $SubjectCode);
	if(sizeof($temp)>1)
	{	
		if($SubjectComponentCode=="")
		{
			$SubjectCode = trim($temp[0]);
			$SubjectComponentCode = trim($temp[1]);
		}
	}
}

list($SubjectSelection, $SubjectName) = $li_pf->getSubjectSelection($SubjectArray, "name='SubjectCode' onChange='document.form1.submit();'", $SubjectCode, $SubjectComponentCode);
// retrieve distinct years
$YearArray = $li_pf->returnAssessmentYear($SubjectCode, $SubjectComponentCode);
$Year = ($Year=="" || !in_multi_array($Year, $YearArray)) ? $YearArray[0][0] : $Year;
//$YearArray[] = array($Year,$Year);

//$year_selection = (sizeof($YearArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($YearArray, "name='Year' onChange='document.form1.submit();'", $ec_iPortfolio['school_yr'], $Year);
$year_selection = (sizeof($YearArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArray, "name='Year' onChange='document.form1.submit();'", $Year,0,1);

/////////////////////////////////////////////////
// retrieve distinct form
$FormArray = $li_pf->returnAssessmentForm($SubjectCode, $SubjectComponentCode, $Year);
/*
$classArr = $li_pf->returnAssessmentClass($SubjectCode, $SubjectComponentCode);
for($i=0; $i<sizeof($classArr); $i++)
{
	$FormArray[] = array($classArr[$i], $classArr[$i]);
}
*/
//$Form = ($Form=="" || !in_multi_array($Form, $FormArray)) ? $FormArray[0][0] : $Form;

//$form_selection = (sizeof($FormArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($FormArray, "name='Form' onChange='document.form1.submit();'", $ec_iPortfolio['whole_school'], $Form);
$form_selection = (sizeof($FormArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($FormArray, "name='Form' onChange='document.form1.submit();'", $ClassName,0,1);

/////////////////////////////////////////////////
// retrieve semester selection list
$SemesterArray = $li_pf->returnAssessmentSemester($SubjectCode, $SubjectComponentCode, $Year);
$Semester = ($Semester!="" && !in_multi_array($Semester, $SemesterArray)) ? $SemesterArray[0][0] : $Semester;
$LastSemester = $SemesterArray[sizeof($SemesterArray)-1];

$semester_selection = (sizeof($SemesterArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($SemesterArray, "name='Semester' onChange='document.form1.submit();'", "-- ".$range_all." --", $Semester);
//$semester_selection = (sizeof($SemesterArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($SemesterArray, "name='Semester' onChange='document.form1.submit();'", $Semester,0,1);

$luser = new libuser($UserID);

# define the navigation
// template for teacher page
$linterface = new interface_html("popup.html");
$CurrentPage = "ole_report_stat";
$title = $iPort['menu']['assessment_statistic_report'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

<FORM name="form1" method="POST" action='stat_student_list.php'>          

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="navigation"  colspan="3">
<!--
			<img src="<?=$PATH_WRT_ROOT?>/images/2009a/nav_arrow.gif" align="middle" height="15" width="15">
			<a href="class_performance_stat.php?SubjectCode=<?=urlencode($SubjectCode)?>&SubjectComponentCode=<?=urlencode($SubjectComponentCode)?>&Year=<?=$Year?>&ClassName=S<?=$Form?>&Semester=<?=$Semester?>"><?=$ec_iPortfolio['all_statistic'] ?></a>
			<img src="<?=$PATH_WRT_ROOT?>/images/2009a/nav_arrow.gif" align="middle" height="15" width="15">
			<a href="stat_class.php?SubjectCode=<?=urlencode($SubjectCode)?>&SubjectComponentCode=<?=urlencode($SubjectComponentCode)?>&Year=<?=$Year?>&Form=<?=$Form?>&Semester=<?=$Semester?>">Form <?=$Form ?></a>
-->
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
							<table border="0" cellpadding="5">
								<tbody>
									<tr>
										<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio_Report['subject']?> : <strong> <?=$SubjectCode." ".$SubjectComponentCode?></strong> </td>
										<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['year']?> :<strong> <?=$Year?></strong></td>
										<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['semester']?> : <strong><?=$Semester?></strong></td>
<?php if($ClassName!="") { ?>
										<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['class']?> : <strong><?=$ClassName?></strong></td>
<?php } ?>
									</tr>
								</tbody>
							</table>
						</td>
						<td align="right">&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td height="10">&nbsp;</td>
		<td align="center">
			<?= $li->displayPlain() ?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
				<tr class='tablebottom'>
					<td class="tabletext" align="right"><?=$li->navigation(1)?></td>
				</tr>
<?php } ?>
			</table>
		</td>
		<td height="10">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="Year" value="<?=$Year?>">
<input type="hidden" name="Semester" value="<?=$Semester?>">
<input type="hidden" name="SubjectCode" value="<?=$SubjectCode?>">
<input type="hidden" name="SubjectComponentCode" value="<?=$SubjectComponentCode?>">


<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=numPerPage value="<?=$numPerPage?>">
<input type=hidden name="page_size_change">

<!--
<input type=hidden name=Page value="<?=$Page ?>" />
<input type=hidden name=PageDivision value="<?=$PageDivision ?>" />
-->
</FORM>

<?
/*
echo "SubjectCode: ".$SubjectCode."<br/>";
echo "Year: ".$Year."<br/>";
echo "Form: ".$Form."<br/>";
echo "ClassName: ".$ClassName."<br/>";
echo "Semester: ".$Semester."<br/>";
*/
?>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
