<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$lpf_ui = new libpf_asr();
$li = new libdbtable2007($field, $order, $pageNo);

# Retrieve all subjects
$SubjectArray = $li_pf->returnAllSubjectWithComponents();
if($SubjectCode=="")
{
	for($i=0; $i<sizeof($SubjectArray); $i++)
	{
		if($SubjectArray[$i]["have_score"]==1)
		{
			$SubjectCode = $SubjectArray[$i][0];
			$SubjectComponentCode = $SubjectArray[$i][1];
			break;
		}
	}
}
else
{
	$temp = explode("###", $SubjectCode);
	if(sizeof($temp)>1)
	{	
		if($SubjectComponentCode=="")
		{
			$SubjectCode = trim($temp[0]);
			$SubjectComponentCode = trim($temp[1]);
		}
	}
}

# Resetting page number
switch($FieldChanged)
{
	case "page":
		break;
	case "subject":
		$Year = "";
	case "year":
		$Form = "";
	case "form":
	case "division":
	default:
		$pageNo = 1;
		break;
}
if(isset($FormHidden2))
	$Form = $FormHidden2;

// $form = $_GET['form'];
list($SubjectSelection, $SubjectName) = $li_pf->getSubjectSelection($SubjectArray,"name='SubjectCode' onChange='jCHANGE_FIELD(\"subject\");'",$SubjectCode,$SubjectComponentCode);
$summary_table = $lpf_ui->generateSchoolSummary($SubjectCode,$SubjectComponentCode,array($PageDivision,$pageNo),$Year,$Form,$Style);

$pageSizeChangeEnabled = true;

# Retrieve Numner of Rows of Table
if($Style==1)
	$lp_template_list = $li_pf->returnSchoolSummaryData($SubjectCode,$SubjectComponentCode,$Year,$Form,1,1);
else
	$lp_template_list = $li_pf->returnSchoolSummaryData($SubjectCode,$SubjectComponentCode,$Year,$Form,1,0);

//$YearSelect = getSelectByArrayTitle($YearArr, "name='Year' onChange='document.form1.submit();'",$i_ReportCard_All_Year,$Year);
$YearSelect = $li_pf->GEN_YEAR_SELECTION($SubjectCode,$SubjectComponentCode,$Year);
$FormSelect = $li_pf->GEN_CLASSNAME_SELECTION($SubjectCode,$SubjectComponentCode,$Year,$Form);

# define the navigation, page title and table size
# template for teacher page
$linterface = new interface_html();
# set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("score_list.php", $ec_iPortfolio['master_score_list'], 0);
$TabMenuArr[] = array("overall_performance_stat.php", $ec_iPortfolio['overall_perform_stat'], 0);
$TabMenuArr[] = array("class_performance_stat.php", $ec_iPortfolio['class_perform_stat'], 1);
$TabMenuArr[] = array("student_performance_stat.php", $ec_iPortfolio['student_perform_stat'], 0);
?>
<script language="javascript">
// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "class_performance_stat.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "class_performance_stat.php";
	document.form1.submit();	
}

// Change display style
function jCHANGE_STYLE(jParStyle){
	document.form1.Style.value = jParStyle;
	document.form1.action = "class_performance_stat.php";
	document.form1.submit();
}

// Change page to display class statistic
function jTO_STAT_CLASS(jParYear, jParLevel){
	document.form1.Year.value = jParYear;
	document.form1.FormHidden.value = jParLevel;
	document.form1.Style.value = '';
	document.form1.action = "stat_class.php";
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="class_performance_stat.php" onSubmit="return checkform(this)">
<?
echo $lpf_ui->GET_TAB_MENU($TabMenuArr);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<span class="thumb_list">
							&nbsp;&nbsp;<?=$SubjectSelection?>
							&nbsp;<?=$YearSelect?>
							&nbsp;<?=$FormSelect?>
						</span>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td width="40">&nbsp;</td>
			<td>
				<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td class="navigation"><img src="<?=$PATH_WRT_ROOT?>/images/2009a/nav_arrow.gif" align="middle" height="15" width="15"> <?=$ec_iPortfolio['all_statistic']?></td>
							<td class="thumb_list" align="right" valign="top">
								<?=$ec_iPortfolio['group_by'] ?>:
<?php if($Style==1) { ?>
								<a href="javascript:jCHANGE_STYLE(0)"><?=$ec_iPortfolio['school_year']?> </a> | <span> <?=$ec_iPortfolio['form']?></span>
<?php } else { ?>
								<span><?=$ec_iPortfolio['school_year']?> </span> | <a href="javascript:jCHANGE_STYLE(1)"> <?=$ec_iPortfolio['form']?></a>
<?php } ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="40">&nbsp;</td>
		</tr>
		<tr>
			<td width="40">&nbsp;</td>
			<td align="center">
				<?=$summary_table?>
			</td>
			<td width="40">&nbsp;</td>
		</tr>
		<tr>
			<td width="40">&nbsp;</td>
			<td>
				<table border=0 width="100%" cellspacing="0" cellpadding="0">
					<tr class="tablebottom">
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tabletext"><?=$ec_iPortfolio['record']?> <?=$lpf_ui->GEN_PAGE_ROW_NUMBER(count($lp_template_list), $PageDivision, $pageNo)?>, <?=str_replace("<!--NoRecord-->", count($lp_template_list), $ec_iPortfolio['total_record'])?></td>
									<td align="right">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td>
																<a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp25" width="11" height="10" border="0" align="absmiddle" id="prevp25"></a> <span class="tabletext"> <?=$list_page?> </span>
															</td>
															<td class="tabletext">
																<?=$lpf_ui->GEN_PAGE_SELECTION(count($lp_template_list), $PageDivision, $pageNo, "name='pageNo' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
															</td>
															<td>
																<span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp25" width="11" height="10" border="0" align="absmiddle" id="nextp25"></a>
															</td>
														</tr>
													</table>
												</td>
												<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
												<td>
													<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
														<tr>
															<td><?=$i_general_EachDisplay?></td>
															<td>
																<?=$lpf_ui->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'")?>
															</td>
															<td><?=$i_general_PerPage?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="40">&nbsp;</td>
		</tr>
	</tbody>
</table>

<input type="hidden" name="FieldChanged" />		
<input type="hidden" name="Style" value="<?=$Style ?>" />		
<input type="hidden" name="FormHidden" value="<?=$Form ?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
