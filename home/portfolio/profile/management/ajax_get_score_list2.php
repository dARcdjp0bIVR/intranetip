<?php
// Using: 
/**
 * Change Log:
 * 2017-12-20 (Anna)
 * - Added $StudentIDconds, change student sort
 * 2017-12-12 (Pun)
 * - Add cust T1A3 hardcode full mark
 * 2017-09-12 (Omas)
 * - fix cannot show rank problem Y125606
 * 2017-06-13 (Villa) #Z112708
 * - Set Default Bias to 1/ Fullmarks to 100 for all subject to handle the display error if user have not set the full mark before
 * 2017-06-02 (Villa)
 * - Add convert to 100 Related #Z112708
 * - Add new highchart mode: Box Chart #B111270
 * 2017-05-10 (Villa) #T116799 
 * modified the access right checking for class teacher in handling the change of passing in ClassName must be in Eng
 * 2017-02-21 (villa)
 * add arhive teacher can be shown with star
 * 2016-11-24 (Villa)
 * add new calculation SD mean method in stead of using SD_MEAN TB for advanced searching
 * 2016-11-23 (Villa)
 * Add subject teacher
 * 2016-10-31 (Omas)
 * - fix archive student cannot show name problem
 * 2016-09-28 (Omas)
 * - fix rank -1 problem
 * 2016-06-03 (Cara)
 * - Change graph rendering plugin, data table's CSS
 * 2016-02-26 (Pun)
 * - Added barchart and summary table
 * 2016-01-20 (Pun)
 * - New File, rewrite ajax_get_score_list.php
 */
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/subject_class_mapping.php");
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/cust/analysis_system_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
intranet_auth ();
intranet_opendb ();

// ######### Init START ##########
$li = new libdb ();
$lpf = new libportfolio ();
$scm = new subject_class_mapping ();
$json = new JSON_obj ();
$libpf_exam = new libpf_exam();
$libinterface = new interface_html();


if (strstr ( $YearTermID, "_" )) {
	$tmpArr = explode ( "_", $YearTermID );
	$YearTermID = ( int ) $tmpArr [0];
	$TermAssessment = trim ( $tmpArr [1] );
}

$ResultType = ( array ) $ResultType;
// ### Fail Grade Setting START ####
$failGrade = explode ( ",", preg_replace ( "/[ ]*/", "", $grade_highlight ) );
$failGrade = array_map ( 'strtoupper', $failGrade );
$failGradeStyle = '';

$_bold = isset ( $range_bold );
$_italic = isset ( $range_italic );
$_underline = isset ( $range_underline );
$_color = (isset ( $range_color ) ? $r_color : NULL);

$failGradeStyle .= ($_bold) ? "font-weight:bold;" : "";
$failGradeStyle .= ($_italic) ? "font-style:italic;" : "";
$failGradeStyle .= ($_underline) ? "text-decoration:underline;" : "";
$failGradeStyle .= isset ( $_color ) ? "color:{$_color};" : "";

##### Set Advanced_Search Array START #####
// $Advanced_Search = array();
if($Advanced_Search!=''){
	$Advanced_Search_filter['Advanced_Search'] = $Advanced_Search;
}else{
	$Advanced_Search_filter['Advanced_Search'] = '0';
}
foreach((array)$gender as $_gender){
	$gender_arr[] = "'".$_gender."'";
}
$Advanced_Search_filter['gender'] = $gender_arr;
$Advanced_Search_filter['non_chinese'] = $non_chinese;
$Advanced_Search_filter['come_from_china'] = $come_from_china;
$Advanced_Search_filter['cross_boundary'] =$cross_boundary;
$Advanced_Search_filter['SEN'] = $SEN_filter;
##### Set Advanced_Search Array END #####

// ### Fail Grade Setting END ####
// ######### Init END ##########

// ######### Access Right START ##########
$accessRight = $lpf->getAssessmentStatReportAccessRight ();
$canAccess = true;
$currentAcademicYearID = Get_Current_Academic_Year_ID ();

if (! $accessRight ['admin']) {
	if (count ( $accessRight ['subjectPanel'] )) {
		// ### Subject Panel access right START ####
		
		// # Filter subject for subject panel START ##
		$filterSubjectArr = array_keys ( $accessRight ['subjectPanel'] );
		// # Filter subject for subject panel END ##
		
		// # Remove filter if teacher is viewing his class START ##
		if ($academicYearID == $currentAcademicYearID) {
			$ClassNameDB = ($intranet_session_language == "EN") ? "ClassTitleEN" : "ClassTitleB5";
// 			$ClassNameDB = "ClassTitleEN";
			$classNameArr = array ();
			foreach ( $accessRight ['classTeacher'] as $yearClass ) {
				$classNameArr [] = $yearClass [$ClassNameDB];
			}
			if (in_array ( $ClassName, $classNameArr )) {
				$filterSubjectArr = array ();
			}
		}
		// # Remove filter if teacher is viewing his class END ##
		
		// ### Subject Panel access right END ####
	} else {
		// ### Class teacher access right START ####
		
		if ($academicYearID != $currentAcademicYearID) {
			$canAccess = false;
		}
// 		$ClassNameDB = ($intranet_session_language == "EN") ? "ClassTitleEN" : "ClassTitleB5";
		$ClassNameDB = "ClassTitleEN";
		$classNameArr = array ();
		foreach ( $accessRight ['classTeacher'] as $yearClass ) {
			$classNameArr [] = $yearClass [$ClassNameDB];
		}
		if (! in_array ( $ClassName, $classNameArr )) {
			$canAccess = false;
		}
		
		// ### Class teacher access right END ####
	}
	
	if (! $canAccess) {
		echo 'Access Denied.';
		die ();
	}
}
// ######### Access Right END ##########

function sd_square($x, $mean) { return pow($x - $mean,2); }

// Function to calculate standard deviation (uses sd_square)
function sd($array) {
	// square root of sum of squares devided by N-1
	if(count($array)<2){
		return "0";		
	}else{
	return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
	}
}
// ######### Get Pass Mark START ##########
if ($TermAssessment) {
	$termAssessmentFilterSQL = " AND ASSR.TermAssessment = '{$TermAssessment}'";
} else {
	$termAssessmentFilterSQL = " AND ASSR.TermAssessment IS NULL";
}
if ($YearTermID) {
	$yearTermIdFilterSQL = " AND ASSR.YearTermID = '{$YearTermID}'";
} else {
	$yearTermIdFilterSQL = " AND ASSR.YearTermID IS NULL";
}
$sql = "SELECT DISTINCT
	ASF.SubjectID,
	ASF.FullMarkInt,
	ASF.PassMarkInt,
	ASSR.YearClassID
FROM
	{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK ASF
INNER JOIN
	{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
ON
	ASSR.AcademicYearID = '{$academicYearID}'
AND
	ASSR.ClassName = '{$ClassName}'
AND
	ASF.SubjectID = IF(ASSR.SubjectComponentID IS NULL, ASSR.SubjectID, ASSR.SubjectComponentID)
	{$termAssessmentFilterSQL}
	{$yearTermIdFilterSQL}
INNER JOIN
	{$intranet_db}.YEAR_CLASS YC
ON
	ASF.YearID = YC.YearID
AND
	YC.AcademicYearID = '{$academicYearID}'
AND
	YC.YearClassID = ASSR.YearClassID
WHERE
	ASF.AcademicYearID = '{$academicYearID}'
";
$rs = $lpf->returnResultSet ( $sql );

$subjectFullMarkArr = BuildMultiKeyAssoc ( $rs, array (
		'SubjectID' 
), array (
		'FullMarkInt' 
), $SingleValue = 1 );
$subjectPassMarkArr = BuildMultiKeyAssoc ( $rs, array (
		'SubjectID' 
), array (
		'PassMarkInt' 
), $SingleValue = 1 );
// debug_r($sql);
// debug_rt($rs);

if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
    if(
        (strpos($ClassName, '6') !== false && $TermAssessment == 'T1A3') ||
        ($TermAssessment == 'T2A3')
    ){
        foreach($subjectFullMarkArr as $index=>$_){
            $subjectFullMarkArr[$index] = 100;
        }
        foreach($subjectPassMarkArr as $index=>$_){
            $subjectPassMarkArr[$index] = 50;
        }
    }
}else if (count ( $subjectFullMarkArr ) > 1) {
    $subjectMaxFullMark = max ( $subjectFullMarkArr );
} else if (count ( $subjectFullMarkArr ) == 1) {
	$subjectMaxFullMark = array_values ( $subjectFullMarkArr );
	$subjectMaxFullMark = $subjectMaxFullMark [0];
} else {
	$subjectMaxFullMark = 100;
}
// ######### Get Pass Mark END ##########

## get this class studentID
$sql = "SELECT
			ycu.UserID
		FROM YEAR_CLASS_USER AS ycu
		INNER JOIN YEAR_CLASS AS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE ycu.YearClassID
				= (SELECT YearClassID
				FROM YEAR_CLASS
				WHERE AcademicYearID = '{$academicYearID}' AND
				(ClassTitleEN= '{$ClassName}' OR ClassTitleB5='{$ClassName}'));
";
$StudentIDAry = $lpf->returnVector($sql);
$StudentList =  implode ( "','", $StudentIDAry);

$StudentIDconds = " and ASSR.UserID IN ('{$StudentList}')";

// ######### Get All Data START ##########
// ### Get Score Data START ####
if (count ( $filterSubjectArr )) {
	$filterSubjectList = implode ( "','", $filterSubjectArr );
	$subjectIdFilterSQL = " AND ASSR.SubjectID IN ('{$filterSubjectList}')";
}
if ($YearTermID) {
	$yearTermIdFilterSQL = " AND ASSR.YearTermID = '{$YearTermID}'";
} else {
	$yearTermIdFilterSQL = " AND ASSR.YearTermID IS NULL";
}
if ($TermAssessment) {
	$termAssessmentFilterSQL = " AND ASSR.TermAssessment = '{$TermAssessment}'";
} else {
	$termAssessmentFilterSQL = " AND ASSR.TermAssessment IS NULL";
}
$sql = "SELECT
			ASSR.UserID,
			ASSR.ClassName,
			ASSR.ClassNumber,
			ASSR.SubjectID,
			ASSR.SubjectComponentID,
			ASSR.Score,
			ASSR.Grade,
			ASSR.OrderMeritForm,
			ASSR.OrderMeritFormTotal,
			ASSR.YearClassID
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
			/*INNER JOIN
			{$intranet_db}.INTRANET_USER iu ON iu.UserID = ASSR.UserID*/
		WHERE
			ASSR.AcademicYearID = '{$academicYearID}'
			{$yearTermIdFilterSQL}
			{$termAssessmentFilterSQL}
			{$subjectIdFilterSQL}
			{$StudentIDconds}
		ORDER BY
			LENGTH( ASSR.ClassNumber), ASSR.ClassNumber, ASSR.SubjectCode, SubjectComponentID IS NOT NULL
			";
$rs = $lpf->returnResultSet ( $sql );

###### Start Advance Searching START ######
if($Advanced_Search_filter['Advanced_Search']){ //if open the advanced searching
	$cond = '';
	foreach ($Advanced_Search_filter as $filter_key=>$filter){
		switch ($filter_key){
				
			case 'gender':
				$gender=implode(',',(array)$filter);
				$cond .= " AND Gender IN ({$gender}) ";
				break;
				// 					case 'non_chinese':
				// 						$cond .= " AND NonChineseSpeaking = '{$filter}' ";
				// 						break;
				// 					case 'come_from_china':
				// 						$cond .= " AND NonChineseSpeaking = '{$filter}' ";
				// 						break;
				// 					case 'cross_boundary':
				// 						$cond .= " AND NonChineseSpeaking = '{$filter}' ";
				// 						break;
				// 					case 'SEN':
				// 						$SEN = implode(',',(array)$filter);
				// 						$cond .= " AND NonChineseSpeaking IN ({$SEN}) ";
				// 						break;
			default:
				break;
		}
	}
	foreach($rs as $key=>$studentRecord){
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = '{$studentRecord['UserID']}' ".$cond;
		$result = $lpf->returnResultSet($sql);
		if(empty($result[0])){
			unset($rs[$key]); // filter the unwanted record
		}else{
// 			unset($rs[$key]['StudentID']);// unset student ID avoid display in the tb
		}
	}
}
###### Start Advance Searching END ######

$yearClassID = 0;
$allStudentIdArr = array ();
$allSubjectIdArr = array ();
$allScoreArr = array ();
foreach ( $rs as $r ) {
	$yearClassID = $r ['YearClassID'];
	$allStudentIdArr [] = $r ['UserID'];
	if ($r ['SubjectComponentID']) {
		$allSubjectIdArr [] = $r ['SubjectComponentID'];
		$allScoreArr [$r ['UserID']] [$r ['SubjectComponentID']] = $r;
	} else {
		$allSubjectIdArr [] = $r ['SubjectID'];
		# 2020-06-16 (Philips) [#H187376] - Prevent two subject using same subjectID (caused by cust)
		if($allScoreArr [$r ['UserID']] [$r ['SubjectID']]){
			$scoreA = $allScoreArr [$r ['UserID']] [$r ['SubjectID']];
			$scoreB = $r;
			if($scoreB['OrderMeritFormTotal']!=0){
				$allScoreArr [$r ['UserID']] [$r ['SubjectID']] = $scoreB;
			} else if($scoreA['OrderMeritFormTotal']!=0) {
				$allScoreArr [$r ['UserID']] [$r ['SubjectID']] = $scoreA;
			}
		} else {
			$allScoreArr [$r ['UserID']] [$r ['SubjectID']] = $r;
		}
		// 		$allScoreArr [$r ['UserID']] [$r ['SubjectID']] = $r;
	}
}
$allStudentIdList = implode ( "','", $allStudentIdArr );
// ### Get Score Data END ####

// ### Get Overall Merit START ####
$yearTermIdSQL = ($YearTermID) ? "ASSR.YearTermID ='{$YearTermID}'" : 'ASSR.YearTermID IS NULL';
$sql = "SELECT
	ASSR.UserID,
	ASSR.Score,
	ASSR.Grade,
	ASSR.OrderMeritForm,
	ASSR.OrderMeritFormTotal
FROM 
	{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD ASSR
WHERE
	ASSR.AcademicYearID = '{$academicYearID}'
AND
	ASSR.ClassName = '{$ClassName}'
AND
	{$yearTermIdSQL}
	{$termAssessmentFilterSQL}
AND
	ASSR.UserID IN ('{$allStudentIdList}')";
$rs = $lpf->returnResultSet ( $sql );
$studentOverallMarkArr = array ();
foreach ( $rs as $r ) {
	$studentOverallMarkArr [$r ['UserID']] = $r;
}
// ### Get Overall Merit END ####

 ### Get Avg/SD/Max/Min data START ####
$ytSQL = ($YearTermID) ? "YearTermID = '{$YearTermID}'" : "YearTermID = '0'";
$taSQL = ($TermAssessment) ? "TermAssessment = '{$TermAssessment}'" : "TermAssessment = '0'";
if($Advanced_Search_filter['Advanced_Search']=='0'){
	##### Case: NOT Using Advanced Search #####
	$sql = "SELECT 
		* 
	FROM 
		{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
	WHERE
		AcademicYearID = '{$academicYearID}'
	AND
		YearClassID = '{$yearClassID}'
	AND
		{$ytSQL}
	AND
		{$taSQL}
	";
	$rs = $lpf->returnResultSet ( $sql );
}else{ 
	##### Case: Using Advanced Search #####
	$ytSQL = ($YearTermID) ? "YearTermID = '{$YearTermID}'" : "YearTermID  IS NULL";
	$taSQL = ($TermAssessment) ? "TermAssessment = '{$TermAssessment}'" : "TermAssessment IS NULL";
	$sql = "select assr.SubjectID, assr.Score, assr.SubjectComponentID
	FROM 
		{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
	LEFT JOIN 
		{$intranet_db}.INTRANET_USER iu ON assr.UserID = iu.UserID
	WHERE
		AcademicYearID = '{$academicYearID}'
	AND 
		assr.ClassName = '{$ClassName}'
	AND
		Score >= 0
	AND
		{$ytSQL}
	AND
		{$taSQL}
		".$cond;

	$rs = $lpf->returnResultSet ( $sql );
	$i = 0;
	foreach($rs as $_rs){
		if(empty($_rs['SubjectID'])){
			$_rs['SubjectID'] = '0';
		}
		if(empty($_rs['SubjectComponentID'])){
			$_rs['SubjectComponentID'] = '0';
		}
		$scoreBySubject[$_rs['SubjectID']] [$_rs['SubjectComponentID']] []= $_rs['Score'];
	}
	unset($rs);
	if(!empty($scoreBySubject)){
		foreach ($scoreBySubject as $subID =>$_scoreBySubject){//sub
			foreach($_scoreBySubject as $comSubID=>$__scoreBySubject){//comsub
				$highMarks = max($__scoreBySubject);
				$lowMarks = min($__scoreBySubject);
				$sum = array_sum($__scoreBySubject);
				$total = count($__scoreBySubject);
				$SD = standard_deviation($__scoreBySubject);
	// 			$sd = stats_standard_deviation($__scoreBySubject);
				$rs[$i]['HighestMark']=$highMarks;
				$rs[$i]['LowestMark']=$lowMarks;
				$rs[$i]['MEAN']=round($sum/$total,2);
				$rs[$i]['SubjectID'] = $subID;
				$rs[$i]['SubjectComponentID'] = $comSubID;
				$rs[$i]['SD'] = round($SD,2);
				$i++;
			}
		}
	}
}
#### add responding teacher START ####
if(!empty($rs)){
	foreach ($rs as $_rskey=>$r){
		$sql = "SELECT Distinct SubjectGroupID 
		FROM {$intranet_db}.SUBJECT_TERM_CLASS_USER 
		WHERE UserID in ('{$allStudentIdList}')";
		$SubjectGroupID_arr = $lpf->returnResultSet( $sql );
		$SubjectGroupID_arr = Get_Array_By_Key($SubjectGroupID_arr,'SubjectGroupID');
		$SubjectGroupID = implode(',', $SubjectGroupID_arr);
		$sql = "Select Distinct iu.EnglishName as IU_EnglishName, iu.ChineseName as IU_ChineseName, iau.EnglishName as IAU_EnglishName, iau.ChineseName as IAU_ChineseName FROM {$intranet_db}.SUBJECT_TERM st 
				INNER JOIN {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER  stct on st.SubjectGroupID =stct.SubjectGroupID
				LEFT JOIN {$intranet_db}.INTRANET_USER iu on stct.UserID = iu.UserID
				LEFT JOIN {$intranet_db}.INTRANET_ARCHIVE_USER iau on stct.UserID = iau.UserID
				WHERE st.SubjectID = '{$r['SubjectID']}'
				AND st.SubjectGroupID in ({$SubjectGroupID})
				AND {$ytSQL}";
		$teacher_name = $lpf->returnResultSet( $sql );
		$tn = '';
		$comma = '';
		foreach((array)$teacher_name as $_teacher_name){
			$tn .=$comma;
			
			$IntranetUser = Get_Lang_Selection($_teacher_name['IU_ChineseName'], $_teacher_name['IU_EnglishName']);
			$ArchiveUser = Get_Lang_Selection($_teacher_name['IAU_ChineseName'], $_teacher_name['IAU_EnglishName']);
			if($IntranetUser){
				$tn .= $IntranetUser;
			}else{
				$tn .= $libinterface->RequiredSymbol().$ArchiveUser;
			}
			$comma = ' , ';
		}
		if(!$tn){
			$tn = '-';
		}
		$rs[$_rskey]['Teacher'] = $tn;
	}
}
#### add responding teacher END  ####
$sdData = BuildMultiKeyAssoc ( $rs, array (
		'SubjectID',
		'SubjectComponentID' 
) );
 ### Get Avg/SD/Max/Min data END ####
// ######### Get All Data END ##########

// ######### Get Student Info START ##########
// $nameField = getNameFieldByLang2 () . ' As StudentName';
// $sql = "SELECT 
// 	UserID,
// 	{$nameField},
// 	ClassName,
// 	ClassNumber
// FROM 
// 	{$intranet_db}.INTRANET_USER
// WHERE 
// 	UserID IN ('{$allStudentIdList}')";
// $rs = $lpf->returnArray ( $sql );
$rs = $libpf_exam->getStudentsInfoByID($allStudentIdArr, $academicYearID);
$allStudent = BuildMultiKeyAssoc ( $rs, array (
		'UserID' 
), array (
//		'StudentName',
		'Name',
		'ClassName',
		'ClassNumber' 
), $SingleValue = 0 );

// ######### Get Student Info END ##########

// ######### Get Subject Detail with correct order START ##########
$rs = $scm->Get_Subject_List_With_Component ();
$orderSubjectIdArr = array ();
$allSubjectArr = array ();

foreach ( $rs as $r ) {
	if (! $showSubjectComponent && $r ['IsComponent']) {
		continue;
	}
	
	if (in_array ( $r ['SubjectID'], $allSubjectIdArr )) {
		$orderSubjectIdArr [] = $r ['SubjectID'];
		$allSubjectArr [$r ['SubjectID']] = $r;
	}
}
// ######### Get Subject Detail with correct order END ##########

// ######### Pack data to array START ##########
// ### Summary Data START ####
// $sdData = BuildMultiKeyAssoc($rs, array('SubjectID', 'SubjectComponentID'));
$chartSdData = array ();
$subjectNameArr = array ();
$chartTypeNameArr = array (
		$iPort ['MEAN'] 
);

##Get SubjectFull Mark Start Z112708 
$sql = 'SELECT 
			asf.SubjectID, asf.FullMarkInt 
		FROM 
			'.$eclass_db.'.ASSESSMENT_SUBJECT_FULLMARK asf 
			INNER JOIN 
				YEAR_CLASS yc 
					ON	asf.YearID=yc.YearID AND (yc.ClassTitleEN ="'.$ClassName.'" OR yc.ClassTitleB5="'.$ClassName.'") AND yc.AcademicYearID=asf.AcademicYearID AND yc.AcademicYearID = '.$academicYearID;
$rs = $libpf_exam->returnResultSet($sql);
$rs = BuildMultiKeyAssoc($rs,'SubjectID');
$HighestFullMark = 0;
foreach ((array)$allSubjectArr as $_allSubjectArr){
	$SubjectFullMark[$_allSubjectArr['SubjectID']] = $rs[$_allSubjectArr['SubjectID']]['FullMarkInt']? $rs[$_allSubjectArr['SubjectID']]['FullMarkInt']:'100';
	$Bias[$_allSubjectArr['SubjectID']] = 100/$SubjectFullMark[$_allSubjectArr['SubjectID']];
	if($SubjectFullMark[$_allSubjectArr['SubjectID']] > $HighestFullMark){
		$HighestFullMark = $SubjectFullMark[$_allSubjectArr['SubjectID']];
	}
}

// initialization of high chart data
$highChartDataAssoAry = array ();
$highChartDataAssoAry ['subjectMeanChart'] = array ();
foreach ( $orderSubjectIdArr as $subjectID ) {
	$lang = Get_Lang_Selection ( 'SubjectDescB5', 'SubjectDescEN' );
	$parentLang = Get_Lang_Selection ( 'ParentSubjectDescB5', 'ParentSubjectDescEN' );
	$subject = $allSubjectArr [$subjectID];
	
	if ($subject ['IsComponent']) {
		$subjectName = "{$subject[$parentLang]} - {$subject[$lang]}";
	} else {
		$subjectName = $subject [$lang];
	}
	$subjectNameArr [] = $subjectName;
	
	$parentSubjectID = $subject ['ParentSubjectID'];
	$subjectComponentID = ($subject ['IsComponent']) ? $subject ['SubjectID'] : 0;
	$biasSubjectID = $subjectComponentID? $subjectComponentID:$subjectID;
	$score = round ( $sdData [$parentSubjectID] [$subjectComponentID] ['MEAN'], 2 );
	#Z112708 
	if($convertCheckBox){
		$score = $Bias[$biasSubjectID]*$score;
	}
	
	$item = array (
			'itemGroup' => 'MEAN',
			'value' => $score 
	);
	$chartSdData [] = array (
			'item' => $subjectID,
			'values' => array (
					$item 
			) 
	);
	
	// consolidate highchart data
	$highChartDataAssoAry ['subjectMeanChart'] ['xAxisItem'] [] = $subjectName;
	$highChartDataAssoAry ['subjectMeanChart'] ['xAxisValue'] [] = $score;
}

// convert highchart data to json
$highChartDataAssoAry ['subjectMeanChart'] ['xAxisItemJson'] = $json->encode ( $highChartDataAssoAry ['subjectMeanChart'] ['xAxisItem'] );
$highChartDataAssoAry ['subjectMeanChart'] ['xAxisValueJson'] = $json->encode ( $highChartDataAssoAry ['subjectMeanChart'] ['xAxisValue'] );

// ### Summary Data END ####

// ### Student Data START ####
$data = array ();
foreach ( $allScoreArr as $studentID => $score ) {
	foreach ( $allSubjectArr as $subject ) {
		$data [$studentID] [$subject ['SubjectID']] = $score [$subject ['SubjectID']];
		if($HighChartMode=='Box'){
			$HighChartBoxData_Temp[$subject ['SubjectID']][] = $score [$subject ['SubjectID']]['Score'];
		}
	}
}
##B111270 
if($HighChartMode=='Box'){
	foreach ( $allSubjectArr as $subject) {
		$HighChartBoxData[$subject ['SubjectID']] = $libpf_exam->getStatisData($HighChartBoxData_Temp[$subject ['SubjectID']]);
	}
	foreach ((array)$HighChartBoxData as $subjectID=>$_HighChartBoxData){
		$lang = Get_Lang_Selection ( 'SubjectDescB5', 'SubjectDescEN' );
		$parentLang = Get_Lang_Selection ( 'ParentSubjectDescB5', 'ParentSubjectDescEN' );
		$subject = $allSubjectArr [$subjectID];
		$parentSubjectID = $subject ['ParentSubjectID'];
		$subjectComponentID = ($subject ['IsComponent']) ? $subject ['SubjectID'] : 0;
		$biasSubjectID = $subjectComponentID? $subjectComponentID:$subjectID;
		
		if ($subject ['IsComponent']) {
			$subjectName = "{$subject[$parentLang]} - {$subject[$lang]}";
		} else {
			$subjectName = $subject [$lang];
		}
		if($convertCheckBox){
			foreach ((array)$_HighChartBoxData as $key => $value){
				$_HighChartBoxData[$key] = $Bias[$biasSubjectID]*$_HighChartBoxData[$key];
			}
		}
		$StatArr = array((int)$_HighChartBoxData['Min'], (int)$_HighChartBoxData['Q1'], (int)$_HighChartBoxData['Median'], (int)$_HighChartBoxData['Q3'], (int)$_HighChartBoxData['Max']);
		$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisItem'] [] = $subjectName;
		$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisValue'] [] = $StatArr;
	}
	$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisItemJson'] = $json->encode ( $highChartDataAssoAry_Box['subjectMeanChart'] ['xAxisItem'] );
	$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisValueJson'] = $json->encode ( $highChartDataAssoAry_Box['subjectMeanChart'] ['xAxisValue'] );
}
// ### Student Data END ####
// ######### Pack data to array END ##########

// ######### UI START ##########
if (count ( $data ) == 0) {
	?>

<table border="1" bordercolor="#999999" cellpadding="5" cellspacing="0"
	width="100%">
	<tr>
		<td align="center"><i><?=$i_no_record_exists_msg?></i></td>
	</tr>
</table>

<?php
} else {
	// debug_pr ( $json->encode ( $chartSdData ) );
	?>


<style>
.resultTable tr, .resultTable td {
	white-space: nowrap;
}
</style>


<script language="JavaScript"
	src="/templates/jquery/jquery.floatheader.min.js"></script>
<script>
$(function() {$('.resultTable').floatHeader();});
</script>


<!-- ------------------------ Summary START ------------------------ -->
<!--
<script src="<?=$PATH_WRT_ROOT?>/templates/d3/d3_v3_5_16.js"></script>
<script
	src="<?=$PATH_WRT_ROOT?>/templates/d3/blchart/barchart/barchart.js"></script>
<link
	href="<?=$PATH_WRT_ROOT?>/templates/d3/blchart/barchart/barchart_basic.css"
	rel="stylesheet" type="text/css">
-->

<div class="chart" style="height: 500px"></div>
<style type="text/css">
${demo.css}
</style>
		
<script type="text/javascript">
$(function () {


	/**
	 * Grid-light theme for Highcharts JS
	 * @author Torstein Honsi
	 */

	// Load the fonts
	Highcharts.createElement('link', {
	   href: 'https://fonts.googleapis.com/css?family=Dosis:400,600',
	   rel: 'stylesheet',
	   type: 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
	   colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
	      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	   chart: {
	      backgroundColor: null,
	      style: {
	         fontFamily: "Dosis, sans-serif"
	      }
	   },
	   title: {
	      style: {
	         fontSize: '16px',
	         fontWeight: 'bold',
	         textTransform: 'uppercase'
	      }
	   },
	   tooltip: {
	      borderWidth: 0,
	      backgroundColor: 'rgba(219,219,216,0.8)',
	      shadow: false
	   },
	   legend: {
	      itemStyle: {
	         fontWeight: 'bold',
	         fontSize: '13px'
	      }
	   },
	   xAxis: {
	      gridLineWidth: 1,
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   yAxis: {
	      minorTickInterval: 'auto',
	      title: {
	         style: {
	            textTransform: 'uppercase'
	         }
	      },
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   plotOptions: {
	      candlestick: {
	         lineColor: '#404048'
	      }
	   },


	   // General
	   background2: '#F0F0EA'

	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);


// chart randering
	subject ='<?=$highChartDataAssoAry['subjectMeanChart']['xAxisItemJson']?>';
	mark ='<?=$highChartDataAssoAry['subjectMeanChart']['xAxisValueJson']?>';
	subject = JSON.parse(subject);
	mark = JSON.parse(mark);

	var summary = [];
	for (i=0; i<subject.length; i++){
		summary[i] = [];
		summary[i][0]=subject[i];
		summary[i][1]=mark[i];}

	<?php if($HighChartMode=='Column'){?>
	$('.chart').highcharts({
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: '<?=str_replace('<!-- Name -->', $ClassName, $iPort["AvgerageMarkOf"]) ?>'
	    },
	    xAxis: {
	        type: 'category',
	        labels: {
	            rotation: -45,
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    },
	    yAxis: {
	    	 <?php if($convertCheckBox){?>
	         max: 100,
	         <?php }?>
	        min: 0,
	        title: {
	            text: '<?=$Lang['SDAS']['class_subject_performance_summary']['Score']?>'
	        }
	       
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    series: [ 
		{
	        name: '<?=$Lang['SDAS']['class_subject_performance_summary']['Score']?>',
	        data:  summary 
		}
	    ],
	    credits : {enabled: false,},
	});
	<?php }elseif($HighChartMode=='Box'){ #B111270 ?>
	var category = <?=$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisItemJson']?>;
	var data = <?=$highChartDataAssoAry_Box ['subjectMeanChart'] ['xAxisValueJson']?>;
	$('.chart').highcharts({

	    chart: {
	        type: 'boxplot'
	    },
	    title: {
	    	  text: '<?=str_replace('<!-- Name -->', $ClassName, $Lang['SDAS']['class_subject_performance_summary']['ScoreStatistic']) ?>'
	    },

	    legend: {
	        enabled: false
	    },
	    xAxis: {
	        categories: category,
	        title: {
	            text: '<?=$Lang['SDAS']['class_subject_performance_summary']['Subject']?>'
	        }
	    },

	    yAxis: {
		    <?php if($convertCheckBox){?>
		    max: 100,
		    <?php }else{?>
		    max: '<?=$HighestFullMark?>',
		    <?php }?>
	        title: {
	            text: '<?=$Lang['SDAS']['class_subject_performance_summary']['Score']?>'
	        }
	    },
	    series: [{
	        data:  data
	    }],
	    credits : {enabled: false,}

	});
	<?php }?>
});

// var chart = blchart.barchart.init({
// 	'container':'.chart',
//	'chartTitle': '<?=str_replace('<!-- Name -->', $ClassName, $iPort["AvgerageMarkOf"]) ?>',
// 	'margin': {top: 30, right: 120, bottom: 60, left: 60},
//	'data': <?=$json->encode($chartSdData) ?>,
// 	'xAxis': {
//		'items': <?=$json->encode($subjectNameArr) ?>,
// 		'itemGroups': ['MEAN']
// 	},
// 	'yAxis': {
//		'max': <?=($subjectMaxFullMark)? $subjectMaxFullMark : 100?>
// 	},
// 	'legend': {
// 		'show': false
// 	}
// }).create();
</script>

<br />

<div class="chart_tables">
<table class="common_table_list_v30 view_table_list_v30 resultTable">

	<!-- -------- Header START -------- -->
	<thead>
		<tr>
			<th>
				&nbsp;</th>
		
		<?php
	foreach ( $orderSubjectIdArr as $subjectID ) {
		$lang = Get_Lang_Selection ( 'SubjectDescB5', 'SubjectDescEN' );
		$subjectName = $allSubjectArr [$subjectID] [$lang];
		$isSubjectComponent = $allSubjectArr [$subjectID] ['IsComponent'];
// 		$tdStyle = ($isSubjectComponent) ? 'background-color:' : 'background-color: dimgrey';
		?>
			<th>
				<?= $subjectName?>
			</th>
		<?php
	}
	?>
		
	</tr>
	</thead>
	<!-- -------- Header END -------- -->


	<!-- -------- Data START -------- -->
<?php
	
	$chartTypeArr = array (
			'MEAN',
			'SD',
			'HighestMark',
			'LowestMark',
			'Teacher'
	);
	foreach ( $chartTypeArr as $_display ) {
		?>
	<tr>
		<td><?=$iPort[$_display] ?></td>
		<?php
		foreach ( $allSubjectArr as $subject ) {
			$subjectID = $subject ['ParentSubjectID'];
			$subjectComponentID = ($subject ['IsComponent']) ? $subject ['SubjectID'] : 0;
			$biasSubjectID = $subjectComponentID? $subjectComponentID:$subjectID;
			#Z112708 
			if(($_display!='SD'&&$_display!='Teacher')&&$convertCheckBox){
				$display = round($sdData[$subjectID][$subjectComponentID][$_display]*$Bias[$biasSubjectID],2).'%';
			}else{
				$display = $sdData[$subjectID][$subjectComponentID][$_display];
			}
			?>
			<td>
				<?=$display?>
			</td>
		<?php
		}
		?>
	</tr>
<?php
	}
	?>
<!-- -------- Data END -------- -->

</table>
</div>
<!-- ------------------------ Summary END ------------------------ -->

<br />
<br />
<br />

<!-- ------------------------ Student Data START ------------------------ -->
<div class="chart_tables">
<table class="common_table_list_v30 view_table_list_v30 resultTable">

	<!-- -------- Header START -------- -->
	<thead>
		<tr>
			<th>
			<?=$iPort['usertype_s']?>
		</th>

			<th>
			<?=$ec_iPortfolio['overall_rank']?>
		</th>
		
		<?php
	foreach ( $orderSubjectIdArr as $subjectID ) {
		$lang = Get_Lang_Selection ( 'SubjectDescB5', 'SubjectDescEN' );
		$subjectName = $allSubjectArr [$subjectID] [$lang];
		$isSubjectComponent = $allSubjectArr [$subjectID] ['IsComponent'];
// 		$tdStyle = ($isSubjectComponent) ? 'background-color:' : 'background-color: dimgrey';
		?>
			<th>
				<?= $subjectName?>
			</th>
		<?php
	}
	?>
		
	</tr>
	</thead>
	<!-- -------- Header END -------- -->


	<!-- -------- Data START -------- -->
<?php
	$i = 1;
	foreach ( $allStudent as $studentID => $studentAry) {
		$d1 = $data[$studentID];
	//foreach ( $data as $studentID => $d1 ) {
// 		if ($i % 2 == 0) {
// 			$bgcolor = '#FFFFFF';
// 		} else {
// 			$bgcolor = '#F3F3F3';
// 		}
		
		// # Student START ##
		$studentName = $allStudent [$studentID] ['Name'];
		$className = $allStudent [$studentID] ['ClassName'];
		$classNumber = $allStudent [$studentID] ['ClassNumber'];
		// # Student END ##
		
		// # Overall Merit START ##
		$omf = $studentOverallMarkArr [$studentID] ['OrderMeritForm'];
		$omf = ($omf) ? $omf : '--';
		$omfTotal = $studentOverallMarkArr [$studentID] ['OrderMeritFormTotal'];
		$omfTotal = ($omfTotal) ? $omfTotal : '--';
		$omfHTML = "<span>{$omf} / {$omfTotal}</span>";
		// # Overall Merit END ##
		?>	

	<tr>
		<td><span><?=$studentName?></span><br />
			<span><?=$className?> - <?=$classNumber?></span></td>

		<td>
			<?=$omfHTML?>
		</td>
<?php
		foreach ( $orderSubjectIdArr as $subjectID ) {
			$_data = $d1 [$subjectID];
			$isSubjectComponent = $allSubjectArr [$subjectID] ['IsComponent'];
			$hasData = false;
			
			// # Mark START ##
			if (in_array ( 'SCORE', $ResultType )) {
				$passMark = $subjectPassMarkArr [$subjectID];
				
				$score = $_data ['Score'];
				#Z112708 
				if($convertCheckBox){
					$subject = $allSubjectArr [$subjectID];
					$parentSubjectID = $subject ['ParentSubjectID'];
					$subjectComponentID = ($subject ['IsComponent']) ? $subject ['SubjectID'] : 0;
					$biasSubjectID = $subjectComponentID? $subjectComponentID:$subjectID;
					$score = round($score * $Bias[$biasSubjectID],2).'%';
					$passMark = $passMark*$Bias[$parentSubjectID];
				}
				
				if ($score === '' || $score == '-1' || is_null ( $score )) {
					$scoreHTML = "<span>--</span>";
				} else if ($score < $passMark) {
					$scoreHTML = "<span style=\"color: red;\">{$score}</span>";
					$hasData = true;
				} else {
					$scoreHTML = "<span>{$score}</span>";
					$hasData = true;
				}
			}
			// # Mark END ##
			
			// # Grade START ##
			if (in_array ( 'GRADE', $ResultType )) {
				$grade = strtoupper ( $_data ['Grade'] );
				if ($grade == '') {
					$gradeHTML = "<span>--</span>";
				} else if (in_array ( $grade, $failGrade )) {
					$gradeHTML = "<span style=\"{$failGradeStyle}\">{$grade}</span>";
					$hasData = true;
				} else {
					$gradeHTML = "<span>{$grade}</span>";
					$hasData = true;
				}
				if (count ( $ResultType ) > 1) {
					$gradeHTML = "({$gradeHTML})";
				}
			}
			// # Grade END ##
			
			// # Rank START ##
			if (in_array ( 'RANK', $ResultType )) {
				// fix displayed -1 problem
// 				$rank = ($_data ['OrderMeritForm']) ? $_data ['OrderMeritForm'] : '--';
// 				$rankTotal = ($_data ['OrderMeritFormTotal']) ? $_data ['OrderMeritFormTotal'] : '--';
				$rank = ($_data ['OrderMeritForm'] && $_data ['OrderMeritForm'] != -1) ? $_data ['OrderMeritForm'] : '--';
				$rankTotal = ($_data ['OrderMeritFormTotal'] && $_data ['OrderMeritForm'] != -1) ? $_data ['OrderMeritFormTotal'] : '--';
				
				if ($_data ['OrderMeritForm']) {
					$hasData = true;
				}
				if (count ( $ResultType ) > 1) {
					$rankHTML = "<br />[ <span>{$rank} / {$rankTotal}</span> ]";
				} else {
					$rankHTML = "<span>{$rank} / {$rankTotal}</span>";
				}
			}
			// # Rank END ##
			?>
		<td>
			<?php
			if ($hasData) {
				echo "{$scoreHTML} {$gradeHTML} {$rankHTML}";
			} else {
				echo "<span>-- / --</span>";
			}
			?>
		</td>
<?php
		}
		?>
	</tr>

<?php
		$i ++;
	}
	?>
<!-- -------- Data END -------- -->

</table>
</div>
<span class="tabletextremark">
	<?=$Lang['SDAS']['DeletedUserLegend'] ?>
	<br />
	<?= $ec_iPortfolio['scoreHighlightRemarks']?>
</span>

<?php
} // End if

?>