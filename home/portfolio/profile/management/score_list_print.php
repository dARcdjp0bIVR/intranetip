<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

?>

<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_link.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_content.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_table.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_button.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_body.css" rel="stylesheet" type="text/css">
<link href="http://<?=$eclass_httppath?>/src/includes/css/style_portfolio_test.css" rel="stylesheet" type="text/css"> 

<?php
$li_pf = new libpf_asr();
$li_pf_2007a = new libpf_asr();

include_once("score_list_common.php");
$x = generateResultTable(1);

switch ($ResultType)
{
	case "SCORE":
		$display_type = $ec_iPortfolio['score'];
		break;
	case "RANK":
		$display_type = $ec_iPortfolio['rank'];
		break;
	case "GRADE":
		$display_type = $ec_iPortfolio['grade'];
		break;
}
$info = "<tr>";
$info .= "<td class='text_11_black' width='30%'>".$ec_iPortfolio['year'].": ".$Year."</td>";
$info .= "<td class='text_11_black' width='30%'>".$ec_iPortfolio['semester'].": ".$Semester."</td>";
$info .= "<td class='text_11_black' width='30%'>".$ec_iPortfolio['class'].": ".$ClassName."</td>";
$info .= "<td class='text_11_black' width='30%'>".$ec_iPortfolio['display'].": ".$display_type."</td>";
$info .= "<td class='text_11_black'>&nbsp;</td>";
$info .= "</tr>";

intranet_closedb();
?>
<table cellSpacing=0 cellPadding=0 width='98%' border=0 align='center'>
<?=$info?>
<tr>
<td colspan="5">
	<table width='100%' border='0' cellspacing='0' cellpadding='0'>
	<tr><td><?=$x?></td></tr>
	</table>
</td>
</tr>
</table>