<?php
// using : Pun
/**
 * Change Log:
 * 2016-08-16 (Ronald)
 *  - add sorting option
 * 
 * 2016-03-11 (Pun)
 *  - Fix cannot show deleted student name
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

#### Init START ####
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
$lpf = new libportfolio();

$AcademicYearID = $_REQUEST['AcademicYearID'];
$FromYearTermID = $_REQUEST['FromYearTermID'];
$FromYearTermAssessment = $_REQUEST['FromTermAssessment'];
$ToYearTermID = $_REQUEST['ToYearTermID'];
$ToYearTermAssessment = $_REQUEST['ToTermAssessment'];
$studentIdArr = $_REQUEST['StudentID'];
$sort_by =  (int)$_REQUEST['sort_by'];
$order = (int)$_REQUEST['order'];

if($sort_by == NULL){
	$sort_by = 0;
}

if($order == NULL){
	$order = 0;
}

#### Init End ####


#### Get Record START ####
$result = $lpf->GetSubjectStatsCompareByTermDetails($AcademicYearID, $SubjectID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $studentIdArr);

$studentIdList = implode("','", (array)$studentIdArr);
/*
$sql = "SELECT UserID, ChineseName,EnglishName FROM INTRANET_USER WHERE UserID IN ('{$studentIdList}')";
$rs = $lpf->returnResultSet($sql);
$userNameArr = BuildMultiKeyAssoc($rs, array('UserID'), array('ChineseName', 'EnglishName'));
*/

$NameField = getNameFieldByLang('u.');
$ArchiveNameField = getNameFieldByLang2('au.');
$sql = "SELECT 
	IF(u.UserID IS NULL, au.UserID, u.UserID) AS UserID, 
	CASE 
		WHEN au.UserID IS NOT NULL then CONCAT('<font style=\"color:red;\">*</font>',{$ArchiveNameField}) 
		ELSE {$NameField} 
	END as StudentName 
FROM
	INTRANET_USER u
LEFT JOIN
	INTRANET_ARCHIVE_USER au
ON
	u.UserID = au.UserID
WHERE
	u.UserID IN ('{$studentIdList}')
";
$rs = $lpf->returnResultSet($sql);
$userNameArr = BuildMultiKeyAssoc($rs, array('UserID'), array('StudentName'));


$sql = "SELECT 
	u.UserID, 
	{$NameField} AS StudentName
FROM 
	INTRANET_USER u
WHERE
	u.UserID IN ('{$studentIdList}')
";
$rs = $lpf->returnResultSet($sql);
$userNameArr = BuildMultiKeyAssoc($rs, array('UserID'), array('StudentName'));


$sql = "SELECT 
	au.UserID, 
	CONCAT('<font style=\"color:red;\">*</font>',{$ArchiveNameField}) AS StudentName
FROM 
	INTRANET_ARCHIVE_USER au
WHERE
	au.UserID IN ('{$studentIdList}')
";
$rs = $lpf->returnResultSet($sql);
$archiveUserNameArr = BuildMultiKeyAssoc($rs, array('UserID'), array('StudentName'));

foreach ($archiveUserNameArr as $userID => $name){
	if(!$userNameArr[$userID]){
		$userNameArr[$userID] = $name;
	}
}
#### Get Record END ####


#### UI START ####
if($FromYearTermAssessment == ''){
	if($FromYearTermID == '0'){
		$FromHeader = $ec_iPortfolio['overall_result'];
	}else{
		$objTerm = new academic_year_term($FromYearTermID);
		$FromHeader = $objTerm->Get_Year_Term_Name();
	}
}else{
	$FromHeader = $FromYearTermAssessment;
}
if($ToYearTermAssessment == ''){
	if($ToYearTermID == '0'){
		$ToHeader = $ec_iPortfolio['overall_result'];
	}else{
		$objTerm = new academic_year_term($ToYearTermID);
		$ToHeader = $objTerm->Get_Year_Term_Name();
	}
}else{
	$ToHeader = $ToYearTermAssessment;
}
?>

<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language=JavaScript1.2 src=/templates/script.js></script>

<script language="javascript">

var sort_by = <?=$sort_by?>;
var sort_order = <?=$order?>;

$(function(){
	$('#sort_by_0').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 0;
	    }
	});
	$('#sort_by_1').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 1;
	    }
	});
	$('#sort_by_2').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 2;
	    }
	});
	$('#sort_order_0').click(function(){
	    if($(this).is(':checked')){
	        sort_order = 0;
	    }
	});
	$('#sort_order_1').click(function(){
	    if($(this).is(':checked')){
	    	sort_order = 1;
	    }
	});
	
});

function js_Reload_DBTable()
{
	reload_url = window.location.href;
	reload_url += "&sort_by="+sort_by+"&order="+sort_order;
	
	document.location.href = reload_url;
}

</script>
<div>
	<table border="0" cellspacing="0" cellpadding="6" class="form_table_v30">						
		<tr>
			<td class="field_title" style="width:200px"><?=$Lang['SDAS']['AcademicProgress']['Order']?></td>
			<td valign="top" >
				<div><span class="tabletext"><input type="radio" id="sort_order_0" name="sort_order" value="0" <?=($order==0?"checked='checked'":"")?>/><label for="sort_order_0"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Asc']?></label></span></div>
				<div><span class="tabletext"><input type="radio" id="sort_order_1" name="sort_order" value="1" <?=($order==1?"checked='checked'":"")?>/><label for="sort_order_1"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Dec']?></label></span></div>
			</td>
	   </tr>
	   		<tr>
			<td class="field_title" style="width:200px"><?=$Lang['SDAS']['AcademicProgress']['Sort']?></td>
			<td valign="top" >
				<div><span class="tabletext"><input type="radio" id="sort_by_0" name="sort_by" value="0" <?=($sort_by==0?"checked='checked'":"")?>/><label for="sort_by_0"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Class']?></label></span></div>
				<div><span class="tabletext"><input type="radio" id="sort_by_1" name="sort_by" value="1" <?=($sort_by==1?"checked='checked'":"")?>/><label for="sort_by_1"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['ScoreDiff']?></label></span></div>
				<div><span class="tabletext"><input type="radio" id="sort_by_2" name="sort_by" value="2" <?=($sort_by==2?"checked='checked'":"")?>/><label for="sort_by_2"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['ZScoreDiff']?></label></span></div>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30">
		<input type="button"value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
</div>

<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30">
		<thead>
			<tr>
				<th rowspan="2">&nbsp;</th>
				<th colspan="3"><?=$ec_iPortfolio['score']?></th>
				<th colspan="3"><?=$Lang['iPortfolio']['StandardScore']?></th>
			</tr>
			<tr>
				<th><?=$FromHeader?></th>
				<th><?=$ToHeader?></th>
				<th id="per_sort"><?=$iPort["scoreImprovementPercent"]?></th>
				<th><?=$FromHeader?></th>
				<th><?=$ToHeader?></th>
				<th><?=$Lang['iPortfolio']["StandardScoreDifference"]?></th>
			</tr>
		</thead>
		<tbody>
			<!---- isplay result data START ---->
			<?php 
			$final_result;
			$i = 0;
			foreach($result as $r){
				$studentName = $userNameArr[$r['from']['UserID']]['StudentName'];
				$studentInfo = "{$studentName}<br />{$r['from']['ClassName']} - {$r['from']['ClassNumber']}";
				$fromScore = $r['from']['Score'];
				$toScore = $r['to']['Score'];
				if($fromScore == 0){
					$compareScoreHTML = '--';
				}else{
					$compareScore = round(100*($toScore-$fromScore)/$fromScore, 2);
				}
				$fromZScore = $r['from']['StandardScore'];
				$toZScore = $r['to']['StandardScore'];
				$compareZScore = round($toZScore-$fromZScore, 2);
				
				$final_result[$i]['studentInfo'] = $studentInfo;
				$final_result[$i]['fromScore'] = $fromScore;
				$final_result[$i]['toScore'] = $toScore;
				$final_result[$i]['compareScore'] = $compareScore;
				$final_result[$i]['fromZScore'] = $fromZScore;
				$final_result[$i]['toZScore'] = $toZScore;
				$final_result[$i]['compareZScore'] = $compareZScore;
				$i++;
			}
			
			function sort_result_s($a,$b){
				return ($a['compareScore'] < $b['compareScore']) ? -1 : 1;
			}
			function sort_result_z($a,$b){
				return ($a['compareZScore'] < $b['compareZScore']) ? -1 : 1;
			}
		
			if($sort_by == 1){
				usort($final_result, 'sort_result_s');
			}else if($sort_by == 2){
				usort($final_result, 'sort_result_z');
			}
		
			if($sort_by >= 0 && $order == 1){
				$final_result = array_reverse($final_result);
			}
			
			foreach((array)$final_result as $r){
				$studentInfo = $r['studentInfo'];
				$fromScore = $r['fromScore'];
				$toScore = $r['toScore'];
				$compareScore = $r['compareScore'];
				$compareScoreHTML = "<font color=\"" . (($compareScore>0)?'green':'red') . "\">{$compareScore}%</font>";
				$fromZScore = $r['fromZScore'];
				$toZScore = $r['toZScore'];
				$compareZScore = $r['compareZScore'];
				$compareZScoreHTML = "<font color=\"" . (($compareZScore>0)?'green':'red') . "\">{$compareZScore}</font>";

			?>
			
				<tr>
					<td><?=$studentInfo?></td>
					<td><?=$fromScore?></td>
					<td><?=$toScore?></td>
					<td><?=$compareScoreHTML?></td>
					<td><?=$fromZScore?></td>
					<td><?=$toZScore?></td>
					<td><?=$compareZScoreHTML?></td>
				</tr>
			<?php 
			}
			//debug_r($final_result);
			if(count($result) == 0){
				echo "<tr><td colspan='99' align='center'>{$Lang['General']['NoRecordAtThisMoment']}</td></tr>";
			}
			?>
			<!---- Display result data END ---->
		</tbody>
	</table>
</div>


<?php
intranet_closedb();
?>