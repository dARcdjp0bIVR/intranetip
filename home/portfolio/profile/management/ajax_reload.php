<?php
// using : 
/**
 * Change Log: 
 * 2020-07-30 Philips
 *  -	modified Assessment_Statistics_Report_DBTable : use Fr instead of From
 * 2017-06-27 Villa
 *  -   modified Assessment_Statistic_report_SDAS : add monitoring group 
 * 2016-11-24 Villa
 *  -	modified "Assessment_Statistics_Report_SDAS" for pass parameter for advanced searching filter
 * 2016-08-01 Ronald<3
 * 	-	Create  "Assessment_Statistics_Report_SDAS" for getting more parameters
 * 
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
$lpf_ui = new libportfolio_ui();
$lpf = new libportfolio();


########## Access Right START ##########
// $accessRight = $lpf->getAssessmentStatReportAccessRight();
// if(!$accessRight['admin'] && !$accessRight['subjectPanel']){
// 	echo 'Access Denied.';
// 	die;
// }else if(!$accessRight['admin']){ // Subject Panel
// 	$subjectArr = array_keys($accessRight['subjectPanel']);
// 	if(
// 		(
// 			$_REQUEST['FromSubjectID'] != '' &&
// 			!in_array($_REQUEST['FromSubjectID'], $subjectArr)
// 		) || (
// 			$_REQUEST['ToSubjectID'] != '' &&
// 			!in_array($_REQUEST['ToSubjectID'], $subjectArr)
// 		)
// 	){
// 		echo 'Access Denied.';
// 		die;
// 	}
// }
########## Access Right END ##########

########## Cookies handling START ##########
if($updateCookie){
	# set cookies
	$arrCookies = array();
	$arrCookies[] = array("ck_management_advancement_page_size", "numPerPage");
	$arrCookies[] = array("ck_management_advancement_page_number", "pageNo");
	$arrCookies[] = array("ck_management_advancement_page_order", "order");
	$arrCookies[] = array("ck_management_advancement_page_field", "field");
	$arrCookies[] = array("ck_management_advancement_YearID", "YearID");
	$arrCookies[] = array("ck_management_advancement_YearClassID", "YearClassID");
	$arrCookies[] = array("ck_management_advancement_FromAcademicYearID", "FromAcademicYearID");
	$arrCookies[] = array("ck_management_advancement_FromYearTermID", "FromYearTermID");
	$arrCookies[] = array("ck_management_advancement_FromSubjectID", "FromSubjectID");
	$arrCookies[] = array("ck_management_advancement_ToAcademicYearID", "ToAcademicYearID");
	$arrCookies[] = array("ck_management_advancement_ToYearTermID", "ToYearTermID");
	$arrCookies[] = array("ck_management_advancement_ToSubjectID", "ToSubjectID");
	
	if(isset($clearCoo) && $clearCoo == 1)
	{
		clearCookies($arrCookies);
		$ck_management_advancement_page_size = '';
	}
	else 
		updateGetCookies($arrCookies);
}
########## Cookies handling END ##########
# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Assessment_Statistics_Report_DBTable")
{
	$YearID = $_REQUEST['YearID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$FrAcademicYearID = $_REQUEST['FrAcademicYearID'];
	$FrYearTermID = $_REQUEST['FrYearTermID'];
	$FrSubjectID = $_REQUEST['FrSubjectID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	$ToYearTermID = $_REQUEST['ToYearTermID'];
	$ToSubjectID = $_REQUEST['ToSubjectID'];
	$field = $_REQUEST['field'];
	$order = $_REQUEST['order'];
	$pageNo = $_REQUEST['pageNo'];
	
	echo $lpf_ui->Get_Management_Advancement_DBTable($YearID, $YearClassID, $FrAcademicYearID, $FrYearTermID, $FrSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $field, $order, $pageNo);
}

if ($Action == "Assessment_Statistics_Report_SDAS")
{
	$YearID = $_REQUEST['YearID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$FromYearTermID = $_REQUEST['FromYearTermID'];
	$FromSubjectID = $_REQUEST['FromSubjectID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	$ToYearTermID = $_REQUEST['ToYearTermID'];
	$ToSubjectID = $_REQUEST['ToSubjectID'];
	$field = $_REQUEST['field'];
	$order = $_REQUEST['order'];
	$pageNo = $_REQUEST['pageNo'];
	$sort_by =  $_REQUEST['sort_by'];
	$sort_order = $_REQUEST['sort_order'];
	$low_score = $_REQUEST['low_score'];
	$high_score = $_REQUEST['high_score'];
	$x_name = cleanHtmlJavascript($_REQUEST['x_name']);
	$y_name = cleanHtmlJavascript($_REQUEST['y_name']);
	$Advanced_Search = array();
	$Advanced_Search['Advanced_Search'] = $_POST['Advanced_Search'];
// 	foreach((array)$_POST['gender'] as $_gender){
// 		$gender_arr[] = "'".$_gender."'";
// 	}
	$Advanced_Search['gender'] =  $_POST['gender'];
	$Advanced_Search['non_chinese'] = $_POST['non_chinese'];
	$Advanced_Search['come_from_china'] = $_POST['come_from_china'];
	$Advanced_Search['cross_boundary'] = $_POST['cross_boundary'];
	$Advanced_Search['SEN'] = $_POST['SEN'];
	$TargetType['Target'] = $_POST['Target'];
	$TargetType['SubjectGroup'] = $_POST['SubjectGroup'];
	$TargetType['MonitoringGroup'] = $_POST['MonitoringGroup'];
	echo $lpf_ui->Get_Management_Advancement_DBTable_SDAS($YearID, $YearClassID, $FromAcademicYearID, $FromYearTermID, $FromSubjectID, $ToAcademicYearID, $ToYearTermID, $ToSubjectID, $field, $order, $pageNo, $sort_by, $sort_order, $low_score, $high_score, $x_name, $y_name,$Advanced_Search,$TargetType);

}


intranet_closedb();
?>