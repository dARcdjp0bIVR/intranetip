<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $ay_id);

$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
echo $ayterm_selection_html;

?>