<?php
//Using: 
/**
 * Change Log
 * 2019-06-19 Philips
 * Add Subject Group class checking
 * 
 * 2016-11-22 Villa
 * Add comparsion item
 * 
 * 2016-08-30 Pun
 *     Added filter no score subject
 * 2016-07-27 Ronald
 *     Change to line chart
 * 2016-06-06 Cara
 * -    Change data table's CSS
 * 2016-06-03 Anna
 * add CSS and change view of bar chart
 * 2016-02-29 Pun
 * 	-	Fixed class teacher cannot view result
 * 2016-02-26 Pun
 * 	-	Moved d3.js path
 *  -	Added Y-Axis full mark load from system
 * 2016-02-23 Pun
 * 	-	New File
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT . 'includes/subject_class_mapping.php');

intranet_auth();
intranet_opendb();

$fcm = new form_class_manage();
$lpf = new libportfolio();
$json = new JSON_obj();

if (strstr($YearTermID, "_"))
{
	$tmpArr = explode("_", $YearTermID);
	$YearTermID = (int) $tmpArr[0];
	$TermAssessment = trim($tmpArr[1]);
}


########## Access Right START ##########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$canAccess = true;
$currentAcademicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
	if( count($accessRight['subjectPanel']) ){
		#### Subject Panel access right START ####
		
		$canAccess = true; // Subject Panel can always access this page
		$filterSubjectArr = array_keys($accessRight['subjectPanel']);
		
		#### Subject Panel access right END ####
	}else{
		#### Class teacher access right START ####
		
		if($academicYearID != $currentAcademicYearID){
			$canAccess = false;
		}
		$yearClassIdArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
			$yearClassIdArr[] = $yearClass['YearClassID'];
		}
		foreach($accessRight['subjectGroup'] as $subjectGroup){
		    $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
		    $studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true);
		    $classList = Get_Array_By_Key($studentList, "YearClassID");
		    $yearClassIdArr = array_merge($yearClassIdArr, $classList);
		}
		foreach((array)$YearClassID as $id){
			if(!in_array($id, $yearClassIdArr)){
				$canAccess = false;
				break;
			}
		}
		
		#### Class teacher access right END ####
	}
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
}
########## Access Right END ##########

$li = new libdb();
$lpf = new libpf_sturec();

$lpf->createTempAssessmentSubjectTable($li, true);

########## Group Subject START ##########
$sql = "SELECT 
	EN_SNAME,
	CODEID
FROM
	{$intranet_db}.ASSESSMENT_SUBJECT
WHERE
	CMP_CODEID = ''
OR 
	CMP_CODEID IS NULL
";
$rs = $li->returnResultSet($sql);
$allCodeArr = BuildMultiKeyAssoc($rs, array('CODEID') , array('EN_SNAME'), $SingleValue=1);

$childToParent = array();
foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $parentCodeID => $childCodeIds){
	foreach($childCodeIds as $childCodeId){
		$childToParent[ $allCodeArr[$parentCodeID] ] = $allCodeArr[ $childCodeId ];
	}
}

########## Group Subject END ##########


######## Get Data START ########
#### Get YearID for class teacher START ####
if($yearID == ''){
	$objYearClass = new year_class($YearClassID[0]);
	$yearID = $objYearClass->YearID;
}
#### Get YearID for class teacher END ####

#### Get Class Name START ####
$rs = $fcm->Get_Class_List($academicYearID,$yearID);
$allClassArr = BuildMultiKeyAssoc($rs, array('YearClassID') , array('ClassTitleEN', 'ClassTitleB5'));
$classNameArr = array();
foreach($YearClassID as $class){
	$classNameArr[$class] = Get_Lang_Selection($allClassArr[$class]['ClassTitleB5'], $allClassArr[$class]['ClassTitleEN']);

}


#### Get Class Name END ####


// Assessment data retrieval
$subjMap_arr = array();
for($i=0, $i_max=count($Subjects); $i<$i_max; $i++)
{
  list($_mainSubj, $_compSubj) = explode("###", $Subjects[$i]);
  
  $_mainSubj2 = '';
  if($childToParent[$_mainSubj]){
  	$_mainSubj2 = $childToParent[$_mainSubj];
  }
  $conds = "AND assr.SubjectCode IN ('{$_mainSubj}', '{$_mainSubj2}') ";
  $conds .= ($_compSubj == "") ? "AND IFNULL(assr.SubjectComponentCode, '') = '' " : "AND assr.SubjectComponentCode = '{$_compSubj}' ";
  $conds .= ($YearTermID == "") ? "AND IFNULL(assr.YearTermID, 0) = 0 " : "AND assr.YearTermID = {$YearTermID} ";
  $conds .= "AND assr.Score <> -1 ";

  $sql = "SELECT assr.ClassName, AVG(assr.Score) AS avgScore, s.CODEID, assr.YearClassID, 
  		assr.SubjectID, assr.SubjectComponentID, assr.AcademicYearID ";
  $sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON assr.UserID = ycu.UserID ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID ";
  $sql .= "INNER JOIN {$intranet_db}.ASSESSMENT_SUBJECT s ON assr.SubjectID = s.RecordID ";
  $sql .= "WHERE assr.AcademicYearID = {$academicYearID} AND yc.AcademicYearID = {$academicYearID} AND yc.YearID = {$yearID} ";
  $sql .= $conds;
  $sql .= "AND IF(assr.Score > 0, true, IF(assr.Score < 0, false, IFNULL(assr.Grade, '') <> '')) ";
  
  if ($TermAssessment=="")
  {
  	$sql .= " AND assr.TermAssessment IS NULL ";
  }else{
  	$sql .= " AND assr.TermAssessment = '".addslashes($TermAssessment)."' ";
  }
  $sql .= "GROUP BY assr.ClassName";
  $rs = $li->returnResultSet($sql);
  
  
  if(count($rs)==0){
      continue;
  }
  
  foreach($rs as $r){
	if(empty($YearTermID)){
		$cond = " YearTermID = '0' AND TermAssessment = '0'";
	}else{
		$cond = " YearTermID = '{$YearTermID}'";
		if(!empty($TermAssessment)){
			$cond .= " AND TermAssessment = '{$TermAssessment}'";
		}else{
			$cond .= " AND TermAssessment = '0'";
		}
	}
		
  	$sql = "Select SD,HighestMark,LowestMark,PassTotal,AttemptTotal FROM {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
  			WHERE SubjectID = '{$r['SubjectID']}'  AND AcademicYearID = '{$r['AcademicYearID']}' 
  			AND YearClassID = '{$r['YearClassID']}'
  			AND SubjectComponentID = '{$r['SubjectComponentID']}'
  			AND $cond";
  	
  	$sd_mean = $li->returnResultSet($sql);
  	$r['avgScore'] = number_format($r['avgScore'],1);
  	$r['SD'] = number_format($sd_mean[0]['SD'],1);
  	$r['HighestMark'] = number_format($sd_mean[0]['HighestMark'],1);
  	$r['LowestMark'] = number_format($sd_mean[0]['LowestMark'],1);
  	$r['PassTotal'] = number_format($sd_mean[0]['PassTotal'],1);
  	$r['AttemptTotal'] = number_format($sd_mean[0]['AttemptTotal'],1);
  	if($r['AttemptTotal'] > 0){
  		$r['PassPercent'] =($r['PassTotal']/$r['AttemptTotal'])*100;
  		$r['PassPercent'] = number_format($r['PassPercent'],1);
  	}
  	$a_rec_arr[$Subjects[$i]][$r['YearClassID']] = $r;
  }
  
  #### Group Subject START ####
  if($childToParent[$_mainSubj]){
	  $sql = "SELECT CODEID ";
	  $sql .= "FROM tempAssessmentSubject ";
	  $sql .= "WHERE MainSubjCode = '{$_mainSubj}' ";
	  $sql .= "AND CompSubjCode = '{$_compSubj}'";
	  $rs = $li->returnResultSet($sql);
	  
	  $rs = $lpf->getParentReleatedSubject($rs[0]['CODEID']);
	  $_subjName = Get_Lang_Selection($rs["CH_DES"], $rs["EN_DES"]);;
	  
	  $display_subj_arr[] = $_subjName;
	  $subjMap_arr[$Subjects[$i]] = $_subjName;
  }else{
	  // Translate subject code to subject name
	  $sql = "SELECT SubjName ";
	  $sql .= "FROM tempAssessmentSubject ";
	  $sql .= "WHERE MainSubjCode = '{$_mainSubj}' ";
	  $sql .= "AND CompSubjCode = '{$_compSubj}'";
	  $_subjName = current($li->returnVector($sql));
	  $display_subj_arr[] = $_subjName;
	  $subjMap_arr[$Subjects[$i]] = $_subjName;
  }
  #### Group Subject END ####
}

########## Get Pass Mark START ##########
$allSubjectIdArr = array();
foreach((array)$a_rec_arr as $score){
	$_score  = array_values($score);
	$_subjectID = $_score[0]['SubjectID'];
	$_subjectCompID = $_score[0]['SubjectComponentID'];
	$allSubjectIdArr[ $_subjectID ] = 1;
	$allSubjectIdArr[ $_subjectCompID ] = 1;
}
$allSubjectIdArr = array_unique(array_filter(array_keys($allSubjectIdArr)));

$allSubjectIdList = implode("','", $allSubjectIdArr);
$sql = "SELECT DISTINCT
	ASF.SubjectID,
	ASF.FullMarkInt,
	ASF.PassMarkInt
FROM
	{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK ASF
WHERE
	ASF.AcademicYearID = '{$academicYearID}'
AND
	ASF.YearID = '{$yearID}'
AND
	ASF.SubjectID IN ('{$allSubjectIdList}')
";
$rs = $lpf->returnResultSet($sql);

$subjectFullMarkArr = BuildMultiKeyAssoc($rs, array('SubjectID') , array('FullMarkInt'), $SingleValue=1);
$subjectPassMarkArr = BuildMultiKeyAssoc($rs, array('SubjectID') , array('PassMarkInt'), $SingleValue=1);

if(count($subjectFullMarkArr)>1){
	$subjectMaxFullMark = max($subjectFullMarkArr);
}else if(count($subjectFullMarkArr) == 1){
	$subjectMaxFullMark = array_values($subjectFullMarkArr);
	$subjectMaxFullMark = $subjectMaxFullMark[0];
}else{
	$subjectMaxFullMark = 100;
}
########## Get Pass Mark END ##########

######## Get Data END ########

######## Group Data START ########
$highChartDataAssoAry = array();
$highChartDataAssoAry['meanChart'] = array();
//  $highChartDataAssoAry['meanChart']['series'][$_classCount][]='';
switch($compare){
	case 'avgScore':
		$type = 'avgScore';
		$yAixs = $iPort["AvgerageMarks"];
		break;
	case 'SD':
		$type = 'SD';
		$yAixs = $iPort['SD'];
		break;
	case 'HighestMark':
		$type = 'HighestMark';
		$yAixs = $iPort['HighestMark'];
		break;
	case 'LowestMark':
		$type = 'LowestMark';
		$yAixs = $iPort['LowestMark'];
		break;
	case 'PassPercent':
		$type = 'PassPercent';
		$yAixs = $iPort['report_col']['passing_rate'];
		break;
}
if($based_on == 0){
	$data = array();
	foreach($YearClassID as $class){
		
		$scoreArr = array();
		$_subjectCount = 0;
		foreach($subjMap_arr as $subjectCode=>$subjName){

			$scoreArr[] = array(
				'name' => $subjName,
				'value' => $a_rec_arr[$subjectCode][$class][$type]
			);
			
			// consolidate highchart data
			$highChartDataAssoAry['meanChart']['series'][$_subjectCount]['name'] = $subjName;
			$highChartDataAssoAry['meanChart']['series'][$_subjectCount]['data'][] = ($a_rec_arr[$subjectCode][$class][$type])? (float)$a_rec_arr[$subjectCode][$class][$type] : 0;
			$_subjectCount++;
		}
		$data[] = array(
			'title' => $classNameArr[$class],
			'score' => $scoreArr
		);
		
		// consolidate highchart data
		$highChartDataAssoAry['meanChart']['xAxisItem'][] = $classNameArr[$class];
	}
}else{
	if(count($subjMap_arr) > 0){
		foreach($subjMap_arr as $subjectCode=>$subjName){
			$scoreArr = array();
			$_classCount = 0;
			foreach($YearClassID as $class){
				
				$scoreArr[] = array(
						'name' => $subjName,
						'value' => $a_rec_arr[$subjectCode][$class][$type]
				);
				$scoreArr[] = array(
					'name' => $classNameArr[$class],
					'value' => $a_rec_arr[$subjectCode][$class][$type]
				);
				
				// consolidate highchart data
				$highChartDataAssoAry['meanChart']['series'][$_classCount]['name'] = $classNameArr[$class];
				$highChartDataAssoAry['meanChart']['series'][$_classCount]['data'][] = ($a_rec_arr[$subjectCode][$class][$type])? (float)$a_rec_arr[$subjectCode][$class][$type] : 0;
				$_classCount++;
			}
			$data[] = array(
				'title' => $subjName,
				'score' => $scoreArr
			);
			
			
			// consolidate highchart data
			$highChartDataAssoAry['meanChart']['xAxisItem'][] = $subjName;
			
		}
	}else{
		$highChartDataAssoAry['meanChart']['series'] = array();
	}
}
// convert highchart data to json
$highChartDataAssoAry['meanChart']['xAxisItemJson'] = $json->encode($highChartDataAssoAry['meanChart']['xAxisItem']);
$highChartDataAssoAry['meanChart']['seriesJson'] = $json->encode($highChartDataAssoAry['meanChart']['series']);

/* */
######## Group Data END ########


######## UI START ########

#### Color START ####
$colorArr = array();
if(count($subjMap_arr) > 19){
	foreach($subjMap_arr as $subjectCode=>$subjName){ 
		mt_srand((double)microtime()*1000000);
		$rand_c = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
		$colorArr[$subjectCode] = $rand_c;
	}
}
#### Color END ####



?>

<!-- -------- Generate bar chart START -------- -->
<script src="<?=$PATH_WRT_ROOT?>/templates/d3/d3_v3_5_16.js"></script>
<style>
.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
}

.x.axis path {
  display: none;
}

</style>

<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<?php 
if(count($subjMap_arr) > 0){ ?>


<div class="chart" style="height: 500px">
</div>


<script>
// var 

xAxis='<?=$highChartDataAssoAry['meanChart']['xAxisItemJson']?>';
series='<?=$highChartDataAssoAry['meanChart']['seriesJson']?>';

xAxis = JSON.parse(xAxis);
series = JSON.parse(series);

// var summary =[]
// for (i=0;i<series.length;i++){
// 	summary[i]=[];
// 	summary[i]=series[i];
// 	for (j=0;j<series[i].data.length;j++){
// 		series[i].data[j]=series[i].data[j];
// 	}
// 	summary[i].data =series[i].data;}
 var summary = [];
 for (i=0; i<series.length; i++){
 	summary[i] = {};
 	summary[i].name = series[i].name;
	
  	for (j=0; j<series[i].data.length; j++){
  		series[i].data[j] = parseFloat(series[i].data[j]);
  	}
  	summary[i].data =series[i].data;
 }

$('.chart').highcharts({
    chart: {
        type: '<?=($chartType)?$chartType:'column' ?>'
    },
    title: {
        text: '<?=($based_on == 0)? $iPort["ClassComparison"] : $iPort["SubjectComparison"] ?>'
    },
    xAxis: {
        
     categories: xAxis,
     crosshair: true
},
    yAxis: {
        min: 0,
        title: {
            text: '<?=$yAixs ?>'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: summary,
    credits : {enabled: false,},
//         [ 
//               {
// 	     name: xAxis,
//          data: summary
//      }, 

//     ]
});

</script>
<?php ;}?>
<script>
// $(function(){
// 	//// Lang START ////
//	var langTitle ='<?=($based_on == 0)? $iPort["ClassComparison"] : $iPort["SubjectComparison"] ?>';
//	var langYAxis = '<?=$iPort["AvgerageMarks"] ?>';
// 	//// Lang END ////
	
// 	//// Graph Basic Setup START ////
// 	var margin = {top: 20, right: 100, bottom: 30, left: 60},
// 		width = $('.chart').width() - margin.left - margin.right,
// 		height = 500 - margin.top - margin.bottom;
	
// 	var svg = d3.select(".chart").append("svg")
// 		.attr("width", width + margin.left + margin.right)
// 		.attr("height", height + margin.top + margin.bottom)
// 		.append("g")
// 		.attr("transform", "translate(" + (margin.left+10) + "," + margin.top + ")");
// 	//// Graph Basic Setup END ////
	
// 	//// Axis Setup START ////
// 	var x0 = d3.scale.ordinal()
// 		.rangeRoundBands([0, width], .1);
// 	var x1 = d3.scale.ordinal();
// 	var y = d3.scale.linear()
// 		.range([height, 0]);
	
// 	var xAxis = d3.svg.axis()
// 		.scale(x0)
// 		.orient("bottom");
	
// 	var yAxis = d3.svg.axis()
// 		.scale(y)
// 		.orient("left");
// 	//// Axis Setup END ////
	
// 	//// Bar Color Setup START ////
//	<?php if($colorArr){ ?>
// 		var color = d3.scale.ordinal()
//		.range(['<?=implode("','", $colorArr) ?>']);
//	<?php }else{ ?>
// 		var color = d3.scale.category20();
//	<?php } ?>
// 	//// Bar Color Setup END ////
	
	
// 	//// Data Setup START ////
//	var yAxisMax = <?=$subjectMaxFullMark ?>;
//	var classNameArr = <?=$json->encode( array_values($classNameArr) ) ?>;
//	var subjectNameArr = <?=$json->encode(array_values((array)$subjMap_arr)) ?>;
	
//	var data = <?=$json->encode($data) ?>;
// 	//// Data Setup END ////
	
	
// 	//// Chart START ////
// 	// Title START //
// 	svg.append("text")
// 		.attr("class", "title")
// 		.attr("x", width/2)
// 		.text(langTitle)
// 	    .style("text-anchor", "end");
// 	// Title END //
	
	
// 	// Axis START //
//	<?php if($based_on == 0){ ?>
// 		x0.domain(classNameArr);
// 		x1.domain(subjectNameArr).rangeRoundBands([0, x0.rangeBand()]);
//	<?php }else{ ?>
// 		x0.domain(subjectNameArr);
// 		x1.domain(classNameArr).rangeRoundBands([0, x0.rangeBand()]);
//	<?php } ?>
// 	y.domain([0, yAxisMax]);
	
// 	svg.append("g")
// 		.attr("class", "x axis")
// 		.attr("transform", "translate(0," + height + ")") // Move the axis to the bottom
// 		.call(xAxis);
	
// 	svg.append("g")
// 		.attr("class", "y axis")
// 		.call(yAxis);
// 	svg.select(".y.axis") // Y-axis text
// 		.append("text")
// 		.attr("transform", "rotate(-90)")
// 		.attr("y", -50)
// 		.attr("x", -height/2+20)
// 		.attr("dy", "1em")
// 		.style("text-anchor", "end")
// 		.text(langYAxis);
// 	// Axis END //
	
// 	// Bar START //
// 	var state = svg.selectAll(".state")
// 		.data(data)
// 		.enter().append("g")
// 		.attr("class", "state")
// 		.attr("transform", function(d) { return "translate(" + x0(d.title) + ",0)"; });
	
// 	state.selectAll("rect")
// 		.data(function(d) { return d.score; })
// 		.enter().append("rect")
// 		.attr("width", x1.rangeBand())
// 		.attr("x", function(d) { return x1(d.name); })
// 		.attr("y", function(d) { return y(d.value); })
// 		.attr("height", function(d) { return height - y(d.value); })
// 		.style("fill", function(d) { return color(d.name); });
	
// 	state.selectAll("text")
// 		.data(function(d) { return d.score; })
// 		.enter()
// 		.append("text")
// 		.text(function(d) { return d.value; })
// 		.attr("x", function(d) { return x1(d.name) + (x1.rangeBand() / 2); })
// 		.attr("y", function(d) { return y(d.value)-5; })
// 		.style("text-anchor", "middle");
// 	// Bar End //
	
// 	// Legend START //
//	var legendData = <?= ($based_on == 0)? 'subjectNameArr' : 'classNameArr' ?>;
// 	var legend = svg.selectAll(".legend")
// 	    .data(legendData)
// 		.enter().append("g")
// 	    .attr("class", "legend")
// 	    .attr("transform", function(d, i) { return "translate(40," + i * 20 + ")"; });
	
// 	legend.append("rect")
// 	    .attr("x", width - 18)
// 	    .attr("width", 18)
// 	    .attr("height", 18)
// 	    .style("fill", color);
	
// 	legend.append("text")
// 	    .attr("x", width - 24)
// 	    .attr("y", 9)
// 	    .attr("dy", ".35em")
// 	    .style("text-anchor", "end")
// 	    .text(function(d) { return d; });
// 	//Legend END //
// 	//// Chart END ////

// }); // End $(function(){...
</script>



<!-- -------- Generate bar chart END -------- -->

<br/>

<!-- -------- Generate table START -------- -->
<?php //debug_pr($highChartDataAssoAry);?>
<div class='chart_tables'>
	<table class="common_table_list_v30 view_table_list_v30">
	
		<thead>
		
			<tr>
				<th style="width: 20%; white-space: nowrap;"><?=$iPort["Subject"] ?> \ <?=$iPort["Class"] ?> </th>
				<?php foreach($YearClassID as $class){ ?>
					<th><?=$classNameArr[$class] ?> </th>
				<?php } ?>
			</tr>
			
		</thead>
		
		<tbody>
		
			<?php 
			foreach($subjMap_arr as $subjectCode=>$subjName){ 
				$isComponent = (strpos($subjectCode,'###')!==false);
			?>
				<tr>
					
					<td><?=($isComponent)? '&nbsp;&nbsp;&nbsp;' : '' ?><?= $subjName ?></td>
					<?php 
					foreach($YearClassID as $class){ 
						$score = $a_rec_arr[$subjectCode][$class][$type];
					?>
						<td><?= ($score)? $score : '--' ?></td>
					<?php 
					} 
					?>
					
				</tr>
			<?php 
			} 
			?>
		</tbody>
	</table>
</div>
<script>$(function() {$('#resultTable').floatHeader();});</script>
<!-- -------- Generate table END -------- -->



<?php


intranet_closedb();
?>