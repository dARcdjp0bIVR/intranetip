<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
$EC_BL_ACCESS_HIGH = (!strstr($ck_user_rights, ":manage:"));

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();

$RawData = $li_pf->returnIndividualClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $ClassName, $Semester);
$student_num = sizeof($RawData);
$max_student_num = $student_num-($student_num%10) + 10;

for($i=0; $i<sizeof($RawData); $i++)
{
	$ScoreArray[] = $RawData[$i][1];
}

//$chart_data .= "&data1=".$SubjectName.";";
$MaxScore = max($ScoreArray);
$MaxRange = ($MaxScore-($MaxScore%10)+10)/10;
$chart_title = "&title=;";
for($j=1; $j<=$MaxRange; $j++)
{
	$lowest = ($j==1) ? ($j-1)*10 : ($j-1)*10+1;
	$highest = $j*10;

	$chart_title .= $lowest."-".$highest.";";
	$data[$j] = 0;
}

for($i=0; $i<sizeof($ScoreArray); $i++)
{
	for($j=1; $j<=$MaxRange; $j++)
	{
		$score = $ScoreArray[$i];
		if($score<=$j*10)
		{
			$data[$j] = $data[$j] + 1;
			break;
		}
	}
}

$chart_data = "&data1=;";
for($i=1; $i<=sizeof($data); $i++)
{
	$chart_data .= $data[$i].";";
}
$chart_data = $chart_title.$chart_data;
//$chart_data = "&title=;0-10;11-20;21-30;31-40;41-50;51-60;61-70;71-80;81-90;91-100;&data1=;0;0;0;0;0;0;0;0;1;2; ";

$url_swf = "http://$eclass_httppath/src/includes/swf/zxchart_600x400.swf";
$y_text = $ec_iPortfolio['score'];
$x_text = $ec_iPortfolio['student_amount'];
$xy_text = $x_text."_".$y_text;
$axis_url = "xy=$xy_text";

$and_code = "%26";
$style_url = "ec_max=".$max_student_num.$and_code."ec_min=0".$and_code."hint_style=1";

$param_send = "stylefile=http://$eclass_httppath/src/includes/swf/portfolio/style_barchart.php?$style_url&datafile=$eclass_httppath/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;

# define the navigation, page title and table size
/*
$template_pages = Array(
					Array($ec_iPortfolio['class_dist_graph'], "")
					);
$template_width = "600";

$nav_color = ($ck_teacher_view) ? "red" : "blue";
echo getBodyBeginning($template_pages, $template_width, 1, $nav_color);
*/

$linterface = new interface_html("popup.html");
$CurrentPage = "ole_report_stat";
$title = $ec_iPortfolio['display_class_distribution'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td align="center">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td align="center">
								<table border="0" cellpadding="5">
									<tbody>
										<tr>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio_Report['subject']?> : <strong> <?=$SubjectCode." ".$SubjectComponentCode?></strong> </td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['year']?> :<strong> <?=$Year?></strong></td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['semester']?> : <strong><?=$Semester?></strong></td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['class']?> : <strong><?=$ClassName?></strong></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td align="right">&nbsp;</td>
						</tr>
					</tbody>
				</table>
      	
				<!-- flash start-->
				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="600" height="400">
					<param name="allowScriptAccess" value="sameDomain" >
					<param name="loop" value="false" >
					<param name="menu" value="false" >
					<param name="salign" value="lt" >
					<param name="scale" value="showall" >
					<param name="movie" value="<?=$url_swf?>">
					<param name="quality" value="high">
					<param name="flashvars" value="<?=$param_send?>" >
					<embed src="<?=$url_swf?>" quality="high" type="application/x-shockwave-flash" width="600" height="400">
					</embed>
				</object>
				<!-- flash end -->
			
			</td>
		</tr>
	</tbody>
</table>

<!--
<br>
<table width="450" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td class="13GrayP5555" width="25%" nowrap='nowrap' align="center"><?=$ec_iPortfolio_Report['subject']?> : <?=$SubjectCode." ".$SubjectComponentCode?></td>
	<td class="13GrayP5555" width="25%" nowrap='nowrap' align="center"><?=$ec_iPortfolio['year']?> : <?=$Year?></td>
	<td class="13GrayP5555" width="25%" nowrap='nowrap' align="center"><?=$ec_iPortfolio['semester']?> : <?=$Semester?></td>
	<td class="13GrayP5555" width="25%" nowrap='nowrap' align="center"><?=$ec_iPortfolio['class']?> : <?=$ClassName?></td>
</tr>
</table>
<table width="450" height="340" border="0" cellpadding="0" cellspacing="0" class="table_b">
<tr>
	<td align="center">
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="600" height="400">
	<param name="allowScriptAccess" value="sameDomain" >
	<param name="loop" value="false" >
	<param name="menu" value="false" >
	<param name="salign" value="lt" >
	<param name="scale" value="showall" >
	<param name="movie" value="<?=$url_swf?>">
	<param name="quality" value="high">
	<param name="flashvars" value="<?=$param_send?>" >
	<embed src="<?=$url_swf?>" quality="high" type="application/x-shockwave-flash" width="600" height="400">
	</embed>
	</object>
	</td>
</tr>
</table>
-->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
