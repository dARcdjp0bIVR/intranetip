<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ImportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

# Error Code
define ("ERROR_TYPE_STUDENT_NOT_FOUND", 1);
define ("ERROR_TYPE_DUPLICATED_REGNO", 2);
define ("ERROR_TYPE_ACTIVATED_ACOUNT", 3);
define ("ERROR_TYPE_DUPLICATED_CLASSNUM", 4);
define ("ERROR_TYPE_INCORRECT_REGNO", 5);
define ("ERROR_TYPE_INCORRECT_REGNO_2", 6);
unset($error_data);
$count_success = 0;

if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

$li_pf = new libportfolio2007();
$limport = new libimporttext();

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
   header("Location: import_regno.php?YearClassID=$YearClassID");
}
else
{
	$li = new libdb();
	$li->db = $intranet_db;
	$lf = new libfilesystem();

  $ext = strtoupper($lf->file_ext($filename));
  if ($ext == ".CSV" || $ext == ".TXT")
  {
     //$data = $lf->file_read_csv($filepath);
     $data = $limport->GET_IMPORT_TXT($filepath);
     $header_row = array_shift($data);                   # drop the title bar
  }

  # Check Title Row
  $format_wrong = false;
  if ($format == 1)
  {
      $file_format = array("LoginName","RegNo");
  } elseif ($format == 2)
  {
       $file_format = array("ClassName","ClassNumber","RegNo");
  } else
  {
      $format_wrong = true;
  }

  for ($i=0; $i<sizeof($file_format); $i++)
  {
       if ($header_row[$i]!=$file_format[$i])
       {
           $format_wrong = true;
           break;
       }
  }
  if ($format_wrong)
  {
    $correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
    for ($i=0; $i<sizeof($file_format); $i++)
    {
      $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
    }
    $correct_format .= "</table>\n";

    $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
    for ($i=0; $i<sizeof($header_row); $i++)
    {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
    }
    $wrong_format .= "</table>\n";

    $display_content .= "<br><span class='chi_content_15'>".$ec_guide['import_error_wrong_format']."</span><br>\n";
    $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
    $display_content .= "</table>\n";
  }
  else
  {
		# Get duplicated Class Name and Class Number
		$sql =	"
							SELECT 
								ClassName,
								ClassNumber,
								COUNT(UserID) 
							FROM 
								{$intranet_db}.INTRANET_USER 
							WHERE 
								RecordType = '2' AND
								(ClassName IS NOT NULL AND ClassName <> '') AND
								(ClassNumber IS NOT NULL AND ClassNumber <> '') AND
								RecordStatus IN (1,2)
							GROUP BY
								ClassName, ClassNumber
							ORDER BY
								ClassName, ClassNumber
						";
		$DuplicatedCheck = $li->returnArray($sql);
		for($i=0; $i<sizeof($DuplicatedCheck); $i++)
		{
			list($cname, $cnum, $count) = $DuplicatedCheck[$i];
			if($count>1)
			{
				$DuplicatedArr[trim($cname)][] = $cnum;
			}
		}

		# Get Student existing RegNo
    $sql =	"
							SELECT DISTINCT
								WebSAMSRegNo, 
								UserLogin, 
								ClassName, 
								ClassNumber
							FROM
							 INTRANET_USER
							WHERE
								(RecordType = '2' OR RecordType = '4') AND
								WebSAMSRegNo IS NOT NULL AND
								WebSAMSRegNo != ''
							ORDER BY
								ClassName, ClassNumber
						";
    $CurrentArray = $li->returnArray($sql);

		# Get Alumni existing RegNo 
		$sql =	"
							SELECT DISTINCT 
								WebSAMSRegNo, 
								UserLogin, 
								ClassName, 
								ClassNumber 
							FROM 
								INTRANET_ARCHIVE_USER                       
							WHERE 
								RecordType = '2' AND
								WebSAMSRegNo IS NOT NULL AND
								WebSAMSRegNo != ''
						";
		$ArchiveArray = $li->returnArray($sql);	
		$temp = array_merge($CurrentArray, $ArchiveArray);

    unset($array_websams_existing);
		for ($i=0; $i<sizeof($temp); $i++)
		{
			list($t_regno, $t_userlogin, $t_classname, $t_classno) = $temp[$i];
			$t_regno = trim($t_regno);
			if ($t_regno != "")
			{
				//$t_regno = str_replace("#", "", $t_regno);
				$array_websams_existing[$t_regno]["flag"] = 1;
				$array_websams_existing[$t_regno]["UserLogin"] = trim($t_userlogin);
				$array_websams_existing[$t_regno]["ClassName"] = trim($t_classname);
				$array_websams_existing[$t_regno]["ClassNumber"] = trim($t_classno);
			}
		}

		if ($format == 1) # Login name
		{
			$data = array_filter($data);

			for ($i=0; $i<sizeof($data); $i++)
			{
				list($t_login, $t_regno) = $data[$i];
				$t_login = trim($t_login);
				$t_regno = trim($t_regno);
				
				if ($t_login != "")
				{
					# check student exist or not
					$sql =	"
										SELECT
											UserID,
											ClassName,
											ClassNumber
										FROM
											INTRANET_USER
										WHERE
											UserLogin = '$t_login' AND
											RecordType = '2' AND
											RecordStatus IN (1,2)
									";
					$checkUserExist = $li->returnArray($sql);
					
					if (sizeof($checkUserExist)!=0)
					{
						# check wether the class name and class name are duplicated
						$ClassName = trim($checkUserExist[0]["ClassName"]);
						$ClassNumber = trim($checkUserExist[0]["ClassNumber"]);
						if(!empty($DuplicatedArr[$ClassName]))
						{
							if(in_array($ClassNumber, $DuplicatedArr[$ClassName]))
							{
								$error_data[] = array($i,ERROR_TYPE_DUPLICATED_CLASSNUM,$data[$i]);
								continue;
							}
						}
						
						# check if the student account has been activated
						$sql =	"
											SELECT DISTINCT
												ps.WebSAMSRegNo
											FROM
												INTRANET_USER as iu
											LEFT JOIN
												$eclass_db.PORTFOLIO_STUDENT as ps
											ON
												iu.WebSAMSRegNo = ps.WebSAMSRegNo
											WHERE
												iu.UserLogin = '$t_login' AND
												ps.WebSAMSRegNo IS NOT NULL
										";
						$checkActivated = $li->returnVector($sql);
						
						if($checkActivated[0]=="")
						{
							//$TrimRegNo = str_replace("#", "", $t_regno);
							if (substr($t_regno, 0, 1)!="#")
							{
								$error_data[] = array($i,ERROR_TYPE_INCORRECT_REGNO,$data[$i]);
								continue;
							}
							else if(!preg_match("/[a-zA-Z0-9]+/", substr($t_regno, 1)))
							{
								$error_data[] = array($i,ERROR_TYPE_INCORRECT_REGNO_2,$data[$i]);
								continue;
							}
							else if ($array_websams_existing[$t_regno]["flag"]==1 && $array_websams_existing[$t_regno]["UserLogin"]!=$t_login)
							{
								# Duplicated
								# Add Error Row
								$error_data[] = array($i,ERROR_TYPE_DUPLICATED_REGNO,$data[$i]);
								continue;
							}
							
							$sql =	"
												UPDATE
													INTRANET_USER
												SET
													WebSAMSRegNo = '$t_regno'
												WHERE
													UserLogin = '$t_login'
											";
							$li->db_db_query($sql);
							
							$count_success++;
							$array_websams_existing[$t_regno]["flag"] = 1;
							$array_websams_existing[$t_regno]["UserLogin"] = $t_login;
						}
						else
						{
							# Activated
							# Add Error Row
							$error_data[] = array($i,ERROR_TYPE_ACTIVATED_ACOUNT,$data[$i]);
						}
					}
					else
					{
						$error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
					}
				}
				else
				{
					$error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
				}
			}
		}
		else if ($format == 2) # ClassName, ClassNumber
		{
			for ($i=0; $i<sizeof($data); $i++)
			{
				list($t_class, $t_classnum, $t_regno) = $data[$i];
				$t_class = trim($t_class);
				$t_classnum = trim($t_classnum);
				$t_regno = trim($t_regno);
				
				if ($t_class != "" && $t_classnum != "")
				{
					# check student exist or not
					$sql =	"
										SELECT DISTINCT
											UserID
										FROM
											INTRANET_USER
										WHERE
											RecordType = '2' AND
											RecordStatus IN (1,2) AND
											ClassName = '$t_class' AND
											(ClassNumber = '$t_classnum' OR CONCAT('0',ClassNumber) = '$t_classnum' or ClassNumber = '0".$t_classnum."')
									";
					$checkUserExist = $li->returnVector($sql);
					
					if (sizeof($checkUserExist)>0)
					{
						if(sizeof($checkUserExist)>1)
						{
							$error_data[] = array($i,ERROR_TYPE_DUPLICATED_CLASSNUM,$data[$i]);
							continue;
						}
						
						# check if the student account has been activated
						$sql =	"
											SELECT DISTINCT
												ps.WebSAMSRegNo
											FROM
												INTRANET_USER as iu
											LEFT JOIN
												$eclass_db.PORTFOLIO_STUDENT as ps
											ON
												iu.WebSAMSRegNo = ps.WebSAMSRegNo
											WHERE
												iu.ClassName = '$t_class' AND
												(iu.ClassNumber = '$t_classnum' OR CONCAT('0',iu.ClassNumber) = '$t_classnum' or iu.ClassNumber = '0".$t_classnum."') AND
												ps.WebSAMSRegNo IS NOT NULL
										";
						$checkActivated = $li->returnVector($sql);
						
						if($checkActivated[0]=="")
						{
							//$TrimRegNo = str_replace("#", "", $t_regno);
							if (substr($t_regno, 0, 1)!="#")
							{
								$error_data[] = array($i,ERROR_TYPE_INCORRECT_REGNO,$data[$i]);
								continue;
							}
							else if(!preg_match("/[a-zA-Z0-9]+/", substr($t_regno, 1)))
							{
								$error_data[] = array($i,ERROR_TYPE_INCORRECT_REGNO_2,$data[$i]);
								continue;
							}
							else if($array_websams_existing[$t_regno]["flag"]==1 && ($array_websams_existing[$t_regno]["ClassName"]!=$t_class || $array_websams_existing[$t_regno]["ClassNumber"]!=$t_classnum))
							{
								# Duplicated
								# Add Error Row
								$error_data[] = array($i,ERROR_TYPE_DUPLICATED_REGNO,$data[$i]);
								continue;
							}
							
							$sql =	"
												UPDATE
													INTRANET_USER
												SET
													WebSAMSRegNo = '$t_regno'
												WHERE
												 ClassName = '$t_class' AND
												 (ClassNumber = '$t_classnum' OR CONCAT('0',ClassNumber) = '$t_classnum' or ClassNumber = '0".$t_classnum."')
											";
							$li->db_db_query($sql);
							
							$count_success++;
							$array_websams_existing[$t_regno]["flag"] = 1;
							$array_websams_existing[$t_regno]["ClassName"] = $t_class;
							$array_websams_existing[$t_regno]["ClassNumber"] = $t_classnum;
						}
						else
						{
							# Duplicated
							# Add Error Row
							$error_data[] = array($i,ERROR_TYPE_ACTIVATED_ACOUNT,$data[$i]);
						}
					}
					else
					{
						$error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
					}
				}
				else
				{
					$error_data[] = array($i,ERROR_TYPE_STUDENT_NOT_FOUND, $data[$i]);
				}
			}
		}
	}
}


# Display import stats
if (!$format_wrong)
{
	$display_content = "<span class='tabletext'>".$ec_guide['import_update_no']." : <b>$count_success</b></span><br>";
}

if (sizeof($error_data)>0)
{
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	$error_table .= "<tr class=tabletop><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";
	
	for ($i=0; $i<sizeof($error_data); $i++)
	{
		list ($t_row, $t_type, $t_data) = $error_data[$i];
		$t_row++;     # set first row to 1
		$RowStyle = ($i%2==0) ? "tablerow1" : "tablerow2";
		$error_table .= "<tr class='$RowStyle'><td class='tabletext'>$t_row</td><td class='tabletext'>";
		$reason_string = "Unknown";
		
		switch ($t_type)
		{
			case ERROR_TYPE_STUDENT_NOT_FOUND:
				$reason_string = $ec_guide['import_error_no_user'];
				break;
			case ERROR_TYPE_DUPLICATED_REGNO:
				$reason_string = $ec_guide['import_error_duplicate_regno'];
				break;
			case ERROR_TYPE_ACTIVATED_ACOUNT:
				$reason_string = $ec_guide['import_error_activated_regno'];
				break;
			case ERROR_TYPE_DUPLICATED_CLASSNUM:
				$reason_string = $ec_guide['import_error_duplicate_classnum'];
				break;
			case ERROR_TYPE_INCORRECT_REGNO:
				$reason_string = $ec_guide['import_error_incorrect_regno'];
				break;
			case ERROR_TYPE_INCORRECT_REGNO_2:
				$reason_string = $ec_guide['import_error_incorrect_regno_2'];
				break;
			default:
				$reason_string = $ec_guide['import_error_unknown'];
				break;
		}
		
		$error_table .= $reason_string;
		$error_table .= "</td><td class='tabletext'>".implode(", ",$t_data)."</td></tr>\n";
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

?>


<!-- ===================================== Body Contents ============================= -->

<FORM enctype="multipart/form-data" action="import_regno_update.php" method="POST" name="form1">
	<table width="98%" cellpadding="0" cellspacing="0">
		<tr>
			<td><?= $display_content ?></td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
		<tr>
			<td align="center">
				<input class="formbutton" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='import_regno.php?YearClassID=<?=$YearClassID?>'">
				<input class="formbutton" type="button" value="<?=$button_back?>" onClick="self.location='../../school_records_class.php?YearClassID=<?=$YearClassID?>'">
			</td>
		</tr>
	</table>
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
