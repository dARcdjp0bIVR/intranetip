<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
$EC_BL_ACCESS_HIGH = (!strstr($ck_user_rights, ":manage:"));

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$lpf_ui = new libpf_asr();

# Retrieve all subjects
$SubjectArray = $li_pf->returnAllSubjectWithComponents();
if($SubjectCode=="")
{
	for($i=0; $i<sizeof($SubjectArray); $i++)
	{
		if($SubjectArray[$i]["have_score"]==1)
		{
			$SubjectCode = $SubjectArray[$i][0];
			$SubjectComponentCode = $SubjectArray[$i][1];
			break;
		}
	}
}
else
{
	$temp = explode("###", $SubjectCode);
	if(sizeof($temp)>1)
	{	
		if($SubjectComponentCode=="")
		{
			$SubjectCode = trim($temp[0]);
			$SubjectComponentCode = trim($temp[1]);
		}
	}
}

list($SubjectSelection, $SubjectName) = $li_pf->getSubjectSelection($SubjectArray, "name='SubjectCode' onChange='jCHANGE_FIELD(\"subject\")'", $SubjectCode, $SubjectComponentCode);

# Resetting page number
switch($FieldChanged)
{
	case "page":
		break;
	case "subject":
		$Year = "";
	case "year":
		$Form = "";
	case "form":
	case "division":
	default:
		$Page = 1;
		break;
}
if(isset($FormHidden))
	$Form = $FormHidden;

# Retrieve distinct years
$YearArray = $li_pf->returnYearSelectionData($SubjectCode, $SubjectComponentCode);
$Year = ($Year=="" || !in_multi_array($Year, $YearArray)) ? $YearArray[0][0] : $Year;
//$year_selection = (sizeof($YearArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($YearArray, "name='Year' onChange='document.form1.submit();'", $ec_iPortfolio['school_yr'], $Year);
$year_selection = (sizeof($YearArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArray,"name='Year' onChange='jCHANGE_FIELD(\"year\")'",$Year,0,1);

# Retrieve semester selection list
$SemesterArray = $li_pf->returnAssessmentSemester($SubjectCode,$SubjectComponentCode,$Year);
//$Semester = ($Semester != "" || !in_multi_array($Semester, $SemesterArray)) ? $SemesterArray[0][0] : $Semester;
$Semester = ($Semester == "" || in_multi_array($Semester, $SemesterArray)) ? $Semester : $SemesterArray[0][0];
$LastSemester = $SemesterArray[sizeof($SemesterArray)-1];
//$semester_selection = (sizeof($SemesterArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($SemesterArray, "name='Semester' onChange='document.form1.submit();'", $Semester,0,1);
$semester_selection = (sizeof($SemesterArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($SemesterArray, "name='Semester' onChange='jCHANGE_FIELD(\"semester\")'", "-- ".$range_all." --", $Semester);

# Retrieve distinct form
$FormArray = $li_pf->returnAssessmentForm($SubjectCode, $SubjectComponentCode, $Year);
$Form = ($Form=="" || !in_multi_array($Form, $FormArray)) ? $FormArray[0][0] : $Form;
if(is_array($FormArray))
{
  for($i=0; $i<count($FormArray); $i++)
  {
    if($FormArray[$i][0] == $Form)
    {
      $FormName = $FormArray[$i][1];
      break;
    }
  }
}
//$form_selection = (sizeof($FormArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArrayTitle($FormArray, "name='Form' onChange='document.form1.submit();'", $ec_iPortfolio['whole_school'], $Form);
$form_selection = (sizeof($FormArray)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($FormArray, "name='Form' onChange='jCHANGE_FIELD(\"form\")'", $Form,0,1);

# Generate class performance table
$performance_data = $lpf_ui->generateClassPerformanceTable($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester, $LastSemester,array($PageDivision, $Page),$Style);
$class_table = $performance_data[0];
$chart_data = $performance_data[1];
$max_mean = $performance_data[2];

$url_swf = "$admin_url_path/src/includes/swf/zxchart_600x400.swf";
$y_text = $ec_iPortfolio['semester'];
$x_text = $ec_iPortfolio['mean'];
$xy_text = $x_text."_".$y_text;
$axis_url = "xy=$xy_text";

$and_code = "%26";
$style_url = "ec_max=$max_mean".$and_code."ec_min=0".$and_code."hint_style=3";

$param_send = "stylefile=$admin_url_path/src/includes/swf/portfolio/style_barchart.php?$style_url&datafile=$admin_url_path/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;

# Retrieve Number of Rows of Table For Navigation Menu Bar
if($Style == "class")
{
	$lp_template_list = $li_pf->returnClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester,1);
}
else
{
	$TempSemesterArr = $li_pf->returnClassPerformanceData($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester,2);

	# re-order the semester
	if(sizeof($TempSemesterArr)!=0)
	{
		list($IntranetSemester, $IntranetSemesterShort) = $li_pf->getSemesterNameArrFromIP();

		for($i=0; $i<sizeof($IntranetSemester); $i++)
		{
			for($j=0; $j<sizeof($TempSemesterArr); $j++)
			{
				if($IntranetSemester[$i]==$TempSemesterArr[$j][0])
				{
					$lp_template_list[] = array($TempSemesterArr[$j][0], $TempSemesterArr[$j][0]);
					break;
				}
			}
		}

		if(in_multi_array("", $TempSemesterArr))
			$lp_template_list[] = array(trim($ec_iPortfolio['overall_result']), trim($ec_iPortfolio['overall_result']));
	}
}

# define the navigation, page title and table size
# template for teacher page
$linterface = new interface_html();
# set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("score_list.php", $ec_iPortfolio['master_score_list'], 0);
$TabMenuArr[] = array("overall_performance_stat.php", $ec_iPortfolio['overall_perform_stat'], 0);
$TabMenuArr[] = array("class_performance_stat.php", $ec_iPortfolio['class_perform_stat'], 1);
$TabMenuArr[] = array("student_performance_stat.php", $ec_iPortfolio['student_perform_stat'], 0);

?>

<script language="javascript">

function filter_change(ftype){
	document.form1.submit();
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "stat_class.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "stat_class.php";
	document.form1.submit();	
}

// Change display style
function jCHANGE_STYLE(jParStyle){
	document.form1.Style.value = jParStyle;
	document.form1.action = "stat_class.php";
	document.form1.submit();
}

//
function jTO_CLASS_PERFORMANCE(jParLevel)
{
	document.form1.Style.value = '';
	document.form1.FormHidden2.value = "S"+jParLevel;
	document.form1.action = "class_performance_stat.php";
	document.form1.submit();
}

</script>

<form name="form1" method="POST" action="stat_class.php">
<?
echo $lpf_ui->GET_TAB_MENU($TabMenuArr);
?>
<table width="100%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellpadding="3" cellspacing="0" width="100%">
				<tr>
					<td>
						<span class="thumb_list">
							&nbsp;&nbsp;<?=$SubjectSelection?>&nbsp;
							<?=$year_selection?>
							<?=$form_selection?>
							<?=$semester_selection?>
						</span>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	<tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td width="60">&nbsp;</td>
			<td>
				<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td class="navigation"><img src="<?=$PATH_WRT_ROOT?>/images/2009a/nav_arrow.gif" align="middle" height="15" width="15"> <a href="javascript:jTO_CLASS_PERFORMANCE('<?=$Form?>')"><?=$ec_iPortfolio['all_statistic'] ?></a><img src="<?=$PATH_WRT_ROOT?>/images/2009a/nav_arrow.gif" align="middle" height="15" width="15"><?=$FormName ?></td>
							<td class="thumb_list" align="right" valign="top">
								<?=$ec_iPortfolio['group_by']?> :
<?php if($Style=='term') { ?>
								<span><?=$ec_iPortfolio['term'] ?> </span> | <a href="javascript:jCHANGE_STYLE('class')"> <?=$ec_iPortfolio['class'] ?></a>
<?php } else { ?>
								<a href="javascript:jCHANGE_STYLE('term')"><?=$ec_iPortfolio['term'] ?> </a> | <span> <?=$ec_iPortfolio['class'] ?></span>
<?php } ?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="60">&nbsp;</td>
		</tr>
	</tbody>
</table>

<table border=0 width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center"><?=$class_table?></td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<table width="90%" border="0" cellpadding="0" cellspacing="0">
							<tr class="tablebottom">
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="3"> <!-- navigation -->
										<tr>
											<td class="tabletext">
												<?=$ec_iPortfolio['record']?>
												<?=$lpf_ui->GEN_PAGE_ROW_NUMBER(count($lp_template_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($lp_template_list), $ec_iPortfolio['total_record'])?>
											</td>
											<td align="right">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<table border="0" cellspacing="0" cellpadding="2">
																<tr align="center" valign="middle">
																	<td>
																		<a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp25" width="11" height="10" border="0" align="absmiddle" id="prevp25"></a>
																		<span class="tabletext"> <?=$list_page?> </span>
																	</td>
																	<td class="tabletext">
																		<?=$lpf_ui->GEN_PAGE_SELECTION(count($lp_template_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
																	</td>
																	<td>
																		<span class="tabletext"> </span>
																		<a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp25','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp25" width="11" height="10" border="0" align="absmiddle" id="nextp25"></a>
																	</td>
																</tr>
															</table>
														</td>
														<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
														<td>
															<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
																<tr>
																	<td><?=$i_general_EachDisplay?></td>
																	<td>
																		<?=$lpf_ui->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'")?>
																	</td>
																	<td><?=$i_general_PerPage?></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width='100%' border='0' cellspacing='0' cellpadding='2'>
	<tbody>
		<tr>
			<td width="60">&nbsp;</td>
			<td width="200">
				<span class="tabletextrequire2">&uarr;</span>
				<?=$ec_iPortfolio['above_mean']?>&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				<span class="tabletextrequire">&darr;</span>
				<?=$ec_iPortfolio['below_mean']?>
			</td>
		</tr>
	</tbody>
</table>

<input type="hidden" name="FieldChanged" />
<!--		
<input type="hidden" name="class" value="<?=$class ?>" />		
<input type="hidden" name="term" value="<?=$term ?>" />
-->		
<input type="hidden" name="Style" value="<?=$Style ?>" />
<input type="hidden" name="FormHidden2" value="S<?=$Form?>" />
</form>


<?php
/*
echo "Year: ".$Year."<br>";
echo "Form: ".$Form."<br>";
echo "Semester: ".$Semester."<br>";
echo "SubjectCode: ".$SubjectCode."<br>";
echo "SubjectComponentCode: ".$SubjectComponentCode."<br>";
echo "class: ".$class."<br>";
echo "term: ".$term."<br>";
*/
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
