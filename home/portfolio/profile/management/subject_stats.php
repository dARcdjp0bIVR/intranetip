<?php
// Modifing by Pun
/**
 * Change Log:
 * 2016-08-30 (Pun):
 *  - Fix cannot see reports if not select all year
 * 2015-02-17 (Pun):
 *  - Changed type filter lang
 * 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_management_stdprogress_page_size", "numPerPage");
$arrCookies[] = array("ck_management_advancement_page_number", "pageNo");
$arrCookies[] = array("ck_management_advancement_page_order", "order");
$arrCookies[] = array("ck_management_advancement_page_field", "field");
$arrCookies[] = array("ck_management_advancement_YearID", "YearID");
$arrCookies[] = array("ck_management_advancement_YearClassID", "YearClassID");
$arrCookies[] = array("ck_management_advancement_FromAcademicYearID", "FromAcademicYearID");
$arrCookies[] = array("ck_management_advancement_FromYearTermID", "FromYearTermID");
$arrCookies[] = array("ck_management_advancement_FromSubjectID", "FromSubjectID");
$arrCookies[] = array("ck_management_advancement_ToAcademicYearID", "ToAcademicYearID");
$arrCookies[] = array("ck_management_advancement_ToYearTermID", "ToYearTermID");
$arrCookies[] = array("ck_management_advancement_ToSubjectID", "ToSubjectID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_management_stdprogress_page_size = '';
}
else 
	updateGetCookies($arrCookies);

iportfolio_auth("T");
intranet_opendb();

$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();


########### Assess Right START ###########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
if($accessRight['admin']){
	$subjectIdArr = '';
}else{
	$subjectIdArr = array_keys((array)$accessRight['subjectPanel']);
}
########### Access Right END ###########


########################################################
# Report printing options: Start
########################################################
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

# Form Selection
//$FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);

# retrieve distinct years
//$FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='', $noFirst=1, $noPastYear=0, $FromAcademicYearID);

$YearArr = $li_pf->returnAssessmentYear();
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='SingleAcademicYearID' id='FromAcademicYearID' ", $FromAcademicYearID, 0, 0, $iPort["all_school_years"], 2);


################################################################################
// Subject Selection
$rs = $lpf->getReleatedSubjectID();
$releatedSubjectIdArr = array();
foreach($rs as $r){
	foreach($r as $r2){
// 		$releatedSubjectIdArr[] = $r2;
	}
}
// $FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', '', $OnChange='', $noFirst=1, '', '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdArr, $ExtrudeSubjectIDArr=$releatedSubjectIdArr);
$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection_ForOldSDAS('FromSubjectID', '', $OnChange='', $noFirst=1, '', '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdArr, $ExtrudeSubjectIDArr=$releatedSubjectIdArr);


################################################################################

########################################################
# Report printing options: End
########################################################


# View Button
$linterface = new interface_html();
$btn_View = $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", $onclick="js_Reload_DBTable();", $id="Btn_View");


# define the navigation, page title and table size
// template for teacher page
// set the current page title
$CurrentPage = "Teacher_AssessmentStatisticReport";
$CurrentPageName = $iPort['menu']['assessment_statistic_report'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# get tab menu
$TabIndex = "SubjectStats";
$TabMenuArr = libpf_tabmenu::getAssessmentStatTags($TabIndex);
$html_tab_menu = libportfolio_ui::GET_TAB_MENU($TabMenuArr);

$linterface->LAYOUT_START();

/*
$SubjectID = 21; // for overall result
$YearIDs = array(9, 14);
//$DataType = "TERM"; //"ASSESSMENT"; 
$DataType = "ASSESSMENT";
//$TermIDs = "";
$ClassID = 367;
//$IsMain = true;
//$h_DBTable =  $li_pf->GetClassCrossYearBySubject($YearIDs, $SubjectID, $ClassID, $DataType);
 * 
 */
?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	js_Reload_Term_Selection('From');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
	
	if($('#FromAcademicYearID').val() == 0){
		if(
			$('#resultType_SelectAll:checked').length == 0 &&
			$('#resultType_Form:checked').length == 0 &&
			$('#resultType_Class:checked').length == 0
		){
			alert('<?= $iPort["options_select"]?> <?=$iPort["report_col"]["type"]?>');
			return false;
		}
	}else{
		if(
			$('#resultType_SelectAll:checked').length == 0 &&
			$('#resultType_Form:checked').length == 0 &&
			$('#resultType_Class:checked').length == 0 &&
			$('#resultType_Group:checked').length == 0
		){
			alert('<?= $iPort["options_select"]?> <?=$iPort["report_col"]["type"]?>');
			return false;
		}
	}
// 	console.log($("#form1").serialize());
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_get_mark_analysis.php", 
		$("#form1").serialize(),
		function(ReturnData)
		{
		}
	);
}

</script>

<form id="form1" name="form1" method="POST" action="advancement.php">
	<input type="hidden" name="Action" value="SUBJECT_STATS" />
	<?=$html_tab_menu ?>
	
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$FromAcademicYearSelection?></td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
			<td valign="top"><?=$FromSubjectSelection?></td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["type"]?></span></td>
			<td valign="top">
				<input type="checkbox" id="resultType_SelectAll" />
				<label for="resultType_SelectAll"><?=$Lang['Btn']['SelectAll']?></label> &nbsp;&nbsp;&nbsp;
				
				<input type="checkbox" id="resultType_Form" name="ResultType[]" value="form" />
				<label for="resultType_Form"><?=$ec_iPortfolio['form']?></label> &nbsp;
				<input type="checkbox" id="resultType_Class" name="ResultType[]" value="class" />
				<label for="resultType_Class"><?=$ec_iPortfolio['class']?></label> &nbsp;
				<span id="typeGroupDiv" style="display: none;">
					<input type="checkbox" id="resultType_Group" name="ResultType[]" value="group" />
					<label for="resultType_Group"><?=$iPort["SubjectGroup"]?></label> &nbsp;
				</span>
			</td>
		</tr>
	</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px">
		<?= $btn_View ?>
	</div>
	
	<div id="DBTableDiv"><?=$h_DBTable?></div>
	
</form>

<script>
$("#resultType_SelectAll").click(function(){
	if($(this).attr("checked")){
		$("#resultType_Form").attr("checked", "checked");
		$("#resultType_Class").attr("checked", "checked");
		$("#resultType_Group").attr("checked", "checked");
	}else{
		$("#resultType_Form").removeAttr("checked");
		$("#resultType_Class").removeAttr("checked");
		$("#resultType_Group").removeAttr("checked");
	}
});
$("#resultType_Form, #resultType_Class, #resultType_Group").click(function(){
	if(!$(this).attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
});

$('#FromAcademicYearID').change(function(){
	if($(this).val() == 0){
		$('#typeGroupDiv').hide();
	}else{
		$('#typeGroupDiv').show();
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>