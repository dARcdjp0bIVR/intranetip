<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
$EC_BL_ACCESS_HIGH = (!strstr($ck_user_rights, ":manage:"));

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


$li_pf = new libpf_asr();
$li_pf_2007a = new libpf_asr();

list($chart_data, $MaxValue) = $li_pf->generateLevelPerformanceTable($SubjectCode, $SubjectComponentCode, $Year, $Form, $Semester, $type);

$url_swf = "http://$eclass_httppath/src/includes/swf/zxchart_600x400.swf";
$y_text = $ec_iPortfolio['semester'];
switch($type)
{
	case "mean":
		$x_text = $ec_iPortfolio['mean'];
		break;
	case "sd":
		$x_text = $ec_iPortfolio['standard_deviation'];
		break;
	case "max":
		$x_text = $ec_iPortfolio['highest_score'];
		break;
	case "min":
		$x_text = $ec_iPortfolio['lowest_score'];
		break;
}
$xy_text = $x_text."_".$y_text;
$axis_url = "xy=$xy_text";

$and_code = "%26";
$style_url = "ec_max=".$MaxValue.$and_code."ec_min=0".$and_code."hint_style=3";

$param_send = "stylefile=http://$eclass_httppath/src/includes/swf/portfolio/style_barchart.php?$style_url&datafile=$eclass_httppath/src/includes/swf/portfolio/axis.php?$axis_url". $chart_data;

$linterface = new interface_html("popup.html");
$CurrentPage = "ole_report_stat";
$title = $ec_iPortfolio['display_class_stat'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td align="center">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td align="center">
								<table border="0" cellpadding="5">
									<tbody>
										<tr>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio_Report['subject']?> : <strong><?=$SubjectCode." ".$SubjectComponentCode?></strong> </td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['year']?> :<strong> <?=$Year?></strong></td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio['semester']?> : <strong><?=($Semester==""?$range_all:$Semester)?></strong></td>
											<td align="center" nowrap="nowrap" width="25%"><?=$ec_iPortfolio_Report['form']?> : <strong><?=$li_pf->getClassLevelName($Form)?></strong></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td align="right">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				
				<!-- flash start-->
				<object classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="600" height="400">
					<param name="allowScriptAccess" value="sameDomain" />
					<param name="loop" value="false" />
					<param name="menu" value="false" />
					<param name="salign" value="lt" />
					<param name="scale" value="showall" />
					<param name="movie" value = "<?=$url_swf?>">
					<param name="quality" value = "high">
					<param name="flashvars" value="<?=$param_send?>" />
					<embed src = "<?=$url_swf?>" quality = high type = "application/x-shockwave-flash" width="600" height="400">
					</embed>
				</object>
				<!-- flash end -->
				
			</td>
		</tr>
	</tbody>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
