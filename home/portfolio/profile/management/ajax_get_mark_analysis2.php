<?php
// using : 
/**
 * Change Log:
 * 2017-03-07 Villa X114059
 *  -	SUBJECT_STATS - Fix accessRight after supporting subject CMP in subject panel  
 *  -	SUBJECT_IMPROVE - Fix accessRight problem after supporting subject CMP in subject panel  
 * 2017-02-27 Villa
 *  -	SUBJECT_STATS - for iport (OLD SDAS All Year) -> redirect using SDAS cross year function for fixing MainTerm/ AssessmentTerm Problem
 * 2017-01-11 Villa
 * 	-	SUBJECT_STATS - redirect cross year to new function
 * 2016-12-14 Villa
 *  -	STUDENT_PROGRESS - subject teacher access right
 * 2016-12-09 Omas
 *  -	STUDENT_PROGRESS - add support for SubjectGroup
 * 2016-03-08 Pun
 * 	-	Added 'STUDENT_RESULTS_PERCENTILE' for Student Performance Tracking (Percentile)
 * 2016-02-26 Carlos
 *  - Modified $Action=="TEACHER_STATS_IMPROVE", added request parameter $prepareChartData and pass into $lpf->GetTeacherStatsCompareByTerm() to generate javascript data. 
 * 2016-01-26 Pun
 * 	-	Modified 'STUDENT_PROGRESS', added support subject panel
 * 	-	Modified 'STUDENT_RESULTS', added export function
 * 	-	Modified 'TEACHER_STATS', added support subject panel
 * 	-	Modified 'TEACHER_STATS_IMPROVE', added support subject panel
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libportfolio2.php");
$lpf = new libportfolio();

$accessRight = $lpf->getAssessmentStatReportAccessRight();
$canAccess = true;
$currentAcademicYearID = Get_Current_Academic_Year_ID();

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == "STUDENT_PROGRESS")
{
	########## Access Right START ##########
	$canAccess = false;

	#### Admin Access Right START ####
	if($accessRight['admin']){
		$canAccess = true;
	}
	#### Admin Access Right END ####
	
	#### Subject Panel Access Right START ####
	if(count($accessRight['subjectPanel'])){
		$subjectArr = array_keys($accessRight['subjectPanel']);
		if(in_array($_REQUEST['FromSubjectID'], $subjectArr)){
			$canAccess = true;
		}
	}
	#### Subject Panel Access Right END ####
	
	#### Class Teacher Access Right START ####
	$classIdArr = array();
	foreach($accessRight['classTeacher'] as $yearClass){
		$classIdArr[] = $yearClass['YearClassID'];
	}
	
	if(
		in_array($YearClassID, $classIdArr) || 
		($SubjectGroupID && in_array($SubjectGroupID, $accessRight['subjectGroup']))
	){
		$canAccess = true;
	}
	if($accessRight['subjectTeacher']){
		$canAccess = true;
	}
	#### Class Teacher Access Right END ####
	
	###MonitoringPIC Access Right
	include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
	$libSDAS= new libSDAS();
	$accessRight_SDAS = $libSDAS->getAssessmentStatReportAccessRight();
	if($accessRight_SDAS['MonitoringGroupPIC']){
		$canAccess = true;
	}
	### END
	
	if(!$canAccess){
		echo 'Access Denied.';
		die;
	}
	########## Access Right END ##########
	// 20161209 Omas
	// for generate report by subject Group 
	$MonitoringGroupPIC = '';
	if($TypeBtn == 'SubjectClass'){
		// using subject Group
		$SubjectGroupID =  $_REQUEST['subjectGroup_box'];
		$YearClassID = '';
	}elseif($TypeBtn == 'MonitoringGroup'){
		$MonitoringGroupPIC = $_REQUEST['MonitoringGroupPIC'];
	}else{
		// using Class
		$YearClassID = $_REQUEST['YearClassID'];
	}
	########## Get Student in SubjectGroup START ##########
	$addStudentArr = array();
	if($SubjectGroupID){
		$lib_subject_class_mapping = new subject_class_mapping();
				
		$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID));
		for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++){
			$addStudentArr[] = $InvolvedStudentArr[$i_std]["UserID"];
		}
	}
	if($MonitoringGroupPIC){
		$studentArr = $libSDAS->getMonitoringGroupMember('2',$MonitoringGroupPIC);
		foreach ((array)$studentArr as $_studentArr){
			$addStudentArr[] = $_studentArr['UserID'];
		}
	}
	########## Get Student in SubjectGroup END ##########
	
// 	$YearClassID = $_REQUEST['YearClassID'];

	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$FromSubjectID = $_REQUEST['FromSubjectID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	$DataType = "ALL";
	$filterColumnArr = $_REQUEST['filterColumn'];
	$AcademicYears = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
	$table_html = $lpf->GetClassCrossYearBySubject($AcademicYears, $FromSubjectID, $YearClassID, $DataType, $filterColumnArr, $addStudentArr);

	echo $table_html;
} elseif ($Action=="STUDENT_RESULTS")
{
	########## Access Right START ##########
	if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
			in_array('student_marksheets',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
		$accessRight['admin'] = 1;
	}
	
	if(!$accessRight['admin']){
		$classIdArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
			$classIdArr[] = $yearClass['YearClassID'];
		}
		if(!in_array($YearClassID, $classIdArr)){
			$canAccess = false;
		}
		
		$studentIdArr = array();
		$form_class_manage = new year_class($YearClassID, false, false, true);
		$studentList = $form_class_manage->ClassStudentList;
		foreach($studentList as $student){
			$studentIdArr[] = $student['UserID'];
		}
		if(!in_array($StudentID, $studentIdArr)){
			$canAccess = false;
		}
		
		if(!$canAccess){
			echo 'Access Denied.';
			die;
		}
	}
	########## Access Right END ##########
	
	$StudentID = $_REQUEST['StudentID'];
	$filterColumnArr = $_REQUEST['filterColumn'];
	$isExport = $_REQUEST['isExport'];
	//echo $StudentID;
	echo $lpf->GetStudentCrossYearResults($StudentID, $filterColumnArr, $isExport);
} elseif ($Action=="STUDENT_RESULTS_PERCENTILE")
{
	########## Access Right START ##########
	if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
			in_array('student_marksheets',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
		$accessRight['admin'] = 1;
	}
	if(!$accessRight['admin']){
		$classIdArr = array();
		foreach($accessRight['classTeacher'] as $yearClass){
			$classIdArr[] = $yearClass['YearClassID'];
		}
		if(!in_array($YearClassID, $classIdArr)){
			$canAccess = false;
		}
		
		$studentIdArr = array();
		$form_class_manage = new year_class($YearClassID, false, false, true);
		$studentList = $form_class_manage->ClassStudentList;
		foreach($studentList as $student){
			$studentIdArr[] = $student['UserID'];
		}
		if(!in_array($StudentID, $studentIdArr)){
			$canAccess = false;
		}
		
		if(!$canAccess){
			echo 'Access Denied.';
			die;
		}
	}
	########## Access Right END ##########
	
	$StudentID = $_REQUEST['StudentID'];
	$isExport = $_REQUEST['isExport'];
	//echo $StudentID;
	echo $lpf->GetStudentCrossYearPercentileResults($StudentID/*, $isExport*/);
} elseif ($Action=="SUBJECT_STATS")
{
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	$SingleAcademicYearID = $_REQUEST['SingleAcademicYearID'];
	$FromSubjectID = $_REQUEST['FromSubjectID'];
	$ResultType = (array)$_REQUEST['ResultType'];
	
	$lpf = new libPortfolio();
	########## Access Right START ##########
	$yearIdFilterArr = array();
	if(!$accessRight['admin']){
		$MainSubjectID_temp = explode('_',$FromSubjectID);
		$MainSubjectID = $MainSubjectID_temp[0];
		$yearIdFilterArr = array_keys((array)$accessRight['subjectPanel'][$MainSubjectID]);
		if(!$yearIdFilterArr){
			echo 'Access Denied.';
			die;
		}
		## Get all YearClassID START ##
		/*$yearClassIdArr = array();
		
		foreach($yearIdArr as $yearID){
			$objYear = new Year($yearID);
			$rs = $objYear->Get_All_Classes(0, $FromAcademicYearID);
			foreach($rs as $r){
				$yearClassIdArr[] = $r['YearClassID'];
			}
		}*/
		## Get all YearClassID END ##
	}
	########## Access Right END ##########	
	
	
	
	//debug($FromAcademicYearID,$FromSubjectID );
	if($FromAcademicYearID){
		if($ToAcademicYearID){
			$total_array_selected = $lpf->GetAcademicYears($FromAcademicYearID,$ToAcademicYearID);
// 			echo $lpf->GetSubjectStatsSelectYear($FromSubjectID, $ResultType,$yearIdFilterArr,$total_array_selected);
			echo $lpf->GetSubjectStatsSelectYear_new($FromSubjectID, $ResultType,$yearIdFilterArr,$total_array_selected);
		}
	}else if($SingleAcademicYearID){
		echo $lpf->GetSubjectStats($SingleAcademicYearID, $FromSubjectID, $ResultType, $yearIdFilterArr);
	}else{
		$sql = "SELECT 
					Distinct 
					AcademicYearID
				FROM
					{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
					";
		$rs = $lpf->returnResultSet($sql);
		foreach ($rs as $_rs){
			$total_array_selected[] = $_rs['AcademicYearID'];
		}
// 		echo $lpf->GetSubjectStatsAllYear($FromSubjectID, $ResultType, $yearIdFilterArr);
		echo $lpf->GetSubjectStatsSelectYear_new($FromSubjectID, $ResultType,$yearIdFilterArr,$total_array_selected);
	}
} elseif ($Action=="TEACHER_STATS")
{
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$TeacherID = $_REQUEST['TeacherID'];
	$filterSubjectIdArr = array();
	//debug($FromAcademicYearID, $TeacherID );
	
	if( !$accessRight['admin'] && count($accessRight['subjectPanel']) == 0 ){
		if($TeacherID != $_SESSION['UserID']){
			echo 'Access Denied.';
			die;
		}
	}else if( (!$accessRight['admin']) && ($TeacherID != $_SESSION['UserID']) ){
		// Filter the subject ID if the teacher is not self.
		$filterSubjectIdArr = (array)$_REQUEST['filterSubjectIdArr'];
	}
	echo $lpf->GetTeacherStats($FromAcademicYearID, $TeacherID, $filterSubjectIdArr);
} elseif ($Action=="SUBJECT_IMPROVE")
{
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	//$FromTermAssessmentID = $_REQUEST['FromTermAssessmentID'];
	//$ToTermAssessmentID = $_REQUEST['ToTermAssessmentID'];
	$FromTermAssessmentID = $_REQUEST['FromYearTermID'];
	$ToTermAssessmentID = $_REQUEST['ToYearTermID'];
	$FromSubjectID = $_REQUEST['FromSubjectID'];
	$ResultType = (array)$_REQUEST['ResultType'];
	
	########## Access Right START ##########
	$yearIdFilterArr = array();
	if(!$accessRight['admin']){
		$MainSubjectID_temp = explode('_',$FromSubjectID);
		$MainSubjectID = $MainSubjectID_temp[0];
		$yearIdFilterArr = array_keys((array)$accessRight['subjectPanel'][$MainSubjectID]);
	}
	########## Access Right END ##########	
	
	//debug($FromAcademicYearID, $FromSubjectID, $FromTermAssessmentID,  $ToTermAssessmentID);
	$arrTmp = explode("_", $FromTermAssessmentID);
	if (sizeof($arrTmp)>0)
	{
		$FromYearTermID = $arrTmp[0];
		$FromYearTermAssessment = $arrTmp[1];
	} else
	{
		$FromYearTermID = $FromTermAssessmentID;
	}
	
	$arrTmp = explode("_", $ToTermAssessmentID);
	if (sizeof($arrTmp)>0)
	{
		$ToYearTermID = $arrTmp[0];
		$ToYearTermAssessment = $arrTmp[1];
	} else
	{
		$ToYearTermID = $FromTermAssessmentID;
	}
	
	echo $lpf->GetSubjectStatsCompareByTerm($FromAcademicYearID, $FromSubjectID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $ResultType, $yearIdFilterArr);
} elseif ($Action=="TEACHER_STATS_IMPROVE")
{
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$FromTermAssessmentID = $_REQUEST['FromYearTermID'];
	$ToTermAssessmentID = $_REQUEST['ToYearTermID'];
	$TeacherID = $_REQUEST['TeacherID'];
	//debug($FromAcademicYearID, $TeacherID, $FromTermAssessmentID,  $ToTermAssessmentID);
	
	$prepareChartData = $_REQUEST['prepareChartData']==1;
	
	$filterSubjectIdArr = array();
	if( !$accessRight['admin'] && count($accessRight['subjectPanel']) == 0 ){
		if($TeacherID != $_SESSION['UserID']){
			echo 'Access Denied.';
			die;
		}
	}else if( (!$accessRight['admin']) && ($TeacherID != $_SESSION['UserID']) ){
		// Filter the subject ID if the teacher is not self.
		$filterSubjectIdArr = (array)$_REQUEST['filterSubjectIdArr'];
	}
	
	$arrTmp = explode("_", $FromTermAssessmentID);
	if (sizeof($arrTmp)>0)
	{
		$FromYearTermID = $arrTmp[0];
		$FromYearTermAssessment = $arrTmp[1];
	} else
	{
		$FromYearTermID = $FromTermAssessmentID;
	}
	
	$arrTmp = explode("_", $ToTermAssessmentID);
	if (sizeof($arrTmp)>0)
	{
		$ToYearTermID = $arrTmp[0];
		$ToYearTermAssessment = $arrTmp[1];
	} else
	{
		$ToYearTermID = $FromTermAssessmentID;
	}
// 	debug($FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment);
	echo $lpf->GetTeacherStatsCompareByTerm($FromAcademicYearID, $TeacherID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $filterSubjectIdArr, $prepareChartData);
}elseif ($Action=="INTERNAL_VALUE_ADDED_STAT"){
	$FromYearTermID = $_REQUEST['FromYearTermID'];
	$ToYearTermID = $_REQUEST['ToYearTermID'];
	
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	
	$YearID = $_REQUEST['YearID'];
	$SubjectID = $_REQUEST['FromSubjectID'];
	
	########## Access Right START ##########
	if(!$accessRight['admin']){
		if(!$accessRight['subjectPanel'][$SubjectID][$YearID]){
			echo 'Access Denied.';
			die;
		}
	}
	########## Access Right END ##########	
	
	echo $lpf->GetInternalValueAddedStat2($FromYearTermID, $ToYearTermID, $YearID, $SubjectID);
} 

intranet_closedb();
?>