<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();
$class_arr = $li_pf->getPortfolioClass2($ay_id, "", false);

for($i=0; $i<ceil(count($class_arr)/3)*3; $i++)
{
  $_classEn = $class_arr[$i]['ClassTitleEN'];
  $_classB5 = $class_arr[$i]['ClassTitleB5'];
  $_class = Get_Lang_Selection($_classB5, $_classEn);
  if($_classEn == "") continue;
  
  $class_arr[$i] = array($_classEn, $_class);
}
$html_class_selection = (sizeof($class_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($class_arr, "name='ClassName' id='ClassName'", "", 0, 0, "", 2);

echo $html_class_selection;

intranet_closedb();
?>