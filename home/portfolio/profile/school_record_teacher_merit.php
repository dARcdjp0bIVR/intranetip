<?php
/**
 * [Modification Log] Modifying By:  
 * ************************************************
 *  Date:   2018-04-25 (Anna) #K138549
 *          -   added oreder by MeritDate
 *  
 *  Date:	2018-01-09 (Omas) #W123268 
 *  		-	changed merit lang to follow admin console settings  
 *	Date:   2017-03-29 (Siuwan) #X115139
 *		 	- 	add LP version checking in js function jTO_LP()  
 *
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

update_login_session();

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

include_once($PATH_WRT_ROOT."includes/libpf-sem-map.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("merit");

# Get class selection
$lpf_fc = new libpf_formclass();
$t_class_arr = $lpf_fc->GET_CLASS_LIST();
$class_name = $i_general_WholeSchool;
for($i=0; $i<count($t_class_arr); $i++)
{
  $t_yc_id = $t_class_arr[$i][0];
  $t_yc_title = Get_Lang_Selection($t_class_arr[$i]['ClassTitleB5'], $t_class_arr[$i]['ClassTitleEN']);
  if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
  {
    $class_arr[] = array($t_yc_id, $t_yc_title);
  }
  
  if($t_yc_id == $YearClassID)
  {
    $class_name = $t_yc_title;
  }
}
$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id)
      $default_student_id = $StudentID;
  
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

$miscParameter = explode("&", $miscParameter);
for($i=0; $i<count($miscParameter); $i++)
{
	$temp = explode("=", $miscParameter[$i]);
	${$temp[0]} = $temp[1]; 
}

////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;

$li = new libdbtable2007($field, $order, $pageNo);

# handle semester different problem
list($SemesterArr, $ShortSemesterArr) = $li_pf->getSemesterNameArrFromIP();
list($SemList, $List1, $List2, $List3) = $li_pf->getSemesterMatchingLists($semester, $SemesterArr);

// Modified by Key (29-09-2008): Add checking the value of $SemList is empty or not
if($semester!="" && $semester!=$ec_iPortfolio['whole_year'] && $SemList!="")
{
  # Semester Mapping
  $sem_map_arr = SemesterMapping();
  
  if(is_array($sem_map_arr))
  {
    foreach($sem_map_arr AS $tar_sem => $src_sem)
    {
      if($src_sem == $semester)
      {
        $cond_sem_arr[] = $tar_sem;
      }
    }
  }
  
  if(is_array($cond_sem_arr))
  {
    $cond_sem_str = implode("', '", $cond_sem_arr);
  }

	//$conds = "AND (ms.Semester = '$semester' OR ms.Semester IN ({$SemList}))";
	//$conds = "AND ms.Semester = '$semester'";
	//$conds = "AND ms.Semester IN ('$cond_sem_str')";
	$conds = "AND (ayt.YearTermNameEN IN ('$cond_sem_str') OR ayt.YearTermNameB5 IN ('$cond_sem_str'))";
}

if($sys_custom['iPortfolioHideRemark']==true)
{	
	$li->field_array = array("ms.Year", "ms.Semester", "ms.RecordType", "ms.NumberOfUnit", "ms.Reason", "ms.PersonInCharge", "ms.ModifiedDate");
	$li->no_col = 8;
	$li->column_array = array(0,0,0,0,0,0,0);
	$ReasonWidth = "40%";
}
else
{
	$li->field_array = array("ms.Year", "ms.Semester", "ms.RecordType", "ms.NumberOfUnit", "ms.Reason", "ms.Remark", "ms.PersonInCharge", "ms.ModifiedDate");
	$li->no_col = 9;
	$li->column_array = array(0,0,0,0,0,0,0,0);
	$RemarkField = "if(ms.Remark IS NULL OR ms.Remark='', '--', ms.Remark) as Remark,";
	$ReasonWidth = "20%";
}



/*
$sql =	"
					SELECT
						ms.Year,
						if(INSTR('{$List1}', ms.Semester),'".$SemesterArr[0]."',if(INSTR('{$List2}', ms.Semester),'".$SemesterArr[1]."',if(INSTR('{$List3}', ms.Semester),'".$SemesterArr[2]."',ms.Semester))),
						CASE ms.RecordType
							WHEN 1 THEN '$i_Merit_Merit'
							WHEN 2 THEN '$i_Merit_MinorCredit_unicode'
							WHEN 3 THEN '$i_Merit_MajorCredit_unicode'
							WHEN 4 THEN '$i_Merit_SuperCredit_unicode'
							WHEN 5 THEN '$i_Merit_UltraCredit_unicode'
							WHEN -1 THEN '$i_Merit_BlackMark'
							WHEN -2 THEN '$i_Merit_MinorDemerit'
							WHEN -3 THEN '$i_Merit_MajorDemerit'
							WHEN -4 THEN '$i_Merit_SuperDemerit'
							WHEN -5 THEN '$i_Merit_UltraDemerit'
							ELSE '-'
						END AS MeritType,
						ms.NumberOfUnit,
						ms.Reason,
						{$RemarkField}
						ms.PersonInCharge,
						ms.ModifiedDate
					FROM
						{$eclass_db}.MERIT_STUDENT as ms
					WHERE
						ms.UserID = '$StudentID'
						AND ms.Year = '$year'
						$conds
				";
*/
$sql =	"
					SELECT
						ms.Year,
						".Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN').",
						CASE ms.RecordType
							WHEN 1 THEN '$i_Merit_Merit'
							WHEN 2 THEN '$i_Merit_MinorCredit'
							WHEN 3 THEN '$i_Merit_MajorCredit'
							WHEN 4 THEN '$i_Merit_SuperCredit'
							WHEN 5 THEN '$i_Merit_UltraCredit'
							WHEN -1 THEN '$i_Merit_BlackMark'
							WHEN -2 THEN '$i_Merit_MinorDemerit'
							WHEN -3 THEN '$i_Merit_MajorDemerit'
							WHEN -4 THEN '$i_Merit_SuperDemerit'
							WHEN -5 THEN '$i_Merit_UltraDemerit'
							ELSE '-'
						END AS MeritType,
						TRIM('.' FROM TRIM(TRAILING 0 FROM ROUND(ms.NumberOfUnit, 2))),
						ms.Reason,
						{$RemarkField}
						ms.PersonInCharge,
						ms.ModifiedDate
					FROM
						{$eclass_db}.MERIT_STUDENT as ms
  				LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
  				  ON ms.YearTermID = ayt.YearTermID
  				LEFT JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
  				  ON ms.AcademicYearID = ay.AcademicYearID
					WHERE
						ms.UserID = '$default_student_id'
						AND ms.Year = '$year'
						AND ms.RecordType <> 0
						$conds
				";
				//debug_r($sql);

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $usertype_s;
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
else
$li->page_size = 999;
//$li->noNumber = true;

$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
//$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_alt = array("", "");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";
$li->count_mode = 1;

$li->fieldorder2 = " , ms.MeritDate";
// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['year'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['semester'], 1)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['mtype'], 1)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $ec_iPortfolio['amount'], 1)."</td>\n";
$li->column_list .= "<td width='".$ReasonWidth."'>".$li->column($pos++, $ec_iPortfolio['reason'], 1)."</td>\n";
if($sys_custom['iPortfolioHideRemark']==false) {
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $ec_iPortfolio['remark'] , 1)."</td>\n";
}
$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['pic'], 1)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";

################### Function Start ##########################
function getSummary($my_summary){
	$tmp_arr = explode("SePa", stripslashes($my_summary));
	
	if (sizeof($tmp_arr)%2==0)
	{
		$size_used = sizeof($tmp_arr)/2;
		$rx = "<table cellpadding='8' cellspacing='0' border='0'><tr>\n";
		for ($i=0; $i<$size_used; $i++)
		{
			$rx .= "<td class='tabletext' nowrap>" . $tmp_arr[$i] . " : <b>" . $tmp_arr[$i+$size_used] . "</b>&nbsp;</td>\n";
		}
		$rx .= "</tr></table>\n";
	}

	return $rx;
}
################### Function End #############################

if($flag==1)
{
	$summary_merit = stripslashes($summary_merit);
	$summary_demerit = stripslashes($summary_demerit);
}

$summary_merit_html = getSummary($summary_merit);
$summary_demerit_html = getSummary($summary_demerit);

//////////////////////////////////////////////

# define the page title and table size

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>


<script language="JavaScript" type="text/JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "../school_records_class.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined")
		document.form1.ChooseYear.value = "";
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "sbs/index.php";
	document.form1.submit();
}

$(document).ready(function(){
  $('#search_name').keypress(function(event) {
    if (event.keyCode == '13') {
      event.preventDefault();
      jSUBMIT_SEARCH();
    }
  });
});
</script>

<FORM name="form1" method="POST" action="school_record_teacher_merit.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=Get_Lang_Selection($student_obj['ChineseName'],$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="#" onClick="jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="#" onClick="jTO_SI()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
	if(in_array($default_student_id, $act_student_id_arr)) {
		if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:") && $li_pf->HAS_RIGHT("learning_sharing")) {
?>
															<td valign="top">
																<a href="#" onClick="jTO_LP()">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php
		}
		if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme")) {
?>
			<td valign="top"><a href="javascript:jTO_SBS()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></a></td>
<?php
		}
	}
?>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<table border="0" cellspacing="5" cellpadding="0"  width="100%">
														<tr>
															<td class="tab_underline" nowrap>
																<div class="shadetabs">
																	<ul>
																		<?=$li_pf->HAS_RIGHT("merit") ? ($RecordType=="merit" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_merit']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('merit')\">".$ec_iPortfolio['title_merit']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("assessment_report") ? ($RecordType=="assessment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_academic_report']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('assessment')\">".$ec_iPortfolio['title_academic_report']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("activity") ? ($RecordType=="activity" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_activity']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('activity')\">".$ec_iPortfolio['title_activity']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("award") ? ($RecordType=="award" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_award']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('award')\">".$ec_iPortfolio['title_award']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("teacher_comment") ? ($RecordType=="comment" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_teacher_comments']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('comment')\">".$ec_iPortfolio['title_teacher_comments']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("attendance") ? ($RecordType=="attendance" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['title_attendance']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('attendance')\">".$ec_iPortfolio['title_attendance']."</a></li>") : ""?>
																		<?=$li_pf->HAS_RIGHT("service") ? ($RecordType=="service" ? "<li class='selected'><a href='#'><strong>".$ec_iPortfolio['service']."</strong></a></li>":"<li><a href='#' onClick=\"jCHANGE_RECORD_TYPE('service')\">".$ec_iPortfolio['service']."</a></li>") : ""?>
																	</ul>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table width="100%"  border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td valign="top">
																<table width="100%"  border="0" cellspacing="5" cellpadding="0">
																	<tr>
																		<td>
																			<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"><span class="navigation"><a href="#" onClick="jCHANGE_RECORD_TYPE('merit')"><?=$ec_iPortfolio['overall_summary']?></a>
																			<img src="<?=$PATH_WRT_ROOT?>/images/<?= $LAYOUT_SKIN ?>/iPortfolio/nav_arrow.gif" align="middle" height="15" width="15"></span><span class="tabletext"><?=$ec_iPortfolio['merit_detail']?></span>
																		</td>
																	</tr>
																	<tr>
																		<td><?=$summary_merit_html?></td>
																	</tr>
																	<tr>
																		<td><?=$summary_demerit_html?></td>
																	</tr>
																	<tr>
																		<td align="center">
																			<?= $li->displayPlain() ?>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


<input type="hidden" name="RecordType" value=<?=$RecordType?> />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="year" value="<?=$year?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type="hidden" name="summary" value="<?=$summary?>">
<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
<input type="hidden" name="flag" value=1>
<input type=hidden name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type=hidden name="order" value="<?php echo $li->order; ?>">
<input type=hidden name="field" value="<?php echo $li->field; ?>">
<input type=hidden name="numPerPage" value="<?=$numPerPage?>">
<input type=hidden name="page_size_change" value="<?=$page_size_change?>">
<input type=hidden name="summary_merit" value="<?=$summary_merit?>">
<input type=hidden name="summary_demerit" value="<?=$summary_demerit?>">
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
//closedb();
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
