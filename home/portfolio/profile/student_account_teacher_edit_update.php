<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libportfolio2007();
$lpf_slp = new libpf_slp();
$hasAttachment = false;

$referer = $_POST['referer'];

if(isset($Title) && isset($Details) && isset($RecordID))
{
	
	/*
	//don't handle / allow attach file in student self Account
	if(count($attachment) > 0)
	{
		# Get the attachment path in server
    $target_dir = $li_pf->GET_SELF_ACCOUNT_ATTACHMENT_PATH($RecordID);
		if($target_dir == "")
			$target_dir = session_id()."_sa".$RecordID;
    
    $lf = new libfilesystem();
    
    # Create directory for attachments
    $path = "$file_path/file/portfolio/";
    $lf->folder_new($path);
    $path = "$file_path/file/portfolio/self_account/";
    $lf->folder_new($path);
    $path = "$file_path/file/portfolio/self_account/$target_dir/";
		
		# Upload files to the directory
		for($i=0; $i<count($attachment); $i++)
		{
			if(!empty($attachment[$i]))
			{
				$lf->folder_new($path);
				$hasAttachment = true;
				
				$des = "$path/".stripslashes($attachment_name[$i]);
				$lf->lfs_copy($attachment[$i], $des);
			}
		}
		
		# update database record for the attachment path
		if($hasAttachment || $target_dir != "")
			$li_pf->ADD_SELF_ACCOUNT_ATTACHMENT_PATH($RecordID, $target_dir);
	}
	*/
	$sa_arr['Title'] = $Title;
	$sa_arr['Details'] = $Details;
	$sa_arr['RecordID'] = $RecordID;

	$lpf_slp->EDIT_SELF_ACCOUNT($sa_arr);
}

intranet_closedb();
header("Location: student_info_teacher.php?StudentID=".$StudentID."&YearClassID=".$YearClassID);

?>
