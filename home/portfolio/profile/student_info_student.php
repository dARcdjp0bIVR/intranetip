<?php
// Editing by Carlos
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_sturec();

/*
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $li_pf->getStudentID($ck_user_id);

	# define the navigation
	$template_pages = Array(
					Array($ec_iPortfolio['student_info'], "")
				);
}
else if($ck_memberType=="P")
{
	$template_table_top_right = $li_pf->getChildrenListInfo($StudentID, $ClassName);

	# define the navigation
	$template_pages = Array(
				Array($ec_iPortfolio['student_list'], "../school_records_children.php"),
				Array($ec_iPortfolio['student_info'], "")
				);
}
else
{
	$template_table_top_right = $li_pf->getStudentListInfo($ClassName, $StudentID);

	# define the navigation
	if($ck_is_alumni)
	{
		$template_pages = Array(
					Array($ec_iPortfolio['alumni_list'], "../school_records_alumni.php"),
					Array($ck_alumni_year, "../school_records_alumni_year.php?my_year=$ck_alumni_year"),
					Array($ec_iPortfolio['student_info'], "")
					);
	}
	else
	{
		$template_pages = Array(
					Array($ec_iPortfolio['class_list'], "../school_records.php"),
					Array($ec_iPortfolio['student_list'], "../school_records_class.php?ClassName=$ClassName"),
					Array($ec_iPortfolio['student_info'], "")
					);
	}
}
*/

# Display according to user type
$StudentID = $_SESSION['UserID'];
$luser = new libuser($StudentID);

# Set student photo in left menu
if(is_object($luser))
{
	$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
	$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);
}

$linterface = new interface_html("iportfolio_default.html");
$CurrentPage = "Student_MyInformation";	# Highlighted link in left menu
$CurrentPageName = $MyInfo;		# Title in grey box

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($StudentID);

$ClassSelection = $li_pf->GEN_CLASS_SELECTION_TEACHER($ClassName, "", true, false, false);

# Generate student selection drop-down list and get a student list
list($StudentSelection, $student_list) = $li_pf->GEN_STUDENT_LIST_INFO($ClassName, $StudentID);

# generate student info table
$student_info_display = $li_pf->GEN_STUDENT_INFO_TABLE($StudentID, $student_obj);

# generate class history table
$class_history_display = $li_pf->GEN_CLASS_HISTORY_TABLE($StudentID);

# generate house and admission date table
$house_display = $li_pf->GEN_STUDENT_HOUSE_TABLE($StudentID, $student_obj);

# generate student parent info table
$parent_display = $li_pf->GEN_PARENT_INFO_TABLE($StudentID);

$linterface->LAYOUT_START();

?>


<!-- ===================================== Body Contents ============================= -->

<FORM name="form1" method="POST" action="student_info.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<?php
				if($sys_custom['LivingHomeopathy']){
					echo '<br />';
					echo '<table border="0" cellpadding="5" cellspacing="0" width="100%">';
					echo '<tr><td align="right"><a href="/home/portfolio/profile/personal_profile.php" class="tablebottomlink" target="_BLANK">'.$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'].'</a></td></tr>';
					echo '</table>';
				}
				?>
				<table border="0" cellpadding="5" cellspacing="0" width="100%">
					<tr>
						<td class="sub_page_title"><?=$ec_iPortfolio['basic_info']?></td>
					</tr>
					<tr>
					  <td width="65%" align="center" valign="top" class="stu_info_log stu_info_log_main"><?=$student_info_display?></td>
					  <td valign="top" height="100%" class="stu_info_log stu_info_log_main">
					  	<table border="0" cellpadding="5" cellspacing="0" width="100%">
					  		<tr>
					  			<td align="center" valign="top"><?=$class_history_display?></td>
								</tr>
					  		<tr>
					  			<td align="center" valign="top"><?=$house_display?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<?php if(!$_SESSION['ncs_role']){ ?>
				<br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="sub_page_title"><?=$ec_iPortfolio['guardian_info']?></td>
					</tr>
					<tr>
	  				<td class="stu_info_log">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><?=$parent_display?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<?php } ?>
			</td>
		</tr>
	</table>

	<input type="hidden" name="ClassName" value="<?=$ClassName?>" />
	<input type="hidden" name="StudentID" value="<?=$StudentID?>\" />
	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
</FORM>

<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
