<?php
// Modifing by: Connie
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();
//$lpf->ACCESS_CONTROL("ole");

$lexport = new libexporttext();

# Being Henry Yuen (2010-05-31): to avoid ruuning out of memory, an max number of 10000 records are print in each loop
/*
$ExportContent = $lpf->returnOLRExportContent($Year, $IntExt, $_POST);

// Output the file to user browser
//$year_name = "_".$Year;
$year_name = "_".getAYNameByAyId($Year);
$filename = "olr".$year_name.".csv";
//$filename = "olr".$year_name.".xls";
$isXLS = false;
intranet_closedb();
$lexport->EXPORT_FILE($filename, $ExportContent, $isXLS);
*/

$recordsPerLoop = 10000;
$startIndex = 0;

# print the report title				
$year_name = "_".getAYNameByAyId($Year);
$filename = "olr".$year_name.".csv";
$isXLS = false;		

$ExportContent = $lpf->returnOLRExportHeader($_POST);
$DataArray = $lpf->returnStudentOLRExportData($Year, $IntExt, $startIndex, $recordsPerLoop);
$lexport->EXPORT_FILE($filename, $ExportContent, $isXLS, $ignoreLength=true);	

		
# print content piece by piece
while(true){		 
	$DataArray = $lpf->returnStudentOLRExportData($Year, $IntExt, $startIndex, $recordsPerLoop);
	if(count($DataArray) != 0 or $startIndex == 0){	
		$ExportContent = $lpf->returnOLRExportData($Year, $IntExt, $_POST, $DataArray);
		$ExportContent = mb_convert_encoding($ExportContent,'UTF-16LE','UTF-8');
		print($ExportContent);
	}	
	$startIndex += $recordsPerLoop;
	
	if(count($DataArray) != $recordsPerLoop){
		break;
	}
}
	
intranet_closedb();

# End Henry Yuen (2010-05-31): to avoid ruuning out of memory, an max number of 10000 records are print in each loop
?>
