<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$objDB = new libdb();
$studentNameSql = ($r_alumni_studentName == '')?'':' and au.EnglishName like \'%'.$r_alumni_studentName.'%\' or au.ChineseName like \'%'.$r_alumni_studentName.'%\'';
$studentWebsamsSql = ($r_alumni_studentWebsams == '')?'':' and au.WebSAMSRegNo like \'%'.$r_alumni_studentWebsams.'%\'';

$sql .= ' select au.userid as `au_UserID`, au.EnglishName as `EnglishName`, au.ChineseName as `ChineseName`, au.UserLogin as `UserLogin` , ps.userid as `ps_UserID` , au.WebSAMSRegNo as `WebSAMSRegNo` , ';
$sql .= ' CONCAT(\'<input type="checkbox" name="alumni_StudentID[]" id="alumni_studentArr_\',au.userid,\'" value=\', au.userid ,\'>\') as `checkbox` ';
$sql .= ' from '.$intranet_db.'.INTRANET_ARCHIVE_USER as au inner join '.$eclass_db.'.PORTFOLIO_STUDENT ps on au.UserID = ps.UserID';
$sql .= ' where 1 ';
$sql .= $studentNameSql;
$sql .= $studentWebsamsSql;
$rs = $objDB->returnResultSet($sql);

$h_table = '<table class="common_table_list_v30" style="width:80%;" align="left">';
$h_table .= '<thead>';
$h_table .= '<tr class="tabletop">';
$h_table .= '<th>#</th>';
$h_table .= '<th>'.$Lang['StudentRegistry']['EnglishName'].'</th>';
$h_table .= '<th>'.$Lang['StudentRegistry']['ChineseName'].'</th>';
$h_table .= '<th>'.$ec_iPortfolio['WebSAMSRegNo'].'</th>';
$h_table .= '<th>'.$i_UserLogin.'</th>';
$h_table .= '<th><input type ="checkbox" name="checkAllAlumni" value="1" onclick="checkAllAlumniCheckBox(this.checked)">&nbsp;</th>';
$h_table .= '</tr>';
$h_table .= '</thead>';

$noOfRs = count($rs);

$h_class = 'class="tabletext tablerowtablelink"';
if($noOfRs == 0){
	$h_table .= '<tr><td '.$h_class.' colspan="100%" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
}else{
	for($i = 0;$i<$noOfRs; $i++){
		$_userid = $rs[$i]['au_UserID'];
		$_EnglishName = trim($rs[$i]['EnglishName']) == ''? '--' :$rs[$i]['EnglishName'];

		$_ChineseName = trim($rs[$i]['ChineseName']) == ''? '--' :$rs[$i]['ChineseName'];


		$_WebSAMSRegNo = trim($rs[$i]['WebSAMSRegNo']) == ''? '--' :$rs[$i]['WebSAMSRegNo'];

		$_UserLogin = trim($rs[$i]['UserLogin']) == ''? '--' :$rs[$i]['UserLogin'];



		$_checkbox = $rs[$i]['checkbox'];
				
		$h_table .= '<tr>';
		$h_table .= '<td '.$h_class.'>'.intval($i +1).'</td>';
		$h_table .= '<td '.$h_class.'>'.$_EnglishName.'</td>';
		$h_table .= '<td '.$h_class.'>'.$_ChineseName.'</td>';
		$h_table .= '<td '.$h_class.'>'.$_WebSAMSRegNo.'</td>';
		$h_table .= '<td '.$h_class.'>'.$_UserLogin.'</td>';
		$h_table .= '<td '.$h_class.' width="40">'.$_checkbox.'</td>';
		$h_table .= '</tr>';

	}
}

$h_table .= '</table>';
intranet_closedb();

echo $h_table;
?>
