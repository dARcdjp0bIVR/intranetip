<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$lpf_fm = new libpf_formclass();
$linterface = new interface_html();

$lpf_fm->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$student_id_arr = $lpf_fm->GET_STUDENT_LIST();
$student_detail_arr = $lpf_fm->GET_STUDENT_DETAIL_LIST($student_id_arr);

$student_arr = array();
for($i=0; $i<count($student_detail_arr); $i++)
{
  $t_user_id = $student_detail_arr[$i]['UserID'];
  $t_user_name = "(".$student_detail_arr[$i]["ClassName"]." - ".$student_detail_arr[$i]["ClassNumber"].") ";
  $t_user_name .= Get_Lang_Selection($student_detail_arr[$i]["ChineseName"], $student_detail_arr[$i]["EnglishName"]);

  $student_arr[] = array($t_user_id, $t_user_name);
}

$StudentType = $_REQUEST['StudentType'];

if($StudentType=='single')
{
	$student_list_html = getSelectByArray($student_arr, "id=\"StudentIDSel\" name=\"StudentID\"", "", 1, 0, "", 2);
}
else if($StudentType=='multiple')
{
	# Select All Student Btn
	
	
	$SelectAllStudentBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDSel', 1);");
	$student_list_content_html = getSelectByArray($student_arr, "id=\"StudentIDSel\" name=\"StudentID[]\" style=\"height:200px;\" multiple ", "", 0, 1, "", 1);
	
	$student_list_html ='';
	
	$student_list_html ='<table width="100%">

							<tr>
								<td>'.$student_list_content_html.$SelectAllStudentBtn.'</td>
		          			</tr>
							<tr>
								<td bgcolor="#FFFFFF" >
		          					<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].' </span>
		          				</td>
		          			</tr>
						</table>';
}




echo $student_list_html;

?>