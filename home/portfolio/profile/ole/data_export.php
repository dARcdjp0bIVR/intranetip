<?php
// Modifing by Villa
##########################################################
## Modification Log
## 2017-07-14 Villa
## - Add Program Creator 
##
## 2016-10-06 Villa[M98353]
## - Add checkbox category_b5, subcategory_b5, category_en, subcategory_en
## - Modified  checkAll(), 
## 2010-06-30 Max (201006301518)
## - Add link to alumni export data
##
## 2010-04-07 Max (201004071139)
## - Change year selection box
## - Fine tune export data

## 2010-04-01 Henry Y (2010 04 01)
## allow users to choose displayed columns
##########################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO();
//$lpf->ACCESS_CONTROL("ole");

//$YearArray = $lpf->returnOLRYear();
$Year = ($Year=="") ? Get_Current_Academic_Year_ID() : $Year;
//$year_selection_html = getSelectByArrayTitle($YearArray, "name='Year' onChange='this.form.year_change.value=1;this.form.action=\"data_export.php\";this.form.submit();'", "", $Year, true);
# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$year_selection_html = getSelectByArray($academic_year_arr, "name='Year' id='Year' onChange='this.form.year_change.value=1;checkformSubmit()'", $Year, $Year, true);
# use 0/'' for all records
# use 1 for int records
# use 2 for ext records
$intextArray[] = array(1, $iPort["internal_record"]);
$intextArray[] = array(2, $iPort["external_record"]);
$intext_selection_html = getSelectByArray($intextArray, "name='IntExt' onChange='this.form.action=\"data_export.php\";this.form.submit();'", $IntExt, 1, 0, "", 2);
$record_num = $lpf->countOLEDataNumber($Year, $IntExt);

$CurrentPage = "ole";
$title = $ec_iPortfolio['ole'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<!-- <script language=""> -->
<script type="text/javascript">
window.resizeTo(800, 700)
window.moveTo(0,0);

// check if year and semester are both available
function checkform(frmObj){
	if (typeof(frmObj.Year)=="undefined")
	{
		alert("<?=$ec_warning['eclass_data_no_record']?>");
		return false;
	}
	
	//define the Year Data type according to report type
	if($('input[name="exportFormat"]:checked').val()=='dgs'){
		$('#Year').attr('name','Year[]');
	}else{
		$('#Year').attr('name','Year');
	}
	return true;
}

function checkAll(isChecked){	
	value = isChecked? true: false;
	
	document.form1.WebSAMSRegNo.checked = value;	
	document.form1.Programme.checked = value;
	document.form1.ProgrammeDescription.checked = value;
	document.form1.SchoolYearFrom.checked = value;
	document.form1.SchoolYearTo.checked = value;
	document.form1.ShowSemester.checked = value;
	document.form1.Role.checked = value;
	document.form1.Organization.checked = value;
	document.form1.Awards.checked = value;
	document.form1.Student.checked = value;
	document.form1.ClassName.checked = value;
	document.form1.ClassNumber.checked = value;
	document.form1.Period.checked = value;
	//document.form1.Category.checked = value;
	document.form1.Hours.checked = value;				
	document.form1.ApprovedBy.checked = value;
	document.form1.PreferredApprover.checked = value;
	document.form1.Status.checked = value;
	document.form1.ApprovedDate.checked = value;
	document.form1.Remarks.checked = value;
	document.form1.ELE.checked = value;
	document.form1.Comment.checked = value;
	document.form1.intExtType.checked = value;
	document.form1.SLPOrder.checked = value;
	//document.form1.SubCategory.checked = value;
	//2016-10-06	Villa[M98353]
	document.form1.Category_EN.checked = value;
	document.form1.SubCategory_EN.checked = value;
	document.form1.Category_B5.checked = value;
	document.form1.SubCategory_B5.checked = value;
	<? if ($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
		document.form1.IsSAS.checked = value;
	<? } ?>
	document.form1.ProgramCreator.checked = value;
}

function checkItem(isChecked){
	if(!isChecked){
		document.form1.CheckAll.checked = false;
	}	
}

function dgsReport(isSelected){
	if(isSelected){
		$('#recordTypeTR').css('display','none');
		$('#numofRecordTR').css('display','none');
		$('#studentTypeChoiceTR').css('display','');
		$('#studentTypeChoiceTR td:first-child').html('');
		$('#Year').attr("multiple","multiple");
		$('#classRow').css('display','');
		$('#studentRow').css('display','');
		js_Class_Selection('multiple');
	}else{
		$('#recordTypeTR').css('display','');
		$('#numofRecordTR').css('display','');
		$('#studentTypeChoiceTR').css('display','none');
		$('#Year').attr("multiple","");
		$('#classRow').css('display','none');
		$('#studentRow').css('display','none');
	}
}

function dgsStudentType(currentStudent){
	if(currentStudent){
		$('#classRow').css('display','');
		$('#studentRow').css('display','');
		$('#studentTypeChoiceTR td:first-child').html('');
	}else{
		$('#classRow').css('display','none');
		$('#studentRow').css('display','none');
		$('#studentTypeChoiceTR td:first-child').html('<?=$ec_iPortfolio['DGS_Custom']['Student']?>：');
	}
}

$(document).ready(function(){
	if($('input[name=exportFormat]').filter(':checked').val()=="dgs"){
		$('#numofRecordTR').css('display','none');
		$('#recordTypeTR').css('display','none');
		$('#studentTypeChoiceTR').css('display','');
	}else{
		$('#numofRecordTR').css('display','');
		$('#recordTypeTR').css('display','');
		$('#studentTypeChoiceTR').css('display','none');
	}
})

function checkformSubmit(){
	if($('input[name="exportFormat"]:checked').val()=='dgs'){
	}else{
		
		form1.action='data_export.php'
		$('#form1').submit();
	}
}
//Copy from template/report_printing_menu.tmpl.php
function js_Class_Selection(ParStudentType)
{	
	$('td#classRowTD').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Class_Selection",
			StudentType: ParStudentType
		},
		function(ReturnData) {
			$('td#classRowTD').html(ReturnData);
		}
	);
}

function jCHANGE_CLASS(jParStudentType)
{
	if($("#YearClass").val() != "")
	{
  	$.ajax({
  		type: "GET",
  		url: "ajax_get_class_student.php",
  		data: "YearClassID="+$("#YearClass").val()+"&StudentType="+jParStudentType,
  		beforeSend: function () {
		    $("#studentRow").find("td").eq(1).html('<img src="/images/2009a/indicator.gif" />');
      },
  		success: function (msg) {
        if(msg != "")
        {
          $("#studentRow").find("td").eq(1).html(msg);
          
          if(jParStudentType=='multiple')
          {
          	 js_Select_All('StudentIDSel', 1);
          }
         
        }
  		}
  	});
	
		$("#studentRow").show();
	}
	else
	{
    $("#studentRow").hide();
  }
}

function clickedReset() {
	$('input#CheckAll').attr('disabled', '');
	$('input.ipf_export_field').attr('disabled', '');
}
/////////
</script>

<?php $disabled = ($semester_selection_html=="<i>".$no_record_msg."</i>") ? "DISABLED": ""; ?>

<FORM action="data_export_update.php" method="POST" name="form1" id="form1" onSubmit="return checkform(this)">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
      		<td colspan="2"><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['reportExport_instruction'], "")?>&nbsp;</td>
        </tr>
        <tr>
      		<td align="right" class="tabletext" style="width:160px"><?=$ec_iPortfolio['year']?>：</td><td class="tabletext"><?= $year_selection_html ?><span style="float: right"><a href="data_export_alumni.php"><?=$i_identity_alumni?></a></span></td>
        </tr>
        <tr id="classRow" style="display:none">
      		<td align="right" class="tabletext" style="width:160px"><?=$ec_iPortfolio['class']?>：</td><td id="classRowTD" class="tabletext"></td>
        </tr>
        <tr id="studentRow" style="display:none">
      		<td align="right" class="tabletext" style="width:160px"><?=$i_identity_student?>：</td><td class="tabletext"><?=$ec_warning['select_class']?></td>
        </tr>	       
	 	<tr id="studentTypeChoiceTR" style="display:none">
			<td align="right" class="tabletext" style="padding-left:4px"><?=$ec_iPortfolio['DGS_Custom']['Student']?>：</td>
			<td class="tabletext">
      		<label><input onclick="dgsStudentType(1)" type="radio" name="studentType" value="1" checked><?=$ec_iPortfolio['DGS_Custom']['CurrentStudent']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<label><input onclick="dgsStudentType(0)" type="radio" name="studentType" value="0"><?=$ec_iPortfolio['DGS_Custom']['AllStudent']?></label>
      		</td>
        </tr>  
		<tr id="recordTypeTR" style="display:none">
			<td align="right" class="tabletext"><?=$ec_iPortfolio['record_type']?>：</td><td class="tabletext"><?= $intext_selection_html?></td>
		</tr>		       
	 	<tr id="numofRecordTR" style="display:none">
			<td align="right" class="tabletext" style="padding-left:4px"><?=$ec_iPortfolio['number_of_record']?>：</td>
			<td class="tabletext"><?= $record_num?></td>
        </tr>
        <?php	if($sys_custom['iPf']['Report']['OLE']['WebSAMSReport']||$sys_custom['iPf']['Report']['OLE']['DGS_Custom_Report']) { ?>	       
	 	<tr>
      		<td align="right" class="tabletext"><?=$ec_iPortfolio['Export_Format']?>：</td>
      		<td class="tabletext">
      		<label><input type="radio" name="exportFormat" onclick="dgsReport(0);$('input[name=\'CheckAll\']').removeAttr('disabled');$('.ipf_export_field').removeAttr('disabled'); form1.action='data_export_update.php';" checked><?=$ec_iPortfolio['Export_Custom_Field']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php if($sys_custom['iPf']['Report']['OLE']['WebSAMSReport']) { ?>
      		<label><input type="radio" name="exportFormat" onclick="dgsReport(0);$('input[name=\'CheckAll\']').attr('disabled','true');$('.ipf_export_field').attr('disabled','true'); form1.action='data_export_websams_update.php';"><?=$ec_iPortfolio['Export_WebSAMS_Format']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php } ?>
      		<?php if($sys_custom['iPf']['Report']['OLE']['DGS_Custom_Report']) { ?>
      		<label><input value="dgs" type="radio" name="exportFormat" onclick="dgsReport(1);$('input[name=\'CheckAll\']').attr('disabled','true');$('.ipf_export_field').attr('disabled','true'); form1.action='data_export_dgs_update.php';"><?=$ec_iPortfolio['Export_Custom_Format']?></label>
      		&nbsp;&nbsp;&nbsp;&nbsp;
      		<?php } ?>
      		</td>
        </tr> 	
        <?php } ?>	
        <tr>
        	<td align="right" class="tabletext" valign="top"><?=$ec_iPortfolio['export']['SelectFields']?>：</td>
        	<td class="tabletext">
        		<input type="checkbox" id="CheckAll" name="CheckAll" onclick="checkAll(this.checked)"/><label for="CheckAll"><?=$ec_iPortfolio['export']['CheckAll']?></label> <br>
        		<hr>
        		<input type="checkbox" class="ipf_export_field" id="WebSAMSRegNo" name="WebSAMSRegNo" checked="true" onclick="checkItem(this.checked)"/><label for="WebSAMSRegNo"><?=$ec_iPortfolio['export']['WebSAMSRegNo']?></label> <br>      	        	
				<input type="checkbox" class="ipf_export_field" id="Programme" name="Programme" checked="true" onclick="checkItem(this.checked)"/><label for="Programme"><?=$ec_iPortfolio['export']['Programme']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ProgrammeDescription" name="ProgrammeDescription" checked="true" onclick="checkItem(this.checked)"/><label for="ProgrammeDescription"><?=$ec_iPortfolio['export']['ProgrammeDescription']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SchoolYearFrom" name="SchoolYearFrom" checked="true" onclick="checkItem(this.checked)"/><label for="SchoolYearFrom"><?=$ec_iPortfolio['export']['SchoolYearFrom']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SchoolYearTo" name="SchoolYearTo" checked="true" onclick="checkItem(this.checked)"/><label for="SchoolYearTo"><?=$ec_iPortfolio['export']['SchoolYearTo']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ShowSemester" name="ShowSemester" onclick="checkItem(this.checked)"/><label for="ShowSemester"><?=$ec_iPortfolio['export']['ShowSemester']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Role" name="Role" checked="true" onclick="checkItem(this.checked)"/><label for="Role"><?=$ec_iPortfolio['export']['Role']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Organization" name="Organization" checked="true" onclick="checkItem(this.checked)"/><label for="Organization"><?=$ec_iPortfolio['export']['Organization']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Awards" name="Awards" checked="true" onclick="checkItem(this.checked)"/><label for="Awards"><?=$ec_iPortfolio['export']['Awards']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Student" name="Student" onclick="checkItem(this.checked)"/><label for="Student"><?=$ec_iPortfolio['export']['Student']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ClassName" name="ClassName" onclick="checkItem(this.checked)"/><label for="ClassName"><?=$ec_iPortfolio['export']['ClassName']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ClassNumber" name="ClassNumber" onclick="checkItem(this.checked)"/><label for="ClassNumber"><?=$ec_iPortfolio['export']['ClassNumber']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Period" name="Period" onclick="checkItem(this.checked)"/><label for="Period"><?=$ec_iPortfolio['export']['Period']?></label><br>

<!-- 2016-10-06 villa[M98353] -->
				<input type="checkbox" class="ipf_export_field" id="Category_B5" name="Category_B5" onclick="checkItem(this.checked)"/><label for="Category_B5"><?=$ec_iPortfolio['export']['Category'].'('.$ec_iPortfolio['chinese'].')'?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SubCategory_B5" name="SubCategory_B5" onclick="checkItem(this.checked)"/><label for="SubCategory_B5"><?=$ec_iPortfolio['sub_category'].'('.$ec_iPortfolio['chinese'].')'?></label><br/>	
				<input type="checkbox" class="ipf_export_field" id="Category_EN" name="Category_EN" onclick="checkItem(this.checked)"/><label for="Category_EN"><?=$ec_iPortfolio['export']['Category'].'('.$ec_iPortfolio['english'].')'?></label><br>
				<input type="checkbox" class="ipf_export_field" id="SubCategory_EN" name="SubCategory_EN" onclick="checkItem(this.checked)"/><label for="SubCategory_EN"><?=$ec_iPortfolio['sub_category'].'('.$ec_iPortfolio['english'].')'?></label><br/>	
				
				<input type="checkbox" class="ipf_export_field" id="Hours" name="Hours" onclick="checkItem(this.checked)"/><label for="Hours"><?=$ec_iPortfolio['export']['Hours']?></label><br>				
				<input type="checkbox" class="ipf_export_field" id="ApprovedBy" name="ApprovedBy" onclick="checkItem(this.checked)"/><label for="ApprovedBy"><?=$Lang['iPortfolio']['auditby']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="PreferredApprover" name="PreferredApprover" onclick="checkItem(this.checked)"/><label for="PreferredApprover"><?=$Lang['iPortfolio']['preferred_approver']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Status" name="Status" onclick="checkItem(this.checked)"/><label for="Status"><?=$ec_iPortfolio['export']['Status']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ApprovedDate" name="ApprovedDate" onclick="checkItem(this.checked)"/><label for="ApprovedDate"><?=$Lang['iPortfolio']['auditdate']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Remarks" name="Remarks" onclick="checkItem(this.checked)"/><label for="Remarks"><?=$ec_iPortfolio['export']['Remarks']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="ELE" name="ELE" onclick="checkItem(this.checked)"/><label for="ELE"><?=$ec_iPortfolio['export']['ELE']?></label><br>
				<input type="checkbox" class="ipf_export_field" id="Comment" name="Comment" onclick="checkItem(this.checked)"/><label for="Comment"><?=$ec_iPortfolio['export']['Comment']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="intExtType" name="intExtType" onclick="checkItem(this.checked)"/><label for="intExtType"><?=$ec_iPortfolio['record_type']?></label><br/>
				<input type="checkbox" class="ipf_export_field" id="SLPOrder" name="SLPOrder" onclick="checkItem(this.checked)"/><label for="SLPOrder"><?=$ec_iPortfolio['SLPOrder']?></label><br/>
				<? if ($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
					<input type="checkbox" class="ipf_export_field" id="IsSAS" name="IsSAS" onclick="checkItem(this.checked)"/><label for="IsSAS"><?=$Lang['iPortfolio']['OLE']['SAS']?></label><br>
				<? } ?>
				<input type="checkbox" class="ipf_export_field" id="ProgramCreator" name="ProgramCreator" onclick="checkItem(this.checked)"/><label for="ProgramCreator"><?=$Lang['iPortfolio']['OLE']['ProgramCreator']?></label><br>
        	</td>
        </tr>
      </table>

	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_export?>" name="btn_export">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>" onclick="clickedReset();">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		  </td>
	</tr>
	</table>
	<input type="hidden" name="year_change" />
	<input type="hidden" name="sem_change" />
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
