<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

//$lpf_ui = new libportfolio2007();
$li_pf = new libpf_slp();
$li_pf->ACCESS_CONTROL("ole");
$thisUserID = $_SESSION['UserID'];

$StartYearStr = $li_pf->GEN_OLE_YEAR_SELECTION("", "name='StartYear'", false);
$EndYearStr = $li_pf->GEN_OLE_YEAR_SELECTION("", "name='EndYear'", false);
$ELEStr = $li_pf->GEN_OLE_ELE_CHECKLIST();
$ClassLevelStr = $li_pf->GEN_OLE_CLASSLEVEL_SELECTION("", "", "name='ClassLevel'");

$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];

$luser = new libuser($thisUserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$TabMenuArr = array();
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['statistic'], 0);
if($special_feature['ole_chart'])
{
	$TabMenuArr[] = array("demo/index_stat.php", $ec_iPortfolio['chart_report'], 0);
	$TabMenuArr[] = array("demo/cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
}
$TabMenuArr[] = array("index_analysis.php", $ec_iPortfolio['adv_analyze'], 1);

$linterface->LAYOUT_START();
?>

<script language="JavaScript">

function checkform(obj){

	if(obj.EndYear.value < obj.StartYear.value)
	{
		alert("<?=$ec_warning['start_end_year']?>");
		return false;
	}

	var AtLeastOneChecked = false;
	for(i=0; i<obj.length; i++)
	{
		if(obj.elements[i].name == "ELE[]" && obj.elements[i].checked)
		{
			AtLeastOneChecked = true;
			break;
		}
	}
	if(!AtLeastOneChecked)
	{
		alert("<?=$msg_check_at_least_one?>");
		return false;
	}
	
	if(!check_positive_int(obj.Hour1, "<?=$ec_warning['please_enter_pos_integer']?>", "", 0))
		return false;

	if(obj.hour_range_type.value == "0")
	{
		if(!check_positive_int(obj.Hour2, "<?=$ec_warning['please_enter_pos_integer']?>", "", 0))
			return false;
	
		if(parseInt(obj.Hour2.value) < parseInt(obj.Hour1.value))
		{
			alert("<?=$ec_warning['start_end_hours']?>");
			obj.Hour2.focus();
			return false;
		}
	}

	return true;
}

function jDISPLAY_HOUR2()
{
	var HourRangeType = parseInt(document.getElementsByName("hour_range_type")[0].value);

	if(HourRangeType != 0)
		document.getElementById("Hour2Cell").style.display = "none";
	else
		document.getElementById("Hour2Cell").style.display = "block";
}

function jRESET_FORM()
{
  document.form1.reset();
  jDISPLAY_HOUR2();
}

</script>

<FORM method='POST' name='form1' action='index_analysis_result.php' onSubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		  <?=$li_pf->GET_TAB_MENU($TabMenuArr)?>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td width="80%" valign="top">
									<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
											<td width="80%" valign="top">
												<?=$ClassLevelStr?>
											</td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_year']?></span></td>
											<td>
												<?=$StartYearStr?> <?=$profiles_to?> <?=$EndYearStr?>
											</td>
										</tr>
										<tr valign="top">
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['ele']?></span></td>
											<td>
												<?=$ELEStr?>
											</td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['hours_range']?></span></td>
											<td>
												<table border="0" cellspacing="0" cellpadding="3">
													<tr>
														<td><input name="Hour1" type="text" class="tabletext" style="width:35px" value="0"/></td>
														<td align="center">
															<select name="hour_range_type" class="formtextbox" onChange="jDISPLAY_HOUR2()">
																<option value="0"><?=$ec_iPortfolio['hours_to']?></option>
																<option value="1"><?=$ec_iPortfolio['hours_above']?></option>
																<option value="-1"><?=$ec_iPortfolio['hours_below']?></option>
															</select>
														</td>
														<td align="center" id="Hour2Cell"><input name="Hour2" type="text" class="tabletext" style="width:35px" value="0"/></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$ec_iPortfolio['view_analysis']?>" />
									<input type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onClick="jRESET_FORM()" value="<?=$button_reset?>" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
