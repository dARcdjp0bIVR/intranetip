<?php

// Modifing by 
/*
 * Change Log:
 * 2017/4/13 Villa Fix php5.4
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();

$li_pf = new libpf_slp();
$li_pf->ACCESS_CONTROL("ole");
$thisUserID = $_SESSION['UserID'];

# Resetting page number
if(!isset($Page)) $Page = 1;
switch($FieldChanged)
{
	case "division":
		$Page = 1;
		break;
	case "page":
	default:
		break;
}

if($Field == "")
{
	$Field = 2;
	$Order = "1|1";
}

list($olr_display, $olr_list) = $li_pf->GEN_OLE_STATISTIC_TABLE(array($PageDivision, $Page), $StartYear, $EndYear, $ClassLevel, $Class, $ELE, array($hour_range_type, $Hour1, $Hour2), "", $Field, $Order);

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLEReport";
$CurrentPageName = $iPort['menu']['ole_report'];

$luser = new libuser($thisUserID);

$TabMenuArr = array();
$TabMenuArr[] = array("index_stat.php", $ec_iPortfolio['statistic'], 0);
if($special_feature['ole_chart'])
{
	$TabMenuArr[] = array("demo/index_stat.php", $ec_iPortfolio['chart_report'], 0);
	$TabMenuArr[] = array("demo/cat_stat_chart.php", $ec_iPortfolio['cat_stat_chart'], 0);
}
$TabMenuArr[] = array("index_analysis.php", $ec_iPortfolio['adv_analyze'], 1);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField){
	document.form1.FieldChanged.value = jParField;
	document.form1.action = "index_analysis_result.php";
	document.form1.submit();
}

// Change pages
function jCHANGE_PAGE(jParShift){
	var PageSelection = document.getElementsByName("Page");
	var OriginalIndex = PageSelection[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(TargetIndex >= PageSelection[0].length)
	{
		PageSelection[0].selectedIndex = PageSelection[0].length-1;
		return;
	}
	else if(TargetIndex < 0)
	{
		PageSelection[0].selectedIndex = 0;
		return;
	}
	else
		PageSelection[0].selectedIndex = TargetIndex;

	document.form1.FieldChanged.value = "page";
	document.form1.action = "index_analysis_result.php";
	document.form1.submit();	
}

// Sort table by field
function jSORT_TABLE(jParField, jParOrder)
{
	document.form1.Field.value = jParField;
	document.form1.Order.value = jParOrder;
	
	document.form1.action = "index_analysis_result.php";
	document.form1.submit();	
}
</SCRIPT>

<FORM method='POST' name='form1' action='index_stat.php'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<?=$li_pf->GET_TAB_MENU($TabMenuArr)?>
			
			<table width="96%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><span class="navigation"><a href="index_analysis.php"><?=$ec_iPortfolio['adv_analyze']?></a> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"></span><?=$ec_iPortfolio['analysis_report']?> ( <?=array_shift(explode("-", $StartYear))?>-<?=array_pop(explode("-", $EndYear))?> <?=($ClassLevel=="")?$ec_iPortfolio['all_classlevel']:$ClassLevel?>)
					</td>
				</tr>
			</table>
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
							<?=$olr_display?>
					</td>
				</tr>
				<tr class="tablebottom">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="tabletext"><?=$ec_iPortfolio['record']?> <?=$li_pf->GEN_PAGE_ROW_NUMBER(count($olr_list), $PageDivision, $Page)?>, <?=str_replace("<!--NoRecord-->", count($olr_list), $ec_iPortfolio['total_record'])?></td>
								<td align="right">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table border="0" cellspacing="0" cellpadding="2">
													<tr align="center" valign="middle">
														<td><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('prevp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(-1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp22" width="11" height="10" border="0" align="absmiddle" id="prevp22"></a> <span class="tabletext"> <?=$list_page?> </span></td>
														<td class="tabletext">
															<?=$li_pf->GEN_PAGE_SELECTION(count($olr_list), $PageDivision, $Page, "name='Page' class='formtextbox' onChange='jCHANGE_FIELD(\"page\")'")?>
														</td>
														<td><span class="tabletext"> </span><a href="#" class="tablebottomlink" onMouseOver="MM_swapImage('nextp22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()" onClick="jCHANGE_PAGE(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp22" width="11" height="10" border="0" align="absmiddle" id="nextp22"></a></td>
													</tr>
												</table>
											</td>
											<td>&nbsp;<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
											<td>
												<table border="0" cellpadding="2" cellspacing="0" class="tabletext">
													<tr>
														<td><?=$i_general_EachDisplay?></td>
														<td>
															<?=$li_pf->GEN_PAGE_DIVISION_SELECTION($PageDivision, "name='PageDivision' class='formtextbox' onChange='jCHANGE_FIELD(\"division\")'", 10)?>
														</td>
														<td><?=$i_general_PerPage?></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="StartYear" value="<?=$StartYear?>" />
<input type="hidden" name="EndYear" value="<?=$EndYear?>" />
<input type="hidden" name="ClassLevel" value="<?=$ClassLevel?>" />
<?php
	if(is_array($ELE))
	{
		foreach($ELE as $ELE_element)
		{
			echo "<input type=\"hidden\" name=\"ELE[]\" value=\"".$ELE_element."\" />\n";
		}
	}
?>
<input type="hidden" name="hour_range_type" value="<?=$hour_range_type?>" />
<input type="hidden" name="Hour1" value="<?=$Hour1?>" />
<input type="hidden" name="Hour2" value="<?=$Hour2?>" />
<input type="hidden" name="FieldChanged" />
<input type="hidden" name="Field" value="<?=$Field?>" />
<input type="hidden" name="Order" value="<?=$Order?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
