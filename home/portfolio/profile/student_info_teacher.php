<?php
/**
 * [Modification Log] Modifying By: 
 * ************************************************
 *  Date:	2019-03-19 (Bill) [2017-1207-0959-51277]
 * 			-	show "School Record" icon   ($sys_custom['iPf']['DBSTranscript_SkipActivateChecking'])
 *  Date:	2017-09-26 (Anna) #J124287
 * 			-	added tooltips for icons
 *  Date:	2017-04-12 (Villa) DM#3187
 *  		-	fix cannot change class in select box
 *  Date:	2017-03-27 (Villa) #X115124
 *  		-	modified $class_selection_html: fix all class bug after hiding option <-select->  
 *	Date:   2016-04-15 [Omas]
 *		 - fix class selection access right problem -#U94994
 * ************************************************
 */ 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

iportfolio_auth("T");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_opendb();

$li_pf = new libpf_sturec();
$lpf_slp = new libpf_slp();
$lpf_ui = new libportfolio_ui();

$li_pf->ACCESS_CONTROL("student_info");

$StudentYearClassID = integerSafe(standardizeFormPostValue($_POST['StudentYearClassID']));
if (!empty($StudentYearClassID)) {
	$YearClassID = $StudentYearClassID;
}
$SelectedStudentID = integerSafe(standardizeFormPostValue($_POST['SelectedStudentID']));
if (!empty($SelectedStudentID)) {
	$StudentID = $SelectedStudentID;
}

// access right checking for class selection box
if(!strstr($ck_user_rights, ":manage:"))
{
	# Class teacher view class
	if(strstr($ck_user_rights_ext, "student_info:form_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
	}
	# Subject teacher view class
	else if(strstr($ck_user_rights_ext, "student_info:form_subject_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = array_merge($lpf_acc->GET_CLASS_TEACHER_CLASS(), $lpf_acc->GET_SUBJECT_TEACHER_CLASS());
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
	}
}

// Get class selection with optgroup
$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
for($i=0; $i<count($t_classlevel_arr); $i++)
{
  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];
  
  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
  for($j=0; $j<count($t_class_arr); $j++)
  {
    $t_yc_id = $t_class_arr[$j][0];
    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
    {
      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
    }
    
    if($t_yc_id == $YearClassID)
    {
      $class_name = $t_yc_title;
    }
  }
}

//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$lpf_ui = new libportfolio_ui();
if($_SESSION['SSV_USER_ACCESS']['other-iPortfolio']||$YearClassID){ #X115124
	$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, false);
}elseif($YearClassID==''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$sql = "Select
				ycu.YearClassID
			from
				YEAR_CLASS_USER ycu
			INNER JOIN
				YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$AcademicYearID'
			WHERE
				ycu.UserID = '$StudentID'";
	$Student_Year_ClassID = $li_pf->returnResultSet($sql);
	$YearClassID = $Student_Year_ClassID[0]['YearClassID'];
	$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, true);
}

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearID", "");
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
  for($i=0; $i<count($t_student_detail_arr); $i++)
  {
    $t_classname = $t_student_detail_arr[$i]['ClassName'];
    if(in_array($t_classname, $class_arr)) continue;
  
    $t_user_id = $t_student_detail_arr[$i]['UserID'];
    $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
    $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
    
    # Set default user ID if class is changed
    if($StudentID == $t_user_id) {
      $default_student_id = $StudentID;
    }
    $student_detail_arr[] = array($t_user_id, $t_user_name);
  }
}
if($default_student_id == "") $default_student_id = $student_detail_arr[0][0];
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

// [2017-1207-0959-51277] skip activated account checking logic
$is_skip_activated_checking = false;
if($sys_custom['iPf']['DBSTranscript_SkipActivateChecking']) {
    $is_skip_activated_checking = true;
}

# Retrieve student info
$lpf_acc = new libpf_account_student();
$lpf_acc->SET_CLASS_VARIABLE("user_id", $default_student_id);
$lpf_acc->SET_STUDENT_PROPERTY();
$lpf_acc->SET_PARENT();

$student_name = $lpf_acc->GET_CLASS_VARIABLE(Get_Lang_Selection("chinese_name", "english_name"));
$student_photo_link = $lpf_acc->GET_IPORTFOLIO_PHOTO();
$student_photo_link = "<img src=\"".$student_photo_link."\" width=\"100\" width=\"130\" />";

$student_info_display = $lpf_ui->GEN_STUDENT_INFO_TABLE($lpf_acc);
$class_history_display = $li_pf->GEN_CLASS_HISTORY_TABLE($default_student_id);
$house_display = $lpf_ui->GEN_STUDENT_HOUSE_TABLE($lpf_acc);
$parent_display = $lpf_ui->GEN_PARENT_INFO_TABLE($lpf_acc);

# Self account
$self_account_arr = $lpf_slp->GET_DEFAULT_SELF_ACCOUNT($default_student_id);
if(is_array($self_account_arr))
{
	foreach($self_account_arr AS $sa_record_id => $DefaultSA)
	{
		if(in_array("SLP", $DefaultSA))
		{
			$self_account_default_arr = $lpf_slp->GET_SELF_ACCOUNT($sa_record_id);
		}
	}
}
$self_account_display = $lpf_ui->GEN_SELF_ACCOUNT_TABLE($sa_record_id, $self_account_default_arr, $YearClassID, $default_student_id);

if ($sys_custom['iPf']['StudentInfo']['ShowBrotherSister']) {
	$x = '';
	$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="sub_page_title">'.$ec_iPortfolio['brother_sister_info'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="stu_info_log">'."\r\n";
				$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\r\n";
					$x .= '<tr>'."\r\n";
						$x .= '<td>'.$lpf_ui->GEN_BROTHER_SISTER_INFO_TABLE($default_student_id).'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				$x .= '</table>'."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</table>'."\r\n";
	$x .= '<br />'."\r\n";
	$htmlAry['brotherSisterTable'] = $x;
}

/*
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);
	
# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

# generate student info table
$student_info_display = $li_pf->GEN_STUDENT_INFO_TABLE($default_student_id, $student_obj);

# generate class history table
$class_history_display = $li_pf->GEN_CLASS_HISTORY_TABLE($default_student_id);

# generate house and admission date table
$house_display = $li_pf->GEN_STUDENT_HOUSE_TABLE($default_student_id, $student_obj);

# generate student parent info table
$parent_display = $li_pf->GEN_PARENT_INFO_TABLE($default_student_id);

# generate student self-account table
$self_account_display = $lpf_slp->GEN_SELF_ACCOUNT_TABLE($default_student_id);
*/

$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];

### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");
$linterface->LAYOUT_START();

?>

<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
		{
			StudentSelect[0].selectedIndex = TargetIndex;
		}
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "../school_records_class.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SR()
{
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "sbs/index.php";
	document.form1.submit();
}

function jTO_INFO(jParStudentID, jParYearClassID)
{	
	$('input#SelectedStudentID').val(jParStudentID);
	$('input#StudentYearClassID').val(jParYearClassID);
	
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}
</script>

<FORM name="form1" method="POST" action="student_info_teacher.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$student_name?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['heading']['student_info']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
												<td align="right">
													<table border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td nowrap background="#"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_photo_link?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$student_name?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info_on.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></td>
<?php
	if(in_array($default_student_id, $act_student_id_arr)) {
		if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><a href="javascript:jTO_SR()" title="<?=$ec_iPortfolio['heading']['student_record']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:") && $li_pf->HAS_RIGHT("learning_sharing")) {
?>
															<td valign="top">
																<a href="javascript:jTO_LP()" title="<?=$ec_iPortfolio['heading']['learning_portfolio']?>">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br />
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php 
		}
		if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme")) {
?>
			<td valign="top"><a href="javascript:jTO_SBS()" title="<?=$iPort['menu']['school_based_scheme']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></a></td>
<?php
		}
	} else if ($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT() && $is_skip_activated_checking) {
	    ?>
															<td valign="top"><a href="javascript:jTO_SR()" title="<?=$ec_iPortfolio['heading']['student_record']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></a></td>
<?php
    }
?>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td>
													<?php
													if($sys_custom['LivingHomeopathy']){
														echo '<br />';
														echo '<table border="0" cellpadding="5" cellspacing="0" width="100%">';
														echo '<tr><td align="right"><a href="/home/portfolio/profile/personal_profile.php?TargetUserID='.$default_student_id.'" class="tablebottomlink" target="_BLANK">'.$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'].'</a></td></tr>';
														echo '</table>';
													}
													?>
													<table border="0" cellpadding="5" cellspacing="0" width="100%">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['basic_info']?></td>
														</tr>
														<tr>
														  <td width="65%" align="center" valign="top" class="stu_info_log stu_info_log_main"><?=$student_info_display?></td>
														  <td valign="top" height="100%" class="stu_info_log stu_info_log_main">
														  	<table border="0" cellpadding="5" cellspacing="0" width="100%">
														  		<tr>
														  			<td align="center" valign="top"><?=$class_history_display?></td>
																	</tr>
														  		<tr>
														  			<td align="center" valign="top"><?=$house_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br />
													<?php if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role']==""){ ?>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['guardian_info']?></td>
														</tr>
														<tr>
										  				<td class="stu_info_log">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td><?=$parent_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<br />
													
													<?php echo $htmlAry['brotherSisterTable']?>
													
													<table border="0" cellpadding="5" cellspacing="0" width="100%">
														<tr>
															<td class="sub_page_title"><?=$ec_iPortfolio['self_account']?></td>
														</tr>
														<tr>
														  <td class="stu_info_log stu_info_log_main">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td><?=$self_account_display?></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<?php } ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="SelectedStudentID" id="SelectedStudentID" value="" />
	<input type="hidden" name="StudentYearClassID" id="StudentYearClassID" value="" />
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>