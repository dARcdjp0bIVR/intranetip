<?php
//using:
/* ************************************************
 * 
 * 
 * 
 *
 * ************************************************
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/LivingHomeopathy/liblivinghomeopathy.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
		
intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$llh = new liblivinghomeopathy();
$lpf_slp = new libpf_slp();

# Initialize Export Variables
$ColumnAry = array();
$exportColumnAry = array();
$ExportArr = array();
//$thisRow = array();

$user_fields = array("ChineseName",
    "EnglishName",
    "Photo",
    "Gender",
    "HKID",
    "DateOfBirth",
    "UserEmail",
    "HomeTelNo",
    "MobileTelNo",
    "Address");

$ColumnAry['基本資料']= array('中文姓名','英文姓名','性別','身份證號碼','出生日期','電郵','家庭電話','手提電話','通訊地址');
$ColumnAry['學歷']= array('學歷', '學校名稱','學科、系', '主修', '副修','學業時期(開始)','學業時期(完成)','學業時期(時間)');
$ColumnAry['訓練及進修']= array('機構','課程內容概述','訓練及進修時期(開始)','訓練及進修時期(完結)','訓練及進修時期(時間)');
$ColumnAry['工作經歷']= array('曾任職務','職稱','公司名稱','工作時期(開始)','工作時期(完結)','工作時期(時間)');
$ColumnAry['社會工作']= array('社團 / 學術團體名稱', '參與時期(開始)', '參與時期(完結)', '參與時期(時間)','擔任職務');
$ColumnAry['語文能力']= array('語文','說話','閱讀','書寫','聆聽');
$ColumnAry['專長']= '專長';
$ColumnAry['專業技術']= '專業技術';
$ColumnAry['其他']= '其他';

$exportNumColumnAry['基本資料']= 1;
$exportNumColumnAry['學歷']= 0;
$exportNumColumnAry['訓練及進修']= 0;
$exportNumColumnAry['工作經歷']= 0;
$exportNumColumnAry['社會工作']= 0;
$exportNumColumnAry['語文能力']= 0;
$exportNumColumnAry['專長']= 0;
$exportNumColumnAry['專業技術']= 0;
$exportNumColumnAry['其他']= 0;


$UserIDArr = (is_array($user_id)) ? $user_id : explode(",", $user_id);
$SessionsArr = (is_array($sections)) ? $sections : explode(",", $sections);
// debug_pr($UserIDArr); debug_pr($SessionsArr); die();
$dataCounter = 0;
foreach($UserIDArr as $TargetUserID){
    $libuser = new libuser($TargetUserID);
    $profile_records = $llh->getLivingHomeopathyPersonalProfileRecords(array('UserID'=>$TargetUserID));
    if(count($profile_records)==0){
        $data = $llh->getLivingHomeopathyPersonalProfileRecordStructure($libuser);
    }else{
        $data = unserialize(base64_decode($profile_records[0]['Profile']));
        if($profile_records[0]['DateModified'] < $libuser->DateModified)
        {
            foreach($user_fields as $field){
                if(isset($libuser->$field)){
                    if($field == 'Gender'){
                        $data[$field] = $gender_map[$libuser->$field];
                    }else{
                        $data[$field] = $libuser->$field;
                    }
                }
            }
        }
    }
    
    if($data['Others'] == ''){
        $self_account_arr = $lpf_slp->GET_DEFAULT_SELF_ACCOUNT($TargetUserID);
        if(is_array($self_account_arr))
        {
            foreach($self_account_arr AS $sa_record_id => $DefaultSA)
            {
                if(in_array("SLP", $DefaultSA))
                {
                    $self_account_default_arr = $lpf_slp->GET_SELF_ACCOUNT($sa_record_id);
                    if($self_account_default_arr['Details'] != ''){
                        $data['Others'] = $self_account_default_arr['Details'];
                    }
                }
            }
        }
    }
    if(count($data['Education']) > $exportNumColumnAry['學歷']){
        $exportNumColumnAry['學歷'] = count($data['Education']);
    }
    if(count($data['Training']) > $exportNumColumnAry['訓練及進修']){
        $exportNumColumnAry['訓練及進修'] = count($data['Training']);
    }
    if(count($data['WorkingExperience']) > $exportNumColumnAry['工作經歷']){
        $exportNumColumnAry['工作經歷'] = count($data['WorkingExperience']);
    }
    if(count($data['SocialWork']) > $exportNumColumnAry['社會工作']){
        $exportNumColumnAry['社會工作'] = count($data['SocialWork']);
    }
    if(count($data['LanguageSkills']) > $exportNumColumnAry['語文能力']){
        $exportNumColumnAry['語文能力'] = count($data['LanguageSkills']);
    }
    if(count($data['Expertise']) > $exportNumColumnAry['專長']){
        $exportNumColumnAry['專長'] = count($data['Expertise']);
    }
    if(count($data['ProfessionalSkills']) > $exportNumColumnAry['專業技術']){
        $exportNumColumnAry['專業技術'] = count($data['ProfessionalSkills']);
    }
    if(count($data['Others']) > $exportNumColumnAry['其他']){
        $exportNumColumnAry['其他'] = count($data['Others']);
    }
    $DataArr[$dataCounter]=$data; 
    $dataCounter++;
}
for($i=0; $i<$exportNumColumnAry['基本資料']; $i++){
    $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['基本資料']);
}
if( in_array('學歷',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['學歷']; $i++){
        $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['學歷']);
    }
}
if( in_array('訓練及進修',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['訓練及進修']; $i++){
        $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['訓練及進修']);
    }
}
if( in_array('工作經歷',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['工作經歷']; $i++){
        $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['工作經歷']);
    }
}
if( in_array('社會工作',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['社會工作']; $i++){
        $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['社會工作']);
    }
}
if( in_array('語文能力',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['語文能力']; $i++){
        $exportColumnAry = array_merge($exportColumnAry, $ColumnAry['語文能力']);
    }
}
if( in_array('專長',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['專長']; $i++){
        array_push($exportColumnAry, $ColumnAry['專長']);
    }
}
if( in_array('專業技術',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['專業技術']; $i++){
        array_push($exportColumnAry, $ColumnAry['專業技術']);
    }
}
if( in_array('其他',$SessionsArr)){
    for($i=0; $i<$exportNumColumnAry['其他']; $i++){
         array_push($exportColumnAry, $ColumnAry['其他']);
    }
}

# Export Row(s)
if(empty($UserIDArr) || empty($SessionsArr)){
    $ExportArr[] = $Lang['General']['NoRecordFound'];
} else {
    $totalColumnArys = count($exportColumnAry);
    $rowNumber =0;
    foreach($DataArr as $studentDataArray){
//         debug_pr($studentDataArray);
        $ColumnCounter = 0;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['ChineseName'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['EnglishName'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['Gender'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['HKID'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['DateOfBirth'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['UserEmail'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['HomeTelNo'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['MobileTelNo'];
        $ColumnCounter++;
        $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['Address'];
        if(in_array('學歷',$SessionsArr)){
            $numRecordSet = count($studentDataArray['Education']);
//             debug_pr($studentDataArray['Education']);
            for($i=0; $i<$numRecordSet; $i++){
                foreach($studentDataArray['Education'][$i] as $data){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $data;
                }
                
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['學歷']; $i++){
                for($h=0; $h<count($ColumnAry['學歷']); $h++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
                }
            }
        }
        if( in_array('訓練及進修',$SessionsArr)){
            $numRecordSet = count($studentDataArray['Training']);
            for($i=0; $i<$numRecordSet; $i++){
                foreach($studentDataArray['Training'][$i] as $data){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $data;
                }
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['訓練及進修']; $i++){
                for($h=0; $h<count($ColumnAry['訓練及進修']); $h++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
                }
            }
        }
        if( in_array('工作經歷',$SessionsArr)){
            $numRecordSet = count($studentDataArray['WorkingExperience']);
            for($i=0; $i<$numRecordSet; $i++){
                foreach($studentDataArray['WorkingExperience'][$i] as $data){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $data;
                }
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['工作經歷']; $i++){
                for($h=0; $h<count($ColumnAry['工作經歷']); $h++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
                }
            }
        }
        if( in_array('社會工作',$SessionsArr)){
            $numRecordSet = count($studentDataArray['SocialWork']);
            for($i=0; $i<$numRecordSet; $i++){
                foreach($studentDataArray['SocialWork'][$i] as $data){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $data;
                }
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['社會工作']; $i++){
                for($h=0; $h<count($ColumnAry['社會工作']); $h++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
                }
            }
        }
        if( in_array('語文能力',$SessionsArr)){
            $numRecordSet = count($studentDataArray['LanguageSkills']);
//             debug_pr($numRecordSet);
            for($i=0; $i<$numRecordSet; $i++){
                foreach($studentDataArray['LanguageSkills'][$i] as $data){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $data;
                }
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['語文能力']; $i++){
                for($h=0; $h<count($ColumnAry['語文能力']); $h++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
                }
            }
        }
        if( in_array('專長',$SessionsArr)){
            $numRecordSet = count($studentDataArray['Expertise']);
            for($i=0; $i<$numRecordSet; $i++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['Expertise'][$i];
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['專長']; $i++){
                    $ColumnCounter++;
                    $ExportArr[$rowNumber][$ColumnCounter] = "";
            }
        } 
        if( in_array('專業技術',$SessionsArr)){
            $numRecordSet = count($studentDataArray['ProfessionalSkills']);
            for($i=0; $i<$numRecordSet; $i++){
                $ColumnCounter++;
                $ExportArr[$rowNumber][$ColumnCounter] = $studentDataArray['ProfessionalSkills'][$i];
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['專業技術']; $i++){
                $ColumnCounter++;
                $ExportArr[$rowNumber][$ColumnCounter] = "";
            }
        }
        if( in_array('其他',$SessionsArr)){
            $numRecordSet = count($studentDataArray['Others']);
            for($i=0; $i<$numRecordSet; $i++){
                $ColumnCounter++;
                $ExportArr[$ColumnCounter] = $studentDataArray['Others'][$i];;
            }
            for($i=$numRecordSet; $i<$exportNumColumnAry['其他']; $i++){
                $ColumnCounter++;
                $ExportArr[$rowNumber][$ColumnCounter] = "";
            }
        }
        $rowNumber++;
    }
}
// debug_pr($ExportArr);
// debug_pr($DataArr);

$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumnAry, "\t", "\r\n", "\t", 1);

intranet_closedb();
      
$filename = "personal_profile.csv";
$lexport->EXPORT_FILE($filename, $export_content);	

?>