<?php

// Modifing by 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."ncs/settings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
$eclassApiAuth = new libeclassapiauth();
$json = new JSON_obj();
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];
if ($order=="") $order=1;
if ($field=="") $field=0;

$lpf = new libpf_slp();
$luser = new libuser($UserID);
$li = new libdbtable2007($field, $order, $pageNo);
$CurrentPage = "Student_SchoolRecords";
switch($_SESSION['UserType'])
{
	case 2:
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}
// Get eClass Token & Course ID
$sql = "SELECT c.course_id FROM {$eclass_db}.user_course uc
RIGHT JOIN {$eclass_db}.course c ON uc.course_id=c.course_id AND intranet_user_id='".$_SESSION['UserID']."'
WHERE course_code='NCS'";
$course_id = ($sys_custom['ncs_dev'])?"23":current($lpf->returnVector($sql));

$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserID='".$UserID."'";
$apiKeys = $eclassApiAuth->GetAPIKeyList();

$getTokenRequestVar = array(
		"eClassRequest"=>array(
				"APIKey"=>$apiKeys[2]["APIKey"],
				"SessionID"=>($sys_custom['ncs_dev'])?"68cc18922cad6831b703f57a36cf028b":current($lpf->returnVector($sql)),
				"RequestMethod"=>"LoginCourses"
		),
		"Request"=>null
);
$postFieldStr = $json->encode($getTokenRequestVar);
$ch = curl_init();
$eclassApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080/":$intranet_rel_path;
curl_setopt($ch, CURLOPT_URL, $eclassApiUrl."webserviceapi/?reqtype=json");
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postFieldStr);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($postFieldStr)
)
		);
$curlData=curl_exec($ch);
$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

$tokenList = $json->decode($curlData);
$AuthToken = $tokenList['Result'][$course_id];

// Get List of Lessons to investigate
$pl2_header = array();
$pl2_header[] = 'Content-Type: application/json';
$pl2_header[] = 'X-CourseId: '.$course_id;
$pl2_header[] = 'X-AuthToken: '.$AuthToken;
$pl2_header[] = 'Accept:  application/json';

$sql = "SELECT user_id FROM ".classNamingDB($course_id).".usermaster WHERE intranet_user_id='".$default_student_id."'";
$class_user_id = ($sys_custom['ncs_dev'])?"5":current($li->returnVector($sql));

//if(!$sys_custom['ncs_dev']){
$lessonApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
$ch = curl_init();
$url = $lessonApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/user/".$class_user_id."/quizActivities";
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
$resultData=curl_exec($ch);
curl_close($ch);
//}else{
//	$lessonData = '{"2":"Henry Test","6":"Thomas testing","22":"SW 1221","39":"SW 1221 - copy","40":"quiz","41":"Test 170120","44":"PL Testing 170309","45":"PL Testing 170309 - copy","46":"Ronald Testing","47":"Testing 20170314","48":"","49":"","50":"Siuwan Test 170317","51":"Henry Test 20170329","52":"","53":"","54":"quiz - copy","55":"","56":"Ronald Testing - copy","57":"","58":"Henry Test 20170329 - copy","59":"Thomas testing - copy","60":"SW 1221 - copy","61":"Siuwan Test 170317 - copy","62":"PL Testing 170309 - copy - copy","63":"PL Testing 170309 - copy","64":"Testing 20170314 - copy","65":"Henry Test - copy","66":"Thomas testing - copy - copy","67":"Henry Test - copy","68":"","69":"Ronald Testing1111","70":"Test 170120 - copy","71":"SW 1221 - copy - copy","72":"SW 1221 - copy","73":"SW 1221 - copy - copy","74":"Henry Test 20170407","75":"Henry FC Test 20170410","76":"Henry Test Power Presenter","77":"","78":"Thomas 201704210950","79":"Henry Test 20170421","80":"Henry Test PowerPresenter & PowerPad","81":"","82":"Henry Test 20170508","83":"","84":"Henry Test 20170516","85":"","86":"Henry Test 20170602","87":"","88":"","89":"Henry Test 20170609","90":"","91":"Thomas","92":"Henry Test 20160619","93":"Henry Copy Test 20160619","95":"Henry Copy Test 20160619 - copy","96":"Henry test powerpad","97":"","98":"Discussion 20170626"}';
//}

$resultList = $json->decode($resultData);

$sql = "CREATE TEMPORARY TABLE quiz_result_PL2_report_std(
					quiz_id int(11) NOT NULL,
					lesson_id int(11) default NULL,
					quiz_name varchar(255) default NULL,
					score int(3) default NULL,
					fullMarks int(3) default NULL
				)ENGINE = InnoDB CHARACTER SET=utf8";
$li->db_db_query($sql);
if($responseCode!='404' && !isset($resultList['error'])){
	foreach($resultList as $lid=>$result){
		foreach($result as $qid=>$re){
			if($re['title']==""){
				$re['title'] = $Lang['PL2_Report']['Untitled'];
			}
			$sql = "INSERT INTO quiz_result_PL2_report_std VALUES('".$qid."','".$lid."','".$re['title']."','".$re['score']."','".$re['fullMarks']."')";
			$li->db_db_query($sql);
		}
	}
}

//if(!$sys_custom['ncs_dev']){
$lessonApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
$ch = curl_init();
$url = $lessonApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/lessons";
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
$lessonData=curl_exec($ch);
curl_close($ch);
//}else{
//	$lessonData = '{"2":"Henry Test","6":"Thomas testing","22":"SW 1221","39":"SW 1221 - copy","40":"quiz","41":"Test 170120","44":"PL Testing 170309","45":"PL Testing 170309 - copy","46":"Ronald Testing","47":"Testing 20170314","48":"","49":"","50":"Siuwan Test 170317","51":"Henry Test 20170329","52":"","53":"","54":"quiz - copy","55":"","56":"Ronald Testing - copy","57":"","58":"Henry Test 20170329 - copy","59":"Thomas testing - copy","60":"SW 1221 - copy","61":"Siuwan Test 170317 - copy","62":"PL Testing 170309 - copy - copy","63":"PL Testing 170309 - copy","64":"Testing 20170314 - copy","65":"Henry Test - copy","66":"Thomas testing - copy - copy","67":"Henry Test - copy","68":"","69":"Ronald Testing1111","70":"Test 170120 - copy","71":"SW 1221 - copy - copy","72":"SW 1221 - copy","73":"SW 1221 - copy - copy","74":"Henry Test 20170407","75":"Henry FC Test 20170410","76":"Henry Test Power Presenter","77":"","78":"Thomas 201704210950","79":"Henry Test 20170421","80":"Henry Test PowerPresenter & PowerPad","81":"","82":"Henry Test 20170508","83":"","84":"Henry Test 20170516","85":"","86":"Henry Test 20170602","87":"","88":"","89":"Henry Test 20170609","90":"","91":"Thomas","92":"Henry Test 20160619","93":"Henry Copy Test 20160619","95":"Henry Copy Test 20160619 - copy","96":"Henry test powerpad","97":"","98":"Discussion 20170626"}';
//}
$lessonList = $json->decode($lessonData);
$sql = "CREATE TEMPORARY TABLE quiz_result_PL2_report_lesson(
					lesson_id int(11) NOT NULL,
					lesson_name varchar(255) default NULL
				)ENGINE = InnoDB CHARACTER SET=utf8";
$li->db_db_query($sql);
if(!empty($lessonList) && !isset($lessonList['error'])){
	foreach($lessonList as $lid=>$lname){
		if($lname==""){
			$lname = $Lang['PL2_Report']['Untitled'];
		}
		$sql = "INSERT INTO quiz_result_PL2_report_lesson VALUES('".$lid."','".$lname."')";
		$li->db_db_query($sql);
	}
}
$sql = "SELECT l.lesson_name, r.quiz_name, CONCAT(r.score,'/',r.fullMarks) as result FROM quiz_result_PL2_report_std r INNER JOIN quiz_result_PL2_report_lesson l ON r.lesson_id=l.lesson_id";


////////////////////////////////////////////////////////
///// TABLE SQL
$li->field_array = array("lesson_name","quiz_name", "result");
/*$ChooseYear = $_GET['ChooseYear'];
if($ChooseYear!="")
{$conds = "AND a.Year = '$ChooseYear'";}
$sql = "SELECT
          a.Year,
		  if(a.Semester = '' , '".$Lang['General']['WholeYear']."',a.Semester) as  'Semester',
          a.ActivityName,
          if(a.Role='', '--', a.Role),
          if(a.Performance='', '--', a.Performance),
          a.ModifiedDate FROM {$eclass_db}.ACTIVITY_STUDENT as a
        WHERE 
    			a.UserID = '$StudentID'
    			$conds
      ";*/

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $ec_iPortfolio['record'];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = 4;
//$li->noNumber = true;
//list($year) = $li->db_db_query($sql);
$sql2 = "Select DISTINCT a.Year FROM {$eclass_db}.ACTIVITY_STUDENT as a WHERE a.UserID = '$StudentID' ORDER BY Year";
$ActivityYearArr = $lpf->returnArray($sql2);
$pageSizeChangeEnabled = true;
		/*
		for($i=0; $i<sizeof($ActivityArr); $i++)
		{
			list($year) = $ActivityArr[$i];
			echo $i.";".$year;
		}
		*/
		

$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0' width='100%'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$Lang['PL2_Report']['Lesson'])."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$Lang['PL2_Report']['Quiz'])."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(2,$Lang['iPortfolio']['ThirdParty']['Result'].$Lang['PL2_Report']['Remark'])."</td>\n";
$li->column_array = array(0,0,0);

//$SelectBoxContent = $lpf->displayYearSelectBox($StudentID, $ClassName,$_GET['ChooseYear']);

//////////////////////////////////////////////

# define the page title and table size
//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[3][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);
### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 1);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("pl2_quiz_report");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
-->
</script>


<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
				 		</td>
          </tr> 
          <tr>
            <td>
              <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><?= $SelectBoxContent?></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center">
              <?= $li->displayPlain() ?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td class="tabletext" align="right"><?=$li->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="StudentID" value="<?=$StudentID?>">
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
