<?php
// Modifing by
/*
 * Modification Log:
 * 
 * 2010-12-28 Ivan
 * improved: allow txt file import
 * 
 */ 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ImportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

define ("ERROR_TYPE_NO_MATCH_REGNO", 1);
define ("ERROR_TYPE_EMPTY_REGNO", 2);
define ("ERROR_TYPE_INCORRECT_REGNO", 3);
define ("SBJ_CMP_SEPARATOR","###");

if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# Param: $targetYear, $targetSemester, $IsAnnual

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$count_new = 0;
$count_updated = 0;
$display_content = "";

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
   header("Location: import_sams.php");
}
else
{
	intranet_opendb();
	$lpf = new libpf_sturec();
	
	$lpf->CHECK_ACCESS_IPORTFOLIO();
	
	$CurrentPage = "SAMS_import_student_data";
	$title = $ec_iPortfolio['SAMS_import_student_data'];
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ["title"] = $title;
	
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();

	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV" || $ext == ".TXT")
	{
		$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);                   # drop the title bar
	}

    # Check Format
    $file_format = array("RegNo","Nationality","PlaceOfBirth","AdmissionDate");
    for ($i=0; $i<sizeof($file_format); $i++)
    {
		$hearder = strtolower($header_row[$i]);
		$format_header = strtolower($file_format[$i]);
        if ($hearder!=$format_header)
        {
            $format_wrong = true;
            break;
        }
    }

    if ($format_wrong)
    {
		#############################################################################
		# Wrong file format
		$correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($file_format); $i++)
        {
             $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
        }
        $correct_format .= "</table>\n";

        $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($header_row); $i++)
        {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
        }
        $wrong_format .= "</table>\n";

        $display_content .= "<br><span class='tabletext'>".$ec_guide['import_error_wrong_format']."</span><br>\n";
        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
		$display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
        $display_content .= "</table>\n";
    }
    else
    {
		# Retrieve All Student ID from PORTFOLIO_STUDENT
		$sql = "SELECT WebSAMSRegNo, UserID FROM {$eclass_db}.PORTFOLIO_STUDENT";
		$row = $li->returnArray($sql, 2);
		$studentArr = $lpf->build_assoc_array($row);

		unset($error_data);
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($t_regno, $t_nationality, $t_place_of_birth, $t_admission_date) = $data[$i];
			 $t_regno = trim($t_regno);
			 $t_nationality = addslashes(trim($t_nationality));
			 $t_place_of_birth = addslashes(trim($t_place_of_birth));
			 $t_admission_date = trim($t_admission_date);
			 
			 # Eric Yip (20090929): Parse excel date
			 $t_admission_date_arr = explode("/", $t_admission_date);
			 if(count($t_admission_date_arr) == 3)
			 {
			   $t_year = str_pad($t_admission_date_arr[2], 4, "20", STR_PAD_LEFT);
			   $t_month = str_pad($t_admission_date_arr[1], 2, "0", STR_PAD_LEFT);
			   $t_day = str_pad($t_admission_date_arr[0], 2, "0", STR_PAD_LEFT);
			 
			   $t_admission_date = $t_year."-".$t_month."-".$t_day;
       }
			
			////else if (strpos($t_regno, "#")===false)
             if ($t_regno == "")
             {
                 $error_data[] = array($i, ERROR_TYPE_EMPTY_REGNO);
                 continue;
             }
			 else if (substr($t_regno, 0, 1)!="#")
			 {
				 $error_data[] = array($i, ERROR_TYPE_INCORRECT_REGNO, array($t_regno));
                 continue;
			 }
			
			if($studentArr[$t_regno]!="" || $studentArr[str_replace("#","", $t_regno)]!="")
			{
			   $t_nationality = ($t_nationality=="") ? 'NULL' : $t_nationality;
			   $t_place_of_birth = ($t_place_of_birth=="") ? 'NULL' : $t_place_of_birth;
			   $t_admission_date = ($t_admission_date=="") ? 'NULL' : $t_admission_date;

				 $sql = "UPDATE
							{$eclass_db}.PORTFOLIO_STUDENT
						SET
							Nationality = '$t_nationality' ,
							PlaceOfBirth = '$t_place_of_birth' ,
							AdmissionDate = '$t_admission_date',
							ModifiedDate = now()
						WHERE 
							WebSAMSRegNo = '$t_regno' OR CONCAT('#', WebSAMSRegNo) = '$t_regno'";
				$li->db_db_query($sql);
				$count_updated++;
			}
			else
			{
				$error_data[] = array($i, ERROR_TYPE_NO_MATCH_REGNO, array($t_regno));
				continue;
			}

        }

    }

}

# Display import stats
if (!$format_wrong)
{
	$display_content .= "<table border='0' cellpadding='5' cellspacing='0'>";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_update_no']." : </td><td class='tabletext'><b>".$count_updated."</b></td></tr>\n";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_fail_no']." : </td><td class='tabletext'><b>".sizeof($error_data)."</b></td></tr>\n";
	$display_content .= "</table>\n";
}

if (sizeof($error_data)>0)
{
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";

	for ($i=0; $i<sizeof($error_data); $i++)
	{
		list ($t_row, $t_type, $t_data) = $error_data[$i];
		$t_row++;     # set first row to 1
		$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
		$reason_string = $ec_guide['import_error_unknown'];
		switch ($t_type)
		{
			case ERROR_TYPE_NO_MATCH_REGNO:
					$reason_string = $ec_guide['import_error_no_user'];
					break;
			case ERROR_TYPE_EMPTY_REGNO:
					$reason_string = $ec_iPortfolio['activation_result_no_regno'];
					break;
			case ERROR_TYPE_INCORRECT_REGNO:
					$reason_string = $ec_guide['import_error_incorrect_regno'];
					break;
			default:
					$reason_string = $ec_guide['import_error_unknown'];
					break;
		}
		$error_table .= $reason_string;
		//debug_r($t_data);
		$error_contents = (is_array($t_data)) ? implode(",",$t_data) : $t_data."&nbsp;";
		$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}

?>

<FORM enctype="multipart/form-data" method="POST" name="form1">
	<br />
	<?= $display_content ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='import_sams.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
