<?php

// Modifing by 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//Others:
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
intranet_auth();
iportfolio_auth("TSP");
intranet_opendb();

# define the page title and table size

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPageName = $iPort['menu']['school_records'];

$lpf = new libpf_slp();
$luser = new libuser($UserID);

$CurrentPage = "Student_SchoolRecords";
switch($_SESSION['UserType'])
{
	case 2:
		$StudentID = $_SESSION['UserID'];
		break;
	case 3:
		$StudentID = $ck_current_children_id;
		break;
	default:
		break;
}


////////////////////////////////////////////////////////
///// TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("YearName", "AssessmentTitle", "Report");
$ChooseYear = $_GET['ChooseYear'];
if ($intranet_session_language == "en") {
	$sql_YearName = "y.YearNameEN";
}
else {
	$sql_YearName = "y.YearNameB5";
}
$sql = "SELECT {$sql_YearName} as YearName, AssessmentTitle,
				CONCAT('<a target=\"_blank\" href=\"/home/portfolio/teacher/management/assessment_report/assessment_file_manage.php?action=readFile&assessmentId=',arsr.AssessmentID,'&studentId=',arsr.UserID,'\">".$ec_iPortfolio['view_analysis']."</a>') as Report 
				FROM {$eclass_db}.ASSESSMENT_REPORT_STUDENT_RECORD arsr 
				LEFT JOIN {$eclass_db}.ASSESSMENT_REPORT ar ON arsr.AssessmentID=ar.RecordID
				LEFT JOIN {$intranet_db}.ACADEMIC_YEAR y ON ar.AcademicYearID = y.AcademicYearID
				WHERE arsr.UserID='".$StudentID."'";
$sql .= ($ChooseYear!="")?" AND ar.AcademicYearID='".$ChooseYear."'":"";

// TABLE INFO
$li->sql = $sql;
$li->db = $intranet_db;
$li->title = $ec_iPortfolio['record'];
$li->no_msg = $no_record_msg;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$li->no_col = count($li->field_array)+1;
//$li->noNumber = true;
//list($year) = $li->db_db_query($sql);
$sql2 = "Select DISTINCT a.Year FROM {$eclass_db}.ACTIVITY_STUDENT as a WHERE a.UserID = '$StudentID' ORDER BY Year";
$ActivityYearArr = $lpf->returnArray($sql2);
$pageSizeChangeEnabled = true;
		/*
		for($i=0; $i<sizeof($ActivityArr); $i++)
		{
			list($year) = $ActivityArr[$i];
			echo $i.";".$year;
		}
		*/
		

$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='10' cellspacing='0' width='100%'>";
$li->row_alt = array("#FFFFFF", "F3F3F3");
$li->row_height = 20;
$li->sort_link_style = "class='tbheading'";


// TABLE COLUMN
$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$Lang['General']['AcademicYear'])."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$iPort['Assessment']['Title'])."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(2,$iPort['Assessment']['AssessmentFile'])."</td>\n";
$li->column_array = array(0,0,0);

$SelectBoxContent = $lpf->displayYearSelectBox($StudentID, $ClassName,$_GET['ChooseYear']);

//////////////////////////////////////////////

# define the page title and table size
//$template_width = "98%";
//$template_left_menu = getLeftMenu($menu_arr, $menu_arr[3][1]);
//echo getBodyBeginning($template_pages, $template_width, 1, "red", "", $template_left_menu, $template_table_top_right);
### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// Tab Menu Settings
/*
$TabMenuArr = array();
if($lpf->access_config['merit'] == 1)
	$TabMenuArr[] = array("../merit/index.php", $ec_iPortfolio['title_merit'], 0);
if($lpf->access_config['assessment_report'] == 1)
	$TabMenuArr[] = array("../assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
if($lpf->access_config['activity'] == 1)
	$TabMenuArr[] = array("../activity/index.php", $ec_iPortfolio['title_activity'], 1);
if($lpf->access_config['award'] == 1)
	$TabMenuArr[] = array("../award/index.php", $ec_iPortfolio['title_award'], 0);
if($lpf->access_config['teacher_comment'] == 1)
	$TabMenuArr[] = array("../comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
if($lpf->access_config['attendance'] == 1)
	$TabMenuArr[] = array("../attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
if($lpf->access_config['service'] == 1)
	$TabMenuArr[] = array("../service/index.php", $ec_iPortfolio['service'], 0);
*/
$TabMenuArr = libpf_tabmenu::getSchoolRecordTags("assessment");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
-->
</script>


<FORM name="form1" method="GET">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table width="100%"  border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
				 		</td>
          </tr> 
          <tr>
            <td>
              <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td><?= $SelectBoxContent?></td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td align="center">
              <?= $li->displayPlain() ?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($li->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td class="tabletext" align="right"><?=$li->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <input type="hidden" name="ClassName" value="<?=$ClassName?>">
  <input type="hidden" name="StudentID" value="<?=$StudentID?>">
  <input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>">
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</FORM>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
