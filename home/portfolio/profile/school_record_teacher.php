<?php
/**
 * [Modification Log] Modifying By:  
 * ************************************************
 *  Date:   2020-09-14 [Bill]   [DM#3794]
 *          - handle empty grade display if score > 0
 *  Date:   2020-06-29 [Bill]   [2020-0612-1004-37207]
 *          - show score if Score > 0 + No Grade + No Ranking   ($sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'])
 * 	Date:	2019-09-30 [Philips] [2019-0218-1220-15066]
 * 			- allow AssessmentReport Show Score = 0 in Score, SccoreGrade ($sys_custom['ipf']['Mgmt']['SchoolRecordShowZero_TLCDMC'])
 *  Date:	2019-03-19 [Bill]   [2017-1207-0959-51277]
 * 			- allow access "Academic Report" of non-activated student accounts    ($sys_custom['iPf']['DBSTranscript_SkipActivateChecking'])
 *  Date:   2018-11-30 [Bill]   [2017-1207-0959-51277]
 *          - add edit mode for assessment report
 *  Date:   2018-10-24 [Paul]
 *          - add features to prevent SQL injection
 *  Date:   2018-08-06 [Bill]   [2017-0901-1527-45265]
 *          - add float header for Academic Report
 *          - add special handling for Academic Report display - HKUGA College 
 *  Date:   2018-05-21 [Anna]
 *          - show semester name by mapping YearTermID instead of semester fields in CONDUCT_STUDENT #139772
 * 	Date:	2018-01-22 [Cameron]
 * 			- add reading_records RecordType [case #E118376]
 * 			- add reading_records in jVIEW_DETAIL 
 *  Date:	2017-12-07 [Pun]
 *          - Modified pl2_quiz_report, fixed cannot load result
 *  Date:	2017-11-30 [Carlos] - For OLE section, added Category selection filter for $sys_custom['LivingHomeopathy'].
 * 	Date:	2017-11-07 [Bill]	[2017-0403-1552-04240]
 * 			- HOY Group Member - View all students	($sys_custom['iPf']['HKUGA_eDis_Tab'])
 * 			- Control access of Pastoral Record & Student Record Tab
 * 	Date:	2017-11-06 (Bill)	[2017-0403-1552-04240]
 * 			- added Pastoral Record & Student Record Tab
 * 	Date:	2017-09-26 (Anna) #J124287
 * 			- added tooltips for icons
 *  Date:	2017-08-29 (Carlos)
 * 			- $sys_custom['LivingHomeopathy'] added homework tab.
 *  Date:	2017-04-12 (Villa) DM#3187
 *  		-	fix cannot change class in select box
 *	Date:   2017-03-29 (Siuwan) #X115139
 *		 	- 	add LP version checking in js function jTO_LP()  
 *  Date:	2017-03-27 (Villa) #X115124
 *  		-	modified $class_selection_html: fix all class bug after hiding option <-select->  
 *  Date:	2016-12-13 (Villa) #F110157 
 *  		-	enable keyword searching for OLE title
 *  Date:	2016-09-20 (Villa) #F99723
 *  		-	add two fitlers in case ole -search by tilte -filter by ole case('ole')
 * 	Date:	2016-08-19 (Ivan) [ip.2.5.7.10.1]
 * 			- if enabled flag $sys_custom['iPf']['showAcademicResultWithAssessment'], show academic result table with assessment result
 * 	Date:	2016-08-17 [Ivan] [ip.2.5.7.10.1]
 * 			 - For macau pui ching, hide academic result SD score display if flag $sys_custom['iPf']['hideAcademicResultDisplaySDScore'] is on
 *  Date:	2016-05-20 [Omas]
 * 			 - Fix Fatal error: Call-time pass-by-reference has been removed - changed abc(&$a); -> abc($a);
 *	Date:   2016-04-15 [Omas]
 *			 - fix class selection access right problem -#U94994 
 * 	Date:	2016-02-02 [Cameron] show column "Academic" in teacher's comment list if 
 * 			$sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] is true
 *  
 * 	Date:	2015-11-19 [Cameron] add case 'other_reports' to show all other reports of a students
 * 
 * ************************************************
 */

ini_set('memory_limit', '256M');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

$db = new libdb();
if($db->isMagicQuotesOn()){
	$StudentInfo = standardizeFormPostValue($StudentInfo);
}
$StudentInfo = (isset($StudentInfo) && $StudentInfo != "")?$db->Get_Safe_Sql_Query($StudentInfo):""; // prevent SQL injection

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

// Chung Wing Kwong
if($sys_custom['cwk_SLP'])
{
  include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/libpf-slp-cwk.php");
  include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
}

iportfolio_auth("T");
intranet_opendb();

$li_pf = new libpf_sturec();
$li_pf->ACCESS_CONTROL("student_info");

// Access right checking for class selection box
if(!strstr($ck_user_rights, ":manage:"))
{
	# Class teacher view class
	if(strstr($ck_user_rights_ext, "student_info:form_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
	}
	# Subject teacher view class
	else if(strstr($ck_user_rights_ext, "student_info:form_subject_t"))
	{
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
		$class_teach_arr = array_merge($lpf_acc->GET_CLASS_TEACHER_CLASS(), $lpf_acc->GET_SUBJECT_TEACHER_CLASS());
		$cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
	}
}

// [2017-0403-1552-04240] HOY Group Member - View all students
if($sys_custom['iPf']['HKUGA_eDis_Tab'])
{
	if($li_pf->IS_HOY_GROUP_MEMBER_IPO()) {
		$class_teach_arr = "";
		$cond = "";
	}
	
	// Get Teaching Classes
	if(!isset($lpf_acc)) {
		$lpf_acc = new libpf_account_teacher();
		$lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
	}
	$TeachingClasses = $lpf_acc->GET_CLASS_TEACHER_CLASS();
}

// Get class selection with optgroup
$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
for($i=0; $i<count($t_classlevel_arr); $i++)
{
    $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
    $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];
    
    $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
    $t_class_arr = $lpf_fc->GET_CLASS_LIST();
    
    for($j=0; $j<count($t_class_arr); $j++)
    {
        $t_yc_id = $t_class_arr[$j][0];
        $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
        if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
        {
            $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
        }
        
        if($t_yc_id == $YearClassID)
        {
            $class_name = $t_yc_title;
        }
    }
}

$lpf_ui = new libportfolio_ui();
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
//$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, true);
if($_SESSION['SSV_USER_ACCESS']['other-iPortfolio']||$YearClassID){ #X115124
	$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, false);
}
else if($YearClassID==''){
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$sql = "Select 
				ycu.YearClassID 
			from 
				YEAR_CLASS_USER ycu
			INNER JOIN
				YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$AcademicYearID."'
			WHERE
				ycu.UserID = '".IntegerSafe($StudentID)."'";
	$Student_Year_ClassID = $li_pf->returnResultSet($sql);
	$YearClassID = $Student_Year_ClassID[0]['YearClassID'];
	$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, true);
}

# Get student selection
$lpf_fc->SET_CLASS_VARIABLE("YearID", "");
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$default_student_id = "";
$student_id_arr = $lpf_fc->GET_STUDENT_LIST();
$t_student_detail_arr = $lpf_fc->GET_STUDENT_DETAIL_LIST($student_id_arr);
if(is_array($t_student_detail_arr))
{
    for($i=0; $i<count($t_student_detail_arr); $i++)
    {
        $t_classname = $t_student_detail_arr[$i]['ClassName'];
        if(in_array($t_classname, $class_arr)) continue;
        
        $t_user_id = $t_student_detail_arr[$i]['UserID'];
        $t_user_name = "(".$t_classname." - ".$t_student_detail_arr[$i]['ClassNumber'].") ";
        $t_user_name .= Get_Lang_Selection($t_student_detail_arr[$i]['ChineseName'], $t_student_detail_arr[$i]['EnglishName']);
        
        # Set default user ID if class is changed
        if($StudentID == $t_user_id)
          $default_student_id = IntegerSafe($StudentID);
        
        $student_detail_arr[] = array($t_user_id, $t_user_name);
    }
}
if($default_student_id == "") $default_student_id = IntegerSafe($student_detail_arr[0][0]);
$student_selection_html = getSelectByArray($student_detail_arr, "name='StudentID' onChange='jCHANGE_FIELD()'", $default_student_id, 0, 1, "", 2);

# Get activated students
$act_student_id_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST();

// [2017-1207-0959-51277] skip activated account checking logic
$is_skip_activated_checking = false;
if($sys_custom['iPf']['DBSTranscript_SkipActivateChecking']) {
    $is_skip_activated_checking = !in_array($default_student_id, $act_student_id_arr);
    
    // set record type to assessment report only
    if($is_skip_activated_checking) {
        $RecordType = ($RecordType != 'assessment_report' && $RecordType != 'assessment_report_edit')? 'assessment_report' : $RecordType;
    }
}

$linterface = new interface_html();

$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];

### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR("Teacher");

# Retrieve student info
$student_obj = $li_pf->GET_STUDENT_OBJECT($default_student_id);

# Set links for photo
if(strpos($student_obj['PhotoLink'], $ec_iPortfolio['student_photo_no']) === false && $student_obj['PhotoLink'] != "")
	$student_obj['PhotoLink'] = str_replace("<!--ImageStyle-->", "", $student_obj['PhotoLink']);
else if($StudentList[$i][2] != "" && $StudentList[$i][3] != "" && $StudentList[$i][4] == 0)
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";
else
	$student_obj['PhotoLink'] = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg\" width=\"100\" height=\"130\" />";

if(!isset($RecordType))
{
	# Choose default record type according to user right
	if($sys_custom['LivingHomeopathy'] && $li_pf->HAS_RIGHT("activity")){
		$RecordType = "activity";
	}
	else if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")
		$RecordType = "activity";
	else if(!$sys_custom['iPf']['HKUGA_eDis_Tab'] && $li_pf->HAS_RIGHT("merit"))
		$RecordType = "merit";
	else if($li_pf->HAS_RIGHT("assessment_report"))
		$RecordType = "assessment_report";
	else if($li_pf->HAS_RIGHT("activity"))
		$RecordType = "activity";
	else if($li_pf->HAS_RIGHT("award"))
		$RecordType = "award";
	else if($li_pf->HAS_RIGHT("reading_records"))
		$RecordType = "reading_records";
	else if($li_pf->HAS_RIGHT("teacher_comment"))
		$RecordType = "teacher_comment";
	else if($li_pf->HAS_RIGHT("attendance"))
		$RecordType = "attendance";
	else if($li_pf->HAS_RIGHT("service"))
		$RecordType = "service";
	else if($li_pf->HAS_RIGHT("ole"))
		$RecordType = "ole";
	else if($sys_custom['iPf']['HKUGA_eDis_Tab'])
		$RecordType = "student_record";
}

# Generate menu tab
$availTab = libpf_tabmenu::getSchoolRecordTags($RecordType);
// [2017-1207-0959-51277]
if($sys_custom['iPf']['AcademicReportEdit'] && $RecordType == 'assessment_report_edit') {
    $availTab = libpf_tabmenu::getSchoolRecordTags('assessment_report');
}

// [2017-1207-0959-51277] hide all tabs (not assesssment report)
if($is_skip_activated_checking) {
    foreach($availTab as $thisKeyIndex => $thisTab) {
        unset($availTab[$thisKeyIndex]);
    }
}

//debug_pr($availTab);
// [2017-0403-1552-04240] Only allow Admin / HOY Group Member / Class Teacher to access Pastoral Record & Student Record Tab
if($sys_custom['iPf']['HKUGA_eDis_Tab'])
{
	foreach($availTab as $thisKeyIndex => $thisTab) {
		if($thisTab[1] == $eDiscipline['Good_Conduct_and_Misconduct'] || $thisTab[1] == $eDiscipline['Award_and_Punishment']) {
			if(!$li_pf->IS_IPF_SUPERADMIN() && !$li_pf->IS_HOY_GROUP_MEMBER_IPO() && !in_array($YearClassID, (array)$TeachingClasses)) {
				unset($availTab[$thisKeyIndex]);
			}
		}
	}
	$availTab = array_values((array)$availTab);
}

if($sys_custom['iPf']['HKUGA_eDis_Tab'])
	$split_tab_arr = array_chunk($availTab, 6);
else
	$split_tab_arr = array_chunk($availTab, 5);
$html_menu_tab = "";
for($i=0, $i_max=count($split_tab_arr); $i<$i_max; $i++)
{
    $html_menu_tab .= $li_pf->GET_TAB_MENU($split_tab_arr[$i]);
}

//echo $RecordType."<br/>";
$htmlResultTable = "";
$htmlNavArea = "";
$htmlNavStyle = "";

$page_size = ($page_size=='')? 100 : $page_size;
$numPerPage = ($numPerPage=='')? 100 : $numPerPage;
if ($page_size_change == 1)
{
     setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
     $ck_page_size = $numPerPage;
}

switch($RecordType)
{
	case "activity":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		$conds = ($ChooseYear!="") ? "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'" : "";
		
		// Chung Wing Kwong
		if($sys_custom['cwk_SLP'])
		{
		  $lpf_slp_cwk = new libpf_slp_cwk();
		  $tempTableActAbility = $lpf_slp_cwk->createActAbility_temp();
		  $tempTableActCriteria = $lpf_slp_cwk->createActCriteria_temp();
      
		  $li->field_array = array("a.ActivityName", "a.Year", "Role", "Performance");
      $sql = "SELECT a.ActivityName, a.Year, IF(a.Role='', '--', a.Role) AS Role, ";
      $sql .= "IF(t_aa.AbilityCode IS NULL, '--', AbilityCode), IF(t_ac.CriteriaCode IS NULL, '--', CriteriaCode), ";
      $sql .= "IF(a.Performance='', '--', a.Performance) AS Performance ";
      $sql .= "FROM {$eclass_db}.ACTIVITY_STUDENT as a ";
      $sql .= "LEFT JOIN {$tempTableActAbility} as t_aa ON t_aa.ActivityName = a.ActivityName ";
      $sql .= "LEFT JOIN {$tempTableActCriteria} as t_ac ON t_ac.ActivityName = a.ActivityName ";
      $sql .= "WHERE a.UserID = '$default_student_id' $conds";
      
    	# TABLE COLUMN
  		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
  		$li->column_list .= "<td width='20%'>".$li->column(1, $ec_iPortfolio['activity_name'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['year'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(3, $ec_iPortfolio['role'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>{$Lang["Cust_Cwk"]["CommonAbility"]}</td>\n";
  		$li->column_list .= "<td width='15%'>{$Lang["Cust_Cwk"]["MainCriteria"]}</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['performance'], 1)."</td>\n";
  		$li->column_array = array(0,0,0,0,0,0,0);
    }
    else if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role']!=""))
    {
    	$li->field_array = array("a.Year", "a.Semester", "a.ActivityName", "Role", "Performance", "ActivityDate", "Attendance");
    	$sql = "SELECT a.Year, a.Semester, a.ActivityName,  IF(a.Role='', '--', a.Role) AS Role, ";
    	$sql .= "IF(a.Performance='', '--', a.Performance) AS Performance, CONCAT('".$Lang['General']['From']." ', d.ActivityDateStart, '<br> ".$Lang['General']['To']." ', d.ActivityDateEnd) as ActivityDate, ";
    	$sql .= "(CASE ea.RecordStatus WHEN '1' THEN '".$Lang['StudentAttendance']['Present']."' WHEN '2' THEN '".$Lang['StudentAttendance']['Waived']."'  WHEN '3' THEN '".$i_Profile_Absent."'  WHEN '4' THEN '".$i_Profile_Late."' END ) as Attendance ";
    	$sql .= "FROM {$intranet_db}.PROFILE_STUDENT_ACTIVITY  as a ";
    	$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENTINFO  e ON e.EnrolEventID=a.eEnrolRecordID ";
		$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_ATTENDANCE ea ON ea.StudentID=a.UserID AND ea.EnrolEventID=e.EnrolEventID ";
		$sql .= "LEFT JOIN {$intranet_db}.INTRANET_ENROL_EVENT_DATE d ON ea.EventDateID=d.EventDateID ";
    	$sql .= "WHERE a.UserID = '$default_student_id' $conds";

    	# TABLE COLUMN
    	$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
    	$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['activity_name'], 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(5, $Lang['iPortfolio']['JinYing']['ImportColumns']['WITH-ACTIVITY']['ActivityDate'], 1)."</td>\n";
    	$li->column_list .= "<td width='15%'>".$li->column(6, $ec_iPortfolio['attendance'], 1)."</td>\n";
    	$li->column_array = array(0,0,0,0,0,0,0);
    	
    	$totalActivitiesSql = "SELECT COUNT(*) FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE StudentID='".$default_student_id."'";
    	$totalActivities = current($li->returnVector($totalActivitiesSql));
    	$attendanceSummarySql = "SELECT (CASE RecordStatus WHEN '1' THEN '".$Lang['StudentAttendance']['Present']."' WHEN '2' THEN '".$Lang['StudentAttendance']['Waived']."'  WHEN '3' THEN '".$i_Profile_Absent."'  WHEN '4' THEN '".$i_Profile_Late."' END ) as Attendance , COUNT(*) as days, CONCAT(ROUND((COUNT(*)/".$totalActivities.")*100,2),'%') as percent FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE StudentID='".$default_student_id."' GROUP BY RecordStatus";
    	$summary = $li->returnArray($attendanceSummarySql);
    	$summaryTable = "<p>";
    	foreach($summary as $s){
    		$summaryTable .= $s['Attendance'].": ".$s['days']." (".$s['percent'].")"." ";
    	}
    	$summaryTable .= "</p>";
    }
    else
    {
      $li->field_array = array("a.Year", "a.Semester", "a.ActivityName", "Role", "Performance", "Organization");
    	$sql = "SELECT a.Year, if(a.Semester = '' , '".$Lang['General']['WholeYear']."',a.Semester) as  'Semester', a.ActivityName, IF(a.Role='', '--', a.Role) AS Role, ";
    	$sql .= "IF(a.Performance='', '--', a.Performance) AS Performance, IF(a.Organization='', '--', a.Organization) AS Organization ";
    	$sql .= "FROM {$eclass_db}.ACTIVITY_STUDENT as a ";
      $sql .= "WHERE a.UserID = '$default_student_id' $conds";
      
    	# TABLE COLUMN
  		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
  		$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['activity_name'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(6, $i_ActivityOrganization, 1)."</td>\n";
  		$li->column_array = array(0,0,0,0,0,0,0);
    }

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['record'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = count($li->field_array)+1;
		//$li->noNumber = true;

		$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.ACTIVITY_STUDENT WHERE UserID = '$default_student_id' ORDER BY Year");
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table border='0' cellpadding='10' cellspacing='0' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		
		$htmlResultTable = $li->displayPlain();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
		
		if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role']!="")){
			$htmlNavArea .= $summaryTable;
		}	
		break;
	case "award":		
		if ($order=="") $order=0;
		if ($field=="") $field=0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		if($sys_custom['iPortfolioHideRemark']==true)
		{
			$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.Organization", "a.SubjectArea", "a.AwardFile");
			$li->no_col = 7;
			$li->column_array = array(0,0,0,0,0,0);
			$NameWidth = "30%";
		}
		else
		{
			$li->field_array = array("a.Year", "a.Semester", "a.AwardDate", "a.AwardName", "a.Organization", "a.SubjectArea", "a.Details", "a.AwardFile");
			$li->no_col = 8;
			$li->column_array = array(0,0,0,0,0,0,0);
			$RemarkField = "if(a.Remark!='', a.Remark, '--'), ";
			$NameWidth = "15%";
		}

		$conds = ($ChooseYear!="") ? "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'" : "";

		$sql = "SELECT a.Year, if(a.Semester = '' , '".$Lang['General']['WholeYear']."',a.Semester), IF (a.AwardDate,DATE_FORMAT(a.AwardDate,'%Y-%m-%d'),'--') As AwardDate, ";
		//$sql .= "CONCAT('<a class=navigation href=\"javascript:newWindow(\'award/award_detail.php?record_id=', a.RecordID,'\', 4)\">', a.AwardName, '</a>'), ";
		$sql .= "a.AwardName,";
		$sql .= "IF((a.Organization != '' AND a.Organization IS NOT NULL), a.Organization, '--'), ";
		$sql .= "IF ((a.SubjectArea != '' AND a.SubjectArea IS NOT NULL), a.SubjectArea, '--'), ";
		$sql .= $RemarkField;
		$sql .= "IF ((a.AwardFile!='' AND a.AwardFile IS NOT NULL), CONCAT('<a href=\"award/attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '--') ";
		$sql .= "FROM {$eclass_db}.AWARD_STUDENT as a ";
		$sql .= "WHERE a.UserID = '$default_student_id' AND a.RecordType = '1' $conds";

		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['award'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;

		$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.AWARD_STUDENT WHERE UserID = '$default_student_id' AND RecordType = '1' ORDER BY Year");
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		$pos = 0;
		//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column($pos++, "#", 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++,
		$ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['date'], 1)."</td>\n";
		$li->column_list .= "<td width='".$NameWidth."'>".$li->column($pos++, $ec_iPortfolio['award_name'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_AwardOrganization, 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_AwardSubjectArea, 1)."</td>\n";
		$li->column_list .= ($sys_custom['iPortfolioHideRemark']) ? "" : "<td width='15%'>".$li->column($pos++, $ec_iPortfolio['remark'], 1)."</td>\n";
		//$li->column_list .= "<td width='10%'>".$li->column($pos++, $ec_iPortfolio['upload_cert'], 1)."</td>\n";
		$htmlResultTable = $li->displayPlain();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
		break;
	case "teacher_comment":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		if ($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment']) {
			$li->field_array = array("a.Year", "a.Semester", "ClassAndNumber", "a.AcademicGradeChar", "a.ConductGradeChar", "a.CommentChi", "a.CommentEng", "a.ModifiedDate");
		}
		else {
			//$li->field_array = array("a.Year", "a.Semester", "ClassAndNumber", "a.ConductGradeChar", "a.CommentChi", "a.CommentEng", "a.ModifiedDate");
			$li->field_array[] = "a.Year";
			$li->field_array[] = "a.Semester";
			$li->field_array[] = "ClassAndNumber";
			$li->field_array[] = "a.ConductGradeChar";
			$li->field_array[] = "a.CommentChi";
			$li->field_array[] = "a.CommentEng";
			if ($sys_custom['iPf']['showPromotionInTeacherCommentPage']) {
				$li->field_array[] = "a.PromotionStatus";
				
				$promotionField = " if((a.PromotionStatus IS NULL OR a.PromotionStatus=''), '--', a.PromotionStatus) as PromotionStatus, ";
			}
			$li->field_array[] = "a.ModifiedDate";
		}
		
		$ClassNumberField = getClassNumberField("b.");
		
		// Eric Yip (20100720): Create temporary table for class history
    $sql = "CREATE TEMPORARY TABLE tempClassHistory (YearEN varchar(50), YearB5 varchar(50), ClassName varchar(50), ClassNumber varchar(50), PRIMARY KEY (YearEN)) DEFAULT CHARSET=utf8";
    $li->db_db_query($sql);
    $sql = "INSERT IGNORE INTO tempClassHistory (YearEN, YearB5, ClassName, ClassNumber) ";
    $sql .= "SELECT DISTINCT ay.YearNameEN, ay.YearNameB5, yc.ClassTitleEN, ycu.ClassNumber ";
    $sql .= "FROM {$intranet_db}.YEAR_CLASS_USER ycu ";
    $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID ";
    $sql .= "INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay ON ay.AcademicYearID = yc.AcademicYearID ";
    $sql .= "WHERE ycu.UserID = '$default_student_id'";
    $li->db_db_query($sql);
    $sql = "INSERT IGNORE INTO tempClassHistory (YearEN, YearB5, ClassName, ClassNumber) SELECT DISTINCT Year, Year, ClassName, ClassNumber FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD WHERE UserID = '$default_student_id'";
    $li->db_db_query($sql);

		$conds = ($ChooseYear!="") ? "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'" : "";
		$TermName = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN ');
		$sql =	"
							SELECT DISTINCT
								a.Year,
								if((a.YearTermID=0), '".$ec_iPortfolio['overall_comment']."', $TermName) as Semester,
								if(b.ClassName IS NULL, '--', CONCAT(b.ClassName, ' - ', $ClassNumberField)) AS ClassAndNumber,".
								($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] ? "a.AcademicGradeChar,":"")."
								a.ConductGradeChar,
								if((a.CommentChi IS NULL OR a.CommentChi=''), '--', a.CommentChi) as CommentChi,
								if((a.CommentEng IS NULL OR a.CommentEng=''), '--', a.CommentEng) as CommentEng,
								$promotionField
								a.ModifiedDate
							FROM {$eclass_db}.CONDUCT_STUDENT as a
							LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM as ayt ON (ayt.YearTermID = a.YearTermID)
							LEFT JOIN tempClassHistory as b ON (a.Year = b.YearEN OR a.Year = b.YearB5)
							WHERE
								a.UserID = '$default_student_id'
								$conds
							";
						
		// TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['teacher_comment'];
		$li->no_msg = $no_record_msg;
//		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
//		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->page_size = 100;
//		$li->no_col = $sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment'] ? 9: 8;
		//$li->noNumber = true;
		if ($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment']) {
			$li->no_col = 9;
		}
		else {
			$li->no_col = 8;
			if ($sys_custom['iPf']['showPromotionInTeacherCommentPage']) {
				$li->no_col++;
			}
		} 
		
		$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.CONDUCT_STUDENT WHERE UserID = '$default_student_id' ORDER BY Year");
		$pageSizeChangeEnabled = true;
		
		$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		//$li->column_list .= "<td class='tbheading' height='25' bgcolor='#CFE6FE' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(2, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column(3, $ec_iPortfolio['class_number'], 1)."</td>\n";
		if ($sys_custom['iPf']['SchoolRecord']['ImportWebsams']['ANPActivityComment']) {
			$li->column_list .= "<td width='10%'>".$li->column(4, $Lang['iPortfolio']['StudentAccount']['TeacherComment']['Academic'], 1)."</td>\n";
			$li->column_list .= "<td width='10%'>".$li->column(5, $ec_iPortfolio['conduct_grade'], 1)."</td>\n";
			$li->column_list .= "<td width='15%'>".$li->column(6, $ec_iPortfolio['comment_chi'], 1)."</td>\n";
			$li->column_list .= "<td width='15%'>".$li->column(7, $ec_iPortfolio['comment_eng'], 1)."</td>\n";
			$li->column_list .= "<td width='15%'>".$li->column(8, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
			$li->column_array = array(0,0,0,0,0,0,0,0,0);
		}
		else {
			$counter = 4;
			$li->column_list .= "<td width='10%'>".$li->column($counter++, $ec_iPortfolio['conduct_grade'], 1)."</td>\n";
			$li->column_list .= "<td width='20%'>".$li->column($counter++, $ec_iPortfolio['comment_chi'], 1)."</td>\n";
			$li->column_list .= "<td width='20%'>".$li->column($counter++, $ec_iPortfolio['comment_eng'], 1)."</td>\n";
			if ($sys_custom['iPf']['showPromotionInTeacherCommentPage']) {
				$li->column_list .= "<td width='15%'>".$li->column($counter++, $Lang['iPortfolio']['Promotion'], 1)."</td>\n";
			}
			$li->column_list .= "<td width='15%'>".$li->column($counter++, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
			$li->column_array = array(0,0,0,0,0,0,0,0,0);
		}

		$htmlResultTable = $li->displayPlain();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);

		break;
	case "service":
		if ($order=="") $order=1;
		if ($field=="") $field=0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		$li->field_array = array("a.Year", "a.Semester", "a.ServiceDate", "a.ServiceName", "a.Role", "a.Performance", "a.ModifiedDate");
		if($ChooseYear!="")
			$conds = "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'";

		$sql =	"
							SELECT
								a.Year,
								if(trim(a.Semester) = '','".$Lang['General']['WholeYear']."',a.Semester) as `Semester`,
								if(a.ServiceDate='0000-00-00', '--', a.ServiceDate),
								a.ServiceName,
								if(a.Role='', '--', a.Role),
								if(a.Performance='', '--', a.Performance),
								a.ModifiedDate FROM {$eclass_db}.SERVICE_STUDENT as a
							WHERE
								a.UserID = '$default_student_id'
								$conds
						";
		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $ec_iPortfolio['service'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 8;
//$li->noNumber = true;

		$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.SERVICE_STUDENT WHERE UserID = '$default_student_id' ORDER BY Year");
		$pageSizeChangeEnabled = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='10' cellspacing='0' class='table_b'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";

		# TABLE COLUMN
		$li->column_list .= "<td height='25' nowrap align='center'>#</span></td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(0, $ec_iPortfolio['year'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(1, $ec_iPortfolio['semester'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(2, $ec_iPortfolio['date'], 1)."</td>\n";
		$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(3, $ec_iPortfolio['service_name'], 1)."</td>\n";
		$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(4, $ec_iPortfolio['role'], 1)."</td>\n";
		$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(5, $ec_iPortfolio['performance'], 1)."</td>\n";
		$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(6, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
		$li->column_array = array(0,0,0,0,0,0,0,0);

		$htmlResultTable = $li->displayPlain();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
		break;
	case "assessment_report":
		$displayBy = ($displayBy=="") ? "Score" : $displayBy;

		$link_score = ($displayBy=="Score") ? $ec_iPortfolio['display_by_score'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Score')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";
		$link_grade = ($displayBy=="Grade") ? $ec_iPortfolio['display_by_grade'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Grade')\" class='link_a'>".$ec_iPortfolio['display_by_grade']."</a>";
		$link_score_grade = ($displayBy=="ScoreGrade") ? $ec_iPortfolio['display_by_score_grade'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('ScoreGrade')\" class='link_a'>".$ec_iPortfolio['display_by_score_grade']."</a>";
		$link_rank = ($displayBy=="Rank") ? $ec_iPortfolio['display_by_rank'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Rank')\" class='link_a'>".$ec_iPortfolio['display_by_rank']."</a>";
		$link_stand_score = ($displayBy=="StandardScore") ? $ec_iPortfolio['display_by_stand_score'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('StandardScore')\" class='link_a'>".$ec_iPortfolio['display_by_stand_score']."</a>";

		//$htmlResultTable = $li_pf->generateAssessmentReport3($default_student_id, $ClassName, $displayBy, "", "", "", "", "", true);
		//$htmlResultTable .= "<br />";
		
		// Show Score = 0 if Grade is empty
		$exceptEmptyGrade = $sys_custom['ipf']['Mgmt']['SchoolRecordShowZero_TLCDMC'];

		// [2020-0612-1004-37207] Show Score if Score > 0 + No Grade + No Ranking
        $alwaysDisplayScoreIfValid = $sys_custom['ipf']['Mgmt']['SchoolRecordDisplayScore_EvenWithoutGradeAndRanking'];

		/******************************************************************************************/
		/******************** New Method to display academic records ******************************/
		/******************************************************************************************/
    // Select correct field (with correct format) to insert to tempAssessmentMark
    switch($displayBy)
    {
      case "Score":
      default:
        // If Score is 0 and grade is empty, show '--'
        //$retrieveField = "IF(Score <= 0, IF(Grade = '' OR Grade IS NULL, '--', Score), TRIM('.' FROM TRIM(0 FROM ROUND(Score, 1))))";
        $retrieveField = "CASE ";
        $retrieveField .= "WHEN Score < 0 THEN '--' ";
        $retrieveField .= "WHEN (Score = 0 AND OrderMeritForm > 0) THEN '0' ";	//2012-0224-1652-13073
        if(!$exceptEmptyGrade) {
        	$retrieveField .= "WHEN (ABS(Score) = 0) THEN IF(IFNULL(Grade, '') = '', '--', 0) ";
        } else {
        	$retrieveField .= "WHEN (ABS(Score) = 0) THEN Score ";
        }
        //$retrieveField .= "ELSE TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))) ";
        $retrieveField .= "ELSE Score ";
        $retrieveField .= "END";
        $retrieveFieldSubj = $retrieveField;
        if($sys_custom['iPf']['hideAcademicResultParentSubjectScore'])
        {
            $retrieveFieldSubj = "CASE ";
            $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
            $retrieveFieldSubj .= "WHEN Score < 0 THEN '--' ";
            $retrieveFieldSubj .= "WHEN (Score = 0 AND OrderMeritForm > 0) THEN '0' ";
            $retrieveFieldSubj .= "WHEN (ABS(Score) = 0) THEN IF(IFNULL(Grade, '') = '', '--', 0) ";
            $retrieveFieldSubj .= "ELSE Score ";
            $retrieveFieldSubj .= "END";
        }
        $totalTitle = $ec_iPortfolio['overall_score'];
       	
       	//$htmlNavArea = "<span>{$link_score}</span>  | {$link_grade} | {$link_score_grade} | {$link_rank} | {$link_stand_score}";
       	$htmlNavArea = "<span>{$link_score}</span>  | {$link_grade} | {$link_score_grade} | {$link_rank}";
       	if (!$sys_custom['iPf']['hideAcademicResultDisplaySDScore']) {
       		$htmlNavArea .= " | {$link_stand_score}";
       	}
        break;
      case "Grade":
        $retrieveField = "IF(IFNULL(Grade, '') = '', '--', Grade)";
        $retrieveFieldSubj = $retrieveField;
        if($sys_custom['iPf']['hideAcademicResultParentSubjectScore'])
        {
            $retrieveFieldSubj = "CASE ";
            $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
            $retrieveFieldSubj .= "WHEN IFNULL(Grade, '') = '' THEN '--' ";
            $retrieveFieldSubj .= "ELSE Grade ";
            $retrieveFieldSubj .= "END";
        }
        $totalTitle = $ec_iPortfolio['overall_grade'];
        
        //$htmlNavArea = "{$link_score}  | <span>{$link_grade}</span> | {$link_score_grade} | {$link_rank} | {$link_stand_score}";
        $htmlNavArea = "{$link_score}  | <span>{$link_grade}</span> | {$link_score_grade} | {$link_rank}";
        if (!$sys_custom['iPf']['hideAcademicResultDisplaySDScore']) {
       		$htmlNavArea .= " | {$link_stand_score}";
       	}
        break;
      case "ScoreGrade":
      	$exceptEmptyGrade = $sys_custom['ipf']['Mgmt']['SchoolRecordShowZero_TLCDMC'];
        //$retrieveField = "CONCAT(IF(Score <= 0 AND (Grade = '' OR Grade IS NULL), '--', Score), ' (', IF(Grade IS NULL OR Grade = '', '--', Grade), ')')";
        $retrieveField = "CASE ";
        $retrieveField .= "WHEN Score < 0 THEN CONCAT('-- (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
        $retrieveField .= "WHEN (Score = 0 AND OrderMeritForm > 0) THEN CONCAT('0 (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";	//2012-0224-1652-13073
        if(!$exceptEmptyGrade) {
        	$retrieveField .= "WHEN (ABS(Score) = 0) THEN CONCAT(IF(IFNULL(Grade, '') = '', '--', 0), ' (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
        } else {
        	$retrieveField .= "WHEN (ABS(Score) = 0) THEN CONCAT(Score, ' (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
        }
        // [DM#3794] Handle empty grade display
        //$retrieveField .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))), '(', Grade, ')') ";
        $retrieveField .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))), '(', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
        $retrieveField .= "END";
        $retrieveFieldSubj = $retrieveField;
        if($sys_custom['iPf']['hideAcademicResultParentSubjectScore'])
        {
            $retrieveFieldSubj = "CASE ";
            $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
            $retrieveFieldSubj .= "WHEN Score < 0 THEN CONCAT('-- (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
            $retrieveFieldSubj .= "WHEN (Score = 0 AND OrderMeritForm > 0) THEN CONCAT('0 (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
            $retrieveFieldSubj .= "WHEN (ABS(Score) = 0) THEN CONCAT(IF(IFNULL(Grade, '') = '', '--', 0), ' (', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
            // [DM#3794] Handle empty grade display
            //$retrieveField .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))), '(', Grade, ')') ";
            $retrieveFieldSubj .= "ELSE CONCAT(TRIM('.' FROM TRIM(0 FROM ROUND(Score, 2))), '(', IF(IFNULL(Grade, '') = '', '--', Grade), ')') ";
            $retrieveFieldSubj .= "END";
        }
        $totalTitle = $ec_iPortfolio['overall_score_grade'];
        
        //$htmlNavArea = "{$link_score}  | {$link_grade} | <span>{$link_score_grade}</span> | {$link_rank} | {$link_stand_score}";
        $htmlNavArea = "{$link_score}  | {$link_grade} | <span>{$link_score_grade}</span> | {$link_rank}";
        if (!$sys_custom['iPf']['hideAcademicResultDisplaySDScore']) {
       		$htmlNavArea .= " | {$link_stand_score}";
       	}
        break;
      case "Rank":
        $retrieveField = "IF(IFNULL(OrderMeritForm, 0) = 0 Or OrderMeritForm = -1, '--', OrderMeritForm)";
        $retrieveFieldSubj = $retrieveField;
        if($sys_custom['iPf']['hideAcademicResultParentSubjectScore'])
        {
            $retrieveFieldSubj = "CASE ";
            $retrieveFieldSubj .= "WHEN (SubjectComponentID = 0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL) THEN '--' ";
            $retrieveFieldSubj .= "WHEN IFNULL(OrderMeritForm, 0) = 0 OR OrderMeritForm = -1 THEN '--' ";
            $retrieveFieldSubj .= "ELSE OrderMeritForm ";
            $retrieveFieldSubj .= "END";
        }
        $totalTitle = $ec_iPortfolio['overall_rank'];
        
        $htmlNavArea = "{$link_score}  | {$link_grade} | {$link_score_grade} | <span>{$link_rank}</span>";
        if (!$sys_custom['iPf']['hideAcademicResultDisplaySDScore']) {
       		$htmlNavArea .= " | {$link_stand_score}";
       	}
        break;
      case "StandardScore":
        $totalTitle = $ec_iPortfolio['overall_stand_score'];
        $htmlNavArea = "{$link_score}  | {$link_grade} | {$link_score_grade} | {$link_rank} | <span>{$link_stand_score}</span>";
        break;      
    }
    
    //since accessment Nav Area with a speical layout
	$htmlNavStyle = "align=\"right\" valign=\"middle\" class=\"thumb_list\"";

    if ($sys_custom['iPf']['showAcademicResultWithAssessment'])
    {
    	$htmlResultTable .= $li_pf->getStudentAcademicResultDisplayWithAssessment($default_student_id, $displayBy);
    }
    else
    {
    	// Records of class history
	    $classHistoryArr = $li_pf->getAssessmentClassHistoryArr($default_student_id);
	    
	    // Prepare for:
	    // i) table header (year, class, semester)
	    // ii) partial query strings of temporary table
	    $yearClassStr = "";
	    $semStr = "";
	    $semCount = 0;
        for($i=0, $i_max=count($classHistoryArr); $i<$i_max; $i++)
        {
			  $_ayID = $classHistoryArr[$i]["AcademicYearID"];
			  $_lay = new academic_year($_ayID);
			  $_year = $_lay->Get_Academic_Year_Name();
			  $_year = empty($_year) ? $classHistoryArr[$i]["Year"] : $_year;
			  $_ytID = $classHistoryArr[$i]["YearTermID"];
			  $_layt = new academic_year_term($_ytID);
			  $_sem = $_layt->Get_Year_Term_Name();
			  $_sem = empty($_sem) ? $classHistoryArr[$i]["Semester"] : $_sem;
			  $_className = $classHistoryArr[$i]["ClassName"];
			  
			  $ayArr[$_ayID] = array("yearname"=>$_year, "classname"=>$_className);
			  $semArr[$_ayID][$_ytID] = $_sem;
	    }
	    if(is_array($ayArr))
	    {
	      $insertAssessTableSqlArr = array();
	      foreach($ayArr AS $ayID => $ayDetailArr)
	      {
	        $_yearName = $ayDetailArr["yearname"];
	        $_className = $ayDetailArr["classname"];
	      
	        $yearClassStr .= "<td width='15%' colspan='".count($semArr[$ayID])."' nowrap='nowrap' align='center'>{$_yearName}<br />{$_className}</td>\n";
	        if(is_array($semArr))
	        {
	          $subjOverallRes = (array_key_exists(0, $semArr[$ayID]) || array_key_exists("", $semArr[$ayID]));
	          
	          foreach($semArr[$ayID] AS $ytID => $_ytName)
	          {
	            if($ytID == 0 || $ytID == "") continue;   // "" => annual result, put to last row of the year
	            
	            $semStr .= "<td nowrap='nowrap' align='center'>{$_ytName}</td>\n";
	            $assessTableField .= ", Score_{$ayID}_{$ytID} varchar(16)";
	            
	            // Special handling for standard score since it needs system calculation
	            if($displayBy == "StandardScore")
	            {
	              // Subject records
	              //$insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $default_student_id, $ayID, $ytID));
	              $hideParentSubjectDisplay = $sys_custom['iPf']['hideAcademicResultParentSubjectScore'];
	              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IF(SubjectComponentID=0 Or SubjectComponentID = '' Or SubjectComponentID is null , null, SubjectComponentID)", $default_student_id, $ayID, $ytID, $hideParentSubjectDisplay));
	              // Overall record
	              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_MAIN_RECORD", "", "", "", $default_student_id, $ayID, $ytID));
	              
	              $selectAssessTableSql .= ", IF(t_am.Score_{$ayID}_{$ytID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID}_{$ytID} - t_aam.Score_{$ayID}_{$ytID})/t_asdm.Score_{$ayID}_{$ytID}, 2), '--'))";
	            }
	            else
	            {
	            	
	              // Subject records
	              //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, $ytID);
                  //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL, NULL, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, $ytID, $exceptEmptyGrade);
	              $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL, NULL, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, $ytID, $exceptEmptyGrade, $alwaysDisplayScoreIfValid);
	              // Overall record
                  //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, $ytID, $exceptEmptyGrade);
	              $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, $ytID, $exceptEmptyGrade, $alwaysDisplayScoreIfValid);
	              
	              $selectAssessTableSql .= ", IFNULL(t_am.Score_{$ayID}_{$ytID}, '--')";
	            }
	          }
	          
	          // If overall result exists
	          if($subjOverallRes)
	          {
	            $semStr .= "<td nowrap='nowrap' align='center'>{$ec_iPortfolio['overall_result']}</td>\n";
	            $assessTableField .= ", Score_{$ayID} varchar(16)";
	          
	            // Special handling for standard score since it needs system calculation
	            if($displayBy == "StandardScore")
	            {
	              // Subject records
	              //$insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $default_student_id, $ayID, ""));
	              $hideParentSubjectDisplay = $sys_custom['iPf']['hideAcademicResultParentSubjectScore'];
	              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IF(SubjectComponentID=0 Or SubjectComponentID = '' Or SubjectComponentID is null , null, SubjectComponentID)", $default_student_id, $ayID, "", $hideParentSubjectDisplay));
	              // Overall record
	              $insertAssessTableSqlArr = array_merge($insertAssessTableSqlArr, $li_pf->buildInsertTempAssessmentTableSQL_SD("ASSESSMENT_STUDENT_MAIN_RECORD", "", "", "", $default_student_id, $ayID, ""));
	              
	              $selectAssessTableSql .= ", IF(t_am.Score_{$ayID} = '--', '--', IFNULL(ROUND((t_am.Score_{$ayID} - t_aam.Score_{$ayID})/t_asdm.Score_{$ayID}, 2), '--'))";
	            }
	            else
	            {
	              // Subject records
	              //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectCode", "IFNULL(SubjectComponentCode, '')", $retrieveField, $default_student_id, $ayID, "");
                  //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 Or SubjectComponentID = '' Or SubjectComponentID is null , null, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, "", $exceptEmptyGrade);
                  $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 OR SubjectComponentID = '' OR SubjectComponentID IS NULL , NULL, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, "", $exceptEmptyGrade, $alwaysDisplayScoreIfValid);
	              // Overall record
	              //$insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, "");
                  $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, "", false, $alwaysDisplayScoreIfValid);
	              
	              $selectAssessTableSql .= ", IFNULL(t_am.Score_{$ayID}, '--')";
	            }
	          }
	        }
	        $semCount += count($semArr[$ayID]);
	      }
	    }
	    
			// table object
			if ($order=="") $order=1;
			if ($field=="") $field=0;
			$li = new libpf_dbtable($field, $order, $pageNo);
			
			// Temp table of assessment records
			// Table name : tempAssessmentMark (tempAssessmentAvgMark, tempAssessmentSDMark)
			if($displayBy == "StandardScore")
			{
			  $li_pf->createTempAssessmentSDScoreTable($li, $assessTableField, $insertAssessTableSqlArr);
			  $table_sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
		      $table_sql .= $selectAssessTableSql." ";
		      $table_sql .= "FROM tempAssessmentMark t_am ";
		      //$table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
		      $table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCodeID ";
		      $table_sql .= "LEFT JOIN tempAssessmentAvgMark t_aam ON t_am.SubjectCode = t_aam.SubjectCode AND t_am.SubjectComponentCode = t_aam.SubjectComponentCode ";
		      $table_sql .= "LEFT JOIN tempAssessmentSDMark t_asdm ON t_am.SubjectCode = t_asdm.SubjectCode AND t_am.SubjectComponentCode = t_asdm.SubjectComponentCode ";
		    }
		    // Table name : tempAssessmentMark
		    else
		    {
		    	$li_pf->createTempAssessmentMarkTable($li, $assessTableField, $insertAssessTableSqlArr);
				$table_sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
		      	$table_sql .= $selectAssessTableSql." ";
		      	$table_sql .= "FROM tempAssessmentMark t_am ";
		      	//$table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0, t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND t_am.SubjectComponentCode = t_as.CompSubjCode ";
		      	//$table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0 AND t_am.SubjectCodeID != '', t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND ( IF(t_am.SubjectComponentCode = '##Overall##' or (t_am.SubjectComponentCode != '' and t_am.SubjectComponentCode != 0 and t_am.SubjectComponentCode is not null), t_am.SubjectComponentCode = t_as.CompSubjCode, t_as.CompSubjCode = '')) ";
		      	$table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0 AND t_am.SubjectCodeID != '', t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND ( IF(t_am.SubjectComponentCode = '##Overall##' or (t_am.SubjectComponentCode != '' and t_am.SubjectComponentCode != 0 and t_am.SubjectComponentCode is not null), t_am.SubjectComponentCode = t_as.CompSubjCodeID, t_as.CompSubjCode = '')) ";
			}
			
			// Temp table of subjects
			// Table name : tempAssessmentSubject
			$li_pf->createTempAssessmentSubjectTable($li);
			$sql = "INSERT IGNORE INTO tempAssessmentSubject (MainSubjCode, CompSubjCode, SubjName, MainSubjOrder, CompSubjOrder) ";
		    $sql .= "VALUES ('##Overall##', '##Overall##', '{$totalTitle}', 9999, 9999)";   // 9999 => a large number, ensure it is at the end
		    $li->db_db_query($sql);
		    
		    
		    ### if the parent subject has no mark but the component subjects has mark, insert a dummy record in the mark record so that the parent subject can be displayed in the UI
		    // get all the main subjects from the subject table which has no subject mark
		    $sql = "select MainSubjCodeID, MainSubjCode from tempAssessmentSubject group by MainSubjCodeID";
		    $MainSubjectArr = $li->returnResultSet($sql);
		    $MainSubjectIDArr = Get_Array_By_Key($MainSubjectArr, 'MainSubjCodeID');
		    
		    // get main subjects which has subject mark
		    $sql = "select SubjectCodeID from tempAssessmentMark where SubjectComponentCode = '' group by SubjectCodeID";
		    $MainSubjectWithMark = $li->returnResultSet($sql);
		    $MainSubjectWithMarkIDArr = Get_Array_By_Key($MainSubjectWithMark, 'SubjectCodeID');
		    
		    // get main subjects which has no subject mark
		    $MainSubjectWithoutMarkIDArr = array_diff($MainSubjectIDArr, $MainSubjectWithMarkIDArr);
		    
		    // get all parent subjects from the subject table which component subject has mark
		    $sql = "Select t_as.MainSubjCodeID, assr.SubjectComponentCode, assr.Score, assr.Grade
					From tempAssessmentSubject as t_as inner join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr on (t_as.MainSubjCode = assr.SubjectCode AND t_as.CompSubjCode = assr.SubjectComponentCode)
					Where t_as.CompSubjOrder != 0 and assr.UserID = '".$default_student_id."' and assr.AcademicYearID = '".$ayID."' And ((assr.OrderMeritForm != -1 And assr.OrderMeritForm != 0) Or assr.OrderMeritForm Is Null Or assr.Grade != '')
					Group By t_as.MainSubjCodeID";
			$sql = "Select t_as.MainSubjCodeID, assr.SubjectComponentCode, assr.Score, assr.Grade
					From tempAssessmentSubject as t_as inner join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr on (t_as.MainSubjCode = assr.SubjectID AND t_as.CompSubjCode = assr.SubjectComponentID)
					Where t_as.CompSubjOrder != 0 and assr.UserID = '".$default_student_id."' And ((assr.OrderMeritForm != -1 And assr.OrderMeritForm != 0) Or assr.OrderMeritForm Is Null Or assr.Grade != '')
					Group By t_as.MainSubjCodeID";
		    $MainSubjectWithCmpMarkArr = $li->returnResultSet($sql);
		    $MainSubjectWithCmpMarkIDArr = Get_Array_By_Key($MainSubjectWithCmpMarkArr, 'MainSubjCodeID');
		    
		    for ($i=0, $i_max=count($MainSubjectArr); $i<$i_max; $i++) {
		    	$_mainSubjectId = $MainSubjectArr[$i]['MainSubjCodeID'];
		    	$_mainSubjectCode = $MainSubjectArr[$i]['MainSubjCode'];
		    	
		    	// subject has no mark itself but it's components have mark
		    	if (in_array($_mainSubjectId, $MainSubjectWithoutMarkIDArr) && in_array($_mainSubjectId, $MainSubjectWithCmpMarkIDArr)) {
		    		//$sql = "insert into tempAssessmentMark (SubjectCodeID, SubjectCode) values ('".$_mainSubjectId."', '".$_mainSubjectCode."')";
		    		$sql = "insert into tempAssessmentMark (SubjectCodeID, SubjectCode) values ('".$_mainSubjectId."', '".$_mainSubjectId."')";
		    		$li->db_db_query($sql);
		    	}
		    }
		    
		    // [2017-0901-1527-45265]
		    if($sys_custom['iPf']['hideAcademicResultComponentDisplay'])
		    {
		        $sql = "select RecordID from {$intranet_db}.ASSESSMENT_SUBJECT where CMP_CODEID != '' ";
		        if(!empty($sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry'])) {
		            $sql .= " and CMP_CODEID IN ('".implode("','", (array)$sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry'])."') ";
		        }
		        $hideSubjectIds = $li->returnVector($sql);
		        
		        $sql = "delete from tempAssessmentSubject where CompSubjCodeID > 0 ";
		        if(!empty($sys_custom['iPf']['hideAcademicResultDisplayCmpCodeAry']) && !empty($hideSubjectIds)) {
		            $sql .= " and CompSubjCodeID not in ('".implode("', '", $hideSubjectIds)."') ";
		        }
		        $li->db_db_query($sql);
		    }
		    
	//	    debug_pr('tempAssessmentSubject');
    //	    debug_pr($li->returnResultSet('select * from tempAssessmentSubject'));
	//	    debug_pr('tempAssessmentMark');
	//	    debug_pr($li->returnResultSet('select * from tempAssessmentMark'));
	
		    
			// TABLE INFO
			$li->field_array = array("t_as.MainSubjOrder", "t_as.CompSubjOrder");
			$li->sql = $table_sql;
			$li->db = $intranet_db;
			$li->title = $ec_iPortfolio['teacher_comment'];
			$li->no_msg = $no_record_msg;
			$li->page_size = 999;     // Large number to prevent page change 
			$li->no_col = 3 + $semCount;
			$li->noNumber = true;
			$li->fieldorder2 = ", t_as.CompSubjOrder";
			
	//		debug_pr($li->built_sql());
	//		debug_pr($li->returnResultSet($li->built_sql()));
			
			$li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='1' class='table_b'>";
			$li->row_alt = array("#FFFFFF", "F3F3F3");
			//$li->row_height = 20;
			$li->sort_link_style = "class='tbheading'";
	
			# TABLE COLUMN
			$li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
			$li->column_list .= $yearClassStr;
			$li->column_list .= "</tr>";
			$li->column_list .= "<tr class='tabletop'>";
			$li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
	    	$li->column_list .= $semStr;
	
			//$li->column_array = array(0,3,3,3,3,0,0,0);
			$li->column_array = array(0,0,0);
			if($semCount > 0)
			{
		  		$li->column_array = array_merge($li->column_array, array_fill(1, $semCount, 3));
		  	}
			
			$htmlResultTable .= $li->displayAssessmentReport($displayBy);
		
			$supportFloatHeader = true;
        }
        
        if($sys_custom['iPf']['AcademicReportEdit'] && $li_pf->IS_IPF_SUPERADMIN() && $li->total_row > 0)
        {
            $edit_button_row = '';
            $edit_button_row .= '<tr><td align="center">';
                $edit_button_row .= '<br/>'.$linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', "jCHANGE_RECORD_TYPE('assessment_report_edit')");
            $edit_button_row .= '</td></tr>';
        }
        
		break;
	case "ole":
		# Prepare data
		$pageSizeChangeEnabled = true;
		$lpf_slp = new libpf_slp();
		$ELEArray = $lpf_slp->GET_ELE();
		$ELECodeArray = array_keys($ELEArray);
		$ELECount = count($ELECodeArray);

//Generate oleSelection Box Array
		foreach($ELEArray as $ELECode => $ELETitle){
// 			$a = str_replace(array("[", "]"), "", $ELECode);
			$oleArray[][1] = $ELETitle;
		}
		for($i=0; $i<count($oleArray); $i++)
		{
			$oleArray[$i][0]=str_replace(array("[", "]"), "", $ELECodeArray[$i]);
		}
		if (count($ELECodeArray)>0)
		{
			$ELEImplode = implode(",",$ELECodeArray);
		}
// 		debug_pr($search_text);
// 		$ELECount = count($ELECodeArray);
		
// 		debug_pr( $oleArray );
		$CategoryField = $lpf_slp->GET_OLR_Category_Field_For_Record("a.", true);
		$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";
		
		# Prepare table
		if ($order=="") $order=0;
		if ($field=="") $field=1;
		$li = new libpf_dbtable($field, $order, $pageNo);
		
		//three filter condition
		$conds = ($ChooseYear == "") ? "" : " AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($ChooseYear)."'";
		$ChooseOLE = $li->Get_Safe_Sql_Like_Query($ChooseOLE);
		$conds2 = ($ChooseOLE == "") ? "" : " AND c.ELE LIKE '%$ChooseOLE%'";
		$search_text = $li->Get_Safe_Sql_Like_Query($search_text);
		$conds3 = ($search_text == "") ? "" : " AND c.Title LIKE '%$search_text%'";
		if($sys_custom['LivingHomeopathy'] && $ChooseCategory != ''){
			$conds2 .= " AND c.Category='$ChooseCategory' ";
		}
		
		if($sys_custom['cwk_SLP'])
		{
		  $li->field_array = array("c.Title","YearName", "a.Role","a.Achievement", "a.Hours");
		
		  $lpf_slp_cwk = new libpf_slp_cwk();
		  $tempTableProgramAbility = $lpf_slp_cwk->createProgramAbility_temp();
		  
  		$sql = "SELECT '{$ELECount}', '{$ELEImplode}', c.Title, ".Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN")." as YearName, ";
  		$sql .= "c.ELE, a.Role, t_pa.AbilityCode, a.Achievement, a.Hours ";
  		$sql .= "FROM {$eclass_db}.OLE_STUDENT as a ";
  		//$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID ";
  		$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM as c ON a.ProgramID = c.ProgramID ";
  		$sql .= "LEFT JOIN {$intranet_db}.ACADEMIC_YEAR as ay ON c.AcademicYearID = ay.AcademicYearID ";
  		$sql .= "LEFT JOIN {$tempTableProgramAbility} as t_pa ON c.ProgramID = t_pa.ProgramID ";
  		$sql .= "WHERE a.UserID = '$default_student_id' AND a.RecordStatus IN (2,4) AND c.IntExt = 'INT' $conds $conds2 $conds3";
  		
  		# TABLE COLUMN
  		$li->column_list .= "<tr class='tabletop'>\n";
  		
  		$li->column_list .= "<td height='25' align='center' rowspan='2' >#</span></td>\n";
  		$li->column_list .= "<td nowrap='nowrap' rowspan='2' width='100' >".$li->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
  		$li->column_list .= "<td rowspan='2' >".$li->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
  		$li->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
  		$li->column_list .= "<td rowspan='2' align='center'>".$li->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
  		$li->column_list .= "<td rowspan='2' align='center'>{$Lang["Cust_Cwk"]["CommonAbility"]}</td>\n";
  		$li->column_list .= "<td rowspan='2' align='center'>".$li->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
  		$li->column_list .= "<td rowspan='2' align='center' nowrap='nowrap'>".$li->column(4,$ec_iPortfolio['hours'], 1)."</td>\n";
  		$li->column_list .= "</tr>\n";
		}
    else
    {
      $li->field_array = array("a.Title","OLEDate", "a.Role","a.Achievement","a.Category", "a.Hours");
    
  		$sql = "SELECT '{$ELECount}', '{$ELEImplode}', c.Title, ";
  		$sql .= "IF ((c.StartDate IS NULL OR c.StartDate='0000-00-00'),'--',IF(c.EndDate IS NULL OR c.EndDate='0000-00-00',c.StartDate,CONCAT(DATE_FORMAT(c.StartDate, '%Y-%m-%d'),'<br />$profiles_to<br />',DATE_FORMAT(c.EndDate, '%Y-%m-%d')))) as OLEDate, ";
  		$sql .= "c.ELE, a.Role, a.Achievement, $SqlCategoryField a.Hours ";
  		$sql .= "FROM {$eclass_db}.OLE_STUDENT as a ";
  		//$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID ";
  		$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM as c ON a.ProgramID = c.ProgramID ";
      $sql .= "LEFT JOIN {$intranet_db}.ACADEMIC_YEAR as ay ON c.AcademicYearID = ay.AcademicYearID ";
      $sql .= "WHERE a.UserID = '$default_student_id' AND a.RecordStatus IN (2,4) AND c.IntExt = 'INT' $conds $conds2 $conds3";
  		# TABLE COLUMN
  		$li->column_list .= "<tr class='tabletop'>\n";
  		
  		if ($ELECount > 0) {
  			$rowspan = "rowspan='2'";
  		}
  		
  		$li->column_list .= "<td height='25' align='center' $rowspan >#</span></td>\n";
  		$li->column_list .= "<td nowrap='nowrap' $rowspan width='100' >".$li->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
  		$li->column_list .= "<td $rowspan >".$li->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
  		if ($ELECount > 0) {
  			$li->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
  		}
  		$li->column_list .= "<td $rowspan align='center'>".$li->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
  		$li->column_list .= "<td $rowspan align='center'>".$li->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
  		$li->column_list .= "<td $rowspan align='center' nowrap='nowrap'>".$li->column(4,$ec_iPortfolio['category'], 1)."</td>\n";
  		$li->column_list .= "<td $rowspan align='center' nowrap='nowrap'>".$li->column(5,$ec_iPortfolio['hours'], 1)."</td>\n";
  		$li->column_list .= "</tr>\n";
    }
    
		# TABLE INFO
		$li->sql = $sql;
// 		debug_pr($sql);
		$li->db = $intranet_db;
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 10;
//$li->noNumber = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		$li->IsColOff = "iPortfolioOLEPool";
		$li->additionalCols = count($ELECodeArray);
		
		
		if (count($ELECodeArray) > 0)
		{
			$li->column_list .= "<tr class=\"tabletop\">";
// 			for ($i=0; $i<count($ELECodeArray);$i++){
// 				$ELEDisplay = str_replace(array("[", "]"), "", $ELECodeArray[$i]);
// 				$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
// 				$li->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
// 			}
			foreach($ELEArray as $ELECode => $ELETitle)
			{
			
				$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
				$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
				$li->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
				
			}
		
			$li->column_list .= "</tr>";				
		}
		
		$li->column_array = array(10,10,0,10,0,0,10,0);

		ob_start();
		$li->display();
		$htmlResultTable = ob_get_contents(); // assign buffer contents to variable
		ob_end_clean();
		
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
    if ($li->navigationHTML!="")
    {
      $htmlResultTable .= "<tr class='tablebottom'>";
      $htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
      $htmlResultTable .= "</tr>";
    }
    $htmlResultTable .= "</table>";
    
    // Eric Yip (20100525): Get all year from school settings instead of years having records
		//$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
    $lay = new academic_year();
    $ay_arr = $lay->Get_All_Year_List();
    //generate filter box
    $htmlNavArea = getSelectByArray($ay_arr, "name='ChooseYear' onChange='document.form1.submit()'", $ChooseYear, 1, 0, $iPort["all_school_years"], 2);
	$htmlNavArea .= getSelectByArray($oleArray, "name='ChooseOLE' onChange='document.form1.submit()'", $ChooseOLE, 0, 0, $ec_iPortfolio['all_ele']);
	if($sys_custom['LivingHomeopathy']){
		$htmlNavArea .= $lpf_slp->GET_OLE_CATEGORY_SELECTION("name='ChooseCategory' onChange='document.form1.submit()'", $ChooseCategory, 1);
	}
	$htmlNavArea .= "<div class=\"Conntent_search\"><input name=\"search_text\" id=\"search_text\" type=\"text\" value=\"{$search_text}\" onkeypress=\"PressEnterEvent(event)\"/></div>";
		break;
	case "plkp":
		# Prepare data
		$pageSizeChangeEnabled = true;
		
		$lpf_slp = new libpf_slp();
		
		$CategoryField = $lpf_slp->GET_OLR_Category_Field_For_Record("a.", true);
		$SqlCategoryField = ($CategoryField == "") ? "'-'" : "$CategoryField";
	
		# Prepare table
		if ($order=="") $order=0;
		if ($field=="") $field=1;
		$li = new libpf_dbtable($field, $order, $pageNo);
		$li->field_array = array("a.Title", "OLEDate", "a.Role","a.Achievement","a.Category");
		
		$conds = ($ChooseYear == "") ? "" : " AND c.AcademicYearID = '".$li->Get_Safe_Sql_Query($ChooseYear)."'";

		$sql = "SELECT '{$ELECount}', '{$ELEImplode}', c.Title, IF ((c.StartDate IS NULL OR c.StartDate='0000-00-00'),'--',IF(c.EndDate IS NULL OR c.EndDate='0000-00-00',c.StartDate,CONCAT(DATE_FORMAT(c.StartDate, '%Y-%m-%d'),'<br />$profiles_to<br />',DATE_FORMAT(c.EndDate, '%Y-%m-%d')))) as OLEDate, ";
		$sql .= "c.ELE, a.Role, a.Achievement, {$SqlCategoryField} ";
		$sql .= "FROM {$eclass_db}.OLE_STUDENT as a ";
		//$sql .= "LEFT JOIN {$intranet_db}.INTRANET_USER as b ON a.ApprovedBy = b.UserID ";
		$sql .= "LEFT JOIN {$eclass_db}.OLE_PROGRAM as c ON a.ProgramID = c.ProgramID ";
    $sql .= "LEFT JOIN {$intranet_db}.ACADEMIC_YEAR as ay ON c.AcademicYearID = ay.AcademicYearID ";
    $sql .= "WHERE a.UserID = '$default_student_id' AND a.RecordStatus IN (2,4) AND c.IntExt = 'EXT' $conds";
    
		# TABLE INFO
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = 9;
//$li->noNumber = true;

		$li->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		$li->IsColOff = "iPortfolioOLEPool";

		# TABLE COLUMN
		$li->column_list .= "<tr class='tabletop'>\n";
		
		$li->column_list .= "<td height='25' align='center' >#</span></td>\n";
		$li->column_list .= "<td nowrap='nowrap' width='100' >".$li->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
		$li->column_list .= "<td >".$li->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
		$li->column_list .= "<td align='center'>".$li->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
		$li->column_list .= "<td align='center'>".$li->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
		$li->column_list .= "<td align='center' nowrap='nowrap'>".$li->column(4,$ec_iPortfolio['category'], 1)."</td>\n";
		$li->column_list .= "</tr>\n";
		
		$li->column_array = array(10,10,0,10,0,0,10,0);

		ob_start();
		$li->display();
		$htmlResultTable = ob_get_contents(); // assign buffer contents to variable
		ob_end_clean();
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
    if ($li->navigationHTML!="")
    {
      $htmlResultTable .= "<tr class='tablebottom'>";
      $htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
      $htmlResultTable .= "</tr>";
    }
    $htmlResultTable .= "</table>";
    
    // Eric Yip (20100525): Get all year from school settings instead of years having records
		//$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
    $lay = new academic_year();
    $ay_arr = $lay->Get_All_Year_List();
    $htmlNavArea = getSelectByArray($ay_arr, "name='ChooseYear' onChange='document.form1.submit()'", $ChooseYear, 1, 0, $iPort["all_school_years"], 2);

		break;

	case "attendance":
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear, 1);
		/*if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role']!="")){
			$htmlResultTable = $li_pf->displayStudentActivityAttendanceSummary($default_student_id, $ClassName,$ChooseYear, true);
		}else{
			$htmlResultTable = $li_pf->displayStudentAttendanceSummary($default_student_id, $ClassName,$ChooseYear, true);
		}*/
		$htmlResultTable = $li_pf->displayStudentAttendanceSummary($default_student_id, $ClassName,$ChooseYear, true);
		break;

	########### Other reports start
	case "other_reports":
		if ($intranet_session_language == "en") {
			$sql_UserName = "u.EnglishName";
			$sql_ClassName = "yc.ClassTitleEN";
			
		}
		else {
			$sql_UserName = "u.ChineseName";
			$sql_ClassName = "yc.ClassTitleB5";
		}
		
		$currentAcademicYear = Get_Current_Academic_Year_ID();
		
		$sql = "SELECT  r.ReportTitle,
						CONCAT('<a href=\"javascript: view_report(\'',i.ReportItemID,'\',\'',i.IsDir,'\')\" class=\"tablelink\">',r.ReportTitle,'_',$sql_UserName,'_',$sql_ClassName,'_',ycu.ClassNumber,'</a>')
						AS ReportFile,
						i.DateInput
				FROM    {$eclass_db}.PORTFOLIO_OTHER_REPORT r
				INNER JOIN {$eclass_db}.PORTFOLIO_OTHER_REPORT_ITEM i
					ON i.OtherReportID=r.OtherReportID
				INNER JOIN {$intranet_db}.INTRANET_USER u
					ON  u.UserID=i.StudentID
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu 
					ON  ycu.UserID=u.UserID
				INNER JOIN {$intranet_db}.YEAR_CLASS yc
					ON 	yc.YearClassID=ycu.YearClassID
					AND yc.AcademicYearID='$currentAcademicYear' 
				WHERE 	i.StudentID='".IntegerSafe($StudentID)."'";
				
		$li = new libpf_dbtable($field, $order, $pageNo);
		$li->field_array = array("ReportTitle", "ReportFile", "DateInput");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->table_tag = "<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "#F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 25;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		
		// TABLE COLUMN
		$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$Lang['iPortfolio']['OtherReports']['Title'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$Lang['iPortfolio']['OtherReports']['ReportFile'])."</td>\n";
		$li->column_list .= "<td width='25%' nowrap='nowrap'>".$li->column(2,$Lang['iPortfolio']['OtherReports']['CreationDate'])."</td>\n";
		$li->column_array = array(0,0,0);

		$htmlResultTable = $li->displayPlain();
		
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	    if ($li->navigationHTML!="")
	    {
	      $htmlResultTable .= "<tr class='tablebottom'>";
	      $htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
	      $htmlResultTable .= "</tr>";
	    }
	    $htmlResultTable .= "</table>";
		
		break;
	########### Other reports end

	case "merit":
	default:
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear, 0);
		$htmlResultTable = $li_pf->displayStudentMeritSummary($default_student_id, $ClassName,$ChooseYear, true);
	break;
	
	case "assessment":
		if ($intranet_session_language == "en") {
			$sql_YearName = "y.YearNameEN";
		}
		else {
			$sql_YearName = "y.YearNameB5";
		}
		$lay = new academic_year();
		$ay_arr = $lay->Get_All_Year_List();
		$htmlNavArea = getSelectByArray($ay_arr, "name='ChooseYear' onChange='document.form1.submit()'", $ChooseYear, 1, 0, $iPort["all_school_years"], 2);
		$sql = "SELECT {$sql_YearName},AssessmentTitle,
				CONCAT('<a target=\"_blank\" href=\"/home/portfolio/teacher/management/assessment_report/assessment_file_manage.php?action=readFile&assessmentId=',arsr.AssessmentID,'&studentId=',arsr.UserID,'\">".$ec_iPortfolio['view_analysis']."</a>') as Report 
				FROM {$eclass_db}.ASSESSMENT_REPORT_STUDENT_RECORD arsr 
				LEFT JOIN {$eclass_db}.ASSESSMENT_REPORT ar ON arsr.AssessmentID=ar.RecordID
				LEFT JOIN {$intranet_db}.ACADEMIC_YEAR y ON ar.AcademicYearID = y.AcademicYearID
				WHERE arsr.UserID='".$default_student_id."'";
		$li = new libpf_dbtable($field, $order, $pageNo);
		$sql .= ($ChooseYear!="")?" AND ar.AcademicYearID='".$li->Get_Safe_Sql_Query($ChooseYear)."'":"";
		$li->field_array = array($sql_YearName,"AssessmentTitle", "Report");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->table_tag = "<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "#F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 25;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		
		// TABLE COLUMN
		$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$Lang['General']['AcademicYear'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$iPort['Assessment']['Title'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(2,$iPort['Assessment']['AssessmentFile'])."</td>\n";
		$li->column_array = array(0,0,0);
		
		$htmlResultTable = $li->displayPlain();
		
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
		if ($li->navigationHTML!="")
		{
			$htmlResultTable .= "<tr class='tablebottom'>";
			$htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
			$htmlResultTable .= "</tr>";
		}
		$htmlResultTable .= "</table>";
	break;
	case "third_party":
		$sql = "Select tpr.Title, tpr.Description, tprr.Result, tprr.Remark 
				From {$eclass_db}.THIRD_PARTY_REPORT_RECORD tprr 
				INNER JOIN {$eclass_db}.THIRD_PARTY_REPORT tpr 
				ON tprr.ReportID=tpr.RecordID
				WHERE tprr.UserID='".$default_student_id."'";

		$li = new libpf_dbtable($field, $order, $pageNo);
		$li->field_array = array("Title","Description", "Result", "Remark");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->table_tag = "<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "#F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 25;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
	
		// TABLE COLUMN
		$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$ec_iPortfolio['activity_name'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$Lang['iPortfolio']['OtherReports']['Description'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(2,$Lang['iPortfolio']['ThirdParty']['Result'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(3,$ec_iPortfolio['remark'])."</td>\n";
		$li->column_array = array(0,0,0,0);
	
		$htmlResultTable = $li->displayPlain();
	
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
		if ($li->navigationHTML!="")
		{
			$htmlResultTable .= "<tr class='tablebottom'>";
			$htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
			$htmlResultTable .= "</tr>";
		}
		$htmlResultTable .= "</table>";
		break;
	case "pl2_quiz_report":
		include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
		include_once($PATH_WRT_ROOT."includes/json.php");
		include_once($PATH_WRT_ROOT."includes/libeclass40.php");
		$eclassApiAuth = new libeclassapiauth();
		$json = new JSON_obj();
		$li = new libpf_dbtable($field, $order, $pageNo);
		
		// Get eClass Token & Course ID
		$sql = "SELECT c.course_id FROM {$eclass_db}.user_course uc
		RIGHT JOIN {$eclass_db}.course c ON uc.course_id=c.course_id AND intranet_user_id='".IntegerSafe($_SESSION['UserID'])."'
WHERE course_code='NCS'";
		$course_id = ($sys_custom['ncs_dev'])?"24":current($li->returnVector($sql));
		
		$sql = "SELECT SessionKey FROM INTRANET_USER WHERE UserID='".IntegerSafe($UserID)."'";
		$apiKeys = $eclassApiAuth->GetAPIKeyList();
		
		$getTokenRequestVar = array(
				"eClassRequest"=>array(
						"APIKey"=>$apiKeys[2]["APIKey"],
						"SessionID"=>($sys_custom['ncs_dev'])?"8a092306c043e9d0cdf467e4dde49c6e":current($li->returnVector($sql)),
						"RequestMethod"=>"LoginCourses"
				),
				"Request"=>null
		);
		$postFieldStr = $json->encode($getTokenRequestVar);
		$ch = curl_init();
		$eclassApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080/":$intranet_rel_path;
		curl_setopt($ch, CURLOPT_URL, $eclassApiUrl."webserviceapi/?reqtype=json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFieldStr);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($postFieldStr)
		)
				);
		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curlData=curl_exec($ch);
		curl_close($ch);
		
		$tokenList = $json->decode($curlData);
		$AuthToken = $tokenList['Result'][$course_id];

		// Get List of Lessons to investigate
		$pl2_header = array();
		$pl2_header[] = 'Content-Type: application/json';
		$pl2_header[] = 'X-CourseId: '.$course_id;
		$pl2_header[] = 'X-AuthToken: '.$AuthToken;
		$pl2_header[] = 'Accept:  application/json';
		
		$sql = "SELECT user_id FROM ".classNamingDB((($sys_custom['ncs_dev'])?"881":$course_id)).".usermaster WHERE intranet_user_id='".IntegerSafe($default_student_id)."'";
		$class_user_id = current($li->returnVector($sql));
		
		//if(!$sys_custom['ncs_dev']){
		$lessonApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
		$ch = curl_init();
		$url = $lessonApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/user/".$class_user_id."/quizActivities";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
		$resultData=curl_exec($ch);
		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//}else{
		//	$lessonData = '{"2":"Henry Test","6":"Thomas testing","22":"SW 1221","39":"SW 1221 - copy","40":"quiz","41":"Test 170120","44":"PL Testing 170309","45":"PL Testing 170309 - copy","46":"Ronald Testing","47":"Testing 20170314","48":"","49":"","50":"Siuwan Test 170317","51":"Henry Test 20170329","52":"","53":"","54":"quiz - copy","55":"","56":"Ronald Testing - copy","57":"","58":"Henry Test 20170329 - copy","59":"Thomas testing - copy","60":"SW 1221 - copy","61":"Siuwan Test 170317 - copy","62":"PL Testing 170309 - copy - copy","63":"PL Testing 170309 - copy","64":"Testing 20170314 - copy","65":"Henry Test - copy","66":"Thomas testing - copy - copy","67":"Henry Test - copy","68":"","69":"Ronald Testing1111","70":"Test 170120 - copy","71":"SW 1221 - copy - copy","72":"SW 1221 - copy","73":"SW 1221 - copy - copy","74":"Henry Test 20170407","75":"Henry FC Test 20170410","76":"Henry Test Power Presenter","77":"","78":"Thomas 201704210950","79":"Henry Test 20170421","80":"Henry Test PowerPresenter & PowerPad","81":"","82":"Henry Test 20170508","83":"","84":"Henry Test 20170516","85":"","86":"Henry Test 20170602","87":"","88":"","89":"Henry Test 20170609","90":"","91":"Thomas","92":"Henry Test 20160619","93":"Henry Copy Test 20160619","95":"Henry Copy Test 20160619 - copy","96":"Henry test powerpad","97":"","98":"Discussion 20170626"}';
		//}
				
		$resultList = $json->decode($resultData);
		
		$sql = "CREATE TEMPORARY TABLE quiz_result_PL2_report_std(
					quiz_id int(11) NOT NULL,
					lesson_id int(11) default NULL,
					quiz_name varchar(255) default NULL,
					score int(3) default NULL,
					fullMarks int(3) default NULL
				)ENGINE = InnoDB  CHARACTER SET=utf8";
		$li->db_db_query($sql);
		if($responseCode!='404' && !isset($resultList['error'])){
			foreach($resultList as $lid=>$result){
				foreach($result as $qid=>$re){
					if($re['title']==""){
						$re['title'] = $Lang['PL2_Report']['Untitled'];
					}
					$sql = "INSERT INTO quiz_result_PL2_report_std VALUES('".$qid."','".$lid."','".$re['title']."','".$re['score']."','".$re['fullMarks']."')";
					$li->db_db_query($sql);
				}
			}
		}
		//if(!$sys_custom['ncs_dev']){
		$lessonApiUrl = ($sys_custom['ncs_dev'])?"http://192.168.0.172:8080":$website;
		$ch = curl_init();
		$url = $lessonApiUrl."/eclass40/src/powerlesson/api/ncsStatistics/lessons";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $pl2_header);
		$lessonData=curl_exec($ch);
		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//}else{
		//	$lessonData = '{"2":"Henry Test","6":"Thomas testing","22":"SW 1221","39":"SW 1221 - copy","40":"quiz","41":"Test 170120","44":"PL Testing 170309","45":"PL Testing 170309 - copy","46":"Ronald Testing","47":"Testing 20170314","48":"","49":"","50":"Siuwan Test 170317","51":"Henry Test 20170329","52":"","53":"","54":"quiz - copy","55":"","56":"Ronald Testing - copy","57":"","58":"Henry Test 20170329 - copy","59":"Thomas testing - copy","60":"SW 1221 - copy","61":"Siuwan Test 170317 - copy","62":"PL Testing 170309 - copy - copy","63":"PL Testing 170309 - copy","64":"Testing 20170314 - copy","65":"Henry Test - copy","66":"Thomas testing - copy - copy","67":"Henry Test - copy","68":"","69":"Ronald Testing1111","70":"Test 170120 - copy","71":"SW 1221 - copy - copy","72":"SW 1221 - copy","73":"SW 1221 - copy - copy","74":"Henry Test 20170407","75":"Henry FC Test 20170410","76":"Henry Test Power Presenter","77":"","78":"Thomas 201704210950","79":"Henry Test 20170421","80":"Henry Test PowerPresenter & PowerPad","81":"","82":"Henry Test 20170508","83":"","84":"Henry Test 20170516","85":"","86":"Henry Test 20170602","87":"","88":"","89":"Henry Test 20170609","90":"","91":"Thomas","92":"Henry Test 20160619","93":"Henry Copy Test 20160619","95":"Henry Copy Test 20160619 - copy","96":"Henry test powerpad","97":"","98":"Discussion 20170626"}';
		//}
		$lessonList = $json->decode($lessonData);
		$sql = "CREATE TEMPORARY TABLE quiz_result_PL2_report_lesson(
					lesson_id int(11) NOT NULL,
					lesson_name varchar(255) default NULL
				)ENGINE = InnoDB  CHARACTER SET=utf8";
		$li->db_db_query($sql);
		if(!empty($lessonList) && !isset($lessonList['error'])){
			foreach($lessonList as $lid=>$lname){
				if($lname==""){
					$lname = $Lang['PL2_Report']['Untitled'];
				}
				$sql = "INSERT INTO quiz_result_PL2_report_lesson VALUES('".$lid."','".$lname."')";
				$li->db_db_query($sql);
			}
		}
		
		$sql = "SELECT l.lesson_name, r.quiz_name, CONCAT(r.score,'/',r.fullMarks) as result FROM quiz_result_PL2_report_std r INNER JOIN quiz_result_PL2_report_lesson l ON r.lesson_id=l.lesson_id";

		$li->field_array = array("lesson_name","quiz_name", "result");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->table_tag = "<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "#F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 25;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		
		// TABLE COLUMN
		$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$Lang['PL2_Report']['Lesson'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$Lang['PL2_Report']['Quiz'])."</td>\n";
		$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(2,$Lang['iPortfolio']['ThirdParty']['Result'].$Lang['PL2_Report']['Remark'])."</td>\n";
		$li->column_array = array(0,0,0);
		
		$htmlResultTable = $li->displayPlain();
		
		$htmlResultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
		if ($li->navigationHTML!="")
		{
			$htmlResultTable .= "<tr class='tablebottom'>";
			$htmlResultTable .= "<td class=\"tabletext\" align=\"right\">".$li->navigation(1)."</td>";
			$htmlResultTable .= "</tr>";
		}
		$htmlResultTable .= "</table>";
		break;
	case "homework":
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		
		if ($page_size_change == 1)
		{
		    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		    $ck_page_size = $numPerPage;
		}
		
		if ($order=="") $order=0;
		if ($field=="") $field=3;
		
		$AcademicYearID = !isset($AcademicYearID)? Get_Current_Academic_Year_ID() : $AcademicYearID;
		
		$fcm = new form_class_manage();
		
		$academic_years = $fcm->Get_All_Academic_Year();
		
		$academic_year_selection = getSelectByArray($academic_years, ' name="AcademicYearID" id="AcademicYearID" onchange="document.form1.submit();" ', $AcademicYearID, $___all=1, $___noFirst=0, $___FirstTitle=$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'], $___ParQuoteValue=1);
		
		$li = new libdbtable2007($field, $order, $pageNo);
		
		$li->field_array = array("Subject","SubjectGroup","hw.Title","hw.StartDate","hw.DueDate","Workload","Poster","hl.StudentDocument","hl.TeacherDocument","Rating");
		
		$conds = "";
		if($AcademicYearID != ''){
			$conds .= " AND hw.AcademicYearID='".$li->Get_Safe_Sql_Query($AcademicYearID)."' ";
		}
		$sql = "SELECT  
					".Get_Lang_Selection("sbj.CH_DES","sbj.EN_DES")." as Subject,
					".Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as SubjectGroup,
					hw.Title,
					hw.StartDate, 
					hw.DueDate,
					CONCAT(hw.Loading/2,' ".$Lang['SysMgr']['Homework']['Hours']."') as Workload,
					".getNameFieldByLang2("u1.")." as Poster,
					hl.StudentDocument,
					hl.TeacherDocument,
					hl.TextScore as Rating,
					hl.StudentDocumentUploadTime,
					hl.TeacherDocumentUploadTime,
					".getNameFieldByLang2("u2.")." as TeacherDocumentUploaderName  
				FROM INTRANET_HOMEWORK as hw  
				INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST as hl ON hl.HomeworkID=hw.HomeworkID 
				LEFT JOIN ASSESSMENT_SUBJECT as sbj ON sbj.RecordID = hw.SubjectID 
				LEFT JOIN SUBJECT_TERM_CLASS as stc ON stc.SubjectGroupID = hw.ClassGroupID
				LEFT JOIN SUBJECT_TERM_CLASS_USER AS stcu ON stcu.SubjectGroupID = hw.ClassGroupID 
				LEFT JOIN INTRANET_USER as u1 ON u1.UserID=hw.PosterUserID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=hl.TeacherDocumentUploadedBy 
				WHERE hl.StudentID='".$default_student_id."' $conds
				GROUP BY hw.HomeworkID ";
		
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->title = $Lang['ePayment']['HomeworkRecords'];
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		$li->no_col = count($li->field_array)+1;
		
		$li->table_tag = "<table border='0' cellpadding='10' cellspacing='0' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		
		$col = 0;	
		$li->column_list .= "<td class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
		$li->column_list .= "<td width='13%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Subject'], 1)."</td>\n";
		$li->column_list .= "<td width='13%'>".$li->column($col++, $Lang['SysMgr']['Homework']['SubjectGroup'], 1)."</td>\n";
		$li->column_list .= "<td width='15%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Topic'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['StartDate'], 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['DueDate'], 1)."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")", 1)."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column($col++, $Lang['SysMgr']['Homework']['Poster'], 1)."</td>\n";
		$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['HandedInHomework'], 1)."</td>\n";
		$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['Mark'], 1)."</td>\n";
		$li->column_list .= "<td width='11%'>".$li->column($col++, $Lang['eHomework']['Rating'], 1)."</td>\n";
		$li->column_array = array_fill(0,$col,0);
		
		
		function handedinhomework_formatter($student_document, $row)
		{
			global $file_path, $Lang;
			if($student_document == '') return '';
			$file_name = substr($student_document,strrpos($student_document,'/')+1);
			$download_url = getEncryptedText($file_path.'/file/homework_document/'.$student_document);
			$html = '<a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($file_name).'</a>';
			$html.= '<div class="tabletextremark">('.str_replace('<!--DATETIME-->',$row['StudentDocumentUploadTime'],$Lang['eHomework']['SubmittedTimeRemark']).')</div>';
			return $html;
		};
		
		function marking_formatter($teacher_document, $row)
		{
			global $file_path, $Lang;
			if($teacher_document == '') return '';
			$file_name = substr($teacher_document,strrpos($teacher_document,'/')+1);
			$download_url = getEncryptedText($file_path.'/file/homework_document/'.$teacher_document);
			$html = '<a href="/home/download_attachment.php?target_e='.$download_url.'">'.intranet_htmlspecialchars($file_name).'</a>';
			$html.= '<div class="tabletextremark">('.str_replace('<!--NAME-->',$row['TeacherDocumentUploaderName'],str_replace('<!--DATETIME-->',$row['TeacherDocumentUploadTime'],$Lang['eHomework']['MarkedRemark'])).')</div>';
			return $html;
		};
		
		$li->column_formatter = array();
		$li->column_formatter[7] = handedinhomework_formatter;
		$li->column_formatter[8] = marking_formatter;
		
		ob_start();
		$li->displayPlain();
		if ($li->navigationHTML!="") {
			echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
		                <tr class="tablebottom">
		                  <td class="tabletext" align="right">'.$li->navigation(1).'</td>
		                </tr>
					</table>';
		}
		$htmlResultTable = ob_get_contents();
		ob_end_clean();
		$htmlNavArea = $academic_year_selection;
	
	break;
	
	case "student_record":
		if ($order=="") $order = 1;
		if ($field=="") $field = 0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		
		$conds = $ChooseYear!=""? "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'" : "";
    	$sql = "SELECT 
					a.Year,
					IF(a.Semester = '', '".$Lang['General']['WholeYear']."', a.Semester) as Semester,
					DATE_FORMAT(a.AccuDate,'%Y-%m-%d') as AccuDate,
					a.Reason,
					IF(a.ApplyHOY = '1' AND ".($li_pf->IS_HOY_GROUP_MEMBER_IPO()? "0" : "1").", '".$Lang['eDiscipline']['DetailsReferToHOY']."', a.Remark) as Remark,
					a.ModifiedDate
				FROM
					{$eclass_db}.STUDENT_RECORD_STUDENT as a
				WHERE 
					a.UserID = '$default_student_id'
					$conds";
		
      	# Table Column
  		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
  		$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['date'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['reason'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['remark'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(6, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
		
		# Table Info
      	$li->field_array = array("Year", "Semester", "AccuDate", "Reason", "Remark", "ModifiedDate");
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->no_col = count($li->field_array) + 1;
		$li->title = $ec_iPortfolio['record'];
  		$li->column_array = array(0,0,0,0,0,0,0);
  		$li->wrap_array = array(0,0,0,0,0,0,0);
		$li->IsColOff = 2;
		$li->fieldorder2 = ", a.AccuDate, a.Reason";
  		
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		
		//$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.ACTIVITY_STUDENT WHERE UserID = '$default_student_id' ORDER BY Year");
		$pageSizeChangeEnabled = true;
		
		$li->table_tag = "<table border='0' cellpadding='10' cellspacing='0' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		
		$htmlResultTable = $li->display();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName, $ChooseYear);
	break;
	
	case "pastoral_record":
		if ($order=="") $order = 1;
		if ($field=="") $field = 0;
		$li = new libpf_dbtable($field, $order, $pageNo);
		
		$conds = $ChooseYear!=""? "AND a.Year = '".$li->Get_Safe_Sql_Query($ChooseYear)."'" : "";
    	$sql = "SELECT 
					a.Year,
					IF(a.Semester = '', '".$Lang['General']['WholeYear']."', a.Semester) as Semester,
					DATE_FORMAT(a.MeritDate, '%Y-%m-%d') as MeritDate,
					a.Reason,
					a.Remark,
					a.ModifiedDate
				FROM
					{$eclass_db}.PASTORAL_RECORD_STUDENT as a
				WHERE 
					a.UserID = '$default_student_id'
					$conds";
      	
    	# Table Column
  		$li->column_list .= "<td height='25' nowrap align='center'>".$li->column(0, "#", 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(1, $ec_iPortfolio['year'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(2, $ec_iPortfolio['term'], 1)."</td>\n";
  		$li->column_list .= "<td width='20%'>".$li->column(3, $ec_iPortfolio['date'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(4, $ec_iPortfolio['reason'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(5, $ec_iPortfolio['remark'], 1)."</td>\n";
  		$li->column_list .= "<td width='15%'>".$li->column(6, $ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
		
		# TABLE INFO
      	$li->field_array = array("Year", "Semester", "MeritDate", "Reason", "Remark", "ModifiedDate");
		$li->sql = $sql;
		$li->db = $intranet_db;
		$li->no_col = count($li->field_array)+1;
		$li->title = $ec_iPortfolio['record'];
  		$li->column_array = array(0,0,0,0,0,0,0);
  		$li->wrap_array = array(0,0,0,0,0,0,0);
  		$li->IsColOff = 2;
		$li->fieldorder2 = ", a.MeritDate, a.Reason";
		
		$li->no_msg = $no_record_msg;
		$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
		if ($page_size_change!="") $li->page_size = $numPerPage;
		
		//$ActivityYearArr = $li_pf->returnArray("SELECT DISTINCT Year FROM {$eclass_db}.ACTIVITY_STUDENT WHERE UserID = '$default_student_id' ORDER BY Year");
		$pageSizeChangeEnabled = true;
		
		$li->table_tag = "<table border='0' cellpadding='10' cellspacing='0' width='100%'>";
//		$li->row_alt = array("#FFFFFF", "F3F3F3");
        $li->row_alt = array("", "");
		$li->row_height = 20;
		$li->sort_link_style = "class='tbheading'";
		
		$htmlResultTable = $li->display();
		$htmlNavArea = $li_pf->displayYearSelectBox($default_student_id, $ClassName, $ChooseYear);
	break;
	
	case "reading_records":
		include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
		include_once($PATH_WRT_ROOT."includes/libelibrary.php");
		$libelibrary = new elibrary();
		$academic_year_list = $libelibrary->getYearClassList($default_student_id);
	
		$htmlNavArea = getSelectByArray($academic_year_list, $tag=' name="ChooseYear" class=formtextbox onChange="document.form1.submit();"', $ChooseYear, $all=0, $noFirst=0, $FirstTitle=$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'], $ParQuoteValue=1);
		$htmlResultTable = $libelibrary->getStudentReadingSummaryUI($default_student_id, $ChooseYear);
	
	break;
	
	case "assessment_report_edit":
	    if($sys_custom['iPf']['AcademicReportEdit'] && $li_pf->IS_IPF_SUPERADMIN())
	    {
    	    $displayBy = ($displayBy=="") ? "Score" : $displayBy;
    	    $link_score = ($displayBy=="Score" || $displayBy=="ScoreGrade" || $displayBy=="StandardScore") ? $ec_iPortfolio['display_by_score'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Score')\" class='link_a'>".$ec_iPortfolio['display_by_score']."</a>";
    	    $link_grade = ($displayBy=="Grade") ? $ec_iPortfolio['display_by_grade'] : "<a href=\"javascript:jCHANGE_DISPLAY_TYPE('Grade')\" class='link_a'>".$ec_iPortfolio['display_by_grade']."</a>";
    	    
    	    // Select correct field (with correct format) to insert to tempAssessmentMark
    	    switch($displayBy)
    	    {
    	        case "Score":
    	        default:
    	            // If Score is 0 and grade is empty, show '--'
    	            $retrieveField = "CASE ";
    	            $retrieveField .= "WHEN Score < 0 THEN '' ";
    	            $retrieveField .= "WHEN (Score = 0 And OrderMeritForm > 0) THEN '0' ";
    	            $retrieveField .= "WHEN (ABS(Score) = 0) THEN IF(IFNULL(Grade, '') = '', '', 0) ";
    	            $retrieveField .= "ELSE Score ";
    	            $retrieveField .= "END";
    	            $retrieveFieldSubj = $retrieveField;
    	            
    	            $totalTitle = $ec_iPortfolio['overall_score'];
    	            
    	            $htmlNavArea = "<span>{$link_score}</span>  | {$link_grade}";
    	            break;
    	            
    	        case "Grade":
    	            $retrieveField = "IFNULL(Grade, '')";
    	            $retrieveFieldSubj = $retrieveField;
    	            
    	            $totalTitle = $ec_iPortfolio['overall_grade'];
    	            
    	            $htmlNavArea = "{$link_score}  | <span>{$link_grade}</span>";
    	            break;
    	    }
    	    
    	    //since accessment Nav Area with a speical layout
    	    $htmlNavStyle = "align=\"right\" valign=\"middle\" class=\"thumb_list\"";
    	    
    	    if ($sys_custom['iPf']['showAcademicResultWithAssessment'])
    	    {
    	        $htmlResultTable .= $li_pf->getStudentAcademicResultDisplayWithAssessment($default_student_id, $displayBy);
    	    }
    	    else
    	    {
    	        // Records of class history
    	        $classHistoryArr = $li_pf->getAssessmentClassHistoryArr($default_student_id);
    	        
    	        // Prepare for:
    	        // i) table header (year, class, semester)
    	        // ii) partial query strings of temporary table
    	        $yearClassStr = "";
    	        $semStr = "";
    	        $semCount = 0;
    	        for($i=0, $i_max=count($classHistoryArr); $i<$i_max; $i++)
    	        {
    	            $_ayID = $classHistoryArr[$i]["AcademicYearID"];
    	            $_lay = new academic_year($_ayID);
    	            $_year = $_lay->Get_Academic_Year_Name();
    	            $_year = empty($_year) ? $classHistoryArr[$i]["Year"] : $_year;
    	            
    	            $_ytID = $classHistoryArr[$i]["YearTermID"];
    	            $_layt = new academic_year_term($_ytID);
    	            $_sem = $_layt->Get_Year_Term_Name();
    	            $_sem = empty($_sem) ? $classHistoryArr[$i]["Semester"] : $_sem;
    	            
    	            $_className = $classHistoryArr[$i]["ClassName"];
    	            
    	            $ayArr[$_ayID] = array("yearname"=>$_year, "classname"=>$_className);
    	            $semArr[$_ayID][$_ytID] = $_sem;
    	        }
    	        if(is_array($ayArr))
    	        {
    	            $insertAssessTableSqlArr = array();
    	            foreach($ayArr AS $ayID => $ayDetailArr)
    	            {
    	                $_yearName = $ayDetailArr["yearname"];
    	                $_className = $ayDetailArr["classname"];
    	                $yearClassStr .= "<td width='15%' colspan='".count($semArr[$ayID])."' nowrap='nowrap' align='center'>{$_yearName}<br />{$_className}</td>\n";
    	                
    	                if(is_array($semArr))
    	                {
    	                    $subjOverallRes = (array_key_exists(0, $semArr[$ayID]) || array_key_exists("", $semArr[$ayID]));
    	                    
    	                    foreach($semArr[$ayID] AS $ytID => $_ytName)
    	                    {
    	                        if($ytID == 0 || $ytID == "") continue;        // "" => annual result, put to last row of the year
    	                        
    	                        $semStr .= "<td nowrap='nowrap' align='center'>{$_ytName}</td>\n";
    	                        $assessTableField .= ", Score_{$ayID}_{$ytID} varchar(16)";
	                            
	                            // Subject records
	                            $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 Or SubjectComponentID = '' Or SubjectComponentID is null , null, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, $ytID);
	                            
	                            // Overall record
	                            $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, $ytID);
	                            
	                            $selectAssessTableSql .= ", IFNULL(t_am.Score_{$ayID}_{$ytID}, '')";
    	                    }
    	                    
    	                    // If overall result exists
    	                    if($subjOverallRes)
    	                    {
    	                        $semStr .= "<td nowrap='nowrap' align='center'>{$ec_iPortfolio['overall_result']}</td>\n";
    	                        $assessTableField .= ", Score_{$ayID} varchar(16)";
    	                        
	                            // Subject records
	                            $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_SUBJECT_RECORD", "SubjectID", "SubjectID", "IF(SubjectComponentID=0 Or SubjectComponentID = '' Or SubjectComponentID is null , null, SubjectComponentID)", $retrieveFieldSubj, $default_student_id, $ayID, "");

	                            // Overall record
	                            $insertAssessTableSqlArr[] = $li_pf->buildInsertTempAssessmentTableSQL("ASSESSMENT_STUDENT_MAIN_RECORD", "", "'##Overall##'", "'##Overall##'", $retrieveField, $default_student_id, $ayID, "");
	                            
	                            $selectAssessTableSql .= ", IFNULL(t_am.Score_{$ayID}, '')";
    	                    }
    	                }
    	                
    	                $semCount += count($semArr[$ayID]);
    	            }
    	        }
    	        
    	        // table object
    	        if ($order=="") $order=1;
    	        if ($field=="") $field=0;
    	        $li = new libpf_dbtable($field, $order, $pageNo);
    	        
    	        // Temp table of assessment records
    	        // Table name : tempAssessmentMark
	            $li_pf->createTempAssessmentMarkTable($li, $assessTableField, $insertAssessTableSqlArr);
	            $table_sql = "SELECT DISTINCT t_as.MainSubjCode, t_as.CompSubjCode, t_as.SubjName ";
	            $table_sql .= $selectAssessTableSql.", t_as.MainSubjCodeID, t_as.CompSubjCodeID ";
	            $table_sql .= "FROM tempAssessmentMark t_am ";
	            $table_sql .= "INNER JOIN tempAssessmentSubject t_as ON IF(t_am.SubjectCodeID IS NOT NULL AND t_am.SubjectCodeID != 0 AND t_am.SubjectCodeID != '', t_am.SubjectCodeID = t_as.MainSubjCodeID, t_am.SubjectCode = t_as.MainSubjCode) AND ( IF(t_am.SubjectComponentCode = '##Overall##' or (t_am.SubjectComponentCode != '' and t_am.SubjectComponentCode != 0 and t_am.SubjectComponentCode is not null), t_am.SubjectComponentCode = t_as.CompSubjCodeID, t_as.CompSubjCode = '')) ";
    	        
    	        // Temp table of subjects
    	        // Table name : tempAssessmentSubject
    	        $li_pf->createTempAssessmentSubjectTable($li);
    	        $sql = "INSERT IGNORE INTO tempAssessmentSubject (MainSubjCode, CompSubjCode, SubjName, MainSubjOrder, CompSubjOrder) ";
    	        $sql .= "VALUES ('##Overall##', '##Overall##', '{$totalTitle}', 9999, 9999)";   // 9999 => a large number, ensure it is at the end
    	        $li->db_db_query($sql);
    	        
    	        ### if the parent subject has no mark but the component subjects has mark, insert a dummy record in the mark record so that the parent subject can be displayed in the UI
    	        // get all the main subjects from the subject table which has no subject mark
    	        $sql = "select MainSubjCodeID, MainSubjCode from tempAssessmentSubject group by MainSubjCodeID";
    	        $MainSubjectArr = $li->returnResultSet($sql);
    	        $MainSubjectIDArr = Get_Array_By_Key($MainSubjectArr, 'MainSubjCodeID');
    	        
    	        // get main subjects which has subject mark
    	        $sql = "select SubjectCodeID from tempAssessmentMark where SubjectComponentCode = '' group by SubjectCodeID";
    	        $MainSubjectWithMark = $li->returnResultSet($sql);
    	        $MainSubjectWithMarkIDArr = Get_Array_By_Key($MainSubjectWithMark, 'SubjectCodeID');
    	        
    	        // get main subjects which has no subject mark
    	        $MainSubjectWithoutMarkIDArr = array_diff($MainSubjectIDArr, $MainSubjectWithMarkIDArr);
    	        
    	        // get all parent subjects from the subject table which component subject has mark
    	        $sql = "Select t_as.MainSubjCodeID, assr.SubjectComponentCode, assr.Score, assr.Grade
                	        From tempAssessmentSubject as t_as inner join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr on (t_as.MainSubjCode = assr.SubjectCode AND t_as.CompSubjCode = assr.SubjectComponentCode)
            	        Where t_as.CompSubjOrder != 0 and assr.UserID = '".$default_student_id."' and assr.AcademicYearID = '".$ayID."' And ((assr.OrderMeritForm != -1 And assr.OrderMeritForm != 0) Or assr.OrderMeritForm Is Null Or assr.Grade != '')
    					Group By t_as.MainSubjCodeID";
    	        $sql = "Select t_as.MainSubjCodeID, assr.SubjectComponentCode, assr.Score, assr.Grade
            	           From tempAssessmentSubject as t_as inner join {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr on (t_as.MainSubjCode = assr.SubjectID AND t_as.CompSubjCode = assr.SubjectComponentID)
            	        Where t_as.CompSubjOrder != 0 and assr.UserID = '".$default_student_id."' And ((assr.OrderMeritForm != -1 And assr.OrderMeritForm != 0) Or assr.OrderMeritForm Is Null Or assr.Grade != '')
    					Group By t_as.MainSubjCodeID";
    	        $MainSubjectWithCmpMarkArr = $li->returnResultSet($sql);
    	        $MainSubjectWithCmpMarkIDArr = Get_Array_By_Key($MainSubjectWithCmpMarkArr, 'MainSubjCodeID');
    	        
    	        for ($i=0, $i_max=count($MainSubjectArr); $i<$i_max; $i++)
    	        {
    	            $_mainSubjectId = $MainSubjectArr[$i]['MainSubjCodeID'];
    	            $_mainSubjectCode = $MainSubjectArr[$i]['MainSubjCode'];
    	            
    	            // subject has no mark itself but it's components have mark
    	            if (in_array($_mainSubjectId, $MainSubjectWithoutMarkIDArr) && in_array($_mainSubjectId, $MainSubjectWithCmpMarkIDArr)) {
    	                //$sql = "insert into tempAssessmentMark (SubjectCodeID, SubjectCode) values ('".$_mainSubjectId."', '".$_mainSubjectCode."')";
    	                $sql = "insert into tempAssessmentMark (SubjectCodeID, SubjectCode) values ('".$_mainSubjectId."', '".$_mainSubjectId."')";
    	                $li->db_db_query($sql);
    	            }
    	        }
    	        
    	        // TABLE INFO
    	        $li->field_array = array("t_as.MainSubjOrder", "t_as.CompSubjOrder");
    	        $li->sql = $table_sql;
    	        $li->db = $intranet_db;
    	        $li->title = $ec_iPortfolio['teacher_comment'];
    	        $li->no_msg = $no_record_msg;
    	        $li->page_size = 999;          // Large number to prevent page change
    	        $li->no_col = 3 + $semCount;
    	        $li->noNumber = true;
    	        $li->fieldorder2 = ", t_as.CompSubjOrder";
    	        
    	        $li->table_tag = "<table width='100%' border='0' cellpadding='5' cellspacing='1' class='table_b'>";
    	        //$li->row_alt = array("#FFFFFF", "F3F3F3");
                $li->row_alt = array("", "");
    	        $li->sort_link_style = "class='tbheading'";
    	        
    	        # TABLE COLUMN
    	        $li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
    	        $li->column_list .= $yearClassStr;
    	        $li->column_list .= "</tr>";
    	        $li->column_list .= "<tr class='tabletop'>";
    	        $li->column_list .= "<td width='15%' nowrap='nowrap' style='background:#FFFFFF'>&nbsp;</td>\n";
    	        $li->column_list .= $semStr;
    	        
    	        $li->column_array = array(0,0,0);
    	        if($semCount > 0) {
    	            $li->column_array = array_merge($li->column_array, array_fill(1, $semCount + 1, 3));
    	        }
    	        
    	        $htmlResultTable .= $li->displayAssessmentReportEditTable($displayBy);
    	        
    	        $supportFloatHeader = true;
    	    }
    	    
    	    if($li->total_row > 0)
    	    {
    	        $edit_button_row = '';
    	        $edit_button_row .= '<tr><td align="center">';
    	           $edit_button_row .= '<br/>';
    	           $edit_button_row .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', "jSUBMIT_SCORE('$default_student_id')", 'submitBtn');
        	       $edit_button_row .= '&nbsp;';
        	       $edit_button_row .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', "jCHANGE_RECORD_TYPE('assessment_report')");
    	        $edit_button_row .= '</td></tr>';
    	    }
	    }
	    break;
}

if($RecordType == "attendance")
{
	$SelectBoxContent = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear, 1);
}
else if($RecordType == "merit")
{
	$SelectBoxContent = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear, 0);
}
else
{
	$SelectBoxContent = $li_pf->displayYearSelectBox($default_student_id, $ClassName,$ChooseYear);
}

$linterface->LAYOUT_START();
?>

<? // ===================================== Body Contents ============================= ?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>

<script language="JavaScript">

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Quick student switch 
function jCHANGE_STUDENT(jParShift, jParToExtreme)
{
	var StudentSelect = document.getElementsByName('StudentID');
	var OriginalIndex = StudentSelect[0].selectedIndex;
	
	TargetIndex = OriginalIndex + jParShift;
	if(!jParToExtreme)
	{
		if(TargetIndex >= StudentSelect[0].length)
		{
			StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			return;
		}
		else if(TargetIndex < 0)
		{
			StudentSelect[0].selectedIndex = 0;
			return;
		}
		else
			StudentSelect[0].selectedIndex = TargetIndex;
	}
	else
	{
		if(jParShift < 0)
		{
			if(OriginalIndex > 0)
				StudentSelect[0].selectedIndex = 0;
			else
				return;
		}
		else
		{
			if(OriginalIndex < StudentSelect[0].length-1)
				StudentSelect[0].selectedIndex = StudentSelect[0].length-1;
			else
				return;
		}
	}
	
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Search student name
function jSUBMIT_SEARCH(){
	document.form1.action = "../school_records_class.php";
	document.form1.submit();
}

function jCHANGE_RECORD_TYPE(jRecType)
{
	document.form1.RecordType.value = jRecType;
	if(typeof(document.form1.ChooseYear) != "undefined")
		document.form1.ChooseYear.value = "";
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

function jCHANGE_DISPLAY_TYPE(jDisType)
{
	document.form1.displayBy.value = jDisType;
	document.form1.action = "school_record_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP()
{
	document.form1.action = "learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SI()
{
	document.form1.action = "student_info_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS()
{
	document.form1.action = "sbs/index.php";
	document.form1.submit();
}

function jVIEW_DETAIL(jParRecType, jParParam)
{
	switch(jParRecType)
	{
		case 'merit':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_merit.php";
			document.form1.submit();
			break;
		case 'attendance':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_attendance.php";
			document.form1.submit();
			break;
		case 'assessment_report':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_teacher_stat.php";
			document.form1.submit();
			break;
		case 'reading_records':
			document.form1.miscParameter.value = jParParam;
			document.form1.action = "school_record_reading_records.php";
			document.form1.submit();
			break;
	}
}

function view_report(reportItemID, isDir)
{
	if (isDir == '1') {
		document.form1.target = '_blank';
	}
	document.form1.action = "<?=$PATH_WRT_ROOT?>" + 'home/portfolio/teacher/management/others/view_report.php';
	document.form1.ReportItemID.value = reportItemID;
	document.form1.submit();
	document.form1.target = '_self';
	document.form1.action = 'school_record_teacher.php';
}

function PressEnterEvent(e)
{
	if(e.keyCode == 13) 			// if the pressed key == enter
	{
		document.form1.submit(); 	// refresh the form
	}
	else
	{
	    return false;
	}
}

<?php if($sys_custom['iPf']['AcademicReportEdit'] && $li_pf->IS_IPF_SUPERADMIN()) { ?>
function jSUBMIT_SCORE(sid)
{
	$('#submitBtn').attr('disabled', 'disabled');
	$("#submitBtn").removeClass('formbutton_v30').addClass('formbutton_disable_v30');
	
    $.ajax({
	    type: "POST",
	    url: "assessment_report/ajax_update.php",
	    data: $('#form1').serialize(),
	    success: function(result) {
			if(result == 1) {
				jCHANGE_RECORD_TYPE('assessment_report');
			}
			else {
				$('#submitBtn').attr('disabled', '');
				$("#submitBtn").removeClass('formbutton_disable_v30').addClass('formbutton_v30');
			}
	    }
	});
}
<?php } ?>

</script>

<FORM id="form1" name="form1" method="POST" action="school_record_teacher.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan=2>
				<table border="0" cellspacing="0" cellpadding="3" width="100%">
					<tr>
						<td>
							<?=$class_selection_html?>
						</td>
						<td align="right">
							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$ec_iPortfolio['enter_student_name']:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$ec_iPortfolio['enter_student_name']?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$ec_iPortfolio['enter_student_name']?>'}" />
							<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="navigation">
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records.php"><?=$ec_iPortfolio['class_list']?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="../school_records_class.php?YearClassID=<?=$YearClassID?>"><?=$class_name?></a>
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=($intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName'])?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="17" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_01_tea.gif" width="17" height="37"></td>
						<td height="37" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_03_tea.gif">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_02_tea.gif"  class="page_title"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0" align="absmiddle"> <?=$ec_iPortfolio['school_record']?></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
											<tr>
												<td align="left">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr align="center" valign="middle">
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, true)" class="tablebottomlink">&lt;&lt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(-1, false)" class="tablebottomlink">&lt;</a></td>
															<td nowrap>
																<span class="tabletext">
																	<?=$student_selection_html?>
																</span>
															</td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, false)" class="tablebottomlink">&gt;</a></td>
															<td width="15" nowrap><span class="tabletext"> </span><a href="javascript:jCHANGE_STUDENT(1, true)" class="tablebottomlink">&gt;&gt;</a></td>
															<td width="15" nowrap> | </td>
															<td nowrap class="tabletext"><?=str_replace("<!--NoRecord-->", count($student_detail_arr), $ec_iPortfolio['total_record'])?></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" height="37"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_04_tea.gif" width="13" height="37"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_05.gif" width="17" height="20"></td>
						<td>
							<table border="0" cellpadding="3" cellspacing="0" width="100%">
								<tr>
									<td width="150" valign="top">
										<table border="0" cellspacing="0" cellpadding="0" >
											<tr>
												<td>
													<table border="0" cellspacing="0" cellpadding="0" >
														<tr>
															<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_01.gif" style="padding-left:10px; padding-top:10px;"><?=$student_obj['PhotoLink']?></td>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="10" height="10"></td>
														</tr>
														<tr>
															<td height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
															<td height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/photo_frame_04.gif" width="10" height="10"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" class="tabletext"><?=$intranet_session_language=="b5"?$student_obj['ChineseName']:$student_obj['EnglishName']?></td>
											</tr>
											<tr>
												<td align="center">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td valign="top"><a href="javascript:jTO_SI()" title="<?=$ec_iPortfolio['heading']['student_info']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_info.gif" alt="<?=$ec_iPortfolio['heading']['student_info']?>" width="20" height="20" border="0"></a></td>
<?php
if(in_array($default_student_id, $act_student_id_arr)) {
		if($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT()) {
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
<?php
		}
		if(strstr($ck_user_rights, ":web:") && $li_pf->HAS_RIGHT("learning_sharing")) {
?>
															<td valign="top">
																<a href="javascript:jTO_LP()" title="<?=$ec_iPortfolio['heading']['learning_portfolio']?>">
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" alt="<?=$iPort['menu']['learning_portfolio']?>" width="20" height="20" border="0"><br>
																	<?=$li_pf->GET_LP_NOT_VIEW_COUNT_BY_USER_ID($default_student_id) > 0 ? "<span class='new_alert'>New</span>" : ""?>
																</a>
															</td>
<?php
		}
		if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme")) {
?>
			<td valign="top"><a href="javascript:jTO_SBS()" title="<?=$iPort['menu']['school_based_scheme']?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_sbs.gif" alt="<?=$iPort['menu']['school_based_scheme']?>" width="20" height="20" border="0"></a></td>
<?php
		}
} else if ($li_pf->HAS_SCHOOL_RECORD_VIEW_RIGHT() && $is_skip_activated_checking) {
?>
															<td valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_school_record_on.gif" alt="<?=$ec_iPortfolio['school_record']?>" width="20" height="20" border="0"></td>
<?php
}
?>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td>
<?php if(in_array($default_student_id, $act_student_id_arr) || $is_skip_activated_checking) { ?>
										<table width="100%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>
												  <?=$html_menu_tab?>
													<table border="0" cellpadding="0" cellspacing="6" width="100%">
														<tr>
															<?php
																echo "<td ".$htmlNavStyle.">".$htmlNavArea."</td>";
/*
																if($RecordType != "assessment")
																	echo "<td>".$SelectBoxContent."</td>";
																else{
																	echo "<td align=\"right\" valign=\"middle\" class=\"thumb_list\">";
																	switch($displayBy)
																	{
																		case "Grade":
																			echo $link_score."  | <span>".$link_grade."</span> | ".$link_rank." | ".$link_stand_score;
																			break;
																		case "Rank":
																			echo $link_score."  | ".$link_grade." | <span>".$link_rank."</span> | ".$link_stand_score;
																			break;
																		case "StandardScore":
																			echo $link_score."  | ".$link_grade." | ".$link_rank." | <span>".$link_stand_score."</span>";
																			break;
																		default:
																			echo "<span>".$link_score."</span>  | ".$link_grade." | ".$link_rank." | ".$link_stand_score;
																			break;
																	}
																	echo "</td>";
																}
*/
															?>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">

													<?php
																echo $htmlResultTable;
/*
														switch($RecordType)
														{
															case "activity":
															case "award":
															case "comment":
															case "service":
																$li->displayPlain();
																break;
															case "ole":
																$li->display();
																break;
															case "attendance":
																echo $li_pf->displayStudentAttendanceSummary($StudentID, $ClassName,$ChooseYear, true);
																break;
															case "assessment":
																echo "aaa<br/>";
																		debug_r($StudentID, $ClassName, $displayBy);
																echo $li_pf->generateAssessmentReport3($StudentID, $ClassName, $displayBy, "", "", "", "", "", true);
																//echo $li_pf->generateAssessmentReport4($StudentID, $displayBy);
																break;
															case "merit":
															default:
																echo $li_pf->displayStudentMeritSummary($StudentID, $ClassName,$ChooseYear, true);
																break;
														}
*/
													?>
												</td>
											</tr>
											<?=$edit_button_row?>
										</table>
<?php } else { ?>
										<table>
											<tr>
												<td><?=$ec_iPortfolio['suspend_result_inactive']?></td>
											</tr>
										</table>
<?php } ?>
									</td>
								</tr>
							</table>
						</td>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_07.gif" width="13" height="37"></td>
					</tr>
					<tr>
						<td width="17" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_08.gif" width="17" height="17"></td>
						<td height="17" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_09.gif" width="64" height="17"></td>
						<td width="13" height="17"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/bg_stu_acc_10.gif" width="13" height="17"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<?php 
if($db->isMagicQuotesOn()){
?>
	<input type="hidden" name="StudentInfo" value="<?php echo standardizeFormPostValue($StudentInfo); ?>" />
<?php	
}else{
?>
	<input type="hidden" name="StudentInfo" value="<?php echo $StudentInfo; ?>" />
<?php 
}
?>	
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
	<input type="hidden" name="page_size_change" />
	<input type="hidden" name="miscParameter" />
	<input type="hidden" name="ReportItemID" id="ReportItemID" value="">
</FORM>

<? // ===================================== Body Contents (END) ============================= ?>

<?php if($supportFloatHeader) { ?>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script>$(function() {$('.table_b').floatHeader();});</script>
<?php } ?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>