<?php
// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

intranet_opendb();
$linterface = new interface_html("popup.html");
$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

$a_year_selection = $lpf->getAcademicYearSelectionFromIP("targetYear", "onChange='jLOAD_SEMESTER()'", $ec_iPortfolio['overall_result']);
$a_year_selection_html = ($a_year_selection=="") ? "<i>".$no_record_msg."</i>" : $a_year_selection;

$CurrentPage = "SAMS_import_comment";
$title = $ec_iPortfolio['SAMS_import_comment'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/prototype.js"></script>

<script language="JavaScript">

function jLOAD_SEMESTER()
{
  var targetAYear = document.getElementById('targetYear').value;

  new Ajax.Request(
    '../../ajax/get_semester_selection_ajax.php',
    {
      method:'get',
      parameters: {academicyear: targetAYear},
      onSuccess: function(transport){
        var sem_row = document.getElementById("sem_row");
        var sem_cell = document.getElementById("sem_cell");
        sem_cell.innerHTML = transport.responseText;
//        sem_row.style.display = "block";
		sem_row.show();
      },
      onFailure: function(){
        alert('Something went wrong...')
      }
    }
  );
}
</script>

<FORM enctype="multipart/form-data" action="import_sams_update.php" method="POST" name="form1" onSubmit="this.btn_submit.disabled=true;">
  <table width="420" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['year']?>:</td>
      <td class="tabletext"><?=$a_year_selection?></td>
    </tr>
    <tr id="sem_row" style="display:none">
      <td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['semester']?>:</td>
      <td class="tabletext" id="sem_cell"></td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['SAMS_CSV_file']?>:</td><td class="tabletext"><input class=file type=file name=userfile size=25></td>
    </tr>
  </table>
  
  <br>
  <span class='tabletextremark'><?=$ec_iPortfolio['SAMS_regno_import_remind']?></span>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><a class="contenttool" href="download.php?FileName=comment_sams_sample.csv"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
      <td align="right">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" name="btn_submit">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
      </td>
    </tr>
  </table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>

<script language="JavaScript">
  jLOAD_SEMESTER();
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
