<?php
// Modifing by
################# Change Log [Start] #####
#
#	Date	:	2015-11-04	Omas
#	 			Added insert AcademicYearID and YearTermID to the record 
#
################## Change Log [End] ######
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ImportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

define ("ERROR_TYPE_NO_MATCH_REGNO", 1);
define ("ERROR_TYPE_EMPTY_REGNO", 2);
define ("ERROR_TYPE_INCORRECT_REGNO", 3);
define ("SBJ_CMP_SEPARATOR","###");

if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# Param: $targetYear, $targetSemester, $IsAnnual

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$count_new = 0;
$count_updated = 0;
$display_content = "";

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
  header("Location: import_sams.php");
}
else
{
  intranet_opendb();
  $lpf = new libportfolio();
  
  $lpf->CHECK_ACCESS_IPORTFOLIO();
  $lpf->ADMIN_ACCESS_PAGE();
  
  $CurrentPage = "SAMS_import_comment";
  $title = $ec_iPortfolio['SAMS_import_comment'];
  $TAGS_OBJ[] = array($title,"");
  $MODULE_OBJ["title"] = $title;
  
  $linterface = new interface_html("popup.html");
  $linterface->LAYOUT_START();
  
  $li = new libdb();
  $lo = new libfilesystem();
  $limport = new libimporttext();
  
  $ext = strtoupper($lo->file_ext($filename));
  # Eric Yip (20091201): Add acceptable file extension
  if($ext == ".CSV" || $ext == ".TXT")
  {
    //$data = $lo->file_read_csv($filepath);
    $data = $limport->GET_IMPORT_TXT($filepath);
    $header_row = array_shift($data);                   # drop the title bar
  }

  # Check Format
  $file_format = array("RegNo","Conduct","CommentChi","CommentEng");
  for ($i=0; $i<sizeof($file_format); $i++)
  {
    $hearder = strtolower($header_row[$i]);
    $format_header = strtolower($file_format[$i]);
    if ($hearder!=$format_header)
    {
      $format_wrong = true;
      break;
    }
  }
	
  if ($format_wrong)
  {		
		#############################################################################
		# Wrong file format
		$correct_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
    for ($i=0; $i<sizeof($file_format); $i++)
    {
         $correct_format .= "<tr><td>".$file_format[$i]."</td></tr>\n";
    }
    $correct_format .= "</table>\n";

    $wrong_format = "<table border='0' cellpadding='5' cellspacing='0'>\n";
    for ($i=0; $i<sizeof($header_row); $i++)
    {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
    }
    $wrong_format .= "</table>\n";
    
    $display_content .= "<br><span class='chi_content_15'>".$ec_guide['import_error_wrong_format']."</span><br>\n";
    $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
    $display_content .= "<tr><td valign='top'>$correct_format</td><td width='90%' align='center'>VS</td><td valign='top'>$wrong_format</td></tr>\n";
    $display_content .= "</table>\n";

  }
  else
  {
    # Retrieve WebSAMSRegNo to UserID
    $sql = "SELECT WebSAMSRegNo, UserID FROM {$intranet_db}.INTRANET_USER WHERE WebSAMSRegNo IS NOT NULL AND WebSAMSRegNo != ''";
    $temp = $li->returnArray($sql,2);
    $table_regno2id = $lpf->build_assoc_array($temp);

		unset($error_data);
    for ($i=0; $i<sizeof($data); $i++)
    {
      list($t_regno, $t_conduct, $t_comment_chi, $t_comment_eng) = $data[$i];
      $t_regno = trim($t_regno);
      $t_comment_chi = str_replace("'", "&#39;", $t_comment_chi);
      $t_comment_eng = str_replace("'", "&#39;", $t_comment_eng);
 
      if ($t_regno == "")
      {
        $error_data[] = array($i,ERROR_TYPE_EMPTY_REGNO);
        continue;
      }
      else if (substr($t_regno, 0, 1)!="#")
      {
        $error_data[] = array($i, ERROR_TYPE_INCORRECT_REGNO, array($t_regno));
        continue;
      }

      # Get target Student
      $t_student_id = ($table_regno2id[$t_regno]=="") ? $table_regno2id[str_replace("#","", $t_regno)] : $table_regno2id[$t_regno];
      
      if ($t_student_id == "")
      {
        $error_data[] = array($i,ERROR_TYPE_NO_MATCH_REGNO,array($t_regno));
        continue;
      }
	  
	  if($targetYear!= ''){
		  $sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$targetYear."'";
		  $targetYearIDTemp = $li->returnVector($sql);
		  $targetYearID = $targetYearIDTemp[0];
		  if($targetSemester != ''){
		  	 $sql = "SELECT YearTermID FROM {$intranet_db}.ACADEMIC_YEAR_TERM WHERE AcademicYearID ='".$targetYearID."' and YearTermNameEN = '".$targetSemester."'";
		  	 $targetSemesterIDTemp = $li->returnVector($sql);
		  	 $targetSemesterID = $targetSemesterIDTemp[0];
		  }
		  if($targetSemesterID!=''){
		     $targetSemesterIDString = "'".$targetSemesterID."'";
		  }
		  else{
		  	 $targetSemesterIDString = "NULL";
		  }
	  }
	  		
      # Get existing Record
      $IsAnnual = ($targetSemester=="") ? 1 : 0;
      $conds = ($targetSemester!="") ? " AND Semester = '$targetSemester'" : " AND IsAnnual = '$IsAnnual'";
      $sql = "SELECT 
                RecordID 
              FROM 
                {$eclass_db}.CONDUCT_STUDENT
              WHERE 
                UserID = '$t_student_id'
                AND Year = '$targetYear'
                $conds
            ";
      $temp = $li->returnVector($sql);
      
      $t_comment_chi = addslashes($t_comment_chi);
      # Not exist: Add new record
      if (sizeof($temp)==0)
      {
        $sql = "INSERT INTO {$eclass_db}.CONDUCT_STUDENT 
                  (UserID, Year, Semester, AcademicYearID, YearTermID, IsAnnual, ConductGradeChar, CommentChi, CommentEng, InputDate, ModifiedDate) 
                VALUES ($t_student_id, '$targetYear', '$targetSemester', '$targetYearID', $targetSemesterIDString, '$IsAnnual', '$t_conduct', '$t_comment_chi', '$t_comment_eng', now(), now())
              ";
        $li->db_db_query($sql);
        $count_new ++;
      }
      # Update record
      else
      {
        $t_record_id = $temp[0];
        $sql = "UPDATE 
                  {$eclass_db}.CONDUCT_STUDENT
                SET 
                  ConductGradeChar = '$t_conduct' ,
                  CommentChi = '$t_comment_chi' , 
                  CommentEng = '$t_comment_eng',
                  ModifiedDate = now()
                WHERE 
                  RecordID = '$t_record_id'
              ";
        $li->db_db_query($sql);
        
        $count_updated ++;
      }
    }
  }
}
# Display import stats
$display_content .= "<table border='0' cellpadding='5' cellspacing='0'>";
$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_add_no']." : </td><td class='tabletext'><b>".$count_new."</b></td></tr>\n";
$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_update_no']." : </td><td class='tabletext'><b>".$count_updated."</b></td></tr>\n";
$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_fail_no']." : </td><td class='tabletext'><b>".sizeof($error_data)."</b></td></tr>\n";
$display_content .= "</table>\n";

if (sizeof($error_data)!=0)
{
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
  $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";

	for ($i=0; $i<sizeof($error_data); $i++)
	{
		list ($t_row, $t_type, $t_data) = $error_data[$i];
		$t_row++;     # set first row to 1
		$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
		$reason_string = $ec_guide['import_error_unknown'];
		switch ($t_type)
		{
			case ERROR_TYPE_NO_MATCH_REGNO:
					$reason_string = $ec_guide['import_error_no_user'];
					break;
			case ERROR_TYPE_EMPTY_REGNO:
					$reason_string = $ec_iPortfolio['activation_result_no_regno'];
					break;
			case ERROR_TYPE_INCORRECT_REGNO:
					$reason_string = $ec_guide['import_error_incorrect_regno'];
					break;
			default:
					$reason_string = $ec_guide['import_error_unknown'];
					break;
		}
		$error_table .= $reason_string;

		$error_contents = (is_array($t_data)) ? implode(",",$t_data) : $t_data."&nbsp;";
		$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}

?>

<FORM enctype="multipart/form-data" method="POST" name="form1">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='import_sams.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
