<?php
// Modifing by 

/* Change Log
 * 
 * Date: 2015-02-13 (Omas) ip2.5.6.3.1 - Erase record excluding archived student record
 *   
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_opendb();
$lpf = new libportfolio();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();
$linterface = new interface_html("popup.html");

$Semester = ($IsAnnual==1) ? "" : $Semester;
if($Semester!="")
{
	$conds .= ($Semester==$ec_iPortfolio['overall_comment']) ? " AND a.IsAnnual = 1" : " AND a.Semester = '$Semester'";
}
$conds .= ($ClassName!="") ? " AND b.ClassName = '$ClassName'" : "";

# ARCHIVE User Record not delete
$sql = "SELECT UserID from INTRANET_ARCHIVE_USER where RecordType  = 2";
$ArchivedUserAry = $lpf->returnVector($sql);
$notInArchived_conds = " AND a.UserID not IN('".implode("','",$ArchivedUserAry)."') ";

# retreieve the target record id
$sql = "SELECT 
			DISTINCT a.RecordID
		FROM 
			{$eclass_db}.CONDUCT_STUDENT as a 
			LEFT JOIN {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as b ON a.UserID = b.UserID AND a.Year = b.Year
		WHERE 
			a.Year = '$Year'
			$conds
			$notInArchived_conds
		";
$RecordIDArr = $lpf->returnVector($sql);

$RecordIDStr = implode(",", $RecordIDArr);
$sql = "DELETE FROM {$eclass_db}.CONDUCT_STUDENT WHERE RecordID IN ($RecordIDStr)";
$lpf->db_db_query($sql);

$total = $lpf->db_affected_rows();

$CurrentPage = "remove_conduct_info";
$title = $ec_iPortfolio['remove_conduct_info'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

$display_content = "<table border='0' cellpadding='8' cellspacing='0'>";
$display_content .= "<tr><td class='tabletext'>".$ec_iPortfolio['number_of_removed_record']." : </td><td class='tabletext'><b>".$total."</b></td></tr>\n";
$display_content .= "</table>\n";

?>

<FORM enctype="multipart/form-data" action="import_regno_update.php" method="POST" name="form1">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p>
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_iPortfolio['remove_more_conduct']?>" onClick="self.location='data_remove.php'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="javascript:window.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
