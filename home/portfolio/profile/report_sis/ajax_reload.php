<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$libportfolio_ui = new libportfolio_ui();
$lpf_fc = new libpf_formclass();

$Action = $_POST['Action'];

if ($Action == 'Reload_Export_Batch_Thickbox_Layer') {
	echo $libportfolio_ui->Get_Export_Batch_Thickbox_Layer();
}
else if ($Action == 'Class_Selection') {
	
	$class_arr = $lpf_fc->getClassListArray();
	$StudentType = $_POST['StudentType'];
	$onclick ="jCHANGE_CLASS(\"".$StudentType."\");";
	if($sys_custom['uccke_full_report']){
		$class_selection_stmcSLP_html = $libportfolio_ui->GEN_MULTIPLE_CLASS_SELECTION_OPTGROUP($selectionName='YearClassID[]', $selectionID='YearClass', $class_arr, "size='10'", '', $JsAction = $onclick);
	}else{
		$class_selection_stmcSLP_html = $libportfolio_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' id='YearClass' onChange='".$onclick."'");
	}

	echo $class_selection_stmcSLP_html;
}

intranet_closedb();
?>