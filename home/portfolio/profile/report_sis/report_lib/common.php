<?php

function GET_PRINTABLE_YEARCLASS($ParAllClass){
  global $ck_user_rights_ext;

  if($ParAllClass)
  {
    $returnArr = GET_ALL_YEARCLASS();
  }
  else
  {
    $subject_teacher_class_arr = array();
    $class_teacher_class_arr = array();
  
    if(strstr($ck_user_rights_ext, "print_report:form_subject_t"))
    {
      $subject_teacher_class_arr = GET_SUBJECT_TEACHER_YEARCLASS();
      $class_teacher_class_arr = GET_CLASS_TEACHER_YEARCLASS();
    }
    else if(strstr($ck_user_rights_ext, "print_report:form_t"))
    {
      $subject_teacher_class_arr = GET_SUBJECT_TEACHER_YEARCLASS();
    }
    
    $returnArr = arrayUnique(array_merge($class_teacher_class_arr, $subject_teacher_class_arr));
  }
  return $returnArr;
}

function GET_ALL_YEARCLASS(){
  global $intranet_db;
  
  $libdb = new libdb();
  
  $sql =  "
            SELECT DISTINCT
              YearClassID,
              ".libportfolio::GetYearClassTitleFieldByLang()."
            FROM
              {$intranet_db}.YEAR_CLASS
            WHERE
              AcademicYearID = ".Get_Current_Academic_Year_ID()."
          ";
  $returnArr = $libdb->returnArray($sql);

  return $returnArr;
}

function GET_CLASS_TEACHER_YEARCLASS(){
  global $intranet_db, $UserID;
  
  $libdb = new libdb();
  
  $sql =  "
            SELECT DISTINCT
              yc.YearClassID,
              ".libportfolio::GetYearClassTitleFieldByLang("yc.")."
            FROM
              {$intranet_db}.YEAR_CLASS AS yc
            INNER JOIN
              {$intranet_db}.YEAR_CLASS_TEACHER AS yct
            ON
              yc.YearClassID = yct.YearClassID
            WHERE
              yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
              yct.UserID = ".$UserID."
              
          ";
  $returnArr = $libdb->returnArray($sql);

  return $returnArr;
}

function GET_SUBJECT_TEACHER_YEARCLASS(){
  global $intranet_db, $UserID;
  
  $libdb = new libdb();
  
  $sql =  "
            SELECT DISTINCT
              yc.YearClassID,
              ".libportfolio::GetYearClassTitleFieldByLang("yc.")."
            FROM
              {$intranet_db}.YEAR_CLASS AS yc
            INNER JOIN
              {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
            ON
              yc.AcademicYearID = ayt.AcademicYearID
            INNER JOIN
              {$intranet_db}.SUBJECT_TERM AS st
            ON
              ayt.YearTermID = st.YearTermID
            INNER JOIN
              {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER AS stct
            ON
              st.SubjectGroupID = stct.SubjectGroupID
            WHERE
              yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
              stct.UserID = ".$UserID."
          ";
  $returnArr = $libdb->returnArray($sql);

  return $returnArr;
}

function GET_CLASS_TEACHER_STUDENT($YearClassID, $ActivatedOnly=false, $trgType=''){
  global $intranet_db, $UserID, $eclass_db;
  
  $libdb = new libdb();
  
  if ($ActivatedOnly) {
  	$sql = "Select UserID From {$eclass_db}.PORTFOLIO_STUDENT Where IsSuspend = 0";
  	$activeUserIdAry = $libdb->returnVector($sql);
  	$conds_activeUserId = " And iu.UserID IN ('".implode("','", (array)$activeUserIdAry)."') ";
  }
  
  if ($trgType == 'current') {
  	$conds_recordStatus = " And iu.RecordType = 2 And iu.RecordStatus = 1 ";
  }
  
  $YearClass_ids = (is_array($YearClassID)) ? implode(",", $YearClassID) : $YearClassID;
  
  $sql =  "
            SELECT DISTINCT
              iu.UserID,
              CONCAT('(', iu.ClassName, ' - ', iu.ClassNumber, ') ', ".Get_Lang_Selection("iu.ChineseName", "iu.EnglishName").")
            FROM
              {$intranet_db}.YEAR_CLASS_USER AS ycu
            INNER JOIN
              {$intranet_db}.INTRANET_USER AS iu
            ON
              ycu.UserID = iu.UserID
            WHERE
              ycu.YearClassID in ( ".$YearClass_ids.") 
			  $conds_activeUserId
			  $conds_recordStatus
            ORDER BY
              iu.ClassNumber
          ";
  $returnArr = $libdb->returnArray($sql);

  return $returnArr;
}

function arrayUnique($myArray) 
{ 
  if(!is_array($myArray)) 
    return $myArray; 
  
  foreach ($myArray as &$myvalue){ 
    $myvalue=serialize($myvalue); 
  } 
  
  $myArray=array_unique($myArray); 
  
  foreach ($myArray as &$myvalue){ 
    $myvalue=unserialize($myvalue); 
  } 
  
  return $myArray; 

}

?>