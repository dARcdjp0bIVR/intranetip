<?php
/**
 * [Modification Log] Modifying By: Connie
 * *******************************************
 * 2010-07-15 Max (201007150955)
 * - Modified the method of getting $NoPhotoConfig, $NoStudentDetailsConfig and $NoSchoolNameConfig
 * 
 * 2010-05-06 Max (201005060948)
 * - Modified generateFullReportPrint() to print with Internal and External OLE record
 * - Modified returnOLERecord() to get record can separate INT and EXT OLE records
 * *******************************************
 */
include_once("../../../../includes/libportfolio.php");
include_once("../../../../includes/libportfolio2007a.php");
include_once("../../../../includes/libpf-report.php");

# Generate Student Full Report for Printing
function generateFullReportPrint($StudentID, $AcademicYearID="", $YearTermID="")
{
	global $iPort, $ec_iPortfolio, $eclass_filepath;
	global $lpf_sturec, $lpf_report,$sys_custom; 
	
  # Academic Year
  $lib_ay = new academic_year($AcademicYearID);
  $Year = $lib_ay->YearNameEN;
  
  # Year Term
  $lib_ayt = new academic_year_term($YearTermID);
  $Semester = $lib_ayt->YearTermNameEN;
  
  # School Name
  $SchoolName = GET_SCHOOL_NAME();

	// Page Break Style
	$page_break_style = "<STYLE TYPE='text/css'>\nP.breakhere {page-break-before: always}\n</STYLE>\n";
	$page_break_here = "<p class='breakhere'></p>\n";

	////////////////////////////////////
	// load report settings
	$portfolio_report_config_file = $eclass_filepath."/files/portfolio_report_config.txt";
	$filecontent = trim(get_file_content($portfolio_report_config_file));
	$config_array = unserialize($filecontent);
	##############################################################################
	# 201007151009, seems getting the config from wrong position 13-15 of array,
	# changed to 14-16 <== Whenever added a new column, the number will be shifted by 1
	# and the method of getting the config is like the following, refer to 
	# /home/web/eclass40/intranetIP25/home/portfolio/teacher/settings/report/full_report_config.php
	# $xxx = !$config_array[y];
	$NoPhotoConfig = !$config_array[15];
	$NoStudentDetailsConfig = !$config_array[16];
	$NoSchoolNameConfig = !$config_array[17];
	##############################################################################

	// Get Report Settings
	$fieldArr[1] = "parent_record";
	$fieldArr[2] = "merit_record";
	$fieldArr[3] = "transcript_score_record";
	$fieldArr[4] = "transcript_rank_record";
	$fieldArr[5] = "transcript_grade_record";
	$fieldArr[6] = "transcript_stand_score_record";
	$fieldArr[7] = "activity_record";
	$fieldArr[8] = "award_record";
	$fieldArr[9] = "comment_record";
	$fieldArr[10] = "attendance_record";
	$fieldArr[11] = "service_record";
	$fieldArr[12] = "ole_record";
	$fieldArr[13] = "outside_school";
	$fieldArr[14] = "student_self_account";
	

	for($i=1; $i<=14; $i++)
	{
		$temp = $config_array[$i];

		if($temp[1]==1)
		{
			${"show_".$fieldArr[$i]} = 1;

			switch($i)
			{
				case 3:
					$score_cmp = $temp[2];
					break;
				case 4:
					$rank_cmp = $temp[2];
					break;
				case 5:
					$grade_cmp = $temp[2];
					break;
				case 6:
					$stand_score = $temp[2];
					break;
				case 12:
					$show_remark = $temp[2];
					break;
			}

			$report_array[$temp[0]] = $fieldArr[$i];
		}
	}
	if(is_array($report_array))
		ksort($report_array);

	////////////////////////////////////////

	$StudentObjArr = $lpf_report->getStudentObject_print($StudentID);

	$rx = $page_break_style;

	$display_SchoolName = $SchoolName;
	$display_ReportTitle = "";

	if($sys_custom['iPortfolio_BurnCD']['HinHua'] == true){
		//if this is HinHua, cust the school name
		$display_SchoolName =  $SchoolName."<br/>"."Hin Hua High School";

		$display_ReportTitle =  $ec_iPortfolio['full_reportTitle2'];

	}

	for($sx=0, $sx_max = sizeof($StudentObjArr);  $sx < $sx_max; $sx++)
	{
		$student_obj = $StudentObjArr[$sx];
		$student_id = $student_obj["UserID"];
		if($NoPhotoConfig==1)
		{
			$student_info_display = "&nbsp;";
		}
		else
		{
			# generate student photo table
			$photolink = $student_obj["PhotoLink"];
			
			$datemodified = $student_obj["DateModified"];
			$student_photo_display = $lpf_report->generateStudentPhotoTable($photolink, $datemodified);
		}
		# generate student info table
		$student_info_display = $lpf_report->generateStudentInfoTable_print($student_id, $student_obj, $NoStudentDetailsConfig);

		# retrieve class history
		$class_history_display = $lpf_report->generateClassHistoryTable_print($student_id, $Year);


		// student info part
		if(!$NoSchoolNameConfig)
		{
			//$_reportTitle = $ec_iPortfolio['full_report'];

		
			$rx .= "<table cellSpacing=0 cellPadding=0 width='90%' border=1 bordercolor='black' align='center'>
				   <tr>
					  <td valign='middle' align='center' height='50'>
							<FONT face=simhei size=6>".$display_SchoolName."<br/>".$display_ReportTitle."</FONT>
					   </td>
					</tr>
					</table><br />";
		}

		$personal_info = "<table cellSpacing=0 cellPadding=5 width='80%' align='center' border=0>
							<tr>";
		if(!$NoPhotoConfig)
		{
			$personal_info .= "<td valign='bottom'>
								".$student_photo_display."
							  </td>";
		}
		$personal_info .= "<td valign='top'>
						  ".$student_info_display."
						  </td>
						  <td align='center' valign='top'>
						 ".$class_history_display."
							</td>
						</tr>
						</table><br /><br />";

		if($show_parent_record==1)
		{
			# generate student parent info table
			$parent_display = $lpf_report->generateParentInfoTable_print($student_id);

			// parent info part
			$parent_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['guardian_info']."</td>
							</tr>
							</table>
							<table width='100%' border='1' cellspacing='0' cellpadding='0' bgcolor='#000000' class='table_print'>
							<tr><td>".$parent_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_merit_record==1)
		{

			# generate merit summary table
			$merit_display = $lpf_report->displayStudentMeritSummary_print($student_id, $Year, $Semester);

			// student merit summary part
			$merit_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['merit']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000' align='center'>
							<tr><td>".$merit_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		$Printable = true; // set print grid on
		if($show_transcript_score_record==1)
		{
			# transcript by score
			$transcrip_score_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Score", "Print", $score_cmp, $Year, $Semester, $Printable);

			// transcript part (by score)
			$transcript_score_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr><td height='30'>(".$ec_iPortfolio['display_by_score'].")</td></tr>
							<tr><td>".$transcrip_score_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_transcript_rank_record==1)
		{
			# transcript by rank
			$transcrip_rank_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Rank", "Print", $rank_cmp, $Year, $Semester, $Printable);

			// transcript part (by rank)
			$transcript_rank_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr><td height='30'>(".$ec_iPortfolio['display_by_rank'].")</td></tr>
							<tr><td>".$transcrip_rank_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_transcript_grade_record==1)
		{
			# transcript by grade
			$transcrip_grade_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Grade", "Print", $grade_cmp, $Year, $Semester, $Printable);

			// transcript part (by grade)
			$transcript_grade_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr><td height='30'>(".$ec_iPortfolio['display_by_grade'].")</td></tr>
							<tr><td>".$transcrip_grade_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_transcript_stand_score_record==1)
		{
			# transcript by grade
			$transcrip_stand_score_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "StandardScore", "Print", $grade_cmp, $Year, $Semester, $Printable);

			// transcript part (by standard score)
			$transcript_stand_score_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr><td height='30'>(".$ec_iPortfolio['display_by_stand_score'].")</td></tr>
							<tr><td>".$transcrip_stand_score_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_activity_record==1)
		{
			# generate activity table
			$activity_display = $lpf_report->generateActivityTable_print($student_id, $Year, $Semester);

			// activity info part
			$activity_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['activity']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$activity_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_award_record==1)
		{
			# generate award table

			$award_display = $lpf_report->generateAwardTable_print($student_id, $Year, $Semester);

			// award info part
			$award_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['award']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$award_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_comment_record==1)
		{
			# generate teacher comment table
			$comment_display = $lpf_report->generateCommentTable_print($student_id, $Year, $Semester);

			// teacher comment info part
			$comment_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['teacher_comment']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$comment_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_attendance_record==1)
		{
			# generate attendance table
			$attendance_display = $lpf_report->displayStudentAttendanceSummary_print($student_id, $Year, $Semester);

			// attendance info part
			$attendance_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['attendance']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$attendance_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_service_record==1)
		{
			# generate attendance table
			$service_display = $lpf_report->generateServiceTable_print($student_id, $Year, $Semester);

			// attendance info part
			$service_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$ec_iPortfolio['service']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$service_display."</td></tr>
							</table>
							</td>
							</tr>
							</table><br /><br />";
		}

		if($show_ole_record==1)
		{

			# control which fields to display for Int and Ext
			$showRemarkInt = $show_remark;
			$tempRemarkArray = explode(",", $show_remark);
			$showRemarkExt = implode(",",rem_array_from_array($tempRemarkArray, array(1,3))); // Remove 1 - Component of OLE, 3 - Hours
			
			# generate attendance table
			# call convertSystemYearToOLEYear to convert system year to OLE year
			$IsShowApprovedBy = false;
			$ole_display_Int = generateOLETable_print($student_id, $AcademicYearID, $YearTermID, $IsShowApprovedBy, $showRemarkInt, "INT");
			$ole_display_Ext = generateOLETable_print($student_id, $AcademicYearID, $YearTermID, $IsShowApprovedBy, $showRemarkExt, "EXT");

			// attendance info part
			$ole_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$iPort['internal_record']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$ole_display_Int."</td></tr>
							</table>
							</td></tr>
							</table><br /><br />";
		}

		if($show_outside_school==1)
		{
			# control which fields to display for Int and Ext
			$showRemarkInt = $show_remark;
			$tempRemarkArray = explode(",", $show_remark);
			$showRemarkExt = implode(",",rem_array_from_array($tempRemarkArray, array(1,3))); // Remove 1 - Component of OLE, 3 - Hours
			
			# generate attendance table
			# call convertSystemYearToOLEYear to convert system year to OLE year
			$IsShowApprovedBy = false;

			$ole_display_Ext = generateOLETable_print($student_id, $AcademicYearID, $YearTermID, $IsShowApprovedBy, $showRemarkExt, "EXT");

			// attendance info part
			$outside_school = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
							<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
							<tr>
								<td height='30' class='head_sub_4'>".$iPort['external_record']."</td>
							</tr>
							</table>
							<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
							<tr><td>".$ole_display_Ext."</td></tr>
							</table>
							</td></tr>
							</table><br /><br />";

		}
		
		if ($show_student_self_account == 1) {
			include_once("slp_report.php");
			$slp_report = new slp_report();
			$selfAccountDataArray = current($slp_report->GET_SELF_ACCOUNT_SLP(array($student_id)));
			if (empty($selfAccountDataArray["Details"])) {
				global $no_record_msg;
				$displaySeflAccountDetail = "<tr height=25><td align=center height='100' ' bgcolor='#FFFFFF' nowrap>".$no_record_msg."</td></tr>\n";
			} else {
				$displaySeflAccountDetail = "<tr><td>".$selfAccountDataArray["Details"]."</td></tr>";
			}
			
			$student_self_account = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
							<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['student_self_account']."</td>
								</tr>
								</table>
							<table width='100%' style='border:1px black solid;' cellspacing='0' cellpadding='0'>
							$displaySeflAccountDetail
							</table>
							</td></tr>
							</table><br /><br />";			
		}
		
		
		$rx .= $personal_info;
		if(sizeof($report_array)!=0)
		{
			foreach ($report_array as $order => $values)
			{
				$rx .= ${$values};
			}
		}

		if ($sx<sizeof($StudentObjArr)-1)
		{
			$rx .= $page_break_here;
		}
	}
	return $rx;
}

function rem_array_from_array($arraySrc, $arrayToRemove) {
//	debug_r($arraySrc);
	foreach($arrayToRemove as $key => $element) {
		$arraySrc = rem_array($arraySrc, $element);
	}
	return $arraySrc;
}
function rem_array($array,$str){
	foreach ($array as $key => $value) {
		if ($array[$key] == "$str") unset($array[$key]);
	}
	return $array;
}

# generate OLE table for report printing
function generateOLETable_print($StudentID, $AcademicYearID, $YearTermID, $IsShowApprovedBy=false, $ShowFields="", $IntExt="")
{
	global $ec_iPortfolio, $no_record_msg,$sys_custom;
	global $lpf_sturec, $lpf_slp;

	# Add 3rd parameter to show all category
	$result = returnOLERecord($StudentID, $AcademicYearID, $YearTermID, $IntExt);

	$title_array = array($ec_iPortfolio['date']."/".$ec_iPortfolio['period'], $ec_iPortfolio['title']);
	
	$OLE_fields =	array	(
												$ec_iPortfolio['category'],
												$ec_iPortfolio['ele'],
												$ec_iPortfolio['ole_role'],
												$ec_iPortfolio['hours'],
												$ec_iPortfolio['achievement'],
												$ec_iPortfolio['details'],
												$ec_iPortfolio['approved_by'],
												$ec_iPortfolio['school_remark']
											);
											
	$t_component = explode(",", $ShowFields);
	
	$nowrap_html = 'nowrap';

	if($sys_custom['iPf']['FullReport']['OLEsection_Hide_nowrap'] )
	{	
		$nowrap_html = '';
	}


	for($k=0; $k<count($OLE_fields); $k++)
	{
		if($OLE_fields[$k] == $ec_iPortfolio['approved_by'])
		{
			if($IsShowApprovedBy)
				$title_array[] = $OLE_fields[$k];
		}
		else if($OLE_fields[$k] == $ec_iPortfolio['school_remark'])
		{
			 if(in_array($k-1, $t_component))
				$title_array[] = $OLE_fields[$k];
		}
		else if(in_array($k, $t_component))
			$title_array[] = $OLE_fields[$k];
	}

//		if ($IsShowApprovedBy)
//			$title_array[] = $ec_iPortfolio['approved_by'];
		
//		if($IsShowRemarks)
//			$title_array[] = $ec_iPortfolio['school_remark'];

      //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
      $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
	$x .= "<tr height='35'>\n";
	for ($i=0; $i<sizeof($title_array); $i++)
      {
         $x .= "<td class='head_sub_2' bgcolor='#FFFFFF'><b>".$title_array[$i]."&nbsp;</b></td>";
      }
	$x .= "</tr>\n";

      if (sizeof($result)==0)
      {
         return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>".$no_record_msg."</td></tr></table>\n";
      }

	# Add parameter to show all ELE
	$DefaultELEArray = $lpf_slp->GET_ELE(true);
	$num = 0;
      for($i=0; $i<sizeof($result); $i++)
      {
		list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele) = $result[$i];

		$ELEIDArray = explode(",", $ele);
		unset($ELEArray);
		for($m=0; $m<sizeof($ELEIDArray); $m++)
		{
			$tmp = trim($ELEIDArray[$m]);
			$ELEArray[] = $DefaultELEArray[$tmp];
		}
		$DisplayELE = (!empty($ELEArray)) ? implode("<br />", $ELEArray) : "";

          $x .= "<tr valign='top' height=25>\n";
		$x .= "<td bgcolor='#FFFFFF'>{$period}&nbsp;</td>\n";
		$x .= "<td bgcolor='#FFFFFF' $nowrap_html>{$title}&nbsp;</td>\n";
		
		if(in_array(0, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$category}&nbsp;</td>\n";
		if(in_array(1, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$DisplayELE}&nbsp;</td>\n";
		if(in_array(2, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$role}&nbsp;</td>\n";
		if(in_array(3, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$hours}&nbsp;</td>\n";
		if(in_array(4, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$achievement}&nbsp;</td>\n";
		if(in_array(5, $t_component))
		$x .= "<td bgcolor='#FFFFFF'>{$details}&nbsp;</td>\n";
		if ($IsShowApprovedBy)
		{
			$x .= "<td bgcolor='#FFFFFF'>{$approved_by}&nbsp;</td>\n";
		}
		if(in_array(6, $t_component))
		{
			$x .= "<td bgcolor='#FFFFFF'>{$remark}&nbsp;</td>\n";
		}
		$x .= "</tr>\n";
	}
      $x .= "</table>\n";

      return $x;
}

function returnOLERecord($StudentID, $AcademicYearID, $YearTermID, $IntExt="")
{
	global $eclass_db, $intranet_db, $ec_iPortfolio, $profiles_to;

  # Check by OLE start date
  //$extra_table = empty($AcademicYearID) ? "" : "{$intranet_db}.ACADEMIC_YEAR AS ay, {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt, ";
  //$cond = empty($AcademicYearID) ? "" : " AND (op.StartDate BETWEEN ayt.TermStart and ayt.TermEnd AND ayt.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID = ".$AcademicYearID.")";
  //$cond .= empty($YearTermID) ? "" : " AND (op.StartDate BETWEEN ayt.TermStart and ayt.TermEnd AND ayt.YearTermID = ".$YearTermID.")";
  # Check by OLE year and term
  $cond = empty($AcademicYearID) ? "" : " AND op.AcademicYearID  = ".$AcademicYearID;
  $cond .= empty($YearTermID) ? "" : " AND op.YearTermID = ".$YearTermID;
  $cond .= empty($IntExt) ? "" : " AND op.IntExt = '" . $IntExt . "'";

	$namefield = getNameFieldWithClassNumberByLang("iu.");
	
	$lpf_slp = new libpf_slp();
	$CategoryField = $lpf_slp->getOLRCategoryFieldForRecord("op.", true);
  $sql =  "
            SELECT
      				IF (DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', op.StartDate, CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' $profiles_to<br />', DATE_FORMAT(op.EndDate, '%Y-%m-%d')))) as OLEDate,
      				op.Title,
      				$CategoryField,
      				os.Role,
      				os.Hours,
      				os.Achievement,
      				os.Details,
      				$namefield as ApproveBy,
      				IF (DATE_FORMAT(os.ProcessDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(os.ProcessDate,'%Y-%m-%d')),
      				op.SchoolRemarks,
      				op.ModifiedDate,
      				op.StartDate,
      				op.ele,
					op.IntExt
      			FROM
              {$extra_table}
      				{$eclass_db}.OLE_STUDENT AS os
      			INNER JOIN
              {$eclass_db}.OLE_PROGRAM AS op
            ON
              os.ProgramID = op.ProgramID
            LEFT JOIN
              {$intranet_db}.INTRANET_USER AS iu
            ON
              os.ApprovedBy = iu.UserID
      			WHERE
      				os.UserID = '$StudentID'
      				AND (os.RecordStatus = '2' || os.RecordStatus = '4')
      				$cond
      			ORDER BY
      				os.StartDate DESC
          ";
	$row = $lpf_slp->returnArray($sql);
//debug_r($row);
	return $row;
}

?>