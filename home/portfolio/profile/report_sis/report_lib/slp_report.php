<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - added SET_SubjectInfoOrderedArr, subject ordering follows the School Settings now
 * 
 * 2011-03-04 Ivan [CRM: 2011-0304-1006-35054]
 * - Modified function SET_DETAIL_SETTINGS() to initialize the $this->detail_settings[0] as array()
 * 
 * 2010-07-27 Max (201007261726)
 * - Add configuration for student details
 * 
 * 2010-06-04 Max (201006041431)
 * - Add page break settings and SET_MODULE_PAGE_BREAK_SETTING()
 * *******************************************
 */
include_once($intranet_root."/includes/libportfolio.php");
include_once($intranet_root."/includes/libportfolio2007a.php");
include_once($intranet_root."/includes/libpf-report.php");
include_once($intranet_root."/includes/libpf-sturec.php");

class slp_report extends libpf_report{

  var $SPArr;
  var $SubjArr;
  var $YearArr;
  var $APArr;
  var $AwardArr;
  var $ActivityArr;
  var $OLEArr;
  var $ExtPerformArr;
  var $SelfAccountArr;
  var $ModuleTitle;
  var $controlOrder;
  var $controlShow;
  var $pageBreakSetting;
  var $detail_settings;
  var $SubjectInfoOrderedArr;
  var $SubjectFullMarkArr;
  
  var $report_lang = "BI";	// default bilingual
  
  ########################################################################
  function SET_SPArr($ParSPArr){
    $this->SPArr = $ParSPArr;
  }
  
  function SET_SubjArr($ParSubjArr){
    $this->SubjArr = $ParSubjArr;
  }
  
  function SET_YearArr($ParYearArr){
    $this->YearArr = $ParYearArr;
  }
  
  function SET_APArr($ParAPArr){
    $this->APArr = $ParAPArr;
  }
  
  function SET_AwardArr($ParAwardArr){
    $this->AwardArr = $ParAwardArr;
  }
  
  function SET_ActivityArr($ParActivityArr){
    $this->ActivityArr = $ParActivityArr;
  }
  
  function SET_OLEArr($ParOLEArr){
    $this->OLEArr = $ParOLEArr;
  }
  
  function SET_ExtPerformArr($ParExtPerformArr){
    $this->ExtPerformArr = $ParExtPerformArr;
  }
  
  function SET_SelfAccountArr($ParSelfAccountArr){
    $this->SelfAccountArr = $ParSelfAccountArr;
  }
  
  function SET_SubjectInfoOrderedArr($ParSubjectInfoOrderedArr){
    $this->SubjectInfoOrderedArr = $ParSubjectInfoOrderedArr;
  }
  
  function SET_SubjectFullMarkArr($ParSubjectFullMarkArr){
    $this->SubjectFullMarkArr = $ParSubjectFullMarkArr;
  }
  
  function SET_MODULE_PAGE_BREAK_SETTING() {
  	global $eclass_root,$lpf_slp;
  	
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				list($filecontent, $OLESettings, $EXTOLESettings, $pageBreakSetting) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
				if (!isset($pageBreakSetting)) {
					$this->pageBreakSetting = array();
				} else {
					$pageBreakSetting = unserialize($pageBreakSetting);
					
					# skip activity - *This is due to the handling referred to [Code Bookmark: SkipActivity]
					$sizeOfPageBreakSetting = count($pageBreakSetting);
					for($i=1;$i<=$sizeOfPageBreakSetting;$i++) {
						if($i>2) {
							$modifiedPageBreakSetting[$i+1] = $pageBreakSetting[$i];
						}
						else {
							$modifiedPageBreakSetting[$i] = $pageBreakSetting[$i];
						}
					}				
					
					$this->pageBreakSetting = $modifiedPageBreakSetting;
				}
			} else {
				$this->pageBreakSetting = array();
			}
		}
  }
  
  function SET_DETAIL_SETTINGS() {
		global $eclass_root,$lpf_slp;
  	
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				list($filecontent) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
				$config_array = unserialize($filecontent);
				$this->detail_settings[0] = $config_array[count($config_array)][1];
			} else {
				// [2011-0304-1006-35054] fixed array_sum() error in function GEN_STUDENT_PARTICULARS_TABLE_SLP() in /includes/libpf-report.php
				//$this->detail_settings = array();
				$this->detail_settings[0] = array();
			}
		}
  }
  
  function SET_MODULE_ORDER(){
    global $eclass_root, $lpf_slp, $sys_custom;
  
		# Display Setting
		$this->ModuleTitle[] = "student_particular";
		$this->ModuleTitle[] = "academic_performance";
		$this->ModuleTitle[] = "ole";
		$this->ModuleTitle[] = "activity";
		$this->ModuleTitle[] = "award_in_school";
		$this->ModuleTitle[] = "award_outside_school";
		$this->ModuleTitle[] = "self_account";
		
		# Display order in terms of index of $ModuleTitle
		# Number of entries must be consistent with $ModuleTitle		
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())  //Fai : Should be don't check this condition , schedule remove on 201107
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				list($filecontent, $OLESettings) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
				$config_array_temp = unserialize($filecontent);
	
				# Get order only
				for($i=1; $i<count($config_array_temp); $i++)
				{
					#-[Code Bookmark: SkipActivity]
					# skip activity
					if($i>2)
						$config_array[$i+1] = $config_array_temp[$i][0];
					else
						$config_array[$i] = $config_array_temp[$i][0];
				}
				$this->controlOrder = array_flip($config_array);
				# Default:
				# student_particular : first
				# activity : last
				$this->controlOrder[0] = 0;
				$this->controlOrder[6] = 3;
			}
			else
				$this->controlOrder = array(0,1,2,3,4,5,6);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					$this->controlOrder = array(0,1,4,3,2,5,6);
					break;
			}
		}

		# Display control in terms of index of $ModuleTitle
		# Number of entries must be consistent with $ModuleTitle
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				$filecontent = trim(get_file_content($portfolio_report_config_file));
				$config_array = unserialize($filecontent);
	
				# Get display option only
				for($i=1; $i<count($config_array); $i++)
				{
					# skip activity
					if($i>2)
						$this->controlShow[$i+1] = $config_array[$i][1];
					else
						$this->controlShow[$i] = $config_array[$i][1];
				}
				$this->controlShow[0] = $config_array[count($config_array)][0];
				
				# not show activity
				$this->controlShow[3] = false;
			}
			else
				$this->controlShow = array(true,true,true,false,true,true,true);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					$this->controlShow = array(true,true,true,true,true,false,true);
					break;
			}
		}
  }
  
  ########################################################################

  function GET_SPArr($StudentID=""){
    $SPArr = ($StudentID=="") ? $this->SPArr : $this->SPArr[$StudentID];
    return $SPArr;
  }
  
  function GET_SubjArr(){
    return $this->SubjArr;
  }
  
  function GET_SubjectInfoOrderedArr(){
  	return $this->SubjectInfoOrderedArr;
  }
  
  function GET_SubjectFullMarkArr(){
  	return $this->SubjectFullMarkArr;
  }
  
  function GET_YearArr(){
    return $this->YearArr;
  }
  
  function GET_APArr($StudentID=""){
    $APArr = ($StudentID=="") ? $this->APArr : $this->APArr[$StudentID];
    return $APArr;
  }
  
  function GET_AwardArr($StudentID=""){
    $AwardArr = ($StudentID=="") ? $this->AwardArr : $this->AwardArr[$StudentID];
    return $AwardArr;
  }
  
  function GET_ActivityArr($StudentID=""){
    $ActivityArr = ($StudentID=="") ? $this->ActivityArr : $this->ActivityArr[$StudentID];
    return $ActivityArr;
  }
  
  function GET_OLEArr($StudentID=""){
    $OLEArr = ($StudentID=="") ? $this->OLEArr : $this->OLEArr[$StudentID];
    return $OLEArr;
  }
  
  function GET_ExtPerformArr($StudentID=""){
    $ExtPerformArr = ($StudentID=="") ? $this->ExtPerformArr : $this->ExtPerformArr[$StudentID];
    return $ExtPerformArr;
  }
  
  function GET_SelfAccountArr($StudentID=""){
    $SelfAccountArr = ($StudentID=="") ? $this->SelfAccountArr : $this->SelfAccountArr[$StudentID];
    return $SelfAccountArr;
  }

  function GET_PRINTABLE_OLE_RECORD($result, $StudentID, $IntExt=0, $ReOrder=true)
  {
    global $lpf_slp;
  
    #####################################################################
		# Eric Yip (20090205): Check the number of records can be print
		#####################################################################
		$StudentLevel = $this->GET_ClassLevel($StudentID);
		$OLEAssignedArr = $lpf_slp->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt);
		$OLESettings = $lpf_slp->GET_OLE_SETTINGS_SLP($IntExt);

//debug_r($OLEAssignedArr);
//debug_r($OLESettings);
//debug($StudentLevel, $StudentID);
//debug_r($OLESettings[$StudentLevel][0] );
		if(is_array($OLESettings))
		{
			$IsPrintAll = false;
			if($OLESettings[$StudentLevel][0]=='')
			{
				$IsPrintAll = true;
			}
			else if(!empty($OLEAssignedArr)) // There are records assigned in pool
			{
				for($i=0, $i_max=count($result); $i<$i_max; $i++)
				{
					if(in_array($result[$i]['RecordID'], $OLEAssignedArr))
					{
						$temp_result[$result[$i]['SLPOrder']-1] = $result[$i];
					}
				}
				if(is_array($temp_result))
				{
					if ($ReOrder)
					{
				  		ksort($temp_result);
					}
				  $result = array_values($temp_result);
				}
				
				if($OLESettings[$StudentLevel][0] != "")
				{
					$NumRecordAllowed = MIN(count($result), $OLESettings[$StudentLevel][0]);
				}
				else
				{
					$NumRecordAllowed = count($result);
				}
			}
			// Number of records limited in config
			// Eric Yip (20100721): print no records instead of printing random records upto limit
			else if($OLESettings[$StudentLevel][0] != "")
			{
				$NumRecordAllowed = ($OLESettings["NoPrintToLimit"] == 1) ? 0 : $OLESettings[$StudentLevel][0];
        $result = ($NumRecordAllowed == 0) ? array() : $result;
			}
			// No record number limit
			else
				$IsPrintAll = true;
		}
		else
			$IsPrintAll = true;
		#####################################################################
  
    return array($result, $NumRecordAllowed, $IsPrintAll);
  }

  private function GET_ClassLevel($StudentID)
  {
    global $intranet_db;
    
    $sql =  "
              SELECT
                y.YearName
              FROM
                {$intranet_db}.INTRANET_USER iu
              INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
                ON iu.UserID = ycu.UserID
              INNER JOIN {$intranet_db}.YEAR_CLASS yc
                ON ycu.YearClassID = yc.YearClassID
              INNER JOIN {$intranet_db}.YEAR y
                ON yc.YearID = y.YearID
              WHERE
                yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
                iu.UserID = {$StudentID}
            ";
    $returnVal = current($this->returnVector($sql));
    
    return $returnVal;
  }
  function setReportLang($ParLang="") {
  	if (in_array(strtoupper($ParLang),array("EN","B5","BI"))) {
  		$this->report_lang = $ParLang;
  	}
  }
  function getReportLang(){
	  return $this->report_lang;
  }
	/**
	 * Show words in SLP report
	 * can set $this->report_lang to "EN"/"B5"/"BI" for different lang display
	 * @param String $ParWordString e.g. $slpLang['SysMgr']['FormClassMapping']['ClassTitle'], then "['SysMgr']['FormClassMapping']['ClassTitle']" is required
	 * @param String $ParPrefix The prefix for the words display
	 * @param String $ParPostfix The profix for the words display
	 * @param String $ParDirection The display format in V(vertical) / H(horizontal)
	 * @param String $ParPrior B5/EN Which Lang 1st
	 * @return String The words required
	 */
	function BiLangOut($ParWordString,$ParPrefix="",$ParPostfix="",$ParDirection="V",$ParPrior="B5",$ParLang="") {
		global $slpLang;
		
		$ParWordString = str_replace("\"","'",$ParWordString);
		$langUse = empty($ParLang)?$this->report_lang:$ParLang;

		switch(strtoupper($langUse)) {
			default:
			case "BI":
				eval("\$LangPrior = '$ParPrefix'.\$slpLang{$ParWordString}['{$ParPrior}']$ParPostfix;");
				
				if ($ParPrior == "B5" || empty($ParPrior)) {
					eval("\$LangLater = '$ParPrefix'.\$slpLang{$ParWordString}['EN']{$ParPostfix};");
				} else if ($ParPrior == "EN"){
					eval("\$LangLater = '$ParPrefix'.\$slpLang{$ParWordString}['B5']{$ParPostfix};");
				}
				if (!empty($ParDirection) && $ParDirection == "H") {
					$returnString = "$LangPrior $LangLater";
				} else {
					$returnString = "$LangPrior<br/>$LangLater";
				}
			break;
			case "EN":
				eval("\$returnString = '$ParPrefix'.\$slpLang{$ParWordString}['EN']{$ParPostfix};");
			break;
			case "B5":
				eval("\$returnString = '$ParPrefix'.\$slpLang{$ParWordString}['B5']{$ParPostfix};");
			break;
		}
	
//		$returnString = "<span style='background-color:red'>$returnString</span>";
		return $returnString;
	}
}

?>