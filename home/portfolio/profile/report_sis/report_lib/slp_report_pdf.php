<?php
/** [Modification Log] Modifying By:  connie
 * *******************************************
 * 2012-02-17 Ivan
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT, added para $displayFullMark to add show / hide full mark display logic for SLP report
 * 
 * 2012-02-16 Connie
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT, added $ShowSchoolImage and $SchooImage_html to display School Header image for SLP report
 * 
 * 2012-02-15 Ivan [CRM:2012-0215-1622-23073]
 * - modified GEN_OLE_TABLE_SLP_PDF, GEN_EXTPERFORM_TABLE_SLP to align title and data to center
 * 
 * 2011-06-16 Ivan [CRM:2011-0526-0927-30071]
 * - modified GEN_OLE_TABLE_SLP_PDF to widden the Title Column of the OLE Section
 * 
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT, subject ordering follows the School Settings now
 * 
 * 2010-09-06 Max (201009060900)
 * - Modified GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT for showing custom student info
 * 
 * 2010-06-23 Max (201006231040) Bookmark: 201006231040
 * - Function Header() overrided in class DYN_SLP_TCPDF to clear the header line
 * 
 * 2010-06-04 Max (201006041431)
 * - Handling manuall set page break settings
 * *******************************************
 */
include_once($intranet_root."/includes/tcpdf/tcpdf.php");
include_once($intranet_root."/includes/portfolio25/lib-portfolio_settings.php");
//include_once("../../../../includes/tcpdf.201007141704/tcpdf.php");

class DYN_SLP_TCPDF extends TCPDF {
	
	public function __construct() {
		parent::__construct();
	} 
	public function Footer() {
		global $sys_custom;
		
		if($sys_custom['iPf']['SLPReport']['PrintPageNumber']){

				// print page number
				$this->SetY(-10);
				$ormargins = $this->getOriginalMargins();
				//Print page number
				if (empty($this->pagegroups)) {
					$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
				} else {
					$pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
				}		
					
				if ($this->getRTL()) {
					$this->SetX($ormargins['right']);
					$this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
				} else {
					$this->SetX($ormargins['left']);
					$this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
				}
		}
	}
	public function Header() {
		$ormargins = $this->getOriginalMargins();
		$headerfont = $this->getHeaderFont();
		$headerdata = $this->getHeaderData();
		if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
			$this->Image(K_PATH_IMAGES.$headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
			$imgy = $this->getImageRBY();
		} else {
			$imgy = $this->GetY();
		}
		$cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
		// set starting margin for text data cell
		if ($this->getRTL()) {
			$header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
		} else {
			$header_x = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
		}
		$this->SetTextColor(0, 0, 0);
		// header title
		$this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
		$this->SetX($header_x);			
		$this->Cell(0, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
		// header string
		$this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
		$this->SetX($header_x);
		$this->MultiCell(0, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false);
		
		
		# *This is the overrided part [Bookmark: 201006231040]
		// print an ending header line
//		$this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
//		$this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
//		if ($this->getRTL()) {
//			$this->SetX($ormargins['right']);
//		} else {
//			$this->SetX($ormargins['left']);
//		}
//		$this->Cell(0, 0, '', 'T', 0, 'C');
	}
}
class slp_report_pdf extends slp_report{

  var $pdf;

  function __construct(){
    parent::__construct();
  
		/*generate PDF*/
		$this->Reset_PDF_Object();
//		$this->pdf = new DYN_SLP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
//
//		// set header and footer fonts
//		$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//
//		// set default monospaced font
//		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
//
//		//set margins
//		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//		$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//
//		//set auto page breaks
//		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//
//		//set image scale factor
//		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
//
//		//set some language-dependent strings
//		$this->pdf->setLanguageArray($l); 
//
//			// ---------------------------------------------------------
//
//		// set font
//		//$pdf->SetFont('helvetica', '', 8);
//		//$pdf->SetFont('arialunicid0', 'U', 20);
////		$this->pdf->SetFont('arialunicid0', '', 8);
//		$this->pdf->SetFont('droidsansfallback', '', 8);
////		$this->pdf->SetFont('arialunicid0-chi', '', 8);
  }
  
	private function Reset_PDF_Object() {
		$this->pdf = new DYN_SLP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
	
		// set header and footer fonts
		$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
		//set auto page breaks
		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
		//set image scale factor
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
	
		//set some language-dependent strings
		$this->pdf->setLanguageArray($l); 
	
			// ---------------------------------------------------------
	
		// set font
		//$pdf->SetFont('helvetica', '', 8);
		//$pdf->SetFont('arialunicid0', 'U', 20);
	//		$this->pdf->SetFont('arialunicid0', '', 8);
		$this->pdf->SetFont('droidsansfallback', '', 8);
	//		$this->pdf->SetFont('arialunicid0-chi', '', 8);
	}
  
	# Generate Student Learning Profile for Printing
	function GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($ParStudentID, $ParIssueDate="", $ParYear="", $ParPrintIssueDate, $IsZip=0, $academicScoreDisplayMode='', $displayFullMark=true, $displayComponentSubject=false)
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName, $eclass_root, $sys_custom,$Lang,$iPort, $intranet_root, $PATH_WRT_ROOT, $ipf_cfg;
		
		$ipf_setting = new iportfolio_settings();
		$ShowSchoolImage = $ipf_setting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"]);
		
		$academicScoreDisplayMode = ($academicScoreDisplayMode=='')? $ipf_cfg['slpAcademicResultPrintMode_default'] : $academicScoreDisplayMode;
		
		if ($IsZip==1) {
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$libfs = new libfilesystem();
			
			$TempFolder = $intranet_root."/file/temp/ipf_slp_report_pdf";
			$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
		}
		
	    $this->SET_MODULE_ORDER();
	    $this->SET_MODULE_PAGE_BREAK_SETTING();
	    $this->SET_DETAIL_SETTINGS();
		$this->setPrintFormat('pdf');
		$SchooImage_html = $this->GEN_REPORT_HEADER_SLP_IMAGE($ParIssueDate,$ParPrintIssueDate);
		
		$sizeOfStudentID = count($ParStudentID);
		# Generate Main Content
		$pdfFormat = '';
		for($i=0; $i<$sizeOfStudentID; $i++)
		{
			if ($IsZip==1) {
				$this->Reset_PDF_Object();
				$pdfFormat = '';
			}
			
			$pdfFormat .= "<!-- PDF_BREAK -->";  //start new page content	
			
			$StudentID = $ParStudentID[$i];
			$this->setReportStudentId($StudentID);
			
			$SPArr = $this->GET_SPArr($StudentID);
			$SubjArr = $this->GET_SubjArr();
			$YearArr = $this->GET_YearArr();
			$APArr = $this->GET_APArr($StudentID);
			$OLEArr = $this->GET_OLEArr($StudentID);
			$ActivityArr = $this->GET_ActivityArr($StudentID);
			$AwardArr = $this->GET_AwardArr($StudentID);
			$ExtPerformArr = $this->GET_ExtPerformArr($StudentID);
			$SelfAccountArr = $this->GET_SelfAccountArr($StudentID);
			$SubjectInfoOrderedArr = $this->GET_SubjectInfoOrderedArr();
			$SubjectFullMarkArr = $this->GET_SubjectFullMarkArr();

			if($ShowSchoolImage && $SchooImage_html!='')
			{
				$pdfFormat .=	$SchooImage_html;
			}
			else
			{
				$pdfFormat .= $this->GEN_REPORT_HEADER_SLP_PDF($ParIssueDate, $SPArr['SchoolAddress'], $ParYear,$ParPrintIssueDate);
			}		
			
			for($j=0; $j<count($this->controlOrder); $j++)
			{	
				$module_name = $this->ModuleTitle[$this->controlOrder[$j]];
				$show = $this->controlShow[$this->controlOrder[$j]];
				$isPageBreak = $this->pageBreakSetting[$this->controlOrder[$j]]["IS_SET_PAGE_BREAK"];


				if ($show && $isPageBreak) {
					$pdfFormat .= "<!-- PDF_BREAK -->";
//					$this->pdf->writeHTML("", true, false, false, false, '');
				}

				$ReturnStr .=	"<tr><td valign='top'>";
				$pdfFormat .= "<br/>";	

				switch($module_name)
				{
					# Student Particulars
					case "student_particular":
						if($show)
						{
							$pdfFormat .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF($SPArr, $this->detail_settings[$this->controlOrder[$j]]);
						}
						break;

					# Academic Performance
					case "academic_performance":
						if($show)
						{
							$pdfFormat .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($this->SubjArr, $this->YearArr, $APArr, $SubjectInfoOrderedArr, $SubjectFullMarkArr, $academicScoreDisplayMode, $displayFullMark, $ParEmptySymbol=null, $displayComponentSubject);
							//$pdfFormat .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF($this->SubjArr, $this->YearArr, $APArr, $SubjectInfoOrderedArr);
						}
						break;
					
					# OLE
					case "ole":
						if($show)
						{
							$pdfFormat .= $this->GEN_OLE_TABLE_SLP_PDF($OLEArr, $StudentID);
						}
						break;
						
					# Extra-curricular activity, Customization for lasalle
					case "activity":
						if($show)
						{
							$pdfFormat .= $this->GEN_ACTIVITY_TABLE_SLP_PDF($ActivityArr);
						}
						break;
					
					# Awards and Achievements in school
					case "award_in_school":
						if($show)
						{
							$pdfFormat .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP_PDF($AwardArr);
						}
						break;
						
					# Awards and Achievements outside school
					case "award_outside_school":
						if($show)
						{
							//$pdfFormat .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL_PDF($StudentID, convertSystemYearToOLEYear($ParYear));
							$pdfFormat .= $this->GEN_EXTPERFORM_TABLE_SLP($ExtPerformArr, $StudentID);
						}
						break;
											
					# Self Account
					case "self_account":
						if($show)
						{
							$pdfFormat .= $this->GEN_SELF_ACCOUNT_TABLE_SLP_PDF($SelfAccountArr);
						}
						break;
				}		
			}			

			//debug($pdfFormat);
			$pdfFormat .= $this->GEN_REPORT_FOOTER_SLP_PDF();
			
			if($this->broken_tags($pdfFormat))
			{
				echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
				echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">';
				echo "<head><title>Testing</title></head>\n";
				echo "<body>\n";
				echo "<p style=\"color:red\">data is broken</p>\n";
				echo $pdfFormat;
				echo "</body>\n";
				echo "</html>\n";
//				debug($pdfFormat);
			}
//			$this->pdf->writeHTML($pdfFormat, true, false, false, false, '');	


			if ($IsZip==1) {
				$libuser = new libuser($StudentID);
				
				$thisClassName = $libuser->ClassName;
				$thisClassNumber = $libuser->ClassNumber;
				$thisFileName = $thisClassName.'_'.$thisClassNumber.'.pdf';
				
				$TempPDFFolder = $TempUserFolder.'/slp_report_'.$thisClassName;
				if (!file_exists($TempPDFFolder)) {
					$SuccessArr['CreateTempPDFFolder'] = $libfs->folder_new($TempPDFFolder);
				}
				$thisPDFFilePath = $TempPDFFolder.'/'.$thisFileName;
				
				
				$delimiter = '<!-- PDF_BREAK -->';
				$chunks    = explode($delimiter, $pdfFormat);
				$cnt       = count($chunks);
				for ($k = 0; $k < $cnt; $k++) {
				    $this->pdf->writeHTML($delimiter . $chunks[$k], true, false, false, false, '');
				
				    if ($k < $cnt - 1) {
				        $this->pdf->AddPage();
				    }
				}
				$this->pdf->Output($thisPDFFilePath, 'F');
			}

		}//END OF EACH STDUENT
		
		/*
		test for pdf
		*/
		// add a page
		
		if ($IsZip==1) {
			$ZipFileNameSuffix = '_'.$thisClassName;
			$ZipFileName = "slp_report".$ZipFileNameSuffix.".zip";
			$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;

			### Delete old zip file and zip the current csv files
			$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
			$SuccessArr['ZipPdfFiles'] = $libfs->file_zip('slp_report'.$ZipFileNameSuffix, $ZipFilePath, $TempUserFolder);
				
			### remove the folder
			$SuccessArr['DeleteTempPdfFiles'] = $libfs->folder_remove_recursive($TempPDFFolder);
			
			### output the zip file to browser and let the user download it.
			output2browser(get_file_content($ZipFilePath), $ZipFileName);
			die();
		}
		else {
			###################################
			## Start: handling page break
			###################################
			$delimiter = '<!-- PDF_BREAK -->';
			$chunks    = explode($delimiter, $pdfFormat);
			$cnt       = count($chunks);
			
			for ($i = 0; $i < $cnt; $i++) {
			    $this->pdf->writeHTML($delimiter . $chunks[$i], true, false, false, false, '');
			
			    if ($i < $cnt - 1) {
			        $this->pdf->AddPage();
			    }
			}
			###################################
			## End: handling page break
			###################################
					
			$this->pdf->Output('testing1.pdf', 'I');
			DIE();  // hard code to exist first
		}
	}
  
	# St.Antonious Girls College Customization
	function GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT_SAGC($ParStudentID, $ParIssueDate="", $ParYear="", $ParPrintIssueDate, $IsZip=0, $academicScoreDisplayMode='', $displayFullMark=true, $displayComponentSubject=false)
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName, $eclass_root, $sys_custom,$Lang,$iPort, $intranet_root, $PATH_WRT_ROOT, $ipf_cfg;
		
		$ipf_setting = new iportfolio_settings();
		
		if ($IsZip==1) {
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$libfs = new libfilesystem();
			
			$TempFolder = $intranet_root."/file/temp/ipf_slp_report_pdf";
			$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
		}
		
	    $this->SET_MODULE_ORDER();
	    $this->SET_MODULE_PAGE_BREAK_SETTING();
	    $this->SET_DETAIL_SETTINGS();
		$this->setPrintFormat('pdf');
		$SchooImage_html = $this->GEN_REPORT_HEADER_SLP_IMAGE($ParIssueDate,$ParPrintIssueDate);
		
		$sizeOfStudentID = count($ParStudentID);
		
		//set module display
		/*
		$this->ModuleTitle = array();
		$this->ModuleTitle[] = "student_particular";
		$this->ModuleTitle[] = "academic_performance";
		$this->ModuleTitle[] = "ole";
		$this->ModuleTitle[] = "award_in_school";
		$this->ModuleTitle[] = "award_outside_school";
		$this->ModuleTitle[] = "self_account";
		$this->controlOrder = array(0,1,2,3,4,5);
		*/
		//Signature HTML
		$signatureHTML = "
						<table width=\"100%\">
						<tr>	
							<td align=\"center\">____________________________________</td>	
							<td align=\"center\">____________________________________</td>	
							<td align=\"center\">____________________________________</td>	
						</tr>
						<tr>	
							<td align=\"center\">Principal 校長</td>	
							<td align=\"center\">Class Teacher 班主任</td>	
							<td align=\"center\">School Chop 校印</td>
						</tr>
						</table>
						";
		$signatureStartY = 264;				
		
		
		# Generate Main Content
		$pdfFormat = '';
		for($i=0; $i<$sizeOfStudentID; $i++)
		{
			if ($IsZip==1) {
				$this->Reset_PDF_Object();
				$pdfFormat = '';
			}
				
			
			$StudentID = $ParStudentID[$i];
			$this->setReportStudentId($StudentID);
			
			$SPArr = $this->GET_SPArr($StudentID);
			$APArr = $this->GET_APArr($StudentID);
			$OLEArr = $this->GET_OLEArr($StudentID);
			$ActivityArr = $this->GET_ActivityArr($StudentID);
			$AwardArr = $this->GET_AwardArr($StudentID);
			$ExtPerformArr = $this->GET_ExtPerformArr($StudentID);
			$SelfAccountArr = $this->GET_SelfAccountArr($StudentID);
			$SubjectInfoOrderedArr = $this->GET_SubjectInfoOrderedArr();
			$SubjectFullMarkArr = $this->GET_SubjectFullMarkArr();
			
			$pdfFormat .= "<!-- PDF_BREAK -->";  //start new page content
			
			if($SchooImage_html!='')
			{
				$pdfFormat .=	$SchooImage_html;
			}
			else
			{
				$pdfFormat .= $this->GEN_REPORT_HEADER_SLP_PDF($ParIssueDate, $SPArr['SchoolAddress'], $ParYear,$ParPrintIssueDate);
			}		
			
			
			for($j=0; $j<count($this->controlOrder); $j++)
			{	
				$module_name = $this->ModuleTitle[$this->controlOrder[$j]];
				
				$show = $this->controlShow[$this->controlOrder[$j]];
				$isPageBreak = $this->pageBreakSetting[$this->controlOrder[$j]]["IS_SET_PAGE_BREAK"];
				//$isPageBreak = $this->pageBreakSetting[$this->controlOrder[$j]]["IS_SET_PAGE_BREAK"];


				if ($show && $isPageBreak) {
					$pdfFormat .= "<!-- PDF_BREAK -->";
//					$this->pdf->writeHTML("", true, false, false, false, '');
				}

				$ReturnStr .=	"<tr><td valign='top'>";
				$pdfFormat .= "<br/>";	

				switch($module_name)
				{
					# Student Particulars
					case "student_particular":
						if($show){
							$pdfFormat .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF_SAGC($SPArr, $this->detail_settings[$this->controlOrder[$j]]);
						}	
					break;

					# Academic Performance
					case "academic_performance":
						if($show){
							$pdfFormat .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($this->SubjArr, $this->YearArr, $APArr, $SubjectInfoOrderedArr, $SubjectFullMarkArr, $academicScoreDisplayMode, $displayFullMark, $ParEmptySymbol=null, $displayComponentSubject);
						}
					break;
					
					# OLE
					case "ole":
						if($show){
							$pdfFormat .= $this->GEN_OLE_TABLE_SLP_PDF($OLEArr, $StudentID);
						}	
					break;
						
					# Extra-curricular activity, Customization for lasalle
					case "activity":
						if($show){
							$pdfFormat .= $this->GEN_ACTIVITY_TABLE_SLP_PDF($ActivityArr);
						}
						break;
					
					# Awards and Achievements in school
					case "award_in_school":
						$pdfFormat .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP_PDF($AwardArr);
					break;
						
					# Awards and Achievements outside school
					case "award_outside_school":
						if($show){
							//$pdfFormat .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL_PDF($StudentID, convertSystemYearToOLEYear($ParYear));
							$pdfFormat .= $this->GEN_EXTPERFORM_TABLE_SLP($ExtPerformArr, $StudentID);
						}
					break;
											
					# Self Account
					case "self_account":
						if($show){
							$pdfFormat .= $this->GEN_SELF_ACCOUNT_TABLE_SLP_PDF($SelfAccountArr);
						}
						//$tb = $this->GEN_SELF_ACCOUNT_TABLE_SLP_PDF($SelfAccountArr);
						//$pdfFormat .= "<table border=\"1\"><tr><td height=\"100%\">".$tb."</td></tr></table>";
					break;
							
				}		
			}			

			//debug($pdfFormat);
			$pdfFormat .= $this->GEN_REPORT_FOOTER_SLP_PDF();
			
			if($this->broken_tags($pdfFormat))
			{
				echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
				echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">';
				echo "<head><title>Testing</title></head>\n";
				echo "<body>\n";
				echo "<p style=\"color:red\">data is broken</p>\n";
				echo $pdfFormat;
				echo "</body>\n";
				echo "</html>\n";
//				debug($pdfFormat);
			}
//			$this->pdf->writeHTML($pdfFormat, true, false, false, false, '');	


			if ($IsZip==1) {
				$libuser = new libuser($StudentID);
				
				$thisClassName = $libuser->ClassName;
				$thisClassNumber = $libuser->ClassNumber;
				$thisFileName = $thisClassName.'_'.$thisClassNumber.'.pdf';
				
				$TempPDFFolder = $TempUserFolder.'/slp_report_'.$thisClassName;
				if (!file_exists($TempPDFFolder)) {
					$SuccessArr['CreateTempPDFFolder'] = $libfs->folder_new($TempPDFFolder);
				}
				$thisPDFFilePath = $TempPDFFolder.'/'.$thisFileName;
				
				
				$delimiter = '<!-- PDF_BREAK -->';
				$chunks    = explode($delimiter, $pdfFormat);
				$cnt       = count($chunks);
				for ($k = 0; $k < $cnt; $k++) {
				    $this->pdf->writeHTML($delimiter . $chunks[$k], true, false, false, false, '');
				
				    if ($k+1==$cnt){
						if($this->pdf->getY()>$signatureStartY){
						     $this->pdf->AddPage();
						}
						$this->pdf->setY($signatureStartY);
						$this->pdf->writeHTML($signatureHTML, true, false, false, false, '');
				    }
				    if ($k < $cnt - 1) {
				        $this->pdf->AddPage();
				    }
				}	
				$this->pdf->Output($thisPDFFilePath, 'F');
			}

		}//END OF EACH STDUENT
		
		/*
		test for pdf
		*/
		// add a page
		
		if ($IsZip==1) {
			$ZipFileNameSuffix = '_'.$thisClassName;
			$ZipFileName = "slp_report".$ZipFileNameSuffix.".zip";
			$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;

			### Delete old zip file and zip the current csv files
			$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
			$SuccessArr['ZipPdfFiles'] = $libfs->file_zip('slp_report'.$ZipFileNameSuffix, $ZipFilePath, $TempUserFolder);
				
			### remove the folder
			$SuccessArr['DeleteTempPdfFiles'] = $libfs->folder_remove_recursive($TempPDFFolder);
			
			### output the zip file to browser and let the user download it.
			output2browser(get_file_content($ZipFilePath), $ZipFileName);
			die();
		}
		else {
			###################################
			## Start: handling page break
			###################################
			$delimiter = '<!-- PDF_BREAK -->';
			$chunks    = explode($delimiter, $pdfFormat);
			$cnt       = count($chunks);
			
			for ($i = 0; $i < $cnt; $i++) {
			    $this->pdf->writeHTML($delimiter . $chunks[$i], true, false, false, false, '');
						    
			    if ($i+1==$cnt){
					if($this->pdf->getY()>$signatureStartY){
					     $this->pdf->AddPage();
					}
					$this->pdf->setY($signatureStartY);
					$this->pdf->writeHTML($signatureHTML, true, false, false, false, '');
			    }
			    if ($i < $cnt - 1) {
			        $this->pdf->AddPage();
			    }
			}
			
			
			
			###################################
			## End: handling page break
			###################################
					
			$this->pdf->Output('testing1.pdf', 'I');
			DIE();  // hard code to exist first
		}
	}
	
	# Generate OLE table
	function GEN_OLE_TABLE_SLP_PDF($ParOLEArr="", $StudentID="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp, $sys_custom;

		$emptyRecTable = <<<HTML
<table width="100%" border="1" cellspacing="0">
	<tr>
		<td height="100" valign="middle" align="center">--</td>
	</tr>
</table>
HTML;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParOLEArr) && !empty($ParOLEArr))
			{
    		list($result, $NumRecordAllowed, $IsPrintAll) = $this->GET_PRINTABLE_OLE_RECORD($ParOLEArr, $StudentID, 0);
    		
  		  	$style_detail = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "font-style:italic;" : "";
  		  	$style_title_general = "text-align:center;";
//  		  	$style_title_org = $style_title_general;
			if($sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'] != ''){

				$style_title_general = "text-align:".$sys_custom['iPf']['SLP']['OLE_ORG_ALIGN_VALUE'].";";
			}

        	$t_program_title = $this->BiLangOut("['ProgrammesWithDescription']");
		    $t_program_title .= $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? $this->BiLangOut("['SelectedRecords']","","","V","B5","EN") : "";
		    
    		
        $ele_arr = $lpf_slp->GET_ELE();
        $ele_code_arr = array_keys($ele_arr);
        $ele_title_arr = array_values($ele_arr);

        if(count($result) > 0)
        {

			//for org title not align to left ,it looks not good , so don't apply for title
			//$style_title_org

  				$ContentStr .=	"
  													<table width=\"100%\" border=\"1\" cellspacing=\"0\">
  														<thead>
  														<tr>
  															<td style=\"width:30%; font-size=8pt;\"><strong>{$t_program_title}</strong></td>
  															<td style=\"width:10%; font-size=8pt;{$style_title_general}\"><strong>{$this->BiLangOut("['SchoolYear']")}</strong></td>
  															<td style=\"width:15%; font-size=8pt;{$style_title_general}\"><strong>{$this->BiLangOut("['RoleOfParticipation']")}</strong></td>
  															<td style=\"width:15%; font-size=8pt;{$style_title_general}\"><strong>{$this->BiLangOut("['PartnerOrganizationsIfAny']")}</strong></td>
  															<td style=\"width:15%; font-size=8pt;{$style_title_general}\"><strong>{$this->BiLangOut("['EssentialLearningExperiences']")}</strong></td>
  															<td style=\"width:15%; font-size=8pt;{$style_title_general}\"><strong>{$this->BiLangOut("['AchievementsIfAny']")}</strong></td>
  														</tr>
  														</thead>
  												";
	
  				for($i=0; $i<count($result); $i++)
  				{
  				  if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
  				  	
  				  	$t_program_title = Get_Standardized_PDF_Table_Data_Display($result[$i]['Title']);
		    
  				  $t_year_str = (trim($result[$i]['Year']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Year']);
  				  $t_role_str = (trim($result[$i]['Role']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Role']);
  				  $t_org_str = (trim($result[$i]['Organization']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Organization']);
  				  $t_ach_str = (trim($result[$i]['Achievement']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Achievement']);
  				
  				  $t_ele_str = str_replace($ele_code_arr, $ele_title_arr, $result[$i]['ELE']);
  				  $t_ele_str = str_replace(",", ", ", $t_ele_str);
  				  $t_ele_str = (trim($t_ele_str) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($t_ele_str);
  				  
  				  $t_remark_str = (trim($result[$i]['Remark']) == "") ? "--" : $result[$i]['Remark'];
  				  $t_remark_str = intranet_htmlspecialchars($t_remark_str);
  				
  					$ContentStr .=	"
  														<tr nobr=\"true\">
  														  <td style=\"width:30%; font-size=8pt\">{$t_program_title}<hr />{$t_remark_str}</td>
  															<td rowspan=\"1\" style=\"width:10%; font-size=8pt;{$style_title_general}\">{$t_year_str}</td>
  															<td rowspan=\"1\" style=\"width:15%; font-size=8pt;{$style_title_general}\">{$t_role_str}</td>
  															<td rowspan=\"1\" style=\"width:15%; font-size=8pt;{$style_title_general}\">{$t_org_str}</td>
  															<td rowspan=\"1\" style=\"width:15%; font-size=8pt;{$style_title_general}\">{$t_ele_str}</td>
  															<td rowspan=\"1\" style=\"width:15%; font-size=8pt;{$style_title_general}\">{$t_ach_str}</td>
  														</tr>
  													";
  				}			
  				$ContentStr .=	"</table>";
  			}
  			else
  			{
  				$ContentStr = $emptyRecTable;
  			}
			}
			else
				$ContentStr = $emptyRecTable;

			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['OtherLearningExperiences']"), $this->BiLangOut("['OLE_TableDescription']"), $ContentStr);
		}
		else
		{

		}
		
		return $ReturnStr;
	}
	
	# Generate External Performace table
	function GEN_EXTPERFORM_TABLE_SLP($ParExtPerformArr="", $StudentID="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp, $sys_custom;
		
		$emptyRecTable = <<<HTML
<table width="100%" border="1" cellspacing="0">
	<tr>
		<td height="100" valign="middle" align="center">--</td>
	</tr>
</table>
HTML;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParExtPerformArr) && !empty($ParExtPerformArr))
			{
    		list($result, $NumRecordAllowed, $IsPrintAll) = $this->GET_PRINTABLE_OLE_RECORD($ParExtPerformArr, $StudentID, 1);
    		
		    $style_detail = $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? "font-style:italic;" : "";
		    $style_title_general = "text-align:center;";
		    
		    $t_program_title = $this->BiLangOut("['ProgrammesWithDescription']");
		    $t_program_title .= $sys_custom['iPortfolio_SLP_Report_minor']['uccke'] ? $this->BiLangOut("['SelectedRecords']","","","V","B5","EN") : "";

        if(count($result) > 0)
        {
  				$ContentStr .=	"
  													<table width=\"100%\" border=\"1\" cellspacing=\"0\">
  														<thead>
  														<tr>
  															<td style=\"font-size=8pt;\"><strong>".$t_program_title."</strong></td>
  															<td style=\"font-size=8pt;{$style_title_general}\"><strong>".$this->BiLangOut("['SchoolYear']")."</strong></td>
  															<td style=\"font-size=8pt;{$style_title_general}\"><strong>".$this->BiLangOut("['RoleOfParticipation']")."</strong></td>
  															<td style=\"font-size=8pt;{$style_title_general}\"><strong>".$this->BiLangOut("['Organization']")."</strong></td>
  															<td style=\"font-size=8pt;{$style_title_general}\"><strong>".$this->BiLangOut("['AchievementsIfAny']")."</strong></td>
  														</tr>
  														</thead>
  												";
	
  				for($i=0; $i<count($result); $i++)
  				{
  				  if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
  				  
  				  $t_program_title = Get_Standardized_PDF_Table_Data_Display($result[$i]['Title']);
  				  $t_year_str = (trim($result[$i]['Year']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Year']);
  				  $t_role_str = (trim($result[$i]['Role']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Role']);
  				  $t_org_str = (trim($result[$i]['Organization']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Organization']);
  				  $t_ach_str = (trim($result[$i]['Achievement']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Achievement']);
  				  
  				  $t_remark_str = (trim($result[$i]['Remark']) == "") ? "--" : Get_Standardized_PDF_Table_Data_Display($result[$i]['Remark']);

  						$ContentStr .=	"
  														<tr nobr=\"true\">
  														  <td style=\"font-size=8pt;\">".$t_program_title."<hr />{$t_remark_str}</td>
  															<td rowspan=\"1\" style=\"font-size=8pt;{$style_title_general}\">{$t_year_str}</td>
  															<td rowspan=\"1\" style=\"font-size=8pt;{$style_title_general}\">{$t_role_str}</td>
  															<td rowspan=\"1\" style=\"font-size=8pt;{$style_title_general}\">{$t_org_str}</td>
  															<td rowspan=\"1\" style=\"font-size=8pt;{$style_title_general}\">{$t_ach_str}</td>
  														</tr>
  													";


 /* 					$ContentStr .=	"
  														<tr nobr=\"true\">
  														  <td style=\"font-size=8pt;\">".$t_program_title."</td>
  															<td rowspan=\"2\" style=\"font-size=8pt;{$style_title_general}\">{$t_year_str}</td>
  															<td rowspan=\"2\" style=\"font-size=8pt;{$style_title_general}\">{$t_role_str}</td>
  															<td rowspan=\"2\" style=\"font-size=8pt;{$style_title_general}\">{$t_org_str}</td>
  															<td rowspan=\"2\" style=\"font-size=8pt;{$style_title_general}\">{$t_ach_str}</td>
  														</tr>
  														<tr valign=\"top\" height=25>
  														  <td style=\"font-size=8pt; {$style_detail}\">{$t_remark_str}</td>
  														</tr>
  													";*/
  				}			
  				$ContentStr .=	"</table>";
  			}
  			else
  			{
  				$ContentStr = $emptyRecTable;
  			}
			}
			else
				$ContentStr = $emptyRecTable;

			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['Performace/Awards']"), $this->BiLangOut("['ExternalOLE_TableDescription']"), $ContentStr);
		}
		else
		{

		}
		$ReturnStr .= "* ".$this->BiLangOut("['RequiredRemark']")."<br />";
		
		return $ReturnStr;
	}
}

?>