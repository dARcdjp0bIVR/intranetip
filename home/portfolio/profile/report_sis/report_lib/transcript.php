<?php
# using: 

############################################

# Date:	2013-05-02 Yuen
#		Fixed: show grade by customized flag: $sys_custom['iPf']['Report']['academicResult']['ShowGrade'] 
#

# Date:	2013-03-15 YatWoon
#		Fixed: modified generateAssessmentReport3() [Case#2013-0314-1107-19073] 
#		Improved: Display term name with interface ui
#		modified: returnAssessmentResult(), retrieve YearTermID
#
###########################################
include_once("../../../../includes/libportfolio.php");
include_once("../../../../includes/libportfolio2007a.php");
include_once("../../../../includes/libpf-report.php");
include_once("../../../../includes/libpf-sturec.php");

class transcript extends libpf_report{

  function generateTranscriptPrint($StudentIDArr, $AcademicYearID="", $YearTermID="", $IsShowApprovedBy = false, $IssueDate="")
	{

		global $eclass_filepath, $admin_url_path, $file_path_name, $li_npf, $image_path, $eclass_httppath;

		############################ Initialization ############################

		// get Report Sheet Style
		$templat_config = $this->getTemplateConfig();
		$ip_school_address = ($templat_config['config']['school_address']) ?  nl2br($templat_config['config']['school_address']) : "&nbsp;";
		$ip_transcript_description = ($templat_config['config']['school_description']) ?  $templat_config['config']['school_description'] : "&nbsp;";
		$PhotoConfig = $templat_config['config']['PhotoConfig'];

		// get Template Path
		$template_path = "/files/portfolio/transcript/template_".$templat_config["template"];
		$template_path_root = $eclass_filepath."/".$template_path;
		$template_path_url = "http://".$eclass_httppath.$admin_url_path."/".$template_path;

		// get CSV Columns
		list($CSV_columns, $CSV_records) = $this->loadCSVData($template_path_root);

		# get school name
		$ip_school_name = GET_SCHOOL_NAME();

		// get Pattern
		$patterns = $this->loadReplacementPattern($templat_config["report_style"], $CSV_columns);

		// Page Break Style
		$page_break_style = "<STYLE TYPE='text/css'>\nP.breakhere {page-break-before: always}\n</STYLE>\n";
		$page_break_here = "<p class='breakhere'></p>\n";

		// Transcript Template
		$template_obj = $this->loadTranscriptTemplate($template_path_root);

		$Student_Records = $this->getStudentRecords($StudentIDArr);
		############################ Each Student's Report ############################

		for ($i=0; $i<sizeof($Student_Records); $i++)
		{
			$student_obj = $Student_Records[$i];
			// retrieve student's data
			$image_new_path1 = "\"".$template_path_url."/images/";
			$image_new_path2 = "'".$template_path_url."/images/";
			$css_new_path1 = "\"".$template_path_url."/css/";
			$css_new_path2 = "'".$template_path_url."/css/";
			$ip_student_name_eng = $student_obj["EnglishName"];
			$ip_date_of_birth = $student_obj["DOB"];
			if ($ip_date_of_birth == "0000-00-00")
			{
				$ip_date_of_birth = "--";
			}
			$ip_place_of_birth = $student_obj["POB"];
			$ip_nationality = $student_obj["Nationality"];
			$ip_address = $student_obj["Address"];
			$ip_admission_date = $student_obj["AdmissionDate"];
			$ip_graduate_date = $student_obj["GraduateDate"];
			$photo = ($StudentIDArr==0) ? "<img src=\"http://{$eclass_httppath}{$image_path}/photo_m.gif\" width=100 height=130 border=1>" : (strpos($student_obj["PhotoImg"], "&nbsp;")?"<div style=\"width:100px;height:130px;border-style:solid;border-width:1px\"><i>Photo Here</i></div>":$student_obj["PhotoImg"]);
			$ip_student_photo = ($PhotoConfig) ? "&nbsp;" : $photo;
		
			// generate the report sheet used
			$ip_report_sheet = $this->getAssessmentReport($student_obj["UserID"], $templat_config['report_style'], $AcademicYearID, $YearTermID);

			# Contents of Replacement (ATTENTION: SAME Order as Patterns to be Replaced)
			// CSV
			$replacements_csv = array();
			$CSV_data = $CSV_records[$student_obj["RegNo"]];
			if ($CSV_data == '') {
				$CSV_data = array('');
			}
			for ($j=0; $j<sizeof($CSV_data); $j++)
			{
				$thisCSVData = ($CSV_data[$j]=='')? '&nbsp;' : $CSV_data[$j];
				
				$replacements_csv[] = $thisCSVData;
				${strtolower($CSV_columns[$j])} = $thisCSVData;
			}
			// iPortfolio
			$IssueDate = ($IssueDate=='')? '&nbsp;' : $IssueDate;
			$replacements = array (
							$image_new_path1,
							$image_new_path2,
							$css_new_path1,
							$css_new_path2,
							$ip_student_name_eng,
							$ip_date_of_birth,
							$ip_place_of_birth,

							$ip_nationality,
							$ip_address,
							$ip_admission_date,
							$ip_graduate_date,
							$ip_student_photo,

							$ip_report_sheet,
							$ip_school_name,
							$ip_school_address,
							$ip_transcript_description,
							$IssueDate
							);

			$replacements = array_merge($replacements, $replacements_csv);

			$rx .= preg_replace($patterns, $replacements, $template_obj["body"]);

			if ($i<sizeof($Student_Records)-1)
			{
				$rx .= $page_break_here;
			}
		}

		############################### All Reports ###############################

		# update Template Header for CSS and Image Paths
		$template_obj["header"] = preg_replace($patterns, $replacements, $template_obj["header"]) . $page_break_style;
		
		# finalize With Header and Footer Merged
		$rx = $template_obj["header"] . $rx . $template_obj["footer"];
		
		return $rx;
  }
  
	# get Student's Assessment Report
	function getAssessmentReport($StudentID, $report_style, $AcademicYearID, $YearTermID){
		// YuEn: will extend to provid more choices
		global $lpf_sturec;
		
		$Printable = true;
		switch ($report_style)
		{
			case "A":
			default:
				$rx = $this->generateAssessmentReport3($StudentID, "", "", "Print", 1, $AcademicYearID, $YearTermID, $Printable);
				break;
		}

		return $rx;
	}
	
	# Generate Assessment Report
	function generateAssessmentReport3($StudentID, $ClassName="", $displayBy="", $displayType="", $cmp_show="", $AcademicYearID="", $YearTermID="", $Printable="", $ParChangeLayer=false)
	{
		global $eclass_db, $ec_iPortfolio, $sr_image_path, $no_record_msg, $ec_iPortfolio_Report, $intranet_root, $ck_alumni_year, $sys_custom;

    $lpf_sturec = new libpf_sturec();
	$ayt = new academic_year_term();
	
		$params = "ClassName=$ClassName&StudentID=$StudentID&my_year=".$ck_alumni_year;

		$valueShow = ($displayBy=="") ? "Score" : trim($displayBy);

		switch ($displayBy)
		{
			case "Score":
				$totalTitle = $ec_iPortfolio['overall_score'];
				break;
			case "Rank":
				$totalTitle = $ec_iPortfolio['overall_rank'];
				break;
			case "StandardScore":
				$totalTitle = $ec_iPortfolio['overall_stand_score'];
				break;
			default:
				$totalTitle = $ec_iPortfolio['overall_grade'];
				break;
		}


		# retrieve all subjects with components
		$row_cmp_subject = ($displayType=="Print" && $cmp_show!=1) ? $lpf_sturec->returnAllSubjects($StudentID) : $lpf_sturec->returnAllSubjectWithComponents($StudentID);
		
		# retrieve subject semester records
		$subject_record = $this->returnAssessmentResult($StudentID, 0, $AcademicYearID, $YearTermID);
		
		if($YearTermID=="")
		{
			# retrieve subject annual records
			$annual_subject_record = $this->returnAssessmentResult($StudentID, 1, $AcademicYearID);
		}

		# retrieve school semesters
		list($full_semester_arr, $semester_arr) = $lpf_sturec->getSemesterNameArrFromIP("", 1);
		if(sizeof($annual_subject_record)!=0)
		{
			$semester_arr[] = trim($ec_iPortfolio['overall_result']);
		}
		
		$class_arr = array();
		$year_arr = array();
		for ($i=0; $i<sizeof($subject_record); $i++)
		{
			$record_obj = $subject_record[$i];
			
			# retrieve semester with AcademicYearID and YearTermID 
			//$sem = trim(preg_replace($lpf_sturec->semester_patterns, "", $record_obj["Semester"]));
			$sem = $ayt->Get_YearNameByID($record_obj["YearTermID"], $_SESSION['intranet_session_language']);
			
			//for safe strtolower for compare
			$sem = strtolower($sem);

			# check if score=0, grade=null and rank=0, the subject was not selected by this student
			$RAW[trim($record_obj["SubjectCode"])]["NotSelected"] = ($record_obj["Score"]==0 && $record_obj["Grade"]=="" && $record_obj["OrderMeritForm"]==0) ? 1 : 0;
			$RAW[trim($record_obj["SubjectCode"])]["NoRank"] = ($displayBy=="Rank" && $record_obj["OrderMeritForm"]<=0) ? 1 : 0;

			if($record_obj["SubjectComponentCode"]=="")
			{
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][$sem]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}
			else
			{
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][$sem][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}

			$class = trim($record_obj["ClassName"]);
			if (!in_array($record_obj["Year"], $year_arr))
			{
				$year_arr[] = trim($record_obj["Year"]);
				$class_arr[] = $class;
				$sem_arr[trim($record_obj["Year"])] = array();
			}

			if(!in_array(trim($sem), $sem_arr[$record_obj["Year"]]))
			{
				$sem_arr[trim($record_obj["Year"])][] = $sem;
			}
		}
		
		for ($i=0; $i<sizeof($annual_subject_record); $i++)
		{
			$record_obj = $annual_subject_record[$i];
			if($record_obj["SubjectComponentCode"]=="")
			{
				//$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))]["major"] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$class = trim($record_obj["ClassName"]);

				if(!in_array($record_obj["Year"], $year_arr))
				{
					$year_arr[] = trim($record_obj["Year"]);
					$class_arr[] = $class;
				}
			}
			else
			{
				//$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
				$RAW[trim($record_obj["SubjectCode"])][trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))][trim($record_obj["SubjectComponentCode"])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			}

			if(sizeof($sem_arr[$record_obj["Year"]])==0)
			{
				$sem_arr[trim($record_obj["Year"])][] = trim($ec_iPortfolio['overall_result']);
				//$sem_arr[trim($record_obj["Year"])][] = "overall result";
			}
			else if(!in_array(strtolower(trim($ec_iPortfolio['overall_result'])), $sem_arr[$record_obj["Year"]]))
			{
				$sem_arr[trim($record_obj["Year"])][] = strtolower(trim($ec_iPortfolio['overall_result']));
				//$sem_arr[trim($record_obj["Year"])][] = "overall result";
			}
		}
		
		# retrieve semester main records
		$main_record = $lpf_sturec->returnAssessmentMainRecord($StudentID, 0);
		
		# retrieve annual main records
		$annual_main_record = $lpf_sturec->returnAssessmentMainRecord($StudentID, 1);

		for ($i=0; $i<sizeof($main_record); $i++)
		{
			$record_obj = $main_record[$i];
			//$sem = preg_replace($lpf_sturec->semester_patterns, "", $record_obj["Semester"]);
			$sem = strtolower($ayt->Get_YearNameByID($record_obj["YearTermID"], $_SESSION['intranet_session_language']));
			$RAW_TOTAL[trim($record_obj["Year"])][trim($sem)] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
		}

		for ($i=0; $i<sizeof($annual_main_record); $i++)
		{
			$record_obj = $annual_main_record[$i];
			//$RAW_TOTAL[trim($record_obj["Year"])][trim($ec_iPortfolio['overall_result'])] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
			$RAW_TOTAL[trim($record_obj["Year"])][strtolower(trim($ec_iPortfolio['overall_result']))] = array("Score"=>$record_obj["Score"], "Grade"=>$record_obj["Grade"], "Rank"=>$record_obj["OrderMeritForm"]);
		}

		// use white border for view and black for printing
		switch ($displayType)
		{
			case "Print":
								$bordercolor = "#000000";
								$title_bgcolor = "#FFFFFF";
								$alternative_row_color = "#FFFFFF";
								$table_class = "";
								break;
			default:
								$bordercolor = "#FFFFFF";
								$title_bgcolor = "#CFE6FE";
								$alternative_row_color = "#EEEEEE";
								$table_class = "class='table_b'";
								break;
		}
		//Table Content

		# table columns
		$rx = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='{$bordercolor}' $table_class ><tr><td>\n";
		if($Printable)
		{
		$rx .= "<table width='100%' border='1' cellpadding='0' cellspacing='0' align='center' class='table_print'>\n";
		}else{
		$rx .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		}

		if (sizeof($RAW)>0)
		{
			//$style_bottom = "style='border-bottom: 1px dashed $stylecolor;'";
			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td>&nbsp;</td>";
			$rx .= "<td align='center' class='tbheading' width='150'>".$ec_iPortfolio_Report['year']."</td>";
			for($i=0; $i<sizeof($year_arr); $i++)
			{
				# number of semester of the year
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td align='center' class='tbheading' colspan=".$semCount.">".$year_arr[$i]."</td>";
			}
			$rx .= "</tr>";

			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td align='center' class='tbheading' rowspan=2>".$ec_iPortfolio_Report['subject']."</td>";
			$rx .= "<td align='center' class='tbheading'>".$ec_iPortfolio['class']."</td>";
			for($i=0; $i<sizeof($class_arr); $i++)
			{
				$semCount = (sizeof($sem_arr[$year_arr[$i]])==0) ? 0 : sizeof($sem_arr[$year_arr[$i]]);
				$rx .= "<td align='center' class='tbheading' colspan=".$semCount.">".$class_arr[$i]."</td>";
			}
			$rx .= "</tr>";

			$rx .= "<tr bgcolor='$title_bgcolor' >\n";
			$rx .= "<td align='center' class='tbheading' width='150'>".$ec_iPortfolio_Report['term']."</td>";
			for($i=0; $i<sizeof($year_arr); $i++)
			{
				$year = $year_arr[$i];
				for($j=0; $j<sizeof($semester_arr); $j++)
				{
					if(sizeof($sem_arr[$year])!=0)
					{

						$trgCheckSemester = strtolower(trim($semester_arr[$j]));
						$srcCheckSem_arr = array_map('strtolower', $sem_arr[$year]);
						
//						$rx .= (in_array(trim($semester_arr[$j]), $sem_arr[$year])) ? "<td align='center' nowrap><span class='tbheading'>".$semester_arr[$j]."</span></td>" : "";
						$rx .= (in_array($trgCheckSemester, $srcCheckSem_arr)) ? "<td align='center' nowrap><span class='tbheading'>".$semester_arr[$j]."</span></td>" : "";
					}
				}
			}
			$rx .= "</tr>";
			for($j=0; $j<sizeof($row_cmp_subject); $j++)
			{
				$subject = trim($row_cmp_subject[$j]["SubjectName"]);
				$subject_code = trim($row_cmp_subject[$j]["SubjectCode"]);
				$cmp_subject = trim($row_cmp_subject[$j]["SubjectComponentName"]);
				$cmp_subject_code = trim($row_cmp_subject[$j]["SubjectComponentCode"]);

				# To check wheter this subject have score
				$have_score = ($displayBy=="Score" || $displayBy=="StandardScore" || $displayBy=="") ? $row_cmp_subject[$j]["have_score"] : 1;

				if (!$have_score) {
					continue;
				}
				
				# check whether the subject code exist
				$valid_subject_code = ($cmp_subject_code=="") ? $lpf_sturec->checkSubjectCode($subject_code) : $lpf_sturec->checkSubjectCode($cmp_subject_code);
				if($valid_subject_code==0)
				{
					continue;
				}

				// IF this is not a component subject
				if($cmp_subject_code=="")
				{
					$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
					$rx .= "<tr $bgcolor>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;".$subject."</td>";
					} else
					{
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/gimg/icon_chart.gif' width='21' height='21' border='0' align='absmiddle'></a>";
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;".$subject.$chart_display."</td>";
					}

					for($k=0; $k<sizeof($year_arr); $k++)
					{
						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);
						for($q=0; $q<sizeof($semester_arr); $q++)
						{
//							$sem = trim($semester_arr[$q]);
							$sem = strtolower(trim($semester_arr[$q]));
							
							if(in_array($sem, $sem_arr[$year]))
							{
								if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{
									$value = "--";
								}
								else if ($RAW[$subject_code][$year][$sem]["major"]["Score"] == "-1" && $RAW[$subject_code][$year][$sem]["major"]["Grade"] == '')
								{
									$value = "--";
								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem]["major"]["Score"];

									# get semester full name
									$sem_name = $full_semester_arr[$q];

									# retrieve subject semester standard scores of rank
									$value = $lpf_sturec->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);
								}
								else
								{
									if ($sys_custom['iPf']['Report']['academicResult']['ShowGrade'] )
									{
										$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
									}
									elseif($valueShow=="Score" && $have_score==0)
									{
										$value = ($RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"]["Grade"];
									}
									else
									{
										//$value = ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 || $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];

										$value = ($RAW[$subject_code][$year][$sem]["major"][$valueShow]=="" || ($RAW[$subject_code][$year][$sem]["major"]["Score"]<=0 && $RAW[$subject_code][$year][$sem]["major"]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem]["major"][$valueShow];
										
										if ($RAW[$subject_code][$year][$sem]["major"][$valueShow] == "")
										{
											$value = "--";
										}
										else if ($RAW[$subject_code][$year][$sem]["major"]["Score"] <= 0 && $RAW[$subject_code][$year][$sem]["major"]["Grade"] == "")
										{
											$value = "--";
										}
										else if ($RAW[$subject_code][$year][$sem]["major"][$valueShow] < 0 && $RAW[$subject_code][$year][$sem]["major"]["Grade"] != '')
										{
											$value = $RAW[$subject_code][$year][$sem]["major"]["Grade"];
										}
										else
										{
											$value = $RAW[$subject_code][$year][$sem]["major"][$valueShow];
										}
									}
								}
								$rx .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
				else	//IF it is a component subject
				{
					$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
					$rx .= "<tr $bgcolor>\n";
					if ($displayBy=="" || $displayBy=="Grade")
					{
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$cmp_subject."&nbsp;</td>";
					} else
					{
						$chart_display = ($displayType=="Print" || $have_score==0 || $RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1) ? "&nbsp;" : "&nbsp;<a href='stat2.php?$params&SubjectCode=".urlencode($subject_code)."&SubjectComponentCode=".urlencode($cmp_subject_code)."&displayBy=".$displayBy."' class='link_chi' title=".$ec_iPortfolio['display_stat_report']."><img src='$sr_image_path/gimg/icon_chart.gif' width='21' height='21' border='0' align='absmiddle'></a>";
						$rx .= "<td height='35' colspan='2' class='tbheading'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$cmp_subject.$chart_display."</td>";
					}

					for($k=0; $k<sizeof($year_arr); $k++)
					{
						$year = trim($year_arr[$k]);
						$class = trim($class_arr[$k]);

						for($q=0; $q<sizeof($semester_arr); $q++)
						{
							$sem = strtolower(trim($semester_arr[$q]));
							
							if(in_array($sem, $sem_arr[$year]))
							{
								if($RAW[$subject_code]["NotSelected"]==1 || $RAW[$subject_code]["NoRank"]==1)
								{
									$value = "--";
								}
								else if($displayBy=="StandardScore")
								{
									$score = $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"];

									# get semester full name
									$sem_name = $full_semester_arr[$q];

									# retrieve subject semester standard scores of rank
									$value = $lpf_sturec->returnStandardScore($StudentID, $score, $year, $sem_name, $subject_code, $cmp_subject_code);
								}
								else
								{
									if($sys_custom['iPf']['Report']['academicResult']['ShowGrade'] || ($valueShow=="Score" && $have_score==0))
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="") ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"];
									}
									else
									{
										$value = ($RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow]=="" || ($RAW[$subject_code][$year][$sem][$cmp_subject_code]["Score"]<=0 && $RAW[$subject_code][$year][$sem][$cmp_subject_code]["Grade"]=="")) ? "--" : $RAW[$subject_code][$year][$sem][$cmp_subject_code][$valueShow];

										//for safe if $value == '-1', display with '--'
										$value = ($value == '-1')? '--' : $value;
									}
								}
								$rx .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
							}
						}
					}
					$rx .= "</tr>\n";
				}
			}

			# overall grade
			$bgcolor = ($j%2!=0) ? "bgcolor='$alternative_row_color'" : "bgcolor='#FFFFFF'";
			$rxOverall = "<tr $bgcolor>\n";
			$rxOverall .= "<td height='35' colspan='2' align='center' class='tbheading'>[".$totalTitle."]</td>";
			for($k=0; $k<sizeof($year_arr); $k++)
			{
				$year = trim($year_arr[$k]);
				$class = trim($class_arr[$k]);
				for($q=0; $q<sizeof($semester_arr); $q++)
				{
					//$sem = trim($semester_arr[$q]);
					$sem = strtolower(trim($semester_arr[$q]));
					if(in_array($sem, $sem_arr[$year]))
					{
						if ($sys_custom['iPf']['Report']['academicResult']['ShowGrade'])
						{
							$value = ($RAW_TOTAL[$year][$sem]["Grade"]!="") ? $RAW_TOTAL[$year][$sem]["Grade"] : "--";
						}
						elseif($displayBy=="StandardScore")
						{
							$score = $RAW_TOTAL[$year][$sem]["Score"];

							# get semester full name
							$sem_name = $full_semester_arr[$q];

							# retrieve subject annual standard scores
							$value = $lpf_sturec->returnMainStandardScore($StudentID, $score, $year, $sem_name);
						}
						else
						{
							$value = (($valueShow=="Grade" && $RAW_TOTAL[$year][$sem][$valueShow]!="") || $RAW_TOTAL[$year][$sem][$valueShow]>0) ? $RAW_TOTAL[$year][$sem][$valueShow] : "--";
						}
						if ($value!="--")
						{
							$IsOverallExist = true;
						}
						$rxOverall .= "<td height='35' align='center' class='tbheading'>".$value."</td>";
					}
				}
			}
			$rxOverall .= "</tr>";
			if ($IsOverallExist)
			{
				$rx .= $rxOverall;
			}

		} else
		{
			$rx .= "<tr><td align='center' height='100' bgcolor='#FFFFFF' class='tbheading'>{$no_record_msg}</td></tr>";
		}

		$rx .= "</table>\n";
		$rx .= "</td></tr></table>\n";

		return $rx;
	}
	
		# Retrieve Student Assessment Result
	function returnAssessmentResult($StudentID, $IsAnnual, $AcademicYearID="", $YearTermID="")
	{
		global $eclass_db, $ec_iPortfolio, $intranet_db;

    $conds = "";
    if($AcademicYearID!="")
    {
      # Academic Year
      $lib_ay = new academic_year($AcademicYearID);
      $AYear = $lib_ay->YearNameEN;
    
		  $conds .= " AND (ASSR.AcademicYearID = '$AcademicYearID' OR ASSR.Year = '$AYear')";
		}
		$conds .= ($YearTermID!="" && $IsAnnual==0) ? " AND ASSR.YearTermID = '$YearTermID'" : "" ;

		# Retrieve all RAW Records
		# Eric Yip (20090828): sort Semester with Chinese characters with specific order
		$sql = "	SELECT
							ASSR.Year, ASSR.Semester, ASSR.IsAnnual, ASSR.SubjectCode, ASSR.SubjectComponentCode, ASSR.Score, ASSR.Grade, ASSR.ClassName, ASSR.OrderMeritForm,
					    CASE LEFT(Semester,3)
                WHEN '".$ec_iPortfolio['semester_name_array_1'][2]."' THEN 1
                WHEN '".$ec_iPortfolio['semester_name_array_2'][2]."' THEN 2
                WHEN '".$ec_iPortfolio['semester_name_array_3'][2]."' THEN 3
                ELSE 4
              END AS SemesterOrder,
			ASSR.YearTermID, SUBJ.EN_SNAME
					FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD AS ASSR
							LEFT JOIN {$intranet_db}.ASSESSMENT_SUBJECT AS SUBJ ON SUBJ.EN_SNAME=ASSR.SubjectCode
					WHERE
							ASSR.UserID='$StudentID'
							AND ASSR.IsAnnual = '$IsAnnual'
							AND SUBJ.RecordStatus = 1
							$conds AND (ASSR.Grade<>'' OR ASSR.Score<>'')
					ORDER BY
							ASSR.Year, ASSR.ClassName, SemesterOrder, ASSR.Semester, SUBJ.DisplayOrder, ASSR.ModifiedDate
				";
		$row_all = $this->returnArray($sql);

		return $row_all;
	}
}

?>