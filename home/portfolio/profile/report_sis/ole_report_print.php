<?php

// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-notes.php");
include_once("$eclass_filepath/src/includes/php/lib-notes_portfolio.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");


//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//fitness
include_once($pms_root."/includes/libPMS.php");
include_once($pms_root."/includes/config.inc.php");

intranet_auth();
intranet_opendb();


include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");


$ClassName = $Class;
$Year = trim($Year);
$YearTitle = $Year."/".(substr(($Year+1),2,2));

$linterface = new interface_html();
$objDB = new libdb();
$lpf = new libportfolio();

switch($ReportType)
{
	case "Average":	
		$title = "Community Involvement Programme (CIP) Hours ".$YearTitle." - Average Hours";	
		$display = getTitle($title);
		$display .= CIPAverageReport($Year,$ClassName);
	break;
		
	case "ByClass":
		$title = "Community Involvement Programme (CIP) Hours ".$YearTitle." - ".$ClassName;
		$field = array("Class No", "English Name", "Gender", "CIP Hours");
		$display = getTitle($title);
		$display .= CIPReport($Year,$ClassName, $field, $ReportType);
		break;

	case "ByEvent":
		$ProgramIDs = $_POST['ProgramID'];
		$display='';
		foreach((array)$ProgramIDs as $ProgramID){
			$title = "Community Involvement Programme (CIP) Hours ".$YearTitle." - By Event";	
			$field = array("Class", "English Name", "Gender", "CIP Hours");
			$otherTitle = "Event: ".getProgramTitle($ProgramID);
			$display .= getTitle($title, $otherTitle);
			$display .= CIPReport($Year,$ClassName, $field, $ReportType, $ProgramID);
			$display .= '<DIV style="page-break-after:always"></DIV>';
		}
		break;
	
	case "ByLevel":
		$title = "Community Involvement Programme (CIP) Hours ".$YearTitle." - BY Level";
		$field = array("Level", "Less than 6 hours", "6 hours or more", "Total");
		$display = getTitle($title);
		$display .= CIPReportByLevel($Year,$ClassName, $field);
		break;

	case "AAByClass":
		$title = "Awards & Achievements Report ".$YearTitle." - ".$ClassName;
		$field = array("Class No", "Student Name", "CategoryName", "Event / Competition", "Awards / Certifications / Achievements");
		$display = getTitle($title);
		$display .= AwardAchieveReportByClass($Year,$ClassName, $field);
		break;

	case "AAByEvent":
		
		$CCAProgramIDs = $_POST['CCAProgramID'];
		$display='';
		foreach((array)$CCAProgramIDs as $CCAProgramID){
			$title = "Awards & Achievements Report ".$YearTitle." - By Event";	
			$field = array("English Name", "Class", "Class Number", "Category", "Awards / Certifications / Achievements");
			$otherTitle = "Event / Competition: ".getProgramTitle($CCAProgramID);
			$display .= getTitle($title, $otherTitle);
			$display .= AwardAchieveReportByEvent($Year,$ClassName, $field, $CCAProgramID);
			$display .= '<DIV style="page-break-after:always"></DIV>';
		}
		break;
}

$TemplateCSS = "sis.css";
$TemplateCSSPath = $PATH_WRT_ROOT."templates/2007a/css/iportfolio/". $TemplateCSS;
if($TemplateCSSPath){
	echo "<link href=".$TemplateCSSPath." rel=\"stylesheet\" type=\"text/css\" />\n";
}
echo $display;

intranet_closedb();

function getTitle($title, $otherTitle='')
{
		global $BroadlearningClientName;

		$display = "<br><table width='95%'  border=0 cellpadding=4 cellspacing=0 align='center' >\n";
		//$display .="<tr><td align='center' colspan='2'><b>".$BroadlearningClientName."</b></td></tr>";
		$display .="<tr><td align='center' colspan='2'><b>".$title."</b></td></tr>";
		$display .="<tr><td align='left'><b>&nbsp;</b></td><td align='right'>Print Date: ".date('d M Y')."</td></tr>";
		$display .=($otherTitle=='')? "":"<tr><td align='left' colspan='2'><b>".$otherTitle."</b></td></tr>";
		$display .="</table><br>";	

		return $display;
}

function CIPAverageReport($Year,$ClassName)
{
	global $objDB,$eclass_db;
	
	$sql="SELECT 
	CASE 
		WHEN left(iu.ClassName,2)='PY' THEN 'a' 
		WHEN left(iu.ClassName,1)='P' THEN 'b' 
		WHEN left(iu.ClassName,1)='S' THEN 'c'
		ELSE 'd' 
	END as levelOrder ,cl.LevelName, iu.ClassName, if(SUM(s.Hours) IS NULL,0,SUM(s.Hours)) as TotalHours, count(distinct iu.UserId) as NoStudent
FROM INTRANET_USER as iu 
LEFT JOIN {$eclass_db}.OLE_STUDENT as s ON s.UserId=iu.UserId and s.IntExt='INT' and Year(s.StartDate) ='".$Year."' 
INNER JOIN INTRANET_CLASS as c ON c.ClassName=iu.ClassName 
INNER JOIN INTRANET_CLASSLEVEL as cl ON cl.ClassLevelID=c.ClassLevelID
Where iu.RecordType=2 and iu.ClassName IS NOT NULL and iu.ClassName !='' group by iu.ClassName order by levelOrder,cl.LevelName,iu.ClassName";
	
	$result = $objDB->returnArray($sql);
	

	for($i=0; $i<sizeof($result); $i++)
	{
		list($levelOrder, $classlevel, $className, $Hours, $NoStudent) = $result[$i];
		$avgHourRecord[$classlevel][$className] = round(($Hours/$NoStudent), 2);
	}
	

	$display = '<table width="90%" border=1 cellpadding=4 cellspacing=0 align="center">';
	if(sizeof($avgHourRecord)>0)
	{
		
		#get max. size
		$maxsize =0;
		foreach($avgHourRecord as $key => $val)
		{
			$tempsize = sizeof($avgHourRecord[$key]);
			$maxsize = max($tempsize,$maxsize);
		}

		foreach($avgHourRecord as $level => $value)
		{
			$display .= '<tr>';
			$display .= '<td class="result_header">'.$level.'</td>';
			$count = 0;
			foreach($value as $class => $average)
			{
				$display .= '<td class="result_mark">'.$class.' ('.$average.') </td>';
				$count++;
			}

			$addcol = $maxsize - $count;
			for($i=0; $i< $addcol; $i++)
			{
				$display .= '<td>&nbsp;</td>';
			}
			$display .= '</tr>';
		}
	}	
	$display .= '</table>';

	return $display;
}

function CIPReport($Year,$ClassName, $field, $ReportType, $ProgramID='')
{
	global $objDB,$eclass_db;
	
	if($ReportType == "ByClass")
	{
		$sql="SELECT iu.UserId, iu.ClassNumber, iu.EnglishName, iu.Gender, if(SUM(s.Hours) IS NULL,0,SUM(s.Hours)) as Hours 
		FROM INTRANET_USER as iu  
		LEFT JOIN {$eclass_db}.OLE_STUDENT as s ON s.UserId=iu.UserId and s.IntExt='INT'
		where iu.RecordType=2 and iu.ClassName='".$ClassName."' group by iu.UserId order by CAST(iu.ClassNumber as UNSIGNED)";
//		LEFT JOIN {$eclass_db}.OLE_STUDENT as s ON s.UserId=iu.UserId and s.IntExt='INT' and Year(s.StartDate) ='".$Year."' 
	}
	else
	{
		$sql="SELECT iu.UserId, iu.ClassName, iu.EnglishName, iu.Gender, if(s.Hours IS NULL,0,s.Hours) as Hours 
			FROM {$eclass_db}.OLE_PROGRAM as p 
			INNER JOIN {$eclass_db}.OLE_STUDENT as s ON s.ProgramID=p.ProgramID
			INNER JOIN INTRANET_USER as iu ON s.UserId=iu.UserId 
			where s.IntExt='INT' and p.ProgramID= '$ProgramID' and  iu.RecordType=2 order by iu.ClassName";
//			where s.IntExt='INT' and p.ProgramID= '$ProgramID' and Year(s.StartDate) ='".$Year."'  and  iu.RecordType=2 order by iu.ClassName";
	}
	$result = $objDB->returnArray($sql);
	
	$display = '<table width="95%" border=1 cellpadding=4 cellspacing=0 align="center">';
	$display .= '<tr>';
	foreach($field as $index => $headertitle) $display .= '<td class="result_header">'.$headertitle.'</td>';
		
	
	$display .= '</tr>';

	if(sizeof($result) > 0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			$display .= '<tr>';
			$display .= '<td class="result_mark"> '.$result[$i][1].'</td> <td class="result_mark">'.$result[$i][2].'</td> <td class="result_mark">'.$result[$i][3].'</td><td class="result_mark">'.$result[$i][4].'</td>';	

			$display .= '</tr>';
		}
	}
	else
	{
		$display .= '<tr><td class="result_mark" align="center" colspan="'.sizeof($field).'"> No records found </td></tr>';
	}

	$display .= '</table><br>';

	return $display;
}

function CIPReportByLevel($Year,$ClassName, $field)
{
	global $objDB,$eclass_db;
	
	$sql = "SELECT iu.UserId, 
	CASE 
		WHEN left(ClassName,2)='PY' THEN 'a' 
		WHEN left(ClassName,1)='P' THEN 'b' 
		WHEN left(ClassName,1)='S' THEN 'c'
		ELSE 'd' 
	END as levelOrder ,
	if(left(ClassName,2)='PY', left(ClassName,3),left(ClassName,2)) as Level, if(SUM(s.Hours) IS NULL,0,SUM(s.Hours)) as Hours 
	FROM INTRANET_USER as iu 
	LEFT JOIN {$eclass_db}.OLE_STUDENT as s ON s.UserId=iu.UserId and s.IntExt='INT' and Year(s.StartDate) ='".$Year."' 
	WHERE RecordType=2 and ClassName!='' and ClassName IS NOT NULL GROUP BY iu.UserId ORDER BY levelOrder,Level";

	$result = $objDB->returnArray($sql);

	for($i=0; $i < sizeof($result); $i++)
	{
		list($id, $levelOrder, $level, $hour) =  $result[$i];
		
		if($levelRecord[$level]['A']=='') $levelRecord[$level]['A'] = 0;
		if($levelRecord[$level]['B']=='') $levelRecord[$level]['B'] = 0;

		if($hour < 6)
			$levelRecord[$level]['A'] ++;
		else
			$levelRecord[$level]['B'] ++;

		$levelRecord[$level]['Total'] ++;
	}

	$display = '<table width="70%" border=1 cellpadding=4 cellspacing=0 align="center">';
	$display .= '<tr>';
	foreach($field as $index => $headertitle) $display .= '<td class="result_header">'.$headertitle.'</td>';
		
	
	$display .= '</tr>';

	foreach($levelRecord as $level => $value)
	{
		$LessCount = $value['A'];
		$MoreCount = $value['B'];
		$Total = $value['Total'];
		$percentageA = round($LessCount/$Total * 100);
		$percentageB = round($MoreCount/$Total * 100);

		$GLessCount = $GLessCount+$LessCount;
		$GMoreCount = $GMoreCount+$MoreCount;
		$GTotal = $GTotal+ $Total;

		$display .= '<tr>';
		$display .= '<td class="result_mark"> '.$level.'</td> <td class="result_mark">'.$LessCount.' ( '.$percentageA.'%)</td> <td class="result_mark">'.$MoreCount.' ('.$percentageB.'%)</td><td class="result_mark">'.$Total.'</td>';	

		$display .= '</tr>';
	}

	$display .= '<tr>';
	$display .= '<td class="result_mark"> Total </td> <td class="result_mark">'.$GLessCount.' ( '.(round($GLessCount/$GTotal * 100)).'%)</td> <td class="result_mark">'.$GMoreCount.' ('.(round($GMoreCount/$GTotal * 100)).'%)</td><td class="result_mark">'.$GTotal.'</td>';	
	$display .= '</tr>';	

	$display .= '</table><br>';

	return $display;
}

function AwardAchieveReportByClass($Year,$ClassName, $field)
{
	global $objDB,$eclass_db;

	$sql="SELECT iu.UserId, iu.ClassNumber, iu.EnglishName, 
	if(c.EngTitle IS NULL or c.EngTitle = '', c.ChiTitle, c.EngTitle) as CategoryName, 
	s.Title, s.Achievement 
	FROM INTRANET_USER as iu  
	LEFT JOIN {$eclass_db}.OLE_STUDENT as s ON s.UserId=iu.UserId and s.IntExt='EXT' and Year(s.StartDate) ='".$Year."' 
	LEFT JOIN {$eclass_db}.OLE_CATEGORY as c ON s.Category=c.RecordID  
	where iu.RecordType=2 and iu.ClassName='".$ClassName."'  order by CAST(iu.ClassNumber as UNSIGNED)";

	$result = $objDB->returnArray($sql);

	for($i=0; $i < sizeof($result); $i++)
	{
		list($UserId, $ClassNumber, $EnglishName, $CatName, $Title, $Achievement) = $result[$i];
		$awardRecord[$UserId][] = array($ClassNumber, $EnglishName, $CatName, $Title, $Achievement);
	}

	$display = '<table width="90%" border=1 cellpadding=4 cellspacing=0 align="center">';
	$display .= '<tr>';
	foreach($field as $index => $headertitle) $display .= '<td class="result_header">'.$headertitle.'</td>';		
	
	$display .= '</tr>';

	foreach($awardRecord as $UserId => $value)
	{
		$firstRowByUser = true;
		for($i=0; $i < sizeof($value); $i++)
		{
			$display .= '<tr>';
			$display .= '<td class="result_mark">'.($firstRowByUser? $value[$i][0]:'&nbsp;').'</td> 
							  <td class="result_mark">'.($firstRowByUser? $value[$i][1]:'&nbsp;').'</td> 
							  <td class="result_mark">'.$value[$i][2].'&nbsp;</td>
							  <td class="result_mark">'.$value[$i][3].'&nbsp;</td>
							  <td class="result_mark">'.$value[$i][4].'&nbsp;</td>';	
			$display .= '</tr>';	
			$firstRowByUser = false;
		}

	}
	
	$display .= '</table><br>';

	return $display;

}

function AwardAchieveReportByEvent($Year,$ClassName, $field, $CCAProgramID)
{
	global $objDB,$eclass_db;

	$sql="SELECT iu.UserId,  iu.EnglishName, iu.ClassName, iu.ClassNumber, 
	if(c.EngTitle IS NULL or c.EngTitle = '', c.ChiTitle, c.EngTitle) as CategoryName, s.Achievement 
	FROM {$eclass_db}.OLE_PROGRAM as p 
	INNER JOIN {$eclass_db}.OLE_STUDENT as s ON s.ProgramID=p.ProgramID 
	INNER JOIN INTRANET_USER as iu ON s.UserId=iu.UserId 
	LEFT JOIN {$eclass_db}.OLE_CATEGORY as c ON s.Category=c.RecordID  
	where s.IntExt='EXT' and p.ProgramID=$CCAProgramID and Year(s.StartDate) ='".$Year."'  and  iu.RecordType=2 order by iu.ClassName, CAST(iu.ClassNumber as UNSIGNED)";

	$result = $objDB->returnArray($sql);

	$display = '<table width="90%" border=1 cellpadding=4 cellspacing=0 align="center">';
	$display .= '<tr>';
	$display .= '<td class="result_header"> # </td>';
	foreach($field as $index => $headertitle) $display .= '<td class="result_header">'.$headertitle.'</td>';		
	
	$display .= '</tr>';
	
	if(sizeof($result)>0)
	{
		for($i=0; $i < sizeof($result); $i++)
		{
			list($UserId, $EnglishName, $ClassName, $ClassNumber , $CatName, $Achievement) = $result[$i];
				$display .= '<tr>';
				$display .= '<td class="result_mark">'.($i+1).'</td>';
				$display .= '<td class="result_mark">'.$EnglishName.'</td> 
								  <td class="result_mark">'.$ClassName.'&nbsp;</td> 
								  <td class="result_mark">'.$ClassNumber.'&nbsp;</td>
								  <td class="result_mark">'.$CatName.'&nbsp;</td>
								  <td class="result_mark">'.$Achievement.'&nbsp;</td>';	
				$display .= '</tr>';	

		}
	}
	else
	{
		$display .= '<tr><td class="result_mark" align="center" colspan="'.(sizeof($field)+1).'"> No records found </td></tr>';
	}

	$display .= '</table><br>';

	return $display;

}

function getProgramTitle($ProgramID)
{
	global $objDB,$eclass_db;

	$sql = "SELECT Title FROM {$eclass_db}.OLE_PROGRAM WHERE ProgramID=".$ProgramID;
	$result = $objDB->returnVector($sql);

	return $result[0];

}


?>
