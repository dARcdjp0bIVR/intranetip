<?php
/*
 * 	Log
 * 	Date:	2014-05-30 [Cameron] show fitness data according to user's selection
 * 	Date:	2014-05-20 [Cameron] Add function getStudentFitness(), getStudentFitnessResult()
 */

// Modifing by 
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-notes.php");
include_once("$eclass_filepath/src/includes/php/lib-notes_portfolio.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");


//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//fitness
include_once($pms_root."/includes/libPMS.php");
include_once($pms_root."/includes/config.inc.php");


include_once($pms_root."/includes/libHolistic.php");
intranet_auth();
intranet_opendb();


include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

//$ClassName = 'P5A';
$ClassName = $Class;

$linterface = new interface_html();
$objDB = new libdb();
$objHolistic = new libHolistic();
$objHolistic->setClassName($ClassName);
### Config Setting ####
$excludeLevelFromFitness = array('PY1','PY2','P1','P2','P3');

$Level = substr($ClassName,0,-1);
if(!in_array($Level,$excludeLevelFromFitness))
	$showFitnessResult = true;
else
	$showFitnessResult = false;

# fitness standard Grade
$sGradeArr = getFitnessStandardGrade();

$SignatureTitleArray = $ec_iPortfolio['reportcard_Signature'];

# get Current School Year for SIS
$schoolCurrentYrSetting = getCurrentAcademicYear();
$CurrentYr = substr($schoolCurrentYrSetting,0,4);
//$CurrentYr = 2009;

$objHolistic->setCurrentYear($CurrentYr);
$TemplateCSS = "sis.css";
$TemplateCSSPath = $PATH_WRT_ROOT."templates/2007a/css/iportfolio/". $TemplateCSS;

//debug_r($_REQUEST);


?>

<? if($TemplateCSSPath) {?>
<link href="<?=$TemplateCSSPath?>" rel="stylesheet" type="text/css" /><? } ?>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?

$StudentIDAry = GET_STUDENT_BY_CLASSNAME($ClassName);


for($s=0;$s<sizeof($StudentIDAry);$s++)
{
	$StudentID = $StudentIDAry[$s]['UserID'];
	
	$TitleTable = $objHolistic->getReportHeader();
	$StudentInfoTable = getReportStudentInfo($StudentID, 2);
	$MSTable = getMSTable($StudentID);
	$SignatureTable = getSignatureTable($SignatureTitleArray);
	//$FooterRow = $eRCtemplate->getFooter($ReportID);
	
	$TopTable = "";
	$TopTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top' align='center'>\n";
	$TopTable .= "<tr valign='top'><td>".$TitleTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$StudentInfoTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$MSTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$MiscTable."</td></tr>\n";
	$TopTable .= "</table>\n";
	
	$BottomTable = "";
	$BottomTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='bottom' align='center'>\n";
	$BottomTable .= "<tr><td>".$SignatureTable."</td></tr>\n";
	$BottomTable .= $FooterRow."\n";
	$BottomTable .= "</table>\n";

	?>
	<div id="container" valign='top'>
	<?= ($s==0) ? "<br>":"" ?>
	<table valign='top' width="100%" border="0" cellpadding="0" cellspacing="0" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
	<tr height='570px'><td valign='top' align='center'><?=$TopTable?></td><tr>
	<tr><td valign='bottom' align='center'><?=$BottomTable?></td><tr>
	</table>
	</div>
	<?

}

intranet_closedb();

function GET_STUDENT_BY_CLASSNAME($ClassName)
{
	global $objDB;
	
	$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName='$ClassName' AND RecordType=2 ORDER BY CAST(ClassNumber as UNSIGNED)";
	$result = $objDB->returnArray($sql);

	return $result;

}
//wait for delete 
function _getReportHeader()
{

	global $ec_iPortfolio, $CurrentYr;

	$Year = $CurrentYr."-".substr(($CurrentYr+1),2,2);

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td align='center' class='result_mark'><h2>".$ec_iPortfolio['reportcard_header']."</h2></td></tr>";
	$TitleTable .= "<tr><td align='center' class='result_mark'><h2>".$ec_iPortfolio['reportcard_academicyr']." ".$Year ."</h2></td></tr>";
	$TitleTable .= "<tr><td>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

//keep this function in here , don't move to /home/web/sis-trial/intranetIP/includes/libHolistic.php
function getReportStudentInfo($StudentID, $StudentInfoTableCol)
{
	global $PATH_WRT_ROOT, $pms_root;
	
	if($StudentID)		# retrieve Student Info
	{
		include_once($pms_root."/includes/libuser.php");
		include_once($pms_root."/includes/libclass.php");
		$lu = new libuser($StudentID);
		$lclass = new libclass();
		
		$data['Name'] 		= $lu->UserName2Lang();
		$data['Class'] 		= $lu->ClassName;
		$data['Class No'] 	= $lu->ClassNumber; 
		//$data['ClassIndexNo'] 	= $lu->ClassNumber; 
		//$data['StudentNo'] 	= $lu->ClassNumber;
		//$data['DateOfBirth'] = $lu->DateOfBirth;
		//$data['Gender'] 	= $lu->Gender;
		
						
		$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName);
		foreach($ClassTeacherAry as $key=>$val)
		{
			$CTeacher[] = $val['CTeacher'];
		}
		$data['Class Teacher'] = !empty($CTeacher) ? implode("<br/>", $CTeacher) : "--";
		$data['Student No'] = $lu->AdmissionNo;
	}
	
	$StudentInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
	
	for($i=0;$i<$StudentInfoTableCol;$i++)
	{
		$StudentInfoTable .= "<colgroup span='1' id='info_label'></colgroup>";
		$StudentInfoTable .= "<colgroup span='1' id='colon_col'></colgroup>";
		$StudentInfoTable .= "<colgroup span='1' id='info_value_wide'></colgroup>";
	}
	
	$count = 0;
	foreach($data as $title => $info)
	{
		
		$StudentInfoTable .= ($count == 0)?"<tr>":"";
		$StudentInfoTable .= "<td class='result_col_label' nowrap>".$title."</td>";
		$StudentInfoTable .= "<td>:&nbsp;</td>";

		$width = ($count%$StudentInfoTableCol==0) ? "130" : "50";
		
		if($title == 'Name')
		{
			$StudentInfoTable .= "<td colspan='4' class='result_mark' width='100%'>". ($info ? $info: '--' ) ."</td>";
			$count = 0;
		}
		else
		{
			$StudentInfoTable .= "<td class='result_mark' width='$width'>". ($info ? $info: '--' ) ."</td>";
			$count++;
		}

		if($count == $StudentInfoTableCol)
		{
			$StudentInfoTable .= "</tr>";
			$count = 0;
		}
	}
	$StudentInfoTable .= "<tr class='bottom_dot_border'><td colspan='". ($StudentInfoTableCol*3)."' style='font-size:2px'>&nbsp;</td></tr>";
	$StudentInfoTable .= "<tr><td>&nbsp;</td></tr>";
	$StudentInfoTable .= "</table>";

	
	return $StudentInfoTable;
}

//keep this function in here , don't move to /home/web/sis-trial/intranetIP/includes/libHolistic.php
function getMSTable($StudentID)
{
	global $objHolistic, $_REQUEST;
	$MSTable ="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$MSTable .= "<tr><td>".$objHolistic->getStudentCIP($StudentID)."</td></tr>";
	$MSTable .= "<tr><td>".$objHolistic->getStudentCCAInvolvement($StudentID)."</td></tr>";
	$MSTable .= "<tr><td>".$objHolistic->getStudentCCAAchievements($StudentID)."</td></tr>";

	if ($_REQUEST['show_fitness'])
	{
		$MSTable .= "<tr><td>".getStudentFitness($StudentID)."</td></tr>";
	}
//	$MSTable .= "<tr><td>".getFitnessAward($StudentID)."</td></tr>";	
	$MSTable .= "</table>";

	return $MSTable;
}


function getSignatureTable($SignatureTitleArray)
{

	$SignatureTable = "";
	
	$SignatureTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center' valign='bottom'>";
	
	$SignatureTable .= "<tr>";
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$SignatureTable .= "<td class='dot_bottom_border'>&nbsp;</td>";
		if($k<sizeof($SignatureTitleArray)-1)	
			$SignatureTable .= "<td class='signature_space'>&nbsp;</td>";
	}
	$SignatureTable .= "</tr>";
	
	$SignatureTable .= "<tr>";
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$Title = trim($SignatureTitleArray[$k]);

		$SignatureTable .= "<td class='signature_label'>".$Title."</td>";
		if($k<sizeof($SignatureTitleArray)-1)	
			$SignatureTable .= "<td>&nbsp;</td>";
	}
	$SignatureTable .= "</tr>";
	$SignatureTable .= "</table>";
	
	return $SignatureTable;
}

//wait for delete
function _getStudentCIP($StudentID)
{
	global $ec_iPortfolio, $objHolistic,$CurrentYr;

	$Type = 'INT';
	$StudentCIPAry = $objHolistic->getStudentProgramDetail($StudentID, $Type,$CurrentYr);

	$StudentCIP = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$StudentCIP .= "<tr><td colspan='3' class='result_header'>".$ec_iPortfolio['reportcard_CIP']."</td></tr>";
	$StudentCIP .= "<tr><td colspan='2' class='result_col_label' width='90%'>".$ec_iPortfolio['title_activity']."</td><td class='result_col_label'>".$ec_iPortfolio['hours']."</td></tr>";

	$Hours =0;
	for($i=0; $i < sizeof($StudentCIPAry); $i++)
	{
		$StudentCIP .= "<tr><td class='result_mark' width='70%' nowrap>".$StudentCIPAry[$i]['Title']."</td><td>&nbsp;</td><td class='result_mark'>".$StudentCIPAry[$i]['Hours']."</td></tr>";
		$Hours = $Hours+$StudentCIPAry[$i]['Hours'];
	}

	$StudentCIP .= "<tr><td width='70%' >&nbsp;</td><td class='result_col_label' >".$ec_iPortfolio['total']."&nbsp; :</td><td class='result_col_label'>".$Hours."</td></tr>";

	$StudentCIP .= "</table>";

	return $StudentCIP;
}
//wait for delete
function _getStudentCCAInvolvement($StudentID)
{

	global $ec_iPortfolio,$CurrentYr , $objHolistic;

	
	$StudentCCAInvAry = $objHolistic->getStudentCCAInvDetail($StudentID,$CurrentYr);
	$delim = "";

	$StudentCIP = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$StudentCIP .= "<tr><td colspan='3' class='result_header'>".$ec_iPortfolio['reportcard_CCA_Inv']."</td></tr>";

	
	$StudentCIP .= "<tr><td class='result_mark' >";
	if(sizeof($StudentCCAInvAry) > 0)
	{
		for($i=0; $i < sizeof($StudentCCAInvAry); $i++)
		{
			$StudentCIP .= $delim.$StudentCCAInvAry[$i]['Title'];
			$delim = ", ";
		}
	}
	else
	{
		$StudentCIP .= 'NIL';
	}
	$StudentCIP .= "</td></tr>";

	$StudentCIP .= "</table><br>";

	return $StudentCIP;

}
//wait for delete
function _getStudentCCAAchievements($StudentID)
{
	global $ec_iPortfolio,$CurrentYr,$objHolistic;

	$Type = 'EXT';
	$StudentCCAAry = $objHolistic->getStudentProgramDetail($StudentID, $Type,$CurrentYr);

	$StudentCCAAch = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$StudentCCAAch .= "<tr><td colspan='3' class='result_header'>".$ec_iPortfolio['reportcard_CCA_Ach']."<td></tr>";
	$StudentCCAAch .= "<tr><td colspan='3' class='result_col_label' >".$ec_iPortfolio['title']."</td></tr>";

	$Hours =0;
	if(sizeof($StudentCCAAry) > 0)
	{
		for($i=0; $i < sizeof($StudentCCAAry); $i++)
		{
			$Achievement = $StudentCCAAry[$i]['Achievement'];
			$StudentCCAAch .= "<tr><td class='result_mark' nowrap width='70%'>".$StudentCCAAry[$i]['Title']."</td><td class='result_mark' nowrap  width='30%' align='right'>".($Achievement ?$Achievement :'--')."</td><td>&nbsp;</td></tr>";
		}
	}
	else
	{
		$StudentCCAAch .= "<tr><td class='result_mark' colspan='3'>NIL</td></tr>";
	}


	$StudentCCAAch .= "</table><br>";

	return $StudentCCAAch;

}

function getFitnessAward($StudentID)
{	
	global $ec_iPortfolio,$showFitnessResult;

	$Award = getStudentFitnessAward($StudentID);
	$Award = ($Award == 'Exempted' || $Award == '')?(($showFitnessResult)?'NIL':'&nbsp;'):$Award;

	$classStyle= ($Award == 'NIL')?"class='result_mark'":"class='result_col_label'";

	$StudentFitnessAward = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$StudentFitnessAward .= "<tr><td colspan='2' class='result_header'>".$ec_iPortfolio['reportcard_Fitness']." :<td><td  ".$classStyle.">".$Award."</td></tr>";

	$StudentFitnessAward .= "</table><br>";

	return $StudentFitnessAward;

}

//wait for delete
function _getStudentProgramDetail($StudentID, $IntExt, $CurrentYr)
{
	global $objDB,$eclass_db;
//	global $CurrentYr;
	
	$cond = ($IntExt)?" AND IntExt='$IntExt' ":"";

	$sql = "SELECT UserId,Title,Details,Category,Hours,Achievement , YEAR(StartDate) as AcademicYear
					FROM {$eclass_db}.OLE_STUDENT 
				WHERE UserID=$StudentID AND YEAR(StartDate)='$CurrentYr' ".$cond;

	$result = $objDB->returnArray($sql);

	return $result;
}

//wait for delete
function _getStudentCCAInvDetail($StudentID,$year)
{
	global $objDB,$eclass_db;
	
//	$cond = ($cond)?" AND IntExt='$IntExt' ":"";
	$cond = ($year == "") ?"" :" and AcademicYear = {$year}" ;
/*
	$sql = "SELECT a.Title  
					FROM INTRANET_GROUP as a 
					INNER JOIN INTRANET_USERGROUP as b on a.GroupID=b.GroupID
				WHERE b.UserID=$StudentID AND a.RecordType=5 ".$cond;
*/
	$sql = "select a.Title , a.AcademicYear
					from PMS_ACTIVITY as a
					INNER JOIN PMS_ACTIVITY_USER as b on a.ActivityID = b.ActivityID
				where b.UserID = {$StudentID} ";
	$sql .= $cond;

	$result = $objDB->returnArray($sql);

	return $result;

}


function getStudentFitnessResult($StudentID)
{
	global $objDB;
	
	$result_case = "
          when 'E' then 'Excellent'
          when 'VG' then 'Very Good'
          when 'HA' then 'High Average'
          when 'LA' then 'Low Average'
          when 'P' then 'Poor'
          else '--'"; 
	
	$sql = "SELECT 	case r.PushUp " . $result_case . " end as PushUp,					
					case r.SitUp " . $result_case . " end as SitUp,
					case r.Sprint " . $result_case . " end as Sprint,
					case r.Agility " . $result_case . " end as Agility,
					case r.MultiStageFitnessRun " . $result_case . " end as MultiStageFitnessRun
			  FROM 	PMS_FITNESS_TMP_REPORT r					
			WHERE r.UserID='".$StudentID."'";
	
	$result = $objDB->returnArray($sql);

	return $result;		
}

function getStudentFitness($StudentID)
{
	global $ec_iPortfolio;
	$_colspan = 4;
	
	$x = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$x .= "<tr><td colspan='{$_colspan}' class='result_header'>".$ec_iPortfolio['fitness']."<td></tr>";
	$x .= "<tr><td colspan='{$_colspan}' class='result_col_label' >".$ec_iPortfolio['fitness_item_result']."</td></tr>";
	$rs = getStudentFitnessResult($StudentID);
	$nrRec = count($rs);
	if($nrRec > 0)
	{
		foreach($rs as $r)
		{
			$x .= " <tr>
						<td class='result_mark' nowrap width='70%'>".$ec_iPortfolio['FitnessItem']['PushUp']."</td>
						<td class='result_mark' nowrap  width='30%' align='right'>".$r['PushUp']."</td>
						<td>&nbsp;</td>
					</tr>";
			$x .= " <tr>
						<td class='result_mark' nowrap width='70%'>".$ec_iPortfolio['FitnessItem']['SitUp']."</td>
						<td class='result_mark' nowrap  width='30%' align='right'>".$r['SitUp']."</td>
						<td>&nbsp;</td>
					</tr>";
			$x .= " <tr>
						<td class='result_mark' nowrap width='70%'>".$ec_iPortfolio['FitnessItem']['Sprint']."</td>
						<td class='result_mark' nowrap  width='30%' align='right'>".$r['Sprint']."</td>
						<td>&nbsp;</td>
					</tr>";
			$x .= " <tr>
						<td class='result_mark' nowrap width='70%'>".$ec_iPortfolio['FitnessItem']['Agility']."</td>
						<td class='result_mark' nowrap  width='30%' align='right'>".$r['Agility']."</td>
						<td>&nbsp;</td>
					</tr>";
			$x .= " <tr>
						<td class='result_mark' nowrap width='70%'>".$ec_iPortfolio['FitnessItem']['MultiStageFitnessRun']."</td>
						<td class='result_mark' nowrap  width='30%' align='right'>".$r['MultiStageFitnessRun']."</td>
						<td>&nbsp;</td>
					</tr>";
		}
	}
	else
	{
		$x .= "<tr><td class='result_mark' colspan='{$_colspan}'>NIL</td></tr>";
	}

	$x .= "</table><br>";
	return $x;	
}

?>
