<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once("$eclass_filepath/src/includes/php/lib-notes.php");
include_once("$eclass_filepath/src/includes/php/lib-notes_portfolio.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpf = new libpf_report();
$lpf_slp = new libpf_slp();
$lpf_sturec = new libpf_sturec();
$lpf->ACCESS_CONTROL("print_report");

$li_npf = new notes_iPortfolio();

############################ YuEn: to do ############################

// 1. determine which template should be used
// 2. retrieve student's data
// 3. retrieve CSV data if necessary
// 4. generate the report sheet used

// A. multiple transcript and page break
####################################################################

if($ClassName!="")
{
	$sql = "SELECT 
				iu.UserID 
			FROM 
				{$intranet_db}.INTRANET_USER AS iu 
				LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID=iu.UserID
			WHERE 
				iu.RecordType='2' 
				AND ps.WebSAMSRegNo IS NOT NULL 
				AND ps.UserKey IS NOT NULL
				AND iu.ClassName = '$ClassName'
			";
	$studentArr = $lpf->returnVector($sql);

	$StudentID = implode(",", $studentArr);
}

$report_html = $lpf->generateTranscriptPrint($StudentID);

?>


<?=$report_html?>


<?php
intranet_closedb();
?>