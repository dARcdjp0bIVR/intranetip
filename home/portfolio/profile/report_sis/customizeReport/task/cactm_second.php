<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cactm/libpf-slp-cactmRptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cactm/libpf-slp-cactm.php");

$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
//$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);

$objReportMgr = new cactmRptMgr();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$objReportMgr->setIssueDate($issuedate);
$issuedateStr = $objReportMgr->getIssueDate();

$studentList = $objReportMgr->getStudentIDList();



for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	$objStudentInfo->setPrintIssueDate($issuedateStr);

	$h_studentInfo = $objStudentInfo->getStudentInfo_HTML();


	$h_studentPartA = $objStudentInfo->getIntCreatedByStudentOnly_HTML();	
	$h_studentPartB = $objStudentInfo->getExtCreatedByStudentOnly_HTML();	
//	$h_studentPartC = $objStudentInfo->getExtCreatedBySchoolOnly_HTML();	
//	$h_studentPartD = $objStudentInfo->getSelfAccount_HTML();	


	/**
	$h_studentPartA = $objStudentInfo->getPartA_HTML();	
	$h_studentPartB = $objStudentInfo->getPartB_HTML();
	$h_studentPartC = $objStudentInfo->getPartC_HTML();
	$h_studentPartD = $objStudentInfo->getPartD_HTML();	
	**/
	$html .= <<<HTML
	<div style="width:685px;">
		<!--div align="center" class="HeaderCSS">宣道中學</div-->
		<div align="center" class="HeaderCSS">其他學習經歷記錄</div>
		<div align="center" class="HeaderCSS">Record of Other Learning Experiences</div>
		<!--div align="center" class="HeaderCSS">	<u>Record of Other Learning Experiences</u></div-->
		<table border="0" align="center" width="100%" class="style1">
		<tr><td align="center">$h_studentInfo</td></tr>
		<tr><td><table><tr><td><br /><span class="section_title">以下活動資料未經校方核實。學生應自行向相關人士提供適當證明。</span></td></tr></table></td></tr>
		<tr><td align="center">$h_studentPartA<br/></td></tr>
		<tr><td align="center">$h_studentPartB<br/></td></tr>
		</table>
	</div>
HTML;

	if($i != ($i_max - 1) ){
//		$html .= "<p class=\"pageBreak\"></p>";
		$html .= '<br class="pageBreak" clear="all" />';
	}
}

$htmlCSS =<<<CSS
		<style type="text/css">
@charset "utf-8";
body{
font-size: 13.3px;
}

.section_title {
font-size: 1.25em;
font-weight: bold;
}

br.pageBreak{
	page-break-before:always;
}

.HeaderCSS {
/*	font-family: Arial, Helvetica, sans-serif;*/
	font-size: 18px;
}

table.style1{
//font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
//font-weight: normal;
//font-size: 11px;
margin: 1em;
border-collapse: collapse;
}

table.style1 td { 
	padding: .2em;
	font-family: 新細明體; 
	border: 0px #000 solid; 
	FONT-SIZE: 13.3px;
} 

table.section { 
	font-family: 新細明體; 
	border-left: 0.01em #999999 solid;
	border-top: 0.01em #999999 solid; 
	border-right: 0em #999999 solid;
	border-bottom: 0em #999999 solid; 
} 

table.section td { 
	font-family: 新細明體;
	padding: .2em; 
	border-left: 0em #999999 solid;
	border-top: 0em #999999 solid; 
	border-right: 0.01em #999999 solid;
	border-bottom: 0.01em #999999 solid; 
	FONT-SIZE: 13.3px;
}

table.self_intro_text { 
	border-left: 0.01em #999999 solid;
	border-top: 0.01em #999999 solid; 
	border-right: 0.01em #999999 solid;
	border-bottom: 0.01em #999999 solid; 
} 

table.section td { 
	font-family: 新細明體;
	FONT-SIZE: 13.3px;
}


p.pageBreak{
	page-break-after:always;
}

.chiSmall{
	font-family: 新細明體;
}

</style>
CSS;


_handleCurrentAlumniStudentAfterPrinting("alumni", $AlumniStudentArr);

if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}
exit();
?>