<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/htc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/htc/libpf-slp.php");

$YearClassID = trim($YearClassID);
$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);

### Basic PDF Settings
//$pdf = new StudentReportPDFGenerator_TCPDF();
$left_margin = 7;
$right_margin = 7;
$header_blank_height = 46;
$footer_blank_height = 7;
$header_height = 86;
$footer_height = 30;


$objReportMgr = new RptMgr();
$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	
	$OLEcomponentArr = $objStudentInfo->getOLEcomponentName();
	
	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,'INT',$Selected=true,$OLEcomponentArr);
	$OLEInfoArr_EXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,'EXT',$Selected=true,$OLEcomponentArr);
	
	
	//OLE_ELE

	### Cover Page
	
	
	$html .=$objStudentInfo->getEmptyTable('8%');
	
	$html .='<table width="100%" >';
	$html .= '<tr><td>'.$objStudentInfo->getPart2_HTML().'</td></tr>';
	$html .= '<tr><td>'.$objStudentInfo->getPart3_HTML($OLEInfoArr_INT).'</td></tr>';
	$html .= '<tr><td>'.$objStudentInfo->getEmptyTable('1%','End of this page').'</td></tr>';
	$html .='</table>';
	$html .= $objStudentInfo->getPagebreakTable();
	
	### Page Content
	
	$html .=$objStudentInfo->getEmptyTable('8%');
	$html .='<table width="100%" height="930px">';
	$html .= '<tr><td valign="top" style="padding-top:10px;">';
	$html .= $objStudentInfo->getPart2_HTML();
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr_EXT);
	$html .= $objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart5_HTML();
	$html .= $objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart6_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%','End of this page');
	$html .= '</td></tr>';
	$html .='</table>';
//	$html .=$objStudentInfo->getEmptyTable('1%','End of this page');
	$html .= $objStudentInfo->getPagebreakTable();
	
	
	$html .=$objStudentInfo->getEmptyTable('8%');
	$html .='<table width="100%" height="930px">';
	$html .= '<tr><td valign="top" style="padding-top:10px;">';
	$html .= $objStudentInfo->getPart2_HTML();
	$html .= $objStudentInfo->getPart7_HTML();
	$html .= $objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getPart8_HTML();
	$html .= '</td></tr>';
	$html .='</table>';
	$html .=$objStudentInfo->getEmptyTable('1%','End of the Profile');
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
	$totalPageNo = $objStudentInfo->getPageNo()-1;
	$html = str_replace('<!--totalPageNo-->',$totalPageNo,$html);

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>