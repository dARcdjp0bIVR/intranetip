<?php
/*
 * Using:
 * 
 * Modification:
 * 	2013-03-26	(Ivan) [2013-0326-1646-48073]
 * 		- "OutOfAward" retrieve all OLE and OutsideSchool records now
 * 		- Show Award table at the top and then Activity table at the bottom now
 */
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/hwc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/hwc/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);
$NoOfFirstPage = trim($NoOfFirstPage);
$NoOfOtherPage = trim($NoOfOtherPage);

//debug_r($_POST);

//YearClassID

$objReportMgr = new RptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);


$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

$form_class_manage = new form_class_manage();

$AcademicTermArr = array();
for($i=0,$i_MAX=count($academicYearID);$i<$i_MAX;$i++)
{
	$thisAcademicYearID = $academicYearID[$i];
	$thisTermArr = $form_class_manage->Get_Academic_Year_Term_List($thisAcademicYearID, $NoPastTerm=0, $PastTermOnly=0, $YearTermID_Compare='', $IncludeYearTermIDArr='');

	for($j=0,$j_MAX=count($thisTermArr);$j<$j_MAX;$j++)
	{
		$thisTermID = $thisTermArr[$j]['YearTermID']; 
		$AcademicTermArr[$thisAcademicYearID][] = $thisTermID;
	}
}

$lpf_academic =  new libpf_academic();
$Student_Academic_ResultArr = $lpf_academic->Get_Student_Academic_Result($studentList, $SubjectIDArr='', $academicYearID, $YearTermIDArr='', $ParentSubjectOnly=1, $IgnoreEmptyScore=0, $IgnoreNoRanking=1);

$StdAcademicResultArr = array();
for($i = 0,$i_max = count($Student_Academic_ResultArr);$i < $i_max; $i++)
{
	$thisStdID = $Student_Academic_ResultArr[$i]['StudentID'];
	$thisSubjectID = $Student_Academic_ResultArr[$i]['SubjectID'];
	$thisAcademicYearID = $Student_Academic_ResultArr[$i]['AcademicYearID'];
	$thisYearTermID = $Student_Academic_ResultArr[$i]['YearTermID'];
	$thisGrade = $Student_Academic_ResultArr[$i]['Grade'];

	$StdAcademicResultArr[$thisStdID][$thisSubjectID][$thisAcademicYearID][$thisYearTermID] = $thisGrade;
}

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	$objStudentInfo -> setAcademicTermArr($AcademicTermArr);
	$objStudentInfo -> setStdAcademicResultArr($StdAcademicResultArr);

	$OLEInfoArr_Award = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='',$Selected=true,$category='Award');
	//2013-0326-1646-48073
	//$OLEInfoArr_OutOfAward = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true,$category='OutOfAward');
	$OLEInfoArr_OutOfAward = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='',$Selected=true,$category='OutOfAward');

	$PageOne_html = '';
	$PageOne_html .=$objStudentInfo->getEmptyTable('3%');
	$PageOne_html .= $objStudentInfo->getPart1_HTML();	
	$PageOne_html .=$objStudentInfo->getEmptyTable('1%');
	$PageOne_html .= $objStudentInfo->getPart2_HTML();	
	$PageOne_html .=$objStudentInfo->getEmptyTable('2%');
	$PageOne_html .= $objStudentInfo->getPart3_HTML();	
	
	$PageTwo_html = '';
	//2013-0326-1646-48073
	//$PageTwo_html .=$objStudentInfo->getPart5_HTML($OLEInfoArr_OutOfAward);
	$PageTwo_html .=$objStudentInfo->getPart4_HTML($OLEInfoArr_Award);	
	$PageTwo_html .=$objStudentInfo->getEmptyTable('1%');
	//2013-0326-1646-48073
	//$PageTwo_html .=$objStudentInfo->getPart4_HTML($OLEInfoArr_Award);
	$PageTwo_html .=$objStudentInfo->getPart5_HTML($OLEInfoArr_OutOfAward);

	### Page Content
	$html .='<table width=100%>'; 
		$html .='<col width="50%" />';
		$html .='<col width="50%" />';
		$html .='<tr>
					<td style="vertical-align:top;">
						<table width=100% height="1000px" >
							<tr><td style="vertical-align:top;">'.$PageOne_html.'</td></tr>
						</table>
						'.$objStudentInfo->getSignature_HTML($PageBreak='').'
						'.$objStudentInfo->getEmptyTable($Height='',$Content='P1').'
		  			</td>
					<td style="vertical-align:top;">
						<table width=100% height="1020px" >
							<tr><td style="vertical-align:top;">'.$PageTwo_html.'</td></tr>
						</table>
						'.$objStudentInfo->getEmptyTable($Height='',$Content='P2').'	
		  			</td>
				</tr>';
	$html .='</table>';
	
	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .='<table width=100%>'; 
		$html .='<col width="50%" />';
		$html .='<col width="50%" />';
		$html .='<tr>
					<td style="vertical-align:top;">
						'.$objStudentInfo->getPart6_HTML().'
						'.$objStudentInfo->getEmptyTable('1%').'	
						'.$objStudentInfo->getEmptyTable($Height='',$Content='完<br/>End of Report').'
		  			</td>
					<td>&nbsp;</td>
				</tr>';
	$html .='</table>';
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>