<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cyma/libpf-slp-cyma.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");



$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
//$StudentID = trim($StudentID);
$issuedate  = trim($issuedate);
if ($issuedate!="")
{
	$issuedateStr = date("d/m/Y", strtotime($issuedate));
} else
{
	$issuedateStr = "&nbsp;";
}

	$objReportMgr = new ipfReportManager();
	$objReportMgr->setYearClassID($YearClassID);
	
	$objReportMgr->setStudentID($StudentID);
	
	$academicYearID = Get_Current_Academic_Year_ID();
	$objReportMgr->setAcademicYearID($academicYearID);
	$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();


if ($YearTermID=="")
{
	$YearTermNameB5 = "全年";
	$YearTermNameEN = "Whole Year";
} else
{
	$TermObj =  new academic_year_term($YearTermID);
	$YearTermNameB5 = $TermObj->YearTermNameB5;
	$YearTermNameEN = $TermObj->YearTermNameEN;	
}

	$studentList = $objReportMgr->getStudentIDList();



require_once($PATH_WRT_ROOT.'includes/tcpdf60/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
    	global $PATH_WRT_ROOT;

		// no header
    }

    // Page footer
    public function Footer() {
    	
    	global $YearTermNameEN, $academicYearStartEndDisplay;
    	
        // Position at 15 mm from bottom
        $this->SetY(-15);
		$this->MultiCell(180, 20, '<font color="#AAAAAA">'.$this->StudentName.' - OLE Report '.$academicYearStartEndDisplay.' '.$YearTermNameEN.'</font>', 0, 'R', 0, 0, '', '', true, 0, true, true, 20, 'M');
		//<table width="200" border="0" cellpadding="0" cellspacing="0"><tr><td style=\'border-bottom:1px black solid;\'></td></tr></table>
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
$pdf->SetTitle('T. W. G. Hs. C. Y. MA Memorial College SLP');
$pdf->SetSubject('Student Learning Portfolio');
$pdf->SetKeywords('T. W. G. Hs. C. Y. MA Memorial College', 'eClass, iPortfolio, SLP');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "T. W. G. Hs. C. Y. MA Memorial College", "T. W. G. Hs. C. Y. MA Memorial College");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//debug(PDF_MARGIN_LEFT);die();
$pdf->SetMargins(13, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------


	
$academic_year_lib = new academic_year();
//$ay_records = $academic_year_lib->Get_All_Year_List();
//$AcademicYearArray = build_assoc_array($ay_records);
	
# loop students
for ($i = 0; $i < sizeof($studentList); $i++)
{	
	$_studentId = $studentList[$i];

	$objStudentInfo = new studentInfo($_studentId);
	$objStudentInfo->setAcademicYear($objReportMgr->getAcademicYearID());
	$objStudentInfo->setYearTerm($YearTermID);
	
	//$objStudentInfo->setAcademicYearDisplay($academicYearStartEndDisplay);
	
	$StudentInfoArr = $objStudentInfo->getStudentInfo();
	$StudentInfoArr = $StudentInfoArr[0];
	
	
	if (!isset($ELEArray))
	{
		$ELEArray = build_assoc_array($objStudentInfo->getELE());
	}
	
	if (!isset($CategoryArray))
	{
		$CategoryArray = build_assoc_array($objStudentInfo->getCategory());
	}


	$pdf->StudentName = $StudentInfoArr["EnglishName"];
	// add a page
	$pdf->AddPage();
	
	$pdf->SetY(7);
	 
	$pdf->SetFont('arialunicid0', 'B', 12);
    //Title
    
	$pdf->Cell(0, 0, '東華三院馬振玉紀念中學', 0, 1, 'C', 0, '', 0);
	$pdf->SetFont('arialunicid0', 'B', 11);
	$pdf->Cell(0, 0, 'T. W. G. Hs. C. Y. MA Memorial College', 0, 1, 'C', 0, '', 0);
	$pdf->SetFont('arialunicid0', 'B', 12);    
	$pdf->Cell(0, 0, '學生表現報告：「其他學習經歷」', 0, 1, 'C', 0, '', 0);
	$pdf->SetFont('arialunicid0', 'B', 11);
	$pdf->Cell(0, 0, 'Student Report: Other Learning Experiences', 0, 1, 'C', 0, '', 0);
	$pdf->SetFont('arialunicid0', 'B', 12);    
	$pdf->Cell(0, 0, $academicYearStartEndDisplay, 0, 1, 'C', 0, '', 0);
	$pdf->Cell(0, 0, $YearTermNameB5, 0, 1, 'C', 0, '', 0);
	$pdf->SetFont('arialunicid0', 'B', 11);
	$pdf->Cell(0, 0, $YearTermNameEN, 0, 1, 'C', 0, '', 0);
	
	$StudentName = $StudentInfoArr["EnglishName"];
    
		
	$pdf->SetXY(157, 7);
	$pdf->Image($PATH_WRT_ROOT.'file/customization/cyma_logo.png', '', '', 36, '', '', '', 'T', false, 300, '', false, false, 1, false, false, false);

	$pdf->SetY(34);
	$pdf->ln();
	// set font
	//$pdf->SetFont('dejavusans', '', 10);
	
	$pdf->SetFont('droidsansfallback', '', 9);

	// create some HTML content
	$html_student_info = '<br /><br /><table width="100%" border="0" cellpadding="5" cellspacing="0">' .
	
		'<tr>' .
			'<td width="20%"><font face="arialunicid0">學生姓名</font> <font face="helvetica">Name</font> :</td><td colspan="4" width="47%"><font face="helvetica">'.$StudentInfoArr["EnglishName"].'</font> '.$StudentInfoArr["ChineseName"].'</td>' .			
			'<td width="2%"> </td>' .
			'<td width="22%"><font face="arialunicid0">班別</font> <font face="helvetica">Class Name</font> :</td><td width="11%"><font face="helvetica">'.$StudentInfoArr["ClassName"].'</font></td>' .
		'</tr>' .
		'<tr>' .
			'<td width="20%"><font face="arialunicid0">性別</font> <font face="helvetica">Sex</font> :</td><td width="14%"><font face="helvetica">'.$StudentInfoArr["Gender"].'</font></td>' .			
			'<td width="2%"> </td>' .
			'<td width="18%"><font face="arialunicid0">註冊編號</font> <font face="helvetica">Reg. No</font> :</td><td width="13%"><font face="helvetica">'.$StudentInfoArr["WebSAMSRegNo"].'</font></td>' .
			'<td width="2%"> </td>' .
			'<td width="22%"><font face="arialunicid0">班號</font> <font face="helvetica">Class No</font> :</td><td width="11%"><font face="helvetica">'.$StudentInfoArr["ClassNumber"].'</font></td>' .
		'</tr>' .
		'<tr>' .
			'<td ><font face="arialunicid0">出生日期</font> <font face="helvetica">Date of Birth</font> :</td><td ><font face="helvetica">'.$StudentInfoArr["DateOfBirth"].'</font></td>' .
			'<td > </td>' .
			'<td ><font face="arialunicid0">學生編號</font> <font face="helvetica">STRN</font> :</td><td ><font face="helvetica">'.$StudentInfoArr["STRN"].'</font></td>' .
			'<td > </td>' .
			'<td ><font face="arialunicid0">派發日期</font> <font face="helvetica">Date of Issue</font> :</td><td><font face="helvetica">'.$issuedateStr.'</font></td>' .
		'</tr>' .
	
		'</table>' .
		'<p><font size="9" face="arialunicid0">學會</font></p>';

	$Club_Records = $objStudentInfo->getClubEnrolment();	
	

	$html = $html_student_info;
	
	$html .= '<table width="650" border="1" cellpadding="6" cellspacing="0" align="center">' .
		'<thead><tr>' .
			'<td width="35%"><font face="arialunicid0">學會名稱</font></td>' .
			'<td width="17%"><font face="arialunicid0">職位</font></td>' .
			'<td width="48%" ><font face="arialunicid0">評語</font></td>' .
		'</tr></thead>';
	for ($j=0; $j<sizeof($Club_Records); $j++)
	{
		$RecordObj = $Club_Records[$j];
		$html .= '<tr>' .
				'<td width="35%">'.$RecordObj["GroupTitle"].'</td>' .
				'<td width="17%">'.$RecordObj["RoleTitle"].'</td>' .
				'<td width="48%" >'.$RecordObj["CommentStudent"].'</td>' .
			'</tr>';
	}
		
	if (sizeof($Club_Records)<=0)
	{
		$html .= '<tr>' .
				'<td colspan="3" align="center">沒 有 紀 錄</td>' .
			'</tr>';
	}
		
	$html .= '</table>';
	
	
	$OLE_Records = $objStudentInfo->GetOLE();
	
	$html .=  GetOLESection($OLE_Records, $ELEArray, "[ID]");
	$html .=  GetOLESection($OLE_Records, $ELEArray, "[MCE]");
	$html .=  GetOLESection($OLE_Records, $ELEArray, "[AD]");
	$html .=  GetOLESection($OLE_Records, $ELEArray, "[CE]");
	$html .=  GetOLESection($OLE_Records, $ELEArray, "[PD]");
	$html .=  GetOLESection($OLE_Records, $ELEArray, "OTHERS");
	
		
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');


	
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('pkms_slp.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function GetOLESection($OLE_Records, $ELEArray, $ELE_CODE)
{
	
	$SectionTitle = ($ELE_CODE=="OTHERS") ? "其他" : $ELEArray[$ELE_CODE];
	$rX = '<p><font size="9" face="arialunicid0">'.$SectionTitle.'</font></p>';
	
	
	$rX .= '<table width="650" border="1" cellpadding="6" cellspacing="0" align="center">' .
		'<thead><tr>' .
			'<td width="35%"><font face="arialunicid0">活動項目</font></td>' .
			'<td width="17%"><font face="arialunicid0">職位 / 參與角色</font></td>' .
			'<td width="24%" ><font face="arialunicid0">主辦機構</font></td>' .
			'<td width="9%" ><font face="arialunicid0">活動時數</font></td>' .
			'<td width="15%" ><font face="arialunicid0">備註</font></td>' .
		'</tr></thead>';
	
	$RecordFound = false;
	
	for ($i=0; $i<sizeof($OLE_Records); $i++)
	{
		$RecordObj = $OLE_Records[$i];
		if (strstr($RecordObj["ELE"], $ELE_CODE) || 
			($ELE_CODE=="OTHERS" && !strstr($RecordObj["ELE"], "[ID]") && !strstr($RecordObj["ELE"], "[MCE]") && !strstr($RecordObj["ELE"], "[AD]") && !strstr($RecordObj["ELE"], "[CE]") && !strstr($RecordObj["ELE"], "[PD]")))
		{
			$rX .= '<tr>' .
					'<td width="35%">'.$RecordObj["Title"].'</td>' .
					'<td width="17%">'.$RecordObj["Role"].'</td>' .
					'<td width="24%" >'.$RecordObj["Organization"].'</td>' .
					'<td width="9%" align="Center-Top">'.$RecordObj["Hours"].'</td>' .
					'<td width="15%" >'.$RecordObj["Remark"].'</td>' .
				'</tr>';
			$RecordFound = true;
		}
	}
		
	if (!$RecordFound)
	{
		$rX .= '<tr>' .
				'<td colspan="5" align="center">沒 有 紀 錄</td>' .
			'</tr>';
	}
		
	$rX .= '</table>';
	
	return $rX;
}

?>