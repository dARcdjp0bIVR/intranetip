<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lcg/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/lcg/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);

$objReportMgr = new rptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


$html = '';

$wholeReportTemple = <<<Whole_Report_Temple
<div class="oleReportContainer" >
<table width="760px" align="center" height="940px">
	<tr>
		<td valign="top" height="890">
			<table width="100%" height="100%">
				<tr>
					<td valign="top" align="Center" height="100%">
						<!--ReportHeaderHTML-->
						<br/><br/>
						<!--StudentDetailsHTML-->
						<br/><br/>
						<!-- ExtraCurricularActivitesHtml -->
						<br/><br/>
						<!--PerformanceAndAwardsHtml-->
						<br/><br/>
						<!--OtherHtml-->
						<br/><br/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td valign="botton" height="50">
		<!--FooterHtml-->
	</td></tr>
</table>
</div>
Whole_Report_Temple;


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	$thisStudentHTML = '';	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);


/*
	$htmlPart1 = $objStudentInfo->get2ndReportStudentPersonalInfoHtml();
	$htmlPart2 = $objStudentInfo->get2ndReportExtraCurricularActivitesHtml();
	$htmlPart3 = $objStudentInfo->get2ndReportPerformanceAndAwardsHtml();
	$htmlPart4 = $objStudentInfo->get2ndReportOtherHtml();
	$htmlPart5 = $objStudentInfo->get2ndReportFooter();
*/





	$thisStudentHTML = str_replace('<!--ReportHeaderHTML-->',$objStudentInfo->get2ndReportHeaderHTML($displayReportHeader),$wholeReportTemple);
	$thisStudentHTML = str_replace('<!--StudentDetailsHTML-->',$objStudentInfo->get2ndReportStudentDetailsHTML(),$thisStudentHTML);
	$thisStudentHTML = str_replace('<!-- ExtraCurricularActivitesHtml -->',$objStudentInfo->get2ndReportExtraCurricularActivitesHtml(),$thisStudentHTML);
	$thisStudentHTML = str_replace('<!--PerformanceAndAwardsHtml-->',$objStudentInfo->get2ndReportPerformanceAndAwardsHtml(),$thisStudentHTML);
	$thisStudentHTML = str_replace('<!--OtherHtml-->',$objStudentInfo->get2ndReportOtherHtml(),$thisStudentHTML);
	$thisStudentHTML = str_replace('<!--FooterHtml-->',$objStudentInfo->get2ndReportFooter(),$thisStudentHTML);


	$html .= $thisStudentHTML;
	if($i != $i_max-1){
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
}
?>
<style type="text/css">
.oleRecordTable {
	border:1px solid black;
	width:100%;
}
.oleSectionTitle{
	text-align:center;
	text-decoration: underline;
	font-weight:bold;
}
.oleReportContainer {
	width:100%; 
	height:940px;
    margin:1%;
/*	border:3px #cccccc dashed;*/
/*

	width:90%;
	border:3px #cccccc dashed;
	text-align: center; 

	height:60px;

	*/
}
</style>
<?
if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}
?>