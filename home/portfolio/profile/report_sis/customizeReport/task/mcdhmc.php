<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/mcdhmc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/mcdhmc/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."file/reportcard2008/templates/sha_tin_methodist.css");


$objIPF = new libportfolio();
function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $objIPF; // object of libportfolio
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){
				$objIPF->insertIntoIntranetUser($returnStudentAry[$a]);
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
}
function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $objIPF; // object of libportfolio
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$objIPF->removeFromIntranetUser($StudentArr[$a]);
			}
		}
}

if (is_array($alumni_StudentID) && sizeof($alumni_StudentID)>0 )
{
	$StudentID = $alumni_StudentID;
	//$alumni_StudentID = explode(",", $alumni_StudentID);
	$AlumniStudentArr = _handleCurrentAlumniStudentBeforePrinting("alumni",$alumni_StudentID,$StudentArr);
	$ForAlumni = true;
}

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);



$objReportMgr = new RptMgr();


$objReportMgr->setYearClassID($YearClassID);


$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

for ($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++)
{
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	

	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);
	$OLEInfoArr_EXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=false);

	### Page Content
	$html .= $objStudentInfo->getPart3_HTML($OLEInfoArr_INT);	
	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .=$objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('1%');
	$html .=$objStudentInfo->getPart2_HTML();

	$html .= '<table align="center" width="100%" height="800px" cellpadding="0" style="">
					<tr><td valign="top">';
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr_EXT);
	$html .=$objStudentInfo->getEmptyTable('4%');
	$html .= $objStudentInfo->getPart5_HTML();
	$html .='</td></tr></table>';
	$html .= $objStudentInfo->getFooter_HTML($Page_break); //principal signature	
	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .= $objStudentInfo->getPart6_HTML();

	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}

_handleCurrentAlumniStudentAfterPrinting("alumni", $AlumniStudentArr);

if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>