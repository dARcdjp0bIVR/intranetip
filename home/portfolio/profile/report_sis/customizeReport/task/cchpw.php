<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cchpw/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cchpw/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");
include_once($PATH_WRT_ROOT."home/portfolio/profile/report/report_lib/slp_report.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");


$lpf_slp = new libpf_slp();

$slp_report = new slp_report();

$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);
$NoOfFirstPage = trim($NoOfFirstPage);
$NoOfOtherPage = trim($NoOfOtherPage);

$objReportMgr = new RptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

if (!$printIssueDate)
{
	$issuedate = "&nbsp;";
} else
{
	$issuedate = date("d/m/Y", strtotime($issuedate));
}



$html = '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_report.css"></style>';
$html .= '<style type="text/css" src="http://'.$eclass_httppath.'/src/includes/css/style_portfolio_table.css"></style>';
$html .= '<STYLE TYPE="text/css">
.report_header {
padding:3px;
font-size:13pt;
font-weight:bold;
font-family:Arial
}

.module_title {
color:#FFFFFF;
padding:3px;
font-size:12pt;
font-weight:bold;
font-family:Arial
}

.padding_all {
padding:3px
}

</STYLE>';
for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	if($NoOfFirstPage!=''){
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!=''){
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}
	
	
	$html .= $objStudentInfo->generateReport($_studentId,$academicYearID,$IntExt='INT',$Selected=false,$isMember=false, $lastStudent = (($i+1)==$i_max));
	
	/*
	
	$html .= $objStudentInfo->getStudentInfo_HTML();
	$html .= $objStudentInfo->getOLE_HTML($_studentId,$academicYearID,$IntExt='INT',$Selected=false);
	$html .= $objStudentInfo->getSchoolAward_HTML();
	$html .= $objStudentInfo->getSelfAccount_HTML();
	$html .= $objStudentInfo->getSignature_HTML();
	*/
	/*
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);
	
	if($NoOfFirstPage!='')
	{
		$objStudentInfo -> setNoOfFirstPage($NoOfFirstPage);
	}
	if($NoOfOtherPage!='')
	{
		$objStudentInfo -> setNoOfOtherPage($NoOfOtherPage);
	}

	$OLEInfoArr = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);

	### Page Content
//	$html .=$objStudentInfo->getEmptyTable('3%');
//	$html .= $objStudentInfo->getPart1_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('1%');
//	$html .= $objStudentInfo->getPart2_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
//	$html .= $objStudentInfo->getPart3_HTML();	
//	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr);	
	
//	$html .=$objStudentInfo->getPagebreakTable();
	$html .=$objStudentInfo->getEmptyTable('3%');
	
	$html .='<table width=100% height="925px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart5_HTML().'</td></tr>';
	$html .='</table>';

	$html .= $objStudentInfo->getSignature_HTML();
	$html .=$objStudentInfo->getEmptyTable('3%');
	$html .= $objStudentInfo->getEmptyTable($Height='',$Content='Page '.$objStudentInfo->getPageNumberOfThisPage().' of <!--totalPage-->');	
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
	$thisTotalPage = $objStudentInfo->getPageNumberOfThisPage();
	$html = str_replace('<!--totalPage-->',$thisTotalPage,$html);
	*/
}

echo $objReportMgr->outPutToHTML($htmlCSS.$html);


exit();
?>