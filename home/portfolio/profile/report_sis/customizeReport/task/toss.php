<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/toss/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/toss/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);

//YearClassID

$objReportMgr = new stcecRptMgr();

$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();


for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];

	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	$objStudentInfo -> setYearClassID($YearClassID);

	$OLEInfoArrEXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=false);
	$OLEInfoArrINT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=true);

	### Page Content
	
	$part2_html = $objStudentInfo->getPart2_HTML();
	if($part2_html!='' || count($OLEInfoArrEXT)>0)
	{
		$html .=$objStudentInfo->getEmptyTable('2%');
		$html .= $objStudentInfo->getPart1_HTML();	
		$html .=$objStudentInfo->getEmptyTable('2%');
		$html .= $part2_html;		
		$html .=$objStudentInfo->getEmptyTable('2%');
		$html .= $objStudentInfo->getPart3_HTML($OLEInfoArrEXT);	
		$html .=$objStudentInfo->getPagebreakTable();
	}
	
	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart4_HTML();		
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart5_HTML($OLEInfoArrINT);	
	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .= $objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getEmptyTable('2%');
	$html .='<table width=100% height="800px">'; 
		$html .='<tr><td style="vertical-align:top;">'.$objStudentInfo->getPart6_HTML().'</td></tr>';
	$html .='</table>';

	$html .= $objStudentInfo->getSignature_HTML();
	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>