<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$li_pf = new libportfolio();
$lyc = new year_class($_GET['YearClassID']);
$ClassName = $lyc->ClassTitleEN;

$StudentArr = $li_pf->GET_STUDENT_LIST_DATA($ClassName, false);

for($i=0; $i<count($StudentArr); $i++)
{
	$student = $li_pf->GET_STUDENT_DATA($StudentArr[$i]['UserID']);
	$studentRowArr[] = array($student[0]['ClassNumber'], $student[0]['ChineseName'], "", "");
}
$ColumnDef = array("ClassNumber", "StudentName", "ServiceHour", "StarAward");

$lexport = new libexporttext();
$outputText = $lexport->GET_EXPORT_TXT($studentRowArr, $ColumnDef);
$lexport->EXPORT_FILE("star_award.csv", $outputText);
?>