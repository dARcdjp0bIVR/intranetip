<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$REPORT_WIDTH = 750;

$issueDate = $_GET["issuedate"];
$TargetClass = $_GET["TargetClass"];
$YearInterval = $_GET["Year"];
$academicYearID = $_GET["academicYearID"];
$StudentID = $_GET["StudentID"];

$libportfolio = new libportfolio();
$studentIdList = getUserIdList($TargetClass, $StudentID);
$studentsOleItems = getStudentOleItems($studentIdList);
$html = generateReportHtml($studentsOleItems, $academicYearID,$issueDate);
echo $html;

function getReportTemplate() {
	$META = returnHtmlMETA();
	$html = <<<HTMLEND
	<table border="0" width="100%" style="text-align: center">
	{{{ SCHOOL_BANNER }}}
	{{{ STUDENT_INFO }}}
	{{{ OLE_RECORDS }}}
	{{{ DATE_AND_SIGNATURE }}}
	</table>
HTMLEND;
	return $html;
}

function generateSchoolBannerPartHtml() {
	global $Lang;
	
	$html = <<<HTMLEND
		<tr><td>
<table style="height: 80px">
<tr>
<td>&nbsp;</td></tr></table>
</td></tr>
HTMLEND;
	return $html;
}

function generateStudentInfoPartHtml($ParEnglishName, $ParChineseName, $ParClassName, $ParClassNumber) {
	global $PATH_WRT_ROOT, $intranet_session_language, $REPORT_WIDTH;
	
	include($PATH_WRT_ROOT."lang/iportfolio_lang.en.php");
	$iPortEng = $iPort;
	include($PATH_WRT_ROOT."lang/iportfolio_lang.b5.php");
	$iPortBig5 = $iPort;
	include($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
	$html = <<<HTMLEND
		  <tr>
		    <td>
			  <table width="{$REPORT_WIDTH}" style="font-size: 14pt" border="0">
				  <tr>
				    <td rowspan="2" width="8%">&nbsp;				      
				    </td>
				    <td rowspan="2" width="70%">
				      {$ParEnglishName}<br/>
				      {$ParChineseName}
				    </td>
				    <td width="2%">
				    $ParClassName
				    </td>
				  </tr>
				  <tr>
				    <td>
					$ParClassNumber
				    </td>
				  </tr>
			  </table>
		  	</td>
		  </tr>
HTMLEND;
	return $html;
}

function generateOleRecordsPartHtml($ParItems,$academicYearID) {
	global $iPort, $REPORT_WIDTH;
	$html = "";
	$tableShell = <<<HTMLEND
	<tr><td>
	<table border="0" width="{$REPORT_WIDTH}" style="font-size:14pt; height: 24cm" valign="top">
	  <tr style="text-align:left; font-weight: bold; text-decoration: underline; height:1; line-height:220%" valign="bottom">
        <th width="60%">
          {$iPort["Item"]}
        </th>
        <th nowrap='nowrap' width="25%">
          {$iPort["Award"]}
        </th nowrap='nowrap'>
        <th>
          {$iPort["Post"]}
        </th>
      </tr>
        {{{ RECORDS }}}
      <tr><td colspan='5'>&nbsp;</td></tr> <!-- This line is used to locate the date and signature part -->
    </table>
    </td></tr>
HTMLEND;
	
	
	$yearDateInterval = getYearDateInterval($academicYearID);
	
	if (count($ParItems)>0) {
		foreach($ParItems as $key => $element) {
			$awardPost = array();
			if ( ($element["RECORDSTATUS"] == 2 || $element["RECORDSTATUS"] == 4)
				&& (
						empty($yearDateInterval) ||
						( strtotime($element["STARTDATE"]) >= strtotime($yearDateInterval["YEAR_START_DATE"])
							&& strtotime($element["STARTDATE"]) <= strtotime($yearDateInterval["YEAR_END_DATE"])
						)
					) ) {
						$recordsHtml .= "<tr style='line-height:150%' height=1><td>" . $element["TITLE"] . "</td><td>" . $element["ACHIEVEMENT"] . "</td><td>" . $element["ROLE"] . "</td></tr>";
					}
		}
	} else {
		// do nothing
	}
	$html = str_replace("{{{ RECORDS }}}", $recordsHtml, $tableShell);
	return $html;
}

function generateDateAndSignaturePartHtml($ParIssueDate="") {
	global $ec_iPortfolio, $REPORT_WIDTH;
	$date = date("d-n-Y");
	$html = <<<HTMLEND
	<tr><td>
	<br/><br/>
	<table width="{$REPORT_WIDTH}" style="text-align: center; border-collapse: collapse; font-weight: bold; font-size: 14pt">
		<tr>
			<td rowspan="2" width="4%">
			&nbsp;
			</td>
			<td width="30%">
			{$ParIssueDate}	
			</td>
			<td rowspan="2">
			&nbsp;
			</td>
			<td width="30%">
			&nbsp;
			</td>
			<td rowspan="2" width="10%">
			&nbsp;
			</td>
		</tr>
		<tr>
			<td>
			&nbsp;
			</td>
			<td>
			&nbsp;
			</td>
		</tr>
	</table>
	</td></tr>
HTMLEND;
	return $html;
}

function getReportCss() {
	$css = <<<CSSEND
	<style>
	.colon {text-align: right; width="1";}
	</style>
CSSEND;
	return $css;
}

function generateReportHtml($ParStudentsOleItems, $academicYearID,$ParIssueDate="") {
	$html = "";
	if (count($ParStudentsOleItems)>0) {
		$recordHtmlArray = array();
		$template = getReportTemplate();
		foreach($ParStudentsOleItems as $userid => $element) {
			$schoolBanner = generateSchoolBannerPartHtml();
			$studentInfo = generateStudentInfoPartHtml($element["ENGLISHNAME"], $element["CHINESENAME"], $element["CLASSNAME"], $element["CLASSNUMBER"]);
			$oleRecords = generateOleRecordsPartHtml($element["ITEMS"],$academicYearID);
			
			$dateAndSignature = generateDateAndSignaturePartHtml($ParIssueDate);
			$recordHtmlArray[] = str_replace(
									array("{{{ SCHOOL_BANNER }}}", "{{{ STUDENT_INFO }}}", "{{{ OLE_RECORDS }}}", "{{{ DATE_AND_SIGNATURE }}}"),
									array($schoolBanner,$studentInfo,$oleRecords,$dateAndSignature),
									$template);
		}
		$html = implode("<br style='page-break-before: always;' />", $recordHtmlArray);
	} else {
		$html = "There are no records";
	}
	
	$META = returnHtmlMETA();
	$CSS = getReportCss();
	$html = <<<HTMLEND
	<html>
	{$META}
	{$CSS}
	<body>
	
	{$html}
	</body>
	</html>
HTMLEND;
	return $html;
}

function getStudentOleItems($ParStudentIdList/*, $ParYearInterval*/) {
	
	global $intranet_db, $eclass_db, $libportfolio;

	$sql = "SELECT
				IU.USERID, IU.ENGLISHNAME, IU.CHINESENAME, IU.CLASSNAME, IU.CLASSNUMBER,
				OP.TITLE,
				OS.ACHIEVEMENT, OS.ROLE,
				OS.STARTDATE, OS.RECORDSTATUS
			FROM $intranet_db.INTRANET_USER IU
			LEFT JOIN $eclass_db.OLE_STUDENT OS
				ON OS.USERID = IU.USERID
			LEFT JOIN $eclass_db.OLE_PROGRAM OP
				ON OS.PROGRAMID = OP.PROGRAMID
			WHERE
				IU.USERID IN (" . implode(",",$ParStudentIdList) . ")
			ORDER BY IU.CLASSNUMBER";
//			debug_r($sql);
	$returnArray = $libportfolio->returnArray($sql);
//	debug_r($returnArray);
	
	$formattedArray = array();
	if (count($returnArray)>0) {
		foreach($returnArray as $key => $element) {
			if(!isset($formattedArray[$element["USERID"]])) {
				$formattedArray[$element["USERID"]]["USERID"] = $element["USERID"];
				$formattedArray[$element["USERID"]]["ENGLISHNAME"] = $element["ENGLISHNAME"];
				$formattedArray[$element["USERID"]]["CHINESENAME"] = $element["CHINESENAME"];
				$formattedArray[$element["USERID"]]["CLASSNAME"] = $element["CLASSNAME"];
				$formattedArray[$element["USERID"]]["CLASSNUMBER"] = $element["CLASSNUMBER"];
			}
			$formattedArray[$element["USERID"]]["ITEMS"][] = array("TITLE"=>$element["TITLE"], "ACHIEVEMENT"=>$element["ACHIEVEMENT"], "ROLE"=>$element["ROLE"], "STARTDATE"=>$element["STARTDATE"],"RECORDSTATUS"=>$element["RECORDSTATUS"],);
		}
	}
	
	return $formattedArray;
}

/**
 * Get the year start date and year end date by year range i.e. [2007-2008]
 * @param String $academicYearID 
 * @return Array array("YEAR_START_DATE"=>2007-09-01 00:00:00, "YEAR_END_DATE"=>2008-08-31 23:59:59)
 */
function getYearDateInterval($academicYearID) {
	if (empty($academicYearID)) {
		$returnInterval = array();
	} else {
		$returnInterval = array("YEAR_START_DATE"=>getStartDateOfAcademicYear($academicYearID),"YEAR_END_DATE"=>getEndDateOfAcademicYear($academicYearID));
	}
	
	return $returnInterval;
}

function getUserIdList($ParTargetClass, $ParStudentId) {
	global $intranet_db, $eclass_db, $libportfolio;
	if (!empty($ParStudentId)) {
		$StudentArr = array($ParStudentId);
	} else {

		$sql = "SELECT 
					IU.USERID, IU.CLASSNAME, IU.CLASSNUMBER
				FROM 
					{$intranet_db}.INTRANET_USER AS IU 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS PS ON PS.USERID=IU.USERID
					INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU ON IU.USERID=YCU.USERID
				WHERE 
					IU.RECORDTYPE='2' 
					AND YCU.YEARCLASSID = '$ParTargetClass'
				ORDER BY
					IU.CLASSNUMBER
				";
				$SA = $libportfolio->returnArray($sql);
		$StudentArr = $libportfolio->returnVector($sql);
	}
//	hdebug_r($SA);
	return $StudentArr;
}

intranet_closedb();
?>
