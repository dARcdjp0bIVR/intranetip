<?php
// Modifing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$linterface = new interface_html("popup.html");
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();
$lpf->ADMIN_ACCESS_PAGE();

list($year_selection_html, $semester_selection_html) = $lpf->getAwardYearSemester();

$CurrentPage = "eClass_update_award";
$title = $ec_iPortfolio['eClass_update_award'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();
?>

<script language="">
// check if year and semester are both available
function checkform(frmObj){
	if (typeof(frmObj.Year)=="undefined")
	{
		alert("<?=$ec_warning['eclass_data_no_record']?>");
		return false;
	} else if (!confirm("<?=$ec_warning['eclass_data_syn']?>"))
	{
		return false;
	}
	
	frmObj.btn_submit.disabled=true;
	return true;
}
</script>

<?php $disabled = ($semester_selection_html=="<i>".$no_record_msg."</i>") ? "DISABLED": ""; ?>

<FORM action="data_syn_update.php" method="POST" name="form1" onSubmit="return checkform(this)">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
          <td height="10" colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" class="tabletext"><?=$ec_iPortfolio['year']?>:</td><td class="tabletext"><?= $year_selection_html ?></td>
        </tr>
        <tr>
			<td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['year_period']?>:</td><td class="tabletext"><input type=radio name=IsAnnual id="annual_1" value=1 CHECKED><label for="annual_1"><?=$ec_iPortfolio['whole_year']?></label><br>
			<input type=radio name=IsAnnual id="annual_0" value=0 <?=$disabled?>><label for="annual_0"><?=$ec_iPortfolio['semester']?></label> : <?= $semester_selection_html ?></td>        
		</tr>
      </table>

	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" name="btn_submit">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		</td>
	</tr>
	</table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
