<?php
// Editing by 
/*
 * For project $sys_custom['LivingHomeopathy'].
 * Please edit with UTF-8 encoding.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/LivingHomeopathy/liblivinghomeopathy.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

if(!$sys_custom['LivingHomeopathy']) exit;

intranet_opendb();

$TargetUserID = $_SESSION['UserType'] == USERTYPE_STUDENT? $_SESSION['UserID'] : (isset($TargetUserID)? $TargetUserID : $_SESSION['UserID']);
$view_style = $IsEdit? ' style="display:none" ' : '';
$edit_style = $IsEdit? '' : ' style="display:none" ';
$IsEditable = $_SESSION['UserType'] == USERTYPE_STAFF;

$user_fields = array("ChineseName",
					"EnglishName",
					"Photo",
					"Gender",
					"HKID",
					"DateOfBirth",
					"UserEmail",
					"HomeTelNo",
					"MobileTelNo",
					"Address");
/*
$data = array(
"ChineseName"=>"",
"EnglishName"=>"",
"Photo"=>"",
"Gender"=>"",
"HKID"=>"",
"DateOfBirth"=>"",
"UserEmail"=>"",
"HomeTelNo"=>"",
"MobileTelNo"=>"",
"Address"=>"",
"Education"=>
	array(
		//array("Level"=>"","SchoolName"=>"","Department"=>"","Major"=>"","Minor"=>"","Start"=>"","End"=>"","Period"=>"")
	),
"Training"=>
	array(
		//array("Orginzation"=>"","CourseDescription"=>"","Start"=>"","End"=>"","Period"=>"")
	),
"WorkingExperience"=>
	array(
		//array("Position"=>"","JobTitle"=>"","Company"=>"","Start"=>"","End"=>"")
	),
"SocialWork"=>
	array(
		//array("Community"=>"","Start"=>"","End"=>"","Period"=>"","Job"=>"")
	),
"LanguageSkills"=>
	array(
		array("Language"=>"中文","Speech"=>"","Reading"=>"","Writing"=>"","Listening"=>""),
		array("Language"=>"英語","Speech"=>"","Reading"=>"","Writing"=>"","Listening"=>"")
	),
"Expertise"=>
	array(
		"","",""
	),
"ProfessionalSkills"=>
	array(
		"","",""
	),
"Others"=>""
);
*/

$genders = array(array("",""),array("男","男"), array("女","女"));
$gender_map = array("M"=>"男","F"=>"女");
$education_levels = array(array("小學","小學"), array("中學","中學"), array("大專","大專"), array("大學","大學"));
$ability_levels = array(array("優良","優良"), array("一般","一般"), array("差","差"));

$date_input_remark = '請以YYYY-MM的格式輸入，如要略過請輸入 - 。';

$libuser = new libuser($TargetUserID);
$photo_info = $libuser->GET_USER_PHOTO($libuser->UserLogin, $libuser->UserID, $UseSamplePhoto=false);

//$lpf = new libportfolio2007();
$llh = new liblivinghomeopathy();
$lpf_slp = new libpf_slp();
$linterface = new interface_html();

$profile_records = $llh->getLivingHomeopathyPersonalProfileRecords(array('UserID'=>$TargetUserID));
if(count($profile_records)==0){
	$data = $llh->getLivingHomeopathyPersonalProfileRecordStructure($libuser);
}else{
	$data = unserialize(base64_decode($profile_records[0]['Profile']));
	if($profile_records[0]['DateModified'] < $libuser->DateModified)
	{
		foreach($user_fields as $field){
			if(isset($libuser->$field)){
				if($field == 'Gender'){
					$data[$field] = $gender_map[$libuser->$field];
				}else{
					$data[$field] = $libuser->$field;
				}
			}
		}
	}
}

if($data['Others'] == ''){
	$self_account_arr = $lpf_slp->GET_DEFAULT_SELF_ACCOUNT($TargetUserID);
	if(is_array($self_account_arr))
	{
		foreach($self_account_arr AS $sa_record_id => $DefaultSA)
		{
			if(in_array("SLP", $DefaultSA))
			{
				$self_account_default_arr = $lpf_slp->GET_SELF_ACCOUNT($sa_record_id);
				if($self_account_default_arr['Details'] != ''){
					$data['Others'] = $self_account_default_arr['Details'];
				}
			}
		}
	}
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$is_magic_quotes_on = $llh->isMagicQuotesOn();
	$user_fields[] = "Others";
	$data = $llh->getLivingHomeopathyPersonalProfileRecordStructure();
	foreach($user_fields as $field){
		if(isset($_POST[$field])){
			$post_value = $is_magic_quotes_on? stripslashes($_POST[$field]) : $_POST[$field];
			$data[$field] = $post_value;
		}
	}
	if(isset($_POST['Education'])){
		$fields = array("Level","SchoolName","Department","Major","Minor","Start","End","Period");
		$data['Education'] = array();
		for($i=0;$i<count($_POST['Education'][$fields[0]]);$i++){
			$ary = array();
			foreach($fields as $field){
				$ary[$field] = $_POST['Education'][$field][$i];
			}
			$data['Education'][] = $ary;
		}
	}
	if(isset($_POST['Training'])){
		$fields = array("Organization","CourseDescription","Start","End","Period");
		$data['Training'] = array();
		for($i=0;$i<count($_POST['Training'][$fields[0]]);$i++){
			$ary = array();
			foreach($fields as $field){
				$ary[$field] = $_POST['Training'][$field][$i];
			}
			$data['Training'][] = $ary;
		}
	}
	if(isset($_POST['WorkingExperience'])){
		$fields = array("Position","JobTitle","Company","Start","End","Period");
		$data['WorkingExperience'] = array();
		for($i=0;$i<count($_POST['WorkingExperience'][$fields[0]]);$i++){
			$ary = array();
			foreach($fields as $field){
				$ary[$field] = $_POST['WorkingExperience'][$field][$i];
			}
			$data['WorkingExperience'][] = $ary;
		}
	}
	if(isset($_POST['SocialWork'])){
		$fields = array("Community","Start","End","Period","Job");
		$data['SocialWork'] = array();
		for($i=0;$i<count($_POST['SocialWork'][$fields[0]]);$i++){
			$ary = array();
			foreach($fields as $field){
				$ary[$field] = $_POST['SocialWork'][$field][$i];
			}
			$data['SocialWork'][] = $ary;
		}
	}
	if(isset($_POST['LanguageSkills'])){
		$fields = array("Language","Speech","Reading","Writing","Listening");
		$data['LanguageSkills'] = array();
		for($i=0;$i<count($_POST['LanguageSkills'][$fields[0]]);$i++){
			$ary = array();
			foreach($fields as $field){
				$ary[$field] = $_POST['LanguageSkills'][$field][$i];
			}
			$data['LanguageSkills'][] = $ary;
		}
	}
	if(isset($_POST['Expertise'])){
		$data['Expertise'] = $_POST['Expertise'];
	}
	if(isset($_POST['ProfessionalSkills'])){
		$data['ProfessionalSkills'] = $_POST['ProfessionalSkills'];
	}
	
	$profile = base64_encode(serialize($data));
	$success = $llh->upsertLivingHomeopathyPersonalProfileRecord($_POST['TargetUserID'], $profile);
	
	$_SESSION['LIVING_HOMEOPATHY_PERSONAL_PROFILE_RESULT'] = $success ? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	
	intranet_closedb();
	header("Location:".$_SERVER['SCRIPT_NAME'].'?TargetUserID='.$_POST['TargetUserID']);
	exit;
}

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");


/*
 * Divide the table into 25 cells per row, each cell count as 4% width
 */
?>
<style type="text/css">
.resume-font {
	font-family: 標楷體;
	font-size: 1.5em;
}
table.section {
	width: 100%;
}
table.section, table.section tr, table.section td {
	margin: 0;
	border:1px solid #000000;
	border-collapse: collapse;
	text-align: left;
	vertical-align: middle;
	margin: 0;
	padding: 0;
}
table.section td {
	padding: 5px 5px;
}
table.section td.text-left {
	text-align:left;
}
table.section td.text-center {
	text-align:center;
}
table.section input, table.section textarea {
	width: 99%;
}
table.section td .view {
	text-align:left;
	min-height:1em;
}

.error {
	background-color: #ff6666;
}

@media print {
    .page-break {page-break-inside: avoid;}
}
</style>
<br />
<div style="width:21cm;">
<?php if($IsEditable){
	if(isset($_SESSION['LIVING_HOMEOPATHY_PERSONAL_PROFILE_RESULT'])){
		$msg = $_SESSION['LIVING_HOMEOPATHY_PERSONAL_PROFILE_RESULT'];
		unset($_SESSION['LIVING_HOMEOPATHY_PERSONAL_PROFILE_RESULT']);
		echo $linterface->Get_Thickbox_Return_Message_Layer();
		echo '<script type="text/javascript" language="javascript">'."\n";
		echo '$(document).ready(function(){Get_Return_Message("'.$msg.'");});'."\n";
		echo '</script>'."\n";
	}
?>
<form id="form1" name="form1" method="get" action="<?=$_SERVER['SCRIPT_NAME']?>" onsubmit="return false;">
<?php } ?>
<table width="100%" align="center" class="print_hide" border="0">
<tbody>
	<tr>
		<td align="right"><?=$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_btn")?></td>
	</tr>
</tbody>
</table>
<table class="resume-font" width="100%" align="center">
<tbody>
	<tr>
		<td width="30%"><img src="/templates/LivingHomeopathy/images/module/living/living_logo.png" width="252.75px" height="144px"></td>
		<td width="40%" align="center"><h1>個人履歷表</h1></td>
		<td width="30%">&nbsp;</td>
	</tr>
</tbody>
</table>
<h6 class="resume-font" style="text-align:right">最後更新日期: <?=$profile_records[0]['DateModified']!=''? date("Y年m月d日",strtotime($profile_records[0]['DateModified'])) : "&nbsp;&nbsp;&nbsp;&nbsp;年&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;日"?></h6>
<h5 class="resume-font">基本資料  (所有資料請用正楷填寫)<h5>
<table class="section resume-font">
<tbody>
	<tr class="nodrop">
		<td colspan="5" rowspan="6" width="20%" class="text-center"><?=$photo_info[1]!=''?'<img src="'.$photo_info[1].'" style="max-width:100%;max-height:100%;" />':'照片'?></td>
		<td colspan="4" width="16%" class="text-center">中文姓名</td>
		<td colspan="11" width="44%"><div <?=$view_style?>><?=$data['ChineseName']?></div><input type="text" name="ChineseName" value="<?=$data['ChineseName']?>" <?=$edit_style?> /></td>
		<td colspan="2" width="8%" class="text-center">性別</td>
		<td colspan="3" width="12%"><div <?=$view_style?>><?=$data['Gender']?></div><?=$linterface->GET_SELECTION_BOX($genders, ' name="Gender" '.$edit_style, '', $data['Gender'])?></td>
	</tr>
	<tr class="nodrop">
		<td colspan="4" class="text-center">英文姓名</td>
		<td colspan="17"><div <?=$view_style?>><?=$data['EnglishName']?></div><input type="text" name="EnglishName" value="<?=$data['EnglishName']?>" <?=$edit_style?> /></td>
	</tr>
	<tr class="nodrop">
		<td colspan="4" class="text-center">身份證號碼</td>
		<td colspan="17" ><div <?=$view_style?>><?=$data['HKID']?></div><input type="text" name="HKID" value="<?=$data['HKID']?>" <?=$edit_style?> /></td>
	</tr>
	<tr class="nodrop">
		<td colspan="4" class="text-center">出生日期</td>
		<td colspan="17"><div <?=$view_style?>><?=$data['DateOfBirth']?></div><input type="text" name="DateOfBirth" value="<?=$data['DateOfBirth']?>" <?=$edit_style?> title="YYYY-MM-DD"/></td>
	</tr>
	<tr class="nodrop">
		<td colspan="4" rowspan="2" class="text-center">電郵</td>
		<td colspan="7" rowspan="2" width="28%"><div <?=$view_style?>><?=$data['UserEmail']?></div><input type="text" name="UserEmail" value="<?=$data['UserEmail']?>" <?=$edit_style?>/></td>
		<td colspan="4" width="16%" class="text-center">家庭電話</td>
		<td colspan="5" width="20%"><div <?=$view_style?>><?=$data['HomeTelNo']?></div><input type="text" name="HomeTelNo" value="<?=$data['HomeTelNo']?>" <?=$edit_style?>/></td>
	</tr>
	<tr class="nodrop">
		<td colspan="4" class="text-center">手提電話</td>
		<td colspan="5"><div <?=$view_style?>><?=$data['MobileTelNo']?></div><input type="text" name="MobileTelNo" value="<?=$data['MobileTelNo']?>" <?=$edit_style?>/></td>
	</tr>
	<tr class="nodrop">
		<td colspan="5" width=20%" class="text-center">通訊地址</td>
		<td colspan="20" width="80%"><div <?=$view_style?>><?=nl2br($data['Address'])?></div><textarea name="Address" rows="5" <?=$edit_style?>><?=$data['Address']?></textarea></td>
	</tr>
</tbody>
</table>

<div class="page-break">
<h5 class="resume-font">學歷：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addEducationRow(document.getElementById('EducationTable'));"></a>
	</div>
<h5>
<table id="EducationTable" class="section resume-font DragAndDropTable">
<tbody>	
	<tr class="nodrop">
		<td rowspan="2" colspan="1" width="4%" class="edit" <?=$edit_style?>>&nbsp;</td>
		<td rowspan="2" colspan="1" width="4%">&nbsp;</td>
		<td rowspan="2" colspan="8" width="32%" class="text-center">學校名稱</td>
		<td rowspan="2" colspan="6" width="24%" class="text-center">學科、系</td>
		<td rowspan="1" colspan="9" width="36%" class="text-center">學業時期</td>
	</tr>
	<tr class="nodrop">
		<td colspan="3" width="12%" class="text-center">開始</td>
		<td colspan="3" width="12%" class="text-center">完成</td>
		<td colspan="3" width="12%" class="text-center">時間</td>
	</tr>
<?php for($i=0;$i<count($data['Education']);$i++){ ?>
		<tr class="education">
			<td colspan="1" width="4%" class="Dragable edit" <?=$edit_style?>>
				<div class="table_row_tool row_content_tool">
					<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>
					<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeEducationRow(this);" ></a>
				</div>
			</td>
			<td colspan="1" width="4%" class="text-center" nowrap>
				<div class="view" <?=$view_style?>><?=$data['Education'][$i]['Level']?></div>
				<?=$linterface->GET_SELECTION_BOX($education_levels, ' name="Education[Level][]" class="edit" onchange="onEducationLevelChanged(this);" '.$edit_style, '', $data['Education'][$i]['Level'])?>
			</td>
			<td colspan="8" width="28%" class="text-center"><div class="view" <?=$view_style?>><?=$data['Education'][$i]['SchoolName']?></div><input type="text" name="Education[SchoolName][]" value="<?=$data['Education'][$i]['SchoolName']?>" class="edit" <?=$edit_style?> /></td>
			<td colspan="6" width="24%">
				<div class="course" <?=$data['Education'][$i]['Level']=='大學'?' style="display:none;"':''?>><div class="view" <?=$view_style?>><?=$data['Education'][$i]['Department']?></div><input type="text" name="Education[Department][]" value="<?=$data['Education'][$i]['Department']?>" class="edit" <?=$edit_style?> /></div>
				<div class="major_minor" <?=$data['Education'][$i]['Level']!='大學'?' style="display:none;"':''?>>
					<div class="major">主修: <span class="view"><?=$data['Education'][$i]['Major']?></span><input type="text" name="Education[Major][]" value="<?=$data['Education'][$i]['Major']?>" style="width:70%;<?=$IsEdit?'':'display:none;'?>" class="edit" /></div>
					<div class="minor">副修: <span class="view"><?=$data['Education'][$i]['Minor']?></span><input type="text" name="Education[Minor][]" value="<?=$data['Education'][$i]['Minor']?>" style="width:70%;<?=$IsEdit?'':'display:none;'?>" class="edit" /></div>
				</div>
			</td>
			<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Education'][$i]['Start']?></div><input type="text" name="Education[Start][]" title="<?=$date_input_remark?>" value="<?=$data['Education'][$i]['Start']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
			<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Education'][$i]['End']?></div><input type="text" name="Education[End][]" title="<?=$date_input_remark?>" value="<?=$data['Education'][$i]['End']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
			<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Education'][$i]['Period']?></div><input type="text" name="Education[Period][]" value="<?=$data['Education'][$i]['Period']?>" class="edit" <?=$edit_style?> /></td>
		</tr>
<?php } ?>
	<tr class="nodrop">
		<td colspan="25" class="text-left">註 : 請由小學開始填入。</td>
	</tr>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">訓練及進修：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addTrainingRow(document.getElementById('TrainingTable'));"></a>
	</div>
<h5>
<table id="TrainingTable" class="section resume-font DragAndDropTable">
<tbody>		
	<tr class="nodrop">
		<td rowspan="2" colspan="1" width="4%" class="edit" <?=$edit_style?>></td>
		<td rowspan="2" colspan="8" width="36%" class="text-center">機構</td>
		<td rowspan="2" colspan="8" width="24%" class="text-center">課程內容概述</td>
		<td rowspan="1" colspan="9" width="36%" class="text-center">訓練及進修時期</td>
	</tr>
	<tr class="nodrop">
		<td colspan="3" width="12%" class="text-center">開始</td>
		<td colspan="3" width="12%" class="text-center">完結</td>
		<td colspan="3" width="12%" class="text-center">時間</td>
	</tr>
<?php for($i=0;$i<count($data['Training']);$i++){ ?>
	<tr class="training">
		<td colspan="1" width="4%" class="Dragable edit" <?=$edit_style?>>
			<div class="table_row_tool row_content_tool">
				<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeTrainingRow(this);" ></a>
			</div>
		</td>
		<td colspan="8" width="36%">
			<div class="view" <?=$view_style?>><?=$data['Training'][$i]['Organization']?></div><input type="text" name="Training[Organization][]" value="<?=$data['Training'][$i]['Organization']?>" class="edit" <?=$edit_style?> />
		</td>
		<td colspan="8" width="24%"><div class="view" <?=$view_style?>><?=nl2br($data['Training'][$i]['CourseDescription'])?></div><textarea name="Training[CourseDescription][]" rows="5" class="edit" <?=$edit_style?>><?=$data['Training'][$i]['CourseDescription']?></textarea></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Training'][$i]['Start']?></div><input type="text" name="Training[Start][]" title="<?=$date_input_remark?>" value="<?=$data['Training'][$i]['Start']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Training'][$i]['End']?></div><input type="text" name="Training[End][]" title="<?=$date_input_remark?>" value="<?=$data['Training'][$i]['End']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['Training'][$i]['Period']?></div><input type="text" name="Training[Period][]" value="<?=$data['Training'][$i]['Period']?>" class="edit" <?=$edit_style?> /></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">工作經歷：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addWorkingExperienceRow(document.getElementById('WorkingExperienceTable'));"></a>
	</div>
<h5>
<table id="WorkingExperienceTable" class="section resume-font DragAndDropTable">
<tbody>		
	<tr class="nodrop">
		<td rowspan="2" colspan="1" width="4%" class="edit" <?=$edit_style?>></td>
		<td rowspan="2" colspan="4" width="20%" class="text-center">曾任職務</td>
		<td rowspan="2" colspan="6" width="24%" class="text-center">職稱</td>
		<td rowspan="2" colspan="5" width="20%" class="text-center">公司名稱</td>
		<td rowspan="1" colspan="9" width="36%" class="text-center">工作時期</td>
	</tr>
	<tr class="nodrop">
		<td colspan="3" width="12%" class="text-center">開始</td>
		<td colspan="3" width="12%" class="text-center">完結</td>
		<td colspan="3" width="12%" class="text-center">時間</td>
	</tr>
<?php for($i=0;$i<count($data['WorkingExperience']);$i++){ ?>	
	<tr class="workingexperience">
		<td colspan="1" width="4%" class="Dragable edit" <?=$edit_style?>>
			<div class="table_row_tool row_content_tool">
				<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeWorkingExperienceRow(this);" ></a>
			</div>
		</td>
		<td colspan="4" width="20%">
			<div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['Position']?></div><input type="text" name="WorkingExperience[Position][]" value="<?=$data['WorkingExperience'][$i]['Position']?>" class="edit" <?=$edit_style?> />
		</td>
		<td colspan="6" width="24%"><div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['JobTitle']?></div><input type="text" name="WorkingExperience[JobTitle][]" value="<?=$data['WorkingExperience'][$i]['Position']?>" class="edit" <?=$edit_style?> /></td>
		<td colspan="5" width="20%"><div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['Company']?></div><input type="text" name="WorkingExperience[Company][]" value="<?=$data['WorkingExperience'][$i]['Company']?>" class="edit" <?=$edit_style?> /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['Start']?></div><input type="text" name="WorkingExperience[Start][]" title="<?=$date_input_remark?>" value="<?=$data['WorkingExperience'][$i]['Start']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['End']?></div><input type="text" name="WorkingExperience[End][]" title="<?=$date_input_remark?>" value="<?=$data['WorkingExperience'][$i]['End']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['WorkingExperience'][$i]['Period']?></div><input type="text" name="WorkingExperience[Period][]" value="<?=$data['WorkingExperience'][$i]['Period']?>" class="edit" <?=$edit_style?> /></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">社會工作：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addSocialWorkRow(document.getElementById('SocialWorkTable'));"></a>
	</div>
<h5>
<table id="SocialWorkTable" class="section resume-font DragAndDropTable">
<tbody>
	<tr class="nodrop">
		<td rowspan="2" colspan="1" width="4%" class="edit" <?=$edit_style?>></td>
		<td rowspan="2" colspan="6" width="28%" class="text-center">社團 / 學術團體名稱</td>
		<td rowspan="1" colspan="9" width="36%" class="text-center">參與時期</td>
		<td rowspan="2" colspan="9" width="36%" class="text-center">擔任職務 (若有)</td>
	</tr>
	<tr class="nodrop">
		<td colspan="3" width="12%" class="text-center">開始</td>
		<td colspan="3" width="12%" class="text-center">完結</td>
		<td colspan="3" width="12%" class="text-center">時間</td>
	</tr>
<?php for($i=0;$i<count($data['SocialWork']);$i++){ ?>	
	<tr class="socialwork">
		<td colspan="1" width="4%" class="Dragable edit" <?=$edit_style?>>
			<div class="table_row_tool row_content_tool">
				<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeTrainingRow(this);" ></a>
			</div>
		</td>
		<td colspan="6" width="28%">
			<div class="view" <?=$view_style?>><?=$data['SocialWork'][$i]['Community']?></div><input type="text" name="SocialWork[Community][]" value="<?=$data['SocialWork'][$i]['Community']?>" class="edit" <?=$edit_style?> />
		</td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['SocialWork'][$i]['Start']?></div><input type="text" name="SocialWork[Start][]" title="<?=$date_input_remark?>" value="<?=$data['SocialWork'][$i]['Start']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['SocialWork'][$i]['End']?></div><input type="text" name="SocialWork[End][]" title="<?=$date_input_remark?>" value="<?=$data['SocialWork'][$i]['End']?>" class="edit" <?=$edit_style?> onkeyup="calculatePeriod(this);" /></td>
		<td colspan="3" width="12%" nowrap><div class="view" <?=$view_style?>><?=$data['SocialWork'][$i]['Period']?></div><input type="text" name="SocialWork[Period][]" value="<?=$data['SocialWork'][$i]['Period']?>" class="edit" <?=$edit_style?> /></td>
		<td colspan="9" width="36%"><div class="view" <?=$view_style?>><?=$data['SocialWork'][$i]['Job']?></div><input type="text" name="SocialWork[Job][]" value="<?=$data['SocialWork'][$i]['Job']?>" class="edit" <?=$edit_style?> /></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">語文能力：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addLanguageSkillsRow(this);"></a>
	</div>
<h5>
<table class="section resume-font">
<tbody>
	<tr>
		<td colspan="5" width="20%" class="text-center">語文</td>
		<td colspan="5" width="20%" class="text-center">說話</td>
		<td colspan="5" width="20%" class="text-center">閱讀</td>
		<td colspan="5" width="20%" class="text-center">書寫</td>
		<td colspan="5" width="20%" class="text-center">聆聽</td>
	</tr>
<?php for($i=0;$i<count($data['LanguageSkills']);$i++){ ?>
	<tr class="languageskills">
		<?php if($i>1){ ?>
		<td colspan="5" width="20%" class="text-center">
			<div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Language']?></div><input type="text" name="LanguageSkills[Language][]" value="<?=$data['LanguageSkills'][$i]['Language']?>" class="edit" <?=$edit_style?> />
			<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeLanguageSkillsRow(this);" ></a>
			</div>
		</td>
		<?php }else{ ?>
		<td colspan="5" width="20%" class="text-center"><div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Language']?></div><input type="text" name="LanguageSkills[Language][]" value="<?=$data['LanguageSkills'][$i]['Language']?>" class="edit" <?=$edit_style?> /></td>
		<?php } ?>
		<td colspan="5" width="20%" class="text-center"><div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Speech']?></div><?=$linterface->GET_SELECTION_BOX($ability_levels, ' name="LanguageSkills[Speech][]" class="edit" '.$edit_style, '', $data['LanguageSkills'][$i]['Speech'])?></td>
		<td colspan="5" width="20%" class="text-center"><div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Reading']?></div><?=$linterface->GET_SELECTION_BOX($ability_levels, ' name="LanguageSkills[Reading][]" class="edit" '.$edit_style, '', $data['LanguageSkills'][$i]['Reading'])?></td>
		<td colspan="5" width="20%" class="text-center"><div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Writing']?></div><?=$linterface->GET_SELECTION_BOX($ability_levels, ' name="LanguageSkills[Writing][]" class="edit" '.$edit_style, '', $data['LanguageSkills'][$i]['Writing'])?></td>
		<td colspan="5" width="20%" class="text-center"><div class="view" <?=$view_style?>><?=$data['LanguageSkills'][$i]['Listening']?></div><?=$linterface->GET_SELECTION_BOX($ability_levels, ' name="LanguageSkills[Listening][]" class="edit" '.$edit_style, '', $data['LanguageSkills'][$i]['Listening'])?></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">專長：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addExpertiseRow(this);"></a>
	</div>
<h5>
<table class="section resume-font">
<tbody>
<?php for($i=0;$i<count($data['Expertise']);$i+=3){ ?>
	<tr class="expertise">
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['Expertise'][$i]?></div><input type="text" name="Expertise[]" value="<?=$data['Expertise'][$i]?>" class="edit" <?=$edit_style?> />
		<?php if($i!=0){ ?>	
			<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeExpertiseRow(this);" ></a>
			</div>
		<?php } ?>
		</td>
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['Expertise'][$i+1]?></div><input type="text" name="Expertise[]" value="<?=$data['Expertise'][$i+1]?>" class="edit" <?=$edit_style?> /></td>
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['Expertise'][$i+2]?></div><input type="text" name="Expertise[]" value="<?=$data['Expertise'][$i+2]?>" class="edit" <?=$edit_style?> /></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">專業技術：
	<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
		<a href="javascript:void(0);" title="<?=$Lang['Btn']['Add']?>" class="add_dim thickbox" onclick="addProfessionalSkillsRow(this);"></a>
	</div>
<h5>
<table class="section resume-font">
<tbody>
<?php for($i=0;$i<count($data['ProfessionalSkills']);$i+=3){ ?>
	<tr class="professionalskills">
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['ProfessionalSkills'][$i]?></div><input type="text" name="ProfessionalSkills[]" value="<?=$data['ProfessionalSkills'][$i]?>" class="edit" <?=$edit_style?> />
		<?php if($i!=0){ ?>
			<div class="table_row_tool row_content_tool edit" <?=$edit_style?>>
				<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeProfessionalSkillsRow(this);" ></a>
			</div>
		<?php } ?>
		</td>
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['ProfessionalSkills'][$i+1]?></div><input type="text" name="ProfessionalSkills[]" value="<?=$data['ProfessionalSkills'][$i+1]?>" class="edit" <?=$edit_style?> /></td>
		<td width="33.33%"><div class="view" <?=$view_style?>><?=$data['ProfessionalSkills'][$i+2]?></div><input type="text" name="ProfessionalSkills[]" value="<?=$data['ProfessionalSkills'][$i+2]?>" class="edit" <?=$edit_style?> /></td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

<div class="page-break">
<h5 class="resume-font">其他：<h5>
<table class="section resume-font">
<tbody>
	<tr>
		<td><div class="view" <?=$view_style?>><?=nl2br($data['Others'])?></div><textarea name="Others" rows="5" class="edit" <?=$edit_style?>><?=$data['Others']?></textarea></td>
	</tr>
</tbody>
</table>
</div>

<h5 class="resume-font"><small>註：申請人提交資料後，則同意本機構對  閣下個人資料的收集及保存，並在本機構營運及統計時使用。</small><h5>
<?php if($IsEditable){ ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center" class="print_hide">
<tbody>
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</tbody>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center" class="print_hide">
<tbody>
	<tr>
	    <td align="center" colspan="2">
	    	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "$('.edit').show();$('.view').hide();$('#print_btn').hide();","edit_btn",$IsEdit?' style="display:none" ':'',0,'','view')?>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "onFormSubmitted(document.form1);","submit_btn", $IsEdit?'':' style="display:none" ',0,'','edit') ?>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "$('.edit').hide();$('.view').show();$('#print_btn').show();","cancel_btn", $IsEdit?'':' style="display:none" ',0,'','edit') ?>
		</td>
	</tr>
</tbody>
</table>
<input type="hidden" name="TargetUserID" value="<?=$TargetUserID?>" />
</form>
<?php } ?>
</div>
<?php if($IsEditable){ ?>
<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" language="javascript">
function initDnDTable()
{
	$('.DragAndDropTable').tableDnD({
	    onDrop: function(table, row) {
	    	
		},
		onAllowDrop : function(dragRow, dropRow){
			if(!$('#submit_btn').is(':visible')) return false;
			var dragRowClass = $(dragRow['context']).attr('class');
			var dropRowClass = $(dropRow).attr('class');
			
			return dragRowClass == dropRowClass;
		},
		dragHandle: "Dragable",
		onDragClass: ""
	});
}

function onEducationLevelChanged(obj)
{
	var this_obj = $(obj);
	var val = this_obj.val();
	var parent_tr = this_obj.closest('tr');
	var parent_tr_obj = $(parent_tr);
	if(val == '大學'){
		parent_tr_obj.find('.course').hide();
		parent_tr_obj.find('.major_minor').show();
	}else{
		parent_tr_obj.find('.course').show();
		parent_tr_obj.find('.major_minor').hide();
	}
}

function addEducationRow(obj)
{
	var selection_objs = document.getElementsByName('Education[Level][]');
	var option_values = [];
	for(var i=0;i<selection_objs.length;i++){
		option_values.push($(selection_objs[i]).val());
	}
	var selected_level = '大學';
	if(option_values.indexOf('小學')==-1){
		selected_level = '小學';
	}else if(option_values.indexOf('中學')==-1){
		selected_level = '中學';
	}else if(option_values.indexOf('大專')==-1){
		selected_level = '大專';
	}else if(option_values.indexOf('大學')==-1){
		selected_level = '大學';
	}
	var html = '<tr class="education">';
		html+=		'<td colspan="1" width="4%" class="Dragable edit">';
		html+=			'<div class="table_row_tool row_content_tool">';
		html+=				'<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>';
		html+=				'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeEducationRow(this);" ></a>';
		html+=			'</div>';
		html+=		'</td>';
		html+=		'<td colspan="1" width="4%" class="text-center" nowrap><div class="view" style="display:none;">小學</div>';
		html+=			'<select name="Education[Level][]" class="edit" onchange="onEducationLevelChanged(this);" >';
		html+=				'<option value="小學" '+(selected_level == '小學'?'selected':'')+'>小學</option>';
		html+=				'<option value="中學" '+(selected_level == '中學'?'selected':'')+'>中學</option>';
		html+=				'<option value="大專" '+(selected_level == '大專'?'selected':'')+'>大專</option>';
		html+=				'<option value="大學" '+(selected_level == '大學'?'selected':'')+'>大學</option>';
		html+=			'</select>';
		html+=		'</td>';
		html+=			'<td colspan="8" width="32%" class="text-center"><div class="view" style="display:none;"></div><input type="text" name="Education[SchoolName][]" value="" class="edit" /></td>';
		html+=			'<td colspan="6" width="24%">';
		html+=				'<div class="course"><div class="view" style="display:none;"></div><input type="text" name="Education[Department][]" value="" class="edit" /></div>';
		html+=				'<div class="major_minor" style="display:none;">';
		html+=					'<div class="major">主修: <span class="view"></span><input type="text" name="Education[Major][]" value="" style="width:70%;" class="edit" /></div>';
		html+=					'<div class="minor">副修: <span class="view"></span><input type="text" name="Education[Minor][]" value="" style="width:70%;" class="edit" /></div>';
		html+=				'</div>';
		html+=			'</td>';
		html+=			'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Education[Start][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=			'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Education[End][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=			'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Education[Period][]" value="" class="edit" /></td>';
		html+=		'</tr>';
	//var parent_tr = $(obj).closest('tr');
	var parent_tr = $(obj).find('tr:first');
	var education_rows = $('tr.education');
	if(education_rows.length > 0){
		$(education_rows[education_rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
	
	//$('#TdEducation').attr('rowspan',3+$('tr.education').length);
	
	initDnDTable();
}

function removeEducationRow(obj)
{
	$(obj).closest('tr').remove();
	//$('#TdEducation').attr('rowspan',3+$('tr.education').length);
}

function addTrainingRow(obj)
{
	var html = '<tr class="training">';
		html+= '<td colspan="1" width="4%" class="Dragable edit">';
		html+=		'<div class="table_row_tool row_content_tool">';
		html+=			'<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>';
		html+=			'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeTrainingRow(this);" ></a>';
		html+=		'</div>';
		html+=	'</td>';
		html+= 	'<td colspan="8" width="36%"><div class="view" style="display:none;"></div><input type="text" name="Training[Organization][]" value="" class="edit" />';
		html+=	'</td>';
		html+=	'<td colspan="8" width="24%"><div class="view" style="display:none;"></div><textarea name="Training[CourseDescription][]" rows="5" class="edit" ></textarea></td>';
		html+=	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Training[Start][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+= 	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Training[End][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="Training[Period][]" value="" class="edit" /></td>';
		html+= '</tr>';
	
	//var parent_tr = $(obj).closest('tr');
	var parent_tr = $(obj).find('tr:first');
	var rows = $('tr.training');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
	
	//$('#TdTraining').attr('rowspan',2+$('tr.training').length);
	
	initDnDTable();
}

function removeTrainingRow(obj)
{
	$(obj).closest('tr').remove();
	//$('#TdTraining').attr('rowspan',2+$('tr.training').length);
}

function addWorkingExperienceRow(obj)
{
	var html = '<tr class="workingexperience">';
		html+=		'<td colspan="1" width="4%" class="Dragable edit">';
		html+=			'<div class="table_row_tool row_content_tool">';
		html+=				'<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>';
		html+=				'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeWorkingExperienceRow(this);" ></a>';
		html+=			'</div>';
		html+= 		'</td>';
		html+=		'<td colspan="4" width="20%"><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[Position][]" value="" class="edit" />';
		html+=		'</td>';
		html+= 		'<td colspan="6" width="24%"><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[JobTitle][]" value="" class="edit" /></td>';
		html+=		'<td colspan="5" width="20%"><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[Company][]" value="" class="edit" /></td>';
		html+=		'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[Start][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=		'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[End][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=		'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="WorkingExperience[Period][]" value="" class="edit" /></td>';
		html+= '</tr>';
		
	//var parent_tr = $(obj).closest('tr');
	var parent_tr = $(obj).find('tr:first');
	var rows = $('tr.workingexperience');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
	
	//$('#TdWorkingExperience').attr('rowspan',2+$('tr.workingexperience').length);
	
	initDnDTable();
}

function removeWorkingExperienceRow(obj)
{
	$(obj).closest('tr').remove();
	//$('#TdWorkingExperience').attr('rowspan',2+$('tr.workingexperience').length);
}

function addSocialWorkRow(obj)
{
	var html = '<tr class="socialwork">';
		html+=	'<td colspan="1" width="4%" class="Dragable edit">';
		html+=		'<div class="table_row_tool row_content_tool">';
		html+=			'<a href="javascript:void(0);" class="move_order_dim Dragable" title="<?=$Lang['Btn']['Move']?>"></a>';
		html+=			'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeTrainingRow(this);" ></a>';
		html+=		'</div>';
		html+=	'</td>';
		html+= 	'<td colspan="6" width="28%"><div class="view" style="display:none;"></div><input type="text" name="SocialWork[Community][]" value="" class="edit" />';
		html+=	'</td>';
		html+=	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="SocialWork[Start][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="SocialWork[End][]" title="<?=$date_input_remark?>" value="" class="edit" onkeyup="calculatePeriod(this);" /></td>';
		html+=	'<td colspan="3" width="12%" nowrap><div class="view" style="display:none;"></div><input type="text" name="SocialWork[Period][]" value="" class="edit" /></td>';
		html+=	'<td colspan="9" width="36%"><div class="view" style="display:none;"></div><input type="text" name="SocialWork[Job][]" value="" class="edit" /></td>';
		html+= '</tr>';
		
	//var parent_tr = $(obj).closest('tr');
	var parent_tr = $(obj).find('tr:first');
	var rows = $('tr.socialwork');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
	
	//$('#TdSocialWork').attr('rowspan',2+$('tr.socialwork').length);
	
	initDnDTable();
}

function removeSocialWorkRow(obj)
{
	$(obj).closest('tr').remove();
	//$('#TdSocialWork').attr('rowspan',2+$('tr.socialwork').length);
}

function addLanguageSkillsRow(obj)
{
	var html = '<tr class="languageskills">';
		html+=	 '<td colspan="5" width="20%" class="text-center"><div class="view" style="display:none;"></div><input type="text" name="LanguageSkills[Language][]" value="" class="edit" />';
		html+=		'<div class="table_row_tool row_content_tool edit">';
		html+=			'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeLanguageSkillsRow(this);" ></a>';
		html+=		'</div>';
		html+=	'</td>';
		html+=	'<td colspan="5" width="20%" class="text-center"><div class="view" style="display:none;"></div>';
		html+=			'<select name="LanguageSkills[Speech][]" class="edit">';
		html+=			'<option value="優良">優良</option>';
		html+=			'<option value="一般">一般</option>';
		html+=			'<option value="差">差</option>';
		html+=			'</select>';
		html+=	'</td>';
		html+=	'<td colspan="5" width="20%" class="text-center"><div class="view" style="display:none;"></div>';
		html+=			'<select name="LanguageSkills[Reading][]" class="edit">';
		html+=			'<option value="優良">優良</option>';
		html+=			'<option value="一般">一般</option>';
		html+=			'<option value="差">差</option>';
		html+=			'</select>';
		html+=	'</td>';
		html+=	'<td colspan="5" width="20%" class="text-center"><div class="view" style="display:none;"></div>';
		html+=			'<select name="LanguageSkills[Writing][]" class="edit">';
		html+=			'<option value="優良">優良</option>';
		html+=			'<option value="一般">一般</option>';
		html+=			'<option value="差">差</option>';
		html+=			'</select>';
		html+=	'</td>';
		html+=	'<td colspan="5" width="20%" class="text-center"><div class="view" style="display:none;"></div>';
		html+=			'<select name="LanguageSkills[Listening][]" class="edit">';
		html+=			'<option value="優良">優良</option>';
		html+=			'<option value="一般">一般</option>';
		html+=			'<option value="差">差</option>';
		html+=			'</select>';
		html+=	'</td>';
		html+=	'</tr>';
	
	var parent_tr = $(obj).closest('tr');
	var rows = $('tr.languageskills');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
}

function removeLanguageSkillsRow(obj)
{
	$(obj).closest('tr').remove();
}

function addExpertiseRow(obj)
{
	var html = '<tr class="expertise">';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="Expertise[]" value="" class="edit" />';
		html+=			'<div class="table_row_tool row_content_tool edit">';
		html+=				'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeExpertiseRow(this);" ></a>';
		html+=			'</div>';
		html+=		'</td>';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="Expertise[]" value="" class="edit" /></td>';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="Expertise[]" value="" class="edit" /></td>';
		html+=	'</tr>';
	
	var parent_tr = $(obj).closest('tr');
	var rows = $('tr.expertise');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
}

function removeExpertiseRow(obj)
{
	$(obj).closest('tr').remove();
}

function addProfessionalSkillsRow(obj)
{
	var html = '<tr class="professionalskills">';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="ProfessionalSkills[]" value="" class="edit" />';
		html+=			'<div class="table_row_tool row_content_tool edit">'
		html+=				'<a href="javascript:void(0);" title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" onclick="removeProfessionalSkillsRow(this);" ></a>';
		html+=			'</div>';
		html+=		'</td>';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="ProfessionalSkills[]" value="" class="edit" /></td>';
		html+=		'<td width="33.33%"><div class="view" style="display:none;"></div><input type="text" name="ProfessionalSkills[]" value="" class="edit" /></td>';
		html+=	'</tr>';
		
	var parent_tr = $(obj).closest('tr');
	var rows = $('tr.professionalskills');
	if(rows.length > 0){
		$(rows[rows.length-1]).after(html);
	}else{
		$(parent_tr).next().after(html);
	}
}

function removeProfessionalSkillsRow(obj)
{
	$(obj).closest('tr').remove();
}

function calculatePeriod(obj)
{
	var parent_obj = $(obj).closest('tr');
	var start_obj_name = '';
	var end_obj_name = '';
	var period_obj_name = '';
	var start_or_end = '';
	if(obj.name.indexOf('Start')!=-1){
		start_or_end = 'Start';
		start_obj_name = obj.name;
		end_obj_name = obj.name.replace('Start','End');
		period_obj_name = obj.name.replace('Start','Period');
	}else if(obj.name.indexOf('End')!=-1){
		start_or_end = 'End';
		start_obj_name = obj.name.replace('End','Start');
		end_obj_name = obj.name;
		period_obj_name = obj.name.replace('End','Period');
	}
	var start_obj = parent_obj.find('[name="'+start_obj_name+'"]');
	var end_obj = parent_obj.find('[name="'+end_obj_name+'"]');
	var period_obj = parent_obj.find('[name="'+period_obj_name+'"]');
	var start_obj_value = $.trim(start_obj.val());
	var end_obj_value = $.trim(end_obj.val());
	if(start_obj_value == '-' || end_obj_value == '-'){
		$(start_obj).removeClass('error');
		$(end_obj).removeClass('error');
	}else if(start_obj_value != '' && end_obj_value != ''){
		var start_date_obj = new Date(start_obj_value);
		var end_date_obj = new Date(end_obj_value);
		var start_year = start_date_obj.getFullYear();
		var start_month = start_date_obj.getMonth() + 1;
		var end_year = end_date_obj.getFullYear();
		var end_month = end_date_obj.getMonth() + 1;
		var period_year = end_year - start_year;
		var period_month = 0;
		if(end_month < start_month){
			period_year -= 1;
			period_month = end_month - start_month + 12;
		}else{
			period_month = end_month - start_month;
		}
		period_obj_value = '';
		if(period_year > 0){
			period_obj_value += period_year + '年';
		}
		if(period_month > 0){
			period_obj_value += period_month +'月';
		}
		period_obj.val(period_obj_value);
		$(start_obj).removeClass('error');
		$(end_obj).removeClass('error');
		if(period_year < 0 || !start_obj_value.match(/^\d\d\d\d-\d\d$/gi) || isNaN(start_month) || start_month < 1 || start_month > 12
			|| !end_obj_value.match(/^\d\d\d\d-\d\d$/gi) || isNaN(end_month) || end_month < 1 || end_month > 12){
			$(start_obj).addClass('error');
			$(end_obj).addClass('error');
		}
	}else{
		period_obj.val('');
		if(!(start_obj_value == '' && end_obj_value == '')){
			$(start_obj).addClass('error');
			$(end_obj).addClass('error');
		}else{
			$(start_obj).removeClass('error');
			$(end_obj).removeClass('error');
		}
	}
}

function onFormSubmitted(formObj)
{
	if($('.error').length > 0){
		alert('請先修正錯誤再提交。');
		return;
	}
	var editors = $('.edit');
	for(var i=0;i<editors.length;i++){
		var editor_obj = $(editors[i]);
		var viewer_obj = editor_obj.prev('.view');
		var val = editor_obj.val();
		if(editor_obj.attr('type') == 'textarea'){
			val = val.replace(/\n/g,'<br />');
		}
		viewer_obj.html($.trim(val));
	}
	
	formObj.method = "POST";
	formObj.submit();
}

$(document).ready(function(){
	initDnDTable();
});
</script>
<?php } ?>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>