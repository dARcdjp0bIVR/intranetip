<?php
##	Modifying By: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");

intranet_auth();
intranet_opendb();
$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();
$lpf_acc = new libpf_account_student();
$lpf_acc->SET_CLASS_VARIABLE("user_id", $StudentID);
$lpf_acc->SET_STUDENT_PROPERTY();
$student_name = $lpf_acc->GET_CLASS_VARIABLE(Get_Lang_Selection("chinese_name", "english_name"));

########################################################
# Tab Menu : Start
########################################################

//$TabIndex = "SUNSHINE";
//$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex);

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
//$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

# navigation
$YearClassId = $YearClassId;
$year_class = new year_class($YearClassId);
$classTitle = "ClassTitle" . strtoupper($intranet_session_language);

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "./");
if (empty($year_class->${classTitle})) {
	// do nothing
} else {
	$MenuArr[] = array($year_class->${classTitle}, "./?yearClassID=$YearClassId");
}
$MenuArr[] = array($student_name, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

# academic year selection
$academicYearID = isset($academicYearID) ? $academicYearID : Get_Current_Academic_Year_ID();  
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='submitYearSelect(this)'", $academicYearID, 1, 1, "", 2);

########################################################
# Operations : End
########################################################

########################################################
# Operation result : Start
########################################################
$op_result = ($msg == "") ? "" : "<tr><td>".$linterface->GET_SYS_MSG($msg)."</td></tr>";
########################################################
# Operation result : End
########################################################

########################################################
# Table content : Start
########################################################
$libpf_slp_cnecc = new libpf_slp_cnecc($StudentID, $academicYearID);
$table_content = $libpf_slp_cnecc->getCheckListTable();
########################################################
# Table content : End
########################################################

########################################################
# Buttons : Start
########################################################
$submitButton = $linterface->GET_ACTION_BTN($button_submit, "button", "jSUBMIT_FORM();");
$genHtmlReportButton = $linterface->GET_ACTION_BTN($Lang["Cust_Cnecc"]["GenerateReport"], "button", "goPrintHtml();");
$backButton = $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();");
//$buttons = $submitButton.$genHtmlReportButton.$backButton;
$buttons = "<br/>".$submitButton."&nbsp;".$backButton;
########################################################
# Buttons : End
########################################################



########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Teacher_OLE";
//$CurrentPageName = $iPort['menu']['ole'];

### Title ###
//$TAGS_OBJ[] = array($CurrentPageName,"");
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_SUNSHINE);

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
include_once("template/student_view.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
