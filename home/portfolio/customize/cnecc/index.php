<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();

########################################################
# Tab Menu : Start
########################################################

//$TabIndex = "SUNSHINE";
//$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex);

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
//$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

# year class selection
$lfcm = new form_class_manage();
$t_year_class_arr = $lfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_class_arr = array();
for($i=0; $i<count($t_year_class_arr); $i++)
{
  $t_year_class_id = $t_year_class_arr[$i]['YearClassID'];
  $t_year_class_title = Get_Lang_Selection($t_year_class_arr[$i]['ClassTitleB5'],$t_year_class_arr[$i]['ClassTitleEN']);
  
  $year_class_arr[] = array($t_year_class_id, $t_year_class_title);
}
$yc_selection_html = getSelectByArray($year_class_arr, "name='yearClassID' onChange='jSUBMIT_FORM()'", $yearClassID, 1, 0, $i_general_all_classes, 2);

########################################################
# Operations : End
########################################################

########################################################
# Table content : Start
########################################################

$pageSizeChangeEnabled = true;
$checkmaster = true;

$cond = empty($yearClassID) ? "" : " AND yc.YearClassID = ".$yearClassID;

# Main query
if ($order=="") $order=1;
if ($field=="") $field=1;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT
            CONCAT('<a href=\"student_view.php?StudentID=', iu.UserID, '&YearClassId=', yc.YearClassID, '\" class=\"tablelink\">', ".getNameFieldByLang2("iu.").", '</a>') AS DisplayName,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber),
            IF(MAX(ccss.ModifiedDate) IS NULL, '--', MAX(ccss.ModifiedDate)) AS latestModify,
			yc.YearClassID
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
          LEFT JOIN
            {$eclass_db}.CUSTOM_CNECC_SS_STUDENT AS ccss
          ON
            ccss.StudentID = iu.UserID
          WHERE
            yc.AcademicYearID = ".Get_Current_Academic_Year_ID()."
            $cond
          GROUP BY
            iu.UserID
        ";

// TABLE INFO
$LibTable->field_array = array("DisplayName", "iu.ClassName, iu.ClassNumber", "latestModify");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 4;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='100' >".$LibTable->column(0,$ec_iPortfolio['student'], 1)."</td>\n";
$LibTable->column_list .= "<td class=\"tabletopnolink\">".$LibTable->column(1,$i_general_class."-".$i_ClassNumber, 1)."</td>";
$LibTable->column_list .= "<td class=\"tabletopnolink\">".$LibTable->column(2,$ec_iPortfolio['last_update'], 1)."</td>\n";
$LibTable->column_list .= "</tr>\n";
$LibTable->column_array = array(0,0,3);

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Teacher_OLE";
//$CurrentPageName = $iPort['menu']['ole'];

### Title ###
//$TAGS_OBJ[] = array($CurrentPageName,"");
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_SUNSHINE);

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
include_once("template/student_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>