<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/lang/lang.{$intranet_session_language}.php");
intranet_auth();
intranet_opendb();

//Receive Variable 
$task = trim($task);
//$academicYearId = intval($academicYearId);


$task = (trim($task) =="")? "selectYear": $task;

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();
$objDB = new libdb();
$libpf_slp_cnecc = new libpf_slp_cnecc();

$copiedAcademicYearIDAry = $libpf_slp_cnecc->getCopyLogRecord();



$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);
########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Settings_OLE";
//$CurrentPageName = $iPort['menu']['ole'];

### Title ###
//$TAGS_OBJ[] = array($CurrentPageName,"");
//$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_SUNSHINE);
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_CNECC_SS);

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");




$AcademicYearID = Get_Current_Academic_Year_ID();
$YearTermID = getCurrentSemesterID();

$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_YearTerm = new academic_year_term($YearTermID);


//check whether any CNECC program 
$sql = "select 
			count(*) as NOOFRECORD , p.AcademicYearID as YEARID
		from 
			{$eclass_db}.OLE_PROGRAM as p inner join {$eclass_db}.CUSTOM_CNECC_SS_EVENT as e on p.programid = e.programid 
		group by p.AcademicYearID
		";
$resultSet = $objDB->returnArray($sql);

$jsCheckYearArray = "var YearCheck = new Array(".sizeof($copiedAcademicYearIDAry).");\n";


for($i = 0, $i_max = sizeof($copiedAcademicYearIDAry); $i< $i_max; $i++){
	$_yearId = $copiedAcademicYearIDAry[$i];
//	$_yearId = $resultSet[$i]["YEARID"];
//	$_noOfRecord = $resultSet[$i]["NOOFRECORD"];

	$jsCheckYearArray .= "YearCheck[{$_yearId}] = 1;\n";
}

# Acadermic Year Selection
$fcm = new form_class_manage();
$FutureYearArr = $fcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $NoPastYear=1, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $SelectedAcademicYearID);
$FutureYearIDArr = Get_Array_By_Key($FutureYearArr, 'AcademicYearID');
$AcadermicYearSelection = getSelectAcademicYear('SelectedAcademicYearID', $action='onchange="checkYearWithData(this.options[this.options.selectedIndex].value)"', 
								$noFirst='', $noPastYear=0, $SelectedAcademicYearID, $displayAll=0, $pastYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=$FutureYearIDArr);


/*
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
//$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='get_yearterm_opt()'", $ayID, 0, 0, "", 2);
$AcadermicYearSelection = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='get_yearterm_opt()'", $ayID, 0, 0, "", 2);
*/



//$AcadermicYearSelection = getSelectAcademicYear2('SelectedAcademicYearID', $action='onchange="checkYearWithData(this.options[this.options.selectedIndex].value)"', 
//								$noFirst=1, $noPastYear=0, $SelectedAcademicYearID, $displayAll=0, $pastYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=$FutureYearIDArr);
//function getSelectAcademicYear2($objName, $tag='', $all=0, $noFirst=0, $FirstTitle='', $noPastYear=0)

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update")."<br/>";

$linterface->LAYOUT_START();
include_once("template/selectYear.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();


?>