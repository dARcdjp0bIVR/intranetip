<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

// customized lib
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/libpf-slp-cwk.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf_ui = new libportfolio_ui();
$linterface = new interface_html();
$lpf_slp_cwk = new libpf_slp_cwk();

########################################################
# Tab Menu : Start
########################################################

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs(IPF_CFG_SLP_MGMT_CWK_SLP, 0);

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CWK_SLP);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

# title search
$search_text = ($search_text == $ec_iPortfolio['enter_title_name']) ? "" : $search_text;
$searching_html = "<div class=\"Conntent_search\"><input name='search_text' id='search_text' value=\"".(stripslashes($search_text))."\" ></div>";

// $import_btn_html = strstr($ck_function_rights, "ImportData") ? "<td nowrap><a href=\"ole_import.php?IntExt={$IntExt}&FromPage=pview\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import}</a></td>" : "";

# year selection
$lay = new academic_year();
$t_a_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($t_a_year_arr, "name='AcademicYearID' id='AcademicYearID'", $AcademicYearID, 1, 0, $iPort["all_school_years"], 2);
########################################################
# Operations : End
########################################################

########################################################
# Table content : Start
########################################################
/*
$pageSizeChangeEnabled = true;
$checkmaster = true;
*/
$cond = empty($search_text) ? "" : " AND act_s.ActivityName LIKE '%{$search_text}%'";
$cond .= empty($AcademicYearID) ? "" : " AND act_s.AcademicYearID = {$AcademicYearID}";

// Temp table for activity ability
$tempTableActAbility = $lpf_slp_cwk->createActAbility_temp();

// Temp table for activity criteria
$tempTableActCriteria = $lpf_slp_cwk->createActCriteria_temp();

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT DISTINCT
            act_s.ActivityName,
            t_aa.AbilityCode,
            t_ac.CriteriaCode,
            CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"activity_name[]\" value=\"', act_s.ActivityName ,'\">')
          FROM
            {$eclass_db}.ACTIVITY_STUDENT AS act_s
          LEFT JOIN {$tempTableActAbility} AS t_aa
            ON act_s.ActivityName = t_aa.ActivityName
          LEFT JOIN {$tempTableActCriteria} AS t_ac
            ON act_s.ActivityName = t_ac.ActivityName
          WHERE
            1
            {$cond}
        ";

// TABLE INFO
$LibTable->field_array = array("act_s.ActivityName");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 5;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='30%' >".$LibTable->column(0,$ec_iPortfolio['activity_name'], 1)."</td>\n";
$LibTable->column_list .= "<td width='30%' class=\"tabletopnolink\">{$Lang["Cust_Cwk"]["CommonAbility"]}</td>";
$LibTable->column_list .= "<td width='30%' class=\"tabletopnolink\">{$Lang["Cust_Cwk"]["MainCriteria"]}</td>\n";
$LibTable->column_list .= "<td >".$LibTable->check("activity_name[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";
$LibTable->column_array = array(0,0,0);

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
// set the current page title
$CurrentPage = "Teacher_OLE";

$linterface->LAYOUT_START();
include_once("template/activity.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>