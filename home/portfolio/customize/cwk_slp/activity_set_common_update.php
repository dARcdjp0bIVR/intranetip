<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$li->Start_Trans();

// db data preparation
$activity_name = htmlspecialchars($activity_name);
for($i=0, $i_max=count($act_ability); $i<$i_max; $i++)
{
  $_ability_id = $act_ability[$i];

  $activity_ability_val[] = "('{$activity_name}', '{$_ability_id}')";
}
for($i=0, $i_max=count($act_criteria); $i<$i_max; $i++)
{
  $_criteria_id = $act_criteria[$i];

  $activity_criteria_val[] = "('{$activity_name}', '{$_criteria_id}')";
}

// activity ability update
$sql = "DELETE FROM {$eclass_db}.CWK_ACTIVITY_ABILITY WHERE ActivityName = '{$activity_name}'";
$res[] = $li->db_db_query($sql);
if(count($activity_ability_val) > 0)
{
  $sql_field_val = implode(",", $activity_ability_val);

  $sql = "INSERT INTO {$eclass_db}.CWK_ACTIVITY_ABILITY ";
  $sql .= "(ActivityName, AbilityID) ";
  $sql .= "VALUES {$sql_field_val} ";
  $res[] = $li->db_db_query($sql);
}

// activity criteria update
$sql = "DELETE FROM {$eclass_db}.CWK_ACTIVITY_CRITERIA WHERE ActivityName = '{$activity_name}'";
$res[] = $li->db_db_query($sql);
if(count($activity_criteria_val) > 0)
{
  $sql_field_val = implode(",", $activity_criteria_val);

  $sql = "INSERT INTO {$eclass_db}.CWK_ACTIVITY_CRITERIA ";
  $sql .= "(ActivityName, ComponentID) ";
  $sql .= "VALUES {$sql_field_val} ";
  $res[] = $li->db_db_query($sql);
}

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $msg = "update";
}
else
{
  $li->RollBack_Trans();
  $msg = "update_failed";
}

intranet_closedb();
header("Location: activity.php?msg=$msg");
?>