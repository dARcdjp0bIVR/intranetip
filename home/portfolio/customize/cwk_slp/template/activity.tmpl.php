<?php
/**
 * [Modification Log] Modifying By: 
 * ************************************************
 * 2010-05-06 Max (201005061355)
 * - More eye catching Link
 * ************************************************
 */
?>
<script language="JavaScript">
function jSEARCH(){
  document.form1.submit();
}

$(document).ready(function() {
  $("#search_text").keypress(function(event) {
    if(event.keyCode == 13)
    {
      jSEARCH();
    }
  });
  
  $("#AcademicYearID").change(function() {
    document.form1.submit();
  });
});
</script>

<FORM name="form1" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <?=$op_result?>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
  			<tr>
  				<?=$import_btn_html?>
      		<td width="100%" align="right" valign="bottom" class="thumb_list">
    				<span class="tabletext">
    				  <?=$searching_html?>
    				</span>
    			</td>
  			</tr>
			</table>
    </td>
  </tr>
  <tr>
    <td>
      <?=$ay_selection_html?>
      <?=$ele_selection_html?>
      <?=$category_selection_html?>
      <?=$record_selection_html?>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%">
            <table border="0" cellpadding="3" cellspacing="0">
    					<tr>					
    						<td class="navigation">
    						<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
    						<?=$ec_iPortfolio['program_list']?>				
    						</td>
    					</tr>					
  					</table>
					</td>
          <td align="right">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
                <td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
                  <table border="0" cellpadding="0" cellspacing="2">
                    <tbody>
                      <tr>
                        <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                        <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'activity_name[]','activity_set_common.php',0)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_edit.gif" name="imgEdit" align="absmiddle" border="0" ><?=$ec_iPortfolio['edit']?></a></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <?=$table_content?>
      		</td>
      	</tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="approve" />
<input type="hidden" name="rejectType" />
<input type="hidden" name="pageNo" value="<?= $pageNo ?>" />
<input type="hidden" name="order" value="<?= $order ?>" />
<input type="hidden" name="field" value="<?= $field ?>" />
<input type="hidden" name="numPerPage" value="<?= $numPerPage ?>" />
<input type="hidden" name="page_size_change" />
<input type="hidden" name="FromPage" value="program" />
<input type="hidden" name="IntExt" value="<?=$IntExt?>" />
</FORM>