<?php
##	Modifying By: Max
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/customize/bps/libpf-slp-bps-napr-report.php");

$StudentID = $StudentID; // This student ID is a list of array in form of 01,02,03
$StudentIDArray = explode(",",$StudentID);
$academicYearID = $academicYearID;
$YearTermID = $YearTermID;
$DisplayRecordNum = $DisplayRecordNum;

intranet_auth();
intranet_opendb();


//DisplayRecordNum
switch ($printType) {
	case "html":
	    $libpf_slp_bps_napr_report = new libpf_slp_bps_napr_report($StudentIDArray, $academicYearID, $YearTermID,$DisplayRecordNum);
		$output = $libpf_slp_bps_napr_report->generateHtmlReport();
		break;
	case "word":
		$output = $libpf_slp_bps_napr_report->generateWordReport();
		break;
	case "pdf":
		$output = $libpf_slp_bps_napr_report->generatePdfReport();
		break;
	default:
		break;
}
echo $output;

intranet_closedb();
?>