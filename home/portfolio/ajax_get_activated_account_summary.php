<?php
// using 
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

intranet_opendb();

$liport = new libportfolio();
$liport->ACCESS_CONTROL("student_info");

$result = $liport->returnStudentLicenseSummary();

// Build the table in thick box
$x = "";
$x .= '<div id="summaryArea"></div>';
	$x .= '<div class="pop_summary" style="height:325px;">';
		$x .= '<div id="SummaryInfoSettingLayer" class="edit_pop_board_write" style="height:325px;"><br/>';
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				$x .='<tr><td class="student_thumb_content">';
					$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">';
						$x .= '<tr class="tabletop">';
							$x .= '<td width="55%"nowrap="nowrap">'.$ec_iPortfolio['Activated_Liense_Summary_current'].'</td>';
							$x .= '<td nowrap="nowrap"></td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Active'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.$result[0].'</td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Activated_Liense_Summary_inactive'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.$result[1].'</td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.($result[0]+$result[1]).'</td>';
						$x .= '</tr>';
						$x .= '<tr></tr>';
						$x .= '<tr class="tabletop">';
							$x .= '<td nowrap="nowrap">'.$ec_iPortfolio['Activated_Liense_Summary_without_class'].'</td>';
							$x .= '<td nowrap="nowrap"></td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Active'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.$result[2].'</td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Activated_Liense_Summary_inactive'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.$result[3].'</td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td class="row_off tabletext" nowrap="nowrap">'.($result[2]+$result[3]).'</td>';
						$x .= '</tr>';
						$x .= '<tr></tr>';
						$x .= '<tr class="tabletop">';
							$x .= '<td nowrap="nowrap">'.$ec_iPortfolio['Activated_Liense_Summary_alumni'].'</td>';
							$x .= '<td nowrap="nowrap"></td>';
						$x .= '</tr>';
						$x .= '<tr class="tablerow1">';
							$x .= '<td class="row_off tabletext" nowrap="nowrap">'.$ec_iPortfolio['Activated_Liense_Summary_total_amount'].' : </td><td class="row_off tabletext nowrap="nowrap">'.$result[4].'</td>';
						$x .= '</tr>';
					$x .= '</table>';
			$x .= '</td></tr></table>';
		$x .= '</div>';
	$x .= '</div>';
			
echo $x;
	
intranet_closedb();

?>