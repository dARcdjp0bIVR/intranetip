<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");

intranet_opendb();
libpf_lp2::registerCourseId();
list($portfolio_id, $user_id)=libpf_lp2::decryptPortfolioKey($portfolio);

$liblp2 = new libpf_lp2($user_id, $portfolio_id, $UserID);

$sql = "CREATE TABLE portfolio_prototype (
     portfolio_prototype_id int(8) NOT NULL auto_increment,
     parent_id int(8) default NULL,
     ordering int(4) default NULL,
     web_portfolio_id int(8) default NULL,
     user_id int(8) default NULL,
     title varchar(255) default NULL,
     description varchar(255) default NULL,
     url text,
     content longblob,
     readflag text,
     status char(2) default NULL,
     type char(8) default NULL,
     starttime datetime default NULL,
     endtime datetime default NULL,
     inputdate datetime default NULL,
     modified datetime default NULL,
     PRIMARY KEY (portfolio_prototype_id)
)";
$liblp2->db_db_query($sql);echo mysql_error();

$sql = "CREATE TABLE portfolio_draft (
     portfolio_draft_id int(8) NOT NULL auto_increment,
     parent_id int(8) default NULL,
     ordering int(4) default NULL,
     portfolio_prototype_id int(8) default NULL,
     web_portfolio_id int(8) default NULL,
     user_id int(8) default NULL,
     title varchar(255) default NULL,
     description varchar(255) default NULL,
     url text,
     content longblob,
     readflag text,
     status char(2) default NULL,
     type char(8) default NULL,
     starttime datetime default NULL,
     endtime datetime default NULL,
     inputdate datetime default NULL,
     modified datetime default NULL,
     PRIMARY KEY (portfolio_draft_id)
)";
$liblp2->db_db_query($sql);echo mysql_error();

$sql = "CREATE TABLE portfolio_publish (
     portfolio_publish_id int(8) NOT NULL auto_increment,
     parent_id int(8) default NULL,
     ordering int(4) default NULL,
     portfolio_draft_id int(8) default NULL,
     web_portfolio_id int(8) default NULL,
     user_id int(8) default NULL,
     title varchar(255) default NULL,
     description varchar(255) default NULL,
     url text,
     content longblob,
     readflag text,
     status char(2) default NULL,
     type char(8) default NULL,
     starttime datetime default NULL,
     endtime datetime default NULL,
     inputdate datetime default NULL,
     modified datetime default NULL,
     PRIMARY KEY (portfolio_publish_id)
)";
$liblp2->db_db_query($sql);echo mysql_error();

$sql = "ALTER TABLE web_portfolio ADD COLUMN version int(4) AFTER created_by";
$liblp2->db_db_query($sql);echo mysql_error();



intranet_closedb();
header("Location: ./");
?>