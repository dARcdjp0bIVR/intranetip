<script>

$(function(){
      
        $('#settings_form').submit(function(){
            
	    $.fancybox.showLoading();
            $.post('/home/portfolio/learning_portfolio_v2/ajax.php?action=publishPortfolio',$(this).serialize(),function(url){
		
		alert('<?=$langpf_lp2['common']['publish']['publishsuccess']?>');

		<?php if($is_stem_learning_scheme): ?>
		$.fancybox.close();
		<?php else: ?>
		$.fancybox.open("/home/portfolio/learning_portfolio_v2/ajax.php?action=getShare&portfolio=<?=$portfolio?>",{type: 'ajax', padding: 0, margin: 0});//show share page
		<?php endif; ?>
		
		try{
		    var target = window;
		    if (window.opener){
			target = window.opener;
		    }
		    target.loadPortfolioUsers('<?=$portfolio_id?>',0, $('#user_amount', target.document).val(), $('#user_keyword input',  target.document).val());

		    
		}catch(e){ console.log(e);}
	    });
	    
            return false;
        });
	
	$('#force_all_status').change(function(){
	    $('#settings_form select').val($(this).val());
	    return false;
	});
		
	$('.force_status_all').click(function(){
	    $('.force_status[value="'+$(this).val()+'"]').attr('checked','checked');
	})
        $('.setting_bg .formsubbutton').click($.fancybox.close);

});
</script>

<div class="setting_bg" style="width:500px">
    <form id="settings_form" action="ajax.php?action=publishPortfolio">
	<div class="setting_board">
	    <div class="control_board">
		<h2><span><?=$langpf_lp2['common']['publish']['sectionsettings']?></span></h2>
		<table class="publish_table" style="margin: 0 auto;">
		    <col style="width:40%"/>
		    <col span="4" style="width:10%"/>
		    
		<?php if($is_stem_learning_scheme): ?>
		<tr>
			<th>&nbsp;</th>
			<th><?=$langpf_lp2['common']['form']['myselfonly']?></th>
			<th><?=$langpf_lp2['common']['form']['publish']?></th>
		</tr>
		
		<tr style="background-color: #bbbbbb;">
		    <td><?=$langpf_lp2['common']['publish']['setallsectionsvisibleto']?></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="0"/></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="3"/></td>
		</tr>
		<?php else: ?>
		<tr>
			<th>&nbsp;</th>
			<th><?=$langpf_lp2['common']['form']['myselfonly']?></th>
			<th><?=$langpf_lp2['common']['form']['usersinsamegroup']?></th>
			<th><?=$langpf_lp2['common']['form']['allintranetusers']?></th>
			<th><?=$langpf_lp2['common']['form']['public']?></th>
		</tr>
		
		<tr style="background-color: #bbbbbb;">
		    <td><?=$langpf_lp2['common']['publish']['setallsectionsvisibleto']?></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="0"/></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="1"/></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="2"/></td>
		    <td><input type="radio" class="force_status_all" name="force_status[<?=$element['id']?>]" value="3"/></td>
		</tr>
		<?php endif; ?>
	
		<?php
		if($is_stem_learning_scheme):
		    foreach($data['elements'] as $element) : 
		?>
		    <tr>
    			<td style="text-align:left">
    			    <span style="padding-left:<?=$element['level']*20?>px"><?=$element['title']?></span> <?=$element['type']=='wbitem'?'<strong style="font-size:10px">('.$langpf_lp2['edit']['editform']['weblog'].')</strong>':''?>
    			</td>
    			<td><input type="radio" class="force_status" <?=$element['status']!=3?'checked':''?> name="force_status[<?=$element['id']?>]" value="0"/></td>
    			<td><input type="radio" class="force_status" <?=$element['status']==3?'checked':''?> name="force_status[<?=$element['id']?>]" value="3"/></td>
		    </tr>
		<?php
		    endforeach;
	    else:
		    foreach($data['elements'] as $element) : 
		?>
		    <tr>
    			<td style="text-align:left">
    			    <span style="padding-left:<?=$element['level']*20?>px"><?=$element['title']?></span> <?=$element['type']=='wbitem'?'<strong style="font-size:10px">('.$langpf_lp2['edit']['editform']['weblog'].')</strong>':''?>
    			</td>
    			<td><input type="radio" class="force_status" <?=$element['status']==0?'checked':''?> name="force_status[<?=$element['id']?>]" value="0"/></td>
    			<td><input type="radio" class="force_status" <?=$element['status']==1?'checked':''?> name="force_status[<?=$element['id']?>]" value="1"/></td>
    			<td><input type="radio" class="force_status" <?=$element['status']==2?'checked':''?> name="force_status[<?=$element['id']?>]" value="2"/></td>
    			<td><input type="radio" class="force_status" <?=$element['status']==3?'checked':''?> name="force_status[<?=$element['id']?>]" value="3"/></td>
		    </tr>
		<?php
		    endforeach;
		endif;
		?>
		</table>
    
	    </div>
	    
	    <p class="spacer"></p>
	    <?if(!$disable_facebook):?>
	    <div class="control_board">
		<h2><span><?=$langpf_lp2['common']['publish']['showfacebooklikebutton']?></span></h2>
		    <input name="allow_like" id="allow_like_1" <?=$portfolio_config['allow_like']?'checked="checked"':''?> type="radio" value="1"><label for="allow_like_1"><?=$langpf_lp2['common']['form']['yes']?></label>
		    <input name="allow_like" id="allow_like_0" <?=!$portfolio_config['allow_like']?'checked="checked"':''?>type="radio" value="0"><label for="allow_like_0"><?=$langpf_lp2['common']['form']['no']?></label>
	    </div>
	    <?endif;?>
	</div>
	<input type="hidden" name="mode" value="<?=$mode?>">
	<input type="hidden" name="portfolio" value="<?=$portfolio?>">
	<div class="edit_bottom"><input type="submit" value="<?=$langpf_lp2['common']['form']['publish']?>" class="formbutton" /><input type="button" value="<?=$langpf_lp2['common']['form']['cancel']?>" class="formsubbutton" /></div>
    </form>

</div>