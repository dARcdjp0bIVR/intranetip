<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"> 

<title>:: eClass iPortfolio :: <?=$data['student']['name']?></title>

<script src="<?=$common_files_root?>common_files/jquery-1.8.2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$common_files_root?>common_files/ipf_lp_common.css" />
<link rel="stylesheet" type="text/css" href="<?=$common_files_root?>common_files/themes/<?=$data['config']['template_selected']?>/css/theme.css" />

<script type="text/javascript">
    $(function(){
	
	$('.section a').click(function(){
	    var selector = $(this).attr('href').replace('#','.');
	    
	    $('#sub_menu ul, .page ul').hide();
	    $(selector).fadeIn().siblings().hide();
	    $('#sub_title').slideUp();
	    if($(selector).find('.page').removeClass('current').length==0){
		$('#main_content').removeClass('sub_menu_show').addClass('sub_menu_hide');
	    }else{
		$('#main_content').removeClass('sub_menu_hide').addClass('sub_menu_show');
	    }
	    if ($('.content '+selector).html()==''){
		$(selector).find('li:first').click();
	    }
	    
	    $(this).parent().addClass('current').siblings().removeClass('current');
	    return false;
	});
	$('.page>a').click(function(){
	    var selector = $(this).attr('href').replace('#','.');
	    
	    $('.page ul').not(selector).slideUp();
	    
	    $('#sub_title').show().add('.content').find(selector).fadeIn().siblings().hide();

	    if ($('.content '+selector).html()==''){
		$(selector).find('.subpage:first a').click();
	    }
	    
	    $(this).parent().addClass('current').find('.subpage_menu').slideDown().find('.subpage').removeClass('current_sub');
	    $(this).parent().siblings().find('.subpage_menu').slideUp(function(){$(this).parent().removeClass('current')});
	    
	    return false;
	});
	$('.subpage a').click(function(){
	    var selector = $(this).attr('href').replace('#','.');
	    
	    $(selector).fadeIn().siblings().hide();
	    
	    $(this).parent().addClass('current_sub').siblings().removeClass('current_sub');
	    return false;
	})
	
	$('.section:first a').click();
    });
</script>

</head>
<body>

    <div id="container">

	<div id="header">
           <div id="head_banner" <?=($data['config']['banner_status'] && $data['config']['banner_url'])? 'class="custom_banner" style="background-image: url(\''.$data['config']['banner_url'].'\')"':'class="default_banner"'?>>

		<span class="school_name"><?=$data['student']['school']?></span>
		<span class="student_name"><?=$data['student']['name']?> (<?=$data['student']['class']?>)</span>
		<span class="lp_name"><?=$data['info']['title']?></span>
		<span class="school_logo"><img src="<?=$data['school']['image']?>" /></span>
		
		<span class="animated_element"></span><!---->
		
	    </div>

        </div>
        
	
	<div id="content">
       <!---->
	    <div id="main_menu">
		
		<ul>    
		    <? foreach ((array)$data['sections'] as $i=>$section):?>
			
			<li class="seperator"></li>
			<li class="section" style="position:relative;">
			    <a href="#<?=$section['id']?>">
				<span><?=$section['title']?></span>
			    </a>
			    
			</li>
			
		    <? endforeach; ?>
		    <li class="seperator"></li>
		</ul>
		
	    </div>
                <!---->
            <div id="content_board">
      		
                <div class="content_board_top"><div class="content_board_top_right"><div class="content_board_top_bg"> </div></div></div>
                <div class="content_board_main"><div class="content_board_main_right"><div class="content_board_main_bg">
                
                    <div id="main_content" class="sub_menu_show">
                        <div id="page_title">
                            <? foreach ((array)$data['sections'] as $i=>$section):?>
				<span class="<?=$section['id']?>" style="display:none"><?=$section['title']?></span>
			    <? endforeach; ?>
                        </div>
                                <!---->
                        <div id="sub_menu">
			<? foreach ((array)$data['sections'] as $i=>$section):?>
			    <ul class="<?=$section['id']?>" style="display:none">
				<? foreach ((array)$section['children'] as $i=>$page):?>
				    
				    <li class="seperator"></li>
				    <li class="page" style="position:relative">
					<a href="#<?=$page['id']?>">
					    <span><?=$page['title']?></span>
					</a>
					<ul class="subpage_menu <?=$page['id']?>" style="display:none">
					    <? foreach ((array)$page['children'] as $subpage): ?>
						<li class="subpage">
						    <a href="#<?=$subpage['id']?>">
							<span><?=$subpage['title']?></span>
						    </a>
						</li>
					    <? endforeach; ?>
					</ul>
					
				    </li>
				    
				<? endforeach; ?>
				<li class="seperator"></li>
			    </ul>
			<? endforeach; ?>
			</div>
                        
        
                        <div id="sub_title">
			    <? foreach ((array)$data['sections'] as $i=>$section):?>
			    	 <? foreach ((array)$section['children'] as $i=>$page):?>
                   		<span class="<?=$page['id']?>" style="display:none"><?=$page['title']?></span>
                   		 <? foreach ((array)$page['children'] as $i=>$subpage):?>
                   		 	<span class="<?=$subpage['id']?>" style="display:none"><?=$subpage['title']?></span>
                   		 <? endforeach; ?>
			    	<? endforeach; ?>
			    <? endforeach; ?>
                        </div>
                            
                        <div id="content_detail">
			    
			    <div id="sub_sub_menu">
			    </div>
			    
			    <p class="spacer"></p>
			    
                            <div class="content">
				
				<? foreach ((array)$data['elements'] as $i=>$element):?>
				    <div class="<?=$element['id']?>" style="display:none">
					<ul id="weblog_display">
					<? foreach ((array)$element['weblogs'] as $weblog): ?>
					    <li class="weblog_item">
						
						<h4><?=$weblog['title']?> <span> <?=$weblog['url']?></span></h4>
						<div class="weblog_content">
						    <?=$weblog['content']?>
						</div>
					    </li>
					<? endforeach; ?>
					</ul>
					
					<?=$element['content']?>
				    </div>
				<? endforeach; ?>
				
			    </div>  
                            
                        </div>
                       
                        <p class="spacer"></p>
                    </div>
    
                </div></div></div>
                
                <div class="content_board_bottom"><div class="content_board_bottom_right"><div class="content_board_bottom_bg"> </div></div></div>
            
            </div>
             <!---->
             
        <p class="spacer"></p>
        
	</div>
		
        <div id="footer">
	    <span><a href="http://www.eclass.com.hk" target="_blank" title="eClass" class="link_eclass" style="background-image: url(images/logo_eclass_footer.gif);"></a><em>Powered by</em> </span>
	</div>
        
    </div>
</body>

</html>