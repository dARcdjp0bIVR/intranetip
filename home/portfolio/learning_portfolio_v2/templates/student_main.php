<script>
function loadUserPortfolios(page, amount, keyword){
    
    $.fancybox.showLoading();
    $.get('ajax.php?action=getUserPortfolios',{page: page, amount: amount, keyword: keyword}, function(data){
	$.fancybox.hideLoading();
	$('#user_lp_list').html(data).slideDown(800);
	<?if(!$disable_facebook):?>
	FB.XFBML.parse($('#user_lp_list').get(0));
	<?endif;?>	
	
    });
    
    return false;
    
}
function loadFriendPortfolios(page){
    
    
    $.get('ajax.php?action=getFriendPortfolios',{page: page, amount: 3}, function(data){
	
	$('#friend_lp_list').html(data).show();
	<?if(!$disable_facebook):?>
	FB.XFBML.parse($('#friend_lp_list').get(0));
	<?endif;?>
    });
    return false;
}

$(function(){
    
    //$(".fancybox").fancybox();
    
    var hash = location.hash.replace('#','').split('/');
    
    loadUserPortfolios(hash[0], hash[1], hash[2]);
    loadFriendPortfolios(0);
    
     $(".fancybox").fancybox({
	nextEffect: 'fade',
	prevEffect: 'fade',
	margin: 0,
	padding: 0,
	type: 'ajax',
	beforeClose: function(){
	   
	    $('#user_page').change();
	}
    });
     
    $(".fancybox_iframe").fancybox({
	maxWidth: 600,
	nextEffect: 'fade',
	prevEffect: 'fade',
	type: 'iframe',
	beforeClose: function(){
	   
	    $('#user_page').change();
	}
    });

    $('.lp_info_shared').fancybox({type: 'ajax', padding: 0, margin: 0});
    
});

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td valign="top" height="17"><table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
    <tr>
	<td width="17" height="37"><img width="17" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_01.gif"></td>
	<td height="37" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_03.gif"><table cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
				<td align="center" valign="middle" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_02.gif" class="page_title"><?=$iPort['menu']['learning_portfolio']?></td>
			</tr>
	</tbody></table></td>
	<td width="13" height="37"><img width="13" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_04.gif"></td>
	<td width="200" class="friend_lp_td" style="display:none;" height="37" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_01.gif">&nbsp;</td>
	<td width="20" class="friend_lp_td" style="display:none;" height="37" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_01.gif">&nbsp;</td>
    </tr>
    <tr>
	<td width="17" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_05.gif"><img width="17" height="20" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_05.gif"></td>
	<td valign="top" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_06.gif" class="tab_table">
	    <div id="user_lp_list" style="display:none;">
		
	    </div>
	</td>
	<td width="13" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_07.gif"><img width="13" height="37" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_07.gif"></td>
	<td valign="top" class="friend_lp_td" style="display:none;" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_bg.gif"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody>
	    <tr>
		<td align="center" valign="middle" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_title_01.gif" class="page_title_LP_sub"><?=$iPort['menu']['peer_learning_portfolio']?> <a href="Peer_view.php"><img src="<?=$image_path?>/<?php echo $LAYOUT_SKIN;?>/eEnrollment/icon_member.gif" title="<?=$ec_iPortfolio['view_peer_profolio']?>" border="0"/></a></td>
	    </tr>
	</tbody></table>
	    <div id="friend_lp_list" style="display:none;">
		
	    </div>

</td>
	    <td class="friend_lp_td" style="display:none;" width="20" valign="top" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_04.gif"><img width="20" height="34" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_title_02.gif"></td>
    </tr>
    <tr>
	    <td width="17" height="17"><img width="17" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_08.gif"></td>
	    <td height="17" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_09.gif"><img width="64" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_09.gif"></td>
	    <td width="13" height="17"><img width="13" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_10.gif"></td>
	    <td height="17" class="friend_lp_td" style="display:none;" background="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_05.gif"><img width="64" height="17" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_05.gif"></td>
	    <td width="20" class="friend_lp_td" style="display:none;" valign="top" height="17"><img width="20" height="7" src="/images/<?php echo $LAYOUT_SKIN;?>/iPortfolio/bg_LP_sub_06.gif"></td>
    </tr>
</tbody></table></td>
</tr>
</tbody></table>