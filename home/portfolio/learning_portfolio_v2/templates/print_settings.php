<script>

$(function(){
   $('#select_class').change(function(){
   		var classId = $(this).val();
  		if(classId==''){
			$('#tr_student_list').hide();
				$('#tr_title_list').hide();
				$('#btn_printselected').attr('disabled',true);
  		}else{
  			$.fancybox.showLoading();
  			$.get('ajax.php?action=getPublishedPortfolioUser',{classId: classId,portfolio:'<?=$portfolio?>'}, function(data){
				$('#select_students').html(data);
				$('#select_students option').attr('selected', 'selected');
				$('#tr_student_list').show();
				$('#tr_title_list').show();
				$('#btn_printselected').attr('disabled',false);
				$.fancybox.hideLoading();
  			});
  		}
   });
   $('#btn_selectall').click(function(){
   		$('#select_students option').attr('selected', 'selected');
   });
   $('#btn_printselected').click(function(){
   		if($("#select_students option:selected").length>0&&$("#section_id option:selected").length>0){
   			$("#print_lp_form").submit();
   		}
   });
   
	
	
});
</script>
<form id="print_lp_form" method="POST" action="<?=(checkHttpsWebProtocol())? 'https://' : 'http://'?><?=$eclass40_httppath?>src/iportfolio/print.php" target="_blank">
<div class="setting_bg" style="width:400px;height:400px;">
    <div class="setting_board">
		<div class="control_board">
	    	<h2><span><?=$langpf_lp2['edit']['tool']['print']?></span></h2>
	   		<table style="margin:0;width:100%;" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
				  	<td class="formfieldtitle" nowrap="nowrap" style="border:0;">
				  		<span class="tabletext"><?=$langpf_lp2['admin']['print']['class']?></span>
				  	</td>
				  	<td colspan="2">
					  	<? if ($data['classes']): ?>
						    <select name="select_class" id="select_class">
						    	<option value=""><?=$langpf_lp2['admin']['print']['selectclass']?></option>
								<? foreach($data['classes'] as $_key => $_classAry): ?>
								    <option value="<?=$_classAry['ClassID']?>"><?=$_classAry['ClassTitle']?></option>
								<? endforeach; ?>
						    </select>
			    			</br>
			   			<? endif; ?>
				  	</td>
				</tr>
				<tr id="tr_student_list" style="display:none;">
				  	<td class="formfieldtitle" nowrap="nowrap" valign="top" style="border:0;">
				  		<span class="tabletext"><?=$langpf_lp2['admin']['print']['student']?></span>
				  	</td>
				  	<td>
					    <select size="10" style="width:200px;" multiple="multiple" name="select_students[]" id="select_students">
							<? foreach($data['students'] as $_key => $_studentAry): ?>
							    <option value="<?=$_studentAry['UserID']?>" selected><?=$_studentAry['name']?></option>
							<? endforeach; ?>
					    </select>
			    		</br>
			    		<input type="hidden" name="portfolio" id="portfolio" value="<?=$portfolio?>">
				  	</td>
				  	<td align='left'> 
						<input type="button" id="btn_selectall" class="formsubbutton" value="<?=$langpf_lp2['admin']['print']['selectall']?>" />
					</td>
				</tr>	
				<tr id="tr_title_list" style="display:none;">
				  	<td class="formfieldtitle" nowrap="nowrap" valign="top" style="border:0;">
				  		<span class="tabletext"><?=$langpf_lp2['admin']['print']['page']?></span>
				  	</td>
				  	<td colspan="2">
				    <? if ($pageAry): ?>
					    <select name="section_id" id="section_id">
							<? foreach($pageAry as $_id => $_title): ?>
							    <option value="<?=$_id?>"><?=$_title?></option>
							<? endforeach; ?>
					    </select>
		    			</br>
		   			<? endif; ?>
				  	</td>
				</tr>			
			</table>
	    <p class="spacer"></p>
	</div>
	<div class="edit_bottom"><input type="button" id="btn_printselected" value="<?=$langpf_lp2['admin']['print']['continue']?>" class="formbutton" disabled/><input type="button" value="<?=$langpf_lp2['common']['form']['close']?>" class="formsubbutton" onclick="$.fancybox.close();" /></div>
</div>
</form>