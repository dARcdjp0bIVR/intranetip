<?
/**
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
*/
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
?>
<script>

$(function(){
    location.hash = '<?=$hash?>';

    getEclassSessionKey(function(key){
	$('#user_lp_list a.get_key').attr('href', function(i, href){return href+'&eclasskey='+key;});
    });

    $('#user_page input[name="keyword"]').focus();
    $('#user_page').change(function(){
	loadUserPortfolios($('#user_page').val(), $('#user_amount').val(), $('#user_keyword input').val());
    });
    $('#user_prev').click(function(){
	<? if ($page>0): ?>
	$('#user_page').val(<?=$page-1?>).change();
	<? endif; ?>
	return false;
    });
    $('#user_next').click(function(){
	<? if ($to<$total): ?>
	$('#user_page').val(<?=$page+1?>).change();
	<? endif; ?>
	return false;
    });
    $('#user_keyword').submit(function(e){
	loadUserPortfolios(0, $('#user_amount').val(), $('#user_keyword input').val());
	return false;
    });
    $('#user_amount').change(function(){
	loadUserPortfolios(0, $('#user_amount').val(), $('#user_keyword input').val());
    });
    $('#user_lp_list .lp_list').hide().fadeIn();




});


</script>

<table width="100%" cellspacing="0" cellpadding="3" border="0" class="tablegreenbottom">
    <tbody><tr>
	<td align="left" class="tabletext"><?=$langpf_lp2['admin']['navi']['records']?> <?=$from?> - <?=$to?>, <?=$langpf_lp2['admin']['navi']['total']?> <?=$total?></td>
	<td align="right">
	    <table cellspacing="0" cellpadding="0" border="0"><tbody>
		<tr>
		    <td>
			<table cellspacing="0" cellpadding="2" border="0"><tbody><tr align="center" valign="middle">
			    <td class="tabletext">
				<form id="user_keyword">
				<?=$langpf_lp2['admin']['navi']['search']?>: <input type="text" name="keyword" value="<?=$keyword?>"/>
				</form>
			    </td>
			    <td>
				<a href="#" id="user_prev" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('prevp1','','/images/<?php echo $LAYOUT_SKIN;?>/icon_prev_on.gif',1)" class="tablebottomlink">
				<img align="absmiddle" width="11" height="10" border="0" id="prevp1" name="prevp1" src="/images/<?php echo $LAYOUT_SKIN;?>/icon_prev_off.gif"></a> <span class="tabletext"> <?=$langpf_lp2['admin']['navi']['page']?> </span>
			    </td>
			    <td class="tabletext"><select class="formtextbox" name="Page" id="user_page">

				<? for ($i=0; $i<$max_page; $i++): ?>
				<option <?=$page==$i?'selected="selected"':''?> value="<?=$i?>"><?=$i+1?></option>
				<? endfor; ?>

			    </select></td>
			    <td>
				<span class="tabletext"></span>
				<a id="user_next" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('nextp1','','/images/<?php echo $LAYOUT_SKIN;?>/icon_next_on.gif',1)" class="tablebottomlink" href="#">
				    <img align="absmiddle" width="11" height="10" border="0" id="nextp1" name="nextp1" src="/images/<?php echo $LAYOUT_SKIN;?>/icon_next_off.gif">
				</a>
			    </td>
			</tr></tbody></table>
		    </td>
		    <td>&nbsp;<img src="/images/<?php echo $LAYOUT_SKIN;?>/10x10.gif"></td>
		    <td>
			<table cellspacing="0" cellpadding="2" border="0" class="tabletext"><tbody><tr>
			    <td><?=$langpf_lp2['admin']['navi']['display']?></td>
			    <td><select class="formtextbox" id="user_amount" name="select4">
				<option <?=$amount=='all'?'selected="selected"':''?> value="all"><?=$langpf_lp2['admin']['navi']['all']?></option>
				<? foreach ($amount_choices as $choice): ?>
				<option <?=$amount==$choice?'selected="selected"':''?> value="<?=$choice?>"><?=$choice?></option>
				<? endforeach; ?>
			    </select></td>
			    <td><?=$langpf_lp2['admin']['navi']['perpage']?></td>
			</tr></tbody></table></td>
		</tr>
	    </tbody></table>
	</td>
    </tr></tbody>
</table><br>
<? if ($show_sharepeerbtn): ?><a class="fancybox btn_friend_list" rel="portfolio_share_all" href="ajax.php?action=getShareFriendsSetting"><?=$langpf_lp2['common']['share']['my_friend_list']?></a><? endif; ?>
<div class="lp_list">
    <ul>
    <? foreach ($data['student_lps'] as $lp):?>

	<li class="theme_<?=$lp['published']? $lp['theme'].' lp_status_published':$lp['theme']?>">

	    <div class="lp_content">
		<h1 title="<?=$lp['title']?>"><?=$lp['title']?></h1>
		<div class="lp_theme"></div>

		<div class="lp_edit_tool"<? if($isMobilePlatform) :?> style="visibility: visible; opacity: 1;" <?endif;?>>
		    <? if ($lp['version']>=1): ?>
			<? if ($lp['within_time']): ?>
			    <a href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=draft&portfolio=<?=$lp['key']?>" class="tool_edit" target="_blank"><?=$langpf_lp2['admin']['item']['edit']?></a>
			<? endif; ?>
			<a rel="portfolio_publish" title="<?=$lp['title']?>" href="ajax.php?action=getPublish&portfolio=<?=$lp['key']?>" class="fancybox tool_publish"><?=$langpf_lp2['admin']['item']['publish']?></a>
			<a href="ajax.php?action=getThemeSettings&portfolio=<?=$lp['key']?>" class="tool_settings fancybox"><?=$langpf_lp2['admin']['item']['settings']?></a>
			<? if ($lp['published']): ?>
			<a href="ajax.php?action=getExportDoc&portfolio=<?=$lp['key']?>" class="tool_export"><?=$langpf_lp2['admin']['item']['export']?></a>
			<? endif; ?>
		    <? else: ?>
			<? if ($lp['within_time']): ?>
			<? /*<a href="/home/portfolio/learning_portfolio/browse/student.php?key=<?=$lp['key']?>" class="tool_edit get_key" target="_blank"><?=$langpf_lp2['admin']['item']['edit']?></a> */ ?>
			<a class="tool_edit" href="javascript:openEclassWindow('student','<?=$lp['key']?>','<?=$UserID?>', 93)"><?=$langpf_lp2['admin']['item']['edit']?></a>
			<? endif; ?>
			<a rel="portfolio_settings" href="/home/portfolio/learning_portfolio/contents_student/management/notes_setting.php?WebPortfolioID=<?=$lp['web_portfolio_id']?>" class="tool_settings"><?=$langpf_lp2['admin']['item']['settings']?></a>
		    <? endif; ?>

		</div>

		<div class="lp_publish_info">


		    <? if ($lp['published']): ?>
		    <span class="lp_info_draft" title="<?=$lp['modified']?>"><?=$langpf_lp2['admin']['item']['lastmodified']?>: <?=$lp['modified_days']?></span>
		    <div class="lp_publish_date">
		    <? if ($lp['version']<1):
			$worddoc_path = $eclass_filepath."/files/".$liblp2->db."/portfolio/student_u".$liblp2->current_user_id."_wp".$lp['web_portfolio_id']."/".parseFileName(undo_htmlspecialchars($lp["title"])).".doc";
			$worddoc_action = (file_exists($worddoc_path)) ? "{$HTTP_SSL}{$eclass_httppath}/src/ip2portfolio/contents_student/management/worddoc_download.php?web_portfolio_id=".$lp['web_portfolio_id']."&user_id=".$liblp2->current_user_id : "javascript:alert('{$Lang['iPortfolio']['LPnoWordDoc']}')";

	    	?> <span style="float:left;margin-right: 5px;"><a href="<?=$worddoc_action?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/file/doc.gif" width="16" height="16" border="0"></a></span> <? endif ?>
			<? if ($lp['version']>=1): ?>
			    <a class="lp_info_published" href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=publish&portfolio=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? else: ?>
			  <? /*  <a class="lp_info_published get_key" href="/home/portfolio/learning_portfolio/browse/student_view.php?key=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a> */ ?>
			    <a class="lp_info_published" href="javascript:openEclassWindow('student_view','<?=$lp['key']?>','<?=$UserID?>', 95)"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? endif; ?>



			<span class="date_time" title="<?=$lp['published']?>"> <?=$lp['published_days']?></span>

			<?php if(!$sys_custom['lp_stem_learning_scheme']): ?>
			<a rel="portfolio_comment" title="<?=$lp['title']?>" class="lp_info_comment fancybox_iframe" href="/home/portfolio/learning_portfolio/browse/student_comment.php?key=<?=$lp['key']?>"><?=$lp['comments_count']? '('.$lp['comments_count'].')': '&nbsp;'?></a>
			<?php endif; ?>

		    </div>
		    <? elseif ($lp['modified']): ?>
		    <div class="lp_publish_date"><span><?=$langpf_lp2['admin']['item']['drafted']?> </span>
			<span class="date_time" title="<?=$lp['modified']?>"> <?=$lp['modified_days']?></span>
		    </div>
		    <? endif; ?>

		</div>
	    </div>


	    <? if ($lp['published']): ?>
	    <div class="lp_ref">
		<? if ($lp['version']>=1&&$lp['allow_like']): ?>
		<div class="lp_fb_like">
		    <div class="fb-like" data-href="<?=$lp['share_url']?>" data-send="false" data-action="like" data-font="arial" data-layout="standard" data-colorscheme="light" data-width="80" data-height="20" data-show-faces="false"></div>
		</div>
		<? endif ?>
		<?php if(!$sys_custom['lp_stem_learning_scheme']): ?>
		<a class="lp_info_shared" id="lp_info_shared_s<?=$UserID?>_wp<?=$lp['web_portfolio_id']?>" rel="portfolio_share" title="<?=$lp['title']?>"  href="ajax.php?action=getShare&portfolio=<?=$lp['key']?>"><?=$langpf_lp2['admin']['item']['share']?></a>
	    <? endif; ?>
	    </div>
	    <? endif; ?>
	</li>
    <? endforeach; ?>
    </ul>
</div>
<br>

