<?
/**
 * Modification Log:
 * 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
 *      - Fix hardcoded http
*/
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';
?>
<script>

$(function(){

    block_hashchange=true;
    location.hash = '<?=$hash?>';

    $('#portfolio_page').change(function(){
    	loadAllPublishedPortfolios($('#portfolio_page').val(), $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_group').val(), $('#portfolio_portfolio').val());
    });
    $('#portfolio_group, #portfolio_portfolio').change(function(){
	loadAllPublishedPortfolios(0, $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_group').val(), $('#portfolio_portfolio').val());
    });
    $('#portfolio_keyword').submit(function(e){
	loadAllPublishedPortfolios(0, $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_group').val(), $('#portfolio_portfolio').val());
	return false;
    });
    $('#portfolio_prev').click(function(){
	<? if ($page>0): ?>
	$('#portfolio_page').val(<?=$page-1?>).change();
	<? endif; ?>
	return false;
    });
    $('#portfolio_next').click(function(){
	<? if ($to<$total): ?>
	$('#portfolio_page').val(<?=$page+1?>).change();
	<? endif; ?>
	return false;
    });

    $('#portfolio_amount').change(function(){
	loadAllPublishedPortfolios(0, $('#portfolio_amount').val(), $('#portfolio_keyword input').val(), $('#portfolio_group').val(), $('#portfolio_portfolio').val());
    });
    $('#user_lp_list .lp_list').hide().fadeIn();

    $('#user_lp_list a.get_key').click(function(){

	var href = $(this).attr('href');

	getEclassSessionKey(function(key){
	    window.open(href+'&eclasskey='+key);
	});

	return false;

    });

});


</script>

<?php if(!$is_stem_learning_scheme): ?>
    <div style="text-align:right; padding:10px;">
    <?=$langpf_lp2['admin']['top']['group']?>:
    <select class="formtextbox" name="Page" id="portfolio_group">
        <option value=""><?=$langpf_lp2['admin']['top']['all']?></option>
    	<option value="-100" <? if ($group_id==-100) echo "selected='selected'" ?> > 小 學 </option>
    <?php
    	for ($gi=0; $gi<sizeof($data['groups']); $gi++)
    	{
    			list($gid, $gname) = $data['groups'][$gi];
    			if ($gid>0 && in_array($gname, $CompetitionGroups[-100]))
    			{
    				echo "<option value=\"{$gid}\" ". (($group_id==$gid) ? "selected='selected'" : "" )." >{$gname}</option>\n";
    			}
    	}
    ?>
    	<option value="-200" <? if ($group_id==-200) echo "selected='selected'" ?> > 中 學 </option>
    <?php
    	for ($gi=0; $gi<sizeof($data['groups']); $gi++)
    	{
    			list($gid, $gname) = $data['groups'][$gi];
    			if ($gid>0 && in_array($gname, $CompetitionGroups[-200]))
    			{
    				echo "<option value=\"{$gid}\" ". (($group_id==$gid) ? "selected='selected'" : "" )." >{$gname}</option>\n";
    			}
    	}
    ?>
    </select>

    </div>
<?php endif; ?>


<table width="100%" cellspacing="0" cellpadding="3" border="0" class="tablegreenbottom">
    <tbody><tr>
	<td align="left" class="tabletext"><?=$langpf_lp2['admin']['navi']['records']?> <?=$from?> - <?=$to?>, <?=$langpf_lp2['admin']['navi']['total']?> <?=$total?></td>
	<td align="right">
	    <table cellspacing="0" cellpadding="0" border="0"><tbody>
		<tr>
		    <td>
			<table cellspacing="0" border="0"><tbody><tr align="center" valign="middle"><tr>
			    <td nowrap class="tabletext" style="padding:0px 10px; border-left:1px solid white;">
				<form id="portfolio_keyword">
				<?=$langpf_lp2['admin']['navi']['search']?>: <input type="text" name="keyword" value="<?=$keyword?>"/>
				</form>
			    </td>
			    <td style="white-space:nowrap;padding:0px 10px; border-left:1px solid white;">
				<a href="#" id="portfolio_prev" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('prevp1','','/images/2009a/icon_prev_on.gif',1)" class="tablebottomlink">
				<img align="absmiddle" width="11" height="10" border="0" id="prevp1" name="prevp1" src="/images/2009a/icon_prev_off.gif"></a> <span class="tabletext"> <?=$langpf_lp2['admin']['navi']['page']?> </span>
			    </td>

			    <td class="tabletext" ><select class="formtextbox" name="Page" id="portfolio_page">

				<? for ($i=0; $i<$max_page; $i++): ?>
				<option <?=$page==$i?'selected="selected"':''?> value="<?=$i?>"><?=$i+1?></option>
				<? endfor; ?>

			    </select></td>
			    <td >
				<span class="tabletext"></span>
				<a id="portfolio_next" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('nextp1','','/images/2009a/icon_next_on.gif',1)" class="tablebottomlink" href="#">
				    <img align="absmiddle" width="11" height="10" border="0" id="nextp1" name="nextp1" src="/images/2009a/icon_next_off.gif">
				</a>
			    </td>
			</tr></tbody></table>

		    </td>
		    <td>&nbsp;<img src="/images/2009a/10x10.gif"></td>
		    <td style="padding:0px 10px; border-left:1px solid white;">
			<table cellspacing="0" cellpadding="2" border="0" class="tabletext"><tbody><tr>
			    <td><?=$langpf_lp2['admin']['navi']['display']?></td>
			    <td style="white-space:nowrap;"><select class="formtextbox" id="portfolio_amount" name="select4">
				<option <?=$amount=='all'?'selected="selected"':''?> value="all"><?=$langpf_lp2['admin']['navi']['all']?></option>
				<? foreach ($amount_choices as $choice): ?>
				<option <?=$amount==$choice?'selected="selected"':''?> value="<?=$choice?>"><?=$choice?></option>
				<? endforeach; ?>
			    </select></td>
			    <td><?=$langpf_lp2['admin']['navi']['perpage']?></td>

			</tr></tbody></table>
		    </td>
		</tr>

	    </tbody></table>
	</td>
    </tr></tbody>
</table><br>

<div class="lp_list">
    <ul>
    <? foreach ($data['student_lps'] as $lp):?>

	<li class="theme_<?=$lp['published']? $lp['theme'].' lp_status_published':$lp['theme']?>">

	    <div class="lp_content">
		<h1 title="<?=$lp['name'].'('.$lp['class'].')'?>"><?=$lp['unread']?'<span style="color:red;">*</span>':''?><?=$lp['name'].'('.$lp['class'].')'?><br/> [<?=$lp['title']?>]</h1>
		<div class="lp_theme">
		    <div class="student_photo" style="bottom:20px; right:10px;z-index:1" > <img src="<?=$lp['photo']?>"></div>
		</div>

		<? if ($lp['version']>=1): ?>
		    <div class="lp_edit_tool">

			<a href="ajax.php?action=getExportDoc&portfolio=<?=$lp['key']?>" class="tool_export"><?=$langpf_lp2['admin']['item']['export']?></a>

		    </div>

		<? endif; ?>

		<div class="lp_publish_info">


		    <? if ($lp['published']): ?>

		    <div class="lp_publish_date">

			<? if ($lp['version']>=1): ?>
			    <a class="lp_info_published"  href="<?=$HTTP_SSL.$eclass40_httppath?>src/iportfolio/?mode=publish&portfolio=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? else: ?>
			    <a class="lp_info_published get_key" href="/home/portfolio/learning_portfolio/browse/student_view.php?key=<?=$lp['key']?>" target="_blank"><?=$langpf_lp2['admin']['item']['published']?></a>
			<? endif; ?>

			<span class="date_time" title="<?=$lp['published']?>"> <?=$lp['published_days']?></span>

			<?php if(!$is_stem_learning_scheme): ?>
			<a rel="portfolio_comment" title="<?=$lp['title']?>" class="lp_info_comment fancybox_iframe" href="/home/portfolio/learning_portfolio/browse/student_comment.php?key=<?=$lp['key']?>"><?=$lp['comments_count']? '('.$lp['comments_count'].')': '&nbsp;'?></a>
			<?php endif; ?>

		    </div>
		    <? elseif ($lp['modified']): ?>
		    <div class="lp_publish_date"><span><?=$langpf_lp2['admin']['item']['drafted']?></span>
			<span class="date_time" title="<?=$lp['modified']?>"> <?=$lp['modified_days']?></span>
		    </div>
		    <? endif; ?>

		</div>
	    </div>


	    <? if ($lp['published'] && !$is_stem_learning_scheme): ?>
	    <div class="lp_ref">
		<? if ($lp['version']>=1): ?>
		<div class="lp_fb_like">
		    <div class="fb-like" data-href="<?=$lp['share_url']?>" data-send="false" data-layout="standard" data-width="100" data-show-faces="false"></div>
		</div>
		<? endif ?>
		<a class="lp_info_shared fancybox" href="ajax.php?action=getShare&portfolio=<?=$lp['key']?>"><?=$langpf_lp2['admin']['item']['share']?></a>
	    </div>
	    <? endif; ?>
	</li>
    <? endforeach; ?>
    </ul>
</div>
<br>