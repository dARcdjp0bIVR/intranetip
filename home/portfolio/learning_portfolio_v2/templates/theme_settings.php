<script>
$(function(){
    


	var currentTheme = 'theme_<?=$current_theme?>';
        $('#themesetting .template_thumb').click(function(){
                
		var theme = $(this).parent().attr('id');
		$('#'+theme).addClass('selected').siblings().removeClass('selected');
                return false;
        });
        
        $('#themesetting .formbutton').click(function(){
            
            currentTheme = $('.template_list li.selected').attr('id');
            $.post('/home/portfolio/learning_portfolio_v2/ajax.php?action=setTheme&portfolio=<?=$portfolio?>',{
		    theme: currentTheme.replace('theme_',''),
		    banner_status: $('input[name="banner_status"]:checked').val(),
		    custom_root: $('input[name="custom_root"]:checked').val(),
		    banner_uploaded: $('#uploaded_file_name').is(':empty')?0:1
		},function(){
		    $.fancybox.close();
	    });
           
            return false;
        });
        $('#themesetting .formsubbutton').click(function(){
	    
            $.fancybox.close();
            return false;
        });

});
   </script>
        <div id="themesetting" class="setting_bg">
            <div class="setting_board">
               
                <div class="control_board">
                    <h2><span><?=$langpf_lp2['edit']['themesettings']['theme']?></span></h2>
                    <ul class="template_list">
                        <? foreach($themes as $theme): ?>
                            <li id="theme_<?=$theme?>" <?=$theme==$current_theme? 'class="selected"':''?>>
                                <a href="#" class="template_thumb" title=""><span></span></a>
                            </li>
                        <? endforeach; ?>                    
                    </ul>
                </div>
                
                <p class="spacer"></p>
		

            </div>
            <div class="edit_bottom"><input type="button" value="<?=$langpf_lp2['common']['form']['apply']?>" class="formbutton" /><input type="button" value="<?=$langpf_lp2['common']['form']['cancel']?>" class="formsubbutton" /></div>
        </div>