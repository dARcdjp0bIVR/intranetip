<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

    <h1><?=$data['student']['school']?> - <?=$data['info']['title']?></h1>
    <h2><?=$langpf_lp2['admin']['export']['student'] ?>: <?=$data['student']['name']?></h2>
    <h2><?=$langpf_lp2['admin']['export']['class'] ?>: <?=$data['student']['class']?></h2>
    <ol>
    <? foreach ($data['sections'] as $i=>$section):?>
	<li><h3><?=$section['title']?></h3>
	    <?=$data['elements'][$section['id']]['content']?>
	    <? foreach ($data['elements'][$section['id']]['weblogs'] as $weblog): ?>
		<div class="weblog_item">
		    
		    <h4><?=$weblog['title']?> <span> <?=$weblog['url']?></span></h4>
		    <div class="weblog_content">
			<?=$weblog['content']?>
		    </div>
		</div>
	    <? endforeach; ?>
	    <ol>
		<? foreach ($section['children'] as $i=>$page):?>
		    <li><h3><?=$page['title']?></h3>
			<?=$data['elements'][$page['id']]['content']?>
			<? foreach ($data['elements'][$page['id']]['weblogs'] as $weblog): ?>
			    <div class="weblog_item">
				
				<h4><?=$weblog['title']?> <span> <?=$weblog['url']?></span></h4>
				<div class="weblog_content">
				    <?=$weblog['content']?>
				</div>
			    </div>
			<? endforeach; ?>
			<ol>
			    <? foreach ($page['children'] as $i=>$subpage):?>
				<li><h3><?=$subpage['title']?></h3>
				    <?=$data['elements'][$subpage['id']]['content']?>
				    <? foreach ($data['elements'][$subpage['id']]['weblogs'] as $weblog): ?>
				    <div class="weblog_item">
					
					<h4><?=$weblog['title']?> <span> <?=$weblog['url']?></span></h4>
					<div class="weblog_content">
					    <?=$weblog['content']?>
					</div>
				    </div>
				<? endforeach; ?>
				</li>
			    <? endforeach; ?>
			    
			</ol>
		
		    </li>
		<? endforeach; ?>
		
	    </ol>
	    
	</li>
    <? endforeach; ?>
    </ol>
</body>

</html>