<?
/**
 * LPv2 Manage Page - ajax request controller
*
*
* @author Mick Chiu
* @since 2012-03-27
* 2019-05-08 Pun [160276] [ip.2.5.10.6.1]
*      - Fix hardcoded http
* 2015-11-11 Siuwan [ip.2.5.7.1.1.0] (Case#U80923)
* 		- added case 'printStudentList', 'getPublishedPortfolioUser' for printing student LP
*/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();
if($iportfolio_lp_version != 2){
	exit;
}
$HTTP_SSL = (checkHttpsWebProtocol())? 'https://' : 'http://';

if ($portfolio) list($portfolio_id, $user_id)=libpf_lp2::decryptPortfolioKey($portfolio);
if($_SESSION["platform"]=="KIS"&&$ck_memberType=='S'&&isset($ck_intranet_user_id)){
	$liblp2 = new libpf_lp2($user_id, $portfolio_id, $ck_intranet_user_id, 'publish');
}else{
	$liblp2 = new libpf_lp2($user_id, $portfolio_id, $UserID, 'publish');
}
$portfolio_config 	= $liblp2->getPortfolioConfig();
$portfolio_info 	= $liblp2->getPortfolioInfo();
$version 		= $portfolio_info['version'];
$LPSetting = $liblp2->getLPConfigSetting();
$disable_facebook = $LPSetting['disable_facebook'];

$is_owner 		= $liblp2->user_permission==0;
$is_teacher 		= $liblp2->memberType=='T';
$is_competition 	= libpf_lp2::$is_competition;
$is_stem_learning_scheme = libpf_lp2::$is_stem_learning_scheme;
$is_admin = libpf_lp2::$is_admin;
$isMobilePlatform = $userBrowser->platform == "iPad" || $userBrowser->platform == "Andriod";

$request_ts		= time();

if ($liblp2->memberType=='T'){

	switch($action){

		case 'getAllPortfolios':

			$sortby_choices 	= $langpf_lp2['admin']['navi']['order_choice'];
			$sortby 		= in_array($sortby,array_keys($sortby_choices))? $sortby: 'modified';
			$order 		= in_array($order,array('asc','desc'))? $order : 'desc';
			$keyword 		= trim($keyword);
			$amount_choices	= array(10,20,50,100);
			$amount 		= in_array($amount,$amount_choices)||$amount=='all'? $amount: 20;

			list($total, $lps)  = $liblp2->getAllPortfolios($keyword, $sortby, $order, $page*$amount, $amount);
			$data['student_lps'] = array();

			foreach ($lps as $lp){

				$lp['key'] 		= libpf_lp2::encryptPortfolioKey( $lp['web_portfolio_id'], $UserID);
				$lp['modified_days'] 	= libpf_lp2::getDaysWord($lp['modified']);
				$lp['published_days'] 	= libpf_lp2::getDaysWord($lp['published']);
				$lp['share_url'] 	= libpf_lp2::getPortfolioUrl($lp['key']);
				$data['student_lps'][] 	= $lp;

			}
			$data['user_info'] = $liblp2->getUserInfo();
			$max_page 	= $amount == 'all'? 1: ceil($total/$amount);
			$hash 	= $page.'/'.$amount.'/'.$keyword.'/'.$sortby.'/'.$order;

			$from 	= $total?$page*$amount+1:0;
			$to 	= $page*$amount+sizeof($data['student_lps']);

			include_once('templates/teacher_portfolio_list.php');
			break;

		case 'getAllPublishedPortfolios':

			$keyword 		= trim($keyword);
			$amount_choices	= array(10,20,50,100);
			$amount 		= in_array($amount,$amount_choices)||$amount=='all'? $amount: 20;
			$group_id		= $keyword? '': $group_id;

			if (file_exists("school_list.php"))
			{
				include_once("school_list.php");
			}

			/**** Competition Mode START ****/
			if ($group_id==-100 || $group_id==-200)
			{
				$group_name_arr = $CompetitionGroups[$group_id];
				if (sizeof($group_name_arr)>0)
				{
					$group_names_cond = implode ("', '", $group_name_arr);
					$sql_for_groups = "SELECT group_id FROM grouping WHERE group_name in ('{$group_names_cond}')";
					$group_ids = $liblp2->returnVector($sql_for_groups);
					//debug($sql_for_groups);
					//debug_r($group_ids);
				}
			}else
			{
				$group_ids= $group_id;
			}
			/**** Competition Mode END ****/

			list($total, $lps)  = $liblp2->getAllPublishedPortfolios($keyword, $group_ids, $page*$amount, $amount);

			$data['student_lps']	= array();
			$data['portfolios']		= array();
			$data['groups']		= $liblp2->getAllPortfolioGroups($is_competition);

			foreach ($lps as $lp){

				$lp 			= array_merge($lp, $liblp2->getUserInfo($lp['user_intranet_id']));
				$lp['key'] 		= libpf_lp2::encryptPortfolioKey( $lp['web_portfolio_id'], $lp['user_intranet_id']);
				$lp['published_days'] 	= libpf_lp2::getDaysWord($lp['published']);
				$lp['share_url'] 	= libpf_lp2::getPortfolioUrl($lp['key']);
				$lp['unread'] 		= $lp['readflag']&&!$liblp2->isUserInReadflag($lp['readflag']);

				$data['student_lps'][] = $lp;

			}

			$max_page 	= $amount == 'all'? 1: ceil($total/$amount);
			$hash 	= $page.'/'.$amount.'/'.$keyword.'/'.$group_id;

			$from 	= $total?$page*$amount+1:0;
			$to 	= $page*$amount+sizeof($data['student_lps']);

			include_once('templates/judge_portfolio_list.php');

			break;
		case 'printStudentList':
			$data = $liblp2->getPublishedPortfolioUserClassListByPortfolioId();
			$liblp2->setPortfolioMode('prototype');
			$sectionAry = $liblp2->getElementChildren(-1);
			$pageAry = array();
			foreach((array)$sectionAry as $_key => $_sectionAry){
				$pageAry[$_sectionAry['id']] = $_sectionAry['title'];
				$_subpageAry = $liblp2->getElementChildren($_sectionAry['id']);
				foreach((array)$_subpageAry as $__key => $__subpageAry){
					$pageAry[$__subpageAry['id']] = $__subpageAry['title'];
					$__subsubpageAry = $liblp2->getElementChildren($__subpageAry['id']);
					foreach((array)$__subsubpageAry as $___key => $___subsubpageAry){
						$pageAry[$___subsubpageAry['id']] = $___subsubpageAry['title'];
					}
				}
			}
			include("templates/print_settings.php");
			break;
		case 'getPublishedPortfolioUser':
			$students = $liblp2->getPublishedPortfolioUserListByClassId($classId);
			$html = "";
			foreach($students as $_key => $_studentAry){
				$html .= "<option value=\"".$_studentAry['UserID']."\">".$_studentAry['name']."</option>";
			}
			echo $html;
			break;
		case 'getPortfolioUsers':
		    $studentProgressSortFields = $langpf_lp2['admin']['navi']['studentProgressSortFields'];
		    $selectedStudentProgressSortBy = in_array($selectedStudentProgressSortBy,array_keys($studentProgressSortFields))? $selectedStudentProgressSortBy: 'notes_published|desc';
		    list($sortBy, $order) = explode('|', $selectedStudentProgressSortBy);

		    $studentProgressPublishStatus = $langpf_lp2['admin']['navi']['studentProgressPublishStatus'];
		    $selectedStudentProgressPublishStatus = in_array($selectedStudentProgressPublishStatus,array_keys($studentProgressPublishStatus))? $selectedStudentProgressPublishStatus: 'all';

		    $keyword 		= trim($keyword);
			$amount_choices	= array(10,20,50,100);
			$amount 		= in_array($amount,$amount_choices)||$amount=='all'? $amount: 20;

			list($total, $lps)  = $liblp2->getPortfolioUsers($keyword, $selectedStudentProgressPublishStatus, $page*$amount, $amount, $sortBy, $order);

			$data['student_lps']=array();
			foreach ($lps as $lp){

				$lp['user_intranet_id'] = $liblp2->EC_USER_ID_TO_IP_USER_ID($lp['user_id']);
				$lp = array_merge($lp, $liblp2->getUserInfo($lp['user_intranet_id']));

				$lp['key'] 		= libpf_lp2::encryptPortfolioKey( $lp['web_portfolio_id'], $lp['user_intranet_id']);
				$lp['modified_days'] 	= libpf_lp2::getDaysWord($lp['modified']);
				$lp['published_days'] 	= libpf_lp2::getDaysWord($lp['published']);
				$lp['share_url'] 	= libpf_lp2::getPortfolioUrl($lp['key']);
				$lp['unread'] 		= $lp['readflag']&&!$liblp2->isUserInReadflag($lp['readflag']);

				$data['student_lps'][]	= $lp;
			}

			$max_page 	= $amount == 'all'? 1: ceil($total/$amount);

			$from 	= $total?$page*$amount+1:0;
			$to 	= $page*$amount+sizeof($data['student_lps']);

			$data['title'] 		= $portfolio_info['title'];


			$data['user_info'] = $liblp2->getUserInfo();

			$KIS_showEditTool = true;
			if( $_SESSION["platform"] == 'KIS' && $data['user_info']['KIS_teacher_type'] != 'admin' && time() > strtotime($portfolio_info['deadline']) && strtotime($portfolio_info['deadline'])){
				$KIS_showEditTool = false;
			}

			include_once('templates/teacher_portfolio_progress_list.php');
			break;

		case 'getSettings':

			include_once($eclass_filepath."/src/includes/php/lib-groups.php");
			include_once($intranet_root."/includes/form_class_manage_ui.php");
			include_once($intranet_root."/includes/subject_class_mapping_ui.php");

			$lo = new libgroups($liblp2->db);

			if ($portfolio_id){

				$data['group_out_list'] = $lo->getGroupsListOut($portfolio_id,"PORTw");
				$data['group_in_list'] 	= $lo->getGroupsListIn($portfolio_id,"PORTw");

			}else{
				$data['group_out_list'] = $lo->getGroupsList();
			}

			$data['portfolio_info'] = $portfolio_info;

			if($_SESSION["platform"]=="KIS"){
				######### Get Form Selection START #########
				$objYear = new Year();
				$yearList = $objYear->Get_All_Year_List();
				$FormInput = '<select name="formId" style="width:200px">';
				$FormInput .= "<option >{$Lang['General']['PleaseSelect']}</option>";

				foreach($yearList as $index=>$year){
					if($portfolio_info['form_id'] == $year['YearID']){
						$FormInput .= "<option value = '{$year['YearID']}' selected>{$year['YearName']}</option>";
					}else{
						$FormInput .= "<option value = '{$year['YearID']}'>{$year['YearName']}</option>";
					}
				}
				$FormInput .= '</select>';
				######### Get Form Selection END #########
				$data['form_Selection'] = $FormInput;
			}

			include_once('templates/teacher_portfolio_settings.php');
			break;

		case 'setPortfolio':

			$data = array(

			'title' 		=> HTMLtoDB($title),
			'instruction' 		=> HTMLtoDB($instruction),
			'starttime' 		=> (trim($starttime)!="") ? "'".$starttime." $sh:$sm:00'" : "NULL",
			'endtime' 		=> (trim($endtime)!="") ? "'".$endtime." $eh.$em:59'" : "NULL",
			'user_intranet_id' 	=> $UserID,
			'sizeMax' 		=> $sizeMax*1024,
			'status'		=> $status?1:0,
			'form_id'		=> $formId

			);

			if ($portfolio_id){

				if ($is_clone){
					$liblp2 = $liblp2->clonePortfolio($data);
				}else{
					$liblp2->updatePortfolioInfo($data);
				}

			}else{
				$liblp2 = libpf_lp2::createPortfolio($data);
			}

			$liblp2->createPortfolioGroups($groups);

			break;

		case 'setPortfolioStatuses':

			$liblp2->setPortfolioStatuses($status);

			break;

		case 'removePortfolios':

			$liblp2->removePortfolios();

			break;

		case 'getExportDoc':

		    $fs = new libfilesystem();

		    $student = $liblp2->getUserInfo();

		    $export_basedir 	= "/tmp/iPortfolioBurnCD";
		    $export_filename 	= "student_u".$liblp2->user_id."_wp".$portfolio_id.".zip";
		    $export_zipname 	= $student['name']."_".$portfolio_info['title'].".zip";
		    $export_filepath 	= $export_basedir."/student_u".$liblp2->user_id."_wp".$portfolio_id;

		    if(!file_exists($export_basedir)) $fs->folder_new($export_basedir);
		    if(file_exists($export_basedir.'/'.$export_filename)) $fs->file_remove($export_basedir.'/'.$export_filename);

		    $liblp2->exportPortfolio($export_filepath, '');
		    $liblp2->exportPortfolioSharedFiles($export_filepath);

		    $fs->file_zip(basename($export_filepath), $export_basedir.'/'.$export_filename, dirname($export_filepath));

		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.$export_zipname.'"');
		    header('Content-Transfer-Encoding: binary');
		    header('Content-Length: '. filesize($export_basedir.'/'.$export_filename));
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');

		    readfile($export_basedir.'/'.$export_filename);

		    break;
	}
}
if ($liblp2->memberType=='S'){

	$learning_portfolio_config_file = "$eclass_root/files/learning_portfolio_config.txt";
	if(file_exists($learning_portfolio_config_file)){
		$configFile = fopen($learning_portfolio_config_file, "r") or die("Unable to open file!");
		$config = unserialize(fread($configFile,filesize($learning_portfolio_config_file)));
		fclose($configFile);
		$allowViewPeer = ($config[0]==0 || ($config[0]==1 && $config[1]==''))?false:true;
	}else{
		$allowViewPeer = false;
	}

	switch($action){

		case 'getUserPortfolios':

			$keyword 		= trim($keyword);
			$amount_choices	 	= array(10,20,50,100);
			$amount 		= in_array($amount,$amount_choices)||$amount=='all'? $amount: 20;


			list($total, $lps) 	= $liblp2->getUserPortfolios($keyword, $page*$amount, $amount);

			$data['student_lps']	= array();

			foreach ($lps as $lp){

				$lp['key'] 			= libpf_lp2::encryptPortfolioKey( $lp['web_portfolio_id'], $UserID);
				$lp['modified_days'] 	= libpf_lp2::getDaysWord($lp['modified']);
				$lp['published_days'] 	= libpf_lp2::getDaysWord($lp['published']);
				$lp['share_url'] 		= libpf_lp2::getPortfolioUrl($lp['key']);
				$data['student_lps'][]	= $lp;

			}


			$show_sharepeerbtn = !$is_teacher && $ck_current_academic_year_id && !$is_competition && !$is_stem_learning_scheme &&!libpf_lp2::isFromKIS() && $allowViewPeer;
			$max_page 	= $amount == 'all'? 1: ceil($total/$amount);
			$hash 		= $page.'/'.$hash.'/'.$keyword;

			$from 		= $page*$amount+1;
			$to 		= $page*$amount+sizeof($data['student_lps']);

			include_once('templates/student_portfolio_list.php');

			break;

		case 'getFriendPortfolios':

		    if($is_stem_learning_scheme){
		        break;
		    }

			$amount = 'all';

			list($total, $lps) = $liblp2->getFriendPortfolios($page*$amount, $amount);
			$amount = 3;
			$data['friend_lps']=array();

			foreach ($lps as $lp){
				$friend_intranet_id 	= $liblp2->EC_USER_ID_TO_IP_USER_ID($lp['user_id']);
				if(!empty($friend_intranet_id)){
					$lp 			= array_merge($lp, $liblp2->getUserInfo($friend_intranet_id));
					$lp['key'] 			= libpf_lp2::encryptPortfolioKey($lp['web_portfolio_id'], $friend_intranet_id);
					$lp['share_url'] 		= libpf_lp2::getPortfolioUrl($lp['key']);
					$data['friend_lps'][]	= $lp;
				}else{
					$total--;
				}
			}

			$max_page 	= ceil($total/$amount);
			$from 		= $total?$page*$amount+1:0;

			$tmpArr = array();
			for($no=0; $no < count($data['friend_lps']); $no++){
				if($no < $from-1)continue;
				if(count($tmpArr) >= 3) break;
				$tmpArr[] = $data['friend_lps'][$no];
			}
			$data['friend_lps'] = $tmpArr;

			$to 		= $page*$amount+sizeof($data['friend_lps']);

			include_once('templates/student_portfolio_friend_list.php');

			break;

	}

}

/****Owner Action****/
if ($is_owner && $version>=1){//owner
	switch($action){

		case 'setTheme':

			$liblp2->setPortfolioMode('draft');

			$banner_editable 	= $liblp2->hasSuperPermission('banner');

			if (!$banner_uploaded  && $banner_editable){//user disabled banner

				$banner_url 	= "";

			}else if($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]){//user just uploaded a banner

				$banner_url 	= $fm->copy_fck_flash_image_upload($portfolio, $_SESSION['iPortfolio_uploaded_banner_url'][$portfolio], '../../', $cfg['fck_image']['iPortfolio']);

				unset($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]);

			}else{//nothing happened, use old banner

				$banner_url 	= $portfolio_config['banner_url'];
				$banner_status 	= $banner_editable?$banner_status:$portfolio_config['banner_status'];

			}

			$liblp2->updatePortfolioConfig(array(

					'template_selected'	=> $theme,
					'banner_status'		=> $banner_status,
					'custom_root'		=> $custom_root,
					'banner_url'		=> $banner_url

			));

			break;

		case 'getExportDoc':

			$fs = new libfilesystem();

			$student = $liblp2->getUserInfo();

			$export_basedir 	= "/tmp/iPortfolioBurnCD";
			$export_filename 	= "student_u".$liblp2->user_id."_wp".$portfolio_id.".zip";
			$export_zipname 	= $student['name']."_".$portfolio_info['title'].".zip";
			$export_filepath 	= $export_basedir."/student_u".$liblp2->user_id."_wp".$portfolio_id;

			if(!file_exists($export_basedir)) $fs->folder_new($export_basedir);
			if(file_exists($export_basedir.'/'.$export_filename)) $fs->file_remove($export_basedir.'/'.$export_filename);

			$liblp2->exportPortfolio($export_filepath, '');
			$liblp2->exportPortfolioSharedFiles($export_filepath);

			$fs->file_zip(basename($export_filepath), $export_basedir.'/'.$export_filename, dirname($export_filepath));

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$export_zipname.'"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '. filesize($export_basedir.'/'.$export_filename));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');


			readfile($export_basedir.'/'.$export_filename);



			break;

	}
}
if ($is_owner){
	switch($action){
		case 'setTheme':

			if (!in_array($mode, array('prototype','draft'))){
				$liblp2->setPortfolioMode('draft');
			}

			$banner_editable = $liblp2->hasSuperPermission('banner');
			if (!$banner_uploaded  && $banner_editable){//user disabled banner

				$banner_url = "";

			}else if($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]){//user just uploaded a banner

				$banner_url = $fm->copy_fck_flash_image_upload($portfolio, $_SESSION['iPortfolio_uploaded_banner_url'][$portfolio], '../../', $cfg['fck_image']['iPortfolio']);
				unset($_SESSION['iPortfolio_uploaded_banner_url'][$portfolio]);

			}else{//nothing happened, use old banner

				$banner_url = $portfolio_config['banner_url'];
				$banner_status = $banner_editable?$banner_status:$portfolio_config['banner_status'];
			}

			$liblp2->updatePortfolioConfig(array(
					'template_selected'	=> $theme,
					'banner_status'	=> $banner_status,
					'custom_root'	=> $custom_root,
					'banner_url'	=> $banner_url
			));

			break;
		case 'publishPortfolio':

			$liblp2->publishPortfolioDraft($force_status ,$allow_like);
			echo libpf_lp2::getPortfolioUrl($portfolio);
			break;
		case 'getPublish':

			$liblp2->setPortfolioMode('draft');

			$data['elements'] = $liblp2->getAllElements();
			include("templates/publish.php");

			break;





		case 'getThemeSettings':

			if (!in_array($mode, array('prototype','draft'))){
				$liblp2->setPortfolioMode('draft');
			}
			$portfolio_config 	= $liblp2->getPortfolioConfig();
			$title 		= $portfolio_info['title'];
			$themes 		= libpf_lp2::$themes;
			$banner_editable 	= $liblp2->hasSuperPermission('banner');
			$current_theme 	= $portfolio_config['template_selected']? $portfolio_config['template_selected']: $themes[0];
			$banner_path 	= explode('/',$portfolio_config['banner_url']);
			$banner_path 	= (strpos($portfolio_config['banner_url'],'http://')===0 || strpos($portfolio_config['banner_url'],'https://')===0)? $portfolio_config['banner_url']:array_pop($banner_path);

			$data=array(
					'banner_status' 	=> $portfolio_config['banner_status'],
					'banner' 		=> $portfolio_config['banner_url'],
					'banner_filename' 	=> $banner_path,
					'custom_root' 		=> $portfolio_config['custom_root']
			);

			include ("templates/theme_settings.php");
			break;
	}
}
switch($action){

	case 'getShare':

		$student = $liblp2->getUserInfo();
		$portfolio_url = $version>=1?libpf_lp2::getPortfolioUrl($portfolio):'';
		//	    $show_sharepeer = $is_owner && !$is_teacher && $ck_current_academic_year_id && !$is_competition &&!libpf_lp2::isFromKIS();
		//
		//	    if ($show_sharepeer){//no need to get friends list
		//		$data['friends'] = $liblp2->getUserFriends();
		//		$data['classes'] = $liblp2->getClasses();
		//	    }

		include("templates/share.php");
		break;
	case 'getShareFriendsSetting':

		$student = $liblp2->getUserInfo();
		$show_sharepeer = !$is_teacher && $ck_current_academic_year_id && !$is_competition &&!libpf_lp2::isFromKIS();
		if ($show_sharepeer){//no need to get friends list
			$data['friends'] = $liblp2->getUserFriends();
			$data['classes'] = $liblp2->getClasses();
		}
		include("templates/share_all.php");

		break;
	case 'getShareSearchResult':

		$students = $liblp2->getClassStudents($class, $keyword, $exclude);
		foreach ($students as $student){
			echo "<option value='".$student['user_id']."'>".$student['name'].' ('.$student['class'].')</option>';
		}

		break;
	case 'setShareFriends':
		$liblp2->createFriends($friend_ids);
		break;
}

intranet_closedb();

?>