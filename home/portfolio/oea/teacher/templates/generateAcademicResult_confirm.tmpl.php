<?
echo $h_msg_row;
?>
<script language="javascript">
$(document).ready( function() {	
	
});

function js_Go_Back() {
	window.location = 'index.php';
}

function js_Go_Generate() {
	Block_Document();
	js_Disable_Button('GenerateBtn');
	js_Disable_Button('CancelBtn');
	
	$('input#task', 'form#form1').val('ajax');
	$('input#script', 'form#form1').val('ajax_Academic');
	$('input#action', 'form#form1').val('Generate');
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	
	$.ajax({  
		type: 'POST',
		url: 'index.php',
		data: jsSubmitString,  
		success: function(data) {
			if (data != '' && parseInt(data) > 0) {
				window.location = 'index.php?ReturnMsgKey=GenerateSuccess';
			}
			else {
				js_Enable_Button('GenerateBtn');
				js_Enable_Button('CancelBtn');
				UnBlock_Document();
				Get_Return_Message('<?=$Lang['iPortfolio']['OEA']['AcademicArr']['ReturnMsgArr']['GenerateFailed']?>');
				Scroll_To_Top();
			}
		} 
	});
}
</script>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr><td align="center"><?=$h_Disclaimer?></td></tr>
		<tr>
			<td>
				<div class="content_top_tool">
					<div class="Conntent_tool">
						<?=$h_ExportIcon?>
						<?=$h_GenerateAcademicResultIcon?>
					</div>
				</div>
				<br style="clear:both" />
				
				<?=$h_navigation?>
				<br style="clear:both" />
				
				<?=$h_warningTable?>
				<br style="clear:both" />
				
				<?=$h_remarksTable?>
				<br style="clear:both" />
			</td>
		</tr>
		<tr>
			<td>
				<div class="table_board">
					<table class="form_table_v30">
						<tbody>
							<? if ($numOfSettingsWarning > 0) { ?>
								<tr><td style="text-align:center;"><?=$h_settingsWarning?></td></tr>
							<? } else {?>
								<tr>
									<td class="field_title"><?=	$Lang['General']['Class']?></td>
									<td><?=$h_YearClassName?></td>
								</tr>
								<tr>
									<td class="field_title"><?=	$Lang['iPortfolio']['OEA']['AcademicArr']['MappingSettings']?></td>
									<td><?=$h_percentileMappingTable?></td>
								</tr>
							<? } ?>
						</tbody>
					</table>
				</div>
				<br style="clear:both" />
	
				<div class="edit_bottom">
					<p class="spacer"></p>
			        <?=$h_generateBtn?>
			        <?=$h_cancelBtn?>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
	
	<input type='hidden' name='task' id='task' value='' />
	<input type='hidden' name='script' id='script' value='' />
	<input type='hidden' name='action' id='action' value='' />
	<input type='hidden' name='ApplicableYearClassIDList' id='ApplicableYearClassIDList' value='<?=implode(',', (array)$ApplicableYearClassIDArr)?>' />
</form>
<br/>