<?
// using: 

echo $linterface->Include_Cookies_JS_CSS();

echo $h_navigation;
echo $h_msg_row;
echo $h_TopTabMenu;
echo $h_DefineAbilityAttributeArr;
echo $h_DefineAcademicSubjectIDArr;
echo $h_DefineInternalCodeArr;
?>
<script language="javascript">
var jsLang = new Array();
jsLang['ApproveSuccessReturnMsg_OeaItem'] = '<?=$Lang['iPortfolio']['OEA']['OeaItemApproveSuccessfully']?>';
jsLang['ApproveFailedReturnMsg_OeaItem'] = '<?=$Lang['iPortfolio']['OEA']['OeaItemApproveUnsuccessfully']?>';
jsLang['RejectSuccessReturnMsg_OeaItem'] = '<?=$Lang['iPortfolio']['OEA']['OeaItemRejectSuccessfully']?>';
jsLang['RejectFailedReturnMsg_OeaItem'] = '<?=$Lang['iPortfolio']['OEA']['OeaItemRejectUnsuccessfully']?>';

jsLang['ApproveSuccessReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoApproveSuccessfully']?>';
jsLang['ApproveFailedReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoApproveUnsuccessfully']?>';
jsLang['RejectSuccessReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoRejectSuccessfully']?>';
jsLang['RejectFailedReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoRejectUnsuccessfully']?>';
jsLang['SaveSuccessReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoSaveSuccessfully']?>';
jsLang['SaveFailedReturnMsg_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['AddiInfoSaveUnsuccessfully']?>';

jsLang['ApproveSuccessReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilityApproveSuccessfully']?>';
jsLang['ApproveFailedReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilityApproveUnsuccessfully']?>';
jsLang['RejectSuccessReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilityRejectSuccessfully']?>';
jsLang['RejectFailedReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilityRejectUnsuccessfully']?>';
jsLang['SaveSuccessReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilitySaveSuccessfully']?>';
jsLang['SaveFailedReturnMsg_Ability'] = '<?=$Lang['iPortfolio']['OEA']['AbilitySaveUnsuccessfully']?>';

jsLang['ApproveSuccessReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicApproveSuccessfully']?>';
jsLang['ApproveFailedReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicApproveUnsuccessfully']?>';
jsLang['RejectSuccessReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicRejectSuccessfully']?>';
jsLang['RejectFailedReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicRejectUnsuccessfully']?>';
jsLang['SaveSuccessReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicSaveSuccessfully']?>';
jsLang['SaveFailedReturnMsg_Academic'] = '<?=$Lang['iPortfolio']['OEA']['AcademicSaveUnSuccessfully']?>';

jsLang['ApproveSuccessReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoApproveSuccessfully']?>';
jsLang['ApproveFailedReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoApproveUnsuccessfully']?>';
jsLang['RejectSuccessReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoRejectSuccessfully']?>';
jsLang['RejectFailedReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoRejectUnsuccessfully']?>';
jsLang['SaveSuccessReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoSaveSuccessfully']?>';
jsLang['SaveFailedReturnMsg_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['SuppInfoSaveUnsuccessfully']?>';

jsLang['jsWarning_InputContent_AddiInfo'] = '<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Title']?>';
jsLang['jsWarning_InputContent_SuppInfo'] = '<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_Input_Title']?>';

jsLang['Enabled'] = '<?=$Lang['General']['Enabled']?>';
jsLang['Disabled'] = '<?=$Lang['General']['Disabled']?>';


var jsApprovedStatusArr = new Array();
jsApprovedStatusArr['AddiInfo'] = '<?=$h_AddiInfoApprovedStatus?>';
jsApprovedStatusArr['Ability'] = '<?=$h_AbilityApprovedStatus?>';
jsApprovedStatusArr['Academic'] = '<?=$h_AcademicApprovedStatus?>';
jsApprovedStatusArr['SuppInfo'] = '<?=$h_SuppInfoApprovedStatus?>';

var jsRejectedStatusArr = new Array();
jsRejectedStatusArr['AddiInfo'] = '<?=$h_AddiInfoRejectedStatus?>';
jsRejectedStatusArr['Ability'] = '<?=$h_AbilityRejectedStatus?>';
jsRejectedStatusArr['Academic'] = '<?=$h_AcademicRejectedStatus?>';
jsRejectedStatusArr['SuppInfo'] = '<?=$h_SuppInfoRejectedStatus?>';


var jsMaxWordCountArr = new Array();
jsMaxWordCountArr['AddiInfo'] = '<?=liboea_additional_info::getDetailsMaxWordCount()?>';
jsMaxWordCountArr['SuppInfo'] = '<?=liboea_supplementary_info::getDetailsMaxWordCount()?>';

var jsMaxCharCountArr = new Array();
jsMaxCharCountArr['AddiInfo'] = '<?=liboea_additional_info::getDetailsMaxLength()?>';
jsMaxCharCountArr['SuppInfo'] = '<?=liboea_supplementary_info::getDetailsMaxLength()?>';
jsMaxCharCountArr['Description'] = '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxLength"]?>';


var jsAcademicAutoMappingEnabled;
var jsAcademicMappingArr = new Array();
<? foreach ((array)$AcademicMappingArr as $thisPercentile => $thisOverallRating) { ?> 
		jsAcademicMappingArr['<?=$thisPercentile?>'] = '<?=$thisOverallRating?>';
<? } ?>
var jsAcademicNotJudgePercentile = <?=$oea_cfg["Academic"]["PercentileUnabletoJudge"]?>;
var jsAcademicNotJudgeOverallRating = '<?=$oea_cfg["Academic"]["OverallRatingUnabletoJudge"]?>';
var arrCookies = new Array();
var jsNumOfInternalCode = jsInternalCodeArr.length;
var i;
for (i=0; i<jsNumOfInternalCode; i++) {
	var jsThisInternalCode = jsInternalCodeArr[i];
	arrCookies[arrCookies.length] = "iPf_Jupas_TeacherView_DefaultHide_" + jsThisInternalCode;
}
arrCookies[arrCookies.length] = "iPf_Jupas_TeacherView_Academic_Auto_Mapping_Status";


$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	
	for (i=0; i<jsNumOfInternalCode; i++) {
		var jsThisInternalCode = jsInternalCodeArr[i];
		if ($.cookies.get('iPf_Jupas_TeacherView_DefaultHide_' + jsThisInternalCode) == '1') {
			js_Hide_Option_Div('SectionDiv_' + jsThisInternalCode);
		}
	}
	
	jsAcademicAutoMappingEnabled = $.cookies.get('iPf_Jupas_TeacherView_Academic_Auto_Mapping_Status');
	jsAcademicAutoMappingEnabled = (jsAcademicAutoMappingEnabled=='' || jsAcademicAutoMappingEnabled==null)? '1' : jsAcademicAutoMappingEnabled;
	
	//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxWordCountArr['AddiInfo']);
	//js_Update_Text_Counter(strip_tags($('div#SuppInfoDetailsDiv').html()), 'SuppInfo_WordCountSpan', jsMaxWordCountArr['SuppInfo']);
	//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxCharCountArr['AddiInfo'], 'char');
	//js_Update_Text_Counter(strip_tags($('div#SuppInfoDetailsDiv').html()), 'SuppInfo_WordCountSpan', jsMaxCharCountArr['SuppInfo'], 'char');
	//js_Update_Text_Counter(strip_tags($('textarea#AddiInfoDetailsTextarea').val()), 'AddiInfo_WordCountSpan', jsMaxCharCountArr['AddiInfo'], 'char');
	//count word:
	js_Update_Text_Counter(strip_tags($('textarea#AddiInfoDetailsTextarea').val()), 'AddiInfo_WordCountSpan', jsMaxCharCountArr['AddiInfo'], '');
	js_Update_Text_Counter(strip_tags($('textarea#SuppInfoDetailsTextarea').val()), 'SuppInfo_WordCountSpan', jsMaxCharCountArr['SuppInfo'], 'char');
});

function js_Back_To_Index() {
	window.location = 'index.php';
}

function js_Reload_Page(jsStudentID) {
	for (i=0; i<jsNumOfInternalCode; i++) {
		var jsThisInternalCode = jsInternalCodeArr[i];
		if ($('#SectionDiv_' + jsThisInternalCode).is(':visible')) {
			$.cookies.set('iPf_Jupas_TeacherView_DefaultHide_' + jsThisInternalCode, '0');
		}
		else {
			$.cookies.set('iPf_Jupas_TeacherView_DefaultHide_' + jsThisInternalCode, '1');
		}
	}
	
	window.location = 'index.php?task=listStuAllInfo&StudentID=' + jsStudentID;
}


function js_Changed_Student_Selection(jsStudentID) {
	js_Reload_Page(jsStudentID);
}

function js_Pressed_Info_Textarea(jsObjID, jsDisplaySpanID)
{
	var jsDisplaySpanIDArr = jsDisplaySpanID.split('_');
	var jsObjType = jsDisplaySpanIDArr[0];
	var jsMaxWordCount = jsMaxWordCountArr[jsObjType];
	var jsMaxCharCount = jsMaxCharCountArr[jsObjType];
	
	if(jsDisplaySpanID=='AddiInfo_WordCountSpan'){
		js_Onkeyup_Info_Textarea(jsObjID, jsDisplaySpanID, jsMaxWordCount, jsMaxCharCount, '');
	}
	else{
		js_Onkeyup_Info_Textarea(jsObjID, jsDisplaySpanID, jsMaxWordCount, jsMaxCharCount, 'char');
	}
}

function js_Reload_Content_Table(jsTypeCode, jsObjectIDFieldName, jsTargetMode)
{
	var jsAction = 'Load_' + jsTargetMode + '_Mode';
	var jsObjectID = (jsObjectIDFieldName=='')? '' : $('input#' + jsObjectIDFieldName).val();
	var jsTargetDivID = 'ContentTableDiv_' + jsTypeCode;
	var jsSectionOutestDivID = 'SectionOutestDiv_' + jsTypeCode;
	
	$('div#' + jsTargetDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_' + jsTypeCode,
			action: jsAction,
			StudentID: $('select#StudentID').val(),
			ObjectID: jsObjectID
		},
		function(ReturnData)
		{
			if (jsTargetMode == 'Edit')
			{
				$('input#Btn_Save_And_Approve').attr('disabled', false);
				$('input#Btn_Save').attr('disabled', false);
				$('input#Btn_Cancel').attr('disabled', false);
	
				if (jsTypeCode == 'AddiInfo') {
					$('textarea#AddiInfoDetails').focus();
					js_Pressed_Info_Textarea('AddiInfoDetails', 'AddiInfo_WordCountSpan');
				}
				else if (jsTypeCode == 'SuppInfo') {
					$('textarea#SuppInfoDetails').focus();
					js_Pressed_Info_Textarea('SuppInfoDetails', 'SuppInfo_WordCountSpan');
				}
				else if (jsTypeCode == 'Academic') {
					js_Update_Academic_Auto_Mapping_Status(jsAcademicAutoMappingEnabled);
				}
				
				$('div#BottomButtonDiv_' + jsTypeCode).show();
				$('div#ActionBtnDiv_' + jsTypeCode).hide();
			}
			else if (jsTargetMode == 'View')
			{
				if (jsTypeCode == 'AddiInfo') {
					//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxWordCountArr['AddiInfo']);
					//js_Update_Text_Counter(strip_tags($('div#AddiInfoDetailsDiv').html()), 'AddiInfo_WordCountSpan', jsMaxCharCountArr['AddiInfo'], 'char');
					js_Update_Text_Counter(strip_tags($('textarea#AddiInfoDetailsTextarea').val()), 'AddiInfo_WordCountSpan', jsMaxCharCountArr['AddiInfo'], '');
				}
				else if (jsTypeCode == 'SuppInfo') {
					//js_Update_Text_Counter(strip_tags($('div#SuppInfoDetailsDiv').html()), 'SuppInfo_WordCountSpan', jsMaxWordCountArr['SuppInfo']);
					//js_Update_Text_Counter(strip_tags($('div#SuppInfoDetailsDiv').html()), 'SuppInfo_WordCountSpan', jsMaxCharCountArr['SuppInfo'], 'char');
					js_Update_Text_Counter(strip_tags($('textarea#SuppInfoDetailsTextarea').val()), 'SuppInfo_WordCountSpan', jsMaxCharCountArr['SuppInfo'], 'char');
				}
				
				$('div#BottomButtonDiv_' + jsTypeCode).hide();
				$('div#ActionBtnDiv_' + jsTypeCode).show();
			}
			
			Scroll_To_Element(jsSectionOutestDivID);
		}
	);
}

function js_Reload_Status_Display(jsTypeCode, jsObjectIDFieldName)
{
	$('span#ApprovalStatusSpan_' + jsTypeCode).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_' + jsTypeCode,
			action: 'Load_Approval_Status_Display',
			ObjectID: $('input#' + jsObjectIDFieldName).val()
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Save_Info(jsTypeCode, jsObjectIDFieldName, jsApproveRecord)
{
	if (jsTypeCode == '<?=$liboea->getAddiInfoInternalCode()?>' || jsTypeCode == '<?=$liboea->getSuppInfoInternalCode()?>')
	{
//		if (Trim($('input#AddiInfoTitle').val()) == '')
//		{
//			alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Input_Title']?>');
//			$('input#AddiInfoTitle').focus();
//			return false;
//		}

		var jsInfoText = Trim($('textarea#AddiInfoDetails').val());
		
		if (jsInfoText == '')
		{
			alert(jsLang['jsWarning_InputContent_' + jsTypeCode]);
			$('textarea#' + jsTypeCode + 'Details').focus();
			return false;
		}
		
		// Check character count -> cannot submit if exceeded limit
		var jsCharCount = parseInt(js_Get_Jupas_Character_Count(jsInfoText));
		var jsMaxCharCount = parseInt(jsMaxCharCountArr[jsTypeCode]);
		if (jsMaxCharCount > 0 && jsCharCount > jsMaxCharCount) {
			var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max']?>', jsMaxCharCount);
			alert(jsWarningMsg);
			$('textarea#' + jsTypeCode + 'Details').focus();
			return false;
		}
		
		// Check word count -> still can save if the user confirm even if exceeded limit
		var jsWordCount = parseInt(js_Get_Word_Count(jsInfoText));
		var jsMaxWordCount = parseInt(jsMaxWordCountArr[jsTypeCode]);
		if (jsMaxWordCount > 0 && jsWordCount > jsMaxWordCount) {
			var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_Greater_Than_The_Max_Confirm']?>', jsMaxWordCount);
			if (confirm(jsWarningMsg)) {
				// continue the submit process
			}
			else {
				$('textarea#' + jsTypeCode + 'Details').focus();
				return false;
			}
		}
	}
	if(jsTypeCode == '<?=$liboea->getSuppInfoInternalCode()?>'){
		var jsSuppInfoText = Trim($('textarea#SuppInfoDetails').val());
		if(js_isEnglishInputOnly(jsSuppInfoText)){
			//do nothing
		}else{
			var answer = confirm("<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Sup_Info_Eng_Only']?>");
			if(answer){
				//do noting
			}else{
				return false;
			}
		}
	}
	if (jsTypeCode == '<?=$liboea->getAbilityInternalCode()?>')
	{
		var i;
		var jsThisAttribute;
		var jsNumOfAttribute = jsAbilityAttributeArr.length;
		
		for (i=0; i<jsNumOfAttribute; i++)
		{
			jsThisAttribute = jsAbilityAttributeArr[i];
			
			if ($('input[name="AbilityRadio_' + jsThisAttribute + '"]:checked').val() == null) {
				alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_SelectRating']?>');
				$('input[name="AbilityRadio_' + jsThisAttribute + '"]:first').focus();
				return false;
			}
		}
	}
	
	if (jsTypeCode == '<?=$liboea->getAcademicInternalCode()?>')
	{
		var i, jsThisSubjectID;
		var jsNumOfSubject = jsAcademicSubjectIDArr.length;
		
		for (i=0; i<jsNumOfSubject; i++)
		{
			jsThisSubjectID = jsAcademicSubjectIDArr[i];
			
			if ($('input[name="SubjectPercentileAssoArr\\[' + jsThisSubjectID + '\\]"]:checked').val() == null) {
				alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectPercentile']?>');
				$('input[name="SubjectPercentileAssoArr\\[' + jsThisSubjectID + '\\]"]:first').focus();
				return false;
			}
			
			if ($('input[name="SubjectOverallRatingAssoArr\\[' + jsThisSubjectID + '\\]"]:checked').val() == null) {
				alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Academic_SelectOverallRating']?>');
				$('input[name="SubjectOverallRatingAssoArr\\[' + jsThisSubjectID + '\\]"]:first').focus();
				return false;
			}
		}
			
		if ($('#APpercentileRemarks').is(':visible')){
			wordCount = js_Get_Jupas_Character_Count($('#APpercentileRemarks').val(),2);	
			if($('#APpercentileRemarks').val()==='<?=$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg']?>'||wordCount==0||wordCount>800){
				if($('#APpercentileRemarks').val()==='<?=$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg']?>'||wordCount==0){
					alert('<?=$Lang['iPortfolio']['OEA']['PercentileEmptyMsg']?>');
				}else{
					alert('<?=$Lang['iPortfolio']['OEA']['PercentileExceedMsg']?>');
				}
				return false;
			}
		}else{
			$('#APpercentileRemarks').val('');
		}
		
		if ($('#APratingRemarks').is(':visible')){
			wordCount = js_Get_Jupas_Character_Count($('#APratingRemarks').val(),2);
			if($('#APratingRemarks').val()==='<?=$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg']?>'||wordCount==0||wordCount>800){
				if($('#APratingRemarks').val()==='<?=$Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg']?>'||wordCount==0){
					alert('<?=$Lang['iPortfolio']['OEA']['OverallRatingEmptyMsg']?>');
				}else{
					alert('<?=$Lang['iPortfolio']['OEA']['OverallRatingExceedMsg']?>');
				}
				return false;
			}
		}else{
			$('#APratingRemarks').val('');
		}
	}
	
	// disable the buttons
	$('input#Btn_Save_And_Approve').attr('disabled', true);
	$('input#Btn_Save').attr('disabled', true);
	$('input#Btn_Cancel').attr('disabled', true);
	
	$('input#task').val('ajax');
	$('input#script').val('ajax_' + jsTypeCode);
	$('input#action').val('Save');
	$('input#ObjectID').val($('input#' + jsObjectIDFieldName).val());
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	
	$.ajax({  
		type: "POST",  
		url: "index.php",
		data: jsSubmitString,  
		success: function(data) {
			var jsReturnMsgKey = '';
			
			if (data=='' || data==0) {
				jsReturnMsg = jsLang['SaveFailedReturnMsg_' + jsTypeCode];
			} else {
				jsReturnMsg = jsLang['SaveSuccessReturnMsg_' + jsTypeCode];
				$('input#' + jsObjectIDFieldName).val(data);	// store the updated RecordID in the hidden field
			}
			
			$('div#BtnDiv_' + jsTypeCode).hide();
			$('div#ActionBtnDiv_' + jsTypeCode).show();
			
			var jsReturnMsgDivID = 'ReturnMsgDiv_' + jsTypeCode;
			Scroll_To_Element(jsReturnMsgDivID);
			Get_Return_Message(jsReturnMsg, jsReturnMsgDivID);
			js_Reload_Content_Table(jsTypeCode, jsObjectIDFieldName, 'View');
			
			if (jsApproveRecord == 1) {
				js_Change_Approval_Status(jsTypeCode, jsObjectIDFieldName, jsApprovedStatusArr[jsTypeCode]);
			}
		} 
	});  
	return false;
}

function js_isEnglishInputOnly(str){   

		var isEnglish=true;
        for(i=0;i<str.length;i++) {
            ecStr=str.substr(i,1);
            ascStr=ecStr.charCodeAt();
            if(ascStr>127) { 
				isEnglish=false;   
				break;
            }
        } 
    
	return isEnglish; 
}  
function js_Change_Approval_Status(jsTypeCode, jsObjectIDFieldName, jsParTargetStatus)
{
	var jsObjectID = $('input#' + jsObjectIDFieldName).val();
	
	// Cannot approve if there is no record
	if (jsTypeCode == '<?=$liboea->getAddiInfoInternalCode()?>' && jsObjectID == '')
	{
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AddiInfo_CompleteRatingFirst']?>');
		return;
	}	
	if (jsTypeCode == '<?=$liboea->getAbilityInternalCode()?>' && jsObjectID == '')
	{
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Ability_CompleteRatingFirst']?>');
		return;
	}	
	if (jsTypeCode == '<?=$liboea->getSuppInfoInternalCode()?>' && jsObjectID == '')
	{
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['SuppInfo_CompleteRatingFirst']?>');
		return;
	}

	// Get Target RecordStatus
	var jsRecordStatus = $('input#RecordStatus_' + jsTypeCode).val();
		
	var jsTargetRecordStatus;
	var jsTargetAction;
	
	if (jsParTargetStatus != null)
	{
		if (jsParTargetStatus == jsApprovedStatusArr[jsTypeCode]) {
			// approve
			jsTargetRecordStatus = jsApprovedStatusArr[jsTypeCode];
			jsTargetAction = 'Approve';
		}
		else {
			// reject
			jsTargetRecordStatus = jsRejectedStatusArr[jsTypeCode];
			jsTargetAction = 'Reject';
		}
	}
	else
	{
		if (jsRecordStatus == jsApprovedStatusArr[jsTypeCode]) {
			// approve -> reject
			jsTargetRecordStatus = jsRejectedStatusArr[jsTypeCode];
			jsTargetAction = 'Reject';
		}
		else {
			// reject -> approve
			jsTargetRecordStatus = jsApprovedStatusArr[jsTypeCode];
			jsTargetAction = 'Approve';
		}
	}
	
	// Process the Approval or Rejection	
	var jsSectionDivID = 'SectionDiv_' + jsTypeCode;
	Block_Element(jsSectionDivID);
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: 'ajax_' + jsTypeCode,
			action: jsTargetAction,
			ObjectID: jsObjectID,
			StudentID: $('select#StudentID').val()
		},
		function(jsSuccess)
		{
			// Show Return Message, Change Status Display and the Top-Right Button accordingly
			var jsReturnMsg = '';
			
			if (jsSuccess == '1')
			{
				if (jsTargetRecordStatus == jsApprovedStatusArr[jsTypeCode]) {
					jsReturnMsg = jsLang['ApproveSuccessReturnMsg_' + jsTypeCode];
					js_Reload_Status_Display(jsTypeCode, jsObjectIDFieldName);
					//$('a#ApprovalBtnDiv_' + jsTypeCode).removeClass('tool_approve').addClass('tool_reject').html('<?=$Lang['Btn']['Reject']?>');
					
					$('a#ApprovalBtnDiv_' + jsTypeCode + '.tool_approve').hide();
					$('a#ApprovalBtnDiv_' + jsTypeCode + '.tool_reject').show();
				}
				else {
					jsReturnMsg = jsLang['RejectSuccessReturnMsg_' + jsTypeCode];
					js_Reload_Status_Display(jsTypeCode, jsObjectIDFieldName);
					//$('a#ApprovalBtnDiv_' + jsTypeCode).removeClass('tool_reject').addClass('tool_approve').html('<?=$Lang['Btn']['Approve']?>');
					
					$('a#ApprovalBtnDiv_' + jsTypeCode + '.tool_reject').hide();
					$('a#ApprovalBtnDiv_' + jsTypeCode + '.tool_approve').show();
				}
				
				// Update the Approval Status in the hidden field
				$('input#RecordStatus_' + jsTypeCode).val(jsTargetRecordStatus);
			}
			else
			{
				if (jsTargetRecordStatus == jsApprovedStatusArr[jsTypeCode]) {
					jsReturnMsg = jsLang['ApproveFailedReturnMsg_' + jsTypeCode];
				}
				else {
					jsReturnMsg = jsLang['RejectFailedReturnMsg_' + jsTypeCode];
				}
			}
			
			UnBlock_Element(jsSectionDivID);
			
			var jsReturnMsgDivID = 'ReturnMsgDiv_' + jsTypeCode;
			Scroll_To_Element(jsReturnMsgDivID);
			Get_Return_Message(jsReturnMsg, jsReturnMsgDivID);
		}
	);
}

function js_Approve_Reject_Oea_Item(jsAction, jsTypeCode, jsParticipationType)
{
	var jsCheckboxClass = 'OEA_RecordIDChk_' + jsParticipationType;
	if ($('input.' + jsCheckboxClass + ':checked').length == 0) {
		alert('<?=$Lang['iPortfolio']['OEA']['PleaseSelectOEAItem']?>');
	}
	else {
		var jsRecordIDList = Get_Checkbox_Value_By_Class(jsCheckboxClass);
		
		var jsSectionDivID = 'SectionDiv_' + jsTypeCode;
		Block_Element(jsSectionDivID);
		$.post(
			"index.php", 
			{ 
				task: 'ajax',
				script: 'ajax_' + jsTypeCode,
				action: jsAction,
				OEA_RecordIDList: jsRecordIDList
			},
			function(jsSuccess)
			{
				// Show Return Message, Change Status Display and the Top-Right Button accordingly
				var jsReturnMsg = '';
				
				if (jsSuccess == '1') {
					if (jsAction == 'Approve') {
						jsReturnMsg = jsLang['ApproveSuccessReturnMsg_' + jsTypeCode];
					}
					else if (jsAction == 'Reject') {
						jsReturnMsg = jsLang['RejectSuccessReturnMsg_' + jsTypeCode];
					}
					js_Reload_Content_Table(jsTypeCode, '', 'View');
				}
				else {
					if (jsAction == 'Approve') {
						jsReturnMsg = jsLang['ApproveUnsuccessReturnMsg_' + jsTypeCode];
					}
					else if (jsAction == 'Reject') {
						jsReturnMsg = jsLang['RejectUnsuccessReturnMsg_' + jsTypeCode];
					}
				}
				UnBlock_Element(jsSectionDivID);
				
				var jsReturnMsgDivID = 'ReturnMsgDiv_' + jsTypeCode;
				Scroll_To_Element(jsReturnMsgDivID);
				Get_Return_Message(jsReturnMsg, jsReturnMsgDivID);
			}
		);
	}
}

function js_Clicked_Radio_Td(jsRadioID) {
	$('input#' + jsRadioID).click();
}

function js_Clicked_Academic_Percentile_Radio(jsSubjectID, jsPercentileCode) {
	if (jsAcademicAutoMappingEnabled == '1') {
		var jsTargetOverallRating = jsAcademicMappingArr[jsPercentileCode];
		if (jsTargetOverallRating != '' && jsTargetOverallRating != null) {
			var jsTargetOverallRatingRadioID = 'OverallRatingRadio_' + jsSubjectID + '_' + jsTargetOverallRating;
			$('input#' + jsTargetOverallRatingRadioID).attr('checked', true);
		}
	}
	js_Clicked_Academic_Remarks(jsSubjectID, jsPercentileCode);
}
function js_Clicked_Academic_Remarks(jsSubjectID, jsAPCode) {
	pRemarksshow = 0;
	rRemarksshow = 0;
	if($('input[id^=PercentileRadio][id$=_'+jsAcademicNotJudgePercentile+']:not([id*=ApplyAll]):checked').length){
		pRemarksshow = 1;
	}
	if($('input[id^=OverallRatingRadio][id$=_'+jsAcademicNotJudgeOverallRating+']:not([id*=ApplyAll]):checked').length){
		rRemarksshow = 1;
	}
	if($('#APRemarksRow').is(':hidden')){
		if(pRemarksshow){
			$('#APpercentileRemarks').show();
		}
		if(rRemarksshow){
			$('#APratingRemarks').show();
		}
		if(pRemarksshow||rRemarksshow){
			$('#APRemarksRow').fadeIn(360);
		}
	}else{
		if(!pRemarksshow&&!rRemarksshow){
			$('#APRemarksRow').fadeOut(240,function(){
				$('#APpercentileRemarks').hide();
			$('#APratingRemarks').hide();
			});
		}else{
			if(pRemarksshow&&$('#APpercentileRemarks').is(':hidden')){
				$('#APpercentileRemarks').fadeIn(360);
			}else if(!pRemarksshow&&$('#APpercentileRemarks').is(':visible')){
				$('#APpercentileRemarks').fadeOut(240);
			}
			if(rRemarksshow&&$('#APratingRemarks').is(':hidden')){
				$('#APratingRemarks').fadeIn(360);
			}else if(!rRemarksshow&&$('#APratingRemarks').is(':visible')){
				$('#APratingRemarks').fadeOut(240);
			}
		}
	}
}

function js_Clicked_Academic_Percentile_Select_All_Radio(jsClass) {
	$('input.' + jsClass, 'div#ContentTableDiv_Academic').click();
}

function js_Switch_Academic_Auto_Mapping_Status() {
	if (jsAcademicAutoMappingEnabled == '1') {
		// active -> inactive
		js_Update_Academic_Auto_Mapping_Status('0');
	}
	else {
		// inactive -> active
		js_Update_Academic_Auto_Mapping_Status('1');
	}
}

function js_Update_Academic_Auto_Mapping_Status(jsTargetStatus) {
	if (jsTargetStatus == '1') {
		// inactive -> active
		$('a#AcademicAutoMappingIcon').attr('class', 'active_item').attr('title', jsLang['Enabled']);
		$('span#AcademicAutoMappingSpan').removeClass('tabletextremark');
		jsAcademicAutoMappingEnabled = '1';
	}
	else {
		// active -> inactive
		$('a#AcademicAutoMappingIcon').attr('class', 'inactive_item').attr('title', jsLang['Disabled']);
		$('span#AcademicAutoMappingSpan').addClass('tabletextremark');
		jsAcademicAutoMappingEnabled = '0';
	}
	$.cookies.set('iPf_Jupas_TeacherView_Academic_Auto_Mapping_Status', jsAcademicAutoMappingEnabled);
}

function js_Generate_Academic() {
	var jsSectionDivID = 'SectionDiv_Academic';
	Block_Element(jsSectionDivID);
		
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: 'ajax_Academic',
			action: 'Generate_For_Preset_Radio',
			StudentID: $('select#StudentID').val()
		},
		function(jsData)
		{
			// $objAcademic->convertObjectSubjectPercentileToDBValue.'|||'.$objAcademic->convertObjectSubjectOverallRatingToDBValue();
			
			var jsDataArr = jsData.split('|||');	// separate Percentile and Overall Rating data
			var jsNumOfData = jsDataArr.length;
			var i, j;
			
			$('input.AcademicRadio').attr('checked', '');
			
			for (i=0; i<jsNumOfData; i++) {
				var jsTypeData = jsDataArr[i];		// separate data of each Type of Item
				var jsTypeDataArr = jsTypeData.split('##');
				var jsNumOfTypeData = jsTypeDataArr.length;
				
				var jsThisRadioIDPrefix = '';
				if (i == 0) {
					jsThisRadioIDPrefix = 'PercentileRadio';
				}
				else if (i == 1) {
					jsThisRadioIDPrefix = 'OverallRatingRadio';
					
					if (jsAcademicAutoMappingEnabled == '0') {
						continue;
					}
				}
				
				for (j=0; j<jsNumOfTypeData; j++) {
					var jsItemData = jsTypeDataArr[j];		// separate data of the Item to get the SubjectID and Radio Value
					var jsItemDataArr = jsItemData.split('==');
					var jsThisSubjectID = jsItemDataArr[0];
					var jsThisItemInternalCode = jsItemDataArr[1];
					
					$('input#' + jsThisRadioIDPrefix + '_' + jsThisSubjectID + '_' + jsThisItemInternalCode).click();
				}
			}
			
			// remove not selected subject
			$('tr.AcademicTr').each( function() {
				var jsThisSubjectID = $(this).attr('subjectId');
				
				var jsNumOfSelectedPercentile = $('input.PercentileRadio_SubjectID_' + jsThisSubjectID + ':checked').length;
				var jsNumOfSelectedOverallRating = $('input.OverallRatingRadio_SubjectID_' + jsThisSubjectID + ':checked').length;
				
				if (jsNumOfSelectedPercentile == 0 && jsNumOfSelectedOverallRating == 0) {
					$(this).remove();
					$('input.PercentileRadio_SubjectID_' + jsThisSubjectID).remove();
					$('input.OverallRatingRadio_SubjectID_' + jsThisSubjectID).remove();
					jsAcademicSubjectIDArr.splice(jsAcademicSubjectIDArr.indexOf(jsThisSubjectID), 1);
				}
			});
			
			UnBlock_Element(jsSectionDivID);
		}
	);
}

function js_Show_Academic_Mapping_Details_Layer() {
	//MM_showHideLayers('AutoMappingDetailsDiv', '', 'show');
	$('div#AutoMappingDetailsDiv').show();
	js_Change_Layer_Position('AutoMappingDetailsDiv', 'AllowClientProgramConnectDiv', 0, -20);
}

function js_Hide_Academic_Mapping_Details_Layer() {
	//MM_showHideLayers('AutoMappingDetailsDiv', '', 'hide');
	$('div#AutoMappingDetailsDiv').hide();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsLayerDivID, jsClickedObjID, jsOffsetLeft, jsOffsetTop) 
{		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById(jsLayerDivID).style.left = posleft + "px";
	document.getElementById(jsLayerDivID).style.top = postop + "px";
	document.getElementById(jsLayerDivID).style.visibility = 'visible';
}






function js_Edit_Oea_Item(jsParticipationType) {
	var jsCheckboxClass = 'OEA_RecordIDChk_' + jsParticipationType;
	
	if ($('input.' + jsCheckboxClass + ':checked').length != 1) {
		alert(globalAlertMsg1);
	} else {
		var selected_record = Get_Checkbox_Value_By_Class(jsCheckboxClass);
		$('#item_'+selected_record).click();
	}
}

function js_edit_action(r_id) {	
	
	$.post(
		"index.php", 
		{ 
			recordID: r_id,
			task: 'ajax',
			script: 'ajax_edit_student_oea'
			
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			js_Onkeyup_Info_Textarea('Description', 'DescCharCountSpan', '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxWordCount"]?>', '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxLength"]?>', 'char');
			//document.getElementById('tempDisplay').innerHTML = ReturnData;	
		}
	);	
}

function doSave() {
	$('.formbutton_v30').attr('disabled', 'disabled');
	
	var success = checkForm();
	if(success) {
		$('input#task', 'form#form11').val('ajax');
		$('input#script', 'form#form11').val('ajax_validate');
		$('input#action', 'form#form11').val('StudentAppliedOea');
		
		var PostString = Get_Form_Values(document.getElementById("form11")) + '&fromEdit=1';
		var PostVar = PostString;
		
		$.post(
			'index.php',
			PostVar,
			function(data){
				var jsCanSubmit = true;
				if (data == '1') {
					if (!confirm('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AppliedOeaAlready']?>')) {
						jsCanSubmit = false;
					}
				}
				
				if (jsCanSubmit) {
					$('input#task').val('ajax');
					
					$('input#task', 'form#form11').val('ajax');
					$('input#script', 'form#form11').val('ajax_submit_oea_student_item');
					$('input#action', 'form#form11').val('');
		
					var PostString = Get_Form_Values(document.getElementById("form11"));
					var PostVar = PostString;
				
					var jsSectionDivID = 'SectionDiv_OeaItem';
					Block_Element(jsSectionDivID);		
					
					$.post('index.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								var jsReturnMsgDivID = 'ReturnMsgDiv_OeaItem';
								//Scroll_To_Element(jsReturnMsgDivID);
								Get_Return_Message(data, jsReturnMsgDivID);
								
								//document.getElementById('tempDisplay').innerHTML = data;
								js_Reload_Content_Table('OeaItem', '', 'View');
							}
						}
					);
					window.top.tb_remove()
					UnBlock_Element(jsSectionDivID);	
				}
				else {
					$('.formbutton_v30').attr('disabled', '');
				}
			}
		);
	}
	else {
		$('.formbutton_v30').attr('disabled', '');
	}
}

function checkForm() {
	var obj = document.form11;
	
	if(obj.ItemCode.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Title']?>");
		//obj.ItemCode.focus();
		return false;	
	}	
	else if(obj.startdate.value=="" || isNaN(obj.startdate.value) || obj.startdate.value.length!=4) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_StartDate']?>");
		obj.startdate.focus();
		return false;
	}
	else if(obj.enddate.value=="" || isNaN(obj.enddate.value) || obj.enddate.value.length!=4) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_EndDate']?>");
		obj.enddate.focus();
		return false;
	}
	else if(obj.startdate.value>obj.enddate.value) {
		alert("<?=$Lang['ePOS']['jsWarningArr']['StartDateCannotGreaterThanEndDate']?>");
		obj.startdate.focus();
		return false;		
	}
	else if(obj.RoleID.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Role']?>");
		obj.RoleID.focus();
		return false;				
	}
	else if(obj.ParticipationNature.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature']?>");
		obj.ParticipationNature.focus();
		return false;						
	}
	<? /* if($defaultAwardBearing) { */ ?>
	else if(obj.AchievementID.value!="" && obj.AchievementID.value!="N" && obj.ParticipationNature.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature']?>");
		obj.ParticipationNature.focus();
		return false;						
	}
	//else if(obj.awardBearing[0].checked==true && obj.AchievementID.value=="") {
	else if(($('input#awardBearing').val()=='Y' || $('input#awardBearing1').attr('checked')==true) && obj.AchievementID.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Achievement']?>");
		obj.AchievementID.focus();
		return false;				
	}
	<? /* } */ ?>
	//else if(obj.Participation[0].checked==false && obj.Participation[1].checked==false) {
	else if(($('input#Participation').val=='') || ($('input#Participation_S').attr('checked')==false && $('input#Participation_P').attr('checked')==false)) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Participation']?>");
		//obj.Participation[0].focus();
		$('input#Participation_S').focus();
		return false;						
	}
	else if (obj.ItemCode.value=='<?=$oea_cfg["OEA_Default_Category_Others"]?>' && Trim($('textarea#Description').val())=='') {
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Description']?>');
		$('textarea#Description').focus();
		return false;
	}
	else if (parseInt(js_Get_Jupas_Character_Count($('textarea#Description').val())) > parseInt(jsMaxCharCountArr['Description'])) {
		var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Description_Greater_Than_The_Max']?>', jsMaxCharCountArr['Description']);
		alert(jsWarningMsg);
		$('textarea#Description').focus();
		return false;
	}
	
	return true;
}

function Edit_Oea_Title() {
	//$('#oea_category_others').click();	
}

function CopyOthers() {
	document.getElementById('selectedItemSpan').innerHTML = "<b><input type='text' name='oea_title' id='oea_title' value='"+document.form11.stored_oea_title.value+"' size='40'>";
	$('input#ItemCode').val('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	$('input#oea_title').val(document.form11.stored_oea_title.value);
	checkOEAcategory('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	$('#MapOptions').hide('slow');
	
	//$('textarea#Description').attr('readonly', '');
}

function js_show_layer(layername) {
	$('#'+layername).show('slow');
}

function checkOEAcategory(itemCode) {
	document.getElementById('span_OEA_Category').innerHTML = itemAry[itemCode][1];
	document.getElementById('CatCode').value =  itemAry[itemCode][0];
}

function showHideSpan(layername) {
	if($('input#AdvanceLayerDisplay').val()==1) {
		$('#'+layername).hide('slow');
		$('input#AdvanceLayerDisplay').val(0);
	} else {
		$('#'+layername).show('slow');
		$('input#AdvanceLayerDisplay').val(1);
	}
}

function changeAwardSetting(val) {
	changeParticipation(val);	
	checkAchievement(val);
}

function checkAchievement(val) {
	if(val==1) 
		document.form11.AchievementID.disabled = false;
	else 
		document.form11.AchievementID.disabled = true;
	$('select#AchievementID').val('');
}

function changeParticipation(val) {
	var participationNature = document.form11.elements["ParticipationNature"];
	while (participationNature.options.length > 0) {
	    participationNature.options[0] = null;
	}
	participationNature.options[0] = new Option("-- <?=$button_select?> --", '');

	if(val==1) {
		participationNature.options[1] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["C"]?>","C", true);
		participationNature.options[2] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["N"]?>","N");
		participationNature.options[3] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["P"]?>","P");
	}
	else {
		participationNature.options[1] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["N"]?>","N");
		participationNature.options[2] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["P"]?>","P");
	}
	//participationNature.selectedIndex = 0;
	
		
}

function resetAdvanceLayerDisplay() {
	$('input#AdvanceLayerDisplay').val(0);
}

function loadAdvanceSearch() {
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: 'ajax_advance_search'
			
			 },
		function(ReturnData)
		{
			$('#advanceSearchLayer2').html(ReturnData);	
			//document.getElementById('tempDisplay').innerHTML = ReturnData;	
		}
	);	
}

function loadNextPage(layername, jsLoadContent) {
	var content = (jsLoadContent=="advanceSearch") ? "ajax_advance_search" : "ajax_get_suggestion_list";
	var ole_title = $('input#ole_title').val();
	
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: content,
			ole_title: ole_title
			
			 },
		function(ReturnData)
		{
			$('div#'+layername).html(ReturnData);	
			$('div#'+layername).show();
			$('div#TableDiv').hide();
			$('div#buttonDiv').hide();
		}
	);		
}

function loadPreviousPage() {
	$('div#SearchDiv').hide();
	$('div#TableDiv').show();
	$('div#buttonDiv').show();
	$('#MapOptions').hide('slow');
}

function copyback(code) {

	$('input#ItemCode').val(code);
	checkOEAcategory(code);	
	
	document.getElementById('selectedItemSpan').innerHTML = "<b><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label></b>";
	loadPreviousPage();
	
//	if (code == '<?=$oea_cfg["OEA_Default_Category_Others"]?>') {
//		$('textarea#Description').attr('readonly', '');
//	}
//	else {
//		$('textarea#Description').attr('readonly', 'readonly');
//	}
}

function Show_SuggestionList()
{
	
	var ole_title = $('input#ole_title').val();
	
	$.post(
		"index.php", 
		{ 
			task: 'ajax',
			script: 'ajax_get_suggestion_list',
			ole_title: ole_title
			
			 },
		function(ReturnData)
		{
			//alert(ReturnData);
			$('#TB_ajaxContent').html(ReturnData);	
				
		}
	);	
}

function showShortCutPanel(objID,ParRecordID,ParType,ParStudentID) 
{

	var shortCutPanel = document.getElementById('DescDivID');
	
	var jsOffsetLeft, jsOffsetTop;	
//	jsOffsetLeft = 60;

	jsOffsetLeft = 300;
	
	jsOffsetTop = 20;
		
	var posleft = getPostion(document.getElementById(objID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPostion(document.getElementById(objID), 'offsetTop') + jsOffsetTop;
	
	document.getElementById('DescDivID').style.left = posleft + "px";
	document.getElementById('DescDivID').style.top = postop + "px";

	MM_showHideLayers('DescDivID','','show'); 

	$('div#DescDivContent').html('<?=$linterface->Get_Ajax_Loading_Image()?>');

	$('div#DescDivContent').load(
		"index.php",  
		{ 
			task: 'ajax',
			script: 'ajax_getDescription',
			RecordID: ParRecordID,
			StudentID: ParStudentID,
			Type: ParType
		},
		function(returnString)
		{
			if(returnString=='')
			{
				$('div#DescDivContent').html('<?=$Lang['General']['EmptySymbol']?>');
			}	
		}
	);
	
}


function hideShortCutPanel() {	
	$('div#DescDivContent').html('');
	MM_showHideLayers('DescDivID','','hide');	
}




<?=$h_js?>

</script>
<?=$js_css?>

<span id="tempDisplay"></span>
<form id="form1" name="form1" method="POST" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr><td align="left" class="navigation"><?=$h_PageNavigation?></td></tr>
		<tr><td align="center"><?=$h_Disclaimer?></td></tr>
		<tr>
			<td align="center">
				
				<!-- Student Info Table -->
				<table class="form_table_v30">
					<tr>
						<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></td>
						<td><?=$h_ClassName?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['StudentName']?></td>
						<td><?=$h_StudentSelection?></td>
					</tr>
				</table>
				<br style="clear:both;" />
				<?=$h_RemarksMsgTable?>
				<?=$h_OEAItemTable?>
				<?=$h_AddiInfoTable?>
				<?=$h_AbilityTable?>
				<?=$h_AcademicTable?>
				<?=$h_SuppInfoTable?>
			</td>
		</tr>
	</table>
	
	<div class="edit_bottom_v30" style="border-top:0px;">
		<?=$h_BtnPreviousStudent?>
		<?=$h_BtnBack?>
		<?=$h_BtnNextStudent?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="script" name="script" value="" />
	<input type="hidden" id="action" name="action" value="" />
	<input type="hidden" id="ObjectID" name="ObjectID" value="" />
	<input type="hidden" id="AdvanceLayerDisplay" name="AdvanceLayerDisplay" value="0" />
	<input type="hidden" id="<?=$h_AddiInfoIDField?>" name="<?=$h_AddiInfoIDField?>" value="<?=$h_AddiInfoID?>" />
	<input type="hidden" id="<?=$h_StudentAbilityIDField?>" name="<?=$h_StudentAbilityIDField?>" value="<?=$h_StudentAbilityID?>" />
	<input type="hidden" id="<?=$h_StudentAcademicIDField?>" name="<?=$h_StudentAcademicIDField?>" value="<?=$h_StudentAcademicID?>" />
	<input type="hidden" id="<?=$h_SuppInfoIDField?>" name="<?=$h_SuppInfoIDField?>" value="<?=$h_SuppInfoID?>" />
	
	
	<input type="hidden" id="RecordStatus_AddiInfo" name="RecordStatus_AddiInfo" value="<?=$h_AddiInfoRecordStatus?>" />
	<input type="hidden" id="RecordStatus_Ability" name="RecordStatus_Ability" value="<?=$h_StudentAbilityRecordStatus?>" />
	<input type="hidden" id="RecordStatus_Academic" name="RecordStatus_Academic" value="<?=$h_AcademicRecordStatus?>" />
	<input type="hidden" id="RecordStatus_SuppInfo" name="RecordStatus_SuppInfo" value="<?=$h_SuppInfoRecordStatus?>" />
	
	<div id="DescDivID" class="selectbox_layer"  style="visibility:hidden; width:300px; height:200px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td><?=$linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['Description']) ?></td></tr>
		<tr>
			<td>
				<div id="DescDivContent"></div>
			</td>
		</tr>
	</table>
	</div>
	
</form>
<br/>