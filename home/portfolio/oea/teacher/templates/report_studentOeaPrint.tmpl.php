<?php

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$Lang['Header']['HomeTitle']?></title>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/jupas_report.css" rel="stylesheet" type="text/css">
</head>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<body>
<div class="report_div_landscape">
	<?=$h_printButton?>
	<h1><?=$h_reportTitle?></h1>
	<?=$h_table?>
</div>
</body>
</html>