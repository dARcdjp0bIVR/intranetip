<?php
$liboea = new liboea();
$libfcm = new form_class_manage();

$YearID = trim(urldecode(stripslashes($_REQUEST['YearID_Export'])));
$YearClassID = trim(urldecode(stripslashes($_REQUEST['YearClassID_Export'])));
$ReturnTeachingClassOnly = trim(urldecode(stripslashes($_REQUEST['ReturnTeachingClassOnly_Export'])));


$SettingsWarningArr = array();


### Navigation
$NavigationArr = array(); 
$NavigationArr[] = array($Lang['iPortfolio']['OEA']['StudentList'], "javascript:js_Go_Back();");
$NavigationArr[] = array($Lang['iPortfolio']['OEA']['AcademicArr']['GenerateAcademicPerformance'], '', 1);
$h_navigation = $linterface->GET_NAVIGATION_IP25($NavigationArr);


### Warning Table
$h_warningTable = $linterface->GET_WARNING_TABLE($Lang['iPortfolio']['OEA']['AcademicArr']['GenerateWarning']);


### Get Target Classes
$ApplicableFormWebSAMSCodeArr = $liboea->Get_Academic_Applicable_Form_WebSAMS_Code_Array();

$YearClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID(), $YearID, $ReturnTeachingClassOnly, $YearClassID);
$numOfClass = count($YearClassInfoArr);

$ApplicableYearClassIDArr = array();
$ApplicableYearClassNameArr = array();
for ($i=0; $i<$numOfClass; $i++) {
	$thisYearClassID = $YearClassInfoArr[$i]['YearClassID'];
	$thisYearClassNameEn = $YearClassInfoArr[$i]['ClassTitleEN'];
	$thisYearClassNameCh = $YearClassInfoArr[$i]['ClassTitleB5'];
	$thisFormWebSAMSCode = $YearClassInfoArr[$i]['Form_WebSAMSCode'];
	
	if (!in_array($thisFormWebSAMSCode, (array)$ApplicableFormWebSAMSCodeArr)) {
		continue;
	}
	
	$ApplicableYearClassIDArr[] = $thisYearClassID;
	$ApplicableYearClassNameArr[] = Get_Lang_Selection($thisYearClassNameCh , $thisYearClassNameEn);
}
$h_YearClassName = implode('<br />', (array)$ApplicableYearClassNameArr);

$numOfApplicableClass = count((array)$ApplicableYearClassNameArr);
if ($numOfApplicableClass == 0) {
	$SettingsWarningArr[] = $Lang['iPortfolio']['OEA']['WarningArr']['NoAcademicClassSettings'];	
}


### Get Academic Performance Mapping
$MappingInfoAssoArr = $liboea->getAcademic_PercentileMapWithOverAllRating();
$IsAllPercentileMapped = true;
foreach ((array)$MappingInfoAssoArr as $thisPercentileCode => $thisOverallPerformanceCode) {
	if ($thisOverallPerformanceCode == '') {
		$IsAllPercentileMapped = false;
		break;
	}
}
if ($IsAllPercentileMapped == false) {
	$SettingsWarningArr[] = $Lang['iPortfolio']['OEA']['WarningArr']['NotAllPercentileMapped'];	
}
else {
	$h_percentileMappingTable = $liboea->Get_Academic_Percentile_Overall_Rating_Mapping_View_Table();
}


### Generate Settings Warning Display
$numOfSettingsWarning = count($SettingsWarningArr);
if ($numOfSettingsWarning > 0) {
	$h_settingsWarning = implode('<br />', (array)$SettingsWarningArr);
}


### Generate Calculation Remarks
$h_remarksTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['iPortfolio']['OEA']['AcademicArr']['GenerateFormula']);


### Buttons
$h_generateBtn = $linterface->Get_Action_Btn($Lang['Btn']['Generate'], "button", "js_Go_Generate();", 'GenerateBtn');
$h_cancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Go_Back();", 'CancelBtn');


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>