<?php
# modifying : Bill

### Get Parameters
$StudentID = $_REQUEST['StudentID'];

### Initiate libraries
$libfcm = new form_class_manage();
$libfcm_ui = new form_class_manage_ui();
$liboea = new liboea();
$liboea_setting = new liboea_setting();
$ObjOEAStudent = new liboea_student($StudentID);

### Disclaimer
//$h_Disclaimer = $linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['iPortfolio']['OEA']['Disclaimer'].'</span>', $Lang['iPortfolio']['OEA']['Disclaimer_TeacherView']);

### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['JUPAS'], "javascript:js_Back_To_Index()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['ViewAndApproveStudentJupasData']);
$h_PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### Get Student Info
$StudentInfoArr = $libfcm->Get_Student_Class_Info_In_AcademicYear($StudentID, Get_Current_Academic_Year_ID());
$h_ClassName = Get_Lang_Selection($StudentInfoArr[0]['ClassTitleB5'], $StudentInfoArr[0]['ClassTitleEN']);
$h_ClassNumber = $StudentInfoArr[0]['ClassNumber'];
$h_StudentName = Get_Lang_Selection($StudentInfoArr[0]['ChineseName'], $StudentInfoArr[0]['EnglishName']);

### Get Student Name Drop-down List
$YearClassID = $StudentInfoArr[0]['YearClassID'];
$h_StudentSelection = $libfcm_ui->Get_Student_Selection('StudentID', $YearClassID, $StudentID, 'js_Changed_Student_Selection(this.value);', $noFirst=1, $isMultiple=0, $isAll=0);

### Remark of Depreciated Modules
$h_RemarksMsgTable = '';
$DepreciatedModuleDetected = array();

### Build OEA Table
$showOEAItem = $liboea->isEnabledOEAItem();
$h_OEAItemTable = '';
if ($showOEAItem) {
	$DepreciatedModuleDetected[] = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["OeaItem"]];
	$h_OEAItemTable .= $liboea->getTeacher_listStuAllInfo_oeaItemDiv($StudentID);
	
	$h_OEAItemTable .= '<br style="clear:both;" />'."\n";
	$h_OEAItemTable .= '<br style="clear:both;" />'."\n";
	$h_OEAItemTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";
}

$js_css = $liboea->Include_JS_CSS();

### Build Additional Info Table
$showAddiInfo = $liboea->isEnabledAdditionalInfo();
$h_AddiInfoTable = '';
if ($showAddiInfo) {
	$DepreciatedModuleDetected[] = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["AddiInfo"]];
	$ObjAddiInfo = $ObjOEAStudent->getAdditionalInfo();
	
	$h_AddiInfoApprovedStatus = $ObjAddiInfo->get_approved_status();
	$h_AddiInfoRejectedStatus = $ObjAddiInfo->get_rejected_status();
	
	$h_AddiInfoID = $ObjAddiInfo->getObjectID();
	$h_AddiInfoIDField = $ObjAddiInfo->getObjectIdFieldName();
	$h_AddiInfoRecordStatus = $ObjAddiInfo->getRecordStatus();
	
	$h_AddiInfoTable .= $liboea->getTeacher_listStuAllInfo_addiInfoDiv($ObjAddiInfo);
	$h_AddiInfoTable .= '<br style="clear:both;" />'."\n";
	$h_AddiInfoTable .= '<br style="clear:both;" />'."\n";
	$h_AddiInfoTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";
}

### Remark of Depreciated Modules
if($DepreciatedModuleDetected){
	//$RemarksMsg = implode(' '.$Lang['iPortfolio']['JUPAS']['and'].' ',$DepreciatedModuleDetected).' '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'];
	//$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $RemarksMsg);
	$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $Lang['iPortfolio']['JUPAS']['RemarksOEA']);	
}


### Build Ability Table
$showAbility = $liboea->isEnabledAbility();
$h_AbilityTable = '';
if ($showAbility) {
	$ObjAbility = $ObjOEAStudent->getAbility();
	
	$h_AbilityApprovedStatus = $ObjAbility->get_approved_status();
	$h_AbilityRejectedStatus = $ObjAbility->get_rejected_status();
	
	$h_StudentAbilityID = $ObjAbility->getObjectID();
	$h_StudentAbilityIDField = $ObjAbility->getObjectIdFieldName();
	$h_StudentAbilityRecordStatus = $ObjAbility->getRecordStatus();
	
	$h_DefineAbilityAttributeArr = ConvertToJSArray(array_keys($ObjAbility->getJUPASAbilityAttributesConfigArr()), 'jsAbilityAttributeArr');
		
	$h_AbilityTable .= $liboea->getTeacher_listStuAllInfo_abilityDiv($ObjAbility);
	
	$h_AbilityTable .= '<br style="clear:both;" />'."\n";
	$h_AbilityTable .= '<br style="clear:both;" />'."\n";
	$h_AbilityTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";
}

### Build Academic Perfomance Table
$showAcademic = $liboea->isEnabledAcademicPerformance();
$h_AcademicTable = '';

if ($showAcademic) {
	$ObjAcademic = $ObjOEAStudent->getAcademic();
	
	$h_AcademicApprovedStatus = $ObjAcademic->get_approved_status();
	$h_AcademicRejectedStatus = $ObjAcademic->get_rejected_status();
	
	$h_StudentAcademicID = $ObjAcademic->getObjectID();
	$h_StudentAcademicIDField = $ObjAcademic->getObjectIdFieldName();
	$h_StudentAcademicRecordStatus = $ObjAcademic->getRecordStatus();
	
	$SubjectInfoArr = $ObjAcademic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
	$h_DefineAcademicSubjectIDArr = ConvertToJSArray(Get_Array_By_Key($SubjectInfoArr, 'SubjectID'), 'jsAcademicSubjectIDArr');
	unset($SubjectInfoArr);
	
	$AcademicMappingArr = $liboea->getAcademic_PercentileMapWithOverAllRating();
				
	$h_AcademicTable .= $liboea->getTeacher_listStuAllInfo_academicDiv($ObjAcademic);
	
	$h_AcademicTable .= '<br style="clear:both;" />'."\n";
	$h_AcademicTable .= '<br style="clear:both;" />'."\n";
	$h_AcademicTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";
}

### Build Supplementary Info Table
$showSuppInfo = $liboea->isEnabledSupplementaryInfo();
$h_SuppInfoTable = '';
if ($showSuppInfo) {
	$ObjSuppInfo = $ObjOEAStudent->getSupplementaryInfo();
	
	$h_SuppInfoApprovedStatus = $ObjSuppInfo->get_approved_status();
	$h_SuppInfoRejectedStatus = $ObjSuppInfo->get_rejected_status();
	
	$h_SuppInfoID = $ObjSuppInfo->getObjectID();
	$h_SuppInfoIDField = $ObjSuppInfo->getObjectIdFieldName();
	$h_SuppInfoRecordStatus = $ObjSuppInfo->getRecordStatus();
	
	$h_SuppInfoTable .= $liboea->getTeacher_listStuAllInfo_suppInfoDiv($ObjSuppInfo);
	$h_SuppInfoTable .= '<br style="clear:both;" />'."\n";
	$h_SuppInfoTable .= '<br style="clear:both;" />'."\n";
	$h_SuppInfoTable .= '<div class="edit_bottom_v30" style="height:10px;"></div>'."\n";
}

# prepare js for display of OEA category
$h_js = $liboea->getOeaItemAryForJs();
	

### Buttons
$h_BtnBack = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Index();", $id="Btn_Back")."\n";

# Get Info of previous / next Student
$ObjYearClass = new year_class($YearClassID, $GetYearDetail=true, $GetClassTeacherList=false, $GetClassStudentList=true);
$ClassStudentIDArr = Get_Array_By_Key($ObjYearClass->ClassStudentList, 'UserID');
$numOfClassStudent = count($ClassStudentIDArr);

$PreviousStudentID == '';
$NextStudentID == '';
for ($i=0; $i<$numOfClassStudent; $i++)
{
	$_StudentID = $ClassStudentIDArr[$i];
	
	if ($_StudentID == $StudentID)
	{
		$PreviousStudentID = $ClassStudentIDArr[$i-1];
		$NextStudentID = $ClassStudentIDArr[$i+1];
		break;
	}
}

$thisDisabled = ($PreviousStudentID == '')? true : false;
$thisOnclick = ($PreviousStudentID == '')? '' : "js_Reload_Page('".$PreviousStudentID."');";
$h_BtnPreviousStudent = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['PreviousStudent'], "button", $thisOnclick, $id="Btn_PreStudent", '', $thisDisabled)."\n";

$thisDisabled = ($NextStudentID == '')? true : false;
$thisOnclick = ($NextStudentID == '')? '' : "js_Reload_Page('".$NextStudentID."');";
$h_BtnNextStudent = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['NextStudent'], "button", $thisOnclick, $id="Btn_NextStudent", '', $thisDisabled)."\n";


$h_DefineInternalCodeArr = ConvertToJSArray($liboea->Get_Module_Internal_Code_Array(), 'jsInternalCodeArr');

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>