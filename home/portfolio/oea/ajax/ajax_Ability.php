<?php
$action = trim(urldecode(stripslashes($_REQUEST['action'])));
$ObjectID = trim(urldecode(stripslashes($_REQUEST['ObjectID'])));

$liboea = new liboea();
$objAbility = new liboea_ability($ObjectID);

if ($action == 'Save')
{
	$StudentID = trim(urldecode(stripslashes($_REQUEST['StudentID'])));
	
	if ($ObjectID == '') {
		$objAbility->setStudentID($StudentID);
	}
	
	$AttributeArr = $objAbility->getJUPASAbilityAttributesConfigArr();
	foreach ((array)$AttributeArr as $thisAttributeInternalCode => $thisAttributeInfoArr)
	{
		// same as $objAbility->setAbilityToCommunicate($AbilityRadio_AbilityToCommunicate);
		eval('$objAbility->set'.$thisAttributeInternalCode.'($AbilityRadio_'.$thisAttributeInternalCode.');');
	}

	echo $objAbility->save();
}
else if ($action == 'Load_View_Mode')
{
	echo $liboea->getTeacher_listStuAllInfo_abilityTable($objAbility);
}
else if ($action == 'Load_Edit_Mode')
{
	echo $liboea->getTeacher_listStuAllInfo_abilityEditTable($objAbility);
}
else if ($action == 'Approve')
{
	$success = $objAbility->approve_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Reject')
{
	$success = $objAbility->reject_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Load_Approval_Status_Display')
{
	$liboea = new liboea();
	echo $liboea->getApprovalStatusDisplay($objAbility->getObjectInternalCode(), $objAbility->getRecordStatus(), $objAbility->getApprovedBy(), $objAbility->getApprovalDate());
}

?>
