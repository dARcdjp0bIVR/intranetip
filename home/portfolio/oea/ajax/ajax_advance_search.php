<?php
# modifying : fai

$linterface = new interface_html();
		
$liboea_setting = new liboea_setting();
$_objOEA_ITEM = new liboea_item();

$isNewRecord = $_objOEA_ITEM->getNewRecord();
$default_oea_category = $liboea_setting->get_OEA_Default_Category($associateAry=0, $include_others=0);
$default_oea_category = array_merge(array(array("0", $i_general_all)), $default_oea_category);

$categorySelect = getSelectByArray($default_oea_category, 'name="CatCode2" id="CatCode2" onChange="goGenerateResult()"', $CatCode2, 0, 0, "-- $i_alert_pleaseselect --");
//$x = "<form name='form1' id='form1'>";
$x = '';
$x .= "<br />";
$x .= "<table width='90%'>";
if($ole_title!="") {
	$x .= "<tr>";
	$x .= "<td colspan='2'>".$Lang['iPortfolio']['OEA']['OLE_Title']." : $ole_title<br style='clear:both'><br style='clear:both'></td>";
	$x .= "</tr>";
}
$x .= "<tr>";
$x .= "<td width=\"30%\">".$Lang['iPortfolio']['OEA']['ByKeyword']."</td>";
$x .= "<td><input type='text' name='keyword' id='keyword' class='textbox_name' onKeyUp='goGenerateResult()'></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td>".$Lang['iPortfolio']['OEA']['ByCategory']."</td>";
$x .= "<td>$categorySelect</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "<div class='edit_bottom_v30' style=\"height:10px;\">";
//$x .= $linterface->GET_ACTION_BTN($button_search, "button", "goGenerateResult()")."&nbsp;";
if(!$IsForOLEEdit && $_SESSION['UserType']==USERTYPE_STAFF) {
	$x .= $linterface->GET_ACTION_BTN($button_previous_page, "button", "loadPreviousPage()");	
}
$x .= "</div>";
$x .= "<br style='clear:both;'>";

$x .= "<div id='ResultSpan' style='width:700px;height:45px;overflow:auto;'>";
if($IsForOLEEdit || $_SESSION['UserType']==USERTYPE_STUDENT) {
	$x .= "<div  align='center'>".$linterface->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove()")."</div>";
	$x .= "<br style='clear:both;'>";	
}
$x .= "</div>";
//$x .= "</form>";

intranet_closedb();

echo $x;

?>

<style type="text/css">
.highlight { background-color: yellow }
</style>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.highlight-3.js"></script>
<script language="javascript">
<? if($IsForOLEEdit || $_SESSION['UserType']==USERTYPE_STUDENT) { ?>
function copyback(code) {
	//clearRadioSelection();
	$('input#ItemCode').val(code);

	checkOEAcategory(code);	

	document.getElementById('selectedItemSpan').innerHTML = "<b><label for='ItemCode_"+code+"'>"+document.getElementById('hiddenItemName').value+"</label></b>";

	$('#MapOptions').hide('slow');

	$('#divPencil').show();

	window.tb_remove();

	if (code == '00000') {
		window.CopyOthers();
		//$('textarea#Description').attr('readonly', '');
	}
	else {
		//$('textarea#Description').attr('readonly', 'readonly');
	}
}
<? } ?>

function goGenerateResult(GoToPage, NumPerPage) {

	GoToPage = (GoToPage == null)? 1 : GoToPage;
	NumPerPage = (NumPerPage == null)? <?=$oea_cfg['defaultNumPerPage']?> : NumPerPage;
	
	if(document.getElementById('CatCode2').value != "") {
		
//		document.getElementById('ResultSpan').innerHTML = "<?=$i_general_loading?>";
//		document.getElementById('ResultSpan').style.visibility = 'visible';
//		document.getElementById('ResultSpan').style.display = 'inline';
		$('div#ResultSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').css('visibility', 'visible').css('display', 'inline');
		var catCode = (document.getElementById('CatCode2').value==0) ? "" : document.getElementById('CatCode2').value;
		
		$.post(
			"/home/portfolio/oea/student/index.php", 
			{ 
				task: 'ajax',
				script: 'ajax_advance_search_result_table',
				CatCode: catCode,
				keyword: $('#keyword').val(),
				PageNo: GoToPage,
				NumPerPage:NumPerPage
				 },
			function(ReturnData)
			{
				$('div#ResultSpan').html(ReturnData);
				
				//$('div#SearchDiv').html(ReturnData);	
				$('div#SearchDiv').show();
				//$('div#TableDiv').hide();
				//$('div#buttonDiv').hide();
				
				//$('table#ResultTable').highlight($('#keyword').val());
			}
		);	
	}	
	
}

function clearRadioSelection() {
	$('input[name="ItemCode"]').attr('checked', false);	
	$('input#ItemCode').val('');
}

function assignItemToTemp(tempCode, tempName) {
	$('input#hiddenItemCode').val(tempCode);
	$('input#hiddenItemName').val(tempName);
}
</script>