<?php
$action = trim(urldecode(stripslashes($_REQUEST['action'])));

$liboea = new liboea();

if ($action == 'Import_OleOeaItemMapping')
{
	$TargetFilePath = trim(urldecode(stripslashes($_REQUEST['TargetFilePath'])));
	
	$lexport = new libexporttext();
	$limport = new libimporttext();
	$libpf_ole = new libpf_ole();
	$liboea_setting = new liboea_setting();
	
	
	### Delete the temp data first
	$SuccessArr['DeleteOldTempData'] = $liboea->Delete_Import_OLE_OEA_Mapping_Temp_Data();
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get Csv data
	$DefaultCsvHeaderArr = $liboea->getOLEMappingImportColumnTitleArray();
	$ColumnPropertyArr = $liboea->getOLEMappingImportColumnPropertyArray($forGetAllCsvContent=0);
	$CsvHeaderValidationArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr);
	$CsvHeaderDisplayArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr, $AddBracket=0);
	
	$ColumnPropertyAllInfoArr = $liboea->getOLEMappingImportColumnPropertyArray($forGetAllCsvContent=1);
	$CsvDataArr = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $CsvHeaderValidationArr, $ColumnPropertyAllInfoArr);
	
	$CsvHeaderArr = array_shift($CsvDataArr);	// Remove Chinese Title
	$numOfCsvData = count($CsvDataArr);
	
	
	### Get data for vaildation
	// Get all OLE Program Info
	$OLEInfoArr = $libpf_ole->Get_OLE_Info();
	$OLEProgramIDArr = Get_Array_By_Key($OLEInfoArr, 'ProgramID');
	$OLEProgramIDTitleAssoArr = BuildMultiKeyAssoc($OLEInfoArr, 'ProgramID', $IncludedDBField=array('TitleEn', 'TitleCh'), $SingleValue=0, $BuildNumericArray=0);
	
	
	// Get all OEA Item Code
	$OEAItemInfoAssoArr = $liboea_setting->get_OEA_Default_Item();
	$OEACodeArr = array_keys($OEAItemInfoAssoArr); 
	
	### Variable Initialization
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();		//$ErrorRowInfoArr[$RowNumber][ErrorField] = array(ErrorMsg)
	$InsertValueArr = array();
	$RowNumber = 3;
	
	
	### Loop each csv data
	$IncludedOLEProgramIDArr = array();
	$ImportColumnInfoArr = $liboea->getOLEMappingImportColumnInfoArray();
	$numOfDBColumn = count($ImportColumnInfoArr);
	for ($i=0; $i<$numOfCsvData; $i++)
	{
		# Load Data from each row
		for ($j=0; $j<$numOfDBColumn; $j++)
		{
			$_DBField = $ImportColumnInfoArr[$j]['DBField'];
			$_Value = str_replace('""', '"', trim($CsvDataArr[$i][$j]));
			
			if ($_DBField == 'OEA_ProgramCode' && $_Value != '') {
				$_Value = $liboea->Standardize_OEA_Program_Code($_Value);
			}
			else if ($_DBField == 'OEA_AwardBearing') {
				$_Value = strtoupper($_Value);	// convert to upper case for standardizing the letter
				if ($_Value == '') {
					$_Value = $oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["DefaultAwardBearing"];		// if no input => default assign to "y"	
				}
			}
			
			${"_".$_DBField} = $_Value;
		}
		$_OLE_ProgramID = $_OLE_ProgramID + 0;	// remove the prefix "0" in the OLE_ProgramID
		
		# Warning if the OLE ProgramID exist more than 1 times
		if (in_array($_OLE_ProgramID, $IncludedOLEProgramIDArr))
		{
			$ErrorRowInfoArr[$RowNumber]['OLE_ProgramID'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLECodeDuplicated'];
		}
		$IncludedOLEProgramIDArr[] = $_OLE_ProgramID;
		
		# Warning if the OLE ProgramID is not existing
		if (!in_array($_OLE_ProgramID, $OLEProgramIDArr))
		{
			$ErrorRowInfoArr[$RowNumber]['OLE_ProgramID'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotExist'];
		}
		
		# Warning if the OLE ProgramID is not matched with the Title
		$_OLE_ProgramNameEnInDB = trim(intranet_undo_htmlspecialchars($OLEProgramIDTitleAssoArr[$_OLE_ProgramID]['TitleEn']));
		$_OLE_ProgramNameChInDB = trim(intranet_undo_htmlspecialchars($OLEProgramIDTitleAssoArr[$_OLE_ProgramID]['TitleCh']));
		
		if ($_OLE_ProgramName == $_OLE_ProgramNameEnInDB) {
			// matched English Name => do nth
		}
		else if ($_OLE_ProgramName == $_OLE_ProgramNameChInDB) {
			// matched Chinese Name => do nth
		}
		else {
			// not matched
			$ErrorRowInfoArr[$RowNumber]['OLE_ProgramID'] = 1;
			$ErrorRowInfoArr[$RowNumber]['OLE_ProgramName'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OLEProgramIDNotMatchedWithTitle'];
		}
		
		
		# Warning if OEA Program Code is not existing
		if (!in_array($_OEA_ProgramCode, $OEACodeArr) || strlen($_OEA_ProgramCode) != 5) {
			$ErrorRowInfoArr[$RowNumber]['OEA_ProgramCode'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OEACodeNotExist'];
		}
		
		
		# Warning if the Award Bearing is not "Y" or "N"
		if ($_OEA_AwardBearing == $oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["AwardBearing_Yes"] || $_OEA_AwardBearing == $oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["AwardBearing_No"]) {
			// correct => do nth
		}
		else {
			$ErrorRowInfoArr[$RowNumber]['OEA_AwardBearing'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardBearing'];
		}
		
		
		### Prepare data to insert to Temp Table
		$_InsertFieldArr = array();
		for ($j=0; $j<$numOfDBColumn; $j++) {
			$_DBField = $ImportColumnInfoArr[$j]['DBField'];
			$_Value = ${"_".$_DBField};
			
			$_InsertFieldArr[] = $liboea->Get_Safe_Sql_Query(trim($_Value));
		}
		$InsertValueArr[] = "('".$_SESSION['UserID']."', '".$RowNumber."', '".implode("', '", (array)$_InsertFieldArr)."', now())";
		
		
		### Count success / failed records	
		if (!isset($ErrorRowInfoArr[$RowNumber]))
			$NumOfSuccessRow++;
		else
			$NumOfErrorRow++;
		
			
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 2).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		$RowNumber++;
	}
	
	### Insert the temp records into the temp table
	if (count($InsertValueArr) > 0)	{
		$SuccessArr['InsertTempRecords'] = $liboea->Insert_Import_OLE_OEA_Mapping_Temp_Data($InsertValueArr);
	}
	
	
	### Display Error Result Table if there are any
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Progress_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		
		### Get data of the error rows
		$ErrorRowArr = array_keys($ErrorRowInfoArr);
		$ErrorRowDataArr = $liboea->Get_Import_OLE_OEA_Mapping_Temp_Data($ErrorRowArr);
			
		
		### Build Error Info Table
		$CsvHeaderDisplayArr = Get_Lang_Selection($CsvHeaderDisplayArr[1], $CsvHeaderDisplayArr[0]);
		$numOfCsvHeader = count($CsvHeaderDisplayArr);
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</th>';
					for ($i=0; $i<$numOfCsvHeader; $i++)
					{
						$thisDisplay = $CsvHeaderDisplayArr[$i];					
						$x .= '<th>'.$thisDisplay.'</th>';
					}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
				for ($i=0; $i<$numOfErrorRow; $i++)
				{
					$_RowNumber = $ErrorRowDataArr[$i]['RowNumber'];
					
					# Error Remarks
					$_ErrorInfoArr = $ErrorRowInfoArr[$_RowNumber];
					$_ErrorDisplayArr = $_ErrorInfoArr['ErrorMsgArr'];
					$_ErrorDisplayText = implode('<br />', (array)$_ErrorDisplayArr);
					
					$css_i = ($i % 2) ? "2" : "";
					$x .= '<tr style="vertical-align:top">';
						$x .= '<td>'.$_RowNumber.'</td>';
						for ($j=0; $j<$numOfDBColumn; $j++)
						{
							$_DBField = $ImportColumnInfoArr[$j]['DBField'];
							$_Value = $ErrorRowDataArr[$i][$_DBField];
							
							if ($_DBField == 'OLE_ProgramNature') {
								$_Value = $liboea->getParticipationTypeDisplayByCode($_Value);
							}
							else if ($_DBField == 'OEA_AwardBearing') {
								$_Value = $liboea->getAwardBearingDisplayByCode($_Value);
							}
							
							
							// Missed complusory field
							if ($ColumnPropertyArr[$j] == 1 && $_Value == '') {
								$_Value = '<font color="red">***</font>';
							}
							
							// Error data
							$_Value = ($_ErrorInfoArr[$_DBField]==1)? '<font color="red">'.$_Value.'</font>' : $_Value;
							
							$x .= '<td>'.$_Value.'</td>';
						}
						$x .= '<td><font color="red">'.$_ErrorDisplayText.'</font></td>';
					$x .= '</tr>';
				}
			$x .= '</tbody>';
		$x .= '</table>';
	}
	$x = str_replace("'", "&#039;", $x);
	
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) {
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
if ($action == 'StudentAppliedOea')
{
	$OeaCode = $_POST['ItemCode'];
	$fromEdit = $_POST['fromEdit'];
	$OeaRecordID = ($fromEdit)? $_POST['recordID'] : '';
	
	if ($OeaRecordID=='') {
		$StudentID = $_SESSION['UserID'];
	}
	else {
		$objOeaItem = new liboea_item($OeaRecordID);
		$StudentID = $objOeaItem->getStudentID();
	}
	
	$ReturnVal = '';
	if ($OeaCode == $oea_cfg["OEA_Default_Category_Others"]) {
		// no warning for "Others"
		$ReturnVal = '0';
	}
	else {
		$StudentOeaInfoArr = $liboea->getStudentOeaItemInfoArr($StudentID, $RecordStatusArr='', $ParticipationArr='', $ParRecordIDArr='', $OeaRecordID);
		$StudentOeaCodeArr = Get_Array_By_Key($StudentOeaInfoArr, 'OEA_ProgramCode');
		
		$ReturnVal = in_array($OeaCode, (array)$StudentOeaCodeArr)? '1' : '0';
	}
	
	echo $ReturnVal;
}
?>