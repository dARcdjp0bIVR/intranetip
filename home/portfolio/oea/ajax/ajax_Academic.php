<?php
$action = trim(urldecode(stripslashes($_REQUEST['action'])));
$ObjectID = trim(urldecode(stripslashes($_REQUEST['ObjectID'])));
$StudentID = trim(urldecode(stripslashes($_REQUEST['StudentID'])));

$liboea = new liboea();
$objAcademic = new liboea_academic($ObjectID);

if ($ObjectID == '') {
	$objAcademic->setStudentID($StudentID);
}

if ($action == 'Save')
{
	$StudentID = trim(urldecode(stripslashes($_REQUEST['StudentID'])));
	$SubjectPercentileAssoArr = $_REQUEST['SubjectPercentileAssoArr'];
	$SubjectOverallRatingAssoArr = $_REQUEST['SubjectOverallRatingAssoArr'];
	
	$objAcademic->resetObjectSubjectPercentileInfo();
	foreach ((array)$SubjectPercentileAssoArr as $thisSubjectID => $thisPercentileInternalCode) {
		$objAcademic->setObjectSubjectPercentileInfo($thisSubjectID, $thisPercentileInternalCode);
	}
	
	$objAcademic->resetObjectSubjectOverallRatingInfo();
	foreach ((array)$SubjectOverallRatingAssoArr as $thisSubjectID => $thisOverallRatingInternalCode) {
		$objAcademic->setObjectSubjectOverallRatingInfo($thisSubjectID, $thisOverallRatingInternalCode);
	}
	
	$objAcademic->setPercentileRemark($_REQUEST['APpercentileRemarks']);
	$objAcademic->setOverallRatingRemark($_REQUEST['APratingRemarks']);
	
	echo $objAcademic->save();
}
else if ($action == 'Load_View_Mode')
{
	echo $liboea->getTeacher_listStuAllInfo_academicTable($objAcademic);
}
else if ($action == 'Load_Edit_Mode')
{
	echo $liboea->getTeacher_listStuAllInfo_academicEditTable($objAcademic);
}
else if ($action == 'Approve')
{
	$success = $objAcademic->approve_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Reject')
{
	$success = $objAcademic->reject_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Generate') {
	$ApplicableYearClassIDList = trim(stripslashes($_POST['ApplicableYearClassIDList']));
	$YearClassIDArr = explode(',', $ApplicableYearClassIDList);
	
	$StudentInfoArr = $lpf->Get_Student_With_iPortfolio($YearClassIDArr, $IsSuspend='');
	$numOfStudent = count($StudentInfoArr);
	
	$lpf->Start_Trans();
	$SuccessArr = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentID = $StudentInfoArr[$i]['UserID'];
		$_isIPfActivated = $StudentInfoArr[$i]['IsPortfolioActivated'];
		
		if (!$_isIPfActivated) {
			continue;
		}
		
		$_liboea_student = new liboea_student($_studentID);
		
		$_objAcademic = $_liboea_student->getAcademic();
		$_objAcademic->generate_academic_performance();
				
		$SuccessArr[$_studentID] = $_objAcademic->save();
	}
	
	if (in_array(false, (array)$SuccessArr)) {
		$lpf->Rollback_Trans();
		echo '0';
	}
	else {
		$lpf->Commit_Trans();
		echo '1';
	}
}
else if ($action == 'Generate_For_Preset_Radio') {
	$StudentID = trim(stripslashes($_POST['StudentID']));
	$liboea_student = new liboea_student($StudentID);
		
	$objAcademic = $liboea_student->getAcademic();
	$objAcademic->generate_academic_performance();
	
	echo $objAcademic->convertObjectSubjectPercentileToDBValue().'|||'.$objAcademic->convertObjectSubjectOverallRatingToDBValue();
}

?>