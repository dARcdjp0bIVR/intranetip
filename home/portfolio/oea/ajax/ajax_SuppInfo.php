<?php
$action = trim(urldecode(stripslashes($_REQUEST['action'])));
$ObjectID = trim(urldecode(stripslashes($_REQUEST['ObjectID'])));
$Obj = new liboea_supplementary_info($ObjectID);

if ($action == 'Save')
{
	$Title = trim(urldecode(stripslashes($_REQUEST['SuppInfoTitle'])));
	$Details = trim(urldecode(stripslashes($_REQUEST['SuppInfoDetails'])));
	$StudentID = trim(urldecode(stripslashes($_REQUEST['StudentID'])));
	
	$Obj->setTitle($Title);
	$Obj->setDetails($Details);
	
	if ($ObjectID == '') {
		$Obj->setStudentID($StudentID);
	}
	
	$ResultID = $Obj->save();
	echo $ResultID;
}
else if ($action == 'Load_View_Mode')
{
	echo $Obj->getViewTable();
}
else if ($action == 'Load_Edit_Mode')
{
	echo $Obj->getEditTable();
}
else if ($action == 'Approve')
{
//	Must Save the info before approving now
//	if ($ObjectID == '')
//	{
//		$Obj->setStudentID($StudentID);
//		$ResultID = $Obj->save();
//	}

	$success = $Obj->approve_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Reject')
{
	$success = $Obj->reject_record();
	echo ($success)? '1' : '0';
}
else if ($action == 'Load_Approval_Status_Display')
{
	$liboea = new liboea();
	echo $liboea->getApprovalStatusDisplay($Obj->getObjectInternalCode(), $Obj->getRecordStatus(), $Obj->getApprovedBy(), $Obj->getApprovalDate());
}

?>