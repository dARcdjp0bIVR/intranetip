<?
# modifying : 

//$handleID = intval($handleID);

$h_HiddenOLE_Item = "";

$liboea = new liboea();
//$_objOLE_ITEM = new libole_student_item($_id);
$_objOEA_ITEM = new liboea_item();
$liboea_setting = new liboea_setting();
//$liboea_setting_search = new liboea_setting_search($liboea_setting);

// check if the student can new OEA item
if (!$liboea->getAllowStudentNewOEA_NotFromOLE()) {
	No_Access_Right_Pop_Up();
}

// check if the quota reached the limit or not
$maxNumOfOea = $liboea->getMaxNoOfOea();
$studentNumOfOeaApplied = count($liboea->getStudentOeaItemInfoArr($_SESSION['UserID']));
if ($studentNumOfOeaApplied >= $maxNumOfOea) {
	No_Access_Right_Pop_Up();
}

// check if deadline has past
if ($liboea->getIsSubmissionPeriod()==false) {
	No_Access_Right_Pop_Up();
}


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_List'], "javascript:js_Back_To_OEA_List()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['New_Record'], "");
$h_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# load table of OLE & OEA
$_objOEA_ITEM->setNewRecord($isNewRecord=1);
$h_result = $_objOEA_ITEM->getOeaItemTable();

$js_css = $liboea->Include_JS_CSS();

# prepare js for display of OEA category
//	$default_oea_all = $liboea_setting->get_OEA_Item();
//	if(sizeof($default_oea_all)>0) {
//		$js_default_item_array = "var itemAry = new Array();\n";
//		foreach($default_oea_all as $_key=>$_ary) {
//			$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
//		}
//	}
//	$h_js = $js_default_item_array."\n";
$h_js = $liboea->getOeaItemAryForJs();
	
# hidden field 
for($i = 0, $i_max = sizeof($recordID); $i< $i_max;$i++){
	$h_HiddenOLE_Item .= "<input type =\"hidden\" name=\"recordID[]\" id=\"recordID[]\" value=\"".$recordID[$i]."\">\n";
}

$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentRecordID\" id=\"currentRecordID\" value=\"$_id\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"ole_ProgramID\" id=\"ole_ProgramID\" value=\"$ole_ProgramID\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentPos\" id=\"currentPos\" value=\"$currentPos\">";
//$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"task\" id=\"task\" value=\"$task\">";

# button
$h_button = $linterface->GET_ACTION_BTN($button_submit, "button", "doSave()", "submitBtn")."&nbsp;";
$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();

?>