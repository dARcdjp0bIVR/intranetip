<?
# modifying : henry chow

//$handleID = intval($handleID);

$h_HiddenOLE_Item = "";

$currentPos = (isset($currentPos) && $currentPos!=0) ? $currentPos : 1;


# till end of selection
if($currentPos>sizeof($recordID)) {
	header("Location: index.php?msg=$msg");
	exit;
}
### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_List'], "javascript:js_Back_To_OEA_List()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_CreateFromOLE'], "");
$h_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# step information
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step1'], 0);
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step2'], 1);
$step_navigation = $linterface->GET_STEPS($STEPS_OBJ);


/*
$countText = "<b>".$Lang['iPortfolio']['OEA']['Add_CountText_1']." : ".sizeof($recordID)."<br>";
$countText .= $Lang['iPortfolio']['OEA']['Add_CountText_2']." : ".($currentPos-1)."</b><br><br>";
$countText = showWarningMsg($Lang['iPortfolio']['OEA']['Remark'], $countText);
*/
$countText = "
<br style='clear:both'>
	<div class='this_form' style='width:550'>
	<table class='form_table_v30'>
	<tr>
		<td class='field_title'>".$Lang['iPortfolio']['OEA']['Add_CountText_1']."</td>
		<td>".sizeof($recordID)."</td>
	</tr>
	<tr>
		<td class='field_title'>".$Lang['iPortfolio']['OEA']['Add_CountText_2']."</td>
		<td>".($currentPos-1)."</td>
	</tr>
	</table><br>";

$h_result = "<table class='common_table_list_v30'>";
$h_result .= "<th width='10%'>&nbsp; </th>";
$h_result .= "<th width='40%'>".$Lang['iPortfolio']['OEA']['OLE']."</th>";

$h_result .= "<th width='50%'>".$Lang['iPortfolio']['OEA']['OEA_Name2']."</th>";

$_id = $recordID[$currentPos-1];

//$handleID = ($handleID<= 0) ? $_id : $handleID;

$liboea = new liboea();
$_objOLE_ITEM = new libole_student_item($_id);
$_objOEA_ITEM = new liboea_item();
$liboea_setting = new liboea_setting();
$liboea_setting_search = new liboea_setting_search($liboea_setting);

$js_css = $liboea->Include_JS_CSS();

################
### OLE Info ###
################
# OLE Title
$ole_title = $_objOLE_ITEM->getTitle();
if($ole_title=="") $ole_title = "-";
# OLE Category
$ole_catName = $_objOLE_ITEM->getCategoryTitle($intranet_session_language);
if($ole_catName=="") $ole_catName = "-";
# OLE Date
$ole_date = $_objOLE_ITEM->getStartDate();
if($_objOLE_ITEM->getEndDate()) $ole_date .= " $i_To ".$_objOLE_ITEM->getEndDate();
if($ole_date==$i_To) $ole_date = "-";
# OLE Role
$ole_role = $_objOLE_ITEM->getRole(); 
if($ole_role=="") $ole_role = "-";
# OLE Achievement
$ole_achievement = $_objOLE_ITEM->getAchievement();
if($ole_achievement=="") $ole_achievement = "-";
# OLE Program ID
$ole_ProgramID = $_objOLE_ITEM->getProgramID();
# OLE INT/EXT
$ole_int_ext_temp = $_objOLE_ITEM->getIntExt();
$ole_int_ext = ($ole_int_ext_temp=="INT") ? $oea_cfg["OEA_Participation_Type"]["S"] : $oea_cfg["OEA_Participation_Type"]["P"];

################
### OEA Info ###
################
# OEA Item Code / Category Code
$mappedItemCode = $_objOLE_ITEM->getMapped_OEA_ProgramCode();
if($mappedItemCode) {		# Teacher mapped item already

	$default_oea_item = $liboea_setting->get_OEA_Item($mappedItemCode);
	$oea_itemSelect = $default_oea_item[$mappedItemCode]['ItemName'];
	$oea_itemSelect .= "<input type='hidden' name='ItemCode' id='ItemCode' value='".$default_oea_item[$mappedItemCode]['ItemCode']."'>";
	$oea_catSelect = $default_oea_item[$mappedItemCode]['CatName'];
	$oea_catSelect .= "<input type='hidden' name='CatCode' id='CatCode' value='".$default_oea_item[$mappedItemCode]['CatCode']."'>";
	
} else {
	
	# pulldown menu of default OEA item
	$suggestText = ($ole_title=="-") ? "" : $ole_title; 
	$liboea_setting_search->setCriteriaTitle($suggestText);
	$liboea_setting_search->getSuggestResult();
	$oea_itemSelect = $liboea_setting_search->loadSuggestResultDisplay();
	# prepare js for display of OEA category
	$default_oea_all = $liboea_setting->get_OEA_Item();
	if(sizeof($default_oea_all)>0) {
		$js_default_item_array = "var itemAry = new Array();\n";
		foreach($default_oea_all as $_key=>$_ary) {
			$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
		}
	}
	$h_js = $js_default_item_array."\n";
	//$oeaCatAry = $liboea_setting->get_OEA_Default_Category($returnAssociAry=0);
	//$oea_catSelect = getSelectByArray($oeaCatAry, 'name="CatCode" id="CatCode"', "");
	$oea_catSelect = "<div id='span_OEA_Category'>-</div>";
}
# OEA Date
$oea_start = substr($_objOLE_ITEM->getStartDate(),0,4);
$oea_end = ($_objOLE_ITEM->getEndDate()=="") ? $oea_start : substr($_objOLE_ITEM->getEndDate(),0,4);
$oea_date = $i_From." <input type='text' name='startdate' id='startdate' value='$oea_start' size=4' maxlength='4'> ".$i_To." <input type='text' name='enddate' id='enddate' value='$oea_end' size='4' maxlength='4'> (YYYY)";
# OEA Role
$RoleAry = $_objOEA_ITEM->get_OEA_Role_Array();
$rAry = array();
if(sizeof($RoleAry)>0) {
	$i = 0;
	foreach($RoleAry as $key=>$val) {
		$rAry[$i][] = $key;
		$rAry[$i][] = $val;
		$i++;	
	}	
}
$roleSelect = getSelectByArray($rAry, 'name="RoleID" id="RoleID"', "");
# OEA Achievement
$defaultAwardBearing = 1;
if($mappedItemCode) {
	$defaultAwardBearing = $_objOEA_ITEM->getAwardBearing($mappedItemCode);	
} 

if($defaultAwardBearing) { # with award bearing

	$AwardBearing = "包含獎項";
	$AwardBearing .= "<input type='radio' name='awardBearing' id='awardBearing1' value='Y' checked onClick='changeAwardSetting(1)'><label for='awardBearing1'>".$i_general_yes."</label>";
	$AwardBearing .= "<input type='radio' name='awardBearing' id='awardBearing0' value='N' onClick='changeAwardSetting(0)'><label for='awardBearing0'>".$i_general_no."</label><br>";
	
	$AchievementAry = $_objOEA_ITEM->get_OEA_Achievement_Array();
	$aAry = array();
	if(sizeof($AchievementAry)>0) {
		$i = 0;
		foreach($AchievementAry as $key=>$val) {
			$aAry[$i][] = $key;
			$aAry[$i][] = $val;
			$i++;	
		}	
	}
	$achievementSelect = getSelectByArray($aAry, 'name="AchievementID" id="AchievementID"', "");
# OEA Participation Nature
	$ParticipationNatureAry = $_objOEA_ITEM->get_OEA_ParticipationNature_Array();
	$aAry = array();
	if(sizeof($ParticipationNatureAry)>0) {
		$i = 0;
		foreach($ParticipationNatureAry as $key=>$val) {
			$aAry[$i][] = $key;
			$aAry[$i][] = $val;
			$i++;	
		}	
	}
	$participationNatureSelect = getSelectByArray($aAry, 'name="ParticipationNature" id="ParticipationNature"', $ParticipationNature);
} else {	# without award bearing
	$achievementSelect = $Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'];
	$h_HiddenOLE_Item .= "<input type='hidden' name='awardBearing' id='awardBearing' value='N'>";
	$h_HiddenOLE_Item .= "<input type='hidden' name='AchievementID' id='AchievementID' value='N'>";
	$h_HiddenOLE_Item .= "<input type='hidden' name='ParticipationNature' id='ParticipationNature' value=''>";
}

# OEA Participation
if($ole_int_ext_temp=="INT") $int_selected = " checked";
else $ext_selected = " checked";
$oea_participationOption = "<input type='radio' name='Participation' id='Participation_S' value='S' $int_selected><label for='Participation_S'>".$oea_cfg["OEA_Participation_Type"]["S"]."</label><br>".
								"<input type='radio' name='Participation' id='Participation_P' value='P' $ext_selected><label for='Participation_P'>".$oea_cfg["OEA_Participation_Type"]["P"]."</label>";



$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Title']."</td>";
$h_result .= "<td>$ole_title</td>";
$h_result .= "<td>$oea_itemSelect</td>";
$h_result .= "</tr>";
$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Category']."</td>";
$h_result .= "<td>$ole_catName</td>";
$h_result .= "<td>$oea_catSelect</td>";
$h_result .= "</tr>";
$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Date']."</td>";
$h_result .= "<td>$ole_date</td>";
$h_result .= "<td>$oea_date</td>";
$h_result .= "</tr>";
$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Role']."</td>";
$h_result .= "<td>$ole_role</td>";
$h_result .= "<td>$roleSelect</td>";
$h_result .= "</tr>";
$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Achievement']."</td>";
$h_result .= "<td>$ole_achievement</td>";
$h_result .= "<td>$AwardBearing $participationNatureSelect $achievementSelect</td>";
$h_result .= "</tr>";
$h_result .= "<tr>";
$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Participation']."</td>";
$h_result .= "<td>$ole_int_ext</td>";
$h_result .= "<td>$oea_participationOption</td>";
$h_result .= "</tr>";
	

$h_result .= "</table>";

# hidden field 
for($i = 0, $i_max = sizeof($recordID); $i< $i_max;$i++){
	$h_HiddenOLE_Item .= "<input type =\"hidden\" name=\"recordID[]\" id=\"recordID[]\" value=\"".$recordID[$i]."\">\n";
}

$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentRecordID\" id=\"currentRecordID\" value=\"$_id\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"ole_ProgramID\" id=\"ole_ProgramID\" value=\"$ole_ProgramID\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentPos\" id=\"currentPos\" value=\"$currentPos\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"task\" id=\"task\" value=\"$task\">";

# button
if($currentPos < sizeof($recordID)) {
	$h_button = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['SaveAndNext'], "button", "doSave()", "submitBtn")."&nbsp;";
	//$h_button .= $linterface->GET_ACTION_BTN($button_next, "button", "goMove(1)");
	$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");

} else { 
	$h_button = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['SaveAndFinish'], "button", "doSave()", "submitBtn")."&nbsp;";
	$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");
}

$objOEA_defalut = new liboea_setting();

$objSearch = new liboea_setting_search($objOEA_defalut);

//$objSearch->setCriteriaTitle("BALL");
//$objSearch->setSearchCategory("Activity");
//$result  =  $objSearch->getSuggestResult();


$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();

?>