<?
# modifying : 

$liboea = new liboea();
$_objOLE_ITEM = new libole_student_item();
$_objOEA_ITEM = new liboea_item();
$liboea_setting = new liboea_setting();

// check if deadline has past
if ($liboea->getIsSubmissionPeriod()==false) {
	No_Access_Right_Pop_Up();
}


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$OeaQuotaRemain = $_objOEA_ITEM->OeaQuotaRemain(); 

$h_instructionBox = showWarningMsg($Lang['General']['Instruction'], $Lang['iPortfolio']['OEA']['OLE_Selection_Instruction']);


$h_QuotaText = "
<br style='clear:both'>
	<div class='this_form' style='width:550'>
	<table class='form_table_v30'>
	<tr>
		<td class='field_title' width='35%'>".$Lang['iPortfolio']['OEA']['QuotaLeft']."</td>
		<td width='15%'>$OeaQuotaRemain</td>
		<td class='field_title' width='35%'>".$Lang['iPortfolio']['OEA']['NoOfRecordSelected']."</td>
		<td width='15%'><span id='TotalSpan'>0</span></td>
	</tr>
	</table><br>";

### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_List'], "javascript:js_Back_To_OEA_List()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_CreateFromOLE'], "");
$h_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# step information
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step1'], 1);
$STEPS_OBJ[] = array($Lang['iPortfolio']['OEA']['NewFromOLE_Step2'], 0);
$step_navigation = $linterface->GET_STEPS_IP25($STEPS_OBJ);

$default_oea_item = $liboea_setting->default_oeaItemOnlyArr;


# selected OEA Item (List)
$selectedOeaItemSql = $_objOEA_ITEM->get_Selected_Oea_Item_List_SQL();
$selectedOeaItem = $liboea->returnArray($selectedOeaItemSql);

# selected OEA Item (Table)
$h_selected_content = '<table class="common_table_list" width= "95%">';
$h_selected_content .= '<thead>';
$h_selected_content .= '<tr>';
$h_selected_content .= '<th width="1">#</th>';
$h_selected_content .= '<th width="30%">'.$Lang['iPortfolio']['OEA']['OLE_Title'].'</th>';
$h_selected_content .= '<th width="10%">'.$Lang['iPortfolio']['OEA']['SLPOrder'].'</th>';
$h_selected_content .= '<th width="10%">'.$Lang['iPortfolio']['OEA']['Participation'].'</th>';
$h_selected_content .= '<th width="20%">'.$ec_iPortfolio['date'].' / '.$iPort["period"].'</th>';
$h_selected_content .= '<th width="30%">'.$Lang['iPortfolio']['OEA']['MappedOEAItem'].'</th>';
//$h_selected_content .= '<th width="1"><input type="checkbox" onClick=(this.checked)?setChecked(1,this.form,\'recordID[]\'):setChecked(0,this.form,\'recordID[]\') onChange=\'countTotal()\'></th>';
$h_selected_content .= '</thead>';
for($i=0; $i<sizeof($selectedOeaItem); $i++) {
	if($selectedOeaItem[$i]['OEA_ProgramCode'] != "00000") {
		$item = $default_oea_item[$selectedOeaItem[$i]['OEA_ProgramCode']];
	} else {
		$item = $selectedOeaItem[$i]['TITLE'];
	}
	
	if($item=="") $item = "-";
	$date = $selectedOeaItem[$i]['OLE_StartDate'];
	if($selectedOeaItem[$i]['OLE_EndDate']!="") $date .= " ".$i_To." ".$selectedOeaItem[$i]['OLE_EndDate']; 
	if($date=="") $date = "-";
	$h_selected_content .= '<tr>';
	$h_selected_content .= '<td>'.($i+1).'</td>';
	$h_selected_content .= '<td>'.$selectedOeaItem[$i]['TITLE'].'</td>';
	$h_selected_content .= '<td>'.$selectedOeaItem[$i]['SLPOrder'].'</td>';
	$h_selected_content .= '<td>'.$selectedOeaItem[$i]['OLE_Type'].'</td>';
	$h_selected_content .= '<td>'.$date.'</td>';	
	$h_selected_content .= '<td>'.((($selectedOeaItem[$i]['Mapped']==0 && $item!="-") ? "<span class='tabletextrequire'>*</span>" : "")).$item.'</td>';	
	//$h_selected_content .= '<td>'.$selectedOeaItem[$i]['ACTION'].'</td>';
	$h_selected_content .= '</tr>';	
}
if(sizeof($selectedOeaItem)==0) {
	$h_selected_content .= '<td colspan="6" height="40" align="center">'.$i_no_record_exists_msg.'</td>';	
}
$h_selected_content .= '</table><br>';
$h_selected_layer = '<a class="tablelink" href="javascript:ShowHideLayer(\'selectedOeaFlag\', \'selectedOeaDiv\')">'.$Lang['iPortfolio']['OEA']['DisplaySelectedOeaItem'].'</a><div id="selectedOeaDiv" style="display:none">'.$h_selected_content.'</div>';


# OLE Item (List)
$ApplicableParticipationTypeArr['INT'] = $liboea->getAllowOEAPartByOLEType($ipf_cfg["OLE_TYPE_STR"]["INT"]);
$ApplicableParticipationTypeArr['EXT'] = $liboea->getAllowOEAPartByOLEType($ipf_cfg["OLE_TYPE_STR"]["EXT"]);
$AppicableOleTypeArr = array();
foreach ((array)$ApplicableParticipationTypeArr as $thisIntExt => $thisApplicableTypeArr) {
	if (count((array)$thisApplicableTypeArr) > 0) {
		$AppicableOleTypeArr[] = $thisIntExt;
	}
}
$sql = $_objOLE_ITEM->get_OLE_Student_List_SQL($AppicableOleTypeArr);

$data = $liboea->returnArray($sql);


# OLE Item (Table)
$h_content = '<table class="common_table_list" width= "95%">';
$h_content .= '<thead>';
$h_content .= '<tr>';
$h_content .= '<th width="1">#</th>';
$h_content .= '<th width="30%">'.$Lang['iPortfolio']['OEA']['OLE_Title'].'</th>';
$h_content .= '<th width="10%">'.$Lang['iPortfolio']['OEA']['SLPOrder'].'</th>';
$h_content .= '<th width="10%">'.$Lang['iPortfolio']['OEA']['Participation'].'</th>';
$h_content .= '<th width="20%">'.$ec_iPortfolio['date'].' / '.$iPort["period"].'</th>';
$h_content .= '<th width="30%">'.$Lang['iPortfolio']['OEA']['MappedOEAItem'].'</th>';
$h_content .= '<th width="1"><input type="checkbox" onClick=(this.checked)?setChecked(1,this.form,\'recordID[]\'):setChecked(0,this.form,\'recordID[]\') onChange=\'countTotal()\'></th>';
$h_content .= '</thead>';
for($i=0; $i<sizeof($data); $i++) {
	if($data[$i]['OEA_ProgramCode'] != "00000") {
		$item = $default_oea_item[$data[$i]['OEA_ProgramCode']];
	} else {
		$item = $data[$i]['TITLE'];
	}
	
	if($item=="") $item = "-";
	$date = $data[$i]['OLE_StartDate'];
	if($data[$i]['OLE_EndDate']!="") $date .= " ".$i_To." ".$data[$i]['OLE_EndDate']; 
	if($date=="") $date = "-";
	$h_content .= '<tr>';
	$h_content .= '<td>'.($i+1).'</td>';
	$h_content .= '<td>'.$data[$i]['TITLE'].'</td>';
	$h_content .= '<td>'.$data[$i]['SLPOrder'].'</td>';
	$h_content .= '<td>'.$data[$i]['OLE_Type'].'</td>';
	$h_content .= '<td>'.$date.'</td>';	
	$h_content .= '<td>'.((($data[$i]['Mapped']==0 && $item!="-") ? "<span class='tabletextrequire'>*</span>" : "")).$item.'</td>';	
	$h_content .= '<td>'.$data[$i]['ACTION'].'</td>';
	$h_content .= '</tr>';	
}
if(sizeof($data)==0) {
	$h_content .= '<td colspan="7" height="40" align="center">'.$i_no_record_exists_msg.'</td>';	
}
$h_content .= '</table>';

$IntExtAry[0][] = "INT";
$IntExtAry[0][] = $Lang['iPortfolio']['OEA']['OLE_Int']; 
$IntExtAry[1][] = "EXT";
$IntExtAry[1][] = $Lang['iPortfolio']['OEA']['OLE_Ext'];
$IntExtFilter = getSelectByArray($IntExtAry, 'name="IntExt" id="IntExt" onChange="document.form1.submit()"', $IntExt, 0, 0, $i_general_all);

$filter = $IntExtFilter;

//$h_msg_row = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>