<?

if(!isset($programID) || sizeof($programID)==0) {
	header("Location: index.php");
	exit;	
}

$programID = IntegerSafe($programID);

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['OEA_List'], "javascript:js_Back_To_OEA_List()");
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['Edit'], "");
$h_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$recordID = (is_array($programID)) ? intval($programID[0]) : intval($programID);

$liboea = new liboea();
$liboea_item = new liboea_item($recordID);
$liboea_setting = new liboea_setting();

// check if deadline has past
if ($liboea->getIsSubmissionPeriod()==false) {
	No_Access_Right_Pop_Up();
}


if($liboea_item->getOLE_STUDENT_RecordID()==0) {
	$liboea_item->setNewRecord($isNewRecord=1);	
}
$liboea_item->setEditMode(1);
$h_OEA_Edit_Table = $liboea_item->getOeaItemTable();

//$default_oea_all = $liboea_setting->get_OEA_Item();
//	if(sizeof($default_oea_all)>0) {
//		$js_default_item_array = "var itemAry = new Array();\n";
//		foreach($default_oea_all as $_key=>$_ary) {
//			$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
//		}
//	}
//	$h_js = $js_default_item_array."\n";
$h_js = $liboea->getOeaItemAryForJs();
	
# button 
$h_button = $linterface->GET_ACTION_BTN($button_save, "button", "doSave()", "submitBtn")."&nbsp;";
$h_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Back_To_OEA_List()");

if ($liboea->getAllowStudentApplyOthersOea()) {
	// no need checking
	$h_checkOthers = 0;
}
else {
	// check if user has selected "Others" in OEA
	$h_checkOthers = 1;
}

$js_css = $liboea->Include_JS_CSS();

$linterface->LAYOUT_START();

include_once($template_script);
$linterface->LAYOUT_STOP();
?>