<?
# modifying : 

$liboea = new liboea();
$_objOEA_ITEM = new liboea_item();
$liboea_setting = new liboea_setting();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

### Disclaimer
//$h_Disclaimer = $linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['iPortfolio']['OEA']['Disclaimer'].'</span>', $Lang['iPortfolio']['OEA']['Disclaimer_StudentView']);


if(isset($SelectedParticipation) && $SelectedParticipation!="") {
	$conds = " AND Participation='$SelectedParticipation'";
}
		
	
$liboea->set_SQL_Condition($conds);	

$sql = $liboea->getOEA_StudentList_SQL();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;

$li = new libdbtable2007($field, $order, $pageNo);

$li->sql = $sql;
$li->field_array = array("TITLE_ORDER", "CATEGORY", "ROLE", "ACHIEVEMENT", "PARTICIPATION", "MODIFYDATE", "RECORDSTATUS");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "OEA_StudentList";

$OeaQuotaRemain = $_objOEA_ITEM->OeaQuotaRemain(); 

$period = $liboea->getDisplayStudentSubmissionPeriod();
$totalQuota = $_objOEA_ITEM->getMaxNoOfOea();
# Quote text
$h_TableText = "
<br style='clear:both'>
	<div class='this_form' style='width:550'>
	<table class='form_table_v30'>
	<tr>
		<td class='field_title_short' width='15%'>".$Lang['iPortfolio']['OEA']['SubmissionPeriod']."</td>
		<td width='35%'>$period</td>
		<td class='field_title' width='35%'>".$Lang['iPortfolio']['OEA']['MaxNoOfOeaAllow_StudentView']."</td>
		<td width='15%'>$totalQuota</td>
	</tr>
	</table><br>";


# Participation Filter 
$participationAry = $_objOEA_ITEM->getDefaultParticipation($associateAry=0);
$participationSelect = getSelectByArray($participationAry, 'name="SelectedParticipation" id="SelectedParticipation" onChange="goSubmit()"', $SelectedParticipation, 0, 0, $i_general_all);

# OEA Category Filter 
$categoryAry = $liboea_setting->get_OEA_Default_Category($associateAry=0);
$categorySelect = getSelectByArray($categoryAry, 'name="CategoryID" id="CategoryID" onChange="goSubmit()"', $CategoryID, 0, 0);

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='25%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Title'])."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Category'])."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Role'])."</th>\n";
$li->column_list .= "<th width='20%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Achievement'])."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['iPortfolio']['OEA']['Participation'])."</th>\n";
$li->column_list .= "<th width='10%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $last_modified)."</th>\n";
$li->column_list .= "<th width='5%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $iPort["status"])."</th>\n";
$li->column_list .= "<th width='1' class='tabletop tabletoplink'>".$li->check("programID[]")."</th>\n";

$h_content = '';
$h_content .= $li->display();
$h_content .= '<span class="tabletextremark" style="float:left;">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['OEA']['RepresentMappedFromOLE'].'</span>'."\n";

//$h_submission_period = $liboea->getDisplayStudentSubmissionPeriod();
	
$isSubmissionPeriod = $liboea->getIsSubmissionPeriod();


$displayNewButton = $_objOEA_ITEM->checkDisplayNewButton();
if($isSubmissionPeriod && $displayNewButton) {

	$h_new_button = '';
	$h_new_button .= '<div class="content_top_tool">';
		$h_new_button .= '<div class="Conntent_tool">';
			$h_new_button .= '<div class="btn_option">';
				$h_new_button .= '<a href="javascript:;" class="new" id="btn_new" onclick="MM_showHideLayers(\'new_option\',\'\',\'show\');document.getElementById(\'btn_new\').className = \'new parent_btn\';"> '.$button_new.'</a>';
				$h_new_button .= '<br style="clear:both" />';
				$h_new_button .= '<div class="btn_option_layer" id="new_option" onclick="MM_showHideLayers(\'new_option\',\'\',\'hide\');document.getElementById(\'btn_new\').className = \'new\';">';
					$h_new_button .= '<a href="javascript:goAdd(1)" class="sub_btn">'.$Lang['iPortfolio']['OEA']['New_From_OLE'].'</a>';
					
					if ($liboea->getAllowStudentNewOEA_NotFromOLE()) {
						$h_new_button .= '<a href="javascript:goAdd(2)" class="sub_btn">'.$Lang['iPortfolio']['OEA']['New_Record'].'</a>';
					}
					
				$h_new_button .= '</div>';	  
			$h_new_button .= '</div>';	
		$h_new_button .= '</div>';
	$h_new_button .= '</div>';
}

$h_msg_row = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$h_confirm_delete_msg = $i_Usage_RemoveConfirm;

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>