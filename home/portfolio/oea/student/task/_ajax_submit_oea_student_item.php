<?php
# modifying : henry chow
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_opendb();

$liboea_setting = new liboea_setting();
$_objOEA_ITEM = new liboea_item();

$oeaItemAry = $liboea_setting->get_OEA_Default_Item($returnAssociAry=1);

$dataAry = array();

$dataAry['StudentID'] = $UserID;
$dataAry['Title'] = $oeaItemAry[$ItemCode];
$dataAry['StartYear'] = $startdate;
$dataAry['EndYear'] = $enddate;
$dataAry['Participation'] = $participation;
$dataAry['ParticipationNature'] = $ParticipationNature;
$dataAry['Role'] = $RoleID;
$dataAry['Achievement'] = $AchievementID;
$dataAry['OEA_ProgramCode'] = $ItemCode;
$dataAry['OLE_STUDENT_RecordID'] = $currentRecordID;
$dataAry['OLE_PROGRAM_ProgramID'] = $ole_ProgramID;
$dataAry['RecordStatus'] = $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVE"];

$result = $_objOEA_ITEM->save($dataAry);

intranet_closedb();
//echo $_objOEA_ITEM->recordID.'/';
echo $result ? $Lang['iProtfolio']['OEA']['ItemSaveSuccessfully'] : $Lang['iProtfolio']['OEA']['ItemSaveUnsuccessfully'];
?>