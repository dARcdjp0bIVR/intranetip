<?php
include_once($PATH_WRT_ROOT."includes/libpagination.php");

$lpagination = new libpagination();

$CurPage = ($_GET['CurPage']=='')? 1 : $_GET['CurPage'];
$NumPerPage = ($_GET['NumPerPage']=='')? 100 : $_GET['NumPerPage'];


$lpagination->setTotalNumOfResult(500);
$lpagination->setNumPerPage($NumPerPage);
$lpagination->setCurPage($CurPage);
$lpagination->setJsFunctionName('js_Reload_Page');

$linterface->LAYOUT_START();
echo $lpagination->getPaginationHtml();
$linterface->LAYOUT_STOP();
?>
<script language="javascript">
function js_Reload_Page(GoToPage, NumPerPage) {
	window.location = '?task=test_pagination&CurPage=' + GoToPage + '&NumPerPage=' + NumPerPage;
}
</script>