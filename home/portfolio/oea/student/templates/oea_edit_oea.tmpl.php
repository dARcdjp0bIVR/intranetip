<?
# modifying : 

echo $h_TopTabMenu;
echo '<br style="clear:both"/>'."\n";
echo $h_navigation;
echo '<br style="clear:both"/>'."\n";
echo $step_navigation;
echo '<br style="clear:both"/>'."\n";
echo $h_msg_row;

?>

<script language="javascript">
var jsDescMaxLength = '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxLength"]?>';

$(document).ready( function() {	
	js_Onkeyup_Info_Textarea('Description', 'DescCharCountSpan', '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxWordCount"]?>', '<?=$oea_cfg["OEA_STUDENT_Description"]["MaxLength"]?>', 'char');
});

function js_Back_To_OEA_List() {
	self.location.href = "index.php";	
}

function changeAwardSetting(val) {
	changeParticipation(val);	
	checkAchievement(val);
}

function checkAchievement(val) {
	if(val==1) 
		document.form1.AchievementID.disabled = false;
	else 
		document.form1.AchievementID.disabled = true;
	//document.form1.AchievementID.selectedIndex = 0;
	$('select#AchievementID').val('');
}

function changeParticipation(val) {
	var participationNature = document.form1.elements["ParticipationNature"];
	while (participationNature.options.length > 0) {
	    participationNature.options[0] = null;
	}
	participationNature.options[0] = new Option("-- <?=$button_select?> --", '');

	if(val==1) {
		participationNature.options[1] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["C"]?>","C", true);
		participationNature.options[2] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["N"]?>","N");
		participationNature.options[3] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["P"]?>","P");
	}
	else {
		participationNature.options[1] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["N"]?>","N");
		participationNature.options[2] = new Option("<?=$oea_cfg["OEA_ParticipationNature_Type"]["P"]?>","P");
	}
	//participationNature.selectedIndex = 0;	
}

function Edit_Oea_Title() {
	$('#oea_category_others').click();	
}

function Show_Advance_Search()
{

	var ole_title = document.getElementById('ole_title').value;
	$.post(
		"index.php", 
		{ 
			ole_title: ole_title,
			task: "ajax",
			script: "ajax_advance_search"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);	
}

function Show_SuggestionList()
{

	
	var ole_title = $('input#ole_title').val();
	
	$.post(
		"index.php", 
		{ 
			ole_title: ole_title,
			task: "ajax",
			script: "ajax_get_suggestion_list"
			 },
		function(ReturnData)
		{
			//alert(ReturnData);
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);	
}


function checkOEAcategory(itemCode) {
	document.getElementById('span_OEA_Category').innerHTML = itemAry[itemCode][1];
	document.getElementById('CatCode').value =  itemAry[itemCode][0];
}

function CopyOthers() {
	<? if($liboea_item->getNewRecord()) { ?>
		var oea_title = "<input type='text' name='oea_title' id='oea_title' value='"+$('input#stored_oea_title').val()+"' size='40'>";
		document.getElementById('selectedItemSpan').innerHTML = "<b>"+oea_title+"</b>";
		document.getElementById('oea_title').focus();
	<? } else {?>	
	var ole_title = $('input#ole_title').val();
	document.getElementById('selectedItemSpan').innerHTML = "<b>"+ole_title+"</b><input type='hidden' name='oea_title' id='oea_title' value='<?=intranet_htmlspecialchars($ole_title)?>'>";
	$('input#oea_title').val(document.form1.ole_title.value);
	<? } ?>
	
	$('input#ItemCode').val('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	checkOEAcategory('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	$('#MapOptions').hide('slow');
	
	//$('textarea#Description').attr('readonly', '');
}

function js_show_layer(layername) {
	$('#'+layername).show('slow');
}

function doSave() {
	$('.formbutton_v30').attr('disabled', 'disabled');
	
	var success = checkForm();
	if(success) {
		$('input#task').val('ajax');
		$('input#script').val('ajax_validate');
		$('input#action').val('StudentAppliedOea');
		
		var PostString = Get_Form_Values(document.getElementById("form1")) + '&fromEdit=1';
		var PostVar = PostString;
		
		$.post(
			'index.php',
			PostVar,
			function(data){
				var jsCanSubmit = true;
				if (data == '1') {
					if (!confirm('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['AppliedOeaAlready']?>')) {
						jsCanSubmit = false;
					}
				}
				
				if (jsCanSubmit) {
					$('input#task').val('ajax');
					$('input#script').val('ajax_submit_oea_student_item');
					
					var PostString = Get_Form_Values(document.getElementById("form1"));
					var PostVar = PostString;
				
					// modified by Ivan on 8 Nov 2011 - change the coding to call index.php
					//$.post('../ajax/ajax_submit_oea_student_item.php',PostVar,
					$.post('index.php',PostVar,
						function(data){
							//alert(data);
							if (data == "die") 
								window.top.location = '/';
							else {
								$('input#msg').val(data);
								$('input#task').val('index');
								document.form1.submit();
							}
							//document.getElementById('tempDisplay').innerHTML = data;
						});
				}
				else {
					$('.formbutton_v30').attr('disabled', '');
				}
			}
		);
	}
	else {
		$('.formbutton_v30').attr('disabled', '');
	}
}

function checkForm() {
	var obj = document.form1;
	
	if(obj.ItemCode.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Title']?>");
		//obj.ItemCode.focus();
		return false;	
	}	
	else if(obj.startdate.value=="" || isNaN(obj.startdate.value) || obj.startdate.value.length!=4) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_StartDate']?>");
		obj.startdate.focus();
		return false;
	}
	else if(obj.enddate.value=="" || isNaN(obj.enddate.value) || obj.enddate.value.length!=4) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_EndDate']?>");
		obj.enddate.focus();
		return false;
	}
	else if(obj.startdate.value>obj.enddate.value) {
		alert("<?=$Lang['ePOS']['jsWarningArr']['StartDateCannotGreaterThanEndDate']?>");
		obj.startdate.focus();
		return false;		
	}
	else if(obj.RoleID.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Role']?>");
		obj.RoleID.focus();
		return false;				
	}
	else if(obj.ParticipationNature.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature']?>");
		obj.ParticipationNature.focus();
		return false;						
	}
	<? /* if($defaultAwardBearing) { */ ?>
	else if(obj.AchievementID.value!="" && obj.AchievementID.value!="N" && obj.ParticipationNature.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature']?>");
		obj.ParticipationNature.focus();
		return false;						
	}
	//else if(obj.awardBearing[0].checked==true && obj.AchievementID.value=="") {
	else if(($('input#awardBearing').val()=='Y' || $('input#awardBearing1').attr('checked')==true) && obj.AchievementID.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Achievement']?>");
		obj.AchievementID.focus();
		return false;				
	}
	<? /* } */ ?>
	//else if(obj.Participation[0].checked==false && obj.Participation[1].checked==false) {
	else if(($('input#Participation').val=='') || ($('input#Participation_S').attr('checked')==false && $('input#Participation_P').attr('checked')==false)) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['iPortfolio']['OEA']['Checking']['OEA_Participation']?>");
		//obj.Participation[0].focus();
		$('input#Participation_S').focus();
		return false;						
	}
	else if (obj.ItemCode.value=='<?=$oea_cfg["OEA_Default_Category_Others"]?>' && Trim($('textarea#Description').val())=='') {
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Description']?>');
		$('textarea#Description').focus();
		return false;
	}
	else if (parseInt(js_Get_Jupas_Character_Count($('textarea#Description').val())) > parseInt(jsDescMaxLength)) {
		var jsWarningMsg = js_Get_Content_Exceed_Maximum_Warning_Msg('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['Description_Greater_Than_The_Max']?>', jsDescMaxLength);
		alert(jsWarningMsg);
		$('textarea#Description').focus();
		return false;
	}
	else if ('<?=$h_checkOthers?>' == '1' && obj.ItemCode.value == '<?=$oea_cfg["OEA_Default_Category_Others"]?>') {
		alert('<?=$Lang['iPortfolio']['OEA']['jsWarningArr']['NotAllowToApplyOthers']?>');
		return false;
	}
	
	return true;
}

<?=$h_js?>

</script>


<?=$js_css?>
<span id="tempDisplay"></span>
<form name ="form1" id="form1" method="post" value="index.php">
	
	<?=$h_OEA_Edit_Table?>

<br>
<div class="edit_bottom_v30">
		<?=$h_button?>
	
	
</div>


<input type="hidden" name="task" id="task" value="index">
<input type="hidden" name="script" id="script" value="">
<input type="hidden" name="PATH_WRT_ROOT" id="PATH_WRT_ROOT" value="<?=$PATH_WRT_ROOT?>">
<input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>">
<input type="hidden" name="action" id="action" value="">
<input type="hidden" name="msg" id="msg" value="">


</form>
