<?
echo $js_css;

echo $h_TopTabMenu;
echo '<br style="clear:both"/>'."\n";
echo $h_navigation;
echo '<br style="clear:both"/>'."\n";
echo $step_navigation;
echo '<br style="clear:both"/>'."\n";
echo $h_msg_row;

?>


<script language="javascript">
<?=$h_js?>


function goMove(val) {
	document.getElementById('currentPos').value = parseInt(document.getElementById('currentPos').value) + val;
	document.form1.submit();
}

function doSave() {

	var success = checkForm();
	if(success) {
		document.getElementById('submitBtn').disabled = true;
		//$('input#task').val('ajax');
		//$('input#script').val('ajax_submit_oea_student_item');
		
		var PostString = Get_Form_Values(document.getElementById("form1"));
		var PostVar = PostString;
	
		$.post('../ajax/ajax_submit_oea_student_item.php',PostVar,
		//$.post('index.php',PostVar,
			function(data){
				//alert(data);
				if (data == "die") 
					window.top.location = '/';
				else {
					//Get_Return_Message(data);
					$('input#msg').val(data);
					goMove(1);
				}
				//document.getElementById('tempDisplay').innerHTML = data;
			});
	}

}


function checkAchievement(val) {
	if(val==1) 
		document.form1.AchievementID.disabled = false;
	else 
		document.form1.AchievementID.disabled = true;
	
}

function checkForm() {
	var obj = document.form1;
	
	if(obj.ItemCode.value=="") {
		alert("Please select Title");
		obj.ItemCode.focus();
		return false;	
	}	
	else if(obj.CatCode.value=="") {
		alert("Please select Title");
		obj.CatCode.focus();
		return false;
	}
	else if(obj.startdate.value=="" || isNaN(obj.startdate.value) || obj.startdate.value.length!=4) {
		alert("Please fill in valid Start Date");
		obj.startdate.select();
		return false;
	}
	else if(obj.enddate.value=="" || isNaN(obj.enddate.value) || obj.enddate.value.length!=4) {
		alert("Please fill in valid End Date");
		obj.enddate.select();
		return false;
	}
	else if(obj.startdate.value>obj.enddate.value) {
		alert("Start Date should be earlier than End Date");
		obj.startdate.select();
		return false;		
	}
	else if(obj.RoleID.value=="") {
		alert("Please select Role");
		obj.RoleID.focus();
		return false;				
	}
	<? if($defaultAwardBearing) { ?>
	else if(obj.AchievementID.value!="" && obj.AchievementID.value!="N" && obj.ParticipationNature.value=="") {
		alert("Please select Participation Nature");
		obj.ParticipationNature.focus();
		return false;						
	}
	else if(obj.awardBearing[0].checked==true && obj.AchievementID.value=="") {
		alert("Please select Achievement");
		obj.AchievementID.focus();
		return false;				
	}
	<? } ?>
	return true;
}

function js_Back_To_OEA_List() {
	self.location.href = "index.php";	
}

function checkOEAcategory(itemCode) {
	document.getElementById('span_OEA_Category').innerHTML = itemAry[itemCode][1];
	document.getElementById('CatCode').value =  itemAry[itemCode][0];
}

function Hide_Table(table_name, hide_btn_name, show_btn_name)
{
	$('#'+ table_name).slideUp();
	$('#'+ hide_btn_name).attr('style', 'display:none');
	$('#'+ show_btn_name).attr('style', '');
}

function Show_Table(table_name, hide_btn_name, show_btn_name)
{
	$('#'+ table_name).slideDown();
	$('#'+ hide_btn_name).attr('style', 'display:none');
	$('#'+ show_btn_name).attr('style', '');
}

function Show_Advance_Search()
{
	var currentRecordID;
	$.post(
		"../ajax/ajax_advance_search.php", 
		{ 
			currentRecordID: currentRecordID,
			PATH_WRT_ROOT: "<?=$PATH_WRT_ROOT?>"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);			
		}
	);	
}

function displaySelectedItem(obj) {
	$('#tempItemCode').val(obj.value);
	
}

function changeAwardSetting(val) {
	checkAchievement(val);	
}
</script>
<span id="tempDisplay"></span>
<form name ="form1" method="post" value="index.php" id="form1">
<span id="spanOuter">

<?=$countText?>
<?=$h_result?>

<br>
<div class="edit_bottom_v30">
<?=$h_button?>
</div>
</span>

<?=$h_HiddenOLE_Item?>

<input type="hidden" name="CatCode" id="CatCode" value="">

<input type="hidden" name="task" id="task" value="<?=$task?>">
<input type="hidden" name="script" id="script" value="">
<input type="hidden" name="msg" id="msg" value="">

</form>

<script language="javascript">
function init() {
	checkAchievement(<?=$defaultAwardBearing?>);
}
<? if($defaultAwardBearing) {?>
init();
<?}?>
<? if($msg!="") { ?>
Get_Return_Message("<?=$msg?>");	
<? } ?>
</script>
