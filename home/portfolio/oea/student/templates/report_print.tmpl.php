<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$Lang['Header']['HomeTitle']?></title>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/jupas_report.css" rel="stylesheet" type="text/css">
</head>


<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>


<body>
<div class="report_div_landscape">	
	<?=$h_printButton?>
	<u><h1><?=$h_reportTitle?></h1></u>
	
	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
		<tr>
			<td width ="15%" align="left">
				<b>Name</b>
			</td>
			<td width ="35%" align="left"><?=$h_studentName?>&nbsp;</td>
			<td width ="15%"  align="left"><b>Application number</b></td>
			<td width ="35%" align="left"><?=$h_studentHKJApplNo?>&nbsp;</td>
		</tr>
		<tr>
			<td width ="15%" align="left"><b>Class</b></td>
			<td width ="35%" align="left"><?=$h_studentClassName?>&nbsp;<?=$h_studentClassNumber?>&nbsp;</td>
			<td width ="15%" align="left"><b>Print Date</b></td>
			<td width ="35%" align="left"><?=$h_printDate?>&nbsp;</td>
		</tr>
	</table>
	<br/>
	<?=$h_table?>
</div>
</body>
</html>