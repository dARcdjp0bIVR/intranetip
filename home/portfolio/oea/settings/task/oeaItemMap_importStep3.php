<?php
@SET_TIME_LIMIT(21600);
$numOfCsvData = $_POST['numOfCsvData'];
$OverrideStudentOea = $_POST['OverrideStudentOea'];


$liboea = new liboea();


$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['itemMap'], "javascript:js_Go_Back();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$h_Navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Steps Table
$h_StepTable = $linterface->GET_IMPORT_STEPS($CurrStep=3);


### Thickbox loading message
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfCsvData, $Lang['General']['ImportArr']['RecordsProcessed']);


### Buttons
$BackBtn = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['BackToOLEItemMappingList'], "button", "js_Back_To_OLE_List();");
$ImportOtherBtn = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['ImportOtherMapping'], "button", "js_Back_To_Import_Step1();");


### iFrame
$h_iFrameSrc = "index.php?task=ajax&script=ajax_update&action=Import_OleOeaItemMapping&OverrideStudentOea=$OverrideStudentOea";


$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>