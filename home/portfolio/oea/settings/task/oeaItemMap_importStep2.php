<?php
@SET_TIME_LIMIT(21600);

$liboea = new liboea();
$libimport = new libimporttext();
$libfs = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$OverrideStudentOea = $_POST['OverrideStudentOea'];

if (!$libimport->CHECK_FILE_EXT($name))
{
	intranet_closedb();
	header("location: index.php?task=oeaItemMap_importStep1");
	exit();
}

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));

### Override Student OEA Records Warning
if ($OverrideStudentOea) {
	$h_overrideStudentOeaWarning = $linterface->GET_WARNING_TABLE($Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecords'].' '.$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecordsImport']);
}


### move to temp folder first for others validation
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('iportfolio/jupas/ole_oea_mapping/', $csvfile, $name);


### Validate file header format
$DefaultCsvHeaderArr = $liboea->getOLEMappingImportColumnTitleArray();
$DefaultCsvHeaderArr = $DefaultCsvHeaderArr['En'];
$ColumnPropertyArr = $liboea->getOLEMappingImportColumnPropertyArray($forGetAllCsvContent=1);

$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
$CsvHeaderArr = array_shift($CsvData);
$numOfCsvData = count($CsvData);

$CsvHeaderWrong = false;
for($i=0; $i<$numOfDefaultCsvHeader; $i++)
{
	if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i])
	{
		$CsvHeaderWrong = true;
		break;
	}
}

if($CsvHeaderWrong || $numOfCsvData==0)
{
	$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
	intranet_closedb();
	header("location: index.php?task=oeaItemMap_importStep1&ReturnMsgKey=".$ReturnMsgKey);
	exit();
}




### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['OEA']['itemMap'], "javascript:js_Go_Back();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$h_Navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Steps Table
$h_StepTable = $linterface->GET_IMPORT_STEPS($CurrStep=2);


### Thickbox loading message
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfCsvData, $Lang['General']['ImportArr']['RecordsValidated']);


### iFrame Source
$h_iFrameSrc = "index.php?task=ajax&script=ajax_validate&action=Import_OleOeaItemMapping&TargetFilePath=".$TargetFilePath;


### Buttons
$ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled=1);
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");


$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>