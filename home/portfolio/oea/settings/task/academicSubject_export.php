<?php
$liboea_academic = new liboea_academic();
$lexport = new libexporttext();


$ColHeader = array();
$ColHeader[] = 'Subject Code';


$SubjectInfoArr = $liboea_academic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=0);
$numOfSubject = count($SubjectInfoArr);

$ExportArr = array();
for ($i=0; $i<$numOfSubject; $i++) {
	$ExportArr[$i][0] = $SubjectInfoArr[$i]['JupasSubjectCode'];
}
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $ColHeader, ",", "\r\n", ",", 0, "11", 1);


intranet_closedb();
$filename = 'jupas_subject_code.csv';
$lexport->EXPORT_FILE($filename, $export_content);
?>