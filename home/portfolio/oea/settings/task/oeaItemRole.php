<?
### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_oea_settings_ole_mapping_page_size", "numPerPage");
$arrCookies[] = array("ck_oea_settings_ole_mapping_page_number", "pageNo");
$arrCookies[] = array("ck_oea_settings_ole_mapping_order", "order");
$arrCookies[] = array("ck_oea_settings_ole_mapping_field", "field");
$arrCookies[] = array("ck_oea_settings_ole_mapping_Keyword", "Keyword");	
$arrCookies[] = array("ck_oea_settings_ole_mapping_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("ck_oea_settings_ole_mapping_ProgramNature", "ProgramNature");
$arrCookies[] = array("ck_oea_settings_ole_mapping_MappingStatus", "MappingStatus");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$Keyword = '';
	$ProgramNature = '';
	$MappingStatus = '';
	$ck_oea_settings_ole_mapping_page_size = '';
}
else 
	updateGetCookies($arrCookies);
	

$liboea = new liboea();

$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU(libpf_tabmenu::getJupasAdminOEASettingsTags('ADMIN_SETTING_OEA', $task));

### Import Button
$btn_Import = $linterface->Get_Content_Tool_v30("import","javascript:js_Go_Import();");

### Export Button
$btn_Export = $linterface->Get_Content_Tool_v30("export","javascript:js_Go_Export();");


### Search box
$SearchboxDiv = $linterface->Get_Search_Box_Div("Keyword", $Keyword, 'onkeyup="js_Changed_Keyword(this.value, event);"');


### Filters
// Academic Year Selection
$h_AcademicYearSelection = getSelectAcademicYear('AcademicYearID', 'onchange="js_Changed_Filtering(this.id, this.value);"', $noFirst=1, $noPastYear=0, $AcademicYearID, $All=1);

// Program Nature Selection
$h_ProgramNatureSelection = $liboea->Get_OLE_Nature_Selection('ProgramNature', $ProgramNature, 'js_Changed_Filtering(this.id, this.value);');

// Mapping Status Selection
$h_MappingStatusSelection = $liboea->Get_Mapping_Status_Selection('MappingStatus', $MappingStatus, 'js_Changed_Filtering(this.id, this.value);');

### Info Table => Load by ajax
//$h_dbtable = $liboea->Get_Settings_OLE_Mapping_Index_DBTable($field, $order, $pageNo);


$h_Include_Js_Css = $liboea->Include_JS_CSS();

$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
?>