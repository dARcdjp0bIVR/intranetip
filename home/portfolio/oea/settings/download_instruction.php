<?php


# modifying : 

/******************************************************************************
 * Modification log:
 * 
 * Yuen (2011-12-05):
 * 			use AJAX to prepare for the download of mapping template
 * 
 * 
 *****************************************************************************/
 
 
	$PATH_WRT_ROOT = "../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
	
	intranet_auth();

	# Create a new interface instance
	$linterface = new interface_html("popup_no_layout.html");
?>

<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script> 
<script language="javascript">

var IsStop = false;

function jsCancel()
{
	if (confirm("<?=$Lang['MassMailing']['ConfirmToTerminate']?>"))
	{
		IsStop = true;
		self.parent.tb_remove()
	}		
}

function jsClose()
{
	IsStop = true;
	self.parent.tb_remove();
}

function jsPrepareDownload(StartIndex, e) {

	var parentForm = self.parent.document.form1;
	parentForm.StartIndex.value = StartIndex;
	parentForm.task.value = "oeaItemMap_export";

	if (document.frmDownload.secretFileKey.value=="")
	{
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hours = currentTime.getHours();
		var minutes = currentTime.getMinutes();
		timestr = year+"-"+month+"-"+day+"_"+hours+minutes;
		//document.frmDownload.secretFileKey.value = currentTime.getTime() + parentForm.phKey.value;
		document.frmDownload.secretFileKey.value = timestr + parentForm.phKey.value;
	}
	parentForm.secretFileKey.value = document.frmDownload.secretFileKey.value;
	$.post(
		"index.php", 		
		$("#form1").serialize(),
		function(response)
		{
			//alert(response);
		      rData = response.split("|");
		      CurStatus = rData[0];
		      TotalRecipients = rData[1];
		      CurRecipients = rData[2];
		      OtherNotice = rData[3];
		      if (TotalRecipients>0)
		      {
			      ProgressPercentage = Math.round(100*(CurRecipients/TotalRecipients));
			      $('#progressbarA').progressBar(ProgressPercentage);
		      }
		      if (OtherNotice!="")
		      {
		      	OtherNotice = "<br /><?=$Lang['iPortfolio']['OEA']['download_time_needed_1']?>" + OtherNotice + "<?=$Lang['iPortfolio']['OEA']['download_time_needed_2']?>";
		      }
		      $('#ProgressMessage').html("<strong><?=$Lang['iPortfolio']['OEA']['download_notes1']?> "+TotalRecipients+" <?=$Lang['iPortfolio']['OEA']['download_notes2']?> </strong>"+OtherNotice);
			  if (CurStatus=="ToContinue")
			  //if (CurStatus.search("ToContinue") != -1)
			  {
			  	jsPrepareDownload(CurRecipients);
			  } else
			  {
			  	$('#ActionButtonCancel').hide();
			  	$('#ActionButtonClose').show();
			  	
			  	self.location = "/templates/get_sample_csv.php?path=../file/iportfolio/tmp/&file=oea_mapping_tmp_"+parentForm.secretFileKey.value+".csv";
			  }
		}
	);
}

$(container).ready(function() {
  //jsSendEmail(0);
});


function js_Go_Download()
{
	$("#divInstruction").hide();
	$("#divDownload").show(300);
	jsPrepareDownload(0);
}
</script>

<form id="frmDownload" name="frmDownload" >

<div id="divInstruction">
	<table width="95%" align="center" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
		<?=$Lang['iPortfolio']['OEA']['DownloadMappingInstructions']?> 
		</td>
	</tr>
	</table>
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
						<tr>
							<td align="center">
					<?= $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['DownloadTemplateInstructionConfirm'], "button", "js_Go_Download();") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.parent.tb_remove()") ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
</div>

<div id="divDownload" style="display:none;">

<script>
	$(document).ready(function() {
		$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
	});
</script>
<style>
	#container { width: 80%; margin-left: 10%; margin-top: 30px;}
</style>
<div id="container" align="center">
	<div style="padding-bottom: 25px;">
		<div id="ProgressMessage"><?=$Lang['iPortfolio']['OEA']['download_start']?></div>
		<table align="center">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarA">0%</span></td></tr>
		</table>
		<p id="ActionButtonCancel"><?= $linterface->GET_ACTION_BTN($button_cancel, "button", "jsCancel()")?></p>
		<p id="ActionButtonClose" style="display:none"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "jsClose()")?></p>
	</div>
</div>


</div>

<input type="hidden" name="secretFileKey" id="secretFileKey" />
</form>
