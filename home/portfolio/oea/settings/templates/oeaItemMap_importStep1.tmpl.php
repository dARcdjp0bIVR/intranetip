<?


# modifying : 

/******************************************************************************
 * Modification log:
 * 
 * Yuen (2011-12-05):
 * 			use AJAX to prepare for the download of mapping template
 * 
 * 
 *****************************************************************************/


# random a key and it will be combined with timestamp by JS later
$phKey = "r".rand(10,10000)."_".$UserID;
 
echo $h_TopTabMenu;
?>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<script>
$(document).ready( function() {	
	
});

function js_Continue()
{
	if ($('input#csvfile').val() == '') {
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		$('input#csvfile').focus();
		return false;
	}
	
	$('input#task').val('oeaItemMap_importStep2');
	$('form#form1').submit();
}

function js_Go_Back()
{
	window.location = 'index.php?task=oeaItemMap';
}


function js_Go_Download_OEA_Mapping()
{
	//$('div.WarningDiv').hide();

	var jsValid = true;
	if ($('input[name=OLENatureArr\\[\\]]:checked').length == 0)
	{
		$('div#WarningDiv_OLENature').show();
		jsValid = false;
	}
	
	if ($('input[name=MappingStatusArr\\[\\]]:checked').length == 0)
	{
		$('div#WarningDiv_MappingStatus').show();
		jsValid = false;
	}
	
	if ($('input[name=FormIDArr\\[\\]]:checked').length == 0) {
		$('div#WarningDiv_Form').show();
		jsValid = false;
	}
	
	if (jsValid) {
		//$('input#task').val('oeaItemMap_export');
		//$('form#form1').submit();
		$('#DownloadOEAMappingTmp').click();
	} else
	{
		//self.parent.tb_remove();
	}
}


</script>

<form method="POST" name="form1" id="form1" action="index.php" enctype="multipart/form-data">
	<?=$h_Navigation?>
	<br style="clear:both;" />
	<?=$h_StepTable?>
	<div class="table_board">
		<table id="html_body_frame" width="100%">
			<tr><td align="center"><?=$h_RemarksTable?></td></tr>
			<tr>
				<td>
					<table class="form_table_v30">
						<tr>
							<td class="field_title"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext">
								<input class="file" type="file" name="csvfile" id="csvfile">
								<br />
								<span class="tabletextrequire"><?=$Lang['General']['ImportArr']['SecondRowWarn']?></span>
								<br/>
								<?=$h_overrideStudentOeaChk?>
								<br/>
								<fieldset class="instruction_box">
									<legend class="instruction_title"><?=$Lang['General']['Remark']?></legend>
									<span>
										<?=$Lang['iPortfolio']['OEA']['ImportOtherAutoMapTitleMsg']?>
										<br/>
										<br/>
										<?=$Lang['iPortfolio']['OEA']['ImportOtherWarningMsg']?>
									</span>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['iPortfolio']['OEA']['OEAProgramInfoFile']?></td>
							<td>
								<!--<?=$h_DownloadOEACsvFile?>-->
								<?=$Lang['iPortfolio']['OEA']['OLE_OLE_Mapping_Warning']?>
								<!--
									<br />
									<br />
									<span class="tabletextrequire"><?=$Lang['iPortfolio']['OEA']['Disclaimer']?></span>
									<br />
									<?=$Lang['iPortfolio']['OEA']['Disclaimer_TeacherView']?>
								-->
							</td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['iPortfolio']['OEA']['OLEProgramMappingFile']?></td>
							<td>
								<table>
									<tr>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$Lang['iPortfolio']['OEA']['ProgramNature']?>:</td>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$h_InternalChk?></td>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$h_ExternalChk?></td>
									</tr>
									<tr><td colspan="3" style="border:0px;padding:0px;padding-right:10px;"><?=$h_OLENatureWarning?></td></tr>
									
									<tr>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$Lang['iPortfolio']['OEA']['MappingStatus']?>:</td>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$h_MappedChk?></td>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$h_NotMappedChk?></td>
									</tr>
									<tr><td colspan="3" style="border:0px;padding:0px;padding-right:10px;"><?=$h_MappingStatusWarning?></td></tr>
									
									<tr>
										<td style="border:0px;padding:0px;padding-right:10px;"><?=$Lang['iPortfolio']['OEA']['TargetForm']?>:</td>
										<td colspan="2" style="border:0px;padding:0px;padding-right:10px;"><?=$h_FormCheckboxesTable?></td>
									</tr>
									<tr><td colspan="3" style="border:0px;padding:0px;padding-right:10px;"><?=$h_FormCheckboxesWarning?></td></tr>
									
									<tr>
										<td colspan="3" style="border:0px;padding:0px;">
											<?=$h_DownloadOLECsvFile?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="field_title"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td><?=$ColumnDisplay . "<br />&nbsp;&nbsp;&nbsp;&nbsp;" . $Lang['iPortfolio']['OEA']['OLEMappingRemarks']?></td>
						</tr>
						<tr>
							<td class="tabletextremark" colspan="2">
								<?=$h_MandatoryRemarks?>
								<?=$h_ReferenceRemarks?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<a id="DownloadOEAMappingTmp" href='download_instruction.php?MassMailingID=58&width=780' title="<?=$Lang['iPortfolio']['OEA']['DownloadTemplateInstruction']?>" class='thickbox''></a>
	<div class="edit_bottom_v30">
		<?=$h_ContinueBtn?>
		<?=$h_CancelBtn?>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="FromImport" name="FromImport" value="1" />
	<input type="hidden" id="IsStop" name="IsStop" value="n" />
	<input type="hidden" id="StartIndex" name="StartIndex" value="0" />
	<input type="hidden" id="secretFileKey" name="secretFileKey" value="" />
	<input type="hidden" id="phKey" name="phKey" value="<?=$phKey?>" />
</form>