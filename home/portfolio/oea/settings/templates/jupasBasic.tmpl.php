<?
echo $h_IncludeJsCss;
echo $h_TopTabMenu;
?>

<script language="javascript">
$(document).ready( function() {	
	js_Reload_Table('View');	
});

function js_Reload_Table(jsMode) {
	$('div#TableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Jupas_Basic_Settings_' + jsMode + '_Mode_Table'
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Edit_Settings() {
	js_Reload_Table('Edit');
}

function js_Save_Settings() {
	
	var jsCanDoSave = true;
	var jsIsFirstFocus = true;
	$('div.WarningMsgDiv').hide();
	
	// Check School Code
	var jsSchoolCode = Trim($('input#SchoolCodeTb').val());
	if (jsSchoolCode == '') {
		jsCanDoSave = false;
		$('div#SchoolCodeEmptyWarningMsgDiv').show();
		
		if (jsIsFirstFocus) {
			$('input#SchoolCodeTb').focus();
		}
	}
	
	// Check Applicable Form
	/*Disabled on 2012-10-04
	if ($('input.FormIDChk:checked').length == 0) {
		jsCanDoSave = false;
		$('div#ApplicableFormEmptyWarningMsgDiv').show();
		
		if (jsIsFirstFocus) {
			$('input#FormIDChk_All').focus();
		}
	}
	*/	
	if (jsCanDoSave) {
		var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
		jsSubmitString += '&task=ajax&script=ajax_update&action=JupasBasicSettings';
		
		Block_Document();
		
		$.ajax({
			type: "POST",  
			url: "index.php",
			data: jsSubmitString,  
			success: function(data) {
				
				var jsReturnMsg = '';
				if (data=='1') {
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
					js_Reload_Table('View');
				}
				else {
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
				}
				
				Get_Return_Message(jsReturnMsg);
				Scroll_To_Top();
				UnBlock_Document();
			} 
		});
	}
	
	return false;
}
</script>

<form id="form1" name="form1" method="post">
&nbsp;&nbsp;&nbsp;<?=$h_FAQ?>
	<div class="table_board" id="TableDiv"></div>
	<br style="clear:both;" />
</form>