<?
echo $h_OEA_Remarks;
echo $h_TopTabMenu;
echo $x;
?>

<script language="javascript">

function checkForm() {
	var obj = document.form1;
	if(obj.startDate.value=="") {
		alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['InvalidStartDate']?>");
		return false;	
	}
	else if(obj.endDate.value=="") {
		alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['InvalidEndDate']?>");
		return false;	
	}
	else if(parseInt(obj.startDate.value)>parseInt(obj.endDate.value)) {
		alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['StartTimeShouldBeEarlierThanEndTime']?>1");
		return false;
	}
	else if(obj.startDate.value==obj.endDate.value) {
		if(parseInt(obj.sh.value)>parseInt(obj.eh.value)) {
			alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['StartTimeShouldBeEarlierThanEndTime']?>2");
			return false;	
		}	
		else if(obj.sh.value==obj.eh.value) {
			if(parseInt(obj.sm.value)>parseInt(obj.em.value)) {
				alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['StartTimeShouldBeEarlierThanEndTime']?>3");
				return false;
			}	
		}
	}

	if($('#int_S').is(':checked') || $('#ext_S').is(':checked') || $('#int_P').is(':checked') || $('#ext_P').is(':checked')){
		//do nothing
	}else{
		alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedItem']?>");
		return false;
	}

	var AllowStudentNewOEA_NotFromOLE = $("input[name='AllowStudentNewOEA_NotFromOLE']:checked").val();
	if(AllowStudentNewOEA_NotFromOLE == 1){
		if( $('#newPartI').is(':checked') || $('#newPartII').is(':checked') ){
			//do nothing
		}else{
			alert("<?=$Lang['iPortfolio']['OEA']['jsWarning']['AllowSubmitedOEAItem']?>");
			return false;
		}

	}
	
	$('#task').val('oea_setting_update');	
	$('#script').val('');
}


function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	/*
	if(document.getElementById('AutoApproval1').checked==true) {
		ShowHideLayer('EditableSpan',1);
	} else {
		ShowHideLayer('EditableSpan',0);
	}
	*/
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function ShowHideLayer(layername, attr) {
	if(attr==1) {
		$('#'+layername).show('slow');	
	} else {
		$('#'+layername).hide('slow');	
	}
}

function resetAllowEdit() {
	$('input[name=AllowEdit]').attr('checked', false);
}
function enablePartOption(enableValue){
	if(enableValue == 1){
		$('#newPartI').removeAttr('disabled');
		$('#newPartII').removeAttr('disabled');
	}else{
		$('#newPartI').attr('disabled', true);
		$('#newPartII').attr('disabled', true);
	}
}
<? if($msg!="") { ?>
	Get_Return_Message("<?=$msg?>");
<? } ?>


</script>