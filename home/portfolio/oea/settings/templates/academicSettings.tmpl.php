<?
echo $h_IncludeJsCss;
echo $h_TopTabMenu;
?>
<script>
var jsCurAcademicYearID = '<?=$Academic_AcademicYearID?>';
var jsCurYearTermID = '<?=$Academic_YearTermID?>';

$(document).ready( function() {	
	//initThickBox();
	
	js_Reload_Year_Term_Selection();
});

function js_Changed_AcademicYear_Selection(jsAcademicYearID) {
	jsCurAcademicYearID = jsAcademicYearID;
	js_Reload_Year_Term_Selection();
}

function js_Reload_Year_Term_Selection() {
	$('div#YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsCurAcademicYearID,
			YearTermID: jsCurYearTermID,
			SelectionID: 'YearTermID',
			OnChange: 'js_Changed_Term_Selection(this.value);',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Changed_Term_Selection(jsYearTermID) {
	jsCurYearTermID = jsYearTermID;
}

function js_Save_Settings() {
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	jsSubmitString += '&task=ajax&script=ajax_update&action=AcademicSettings';
	
	Block_Document();
	
	$.ajax({  
		type: "POST",  
		url: "index.php",
		data: jsSubmitString,  
		success: function(data) {
			
			var jsReturnMsg = '';
			if (data=='1') {
				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
			}
			else {
				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			}
			
			Get_Return_Message(jsReturnMsg);
			Scroll_To_Top();
			UnBlock_Document();
		} 
	});  
	return false;
}


//function js_Changed_Mapping_Settings(jsMappingType)
//{
//	if (jsMappingType == 'Percentile')
//	{
//		$('div#OverallRatingMapping_Percentile_SettingsDiv').show();
//		$('div#OverallRatingMapping_Score_SettingsDiv').hide();
//	}
//	else if (jsMappingType == 'Score')
//	{
//		$('div#OverallRatingMapping_Percentile_SettingsDiv').hide();
//		$('div#OverallRatingMapping_Score_SettingsDiv').show();
//	}
//}
//
//function js_Get_Settings_Layer(jsType)
//{
//	$.post(
//		"index.php", 
//		{ 
//			task: 'ajax',
//			script: 'ajax_settings_reload',
//			action: 'Get_Academic_Settings_Map_By_' + jsType + '_Layer'
//		},
//		function(ReturnData)
//		{
//			$('div#TB_ajaxContent').html(ReturnData);
//		}
//	);
//}

</script>

<br />
<form id="form1" name="form1" method="post">
	<div class="table_board">
		<?=$h_AcademicSourceNavigation?>
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolYear']?></td>
				<td><?=$h_AcademicYearSelection?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['SchoolTerm']?></td>
				<td><div id="YearTermSelectionDiv"></div></td>
			</tr>
		</table>
		
<!--
			<br style="clear:both;" />
			<?=$h_PercentileNavigation?>
			<table class="form_table_v30">
				<tr>
					<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AllowManualAdjustment']?></td>
					<td><?=$h_PercentileManualAdjChk?></td>
				</tr>
			</table>
	-->		
			<br style="clear:both;" />
			<?=$h_OverallRatingNavigation?>
			<table class="form_table_v30">
				<tr>
					<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['MappingCriteria']?></td>
					<td><?=$h_MappingCriteriaTable?></td>
				</tr>
<!--
				<tr>
					<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AllowManualAdjustment']?></td>
					<td><?=$h_OverallRatingManualAdjChk?></td>
				</tr>
	-->			
			</table>

	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<?=$SaveBtn?>
	</div>
</form>