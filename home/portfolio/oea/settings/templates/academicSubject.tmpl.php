<?
echo $h_IncludeJsCss;
echo $h_TopTabMenu;
?>
<script>
$(document).ready( function() {	
	js_Reload_Settings_Table();
});


function js_Reload_Settings_Table() {
	var jsSubjectJupasStatus = $('select#SubjectJupasStatusSel').val();
	
	$('div#SettingsTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"index.php", 
		{
			task: 'ajax',
			script: 'ajax_reload',
			action: 'Settings_Academic_Subject',
			SubjectJupasStatus: jsSubjectJupasStatus
		},
		function(ReturnData)
		{
		
		}
	);
}

function js_Changed_Subject_Jupas_Status_Selection() {
	js_Reload_Settings_Table();
}

function js_Change_Global_Apply_To_Jupas_Checkbox(jsChecked) {
	Check_All_Options_By_Class('ApplyToJupasChk', jsChecked);
	
	$('input.JupasSubjectCodeTb').each( function () {
		var jsSubjectID = $(this).attr('subjectID');
		js_Changed_Apply_To_Jupas_Checkbox(jsChecked, jsSubjectID);
	});
}

function js_Changed_Apply_To_Jupas_Checkbox(jsChecked, jsSubjectID) {
	Uncheck_SelectAll('Global_ApplyToJupasChk', jsChecked);
	
	var jsReadOnly = (jsChecked)? '' : 'readonly';
	$('input#JupasSubjectCodeTb_' + jsSubjectID).attr('readonly', jsReadOnly);
	
	if (jsChecked) {
		$('input#JupasSubjectCodeTb_' + jsSubjectID).focus().select();
	}
	
}

function js_Clicked_Subject_Code_Textbox(jsSubjectID) {
	$('input#ApplyToJupasChk_' + jsSubjectID).attr('checked', true);
	js_Changed_Apply_To_Jupas_Checkbox(true, jsSubjectID);
}

function js_Go_Export() {
	window.location = 'index.php?task=academicSubject_export';
}

function js_Save_Settings() {
	
	var jsCanDoSave = true;
	var jsFirstFocus = true;
	$('div.WarningMsgDiv').hide();
	
	// Check empty code
	$('input.JupasSubjectCodeTb').each( function() {
		var jsJupasSubjectCode = Trim($(this).val());
		var jsThisSubjectID = $(this).attr('subjectID');
		var jsThisApplyToJupas = $('input#ApplyToJupasChk_' + jsThisSubjectID).attr('checked');
		
		if (jsJupasSubjectCode == '' && jsThisApplyToJupas == true) {
			jsCanDoSave = false;
			$('div#JupasSubjectCodeEmptyWarningMsgDiv_' + jsThisSubjectID).show();
			
			if (jsFirstFocus) {
				$(this).focus();
				jsFirstFocus = false;
			}
		}
	});
	
	/*Comment out on 11-10-2012
	// Check duplicated code
	$('input.JupasSubjectCodeTb').each( function() {
		var jsJupasSubjectCode = Trim($(this).val()).toString();
		var jsThisSubjectID = $(this).attr('subjectID');
		var jsThisApplyToJupas = $('input#ApplyToJupasChk_' + jsThisSubjectID).attr('checked');
		
		$('input.JupasSubjectCodeTb').each( function() {
			var jsSecondJupasSubjectCode = Trim($(this).val()).toString();
			var jsSecondSubjectID = $(this).attr('subjectID');
			
			if (jsThisSubjectID!=jsSecondSubjectID && jsJupasSubjectCode!='' && jsSecondJupasSubjectCode!='' && jsJupasSubjectCode==jsSecondJupasSubjectCode) {
				jsCanDoSave = false;
				$('div#JupasSubjectCodeDuplicateWarningMsgDiv_' + jsThisSubjectID).show();
				
				if (jsFirstFocus) {
					$(this).focus();
					jsFirstFocus = false;
				}
			}
		});
	});
	*/
	
	
	if (jsCanDoSave) {
		var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
		jsSubmitString += '&task=ajax&script=ajax_update&action=AcademicSubject';
		
		Block_Document();
		
		$.ajax({
			type: "POST",  
			url: "index.php",
			data: jsSubmitString,  
			success: function(data) {
				
				var jsReturnMsg = '';
				if (data=='1') {
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
					js_Reload_Settings_Table();
				}
				else {
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
				}
				
				Get_Return_Message(jsReturnMsg);
				Scroll_To_Top();
				UnBlock_Document();
			} 
		});
	}
	
	return false;
}
</script>

<form id="form1" name="form1" method="post">
	<div class="table_board">
		<div class="content_top_tool">
			<div class="Conntent_tool"><?=$h_Toolbar?></div>
			<br style="clear: both;">
		</div>
		<div><?=$h_SubjectJupasStatusSelection?></div>
		<br />
		<div id="SettingsTableDiv"></div>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<?=$SaveBtn?>
	</div>
</form>