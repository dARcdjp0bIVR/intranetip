<?php
echo $h_TopTabMenu;
?>
<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$h_ProgressMsg?>');
});

function js_Back_To_OLE_List()
{
	window.location = 'index.php?task=oeaItemMap';
}

function js_Back_To_Import_Step1()
{
	window.location = 'index.php?task=oeaItemMap_importStep1';
}
</script>
<form method="POST" name="form1" id="form1" action="index.php" enctype="multipart/form-data">
	<?=$h_Navigation?>
	<br style="clear:both;" />
	<?=$h_StepTable?>
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr>
				<td>
					<table class="form_table_v30">
						<tr>
							<td class="tabletext" style="text-align:center;">
								<span id="ImportStatusSpan"></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="edit_bottom_v30">
		<?=$ImportOtherBtn?>
		<?=$BackBtn?>
	</div>
	
	<iframe id="ImportIFrame" name="ImportIFrame" src="<?=$h_iFrameSrc?>" style="width:100%;height:500px;border:0px;display:none;"></iframe>
	
</form>