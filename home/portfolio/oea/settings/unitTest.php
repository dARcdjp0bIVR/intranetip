<?php
## Using By : 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_additional_info.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_academic.php");

intranet_auth();
intranet_opendb();

$task = trim($task);
//$task = ($task == "")? "index": $task;
$task = ($task == "")? "jupasBasic": $task;
$LibPortfolio = new libportfolio2007();
$lpf = new libpf_slp();

//$TabMenuArr = libpf_tabmenu::getJupasAdminSettingsTags('ADMIN_SETTING', $task);
//$h_TopTabMenu = $LibPortfolio->GET_TAB_MENU($TabMenuArr);

$linterface = new interface_html();
$CurrentPage = "Settings_OEA";

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

//$TAGS_OBJ = array(array($Lang['iPortfolio']['OEA']['JUPAS']));
$TAGS_OBJ = libpf_tabmenu::getJupasAdminSettingsTags('ADMIN_SETTING', $task);

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

switch($task){
  case "ajax":
    $task_script = "../ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		//$task_script = "task/" . str_replace("../", "", $task) . ".php";
		$task_script = "unitTest/" . $task . ".php";
//		$template_script = "templates/" . $task . ".tmpl.php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
exit();
?>