<?php
$default_oea_version = "2011-09-16 13:43:45";

$default_oea = array();
$default_oea["00000"]["ItemCode"] = "00000";
$default_oea["00000"]["ItemName"] = "Others";
$default_oea["00000"]["CatCode"] = "6";
$default_oea["00000"]["CatName"] = "Others";

$default_oea["02251"]["ItemCode"] = "02251";
$default_oea["02251"]["ItemName"] = "'Towards Beijing. Towards the Space' National Youth Aviation and Spaceflight Model Competition";
$default_oea["02251"]["CatCode"] = "4";
$default_oea["02251"]["CatName"] = "Aesthetic Development";

$default_oea["01094"]["ItemCode"] = "01094";
$default_oea["01094"]["ItemName"] = "\"Integration Education\" Poster Design 「共融由學校開始」海報設計 ";
$default_oea["01094"]["CatCode"] = "4";
$default_oea["01094"]["CatName"] = "Aesthetic Development";

$default_oea["00001"]["ItemCode"] = "00001";
$default_oea["00001"]["ItemName"] = "150th Anniversary Musical (Please provide details in \"Description\" box)";
$default_oea["00001"]["CatCode"] = "4";
$default_oea["00001"]["CatName"] = "Aesthetic Development";

$default_oea["02465"]["ItemCode"] = "02465";
$default_oea["02465"]["ItemName"] = "2011 青春祖國行 - 京港澳學生交流夏令營";
$default_oea["02465"]["CatCode"] = "2";
$default_oea["02465"]["CatName"] = "Career-related Experience";

$default_oea["02466"]["ItemCode"] = "02466";
$default_oea["02466"]["ItemName"] = "21世紀東亞青少年大交流計劃";
$default_oea["02466"]["CatCode"] = "5";
$default_oea["02466"]["CatName"] = "Moral and Civic Education";

$default_oea["02467"]["ItemCode"] = "02467";
$default_oea["02467"]["ItemName"] = "40週年校慶系列- 40週年校慶標誌製作";
$default_oea["02467"]["CatCode"] = "4";
$default_oea["02467"]["CatName"] = "Aesthetic Development";

$default_oea["00002"]["ItemCode"] = "00002";
$default_oea["00002"]["ItemName"] = "A decade of Hong Kong - Past and Beyond Essay Competition";
$default_oea["00002"]["CatCode"] = "4";
$default_oea["00002"]["CatName"] = "Aesthetic Development";

$default_oea["00003"]["ItemCode"] = "00003";
$default_oea["00003"]["ItemName"] = "AASF Asian Age Group Championships  AASF亞洲分齡游泳錦標賽";
$default_oea["00003"]["CatCode"] = "3";
$default_oea["00003"]["CatName"] = "Physical Development";

$default_oea["00004"]["ItemCode"] = "00004";
$default_oea["00004"]["ItemName"] = "AAU Junior Olympic Games  AAU青少年奧運會";
$default_oea["00004"]["CatCode"] = "3";
$default_oea["00004"]["CatName"] = "Physical Development";

$default_oea["00005"]["ItemCode"] = "00005";
$default_oea["00005"]["ItemName"] = "Aberdeen International Youth Festival";
$default_oea["00005"]["CatCode"] = "4";
$default_oea["00005"]["CatName"] = "Aesthetic Development";

$default_oea["00006"]["ItemCode"] = "00006";
$default_oea["00006"]["ItemName"] = "ABF Tour - Malaysia Leg  亞洲保齡球巡迴賽馬來西亞站";
$default_oea["00006"]["CatCode"] = "3";
$default_oea["00006"]["CatName"] = "Physical Development";

$default_oea["00007"]["ItemCode"] = "00007";
$default_oea["00007"]["ItemName"] = "ABF Tour (Please provide details in \"Description\" box.)";
$default_oea["00007"]["CatCode"] = "3";
$default_oea["00007"]["CatName"] = "Physical Development";

$default_oea["00008"]["ItemCode"] = "00008";
$default_oea["00008"]["ItemName"] = "Academic Activity on City Development Information - Paper Contest  城市信息化建設與發展 學術活動大學生論文比賽";
$default_oea["00008"]["CatCode"] = "4";
$default_oea["00008"]["CatName"] = "Aesthetic Development";

$default_oea["00009"]["ItemCode"] = "00009";
$default_oea["00009"]["ItemName"] = "Academic Ethics, Shenzhen Children's Spot Painting Competition  學道德少兒書畫賽";
$default_oea["00009"]["CatCode"] = "4";
$default_oea["00009"]["CatName"] = "Aesthetic Development";

$default_oea["00010"]["ItemCode"] = "00010";
$default_oea["00010"]["ItemName"] = "Academic Leaders Group";
$default_oea["00010"]["CatCode"] = "1";
$default_oea["00010"]["CatName"] = "Community Service";

$default_oea["00012"]["ItemCode"] = "00012";
$default_oea["00012"]["ItemName"] = "Accountant Ambassadors Programme";
$default_oea["00012"]["CatCode"] = "2";
$default_oea["00012"]["CatName"] = "Career-related Experience";

$default_oea["00013"]["ItemCode"] = "00013";
$default_oea["00013"]["ItemName"] = "Accounting Software Applications Competition for Secondary School Students";
$default_oea["00013"]["CatCode"] = "2";
$default_oea["00013"]["CatName"] = "Career-related Experience";

$default_oea["00014"]["ItemCode"] = "00014";
$default_oea["00014"]["ItemName"] = "Acers Athletics meet 2011飛達全港新星大賽";
$default_oea["00014"]["CatCode"] = "3";
$default_oea["00014"]["CatName"] = "Physical Development";

$default_oea["00015"]["ItemCode"] = "00015";
$default_oea["00015"]["ItemName"] = "Aces Education Australia English Writing Competition";
$default_oea["00015"]["CatCode"] = "4";
$default_oea["00015"]["CatName"] = "Aesthetic Development";

$default_oea["00016"]["ItemCode"] = "00016";
$default_oea["00016"]["ItemName"] = "Acrobatic Gymnastics Open Championships 全港技巧體操公開比賽";
$default_oea["00016"]["CatCode"] = "3";
$default_oea["00016"]["CatName"] = "Physical Development";

$default_oea["00017"]["ItemCode"] = "00017";
$default_oea["00017"]["ItemName"] = "Acrobatic Gymnastics World Championships International Age Group Competitions";
$default_oea["00017"]["CatCode"] = "3";
$default_oea["00017"]["CatName"] = "Physical Development";

$default_oea["00020"]["ItemCode"] = "00020";
$default_oea["00020"]["ItemName"] = "Activating Your Potential Awards Scheme  ";
$default_oea["00020"]["CatCode"] = "2";
$default_oea["00020"]["CatName"] = "Career-related Experience";

$default_oea["00021"]["ItemCode"] = "00021";
$default_oea["00021"]["ItemName"] = "Acupressure in First Aid 手力急救班課程";
$default_oea["00021"]["CatCode"] = "4";
$default_oea["00021"]["CatName"] = "Aesthetic Development";

$default_oea["00011"]["ItemCode"] = "00011";
$default_oea["00011"]["ItemName"] = "ACC Under-19 Women's Championship (Cricket)";
$default_oea["00011"]["CatCode"] = "3";
$default_oea["00011"]["CatName"] = "Physical Development";

$default_oea["00018"]["ItemCode"] = "00018";
$default_oea["00018"]["ItemName"] = "ACSA Wood Products Council Student Design Competition A Contemporary Arts Gallery C Gallery  建築學院協會木材產品局現代藝術單層館學生設計比賽";
$default_oea["00018"]["CatCode"] = "4";
$default_oea["00018"]["CatName"] = "Aesthetic Development";

$default_oea["00019"]["ItemCode"] = "00019";
$default_oea["00019"]["ItemName"] = "ACSA/Otis Elevator Company International Student Design Competition  建築學院協會．奧迪斯電梯公司國際學生設計比賽";
$default_oea["00019"]["CatCode"] = "4";
$default_oea["00019"]["CatName"] = "Aesthetic Development";

$default_oea["00022"]["ItemCode"] = "00022";
$default_oea["00022"]["ItemName"] = "Adidas Streetball Challenge-Asian Regional Final";
$default_oea["00022"]["CatCode"] = "3";
$default_oea["00022"]["CatName"] = "Physical Development";

$default_oea["00023"]["ItemCode"] = "00023";
$default_oea["00023"]["ItemName"] = "Adidas足球挑戰賽";
$default_oea["00023"]["CatCode"] = "4";
$default_oea["00023"]["CatName"] = "Aesthetic Development";

$default_oea["00024"]["ItemCode"] = "00024";
$default_oea["00024"]["ItemName"] = "Advanced Scout Standard";
$default_oea["00024"]["CatCode"] = "1";
$default_oea["00024"]["CatName"] = "Community Service";

$default_oea["00025"]["ItemCode"] = "00025";
$default_oea["00025"]["ItemName"] = "Adventure Ship Challenge";
$default_oea["00025"]["CatCode"] = "3";
$default_oea["00025"]["CatName"] = "Physical Development";

$default_oea["00026"]["ItemCode"] = "00026";
$default_oea["00026"]["ItemName"] = "Aerospace Science and Technology Summer Camp Summer Aerospace Science and Technology knowledge competition  航天科技夏令營\"航天知識競賽\"";
$default_oea["00026"]["CatCode"] = "2";
$default_oea["00026"]["CatName"] = "Career-related Experience";

$default_oea["00034"]["ItemCode"] = "00034";
$default_oea["00034"]["ItemName"] = "Afterschool Support Team";
$default_oea["00034"]["CatCode"] = "1";
$default_oea["00034"]["CatName"] = "Community Service";

$default_oea["00027"]["ItemCode"] = "00027";
$default_oea["00027"]["ItemName"] = "AFC - Asian Cup";
$default_oea["00027"]["CatCode"] = "3";
$default_oea["00027"]["CatName"] = "Physical Development";

$default_oea["00028"]["ItemCode"] = "00028";
$default_oea["00028"]["ItemName"] = "AFC Festival of Football";
$default_oea["00028"]["CatCode"] = "3";
$default_oea["00028"]["CatName"] = "Physical Development";

$default_oea["00029"]["ItemCode"] = "00029";
$default_oea["00029"]["ItemName"] = "AFC U-16 Championship Qualification Competition (Football)";
$default_oea["00029"]["CatCode"] = "3";
$default_oea["00029"]["CatName"] = "Physical Development";

$default_oea["00030"]["ItemCode"] = "00030";
$default_oea["00030"]["ItemName"] = "AFC U-19 Championship (formerly known as AFC Youth Championship )  亞足聯青年錦標賽";
$default_oea["00030"]["CatCode"] = "3";
$default_oea["00030"]["CatName"] = "Physical Development";

$default_oea["00031"]["ItemCode"] = "00031";
$default_oea["00031"]["ItemName"] = "AFC Women's Championship";
$default_oea["00031"]["CatCode"] = "3";
$default_oea["00031"]["CatName"] = "Physical Development";

$default_oea["00032"]["ItemCode"] = "00032";
$default_oea["00032"]["ItemName"] = "AFS Intercultural Fair  AFS國際文化節";
$default_oea["00032"]["CatCode"] = "5";
$default_oea["00032"]["CatName"] = "Moral and Civic Education";

$default_oea["00033"]["ItemCode"] = "00033";
$default_oea["00033"]["ItemName"] = "AFS International Women's Tournament";
$default_oea["00033"]["CatCode"] = "3";
$default_oea["00033"]["CatName"] = "Physical Development";

$default_oea["00035"]["ItemCode"] = "00035";
$default_oea["00035"]["ItemName"] = "Age Group Long Course Swimming Competition  長池分齡游泳比賽";
$default_oea["00035"]["CatCode"] = "3";
$default_oea["00035"]["CatName"] = "Physical Development";

$default_oea["00036"]["ItemCode"] = "00036";
$default_oea["00036"]["ItemName"] = "Agricultural activity (Please provide details in \"Description\" box.)";
$default_oea["00036"]["CatCode"] = "4";
$default_oea["00036"]["CatName"] = "Aesthetic Development";

$default_oea["00037"]["ItemCode"] = "00037";
$default_oea["00037"]["ItemName"] = "Air Cadet (No. 302 Squadron)";
$default_oea["00037"]["CatCode"] = "2";
$default_oea["00037"]["CatName"] = "Career-related Experience";

$default_oea["00039"]["ItemCode"] = "00039";
$default_oea["00039"]["ItemName"] = "Akita Cup Beach Handball Competition Akita盃沙灘手球公開賽";
$default_oea["00039"]["CatCode"] = "3";
$default_oea["00039"]["CatName"] = "Physical Development";

$default_oea["00038"]["ItemCode"] = "00038";
$default_oea["00038"]["ItemName"] = "AKF (Asian Karate-do Federation) Junior and Cadet Karate-do Championships  AKF亞洲青少年空手道錦標賽";
$default_oea["00038"]["CatCode"] = "3";
$default_oea["00038"]["CatName"] = "Physical Development";

$default_oea["00040"]["ItemCode"] = "00040";
$default_oea["00040"]["ItemName"] = "Aldo Papone International Case Writing Competition";
$default_oea["00040"]["CatCode"] = "4";
$default_oea["00040"]["CatName"] = "Aesthetic Development";

$default_oea["00041"]["ItemCode"] = "00041";
$default_oea["00041"]["ItemName"] = "All China Age Group Athletic Championships";
$default_oea["00041"]["CatCode"] = "3";
$default_oea["00041"]["CatName"] = "Physical Development";

$default_oea["00042"]["ItemCode"] = "00042";
$default_oea["00042"]["ItemName"] = "All China Amateur Fencing Championships Sun Yat-sen University Cup  中山大學杯全國業餘擊劍冠軍系列賽";
$default_oea["00042"]["CatCode"] = "3";
$default_oea["00042"]["CatName"] = "Physical Development";

$default_oea["00043"]["ItemCode"] = "00043";
$default_oea["00043"]["ItemName"] = "All China Athletic Series 2  全國田徑大奬賽系列賽";
$default_oea["00043"]["CatCode"] = "3";
$default_oea["00043"]["CatName"] = "Physical Development";

$default_oea["00044"]["ItemCode"] = "00044";
$default_oea["00044"]["ItemName"] = "All China Cadet Fencing Championships  全國初級劍擊錦標賽";
$default_oea["00044"]["CatCode"] = "3";
$default_oea["00044"]["CatName"] = "Physical Development";

$default_oea["00045"]["ItemCode"] = "00045";
$default_oea["00045"]["ItemName"] = "All China Championships Meet Series III (Tianjin)  全中國錦標賽第三系列(天津)";
$default_oea["00045"]["CatCode"] = "3";
$default_oea["00045"]["CatName"] = "Physical Development";

$default_oea["00046"]["ItemCode"] = "00046";
$default_oea["00046"]["ItemName"] = "All China Cities Table Tennis Game";
$default_oea["00046"]["CatCode"] = "3";
$default_oea["00046"]["CatName"] = "Physical Development";

$default_oea["00047"]["ItemCode"] = "00047";
$default_oea["00047"]["ItemName"] = "All China Elite Orienteering Championships";
$default_oea["00047"]["CatCode"] = "3";
$default_oea["00047"]["CatName"] = "Physical Development";

$default_oea["00048"]["ItemCode"] = "00048";
$default_oea["00048"]["ItemName"] = "All China Games  全國體育競賽";
$default_oea["00048"]["CatCode"] = "3";
$default_oea["00048"]["CatName"] = "Physical Development";

$default_oea["00049"]["ItemCode"] = "00049";
$default_oea["00049"]["ItemName"] = "All China Games for the Disabled  中華人民共和國殘疾人運動會";
$default_oea["00049"]["CatCode"] = "3";
$default_oea["00049"]["CatName"] = "Physical Development";

$default_oea["00050"]["ItemCode"] = "00050";
$default_oea["00050"]["ItemName"] = "All China Gymnastics Competition";
$default_oea["00050"]["CatCode"] = "3";
$default_oea["00050"]["CatName"] = "Physical Development";

$default_oea["00051"]["ItemCode"] = "00051";
$default_oea["00051"]["ItemName"] = "All China High School Basketball League  全國中學生籃球賽";
$default_oea["00051"]["CatCode"] = "3";
$default_oea["00051"]["CatName"] = "Physical Development";

$default_oea["00052"]["ItemCode"] = "00052";
$default_oea["00052"]["ItemName"] = "All China Inter-University Badminton Championship  全國大學校際羽毛球錦標賽";
$default_oea["00052"]["CatCode"] = "3";
$default_oea["00052"]["CatName"] = "Physical Development";

$default_oea["00053"]["ItemCode"] = "00053";
$default_oea["00053"]["ItemName"] = "All China Junior / Youth Fencing Championships";
$default_oea["00053"]["CatCode"] = "3";
$default_oea["00053"]["CatName"] = "Physical Development";

$default_oea["00054"]["ItemCode"] = "00054";
$default_oea["00054"]["ItemName"] = "All China Juniors Athletics Meets";
$default_oea["00054"]["CatCode"] = "3";
$default_oea["00054"]["CatName"] = "Physical Development";

$default_oea["00055"]["ItemCode"] = "00055";
$default_oea["00055"]["ItemName"] = "All China Korfball Spring Tournament  全國合球春季賽";
$default_oea["00055"]["CatCode"] = "3";
$default_oea["00055"]["CatName"] = "Physical Development";

$default_oea["00056"]["ItemCode"] = "00056";
$default_oea["00056"]["ItemName"] = "All China National Students' Calligraphy and Painting Photography Works Exhibition  中華萬歲全國學生書畫攝影展";
$default_oea["00056"]["CatCode"] = "4";
$default_oea["00056"]["CatName"] = "Aesthetic Development";

$default_oea["00057"]["ItemCode"] = "00057";
$default_oea["00057"]["ItemName"] = "All China National Youth Cycling Competition";
$default_oea["00057"]["CatCode"] = "3";
$default_oea["00057"]["CatName"] = "Physical Development";

$default_oea["00058"]["ItemCode"] = "00058";
$default_oea["00058"]["ItemName"] = "All China Orienteering Championships  全國體育競賽－全國定向錦標賽";
$default_oea["00058"]["CatCode"] = "3";
$default_oea["00058"]["CatName"] = "Physical Development";

$default_oea["00059"]["ItemCode"] = "00059";
$default_oea["00059"]["ItemName"] = "All China Secondary School Student Beach Volleyball Championship  全國中學生沙灘排球錦標賽";
$default_oea["00059"]["CatCode"] = "3";
$default_oea["00059"]["CatName"] = "Physical Development";

$default_oea["00060"]["ItemCode"] = "00060";
$default_oea["00060"]["ItemName"] = "All China Secondary School Student Handball Competition";
$default_oea["00060"]["CatCode"] = "3";
$default_oea["00060"]["CatName"] = "Physical Development";

$default_oea["00061"]["ItemCode"] = "00061";
$default_oea["00061"]["ItemName"] = "All China Secondary School Volleyball Championship  全國中學生排球賽";
$default_oea["00061"]["CatCode"] = "3";
$default_oea["00061"]["CatName"] = "Physical Development";

$default_oea["00062"]["ItemCode"] = "00062";
$default_oea["00062"]["ItemName"] = "All China Secondary Schools Handball League  全國中學生手球聯賽";
$default_oea["00062"]["CatCode"] = "3";
$default_oea["00062"]["CatName"] = "Physical Development";

$default_oea["00063"]["ItemCode"] = "00063";
$default_oea["00063"]["ItemName"] = "All China Secondary Schools Students Games  中華人民共和國中學生運動會";
$default_oea["00063"]["CatCode"] = "3";
$default_oea["00063"]["CatName"] = "Physical Development";

$default_oea["00064"]["ItemCode"] = "00064";
$default_oea["00064"]["ItemName"] = "All China Student Orienteering Championship  全國學生野外定向錦標賽";
$default_oea["00064"]["CatCode"] = "3";
$default_oea["00064"]["CatName"] = "Physical Development";

$default_oea["00065"]["ItemCode"] = "00065";
$default_oea["00065"]["ItemName"] = "All China Students Beach Volleyball Competition";
$default_oea["00065"]["CatCode"] = "3";
$default_oea["00065"]["CatName"] = "Physical Development";

$default_oea["00066"]["ItemCode"] = "00066";
$default_oea["00066"]["ItemName"] = "All China Students Championships  全國學生賽";
$default_oea["00066"]["CatCode"] = "3";
$default_oea["00066"]["CatName"] = "Physical Development";

$default_oea["00067"]["ItemCode"] = "00067";
$default_oea["00067"]["ItemName"] = "All China University Baseball League Competition  全國大學生棒球聯賽";
$default_oea["00067"]["CatCode"] = "3";
$default_oea["00067"]["CatName"] = "Physical Development";

$default_oea["00068"]["ItemCode"] = "00068";
$default_oea["00068"]["ItemName"] = "All China University Students Track & Field Championships  全國大學生田徑錦標賽";
$default_oea["00068"]["CatCode"] = "3";
$default_oea["00068"]["CatName"] = "Physical Development";

$default_oea["00069"]["ItemCode"] = "00069";
$default_oea["00069"]["ItemName"] = "All China Youth Athletics Championship  全國青少年田徑錦標賽";
$default_oea["00069"]["CatCode"] = "3";
$default_oea["00069"]["CatName"] = "Physical Development";

$default_oea["00070"]["ItemCode"] = "00070";
$default_oea["00070"]["ItemName"] = "All China Youth Kayak Championships";
$default_oea["00070"]["CatCode"] = "3";
$default_oea["00070"]["CatName"] = "Physical Development";

$default_oea["00071"]["ItemCode"] = "00071";
$default_oea["00071"]["ItemName"] = "All China Youth Soft Volleyball Championships";
$default_oea["00071"]["CatCode"] = "3";
$default_oea["00071"]["CatName"] = "Physical Development";

$default_oea["00072"]["ItemCode"] = "00072";
$default_oea["00072"]["ItemName"] = "All China Youth U-21 Beach Volleyball Championship";
$default_oea["00072"]["CatCode"] = "3";
$default_oea["00072"]["CatName"] = "Physical Development";

$default_oea["00073"]["ItemCode"] = "00073";
$default_oea["00073"]["ItemName"] = "All Hong Kong Schools Gymnastics Competition (Secondary School)";
$default_oea["00073"]["CatCode"] = "3";
$default_oea["00073"]["CatName"] = "Physical Development";

$default_oea["00074"]["ItemCode"] = "00074";
$default_oea["00074"]["ItemName"] = "All Hong Kong Schools Jing Ying Badminton Tourament  全港學界精英羽毛球比賽";
$default_oea["00074"]["CatCode"] = "3";
$default_oea["00074"]["CatName"] = "Physical Development";

$default_oea["00075"]["ItemCode"] = "00075";
$default_oea["00075"]["ItemName"] = "All National Junior Fencing Tournament, Cup of State Fencing  同洲劍校杯全國少年擊劍錦標賽";
$default_oea["00075"]["CatCode"] = "3";
$default_oea["00075"]["CatName"] = "Physical Development";

$default_oea["00076"]["ItemCode"] = "00076";
$default_oea["00076"]["ItemName"] = "All National Secondary Students Amateur League  全國中學生業餘體校聯賽";
$default_oea["00076"]["CatCode"] = "3";
$default_oea["00076"]["CatName"] = "Physical Development";

$default_oea["00077"]["ItemCode"] = "00077";
$default_oea["00077"]["ItemName"] = "All National Youth Gymnastics Tournament  全國青少年體操錦標賽";
$default_oea["00077"]["CatCode"] = "3";
$default_oea["00077"]["CatName"] = "Physical Development";

$default_oea["00078"]["ItemCode"] = "00078";
$default_oea["00078"]["ItemName"] = "All Sports Competition";
$default_oea["00078"]["CatCode"] = "3";
$default_oea["00078"]["CatName"] = "Physical Development";

$default_oea["00079"]["ItemCode"] = "00079";
$default_oea["00079"]["ItemName"] = "American Mathematics Contests";
$default_oea["00079"]["CatCode"] = "2";
$default_oea["00079"]["CatName"] = "Career-related Experience";

$default_oea["00080"]["ItemCode"] = "00080";
$default_oea["00080"]["ItemName"] = "American Regions Mathematics League";
$default_oea["00080"]["CatCode"] = "2";
$default_oea["00080"]["CatName"] = "Career-related Experience";

$default_oea["00081"]["ItemCode"] = "00081";
$default_oea["00081"]["ItemName"] = "Anniversary National Day Hong Kong Swimming Championships  香港國慶游泳錦標賽";
$default_oea["00081"]["CatCode"] = "3";
$default_oea["00081"]["CatName"] = "Physical Development";

$default_oea["00082"]["ItemCode"] = "00082";
$default_oea["00082"]["ItemName"] = "Anniversary Preparatory Committee";
$default_oea["00082"]["CatCode"] = "1";
$default_oea["00082"]["CatName"] = "Community Service";

$default_oea["00083"]["ItemCode"] = "00083";
$default_oea["00083"]["ItemName"] = "Annual Athletics Meet";
$default_oea["00083"]["CatCode"] = "3";
$default_oea["00083"]["CatName"] = "Physical Development";

$default_oea["00084"]["ItemCode"] = "00084";
$default_oea["00084"]["ItemName"] = "Annual Conference International Association of School Librarianship";
$default_oea["00084"]["CatCode"] = "2";
$default_oea["00084"]["CatName"] = "Career-related Experience";

$default_oea["00085"]["ItemCode"] = "00085";
$default_oea["00085"]["ItemName"] = "Annual High School Mathematical Contest in Modeling";
$default_oea["00085"]["CatCode"] = "2";
$default_oea["00085"]["CatName"] = "Career-related Experience";

$default_oea["00086"]["ItemCode"] = "00086";
$default_oea["00086"]["ItemName"] = "Annual Inter-Chinese University Film and Television Festival, The Edge   華語大學生影視作品展";
$default_oea["00086"]["CatCode"] = "4";
$default_oea["00086"]["CatName"] = "Aesthetic Development";

$default_oea["00087"]["ItemCode"] = "00087";
$default_oea["00087"]["ItemName"] = "Annual International Inline Hockey Tournament  國際單線曲棍球比賽";
$default_oea["00087"]["CatCode"] = "3";
$default_oea["00087"]["CatName"] = "Physical Development";

$default_oea["00088"]["ItemCode"] = "00088";
$default_oea["00088"]["ItemName"] = "Anti-Crime Poster Design Competition滅罪海報設計比賽";
$default_oea["00088"]["CatCode"] = "4";
$default_oea["00088"]["CatName"] = "Aesthetic Development";

$default_oea["00089"]["ItemCode"] = "00089";
$default_oea["00089"]["ItemName"] = "Anti-shop Theft Comic Sticker Competition防止店舖盜竊漫畫標貼創作比賽";
$default_oea["00089"]["CatCode"] = "4";
$default_oea["00089"]["CatName"] = "Aesthetic Development";

$default_oea["00090"]["ItemCode"] = "00090";
$default_oea["00090"]["ItemName"] = "APEC International Youth Camp APEC國際青年營";
$default_oea["00090"]["CatCode"] = "5";
$default_oea["00090"]["CatName"] = "Moral and Civic Education";

$default_oea["00091"]["ItemCode"] = "00091";
$default_oea["00091"]["ItemName"] = "APEC Youth Science Festival  亞太經合組織青年科學節";
$default_oea["00091"]["CatCode"] = "2";
$default_oea["00091"]["CatName"] = "Career-related Experience";

$default_oea["00092"]["ItemCode"] = "00092";
$default_oea["00092"]["ItemName"] = "Arafura Games Lifesaving Competition  阿拉費拉運動會之國際拯溺賽";
$default_oea["00092"]["CatCode"] = "3";
$default_oea["00092"]["CatName"] = "Physical Development";

$default_oea["00093"]["ItemCode"] = "00093";
$default_oea["00093"]["ItemName"] = "Arena Korea Open Swimming Championships  Arena韓國游泳公開賽";
$default_oea["00093"]["CatCode"] = "3";
$default_oea["00093"]["CatName"] = "Physical Development";

$default_oea["00094"]["ItemCode"] = "00094";
$default_oea["00094"]["ItemName"] = "Army Training Camp 軍訓營(校際)";
$default_oea["00094"]["CatCode"] = "3";
$default_oea["00094"]["CatName"] = "Physical Development";

$default_oea["00095"]["ItemCode"] = "00095";
$default_oea["00095"]["ItemName"] = "Art Exhibition by the Youth of China  中華濃情少兒美術匯展";
$default_oea["00095"]["CatCode"] = "4";
$default_oea["00095"]["CatName"] = "Aesthetic Development";

$default_oea["00097"]["ItemCode"] = "00097";
$default_oea["00097"]["ItemName"] = "Art therapy for SEN students";
$default_oea["00097"]["CatCode"] = "4";
$default_oea["00097"]["CatName"] = "Aesthetic Development";

$default_oea["00096"]["ItemCode"] = "00096";
$default_oea["00096"]["ItemName"] = "Art Talents Pop Up! Poemography Exp.  太古青年藝術坊 - 藝才群起!詩.相.感.秀創作體";
$default_oea["00096"]["CatCode"] = "4";
$default_oea["00096"]["CatName"] = "Aesthetic Development";

$default_oea["00098"]["ItemCode"] = "00098";
$default_oea["00098"]["ItemName"] = "Artistic Gymnastics China National Youth Championship  全國青年體操錦標賽";
$default_oea["00098"]["CatCode"] = "3";
$default_oea["00098"]["CatName"] = "Physical Development";

$default_oea["00101"]["ItemCode"] = "00101";
$default_oea["00101"]["ItemName"] = "Asia & South Pacific Boccia Championships  亞太區硬地滾球錦標賽";
$default_oea["00101"]["CatCode"] = "3";
$default_oea["00101"]["CatName"] = "Physical Development";

$default_oea["00103"]["ItemCode"] = "00103";
$default_oea["00103"]["ItemName"] = "Asia and South Pacific Table Tennis Championships";
$default_oea["00103"]["CatCode"] = "3";
$default_oea["00103"]["CatName"] = "Physical Development";

$default_oea["00102"]["ItemCode"] = "00102";
$default_oea["00102"]["ItemName"] = "Asia Amateur Baduk Championships";
$default_oea["00102"]["CatCode"] = "4";
$default_oea["00102"]["CatName"] = "Aesthetic Development";

$default_oea["00104"]["ItemCode"] = "00104";
$default_oea["00104"]["ItemName"] = "Asia Basketball Championship for Women  亞洲女子籃球錦標賽";
$default_oea["00104"]["CatCode"] = "3";
$default_oea["00104"]["CatName"] = "Physical Development";

$default_oea["00105"]["ItemCode"] = "00105";
$default_oea["00105"]["ItemName"] = "Asia Basketball Competition";
$default_oea["00105"]["CatCode"] = "3";
$default_oea["00105"]["CatName"] = "Physical Development";

$default_oea["00106"]["ItemCode"] = "00106";
$default_oea["00106"]["ItemName"] = "Asia City Gateball Invitational Competition  亞洲城市門球邀請賽";
$default_oea["00106"]["CatCode"] = "3";
$default_oea["00106"]["CatName"] = "Physical Development";

$default_oea["00107"]["ItemCode"] = "00107";
$default_oea["00107"]["ItemName"] = "Asia Electone Festival";
$default_oea["00107"]["CatCode"] = "4";
$default_oea["00107"]["CatName"] = "Aesthetic Development";

$default_oea["00108"]["ItemCode"] = "00108";
$default_oea["00108"]["ItemName"] = "Asia Finswimming Championships";
$default_oea["00108"]["CatCode"] = "3";
$default_oea["00108"]["CatName"] = "Physical Development";

$default_oea["00109"]["ItemCode"] = "00109";
$default_oea["00109"]["ItemName"] = "Asia Game Force Regional Final";
$default_oea["00109"]["CatCode"] = "3";
$default_oea["00109"]["CatName"] = "Physical Development";

$default_oea["00110"]["ItemCode"] = "00110";
$default_oea["00110"]["ItemName"] = "Asia Handbell and Handchimes Competition";
$default_oea["00110"]["CatCode"] = "4";
$default_oea["00110"]["CatName"] = "Aesthetic Development";

$default_oea["00111"]["ItemCode"] = "00111";
$default_oea["00111"]["ItemName"] = "Asia International Girl's Under 18 Match (Rugby)";
$default_oea["00111"]["CatCode"] = "3";
$default_oea["00111"]["CatName"] = "Physical Development";

$default_oea["00112"]["ItemCode"] = "00112";
$default_oea["00112"]["ItemName"] = "Asia Pacific Age Group Swimming and Diving Championship";
$default_oea["00112"]["CatCode"] = "3";
$default_oea["00112"]["CatName"] = "Physical Development";

$default_oea["00113"]["ItemCode"] = "00113";
$default_oea["00113"]["ItemName"] = "Asia Pacific Age Group Swimming Championships  亞太區分齡游泳錦標賽";
$default_oea["00113"]["CatCode"] = "3";
$default_oea["00113"]["CatName"] = "Physical Development";

$default_oea["00114"]["ItemCode"] = "00114";
$default_oea["00114"]["ItemName"] = "Asia Pacific Chinese Music Exchance and Competition 亞太華樂交流觀摩賽";
$default_oea["00114"]["CatCode"] = "4";
$default_oea["00114"]["CatName"] = "Aesthetic Development";

$default_oea["00115"]["ItemCode"] = "00115";
$default_oea["00115"]["ItemName"] = "Asia Pacific Cup (10 & under)  亞太區單線曲棍球比賽";
$default_oea["00115"]["CatCode"] = "3";
$default_oea["00115"]["CatName"] = "Physical Development";

$default_oea["00116"]["ItemCode"] = "00116";
$default_oea["00116"]["ItemName"] = "Asia Pacific Harmonica Festival  亞太口琴節／亞太國際口琴藝術節";
$default_oea["00116"]["CatCode"] = "4";
$default_oea["00116"]["CatName"] = "Aesthetic Development";

$default_oea["00117"]["ItemCode"] = "00117";
$default_oea["00117"]["ItemName"] = "Asia Pacific High School Level Intelligence Ironman Creativity Competition  亞太區高中職組智慧鐵人創意競賽";
$default_oea["00117"]["CatCode"] = "2";
$default_oea["00117"]["CatName"] = "Career-related Experience";

$default_oea["00118"]["ItemCode"] = "00118";
$default_oea["00118"]["ItemName"] = "Asia Pacific Information and Communications Technology Awards (APICTA)  亞太區資訊及通訊科技大獎";
$default_oea["00118"]["CatCode"] = "2";
$default_oea["00118"]["CatName"] = "Career-related Experience";

$default_oea["00119"]["ItemCode"] = "00119";
$default_oea["00119"]["ItemName"] = "Asia Pacific Junior Golf Championship  亞太青少年高爾夫錦標賽";
$default_oea["00119"]["CatCode"] = "3";
$default_oea["00119"]["CatName"] = "Physical Development";

$default_oea["00120"]["ItemCode"] = "00120";
$default_oea["00120"]["ItemName"] = "Asia Pacific Junior Original Concert";
$default_oea["00120"]["CatCode"] = "4";
$default_oea["00120"]["CatName"] = "Aesthetic Development";

$default_oea["00121"]["ItemCode"] = "00121";
$default_oea["00121"]["ItemName"] = "Asia Pacific Lasallian Youth Congress";
$default_oea["00121"]["CatCode"] = "2";
$default_oea["00121"]["CatName"] = "Career-related Experience";

$default_oea["00122"]["ItemCode"] = "00122";
$default_oea["00122"]["ItemName"] = "Asia Pacific Orienteering Championships  亞洲及太平洋區野外定向錦標賽";
$default_oea["00122"]["CatCode"] = "3";
$default_oea["00122"]["CatName"] = "Physical Development";

$default_oea["00123"]["ItemCode"] = "00123";
$default_oea["00123"]["ItemName"] = "Asia Pacific Region JA Company of the Year Competition";
$default_oea["00123"]["CatCode"] = "2";
$default_oea["00123"]["CatName"] = "Career-related Experience";

$default_oea["00124"]["ItemCode"] = "00124";
$default_oea["00124"]["ItemName"] = "Asia Pacific Regional Life Saving Championships";
$default_oea["00124"]["CatCode"] = "3";
$default_oea["00124"]["CatName"] = "Physical Development";

$default_oea["00125"]["ItemCode"] = "00125";
$default_oea["00125"]["ItemName"] = "Asia Pacific Robot Contest";
$default_oea["00125"]["CatCode"] = "2";
$default_oea["00125"]["CatName"] = "Career-related Experience";

$default_oea["00126"]["ItemCode"] = "00126";
$default_oea["00126"]["ItemName"] = "Asia Pacific Tchoukball Championships  亞洲及太平洋巧固球錦標賽";
$default_oea["00126"]["CatCode"] = "3";
$default_oea["00126"]["CatName"] = "Physical Development";

$default_oea["00127"]["ItemCode"] = "00127";
$default_oea["00127"]["ItemName"] = "Asia Pacific Water Polo Tournament  亞太區水球錦標賽";
$default_oea["00127"]["CatCode"] = "3";
$default_oea["00127"]["CatName"] = "Physical Development";

$default_oea["00128"]["ItemCode"] = "00128";
$default_oea["00128"]["ItemName"] = "Asia Professional Basketball Tournament  亞洲職業籃球標賽";
$default_oea["00128"]["CatCode"] = "3";
$default_oea["00128"]["CatName"] = "Physical Development";

$default_oea["00129"]["ItemCode"] = "00129";
$default_oea["00129"]["ItemName"] = "Asia Shinsumo Championships  亞洲業餘相撲比賽";
$default_oea["00129"]["CatCode"] = "3";
$default_oea["00129"]["CatName"] = "Physical Development";

$default_oea["00130"]["ItemCode"] = "00130";
$default_oea["00130"]["ItemName"] = "Asia Swimming Championships";
$default_oea["00130"]["CatCode"] = "3";
$default_oea["00130"]["CatName"] = "Physical Development";

$default_oea["00131"]["ItemCode"] = "00131";
$default_oea["00131"]["ItemName"] = "Asia Youth Badminton Competition";
$default_oea["00131"]["CatCode"] = "3";
$default_oea["00131"]["CatName"] = "Physical Development";

$default_oea["00250"]["ItemCode"] = "00250";
$default_oea["00250"]["ItemName"] = "Asia-Pacific Championship Canada  亞太錦標賽(加拿大)";
$default_oea["00250"]["CatCode"] = "3";
$default_oea["00250"]["CatName"] = "Physical Development";

$default_oea["00251"]["ItemCode"] = "00251";
$default_oea["00251"]["ItemName"] = "Asia-Pacific Conference on Healthy Universities Booth Competition  亞太區健康大學會議";
$default_oea["00251"]["CatCode"] = "2";
$default_oea["00251"]["CatName"] = "Career-related Experience";

$default_oea["00252"]["ItemCode"] = "00252";
$default_oea["00252"]["ItemName"] = "Asia-Pacific IdN Design Award  亞太區IdN設計大獎";
$default_oea["00252"]["CatCode"] = "4";
$default_oea["00252"]["CatName"] = "Aesthetic Development";

$default_oea["00253"]["ItemCode"] = "00253";
$default_oea["00253"]["ItemName"] = "Asia-Pacific Orienteering Championship Individual Championships Australia  亞太野外定向錦標賽個人賽(澳洲)";
$default_oea["00253"]["CatCode"] = "3";
$default_oea["00253"]["CatName"] = "Physical Development";

$default_oea["00254"]["ItemCode"] = "00254";
$default_oea["00254"]["ItemName"] = "Asia-Pacific Regional Adolescent Health Congress Best Poster Presentation Award  亞太區青少年健康會議最佳海報展示獎";
$default_oea["00254"]["CatCode"] = "4";
$default_oea["00254"]["CatName"] = "Aesthetic Development";

$default_oea["00255"]["ItemCode"] = "00255";
$default_oea["00255"]["ItemName"] = "Asia-Pacific Regional Award for Outstanding Scouts";
$default_oea["00255"]["CatCode"] = "1";
$default_oea["00255"]["CatName"] = "Community Service";

$default_oea["00256"]["ItemCode"] = "00256";
$default_oea["00256"]["ItemName"] = "Asia-Pacific Student Entrepreneurship Forum cum Competition  亞太區學生企業家討論會暨比賽";
$default_oea["00256"]["CatCode"] = "2";
$default_oea["00256"]["CatName"] = "Career-related Experience";

$default_oea["00132"]["ItemCode"] = "00132";
$default_oea["00132"]["ItemName"] = "Asian 5-a-side Football Tournament, Macau  亞洲五人足球錦標賽";
$default_oea["00132"]["CatCode"] = "3";
$default_oea["00132"]["CatName"] = "Physical Development";

$default_oea["00133"]["ItemCode"] = "00133";
$default_oea["00133"]["ItemName"] = "Asian AA Grand Prix  亞洲格蘭披治田徑賽";
$default_oea["00133"]["CatCode"] = "3";
$default_oea["00133"]["CatName"] = "Physical Development";

$default_oea["00134"]["ItemCode"] = "00134";
$default_oea["00134"]["ItemName"] = "Asian Age Group Championships (Synchronised Swimming)  亞洲分齡錦標賽(韻律泳)";
$default_oea["00134"]["CatCode"] = "3";
$default_oea["00134"]["CatName"] = "Physical Development";

$default_oea["00135"]["ItemCode"] = "00135";
$default_oea["00135"]["ItemName"] = "Asian Age Group Swimming Championships";
$default_oea["00135"]["CatCode"] = "3";
$default_oea["00135"]["CatName"] = "Physical Development";

$default_oea["00136"]["ItemCode"] = "00136";
$default_oea["00136"]["ItemName"] = "Asian Aquathlon Championship";
$default_oea["00136"]["CatCode"] = "3";
$default_oea["00136"]["CatName"] = "Physical Development";

$default_oea["00137"]["ItemCode"] = "00137";
$default_oea["00137"]["ItemName"] = "Asian Archery Grand Prix";
$default_oea["00137"]["CatCode"] = "3";
$default_oea["00137"]["CatName"] = "Physical Development";

$default_oea["00138"]["ItemCode"] = "00138";
$default_oea["00138"]["ItemName"] = "Asian Art Exhibition of Students";
$default_oea["00138"]["CatCode"] = "4";
$default_oea["00138"]["CatName"] = "Aesthetic Development";

$default_oea["00139"]["ItemCode"] = "00139";
$default_oea["00139"]["ItemName"] = "Asian Baseball Cup";
$default_oea["00139"]["CatCode"] = "3";
$default_oea["00139"]["CatName"] = "Physical Development";

$default_oea["00140"]["ItemCode"] = "00140";
$default_oea["00140"]["ItemName"] = "Asian Canoe Championships  亞洲獨木舟錦標賽";
$default_oea["00140"]["CatCode"] = "3";
$default_oea["00140"]["CatName"] = "Physical Development";

$default_oea["00141"]["ItemCode"] = "00141";
$default_oea["00141"]["ItemName"] = "Asian Canoe Polo Champion Cup";
$default_oea["00141"]["CatCode"] = "3";
$default_oea["00141"]["CatName"] = "Physical Development";

$default_oea["00142"]["ItemCode"] = "00142";
$default_oea["00142"]["ItemName"] = "Asian Canoe Slalom and Wild Water Championships";
$default_oea["00142"]["CatCode"] = "3";
$default_oea["00142"]["CatName"] = "Physical Development";

$default_oea["00143"]["ItemCode"] = "00143";
$default_oea["00143"]["ItemName"] = "Asian Canoe Sprint Championships";
$default_oea["00143"]["CatCode"] = "3";
$default_oea["00143"]["CatName"] = "Physical Development";

$default_oea["00144"]["ItemCode"] = "00144";
$default_oea["00144"]["ItemName"] = "Asian Champions Cup for Men  亞洲冠軍球會盃";
$default_oea["00144"]["CatCode"] = "3";
$default_oea["00144"]["CatName"] = "Physical Development";

$default_oea["00145"]["ItemCode"] = "00145";
$default_oea["00145"]["ItemName"] = "Asian Championship Tournament  亞洲棒球錦標賽";
$default_oea["00145"]["CatCode"] = "3";
$default_oea["00145"]["CatName"] = "Physical Development";

$default_oea["00146"]["ItemCode"] = "00146";
$default_oea["00146"]["ItemName"] = "Asian Chinese Orchestra Competition";
$default_oea["00146"]["CatCode"] = "4";
$default_oea["00146"]["CatName"] = "Aesthetic Development";

$default_oea["00147"]["ItemCode"] = "00147";
$default_oea["00147"]["ItemName"] = "Asian Cities Gold Cup Taekwondo Championships  亞洲城巿金盃跆拳道錦標賽";
$default_oea["00147"]["CatCode"] = "3";
$default_oea["00147"]["CatName"] = "Physical Development";

$default_oea["00148"]["ItemCode"] = "00148";
$default_oea["00148"]["ItemName"] = "Asian Cities Junior Athletics Invitational Tournament";
$default_oea["00148"]["CatCode"] = "3";
$default_oea["00148"]["CatName"] = "Physical Development";

$default_oea["00149"]["ItemCode"] = "00149";
$default_oea["00149"]["ItemName"] = "Asian Cities Orienteering Championship  亞洲城巿野外定向錦標賽";
$default_oea["00149"]["CatCode"] = "3";
$default_oea["00149"]["CatName"] = "Physical Development";

$default_oea["00150"]["ItemCode"] = "00150";
$default_oea["00150"]["ItemName"] = "Asian City Men's Handball Championship  賀國慶迎回歸暨第二屆亞洲城市男子手球錦標賽";
$default_oea["00150"]["CatCode"] = "3";
$default_oea["00150"]["CatName"] = "Physical Development";

$default_oea["00151"]["ItemCode"] = "00151";
$default_oea["00151"]["ItemName"] = "Asian City Youth Age Group Invitational Athletic Meet  亞洲城市青年分齡田徑邀請賽";
$default_oea["00151"]["CatCode"] = "3";
$default_oea["00151"]["CatName"] = "Physical Development";

$default_oea["00152"]["ItemCode"] = "00152";
$default_oea["00152"]["ItemName"] = "Asian Class Regatta";
$default_oea["00152"]["CatCode"] = "3";
$default_oea["00152"]["CatName"] = "Physical Development";

$default_oea["00153"]["ItemCode"] = "00153";
$default_oea["00153"]["ItemName"] = "Asian Collegiate Fencing Championships  亞洲校際劍擊錦標賽";
$default_oea["00153"]["CatCode"] = "3";
$default_oea["00153"]["CatName"] = "Physical Development";

$default_oea["00154"]["ItemCode"] = "00154";
$default_oea["00154"]["ItemName"] = "Asian Composers League - Young Composers Award  亞洲作曲家聯會青年作曲家大獎";
$default_oea["00154"]["CatCode"] = "4";
$default_oea["00154"]["CatName"] = "Aesthetic Development";

$default_oea["00155"]["ItemCode"] = "00155";
$default_oea["00155"]["ItemName"] = "Asian Continental Championship and China Open Championship (Windsurfing) Youth Ranking";
$default_oea["00155"]["CatCode"] = "3";
$default_oea["00155"]["CatName"] = "Physical Development";

$default_oea["00156"]["ItemCode"] = "00156";
$default_oea["00156"]["ItemName"] = "Asian Continental Championships Mistral Junior One Design Class (Windsurfing)";
$default_oea["00156"]["CatCode"] = "3";
$default_oea["00156"]["CatName"] = "Physical Development";

$default_oea["00157"]["ItemCode"] = "00157";
$default_oea["00157"]["ItemName"] = "Asian Cup Rowing Championship  亞洲盃賽艇錦標賽";
$default_oea["00157"]["CatCode"] = "3";
$default_oea["00157"]["CatName"] = "Physical Development";

$default_oea["00159"]["ItemCode"] = "00159";
$default_oea["00159"]["ItemName"] = "Asian Cup Series (Asian Triathlon Confederation)  亞洲杯分站賽";
$default_oea["00159"]["CatCode"] = "3";
$default_oea["00159"]["CatName"] = "Physical Development";

$default_oea["00158"]["ItemCode"] = "00158";
$default_oea["00158"]["ItemName"] = "Asian Cup Series & National Championships (China Triathlon Sports Association)  亞洲杯分站賽暨國家錦標賽";
$default_oea["00158"]["CatCode"] = "3";
$default_oea["00158"]["CatName"] = "Physical Development";

$default_oea["00160"]["ItemCode"] = "00160";
$default_oea["00160"]["ItemName"] = "Asian Dragons Invitational  亞洲五小龍西洋棋邀請賽";
$default_oea["00160"]["CatCode"] = "4";
$default_oea["00160"]["CatName"] = "Aesthetic Development";

$default_oea["00161"]["ItemCode"] = "00161";
$default_oea["00161"]["ItemName"] = "Asian Duathlon Championship  亞洲陸上兩項鐵人錦標賽";
$default_oea["00161"]["CatCode"] = "3";
$default_oea["00161"]["CatName"] = "Physical Development";

$default_oea["00162"]["ItemCode"] = "00162";
$default_oea["00162"]["ItemName"] = "Asian Fencing Championship  亞洲劍擊錦標賽";
$default_oea["00162"]["CatCode"] = "3";
$default_oea["00162"]["CatName"] = "Physical Development";

$default_oea["00163"]["ItemCode"] = "00163";
$default_oea["00163"]["ItemName"] = "Asian Football Confederation  亞洲足球聯盟";
$default_oea["00163"]["CatCode"] = "3";
$default_oea["00163"]["CatName"] = "Physical Development";

$default_oea["00165"]["ItemCode"] = "00165";
$default_oea["00165"]["ItemName"] = "Asian Games  亞洲運動會 (Please provide details in \"Description\" box.)";
$default_oea["00165"]["CatCode"] = "3";
$default_oea["00165"]["CatName"] = "Physical Development";

$default_oea["00164"]["ItemCode"] = "00164";
$default_oea["00164"]["ItemName"] = "Asian Games - Hong Kong Asian Game 7's Rugby Football  香港亞洲運動會七人欖球賽";
$default_oea["00164"]["CatCode"] = "3";
$default_oea["00164"]["CatName"] = "Physical Development";

$default_oea["00166"]["ItemCode"] = "00166";
$default_oea["00166"]["ItemName"] = "Asian Games Drawing Competition  東亞運動會吉祥物繪畫比賽";
$default_oea["00166"]["CatCode"] = "4";
$default_oea["00166"]["CatName"] = "Aesthetic Development";

$default_oea["00167"]["ItemCode"] = "00167";
$default_oea["00167"]["ItemName"] = "Asian Gymnastics Championships";
$default_oea["00167"]["CatCode"] = "3";
$default_oea["00167"]["CatName"] = "Physical Development";

$default_oea["00168"]["ItemCode"] = "00168";
$default_oea["00168"]["ItemName"] = "Asian Gymnastics For All Festival  亞洲普及體操節";
$default_oea["00168"]["CatCode"] = "3";
$default_oea["00168"]["CatName"] = "Physical Development";

$default_oea["00169"]["ItemCode"] = "00169";
$default_oea["00169"]["ItemName"] = "Asian ICT Confederation AGAMES Software Programming Competition";
$default_oea["00169"]["CatCode"] = "2";
$default_oea["00169"]["CatName"] = "Career-related Experience";

$default_oea["00170"]["ItemCode"] = "00170";
$default_oea["00170"]["ItemName"] = "Asian Individual Squash Junior Women Championship";
$default_oea["00170"]["CatCode"] = "3";
$default_oea["00170"]["CatName"] = "Physical Development";

$default_oea["00171"]["ItemCode"] = "00171";
$default_oea["00171"]["ItemName"] = "Asian Indoor Athletics Invitation Tournament  亞洲室內田徑邀請賽";
$default_oea["00171"]["CatCode"] = "3";
$default_oea["00171"]["CatName"] = "Physical Development";

$default_oea["00172"]["ItemCode"] = "00172";
$default_oea["00172"]["ItemName"] = "Asian Indoor Cycling Championship  亞洲室內單車錦標賽";
$default_oea["00172"]["CatCode"] = "3";
$default_oea["00172"]["CatName"] = "Physical Development";

$default_oea["00173"]["ItemCode"] = "00173";
$default_oea["00173"]["ItemName"] = "Asian Indoor Games  亞洲室內運動會";
$default_oea["00173"]["CatCode"] = "3";
$default_oea["00173"]["CatName"] = "Physical Development";

$default_oea["00175"]["ItemCode"] = "00175";
$default_oea["00175"]["ItemName"] = "Asian Inter-City Tenpin Bowling Championships";
$default_oea["00175"]["CatCode"] = "3";
$default_oea["00175"]["CatName"] = "Physical Development";

$default_oea["00176"]["ItemCode"] = "00176";
$default_oea["00176"]["ItemName"] = "Asian Inter-University Multimedia Design Competition  亞洲區多媒體設計大學賽";
$default_oea["00176"]["CatCode"] = "4";
$default_oea["00176"]["CatName"] = "Aesthetic Development";

$default_oea["00174"]["ItemCode"] = "00174";
$default_oea["00174"]["ItemName"] = "Asian Intercity Bowling Championships";
$default_oea["00174"]["CatCode"] = "3";
$default_oea["00174"]["CatName"] = "Physical Development";

$default_oea["00177"]["ItemCode"] = "00177";
$default_oea["00177"]["ItemName"] = "Asian Journalism Fellowship for Paulinian Writers";
$default_oea["00177"]["CatCode"] = "4";
$default_oea["00177"]["CatName"] = "Aesthetic Development";

$default_oea["00178"]["ItemCode"] = "00178";
$default_oea["00178"]["ItemName"] = "Asian Judo Championships";
$default_oea["00178"]["CatCode"] = "3";
$default_oea["00178"]["CatName"] = "Physical Development";

$default_oea["00179"]["ItemCode"] = "00179";
$default_oea["00179"]["ItemName"] = "Asian Junior & Cadet Fencing Championship  亞洲青年暨青少年盃劍擊錦標賽";
$default_oea["00179"]["CatCode"] = "3";
$default_oea["00179"]["CatName"] = "Physical Development";

$default_oea["00180"]["ItemCode"] = "00180";
$default_oea["00180"]["ItemName"] = "Asian Junior and Cadet Karatedo Championship";
$default_oea["00180"]["CatCode"] = "3";
$default_oea["00180"]["CatName"] = "Physical Development";

$default_oea["00181"]["ItemCode"] = "00181";
$default_oea["00181"]["ItemName"] = "Asian Junior Archery Championships";
$default_oea["00181"]["CatCode"] = "3";
$default_oea["00181"]["CatName"] = "Physical Development";

$default_oea["00182"]["ItemCode"] = "00182";
$default_oea["00182"]["ItemName"] = "Asian Junior Athletic Championships  亞洲青少年田徑錦標賽";
$default_oea["00182"]["CatCode"] = "3";
$default_oea["00182"]["CatName"] = "Physical Development";

$default_oea["00183"]["ItemCode"] = "00183";
$default_oea["00183"]["ItemName"] = "Asian Junior Figure Skating Challenge  亞洲青少年花樣滑冰挑戰賽";
$default_oea["00183"]["CatCode"] = "3";
$default_oea["00183"]["CatName"] = "Physical Development";

$default_oea["00184"]["ItemCode"] = "00184";
$default_oea["00184"]["ItemName"] = "Asian Junior Individual Squash Championship";
$default_oea["00184"]["CatCode"] = "3";
$default_oea["00184"]["CatName"] = "Physical Development";

$default_oea["00185"]["ItemCode"] = "00185";
$default_oea["00185"]["ItemName"] = "Asian Junior Rowing Championships  亞洲青少年賽艇錦標賽";
$default_oea["00185"]["CatCode"] = "3";
$default_oea["00185"]["CatName"] = "Physical Development";

$default_oea["00186"]["ItemCode"] = "00186";
$default_oea["00186"]["ItemName"] = "Asian Junior Squash Championships  亞洲青少年壁球錦標賽";
$default_oea["00186"]["CatCode"] = "3";
$default_oea["00186"]["CatName"] = "Physical Development";

$default_oea["00187"]["ItemCode"] = "00187";
$default_oea["00187"]["ItemName"] = "Asian Junior Table Tennis Championships  亞洲青少年乒乓球錦標賽";
$default_oea["00187"]["CatCode"] = "3";
$default_oea["00187"]["CatName"] = "Physical Development";

$default_oea["00188"]["ItemCode"] = "00188";
$default_oea["00188"]["ItemName"] = "Asian Junior Taekwondo Championships";
$default_oea["00188"]["CatCode"] = "3";
$default_oea["00188"]["CatName"] = "Physical Development";

$default_oea["00189"]["ItemCode"] = "00189";
$default_oea["00189"]["ItemName"] = "Asian Junior Women's Squash Team Championship";
$default_oea["00189"]["CatCode"] = "3";
$default_oea["00189"]["CatName"] = "Physical Development";

$default_oea["00190"]["ItemCode"] = "00190";
$default_oea["00190"]["ItemName"] = "Asian Junior Women's Volleyball Championship";
$default_oea["00190"]["CatCode"] = "3";
$default_oea["00190"]["CatName"] = "Physical Development";

$default_oea["00191"]["ItemCode"] = "00191";
$default_oea["00191"]["ItemName"] = "Asian Junior Wushu Championships";
$default_oea["00191"]["CatCode"] = "3";
$default_oea["00191"]["CatName"] = "Physical Development";

$default_oea["00192"]["ItemCode"] = "00192";
$default_oea["00192"]["ItemName"] = "Asian Juniors Badminton Championships  亞洲青年羽毛球錦標賽";
$default_oea["00192"]["CatCode"] = "3";
$default_oea["00192"]["CatName"] = "Physical Development";

$default_oea["00193"]["ItemCode"] = "00193";
$default_oea["00193"]["ItemName"] = "Asian Karate-do Federation Junior & Cadet Championships  亞洲青少年空手道錦標賽";
$default_oea["00193"]["CatCode"] = "3";
$default_oea["00193"]["CatName"] = "Physical Development";

$default_oea["00194"]["ItemCode"] = "00194";
$default_oea["00194"]["ItemName"] = "Asian Karatedo Invitation Tournament  亞洲空手道邀請賽";
$default_oea["00194"]["CatCode"] = "3";
$default_oea["00194"]["CatName"] = "Physical Development";

$default_oea["00195"]["ItemCode"] = "00195";
$default_oea["00195"]["ItemName"] = "Asian Korfball Championship";
$default_oea["00195"]["CatCode"] = "3";
$default_oea["00195"]["CatName"] = "Physical Development";

$default_oea["00196"]["ItemCode"] = "00196";
$default_oea["00196"]["ItemName"] = "Asian Lawn Bowls Under 25 Singles Championship";
$default_oea["00196"]["CatCode"] = "3";
$default_oea["00196"]["CatName"] = "Physical Development";

$default_oea["00197"]["ItemCode"] = "00197";
$default_oea["00197"]["ItemName"] = "Asian Life Saving (Pool) Championships";
$default_oea["00197"]["CatCode"] = "3";
$default_oea["00197"]["CatName"] = "Physical Development";

$default_oea["00198"]["ItemCode"] = "00198";
$default_oea["00198"]["ItemName"] = "Asian Life Saving Championship";
$default_oea["00198"]["CatCode"] = "3";
$default_oea["00198"]["CatName"] = "Physical Development";

$default_oea["00199"]["ItemCode"] = "00199";
$default_oea["00199"]["ItemName"] = "Asian Machine Rowing Championships  亞洲室內划艇錦標賽";
$default_oea["00199"]["CatCode"] = "3";
$default_oea["00199"]["CatName"] = "Physical Development";

$default_oea["00200"]["ItemCode"] = "00200";
$default_oea["00200"]["ItemName"] = "Asian Man U21 Handball Championship  亞洲男子廿一歲以下錦標賽";
$default_oea["00200"]["CatCode"] = "3";
$default_oea["00200"]["CatName"] = "Physical Development";

$default_oea["00201"]["ItemCode"] = "00201";
$default_oea["00201"]["ItemName"] = "Asian Men's Handball Tournament  賀國慶迎回歸暨亞洲城市男子手球錦標賽";
$default_oea["00201"]["CatCode"] = "3";
$default_oea["00201"]["CatName"] = "Physical Development";

$default_oea["00202"]["ItemCode"] = "00202";
$default_oea["00202"]["ItemName"] = "Asian Men'z Junior Championship (Handball)";
$default_oea["00202"]["CatCode"] = "3";
$default_oea["00202"]["CatName"] = "Physical Development";

$default_oea["00203"]["ItemCode"] = "00203";
$default_oea["00203"]["ItemName"] = "Asian Mountain Bike Championship  亞洲山地車錦標賽";
$default_oea["00203"]["CatCode"] = "3";
$default_oea["00203"]["CatName"] = "Physical Development";

$default_oea["00204"]["ItemCode"] = "00204";
$default_oea["00204"]["ItemName"] = "Asian Oceanian Korfball Championship  亞大合球錦標賽";
$default_oea["00204"]["CatCode"] = "3";
$default_oea["00204"]["CatName"] = "Physical Development";

$default_oea["00205"]["ItemCode"] = "00205";
$default_oea["00205"]["ItemName"] = "Asian Oceanian Youth Korfball Championships  亞大青年合球錦標賽";
$default_oea["00205"]["CatCode"] = "3";
$default_oea["00205"]["CatName"] = "Physical Development";

$default_oea["00206"]["ItemCode"] = "00206";
$default_oea["00206"]["ItemName"] = "Asian Open Water Swimming Championships  亞洲公開水域游泳錦標賽";
$default_oea["00206"]["CatCode"] = "3";
$default_oea["00206"]["CatName"] = "Physical Development";

$default_oea["00207"]["ItemCode"] = "00207";
$default_oea["00207"]["ItemName"] = "Asian Pacific Cities Invitational Dragon Boat Races";
$default_oea["00207"]["CatCode"] = "3";
$default_oea["00207"]["CatName"] = "Physical Development";

$default_oea["00208"]["ItemCode"] = "00208";
$default_oea["00208"]["ItemName"] = "Asian Pacific Goju-Kai Karate-Do Championships";
$default_oea["00208"]["CatCode"] = "3";
$default_oea["00208"]["CatName"] = "Physical Development";

$default_oea["00209"]["ItemCode"] = "00209";
$default_oea["00209"]["ItemName"] = "Asian Pacific Lifesaving Championship";
$default_oea["00209"]["CatCode"] = "3";
$default_oea["00209"]["CatName"] = "Physical Development";

$default_oea["00210"]["ItemCode"] = "00210";
$default_oea["00210"]["ItemName"] = "Asian Roller Skating Championships  亞洲滑輪溜冰錦標賽";
$default_oea["00210"]["CatCode"] = "3";
$default_oea["00210"]["CatName"] = "Physical Development";

$default_oea["00211"]["ItemCode"] = "00211";
$default_oea["00211"]["ItemName"] = "Asian Roller Sports Championships";
$default_oea["00211"]["CatCode"] = "3";
$default_oea["00211"]["CatName"] = "Physical Development";

$default_oea["00212"]["ItemCode"] = "00212";
$default_oea["00212"]["ItemName"] = "Asian Rope Skipping Championship  亞洲跳繩比賽";
$default_oea["00212"]["CatCode"] = "3";
$default_oea["00212"]["CatName"] = "Physical Development";

$default_oea["00213"]["ItemCode"] = "00213";
$default_oea["00213"]["ItemName"] = "Asian Rowing Championship  亞洲划艇錦標賽";
$default_oea["00213"]["CatCode"] = "3";
$default_oea["00213"]["CatName"] = "Physical Development";

$default_oea["00214"]["ItemCode"] = "00214";
$default_oea["00214"]["ItemName"] = "Asian Rugby Football Tournament";
$default_oea["00214"]["CatCode"] = "3";
$default_oea["00214"]["CatName"] = "Physical Development";

$default_oea["00215"]["ItemCode"] = "00215";
$default_oea["00215"]["ItemName"] = "Asian Sailing Championship";
$default_oea["00215"]["CatCode"] = "3";
$default_oea["00215"]["CatName"] = "Physical Development";

$default_oea["00216"]["ItemCode"] = "00216";
$default_oea["00216"]["ItemName"] = "Asian School Tenpin Bowling Championships";
$default_oea["00216"]["CatCode"] = "3";
$default_oea["00216"]["CatName"] = "Physical Development";

$default_oea["00217"]["ItemCode"] = "00217";
$default_oea["00217"]["ItemName"] = "Asian Schools Badminton Championships";
$default_oea["00217"]["CatCode"] = "3";
$default_oea["00217"]["CatName"] = "Physical Development";

$default_oea["00218"]["ItemCode"] = "00218";
$default_oea["00218"]["ItemName"] = "Asian Schools Basketball Championships";
$default_oea["00218"]["CatCode"] = "3";
$default_oea["00218"]["CatName"] = "Physical Development";

$default_oea["00219"]["ItemCode"] = "00219";
$default_oea["00219"]["ItemName"] = "Asian Schools Football Championship  亞洲學界足球錦標賽";
$default_oea["00219"]["CatCode"] = "3";
$default_oea["00219"]["CatName"] = "Physical Development";

$default_oea["00220"]["ItemCode"] = "00220";
$default_oea["00220"]["ItemName"] = "Asian Schools Handball Championship  亞洲學界手球比賽";
$default_oea["00220"]["CatCode"] = "3";
$default_oea["00220"]["CatName"] = "Physical Development";

$default_oea["00221"]["ItemCode"] = "00221";
$default_oea["00221"]["ItemName"] = "Asian Schools International Football Tournament";
$default_oea["00221"]["CatCode"] = "3";
$default_oea["00221"]["CatName"] = "Physical Development";

$default_oea["00222"]["ItemCode"] = "00222";
$default_oea["00222"]["ItemName"] = "Asian Schools Swimming Championship  亞洲中學生游泳錦標賽";
$default_oea["00222"]["CatCode"] = "3";
$default_oea["00222"]["CatName"] = "Physical Development";

$default_oea["00223"]["ItemCode"] = "00223";
$default_oea["00223"]["ItemName"] = "Asian Schools Table Tennis Championships  亞洲學界乒乓球錦標賽";
$default_oea["00223"]["CatCode"] = "3";
$default_oea["00223"]["CatName"] = "Physical Development";

$default_oea["00224"]["ItemCode"] = "00224";
$default_oea["00224"]["ItemName"] = "Asian Schools Volleyball Championships  亞洲中學生排球錦標賽";
$default_oea["00224"]["CatCode"] = "3";
$default_oea["00224"]["CatName"] = "Physical Development";

$default_oea["00225"]["ItemCode"] = "00225";
$default_oea["00225"]["ItemName"] = "Asian Short Track Speed Skating Competition";
$default_oea["00225"]["CatCode"] = "3";
$default_oea["00225"]["CatName"] = "Physical Development";

$default_oea["00226"]["ItemCode"] = "00226";
$default_oea["00226"]["ItemName"] = "Asian Sports Climbing Championships  亞洲攀登速度賽";
$default_oea["00226"]["CatCode"] = "3";
$default_oea["00226"]["CatName"] = "Physical Development";

$default_oea["00227"]["ItemCode"] = "00227";
$default_oea["00227"]["ItemName"] = "Asian Squash Championships  亞洲壁球錦標賽";
$default_oea["00227"]["CatCode"] = "3";
$default_oea["00227"]["CatName"] = "Physical Development";

$default_oea["00228"]["ItemCode"] = "00228";
$default_oea["00228"]["ItemName"] = "Asian Strategy Game by The CLP Power Asia";
$default_oea["00228"]["CatCode"] = "5";
$default_oea["00228"]["CatName"] = "Moral and Civic Education";

$default_oea["00229"]["ItemCode"] = "00229";
$default_oea["00229"]["ItemName"] = "Asian Swimming Championships  亞洲游泳錦標賽";
$default_oea["00229"]["CatCode"] = "3";
$default_oea["00229"]["CatName"] = "Physical Development";

$default_oea["00230"]["ItemCode"] = "00230";
$default_oea["00230"]["ItemName"] = "Asian Triathlon Championships  亞洲三項鐵人錦標賽";
$default_oea["00230"]["CatCode"] = "3";
$default_oea["00230"]["CatName"] = "Physical Development";

$default_oea["00231"]["ItemCode"] = "00231";
$default_oea["00231"]["ItemName"] = "Asian Triathlon Confederation (Please provide details in \"Description\" box.)";
$default_oea["00231"]["CatCode"] = "3";
$default_oea["00231"]["CatName"] = "Physical Development";

$default_oea["00232"]["ItemCode"] = "00232";
$default_oea["00232"]["ItemName"] = "Asian U20 Beach Volleyball Championships";
$default_oea["00232"]["CatCode"] = "3";
$default_oea["00232"]["CatName"] = "Physical Development";

$default_oea["00233"]["ItemCode"] = "00233";
$default_oea["00233"]["ItemName"] = "Asian Windsurfing Championships";
$default_oea["00233"]["CatCode"] = "3";
$default_oea["00233"]["CatName"] = "Physical Development";

$default_oea["00234"]["ItemCode"] = "00234";
$default_oea["00234"]["ItemName"] = "Asian Women's Handball Tournament  亞洲女子手球錦標賽";
$default_oea["00234"]["CatCode"] = "3";
$default_oea["00234"]["CatName"] = "Physical Development";

$default_oea["00235"]["ItemCode"] = "00235";
$default_oea["00235"]["ItemName"] = "Asian Women's Softball Championship";
$default_oea["00235"]["CatCode"] = "3";
$default_oea["00235"]["CatName"] = "Physical Development";

$default_oea["00236"]["ItemCode"] = "00236";
$default_oea["00236"]["ItemName"] = "Asian X-Game Climbing  Competition亞洲XGame運動攀登總賽";
$default_oea["00236"]["CatCode"] = "3";
$default_oea["00236"]["CatName"] = "Physical Development";

$default_oea["00237"]["ItemCode"] = "00237";
$default_oea["00237"]["ItemName"] = "Asian Xiangqi Championships  亞洲象棋錦標賽";
$default_oea["00237"]["CatCode"] = "4";
$default_oea["00237"]["CatName"] = "Aesthetic Development";

$default_oea["00238"]["ItemCode"] = "00238";
$default_oea["00238"]["ItemName"] = "Asian Xiangqi Masters Invitation Tournament  亞洲象棋名手邀請賽";
$default_oea["00238"]["CatCode"] = "4";
$default_oea["00238"]["CatName"] = "Aesthetic Development";

$default_oea["00239"]["ItemCode"] = "00239";
$default_oea["00239"]["ItemName"] = "Asian Youth and Cadet Fencing Championship  亞洲青少年劍擊錦標賽";
$default_oea["00239"]["CatCode"] = "3";
$default_oea["00239"]["CatName"] = "Physical Development";

$default_oea["00240"]["ItemCode"] = "00240";
$default_oea["00240"]["ItemName"] = "Asian Youth Boys' Volleyball Championship";
$default_oea["00240"]["CatCode"] = "3";
$default_oea["00240"]["CatName"] = "Physical Development";

$default_oea["00241"]["ItemCode"] = "00241";
$default_oea["00241"]["ItemName"] = "Asian Youth Chinese National Musical Instrument Invitational Tournament";
$default_oea["00241"]["CatCode"] = "4";
$default_oea["00241"]["CatName"] = "Aesthetic Development";

$default_oea["00242"]["ItemCode"] = "00242";
$default_oea["00242"]["ItemName"] = "Asian Youth Cup (Please provide details in \"Description\" box.)";
$default_oea["00242"]["CatCode"] = "5";
$default_oea["00242"]["CatName"] = "Moral and Civic Education";

$default_oea["00243"]["ItemCode"] = "00243";
$default_oea["00243"]["ItemName"] = "Asian Youth Game  亞洲青年運動會";
$default_oea["00243"]["CatCode"] = "3";
$default_oea["00243"]["CatName"] = "Physical Development";

$default_oea["00244"]["ItemCode"] = "00244";
$default_oea["00244"]["ItemName"] = "Asian Youth Music Competition  亞洲青少年音樂比賽";
$default_oea["00244"]["CatCode"] = "4";
$default_oea["00244"]["CatName"] = "Aesthetic Development";

$default_oea["00245"]["ItemCode"] = "00245";
$default_oea["00245"]["ItemName"] = "Asian Youth Netball Championship";
$default_oea["00245"]["CatCode"] = "3";
$default_oea["00245"]["CatName"] = "Physical Development";

$default_oea["00246"]["ItemCode"] = "00246";
$default_oea["00246"]["ItemName"] = "Asian Youth Rowing Championships";
$default_oea["00246"]["CatCode"] = "3";
$default_oea["00246"]["CatName"] = "Physical Development";

$default_oea["00247"]["ItemCode"] = "00247";
$default_oea["00247"]["ItemName"] = "Asian Youth Rugby Tournament";
$default_oea["00247"]["CatCode"] = "3";
$default_oea["00247"]["CatName"] = "Physical Development";

$default_oea["00248"]["ItemCode"] = "00248";
$default_oea["00248"]["ItemName"] = "Asian Youth Sport Climbing Championship  亞洲青少年運動攀登錦標賽";
$default_oea["00248"]["CatCode"] = "3";
$default_oea["00248"]["CatName"] = "Physical Development";

$default_oea["00249"]["ItemCode"] = "00249";
$default_oea["00249"]["ItemName"] = "Asian Youth Tournament Play Traditional Chinese Musical Instruments  亞洲青少年中國民族樂器邀請賽";
$default_oea["00249"]["CatCode"] = "4";
$default_oea["00249"]["CatName"] = "Aesthetic Development";

$default_oea["00257"]["ItemCode"] = "00257";
$default_oea["00257"]["ItemName"] = "Aspiring Architect Students Category of the International Architecture + Awards  建築師學生組國際建築大獎";
$default_oea["00257"]["CatCode"] = "4";
$default_oea["00257"]["CatName"] = "Aesthetic Development";

$default_oea["00258"]["ItemCode"] = "00258";
$default_oea["00258"]["ItemName"] = "Astronomy Project Competition (APC) (Please provide details in \"Description\" box.)";
$default_oea["00258"]["CatCode"] = "2";
$default_oea["00258"]["CatName"] = "Career-related Experience";

$default_oea["00099"]["ItemCode"] = "00099";
$default_oea["00099"]["ItemName"] = "ASEAN Optimist Championship";
$default_oea["00099"]["CatCode"] = "3";
$default_oea["00099"]["CatName"] = "Physical Development";

$default_oea["00100"]["ItemCode"] = "00100";
$default_oea["00100"]["ItemName"] = "ASEAN Varsities' Debate  東盟大學辯論賽";
$default_oea["00100"]["CatCode"] = "5";
$default_oea["00100"]["CatName"] = "Moral and Civic Education";

$default_oea["00259"]["ItemCode"] = "00259";
$default_oea["00259"]["ItemName"] = "Athlete of the Year";
$default_oea["00259"]["CatCode"] = "3";
$default_oea["00259"]["CatName"] = "Physical Development";

$default_oea["00260"]["ItemCode"] = "00260";
$default_oea["00260"]["ItemName"] = "Athletic Meet: 400m 田徑比賽: 400米";
$default_oea["00260"]["CatCode"] = "3";
$default_oea["00260"]["CatName"] = "Physical Development";

$default_oea["00261"]["ItemCode"] = "00261";
$default_oea["00261"]["ItemName"] = "Athletics Competition (School) 校內田徑賽";
$default_oea["00261"]["CatCode"] = "3";
$default_oea["00261"]["CatName"] = "Physical Development";

$default_oea["00262"]["ItemCode"] = "00262";
$default_oea["00262"]["ItemName"] = "Athletics Team (School) 學校田徑隊";
$default_oea["00262"]["CatCode"] = "3";
$default_oea["00262"]["CatName"] = "Physical Development";

$default_oea["00263"]["ItemCode"] = "00263";
$default_oea["00263"]["ItemName"] = "Atlanta Wheelchair Fencing World Cup  亞特蘭大輪椅劍擊世界盃";
$default_oea["00263"]["CatCode"] = "3";
$default_oea["00263"]["CatName"] = "Physical Development";

$default_oea["00264"]["ItemCode"] = "00264";
$default_oea["00264"]["ItemName"] = "Aurora International Youth Festival For the Performing Arts";
$default_oea["00264"]["CatCode"] = "4";
$default_oea["00264"]["CatName"] = "Aesthetic Development";

$default_oea["00265"]["ItemCode"] = "00265";
$default_oea["00265"]["ItemName"] = "Australia Education Link Schools English Writing Contest";
$default_oea["00265"]["CatCode"] = "4";
$default_oea["00265"]["CatName"] = "Aesthetic Development";

$default_oea["00266"]["ItemCode"] = "00266";
$default_oea["00266"]["ItemName"] = "Australia Queensland Championships  澳洲昆士蘭錦標賽";
$default_oea["00266"]["CatCode"] = "3";
$default_oea["00266"]["CatName"] = "Physical Development";

$default_oea["00267"]["ItemCode"] = "00267";
$default_oea["00267"]["ItemName"] = "Australian Flute Festival - Flute Choir Competition";
$default_oea["00267"]["CatCode"] = "4";
$default_oea["00267"]["CatName"] = "Aesthetic Development";

$default_oea["00268"]["ItemCode"] = "00268";
$default_oea["00268"]["ItemName"] = "Australian Mathematics Competition - Australian Mathematics Trust";
$default_oea["00268"]["CatCode"] = "2";
$default_oea["00268"]["CatName"] = "Career-related Experience";

$default_oea["00269"]["ItemCode"] = "00269";
$default_oea["00269"]["ItemName"] = "Australian Mathematics Competition For the Westpac Awards";
$default_oea["00269"]["CatCode"] = "2";
$default_oea["00269"]["CatName"] = "Career-related Experience";

$default_oea["00270"]["ItemCode"] = "00270";
$default_oea["00270"]["ItemName"] = "Australian Memory Championships";
$default_oea["00270"]["CatCode"] = "2";
$default_oea["00270"]["CatName"] = "Career-related Experience";

$default_oea["00271"]["ItemCode"] = "00271";
$default_oea["00271"]["ItemName"] = "Australian Youth Olympic Festival  澳洲青年奧運會";
$default_oea["00271"]["CatCode"] = "3";
$default_oea["00271"]["CatName"] = "Physical Development";

$default_oea["00272"]["ItemCode"] = "00272";
$default_oea["00272"]["ItemName"] = "Automotive Technology-Taster Program 汽車科技-應用學習導引課程";
$default_oea["00272"]["CatCode"] = "2";
$default_oea["00272"]["CatName"] = "Career-related Experience";

$default_oea["00273"]["ItemCode"] = "00273";
$default_oea["00273"]["ItemName"] = "Aviation Studies and OLE Activity Day 航空學及其他學習經歷活動日";
$default_oea["00273"]["CatCode"] = "2";
$default_oea["00273"]["CatName"] = "Career-related Experience";

$default_oea["00274"]["ItemCode"] = "00274";
$default_oea["00274"]["ItemName"] = "Aviva Asian Bowling Tour  Aviva亞洲保齡球巡迴賽";
$default_oea["00274"]["CatCode"] = "3";
$default_oea["00274"]["CatName"] = "Physical Development";

$default_oea["00275"]["ItemCode"] = "00275";
$default_oea["00275"]["ItemName"] = "Award for Veteran Handbell Ringer資深手鈴隊隊員嘉許獎";
$default_oea["00275"]["CatCode"] = "4";
$default_oea["00275"]["CatName"] = "Aesthetic Development";

$default_oea["00276"]["ItemCode"] = "00276";
$default_oea["00276"]["ItemName"] = "Award in Anti-smoking Radio Programme - Metro Broadcast Corporation Ltd.";
$default_oea["00276"]["CatCode"] = "4";
$default_oea["00276"]["CatName"] = "Aesthetic Development";

$default_oea["00277"]["ItemCode"] = "00277";
$default_oea["00277"]["ItemName"] = "Awarding Program for Future Scientists";
$default_oea["00277"]["CatCode"] = "2";
$default_oea["00277"]["CatName"] = "Career-related Experience";

$default_oea["00278"]["ItemCode"] = "00278";
$default_oea["00278"]["ItemName"] = "Awards for Outstanding European Club Association (ECA)-";
$default_oea["00278"]["CatCode"] = "4";
$default_oea["00278"]["CatName"] = "Aesthetic Development";

$default_oea["00279"]["ItemCode"] = "00279";
$default_oea["00279"]["ItemName"] = "AWE Photo Competition";
$default_oea["00279"]["CatCode"] = "4";
$default_oea["00279"]["CatName"] = "Aesthetic Development";

$default_oea["00280"]["ItemCode"] = "00280";
$default_oea["00280"]["ItemName"] = "AYP challenge of cross-country navigation";
$default_oea["00280"]["CatCode"] = "3";
$default_oea["00280"]["CatName"] = "Physical Development";

$default_oea["00281"]["ItemCode"] = "00281";
$default_oea["00281"]["ItemName"] = "AYP Expedition Course";
$default_oea["00281"]["CatCode"] = "3";
$default_oea["00281"]["CatName"] = "Physical Development";

$default_oea["00282"]["ItemCode"] = "00282";
$default_oea["00282"]["ItemName"] = "AYP Joint-school Service Project";
$default_oea["00282"]["CatCode"] = "1";
$default_oea["00282"]["CatName"] = "Community Service";

$default_oea["00283"]["ItemCode"] = "00283";
$default_oea["00283"]["ItemName"] = "AYP Week";
$default_oea["00283"]["CatCode"] = "3";
$default_oea["00283"]["CatName"] = "Physical Development";

$default_oea["00328"]["ItemCode"] = "00328";
$default_oea["00328"]["ItemName"] = "bodw 2008 - Education Corner Creative Workshop at IDT Expo  設計營商周 - 學生園地";
$default_oea["00328"]["CatCode"] = "4";
$default_oea["00328"]["CatName"] = "Aesthetic Development";

$default_oea["00284"]["ItemCode"] = "00284";
$default_oea["00284"]["ItemName"] = "Baden-Powell Award  (also known as Baden-Powell Scout Award (BPSA))貝登堡獎章";
$default_oea["00284"]["CatCode"] = "1";
$default_oea["00284"]["CatName"] = "Community Service";

$default_oea["00285"]["ItemCode"] = "00285";
$default_oea["00285"]["ItemName"] = "Badges Scheme Award Programme - Hang Seng Table Tennis Academy";
$default_oea["00285"]["CatCode"] = "3";
$default_oea["00285"]["CatName"] = "Physical Development";

$default_oea["00286"]["ItemCode"] = "00286";
$default_oea["00286"]["ItemName"] = "Badminton Asia Junior Mixed Team Championship";
$default_oea["00286"]["CatCode"] = "3";
$default_oea["00286"]["CatName"] = "Physical Development";

$default_oea["00287"]["ItemCode"] = "00287";
$default_oea["00287"]["ItemName"] = "Balboa Bay Club JR Tournament  Balboa Bay Club 青少年錦標賽";
$default_oea["00287"]["CatCode"] = "3";
$default_oea["00287"]["CatName"] = "Physical Development";

$default_oea["00288"]["ItemCode"] = "00288";
$default_oea["00288"]["ItemName"] = "Bangkok International Rugby Sevens Tournament";
$default_oea["00288"]["CatCode"] = "3";
$default_oea["00288"]["CatName"] = "Physical Development";

$default_oea["00289"]["ItemCode"] = "00289";
$default_oea["00289"]["ItemName"] = "Bank of China (Hong Kong) 54th Festival of Sport All Hong Kong Schools Wushu Competition 2010-2011 - Nanquan";
$default_oea["00289"]["CatCode"] = "3";
$default_oea["00289"]["CatName"] = "Physical Development";

$default_oea["00290"]["ItemCode"] = "00290";
$default_oea["00290"]["ItemName"] = "Bank of China (Hong Kong) Festival of Sport 中銀香港體育節";
$default_oea["00290"]["CatCode"] = "3";
$default_oea["00290"]["CatName"] = "Physical Development";

$default_oea["00291"]["ItemCode"] = "00291";
$default_oea["00291"]["ItemName"] = "Basic First Aid Course";
$default_oea["00291"]["CatCode"] = "2";
$default_oea["00291"]["CatName"] = "Career-related Experience";

$default_oea["00292"]["ItemCode"] = "00292";
$default_oea["00292"]["ItemName"] = "Basic Law and National Education Online Quiz 基本法及國民常識網上問答比賽";
$default_oea["00292"]["CatCode"] = "5";
$default_oea["00292"]["CatName"] = "Moral and Civic Education";

$default_oea["00293"]["ItemCode"] = "00293";
$default_oea["00293"]["ItemName"] = "Basic Law Quiz Competition";
$default_oea["00293"]["CatCode"] = "5";
$default_oea["00293"]["CatName"] = "Moral and Civic Education";

$default_oea["00294"]["ItemCode"] = "00294";
$default_oea["00294"]["ItemName"] = "Beethoven Piano Competition Hong Kong";
$default_oea["00294"]["CatCode"] = "4";
$default_oea["00294"]["CatName"] = "Aesthetic Development";

$default_oea["00295"]["ItemCode"] = "00295";
$default_oea["00295"]["ItemName"] = "Beijing & Hong Kong Chinese Calligraphy & Painting Competition (Teenagers)";
$default_oea["00295"]["CatCode"] = "4";
$default_oea["00295"]["CatName"] = "Aesthetic Development";

$default_oea["00296"]["ItemCode"] = "00296";
$default_oea["00296"]["ItemName"] = "Beijing & Hong Kong Youth Calligraphy and Painting Competition - Xinhua News Agency";
$default_oea["00296"]["CatCode"] = "4";
$default_oea["00296"]["CatName"] = "Aesthetic Development";

$default_oea["00297"]["ItemCode"] = "00297";
$default_oea["00297"]["ItemName"] = "Beijing 2008 National Children's Painting Photography activities  北京2008全國青少年兒童書畫攝影展評活動";
$default_oea["00297"]["CatCode"] = "4";
$default_oea["00297"]["CatName"] = "Aesthetic Development";

$default_oea["00298"]["ItemCode"] = "00298";
$default_oea["00298"]["ItemName"] = "Beijing and Hong Kong Junior Chinese Calligraphy & Painting Competition";
$default_oea["00298"]["CatCode"] = "4";
$default_oea["00298"]["CatName"] = "Aesthetic Development";

$default_oea["00299"]["ItemCode"] = "00299";
$default_oea["00299"]["ItemName"] = "Beijing International Children's Painting Overseas Chinese Grand Prix  北京國際華僑華人少兒書畫大奬賽";
$default_oea["00299"]["CatCode"] = "4";
$default_oea["00299"]["CatName"] = "Aesthetic Development";

$default_oea["00300"]["ItemCode"] = "00300";
$default_oea["00300"]["ItemName"] = "Beijing Junior Artists Painting Competition  北京兒童藝術家繪畫比賽";
$default_oea["00300"]["CatCode"] = "4";
$default_oea["00300"]["CatName"] = "Aesthetic Development";

$default_oea["00301"]["ItemCode"] = "00301";
$default_oea["00301"]["ItemName"] = "Beijing National Youth and Children's Calligraphy and Painting Photography Competition  北京全國青少年兒童書畫攝影大賽";
$default_oea["00301"]["CatCode"] = "4";
$default_oea["00301"]["CatName"] = "Aesthetic Development";

$default_oea["00302"]["ItemCode"] = "00302";
$default_oea["00302"]["ItemName"] = "Beijing Olympic Games Draw The Letter ETEGAMI Show  北京奧運會世界畫信ETEGAMI展";
$default_oea["00302"]["CatCode"] = "4";
$default_oea["00302"]["CatName"] = "Aesthetic Development";

$default_oea["00303"]["ItemCode"] = "00303";
$default_oea["00303"]["ItemName"] = "Beijing Olympic Theme Poster Design Contest for university students  北京全國大學生主題招貼設計大賽";
$default_oea["00303"]["CatCode"] = "4";
$default_oea["00303"]["CatName"] = "Aesthetic Development";

$default_oea["00304"]["ItemCode"] = "00304";
$default_oea["00304"]["ItemName"] = "Beijing Tour: Handbell北京隊(手鈴)";
$default_oea["00304"]["CatCode"] = "4";
$default_oea["00304"]["CatName"] = "Aesthetic Development";

$default_oea["00305"]["ItemCode"] = "00305";
$default_oea["00305"]["ItemName"] = "Beijing Zhong Zhou Bei Chinese Orchestra Competition  少兒民族樂隊中州杯北京邀請賽";
$default_oea["00305"]["CatCode"] = "4";
$default_oea["00305"]["CatName"] = "Aesthetic Development";

$default_oea["00307"]["ItemCode"] = "00307";
$default_oea["00307"]["ItemName"] = "Beijing-HK Youth Handball Championship  京港青少年手球錦標賽";
$default_oea["00307"]["CatCode"] = "3";
$default_oea["00307"]["CatName"] = "Physical Development";

$default_oea["00308"]["ItemCode"] = "00308";
$default_oea["00308"]["ItemName"] = "Beijing-Hong Kong Children's Chinese Calligraphy and Painting Competition  北京香港少兒中國書法繪畫大賽";
$default_oea["00308"]["CatCode"] = "4";
$default_oea["00308"]["CatName"] = "Aesthetic Development";

$default_oea["00309"]["ItemCode"] = "00309";
$default_oea["00309"]["ItemName"] = "Beijing-Hong Kong Youth Children Painting Competition  京港青少年書畫比賽";
$default_oea["00309"]["CatCode"] = "4";
$default_oea["00309"]["CatName"] = "Aesthetic Development";

$default_oea["00306"]["ItemCode"] = "00306";
$default_oea["00306"]["ItemName"] = "Beijing, Shanghai, Hong Kong Inter-Cities School Real Time English Debating Competition";
$default_oea["00306"]["CatCode"] = "5";
$default_oea["00306"]["CatName"] = "Moral and Civic Education";

$default_oea["00310"]["ItemCode"] = "00310";
$default_oea["00310"]["ItemName"] = "Beograd International Children Fine Art Competition";
$default_oea["00310"]["CatCode"] = "4";
$default_oea["00310"]["CatName"] = "Aesthetic Development";

$default_oea["00311"]["ItemCode"] = "00311";
$default_oea["00311"]["ItemName"] = "Berlin International Film Festival  柏林國際影展";
$default_oea["00311"]["CatCode"] = "4";
$default_oea["00311"]["CatName"] = "Aesthetic Development";

$default_oea["00312"]["ItemCode"] = "00312";
$default_oea["00312"]["ItemName"] = "Best Cadet Award";
$default_oea["00312"]["CatCode"] = "2";
$default_oea["00312"]["CatName"] = "Career-related Experience";

$default_oea["00313"]["ItemCode"] = "00313";
$default_oea["00313"]["ItemName"] = "Best Choreography周年舞蹈比賽最佳編舞獎";
$default_oea["00313"]["CatCode"] = "4";
$default_oea["00313"]["CatName"] = "Aesthetic Development";

$default_oea["00314"]["ItemCode"] = "00314";
$default_oea["00314"]["ItemName"] = "Best Dancer周年舞蹈比最佳舞蹈員";
$default_oea["00314"]["CatCode"] = "4";
$default_oea["00314"]["CatName"] = "Aesthetic Development";

$default_oea["00315"]["ItemCode"] = "00315";
$default_oea["00315"]["ItemName"] = "Best Improved Student / Outstanding Student / Good Student 傑出學生選舉 (Please provide details in \"Description\" box.)";
$default_oea["00315"]["CatCode"] = "2";
$default_oea["00315"]["CatName"] = "Career-related Experience";

$default_oea["00316"]["ItemCode"] = "00316";
$default_oea["00316"]["ItemName"] = "Best Musicianship Award最佳音樂造詣獎";
$default_oea["00316"]["CatCode"] = "4";
$default_oea["00316"]["CatName"] = "Aesthetic Development";

$default_oea["00317"]["ItemCode"] = "00317";
$default_oea["00317"]["ItemName"] = "Best Performance in Art and Design美術與設計科最佳表現獎";
$default_oea["00317"]["CatCode"] = "4";
$default_oea["00317"]["CatName"] = "Aesthetic Development";

$default_oea["00318"]["ItemCode"] = "00318";
$default_oea["00318"]["ItemName"] = "Best Performance of Non-Cantonese Singing最佳非粵語歌曲演繹獎";
$default_oea["00318"]["CatCode"] = "4";
$default_oea["00318"]["CatName"] = "Aesthetic Development";

$default_oea["00319"]["ItemCode"] = "00319";
$default_oea["00319"]["ItemName"] = "Best Proposal Award on Green Action - Environmental Campaign Committee";
$default_oea["00319"]["CatCode"] = "1";
$default_oea["00319"]["CatName"] = "Community Service";

$default_oea["00320"]["ItemCode"] = "00320";
$default_oea["00320"]["ItemName"] = "Best Reader (Chinese ERS)";
$default_oea["00320"]["CatCode"] = "4";
$default_oea["00320"]["CatName"] = "Aesthetic Development";

$default_oea["00321"]["ItemCode"] = "00321";
$default_oea["00321"]["ItemName"] = "Best Reader (English ERS)";
$default_oea["00321"]["CatCode"] = "4";
$default_oea["00321"]["CatName"] = "Aesthetic Development";

$default_oea["00322"]["ItemCode"] = "00322";
$default_oea["00322"]["ItemName"] = "Best Service Project Competition最佳服務主題計劃比賽";
$default_oea["00322"]["CatCode"] = "4";
$default_oea["00322"]["CatName"] = "Aesthetic Development";

$default_oea["00323"]["ItemCode"] = "00323";
$default_oea["00323"]["ItemName"] = "Betawi Cup Water Polo Competition";
$default_oea["00323"]["CatCode"] = "3";
$default_oea["00323"]["CatName"] = "Physical Development";

$default_oea["00324"]["ItemCode"] = "00324";
$default_oea["00324"]["ItemName"] = "BFA AAA Asian Junior Championship  BFA亞洲青棒錦標賽";
$default_oea["00324"]["CatCode"] = "3";
$default_oea["00324"]["CatName"] = "Physical Development";

$default_oea["00325"]["ItemCode"] = "00325";
$default_oea["00325"]["ItemName"] = "Biliteracy and Trilingualism Composition and Speech Competition  全港中學兩文三語菁英大比拼";
$default_oea["00325"]["CatCode"] = "4";
$default_oea["00325"]["CatName"] = "Aesthetic Development";

$default_oea["00327"]["ItemCode"] = "00327";
$default_oea["00327"]["ItemName"] = "Boccia World Cup  草地滾球世界杯";
$default_oea["00327"]["CatCode"] = "3";
$default_oea["00327"]["CatName"] = "Physical Development";

$default_oea["00329"]["ItemCode"] = "00329";
$default_oea["00329"]["ItemName"] = "Bollywood Dance Show";
$default_oea["00329"]["CatCode"] = "4";
$default_oea["00329"]["CatName"] = "Aesthetic Development";

$default_oea["00330"]["ItemCode"] = "00330";
$default_oea["00330"]["ItemName"] = "Book Report Competition of Shanghai, Singapore, Macau and Hong Kong (Please provide details in \"Description\" box.)";
$default_oea["00330"]["CatCode"] = "4";
$default_oea["00330"]["CatName"] = "Aesthetic Development";

$default_oea["00331"]["ItemCode"] = "00331";
$default_oea["00331"]["ItemName"] = "Boy Scouts 男童軍";
$default_oea["00331"]["CatCode"] = "1";
$default_oea["00331"]["CatName"] = "Community Service";

$default_oea["00332"]["ItemCode"] = "00332";
$default_oea["00332"]["ItemName"] = "Boysen - PBC International Open Tenpin Bowling Championships  保爾臣-PBC國際保齡球公開賽";
$default_oea["00332"]["CatCode"] = "3";
$default_oea["00332"]["CatName"] = "Physical Development";

$default_oea["00326"]["ItemCode"] = "00326";
$default_oea["00326"]["ItemName"] = "BOC Hong Kong Sports Festival-Hong Kong and Shenzhen Young Chinese Chess Tournament  中銀香港體育節-中國象棋港深青少年對抗賽";
$default_oea["00326"]["CatCode"] = "4";
$default_oea["00326"]["CatName"] = "Aesthetic Development";

$default_oea["00333"]["ItemCode"] = "00333";
$default_oea["00333"]["ItemName"] = "British Council Real UK Competition";
$default_oea["00333"]["CatCode"] = "5";
$default_oea["00333"]["CatName"] = "Moral and Civic Education";

$default_oea["00334"]["ItemCode"] = "00334";
$default_oea["00334"]["ItemName"] = "British Junior Open Squash  英國青少年壁球公開賽";
$default_oea["00334"]["CatCode"] = "3";
$default_oea["00334"]["CatName"] = "Physical Development";

$default_oea["00335"]["ItemCode"] = "00335";
$default_oea["00335"]["ItemName"] = "BSI Monaco Music Masters";
$default_oea["00335"]["CatCode"] = "4";
$default_oea["00335"]["CatName"] = "Aesthetic Development";

$default_oea["00336"]["ItemCode"] = "00336";
$default_oea["00336"]["ItemName"] = "Budapest International Wheelchair Fencing Competition  匈牙利布達佩斯國際輪椅劍擊錦標賽";
$default_oea["00336"]["CatCode"] = "3";
$default_oea["00336"]["CatName"] = "Physical Development";

$default_oea["00337"]["ItemCode"] = "00337";
$default_oea["00337"]["ItemName"] = "Budapest Wheelchair Fencing World Cup  匈牙利布達佩斯輪椅劍擊世界盃";
$default_oea["00337"]["CatCode"] = "3";
$default_oea["00337"]["CatName"] = "Physical Development";

$default_oea["00338"]["ItemCode"] = "00338";
$default_oea["00338"]["ItemName"] = "Building Facilities Engineering-Taster Program 屋宇設施工程導引課程";
$default_oea["00338"]["CatCode"] = "2";
$default_oea["00338"]["CatName"] = "Career-related Experience";

$default_oea["00339"]["ItemCode"] = "00339";
$default_oea["00339"]["ItemName"] = "Cambridge Young Learners English FLE Flyers";
$default_oea["00339"]["CatCode"] = "4";
$default_oea["00339"]["CatName"] = "Aesthetic Development";

$default_oea["00340"]["ItemCode"] = "00340";
$default_oea["00340"]["ItemName"] = "Campfire of 10th Island Company, Girl Guides";
$default_oea["00340"]["CatCode"] = "4";
$default_oea["00340"]["CatName"] = "Aesthetic Development";

$default_oea["00342"]["ItemCode"] = "00342";
$default_oea["00342"]["ItemName"] = "Canadian Computing Competition (Hong Kong Contest)  (Please provide details in \"Description\" box.)";
$default_oea["00342"]["CatCode"] = "2";
$default_oea["00342"]["CatName"] = "Career-related Experience";

$default_oea["00341"]["ItemCode"] = "00341";
$default_oea["00341"]["ItemName"] = "Canadian Computing Competition (Please provide details in \"Description\" box.)";
$default_oea["00341"]["CatCode"] = "2";
$default_oea["00341"]["CatName"] = "Career-related Experience";

$default_oea["00343"]["ItemCode"] = "00343";
$default_oea["00343"]["ItemName"] = "Canadian Mathematics Competition  加拿大數學比賽";
$default_oea["00343"]["CatCode"] = "2";
$default_oea["00343"]["CatName"] = "Career-related Experience";

$default_oea["00344"]["ItemCode"] = "00344";
$default_oea["00344"]["ItemName"] = "Canadian National Junior Achievement Conference";
$default_oea["00344"]["CatCode"] = "2";
$default_oea["00344"]["CatName"] = "Career-related Experience";

$default_oea["00345"]["ItemCode"] = "00345";
$default_oea["00345"]["ItemName"] = "Canadian Open Mathematics Challenge";
$default_oea["00345"]["CatCode"] = "2";
$default_oea["00345"]["CatName"] = "Career-related Experience";

$default_oea["00346"]["ItemCode"] = "00346";
$default_oea["00346"]["ItemName"] = "Canadian Short Championships (Canada)  加拿大短道賽";
$default_oea["00346"]["CatCode"] = "3";
$default_oea["00346"]["CatName"] = "Physical Development";

$default_oea["00347"]["ItemCode"] = "00347";
$default_oea["00347"]["ItemName"] = "Canoe trip 獨木舟旅程";
$default_oea["00347"]["CatCode"] = "3";
$default_oea["00347"]["CatName"] = "Physical Development";

$default_oea["00348"]["ItemCode"] = "00348";
$default_oea["00348"]["ItemName"] = "Canton, Hong Kong and Macau Youth and Children Art, Calligraphy Contest  粵港澳少年兒童美術書法大賽";
$default_oea["00348"]["CatCode"] = "4";
$default_oea["00348"]["CatName"] = "Aesthetic Development";

$default_oea["00349"]["ItemCode"] = "00349";
$default_oea["00349"]["ItemName"] = "Card Design Competition of the Hong Kong Federation of Youth Groups Felix Wong Youth Improvement Award";
$default_oea["00349"]["CatCode"] = "4";
$default_oea["00349"]["CatName"] = "Aesthetic Development";

$default_oea["00350"]["ItemCode"] = "00350";
$default_oea["00350"]["ItemName"] = "Care your family Card Design Competition《家家愛護家家》行動之心意咭創作比賽";
$default_oea["00350"]["CatCode"] = "4";
$default_oea["00350"]["CatName"] = "Aesthetic Development";

$default_oea["00351"]["ItemCode"] = "00351";
$default_oea["00351"]["ItemName"] = "Career Related Activity - Talks / Seminars / Visits / Workshop / Progamme  (Please provide details in \"Description\" box.)";
$default_oea["00351"]["CatCode"] = "2";
$default_oea["00351"]["CatName"] = "Career-related Experience";

$default_oea["00352"]["ItemCode"] = "00352";
$default_oea["00352"]["ItemName"] = "Careers Prefect升學及就業輔導組聯絡員";
$default_oea["00352"]["CatCode"] = "2";
$default_oea["00352"]["CatName"] = "Career-related Experience";

$default_oea["00353"]["ItemCode"] = "00353";
$default_oea["00353"]["ItemName"] = "Caretakers of the Environment International and Global Environmental Youth Convention";
$default_oea["00353"]["CatCode"] = "2";
$default_oea["00353"]["CatName"] = "Career-related Experience";

$default_oea["00354"]["ItemCode"] = "00354";
$default_oea["00354"]["ItemName"] = "Caring the Disabled Comic Drawing Competition關注殘疾人權四格漫畫比賽";
$default_oea["00354"]["CatCode"] = "4";
$default_oea["00354"]["CatName"] = "Aesthetic Development";

$default_oea["00355"]["ItemCode"] = "00355";
$default_oea["00355"]["ItemName"] = "Carmel Infotainment Network";
$default_oea["00355"]["CatCode"] = "4";
$default_oea["00355"]["CatName"] = "Aesthetic Development";

$default_oea["00356"]["ItemCode"] = "00356";
$default_oea["00356"]["ItemName"] = "Carmelian Vision (Junior Form Leadership Training)";
$default_oea["00356"]["CatCode"] = "1";
$default_oea["00356"]["CatName"] = "Community Service";

$default_oea["00357"]["ItemCode"] = "00357";
$default_oea["00357"]["ItemName"] = "Castrol Southern Downs Championships (Australia)  Castrol 澳洲南區錦標賽";
$default_oea["00357"]["CatCode"] = "3";
$default_oea["00357"]["CatName"] = "Physical Development";

$default_oea["00359"]["ItemCode"] = "00359";
$default_oea["00359"]["ItemName"] = "Cathay Pacific I CAN FLY Programme  國泰航空飛躍理想計劃  (Please provide details in \"Description\" box.)";
$default_oea["00359"]["CatCode"] = "2";
$default_oea["00359"]["CatName"] = "Career-related Experience";

$default_oea["00360"]["ItemCode"] = "00360";
$default_oea["00360"]["ItemName"] = "Cathay Pacific International Chinese New Year Night Parade  國泰航空新春國際滙演之夜";
$default_oea["00360"]["CatCode"] = "5";
$default_oea["00360"]["CatName"] = "Moral and Civic Education";

$default_oea["00361"]["ItemCode"] = "00361";
$default_oea["00361"]["ItemName"] = "Cathay Pacific International Wilderness Experience  國泰航空非洲野外體驗計劃";
$default_oea["00361"]["CatCode"] = "5";
$default_oea["00361"]["CatName"] = "Moral and Civic Education";

$default_oea["00362"]["ItemCode"] = "00362";
$default_oea["00362"]["ItemName"] = "Cathy Pacific Hong Kong Squash Open  國泰航空香港壁球公開賽";
$default_oea["00362"]["CatCode"] = "3";
$default_oea["00362"]["CatName"] = "Physical Development";

$default_oea["00363"]["ItemCode"] = "00363";
$default_oea["00363"]["ItemName"] = "Cayley Mathematics Contest (University of Waterloo)";
$default_oea["00363"]["CatCode"] = "2";
$default_oea["00363"]["CatName"] = "Career-related Experience";

$default_oea["00364"]["ItemCode"] = "00364";
$default_oea["00364"]["ItemName"] = "Celebrate the Sea Festival - Children's Art Competition";
$default_oea["00364"]["CatCode"] = "4";
$default_oea["00364"]["CatName"] = "Aesthetic Development";

$default_oea["00365"]["ItemCode"] = "00365";
$default_oea["00365"]["ItemName"] = "Celebration for the 60th Anniversary of the People Republic of China(Exhibition) 建國六十年展覽";
$default_oea["00365"]["CatCode"] = "5";
$default_oea["00365"]["CatName"] = "Moral and Civic Education";

$default_oea["00366"]["ItemCode"] = "00366";
$default_oea["00366"]["ItemName"] = "Celebration for the 60th Anniversary of the People Republic of China(Learning Award Scheme) 建國六十年專題研習獎勵計劃";
$default_oea["00366"]["CatCode"] = "5";
$default_oea["00366"]["CatName"] = "Moral and Civic Education";

$default_oea["00367"]["ItemCode"] = "00367";
$default_oea["00367"]["ItemName"] = "Central and Western District Children Dance Group Annual Dance";
$default_oea["00367"]["CatCode"] = "4";
$default_oea["00367"]["CatName"] = "Aesthetic Development";

$default_oea["00368"]["ItemCode"] = "00368";
$default_oea["00368"]["ItemName"] = "Certiport Worldwide Competition on Microsoft Office (Word 2003) / (Excel 2003)";
$default_oea["00368"]["CatCode"] = "2";
$default_oea["00368"]["CatName"] = "Career-related Experience";

$default_oea["00369"]["ItemCode"] = "00369";
$default_oea["00369"]["ItemName"] = "CGU Asian Bowling Tour  CGU亞洲保齡球巡迴賽";
$default_oea["00369"]["CatCode"] = "3";
$default_oea["00369"]["CatName"] = "Physical Development";

$default_oea["00370"]["ItemCode"] = "00370";
$default_oea["00370"]["ItemName"] = "Chairperson of joint school activities";
$default_oea["00370"]["CatCode"] = "1";
$default_oea["00370"]["CatName"] = "Community Service";

$default_oea["00371"]["ItemCode"] = "00371";
$default_oea["00371"]["ItemName"] = "Chaiyi Band Festival in Taiwan嘉義管樂節";
$default_oea["00371"]["CatCode"] = "4";
$default_oea["00371"]["CatName"] = "Aesthetic Development";

$default_oea["00372"]["ItemCode"] = "00372";
$default_oea["00372"]["ItemName"] = "Challenger Award  挑戰獎章";
$default_oea["00372"]["CatCode"] = "1";
$default_oea["00372"]["CatName"] = "Community Service";

$default_oea["00373"]["ItemCode"] = "00373";
$default_oea["00373"]["ItemName"] = "Changping-HK Interflow Friendly Competition  常平巿手球交流友誼賽";
$default_oea["00373"]["CatCode"] = "3";
$default_oea["00373"]["CatName"] = "Physical Development";

$default_oea["00374"]["ItemCode"] = "00374";
$default_oea["00374"]["ItemName"] = "Cheerleading Asia International Open Championship";
$default_oea["00374"]["CatCode"] = "3";
$default_oea["00374"]["CatName"] = "Physical Development";

$default_oea["00375"]["ItemCode"] = "00375";
$default_oea["00375"]["ItemName"] = "Chester Competitive Festival of Performing Arts  徹斯特表演藝術節";
$default_oea["00375"]["CatCode"] = "4";
$default_oea["00375"]["CatName"] = "Aesthetic Development";

$default_oea["00376"]["ItemCode"] = "00376";
$default_oea["00376"]["ItemName"] = "Chiang Mai Open Championship (Softball)";
$default_oea["00376"]["CatCode"] = "3";
$default_oea["00376"]["CatName"] = "Physical Development";

$default_oea["00377"]["ItemCode"] = "00377";
$default_oea["00377"]["ItemName"] = "Chief School Ambassador";
$default_oea["00377"]["CatCode"] = "1";
$default_oea["00377"]["CatName"] = "Community Service";

$default_oea["00378"]["ItemCode"] = "00378";
$default_oea["00378"]["ItemName"] = "Chief Scout's Award  總領袖奬章";
$default_oea["00378"]["CatCode"] = "1";
$default_oea["00378"]["CatName"] = "Community Service";

$default_oea["00379"]["ItemCode"] = "00379";
$default_oea["00379"]["ItemName"] = "Child Development and Care-Taster Program 幼兒成長教育導引課程";
$default_oea["00379"]["CatCode"] = "2";
$default_oea["00379"]["CatName"] = "Career-related Experience";

$default_oea["00380"]["ItemCode"] = "00380";
$default_oea["00380"]["ItemName"] = "Child Prodigy Cup National Student Painting and Calligraphy Exhibition Game Award  神童杯全國學生書畫大展賽";
$default_oea["00380"]["CatCode"] = "4";
$default_oea["00380"]["CatName"] = "Aesthetic Development";

$default_oea["00381"]["ItemCode"] = "00381";
$default_oea["00381"]["ItemName"] = "Children and Youth Reading Award Scheme - Leisure and Cultural Services Department  兒童及青少年閱讀計劃";
$default_oea["00381"]["CatCode"] = "4";
$default_oea["00381"]["CatName"] = "Aesthetic Development";

$default_oea["00382"]["ItemCode"] = "00382";
$default_oea["00382"]["ItemName"] = "Children Art Festival in Asia Diary Drawing Competition";
$default_oea["00382"]["CatCode"] = "4";
$default_oea["00382"]["CatName"] = "Aesthetic Development";

$default_oea["00383"]["ItemCode"] = "00383";
$default_oea["00383"]["ItemName"] = "Children Calligraphy Competition  我愛祖國－中華少年兒童藝術作品精選大型畫冊編選賽";
$default_oea["00383"]["CatCode"] = "4";
$default_oea["00383"]["CatName"] = "Aesthetic Development";

$default_oea["00384"]["ItemCode"] = "00384";
$default_oea["00384"]["ItemName"] = "Children of the World illustrate the Bible Drawing Competition";
$default_oea["00384"]["CatCode"] = "4";
$default_oea["00384"]["CatName"] = "Aesthetic Development";

$default_oea["00385"]["ItemCode"] = "00385";
$default_oea["00385"]["ItemName"] = "Children of the World in Harmony - International Children Choral and Performing Arts Festival Hong Kong  香港國際兒童合唱及演藝節";
$default_oea["00385"]["CatCode"] = "4";
$default_oea["00385"]["CatName"] = "Aesthetic Development";

$default_oea["00386"]["ItemCode"] = "00386";
$default_oea["00386"]["ItemName"] = "Children's Performing Arts Festival in East Asia  東亞地區兒童藝術節";
$default_oea["00386"]["CatCode"] = "4";
$default_oea["00386"]["CatName"] = "Aesthetic Development";

$default_oea["00387"]["ItemCode"] = "00387";
$default_oea["00387"]["ItemName"] = "Chimes Team";
$default_oea["00387"]["CatCode"] = "4";
$default_oea["00387"]["CatName"] = "Aesthetic Development";

$default_oea["00388"]["ItemCode"] = "00388";
$default_oea["00388"]["ItemName"] = "China - ASEAN Youth Dance Interaction and Competition  中國 - 東盟青少年舞蹈交流展演";
$default_oea["00388"]["CatCode"] = "4";
$default_oea["00388"]["CatName"] = "Aesthetic Development";

$default_oea["00389"]["ItemCode"] = "00389";
$default_oea["00389"]["ItemName"] = "China - Japan International Fine Art Competition  中國－日本國際書畫大賽";
$default_oea["00389"]["CatCode"] = "4";
$default_oea["00389"]["CatName"] = "Aesthetic Development";

$default_oea["00390"]["ItemCode"] = "00390";
$default_oea["00390"]["ItemName"] = "China (Fujian) International (Various Grades) Dance Competition  中國國際分級舞蹈大賽";
$default_oea["00390"]["CatCode"] = "4";
$default_oea["00390"]["CatName"] = "Aesthetic Development";

$default_oea["00391"]["ItemCode"] = "00391";
$default_oea["00391"]["ItemName"] = "China Adolescents Science and Technology Invention Contest  全國青少年科技創新大賽";
$default_oea["00391"]["CatCode"] = "2";
$default_oea["00391"]["CatName"] = "Career-related Experience";

$default_oea["00392"]["ItemCode"] = "00392";
$default_oea["00392"]["ItemName"] = "China AIDI Accordion Championship";
$default_oea["00392"]["CatCode"] = "4";
$default_oea["00392"]["CatName"] = "Aesthetic Development";

$default_oea["00393"]["ItemCode"] = "00393";
$default_oea["00393"]["ItemName"] = "China Amateur Radio Youth Champion Competition";
$default_oea["00393"]["CatCode"] = "4";
$default_oea["00393"]["CatName"] = "Aesthetic Development";

$default_oea["00394"]["ItemCode"] = "00394";
$default_oea["00394"]["ItemName"] = "China Art Festival in Tsing Yuen";
$default_oea["00394"]["CatCode"] = "4";
$default_oea["00394"]["CatName"] = "Aesthetic Development";

$default_oea["00395"]["ItemCode"] = "00395";
$default_oea["00395"]["ItemName"] = "China Association for Symphonic Bands and Ensembles  中國音協管樂學會";
$default_oea["00395"]["CatCode"] = "4";
$default_oea["00395"]["CatName"] = "Aesthetic Development";

$default_oea["00396"]["ItemCode"] = "00396";
$default_oea["00396"]["ItemName"] = "China Celebrities Calligraphy and Painting Exhibition cum National Outstanding Instructors and Youth and Children Joint Exhibition  中國名人書畫大展暨全國優秀指導教師及優秀青少兒幼聯展";
$default_oea["00396"]["CatCode"] = "4";
$default_oea["00396"]["CatName"] = "Aesthetic Development";

$default_oea["00397"]["ItemCode"] = "00397";
$default_oea["00397"]["ItemName"] = "China Children's Calligraphy and Painting Match";
$default_oea["00397"]["CatCode"] = "4";
$default_oea["00397"]["CatName"] = "Aesthetic Development";

$default_oea["00398"]["ItemCode"] = "00398";
$default_oea["00398"]["ItemName"] = "China City Test Games";
$default_oea["00398"]["CatCode"] = "3";
$default_oea["00398"]["CatName"] = "Physical Development";

$default_oea["00399"]["ItemCode"] = "00399";
$default_oea["00399"]["ItemName"] = "China Climbing Championship  全國運動攀登錦標賽";
$default_oea["00399"]["CatCode"] = "3";
$default_oea["00399"]["CatName"] = "Physical Development";

$default_oea["00400"]["ItemCode"] = "00400";
$default_oea["00400"]["ItemName"] = "China Cup Judo Championship 中華盃柔道邀請賽";
$default_oea["00400"]["CatCode"] = "3";
$default_oea["00400"]["CatName"] = "Physical Development";

$default_oea["00401"]["ItemCode"] = "00401";
$default_oea["00401"]["ItemName"] = "China Do-Win Cup Athletic Competition";
$default_oea["00401"]["CatCode"] = "3";
$default_oea["00401"]["CatName"] = "Physical Development";

$default_oea["00402"]["ItemCode"] = "00402";
$default_oea["00402"]["ItemName"] = "China Electrical Keyboard Competition";
$default_oea["00402"]["CatCode"] = "4";
$default_oea["00402"]["CatName"] = "Aesthetic Development";

$default_oea["00403"]["ItemCode"] = "00403";
$default_oea["00403"]["ItemName"] = "China High-Class Amateur Band Festival  中國非職業優秀管樂團隊展演";
$default_oea["00403"]["CatCode"] = "4";
$default_oea["00403"]["CatName"] = "Aesthetic Development";

$default_oea["00404"]["ItemCode"] = "00404";
$default_oea["00404"]["ItemName"] = "China Hong Kong Golden Bauhinia Youth & Children International Standard Dance Public Competition  中國香港金紫荊花獎青少年國際標准舞公開賽";
$default_oea["00404"]["CatCode"] = "3";
$default_oea["00404"]["CatName"] = "Physical Development";

$default_oea["00412"]["ItemCode"] = "00412";
$default_oea["00412"]["ItemName"] = "China is concerned about the next generation of physical and mental health project - National talent search  中國關心下一代身心健康工程全國義演活動";
$default_oea["00412"]["CatCode"] = "4";
$default_oea["00412"]["CatName"] = "Aesthetic Development";

$default_oea["00405"]["ItemCode"] = "00405";
$default_oea["00405"]["ItemName"] = "China International Chorus Festival  中國國際合唱節";
$default_oea["00405"]["CatCode"] = "4";
$default_oea["00405"]["CatName"] = "Aesthetic Development";

$default_oea["00406"]["ItemCode"] = "00406";
$default_oea["00406"]["ItemName"] = "China International Music Art Competition  中華國際音樂藝術大賽 - 國際校際藝術展示大賽";
$default_oea["00406"]["CatCode"] = "4";
$default_oea["00406"]["CatName"] = "Aesthetic Development";

$default_oea["00407"]["ItemCode"] = "00407";
$default_oea["00407"]["ItemName"] = "China International Open Bowling Championships  中國國際保齡球公開賽";
$default_oea["00407"]["CatCode"] = "3";
$default_oea["00407"]["CatName"] = "Physical Development";

$default_oea["00408"]["ItemCode"] = "00408";
$default_oea["00408"]["ItemName"] = "China International Orienteering Competition  中國重慶國際山地越野挑戰賽";
$default_oea["00408"]["CatCode"] = "3";
$default_oea["00408"]["CatName"] = "Physical Development";

$default_oea["00409"]["ItemCode"] = "00409";
$default_oea["00409"]["ItemName"] = "China International Photo Competition  國際新聞攝影比賽";
$default_oea["00409"]["CatCode"] = "4";
$default_oea["00409"]["CatName"] = "Aesthetic Development";

$default_oea["00410"]["ItemCode"] = "00410";
$default_oea["00410"]["ItemName"] = "China International Piano Competition";
$default_oea["00410"]["CatCode"] = "4";
$default_oea["00410"]["CatName"] = "Aesthetic Development";

$default_oea["00411"]["ItemCode"] = "00411";
$default_oea["00411"]["ItemName"] = "China International Student Fashion Design Competition  中國國際服裝設計名校學生作品大賽";
$default_oea["00411"]["CatCode"] = "4";
$default_oea["00411"]["CatName"] = "Aesthetic Development";

$default_oea["00413"]["ItemCode"] = "00413";
$default_oea["00413"]["ItemName"] = "China Junior Taekwondo Championships";
$default_oea["00413"]["CatCode"] = "3";
$default_oea["00413"]["CatName"] = "Physical Development";

$default_oea["00414"]["ItemCode"] = "00414";
$default_oea["00414"]["ItemName"] = "China Junior Tennis Tour Grand Masters (Fai On Cup)  輝安盃全國青少年網球排名錦標賽";
$default_oea["00414"]["CatCode"] = "3";
$default_oea["00414"]["CatName"] = "Physical Development";

$default_oea["00415"]["ItemCode"] = "00415";
$default_oea["00415"]["ItemName"] = "China Kunming Spring City Cup National Youth's Music Art Invitational Competition  中國昆明春城杯全國青少年音樂藝術邀請賽";
$default_oea["00415"]["CatCode"] = "3";
$default_oea["00415"]["CatName"] = "Physical Development";

$default_oea["00416"]["ItemCode"] = "00416";
$default_oea["00416"]["ItemName"] = "China Macau Inter-cities Robot Olympic  中國澳門機械奧運會埠際賽";
$default_oea["00416"]["CatCode"] = "2";
$default_oea["00416"]["CatName"] = "Career-related Experience";

$default_oea["00417"]["ItemCode"] = "00417";
$default_oea["00417"]["ItemName"] = "China Minority Dance Federation";
$default_oea["00417"]["CatCode"] = "4";
$default_oea["00417"]["CatName"] = "Aesthetic Development";

$default_oea["00418"]["ItemCode"] = "00418";
$default_oea["00418"]["ItemName"] = "China Modern Juvenile Art Exhibition";
$default_oea["00418"]["CatCode"] = "4";
$default_oea["00418"]["CatName"] = "Aesthetic Development";

$default_oea["00419"]["ItemCode"] = "00419";
$default_oea["00419"]["ItemName"] = "China National Junior Figure Skating Championships  全國少年花樣滑冰賽";
$default_oea["00419"]["CatCode"] = "3";
$default_oea["00419"]["CatName"] = "Physical Development";

$default_oea["00420"]["ItemCode"] = "00420";
$default_oea["00420"]["ItemName"] = "China National Orienteering Championship  中國國家野外定向錦標賽";
$default_oea["00420"]["CatCode"] = "3";
$default_oea["00420"]["CatName"] = "Physical Development";

$default_oea["00421"]["ItemCode"] = "00421";
$default_oea["00421"]["ItemName"] = "China National Road Race Championship";
$default_oea["00421"]["CatCode"] = "3";
$default_oea["00421"]["CatName"] = "Physical Development";

$default_oea["00422"]["ItemCode"] = "00422";
$default_oea["00422"]["ItemName"] = "China National Roller Skating Championships";
$default_oea["00422"]["CatCode"] = "3";
$default_oea["00422"]["CatName"] = "Physical Development";

$default_oea["00423"]["ItemCode"] = "00423";
$default_oea["00423"]["ItemName"] = "China National Youth Windsurfing Championship  全國青少年帆板錦標賽";
$default_oea["00423"]["CatCode"] = "3";
$default_oea["00423"]["CatName"] = "Physical Development";

$default_oea["00424"]["ItemCode"] = "00424";
$default_oea["00424"]["ItemName"] = "China Optimist Sailing Championship";
$default_oea["00424"]["CatCode"] = "3";
$default_oea["00424"]["CatName"] = "Physical Development";

$default_oea["00425"]["ItemCode"] = "00425";
$default_oea["00425"]["ItemName"] = "China Roller Skating Invitation Championships";
$default_oea["00425"]["CatCode"] = "3";
$default_oea["00425"]["CatName"] = "Physical Development";

$default_oea["00426"]["ItemCode"] = "00426";
$default_oea["00426"]["ItemName"] = "China Roller Speed Skating Competition  廣州巿元旦穗港輪滑俱樂部友誼賽";
$default_oea["00426"]["CatCode"] = "3";
$default_oea["00426"]["CatName"] = "Physical Development";

$default_oea["00427"]["ItemCode"] = "00427";
$default_oea["00427"]["ItemName"] = "China School Sports Competition  中國中學生體育競賽 (Please provide details in \"Description\" box.)";
$default_oea["00427"]["CatCode"] = "3";
$default_oea["00427"]["CatName"] = "Physical Development";

$default_oea["00428"]["ItemCode"] = "00428";
$default_oea["00428"]["ItemName"] = "China School Sports Competition  中國中學生體育競賽－全國中學生優勝者杯排球賽 (Please provide details in \"Description\" box.)";
$default_oea["00428"]["CatCode"] = "3";
$default_oea["00428"]["CatName"] = "Physical Development";

$default_oea["00429"]["ItemCode"] = "00429";
$default_oea["00429"]["ItemName"] = "China Secondary School Ping Pong Championships  全國中學生希望杯乒乓球比賽";
$default_oea["00429"]["CatCode"] = "3";
$default_oea["00429"]["CatName"] = "Physical Development";

$default_oea["00430"]["ItemCode"] = "00430";
$default_oea["00430"]["ItemName"] = "China Shanghai Ocean Exploration Competition (Exploration of the Sea) by HK Robotic Olympic Association";
$default_oea["00430"]["CatCode"] = "2";
$default_oea["00430"]["CatName"] = "Career-related Experience";

$default_oea["00431"]["ItemCode"] = "00431";
$default_oea["00431"]["ItemName"] = "China Soong Ching Ling Foundation China Electric Piano Invitational Competition  中國宋慶齡基金會全國少年兒童電子琴大賽";
$default_oea["00431"]["CatCode"] = "4";
$default_oea["00431"]["CatName"] = "Aesthetic Development";

$default_oea["00432"]["ItemCode"] = "00432";
$default_oea["00432"]["ItemName"] = "China Tianjin International Children's Art Exhibition Activity  中國天津國際少年兒童繪畫作品交流展示活動";
$default_oea["00432"]["CatCode"] = "4";
$default_oea["00432"]["CatName"] = "Aesthetic Development";

$default_oea["00433"]["ItemCode"] = "00433";
$default_oea["00433"]["ItemName"] = "China Wong Fei Hung World Lion Dance Championship";
$default_oea["00433"]["CatCode"] = "3";
$default_oea["00433"]["CatName"] = "Physical Development";

$default_oea["00434"]["ItemCode"] = "00434";
$default_oea["00434"]["ItemName"] = "China Youngsters Art Album (I love China)";
$default_oea["00434"]["CatCode"] = "4";
$default_oea["00434"]["CatName"] = "Aesthetic Development";

$default_oea["00435"]["ItemCode"] = "00435";
$default_oea["00435"]["ItemName"] = "China Youth and Children Arts, Photography and Calligraphy Competition";
$default_oea["00435"]["CatCode"] = "4";
$default_oea["00435"]["CatName"] = "Aesthetic Development";

$default_oea["00436"]["ItemCode"] = "00436";
$default_oea["00436"]["ItemName"] = "China Youth Calligraphy and Painting Competition Dawn Cup";
$default_oea["00436"]["CatCode"] = "4";
$default_oea["00436"]["CatName"] = "Aesthetic Development";

$default_oea["00437"]["ItemCode"] = "00437";
$default_oea["00437"]["ItemName"] = "China Youth Robotics Competition  中國青少年機器人競賽";
$default_oea["00437"]["CatCode"] = "2";
$default_oea["00437"]["CatName"] = "Career-related Experience";

$default_oea["00438"]["ItemCode"] = "00438";
$default_oea["00438"]["ItemName"] = "China Youth Technology Creative Competition";
$default_oea["00438"]["CatCode"] = "2";
$default_oea["00438"]["CatName"] = "Career-related Experience";

$default_oea["00440"]["ItemCode"] = "00440";
$default_oea["00440"]["ItemName"] = "China-ASEAN Basketball Invitational Tournament  廣西東盟國際籃球錦標賽";
$default_oea["00440"]["CatCode"] = "3";
$default_oea["00440"]["CatName"] = "Physical Development";

$default_oea["00441"]["ItemCode"] = "00441";
$default_oea["00441"]["ItemName"] = "China-Hong Kong Traditional Martial Art Place A";
$default_oea["00441"]["CatCode"] = "3";
$default_oea["00441"]["CatName"] = "Physical Development";

$default_oea["00442"]["ItemCode"] = "00442";
$default_oea["00442"]["ItemName"] = "China-Hong Kong-Macau Youth and Children Arts, Photography and Calligraphy Competition";
$default_oea["00442"]["CatCode"] = "4";
$default_oea["00442"]["CatName"] = "Aesthetic Development";

$default_oea["00443"]["ItemCode"] = "00443";
$default_oea["00443"]["ItemName"] = "China-Japan International Art Exhibition / Competition (Please provide details in \"Description\" box.) ";
$default_oea["00443"]["CatCode"] = "4";
$default_oea["00443"]["CatName"] = "Aesthetic Development";

$default_oea["00444"]["ItemCode"] = "00444";
$default_oea["00444"]["ItemName"] = "China-Japan-Korea Children's Exhibition Match (Piano)";
$default_oea["00444"]["CatCode"] = "4";
$default_oea["00444"]["CatName"] = "Aesthetic Development";

$default_oea["00439"]["ItemCode"] = "00439";
$default_oea["00439"]["ItemName"] = "China, Hong Kong, Macau and Taiwan National Youth and Children's Art Competition  中、港、澳、台全國少年兒童美術大賽";
$default_oea["00439"]["CatCode"] = "4";
$default_oea["00439"]["CatName"] = "Aesthetic Development";

$default_oea["00445"]["ItemCode"] = "00445";
$default_oea["00445"]["ItemName"] = "China's International Children's Art Exhibition  中國國際少年兒童藝術展";
$default_oea["00445"]["CatCode"] = "4";
$default_oea["00445"]["CatName"] = "Aesthetic Development";

$default_oea["00446"]["ItemCode"] = "00446";
$default_oea["00446"]["ItemName"] = "China's National Acrobatic Gymnastics Championship  全國青少年技巧錦標賽";
$default_oea["00446"]["CatCode"] = "3";
$default_oea["00446"]["CatName"] = "Physical Development";

$default_oea["00447"]["ItemCode"] = "00447";
$default_oea["00447"]["ItemName"] = "Chinese Amateur Swimming Association Interport Swimming Competition  中華業餘游泳總會埠際游泳比賽";
$default_oea["00447"]["CatCode"] = "3";
$default_oea["00447"]["CatName"] = "Physical Development";

$default_oea["00448"]["ItemCode"] = "00448";
$default_oea["00448"]["ItemName"] = "Chinese Children and Juvenile Art Exhibition For Brush and Ink on Paper Competition  海峽兩岸少年兒童水墨畫大賽";
$default_oea["00448"]["CatCode"] = "4";
$default_oea["00448"]["CatName"] = "Aesthetic Development";

$default_oea["00449"]["ItemCode"] = "00449";
$default_oea["00449"]["ItemName"] = "Chinese Classical Poetry Recitation Competition";
$default_oea["00449"]["CatCode"] = "4";
$default_oea["00449"]["CatName"] = "Aesthetic Development";

$default_oea["00450"]["ItemCode"] = "00450";
$default_oea["00450"]["ItemName"] = "Chinese Commonweal Angelet Contest";
$default_oea["00450"]["CatCode"] = "4";
$default_oea["00450"]["CatName"] = "Aesthetic Development";

$default_oea["00451"]["ItemCode"] = "00451";
$default_oea["00451"]["ItemName"] = "Chinese Culture Cup";
$default_oea["00451"]["CatCode"] = "5";
$default_oea["00451"]["CatName"] = "Moral and Civic Education";

$default_oea["00452"]["ItemCode"] = "00452";
$default_oea["00452"]["ItemName"] = "Chinese Culture Showcase (Hong Kong) Music Competition";
$default_oea["00452"]["CatCode"] = "4";
$default_oea["00452"]["CatName"] = "Aesthetic Development";

$default_oea["00453"]["ItemCode"] = "00453";
$default_oea["00453"]["ItemName"] = "Chinese Drum Festival";
$default_oea["00453"]["CatCode"] = "4";
$default_oea["00453"]["CatName"] = "Aesthetic Development";

$default_oea["00454"]["ItemCode"] = "00454";
$default_oea["00454"]["ItemName"] = "Chinese families, Women, Children Painting Exhibition  中國家庭‧婦女‧兒童書畫評展活動";
$default_oea["00454"]["CatCode"] = "4";
$default_oea["00454"]["CatName"] = "Aesthetic Development";

$default_oea["00455"]["ItemCode"] = "00455";
$default_oea["00455"]["ItemName"] = "Chinese Instrument Competition by Beijing Music Association";
$default_oea["00455"]["CatCode"] = "4";
$default_oea["00455"]["CatName"] = "Aesthetic Development";

$default_oea["00456"]["ItemCode"] = "00456";
$default_oea["00456"]["ItemName"] = "Chinese International Dance Competition 中國舞蹈比賽 (Please provide details in \"Description\" box.)";
$default_oea["00456"]["CatCode"] = "4";
$default_oea["00456"]["CatName"] = "Aesthetic Development";

$default_oea["00457"]["ItemCode"] = "00457";
$default_oea["00457"]["ItemName"] = "Chinese Junior Dance Competition  中國少年兒童舞蹈比賽 (Please provide details in \"Description\" box.)";
$default_oea["00457"]["CatCode"] = "4";
$default_oea["00457"]["CatName"] = "Aesthetic Development";

$default_oea["00458"]["ItemCode"] = "00458";
$default_oea["00458"]["ItemName"] = "Chinese Landscape Painting from the Xubaizhai臥遊山水 - 虛白齋藏畫選";
$default_oea["00458"]["CatCode"] = "4";
$default_oea["00458"]["CatName"] = "Aesthetic Development";

$default_oea["00459"]["ItemCode"] = "00459";
$default_oea["00459"]["ItemName"] = "Chinese Motor Cup (Taiwan) International Invitation Gymnastics Competition";
$default_oea["00459"]["CatCode"] = "3";
$default_oea["00459"]["CatName"] = "Physical Development";

$default_oea["00460"]["ItemCode"] = "00460";
$default_oea["00460"]["ItemName"] = "Chinese Music Appreciation Magic Chinese Music  ";
$default_oea["00460"]["CatCode"] = "4";
$default_oea["00460"]["CatName"] = "Aesthetic Development";

$default_oea["00461"]["ItemCode"] = "00461";
$default_oea["00461"]["ItemName"] = "Chinese Music International Competition  中國音樂國際比賽 (Please provide details in \"Description\" box.)";
$default_oea["00461"]["CatCode"] = "4";
$default_oea["00461"]["CatName"] = "Aesthetic Development";

$default_oea["00462"]["ItemCode"] = "00462";
$default_oea["00462"]["ItemName"] = "Chinese National Competition for Children and Youth";
$default_oea["00462"]["CatCode"] = "5";
$default_oea["00462"]["CatName"] = "Moral and Civic Education";

$default_oea["00463"]["ItemCode"] = "00463";
$default_oea["00463"]["ItemName"] = "Chinese Orchestra Competition中樂比賽 (Please provide details in \"Description\" box.)";
$default_oea["00463"]["CatCode"] = "4";
$default_oea["00463"]["CatName"] = "Aesthetic Development";

$default_oea["00464"]["ItemCode"] = "00464";
$default_oea["00464"]["ItemName"] = "Chinese Painting Competition for Teenagers 少年國畫比賽 (Please provide details in \"Description\" box.)";
$default_oea["00464"]["CatCode"] = "4";
$default_oea["00464"]["CatName"] = "Aesthetic Development";

$default_oea["00465"]["ItemCode"] = "00465";
$default_oea["00465"]["ItemName"] = "Chinese Painting Course";
$default_oea["00465"]["CatCode"] = "4";
$default_oea["00465"]["CatName"] = "Aesthetic Development";

$default_oea["00466"]["ItemCode"] = "00466";
$default_oea["00466"]["ItemName"] = "Chinese Public cum Angel Named International Children's Art Eurasian National Finals Open  藍天杯中國公益小天使評選活動暨歐亞國際少兒藝術公開選拔賽";
$default_oea["00466"]["CatCode"] = "4";
$default_oea["00466"]["CatName"] = "Aesthetic Development";

$default_oea["00467"]["ItemCode"] = "00467";
$default_oea["00467"]["ItemName"] = "Chinese Shun Tak Trophy Campus Literature Open Competition  中華\"信德杯\"校園文學大獎賽";
$default_oea["00467"]["CatCode"] = "4";
$default_oea["00467"]["CatName"] = "Aesthetic Development";

$default_oea["00468"]["ItemCode"] = "00468";
$default_oea["00468"]["ItemName"] = "Chinese Taipei International Athletic Meet  台灣國際田徑邀請賽";
$default_oea["00468"]["CatCode"] = "3";
$default_oea["00468"]["CatName"] = "Physical Development";

$default_oea["00469"]["ItemCode"] = "00469";
$default_oea["00469"]["ItemName"] = "Chinese Universities-Shakespeare Festival";
$default_oea["00469"]["CatCode"] = "4";
$default_oea["00469"]["CatName"] = "Aesthetic Development";

$default_oea["00471"]["ItemCode"] = "00471";
$default_oea["00471"]["ItemName"] = "Chinese writing with Biology themes";
$default_oea["00471"]["CatCode"] = "4";
$default_oea["00471"]["CatName"] = "Aesthetic Development";

$default_oea["00470"]["ItemCode"] = "00470";
$default_oea["00470"]["ItemName"] = "Chinese White Dolphin Painting & Drawing Competition  中華白海豚填色繪畫比賽";
$default_oea["00470"]["CatCode"] = "4";
$default_oea["00470"]["CatName"] = "Aesthetic Development";

$default_oea["00472"]["ItemCode"] = "00472";
$default_oea["00472"]["ItemName"] = "Chinese Youth Arts Festival  中國青少年藝術節活動";
$default_oea["00472"]["CatCode"] = "4";
$default_oea["00472"]["CatName"] = "Aesthetic Development";

$default_oea["00473"]["ItemCode"] = "00473";
$default_oea["00473"]["ItemName"] = "Chinese Youth SiNuo Oral English Contest  全國青少年斯諾英語口語大賽";
$default_oea["00473"]["CatCode"] = "4";
$default_oea["00473"]["CatName"] = "Aesthetic Development";

$default_oea["00474"]["ItemCode"] = "00474";
$default_oea["00474"]["ItemName"] = "Chinese-Speaking region University Students' Film & Video Competition  華語區大學生影片及短片比賽";
$default_oea["00474"]["CatCode"] = "4";
$default_oea["00474"]["CatName"] = "Aesthetic Development";

$default_oea["00475"]["ItemCode"] = "00475";
$default_oea["00475"]["ItemName"] = "Ching Ching Scholarship";
$default_oea["00475"]["CatCode"] = "2";
$default_oea["00475"]["CatName"] = "Career-related Experience";

$default_oea["00476"]["ItemCode"] = "00476";
$default_oea["00476"]["ItemName"] = "Choir / Choir Olympics (Please provide details in \"Description\" box.) ";
$default_oea["00476"]["CatCode"] = "4";
$default_oea["00476"]["CatName"] = "Aesthetic Development";

$default_oea["00480"]["ItemCode"] = "00480";
$default_oea["00480"]["ItemName"] = "Citigroup Stock Challenge - Citigroup Hong Kong";
$default_oea["00480"]["CatCode"] = "2";
$default_oea["00480"]["CatName"] = "Career-related Experience";

$default_oea["00481"]["ItemCode"] = "00481";
$default_oea["00481"]["ItemName"] = "City Tracing";
$default_oea["00481"]["CatCode"] = "3";
$default_oea["00481"]["CatName"] = "Physical Development";

$default_oea["00482"]["ItemCode"] = "00482";
$default_oea["00482"]["ItemName"] = "CityU Virtual Character Design Competition城大全港中學生虛擬角色設計比賽";
$default_oea["00482"]["CatCode"] = "5";
$default_oea["00482"]["CatName"] = "Moral and Civic Education";

$default_oea["00483"]["ItemCode"] = "00483";
$default_oea["00483"]["ItemName"] = "Civic Ambassador / Education (Please provide details in \"Description\" box.)";
$default_oea["00483"]["CatCode"] = "5";
$default_oea["00483"]["CatName"] = "Moral and Civic Education";

$default_oea["00484"]["ItemCode"] = "00484";
$default_oea["00484"]["ItemName"] = "Civil Aid Service Cadet Corps香港民眾安全服務隊少年團";
$default_oea["00484"]["CatCode"] = "1";
$default_oea["00484"]["CatName"] = "Community Service";

$default_oea["00477"]["ItemCode"] = "00477";
$default_oea["00477"]["ItemName"] = "CILTHK Essay Competition  香港運輸物流學會中學生徵文比賽";
$default_oea["00477"]["CatCode"] = "4";
$default_oea["00477"]["CatName"] = "Aesthetic Development";

$default_oea["00478"]["ItemCode"] = "00478";
$default_oea["00478"]["ItemName"] = "CISM & Mid Europe International Youth Wind Orchestra Competition";
$default_oea["00478"]["CatCode"] = "4";
$default_oea["00478"]["CatName"] = "Aesthetic Development";

$default_oea["00479"]["ItemCode"] = "00479";
$default_oea["00479"]["ItemName"] = "CITI Youth Investment Education Programme  花旗集團財智星級爭霸戰";
$default_oea["00479"]["CatCode"] = "2";
$default_oea["00479"]["CatName"] = "Career-related Experience";

$default_oea["00485"]["ItemCode"] = "00485";
$default_oea["00485"]["ItemName"] = "CKC Cup CKC Chinese Input Method Contest in Changzhou, Jiangsu";
$default_oea["00485"]["CatCode"] = "2";
$default_oea["00485"]["CatName"] = "Career-related Experience";

$default_oea["00486"]["ItemCode"] = "00486";
$default_oea["00486"]["ItemName"] = "Class-based/ School-based Service (Please provide details in \"Description\" box.) ";
$default_oea["00486"]["CatCode"] = "1";
$default_oea["00486"]["CatName"] = "Community Service";

$default_oea["00487"]["ItemCode"] = "00487";
$default_oea["00487"]["ItemName"] = "Classical Chinese Poetry (Student Section) of the Chinese Poetry Writing Competition - The Hong Kong Youth Cultural and Arts Competitions";
$default_oea["00487"]["CatCode"] = "4";
$default_oea["00487"]["CatName"] = "Aesthetic Development";

$default_oea["00488"]["ItemCode"] = "00488";
$default_oea["00488"]["ItemName"] = "Clip-it Competition (Please provide details in \"Description\" box.)";
$default_oea["00488"]["CatCode"] = "4";
$default_oea["00488"]["CatName"] = "Aesthetic Development";

$default_oea["00489"]["ItemCode"] = "00489";
$default_oea["00489"]["ItemName"] = "CLP Young Power Program Project Competition";
$default_oea["00489"]["CatCode"] = "2";
$default_oea["00489"]["CatName"] = "Career-related Experience";

$default_oea["00490"]["ItemCode"] = "00490";
$default_oea["00490"]["ItemName"] = "CLSA Outward Bound Adventure Race";
$default_oea["00490"]["CatCode"] = "3";
$default_oea["00490"]["CatName"] = "Physical Development";

$default_oea["00491"]["ItemCode"] = "00491";
$default_oea["00491"]["ItemName"] = "CMA-Young Post Green Manufacturing Competition";
$default_oea["00491"]["CatCode"] = "2";
$default_oea["00491"]["CatName"] = "Career-related Experience";

$default_oea["00513"]["ItemCode"] = "00513";
$default_oea["00513"]["ItemName"] = "Co-organized Celebration of the Fiftieth Anniversary of the Founding and the Handover of Macao Painting and Calligraphy Exhibition  大禹杯迎澳門回歸暨建國五十周年書畫藝術大展賽";
$default_oea["00513"]["CatCode"] = "4";
$default_oea["00513"]["CatName"] = "Aesthetic Development";

$default_oea["00492"]["ItemCode"] = "00492";
$default_oea["00492"]["ItemName"] = "Collection of the Artworks from the Chinese Youths and Children Competition";
$default_oea["00492"]["CatCode"] = "4";
$default_oea["00492"]["CatName"] = "Aesthetic Development";

$default_oea["00493"]["ItemCode"] = "00493";
$default_oea["00493"]["ItemName"] = "College Editorial Board";
$default_oea["00493"]["CatCode"] = "4";
$default_oea["00493"]["CatName"] = "Aesthetic Development";

$default_oea["00494"]["ItemCode"] = "00494";
$default_oea["00494"]["ItemName"] = "Color Ink Painting and Calligraphy Works by Children and Adolescents Cup Television Network Zhanping Activities  墨彩杯全國少年兒童書畫作品電視網絡展";
$default_oea["00494"]["CatCode"] = "4";
$default_oea["00494"]["CatName"] = "Aesthetic Development";

$default_oea["00495"]["ItemCode"] = "00495";
$default_oea["00495"]["ItemName"] = "Colts Rugby International Tournament  國際青年欖球賽";
$default_oea["00495"]["CatCode"] = "3";
$default_oea["00495"]["CatName"] = "Physical Development";

$default_oea["00496"]["ItemCode"] = "00496";
$default_oea["00496"]["ItemName"] = "Commercial Comic Art-Taster Program 商業漫畫設計導引課程";
$default_oea["00496"]["CatCode"] = "4";
$default_oea["00496"]["CatName"] = "Aesthetic Development";

$default_oea["00497"]["ItemCode"] = "00497";
$default_oea["00497"]["ItemName"] = "Commonwealth Essay Competition (The Royal Commonwealth Society)";
$default_oea["00497"]["CatCode"] = "4";
$default_oea["00497"]["CatName"] = "Aesthetic Development";

$default_oea["00498"]["ItemCode"] = "00498";
$default_oea["00498"]["ItemName"] = "Community Harmony Ambassadors";
$default_oea["00498"]["CatCode"] = "1";
$default_oea["00498"]["CatName"] = "Community Service";

$default_oea["00499"]["ItemCode"] = "00499";
$default_oea["00499"]["ItemName"] = "Community Health Campaign Volunteer";
$default_oea["00499"]["CatCode"] = "1";
$default_oea["00499"]["CatName"] = "Community Service";

$default_oea["00500"]["ItemCode"] = "00500";
$default_oea["00500"]["ItemName"] = "Community Health Project - Chinese Medicine Course";
$default_oea["00500"]["CatCode"] = "1";
$default_oea["00500"]["CatName"] = "Community Service";

$default_oea["00501"]["ItemCode"] = "00501";
$default_oea["00501"]["ItemName"] = "Community Mural Painting Competition共建、關愛社區壁畫設計比賽";
$default_oea["00501"]["CatCode"] = "4";
$default_oea["00501"]["CatName"] = "Aesthetic Development";

$default_oea["00502"]["ItemCode"] = "00502";
$default_oea["00502"]["ItemName"] = "Community Youth Club公益少年團";
$default_oea["00502"]["CatCode"] = "1";
$default_oea["00502"]["CatName"] = "Community Service";

$default_oea["00503"]["ItemCode"] = "00503";
$default_oea["00503"]["ItemName"] = "Competition of RoboCup Junior China Event  RoboCup青少年世界杯中國賽區選拔賽";
$default_oea["00503"]["CatCode"] = "2";
$default_oea["00503"]["CatName"] = "Career-related Experience";

$default_oea["00504"]["ItemCode"] = "00504";
$default_oea["00504"]["ItemName"] = "Competition on Story Writing for Students (Senior Secondary Section) - Hong Kong Public Libraries";
$default_oea["00504"]["CatCode"] = "4";
$default_oea["00504"]["CatName"] = "Aesthetic Development";

$default_oea["00505"]["ItemCode"] = "00505";
$default_oea["00505"]["ItemName"] = "Competition on System Modeling & Optimization  校際系統建模與優化競賽";
$default_oea["00505"]["CatCode"] = "2";
$default_oea["00505"]["CatName"] = "Career-related Experience";

$default_oea["00506"]["ItemCode"] = "00506";
$default_oea["00506"]["ItemName"] = "Composition of Proverbs on Life - Life Education Centre";
$default_oea["00506"]["CatCode"] = "4";
$default_oea["00506"]["CatName"] = "Aesthetic Development";

$default_oea["00507"]["ItemCode"] = "00507";
$default_oea["00507"]["ItemName"] = "Computer Training for Minority Children";
$default_oea["00507"]["CatCode"] = "1";
$default_oea["00507"]["CatName"] = "Community Service";

$default_oea["00508"]["ItemCode"] = "00508";
$default_oea["00508"]["ItemName"] = "Confidence Passbook Painting and Drawing Competition";
$default_oea["00508"]["CatCode"] = "4";
$default_oea["00508"]["CatName"] = "Aesthetic Development";

$default_oea["00509"]["ItemCode"] = "00509";
$default_oea["00509"]["ItemName"] = "Congressional Youth Leadership Council Global Young Leaders Conference - Youth Leadership Award";
$default_oea["00509"]["CatCode"] = "2";
$default_oea["00509"]["CatName"] = "Career-related Experience";

$default_oea["00510"]["ItemCode"] = "00510";
$default_oea["00510"]["ItemName"] = "Consumer Culture Study Award  消費文化考察報告獎";
$default_oea["00510"]["CatCode"] = "5";
$default_oea["00510"]["CatName"] = "Moral and Civic Education";

$default_oea["00511"]["ItemCode"] = "00511";
$default_oea["00511"]["ItemName"] = "Continuous service rendered to Social Services Organization (not less than 1 year) e.g. YMCA, Community Youth Centre, Youth Red Cross";
$default_oea["00511"]["CatCode"] = "1";
$default_oea["00511"]["CatName"] = "Community Service";

$default_oea["00512"]["ItemCode"] = "00512";
$default_oea["00512"]["ItemName"] = "Cool Forum";
$default_oea["00512"]["CatCode"] = "2";
$default_oea["00512"]["CatName"] = "Career-related Experience";

$default_oea["00514"]["ItemCode"] = "00514";
$default_oea["00514"]["ItemName"] = "Corel Creative Design Award";
$default_oea["00514"]["CatCode"] = "4";
$default_oea["00514"]["CatName"] = "Aesthetic Development";

$default_oea["00515"]["ItemCode"] = "00515";
$default_oea["00515"]["ItemName"] = "Corta Junior Open Classic (Columbus) Championship  青少年網球公開賽(哥倫布)";
$default_oea["00515"]["CatCode"] = "3";
$default_oea["00515"]["CatName"] = "Physical Development";

$default_oea["00516"]["ItemCode"] = "00516";
$default_oea["00516"]["ItemName"] = "Country Parks Nature in Touch Hiking and Planting Days";
$default_oea["00516"]["CatCode"] = "4";
$default_oea["00516"]["CatName"] = "Aesthetic Development";

$default_oea["00518"]["ItemCode"] = "00518";
$default_oea["00518"]["ItemName"] = "Creating Harmony Drawing Competition創造共融天繪畫比賽";
$default_oea["00518"]["CatCode"] = "4";
$default_oea["00518"]["CatName"] = "Aesthetic Development";

$default_oea["00519"]["ItemCode"] = "00519";
$default_oea["00519"]["ItemName"] = "Creative Eco-model Tournament 環保創意模型設計比賽";
$default_oea["00519"]["CatCode"] = "4";
$default_oea["00519"]["CatName"] = "Aesthetic Development";

$default_oea["00520"]["ItemCode"] = "00520";
$default_oea["00520"]["ItemName"] = "Creative Music Fusion";
$default_oea["00520"]["CatCode"] = "4";
$default_oea["00520"]["CatName"] = "Aesthetic Development";

$default_oea["00521"]["ItemCode"] = "00521";
$default_oea["00521"]["ItemName"] = "Creative Science Fiction writing workshop 創意科幻寫作工作坊";
$default_oea["00521"]["CatCode"] = "4";
$default_oea["00521"]["CatName"] = "Aesthetic Development";

$default_oea["00522"]["ItemCode"] = "00522";
$default_oea["00522"]["ItemName"] = "Creative Story Writing Competition (Junior / Senior Secondary Section) - The Hong Kong Youth Cultural and Arts Competitions";
$default_oea["00522"]["CatCode"] = "4";
$default_oea["00522"]["CatName"] = "Aesthetic Development";

$default_oea["00523"]["ItemCode"] = "00523";
$default_oea["00523"]["ItemName"] = "Creative Video Production Workshop";
$default_oea["00523"]["CatCode"] = "4";
$default_oea["00523"]["CatName"] = "Aesthetic Development";

$default_oea["00524"]["ItemCode"] = "00524";
$default_oea["00524"]["ItemName"] = "Creativity Acceleration through Science Learning Programme - Hong Kong Institute of Education";
$default_oea["00524"]["CatCode"] = "2";
$default_oea["00524"]["CatName"] = "Career-related Experience";

$default_oea["00525"]["ItemCode"] = "00525";
$default_oea["00525"]["ItemName"] = "Cross Country Championships";
$default_oea["00525"]["CatCode"] = "3";
$default_oea["00525"]["CatName"] = "Physical Development";

$default_oea["00526"]["ItemCode"] = "00526";
$default_oea["00526"]["ItemName"] = "Cross Country Race Competition  校際越野比賽";
$default_oea["00526"]["CatCode"] = "3";
$default_oea["00526"]["CatName"] = "Physical Development";

$default_oea["00527"]["ItemCode"] = "00527";
$default_oea["00527"]["ItemName"] = "Cross-Century Stars of Art National Youth and Children's Calligraphy and Painting Competition  跨世紀美術之星全國少兒書畫大賽";
$default_oea["00527"]["CatCode"] = "4";
$default_oea["00527"]["CatName"] = "Aesthetic Development";

$default_oea["00528"]["ItemCode"] = "00528";
$default_oea["00528"]["ItemName"] = "Cross-straits Youth Calligraphy and Painting Competition  海崍兩岸少兒書法繪畫大獎賽";
$default_oea["00528"]["CatCode"] = "4";
$default_oea["00528"]["CatName"] = "Aesthetic Development";

$default_oea["00529"]["ItemCode"] = "00529";
$default_oea["00529"]["ItemName"] = "Cross-word Puzzle Contest (Shamshuipo District) - Hong Kong Commerce, Industry & Professionals Association";
$default_oea["00529"]["CatCode"] = "4";
$default_oea["00529"]["CatName"] = "Aesthetic Development";

$default_oea["00517"]["ItemCode"] = "00517";
$default_oea["00517"]["ItemName"] = "CRE about Health Care Profession Workshop醫護專業的與工作有關的經驗工作坊";
$default_oea["00517"]["CatCode"] = "2";
$default_oea["00517"]["CatName"] = "Career-related Experience";

$default_oea["00530"]["ItemCode"] = "00530";
$default_oea["00530"]["ItemName"] = "Cultural and Art Courses  文化藝術 課程";
$default_oea["00530"]["CatCode"] = "4";
$default_oea["00530"]["CatName"] = "Aesthetic Development";

$default_oea["00531"]["ItemCode"] = "00531";
$default_oea["00531"]["ItemName"] = "Cultural Star of Wing Lung  永隆文學之星中國中學生作文大賽(香港賽區)";
$default_oea["00531"]["CatCode"] = "4";
$default_oea["00531"]["CatName"] = "Aesthetic Development";

$default_oea["00532"]["ItemCode"] = "00532";
$default_oea["00532"]["ItemName"] = "CYC Outstanding Members' Visit to Singapore and Community Youth Club";
$default_oea["00532"]["CatCode"] = "5";
$default_oea["00532"]["CatName"] = "Moral and Civic Education";

$default_oea["00533"]["ItemCode"] = "00533";
$default_oea["00533"]["ItemName"] = "Damascus International Junior Tournament ITF Grade  大馬士革國際ITF青少年大賽";
$default_oea["00533"]["CatCode"] = "3";
$default_oea["00533"]["CatName"] = "Physical Development";

$default_oea["00534"]["ItemCode"] = "00534";
$default_oea["00534"]["ItemName"] = "Dance & Care 2006 Chinese Dance Competition - The Arts Volunteer Association";
$default_oea["00534"]["CatCode"] = "4";
$default_oea["00534"]["CatName"] = "Aesthetic Development";

$default_oea["00535"]["ItemCode"] = "00535";
$default_oea["00535"]["ItemName"] = "Dance Exposition in China";
$default_oea["00535"]["CatCode"] = "4";
$default_oea["00535"]["CatName"] = "Aesthetic Development";

$default_oea["00536"]["ItemCode"] = "00536";
$default_oea["00536"]["ItemName"] = "DanceSport sa SUGBU and CebuIDSF World Dancesport Open, Latin & Standard";
$default_oea["00536"]["CatCode"] = "4";
$default_oea["00536"]["CatName"] = "Aesthetic Development";

$default_oea["00537"]["ItemCode"] = "00537";
$default_oea["00537"]["ItemName"] = "Dancing Lessons -Cultural Trip To Shanghai";
$default_oea["00537"]["CatCode"] = "4";
$default_oea["00537"]["CatName"] = "Aesthetic Development";

$default_oea["00538"]["ItemCode"] = "00538";
$default_oea["00538"]["ItemName"] = "Dancing to U.S.A. and Stockton Folk Dance Camp of University of Pacific";
$default_oea["00538"]["CatCode"] = "4";
$default_oea["00538"]["CatName"] = "Aesthetic Development";

$default_oea["00539"]["ItemCode"] = "00539";
$default_oea["00539"]["ItemName"] = "Darwin Tennis Championships  達爾文網球錦標賽";
$default_oea["00539"]["CatCode"] = "3";
$default_oea["00539"]["CatName"] = "Physical Development";

$default_oea["00540"]["ItemCode"] = "00540";
$default_oea["00540"]["ItemName"] = "DBS Social Enterprise Experience";
$default_oea["00540"]["CatCode"] = "2";
$default_oea["00540"]["CatName"] = "Career-related Experience";

$default_oea["00541"]["ItemCode"] = "00541";
$default_oea["00541"]["ItemName"] = "Dear China Photography, Art and Calligraphing Competition (by Dandelion Children Culture Center of Hong Kong)  祖國您好全國少兒美術攝影書法大展 (全國總選)";
$default_oea["00541"]["CatCode"] = "4";
$default_oea["00541"]["CatName"] = "Aesthetic Development";

$default_oea["00542"]["ItemCode"] = "00542";
$default_oea["00542"]["ItemName"] = "Debate Competition of Chinese Top Colleges  中國名校大專辯論賽";
$default_oea["00542"]["CatCode"] = "5";
$default_oea["00542"]["CatName"] = "Moral and Civic Education";

$default_oea["00544"]["ItemCode"] = "00544";
$default_oea["00544"]["ItemName"] = "Decoration of Community: Poster Design美化社區齊嚮應推廣計劃海報設計比賽";
$default_oea["00544"]["CatCode"] = "4";
$default_oea["00544"]["CatName"] = "Aesthetic Development";

$default_oea["00545"]["ItemCode"] = "00545";
$default_oea["00545"]["ItemName"] = "Desaru Triathlon  迪沙魯三項鐵人賽";
$default_oea["00545"]["CatCode"] = "3";
$default_oea["00545"]["CatName"] = "Physical Development";

$default_oea["00546"]["ItemCode"] = "00546";
$default_oea["00546"]["ItemName"] = "Devotional Service Award";
$default_oea["00546"]["CatCode"] = "1";
$default_oea["00546"]["CatName"] = "Community Service";

$default_oea["00543"]["ItemCode"] = "00543";
$default_oea["00543"]["ItemName"] = "DECA's International Career Development Conference";
$default_oea["00543"]["CatCode"] = "2";
$default_oea["00543"]["CatName"] = "Career-related Experience";

$default_oea["00547"]["ItemCode"] = "00547";
$default_oea["00547"]["ItemName"] = "Dialogue in the Dark - 黑暗中對話 (Please provide details in \"Description\" box.)";
$default_oea["00547"]["CatCode"] = "5";
$default_oea["00547"]["CatName"] = "Moral and Civic Education";

$default_oea["00548"]["ItemCode"] = "00548";
$default_oea["00548"]["ItemName"] = "Digital Media DIY 數碼媒體由我創 (Please provide details in \"Description\" box.)";
$default_oea["00548"]["CatCode"] = "4";
$default_oea["00548"]["CatName"] = "Aesthetic Development";

$default_oea["00549"]["ItemCode"] = "00549";
$default_oea["00549"]["ItemName"] = "Digital Starlab Experience  數碼模擬星空體驗";
$default_oea["00549"]["CatCode"] = "4";
$default_oea["00549"]["CatName"] = "Aesthetic Development";

$default_oea["00550"]["ItemCode"] = "00550";
$default_oea["00550"]["ItemName"] = "Disney Theme Park Adventures滅罪迪士尼探索之旅";
$default_oea["00550"]["CatCode"] = "1";
$default_oea["00550"]["CatName"] = "Community Service";

$default_oea["00551"]["ItemCode"] = "00551";
$default_oea["00551"]["ItemName"] = "Disney Y.E.S.迪士尼青少年奇妙學習系列";
$default_oea["00551"]["CatCode"] = "4";
$default_oea["00551"]["CatName"] = "Aesthetic Development";

$default_oea["00552"]["ItemCode"] = "00552";
$default_oea["00552"]["ItemName"] = "Disney Youth Program - Disney's World of Physics";
$default_oea["00552"]["CatCode"] = "2";
$default_oea["00552"]["CatName"] = "Career-related Experience";

$default_oea["00553"]["ItemCode"] = "00553";
$default_oea["00553"]["ItemName"] = "Display Board Competition街板設計比賽";
$default_oea["00553"]["CatCode"] = "4";
$default_oea["00553"]["CatName"] = "Aesthetic Development";

$default_oea["00554"]["ItemCode"] = "00554";
$default_oea["00554"]["ItemName"] = "Distinguished Member of Junior Police Cadet: Tsuen Wan Division最佳少年警訊會員獎(荃灣)";
$default_oea["00554"]["CatCode"] = "1";
$default_oea["00554"]["CatName"] = "Community Service";

$default_oea["00555"]["ItemCode"] = "00555";
$default_oea["00555"]["ItemName"] = "District Speech Competition";
$default_oea["00555"]["CatCode"] = "2";
$default_oea["00555"]["CatName"] = "Career-related Experience";

$default_oea["00557"]["ItemCode"] = "00557";
$default_oea["00557"]["ItemName"] = "DIY POP Card 顯心意創作比賽";
$default_oea["00557"]["CatCode"] = "4";
$default_oea["00557"]["CatName"] = "Aesthetic Development";

$default_oea["00556"]["ItemCode"] = "00556";
$default_oea["00556"]["ItemName"] = "DIY POP CARD Express Your Love Through Creativity Competition  DIY Pop Card顯心意創作比賽";
$default_oea["00556"]["CatCode"] = "4";
$default_oea["00556"]["CatName"] = "Aesthetic Development";

$default_oea["00558"]["ItemCode"] = "00558";
$default_oea["00558"]["ItemName"] = "DMDIY 210 Final Competition and Price A《數碼媒體由我創210》決賽及頒獎典禮";
$default_oea["00558"]["CatCode"] = "4";
$default_oea["00558"]["CatName"] = "Aesthetic Development";

$default_oea["00559"]["ItemCode"] = "00559";
$default_oea["00559"]["ItemName"] = "Dr. C. Y. Tam Scholarship for Science Student";
$default_oea["00559"]["CatCode"] = "2";
$default_oea["00559"]["CatName"] = "Career-related Experience";

$default_oea["00560"]["ItemCode"] = "00560";
$default_oea["00560"]["ItemName"] = "Dr. S. C. Leung Memorial Scholarship";
$default_oea["00560"]["CatCode"] = "2";
$default_oea["00560"]["CatName"] = "Career-related Experience";

$default_oea["00561"]["ItemCode"] = "00561";
$default_oea["00561"]["ItemName"] = "Dragon 100 - The Young Chinese Leaders Forum  龍匯100華人青年領袖論壇";
$default_oea["00561"]["CatCode"] = "2";
$default_oea["00561"]["CatName"] = "Career-related Experience";

$default_oea["00562"]["ItemCode"] = "00562";
$default_oea["00562"]["ItemName"] = "Dragon at the Great Wall Painting Competition";
$default_oea["00562"]["CatCode"] = "4";
$default_oea["00562"]["CatName"] = "Aesthetic Development";

$default_oea["00563"]["ItemCode"] = "00563";
$default_oea["00563"]["ItemName"] = "Dragon Boat Race Helper 龍舟競渡工作人員";
$default_oea["00563"]["CatCode"] = "1";
$default_oea["00563"]["CatName"] = "Community Service";

$default_oea["00564"]["ItemCode"] = "00564";
$default_oea["00564"]["ItemName"] = "Dragon Scout Award  榮譽童軍獎章";
$default_oea["00564"]["CatCode"] = "1";
$default_oea["00564"]["CatName"] = "Community Service";

$default_oea["00565"]["ItemCode"] = "00565";
$default_oea["00565"]["ItemName"] = "Drama (Prevention Against Teenage Gambling)";
$default_oea["00565"]["CatCode"] = "4";
$default_oea["00565"]["CatName"] = "Aesthetic Development";

$default_oea["00566"]["ItemCode"] = "00566";
$default_oea["00566"]["ItemName"] = "Drama appreciation";
$default_oea["00566"]["CatCode"] = "4";
$default_oea["00566"]["CatName"] = "Aesthetic Development";

$default_oea["00567"]["ItemCode"] = "00567";
$default_oea["00567"]["ItemName"] = "Drama Presentation";
$default_oea["00567"]["CatCode"] = "4";
$default_oea["00567"]["CatName"] = "Aesthetic Development";

$default_oea["00568"]["ItemCode"] = "00568";
$default_oea["00568"]["ItemName"] = "Drama Training Course";
$default_oea["00568"]["CatCode"] = "4";
$default_oea["00568"]["CatName"] = "Aesthetic Development";

$default_oea["00569"]["ItemCode"] = "00569";
$default_oea["00569"]["ItemName"] = "Dui lian workshop";
$default_oea["00569"]["CatCode"] = "4";
$default_oea["00569"]["CatName"] = "Aesthetic Development";

$default_oea["00570"]["ItemCode"] = "00570";
$default_oea["00570"]["ItemName"] = "Duke of Edinburgh's Award Scheme愛丁堡公爵獎勵計劃";
$default_oea["00570"]["CatCode"] = "1";
$default_oea["00570"]["CatName"] = "Community Service";

$default_oea["00571"]["ItemCode"] = "00571";
$default_oea["00571"]["ItemName"] = "DuPont Benedictus Awards Student Design Competition -  A High School for the Century  DuPont Benedictus世紀高校學生設計比大獎";
$default_oea["00571"]["CatCode"] = "4";
$default_oea["00571"]["CatName"] = "Aesthetic Development";

$default_oea["00573"]["ItemCode"] = "00573";
$default_oea["00573"]["ItemName"] = "Earth from Above-An Ariel Portrait of our Planet Photography";
$default_oea["00573"]["CatCode"] = "4";
$default_oea["00573"]["CatName"] = "Aesthetic Development";

$default_oea["00574"]["ItemCode"] = "00574";
$default_oea["00574"]["ItemName"] = "East Asia Grand Prix Hopes Table Tennis Championships  東亞兒童盃乒乓球錦標賽";
$default_oea["00574"]["CatCode"] = "3";
$default_oea["00574"]["CatName"] = "Physical Development";

$default_oea["00575"]["ItemCode"] = "00575";
$default_oea["00575"]["ItemName"] = "East Asia Hopes Championships";
$default_oea["00575"]["CatCode"] = "3";
$default_oea["00575"]["CatName"] = "Physical Development";

$default_oea["00576"]["ItemCode"] = "00576";
$default_oea["00576"]["ItemName"] = "East Asia Inter-University Championship  東亞大學錦標賽";
$default_oea["00576"]["CatCode"] = "3";
$default_oea["00576"]["CatName"] = "Physical Development";

$default_oea["00577"]["ItemCode"] = "00577";
$default_oea["00577"]["ItemName"] = "East Asia Inter-University Invitational Bridge Championship  東亞區橋牌大學邀請錦標賽";
$default_oea["00577"]["CatCode"] = "4";
$default_oea["00577"]["CatName"] = "Aesthetic Development";

$default_oea["00580"]["ItemCode"] = "00580";
$default_oea["00580"]["ItemName"] = "East Asian Games  東亞運動會 (Please provide details in \"Description\" box.)";
$default_oea["00580"]["CatCode"] = "3";
$default_oea["00580"]["CatName"] = "Physical Development";

$default_oea["00578"]["ItemCode"] = "00578";
$default_oea["00578"]["ItemName"] = "East Asian Games - Extended learning of Physical Education  東亞運動會 - 體育科延展學習套";
$default_oea["00578"]["CatCode"] = "3";
$default_oea["00578"]["CatName"] = "Physical Development";

$default_oea["00579"]["ItemCode"] = "00579";
$default_oea["00579"]["ItemName"] = "East Asian Games - Test Event";
$default_oea["00579"]["CatCode"] = "3";
$default_oea["00579"]["CatName"] = "Physical Development";

$default_oea["00581"]["ItemCode"] = "00581";
$default_oea["00581"]["ItemName"] = "East Asian Judo Championships  澳門東亞柔道錦標賽";
$default_oea["00581"]["CatCode"] = "3";
$default_oea["00581"]["CatName"] = "Physical Development";

$default_oea["00582"]["ItemCode"] = "00582";
$default_oea["00582"]["ItemName"] = "East Asian KRA Nations CUP (CSIO)  東亞杯馬術挑戰賽";
$default_oea["00582"]["CatCode"] = "3";
$default_oea["00582"]["CatName"] = "Physical Development";

$default_oea["00583"]["ItemCode"] = "00583";
$default_oea["00583"]["ItemName"] = "East Overseas Bridge Net Summer Camp";
$default_oea["00583"]["CatCode"] = "3";
$default_oea["00583"]["CatName"] = "Physical Development";

$default_oea["00584"]["ItemCode"] = "00584";
$default_oea["00584"]["ItemName"] = "Eastern Athletic Meet";
$default_oea["00584"]["CatCode"] = "3";
$default_oea["00584"]["CatName"] = "Physical Development";

$default_oea["00585"]["ItemCode"] = "00585";
$default_oea["00585"]["ItemName"] = "Eastern District Youth Visual Arts Competition東區文藝展青年視覺藝術比賽";
$default_oea["00585"]["CatCode"] = "4";
$default_oea["00585"]["CatName"] = "Aesthetic Development";

$default_oea["00572"]["ItemCode"] = "00572";
$default_oea["00572"]["ItemName"] = "EAFF U-14 Youth Tournament  東亞U14青年足球錦標賽";
$default_oea["00572"]["CatCode"] = "3";
$default_oea["00572"]["CatName"] = "Physical Development";

$default_oea["00587"]["ItemCode"] = "00587";
$default_oea["00587"]["ItemName"] = "EcoTourism and Mountain Art and Design Competition國際生態旅遊及山區年美術設計比賽";
$default_oea["00587"]["CatCode"] = "4";
$default_oea["00587"]["CatName"] = "Aesthetic Development";

$default_oea["00586"]["ItemCode"] = "00586";
$default_oea["00586"]["ItemName"] = "ECA Exhibition in School";
$default_oea["00586"]["CatCode"] = "4";
$default_oea["00586"]["CatName"] = "Aesthetic Development";

$default_oea["00589"]["ItemCode"] = "00589";
$default_oea["00589"]["ItemName"] = "Eduarts Classic Theatre for NSS Language Arts DRAMA: Adaptat";
$default_oea["00589"]["CatCode"] = "4";
$default_oea["00589"]["CatName"] = "Aesthetic Development";

$default_oea["00590"]["ItemCode"] = "00590";
$default_oea["00590"]["ItemName"] = "Education Bureau Web-based Learning Course 教育局網上學習課程";
$default_oea["00590"]["CatCode"] = "2";
$default_oea["00590"]["CatName"] = "Career-related Experience";

$default_oea["00591"]["ItemCode"] = "00591";
$default_oea["00591"]["ItemName"] = "Education Exchange Program to Nanjiang & Shanghai Expo";
$default_oea["00591"]["CatCode"] = "2";
$default_oea["00591"]["CatName"] = "Career-related Experience";

$default_oea["00588"]["ItemCode"] = "00588";
$default_oea["00588"]["ItemName"] = "EDB National Education Exchange Programme: Tracing Our Origin";
$default_oea["00588"]["CatCode"] = "2";
$default_oea["00588"]["CatName"] = "Career-related Experience";

$default_oea["00592"]["ItemCode"] = "00592";
$default_oea["00592"]["ItemName"] = "Elder Academy長者學苑";
$default_oea["00592"]["CatCode"] = "1";
$default_oea["00592"]["CatName"] = "Community Service";

$default_oea["00593"]["ItemCode"] = "00593";
$default_oea["00593"]["ItemName"] = "Electronics Innovation Award  校際電腦控制創作大賽";
$default_oea["00593"]["CatCode"] = "2";
$default_oea["00593"]["CatName"] = "Career-related Experience";

$default_oea["00594"]["ItemCode"] = "00594";
$default_oea["00594"]["ItemName"] = "English Debating Competition (Hong Kong & Macau)  校際英文辯論比賽(香港及澳門)";
$default_oea["00594"]["CatCode"] = "2";
$default_oea["00594"]["CatName"] = "Career-related Experience";

$default_oea["00595"]["ItemCode"] = "00595";
$default_oea["00595"]["ItemName"] = "English Extensive Reading Scheme Committee";
$default_oea["00595"]["CatCode"] = "1";
$default_oea["00595"]["CatName"] = "Community Service";

$default_oea["00596"]["ItemCode"] = "00596";
$default_oea["00596"]["ItemName"] = "English Musical Theatre Animateur Scheme英語音樂劇培訓計劃";
$default_oea["00596"]["CatCode"] = "4";
$default_oea["00596"]["CatName"] = "Aesthetic Development";

$default_oea["00597"]["ItemCode"] = "00597";
$default_oea["00597"]["ItemName"] = "Enterprise Training Scheme企業實習計劃";
$default_oea["00597"]["CatCode"] = "2";
$default_oea["00597"]["CatName"] = "Career-related Experience";

$default_oea["00598"]["ItemCode"] = "00598";
$default_oea["00598"]["ItemName"] = "Environmental Prefect";
$default_oea["00598"]["CatCode"] = "1";
$default_oea["00598"]["CatName"] = "Community Service";

$default_oea["00599"]["ItemCode"] = "00599";
$default_oea["00599"]["ItemName"] = "Environmentally Friendly Fashion Show";
$default_oea["00599"]["CatCode"] = "4";
$default_oea["00599"]["CatName"] = "Aesthetic Development";

$default_oea["00600"]["ItemCode"] = "00600";
$default_oea["00600"]["ItemName"] = "Epson Colour Imaging Contest  EPSON彩色影像大賽";
$default_oea["00600"]["CatCode"] = "2";
$default_oea["00600"]["CatName"] = "Career-related Experience";

$default_oea["00601"]["ItemCode"] = "00601";
$default_oea["00601"]["ItemName"] = "ERS Book & Newspaper Report";
$default_oea["00601"]["CatCode"] = "4";
$default_oea["00601"]["CatName"] = "Aesthetic Development";

$default_oea["00605"]["ItemCode"] = "00605";
$default_oea["00605"]["ItemName"] = "Ettlingen International Competition for Young Pianists  德國埃特林根國際青少年鋼琴比賽";
$default_oea["00605"]["CatCode"] = "4";
$default_oea["00605"]["CatName"] = "Aesthetic Development";

$default_oea["00602"]["ItemCode"] = "00602";
$default_oea["00602"]["ItemName"] = "ET - English Introduction of Theatre Product經濟日報網上練習 (戲劇創作)";
$default_oea["00602"]["CatCode"] = "4";
$default_oea["00602"]["CatName"] = "Aesthetic Development";

$default_oea["00603"]["ItemCode"] = "00603";
$default_oea["00603"]["ItemName"] = "ET Learning網上訓練 - ET Learning - OLE Activities (Workplace)";
$default_oea["00603"]["CatCode"] = "4";
$default_oea["00603"]["CatName"] = "Aesthetic Development";

$default_oea["00604"]["ItemCode"] = "00604";
$default_oea["00604"]["ItemName"] = "ET Learning網上訓練 II -  ET Learning - OLE Activities (Advertising)";
$default_oea["00604"]["CatCode"] = "4";
$default_oea["00604"]["CatName"] = "Aesthetic Development";

$default_oea["00606"]["ItemCode"] = "00606";
$default_oea["00606"]["ItemName"] = "Euro-Asia International Children's Art Contest";
$default_oea["00606"]["CatCode"] = "4";
$default_oea["00606"]["CatName"] = "Aesthetic Development";

$default_oea["00607"]["ItemCode"] = "00607";
$default_oea["00607"]["ItemName"] = "Europa Cantat International Singing Week  - Youth Choirs";
$default_oea["00607"]["CatCode"] = "4";
$default_oea["00607"]["CatName"] = "Aesthetic Development";

$default_oea["00608"]["ItemCode"] = "00608";
$default_oea["00608"]["ItemName"] = "Europe Aged Group Handball Tournament";
$default_oea["00608"]["CatCode"] = "3";
$default_oea["00608"]["CatName"] = "Physical Development";

$default_oea["00609"]["ItemCode"] = "00609";
$default_oea["00609"]["ItemName"] = "Event Test for the East Asia Games Invitational Gymnastics Competition  東亞運動會試運轉體操邀請賽";
$default_oea["00609"]["CatCode"] = "3";
$default_oea["00609"]["CatName"] = "Physical Development";

$default_oea["00610"]["ItemCode"] = "00610";
$default_oea["00610"]["ItemName"] = "Events planning and Operation-Taster Program 項目策劃及運作導引課程";
$default_oea["00610"]["CatCode"] = "2";
$default_oea["00610"]["CatName"] = "Career-related Experience";

$default_oea["00611"]["ItemCode"] = "00611";
$default_oea["00611"]["ItemName"] = "Evora - International Meeting of Juvenile Art (Competition)  埃武拉國際少年藝術會議(比賽)";
$default_oea["00611"]["CatCode"] = "4";
$default_oea["00611"]["CatName"] = "Aesthetic Development";

$default_oea["00612"]["ItemCode"] = "00612";
$default_oea["00612"]["ItemName"] = "Excellent Digital Video Documentary Competition for University Students  大學生優秀數碼紀錄影片大獎";
$default_oea["00612"]["CatCode"] = "4";
$default_oea["00612"]["CatName"] = "Aesthetic Development";

$default_oea["00613"]["ItemCode"] = "00613";
$default_oea["00613"]["ItemName"] = "Exchange Programme (Please provide details in \"Description\" box)";
$default_oea["00613"]["CatCode"] = "2";
$default_oea["00613"]["CatName"] = "Career-related Experience";

$default_oea["00615"]["ItemCode"] = "00615";
$default_oea["00615"]["ItemName"] = "Extensive Reading Scheme";
$default_oea["00615"]["CatCode"] = "4";
$default_oea["00615"]["CatName"] = "Aesthetic Development";

$default_oea["00617"]["ItemCode"] = "00617";
$default_oea["00617"]["ItemName"] = "Facebook 世界面面觀";
$default_oea["00617"]["CatCode"] = "5";
$default_oea["00617"]["CatName"] = "Moral and Civic Education";

$default_oea["00618"]["ItemCode"] = "00618";
$default_oea["00618"]["ItemName"] = "Fair Trade Fair 公平貿易展會";
$default_oea["00618"]["CatCode"] = "5";
$default_oea["00618"]["CatName"] = "Moral and Civic Education";

$default_oea["00619"]["ItemCode"] = "00619";
$default_oea["00619"]["ItemName"] = "Famine 30  饑饉三十";
$default_oea["00619"]["CatCode"] = "1";
$default_oea["00619"]["CatName"] = "Community Service";

$default_oea["00620"]["ItemCode"] = "00620";
$default_oea["00620"]["ItemName"] = "Far East Judo Club Judo Competition";
$default_oea["00620"]["CatCode"] = "3";
$default_oea["00620"]["CatName"] = "Physical Development";

$default_oea["00616"]["ItemCode"] = "00616";
$default_oea["00616"]["ItemName"] = "FABULOUS READING Reading Award Scheme";
$default_oea["00616"]["CatCode"] = "4";
$default_oea["00616"]["CatName"] = "Aesthetic Development";

$default_oea["00621"]["ItemCode"] = "00621";
$default_oea["00621"]["ItemName"] = "Federation for Persons of Intellectual Disability Swimming Gala  智障人士協會水泳大會(世界)";
$default_oea["00621"]["CatCode"] = "3";
$default_oea["00621"]["CatName"] = "Physical Development";

$default_oea["00622"]["ItemCode"] = "00622";
$default_oea["00622"]["ItemName"] = "FedEx Express/Junior Achievement International Trade Challenge";
$default_oea["00622"]["CatCode"] = "2";
$default_oea["00622"]["CatName"] = "Career-related Experience";

$default_oea["00623"]["ItemCode"] = "00623";
$default_oea["00623"]["ItemName"] = "FedEx International Junior Leadership Conference";
$default_oea["00623"]["CatCode"] = "2";
$default_oea["00623"]["CatName"] = "Career-related Experience";

$default_oea["00624"]["ItemCode"] = "00624";
$default_oea["00624"]["ItemName"] = "FedEx RS:X Asian Championships cum Hong Kong Open Windsurfing Championships  亞洲滑浪風帆錦標賽暨香港滑浪風帆公開錦標賽";
$default_oea["00624"]["CatCode"] = "3";
$default_oea["00624"]["CatName"] = "Physical Development";

$default_oea["00625"]["ItemCode"] = "00625";
$default_oea["00625"]["ItemName"] = "Felix Wong Youth Improvement Award - The Hong Kong Federation of Youth Groups  香港青年協會黃寬洋青少年進修獎勵計劃";
$default_oea["00625"]["CatCode"] = "5";
$default_oea["00625"]["CatName"] = "Moral and Civic Education";

$default_oea["00626"]["ItemCode"] = "00626";
$default_oea["00626"]["ItemName"] = "Fendi 'Design Your Dream Baguette' Competition (Hong Kong Region)";
$default_oea["00626"]["CatCode"] = "3";
$default_oea["00626"]["CatName"] = "Physical Development";

$default_oea["00628"]["ItemCode"] = "00628";
$default_oea["00628"]["ItemName"] = "Festival of Sport by the International Olympic Committee";
$default_oea["00628"]["CatCode"] = "3";
$default_oea["00628"]["CatName"] = "Physical Development";

$default_oea["00629"]["ItemCode"] = "00629";
$default_oea["00629"]["ItemName"] = "Festival of Sports - Sprint Triathlon  體育節 - 三項鐵人短途賽程";
$default_oea["00629"]["CatCode"] = "3";
$default_oea["00629"]["CatName"] = "Physical Development";

$default_oea["00630"]["ItemCode"] = "00630";
$default_oea["00630"]["ItemName"] = "Festival of Sports 15km Cycling Time Trial  體育節 - 十五千米單車計時賽";
$default_oea["00630"]["CatCode"] = "3";
$default_oea["00630"]["CatName"] = "Physical Development";

$default_oea["00627"]["ItemCode"] = "00627";
$default_oea["00627"]["ItemName"] = "FESPIC Games  遠東及南太平洋區傷殘人士運動會";
$default_oea["00627"]["CatCode"] = "3";
$default_oea["00627"]["CatName"] = "Physical Development";

$default_oea["00634"]["ItemCode"] = "00634";
$default_oea["00634"]["ItemName"] = "Field Trip 校外考察 - Aesthetic Development (Please provide details in \"Description\" box)";
$default_oea["00634"]["CatCode"] = "4";
$default_oea["00634"]["CatName"] = "Aesthetic Development";

$default_oea["00635"]["ItemCode"] = "00635";
$default_oea["00635"]["ItemName"] = "Field Trip 校外考察 - Career Related (Please provide details in \"Description\" box)";
$default_oea["00635"]["CatCode"] = "2";
$default_oea["00635"]["CatName"] = "Career-related Experience";

$default_oea["00636"]["ItemCode"] = "00636";
$default_oea["00636"]["ItemName"] = "Field Trip 校外考察 - Moral and Civic Education (Please provide details in \"Description\" box)";
$default_oea["00636"]["CatCode"] = "5";
$default_oea["00636"]["CatName"] = "Moral and Civic Education";

$default_oea["00637"]["ItemCode"] = "00637";
$default_oea["00637"]["ItemName"] = "Field Trip 校外考察 - Physical Development (Please provide details in \"Description\" box)";
$default_oea["00637"]["CatCode"] = "3";
$default_oea["00637"]["CatName"] = "Physical Development";

$default_oea["00638"]["ItemCode"] = "00638";
$default_oea["00638"]["ItemName"] = "Field Trip校外考察 - Community Service- (Please provide details in \"Description\" box)";
$default_oea["00638"]["CatCode"] = "1";
$default_oea["00638"]["CatName"] = "Community Service";

$default_oea["00639"]["ItemCode"] = "00639";
$default_oea["00639"]["ItemName"] = "Field Trip校外考察 - Others- (Please provide details in \"Description\" box)";
$default_oea["00639"]["CatCode"] = "6";
$default_oea["00639"]["CatName"] = "Others";

$default_oea["00641"]["ItemCode"] = "00641";
$default_oea["00641"]["ItemName"] = "Fight Crime Poster Design Competition";
$default_oea["00641"]["CatCode"] = "4";
$default_oea["00641"]["CatName"] = "Aesthetic Development";

$default_oea["00642"]["ItemCode"] = "00642";
$default_oea["00642"]["ItemName"] = "Filipino-Chinese-Japanese International Fine Arts Exchange Exhibit.  菲律賽－中國－日本國際書畫大賽";
$default_oea["00642"]["CatCode"] = "4";
$default_oea["00642"]["CatName"] = "Aesthetic Development";

$default_oea["00643"]["ItemCode"] = "00643";
$default_oea["00643"]["ItemName"] = "Film and Digital Arts Course電影及數碼藝術課程";
$default_oea["00643"]["CatCode"] = "4";
$default_oea["00643"]["CatName"] = "Aesthetic Development";

$default_oea["00657"]["ItemCode"] = "00657";
$default_oea["00657"]["ItemName"] = "Fire Safety Ambassadors Training Day 消防安全大使訓練日";
$default_oea["00657"]["CatCode"] = "1";
$default_oea["00657"]["CatName"] = "Community Service";

$default_oea["00658"]["ItemCode"] = "00658";
$default_oea["00658"]["ItemName"] = "Fire Safety Carnival The Most Popular Award防火嘉年華最受歡迎獎";
$default_oea["00658"]["CatCode"] = "4";
$default_oea["00658"]["CatName"] = "Aesthetic Development";

$default_oea["00659"]["ItemCode"] = "00659";
$default_oea["00659"]["ItemName"] = "First Aid Course";
$default_oea["00659"]["CatCode"] = "1";
$default_oea["00659"]["CatName"] = "Community Service";

$default_oea["00662"]["ItemCode"] = "00662";
$default_oea["00662"]["ItemName"] = "Fitti青少年壁球章別獎勵計劃";
$default_oea["00662"]["CatCode"] = "3";
$default_oea["00662"]["CatName"] = "Physical Development";

$default_oea["00631"]["ItemCode"] = "00631";
$default_oea["00631"]["ItemName"] = "FIBA Asia Basketball Championship  亞洲籃球錦標賽";
$default_oea["00631"]["CatCode"] = "3";
$default_oea["00631"]["CatName"] = "Physical Development";

$default_oea["00632"]["ItemCode"] = "00632";
$default_oea["00632"]["ItemName"] = "FIBA Asia Under-16 Championship";
$default_oea["00632"]["CatCode"] = "3";
$default_oea["00632"]["CatName"] = "Physical Development";

$default_oea["00633"]["ItemCode"] = "00633";
$default_oea["00633"]["ItemName"] = "FIBA Asia Under-18 Championship";
$default_oea["00633"]["CatCode"] = "3";
$default_oea["00633"]["CatName"] = "Physical Development";

$default_oea["00640"]["ItemCode"] = "00640";
$default_oea["00640"]["ItemName"] = "FIFA U-17 World Cup";
$default_oea["00640"]["CatCode"] = "3";
$default_oea["00640"]["CatName"] = "Physical Development";

$default_oea["00644"]["ItemCode"] = "00644";
$default_oea["00644"]["ItemName"] = "FINA - 10KM Marathon Swimming World Cup  FINA國際泳聯10公里馬拉松游泳世界杯賽";
$default_oea["00644"]["CatCode"] = "3";
$default_oea["00644"]["CatName"] = "Physical Development";

$default_oea["00645"]["ItemCode"] = "00645";
$default_oea["00645"]["ItemName"] = "FINA - Diving World Series";
$default_oea["00645"]["CatCode"] = "3";
$default_oea["00645"]["CatName"] = "Physical Development";

$default_oea["00646"]["ItemCode"] = "00646";
$default_oea["00646"]["ItemName"] = "FINA - Grand Prix";
$default_oea["00646"]["CatCode"] = "3";
$default_oea["00646"]["CatName"] = "Physical Development";

$default_oea["00647"]["ItemCode"] = "00647";
$default_oea["00647"]["ItemName"] = "FINA - Junior Worlds";
$default_oea["00647"]["CatCode"] = "3";
$default_oea["00647"]["CatName"] = "Physical Development";

$default_oea["00648"]["ItemCode"] = "00648";
$default_oea["00648"]["ItemName"] = "FINA - Synchronized Swimming World Trophy";
$default_oea["00648"]["CatCode"] = "3";
$default_oea["00648"]["CatName"] = "Physical Development";

$default_oea["00649"]["ItemCode"] = "00649";
$default_oea["00649"]["ItemName"] = "FINA - Water Polo World Leagues (men’s and women’s)";
$default_oea["00649"]["CatCode"] = "3";
$default_oea["00649"]["CatName"] = "Physical Development";

$default_oea["00650"]["ItemCode"] = "00650";
$default_oea["00650"]["ItemName"] = "FINA - World Championships (also known as World Aquatics Championships)";
$default_oea["00650"]["CatCode"] = "3";
$default_oea["00650"]["CatName"] = "Physical Development";

$default_oea["00651"]["ItemCode"] = "00651";
$default_oea["00651"]["ItemName"] = "FINA - World Masters Championships (also known as \"Masters Worlds\"";
$default_oea["00651"]["CatCode"] = "3";
$default_oea["00651"]["CatName"] = "Physical Development";

$default_oea["00652"]["ItemCode"] = "00652";
$default_oea["00652"]["ItemName"] = "FINA - World Men’s Water Polo Development Trophy";
$default_oea["00652"]["CatCode"] = "3";
$default_oea["00652"]["CatName"] = "Physical Development";

$default_oea["00653"]["ItemCode"] = "00653";
$default_oea["00653"]["ItemName"] = "FINA - World Open Water Swimming Championships (also known as \"Open Water Worlds\")";
$default_oea["00653"]["CatCode"] = "3";
$default_oea["00653"]["CatName"] = "Physical Development";

$default_oea["00654"]["ItemCode"] = "00654";
$default_oea["00654"]["ItemName"] = "FINA -World Cups  FINA世界盃游泳賽";
$default_oea["00654"]["CatCode"] = "3";
$default_oea["00654"]["CatName"] = "Physical Development";

$default_oea["00655"]["ItemCode"] = "00655";
$default_oea["00655"]["ItemName"] = "FINA -World Swimming Championships  FINA(also known as \"Short Course Worlds\") 世界游泳錦標賽";
$default_oea["00655"]["CatCode"] = "3";
$default_oea["00655"]["CatName"] = "Physical Development";

$default_oea["00656"]["ItemCode"] = "00656";
$default_oea["00656"]["ItemName"] = "FIQ Asian Youth Tenpin Bowling Championships  FIQ亞洲青少年保齡球錦標賽";
$default_oea["00656"]["CatCode"] = "3";
$default_oea["00656"]["CatName"] = "Physical Development";

$default_oea["00660"]["ItemCode"] = "00660";
$default_oea["00660"]["ItemName"] = "FIRST Lego League World Festival";
$default_oea["00660"]["CatCode"] = "2";
$default_oea["00660"]["CatName"] = "Career-related Experience";

$default_oea["00661"]["ItemCode"] = "00661";
$default_oea["00661"]["ItemName"] = "FISA Rowing World Cup";
$default_oea["00661"]["CatCode"] = "3";
$default_oea["00661"]["CatName"] = "Physical Development";

$default_oea["00663"]["ItemCode"] = "00663";
$default_oea["00663"]["ItemName"] = "Flag Selling (Please provide details in \"Description\" box)";
$default_oea["00663"]["CatCode"] = "1";
$default_oea["00663"]["CatName"] = "Community Service";

$default_oea["00664"]["ItemCode"] = "00664";
$default_oea["00664"]["ItemName"] = "Fledgling Eagle Cup National Youth and Children's Calligraphy and Painting Photography Competition  雛鷹杯全國青少年兒童書畫攝影大賽";
$default_oea["00664"]["CatCode"] = "4";
$default_oea["00664"]["CatName"] = "Aesthetic Development";

$default_oea["00665"]["ItemCode"] = "00665";
$default_oea["00665"]["ItemName"] = "FLL機器人世界錦標賽中國南方區域公開賽暨香港邀請交流賽";
$default_oea["00665"]["CatCode"] = "2";
$default_oea["00665"]["CatName"] = "Career-related Experience";

$default_oea["00666"]["ItemCode"] = "00666";
$default_oea["00666"]["ItemName"] = "Forensic Science Workshop";
$default_oea["00666"]["CatCode"] = "2";
$default_oea["00666"]["CatName"] = "Career-related Experience";

$default_oea["00667"]["ItemCode"] = "00667";
$default_oea["00667"]["ItemName"] = "Forum on Film Arts with Director Wong Kar Wai";
$default_oea["00667"]["CatCode"] = "4";
$default_oea["00667"]["CatName"] = "Aesthetic Development";

$default_oea["00668"]["ItemCode"] = "00668";
$default_oea["00668"]["ItemName"] = "Foundation for the Arts and Music in Asia - The Met: Live in HD Highlight Summer Festival";
$default_oea["00668"]["CatCode"] = "4";
$default_oea["00668"]["CatName"] = "Aesthetic Development";

$default_oea["00669"]["ItemCode"] = "00669";
$default_oea["00669"]["ItemName"] = "French Impressionism Exhibition Partner Schools Scheme  法國印象派畫展夥伴學校計劃";
$default_oea["00669"]["CatCode"] = "4";
$default_oea["00669"]["CatName"] = "Aesthetic Development";

$default_oea["00670"]["ItemCode"] = "00670";
$default_oea["00670"]["ItemName"] = "French Junior Squash Open  法國青少年壁球公開賽";
$default_oea["00670"]["CatCode"] = "3";
$default_oea["00670"]["CatName"] = "Physical Development";

$default_oea["00671"]["ItemCode"] = "00671";
$default_oea["00671"]["ItemName"] = "Fresh Wave Short Film Competition of Hong Kong International Film Festival  鮮浪潮國際短片競賽";
$default_oea["00671"]["CatCode"] = "4";
$default_oea["00671"]["CatName"] = "Aesthetic Development";

$default_oea["00672"]["ItemCode"] = "00672";
$default_oea["00672"]["ItemName"] = "FS master 理財達人";
$default_oea["00672"]["CatCode"] = "2";
$default_oea["00672"]["CatName"] = "Career-related Experience";

$default_oea["00673"]["ItemCode"] = "00673";
$default_oea["00673"]["ItemName"] = "Full attendance  (Please provide details in \"Description\" box.)";
$default_oea["00673"]["CatCode"] = "2";
$default_oea["00673"]["CatName"] = "Career-related Experience";

$default_oea["00674"]["ItemCode"] = "00674";
$default_oea["00674"]["ItemName"] = "Fun Fair: Bon Voyage";
$default_oea["00674"]["CatCode"] = "4";
$default_oea["00674"]["CatName"] = "Aesthetic Development";

$default_oea["00675"]["ItemCode"] = "00675";
$default_oea["00675"]["ItemName"] = "Fun with Learning Program Design / Computer-Assisted Project Design Contest";
$default_oea["00675"]["CatCode"] = "2";
$default_oea["00675"]["CatName"] = "Career-related Experience";

$default_oea["00676"]["ItemCode"] = "00676";
$default_oea["00676"]["ItemName"] = "FUN 宵展關懷";
$default_oea["00676"]["CatCode"] = "1";
$default_oea["00676"]["CatName"] = "Community Service";

$default_oea["00677"]["ItemCode"] = "00677";
$default_oea["00677"]["ItemName"] = "FUN上加分健康網絡資訊站";
$default_oea["00677"]["CatCode"] = "1";
$default_oea["00677"]["CatName"] = "Community Service";

$default_oea["00678"]["ItemCode"] = "00678";
$default_oea["00678"]["ItemName"] = "G.T. Creative Writing Award -English Story Writing Competion";
$default_oea["00678"]["CatCode"] = "4";
$default_oea["00678"]["CatName"] = "Aesthetic Development";

$default_oea["00679"]["ItemCode"] = "00679";
$default_oea["00679"]["ItemName"] = "Game Development Competition 全港數碼遊戲創作大賽";
$default_oea["00679"]["CatCode"] = "4";
$default_oea["00679"]["CatName"] = "Aesthetic Development";

$default_oea["00680"]["ItemCode"] = "00680";
$default_oea["00680"]["ItemName"] = "GATSBY Student CM Award  GATSBY學生CM廣告創作比賽";
$default_oea["00680"]["CatCode"] = "2";
$default_oea["00680"]["CatName"] = "Career-related Experience";

$default_oea["00681"]["ItemCode"] = "00681";
$default_oea["00681"]["ItemName"] = "Gifted Programmes for secondary school students";
$default_oea["00681"]["CatCode"] = "2";
$default_oea["00681"]["CatName"] = "Career-related Experience";

$default_oea["00682"]["ItemCode"] = "00682";
$default_oea["00682"]["ItemName"] = "Gigabyte Intel P35 Asia-Pacific Overclocking Contest  技嘉Intel P35亞太區超頻挑戰賽";
$default_oea["00682"]["CatCode"] = "2";
$default_oea["00682"]["CatName"] = "Career-related Experience";

$default_oea["00683"]["ItemCode"] = "00683";
$default_oea["00683"]["ItemName"] = "Gina Bachauer International Junior Piano Competition  吉娜巴考爾國際青少年鋼琴比賽";
$default_oea["00683"]["CatCode"] = "4";
$default_oea["00683"]["CatName"] = "Aesthetic Development";

$default_oea["00684"]["ItemCode"] = "00684";
$default_oea["00684"]["ItemName"] = "Gina Bachauer International Young Artists Piano Competition, USA  美國吉娜巴考爾國際青年藝術家鋼琴比賽";
$default_oea["00684"]["CatCode"] = "4";
$default_oea["00684"]["CatName"] = "Aesthetic Development";

$default_oea["00685"]["ItemCode"] = "00685";
$default_oea["00685"]["ItemName"] = "Girl Guide Annual Parade 女童軍週年大會操";
$default_oea["00685"]["CatCode"] = "1";
$default_oea["00685"]["CatName"] = "Community Service";

$default_oea["00686"]["ItemCode"] = "00686";
$default_oea["00686"]["ItemName"] = "Girl Guides Camp";
$default_oea["00686"]["CatCode"] = "1";
$default_oea["00686"]["CatName"] = "Community Service";

$default_oea["00687"]["ItemCode"] = "00687";
$default_oea["00687"]["ItemName"] = "Girl Guides女童軍";
$default_oea["00687"]["CatCode"] = "1";
$default_oea["00687"]["CatName"] = "Community Service";

$default_oea["00688"]["ItemCode"] = "00688";
$default_oea["00688"]["ItemName"] = "Girl Nation Street Dance Competition  Girl Nation女子街舞比賽";
$default_oea["00688"]["CatCode"] = "4";
$default_oea["00688"]["CatName"] = "Aesthetic Development";

$default_oea["00689"]["ItemCode"] = "00689";
$default_oea["00689"]["ItemName"] = "Global Conference of Chinese Culture Classic  全球中華文化經典誦讀大會";
$default_oea["00689"]["CatCode"] = "4";
$default_oea["00689"]["CatName"] = "Aesthetic Development";

$default_oea["00690"]["ItemCode"] = "00690";
$default_oea["00690"]["ItemName"] = "Global Outstanding Chinese Children and Youth Singing Festival  全球傑出華人青少年歌唱節 - 獨唱比賽";
$default_oea["00690"]["CatCode"] = "4";
$default_oea["00690"]["CatName"] = "Aesthetic Development";

$default_oea["00691"]["ItemCode"] = "00691";
$default_oea["00691"]["ItemName"] = "Global Young Leaders Conference  全球青年領袖會議";
$default_oea["00691"]["CatCode"] = "2";
$default_oea["00691"]["CatName"] = "Career-related Experience";

$default_oea["00692"]["ItemCode"] = "00692";
$default_oea["00692"]["ItemName"] = "Global Youth Hockey Promoter  全球青少年曲棍球大賽";
$default_oea["00692"]["CatCode"] = "3";
$default_oea["00692"]["CatName"] = "Physical Development";

$default_oea["00693"]["ItemCode"] = "00693";
$default_oea["00693"]["ItemName"] = "Glocalization Project - High School Campaign 2008 - AIESEC (LC-CUHK)";
$default_oea["00693"]["CatCode"] = "5";
$default_oea["00693"]["CatName"] = "Moral and Civic Education";

$default_oea["00694"]["ItemCode"] = "00694";
$default_oea["00694"]["ItemName"] = "Go Game Workshop";
$default_oea["00694"]["CatCode"] = "4";
$default_oea["00694"]["CatName"] = "Aesthetic Development";

$default_oea["00696"]["ItemCode"] = "00696";
$default_oea["00696"]["ItemName"] = "Gold Service Flash - The Hong Kong Girl Guides Association";
$default_oea["00696"]["CatCode"] = "1";
$default_oea["00696"]["CatName"] = "Community Service";

$default_oea["00697"]["ItemCode"] = "00697";
$default_oea["00697"]["ItemName"] = "Golden Bauhinia Youngster Music, Dance International Competition of Hong Kong  香港國際青少年金紫荊花獎音樂、舞蹈、藝術大賽";
$default_oea["00697"]["CatCode"] = "4";
$default_oea["00697"]["CatName"] = "Aesthetic Development";

$default_oea["00698"]["ItemCode"] = "00698";
$default_oea["00698"]["ItemName"] = "Golden Bridge International Classification of Education Cup Dance Competition  金橋教育杯中國國際舞蹈分級大賽";
$default_oea["00698"]["CatCode"] = "4";
$default_oea["00698"]["CatName"] = "Aesthetic Development";

$default_oea["00699"]["ItemCode"] = "00699";
$default_oea["00699"]["ItemName"] = "Golden Lotus Cup International Competition for Children's Poems, Calligraphy & Paintings  國際金蓮花杯少兒詩書畫大獎賽";
$default_oea["00699"]["CatCode"] = "4";
$default_oea["00699"]["CatName"] = "Aesthetic Development";

$default_oea["00700"]["ItemCode"] = "00700";
$default_oea["00700"]["ItemName"] = "Golden Racket International Tournament(Table-tennis)  金球拍國際邀請賽";
$default_oea["00700"]["CatCode"] = "3";
$default_oea["00700"]["CatName"] = "Physical Development";

$default_oea["00701"]["ItemCode"] = "00701";
$default_oea["00701"]["ItemName"] = "Good Conduct Award";
$default_oea["00701"]["CatCode"] = "2";
$default_oea["00701"]["CatName"] = "Career-related Experience";

$default_oea["00702"]["ItemCode"] = "00702";
$default_oea["00702"]["ItemName"] = "Gospel Camp";
$default_oea["00702"]["CatCode"] = "5";
$default_oea["00702"]["CatName"] = "Moral and Civic Education";

$default_oea["00695"]["ItemCode"] = "00695";
$default_oea["00695"]["ItemName"] = "GOGOGO! Summer Musical Day-Camp 仲夏英語音樂劇體驗營";
$default_oea["00695"]["CatCode"] = "4";
$default_oea["00695"]["CatName"] = "Aesthetic Development";

$default_oea["00703"]["ItemCode"] = "00703";
$default_oea["00703"]["ItemName"] = "Grantham Scholars of the Year Award";
$default_oea["00703"]["CatCode"] = "2";
$default_oea["00703"]["CatName"] = "Career-related Experience";

$default_oea["00704"]["ItemCode"] = "00704";
$default_oea["00704"]["ItemName"] = "Graphic Design Competition - Breakthrough Limited";
$default_oea["00704"]["CatCode"] = "4";
$default_oea["00704"]["CatName"] = "Aesthetic Development";

$default_oea["00705"]["ItemCode"] = "00705";
$default_oea["00705"]["ItemName"] = "Graphical Communication Club圖象傳意學會";
$default_oea["00705"]["CatCode"] = "4";
$default_oea["00705"]["CatName"] = "Aesthetic Development";

$default_oea["00706"]["ItemCode"] = "00706";
$default_oea["00706"]["ItemName"] = "Green camp";
$default_oea["00706"]["CatCode"] = "1";
$default_oea["00706"]["CatName"] = "Community Service";

$default_oea["00707"]["ItemCode"] = "00707";
$default_oea["00707"]["ItemName"] = "Green Living Tips Competition";
$default_oea["00707"]["CatCode"] = "1";
$default_oea["00707"]["CatName"] = "Community Service";

$default_oea["00708"]["ItemCode"] = "00708";
$default_oea["00708"]["ItemName"] = "Green Lyrics Rewriting Competition";
$default_oea["00708"]["CatCode"] = "4";
$default_oea["00708"]["CatName"] = "Aesthetic Development";

$default_oea["00709"]["ItemCode"] = "00709";
$default_oea["00709"]["ItemName"] = "Green Manufacturing Competition  綠色創意工業比賽";
$default_oea["00709"]["CatCode"] = "2";
$default_oea["00709"]["CatName"] = "Career-related Experience";

$default_oea["00710"]["ItemCode"] = "00710";
$default_oea["00710"]["ItemName"] = "Green Power Hike  綠色力量環島行慈善行山比賽";
$default_oea["00710"]["CatCode"] = "3";
$default_oea["00710"]["CatName"] = "Physical Development";

$default_oea["00711"]["ItemCode"] = "00711";
$default_oea["00711"]["ItemName"] = "Green Talk - (Please provide details in \"Description\" box)";
$default_oea["00711"]["CatCode"] = "1";
$default_oea["00711"]["CatName"] = "Community Service";

$default_oea["00712"]["ItemCode"] = "00712";
$default_oea["00712"]["ItemName"] = "Green Tour - Explore the Environment";
$default_oea["00712"]["CatCode"] = "1";
$default_oea["00712"]["CatName"] = "Community Service";

$default_oea["00713"]["ItemCode"] = "00713";
$default_oea["00713"]["ItemName"] = "Grooming Training Class";
$default_oea["00713"]["CatCode"] = "4";
$default_oea["00713"]["CatName"] = "Aesthetic Development";

$default_oea["00714"]["ItemCode"] = "00714";
$default_oea["00714"]["ItemName"] = "Group Project Competition - Hong Kong Young Ambassador Scheme";
$default_oea["00714"]["CatCode"] = "1";
$default_oea["00714"]["CatName"] = "Community Service";

$default_oea["00715"]["ItemCode"] = "00715";
$default_oea["00715"]["ItemName"] = "GTTP Student Conference in Germany";
$default_oea["00715"]["CatCode"] = "2";
$default_oea["00715"]["CatName"] = "Career-related Experience";

$default_oea["00716"]["ItemCode"] = "00716";
$default_oea["00716"]["ItemName"] = "Guangdong - Hong Kong Cup  省港盃";
$default_oea["00716"]["CatCode"] = "3";
$default_oea["00716"]["CatName"] = "Physical Development";

$default_oea["00717"]["ItemCode"] = "00717";
$default_oea["00717"]["ItemName"] = "Guangdong and Hong Kong City Tour Windsurfing Championships";
$default_oea["00717"]["CatCode"] = "3";
$default_oea["00717"]["CatName"] = "Physical Development";

$default_oea["00718"]["ItemCode"] = "00718";
$default_oea["00718"]["ItemName"] = "Guangdong Children Art Festival And Asian Children Folk Arts Festival of CIOFF";
$default_oea["00718"]["CatCode"] = "4";
$default_oea["00718"]["CatName"] = "Aesthetic Development";

$default_oea["00720"]["ItemCode"] = "00720";
$default_oea["00720"]["ItemName"] = "Guangdong Hong Kong Roller Skating Invitation Race";
$default_oea["00720"]["CatCode"] = "3";
$default_oea["00720"]["CatName"] = "Physical Development";

$default_oea["00721"]["ItemCode"] = "00721";
$default_oea["00721"]["ItemName"] = "Guangdong Province Judo Tournament";
$default_oea["00721"]["CatCode"] = "3";
$default_oea["00721"]["CatName"] = "Physical Development";

$default_oea["00722"]["ItemCode"] = "00722";
$default_oea["00722"]["ItemName"] = "Guangdong Province, the door matches  廣東省少年門球賽";
$default_oea["00722"]["CatCode"] = "3";
$default_oea["00722"]["CatName"] = "Physical Development";

$default_oea["00729"]["ItemCode"] = "00729";
$default_oea["00729"]["ItemName"] = "Guangdong-Hong Kong Handball Cup  省港盃手球賽";
$default_oea["00729"]["CatCode"] = "3";
$default_oea["00729"]["CatName"] = "Physical Development";

$default_oea["00730"]["ItemCode"] = "00730";
$default_oea["00730"]["ItemName"] = "Guangdong-Hong Kong-Macau Children Art, Calligraphy and Photography Competition";
$default_oea["00730"]["CatCode"] = "4";
$default_oea["00730"]["CatName"] = "Aesthetic Development";

$default_oea["00731"]["ItemCode"] = "00731";
$default_oea["00731"]["ItemName"] = "Guangdong-Hong Kong-Macau Youth Basketball Competition  粵港澳青少年籃球賽";
$default_oea["00731"]["CatCode"] = "3";
$default_oea["00731"]["CatName"] = "Physical Development";

$default_oea["00732"]["ItemCode"] = "00732";
$default_oea["00732"]["ItemName"] = "Guangdong-Shenzhen-Hong Kong Amateur Judo Invitation Championships  粵深港業餘柔道邀請賽";
$default_oea["00732"]["CatCode"] = "3";
$default_oea["00732"]["CatName"] = "Physical Development";

$default_oea["00723"]["ItemCode"] = "00723";
$default_oea["00723"]["ItemName"] = "Guangdong, Hong Kong and Macao Amateur Fencing Invitational Competition";
$default_oea["00723"]["CatCode"] = "3";
$default_oea["00723"]["CatName"] = "Physical Development";

$default_oea["00724"]["ItemCode"] = "00724";
$default_oea["00724"]["ItemName"] = "Guangdong, Hong Kong and Macao Children and Adolescents meet Fine Asian Calligraphy Art Photography Competition  粵港澳少年兒童美術書法攝影大賽";
$default_oea["00724"]["CatCode"] = "4";
$default_oea["00724"]["CatName"] = "Aesthetic Development";

$default_oea["00725"]["ItemCode"] = "00725";
$default_oea["00725"]["ItemName"] = "Guangdong, Hong Kong and Macau Putonghua Competition  粵港澳普通話大賽";
$default_oea["00725"]["CatCode"] = "4";
$default_oea["00725"]["CatName"] = "Aesthetic Development";

$default_oea["00726"]["ItemCode"] = "00726";
$default_oea["00726"]["ItemName"] = "Guangdong, Hong Kong, Macao, Children Art Calligraphy Competition  粵港澳少年兒童美術書法大賽";
$default_oea["00726"]["CatCode"] = "4";
$default_oea["00726"]["CatName"] = "Aesthetic Development";

$default_oea["00727"]["ItemCode"] = "00727";
$default_oea["00727"]["ItemName"] = "Guangdong/HK/Macau Inter-port Friendship Tournament";
$default_oea["00727"]["CatCode"] = "3";
$default_oea["00727"]["CatName"] = "Physical Development";

$default_oea["00728"]["ItemCode"] = "00728";
$default_oea["00728"]["ItemName"] = "Guangdong/HK/Macau Invitation Tournament  粵港澳邀請聯賽";
$default_oea["00728"]["CatCode"] = "3";
$default_oea["00728"]["CatName"] = "Physical Development";

$default_oea["00719"]["ItemCode"] = "00719";
$default_oea["00719"]["ItemName"] = "GuangDong Dowin Cup Youth Athletics Championships";
$default_oea["00719"]["CatCode"] = "3";
$default_oea["00719"]["CatName"] = "Physical Development";

$default_oea["00733"]["ItemCode"] = "00733";
$default_oea["00733"]["ItemName"] = "Guangzhou and Hong Kong Shenzhen (amateur) Judo Tournament  穗港深業餘柔道邀請賽";
$default_oea["00733"]["CatCode"] = "3";
$default_oea["00733"]["CatName"] = "Physical Development";

$default_oea["00734"]["ItemCode"] = "00734";
$default_oea["00734"]["ItemName"] = "Guangzhou Secondary Schools Mandarin Debate";
$default_oea["00734"]["CatCode"] = "5";
$default_oea["00734"]["CatName"] = "Moral and Civic Education";

$default_oea["00735"]["ItemCode"] = "00735";
$default_oea["00735"]["ItemName"] = "Guangzhou Water Polo Tournament";
$default_oea["00735"]["CatCode"] = "3";
$default_oea["00735"]["CatName"] = "Physical Development";

$default_oea["00738"]["ItemCode"] = "00738";
$default_oea["00738"]["ItemName"] = "Guangzhou-Hong Kong Cup Judo Competition  粵港杯柔道邀請賽";
$default_oea["00738"]["CatCode"] = "3";
$default_oea["00738"]["CatName"] = "Physical Development";

$default_oea["00739"]["ItemCode"] = "00739";
$default_oea["00739"]["ItemName"] = "Guangzhou-Hong Kong Taekwondo Championship  穗港杯跆拳道錦標賽";
$default_oea["00739"]["CatCode"] = "3";
$default_oea["00739"]["CatName"] = "Physical Development";

$default_oea["00736"]["ItemCode"] = "00736";
$default_oea["00736"]["ItemName"] = "Guangzhou, Hong Kong and Macau Chinese Chess Player Competition";
$default_oea["00736"]["CatCode"] = "4";
$default_oea["00736"]["CatName"] = "Aesthetic Development";

$default_oea["00737"]["ItemCode"] = "00737";
$default_oea["00737"]["ItemName"] = "Guangzhou, Hong Kong and Macau Youth Skills Competition  穗港澳青年技能競賽";
$default_oea["00737"]["CatCode"] = "2";
$default_oea["00737"]["CatName"] = "Career-related Experience";

$default_oea["00740"]["ItemCode"] = "00740";
$default_oea["00740"]["ItemName"] = "Guilin-Jecheon, Chinese-Korean Friendly Exchange Exhibition Contest  桂林堤川中韓友好交流大展賽";
$default_oea["00740"]["CatCode"] = "4";
$default_oea["00740"]["CatName"] = "Aesthetic Development";

$default_oea["00741"]["ItemCode"] = "00741";
$default_oea["00741"]["ItemName"] = "Hall Assembly禮堂聚會I: 90後社會抗爭";
$default_oea["00741"]["CatCode"] = "5";
$default_oea["00741"]["CatName"] = "Moral and Civic Education";

$default_oea["00742"]["ItemCode"] = "00742";
$default_oea["00742"]["ItemName"] = "Hamamatsu International Piano Academy Competition  濱松國際鋼琴學院大賽";
$default_oea["00742"]["CatCode"] = "4";
$default_oea["00742"]["CatName"] = "Aesthetic Development";

$default_oea["00743"]["ItemCode"] = "00743";
$default_oea["00743"]["ItemName"] = "Handball Competition (Please provide details in \"Description\" box)";
$default_oea["00743"]["CatCode"] = "3";
$default_oea["00743"]["CatName"] = "Physical Development";

$default_oea["00744"]["ItemCode"] = "00744";
$default_oea["00744"]["ItemName"] = "Hang Lung Mathematics Awards 恆隆數學獎";
$default_oea["00744"]["CatCode"] = "2";
$default_oea["00744"]["CatName"] = "Career-related Experience";

$default_oea["00745"]["ItemCode"] = "00745";
$default_oea["00745"]["ItemName"] = "Hang Sang Bank - Leaders to Leaders Lecture Series";
$default_oea["00745"]["CatCode"] = "2";
$default_oea["00745"]["CatName"] = "Career-related Experience";

$default_oea["00746"]["ItemCode"] = "00746";
$default_oea["00746"]["ItemName"] = "Hang Sang Bank's Social Entrepreneurship Programme  恒生銀行社會企業家計劃";
$default_oea["00746"]["CatCode"] = "2";
$default_oea["00746"]["CatName"] = "Career-related Experience";

$default_oea["00747"]["ItemCode"] = "00747";
$default_oea["00747"]["ItemName"] = "Happy Run Power Fitness Training Fun Fair";
$default_oea["00747"]["CatCode"] = "3";
$default_oea["00747"]["CatName"] = "Physical Development";

$default_oea["00748"]["ItemCode"] = "00748";
$default_oea["00748"]["ItemName"] = "Harmony of the World Drawing Competition和諧世界一家欣繪畫比賽";
$default_oea["00748"]["CatCode"] = "3";
$default_oea["00748"]["CatName"] = "Physical Development";

$default_oea["00749"]["ItemCode"] = "00749";
$default_oea["00749"]["ItemName"] = "Harvard Book Prize - The Harvard Club of Hong Kong";
$default_oea["00749"]["CatCode"] = "4";
$default_oea["00749"]["CatName"] = "Aesthetic Development";

$default_oea["00750"]["ItemCode"] = "00750";
$default_oea["00750"]["ItemName"] = "Harvard Model Congress Asia";
$default_oea["00750"]["CatCode"] = "2";
$default_oea["00750"]["CatName"] = "Career-related Experience";

$default_oea["00751"]["ItemCode"] = "00751";
$default_oea["00751"]["ItemName"] = "Hatyai International Junior Tennis Championships  泰國合艾國際青少年網球比賽";
$default_oea["00751"]["CatCode"] = "3";
$default_oea["00751"]["CatName"] = "Physical Development";

$default_oea["00752"]["ItemCode"] = "00752";
$default_oea["00752"]["ItemName"] = "Haunted House Soundtrack Mixing Competition";
$default_oea["00752"]["CatCode"] = "4";
$default_oea["00752"]["CatName"] = "Aesthetic Development";

$default_oea["00753"]["ItemCode"] = "00753";
$default_oea["00753"]["ItemName"] = "Healing the Heave Healing the Mind Competition痊心痊意-接納與關懷精神病者模型創作比賽";
$default_oea["00753"]["CatCode"] = "4";
$default_oea["00753"]["CatName"] = "Aesthetic Development";

$default_oea["00754"]["ItemCode"] = "00754";
$default_oea["00754"]["ItemName"] = "Health & Environmental Education Club健康環保小組";
$default_oea["00754"]["CatCode"] = "1";
$default_oea["00754"]["CatName"] = "Community Service";

$default_oea["00755"]["ItemCode"] = "00755";
$default_oea["00755"]["ItemName"] = "Health In Mind Programme - Joint School Drama competition";
$default_oea["00755"]["CatCode"] = "4";
$default_oea["00755"]["CatName"] = "Aesthetic Development";

$default_oea["00756"]["ItemCode"] = "00756";
$default_oea["00756"]["ItemName"] = "Health In Mind Programme - Mass Service I: Interview with the mentally-challenged";
$default_oea["00756"]["CatCode"] = "4";
$default_oea["00756"]["CatName"] = "Aesthetic Development";

$default_oea["00757"]["ItemCode"] = "00757";
$default_oea["00757"]["ItemName"] = "Health In Mind Programme - Orientation Camp";
$default_oea["00757"]["CatCode"] = "4";
$default_oea["00757"]["CatName"] = "Aesthetic Development";

$default_oea["00758"]["ItemCode"] = "00758";
$default_oea["00758"]["ItemName"] = "Health In Mind Programme - Service I: BBQ Party";
$default_oea["00758"]["CatCode"] = "4";
$default_oea["00758"]["CatName"] = "Aesthetic Development";

$default_oea["00759"]["ItemCode"] = "00759";
$default_oea["00759"]["ItemName"] = "Health In Mind Programme - Workshop III";
$default_oea["00759"]["CatCode"] = "4";
$default_oea["00759"]["CatName"] = "Aesthetic Development";

$default_oea["00760"]["ItemCode"] = "00760";
$default_oea["00760"]["ItemName"] = "Health In Mind Programme - Workshop IV";
$default_oea["00760"]["CatCode"] = "4";
$default_oea["00760"]["CatName"] = "Aesthetic Development";

$default_oea["00761"]["ItemCode"] = "00761";
$default_oea["00761"]["ItemName"] = "Health In Mind Programme x Sports Society - Talk on mental i";
$default_oea["00761"]["CatCode"] = "4";
$default_oea["00761"]["CatName"] = "Aesthetic Development";

$default_oea["00762"]["ItemCode"] = "00762";
$default_oea["00762"]["ItemName"] = "Heart to Heart, Hand in Hand Shanghai Youth Drawing Competition  心連心、手拉手上海國際少兒書畫大賽";
$default_oea["00762"]["CatCode"] = "4";
$default_oea["00762"]["CatName"] = "Aesthetic Development";

$default_oea["00763"]["ItemCode"] = "00763";
$default_oea["00763"]["ItemName"] = "Hedy King Robinson Prize for Theory of Music";
$default_oea["00763"]["CatCode"] = "4";
$default_oea["00763"]["CatName"] = "Aesthetic Development";

$default_oea["00764"]["ItemCode"] = "00764";
$default_oea["00764"]["ItemName"] = "Help the Police Fight Youth Crime Competition - Hang Seng Bank & Hong Kong Police Force";
$default_oea["00764"]["CatCode"] = "1";
$default_oea["00764"]["CatName"] = "Community Service";

$default_oea["00765"]["ItemCode"] = "00765";
$default_oea["00765"]["ItemName"] = "Hengyang-Hu Nan Province-Hong Kong Gymnastics Competition HKAGA";
$default_oea["00765"]["CatCode"] = "3";
$default_oea["00765"]["CatName"] = "Physical Development";

$default_oea["00766"]["ItemCode"] = "00766";
$default_oea["00766"]["ItemName"] = "Hengyuanxiang Literary Star Chinese Students Composition Contest  恆源祥文學之星中國中學生作文大賽";
$default_oea["00766"]["CatCode"] = "4";
$default_oea["00766"]["CatName"] = "Aesthetic Development";

$default_oea["00767"]["ItemCode"] = "00767";
$default_oea["00767"]["ItemName"] = "Hewlett-Packard Global Business Challenge";
$default_oea["00767"]["CatCode"] = "2";
$default_oea["00767"]["CatName"] = "Career-related Experience";

$default_oea["00768"]["ItemCode"] = "00768";
$default_oea["00768"]["ItemName"] = "Hexarchy Summit";
$default_oea["00768"]["CatCode"] = "2";
$default_oea["00768"]["CatName"] = "Career-related Experience";

$default_oea["00769"]["ItemCode"] = "00769";
$default_oea["00769"]["ItemName"] = "Hibiscus Cup National Youth and Children's Calligraphy and Painting Photography Competition  芙蓉杯全國青少年兒童書畫攝影大賽";
$default_oea["00769"]["CatCode"] = "4";
$default_oea["00769"]["CatName"] = "Aesthetic Development";

$default_oea["00770"]["ItemCode"] = "00770";
$default_oea["00770"]["ItemName"] = "Hip Hop Day 舞蹈體驗日";
$default_oea["00770"]["CatCode"] = "4";
$default_oea["00770"]["CatName"] = "Aesthetic Development";

$default_oea["00771"]["ItemCode"] = "00771";
$default_oea["00771"]["ItemName"] = "Historical Architectural Drawing Competition";
$default_oea["00771"]["CatCode"] = "4";
$default_oea["00771"]["CatName"] = "Aesthetic Development";

$default_oea["00772"]["ItemCode"] = "00772";
$default_oea["00772"]["ItemName"] = "History and Art Exhibition歷史與藝術 國家重大歷史題材美術創作工程作品香港展";
$default_oea["00772"]["CatCode"] = "4";
$default_oea["00772"]["CatName"] = "Aesthetic Development";

$default_oea["00773"]["ItemCode"] = "00773";
$default_oea["00773"]["ItemName"] = "HKDA KongCept 2031";
$default_oea["00773"]["CatCode"] = "4";
$default_oea["00773"]["CatName"] = "Aesthetic Development";

$default_oea["00774"]["ItemCode"] = "00774";
$default_oea["00774"]["ItemName"] = "HKDSE-公共衛生及可持續發展計劃";
$default_oea["00774"]["CatCode"] = "2";
$default_oea["00774"]["CatName"] = "Career-related Experience";

$default_oea["00775"]["ItemCode"] = "00775";
$default_oea["00775"]["ItemName"] = "HKFYG Against Gambling Competition香港青年協會戒賭漫畫創作比賽";
$default_oea["00775"]["CatCode"] = "4";
$default_oea["00775"]["CatName"] = "Aesthetic Development";

$default_oea["00776"]["ItemCode"] = "00776";
$default_oea["00776"]["ItemName"] = "HKGGA 95th Anniversary Log Design Competition";
$default_oea["00776"]["CatCode"] = "4";
$default_oea["00776"]["CatName"] = "Aesthetic Development";

$default_oea["00777"]["ItemCode"] = "00777";
$default_oea["00777"]["ItemName"] = "HKIAAT Accounting and Business Management Case Competition  香港財務會計協會-會計及商業管理個案比賽";
$default_oea["00777"]["CatCode"] = "2";
$default_oea["00777"]["CatName"] = "Career-related Experience";

$default_oea["00778"]["ItemCode"] = "00778";
$default_oea["00778"]["ItemName"] = "HKIE Virtual Bridge Design Contest - Hong Kong Institution of Engineers";
$default_oea["00778"]["CatCode"] = "4";
$default_oea["00778"]["CatName"] = "Aesthetic Development";

$default_oea["00779"]["ItemCode"] = "00779";
$default_oea["00779"]["ItemName"] = "HKPO Swire Maestro - Ton Koopman’s Haydn and Mozart";
$default_oea["00779"]["CatCode"] = "4";
$default_oea["00779"]["CatName"] = "Aesthetic Development";

$default_oea["00780"]["ItemCode"] = "00780";
$default_oea["00780"]["ItemName"] = "HKPO Swire New Generation Series - Tianwa, the Rising Star";
$default_oea["00780"]["CatCode"] = "4";
$default_oea["00780"]["CatName"] = "Aesthetic Development";

$default_oea["00781"]["ItemCode"] = "00781";
$default_oea["00781"]["ItemName"] = "HKSAR Scout Award  榮譽童軍獎章";
$default_oea["00781"]["CatCode"] = "1";
$default_oea["00781"]["CatName"] = "Community Service";

$default_oea["00782"]["ItemCode"] = "00782";
$default_oea["00782"]["ItemName"] = "HKU Science Workshop";
$default_oea["00782"]["CatCode"] = "2";
$default_oea["00782"]["CatName"] = "Career-related Experience";

$default_oea["00783"]["ItemCode"] = "00783";
$default_oea["00783"]["ItemName"] = "HKU-Dragon Camp 香港大學偉倫堂生活體驗營";
$default_oea["00783"]["CatCode"] = "4";
$default_oea["00783"]["CatName"] = "Aesthetic Development";

$default_oea["00784"]["ItemCode"] = "00784";
$default_oea["00784"]["ItemName"] = "HKUST / IBM Technology Exploration Camp";
$default_oea["00784"]["CatCode"] = "2";
$default_oea["00784"]["CatName"] = "Career-related Experience";

$default_oea["00785"]["ItemCode"] = "00785";
$default_oea["00785"]["ItemName"] = "Hockey 5's  五人冰球賽";
$default_oea["00785"]["CatCode"] = "3";
$default_oea["00785"]["CatName"] = "Physical Development";

$default_oea["00786"]["ItemCode"] = "00786";
$default_oea["00786"]["ItemName"] = "Hong Kong  - Chinese Billiards Competition  康樂棋比賽 (Please provide details in \"Description\" box.) ";
$default_oea["00786"]["CatCode"] = "3";
$default_oea["00786"]["CatName"] = "Physical Development";

$default_oea["00787"]["ItemCode"] = "00787";
$default_oea["00787"]["ItemName"] = "Hong Kong - Macau Interport Swimming Competition  港澳埠際游泳比賽";
$default_oea["00787"]["CatCode"] = "3";
$default_oea["00787"]["CatName"] = "Physical Development";

$default_oea["00788"]["ItemCode"] = "00788";
$default_oea["00788"]["ItemName"] = "Hong Kong - Macau Youth Networking Skills Competition  港澳青少年網絡技能大賽";
$default_oea["00788"]["CatCode"] = "2";
$default_oea["00788"]["CatName"] = "Career-related Experience";

$default_oea["00789"]["ItemCode"] = "00789";
$default_oea["00789"]["ItemName"] = "Hong Kong - Shanghai Student Exchange Ambassadors";
$default_oea["00789"]["CatCode"] = "2";
$default_oea["00789"]["CatName"] = "Career-related Experience";

$default_oea["00794"]["ItemCode"] = "00794";
$default_oea["00794"]["ItemName"] = "Hong Kong / Kagoshima Sports Exchange Programme - Volleyball  香港/鹿兒島體育交流計劃- 排球";
$default_oea["00794"]["CatCode"] = "3";
$default_oea["00794"]["CatName"] = "Physical Development";

$default_oea["00790"]["ItemCode"] = "00790";
$default_oea["00790"]["ItemName"] = "Hong Kong (Asia Pacific) Grieg and Bartok Piano Competition  香港(亞太區)格里格、巴托克鋼琴大賽";
$default_oea["00790"]["CatCode"] = "4";
$default_oea["00790"]["CatName"] = "Aesthetic Development";

$default_oea["00791"]["ItemCode"] = "00791";
$default_oea["00791"]["ItemName"] = "Hong Kong (Asia Pacific) Mozart Piano Competition  香港(亞太區)莫扎特鋼琴大賽";
$default_oea["00791"]["CatCode"] = "4";
$default_oea["00791"]["CatName"] = "Aesthetic Development";

$default_oea["00792"]["ItemCode"] = "00792";
$default_oea["00792"]["ItemName"] = "Hong Kong (Asia) Piano Open Competition  香港(亞洲)鋼琴公開比賽";
$default_oea["00792"]["CatCode"] = "4";
$default_oea["00792"]["CatName"] = "Aesthetic Development";

$default_oea["00793"]["ItemCode"] = "00793";
$default_oea["00793"]["ItemName"] = "Hong Kong (China) Youth and Children Piano Competition  香港(中國)少年及兒童鋼琴比賽";
$default_oea["00793"]["CatCode"] = "4";
$default_oea["00793"]["CatName"] = "Aesthetic Development";

$default_oea["00800"]["ItemCode"] = "00800";
$default_oea["00800"]["ItemName"] = "Hong Kong and Guang Dong Youth Athletic Meet";
$default_oea["00800"]["CatCode"] = "3";
$default_oea["00800"]["CatName"] = "Physical Development";

$default_oea["00801"]["ItemCode"] = "00801";
$default_oea["00801"]["ItemName"] = "Hong Kong and Guangzhou Handball Tournament";
$default_oea["00801"]["CatCode"] = "3";
$default_oea["00801"]["CatName"] = "Physical Development";

$default_oea["00802"]["ItemCode"] = "00802";
$default_oea["00802"]["ItemName"] = "Hong Kong and Jiang Men Exchange Program - Volleyball Competition";
$default_oea["00802"]["CatCode"] = "3";
$default_oea["00802"]["CatName"] = "Physical Development";

$default_oea["00803"]["ItemCode"] = "00803";
$default_oea["00803"]["ItemName"] = "Hong Kong and Macau Handball Tournament";
$default_oea["00803"]["CatCode"] = "3";
$default_oea["00803"]["CatName"] = "Physical Development";

$default_oea["00804"]["ItemCode"] = "00804";
$default_oea["00804"]["ItemName"] = "Hong Kong and Macau Straw Competition  港澳中學生飲管比賽";
$default_oea["00804"]["CatCode"] = "4";
$default_oea["00804"]["CatName"] = "Aesthetic Development";

$default_oea["00805"]["ItemCode"] = "00805";
$default_oea["00805"]["ItemName"] = "Hong Kong and Shanghai Youth Financial son BLOG War of the Worlds  港滬青年財子Blog爭霸戰";
$default_oea["00805"]["CatCode"] = "2";
$default_oea["00805"]["CatName"] = "Career-related Experience";

$default_oea["00806"]["ItemCode"] = "00806";
$default_oea["00806"]["ItemName"] = "Hong Kong and Southern China Schools English Writing Contest";
$default_oea["00806"]["CatCode"] = "4";
$default_oea["00806"]["CatName"] = "Aesthetic Development";

$default_oea["00807"]["ItemCode"] = "00807";
$default_oea["00807"]["ItemName"] = "Hong Kong and Suizho Taekwondo Competition";
$default_oea["00807"]["CatCode"] = "3";
$default_oea["00807"]["CatName"] = "Physical Development";

$default_oea["00808"]["ItemCode"] = "00808";
$default_oea["00808"]["ItemName"] = "Hong Kong and Taiwan My video Competiton  台港兩地短片由我創製作大賽";
$default_oea["00808"]["CatCode"] = "4";
$default_oea["00808"]["CatName"] = "Aesthetic Development";

$default_oea["00795"]["ItemCode"] = "00795";
$default_oea["00795"]["ItemName"] = "Hong Kong Academy for Performing Arts Open Day";
$default_oea["00795"]["CatCode"] = "4";
$default_oea["00795"]["CatName"] = "Aesthetic Development";

$default_oea["00796"]["ItemCode"] = "00796";
$default_oea["00796"]["ItemName"] = "Hong Kong Adventure Corps香港少年領袖團 (Please provide details in \"Description\" box.)";
$default_oea["00796"]["CatCode"] = "1";
$default_oea["00796"]["CatName"] = "Community Service";

$default_oea["00797"]["ItemCode"] = "00797";
$default_oea["00797"]["ItemName"] = "Hong Kong Age Group Asian-City Invitational Athletic Meet";
$default_oea["00797"]["CatCode"] = "3";
$default_oea["00797"]["CatName"] = "Physical Development";

$default_oea["00798"]["ItemCode"] = "00798";
$default_oea["00798"]["ItemName"] = "Hong Kong Air Cadet Corps香港航空青年團 (Please provide details in \"Description\" box.)";
$default_oea["00798"]["CatCode"] = "1";
$default_oea["00798"]["CatName"] = "Community Service";

$default_oea["00799"]["ItemCode"] = "00799";
$default_oea["00799"]["ItemName"] = "Hong Kong Airplane Contest";
$default_oea["00799"]["CatCode"] = "4";
$default_oea["00799"]["CatName"] = "Aesthetic Development";

$default_oea["00809"]["ItemCode"] = "00809";
$default_oea["00809"]["ItemName"] = "Hong Kong Artistic Gymnastics Open & Novice Championship";
$default_oea["00809"]["CatCode"] = "3";
$default_oea["00809"]["CatName"] = "Physical Development";

$default_oea["00810"]["ItemCode"] = "00810";
$default_oea["00810"]["ItemName"] = "Hong Kong Arts Ambassadoss-in-school香港藝術發展局校園藝術大使計劃";
$default_oea["00810"]["CatCode"] = "4";
$default_oea["00810"]["CatName"] = "Aesthetic Development";

$default_oea["00811"]["ItemCode"] = "00811";
$default_oea["00811"]["ItemName"] = "Hong Kong Astronomy Year Art and Design Competition國際天文年美術設計創作比賽(香港賽)";
$default_oea["00811"]["CatCode"] = "4";
$default_oea["00811"]["CatName"] = "Aesthetic Development";

$default_oea["00812"]["ItemCode"] = "00812";
$default_oea["00812"]["ItemName"] = "Hong Kong Athletic Meet Events";
$default_oea["00812"]["CatCode"] = "3";
$default_oea["00812"]["CatName"] = "Physical Development";

$default_oea["00813"]["ItemCode"] = "00813";
$default_oea["00813"]["ItemName"] = "Hong Kong Athletics League  香港田徑聯賽";
$default_oea["00813"]["CatCode"] = "3";
$default_oea["00813"]["CatName"] = "Physical Development";

$default_oea["00814"]["ItemCode"] = "00814";
$default_oea["00814"]["ItemName"] = "Hong Kong Award for Young People - Bronze Award  香港青年獎勵計劃銅章級";
$default_oea["00814"]["CatCode"] = "2";
$default_oea["00814"]["CatName"] = "Career-related Experience";

$default_oea["00815"]["ItemCode"] = "00815";
$default_oea["00815"]["ItemName"] = "Hong Kong Award for Young People - Gold Award    香港青年獎勵計劃金章級";
$default_oea["00815"]["CatCode"] = "2";
$default_oea["00815"]["CatName"] = "Career-related Experience";

$default_oea["00816"]["ItemCode"] = "00816";
$default_oea["00816"]["ItemName"] = "Hong Kong Award for Young People - Silver Award  香港青年獎勵計劃銀章級";
$default_oea["00816"]["CatCode"] = "2";
$default_oea["00816"]["CatName"] = "Career-related Experience";

$default_oea["00817"]["ItemCode"] = "00817";
$default_oea["00817"]["ItemName"] = "Hong Kong Ballroom Dance and Latin Competion";
$default_oea["00817"]["CatCode"] = "3";
$default_oea["00817"]["CatName"] = "Physical Development";

$default_oea["00818"]["ItemCode"] = "00818";
$default_oea["00818"]["ItemName"] = "Hong Kong Bauhinia Cup Asian Figure Skating Invitation Competition  香港紫荊花盃花式滑冰邀請賽";
$default_oea["00818"]["CatCode"] = "3";
$default_oea["00818"]["CatName"] = "Physical Development";

$default_oea["00819"]["ItemCode"] = "00819";
$default_oea["00819"]["ItemName"] = "Hong Kong Budding Scientists Award  香港科學青苗獎";
$default_oea["00819"]["CatCode"] = "2";
$default_oea["00819"]["CatName"] = "Career-related Experience";

$default_oea["00820"]["ItemCode"] = "00820";
$default_oea["00820"]["ItemName"] = "Hong Kong Business Challenge - Junior Achievement Hong Kong";
$default_oea["00820"]["CatCode"] = "2";
$default_oea["00820"]["CatName"] = "Career-related Experience";

$default_oea["00821"]["ItemCode"] = "00821";
$default_oea["00821"]["ItemName"] = "Hong Kong Butterfly Watching Race and Photo Contest (Please provide details in \"Description\" box)";
$default_oea["00821"]["CatCode"] = "4";
$default_oea["00821"]["CatName"] = "Aesthetic Development";

$default_oea["00822"]["ItemCode"] = "00822";
$default_oea["00822"]["ItemName"] = "Hong Kong Butterfly Watching Race 香港觀蝶大賽 (Please provide details in \"Description\" box)";
$default_oea["00822"]["CatCode"] = "4";
$default_oea["00822"]["CatName"] = "Aesthetic Development";

$default_oea["00823"]["ItemCode"] = "00823";
$default_oea["00823"]["ItemName"] = "Hong Kong Chinese Drum Competition 全港鼓樂比賽";
$default_oea["00823"]["CatCode"] = "4";
$default_oea["00823"]["CatName"] = "Aesthetic Development";

$default_oea["00824"]["ItemCode"] = "00824";
$default_oea["00824"]["ItemName"] = "Hong Kong Civil Aid Service (HKCAS) Commanders' Commendation";
$default_oea["00824"]["CatCode"] = "5";
$default_oea["00824"]["CatName"] = "Moral and Civic Education";

$default_oea["00825"]["ItemCode"] = "00825";
$default_oea["00825"]["ItemName"] = "Hong Kong Cup Diplomatic Knowledge Contest  香港杯外交知識競賽";
$default_oea["00825"]["CatCode"] = "5";
$default_oea["00825"]["CatName"] = "Moral and Civic Education";

$default_oea["00826"]["ItemCode"] = "00826";
$default_oea["00826"]["ItemName"] = "Hong Kong Dance Expo  香港舞蹈博覽";
$default_oea["00826"]["CatCode"] = "4";
$default_oea["00826"]["CatName"] = "Aesthetic Development";

$default_oea["00827"]["ItemCode"] = "00827";
$default_oea["00827"]["ItemName"] = "Hong Kong District Handball Team Training Competition";
$default_oea["00827"]["CatCode"] = "3";
$default_oea["00827"]["CatName"] = "Physical Development";

$default_oea["00828"]["ItemCode"] = "00828";
$default_oea["00828"]["ItemName"] = "Hong Kong Dodgeball Open Competition";
$default_oea["00828"]["CatCode"] = "3";
$default_oea["00828"]["CatName"] = "Physical Development";

$default_oea["00829"]["ItemCode"] = "00829";
$default_oea["00829"]["ItemName"] = "Hong Kong Drawing Contest  香港學界素描比賽";
$default_oea["00829"]["CatCode"] = "4";
$default_oea["00829"]["CatName"] = "Aesthetic Development";

$default_oea["00830"]["ItemCode"] = "00830";
$default_oea["00830"]["ItemName"] = "Hong Kong Elite Handball District Team Championship";
$default_oea["00830"]["CatCode"] = "3";
$default_oea["00830"]["CatName"] = "Physical Development";

$default_oea["00831"]["ItemCode"] = "00831";
$default_oea["00831"]["ItemName"] = "Hong Kong Engineering Challenge 香港工程挑戰賽";
$default_oea["00831"]["CatCode"] = "2";
$default_oea["00831"]["CatName"] = "Career-related Experience";

$default_oea["00832"]["ItemCode"] = "00832";
$default_oea["00832"]["ItemName"] = "Hong Kong English Public Speaking Contest";
$default_oea["00832"]["CatCode"] = "4";
$default_oea["00832"]["CatName"] = "Aesthetic Development";

$default_oea["00833"]["ItemCode"] = "00833";
$default_oea["00833"]["ItemName"] = "Hong Kong English Public Speaking Enhancement Workshop";
$default_oea["00833"]["CatCode"] = "4";
$default_oea["00833"]["CatName"] = "Aesthetic Development";

$default_oea["00834"]["ItemCode"] = "00834";
$default_oea["00834"]["ItemName"] = "Hong Kong Federation of Education Workers香港教育工作者聯會- Catch me if you can 探索號";
$default_oea["00834"]["CatCode"] = "2";
$default_oea["00834"]["CatName"] = "Career-related Experience";

$default_oea["00835"]["ItemCode"] = "00835";
$default_oea["00835"]["ItemName"] = "Hong Kong FIRST Lego League Robotic Tournament  香港FLL創意機械人大賽";
$default_oea["00835"]["CatCode"] = "2";
$default_oea["00835"]["CatName"] = "Career-related Experience";

$default_oea["00836"]["ItemCode"] = "00836";
$default_oea["00836"]["ItemName"] = "Hong Kong Flower Show Student Drawing Competition香港花卉展覽學童繪畫比賽";
$default_oea["00836"]["CatCode"] = "4";
$default_oea["00836"]["CatName"] = "Aesthetic Development";

$default_oea["00839"]["ItemCode"] = "00839";
$default_oea["00839"]["ItemName"] = "Hong Kong government study and leardership training";
$default_oea["00839"]["CatCode"] = "2";
$default_oea["00839"]["CatName"] = "Career-related Experience";

$default_oea["00837"]["ItemCode"] = "00837";
$default_oea["00837"]["ItemName"] = "Hong Kong Girl Guide Association Annual Parade";
$default_oea["00837"]["CatCode"] = "1";
$default_oea["00837"]["CatName"] = "Community Service";

$default_oea["00838"]["ItemCode"] = "00838";
$default_oea["00838"]["ItemName"] = "Hong Kong Girl Guide Association Raffle Tickets Open Sale  女童軍獎券公開銷售";
$default_oea["00838"]["CatCode"] = "1";
$default_oea["00838"]["CatName"] = "Community Service";

$default_oea["00840"]["ItemCode"] = "00840";
$default_oea["00840"]["ItemName"] = "Hong Kong Handbell Festival - School Competition";
$default_oea["00840"]["CatCode"] = "4";
$default_oea["00840"]["CatName"] = "Aesthetic Development";

$default_oea["00841"]["ItemCode"] = "00841";
$default_oea["00841"]["ItemName"] = "Hong Kong Independent Short Film & Video Awards (IFVA)  香港獨立短片及錄像比賽";
$default_oea["00841"]["CatCode"] = "4";
$default_oea["00841"]["CatName"] = "Aesthetic Development";

$default_oea["00842"]["ItemCode"] = "00842";
$default_oea["00842"]["ItemName"] = "Hong Kong Institute of Professional Photographers Asia Photo Awards  香港專業攝影師公會亞洲攝影年獎";
$default_oea["00842"]["CatCode"] = "4";
$default_oea["00842"]["CatName"] = "Aesthetic Development";

$default_oea["00844"]["ItemCode"] = "00844";
$default_oea["00844"]["ItemName"] = "Hong Kong Inter-city Bridge Championships  香港城巿橋牌錦標賽";
$default_oea["00844"]["CatCode"] = "4";
$default_oea["00844"]["CatName"] = "Aesthetic Development";

$default_oea["00843"]["ItemCode"] = "00843";
$default_oea["00843"]["ItemName"] = "Hong Kong Inter-City Athletics Championships  香港城市田徑錦標賽";
$default_oea["00843"]["CatCode"] = "3";
$default_oea["00843"]["CatName"] = "Physical Development";

$default_oea["00845"]["ItemCode"] = "00845";
$default_oea["00845"]["ItemName"] = "Hong Kong Inter-City School Real Time Debate on Net  香港校際網上實時埠際辯論比賽";
$default_oea["00845"]["CatCode"] = "5";
$default_oea["00845"]["CatName"] = "Moral and Civic Education";

$default_oea["00861"]["ItemCode"] = "00861";
$default_oea["00861"]["ItemName"] = "Hong Kong Inter-school Film Competition 香港學界電影節短片比賽";
$default_oea["00861"]["CatCode"] = "4";
$default_oea["00861"]["CatName"] = "Aesthetic Development";

$default_oea["00862"]["ItemCode"] = "00862";
$default_oea["00862"]["ItemName"] = "Hong Kong Inter-school Judo Competition 香港學界柔道邀請賽";
$default_oea["00862"]["CatCode"] = "3";
$default_oea["00862"]["CatName"] = "Physical Development";

$default_oea["00863"]["ItemCode"] = "00863";
$default_oea["00863"]["ItemName"] = "Hong Kong Inter-school Theme Video Competition 全港學界專題錄像比賽";
$default_oea["00863"]["CatCode"] = "4";
$default_oea["00863"]["CatName"] = "Aesthetic Development";

$default_oea["00864"]["ItemCode"] = "00864";
$default_oea["00864"]["ItemName"] = "Hong Kong Inter-secondary School Gymnastic Competition 全港校際體育比賽 (Please provide details in \"Description\" box.)";
$default_oea["00864"]["CatCode"] = "3";
$default_oea["00864"]["CatName"] = "Physical Development";

$default_oea["00846"]["ItemCode"] = "00846";
$default_oea["00846"]["ItemName"] = "Hong Kong International Band Festival  香港國際管樂節";
$default_oea["00846"]["CatCode"] = "4";
$default_oea["00846"]["CatName"] = "Aesthetic Development";

$default_oea["00847"]["ItemCode"] = "00847";
$default_oea["00847"]["ItemName"] = "Hong Kong International Dragon and Lion Dance Competition";
$default_oea["00847"]["CatCode"] = "3";
$default_oea["00847"]["CatName"] = "Physical Development";

$default_oea["00848"]["ItemCode"] = "00848";
$default_oea["00848"]["ItemName"] = "Hong Kong International Fencing Invitation Tournament  香港國際劍擊邀請賽";
$default_oea["00848"]["CatCode"] = "3";
$default_oea["00848"]["CatName"] = "Physical Development";

$default_oea["00849"]["ItemCode"] = "00849";
$default_oea["00849"]["ItemName"] = "Hong Kong International Golden Mile  香港國際金一哩";
$default_oea["00849"]["CatCode"] = "3";
$default_oea["00849"]["CatName"] = "Physical Development";

$default_oea["00850"]["ItemCode"] = "00850";
$default_oea["00850"]["ItemName"] = "Hong Kong International Handball Championships  香港國際手球錦標賽";
$default_oea["00850"]["CatCode"] = "3";
$default_oea["00850"]["CatName"] = "Physical Development";

$default_oea["00851"]["ItemCode"] = "00851";
$default_oea["00851"]["ItemName"] = "Hong Kong International Judo Tournament  香港國際柔道錦標賽";
$default_oea["00851"]["CatCode"] = "3";
$default_oea["00851"]["CatName"] = "Physical Development";

$default_oea["00852"]["ItemCode"] = "00852";
$default_oea["00852"]["ItemName"] = "Hong Kong International Junior Judo Championships  香港國際青少年柔道錦標賽";
$default_oea["00852"]["CatCode"] = "3";
$default_oea["00852"]["CatName"] = "Physical Development";

$default_oea["00853"]["ItemCode"] = "00853";
$default_oea["00853"]["ItemName"] = "Hong Kong International Long Distance Finswimming Competition  香港國際長距離蹼泳邀請賽";
$default_oea["00853"]["CatCode"] = "3";
$default_oea["00853"]["CatName"] = "Physical Development";

$default_oea["00854"]["ItemCode"] = "00854";
$default_oea["00854"]["ItemName"] = "Hong Kong International Model United Nations";
$default_oea["00854"]["CatCode"] = "2";
$default_oea["00854"]["CatName"] = "Career-related Experience";

$default_oea["00855"]["ItemCode"] = "00855";
$default_oea["00855"]["ItemName"] = "Hong Kong International Open Tenpin Bowling Championships  香港國際保齡球公開賽";
$default_oea["00855"]["CatCode"] = "3";
$default_oea["00855"]["CatName"] = "Physical Development";

$default_oea["00856"]["ItemCode"] = "00856";
$default_oea["00856"]["ItemName"] = "Hong Kong International Piano Competition  香港國際鋼琴大賽";
$default_oea["00856"]["CatCode"] = "4";
$default_oea["00856"]["CatName"] = "Aesthetic Development";

$default_oea["00857"]["ItemCode"] = "00857";
$default_oea["00857"]["ItemName"] = "Hong Kong International Praying Mantis Martial Arts Convention  香港國際螳螂拳群英大會";
$default_oea["00857"]["CatCode"] = "3";
$default_oea["00857"]["CatName"] = "Physical Development";

$default_oea["00858"]["ItemCode"] = "00858";
$default_oea["00858"]["ItemName"] = "Hong Kong International Triathlon";
$default_oea["00858"]["CatCode"] = "3";
$default_oea["00858"]["CatName"] = "Physical Development";

$default_oea["00859"]["ItemCode"] = "00859";
$default_oea["00859"]["ItemName"] = "Hong Kong International Wushu Festival  香港國際武術節";
$default_oea["00859"]["CatCode"] = "3";
$default_oea["00859"]["CatName"] = "Physical Development";

$default_oea["00860"]["ItemCode"] = "00860";
$default_oea["00860"]["ItemName"] = "Hong Kong International Youth & Children's Choir Festival  香港國際青少年合唱節";
$default_oea["00860"]["CatCode"] = "4";
$default_oea["00860"]["CatName"] = "Aesthetic Development";

$default_oea["00865"]["ItemCode"] = "00865";
$default_oea["00865"]["ItemName"] = "Hong Kong ITU Premium Triathlon Asian Cup  香港ITU三項鐵人賽亞洲杯";
$default_oea["00865"]["CatCode"] = "3";
$default_oea["00865"]["CatName"] = "Physical Development";

$default_oea["00866"]["ItemCode"] = "00866";
$default_oea["00866"]["ItemName"] = "Hong Kong ITU Triathlon Asian Cup";
$default_oea["00866"]["CatCode"] = "3";
$default_oea["00866"]["CatName"] = "Physical Development";

$default_oea["00867"]["ItemCode"] = "00867";
$default_oea["00867"]["ItemName"] = "Hong Kong Jockey Club Award Scheme for Student Leaders of Hong Kong: National Education Course - Education Bureau  香港賽馬會香港領袖生獎勵計劃：國情教育課程";
$default_oea["00867"]["CatCode"] = "5";
$default_oea["00867"]["CatName"] = "Moral and Civic Education";

$default_oea["00868"]["ItemCode"] = "00868";
$default_oea["00868"]["ItemName"] = "Hong Kong Jockey Club Youth Science & Technology Invention Competition - Hong Kong Generation Cultural Association";
$default_oea["00868"]["CatCode"] = "2";
$default_oea["00868"]["CatName"] = "Career-related Experience";

$default_oea["00869"]["ItemCode"] = "00869";
$default_oea["00869"]["ItemName"] = "Hong Kong Joint School Economics Quiz";
$default_oea["00869"]["CatCode"] = "2";
$default_oea["00869"]["CatName"] = "Career-related Experience";

$default_oea["00870"]["ItemCode"] = "00870";
$default_oea["00870"]["ItemName"] = "Hong Kong Judo Champion Tournament (The Judo Association of Hong Kong)  香港柔道冠軍賽(中國香港柔道總會)";
$default_oea["00870"]["CatCode"] = "3";
$default_oea["00870"]["CatName"] = "Physical Development";

$default_oea["00871"]["ItemCode"] = "00871";
$default_oea["00871"]["ItemName"] = "Hong Kong Junior Age Group Athletic Meet  香港青少年分齡田徑賽";
$default_oea["00871"]["CatCode"] = "3";
$default_oea["00871"]["CatName"] = "Physical Development";

$default_oea["00872"]["ItemCode"] = "00872";
$default_oea["00872"]["ItemName"] = "Hong Kong Junior Badminton Squad";
$default_oea["00872"]["CatCode"] = "3";
$default_oea["00872"]["CatName"] = "Physical Development";

$default_oea["00873"]["ItemCode"] = "00873";
$default_oea["00873"]["ItemName"] = "Hong Kong Junior Elite Athletic Championships  香港青少年精英田徑錦標賽";
$default_oea["00873"]["CatCode"] = "3";
$default_oea["00873"]["CatName"] = "Physical Development";

$default_oea["00875"]["ItemCode"] = "00875";
$default_oea["00875"]["ItemName"] = "Hong Kong Junior Judo Championships 香港青少年柔道錦標賽 (Please provide details in \"Description\" box)";
$default_oea["00875"]["CatCode"] = "3";
$default_oea["00875"]["CatName"] = "Physical Development";

$default_oea["00876"]["ItemCode"] = "00876";
$default_oea["00876"]["ItemName"] = "Hong Kong Junior Judo Team Championships 香港青少年柔道隊際錦標賽";
$default_oea["00876"]["CatCode"] = "3";
$default_oea["00876"]["CatName"] = "Physical Development";

$default_oea["00877"]["ItemCode"] = "00877";
$default_oea["00877"]["ItemName"] = "Hong Kong Junior Squash Championships";
$default_oea["00877"]["CatCode"] = "3";
$default_oea["00877"]["CatName"] = "Physical Development";

$default_oea["00878"]["ItemCode"] = "00878";
$default_oea["00878"]["ItemName"] = "Hong Kong Junior Squash Open  香港青少年壁球公開賽";
$default_oea["00878"]["CatCode"] = "3";
$default_oea["00878"]["CatName"] = "Physical Development";

$default_oea["00879"]["ItemCode"] = "00879";
$default_oea["00879"]["ItemName"] = "Hong Kong Karate-Do Union";
$default_oea["00879"]["CatCode"] = "3";
$default_oea["00879"]["CatName"] = "Physical Development";

$default_oea["00880"]["ItemCode"] = "00880";
$default_oea["00880"]["ItemName"] = "Hong Kong Kung Fu Festival - World Kung Fu Event  香港功夫節-世界武林大會";
$default_oea["00880"]["CatCode"] = "3";
$default_oea["00880"]["CatName"] = "Physical Development";

$default_oea["00882"]["ItemCode"] = "00882";
$default_oea["00882"]["ItemName"] = "Hong Kong little Executives";
$default_oea["00882"]["CatCode"] = "2";
$default_oea["00882"]["CatName"] = "Career-related Experience";

$default_oea["00881"]["ItemCode"] = "00881";
$default_oea["00881"]["ItemName"] = "Hong Kong Ladies Taekwondo Competitions  全港女子跆拳道比賽";
$default_oea["00881"]["CatCode"] = "3";
$default_oea["00881"]["CatName"] = "Physical Development";

$default_oea["00883"]["ItemCode"] = "00883";
$default_oea["00883"]["ItemName"] = "Hong Kong Lutheran Social Service Bookmark - Basic Laws香港路德會社會服務處路德會長青群康中心全面看法—書籤設計比賽";
$default_oea["00883"]["CatCode"] = "4";
$default_oea["00883"]["CatName"] = "Aesthetic Development";

$default_oea["00884"]["ItemCode"] = "00884";
$default_oea["00884"]["ItemName"] = "Hong Kong Macau Inter-port Cup  港澳手球埠際賽";
$default_oea["00884"]["CatCode"] = "3";
$default_oea["00884"]["CatName"] = "Physical Development";

$default_oea["00885"]["ItemCode"] = "00885";
$default_oea["00885"]["ItemName"] = "Hong Kong Macau Interport Indoor Cycling Championships";
$default_oea["00885"]["CatCode"] = "3";
$default_oea["00885"]["CatName"] = "Physical Development";

$default_oea["00886"]["ItemCode"] = "00886";
$default_oea["00886"]["ItemName"] = "Hong Kong Macau Youth Networking Skills Competition  港澳青少年網絡技能大賽";
$default_oea["00886"]["CatCode"] = "2";
$default_oea["00886"]["CatName"] = "Career-related Experience";

$default_oea["00887"]["ItemCode"] = "00887";
$default_oea["00887"]["ItemName"] = "Hong Kong Mathematical High Achievers Selection Contest  香港青少年數學精英選拔賽";
$default_oea["00887"]["CatCode"] = "2";
$default_oea["00887"]["CatName"] = "Career-related Experience";

$default_oea["00888"]["ItemCode"] = "00888";
$default_oea["00888"]["ItemName"] = "Hong Kong Mini-fiction Writing Competition (Senior Secondary Section) - QualiEd Educational Organization Ltd & Hong Kong Novelist Association";
$default_oea["00888"]["CatCode"] = "4";
$default_oea["00888"]["CatName"] = "Aesthetic Development";

$default_oea["00889"]["ItemCode"] = "00889";
$default_oea["00889"]["ItemName"] = "Hong Kong Model United Nations";
$default_oea["00889"]["CatCode"] = "2";
$default_oea["00889"]["CatName"] = "Career-related Experience";

$default_oea["00890"]["ItemCode"] = "00890";
$default_oea["00890"]["ItemName"] = "Hong Kong National Duathlon Championship Male Elite Junior Champion";
$default_oea["00890"]["CatCode"] = "3";
$default_oea["00890"]["CatName"] = "Physical Development";

$default_oea["00891"]["ItemCode"] = "00891";
$default_oea["00891"]["ItemName"] = "Hong Kong National Tennis Championship  香港網球錦標賽";
$default_oea["00891"]["CatCode"] = "3";
$default_oea["00891"]["CatName"] = "Physical Development";

$default_oea["00892"]["ItemCode"] = "00892";
$default_oea["00892"]["ItemName"] = "Hong Kong Open Athletics Meet 香港田徑公開賽";
$default_oea["00892"]["CatCode"] = "3";
$default_oea["00892"]["CatName"] = "Physical Development";

$default_oea["00893"]["ItemCode"] = "00893";
$default_oea["00893"]["ItemName"] = "Hong Kong Open International Dance Sport Championships  香港國際體育舞蹈公開賽";
$default_oea["00893"]["CatCode"] = "4";
$default_oea["00893"]["CatName"] = "Aesthetic Development";

$default_oea["00894"]["ItemCode"] = "00894";
$default_oea["00894"]["ItemName"] = "Hong Kong Open Junior Taekwondo Championships  香港公開青少年跆拳道錦標賽";
$default_oea["00894"]["CatCode"] = "3";
$default_oea["00894"]["CatName"] = "Physical Development";

$default_oea["00895"]["ItemCode"] = "00895";
$default_oea["00895"]["ItemName"] = "Hong Kong Open Windsurfing Championships  香港滑浪風帆公開錦標賽";
$default_oea["00895"]["CatCode"] = "3";
$default_oea["00895"]["CatName"] = "Physical Development";

$default_oea["00896"]["ItemCode"] = "00896";
$default_oea["00896"]["ItemName"] = "Hong Kong Outstanding Students Awards";
$default_oea["00896"]["CatCode"] = "2";
$default_oea["00896"]["CatName"] = "Career-related Experience";

$default_oea["00897"]["ItemCode"] = "00897";
$default_oea["00897"]["ItemName"] = "Hong Kong Paper Airplane Contest";
$default_oea["00897"]["CatCode"] = "4";
$default_oea["00897"]["CatName"] = "Aesthetic Development";

$default_oea["00898"]["ItemCode"] = "00898";
$default_oea["00898"]["ItemName"] = "Hong Kong Philharmonic Orchestra Student Concert香港管弦樂團學生音樂會";
$default_oea["00898"]["CatCode"] = "4";
$default_oea["00898"]["CatName"] = "Aesthetic Development";

$default_oea["00899"]["ItemCode"] = "00899";
$default_oea["00899"]["ItemName"] = "Hong Kong Piano Open Competition  香港鋼琴公開比賽";
$default_oea["00899"]["CatCode"] = "4";
$default_oea["00899"]["CatName"] = "Aesthetic Development";

$default_oea["00900"]["ItemCode"] = "00900";
$default_oea["00900"]["ItemName"] = "Hong Kong Playground Association - 香港遊樂場協會 - 莫等閑義工團";
$default_oea["00900"]["CatCode"] = "1";
$default_oea["00900"]["CatName"] = "Community Service";

$default_oea["00901"]["ItemCode"] = "00901";
$default_oea["00901"]["ItemName"] = "Hong Kong Primary Mathematics Contest  香港小學數學精英選拔賽";
$default_oea["00901"]["CatCode"] = "2";
$default_oea["00901"]["CatName"] = "Career-related Experience";

$default_oea["00902"]["ItemCode"] = "00902";
$default_oea["00902"]["ItemName"] = "Hong Kong Problem Solving Olympiad  香港解難奧林匹克 (Please provide details in \"Description\" box.)";
$default_oea["00902"]["CatCode"] = "2";
$default_oea["00902"]["CatName"] = "Career-related Experience";

$default_oea["00903"]["ItemCode"] = "00903";
$default_oea["00903"]["ItemName"] = "Hong Kong Public Policy Innovation Award : Strategies for Sustainable Development in Hong Kong - Hong Kong Federation of Youth Groups  香港公共政策創意獎勵計劃 :青年獻策：凝聚意念‧持續發展";
$default_oea["00903"]["CatCode"] = "5";
$default_oea["00903"]["CatName"] = "Moral and Civic Education";

$default_oea["00904"]["ItemCode"] = "00904";
$default_oea["00904"]["ItemName"] = "Hong Kong Public Policy Innovation Award 香港公共政策創意獎勵計劃";
$default_oea["00904"]["CatCode"] = "2";
$default_oea["00904"]["CatName"] = "Career-related Experience";

$default_oea["00905"]["ItemCode"] = "00905";
$default_oea["00905"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Asia Pacific Red Cross Youth Network  亞太區紅十字青年網絡";
$default_oea["00905"]["CatCode"] = "1";
$default_oea["00905"]["CatName"] = "Community Service";

$default_oea["00906"]["ItemCode"] = "00906";
$default_oea["00906"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Best Service Project Competition";
$default_oea["00906"]["CatCode"] = "1";
$default_oea["00906"]["CatName"] = "Community Service";

$default_oea["00907"]["ItemCode"] = "00907";
$default_oea["00907"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - East Kowloon District Red Cross Youth of the Year  東九龍區傑出紅十字青年會員";
$default_oea["00907"]["CatCode"] = "1";
$default_oea["00907"]["CatName"] = "Community Service";

$default_oea["00908"]["ItemCode"] = "00908";
$default_oea["00908"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - East New Territories District Red Cross Youth of the Year  新界東區傑出紅十字青年會員";
$default_oea["00908"]["CatCode"] = "1";
$default_oea["00908"]["CatName"] = "Community Service";

$default_oea["00909"]["ItemCode"] = "00909";
$default_oea["00909"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Global Red Cross Youth Network  世界紅十字青年網絡";
$default_oea["00909"]["CatCode"] = "1";
$default_oea["00909"]["CatName"] = "Community Service";

$default_oea["00910"]["ItemCode"] = "00910";
$default_oea["00910"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Hong Kong Island District Red Cross Youth of the Year  港島區傑出紅十字青年會員";
$default_oea["00910"]["CatCode"] = "1";
$default_oea["00910"]["CatName"] = "Community Service";

$default_oea["00911"]["ItemCode"] = "00911";
$default_oea["00911"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Overseas Exchange Activity  紅十字海外交流活動 (國際)";
$default_oea["00911"]["CatCode"] = "1";
$default_oea["00911"]["CatName"] = "Community Service";

$default_oea["00912"]["ItemCode"] = "00912";
$default_oea["00912"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Red Cross Youth Unit紅十字會青年團";
$default_oea["00912"]["CatCode"] = "1";
$default_oea["00912"]["CatName"] = "Community Service";

$default_oea["00913"]["ItemCode"] = "00913";
$default_oea["00913"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Red Cross-Bronze Medal in the Voluntary Service Award";
$default_oea["00913"]["CatCode"] = "1";
$default_oea["00913"]["CatName"] = "Community Service";

$default_oea["00914"]["ItemCode"] = "00914";
$default_oea["00914"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Red Cross紅十字會 (Please provide details in \"Description\" box.)";
$default_oea["00914"]["CatCode"] = "1";
$default_oea["00914"]["CatName"] = "Community Service";

$default_oea["00915"]["ItemCode"] = "00915";
$default_oea["00915"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Regular CS (continuous at least one year)  紅十字定期社區服務(持續一年以上)";
$default_oea["00915"]["CatCode"] = "1";
$default_oea["00915"]["CatName"] = "Community Service";

$default_oea["00916"]["ItemCode"] = "00916";
$default_oea["00916"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Sham Shui Po Drill Competition";
$default_oea["00916"]["CatCode"] = "1";
$default_oea["00916"]["CatName"] = "Community Service";

$default_oea["00917"]["ItemCode"] = "00917";
$default_oea["00917"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Territory-wide Red Cross Youth of the Year  全港傑出紅十字青年會員";
$default_oea["00917"]["CatCode"] = "1";
$default_oea["00917"]["CatName"] = "Community Service";

$default_oea["00918"]["ItemCode"] = "00918";
$default_oea["00918"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - West Kowloon District (SSP) Best Unit Award";
$default_oea["00918"]["CatCode"] = "1";
$default_oea["00918"]["CatName"] = "Community Service";

$default_oea["00919"]["ItemCode"] = "00919";
$default_oea["00919"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - West Kowloon District First Aid Competition";
$default_oea["00919"]["CatCode"] = "1";
$default_oea["00919"]["CatName"] = "Community Service";

$default_oea["00920"]["ItemCode"] = "00920";
$default_oea["00920"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - West Kowloon District Red Cross Youth of the Year  西九龍區傑出紅十字青年會員";
$default_oea["00920"]["CatCode"] = "1";
$default_oea["00920"]["CatName"] = "Community Service";

$default_oea["00921"]["ItemCode"] = "00921";
$default_oea["00921"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - West New Territories District Red Cross Youth of the Year  新界西區傑出紅十字青年會員";
$default_oea["00921"]["CatCode"] = "1";
$default_oea["00921"]["CatName"] = "Community Service";

$default_oea["00922"]["ItemCode"] = "00922";
$default_oea["00922"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Advancement Badge  青年深造章";
$default_oea["00922"]["CatCode"] = "1";
$default_oea["00922"]["CatName"] = "Community Service";

$default_oea["00923"]["ItemCode"] = "00923";
$default_oea["00923"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Attainment Badge  青年榮譽章";
$default_oea["00923"]["CatCode"] = "1";
$default_oea["00923"]["CatName"] = "Community Service";

$default_oea["00924"]["ItemCode"] = "00924";
$default_oea["00924"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Best Service Theme Project Competition (Districts)  紅十字青年最佳服務主題計劃比賽";
$default_oea["00924"]["CatCode"] = "1";
$default_oea["00924"]["CatName"] = "Community Service";

$default_oea["00925"]["ItemCode"] = "00925";
$default_oea["00925"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Best Service Theme Project Competition (Territory-wide)  紅十字青年最佳服務主題計劃比賽(全港)";
$default_oea["00925"]["CatCode"] = "1";
$default_oea["00925"]["CatName"] = "Community Service";

$default_oea["00926"]["ItemCode"] = "00926";
$default_oea["00926"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Bronze Instructor Service Award  紅十字青年導師服務銅獎";
$default_oea["00926"]["CatCode"] = "1";
$default_oea["00926"]["CatName"] = "Community Service";

$default_oea["00927"]["ItemCode"] = "00927";
$default_oea["00927"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Bronze Service Award  紅十字青年服務銅獎";
$default_oea["00927"]["CatCode"] = "1";
$default_oea["00927"]["CatName"] = "Community Service";

$default_oea["00928"]["ItemCode"] = "00928";
$default_oea["00928"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth First Aid Competition (Districts)  紅十字青年急救比賽(分區)";
$default_oea["00928"]["CatCode"] = "1";
$default_oea["00928"]["CatName"] = "Community Service";

$default_oea["00929"]["ItemCode"] = "00929";
$default_oea["00929"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth First Aid Competition (Territory-wide)  紅十字青年急救比賽(全港)";
$default_oea["00929"]["CatCode"] = "1";
$default_oea["00929"]["CatName"] = "Community Service";

$default_oea["00930"]["ItemCode"] = "00930";
$default_oea["00930"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Gold Instructor Service Award  紅十字青年導師服務金獎";
$default_oea["00930"]["CatCode"] = "1";
$default_oea["00930"]["CatName"] = "Community Service";

$default_oea["00931"]["ItemCode"] = "00931";
$default_oea["00931"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Gold Service Award  紅十字青年服務金獎";
$default_oea["00931"]["CatCode"] = "1";
$default_oea["00931"]["CatName"] = "Community Service";

$default_oea["00932"]["ItemCode"] = "00932";
$default_oea["00932"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth International Friendship Album Design Competition (Districts)  紅十字青年國際交誼紀念品設計比賽(分區)";
$default_oea["00932"]["CatCode"] = "4";
$default_oea["00932"]["CatName"] = "Aesthetic Development";

$default_oea["00933"]["ItemCode"] = "00933";
$default_oea["00933"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth International Friendship Album Design Competition (Territory-wide)  紅十字青年國際交誼紀念品設計比賽(全港)";
$default_oea["00933"]["CatCode"] = "4";
$default_oea["00933"]["CatName"] = "Aesthetic Development";

$default_oea["00934"]["ItemCode"] = "00934";
$default_oea["00934"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Leadership Badge  青年領袖章";
$default_oea["00934"]["CatCode"] = "1";
$default_oea["00934"]["CatName"] = "Community Service";

$default_oea["00935"]["ItemCode"] = "00935";
$default_oea["00935"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Nursing Competition (Districts)   紅十字青年護理比賽(分區)";
$default_oea["00935"]["CatCode"] = "1";
$default_oea["00935"]["CatName"] = "Community Service";

$default_oea["00936"]["ItemCode"] = "00936";
$default_oea["00936"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Nursing Competition (Territory-wide)  紅十字青年護理比賽(全港)";
$default_oea["00936"]["CatCode"] = "1";
$default_oea["00936"]["CatName"] = "Community Service";

$default_oea["00937"]["ItemCode"] = "00937";
$default_oea["00937"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth of the Year";
$default_oea["00937"]["CatCode"] = "1";
$default_oea["00937"]["CatName"] = "Community Service";

$default_oea["00938"]["ItemCode"] = "00938";
$default_oea["00938"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Silver Instructor Service Award  紅十字青年導師服務銀獎";
$default_oea["00938"]["CatCode"] = "1";
$default_oea["00938"]["CatName"] = "Community Service";

$default_oea["00939"]["ItemCode"] = "00939";
$default_oea["00939"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Silver Service Award  紅十字青年服務銀獎";
$default_oea["00939"]["CatCode"] = "1";
$default_oea["00939"]["CatName"] = "Community Service";

$default_oea["00940"]["ItemCode"] = "00940";
$default_oea["00940"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Unit Activity  紅十字青年團活動籌備委員會";
$default_oea["00940"]["CatCode"] = "2";
$default_oea["00940"]["CatName"] = "Career-related Experience";

$default_oea["00941"]["ItemCode"] = "00941";
$default_oea["00941"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Unit Head Section Leader  紅十字青年團總隊長";
$default_oea["00941"]["CatCode"] = "2";
$default_oea["00941"]["CatName"] = "Career-related Experience";

$default_oea["00942"]["ItemCode"] = "00942";
$default_oea["00942"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Unit Section Leader  紅十字青年團隊長";
$default_oea["00942"]["CatCode"] = "2";
$default_oea["00942"]["CatName"] = "Career-related Experience";

$default_oea["00943"]["ItemCode"] = "00943";
$default_oea["00943"]["ItemName"] = "Hong Kong Red Cross 香港紅十字會 - Youth Unit Senior Section Leader  紅十字青年團高級隊長";
$default_oea["00943"]["CatCode"] = "2";
$default_oea["00943"]["CatName"] = "Career-related Experience";

$default_oea["00944"]["ItemCode"] = "00944";
$default_oea["00944"]["ItemName"] = "Hong Kong Red Cross香港紅十字會 (Please provide details in the \"Description\"box.)";
$default_oea["00944"]["CatCode"] = "1";
$default_oea["00944"]["CatName"] = "Community Service";

$default_oea["00945"]["ItemCode"] = "00945";
$default_oea["00945"]["ItemName"] = "Hong Kong Road Safety Patrol Commissioner’s Award - Hong Kong Road Safety Patrol";
$default_oea["00945"]["CatCode"] = "1";
$default_oea["00945"]["CatName"] = "Community Service";

$default_oea["00946"]["ItemCode"] = "00946";
$default_oea["00946"]["ItemName"] = "Hong Kong Road Safety Patrol Scholarship - Hong Kong Road Safety Patrol";
$default_oea["00946"]["CatCode"] = "1";
$default_oea["00946"]["CatName"] = "Community Service";

$default_oea["00947"]["ItemCode"] = "00947";
$default_oea["00947"]["ItemName"] = "Hong Kong Robotic Olympiad (modeling) Competition 香港機械奧運會比賽";
$default_oea["00947"]["CatCode"] = "2";
$default_oea["00947"]["CatName"] = "Career-related Experience";

$default_oea["00948"]["ItemCode"] = "00948";
$default_oea["00948"]["ItemName"] = "Hong Kong Rugby Football Union Schools Rugby League";
$default_oea["00948"]["CatCode"] = "3";
$default_oea["00948"]["CatName"] = "Physical Development";

$default_oea["00949"]["ItemCode"] = "00949";
$default_oea["00949"]["ItemName"] = "Hong Kong Safety Mark Comic Design Competition香港安全認證漫畫設計比賽";
$default_oea["00949"]["CatCode"] = "4";
$default_oea["00949"]["CatName"] = "Aesthetic Development";

$default_oea["00950"]["ItemCode"] = "00950";
$default_oea["00950"]["ItemName"] = "Hong Kong SAR Roller Skating Invitation Championships  香港特別行政區滾軸溜冰邀請賽";
$default_oea["00950"]["CatCode"] = "3";
$default_oea["00950"]["CatName"] = "Physical Development";

$default_oea["00951"]["ItemCode"] = "00951";
$default_oea["00951"]["ItemName"] = "Hong Kong School Drama Festival  香港學校戲劇節 (Please provide details in \"Description\" box.)";
$default_oea["00951"]["CatCode"] = "4";
$default_oea["00951"]["CatName"] = "Aesthetic Development";

$default_oea["00952"]["ItemCode"] = "00952";
$default_oea["00952"]["ItemName"] = "Hong Kong Schools Dance Festival  香港學校舞蹈節 (Please provide details in \"Description\" box.)";
$default_oea["00952"]["CatCode"] = "4";
$default_oea["00952"]["CatName"] = "Aesthetic Development";

$default_oea["00953"]["ItemCode"] = "00953";
$default_oea["00953"]["ItemName"] = "Hong Kong Schools Music Festival  香港學校音樂節 (Please provide details in \"Description\" box.) ";
$default_oea["00953"]["CatCode"] = "4";
$default_oea["00953"]["CatName"] = "Aesthetic Development";

$default_oea["00954"]["ItemCode"] = "00954";
$default_oea["00954"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Bible Speaking";
$default_oea["00954"]["CatCode"] = "4";
$default_oea["00954"]["CatName"] = "Aesthetic Development";

$default_oea["00955"]["ItemCode"] = "00955";
$default_oea["00955"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Choral Speaking - Non-Open";
$default_oea["00955"]["CatCode"] = "4";
$default_oea["00955"]["CatName"] = "Aesthetic Development";

$default_oea["00956"]["ItemCode"] = "00956";
$default_oea["00956"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Dramatic Duologue";
$default_oea["00956"]["CatCode"] = "4";
$default_oea["00956"]["CatName"] = "Aesthetic Development";

$default_oea["00957"]["ItemCode"] = "00957";
$default_oea["00957"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Dramatic Scenes";
$default_oea["00957"]["CatCode"] = "4";
$default_oea["00957"]["CatName"] = "Aesthetic Development";

$default_oea["00958"]["ItemCode"] = "00958";
$default_oea["00958"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Harmonic Choral Speaking - Open";
$default_oea["00958"]["CatCode"] = "4";
$default_oea["00958"]["CatName"] = "Aesthetic Development";

$default_oea["00959"]["ItemCode"] = "00959";
$default_oea["00959"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Improvised Dramatic Scenes";
$default_oea["00959"]["CatCode"] = "4";
$default_oea["00959"]["CatName"] = "Aesthetic Development";

$default_oea["00960"]["ItemCode"] = "00960";
$default_oea["00960"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Others (Please provide details in \"Description\" box.)  ";
$default_oea["00960"]["CatCode"] = "4";
$default_oea["00960"]["CatName"] = "Aesthetic Development";

$default_oea["00961"]["ItemCode"] = "00961";
$default_oea["00961"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Public Speaking Solo";
$default_oea["00961"]["CatCode"] = "4";
$default_oea["00961"]["CatName"] = "Aesthetic Development";

$default_oea["00962"]["ItemCode"] = "00962";
$default_oea["00962"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Public Speaking Team";
$default_oea["00962"]["CatCode"] = "4";
$default_oea["00962"]["CatName"] = "Aesthetic Development";

$default_oea["00963"]["ItemCode"] = "00963";
$default_oea["00963"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Rehearsed Original Scene";
$default_oea["00963"]["CatCode"] = "4";
$default_oea["00963"]["CatName"] = "Aesthetic Development";

$default_oea["00964"]["ItemCode"] = "00964";
$default_oea["00964"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Shakespeare Monologue";
$default_oea["00964"]["CatCode"] = "4";
$default_oea["00964"]["CatName"] = "Aesthetic Development";

$default_oea["00965"]["ItemCode"] = "00965";
$default_oea["00965"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Solo Dramatic Performance";
$default_oea["00965"]["CatCode"] = "4";
$default_oea["00965"]["CatName"] = "Aesthetic Development";

$default_oea["00966"]["ItemCode"] = "00966";
$default_oea["00966"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Solo Prose Reading - Non-Open";
$default_oea["00966"]["CatCode"] = "4";
$default_oea["00966"]["CatName"] = "Aesthetic Development";

$default_oea["00967"]["ItemCode"] = "00967";
$default_oea["00967"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Solo Prose Speaking - Open";
$default_oea["00967"]["CatCode"] = "4";
$default_oea["00967"]["CatName"] = "Aesthetic Development";

$default_oea["00968"]["ItemCode"] = "00968";
$default_oea["00968"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Solo Verse Speaking";
$default_oea["00968"]["CatCode"] = "4";
$default_oea["00968"]["CatName"] = "Aesthetic Development";

$default_oea["00969"]["ItemCode"] = "00969";
$default_oea["00969"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Thematic Group Speaking";
$default_oea["00969"]["CatCode"] = "4";
$default_oea["00969"]["CatName"] = "Aesthetic Development";

$default_oea["00970"]["ItemCode"] = "00970";
$default_oea["00970"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - Words and Movements";
$default_oea["00970"]["CatCode"] = "4";
$default_oea["00970"]["CatName"] = "Aesthetic Development";

$default_oea["00971"]["ItemCode"] = "00971";
$default_oea["00971"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 二人朗誦";
$default_oea["00971"]["CatCode"] = "4";
$default_oea["00971"]["CatName"] = "Aesthetic Development";

$default_oea["00973"]["ItemCode"] = "00973";
$default_oea["00973"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 宗教作品朗誦";
$default_oea["00973"]["CatCode"] = "4";
$default_oea["00973"]["CatName"] = "Aesthetic Development";

$default_oea["00972"]["ItemCode"] = "00972";
$default_oea["00972"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 幼兒朗誦";
$default_oea["00972"]["CatCode"] = "4";
$default_oea["00972"]["CatName"] = "Aesthetic Development";

$default_oea["00976"]["ItemCode"] = "00976";
$default_oea["00976"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 散文獨誦";
$default_oea["00976"]["CatCode"] = "4";
$default_oea["00976"]["CatName"] = "Aesthetic Development";

$default_oea["00975"]["ItemCode"] = "00975";
$default_oea["00975"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 散文集誦";
$default_oea["00975"]["CatCode"] = "4";
$default_oea["00975"]["CatName"] = "Aesthetic Development";

$default_oea["00974"]["ItemCode"] = "00974";
$default_oea["00974"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 校園生活集誦";
$default_oea["00974"]["CatCode"] = "4";
$default_oea["00974"]["CatName"] = "Aesthetic Development";

$default_oea["00980"]["ItemCode"] = "00980";
$default_oea["00980"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 歌詞朗誦";
$default_oea["00980"]["CatCode"] = "4";
$default_oea["00980"]["CatName"] = "Aesthetic Development";

$default_oea["00977"]["ItemCode"] = "00977";
$default_oea["00977"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 詩文集誦";
$default_oea["00977"]["CatCode"] = "4";
$default_oea["00977"]["CatName"] = "Aesthetic Development";

$default_oea["00979"]["ItemCode"] = "00979";
$default_oea["00979"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 詩詞獨誦";
$default_oea["00979"]["CatCode"] = "4";
$default_oea["00979"]["CatName"] = "Aesthetic Development";

$default_oea["00978"]["ItemCode"] = "00978";
$default_oea["00978"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 詩詞集誦";
$default_oea["00978"]["CatCode"] = "4";
$default_oea["00978"]["CatName"] = "Aesthetic Development";

$default_oea["00981"]["ItemCode"] = "00981";
$default_oea["00981"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 說故事";
$default_oea["00981"]["CatCode"] = "4";
$default_oea["00981"]["CatName"] = "Aesthetic Development";

$default_oea["00982"]["ItemCode"] = "00982";
$default_oea["00982"]["ItemName"] = "Hong Kong Schools Speech Festival  香港學校朗誦節 - 韻文及散文獨誦 - 粤語";
$default_oea["00982"]["CatCode"] = "4";
$default_oea["00982"]["CatName"] = "Aesthetic Development";

$default_oea["00983"]["ItemCode"] = "00983";
$default_oea["00983"]["ItemName"] = "Hong Kong Schools Sports Federation - Cross Country Championships  香港學界體育聯會 - 校際越野賽";
$default_oea["00983"]["CatCode"] = "3";
$default_oea["00983"]["CatName"] = "Physical Development";

$default_oea["00984"]["ItemCode"] = "00984";
$default_oea["00984"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Athletics Championships";
$default_oea["00984"]["CatCode"] = "3";
$default_oea["00984"]["CatName"] = "Physical Development";

$default_oea["00985"]["ItemCode"] = "00985";
$default_oea["00985"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - BOCHK Bauhinia Bowl Award (Please provide details in \"Description\" box.)";
$default_oea["00985"]["CatCode"] = "3";
$default_oea["00985"]["CatName"] = "Physical Development";

$default_oea["00986"]["ItemCode"] = "00986";
$default_oea["00986"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - BOCHK Rising Star Award (Please provide details in \"Description\" box.)";
$default_oea["00986"]["CatCode"] = "3";
$default_oea["00986"]["CatName"] = "Physical Development";

$default_oea["00987"]["ItemCode"] = "00987";
$default_oea["00987"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Inter-Secondary Schools Sports Competitions 香港學界體育聯會中學學界體育比賽 (Please provide details in \"Description\" box.)";
$default_oea["00987"]["CatCode"] = "3";
$default_oea["00987"]["CatName"] = "Physical Development";

$default_oea["00988"]["ItemCode"] = "00988";
$default_oea["00988"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Jing Ying Tournaments (Please provide details in \"Description\" box.)";
$default_oea["00988"]["CatCode"] = "3";
$default_oea["00988"]["CatName"] = "Physical Development";

$default_oea["00989"]["ItemCode"] = "00989";
$default_oea["00989"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Jing-Ying, Interport and Overseas Competitions香港學界體育聯會精英賽、埠際賽及其他海外比賽  (Please provide details in \"Description\" box.)";
$default_oea["00989"]["CatCode"] = "3";
$default_oea["00989"]["CatName"] = "Physical Development";

$default_oea["00990"]["ItemCode"] = "00990";
$default_oea["00990"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Others (Please provide details in \"Description\" box.)";
$default_oea["00990"]["CatCode"] = "3";
$default_oea["00990"]["CatName"] = "Physical Development";

$default_oea["00991"]["ItemCode"] = "00991";
$default_oea["00991"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF) - Various Sports (Please provide details in \"Description\" box.)";
$default_oea["00991"]["CatCode"] = "3";
$default_oea["00991"]["CatName"] = "Physical Development";

$default_oea["00992"]["ItemCode"] = "00992";
$default_oea["00992"]["ItemName"] = "Hong Kong Schools Sports Federation (HKSSF)- Interport Championships (Please provide details in \"Description\" box.)";
$default_oea["00992"]["CatCode"] = "3";
$default_oea["00992"]["CatName"] = "Physical Development";

$default_oea["00993"]["ItemCode"] = "00993";
$default_oea["00993"]["ItemName"] = "Hong Kong Sea Cadet Corps香港海事青年團 (Please provide details in \"Description\" box.)";
$default_oea["00993"]["CatCode"] = "2";
$default_oea["00993"]["CatName"] = "Career-related Experience";

$default_oea["00994"]["ItemCode"] = "00994";
$default_oea["00994"]["ItemName"] = "Hong Kong Sevens  香港國際七人欖球賽";
$default_oea["00994"]["CatCode"] = "3";
$default_oea["00994"]["CatName"] = "Physical Development";

$default_oea["00995"]["ItemCode"] = "00995";
$default_oea["00995"]["ItemName"] = "Hong Kong Shenzhen Dance Exchange Programme香港深圳舞蹈交流";
$default_oea["00995"]["CatCode"] = "4";
$default_oea["00995"]["CatName"] = "Aesthetic Development";

$default_oea["00996"]["ItemCode"] = "00996";
$default_oea["00996"]["ItemName"] = "Hong Kong South-West and Islands Deanery - Mission Sunday";
$default_oea["00996"]["CatCode"] = "1";
$default_oea["00996"]["CatName"] = "Community Service";

$default_oea["00997"]["ItemCode"] = "00997";
$default_oea["00997"]["ItemName"] = "Hong Kong Speed Skating Championship香港速度滾軸溜冰公開賽";
$default_oea["00997"]["CatCode"] = "4";
$default_oea["00997"]["CatName"] = "Aesthetic Development";

$default_oea["00998"]["ItemCode"] = "00998";
$default_oea["00998"]["ItemName"] = "Hong Kong Sports Association香港學界體育協進會";
$default_oea["00998"]["CatCode"] = "3";
$default_oea["00998"]["CatName"] = "Physical Development";

$default_oea["00999"]["ItemCode"] = "00999";
$default_oea["00999"]["ItemName"] = "Hong Kong Sports Council香港學界體育理事會";
$default_oea["00999"]["CatCode"] = "3";
$default_oea["00999"]["CatName"] = "Physical Development";

$default_oea["01000"]["ItemCode"] = "01000";
$default_oea["01000"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會 - St. John Ambulance Brigade Cadet Command - Corporal Course";
$default_oea["01000"]["CatCode"] = "1";
$default_oea["01000"]["CatName"] = "Community Service";

$default_oea["01001"]["ItemCode"] = "01001";
$default_oea["01001"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會 - St. John Ambulance International First Aid Competition For Cadets";
$default_oea["01001"]["CatCode"] = "1";
$default_oea["01001"]["CatName"] = "Community Service";

$default_oea["01002"]["ItemCode"] = "01002";
$default_oea["01002"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會 - St. John Annual Inter-divisional Footdrill Competition";
$default_oea["01002"]["CatCode"] = "1";
$default_oea["01002"]["CatName"] = "Community Service";

$default_oea["01003"]["ItemCode"] = "01003";
$default_oea["01003"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會 - 聖約翰救傷隊週年大會操及制服檢閱";
$default_oea["01003"]["CatCode"] = "1";
$default_oea["01003"]["CatName"] = "Community Service";

$default_oea["01004"]["ItemCode"] = "01004";
$default_oea["01004"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會- Hong Kong St. John Ambulance Brigade Cadet Command Inter-Divisional Competition  聖約翰救傷隊少青團隊際比賽";
$default_oea["01004"]["CatCode"] = "5";
$default_oea["01004"]["CatName"] = "Moral and Civic Education";

$default_oea["01005"]["ItemCode"] = "01005";
$default_oea["01005"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會- Hong Kong St. John Ambulance Brigade Kowloon and N.T. Command Cadet Region Inter-Divisional Competition";
$default_oea["01005"]["CatCode"] = "5";
$default_oea["01005"]["CatName"] = "Moral and Civic Education";

$default_oea["01006"]["ItemCode"] = "01006";
$default_oea["01006"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會- Hong Kong St. John Ambulance Brigade香港聖約翰救傷隊見習隊";
$default_oea["01006"]["CatCode"] = "1";
$default_oea["01006"]["CatName"] = "Community Service";

$default_oea["01007"]["ItemCode"] = "01007";
$default_oea["01007"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會- St. John Ambulance Brigade Inter-Divisional Competition For Uniform Inspection Champion";
$default_oea["01007"]["CatCode"] = "1";
$default_oea["01007"]["CatName"] = "Community Service";

$default_oea["01008"]["ItemCode"] = "01008";
$default_oea["01008"]["ItemName"] = "Hong Kong St. John Ambulance 香港聖約翰救傷會- St. John Ambulance Brigade 聖約翰救傷隊";
$default_oea["01008"]["CatCode"] = "1";
$default_oea["01008"]["CatName"] = "Community Service";

$default_oea["01009"]["ItemCode"] = "01009";
$default_oea["01009"]["ItemName"] = "Hong Kong Student Ambassador Programme  香港學生大使計劃";
$default_oea["01009"]["CatCode"] = "5";
$default_oea["01009"]["CatName"] = "Moral and Civic Education";

$default_oea["01010"]["ItemCode"] = "01010";
$default_oea["01010"]["ItemName"] = "Hong Kong Student Book writing Competition";
$default_oea["01010"]["CatCode"] = "4";
$default_oea["01010"]["CatName"] = "Aesthetic Development";

$default_oea["01011"]["ItemCode"] = "01011";
$default_oea["01011"]["ItemName"] = "Hong Kong Student Exchange Programme of Shanghai";
$default_oea["01011"]["CatCode"] = "2";
$default_oea["01011"]["CatName"] = "Career-related Experience";

$default_oea["01012"]["ItemCode"] = "01012";
$default_oea["01012"]["ItemName"] = "Hong Kong Student Science Project Competition  香港學生科學比賽";
$default_oea["01012"]["CatCode"] = "2";
$default_oea["01012"]["CatName"] = "Career-related Experience";

$default_oea["01013"]["ItemCode"] = "01013";
$default_oea["01013"]["ItemName"] = "Hong Kong Summer Invitational Athletic Meet  香港夏季田徑邀請賽";
$default_oea["01013"]["CatCode"] = "3";
$default_oea["01013"]["CatName"] = "Physical Development";

$default_oea["01014"]["ItemCode"] = "01014";
$default_oea["01014"]["ItemName"] = "Hong Kong Swimming Championships  香港游泳錦標賽";
$default_oea["01014"]["CatCode"] = "3";
$default_oea["01014"]["CatName"] = "Physical Development";

$default_oea["01015"]["ItemCode"] = "01015";
$default_oea["01015"]["ItemName"] = "Hong Kong Swimming Competition Events";
$default_oea["01015"]["CatCode"] = "3";
$default_oea["01015"]["CatName"] = "Physical Development";

$default_oea["01016"]["ItemCode"] = "01016";
$default_oea["01016"]["ItemName"] = "Hong Kong Technology & Renewable Energy Events  應用可再生能源設計暨競技大賽";
$default_oea["01016"]["CatCode"] = "2";
$default_oea["01016"]["CatName"] = "Career-related Experience";

$default_oea["01017"]["ItemCode"] = "01017";
$default_oea["01017"]["ItemName"] = "Hong Kong Teenager Science Fiction Writing Competition";
$default_oea["01017"]["CatCode"] = "4";
$default_oea["01017"]["CatName"] = "Aesthetic Development";

$default_oea["01018"]["ItemCode"] = "01018";
$default_oea["01018"]["ItemName"] = "Hong Kong Trampoline Open Competition";
$default_oea["01018"]["CatCode"] = "3";
$default_oea["01018"]["CatName"] = "Physical Development";

$default_oea["01019"]["ItemCode"] = "01019";
$default_oea["01019"]["ItemName"] = "Hong Kong Tug of War Competition 全港拔河公開賽";
$default_oea["01019"]["CatCode"] = "3";
$default_oea["01019"]["CatName"] = "Physical Development";

$default_oea["01020"]["ItemCode"] = "01020";
$default_oea["01020"]["ItemName"] = "Hong Kong Underwater Robot Challenge  香港潛水機械人挑戰賽";
$default_oea["01020"]["CatCode"] = "2";
$default_oea["01020"]["CatName"] = "Career-related Experience";

$default_oea["01021"]["ItemCode"] = "01021";
$default_oea["01021"]["ItemName"] = "Hong Kong Wireless Technology Industry Association (WTIA)(香港無線科技商會) - Safe WiFi HK2010-War Sailing";
$default_oea["01021"]["CatCode"] = "3";
$default_oea["01021"]["CatName"] = "Physical Development";

$default_oea["01022"]["ItemCode"] = "01022";
$default_oea["01022"]["ItemName"] = "Hong Kong Wushu Competition全港公開新秀武術錦標賽";
$default_oea["01022"]["CatCode"] = "3";
$default_oea["01022"]["CatName"] = "Physical Development";

$default_oea["01023"]["ItemCode"] = "01023";
$default_oea["01023"]["ItemName"] = "Hong Kong Young Ambassador Scheme  香港青年大使計劃";
$default_oea["01023"]["CatCode"] = "5";
$default_oea["01023"]["CatName"] = "Moral and Civic Education";

$default_oea["01024"]["ItemCode"] = "01024";
$default_oea["01024"]["ItemName"] = "Hong Kong Young Women’s Christian Association﹝香港基督教女青年會 ﹞- 正氣起動︰第二炮";
$default_oea["01024"]["CatCode"] = "1";
$default_oea["01024"]["CatName"] = "Community Service";

$default_oea["01025"]["ItemCode"] = "01025";
$default_oea["01025"]["ItemName"] = "Hong Kong Youth Anti Gambling Fan Design Competition全港青少年防賭運動紙扇創作比賽";
$default_oea["01025"]["CatCode"] = "4";
$default_oea["01025"]["CatName"] = "Aesthetic Development";

$default_oea["01026"]["ItemCode"] = "01026";
$default_oea["01026"]["ItemName"] = "Hong Kong Youth Cultural and Arts Competitions  全港青年學藝比賽";
$default_oea["01026"]["CatCode"] = "4";
$default_oea["01026"]["CatName"] = "Aesthetic Development";

$default_oea["01027"]["ItemCode"] = "01027";
$default_oea["01027"]["ItemName"] = "Hong Kong Youth Dancing Competition - The Hong Kong Youth Cultural & Arts Competitions";
$default_oea["01027"]["CatCode"] = "3";
$default_oea["01027"]["CatName"] = "Physical Development";

$default_oea["01028"]["ItemCode"] = "01028";
$default_oea["01028"]["ItemName"] = "Hong Kong Youth Indoor Archery Interport Tournament  香港青少年室內射箭埠際錦標賽";
$default_oea["01028"]["CatCode"] = "3";
$default_oea["01028"]["CatName"] = "Physical Development";

$default_oea["01029"]["ItemCode"] = "01029";
$default_oea["01029"]["ItemName"] = "Hong Kong Youth Leadership Forum";
$default_oea["01029"]["CatCode"] = "2";
$default_oea["01029"]["CatName"] = "Career-related Experience";

$default_oea["01031"]["ItemCode"] = "01031";
$default_oea["01031"]["ItemName"] = "Hong Kong Youth Music Interflow  香港青年音樂滙演";
$default_oea["01031"]["CatCode"] = "4";
$default_oea["01031"]["CatName"] = "Aesthetic Development";

$default_oea["01030"]["ItemCode"] = "01030";
$default_oea["01030"]["ItemName"] = "Hong Kong Youth Music Interflow - Symphonic Band Contest  香港青年音樂滙演 - 管樂團比賽";
$default_oea["01030"]["CatCode"] = "4";
$default_oea["01030"]["CatName"] = "Aesthetic Development";

$default_oea["01032"]["ItemCode"] = "01032";
$default_oea["01032"]["ItemName"] = "Hong Kong Youth Music Interflows-Chinese Orchestra Contest";
$default_oea["01032"]["CatCode"] = "4";
$default_oea["01032"]["CatName"] = "Aesthetic Development";

$default_oea["01033"]["ItemCode"] = "01033";
$default_oea["01033"]["ItemName"] = "Hong Kong Youth Science and Technology Invention Competition  香港青少年科技創新大賽";
$default_oea["01033"]["CatCode"] = "2";
$default_oea["01033"]["CatName"] = "Career-related Experience";

$default_oea["01034"]["ItemCode"] = "01034";
$default_oea["01034"]["ItemName"] = "Hong Kong Youth Singing Festival  香港青少年歌唱節歌唱比賽";
$default_oea["01034"]["CatCode"] = "4";
$default_oea["01034"]["CatName"] = "Aesthetic Development";

$default_oea["01035"]["ItemCode"] = "01035";
$default_oea["01035"]["ItemName"] = "Hong Kong Youth Summit";
$default_oea["01035"]["CatCode"] = "2";
$default_oea["01035"]["CatName"] = "Career-related Experience";

$default_oea["01037"]["ItemCode"] = "01037";
$default_oea["01037"]["ItemName"] = "Hong Kong Youth Volleyball Championships";
$default_oea["01037"]["CatCode"] = "3";
$default_oea["01037"]["CatName"] = "Physical Development";

$default_oea["01038"]["ItemCode"] = "01038";
$default_oea["01038"]["ItemName"] = "Hong Kong Youth Volleyball Competition - Hong Kong Volleyball Association";
$default_oea["01038"]["CatCode"] = "3";
$default_oea["01038"]["CatName"] = "Physical Development";

$default_oea["01039"]["ItemCode"] = "01039";
$default_oea["01039"]["ItemName"] = "Hong Kong Youth Wushu Competition全港公開青年少年武術分齡賽";
$default_oea["01039"]["CatCode"] = "3";
$default_oea["01039"]["CatName"] = "Physical Development";

$default_oea["01042"]["ItemCode"] = "01042";
$default_oea["01042"]["ItemName"] = "Hong Kong-Beijing-Macau Putonghua Competition";
$default_oea["01042"]["CatCode"] = "4";
$default_oea["01042"]["CatName"] = "Aesthetic Development";

$default_oea["01043"]["ItemCode"] = "01043";
$default_oea["01043"]["ItemName"] = "Hong Kong-Guangzhou Cup Taekwondo Tournament  港穗盃跆拳道比賽";
$default_oea["01043"]["CatCode"] = "3";
$default_oea["01043"]["CatName"] = "Physical Development";

$default_oea["01044"]["ItemCode"] = "01044";
$default_oea["01044"]["ItemName"] = "Hong Kong-Guangzhou Interport (Softball)";
$default_oea["01044"]["CatCode"] = "3";
$default_oea["01044"]["CatName"] = "Physical Development";

$default_oea["01045"]["ItemCode"] = "01045";
$default_oea["01045"]["ItemName"] = "Hong Kong-Macau Students Photographic Competition  港澳學界攝影大賽";
$default_oea["01045"]["CatCode"] = "4";
$default_oea["01045"]["CatName"] = "Aesthetic Development";

$default_oea["01046"]["ItemCode"] = "01046";
$default_oea["01046"]["ItemName"] = "Hong Kong-Zhuhai-Macao Smoke-free Campus: Poster Design Competition  港珠澳無煙校園校際壁報設計比賽";
$default_oea["01046"]["CatCode"] = "4";
$default_oea["01046"]["CatName"] = "Aesthetic Development";

$default_oea["01040"]["ItemCode"] = "01040";
$default_oea["01040"]["ItemName"] = "Hong Kong, Macau and Guangzhou, Guangzhou Children's Palace Fable Writing Competition";
$default_oea["01040"]["CatCode"] = "4";
$default_oea["01040"]["CatName"] = "Aesthetic Development";

$default_oea["01041"]["ItemCode"] = "01041";
$default_oea["01041"]["ItemName"] = "Hong Kong, Macau, China Board Design Competition";
$default_oea["01041"]["CatCode"] = "4";
$default_oea["01041"]["CatName"] = "Aesthetic Development";

$default_oea["01047"]["ItemCode"] = "01047";
$default_oea["01047"]["ItemName"] = "Honours / Services awards received from voluntary services e.g. YMCA, Junior Chamber of Commerce, Education Bureau, Social Welfare Department, District Board, St. John Ambulance Brigade, Red Cross, Junior Police Call少年警訊, Community Youth Club, Civil Aid Service Cadet Corps, Hong Kong Sea Cadet Corps, etc.";
$default_oea["01047"]["CatCode"] = "1";
$default_oea["01047"]["CatName"] = "Community Service";

$default_oea["01048"]["ItemCode"] = "01048";
$default_oea["01048"]["ItemName"] = "HSBC Living Finance Workshop";
$default_oea["01048"]["CatCode"] = "2";
$default_oea["01048"]["CatName"] = "Career-related Experience";

$default_oea["01049"]["ItemCode"] = "01049";
$default_oea["01049"]["ItemName"] = "HSBC Living Finance Young Financial Planner Competition  匯豐理財小百科少年財務策劃師大賽";
$default_oea["01049"]["CatCode"] = "2";
$default_oea["01049"]["CatName"] = "Career-related Experience";

$default_oea["01050"]["ItemCode"] = "01050";
$default_oea["01050"]["ItemName"] = "Huai Guang Cup Art Competition";
$default_oea["01050"]["CatCode"] = "4";
$default_oea["01050"]["CatName"] = "Aesthetic Development";

$default_oea["01051"]["ItemCode"] = "01051";
$default_oea["01051"]["ItemName"] = "HuaXia Cup  華夏盃";
$default_oea["01051"]["CatCode"] = "4";
$default_oea["01051"]["CatName"] = "Aesthetic Development";

$default_oea["01052"]["ItemCode"] = "01052";
$default_oea["01052"]["ItemName"] = "Hugh O'Brian Youth (HOBY's) World Leadership Congress";
$default_oea["01052"]["CatCode"] = "2";
$default_oea["01052"]["CatName"] = "Career-related Experience";

$default_oea["01053"]["ItemCode"] = "01053";
$default_oea["01053"]["ItemName"] = "Humanity Experience Camp: People in War 人道體驗營: 戰火人間";
$default_oea["01053"]["CatCode"] = "5";
$default_oea["01053"]["CatName"] = "Moral and Civic Education";

$default_oea["01054"]["ItemCode"] = "01054";
$default_oea["01054"]["ItemName"] = "Hwarang Taekwondo Festival";
$default_oea["01054"]["CatCode"] = "3";
$default_oea["01054"]["CatName"] = "Physical Development";

$default_oea["01075"]["ItemCode"] = "01075";
$default_oea["01075"]["ItemName"] = "i-LEAD Youth Project";
$default_oea["01075"]["CatCode"] = "2";
$default_oea["01075"]["CatName"] = "Career-related Experience";

$default_oea["01070"]["ItemCode"] = "01070";
$default_oea["01070"]["ItemName"] = "iClone animation design";
$default_oea["01070"]["CatCode"] = "4";
$default_oea["01070"]["CatName"] = "Aesthetic Development";

$default_oea["01072"]["ItemCode"] = "01072";
$default_oea["01072"]["ItemName"] = "iCUBED.us Silent Film Fest Contest";
$default_oea["01072"]["CatCode"] = "4";
$default_oea["01072"]["CatName"] = "Aesthetic Development";

$default_oea["01055"]["ItemCode"] = "01055";
$default_oea["01055"]["ItemName"] = "I Love Math Previous Primary Contests Young Camp  我愛數學夏令營－數學／計算競賽";
$default_oea["01055"]["CatCode"] = "2";
$default_oea["01055"]["CatName"] = "Career-related Experience";

$default_oea["01299"]["ItemCode"] = "01299";
$default_oea["01299"]["ItemName"] = "I-study After School Care Programme";
$default_oea["01299"]["CatCode"] = "2";
$default_oea["01299"]["CatName"] = "Career-related Experience";

$default_oea["01300"]["ItemCode"] = "01300";
$default_oea["01300"]["ItemName"] = "I-SWEEEP International Sustainable World (Energy, Engineering, & Environment) Project Olympiad 2009";
$default_oea["01300"]["CatCode"] = "5";
$default_oea["01300"]["CatName"] = "Moral and Civic Education";

$default_oea["01056"]["ItemCode"] = "01056";
$default_oea["01056"]["ItemName"] = "I.T. Prefect";
$default_oea["01056"]["CatCode"] = "1";
$default_oea["01056"]["CatName"] = "Community Service";

$default_oea["01057"]["ItemCode"] = "01057";
$default_oea["01057"]["ItemName"] = "I.T. Week";
$default_oea["01057"]["CatCode"] = "4";
$default_oea["01057"]["CatName"] = "Aesthetic Development";

$default_oea["01058"]["ItemCode"] = "01058";
$default_oea["01058"]["ItemName"] = "IAAF Asian Youth Training Race  國際田聯亞洲青少年訓練營田徑比賽";
$default_oea["01058"]["CatCode"] = "3";
$default_oea["01058"]["CatName"] = "Physical Development";

$default_oea["01059"]["ItemCode"] = "01059";
$default_oea["01059"]["ItemName"] = "IAAF World Athletic Meet";
$default_oea["01059"]["CatCode"] = "3";
$default_oea["01059"]["CatName"] = "Physical Development";

$default_oea["01060"]["ItemCode"] = "01060";
$default_oea["01060"]["ItemName"] = "IAAF World Athletic Meet (Hong Kong)";
$default_oea["01060"]["CatCode"] = "3";
$default_oea["01060"]["CatName"] = "Physical Development";

$default_oea["01061"]["ItemCode"] = "01061";
$default_oea["01061"]["ItemName"] = "IBAF Women's Baseball World Cup  IBAF世界盃女子棒球錦標賽";
$default_oea["01061"]["CatCode"] = "3";
$default_oea["01061"]["CatName"] = "Physical Development";

$default_oea["01062"]["ItemCode"] = "01062";
$default_oea["01062"]["ItemName"] = "IBM Career Day";
$default_oea["01062"]["CatCode"] = "2";
$default_oea["01062"]["CatName"] = "Career-related Experience";

$default_oea["01063"]["ItemCode"] = "01063";
$default_oea["01063"]["ItemName"] = "IBSA World Championships and Games";
$default_oea["01063"]["CatCode"] = "3";
$default_oea["01063"]["CatName"] = "Physical Development";

$default_oea["01067"]["ItemCode"] = "01067";
$default_oea["01067"]["ItemName"] = "Ice conditions Before Painting Exhibition Young Century  冰雪情世紀青少年書畫攝影展";
$default_oea["01067"]["CatCode"] = "4";
$default_oea["01067"]["CatName"] = "Aesthetic Development";

$default_oea["01068"]["ItemCode"] = "01068";
$default_oea["01068"]["ItemName"] = "Ice Skating Institute of Asia Competition";
$default_oea["01068"]["CatCode"] = "3";
$default_oea["01068"]["CatName"] = "Physical Development";

$default_oea["01064"]["ItemCode"] = "01064";
$default_oea["01064"]["ItemName"] = "ICAC Drama Competition";
$default_oea["01064"]["CatCode"] = "5";
$default_oea["01064"]["CatName"] = "Moral and Civic Education";

$default_oea["01065"]["ItemCode"] = "01065";
$default_oea["01065"]["ItemName"] = "ICAC劇場 ICAC Interactive Play";
$default_oea["01065"]["CatCode"] = "4";
$default_oea["01065"]["CatName"] = "Aesthetic Development";

$default_oea["01066"]["ItemCode"] = "01066";
$default_oea["01066"]["ItemName"] = "ICAN training";
$default_oea["01066"]["CatCode"] = "2";
$default_oea["01066"]["CatName"] = "Career-related Experience";

$default_oea["01069"]["ItemCode"] = "01069";
$default_oea["01069"]["ItemName"] = "ICF Dragon Boat World Championships  國際獨木舟聯合會世界龍舟錦標賽";
$default_oea["01069"]["CatCode"] = "3";
$default_oea["01069"]["CatName"] = "Physical Development";

$default_oea["01071"]["ItemCode"] = "01071";
$default_oea["01071"]["ItemName"] = "ICT Study Tour to Shenzhen";
$default_oea["01071"]["CatCode"] = "2";
$default_oea["01071"]["CatName"] = "Career-related Experience";

$default_oea["01073"]["ItemCode"] = "01073";
$default_oea["01073"]["ItemName"] = "ICYAF KOREA International Child & Young Art Festival";
$default_oea["01073"]["CatCode"] = "4";
$default_oea["01073"]["CatName"] = "Aesthetic Development";

$default_oea["01074"]["ItemCode"] = "01074";
$default_oea["01074"]["ItemName"] = "IES採訪技巧工作坊";
$default_oea["01074"]["CatCode"] = "2";
$default_oea["01074"]["CatName"] = "Career-related Experience";

$default_oea["01076"]["ItemCode"] = "01076";
$default_oea["01076"]["ItemName"] = "ILS Asia Pacific Region Lifesaving Competition";
$default_oea["01076"]["CatCode"] = "3";
$default_oea["01076"]["CatName"] = "Physical Development";

$default_oea["01077"]["ItemCode"] = "01077";
$default_oea["01077"]["ItemName"] = "Imagine Cup Software Design Competition - Microsoft (Hong Kong)";
$default_oea["01077"]["CatCode"] = "2";
$default_oea["01077"]["CatName"] = "Career-related Experience";

$default_oea["01078"]["ItemCode"] = "01078";
$default_oea["01078"]["ItemName"] = "Imagine Cup Web Development Competition - Microsoft (Hong Kong)";
$default_oea["01078"]["CatCode"] = "2";
$default_oea["01078"]["CatName"] = "Career-related Experience";

$default_oea["01080"]["ItemCode"] = "01080";
$default_oea["01080"]["ItemName"] = "Improvised Drama Competition 即興話劇比賽";
$default_oea["01080"]["CatCode"] = "4";
$default_oea["01080"]["CatName"] = "Aesthetic Development";

$default_oea["01079"]["ItemCode"] = "01079";
$default_oea["01079"]["ItemName"] = "IMCO Youth World Championship";
$default_oea["01079"]["CatCode"] = "3";
$default_oea["01079"]["CatName"] = "Physical Development";

$default_oea["01084"]["ItemCode"] = "01084";
$default_oea["01084"]["ItemName"] = "Inauguration of Student Leaders";
$default_oea["01084"]["CatCode"] = "1";
$default_oea["01084"]["CatName"] = "Community Service";

$default_oea["01085"]["ItemCode"] = "01085";
$default_oea["01085"]["ItemName"] = "India International Children's Art Creation Competition  印度國際兒童藝術創作比賽";
$default_oea["01085"]["CatCode"] = "4";
$default_oea["01085"]["CatName"] = "Aesthetic Development";

$default_oea["01086"]["ItemCode"] = "01086";
$default_oea["01086"]["ItemName"] = "Individual First Aid and Home Nursing Competition (Written Test)  個人急救及家居護理比賽(筆試)";
$default_oea["01086"]["CatCode"] = "1";
$default_oea["01086"]["CatName"] = "Community Service";

$default_oea["01087"]["ItemCode"] = "01087";
$default_oea["01087"]["ItemName"] = "Indonesia Gigabyte Overclocking Contest";
$default_oea["01087"]["CatCode"] = "2";
$default_oea["01087"]["CatName"] = "Career-related Experience";

$default_oea["01088"]["ItemCode"] = "01088";
$default_oea["01088"]["ItemName"] = "Indonesia International Open Tenpin Bowling Championships  印尼國際保齡球公開賽";
$default_oea["01088"]["CatCode"] = "3";
$default_oea["01088"]["CatName"] = "Physical Development";

$default_oea["01089"]["ItemCode"] = "01089";
$default_oea["01089"]["ItemName"] = "Indoor Cycling World Championship  世界室內單車錦標賽";
$default_oea["01089"]["CatCode"] = "3";
$default_oea["01089"]["CatName"] = "Physical Development";

$default_oea["01090"]["ItemCode"] = "01090";
$default_oea["01090"]["ItemName"] = "Indoor Rowing Championships";
$default_oea["01090"]["CatCode"] = "3";
$default_oea["01090"]["CatName"] = "Physical Development";

$default_oea["01091"]["ItemCode"] = "01091";
$default_oea["01091"]["ItemName"] = "Information Technology Open Day資訊科技開放日";
$default_oea["01091"]["CatCode"] = "2";
$default_oea["01091"]["CatName"] = "Career-related Experience";

$default_oea["01092"]["ItemCode"] = "01092";
$default_oea["01092"]["ItemName"] = "InnoCarnival 2010 創新科技嘉年華2010";
$default_oea["01092"]["CatCode"] = "2";
$default_oea["01092"]["CatName"] = "Career-related Experience";

$default_oea["01093"]["ItemCode"] = "01093";
$default_oea["01093"]["ItemName"] = "Integrated Skills Competition綜合技能比賽";
$default_oea["01093"]["CatCode"] = "2";
$default_oea["01093"]["CatName"] = "Career-related Experience";

$default_oea["01095"]["ItemCode"] = "01095";
$default_oea["01095"]["ItemName"] = "Intel Cup Undergraduate Electronic Design Contest - Embedded System Design Invitational Contest  英特爾杯全國大學生電子設計競賽嵌入式系統專題邀請賽";
$default_oea["01095"]["CatCode"] = "2";
$default_oea["01095"]["CatName"] = "Career-related Experience";

$default_oea["01096"]["ItemCode"] = "01096";
$default_oea["01096"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) - Best Category Awards (Please provide details in \"Description\" box";
$default_oea["01096"]["CatCode"] = "2";
$default_oea["01096"]["CatName"] = "Career-related Experience";

$default_oea["01097"]["ItemCode"] = "01097";
$default_oea["01097"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) - Dudley R. Herschbach SIYSS Award";
$default_oea["01097"]["CatCode"] = "2";
$default_oea["01097"]["CatName"] = "Career-related Experience";

$default_oea["01098"]["ItemCode"] = "01098";
$default_oea["01098"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) - Gordon Moore Award";
$default_oea["01098"]["CatCode"] = "2";
$default_oea["01098"]["CatName"] = "Career-related Experience";

$default_oea["01099"]["ItemCode"] = "01099";
$default_oea["01099"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) - Grand Awards (Please provide details in \"Description\" box";
$default_oea["01099"]["CatCode"] = "2";
$default_oea["01099"]["CatName"] = "Career-related Experience";

$default_oea["01100"]["ItemCode"] = "01100";
$default_oea["01100"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) - Intel Foundation Young Scientist Award";
$default_oea["01100"]["CatCode"] = "2";
$default_oea["01100"]["CatName"] = "Career-related Experience";

$default_oea["01101"]["ItemCode"] = "01101";
$default_oea["01101"]["ItemName"] = "Intel International Science and Engineering Fair (ISEF) 英特爾國際科學與工程大獎賽 (Please provide details in \"Description\" box.)";
$default_oea["01101"]["CatCode"] = "2";
$default_oea["01101"]["CatName"] = "Career-related Experience";

$default_oea["01102"]["ItemCode"] = "01102";
$default_oea["01102"]["ItemName"] = "Intelligent Ironman Creativity Contest  智慧鐵人創意競賽";
$default_oea["01102"]["CatCode"] = "2";
$default_oea["01102"]["CatName"] = "Career-related Experience";

$default_oea["01105"]["ItemCode"] = "01105";
$default_oea["01105"]["ItemName"] = "Inter-Beijing and Hong Kong Secondary School Debating Competition in Putonghua";
$default_oea["01105"]["CatCode"] = "5";
$default_oea["01105"]["CatName"] = "Moral and Civic Education";

$default_oea["01109"]["ItemCode"] = "01109";
$default_oea["01109"]["ItemName"] = "Inter-city Ocean Exploration Competition cum Joint-school Robotic Olympics  中國上海海洋探索比賽暨聯校機械奧運會埠際賽";
$default_oea["01109"]["CatCode"] = "2";
$default_oea["01109"]["CatName"] = "Career-related Experience";

$default_oea["01110"]["ItemCode"] = "01110";
$default_oea["01110"]["ItemName"] = "Inter-city Robot Olympic  機械人奧運會埠際賽";
$default_oea["01110"]["CatCode"] = "2";
$default_oea["01110"]["CatName"] = "Career-related Experience";

$default_oea["01106"]["ItemCode"] = "01106";
$default_oea["01106"]["ItemName"] = "Inter-Cities Debating Competition";
$default_oea["01106"]["CatCode"] = "5";
$default_oea["01106"]["CatName"] = "Moral and Civic Education";

$default_oea["01108"]["ItemCode"] = "01108";
$default_oea["01108"]["ItemName"] = "Inter-City Judo Tournament";
$default_oea["01108"]["CatCode"] = "3";
$default_oea["01108"]["CatName"] = "Physical Development";

$default_oea["01111"]["ItemCode"] = "01111";
$default_oea["01111"]["ItemName"] = "Inter-Government Secondary School Debating Competition";
$default_oea["01111"]["CatCode"] = "2";
$default_oea["01111"]["CatName"] = "Career-related Experience";

$default_oea["01112"]["ItemCode"] = "01112";
$default_oea["01112"]["ItemName"] = "Inter-house Current Affairs Forum";
$default_oea["01112"]["CatCode"] = "5";
$default_oea["01112"]["CatName"] = "Moral and Civic Education";

$default_oea["01274"]["ItemCode"] = "01274";
$default_oea["01274"]["ItemName"] = "Inter-port (Humen, Macau, Taipei, Hong Kong) Junior Canoe Championship  虎門、香港、澳門、台灣青少年獨木舟錦標賽";
$default_oea["01274"]["CatCode"] = "3";
$default_oea["01274"]["CatName"] = "Physical Development";

$default_oea["01276"]["ItemCode"] = "01276";
$default_oea["01276"]["ItemName"] = "Inter-port Canoe Invitation Tournament  埠際獨木舟邀請賽";
$default_oea["01276"]["CatCode"] = "3";
$default_oea["01276"]["CatName"] = "Physical Development";

$default_oea["01278"]["ItemCode"] = "01278";
$default_oea["01278"]["ItemName"] = "Inter-port Schools Volleyball Championships";
$default_oea["01278"]["CatCode"] = "3";
$default_oea["01278"]["CatName"] = "Physical Development";

$default_oea["01279"]["ItemCode"] = "01279";
$default_oea["01279"]["ItemName"] = "Inter-school Bird Race 學界觀鳥比賽";
$default_oea["01279"]["CatCode"] = "4";
$default_oea["01279"]["CatName"] = "Aesthetic Development";

$default_oea["01280"]["ItemCode"] = "01280";
$default_oea["01280"]["ItemName"] = "Inter-school Bird Watching Network, World Wetland Day";
$default_oea["01280"]["CatCode"] = "4";
$default_oea["01280"]["CatName"] = "Aesthetic Development";

$default_oea["01281"]["ItemCode"] = "01281";
$default_oea["01281"]["ItemName"] = "Inter-school Drama Competition 大埔區中、小學校際戲劇比賽";
$default_oea["01281"]["CatCode"] = "4";
$default_oea["01281"]["CatName"] = "Aesthetic Development";

$default_oea["01282"]["ItemCode"] = "01282";
$default_oea["01282"]["ItemName"] = "Inter-school English Penmanship Competition";
$default_oea["01282"]["CatCode"] = "4";
$default_oea["01282"]["CatName"] = "Aesthetic Development";

$default_oea["01283"]["ItemCode"] = "01283";
$default_oea["01283"]["ItemName"] = "Inter-school Invitational Tournament 校際柔道邀請賽";
$default_oea["01283"]["CatCode"] = "3";
$default_oea["01283"]["CatName"] = "Physical Development";

$default_oea["01288"]["ItemCode"] = "01288";
$default_oea["01288"]["ItemName"] = "Inter-school/port sports events organised by HKSSF/HKSAM由香港學界體育聯會/香港弱智人士體育協會舉辦的校際或埠際運動項目";
$default_oea["01288"]["CatCode"] = "3";
$default_oea["01288"]["CatName"] = "Physical Development";

$default_oea["01284"]["ItemCode"] = "01284";
$default_oea["01284"]["ItemName"] = "Inter-School Painting Competition校際繪畫比賽";
$default_oea["01284"]["CatCode"] = "4";
$default_oea["01284"]["CatName"] = "Aesthetic Development";

$default_oea["01285"]["ItemCode"] = "01285";
$default_oea["01285"]["ItemName"] = "Inter-School Scrabble Championship";
$default_oea["01285"]["CatCode"] = "4";
$default_oea["01285"]["CatName"] = "Aesthetic Development";

$default_oea["01286"]["ItemCode"] = "01286";
$default_oea["01286"]["ItemName"] = "Inter-School Sports Competitions (Please provide details in \"Description\" box.)";
$default_oea["01286"]["CatCode"] = "3";
$default_oea["01286"]["CatName"] = "Physical Development";

$default_oea["01287"]["ItemCode"] = "01287";
$default_oea["01287"]["ItemName"] = "Inter-School World Cultures Fiesta";
$default_oea["01287"]["CatCode"] = "4";
$default_oea["01287"]["CatName"] = "Aesthetic Development";

$default_oea["01289"]["ItemCode"] = "01289";
$default_oea["01289"]["ItemName"] = "Inter-Secondary School Sports Competition - Touch Champions";
$default_oea["01289"]["CatCode"] = "3";
$default_oea["01289"]["CatName"] = "Physical Development";

$default_oea["01103"]["ItemCode"] = "01103";
$default_oea["01103"]["ItemName"] = "Interact Club扶輪少年服務團 (Please provide details in \"Description\" box.)";
$default_oea["01103"]["CatCode"] = "1";
$default_oea["01103"]["CatName"] = "Community Service";

$default_oea["01104"]["ItemCode"] = "01104";
$default_oea["01104"]["ItemName"] = "Interactive Drama by the Ming Yin Elder Academy";
$default_oea["01104"]["CatCode"] = "4";
$default_oea["01104"]["CatName"] = "Aesthetic Development";

$default_oea["01107"]["ItemCode"] = "01107";
$default_oea["01107"]["ItemName"] = "Intercity Games of China  中華人民共和國城巿運動會";
$default_oea["01107"]["CatCode"] = "3";
$default_oea["01107"]["CatName"] = "Physical Development";

$default_oea["01113"]["ItemCode"] = "01113";
$default_oea["01113"]["ItemName"] = "Internal Penmanship Competition  全港中小學中英文硬筆書法比賽(校內比賽)";
$default_oea["01113"]["CatCode"] = "4";
$default_oea["01113"]["CatName"] = "Aesthetic Development";

$default_oea["01114"]["ItemCode"] = "01114";
$default_oea["01114"]["ItemName"] = "International a cappella Extravaganza  國際無伴奏音樂盛典";
$default_oea["01114"]["CatCode"] = "4";
$default_oea["01114"]["CatName"] = "Aesthetic Development";

$default_oea["01115"]["ItemCode"] = "01115";
$default_oea["01115"]["ItemName"] = "International Age Group Swimming Invitations";
$default_oea["01115"]["CatCode"] = "3";
$default_oea["01115"]["CatName"] = "Physical Development";

$default_oea["01116"]["ItemCode"] = "01116";
$default_oea["01116"]["ItemName"] = "International Art and Design Competition  國際美術創作比賽";
$default_oea["01116"]["CatCode"] = "4";
$default_oea["01116"]["CatName"] = "Aesthetic Development";

$default_oea["01117"]["ItemCode"] = "01117";
$default_oea["01117"]["ItemName"] = "International Art Handicraft Contest";
$default_oea["01117"]["CatCode"] = "4";
$default_oea["01117"]["CatName"] = "Aesthetic Development";

$default_oea["01118"]["ItemCode"] = "01118";
$default_oea["01118"]["ItemName"] = "International Arts Carnival - Actor in Drama";
$default_oea["01118"]["CatCode"] = "4";
$default_oea["01118"]["CatName"] = "Aesthetic Development";

$default_oea["01119"]["ItemCode"] = "01119";
$default_oea["01119"]["ItemName"] = "International Arts Carnival Graphic Design Competition  國際綜藝合家歡平面設計比賽";
$default_oea["01119"]["CatCode"] = "4";
$default_oea["01119"]["CatName"] = "Aesthetic Development";

$default_oea["01120"]["ItemCode"] = "01120";
$default_oea["01120"]["ItemName"] = "International Astronomy Year Art andDesign Competition (HK)國際天文年美術設計創作比賽(香港賽)";
$default_oea["01120"]["CatCode"] = "4";
$default_oea["01120"]["CatName"] = "Aesthetic Development";

$default_oea["01121"]["ItemCode"] = "01121";
$default_oea["01121"]["ItemName"] = "International Athletic Competitions";
$default_oea["01121"]["CatCode"] = "3";
$default_oea["01121"]["CatName"] = "Physical Development";

$default_oea["01122"]["ItemCode"] = "01122";
$default_oea["01122"]["ItemName"] = "International Boccia Masters Tournament  國際硬地滾球比賽";
$default_oea["01122"]["CatCode"] = "3";
$default_oea["01122"]["CatName"] = "Physical Development";

$default_oea["01123"]["ItemCode"] = "01123";
$default_oea["01123"]["ItemName"] = "International Calligraphy Conference  日本中國國際書畫大會";
$default_oea["01123"]["CatCode"] = "4";
$default_oea["01123"]["CatName"] = "Aesthetic Development";

$default_oea["01124"]["ItemCode"] = "01124";
$default_oea["01124"]["ItemName"] = "International Child & Young Art Festival";
$default_oea["01124"]["CatCode"] = "4";
$default_oea["01124"]["CatName"] = "Aesthetic Development";

$default_oea["01125"]["ItemCode"] = "01125";
$default_oea["01125"]["ItemName"] = "International Children Music Festival";
$default_oea["01125"]["CatCode"] = "4";
$default_oea["01125"]["CatName"] = "Aesthetic Development";

$default_oea["01126"]["ItemCode"] = "01126";
$default_oea["01126"]["ItemName"] = "International Children's Art Exhibition";
$default_oea["01126"]["CatCode"] = "4";
$default_oea["01126"]["CatName"] = "Aesthetic Development";

$default_oea["01127"]["ItemCode"] = "01127";
$default_oea["01127"]["ItemName"] = "International Children's Art Exhibition Match of China Japan Korea  中日韓國際少兒藝術展示大賽";
$default_oea["01127"]["CatCode"] = "4";
$default_oea["01127"]["CatName"] = "Aesthetic Development";

$default_oea["01128"]["ItemCode"] = "01128";
$default_oea["01128"]["ItemName"] = "International Children's Art Festival, Shenzhen  中國深圳南山國際兒童文化藝術周";
$default_oea["01128"]["CatCode"] = "4";
$default_oea["01128"]["CatName"] = "Aesthetic Development";

$default_oea["01129"]["ItemCode"] = "01129";
$default_oea["01129"]["ItemName"] = "International Children's Games  國際少年運動會";
$default_oea["01129"]["CatCode"] = "3";
$default_oea["01129"]["CatName"] = "Physical Development";

$default_oea["01130"]["ItemCode"] = "01130";
$default_oea["01130"]["ItemName"] = "International Chinese Calligraphy and Painting Contest and Exhibition";
$default_oea["01130"]["CatCode"] = "4";
$default_oea["01130"]["CatName"] = "Aesthetic Development";

$default_oea["01131"]["ItemCode"] = "01131";
$default_oea["01131"]["ItemName"] = "International Chinese Martial Arts Championship";
$default_oea["01131"]["CatCode"] = "3";
$default_oea["01131"]["CatName"] = "Physical Development";

$default_oea["01132"]["ItemCode"] = "01132";
$default_oea["01132"]["ItemName"] = "International Choir Festival Isola Del Sole Grado in Italy";
$default_oea["01132"]["CatCode"] = "4";
$default_oea["01132"]["CatName"] = "Aesthetic Development";

$default_oea["01133"]["ItemCode"] = "01133";
$default_oea["01133"]["ItemName"] = "International Choral Festival";
$default_oea["01133"]["CatCode"] = "4";
$default_oea["01133"]["CatName"] = "Aesthetic Development";

$default_oea["01134"]["ItemCode"] = "01134";
$default_oea["01134"]["ItemName"] = "International Choral Kathaumixw";
$default_oea["01134"]["CatCode"] = "4";
$default_oea["01134"]["CatName"] = "Aesthetic Development";

$default_oea["01135"]["ItemCode"] = "01135";
$default_oea["01135"]["ItemName"] = "International Climate Champion of Hong Kong  氣候大使";
$default_oea["01135"]["CatCode"] = "5";
$default_oea["01135"]["CatName"] = "Moral and Civic Education";

$default_oea["01136"]["ItemCode"] = "01136";
$default_oea["01136"]["ItemName"] = "International Clubs Open Taekwondo Championship";
$default_oea["01136"]["CatCode"] = "3";
$default_oea["01136"]["CatName"] = "Physical Development";

$default_oea["01137"]["ItemCode"] = "01137";
$default_oea["01137"]["ItemName"] = "International Competition of Fine Arts Lidice國際兒童利迪斯藝術展及比賽";
$default_oea["01137"]["CatCode"] = "4";
$default_oea["01137"]["CatName"] = "Aesthetic Development";

$default_oea["01138"]["ItemCode"] = "01138";
$default_oea["01138"]["ItemName"] = "International Competitions and Assessments for Schools";
$default_oea["01138"]["CatCode"] = "4";
$default_oea["01138"]["CatName"] = "Aesthetic Development";

$default_oea["01139"]["ItemCode"] = "01139";
$default_oea["01139"]["ItemName"] = "International Competitions in Triathlon-Asian Series (Junior)";
$default_oea["01139"]["CatCode"] = "3";
$default_oea["01139"]["CatName"] = "Physical Development";

$default_oea["01140"]["ItemCode"] = "01140";
$default_oea["01140"]["ItemName"] = "International Computer Project Competition INFOMATRIX";
$default_oea["01140"]["CatCode"] = "2";
$default_oea["01140"]["CatName"] = "Career-related Experience";

$default_oea["01141"]["ItemCode"] = "01141";
$default_oea["01141"]["ItemName"] = "International Contest and Masterwork Exhibition of Chinese Painting, Calligraphy  海內外中國書畫大賽暨精品展";
$default_oea["01141"]["CatCode"] = "4";
$default_oea["01141"]["CatName"] = "Aesthetic Development";

$default_oea["01142"]["ItemCode"] = "01142";
$default_oea["01142"]["ItemName"] = "International Contest of Trade Marks and Logotypes TaMga";
$default_oea["01142"]["CatCode"] = "4";
$default_oea["01142"]["CatName"] = "Aesthetic Development";

$default_oea["01143"]["ItemCode"] = "01143";
$default_oea["01143"]["ItemName"] = "International Convention on Students' Quality Control Circles in India";
$default_oea["01143"]["CatCode"] = "2";
$default_oea["01143"]["CatName"] = "Career-related Experience";

$default_oea["01144"]["ItemCode"] = "01144";
$default_oea["01144"]["ItemName"] = "International Convergence";
$default_oea["01144"]["CatCode"] = "2";
$default_oea["01144"]["CatName"] = "Career-related Experience";

$default_oea["01145"]["ItemCode"] = "01145";
$default_oea["01145"]["ItemName"] = "International Cyberfair for Schools";
$default_oea["01145"]["CatCode"] = "2";
$default_oea["01145"]["CatName"] = "Career-related Experience";

$default_oea["01146"]["ItemCode"] = "01146";
$default_oea["01146"]["ItemName"] = "International Dance Competition";
$default_oea["01146"]["CatCode"] = "4";
$default_oea["01146"]["CatName"] = "Aesthetic Development";

$default_oea["01147"]["ItemCode"] = "01147";
$default_oea["01147"]["ItemName"] = "International Dinosaur Drawing Contest";
$default_oea["01147"]["CatCode"] = "4";
$default_oea["01147"]["CatName"] = "Aesthetic Development";

$default_oea["01148"]["ItemCode"] = "01148";
$default_oea["01148"]["ItemName"] = "International Drawing Competition";
$default_oea["01148"]["CatCode"] = "4";
$default_oea["01148"]["CatName"] = "Aesthetic Development";

$default_oea["01149"]["ItemCode"] = "01149";
$default_oea["01149"]["ItemName"] = "International ECO-City Conference";
$default_oea["01149"]["CatCode"] = "2";
$default_oea["01149"]["CatName"] = "Career-related Experience";

$default_oea["01150"]["ItemCode"] = "01150";
$default_oea["01150"]["ItemName"] = "International English Contest  國際英語大賽";
$default_oea["01150"]["CatCode"] = "4";
$default_oea["01150"]["CatName"] = "Aesthetic Development";

$default_oea["01151"]["ItemCode"] = "01151";
$default_oea["01151"]["ItemName"] = "International English Contest Hong Kong";
$default_oea["01151"]["CatCode"] = "4";
$default_oea["01151"]["CatName"] = "Aesthetic Development";

$default_oea["01152"]["ItemCode"] = "01152";
$default_oea["01152"]["ItemName"] = "International Federation of Muaythai Amateur (IFMA) World Championship";
$default_oea["01152"]["CatCode"] = "3";
$default_oea["01152"]["CatName"] = "Physical Development";

$default_oea["01153"]["ItemCode"] = "01153";
$default_oea["01153"]["ItemName"] = "International Federation of Roller Sports - World Championships";
$default_oea["01153"]["CatCode"] = "3";
$default_oea["01153"]["CatName"] = "Physical Development";

$default_oea["01154"]["ItemCode"] = "01154";
$default_oea["01154"]["ItemName"] = "International Film Festival";
$default_oea["01154"]["CatCode"] = "4";
$default_oea["01154"]["CatName"] = "Aesthetic Development";

$default_oea["01155"]["ItemCode"] = "01155";
$default_oea["01155"]["ItemName"] = "International Fine Art Aesthetic Exhibition  國際書畫審美大展賽";
$default_oea["01155"]["CatCode"] = "4";
$default_oea["01155"]["CatName"] = "Aesthetic Development";

$default_oea["01156"]["ItemCode"] = "01156";
$default_oea["01156"]["ItemName"] = "International Folk Art Festival  國際民間藝術節";
$default_oea["01156"]["CatCode"] = "4";
$default_oea["01156"]["CatName"] = "Aesthetic Development";

$default_oea["01157"]["ItemCode"] = "01157";
$default_oea["01157"]["ItemName"] = "International Frederick Chopin Piano Competition  蕭邦國際鋼琴比賽";
$default_oea["01157"]["CatCode"] = "4";
$default_oea["01157"]["CatName"] = "Aesthetic Development";

$default_oea["01158"]["ItemCode"] = "01158";
$default_oea["01158"]["ItemName"] = "International Friends of Peace Children and Youth's Painting Competition  國際和平之友兒童及青少年繪畫賽";
$default_oea["01158"]["CatCode"] = "4";
$default_oea["01158"]["CatName"] = "Aesthetic Development";

$default_oea["01159"]["ItemCode"] = "01159";
$default_oea["01159"]["ItemName"] = "International Genetically Engineered Machine Competition";
$default_oea["01159"]["CatCode"] = "2";
$default_oea["01159"]["CatName"] = "Career-related Experience";

$default_oea["01160"]["ItemCode"] = "01160";
$default_oea["01160"]["ItemName"] = "International Inline Speed Skating Championship";
$default_oea["01160"]["CatCode"] = "3";
$default_oea["01160"]["CatName"] = "Physical Development";

$default_oea["01161"]["ItemCode"] = "01161";
$default_oea["01161"]["ItemName"] = "International Invitational Youth Chess Tournament";
$default_oea["01161"]["CatCode"] = "4";
$default_oea["01161"]["CatName"] = "Aesthetic Development";

$default_oea["01162"]["ItemCode"] = "01162";
$default_oea["01162"]["ItemName"] = "International Johannes Brahms Choir Festival and Competition  布拉姆斯國際合唱節暨合唱比賽";
$default_oea["01162"]["CatCode"] = "4";
$default_oea["01162"]["CatName"] = "Aesthetic Development";

$default_oea["01163"]["ItemCode"] = "01163";
$default_oea["01163"]["ItemName"] = "International Junior Art Selection Campaign - The National Excellent Junior Artist";
$default_oea["01163"]["CatCode"] = "4";
$default_oea["01163"]["CatName"] = "Aesthetic Development";

$default_oea["01164"]["ItemCode"] = "01164";
$default_oea["01164"]["ItemName"] = "International Kung Fu Master Performance & Championship  世界功夫羣英會";
$default_oea["01164"]["CatCode"] = "3";
$default_oea["01164"]["CatName"] = "Physical Development";

$default_oea["01165"]["ItemCode"] = "01165";
$default_oea["01165"]["ItemName"] = "International Letter-Writing Competition for Young People (Hong Kong Contest)  國際青少年書信寫作比賽(香港區賽事)";
$default_oea["01165"]["CatCode"] = "4";
$default_oea["01165"]["CatName"] = "Aesthetic Development";

$default_oea["01166"]["ItemCode"] = "01166";
$default_oea["01166"]["ItemName"] = "International Lifesaver from International Life Saving Federation";
$default_oea["01166"]["CatCode"] = "3";
$default_oea["01166"]["CatName"] = "Physical Development";

$default_oea["01167"]["ItemCode"] = "01167";
$default_oea["01167"]["ItemName"] = "International Lion Dance Tournament";
$default_oea["01167"]["CatCode"] = "3";
$default_oea["01167"]["CatName"] = "Physical Development";

$default_oea["01168"]["ItemCode"] = "01168";
$default_oea["01168"]["ItemName"] = "International Lion Dance, Dragon Dance and Kung Fu Competition  國際舞獅舞龍功夫錦標賽";
$default_oea["01168"]["CatCode"] = "3";
$default_oea["01168"]["CatName"] = "Physical Development";

$default_oea["01169"]["ItemCode"] = "01169";
$default_oea["01169"]["ItemName"] = "International Lion Dance, Dragon Dance Competition";
$default_oea["01169"]["CatCode"] = "3";
$default_oea["01169"]["CatName"] = "Physical Development";

$default_oea["01170"]["ItemCode"] = "01170";
$default_oea["01170"]["ItemName"] = "International Maritime Law Arbitration Moot Competition (Asia Pacific Rim)  國際軍事法仲裁案件模擬會賽(亞太圈)";
$default_oea["01170"]["CatCode"] = "5";
$default_oea["01170"]["CatName"] = "Moral and Civic Education";

$default_oea["01171"]["ItemCode"] = "01171";
$default_oea["01171"]["ItemName"] = "International Martial Arts Games";
$default_oea["01171"]["CatCode"] = "3";
$default_oea["01171"]["CatName"] = "Physical Development";

$default_oea["01174"]["ItemCode"] = "01174";
$default_oea["01174"]["ItemName"] = "International Multi-Sport Thailand Championships (Triathlon)";
$default_oea["01174"]["CatCode"] = "3";
$default_oea["01174"]["CatName"] = "Physical Development";

$default_oea["01172"]["ItemCode"] = "01172";
$default_oea["01172"]["ItemName"] = "International Multisport ITU/ ASTC Asian Cup  亞洲盃巡迴賽";
$default_oea["01172"]["CatCode"] = "3";
$default_oea["01172"]["CatName"] = "Physical Development";

$default_oea["01173"]["ItemCode"] = "01173";
$default_oea["01173"]["ItemName"] = "International Multisport Thailand Championships  泰國國際體育錦標賽";
$default_oea["01173"]["CatCode"] = "3";
$default_oea["01173"]["CatName"] = "Physical Development";

$default_oea["01175"]["ItemCode"] = "01175";
$default_oea["01175"]["ItemName"] = "International Music Competition Jeunesses Musicales Bucharest Jeunesses Musicales Bucharest國際音樂大賽";
$default_oea["01175"]["CatCode"] = "4";
$default_oea["01175"]["CatName"] = "Aesthetic Development";

$default_oea["01176"]["ItemCode"] = "01176";
$default_oea["01176"]["ItemName"] = "International Music Festival";
$default_oea["01176"]["CatCode"] = "4";
$default_oea["01176"]["CatName"] = "Aesthetic Development";

$default_oea["01177"]["ItemCode"] = "01177";
$default_oea["01177"]["ItemName"] = "International New Year Winter Swimming Championships  國際元旦冬泳錦標賽";
$default_oea["01177"]["CatCode"] = "3";
$default_oea["01177"]["CatName"] = "Physical Development";

$default_oea["01178"]["ItemCode"] = "01178";
$default_oea["01178"]["ItemName"] = "International Olympiad in Informatics (IOI)  - Hong Kong Olympiad in Informatics (HKOI) (Please provide details in \"Description\" Box.)";
$default_oea["01178"]["CatCode"] = "2";
$default_oea["01178"]["CatName"] = "Career-related Experience";

$default_oea["01179"]["ItemCode"] = "01179";
$default_oea["01179"]["ItemName"] = "International Olympiad in Informatics (IOI) - China National Olympiad in Informatics  (NOI) 全國青少年信息學奧林匹克競賽 (Please provide details in \"Description\" Box.)";
$default_oea["01179"]["CatCode"] = "2";
$default_oea["01179"]["CatName"] = "Career-related Experience";

$default_oea["01180"]["ItemCode"] = "01180";
$default_oea["01180"]["ItemName"] = "International Olympiad in Informatics (IOI) (Please provide details in \"Description\" Box.) ";
$default_oea["01180"]["CatCode"] = "2";
$default_oea["01180"]["CatName"] = "Career-related Experience";

$default_oea["01181"]["ItemCode"] = "01181";
$default_oea["01181"]["ItemName"] = "International Open TenPin Bowling Championships  國際保齡球公開賽";
$default_oea["01181"]["CatCode"] = "3";
$default_oea["01181"]["CatName"] = "Physical Development";

$default_oea["01182"]["ItemCode"] = "01182";
$default_oea["01182"]["ItemName"] = "International Orchid Show  國際蘭花博覽會";
$default_oea["01182"]["CatCode"] = "4";
$default_oea["01182"]["CatName"] = "Aesthetic Development";

$default_oea["01183"]["ItemCode"] = "01183";
$default_oea["01183"]["ItemName"] = "International Painting Competition";
$default_oea["01183"]["CatCode"] = "4";
$default_oea["01183"]["CatName"] = "Aesthetic Development";

$default_oea["01184"]["ItemCode"] = "01184";
$default_oea["01184"]["ItemName"] = "International Painting Competition Joy of Europe";
$default_oea["01184"]["CatCode"] = "4";
$default_oea["01184"]["CatName"] = "Aesthetic Development";

$default_oea["01185"]["ItemCode"] = "01185";
$default_oea["01185"]["ItemName"] = "International Peace Pals Art Exhibition & Awards";
$default_oea["01185"]["CatCode"] = "4";
$default_oea["01185"]["CatName"] = "Aesthetic Development";

$default_oea["01186"]["ItemCode"] = "01186";
$default_oea["01186"]["ItemName"] = "International Remotely Operated Vehicle (ROV) Competition";
$default_oea["01186"]["CatCode"] = "2";
$default_oea["01186"]["CatName"] = "Career-related Experience";

$default_oea["01187"]["ItemCode"] = "01187";
$default_oea["01187"]["ItemName"] = "International Robot Olympiad  國際奧林匹克機器人運動會";
$default_oea["01187"]["CatCode"] = "2";
$default_oea["01187"]["CatName"] = "Career-related Experience";

$default_oea["01188"]["ItemCode"] = "01188";
$default_oea["01188"]["ItemName"] = "International Running Competition Mini Maratona";
$default_oea["01188"]["CatCode"] = "3";
$default_oea["01188"]["CatName"] = "Physical Development";

$default_oea["01189"]["ItemCode"] = "01189";
$default_oea["01189"]["ItemName"] = "International Science Fair (Please provide details in \"Description\"box.)";
$default_oea["01189"]["CatCode"] = "2";
$default_oea["01189"]["CatName"] = "Career-related Experience";

$default_oea["01196"]["ItemCode"] = "01196";
$default_oea["01196"]["ItemName"] = "International Science Olympiad  國際科學奧林匹克 - Others (Please provide details in \"Description\"box.)";
$default_oea["01196"]["CatCode"] = "2";
$default_oea["01196"]["CatName"] = "Career-related Experience";

$default_oea["01190"]["ItemCode"] = "01190";
$default_oea["01190"]["ItemName"] = "International Science Olympiad -  -The International Geography Olympiad- 全港校際地理奧林匹克大賽";
$default_oea["01190"]["CatCode"] = "2";
$default_oea["01190"]["CatName"] = "Career-related Experience";

$default_oea["01191"]["ItemCode"] = "01191";
$default_oea["01191"]["ItemName"] = "International Science Olympiad - International Junior Science Olympiad  國際初中科學奧林匹克";
$default_oea["01191"]["CatCode"] = "2";
$default_oea["01191"]["CatName"] = "Career-related Experience";

$default_oea["01192"]["ItemCode"] = "01192";
$default_oea["01192"]["ItemName"] = "International Science Olympiad - The International Biology Olympiad - Hong Kong Biology Olympiad for Secondary Schools";
$default_oea["01192"]["CatCode"] = "2";
$default_oea["01192"]["CatName"] = "Career-related Experience";

$default_oea["01193"]["ItemCode"] = "01193";
$default_oea["01193"]["ItemName"] = "International Science Olympiad - The International Mathematical Olympiad - Hong Kong Mathematical Olympiad  香港數學競賽";
$default_oea["01193"]["CatCode"] = "2";
$default_oea["01193"]["CatName"] = "Career-related Experience";

$default_oea["01194"]["ItemCode"] = "01194";
$default_oea["01194"]["ItemName"] = "International Science Olympiad - The International Physics Olympiad - Hong Kong Physics Olympiad  香港物理奧林匹克";
$default_oea["01194"]["CatCode"] = "2";
$default_oea["01194"]["CatName"] = "Career-related Experience";

$default_oea["01195"]["ItemCode"] = "01195";
$default_oea["01195"]["ItemName"] = "International Science Olympiad - The International Physics Olympiad Asian Physics Olympiad (Please provide details in \"Description\" box.)";
$default_oea["01195"]["CatCode"] = "5";
$default_oea["01195"]["CatName"] = "Moral and Civic Education";

$default_oea["01197"]["ItemCode"] = "01197";
$default_oea["01197"]["ItemName"] = "International Science Olympiad -International Philosophy Olympiad";
$default_oea["01197"]["CatCode"] = "2";
$default_oea["01197"]["CatName"] = "Career-related Experience";

$default_oea["01198"]["ItemCode"] = "01198";
$default_oea["01198"]["ItemName"] = "International Science Olympiad -The  International Chemistry Olympiad - Hong Kong Chemistry Olympiad";
$default_oea["01198"]["CatCode"] = "2";
$default_oea["01198"]["CatName"] = "Career-related Experience";

$default_oea["01199"]["ItemCode"] = "01199";
$default_oea["01199"]["ItemName"] = "International Science Olympiad -The  International Mathematical Olympiad - Hong Kong & Macao Mathematical Olympiad Open Contest  港澳數學奧林匹克公開賽《港澳盃》";
$default_oea["01199"]["CatCode"] = "2";
$default_oea["01199"]["CatName"] = "Career-related Experience";

$default_oea["01200"]["ItemCode"] = "01200";
$default_oea["01200"]["ItemName"] = "International Science Olympiad -The  International Mathematical Olympiad - National Primary Mathematics Olympiad  全國小學數學奧林匹克(中國數學會)";
$default_oea["01200"]["CatCode"] = "2";
$default_oea["01200"]["CatName"] = "Career-related Experience";

$default_oea["01201"]["ItemCode"] = "01201";
$default_oea["01201"]["ItemName"] = "International Science Olympiad -The  International Mathematical Olympiad - Singapore Mathematical Olympiads";
$default_oea["01201"]["CatCode"] = "2";
$default_oea["01201"]["CatName"] = "Career-related Experience";

$default_oea["01202"]["ItemCode"] = "01202";
$default_oea["01202"]["ItemName"] = "International Science Olympiad -The International Astronomy Olympiad";
$default_oea["01202"]["CatCode"] = "2";
$default_oea["01202"]["CatName"] = "Career-related Experience";

$default_oea["01203"]["ItemCode"] = "01203";
$default_oea["01203"]["ItemName"] = "International Science Olympiad -The International Biology Olympiad";
$default_oea["01203"]["CatCode"] = "2";
$default_oea["01203"]["CatName"] = "Career-related Experience";

$default_oea["01204"]["ItemCode"] = "01204";
$default_oea["01204"]["ItemName"] = "International Science Olympiad -The International Chemistry Olympiad";
$default_oea["01204"]["CatCode"] = "2";
$default_oea["01204"]["CatName"] = "Career-related Experience";

$default_oea["01205"]["ItemCode"] = "01205";
$default_oea["01205"]["ItemName"] = "International Science Olympiad -The International Geography Olympiad";
$default_oea["01205"]["CatCode"] = "2";
$default_oea["01205"]["CatName"] = "Career-related Experience";

$default_oea["01206"]["ItemCode"] = "01206";
$default_oea["01206"]["ItemName"] = "International Science Olympiad -The International Linguistic Olympiad";
$default_oea["01206"]["CatCode"] = "2";
$default_oea["01206"]["CatName"] = "Career-related Experience";

$default_oea["01207"]["ItemCode"] = "01207";
$default_oea["01207"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad";
$default_oea["01207"]["CatCode"] = "2";
$default_oea["01207"]["CatName"] = "Career-related Experience";

$default_oea["01208"]["ItemCode"] = "01208";
$default_oea["01208"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad -   Hong Kong Mathematical Olympiad  中國香港數學奧林匹克";
$default_oea["01208"]["CatCode"] = "2";
$default_oea["01208"]["CatName"] = "Career-related Experience";

$default_oea["01210"]["ItemCode"] = "01210";
$default_oea["01210"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - Asia-Pacific Mathematical Olympiad for Primary Schools (APMOPS)  亞太小學數學奧林匹克";
$default_oea["01210"]["CatCode"] = "2";
$default_oea["01210"]["CatName"] = "Career-related Experience";

$default_oea["01209"]["ItemCode"] = "01209";
$default_oea["01209"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - Asian Pacific Mathematical Olympiad  (APMO) 亞太數學奧林匹克";
$default_oea["01209"]["CatCode"] = "2";
$default_oea["01209"]["CatName"] = "Career-related Experience";

$default_oea["01211"]["ItemCode"] = "01211";
$default_oea["01211"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - Canadian Mathematical Olympiad (CMO)";
$default_oea["01211"]["CatCode"] = "2";
$default_oea["01211"]["CatName"] = "Career-related Experience";

$default_oea["01212"]["ItemCode"] = "01212";
$default_oea["01212"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - China Mathematical Olympiad  中國數學奧林匹克";
$default_oea["01212"]["CatCode"] = "2";
$default_oea["01212"]["CatName"] = "Career-related Experience";

$default_oea["01213"]["ItemCode"] = "01213";
$default_oea["01213"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - China Mathematical Olympiad for Girls  女子數學奧林匹克";
$default_oea["01213"]["CatCode"] = "2";
$default_oea["01213"]["CatName"] = "Career-related Experience";

$default_oea["01214"]["ItemCode"] = "01214";
$default_oea["01214"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - China Southeast Mathematical Olympiad  中國東南地區數學奧林匹克";
$default_oea["01214"]["CatCode"] = "2";
$default_oea["01214"]["CatName"] = "Career-related Experience";

$default_oea["01215"]["ItemCode"] = "01215";
$default_oea["01215"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - China Western Mathematical Olympiad  中國西部數學奧林匹克";
$default_oea["01215"]["CatCode"] = "2";
$default_oea["01215"]["CatName"] = "Career-related Experience";

$default_oea["01216"]["ItemCode"] = "01216";
$default_oea["01216"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - National Mathematical Olympiad  全國數學奧林匹克 (Please provide details in \"Description\" box.)";
$default_oea["01216"]["CatCode"] = "2";
$default_oea["01216"]["CatName"] = "Career-related Experience";

$default_oea["01217"]["ItemCode"] = "01217";
$default_oea["01217"]["ItemName"] = "International Science Olympiad -The International Mathematical Olympiad - 華夏盃全國中小學數學奧林匹克邀請賽(香港賽區)";
$default_oea["01217"]["CatCode"] = "2";
$default_oea["01217"]["CatName"] = "Career-related Experience";

$default_oea["01218"]["ItemCode"] = "01218";
$default_oea["01218"]["ItemName"] = "International Science Olympiad -The International Olympiad in Informatics";
$default_oea["01218"]["CatCode"] = "2";
$default_oea["01218"]["CatName"] = "Career-related Experience";

$default_oea["01219"]["ItemCode"] = "01219";
$default_oea["01219"]["ItemName"] = "International Science Olympiad -The International Physics Olympiad";
$default_oea["01219"]["CatCode"] = "2";
$default_oea["01219"]["CatName"] = "Career-related Experience";

$default_oea["01220"]["ItemCode"] = "01220";
$default_oea["01220"]["ItemName"] = "International Science Olympiad -The Mathematical Olympiad - SMCC Mathematics Olympiad";
$default_oea["01220"]["CatCode"] = "2";
$default_oea["01220"]["CatName"] = "Career-related Experience";

$default_oea["01221"]["ItemCode"] = "01221";
$default_oea["01221"]["ItemName"] = "International Seiler Piano Competition  Seiler國際鋼琴比賽";
$default_oea["01221"]["CatCode"] = "4";
$default_oea["01221"]["CatName"] = "Aesthetic Development";

$default_oea["01222"]["ItemCode"] = "01222";
$default_oea["01222"]["ItemName"] = "International Senior High School Intelligent Ironman Creativity Contest";
$default_oea["01222"]["CatCode"] = "2";
$default_oea["01222"]["CatName"] = "Career-related Experience";

$default_oea["01223"]["ItemCode"] = "01223";
$default_oea["01223"]["ItemName"] = "International Shaolin Wushu Trational Routine Contest";
$default_oea["01223"]["CatCode"] = "3";
$default_oea["01223"]["CatName"] = "Physical Development";

$default_oea["01224"]["ItemCode"] = "01224";
$default_oea["01224"]["ItemName"] = "International Short Film Festival Oberhausen, Germany  德國國際短片節";
$default_oea["01224"]["CatCode"] = "4";
$default_oea["01224"]["CatName"] = "Aesthetic Development";

$default_oea["01226"]["ItemCode"] = "01226";
$default_oea["01226"]["ItemName"] = "International Students' Visual Arts Contest cum Exhibition of Hong Kong  香港國際學生視覺藝術創作比賽暨展覽";
$default_oea["01226"]["CatCode"] = "4";
$default_oea["01226"]["CatName"] = "Aesthetic Development";

$default_oea["01225"]["ItemCode"] = "01225";
$default_oea["01225"]["ItemName"] = "International STA 5000 Metre Distance Award (Swimming)";
$default_oea["01225"]["CatCode"] = "3";
$default_oea["01225"]["CatName"] = "Physical Development";

$default_oea["01227"]["ItemCode"] = "01227";
$default_oea["01227"]["ItemName"] = "International Table Tennis Federation (ITTF) Junior Circuit-Tai Yuan International Youth Open";
$default_oea["01227"]["CatCode"] = "3";
$default_oea["01227"]["CatName"] = "Physical Development";

$default_oea["01228"]["ItemCode"] = "01228";
$default_oea["01228"]["ItemName"] = "International Table Tennis Federation (ITTF) World Cadet Challenge  國際乒聯少年挑戰賽";
$default_oea["01228"]["CatCode"] = "3";
$default_oea["01228"]["CatName"] = "Physical Development";

$default_oea["01229"]["ItemCode"] = "01229";
$default_oea["01229"]["ItemName"] = "International Table Tennis Federation (ITTF) World Junior Championships";
$default_oea["01229"]["CatCode"] = "3";
$default_oea["01229"]["CatName"] = "Physical Development";

$default_oea["01230"]["ItemCode"] = "01230";
$default_oea["01230"]["ItemName"] = "International Table Tennis Federation (ITTF) World Junior Circuit-Taiyuan International Open Competitions";
$default_oea["01230"]["CatCode"] = "3";
$default_oea["01230"]["CatName"] = "Physical Development";

$default_oea["01231"]["ItemCode"] = "01231";
$default_oea["01231"]["ItemName"] = "International Taekwondo Championship";
$default_oea["01231"]["CatCode"] = "3";
$default_oea["01231"]["CatName"] = "Physical Development";

$default_oea["01232"]["ItemCode"] = "01232";
$default_oea["01232"]["ItemName"] = "International Tai Chi Tournament  國際太極邀請賽";
$default_oea["01232"]["CatCode"] = "3";
$default_oea["01232"]["CatName"] = "Physical Development";

$default_oea["01233"]["ItemCode"] = "01233";
$default_oea["01233"]["ItemName"] = "International Team Fencing Competition, Italy  意大利國際輪椅劍擊團體賽";
$default_oea["01233"]["CatCode"] = "3";
$default_oea["01233"]["CatName"] = "Physical Development";

$default_oea["01234"]["ItemCode"] = "01234";
$default_oea["01234"]["ItemName"] = "International Tennis Friendship Game";
$default_oea["01234"]["CatCode"] = "3";
$default_oea["01234"]["CatName"] = "Physical Development";

$default_oea["01235"]["ItemCode"] = "01235";
$default_oea["01235"]["ItemName"] = "International Thailand Triathlon Championship";
$default_oea["01235"]["CatCode"] = "3";
$default_oea["01235"]["CatName"] = "Physical Development";

$default_oea["01236"]["ItemCode"] = "01236";
$default_oea["01236"]["ItemName"] = "International Traditional Martial Arts Festival for Youth - Traditional Martial Arts Tournament, ZhengZhou  中國鄭州國際少林武術節武術比賽";
$default_oea["01236"]["CatCode"] = "3";
$default_oea["01236"]["CatName"] = "Physical Development";

$default_oea["01237"]["ItemCode"] = "01237";
$default_oea["01237"]["ItemName"] = "International Traditional Martial Arts Tournament, Qingdao  中國青島國際武術錦標賽";
$default_oea["01237"]["CatCode"] = "3";
$default_oea["01237"]["CatName"] = "Physical Development";

$default_oea["01238"]["ItemCode"] = "01238";
$default_oea["01238"]["ItemName"] = "International Travel Writing Competition";
$default_oea["01238"]["CatCode"] = "4";
$default_oea["01238"]["CatName"] = "Aesthetic Development";

$default_oea["01239"]["ItemCode"] = "01239";
$default_oea["01239"]["ItemName"] = "International Triathlon  亞洲盃國際三項鐵人邀請賽";
$default_oea["01239"]["CatCode"] = "3";
$default_oea["01239"]["CatName"] = "Physical Development";

$default_oea["01240"]["ItemCode"] = "01240";
$default_oea["01240"]["ItemName"] = "International Triathlon Union (ITU) Asian Continental Cup Triathlon  國際鐵人三項洲際杯賽暨全國鐵人三項錦標賽";
$default_oea["01240"]["CatCode"] = "3";
$default_oea["01240"]["CatName"] = "Physical Development";

$default_oea["01241"]["ItemCode"] = "01241";
$default_oea["01241"]["ItemName"] = "International Triathlon Union (ITU) Asian Triathlon";
$default_oea["01241"]["CatCode"] = "3";
$default_oea["01241"]["CatName"] = "Physical Development";

$default_oea["01242"]["ItemCode"] = "01242";
$default_oea["01242"]["ItemName"] = "International Triathlon Union (ITU) International Triathlon Series";
$default_oea["01242"]["CatCode"] = "3";
$default_oea["01242"]["CatName"] = "Physical Development";

$default_oea["01243"]["ItemCode"] = "01243";
$default_oea["01243"]["ItemName"] = "International Triathlon Union (ITU) Macau Asian Cup Triathlon   ITU澳門國際鐵人賽暨亞洲杯分站賽";
$default_oea["01243"]["CatCode"] = "3";
$default_oea["01243"]["CatName"] = "Physical Development";

$default_oea["01244"]["ItemCode"] = "01244";
$default_oea["01244"]["ItemName"] = "International Triathlon Union (ITU) Macau International Triathlon";
$default_oea["01244"]["CatCode"] = "3";
$default_oea["01244"]["CatName"] = "Physical Development";

$default_oea["01245"]["ItemCode"] = "01245";
$default_oea["01245"]["ItemName"] = "International Triathlon Union (ITU) Triathlon World Championships  ITU三項鐵人世界錦標賽";
$default_oea["01245"]["CatCode"] = "3";
$default_oea["01245"]["CatName"] = "Physical Development";

$default_oea["01246"]["ItemCode"] = "01246";
$default_oea["01246"]["ItemName"] = "International U18 Tenpin Bowling Championship  十八歲以下青少年保齡球錦標賽";
$default_oea["01246"]["CatCode"] = "3";
$default_oea["01246"]["CatName"] = "Physical Development";

$default_oea["01247"]["ItemCode"] = "01247";
$default_oea["01247"]["ItemName"] = "International University Students' Movies & TV and New Media Works Exhibition  國際大學生電影、電視和新媒體作品展評";
$default_oea["01247"]["CatCode"] = "4";
$default_oea["01247"]["CatName"] = "Aesthetic Development";

$default_oea["01248"]["ItemCode"] = "01248";
$default_oea["01248"]["ItemName"] = "International Varsity Debates Mandarin  國際大專辯論賽";
$default_oea["01248"]["CatCode"] = "5";
$default_oea["01248"]["CatName"] = "Moral and Civic Education";

$default_oea["01249"]["ItemCode"] = "01249";
$default_oea["01249"]["ItemName"] = "International Water Polo Friendly Matches";
$default_oea["01249"]["CatCode"] = "3";
$default_oea["01249"]["CatName"] = "Physical Development";

$default_oea["01250"]["ItemCode"] = "01250";
$default_oea["01250"]["ItemName"] = "International Website Design Competition on Opportunities and Challenges of the Cyber World for Youth Development  《網絡發展與青年成長》國際計劃網頁設計比賽";
$default_oea["01250"]["CatCode"] = "4";
$default_oea["01250"]["CatName"] = "Aesthetic Development";

$default_oea["01251"]["ItemCode"] = "01251";
$default_oea["01251"]["ItemName"] = "International Women's Handball Tournament";
$default_oea["01251"]["CatCode"] = "3";
$default_oea["01251"]["CatName"] = "Physical Development";

$default_oea["01252"]["ItemCode"] = "01252";
$default_oea["01252"]["ItemName"] = "International Women's Taekwondo Open Championships";
$default_oea["01252"]["CatCode"] = "3";
$default_oea["01252"]["CatName"] = "Physical Development";

$default_oea["01253"]["ItemCode"] = "01253";
$default_oea["01253"]["ItemName"] = "International Year for the Culture of Peace Art and Design Competition (International)  國際和平文化年美術設計創作比賽(世界賽)";
$default_oea["01253"]["CatCode"] = "4";
$default_oea["01253"]["CatName"] = "Aesthetic Development";

$default_oea["01254"]["ItemCode"] = "01254";
$default_oea["01254"]["ItemName"] = "International Year of Astronomy Art and Design Competition (International)  國際天文年美術設計創作比賽(世界賽)";
$default_oea["01254"]["CatCode"] = "4";
$default_oea["01254"]["CatName"] = "Aesthetic Development";

$default_oea["01255"]["ItemCode"] = "01255";
$default_oea["01255"]["ItemName"] = "International Year of Deserts & Desertification Competition: Hong Kong 國際沙漠及荒漠化年美術設計創作比賽：香港";
$default_oea["01255"]["CatCode"] = "4";
$default_oea["01255"]["CatName"] = "Aesthetic Development";

$default_oea["01256"]["ItemCode"] = "01256";
$default_oea["01256"]["ItemName"] = "International Year of Deserts and Desertification Art and Design Competition (International)  國際沙漠及荒漠化年美術設計創作比賽(世界賽)";
$default_oea["01256"]["CatCode"] = "4";
$default_oea["01256"]["CatName"] = "Aesthetic Development";

$default_oea["01257"]["ItemCode"] = "01257";
$default_oea["01257"]["ItemName"] = "International Year of Dolphin A & D Competition: Hong Kong 國際海豚年美術設計創作比賽：香港";
$default_oea["01257"]["CatCode"] = "4";
$default_oea["01257"]["CatName"] = "Aesthetic Development";

$default_oea["01258"]["ItemCode"] = "01258";
$default_oea["01258"]["ItemName"] = "International Year of Dolphin Art and Design Competition (International)  國際海豚年美術設計創作比賽 (世界賽)";
$default_oea["01258"]["CatCode"] = "4";
$default_oea["01258"]["CatName"] = "Aesthetic Development";

$default_oea["01259"]["ItemCode"] = "01259";
$default_oea["01259"]["ItemName"] = "International Year of Ecotourism and Mountains Art and Design Competition (International)  國際生態旅遊及山區年美術設計創作比賽(世界賽)";
$default_oea["01259"]["CatCode"] = "4";
$default_oea["01259"]["CatName"] = "Aesthetic Development";

$default_oea["01260"]["ItemCode"] = "01260";
$default_oea["01260"]["ItemName"] = "International Year of Freshwater Art and Design Competition (International)  國際淡水年美術設計創作比賽(世界賽)";
$default_oea["01260"]["CatCode"] = "4";
$default_oea["01260"]["CatName"] = "Aesthetic Development";

$default_oea["01261"]["ItemCode"] = "01261";
$default_oea["01261"]["ItemName"] = "International Year of Rice A & D Competition: Hong Kong 國際稻米年美術設計創作比賽：香港";
$default_oea["01261"]["CatCode"] = "4";
$default_oea["01261"]["CatName"] = "Aesthetic Development";

$default_oea["01262"]["ItemCode"] = "01262";
$default_oea["01262"]["ItemName"] = "International Year of Rice Art and Design Competition (International)  國際稻米年美術設計創作比賽(世界賽)";
$default_oea["01262"]["CatCode"] = "4";
$default_oea["01262"]["CatName"] = "Aesthetic Development";

$default_oea["01263"]["ItemCode"] = "01263";
$default_oea["01263"]["ItemName"] = "International Year of Sanitation Art and Design Competition (International)  國際環境衛生年美術設計創作比賽(世界賽)";
$default_oea["01263"]["CatCode"] = "4";
$default_oea["01263"]["CatName"] = "Aesthetic Development";

$default_oea["01264"]["ItemCode"] = "01264";
$default_oea["01264"]["ItemName"] = "International Year of Volunteers, Love Without Border, Art and Design Competition (International)  國際義工年愛是無疆界美術設計創作比賽(世界賽)";
$default_oea["01264"]["CatCode"] = "4";
$default_oea["01264"]["CatName"] = "Aesthetic Development";

$default_oea["01265"]["ItemCode"] = "01265";
$default_oea["01265"]["ItemName"] = "International Youth and Music Festival Youth and Music In Vienna";
$default_oea["01265"]["CatCode"] = "4";
$default_oea["01265"]["CatName"] = "Aesthetic Development";

$default_oea["01266"]["ItemCode"] = "01266";
$default_oea["01266"]["ItemName"] = "International Youth Art Competition";
$default_oea["01266"]["CatCode"] = "4";
$default_oea["01266"]["CatName"] = "Aesthetic Development";

$default_oea["01267"]["ItemCode"] = "01267";
$default_oea["01267"]["ItemName"] = "International Youth Chinese Orchestra Competition";
$default_oea["01267"]["CatCode"] = "4";
$default_oea["01267"]["CatName"] = "Aesthetic Development";

$default_oea["01268"]["ItemCode"] = "01268";
$default_oea["01268"]["ItemName"] = "International Youth Cultural Exchange Association Painting and Chinese Calligraphy Competition";
$default_oea["01268"]["CatCode"] = "4";
$default_oea["01268"]["CatName"] = "Aesthetic Development";

$default_oea["01269"]["ItemCode"] = "01269";
$default_oea["01269"]["ItemName"] = "International Youth Football Invitation Tournament  國際青年足球邀請賽";
$default_oea["01269"]["CatCode"] = "3";
$default_oea["01269"]["CatName"] = "Physical Development";

$default_oea["01270"]["ItemCode"] = "01270";
$default_oea["01270"]["ItemName"] = "International Youth Games  國際少年運動會";
$default_oea["01270"]["CatCode"] = "3";
$default_oea["01270"]["CatName"] = "Physical Development";

$default_oea["01271"]["ItemCode"] = "01271";
$default_oea["01271"]["ItemName"] = "International Youth Poster Contest Green Power";
$default_oea["01271"]["CatCode"] = "4";
$default_oea["01271"]["CatName"] = "Aesthetic Development";

$default_oea["01272"]["ItemCode"] = "01272";
$default_oea["01272"]["ItemName"] = "International Youth Track Cycling Tournament";
$default_oea["01272"]["CatCode"] = "3";
$default_oea["01272"]["CatName"] = "Physical Development";

$default_oea["01273"]["ItemCode"] = "01273";
$default_oea["01273"]["ItemName"] = "Interport (Guangdong-Hong Kong-Macau) Junior Athletics Championships";
$default_oea["01273"]["CatCode"] = "3";
$default_oea["01273"]["CatName"] = "Physical Development";

$default_oea["01275"]["ItemCode"] = "01275";
$default_oea["01275"]["ItemName"] = "Interport Athletic Meet  埠際田徑賽";
$default_oea["01275"]["CatCode"] = "3";
$default_oea["01275"]["CatName"] = "Physical Development";

$default_oea["01277"]["ItemCode"] = "01277";
$default_oea["01277"]["ItemName"] = "Interport Korfball Championship  港澳埠際錦標賽";
$default_oea["01277"]["CatCode"] = "3";
$default_oea["01277"]["CatName"] = "Physical Development";

$default_oea["01290"]["ItemCode"] = "01290";
$default_oea["01290"]["ItemName"] = "Interviewing the physically challenged";
$default_oea["01290"]["CatCode"] = "3";
$default_oea["01290"]["CatName"] = "Physical Development";

$default_oea["01292"]["ItemCode"] = "01292";
$default_oea["01292"]["ItemName"] = "Invitational Tournament of Contemporary Chinese Artists (Please provide details in \"Description\" box.)";
$default_oea["01292"]["CatCode"] = "4";
$default_oea["01292"]["CatName"] = "Aesthetic Development";

$default_oea["01293"]["ItemCode"] = "01293";
$default_oea["01293"]["ItemName"] = "Invitational World Youth Mathematics Inter-City Competition  青少年數學國際城市邀請賽";
$default_oea["01293"]["CatCode"] = "2";
$default_oea["01293"]["CatName"] = "Career-related Experience";

$default_oea["01082"]["ItemCode"] = "01082";
$default_oea["01082"]["ItemName"] = "INAS-FID World Swimming Championships  國際智障人士體育聯盟世界游泳錦標賽";
$default_oea["01082"]["CatCode"] = "3";
$default_oea["01082"]["CatName"] = "Physical Development";

$default_oea["01083"]["ItemCode"] = "01083";
$default_oea["01083"]["ItemName"] = "INAS-FID World Table Tennis Championships  國際智障人士體育聯盟世界乒乓球錦標賽";
$default_oea["01083"]["CatCode"] = "3";
$default_oea["01083"]["CatName"] = "Physical Development";

$default_oea["01081"]["ItemCode"] = "01081";
$default_oea["01081"]["ItemName"] = "INASFID Global Games  國際智障人士體育聯盟環球運動會";
$default_oea["01081"]["CatCode"] = "3";
$default_oea["01081"]["CatName"] = "Physical Development";

$default_oea["01291"]["ItemCode"] = "01291";
$default_oea["01291"]["ItemName"] = "INTL Filial Piety Essay Competition香港澳門中學生弘揚孝文化徵文比賽";
$default_oea["01291"]["CatCode"] = "4";
$default_oea["01291"]["CatName"] = "Aesthetic Development";

$default_oea["01294"]["ItemCode"] = "01294";
$default_oea["01294"]["ItemName"] = "ISF World Gymnasiade  世界中學生運動會";
$default_oea["01294"]["CatCode"] = "3";
$default_oea["01294"]["CatName"] = "Physical Development";

$default_oea["01295"]["ItemCode"] = "01295";
$default_oea["01295"]["ItemName"] = "ISI Skate Hong Kong  ISI香港溜冰賽";
$default_oea["01295"]["CatCode"] = "3";
$default_oea["01295"]["CatName"] = "Physical Development";

$default_oea["01296"]["ItemCode"] = "01296";
$default_oea["01296"]["ItemName"] = "ISI World Recreational Team Championships";
$default_oea["01296"]["CatCode"] = "3";
$default_oea["01296"]["CatName"] = "Physical Development";

$default_oea["01297"]["ItemCode"] = "01297";
$default_oea["01297"]["ItemName"] = "ISM Goodwill Gymnastics Meet  ISM友好體操邀請賽";
$default_oea["01297"]["CatCode"] = "3";
$default_oea["01297"]["CatName"] = "Physical Development";

$default_oea["01298"]["ItemCode"] = "01298";
$default_oea["01298"]["ItemName"] = "ISME Conference of The International Society for Music Education in Norway";
$default_oea["01298"]["CatCode"] = "4";
$default_oea["01298"]["CatName"] = "Aesthetic Development";

$default_oea["01301"]["ItemCode"] = "01301";
$default_oea["01301"]["ItemName"] = "ITCA-YITAA Chinese Typing Competition  ITCA-YITAA中文打字比賽";
$default_oea["01301"]["CatCode"] = "2";
$default_oea["01301"]["CatName"] = "Career-related Experience";

$default_oea["01302"]["ItemCode"] = "01302";
$default_oea["01302"]["ItemName"] = "ITF Junior Circuit -  Trofeo Bonfiglio";
$default_oea["01302"]["CatCode"] = "3";
$default_oea["01302"]["CatName"] = "Physical Development";

$default_oea["01303"]["ItemCode"] = "01303";
$default_oea["01303"]["ItemName"] = "ITF Junior Circuit - Abu Dhabi ITF Junior Championships  阿拉伯阿布達比 ITF 青少年錦標賽";
$default_oea["01303"]["CatCode"] = "3";
$default_oea["01303"]["CatName"] = "Physical Development";

$default_oea["01304"]["ItemCode"] = "01304";
$default_oea["01304"]["ItemName"] = "ITF Junior Circuit - Australian Open Junior Championships";
$default_oea["01304"]["CatCode"] = "3";
$default_oea["01304"]["CatName"] = "Physical Development";

$default_oea["01306"]["ItemCode"] = "01306";
$default_oea["01306"]["ItemName"] = "ITF Junior Circuit -Copa Gerdau";
$default_oea["01306"]["CatCode"] = "3";
$default_oea["01306"]["CatName"] = "Physical Development";

$default_oea["01307"]["ItemCode"] = "01307";
$default_oea["01307"]["ItemName"] = "ITF Junior Circuit -Dunlop Orange Bowl";
$default_oea["01307"]["CatCode"] = "3";
$default_oea["01307"]["CatName"] = "Physical Development";

$default_oea["01308"]["ItemCode"] = "01308";
$default_oea["01308"]["ItemName"] = "ITF Junior Circuit -International Casablanca Junior Cup";
$default_oea["01308"]["CatCode"] = "3";
$default_oea["01308"]["CatName"] = "Physical Development";

$default_oea["01309"]["ItemCode"] = "01309";
$default_oea["01309"]["ItemName"] = "ITF Junior Circuit -Osaka Mayor's Cup - World Super Junior Tennis Championships";
$default_oea["01309"]["CatCode"] = "3";
$default_oea["01309"]["CatName"] = "Physical Development";

$default_oea["01310"]["ItemCode"] = "01310";
$default_oea["01310"]["ItemName"] = "ITF Junior Circuit -Roland Garros Junior Championships";
$default_oea["01310"]["CatCode"] = "3";
$default_oea["01310"]["CatName"] = "Physical Development";

$default_oea["01311"]["ItemCode"] = "01311";
$default_oea["01311"]["ItemName"] = "ITF Junior Circuit -The Junior Championships, Wimbledon";
$default_oea["01311"]["CatCode"] = "3";
$default_oea["01311"]["CatName"] = "Physical Development";

$default_oea["01312"]["ItemCode"] = "01312";
$default_oea["01312"]["ItemName"] = "ITF Junior Circuit -US Open Junior Tennis Championships";
$default_oea["01312"]["CatCode"] = "3";
$default_oea["01312"]["CatName"] = "Physical Development";

$default_oea["01305"]["ItemCode"] = "01305";
$default_oea["01305"]["ItemName"] = "ITF Junior Circuit (Please provide details in \"Description\" box)";
$default_oea["01305"]["CatCode"] = "3";
$default_oea["01305"]["CatName"] = "Physical Development";

$default_oea["01313"]["ItemCode"] = "01313";
$default_oea["01313"]["ItemName"] = "ITFHK National Taekwon-Do Black Belt Championships  全港跆拳道黑帶錦標賽";
$default_oea["01313"]["CatCode"] = "3";
$default_oea["01313"]["CatName"] = "Physical Development";

$default_oea["01326"]["ItemCode"] = "01326";
$default_oea["01326"]["ItemName"] = "Japan High School Orienteering Relay Race  日本高校野外定向接力賽";
$default_oea["01326"]["CatCode"] = "3";
$default_oea["01326"]["CatName"] = "Physical Development";

$default_oea["01327"]["ItemCode"] = "01327";
$default_oea["01327"]["ItemName"] = "Japan Junior Open Squash Championships";
$default_oea["01327"]["CatCode"] = "3";
$default_oea["01327"]["CatName"] = "Physical Development";

$default_oea["01328"]["ItemCode"] = "01328";
$default_oea["01328"]["ItemName"] = "Japan Nejime Dragon Boat Festival";
$default_oea["01328"]["CatCode"] = "3";
$default_oea["01328"]["CatName"] = "Physical Development";

$default_oea["01329"]["ItemCode"] = "01329";
$default_oea["01329"]["ItemName"] = "Japan Open Squash Champion  日本壁球公開賽";
$default_oea["01329"]["CatCode"] = "3";
$default_oea["01329"]["CatName"] = "Physical Development";

$default_oea["01330"]["ItemCode"] = "01330";
$default_oea["01330"]["ItemName"] = "Japan Orienteering Championship  全日本野外定向錦標賽";
$default_oea["01330"]["CatCode"] = "3";
$default_oea["01330"]["CatName"] = "Physical Development";

$default_oea["01331"]["ItemCode"] = "01331";
$default_oea["01331"]["ItemName"] = "Japan Pioneer Squash Cup  日本壁球先鋒杯";
$default_oea["01331"]["CatCode"] = "3";
$default_oea["01331"]["CatName"] = "Physical Development";

$default_oea["01332"]["ItemCode"] = "01332";
$default_oea["01332"]["ItemName"] = "Japanese Culture - Making Daifuku";
$default_oea["01332"]["CatCode"] = "4";
$default_oea["01332"]["CatName"] = "Aesthetic Development";

$default_oea["01314"]["ItemCode"] = "01314";
$default_oea["01314"]["ItemName"] = "JA Accounting Success Skill Workshop 會計成功技巧工作坊";
$default_oea["01314"]["CatCode"] = "2";
$default_oea["01314"]["CatName"] = "Career-related Experience";

$default_oea["01315"]["ItemCode"] = "01315";
$default_oea["01315"]["ItemName"] = "JA Company Programme - Junior Achievement Hong Kong  國際成就計劃香港部- 學生營商體驗";
$default_oea["01315"]["CatCode"] = "2";
$default_oea["01315"]["CatName"] = "Career-related Experience";

$default_oea["01316"]["ItemCode"] = "01316";
$default_oea["01316"]["ItemName"] = "JA Financial Management JA財務管理";
$default_oea["01316"]["CatCode"] = "2";
$default_oea["01316"]["CatName"] = "Career-related Experience";

$default_oea["01317"]["ItemCode"] = "01317";
$default_oea["01317"]["ItemName"] = "JA International Trade Workshop";
$default_oea["01317"]["CatCode"] = "2";
$default_oea["01317"]["CatName"] = "Career-related Experience";

$default_oea["01318"]["ItemCode"] = "01318";
$default_oea["01318"]["ItemName"] = "JA Job Shadowing";
$default_oea["01318"]["CatCode"] = "2";
$default_oea["01318"]["CatName"] = "Career-related Experience";

$default_oea["01319"]["ItemCode"] = "01319";
$default_oea["01319"]["ItemName"] = "JA Leadership Seminar";
$default_oea["01319"]["CatCode"] = "2";
$default_oea["01319"]["CatName"] = "Career-related Experience";

$default_oea["01320"]["ItemCode"] = "01320";
$default_oea["01320"]["ItemName"] = "JA My little business 小生意‧大創意";
$default_oea["01320"]["CatCode"] = "2";
$default_oea["01320"]["CatName"] = "Career-related Experience";

$default_oea["01321"]["ItemCode"] = "01321";
$default_oea["01321"]["ItemName"] = "JA Personal Finance  國際成就計劃香港部-個人經濟學";
$default_oea["01321"]["CatCode"] = "2";
$default_oea["01321"]["CatName"] = "Career-related Experience";

$default_oea["01322"]["ItemCode"] = "01322";
$default_oea["01322"]["ItemName"] = "JA Personal Leadership";
$default_oea["01322"]["CatCode"] = "2";
$default_oea["01322"]["CatName"] = "Career-related Experience";

$default_oea["01323"]["ItemCode"] = "01323";
$default_oea["01323"]["ItemName"] = "JA Workshop - Goal for Youth  國際成就計劃香港部 - Goal青人生工作坊";
$default_oea["01323"]["CatCode"] = "2";
$default_oea["01323"]["CatName"] = "Career-related Experience";

$default_oea["01324"]["ItemCode"] = "01324";
$default_oea["01324"]["ItemName"] = "JA Workshop - It’s my business";
$default_oea["01324"]["CatCode"] = "2";
$default_oea["01324"]["CatName"] = "Career-related Experience";

$default_oea["01325"]["ItemCode"] = "01325";
$default_oea["01325"]["ItemName"] = "JA Workshop - Success Skills Workshop  國際成就計劃香港部 - 成功技巧工作坊";
$default_oea["01325"]["CatCode"] = "2";
$default_oea["01325"]["CatName"] = "Career-related Experience";

$default_oea["01333"]["ItemCode"] = "01333";
$default_oea["01333"]["ItemName"] = "Jiayuguan Asian Triathlon Championships  嘉裕關亞洲鐵人三項錦標賽";
$default_oea["01333"]["CatCode"] = "3";
$default_oea["01333"]["CatName"] = "Physical Development";

$default_oea["01334"]["ItemCode"] = "01334";
$default_oea["01334"]["ItemName"] = "Jing He Zhai International Junior Painter Selection  静鶴齋國際小書畫家評選活動";
$default_oea["01334"]["CatCode"] = "4";
$default_oea["01334"]["CatName"] = "Aesthetic Development";

$default_oea["01335"]["ItemCode"] = "01335";
$default_oea["01335"]["ItemName"] = "Job Shadowing at Housing Authority";
$default_oea["01335"]["CatCode"] = "2";
$default_oea["01335"]["CatName"] = "Career-related Experience";

$default_oea["01336"]["ItemCode"] = "01336";
$default_oea["01336"]["ItemName"] = "Jockey Club INAS-FID World Swimming Championships  賽馬會智障人士世界游泳錦標賽";
$default_oea["01336"]["CatCode"] = "3";
$default_oea["01336"]["CatName"] = "Physical Development";

$default_oea["01337"]["ItemCode"] = "01337";
$default_oea["01337"]["ItemName"] = "Joint Po Leung Kuk Physical Fitness Scheme保良局體適能獎勵計劃";
$default_oea["01337"]["CatCode"] = "3";
$default_oea["01337"]["CatName"] = "Physical Development";

$default_oea["01338"]["ItemCode"] = "01338";
$default_oea["01338"]["ItemName"] = "Joint School Activities - Current Affairs Forum";
$default_oea["01338"]["CatCode"] = "5";
$default_oea["01338"]["CatName"] = "Moral and Civic Education";

$default_oea["01339"]["ItemCode"] = "01339";
$default_oea["01339"]["ItemName"] = "Joint School Detective Game";
$default_oea["01339"]["CatCode"] = "4";
$default_oea["01339"]["CatName"] = "Aesthetic Development";

$default_oea["01340"]["ItemCode"] = "01340";
$default_oea["01340"]["ItemName"] = "Joint School Drama Production聯校製作";
$default_oea["01340"]["CatCode"] = "4";
$default_oea["01340"]["CatName"] = "Aesthetic Development";

$default_oea["01341"]["ItemCode"] = "01341";
$default_oea["01341"]["ItemName"] = "Joint School National Culture Knowledge Competition";
$default_oea["01341"]["CatCode"] = "5";
$default_oea["01341"]["CatName"] = "Moral and Civic Education";

$default_oea["01342"]["ItemCode"] = "01342";
$default_oea["01342"]["ItemName"] = "Joint School Orienteering Championships - Kowloon District";
$default_oea["01342"]["CatCode"] = "3";
$default_oea["01342"]["CatName"] = "Physical Development";

$default_oea["01343"]["ItemCode"] = "01343";
$default_oea["01343"]["ItemName"] = "Joint School Video Production Project";
$default_oea["01343"]["CatCode"] = "4";
$default_oea["01343"]["CatName"] = "Aesthetic Development";

$default_oea["01344"]["ItemCode"] = "01344";
$default_oea["01344"]["ItemName"] = "Joint Secondary School Visual Arts Exhibition & Top Ten Outstanding Visual Arts Students Election & Competition";
$default_oea["01344"]["CatCode"] = "4";
$default_oea["01344"]["CatName"] = "Aesthetic Development";

$default_oea["01345"]["ItemCode"] = "01345";
$default_oea["01345"]["ItemName"] = "Joint TWGHs Secondary Sch. Liberal Studies Subject Seminar";
$default_oea["01345"]["CatCode"] = "5";
$default_oea["01345"]["CatName"] = "Moral and Civic Education";

$default_oea["01346"]["ItemCode"] = "01346";
$default_oea["01346"]["ItemName"] = "Joint-School Prefect Team Training Day";
$default_oea["01346"]["CatCode"] = "1";
$default_oea["01346"]["CatName"] = "Community Service";

$default_oea["01347"]["ItemCode"] = "01347";
$default_oea["01347"]["ItemName"] = "Jonah Jones Sevens Tournament";
$default_oea["01347"]["CatCode"] = "3";
$default_oea["01347"]["CatName"] = "Physical Development";

$default_oea["01348"]["ItemCode"] = "01348";
$default_oea["01348"]["ItemName"] = "JPC Anti-drug Day and Visit to Noah's Ark全城禁毒日及參觀挪亞方舟公園";
$default_oea["01348"]["CatCode"] = "5";
$default_oea["01348"]["CatName"] = "Moral and Civic Education";

$default_oea["01349"]["ItemCode"] = "01349";
$default_oea["01349"]["ItemName"] = "JTB and Wakayama International Baseball Cup";
$default_oea["01349"]["CatCode"] = "3";
$default_oea["01349"]["CatName"] = "Physical Development";

$default_oea["01350"]["ItemCode"] = "01350";
$default_oea["01350"]["ItemName"] = "Judo Competition of Secondary School 中學柔道錦標賽";
$default_oea["01350"]["CatCode"] = "3";
$default_oea["01350"]["CatName"] = "Physical Development";

$default_oea["01351"]["ItemCode"] = "01351";
$default_oea["01351"]["ItemName"] = "Junior Achievement Company Programme";
$default_oea["01351"]["CatCode"] = "2";
$default_oea["01351"]["CatName"] = "Career-related Experience";

$default_oea["01352"]["ItemCode"] = "01352";
$default_oea["01352"]["ItemName"] = "Junior Achievement Hong Kong 國際成就計劃香港部 (Please provide details in \"Description\" box.)";
$default_oea["01352"]["CatCode"] = "2";
$default_oea["01352"]["CatName"] = "Career-related Experience";

$default_oea["01353"]["ItemCode"] = "01353";
$default_oea["01353"]["ItemName"] = "Junior Achievement New Leaders Seminar";
$default_oea["01353"]["CatCode"] = "2";
$default_oea["01353"]["CatName"] = "Career-related Experience";

$default_oea["01354"]["ItemCode"] = "01354";
$default_oea["01354"]["ItemName"] = "Junior Artistic Gymnastics Invitational Championship for Hunan, Hong Kong, Macau, Taiwan  香港澳門台灣湖南少兒體操比賽";
$default_oea["01354"]["CatCode"] = "3";
$default_oea["01354"]["CatName"] = "Physical Development";

$default_oea["01355"]["ItemCode"] = "01355";
$default_oea["01355"]["ItemName"] = "Junior Davis Cup - Regional Qualifying";
$default_oea["01355"]["CatCode"] = "3";
$default_oea["01355"]["CatName"] = "Physical Development";

$default_oea["01356"]["ItemCode"] = "01356";
$default_oea["01356"]["ItemName"] = "Junior Davis Cup - World Final";
$default_oea["01356"]["CatCode"] = "3";
$default_oea["01356"]["CatName"] = "Physical Development";

$default_oea["01357"]["ItemCode"] = "01357";
$default_oea["01357"]["ItemName"] = "Junior Drill Instructor Course";
$default_oea["01357"]["CatCode"] = "1";
$default_oea["01357"]["CatName"] = "Community Service";

$default_oea["01358"]["ItemCode"] = "01358";
$default_oea["01358"]["ItemName"] = "Junior Environment Ambassador Project";
$default_oea["01358"]["CatCode"] = "5";
$default_oea["01358"]["CatName"] = "Moral and Civic Education";

$default_oea["01359"]["ItemCode"] = "01359";
$default_oea["01359"]["ItemName"] = "Junior Fencing Championship";
$default_oea["01359"]["CatCode"] = "3";
$default_oea["01359"]["CatName"] = "Physical Development";

$default_oea["01360"]["ItemCode"] = "01360";
$default_oea["01360"]["ItemName"] = "Junior Form Leadership Training Program - Carmelian Vision";
$default_oea["01360"]["CatCode"] = "1";
$default_oea["01360"]["CatName"] = "Community Service";

$default_oea["01361"]["ItemCode"] = "01361";
$default_oea["01361"]["ItemName"] = "Junior Painters Cup China and Overseas Youth and Children's Art Calligraphy Exhibition  少年畫家杯中外少年兒童美術書法大展";
$default_oea["01361"]["CatCode"] = "4";
$default_oea["01361"]["CatName"] = "Aesthetic Development";

$default_oea["01362"]["ItemCode"] = "01362";
$default_oea["01362"]["ItemName"] = "Junior Police Call 少年警訊 -  Anti-Drug Painting Competition 少年警訊禁毒杯繪畫比賽";
$default_oea["01362"]["CatCode"] = "2";
$default_oea["01362"]["CatName"] = "Career-related Experience";

$default_oea["01363"]["ItemCode"] = "01363";
$default_oea["01363"]["ItemName"] = "Junior Police Call 少年警訊 -  Anti-Drug Poster Design Competition少年警訊禁毒標語設計比賽";
$default_oea["01363"]["CatCode"] = "2";
$default_oea["01363"]["CatName"] = "Career-related Experience";

$default_oea["01364"]["ItemCode"] = "01364";
$default_oea["01364"]["ItemName"] = "Junior Police Call 少年警訊 -  Leadership and Management Training Camp少年警訊領導才能及管理技巧訓練營";
$default_oea["01364"]["CatCode"] = "2";
$default_oea["01364"]["CatName"] = "Career-related Experience";

$default_oea["01366"]["ItemCode"] = "01366";
$default_oea["01366"]["ItemName"] = "Junior Police Call 少年警訊 - Environmental Badge Training Programme少年警訊環保專章訓練班";
$default_oea["01366"]["CatCode"] = "2";
$default_oea["01366"]["CatName"] = "Career-related Experience";

$default_oea["01365"]["ItemCode"] = "01365";
$default_oea["01365"]["ItemName"] = "Junior Police Call 少年警訊 - 《警訊》海報設計比賽";
$default_oea["01365"]["CatCode"] = "4";
$default_oea["01365"]["CatName"] = "Aesthetic Development";

$default_oea["01367"]["ItemCode"] = "01367";
$default_oea["01367"]["ItemName"] = "Junior Police Call 少年警訊 (Please provide details in \"Description\" box.)";
$default_oea["01367"]["CatCode"] = "2";
$default_oea["01367"]["CatName"] = "Career-related Experience";

$default_oea["01368"]["ItemCode"] = "01368";
$default_oea["01368"]["ItemName"] = "Junior Police Call少年警訊 - Hong Kong Bank Foundation JPC Awards  滙豐銀行少年警訊獎勵計劃";
$default_oea["01368"]["CatCode"] = "1";
$default_oea["01368"]["CatName"] = "Community Service";

$default_oea["01369"]["ItemCode"] = "01369";
$default_oea["01369"]["ItemName"] = "Junior Police Call少年警訊 - Leadership Training Programme 少年警訊領袖訓練計劃";
$default_oea["01369"]["CatCode"] = "2";
$default_oea["01369"]["CatName"] = "Career-related Experience";

$default_oea["01370"]["ItemCode"] = "01370";
$default_oea["01370"]["ItemName"] = "Junior Police Call少年警訊 - Merit Prize in Fight Crime Ambassador Scheme - Sham Shui Po Junior Police Call";
$default_oea["01370"]["CatCode"] = "1";
$default_oea["01370"]["CatName"] = "Community Service";

$default_oea["01371"]["ItemCode"] = "01371";
$default_oea["01371"]["ItemName"] = "Junior Police Call少年警訊 - Merit Prize in Junior Police Call Environmental Protection Ambassador Scheme - Sham Shui Po Junior Police Call";
$default_oea["01371"]["CatCode"] = "1";
$default_oea["01371"]["CatName"] = "Community Service";

$default_oea["01372"]["ItemCode"] = "01372";
$default_oea["01372"]["ItemName"] = "Junior Police Call少年警訊 - Mural Painting";
$default_oea["01372"]["CatCode"] = "4";
$default_oea["01372"]["CatName"] = "Aesthetic Development";

$default_oea["01373"]["ItemCode"] = "01373";
$default_oea["01373"]["ItemName"] = "Junior Police Call少年警訊 - National Education Training Programme少年警訊國民教育中心訓練課程";
$default_oea["01373"]["CatCode"] = "2";
$default_oea["01373"]["CatName"] = "Career-related Experience";

$default_oea["01374"]["ItemCode"] = "01374";
$default_oea["01374"]["ItemName"] = "Junior Police Call少年警訊 - Song writing Competition少年警訊禁毒舊曲新詞創作比賽";
$default_oea["01374"]["CatCode"] = "2";
$default_oea["01374"]["CatName"] = "Career-related Experience";

$default_oea["01375"]["ItemCode"] = "01375";
$default_oea["01375"]["ItemName"] = "Junior Police Call少年警訊 - TEEN TEEN 有愛無毒 show";
$default_oea["01375"]["CatCode"] = "2";
$default_oea["01375"]["CatName"] = "Career-related Experience";

$default_oea["01376"]["ItemCode"] = "01376";
$default_oea["01376"]["ItemName"] = "Junior Women's Asia Hockey Federation Cup";
$default_oea["01376"]["CatCode"] = "3";
$default_oea["01376"]["CatName"] = "Physical Development";

$default_oea["01377"]["ItemCode"] = "01377";
$default_oea["01377"]["ItemName"] = "Junior World Orienteering Championship  世界青少年野外定向錦標賽";
$default_oea["01377"]["CatCode"] = "3";
$default_oea["01377"]["CatName"] = "Physical Development";

$default_oea["01378"]["ItemCode"] = "01378";
$default_oea["01378"]["ItemName"] = "Junior World Rowing Championship";
$default_oea["01378"]["CatCode"] = "3";
$default_oea["01378"]["CatName"] = "Physical Development";

$default_oea["01393"]["ItemCode"] = "01393";
$default_oea["01393"]["ItemName"] = "K-Swiss HK National Junior Championship  K-Swiss香港國際青少年錦標賽";
$default_oea["01393"]["CatCode"] = "3";
$default_oea["01393"]["CatName"] = "Physical Development";

$default_oea["01379"]["ItemCode"] = "01379";
$default_oea["01379"]["ItemName"] = "Kaohsiung International Age Group Swimming Invitation  高雄國際分齡游泳邀請賽";
$default_oea["01379"]["CatCode"] = "3";
$default_oea["01379"]["CatName"] = "Physical Development";

$default_oea["01380"]["ItemCode"] = "01380";
$default_oea["01380"]["ItemName"] = "Karate World Championships";
$default_oea["01380"]["CatCode"] = "3";
$default_oea["01380"]["CatName"] = "Physical Development";

$default_oea["01381"]["ItemCode"] = "01381";
$default_oea["01381"]["ItemName"] = "Karate-Do Competition";
$default_oea["01381"]["CatCode"] = "3";
$default_oea["01381"]["CatName"] = "Physical Development";

$default_oea["01382"]["ItemCode"] = "01382";
$default_oea["01382"]["ItemName"] = "Kathaumixw and Missoula Choral Festival";
$default_oea["01382"]["CatCode"] = "4";
$default_oea["01382"]["CatName"] = "Aesthetic Development";

$default_oea["01383"]["ItemCode"] = "01383";
$default_oea["01383"]["ItemName"] = "KFC National 3 on 3 Basketball Championship  肯德基全國青少年三人籃球冠軍挑戰賽";
$default_oea["01383"]["CatCode"] = "3";
$default_oea["01383"]["CatName"] = "Physical Development";

$default_oea["01384"]["ItemCode"] = "01384";
$default_oea["01384"]["ItemName"] = "Kiwanis CS Award";
$default_oea["01384"]["CatCode"] = "1";
$default_oea["01384"]["CatName"] = "Community Service";

$default_oea["01385"]["ItemCode"] = "01385";
$default_oea["01385"]["ItemName"] = "Kiwanis International (Global organization of volunteers)";
$default_oea["01385"]["CatCode"] = "1";
$default_oea["01385"]["CatName"] = "Community Service";

$default_oea["01386"]["ItemCode"] = "01386";
$default_oea["01386"]["ItemName"] = "KLRC World Junior Championships";
$default_oea["01386"]["CatCode"] = "3";
$default_oea["01386"]["CatName"] = "Physical Development";

$default_oea["01388"]["ItemCode"] = "01388";
$default_oea["01388"]["ItemName"] = "Korea International Youth Art Festival  韓國國際青少年藝術節";
$default_oea["01388"]["CatCode"] = "4";
$default_oea["01388"]["CatName"] = "Aesthetic Development";

$default_oea["01389"]["ItemCode"] = "01389";
$default_oea["01389"]["ItemName"] = "Korea Open International Karatedo Championships  韓國空手道國際公開邀請賽";
$default_oea["01389"]["CatCode"] = "3";
$default_oea["01389"]["CatName"] = "Physical Development";

$default_oea["01390"]["ItemCode"] = "01390";
$default_oea["01390"]["ItemName"] = "Korea Open Swimming Championships  韓國公開游泳錦標賽";
$default_oea["01390"]["CatCode"] = "3";
$default_oea["01390"]["CatName"] = "Physical Development";

$default_oea["01391"]["ItemCode"] = "01391";
$default_oea["01391"]["ItemName"] = "Kowloon City District Open Dance Competition九龍城區舞蹈比賽";
$default_oea["01391"]["CatCode"] = "3";
$default_oea["01391"]["CatName"] = "Physical Development";

$default_oea["01392"]["ItemCode"] = "01392";
$default_oea["01392"]["ItemName"] = "Kowloon Region Carlton Trophy Competition";
$default_oea["01392"]["CatCode"] = "3";
$default_oea["01392"]["CatName"] = "Physical Development";

$default_oea["01387"]["ItemCode"] = "01387";
$default_oea["01387"]["ItemName"] = "KOI (Kobe Osaka International) Asian Regional Karate Championships";
$default_oea["01387"]["CatCode"] = "3";
$default_oea["01387"]["CatName"] = "Physical Development";

$default_oea["01394"]["ItemCode"] = "01394";
$default_oea["01394"]["ItemName"] = "Kuwait International Open Bowling Tournament  科威特國際保齡球公開賽";
$default_oea["01394"]["CatCode"] = "3";
$default_oea["01394"]["CatName"] = "Physical Development";

$default_oea["01404"]["ItemCode"] = "01404";
$default_oea["01404"]["ItemName"] = "level up 領袖訓練計劃課程";
$default_oea["01404"]["CatCode"] = "2";
$default_oea["01404"]["CatName"] = "Career-related Experience";

$default_oea["01395"]["ItemCode"] = "01395";
$default_oea["01395"]["ItemName"] = "La Suisse plurielle: Switzerland and Migration Writing Contest";
$default_oea["01395"]["CatCode"] = "4";
$default_oea["01395"]["CatName"] = "Aesthetic Development";

$default_oea["01396"]["ItemCode"] = "01396";
$default_oea["01396"]["ItemName"] = "Laguna Phuket Triathlon  布吉拉古拿三項鐵人賽";
$default_oea["01396"]["CatCode"] = "3";
$default_oea["01396"]["CatName"] = "Physical Development";

$default_oea["01397"]["ItemCode"] = "01397";
$default_oea["01397"]["ItemName"] = "Landseed Taipei Fencing Open";
$default_oea["01397"]["CatCode"] = "3";
$default_oea["01397"]["CatName"] = "Physical Development";

$default_oea["01398"]["ItemCode"] = "01398";
$default_oea["01398"]["ItemName"] = "Leadership Training Program";
$default_oea["01398"]["CatCode"] = "2";
$default_oea["01398"]["CatName"] = "Career-related Experience";

$default_oea["01399"]["ItemCode"] = "01399";
$default_oea["01399"]["ItemName"] = "Leadership Training Scheme 課外活動小組領袖訓練課程";
$default_oea["01399"]["CatCode"] = "2";
$default_oea["01399"]["CatName"] = "Career-related Experience";

$default_oea["01400"]["ItemCode"] = "01400";
$default_oea["01400"]["ItemName"] = "Lee Ning Cup China National Junior Gymnastics Championship (Final)  李寧杯全國少年兒童體操比賽總決賽";
$default_oea["01400"]["CatCode"] = "3";
$default_oea["01400"]["CatName"] = "Physical Development";

$default_oea["01401"]["ItemCode"] = "01401";
$default_oea["01401"]["ItemName"] = "Leeds Millennium Festival of the Amateur Performing Arts";
$default_oea["01401"]["CatCode"] = "4";
$default_oea["01401"]["CatName"] = "Aesthetic Development";

$default_oea["01402"]["ItemCode"] = "01402";
$default_oea["01402"]["ItemName"] = "Lesson on Other Learning Experience與其他學習經歷相關課節";
$default_oea["01402"]["CatCode"] = "2";
$default_oea["01402"]["CatName"] = "Career-related Experience";

$default_oea["01403"]["ItemCode"] = "01403";
$default_oea["01403"]["ItemName"] = "Let's Run the World";
$default_oea["01403"]["CatCode"] = "3";
$default_oea["01403"]["CatName"] = "Physical Development";

$default_oea["01405"]["ItemCode"] = "01405";
$default_oea["01405"]["ItemName"] = "Li Ning Cup - National Acrobatic Gymnastics Championship  李寧杯全國技巧體操冠軍賽";
$default_oea["01405"]["CatCode"] = "3";
$default_oea["01405"]["CatName"] = "Physical Development";

$default_oea["01406"]["ItemCode"] = "01406";
$default_oea["01406"]["ItemName"] = "Life Education (Please provide details in \"Description\" box.)";
$default_oea["01406"]["CatCode"] = "5";
$default_oea["01406"]["CatName"] = "Moral and Civic Education";

$default_oea["01407"]["ItemCode"] = "01407";
$default_oea["01407"]["ItemName"] = "Life-planning Workshop";
$default_oea["01407"]["CatCode"] = "5";
$default_oea["01407"]["CatName"] = "Moral and Civic Education";

$default_oea["01408"]["ItemCode"] = "01408";
$default_oea["01408"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Airsoft Practical Shooting Class) 一生一體藝: 體育類訓練班 - 氣槍實用射擊班";
$default_oea["01408"]["CatCode"] = "3";
$default_oea["01408"]["CatName"] = "Physical Development";

$default_oea["01409"]["ItemCode"] = "01409";
$default_oea["01409"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Archery) 一生一體藝: 體育類訓練班 - 射箭班";
$default_oea["01409"]["CatCode"] = "3";
$default_oea["01409"]["CatName"] = "Physical Development";

$default_oea["01410"]["ItemCode"] = "01410";
$default_oea["01410"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Basketball) 一生一體藝: 體育類訓練班 - 籃球班";
$default_oea["01410"]["CatCode"] = "3";
$default_oea["01410"]["CatName"] = "Physical Development";

$default_oea["01411"]["ItemCode"] = "01411";
$default_oea["01411"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Bowling) 一生一體藝: 體育類訓練班 - 保齡球班";
$default_oea["01411"]["CatCode"] = "3";
$default_oea["01411"]["CatName"] = "Physical Development";

$default_oea["01412"]["ItemCode"] = "01412";
$default_oea["01412"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Dragon Lion Dance Class) 一生一體藝: 體育類訓練班 - 武術龍獅藝班";
$default_oea["01412"]["CatCode"] = "3";
$default_oea["01412"]["CatName"] = "Physical Development";

$default_oea["01413"]["ItemCode"] = "01413";
$default_oea["01413"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Fitness Class) 一生一體藝: 體育類訓練班 - 健體班";
$default_oea["01413"]["CatCode"] = "3";
$default_oea["01413"]["CatName"] = "Physical Development";

$default_oea["01414"]["ItemCode"] = "01414";
$default_oea["01414"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Football) 一生一體藝: 體育類訓練班 - 足球班";
$default_oea["01414"]["CatCode"] = "3";
$default_oea["01414"]["CatName"] = "Physical Development";

$default_oea["01415"]["ItemCode"] = "01415";
$default_oea["01415"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Golf) 一生一體藝: 體育類訓練班 -高爾夫球班";
$default_oea["01415"]["CatCode"] = "3";
$default_oea["01415"]["CatName"] = "Physical Development";

$default_oea["01416"]["ItemCode"] = "01416";
$default_oea["01416"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Gymnastics Trampoline) 一生一體藝: 體育類訓練班 - 體操彈網班";
$default_oea["01416"]["CatCode"] = "3";
$default_oea["01416"]["CatName"] = "Physical Development";

$default_oea["01417"]["ItemCode"] = "01417";
$default_oea["01417"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Judo) 一生一體藝: 體育類訓練班 - 柔道班";
$default_oea["01417"]["CatCode"] = "3";
$default_oea["01417"]["CatName"] = "Physical Development";

$default_oea["01418"]["ItemCode"] = "01418";
$default_oea["01418"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Table-tennis) 一生一體藝: 體育類訓練班 - 乒乓球班";
$default_oea["01418"]["CatCode"] = "3";
$default_oea["01418"]["CatName"] = "Physical Development";

$default_oea["01419"]["ItemCode"] = "01419";
$default_oea["01419"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Taekwondo) 一生一體藝: 體育類訓練班 - 跆拳道班";
$default_oea["01419"]["CatCode"] = "3";
$default_oea["01419"]["CatName"] = "Physical Development";

$default_oea["01420"]["ItemCode"] = "01420";
$default_oea["01420"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Volleyball) 一生一體藝: 體育類訓練班 - 排球班";
$default_oea["01420"]["CatCode"] = "3";
$default_oea["01420"]["CatName"] = "Physical Development";

$default_oea["01421"]["ItemCode"] = "01421";
$default_oea["01421"]["ItemName"] = "Life-wide Activities:  Sports Training Course (Yoga) 一生一體藝: 體育類訓練班 - 瑜珈班";
$default_oea["01421"]["CatCode"] = "3";
$default_oea["01421"]["CatName"] = "Physical Development";

$default_oea["01422"]["ItemCode"] = "01422";
$default_oea["01422"]["ItemName"] = "Linguistics Wonderland 語言學新天地";
$default_oea["01422"]["CatCode"] = "4";
$default_oea["01422"]["CatName"] = "Aesthetic Development";

$default_oea["01423"]["ItemCode"] = "01423";
$default_oea["01423"]["ItemName"] = "Lion and Globe Education Trust  獅球教育基金會 (Please provide details in \"Description\" box.)";
$default_oea["01423"]["CatCode"] = "5";
$default_oea["01423"]["CatName"] = "Moral and Civic Education";

$default_oea["01424"]["ItemCode"] = "01424";
$default_oea["01424"]["ItemName"] = "Lion City Football Cup";
$default_oea["01424"]["CatCode"] = "3";
$default_oea["01424"]["CatName"] = "Physical Development";

$default_oea["01425"]["ItemCode"] = "01425";
$default_oea["01425"]["ItemName"] = "Lions Clubs International Peace Poster Contest  國際獅子會和平海報比賽";
$default_oea["01425"]["CatCode"] = "4";
$default_oea["01425"]["CatName"] = "Aesthetic Development";

$default_oea["01426"]["ItemCode"] = "01426";
$default_oea["01426"]["ItemName"] = "Lions Hong Kong National Junior Tennis Championships  國際獅子會全港青少年網球錦標賽";
$default_oea["01426"]["CatCode"] = "3";
$default_oea["01426"]["CatName"] = "Physical Development";

$default_oea["01427"]["ItemCode"] = "01427";
$default_oea["01427"]["ItemName"] = "Lions International Youth Environmental Camps";
$default_oea["01427"]["CatCode"] = "5";
$default_oea["01427"]["CatName"] = "Moral and Civic Education";

$default_oea["01428"]["ItemCode"] = "01428";
$default_oea["01428"]["ItemName"] = "Lions International Youth Exchange Camp Clean Air Project";
$default_oea["01428"]["CatCode"] = "5";
$default_oea["01428"]["CatName"] = "Moral and Civic Education";

$default_oea["01429"]["ItemCode"] = "01429";
$default_oea["01429"]["ItemName"] = "Lions International Youth Exchange Scholarship: Environmental Essay Competition  獅子會國際青年交流環保徵文比賽";
$default_oea["01429"]["CatCode"] = "4";
$default_oea["01429"]["CatName"] = "Aesthetic Development";

$default_oea["01430"]["ItemCode"] = "01430";
$default_oea["01430"]["ItemName"] = "Literacy Competition in Beijing";
$default_oea["01430"]["CatCode"] = "5";
$default_oea["01430"]["CatName"] = "Moral and Civic Education";

$default_oea["01431"]["ItemCode"] = "01431";
$default_oea["01431"]["ItemName"] = "Little League Baseball Far East Tournament  遠東區少年棒球聯盟錦標賽";
$default_oea["01431"]["CatCode"] = "3";
$default_oea["01431"]["CatName"] = "Physical Development";

$default_oea["01432"]["ItemCode"] = "01432";
$default_oea["01432"]["ItemName"] = "Little Reporter Competiton小記者評審";
$default_oea["01432"]["CatCode"] = "2";
$default_oea["01432"]["CatName"] = "Career-related Experience";

$default_oea["01433"]["ItemCode"] = "01433";
$default_oea["01433"]["ItemName"] = "Little Scientists of Tomorrow";
$default_oea["01433"]["CatCode"] = "2";
$default_oea["01433"]["CatName"] = "Career-related Experience";

$default_oea["01434"]["ItemCode"] = "01434";
$default_oea["01434"]["ItemName"] = "Live Curious Documentary Awards 國家地理頻道live curious紀錄片比賽";
$default_oea["01434"]["CatCode"] = "4";
$default_oea["01434"]["CatName"] = "Aesthetic Development";

$default_oea["01435"]["ItemCode"] = "01435";
$default_oea["01435"]["ItemName"] = "Live it, use it, use nutrition labels scheme";
$default_oea["01435"]["CatCode"] = "4";
$default_oea["01435"]["CatName"] = "Aesthetic Development";

$default_oea["01436"]["ItemCode"] = "01436";
$default_oea["01436"]["ItemName"] = "Llangollen International Musical Eisteddfod";
$default_oea["01436"]["CatCode"] = "4";
$default_oea["01436"]["CatName"] = "Aesthetic Development";

$default_oea["01437"]["ItemCode"] = "01437";
$default_oea["01437"]["ItemName"] = "Local and Overseas Chinese Carving Fine Works Exhibition  海內外中國書畫篆刻精品展";
$default_oea["01437"]["CatCode"] = "4";
$default_oea["01437"]["CatName"] = "Aesthetic Development";

$default_oea["01438"]["ItemCode"] = "01438";
$default_oea["01438"]["ItemName"] = "Logo and Letterhead Design Competition for the Hong Kong Academy for Gifted Education  香港資優教育學院院徽及信紙設計比賽";
$default_oea["01438"]["CatCode"] = "4";
$default_oea["01438"]["CatName"] = "Aesthetic Development";

$default_oea["01439"]["ItemCode"] = "01439";
$default_oea["01439"]["ItemName"] = "Lok Sin Tong Joint Schools Badminton Compionship樂善糖轄屬中小學聯校羽毛球錦標賽";
$default_oea["01439"]["CatCode"] = "3";
$default_oea["01439"]["CatName"] = "Physical Development";

$default_oea["01440"]["ItemCode"] = "01440";
$default_oea["01440"]["ItemName"] = "London International Youth Science Forum  倫敦國際青年科學論壇";
$default_oea["01440"]["CatCode"] = "2";
$default_oea["01440"]["CatName"] = "Career-related Experience";

$default_oea["01441"]["ItemCode"] = "01441";
$default_oea["01441"]["ItemName"] = "Longman Essay Writing Contest  朗文英語寫作比賽";
$default_oea["01441"]["CatCode"] = "4";
$default_oea["01441"]["CatName"] = "Aesthetic Development";

$default_oea["01442"]["ItemCode"] = "01442";
$default_oea["01442"]["ItemName"] = "Look and Learn International Children's Art Competition";
$default_oea["01442"]["CatCode"] = "4";
$default_oea["01442"]["CatName"] = "Aesthetic Development";

$default_oea["01443"]["ItemCode"] = "01443";
$default_oea["01443"]["ItemName"] = "Love for Neve Century Youth's Calligraphy and Painting Photography Invitational Competition  冰雪情世紀青少年書畫攝影邀請賽";
$default_oea["01443"]["CatCode"] = "4";
$default_oea["01443"]["CatName"] = "Aesthetic Development";

$default_oea["01444"]["ItemCode"] = "01444";
$default_oea["01444"]["ItemName"] = "Luk Yeung Festival Art Competition綠楊節籌備委員會-綠楊節我愛綠楊填色比賽";
$default_oea["01444"]["CatCode"] = "4";
$default_oea["01444"]["CatName"] = "Aesthetic Development";

$default_oea["01446"]["ItemCode"] = "01446";
$default_oea["01446"]["ItemName"] = "Macao and Hong Kong Museum of Guangzhou pines Karate Competition  澳港穗松濤館空手道大賽";
$default_oea["01446"]["CatCode"] = "3";
$default_oea["01446"]["CatName"] = "Physical Development";

$default_oea["01447"]["ItemCode"] = "01447";
$default_oea["01447"]["ItemName"] = "Macau Asian City Junior Athletic Invitational Tournament  澳門亞洲城市青少年運動邀請賽";
$default_oea["01447"]["CatCode"] = "3";
$default_oea["01447"]["CatName"] = "Physical Development";

$default_oea["01448"]["ItemCode"] = "01448";
$default_oea["01448"]["ItemName"] = "Macau Asian City Junior Championships  澳門亞洲城市青少年錦標賽";
$default_oea["01448"]["CatCode"] = "3";
$default_oea["01448"]["CatName"] = "Physical Development";

$default_oea["01449"]["ItemCode"] = "01449";
$default_oea["01449"]["ItemName"] = "Macau Competition Mandarin Dubbing  港澳校際普通話配音創作比賽";
$default_oea["01449"]["CatCode"] = "4";
$default_oea["01449"]["CatName"] = "Aesthetic Development";

$default_oea["01450"]["ItemCode"] = "01450";
$default_oea["01450"]["ItemName"] = "Macau Diving Invitational  澳門跳水邀請賽";
$default_oea["01450"]["CatCode"] = "3";
$default_oea["01450"]["CatName"] = "Physical Development";

$default_oea["01451"]["ItemCode"] = "01451";
$default_oea["01451"]["ItemName"] = "Macau Galaxy Entertainment International Marathon  澳門銀河娛樂國際馬拉松";
$default_oea["01451"]["CatCode"] = "3";
$default_oea["01451"]["CatName"] = "Physical Development";

$default_oea["01452"]["ItemCode"] = "01452";
$default_oea["01452"]["ItemName"] = "Macau Half-distance Triathlon Race";
$default_oea["01452"]["CatCode"] = "3";
$default_oea["01452"]["CatName"] = "Physical Development";

$default_oea["01453"]["ItemCode"] = "01453";
$default_oea["01453"]["ItemName"] = "Macau Inter-city Athletic Meet";
$default_oea["01453"]["CatCode"] = "3";
$default_oea["01453"]["CatName"] = "Physical Development";

$default_oea["01454"]["ItemCode"] = "01454";
$default_oea["01454"]["ItemName"] = "Macau International All Girls Traditional Lion Dance Competition  澳門粵港澳女子傳統醒獅賽";
$default_oea["01454"]["CatCode"] = "3";
$default_oea["01454"]["CatName"] = "Physical Development";

$default_oea["01455"]["ItemCode"] = "01455";
$default_oea["01455"]["ItemName"] = "Macau Junior Invitation Athletic Meet";
$default_oea["01455"]["CatCode"] = "3";
$default_oea["01455"]["CatName"] = "Physical Development";

$default_oea["01456"]["ItemCode"] = "01456";
$default_oea["01456"]["ItemName"] = "Macau, Shanghai, Taipei, Hong Kong Youth Speech Competition  澳門、上海、台北、香港青少年朗誦表演比賽";
$default_oea["01456"]["CatCode"] = "4";
$default_oea["01456"]["CatName"] = "Aesthetic Development";

$default_oea["01457"]["ItemCode"] = "01457";
$default_oea["01457"]["ItemName"] = "Macgregor Hong Kong Junior Golf Open Championship  香港麥貴格青少年錦標賽";
$default_oea["01457"]["CatCode"] = "3";
$default_oea["01457"]["CatName"] = "Physical Development";

$default_oea["01459"]["ItemCode"] = "01459";
$default_oea["01459"]["ItemName"] = "Madrid Wheelchair Fencing World Cup  馬德里輪椅劍擊世界盃";
$default_oea["01459"]["CatCode"] = "3";
$default_oea["01459"]["CatName"] = "Physical Development";

$default_oea["01458"]["ItemCode"] = "01458";
$default_oea["01458"]["ItemName"] = "MaD/JA Job Shadowing  國際成就計劃香港部 - 工作影子計劃";
$default_oea["01458"]["CatCode"] = "2";
$default_oea["01458"]["CatName"] = "Career-related Experience";

$default_oea["01460"]["ItemCode"] = "01460";
$default_oea["01460"]["ItemName"] = "Mainland, Taiwan, Creative Children's Art Exhibition  兩岸四地(深、港、澳、台)少兒美術創意作品聯展";
$default_oea["01460"]["CatCode"] = "4";
$default_oea["01460"]["CatName"] = "Aesthetic Development";

$default_oea["01461"]["ItemCode"] = "01461";
$default_oea["01461"]["ItemName"] = "Mainland, Taiwan, Young Mandarin Speech Competition  兩岸四地青少年普通話朗誦比賽";
$default_oea["01461"]["CatCode"] = "4";
$default_oea["01461"]["CatName"] = "Aesthetic Development";

$default_oea["01462"]["ItemCode"] = "01462";
$default_oea["01462"]["ItemName"] = "Makatrako Cup International Open Bowling Championships  Makatrako杯國際保齡球公開賽";
$default_oea["01462"]["CatCode"] = "3";
$default_oea["01462"]["CatName"] = "Physical Development";

$default_oea["01463"]["ItemCode"] = "01463";
$default_oea["01463"]["ItemName"] = "Malaysia International Bowling Championships  馬來西亞國際保齡球賽";
$default_oea["01463"]["CatCode"] = "3";
$default_oea["01463"]["CatName"] = "Physical Development";

$default_oea["01464"]["ItemCode"] = "01464";
$default_oea["01464"]["ItemName"] = "Malaysia International Gymnastics Invitational Championships for Children  馬尼拉國際兒童體操邀請賽";
$default_oea["01464"]["CatCode"] = "3";
$default_oea["01464"]["CatName"] = "Physical Development";

$default_oea["01465"]["ItemCode"] = "01465";
$default_oea["01465"]["ItemName"] = "Malaysia Invitational Open Swimming Championships";
$default_oea["01465"]["CatCode"] = "3";
$default_oea["01465"]["CatName"] = "Physical Development";

$default_oea["01466"]["ItemCode"] = "01466";
$default_oea["01466"]["ItemName"] = "Malaysia Open Swimming Championship  馬來西亞公開游泳錦標賽";
$default_oea["01466"]["CatCode"] = "3";
$default_oea["01466"]["CatName"] = "Physical Development";

$default_oea["01467"]["ItemCode"] = "01467";
$default_oea["01467"]["ItemName"] = "Malaysia Open Taekwondo Championship  馬來西亞跆拳道公開賽";
$default_oea["01467"]["CatCode"] = "3";
$default_oea["01467"]["CatName"] = "Physical Development";

$default_oea["01468"]["ItemCode"] = "01468";
$default_oea["01468"]["ItemName"] = "Malaysia Open Track & Field Championships  馬來西亞公開田徑錦標賽";
$default_oea["01468"]["CatCode"] = "3";
$default_oea["01468"]["CatName"] = "Physical Development";

$default_oea["01469"]["ItemCode"] = "01469";
$default_oea["01469"]["ItemName"] = "Malaysia World Chinese Druming Championship";
$default_oea["01469"]["CatCode"] = "4";
$default_oea["01469"]["CatName"] = "Aesthetic Development";

$default_oea["01470"]["ItemCode"] = "01470";
$default_oea["01470"]["ItemName"] = "Malaysia Youth Squash Circuit  馬來西亞青少年壁球巡迴賽";
$default_oea["01470"]["CatCode"] = "3";
$default_oea["01470"]["CatName"] = "Physical Development";

$default_oea["01471"]["ItemCode"] = "01471";
$default_oea["01471"]["ItemName"] = "Malaysian International Open Bowling Championships  馬來西亞國際保齡球公開賽";
$default_oea["01471"]["CatCode"] = "3";
$default_oea["01471"]["CatName"] = "Physical Development";

$default_oea["01472"]["ItemCode"] = "01472";
$default_oea["01472"]["ItemName"] = "Malaysian National Rhythmic Gymnastics Championship  馬來西亞國際藝術體操公開賽";
$default_oea["01472"]["CatCode"] = "3";
$default_oea["01472"]["CatName"] = "Physical Development";

$default_oea["01473"]["ItemCode"] = "01473";
$default_oea["01473"]["ItemName"] = "Malaysian Open Individual Event  馬來西亞個人公開賽";
$default_oea["01473"]["CatCode"] = "3";
$default_oea["01473"]["CatName"] = "Physical Development";

$default_oea["01474"]["ItemCode"] = "01474";
$default_oea["01474"]["ItemName"] = "Malaysian Open Karatedo Championships  馬來西亞空手道公開賽";
$default_oea["01474"]["CatCode"] = "3";
$default_oea["01474"]["CatName"] = "Physical Development";

$default_oea["01475"]["ItemCode"] = "01475";
$default_oea["01475"]["ItemName"] = "Malina Gymnastics International School Invitation Competition";
$default_oea["01475"]["CatCode"] = "3";
$default_oea["01475"]["CatName"] = "Physical Development";

$default_oea["01476"]["ItemCode"] = "01476";
$default_oea["01476"]["ItemName"] = "Manchester United Premier Cup (World Final)";
$default_oea["01476"]["CatCode"] = "3";
$default_oea["01476"]["CatName"] = "Physical Development";

$default_oea["01477"]["ItemCode"] = "01477";
$default_oea["01477"]["ItemName"] = "Mathematics Achievement Contest";
$default_oea["01477"]["CatCode"] = "2";
$default_oea["01477"]["CatName"] = "Career-related Experience";

$default_oea["01445"]["ItemCode"] = "01445";
$default_oea["01445"]["ItemName"] = "MAAU-NSC All-comers' Athletic Championships";
$default_oea["01445"]["CatCode"] = "3";
$default_oea["01445"]["CatName"] = "Physical Development";

$default_oea["01478"]["ItemCode"] = "01478";
$default_oea["01478"]["ItemName"] = "MCE in Renmin University of China of the Hong Kong Prefect Award Scheme";
$default_oea["01478"]["CatCode"] = "5";
$default_oea["01478"]["CatName"] = "Moral and Civic Education";

$default_oea["01479"]["ItemCode"] = "01479";
$default_oea["01479"]["ItemName"] = "Medical Laboratory Science - Taster Program 醫務化驗科學-應用學習導引課程";
$default_oea["01479"]["CatCode"] = "2";
$default_oea["01479"]["CatName"] = "Career-related Experience";

$default_oea["01480"]["ItemCode"] = "01480";
$default_oea["01480"]["ItemName"] = "Mel Zajac Junior International Swim Meet Vancouver  加拿大Mel Zajac青少年游泳國際賽";
$default_oea["01480"]["CatCode"] = "3";
$default_oea["01480"]["CatName"] = "Physical Development";

$default_oea["01481"]["ItemCode"] = "01481";
$default_oea["01481"]["ItemName"] = "Melbourne School Bands Festival墨爾本學校管樂節";
$default_oea["01481"]["CatCode"] = "3";
$default_oea["01481"]["CatName"] = "Physical Development";

$default_oea["01482"]["ItemCode"] = "01482";
$default_oea["01482"]["ItemName"] = "Mercedes Cup Bangkok International Junior Tennis Championships  曼谷國際青少年網球比賽";
$default_oea["01482"]["CatCode"] = "3";
$default_oea["01482"]["CatName"] = "Physical Development";

$default_oea["01483"]["ItemCode"] = "01483";
$default_oea["01483"]["ItemName"] = "MESE One-Day Workshop";
$default_oea["01483"]["CatCode"] = "2";
$default_oea["01483"]["CatName"] = "Career-related Experience";

$default_oea["01484"]["ItemCode"] = "01484";
$default_oea["01484"]["ItemName"] = "Microsoft Imagine Cup Worldwide Final Competition";
$default_oea["01484"]["CatCode"] = "2";
$default_oea["01484"]["CatName"] = "Career-related Experience";

$default_oea["01485"]["ItemCode"] = "01485";
$default_oea["01485"]["ItemName"] = "Military Training 軍訓";
$default_oea["01485"]["CatCode"] = "3";
$default_oea["01485"]["CatName"] = "Physical Development";

$default_oea["01486"]["ItemCode"] = "01486";
$default_oea["01486"]["ItemName"] = "Millennium Dragon Cup International Teenager Painting and Calligraphy Expo (Competition)  千禧龍杯國際青少年書畫作品聯展(大賽)";
$default_oea["01486"]["CatCode"] = "4";
$default_oea["01486"]["CatName"] = "Aesthetic Development";

$default_oea["01487"]["ItemCode"] = "01487";
$default_oea["01487"]["ItemName"] = "Millennium Dreamers Award";
$default_oea["01487"]["CatCode"] = "4";
$default_oea["01487"]["CatName"] = "Aesthetic Development";

$default_oea["01488"]["ItemCode"] = "01488";
$default_oea["01488"]["ItemName"] = "Millennium Peaceful Art Prize of Russia International Youngster World Peace Calligraphy and Painting Exhibition  俄羅斯‧世界和平書畫展國際青少年兒童書畫評奬";
$default_oea["01488"]["CatCode"] = "4";
$default_oea["01488"]["CatName"] = "Aesthetic Development";

$default_oea["01489"]["ItemCode"] = "01489";
$default_oea["01489"]["ItemName"] = "Milo All Star International Junior Squash Tournament";
$default_oea["01489"]["CatCode"] = "3";
$default_oea["01489"]["CatName"] = "Physical Development";

$default_oea["01490"]["ItemCode"] = "01490";
$default_oea["01490"]["ItemName"] = "Milo All Star International Squash Championship";
$default_oea["01490"]["CatCode"] = "3";
$default_oea["01490"]["CatName"] = "Physical Development";

$default_oea["01491"]["ItemCode"] = "01491";
$default_oea["01491"]["ItemName"] = "Milo Junior Indonesia Open";
$default_oea["01491"]["CatCode"] = "3";
$default_oea["01491"]["CatName"] = "Physical Development";

$default_oea["01492"]["ItemCode"] = "01492";
$default_oea["01492"]["ItemName"] = "Milo Malaysia International Junior All Star Championships  Milo馬來西亞青少年錦標賽";
$default_oea["01492"]["CatCode"] = "3";
$default_oea["01492"]["CatName"] = "Physical Development";

$default_oea["01494"]["ItemCode"] = "01494";
$default_oea["01494"]["ItemName"] = "Milo-Malaysia Airlines International Youth All Stars Championships  Milo馬來西亞國際航空青少年錦標賽";
$default_oea["01494"]["CatCode"] = "3";
$default_oea["01494"]["CatName"] = "Physical Development";

$default_oea["01493"]["ItemCode"] = "01493";
$default_oea["01493"]["ItemName"] = "Milo/Pram Malaysia Open Swimming Championship";
$default_oea["01493"]["CatCode"] = "3";
$default_oea["01493"]["CatName"] = "Physical Development";

$default_oea["01495"]["ItemCode"] = "01495";
$default_oea["01495"]["ItemName"] = "Mime Performance  黑犬劇團默劇表演";
$default_oea["01495"]["CatCode"] = "4";
$default_oea["01495"]["CatName"] = "Aesthetic Development";

$default_oea["01496"]["ItemCode"] = "01496";
$default_oea["01496"]["ItemName"] = "Ming Pao Student Reporter明報校園記者";
$default_oea["01496"]["CatCode"] = "2";
$default_oea["01496"]["CatName"] = "Career-related Experience";

$default_oea["01497"]["ItemCode"] = "01497";
$default_oea["01497"]["ItemName"] = "Ming Yin Cup Competition";
$default_oea["01497"]["CatCode"] = "3";
$default_oea["01497"]["CatName"] = "Physical Development";

$default_oea["01498"]["ItemCode"] = "01498";
$default_oea["01498"]["ItemName"] = "Minority Arts Festival of China  全國少數民族文藝會演";
$default_oea["01498"]["CatCode"] = "4";
$default_oea["01498"]["CatName"] = "Aesthetic Development";

$default_oea["01499"]["ItemCode"] = "01499";
$default_oea["01499"]["ItemName"] = "Mistral Asian Championship";
$default_oea["01499"]["CatCode"] = "3";
$default_oea["01499"]["CatName"] = "Physical Development";

$default_oea["01500"]["ItemCode"] = "01500";
$default_oea["01500"]["ItemName"] = "Mistral Asian Continental Championships  亞洲滑浪風帆錦標賽";
$default_oea["01500"]["CatCode"] = "3";
$default_oea["01500"]["CatName"] = "Physical Development";

$default_oea["01501"]["ItemCode"] = "01501";
$default_oea["01501"]["ItemName"] = "Mistral Asian Junior & Youth Championship";
$default_oea["01501"]["CatCode"] = "3";
$default_oea["01501"]["CatName"] = "Physical Development";

$default_oea["01502"]["ItemCode"] = "01502";
$default_oea["01502"]["ItemName"] = "Mistral One Design Junior and Youth World Championships";
$default_oea["01502"]["CatCode"] = "3";
$default_oea["01502"]["CatName"] = "Physical Development";

$default_oea["01503"]["ItemCode"] = "01503";
$default_oea["01503"]["ItemName"] = "Mock Legco模擬立法會";
$default_oea["01503"]["CatCode"] = "5";
$default_oea["01503"]["CatName"] = "Moral and Civic Education";

$default_oea["01504"]["ItemCode"] = "01504";
$default_oea["01504"]["ItemName"] = "Model United Nations Assembly";
$default_oea["01504"]["CatCode"] = "2";
$default_oea["01504"]["CatName"] = "Career-related Experience";

$default_oea["01505"]["ItemCode"] = "01505";
$default_oea["01505"]["ItemName"] = "Most Outstanding Dancer of the Year全年最傑出舞蹈表現獎";
$default_oea["01505"]["CatCode"] = "4";
$default_oea["01505"]["CatName"] = "Aesthetic Development";

$default_oea["01506"]["ItemCode"] = "01506";
$default_oea["01506"]["ItemName"] = "Most Outstanding Prefect最傑出領袖生獎";
$default_oea["01506"]["CatCode"] = "1";
$default_oea["01506"]["CatName"] = "Community Service";

$default_oea["01507"]["ItemCode"] = "01507";
$default_oea["01507"]["ItemName"] = "Most Outstanding Sportsman of the Year全年最佳運動員獎";
$default_oea["01507"]["CatCode"] = "3";
$default_oea["01507"]["CatName"] = "Physical Development";

$default_oea["01508"]["ItemCode"] = "01508";
$default_oea["01508"]["ItemName"] = "Movie Review Competition - Caritas Hong Kong Youth and Community Centre";
$default_oea["01508"]["CatCode"] = "4";
$default_oea["01508"]["CatName"] = "Aesthetic Development";

$default_oea["01509"]["ItemCode"] = "01509";
$default_oea["01509"]["ItemName"] = "Mozart Piano Competition (HK)  莫札特鋼琴大賽香港區選拔賽";
$default_oea["01509"]["CatCode"] = "4";
$default_oea["01509"]["CatName"] = "Aesthetic Development";

$default_oea["01510"]["ItemCode"] = "01510";
$default_oea["01510"]["ItemName"] = "Mr Work Simulated Workplace";
$default_oea["01510"]["CatCode"] = "2";
$default_oea["01510"]["CatName"] = "Career-related Experience";

$default_oea["01511"]["ItemCode"] = "01511";
$default_oea["01511"]["ItemName"] = "Mr. C. C. Lung Memorial Scholarship";
$default_oea["01511"]["CatCode"] = "2";
$default_oea["01511"]["CatName"] = "Career-related Experience";

$default_oea["01512"]["ItemCode"] = "01512";
$default_oea["01512"]["ItemName"] = "Mr. Hou Baolin Cup China National Youth & Teenage QuYi Competition  侯寶林獎中華青少年曲藝大賽";
$default_oea["01512"]["CatCode"] = "4";
$default_oea["01512"]["CatName"] = "Aesthetic Development";

$default_oea["01513"]["ItemCode"] = "01513";
$default_oea["01513"]["ItemName"] = "Mr. Lau Fae Memorial Scholarship";
$default_oea["01513"]["CatCode"] = "2";
$default_oea["01513"]["CatName"] = "Career-related Experience";

$default_oea["01514"]["ItemCode"] = "01514";
$default_oea["01514"]["ItemName"] = "MTR Training for the brighter journey 港鐵Train出光輝每一程訓練計劃";
$default_oea["01514"]["CatCode"] = "2";
$default_oea["01514"]["CatName"] = "Career-related Experience";

$default_oea["01515"]["ItemCode"] = "01515";
$default_oea["01515"]["ItemName"] = "Multimedia Science Quiz (International Group)";
$default_oea["01515"]["CatCode"] = "2";
$default_oea["01515"]["CatName"] = "Career-related Experience";

$default_oea["01516"]["ItemCode"] = "01516";
$default_oea["01516"]["ItemName"] = "Musical - Rising Sun - Young Sun Yat-sen in Hong Kong";
$default_oea["01516"]["CatCode"] = "4";
$default_oea["01516"]["CatName"] = "Aesthetic Development";

$default_oea["01517"]["ItemCode"] = "01517";
$default_oea["01517"]["ItemName"] = "Musical Instrument Training Scheme校內樂器訓練計劃";
$default_oea["01517"]["CatCode"] = "4";
$default_oea["01517"]["CatName"] = "Aesthetic Development";

$default_oea["01518"]["ItemCode"] = "01518";
$default_oea["01518"]["ItemName"] = "My Blue Sky International Environment Picture Books Competition  我的藍天國際環境圖繪本比賽";
$default_oea["01518"]["CatCode"] = "4";
$default_oea["01518"]["CatName"] = "Aesthetic Development";

$default_oea["01519"]["ItemCode"] = "01519";
$default_oea["01519"]["ItemName"] = "My Dream Home Drawing Contest 理想家園繪畫比賽";
$default_oea["01519"]["CatCode"] = "4";
$default_oea["01519"]["CatName"] = "Aesthetic Development";

$default_oea["01520"]["ItemCode"] = "01520";
$default_oea["01520"]["ItemName"] = "My Favourite Inventions for Quality Living Competition";
$default_oea["01520"]["CatCode"] = "2";
$default_oea["01520"]["CatName"] = "Career-related Experience";

$default_oea["01521"]["ItemCode"] = "01521";
$default_oea["01521"]["ItemName"] = "My Hong Kong Dream Concert 我的香港夢音樂會";
$default_oea["01521"]["CatCode"] = "4";
$default_oea["01521"]["CatName"] = "Aesthetic Development";

$default_oea["01522"]["ItemCode"] = "01522";
$default_oea["01522"]["ItemName"] = "My Kai Tak River Painting Competition我的啟德河兒童繪畫比賽";
$default_oea["01522"]["CatCode"] = "4";
$default_oea["01522"]["CatName"] = "Aesthetic Development";

$default_oea["01523"]["ItemCode"] = "01523";
$default_oea["01523"]["ItemName"] = "MYCOSA Scholarship";
$default_oea["01523"]["CatCode"] = "2";
$default_oea["01523"]["CatName"] = "Career-related Experience";

$default_oea["01524"]["ItemCode"] = "01524";
$default_oea["01524"]["ItemName"] = "MYCPTA Scholarship";
$default_oea["01524"]["CatCode"] = "2";
$default_oea["01524"]["CatName"] = "Career-related Experience";

$default_oea["01526"]["ItemCode"] = "01526";
$default_oea["01526"]["ItemName"] = "Nation Elite Competition (Piano Section)";
$default_oea["01526"]["CatCode"] = "4";
$default_oea["01526"]["CatName"] = "Aesthetic Development";

$default_oea["01527"]["ItemCode"] = "01527";
$default_oea["01527"]["ItemName"] = "National Adolescent Science and Technology Invention Contest";
$default_oea["01527"]["CatCode"] = "2";
$default_oea["01527"]["CatName"] = "Career-related Experience";

$default_oea["01528"]["ItemCode"] = "01528";
$default_oea["01528"]["ItemName"] = "National Amateur Fencing Invitation Competitions";
$default_oea["01528"]["CatCode"] = "3";
$default_oea["01528"]["CatName"] = "Physical Development";

$default_oea["01529"]["ItemCode"] = "01529";
$default_oea["01529"]["ItemName"] = "National Amateur Outstanding Symphonic Bands Competition";
$default_oea["01529"]["CatCode"] = "4";
$default_oea["01529"]["CatName"] = "Aesthetic Development";

$default_oea["01530"]["ItemCode"] = "01530";
$default_oea["01530"]["ItemName"] = "National Artistic Talents Competition";
$default_oea["01530"]["CatCode"] = "4";
$default_oea["01530"]["CatName"] = "Aesthetic Development";

$default_oea["01531"]["ItemCode"] = "01531";
$default_oea["01531"]["ItemName"] = "National Beach Volleyball Championship  全國沙灘排球巡迴賽";
$default_oea["01531"]["CatCode"] = "3";
$default_oea["01531"]["CatName"] = "Physical Development";

$default_oea["01532"]["ItemCode"] = "01532";
$default_oea["01532"]["ItemName"] = "National Calligraphy and Painting Talents Nomination Charity Performance  全國義演書畫人才推選活動";
$default_oea["01532"]["CatCode"] = "4";
$default_oea["01532"]["CatName"] = "Aesthetic Development";

$default_oea["01533"]["ItemCode"] = "01533";
$default_oea["01533"]["ItemName"] = "National Chemistry Competition  全國高中學生化學競賽";
$default_oea["01533"]["CatCode"] = "2";
$default_oea["01533"]["CatName"] = "Career-related Experience";

$default_oea["01534"]["ItemCode"] = "01534";
$default_oea["01534"]["ItemName"] = "National Children's Art Calligraphy and Photo Contest  四方杯全國少兒書法美術攝影大賽";
$default_oea["01534"]["CatCode"] = "4";
$default_oea["01534"]["CatName"] = "Aesthetic Development";

$default_oea["01535"]["ItemCode"] = "01535";
$default_oea["01535"]["ItemName"] = "National Children's Art Cup";
$default_oea["01535"]["CatCode"] = "4";
$default_oea["01535"]["CatName"] = "Aesthetic Development";

$default_oea["01536"]["ItemCode"] = "01536";
$default_oea["01536"]["ItemName"] = "National Chinese Painting and Calligraphy Competition";
$default_oea["01536"]["CatCode"] = "4";
$default_oea["01536"]["CatName"] = "Aesthetic Development";

$default_oea["01537"]["ItemCode"] = "01537";
$default_oea["01537"]["ItemName"] = "National Chinese Writing Competition";
$default_oea["01537"]["CatCode"] = "4";
$default_oea["01537"]["CatName"] = "Aesthetic Development";

$default_oea["01538"]["ItemCode"] = "01538";
$default_oea["01538"]["ItemName"] = "National Chinese Youth Orchestra Invitational Competition";
$default_oea["01538"]["CatCode"] = "4";
$default_oea["01538"]["CatName"] = "Aesthetic Development";

$default_oea["01539"]["ItemCode"] = "01539";
$default_oea["01539"]["ItemName"] = "National City Games  全國城市體育會";
$default_oea["01539"]["CatCode"] = "3";
$default_oea["01539"]["CatName"] = "Physical Development";

$default_oea["01540"]["ItemCode"] = "01540";
$default_oea["01540"]["ItemName"] = "National Computer Artscraft Design";
$default_oea["01540"]["CatCode"] = "4";
$default_oea["01540"]["CatName"] = "Aesthetic Development";

$default_oea["01541"]["ItemCode"] = "01541";
$default_oea["01541"]["ItemName"] = "National Contest for Excellent Works of Arts, Calligraphy and Photography of Pupils & Middle School Students  全國中小學生優秀美術書法攝影作品大賽";
$default_oea["01541"]["CatCode"] = "4";
$default_oea["01541"]["CatName"] = "Aesthetic Development";

$default_oea["01542"]["ItemCode"] = "01542";
$default_oea["01542"]["ItemName"] = "National Contest of Calligraphy and Painting for the Reunification of Hong Kong  迎接香港回歸祖國全國青少年書畫大賽";
$default_oea["01542"]["CatCode"] = "4";
$default_oea["01542"]["CatName"] = "Aesthetic Development";

$default_oea["01543"]["ItemCode"] = "01543";
$default_oea["01543"]["ItemName"] = "National Day Celebration-Hong Kong Judo Touranment";
$default_oea["01543"]["CatCode"] = "3";
$default_oea["01543"]["CatName"] = "Physical Development";

$default_oea["01544"]["ItemCode"] = "01544";
$default_oea["01544"]["ItemName"] = "National Day Cup Judo Competition 國慶盃柔道錦標賽";
$default_oea["01544"]["CatCode"] = "3";
$default_oea["01544"]["CatName"] = "Physical Development";

$default_oea["01545"]["ItemCode"] = "01545";
$default_oea["01545"]["ItemName"] = "National Day Greeting Card Design Competition";
$default_oea["01545"]["CatCode"] = "4";
$default_oea["01545"]["CatName"] = "Aesthetic Development";

$default_oea["01546"]["ItemCode"] = "01546";
$default_oea["01546"]["ItemName"] = "National Day Run國慶長跑";
$default_oea["01546"]["CatCode"] = "3";
$default_oea["01546"]["CatName"] = "Physical Development";

$default_oea["01547"]["ItemCode"] = "01547";
$default_oea["01547"]["ItemName"] = "National Debate Competition of Chinese Top Universities  中國名校大學生辯論賽";
$default_oea["01547"]["CatCode"] = "5";
$default_oea["01547"]["CatName"] = "Moral and Civic Education";

$default_oea["01548"]["ItemCode"] = "01548";
$default_oea["01548"]["ItemName"] = "National Duathlon Championship  全國兩項鐵人賽";
$default_oea["01548"]["CatCode"] = "3";
$default_oea["01548"]["CatName"] = "Physical Development";

$default_oea["01549"]["ItemCode"] = "01549";
$default_oea["01549"]["ItemName"] = "National Education國情教育 (Please provide details in \"Description\" box.) ";
$default_oea["01549"]["CatCode"] = "5";
$default_oea["01549"]["CatName"] = "Moral and Civic Education";

$default_oea["01550"]["ItemCode"] = "01550";
$default_oea["01550"]["ItemName"] = "National Electronic Competition";
$default_oea["01550"]["CatCode"] = "4";
$default_oea["01550"]["CatName"] = "Aesthetic Development";

$default_oea["01551"]["ItemCode"] = "01551";
$default_oea["01551"]["ItemName"] = "National Fine Arts Competition Hello, Motherland";
$default_oea["01551"]["CatCode"] = "4";
$default_oea["01551"]["CatName"] = "Aesthetic Development";

$default_oea["01552"]["ItemCode"] = "01552";
$default_oea["01552"]["ItemName"] = "National Games of The People's Republic of China  中華人民共和國運動會";
$default_oea["01552"]["CatCode"] = "3";
$default_oea["01552"]["CatName"] = "Physical Development";

$default_oea["01553"]["ItemCode"] = "01553";
$default_oea["01553"]["ItemName"] = "National Geographic Channel and Education Bureau - Documentary Making Competition";
$default_oea["01553"]["CatCode"] = "4";
$default_oea["01553"]["CatName"] = "Aesthetic Development";

$default_oea["01554"]["ItemCode"] = "01554";
$default_oea["01554"]["ItemName"] = "National Geographic Channel Think Again Awards - Documentary Making Competition  國家地理頻道思.賞.行記錄短片製作大賽";
$default_oea["01554"]["CatCode"] = "4";
$default_oea["01554"]["CatName"] = "Aesthetic Development";

$default_oea["01555"]["ItemCode"] = "01555";
$default_oea["01555"]["ItemName"] = "National Handball Tournament";
$default_oea["01555"]["CatCode"] = "3";
$default_oea["01555"]["CatName"] = "Physical Development";

$default_oea["01556"]["ItemCode"] = "01556";
$default_oea["01556"]["ItemName"] = "National Handball Tournament for Secondary School in China";
$default_oea["01556"]["CatCode"] = "3";
$default_oea["01556"]["CatName"] = "Physical Development";

$default_oea["01557"]["ItemCode"] = "01557";
$default_oea["01557"]["ItemName"] = "National Junior Fencing Championship (Jiangsu Province)";
$default_oea["01557"]["CatCode"] = "3";
$default_oea["01557"]["CatName"] = "Physical Development";

$default_oea["01558"]["ItemCode"] = "01558";
$default_oea["01558"]["ItemName"] = "National Junior Fencing Championships  全國青年劍擊錦標賽";
$default_oea["01558"]["CatCode"] = "3";
$default_oea["01558"]["CatName"] = "Physical Development";

$default_oea["01559"]["ItemCode"] = "01559";
$default_oea["01559"]["ItemName"] = "National Junior Men's Volleyball Championship";
$default_oea["01559"]["CatCode"] = "3";
$default_oea["01559"]["CatName"] = "Physical Development";

$default_oea["01560"]["ItemCode"] = "01560";
$default_oea["01560"]["ItemName"] = "National Karate Championships  全國大眾空手道錦標賽";
$default_oea["01560"]["CatCode"] = "3";
$default_oea["01560"]["CatName"] = "Physical Development";

$default_oea["01561"]["ItemCode"] = "01561";
$default_oea["01561"]["ItemName"] = "National Life Saving Championships  全國救生錦標賽";
$default_oea["01561"]["CatCode"] = "3";
$default_oea["01561"]["CatName"] = "Physical Development";

$default_oea["01562"]["ItemCode"] = "01562";
$default_oea["01562"]["ItemName"] = "National Men's Handball Championships";
$default_oea["01562"]["CatCode"] = "3";
$default_oea["01562"]["CatName"] = "Physical Development";

$default_oea["01563"]["ItemCode"] = "01563";
$default_oea["01563"]["ItemName"] = "National Mind Sports Games";
$default_oea["01563"]["CatCode"] = "4";
$default_oea["01563"]["CatName"] = "Aesthetic Development";

$default_oea["01564"]["ItemCode"] = "01564";
$default_oea["01564"]["ItemName"] = "National Non-Professional Symphonic Band Interflow";
$default_oea["01564"]["CatCode"] = "4";
$default_oea["01564"]["CatName"] = "Aesthetic Development";

$default_oea["01565"]["ItemCode"] = "01565";
$default_oea["01565"]["ItemName"] = "National Penmanship Competition";
$default_oea["01565"]["CatCode"] = "4";
$default_oea["01565"]["CatName"] = "Aesthetic Development";

$default_oea["01566"]["ItemCode"] = "01566";
$default_oea["01566"]["ItemName"] = "National Primary and Secondary Works of Painting and Calligraphy Competition  全國中小學生繪畫、書法作品比賽";
$default_oea["01566"]["CatCode"] = "4";
$default_oea["01566"]["CatName"] = "Aesthetic Development";

$default_oea["01567"]["ItemCode"] = "01567";
$default_oea["01567"]["ItemName"] = "National Rhythmic Gymnastics Championships  全國藝術體操冠軍賽";
$default_oea["01567"]["CatCode"] = "3";
$default_oea["01567"]["CatName"] = "Physical Development";

$default_oea["01568"]["ItemCode"] = "01568";
$default_oea["01568"]["ItemName"] = "National Robotic Competition Championship";
$default_oea["01568"]["CatCode"] = "2";
$default_oea["01568"]["CatName"] = "Career-related Experience";

$default_oea["01569"]["ItemCode"] = "01569";
$default_oea["01569"]["ItemName"] = "National School Historical Confucianists' Best Quotes Calligraphy and Painting Competition  全國學界歷代儒家名言書畫大賽";
$default_oea["01569"]["CatCode"] = "4";
$default_oea["01569"]["CatName"] = "Aesthetic Development";

$default_oea["01570"]["ItemCode"] = "01570";
$default_oea["01570"]["ItemName"] = "National Secondary and Primary School Pre-Olympics Writing Competition  全國中小學生迎奧運作文大賽";
$default_oea["01570"]["CatCode"] = "4";
$default_oea["01570"]["CatName"] = "Aesthetic Development";

$default_oea["01571"]["ItemCode"] = "01571";
$default_oea["01571"]["ItemName"] = "National Secondary School Students Games cum Interport Selection Competition  全國中學生運動會暨埠際選拔賽";
$default_oea["01571"]["CatCode"] = "3";
$default_oea["01571"]["CatName"] = "Physical Development";

$default_oea["01572"]["ItemCode"] = "01572";
$default_oea["01572"]["ItemName"] = "National Secondary School Table Tennis Cup";
$default_oea["01572"]["CatCode"] = "3";
$default_oea["01572"]["CatName"] = "Physical Development";

$default_oea["01573"]["ItemCode"] = "01573";
$default_oea["01573"]["ItemName"] = "National Snow and Ice situation Painting Photography Exhibition  冰雪情全國師生書畫攝影作品邀請展";
$default_oea["01573"]["CatCode"] = "4";
$default_oea["01573"]["CatName"] = "Aesthetic Development";

$default_oea["01574"]["ItemCode"] = "01574";
$default_oea["01574"]["ItemName"] = "National Spring Fin Swimming Competition  全國蹼泳錦標賽";
$default_oea["01574"]["CatCode"] = "3";
$default_oea["01574"]["CatName"] = "Physical Development";

$default_oea["01575"]["ItemCode"] = "01575";
$default_oea["01575"]["ItemName"] = "National Students, Amateur Sports School Handball League  全國中學生、業餘體校手球聯賽";
$default_oea["01575"]["CatCode"] = "3";
$default_oea["01575"]["CatName"] = "Physical Development";

$default_oea["01576"]["ItemCode"] = "01576";
$default_oea["01576"]["ItemName"] = "National Synchronized Swimming Competition";
$default_oea["01576"]["CatCode"] = "3";
$default_oea["01576"]["CatName"] = "Physical Development";

$default_oea["01577"]["ItemCode"] = "01577";
$default_oea["01577"]["ItemName"] = "National Taiwan University International Handball Tournament  台灣手球巡迴挑戰賽暨集訓";
$default_oea["01577"]["CatCode"] = "3";
$default_oea["01577"]["CatName"] = "Physical Development";

$default_oea["01578"]["ItemCode"] = "01578";
$default_oea["01578"]["ItemName"] = "National Teachers and Students' Outstanding Calligraphy and Painting Works Collection  全國師生優秀書畫作品集";
$default_oea["01578"]["CatCode"] = "4";
$default_oea["01578"]["CatName"] = "Aesthetic Development";

$default_oea["01579"]["ItemCode"] = "01579";
$default_oea["01579"]["ItemName"] = "National Teenage Painting for the Olympics";
$default_oea["01579"]["CatCode"] = "4";
$default_oea["01579"]["CatName"] = "Aesthetic Development";

$default_oea["01580"]["ItemCode"] = "01580";
$default_oea["01580"]["ItemName"] = "National Teenager Outstanding Fine Arts Calligraphy Work and Calligraphy Exhibition";
$default_oea["01580"]["CatCode"] = "4";
$default_oea["01580"]["CatName"] = "Aesthetic Development";

$default_oea["01581"]["ItemCode"] = "01581";
$default_oea["01581"]["ItemName"] = "National Teenagers Painting Calligraphy and Photography Competition";
$default_oea["01581"]["CatCode"] = "4";
$default_oea["01581"]["CatName"] = "Aesthetic Development";

$default_oea["01582"]["ItemCode"] = "01582";
$default_oea["01582"]["ItemName"] = "National Teenagers Soft Volleyball Competition  全國青少年體彩資助軟式排球冬菅暨錦標賽";
$default_oea["01582"]["CatCode"] = "3";
$default_oea["01582"]["CatName"] = "Physical Development";

$default_oea["01583"]["ItemCode"] = "01583";
$default_oea["01583"]["ItemName"] = "National Tertiary Institutions Championship  中國大學生棒球聯賽";
$default_oea["01583"]["CatCode"] = "3";
$default_oea["01583"]["CatName"] = "Physical Development";

$default_oea["01584"]["ItemCode"] = "01584";
$default_oea["01584"]["ItemName"] = "National Triathlon Championship  全國三項鐵人賽";
$default_oea["01584"]["CatCode"] = "3";
$default_oea["01584"]["CatName"] = "Physical Development";

$default_oea["01585"]["ItemCode"] = "01585";
$default_oea["01585"]["ItemName"] = "National Violin Competition for Youth and Children  全國青年及兒童小提琴比賽";
$default_oea["01585"]["CatCode"] = "4";
$default_oea["01585"]["CatName"] = "Aesthetic Development";

$default_oea["01586"]["ItemCode"] = "01586";
$default_oea["01586"]["ItemName"] = "National Youngsters' Practical Activities on Biology & Environment";
$default_oea["01586"]["CatCode"] = "2";
$default_oea["01586"]["CatName"] = "Career-related Experience";

$default_oea["01588"]["ItemCode"] = "01588";
$default_oea["01588"]["ItemName"] = "National Youth and Children Calligraphy and Painting Competition Dawn Cup";
$default_oea["01588"]["CatCode"] = "4";
$default_oea["01588"]["CatName"] = "Aesthetic Development";

$default_oea["01587"]["ItemCode"] = "01587";
$default_oea["01587"]["ItemName"] = "National Youth Amateur Radio Contest";
$default_oea["01587"]["CatCode"] = "4";
$default_oea["01587"]["CatName"] = "Aesthetic Development";

$default_oea["01589"]["ItemCode"] = "01589";
$default_oea["01589"]["ItemName"] = "National Youth Art Competition";
$default_oea["01589"]["CatCode"] = "4";
$default_oea["01589"]["CatName"] = "Aesthetic Development";

$default_oea["01590"]["ItemCode"] = "01590";
$default_oea["01590"]["ItemName"] = "National Youth Biological and Environmental Sciences Practice  全國青少年生物和環境科學實踐活動";
$default_oea["01590"]["CatCode"] = "2";
$default_oea["01590"]["CatName"] = "Career-related Experience";

$default_oea["01591"]["ItemCode"] = "01591";
$default_oea["01591"]["ItemName"] = "National Youth Competition Children's Calligraphy and Painting Angel and the International Art Festival  國際天使藝術節暨全國青少年兒童文化藝術展評";
$default_oea["01591"]["CatCode"] = "4";
$default_oea["01591"]["CatName"] = "Aesthetic Development";

$default_oea["01592"]["ItemCode"] = "01592";
$default_oea["01592"]["ItemName"] = "National Youth Innovative Science Project";
$default_oea["01592"]["CatCode"] = "2";
$default_oea["01592"]["CatName"] = "Career-related Experience";

$default_oea["01593"]["ItemCode"] = "01593";
$default_oea["01593"]["ItemName"] = "National Youth Reunification Cup Handball Tournament  全國青少年回歸盃手球邀請賽";
$default_oea["01593"]["CatCode"] = "3";
$default_oea["01593"]["CatName"] = "Physical Development";

$default_oea["01594"]["ItemCode"] = "01594";
$default_oea["01594"]["ItemName"] = "National Youth Science and Technology Invention Contest";
$default_oea["01594"]["CatCode"] = "2";
$default_oea["01594"]["CatName"] = "Career-related Experience";

$default_oea["01595"]["ItemCode"] = "01595";
$default_oea["01595"]["ItemName"] = "Nationwide Schools Handball Tournament";
$default_oea["01595"]["CatCode"] = "3";
$default_oea["01595"]["CatName"] = "Physical Development";

$default_oea["01525"]["ItemCode"] = "01525";
$default_oea["01525"]["ItemName"] = "NAC社會適應課程 (Please provide details in \"Description\" box.) ";
$default_oea["01525"]["CatCode"] = "4";
$default_oea["01525"]["CatName"] = "Aesthetic Development";

$default_oea["01596"]["ItemCode"] = "01596";
$default_oea["01596"]["ItemName"] = "Nesta-SCMP Debating Competition";
$default_oea["01596"]["CatCode"] = "2";
$default_oea["01596"]["CatName"] = "Career-related Experience";

$default_oea["01597"]["ItemCode"] = "01597";
$default_oea["01597"]["ItemName"] = "Network Originality Competition  全國中小學信息技術創新與實踐活動";
$default_oea["01597"]["CatCode"] = "2";
$default_oea["01597"]["CatName"] = "Career-related Experience";

$default_oea["01598"]["ItemCode"] = "01598";
$default_oea["01598"]["ItemName"] = "New Balance Tai Mei Tuk 10K 2010: 4km Students Cup";
$default_oea["01598"]["CatCode"] = "3";
$default_oea["01598"]["CatName"] = "Physical Development";

$default_oea["01599"]["ItemCode"] = "01599";
$default_oea["01599"]["ItemName"] = "New Century Chinese Families, Women and Children's Calligraphy and Painting Fair Contest Week  新世紀中國家庭、婦女、兒童書畫評展周";
$default_oea["01599"]["CatCode"] = "4";
$default_oea["01599"]["CatName"] = "Aesthetic Development";

$default_oea["01600"]["ItemCode"] = "01600";
$default_oea["01600"]["ItemName"] = "New Century Cup Contest of Juvenile Painting, Calligraphy & Photography  新世紀杯中華青少年美術書法攝影大獎賽";
$default_oea["01600"]["CatCode"] = "4";
$default_oea["01600"]["CatName"] = "Aesthetic Development";

$default_oea["01601"]["ItemCode"] = "01601";
$default_oea["01601"]["ItemName"] = "New South Wales Geography Competition";
$default_oea["01601"]["CatCode"] = "5";
$default_oea["01601"]["CatName"] = "Moral and Civic Education";

$default_oea["01602"]["ItemCode"] = "01602";
$default_oea["01602"]["ItemName"] = "New Territories North Division - Service Project 新界北區 - 社區服務";
$default_oea["01602"]["CatCode"] = "1";
$default_oea["01602"]["CatName"] = "Community Service";

$default_oea["01603"]["ItemCode"] = "01603";
$default_oea["01603"]["ItemName"] = "New Tune Cup Competition (Erhu)  新聲盃二胡大賽";
$default_oea["01603"]["CatCode"] = "4";
$default_oea["01603"]["CatName"] = "Aesthetic Development";

$default_oea["01604"]["ItemCode"] = "01604";
$default_oea["01604"]["ItemName"] = "New Year's Day Youth Rugby Tournament";
$default_oea["01604"]["CatCode"] = "3";
$default_oea["01604"]["CatName"] = "Physical Development";

$default_oea["01605"]["ItemCode"] = "01605";
$default_oea["01605"]["ItemName"] = "New York University's Tisch School of the Arts International Student Film Festival  紐約大學藝術學院國際學生電影節";
$default_oea["01605"]["CatCode"] = "4";
$default_oea["01605"]["CatName"] = "Aesthetic Development";

$default_oea["01606"]["ItemCode"] = "01606";
$default_oea["01606"]["ItemName"] = "New Zealand Forest Managers Taupo Triathlon  紐西蘭塔坡林業經理三項鐵人賽";
$default_oea["01606"]["CatCode"] = "3";
$default_oea["01606"]["CatName"] = "Physical Development";

$default_oea["01607"]["ItemCode"] = "01607";
$default_oea["01607"]["ItemName"] = "New Zealand Olympic Distance Triathlon  紐西蘭奧林匹克三項鐵人長跑賽";
$default_oea["01607"]["CatCode"] = "3";
$default_oea["01607"]["CatName"] = "Physical Development";

$default_oea["01608"]["ItemCode"] = "01608";
$default_oea["01608"]["ItemName"] = "New Zealand Student Ambassador Selection Programme  新西蘭學生大使選拔計劃";
$default_oea["01608"]["CatCode"] = "5";
$default_oea["01608"]["CatName"] = "Moral and Civic Education";

$default_oea["01609"]["ItemCode"] = "01609";
$default_oea["01609"]["ItemName"] = "New Zealand Youth Open  新西蘭青少年公開賽";
$default_oea["01609"]["CatCode"] = "3";
$default_oea["01609"]["CatName"] = "Physical Development";

$default_oea["01610"]["ItemCode"] = "01610";
$default_oea["01610"]["ItemName"] = "News Competition (Please provide details in \"Description\" box.)";
$default_oea["01610"]["CatCode"] = "2";
$default_oea["01610"]["CatName"] = "Career-related Experience";

$default_oea["01611"]["ItemCode"] = "01611";
$default_oea["01611"]["ItemName"] = "Ngong Ping 360 Limited and Education Bureau - Working Experience - Student Ambassador";
$default_oea["01611"]["CatCode"] = "1";
$default_oea["01611"]["CatName"] = "Community Service";

$default_oea["01612"]["ItemCode"] = "01612";
$default_oea["01612"]["ItemName"] = "Ngong Ping 360 Limited and Education Bureau - Working Experience -Kiosk Sales";
$default_oea["01612"]["CatCode"] = "2";
$default_oea["01612"]["CatName"] = "Career-related Experience";

$default_oea["01613"]["ItemCode"] = "01613";
$default_oea["01613"]["ItemName"] = "Ngong Ping 360 Limited and Education Bureau - Working Experience -Street Performance";
$default_oea["01613"]["CatCode"] = "4";
$default_oea["01613"]["CatName"] = "Aesthetic Development";

$default_oea["01614"]["ItemCode"] = "01614";
$default_oea["01614"]["ItemName"] = "Ngong Ping 360 Other Learning Experience (OLE) Programme (Please provide details in \"Description\" box.)";
$default_oea["01614"]["CatCode"] = "2";
$default_oea["01614"]["CatName"] = "Career-related Experience";

$default_oea["01615"]["ItemCode"] = "01615";
$default_oea["01615"]["ItemName"] = "Ngong Ping Charity Walk";
$default_oea["01615"]["CatCode"] = "1";
$default_oea["01615"]["CatName"] = "Community Service";

$default_oea["01616"]["ItemCode"] = "01616";
$default_oea["01616"]["ItemName"] = "Nike All China High Schools Basketball League  Nike全國中學生藍球賽";
$default_oea["01616"]["CatCode"] = "3";
$default_oea["01616"]["CatName"] = "Physical Development";

$default_oea["01617"]["ItemCode"] = "01617";
$default_oea["01617"]["ItemName"] = "Nike HK Junior Tennis Series  Nike香港青少年女子網球大賽";
$default_oea["01617"]["CatCode"] = "3";
$default_oea["01617"]["CatName"] = "Physical Development";

$default_oea["01618"]["ItemCode"] = "01618";
$default_oea["01618"]["ItemName"] = "Nike Hong Kong Football Five  Nike香港五人足球賽";
$default_oea["01618"]["CatCode"] = "3";
$default_oea["01618"]["CatName"] = "Physical Development";

$default_oea["01619"]["ItemCode"] = "01619";
$default_oea["01619"]["ItemName"] = "Ning Po Forum - English mini-debate";
$default_oea["01619"]["CatCode"] = "2";
$default_oea["01619"]["CatName"] = "Career-related Experience";

$default_oea["01620"]["ItemCode"] = "01620";
$default_oea["01620"]["ItemName"] = "NING 超手球賽";
$default_oea["01620"]["CatCode"] = "3";
$default_oea["01620"]["CatName"] = "Physical Development";

$default_oea["01621"]["ItemCode"] = "01621";
$default_oea["01621"]["ItemName"] = "No drug, No crime camping activities無毒新天地—滅罪、禁毒挑戰營 ";
$default_oea["01621"]["CatCode"] = "5";
$default_oea["01621"]["CatName"] = "Moral and Civic Education";

$default_oea["01622"]["ItemCode"] = "01622";
$default_oea["01622"]["ItemName"] = "No Gambling Fan Design Competition運動要拼搏無需要賭博紙扇創作比賽";
$default_oea["01622"]["CatCode"] = "4";
$default_oea["01622"]["CatName"] = "Aesthetic Development";

$default_oea["01623"]["ItemCode"] = "01623";
$default_oea["01623"]["ItemName"] = "Non-Commissioned Officer (NCO) Training 小隊長訓練課程";
$default_oea["01623"]["CatCode"] = "2";
$default_oea["01623"]["CatName"] = "Career-related Experience";

$default_oea["01624"]["ItemCode"] = "01624";
$default_oea["01624"]["ItemName"] = "North American Orienteering Championships";
$default_oea["01624"]["CatCode"] = "3";
$default_oea["01624"]["CatName"] = "Physical Development";

$default_oea["01625"]["ItemCode"] = "01625";
$default_oea["01625"]["ItemName"] = "NSS Orientation Camp";
$default_oea["01625"]["CatCode"] = "4";
$default_oea["01625"]["CatName"] = "Aesthetic Development";

$default_oea["01626"]["ItemCode"] = "01626";
$default_oea["01626"]["ItemName"] = "Oakville's Chidren Art Competition";
$default_oea["01626"]["CatCode"] = "4";
$default_oea["01626"]["CatName"] = "Aesthetic Development";

$default_oea["01627"]["ItemCode"] = "01627";
$default_oea["01627"]["ItemName"] = "Occupational Safety Quiz";
$default_oea["01627"]["CatCode"] = "2";
$default_oea["01627"]["CatName"] = "Career-related Experience";

$default_oea["01628"]["ItemCode"] = "01628";
$default_oea["01628"]["ItemName"] = "Odaka Ryokuchi O-event  大高綠地大會";
$default_oea["01628"]["CatCode"] = "3";
$default_oea["01628"]["CatName"] = "Physical Development";

$default_oea["01629"]["ItemCode"] = "01629";
$default_oea["01629"]["ItemName"] = "Odyssey of the Mind Club (Please provide details in \"Description\" box.)";
$default_oea["01629"]["CatCode"] = "2";
$default_oea["01629"]["CatName"] = "Career-related Experience";

$default_oea["01630"]["ItemCode"] = "01630";
$default_oea["01630"]["ItemName"] = "Odyssey of the Mind Regional Competition (Please provide details in \"Description\" box.)";
$default_oea["01630"]["CatCode"] = "2";
$default_oea["01630"]["CatName"] = "Career-related Experience";

$default_oea["01631"]["ItemCode"] = "01631";
$default_oea["01631"]["ItemName"] = "Odyssey of the Mind Tournament - China";
$default_oea["01631"]["CatCode"] = "2";
$default_oea["01631"]["CatName"] = "Career-related Experience";

$default_oea["01632"]["ItemCode"] = "01632";
$default_oea["01632"]["ItemName"] = "Odyssey of the Mind World  - Beijing Odyssey of the Mind Television Competition";
$default_oea["01632"]["CatCode"] = "2";
$default_oea["01632"]["CatName"] = "Career-related Experience";

$default_oea["01633"]["ItemCode"] = "01633";
$default_oea["01633"]["ItemName"] = "Odyssey of the Mind World Final (Please provide details in \"Description\" box.)";
$default_oea["01633"]["CatCode"] = "2";
$default_oea["01633"]["CatName"] = "Career-related Experience";

$default_oea["01634"]["ItemCode"] = "01634";
$default_oea["01634"]["ItemName"] = "Odyssey of the Mind World Final (Please provide details in \"Description\" box.) ";
$default_oea["01634"]["CatCode"] = "2";
$default_oea["01634"]["CatName"] = "Career-related Experience";

$default_oea["01636"]["ItemCode"] = "01636";
$default_oea["01636"]["ItemName"] = "Olympiad and Hong Kong Chinese Chess Association Millenium Chinese Chess Carnival  奧林匹克聯會及香港象棋聯會主辦千禧象棋嘉年華";
$default_oea["01636"]["CatCode"] = "4";
$default_oea["01636"]["CatName"] = "Aesthetic Development";

$default_oea["01637"]["ItemCode"] = "01637";
$default_oea["01637"]["ItemName"] = "Olympic Games  奧林匹克運動會 (Please provide details in \"Description\" box.)";
$default_oea["01637"]["CatCode"] = "3";
$default_oea["01637"]["CatName"] = "Physical Development";

$default_oea["01638"]["ItemCode"] = "01638";
$default_oea["01638"]["ItemName"] = "Olympic Games For Children With Asthma";
$default_oea["01638"]["CatCode"] = "3";
$default_oea["01638"]["CatName"] = "Physical Development";

$default_oea["01635"]["ItemCode"] = "01635";
$default_oea["01635"]["ItemName"] = "OLE Sports Day Camp";
$default_oea["01635"]["CatCode"] = "3";
$default_oea["01635"]["CatName"] = "Physical Development";

$default_oea["01639"]["ItemCode"] = "01639";
$default_oea["01639"]["ItemName"] = "Online reading and writing competition";
$default_oea["01639"]["CatCode"] = "4";
$default_oea["01639"]["CatName"] = "Aesthetic Development";

$default_oea["01640"]["ItemCode"] = "01640";
$default_oea["01640"]["ItemName"] = "Open Dance Contest全港公開舞蹈比賽";
$default_oea["01640"]["CatCode"] = "4";
$default_oea["01640"]["CatName"] = "Aesthetic Development";

$default_oea["01641"]["ItemCode"] = "01641";
$default_oea["01641"]["ItemName"] = "Open Tournament Athletic Competition田徑公開賽";
$default_oea["01641"]["CatCode"] = "3";
$default_oea["01641"]["CatName"] = "Physical Development";

$default_oea["01642"]["ItemCode"] = "01642";
$default_oea["01642"]["ItemName"] = "Opensource Community Leadership Scheme";
$default_oea["01642"]["CatCode"] = "2";
$default_oea["01642"]["CatName"] = "Career-related Experience";

$default_oea["01644"]["ItemCode"] = "01644";
$default_oea["01644"]["ItemName"] = "Orientation Camp of the Health in Mind Programme";
$default_oea["01644"]["CatCode"] = "4";
$default_oea["01644"]["CatName"] = "Aesthetic Development";

$default_oea["01645"]["ItemCode"] = "01645";
$default_oea["01645"]["ItemName"] = "Orienteering Competition - Orienteering Association of Hong Kong";
$default_oea["01645"]["CatCode"] = "3";
$default_oea["01645"]["CatName"] = "Physical Development";

$default_oea["01643"]["ItemCode"] = "01643";
$default_oea["01643"]["ItemName"] = "ORBIS Student Ambassador Campaign  奧比斯學生大使運動";
$default_oea["01643"]["CatCode"] = "1";
$default_oea["01643"]["CatName"] = "Community Service";

$default_oea["01646"]["ItemCode"] = "01646";
$default_oea["01646"]["ItemName"] = "OSIM Singapore International Triathlon  OSIM星加坡三項鐵人賽";
$default_oea["01646"]["CatCode"] = "3";
$default_oea["01646"]["CatName"] = "Physical Development";

$default_oea["01647"]["ItemCode"] = "01647";
$default_oea["01647"]["ItemName"] = "OSIM Singapore ITU Triathlon Asian Cup";
$default_oea["01647"]["CatCode"] = "3";
$default_oea["01647"]["CatName"] = "Physical Development";

$default_oea["01648"]["ItemCode"] = "01648";
$default_oea["01648"]["ItemName"] = "Other CS rendered for a continuous period of not less than 1 year";
$default_oea["01648"]["CatCode"] = "1";
$default_oea["01648"]["CatName"] = "Community Service";

$default_oea["01650"]["ItemCode"] = "01650";
$default_oea["01650"]["ItemName"] = "Our Collective Memories Modern Dance Animateur Scheme  ";
$default_oea["01650"]["CatCode"] = "4";
$default_oea["01650"]["CatName"] = "Aesthetic Development";

$default_oea["01651"]["ItemCode"] = "01651";
$default_oea["01651"]["ItemName"] = "Our World, Our Climate, Our Food : Local Action for a Global Challenge International Drawing Competition";
$default_oea["01651"]["CatCode"] = "4";
$default_oea["01651"]["CatName"] = "Aesthetic Development";

$default_oea["01653"]["ItemCode"] = "01653";
$default_oea["01653"]["ItemName"] = "Out-doors War Game野外狙擊手大挑戰";
$default_oea["01653"]["CatCode"] = "3";
$default_oea["01653"]["CatName"] = "Physical Development";

$default_oea["01652"]["ItemCode"] = "01652";
$default_oea["01652"]["ItemName"] = "Outdoor Painting Competition";
$default_oea["01652"]["CatCode"] = "4";
$default_oea["01652"]["CatName"] = "Aesthetic Development";

$default_oea["01654"]["ItemCode"] = "01654";
$default_oea["01654"]["ItemName"] = "Outreach Athletics Challenge - Leisure and Cultural Services Department";
$default_oea["01654"]["CatCode"] = "3";
$default_oea["01654"]["CatName"] = "Physical Development";

$default_oea["01655"]["ItemCode"] = "01655";
$default_oea["01655"]["ItemName"] = "Outreach Squash Challenge - Hong Kong Squash";
$default_oea["01655"]["CatCode"] = "3";
$default_oea["01655"]["CatName"] = "Physical Development";

$default_oea["01656"]["ItemCode"] = "01656";
$default_oea["01656"]["ItemName"] = "Outstanding Academic Award";
$default_oea["01656"]["CatCode"] = "2";
$default_oea["01656"]["CatName"] = "Career-related Experience";

$default_oea["01657"]["ItemCode"] = "01657";
$default_oea["01657"]["ItemName"] = "Outstanding Anti-drug Ambassador Programme";
$default_oea["01657"]["CatCode"] = "1";
$default_oea["01657"]["CatName"] = "Community Service";

$default_oea["01658"]["ItemCode"] = "01658";
$default_oea["01658"]["ItemName"] = "Outstanding Artist Award";
$default_oea["01658"]["CatCode"] = "4";
$default_oea["01658"]["CatName"] = "Aesthetic Development";

$default_oea["01659"]["ItemCode"] = "01659";
$default_oea["01659"]["ItemName"] = "Outstanding Athlete Award";
$default_oea["01659"]["CatCode"] = "3";
$default_oea["01659"]["CatName"] = "Physical Development";

$default_oea["01660"]["ItemCode"] = "01660";
$default_oea["01660"]["ItemName"] = "Outstanding ECA Group傑出課外活動團隊";
$default_oea["01660"]["CatCode"] = "4";
$default_oea["01660"]["CatName"] = "Aesthetic Development";

$default_oea["01661"]["ItemCode"] = "01661";
$default_oea["01661"]["ItemName"] = "Outstanding Girl Guides";
$default_oea["01661"]["CatCode"] = "5";
$default_oea["01661"]["CatName"] = "Moral and Civic Education";

$default_oea["01662"]["ItemCode"] = "01662";
$default_oea["01662"]["ItemName"] = "Outstanding Guidance Prefect";
$default_oea["01662"]["CatCode"] = "1";
$default_oea["01662"]["CatName"] = "Community Service";

$default_oea["01663"]["ItemCode"] = "01663";
$default_oea["01663"]["ItemName"] = "Outstanding Musician Award";
$default_oea["01663"]["CatCode"] = "4";
$default_oea["01663"]["CatName"] = "Aesthetic Development";

$default_oea["01664"]["ItemCode"] = "01664";
$default_oea["01664"]["ItemName"] = "Outstanding OLE Prefect";
$default_oea["01664"]["CatCode"] = "1";
$default_oea["01664"]["CatName"] = "Community Service";

$default_oea["01665"]["ItemCode"] = "01665";
$default_oea["01665"]["ItemName"] = "Outstanding Patrol Member Award - Hong Kong Road Safety Patrol";
$default_oea["01665"]["CatCode"] = "1";
$default_oea["01665"]["CatName"] = "Community Service";

$default_oea["01666"]["ItemCode"] = "01666";
$default_oea["01666"]["ItemName"] = "Outstanding Performance in ECA傑出課外活動表現獎";
$default_oea["01666"]["CatCode"] = "4";
$default_oea["01666"]["CatName"] = "Aesthetic Development";

$default_oea["01667"]["ItemCode"] = "01667";
$default_oea["01667"]["ItemName"] = "Outstanding Prefect";
$default_oea["01667"]["CatCode"] = "1";
$default_oea["01667"]["CatName"] = "Community Service";

$default_oea["01668"]["ItemCode"] = "01668";
$default_oea["01668"]["ItemName"] = "Outstanding Secondary School Student Leaders";
$default_oea["01668"]["CatCode"] = "2";
$default_oea["01668"]["CatName"] = "Career-related Experience";

$default_oea["01669"]["ItemCode"] = "01669";
$default_oea["01669"]["ItemName"] = "Outstanding Social Service傑出服務公民獎";
$default_oea["01669"]["CatCode"] = "5";
$default_oea["01669"]["CatName"] = "Moral and Civic Education";

$default_oea["01670"]["ItemCode"] = "01670";
$default_oea["01670"]["ItemName"] = "Outstanding Student Election中學十大傑出學生選舉 (Please provide details in \"Description\" box.)";
$default_oea["01670"]["CatCode"] = "2";
$default_oea["01670"]["CatName"] = "Career-related Experience";

$default_oea["01671"]["ItemCode"] = "01671";
$default_oea["01671"]["ItemName"] = "Outstanding Student Environmental Protection Ambassador Award - Environmental Campaign Committee";
$default_oea["01671"]["CatCode"] = "1";
$default_oea["01671"]["CatName"] = "Community Service";

$default_oea["01672"]["ItemCode"] = "01672";
$default_oea["01672"]["ItemName"] = "Outstanding Volunteer / Outstanding Youth Volunteer / Volunteer Movement Award";
$default_oea["01672"]["CatCode"] = "1";
$default_oea["01672"]["CatName"] = "Community Service";

$default_oea["01673"]["ItemCode"] = "01673";
$default_oea["01673"]["ItemName"] = "Outward Bound 外展訓練";
$default_oea["01673"]["CatCode"] = "3";
$default_oea["01673"]["CatName"] = "Physical Development";

$default_oea["01674"]["ItemCode"] = "01674";
$default_oea["01674"]["ItemName"] = "Overseas Chinese Painting and Calligraphy Exhibition  海內外中國書畫大賽暨精品展";
$default_oea["01674"]["CatCode"] = "4";
$default_oea["01674"]["CatName"] = "Aesthetic Development";

$default_oea["01675"]["ItemCode"] = "01675";
$default_oea["01675"]["ItemName"] = "Overseas Chinese Youth and Children's Calligraphy and Painting Contest  中華海內外少年兒童書畫大賽";
$default_oea["01675"]["CatCode"] = "4";
$default_oea["01675"]["CatName"] = "Aesthetic Development";

$default_oea["01676"]["ItemCode"] = "01676";
$default_oea["01676"]["ItemName"] = "Oxfam Club  (Please provide details in \"Description\" box.)";
$default_oea["01676"]["CatCode"] = "1";
$default_oea["01676"]["CatName"] = "Community Service";

$default_oea["01677"]["ItemCode"] = "01677";
$default_oea["01677"]["ItemName"] = "Oxfam Trialwalker  樂施毅行者";
$default_oea["01677"]["CatCode"] = "3";
$default_oea["01677"]["CatName"] = "Physical Development";

$default_oea["01679"]["ItemCode"] = "01679";
$default_oea["01679"]["ItemName"] = "Pacific Alliance Gymnastics Championships";
$default_oea["01679"]["CatCode"] = "3";
$default_oea["01679"]["CatName"] = "Physical Development";

$default_oea["01680"]["ItemCode"] = "01680";
$default_oea["01680"]["ItemName"] = "Pacific Asia Bridge Federation Championships (Youth Series)  亞太區橋牌聯會錦標賽(少年組)";
$default_oea["01680"]["CatCode"] = "4";
$default_oea["01680"]["CatName"] = "Aesthetic Development";

$default_oea["01681"]["ItemCode"] = "01681";
$default_oea["01681"]["ItemName"] = "Pacific International City Cup Teenagers Cycling Competition  太平洋國際城巿盃青少年自由場地賽";
$default_oea["01681"]["CatCode"] = "3";
$default_oea["01681"]["CatName"] = "Physical Development";

$default_oea["01682"]["ItemCode"] = "01682";
$default_oea["01682"]["ItemName"] = "Pacific Middle School Cycling Champions";
$default_oea["01682"]["CatCode"] = "3";
$default_oea["01682"]["CatName"] = "Physical Development";

$default_oea["01683"]["ItemCode"] = "01683";
$default_oea["01683"]["ItemName"] = "Pan Pacific Swimming Championships  泛太平洋游泳錦標賽";
$default_oea["01683"]["CatCode"] = "3";
$default_oea["01683"]["CatName"] = "Physical Development";

$default_oea["01684"]["ItemCode"] = "01684";
$default_oea["01684"]["ItemName"] = "Pan Pearl River Delta Physics Olympiad  泛珠三角物理奧林匹克競賽暨中華名校邀請賽";
$default_oea["01684"]["CatCode"] = "2";
$default_oea["01684"]["CatName"] = "Career-related Experience";

$default_oea["01685"]["ItemCode"] = "01685";
$default_oea["01685"]["ItemName"] = "Parade in Shanghai Expo上海世界博覽會香港週花車巡遊";
$default_oea["01685"]["CatCode"] = "4";
$default_oea["01685"]["CatName"] = "Aesthetic Development";

$default_oea["01686"]["ItemCode"] = "01686";
$default_oea["01686"]["ItemName"] = "Participation in the Summer Self-Direction Programme";
$default_oea["01686"]["CatCode"] = "2";
$default_oea["01686"]["CatName"] = "Career-related Experience";

$default_oea["01687"]["ItemCode"] = "01687";
$default_oea["01687"]["ItemName"] = "Passing on the Torch: Asian Olympics for students of HK, Macau";
$default_oea["01687"]["CatCode"] = "4";
$default_oea["01687"]["CatName"] = "Aesthetic Development";

$default_oea["01678"]["ItemCode"] = "01678";
$default_oea["01678"]["ItemName"] = "PABF Youth Championships (Bridge Tournament)";
$default_oea["01678"]["CatCode"] = "4";
$default_oea["01678"]["CatName"] = "Aesthetic Development";

$default_oea["01688"]["ItemCode"] = "01688";
$default_oea["01688"]["ItemName"] = "PATHS 共創成長路";
$default_oea["01688"]["CatCode"] = "2";
$default_oea["01688"]["CatName"] = "Career-related Experience";

$default_oea["01689"]["ItemCode"] = "01689";
$default_oea["01689"]["ItemName"] = "PBAP BEVIDA Storm International Classic  PBAP BEVIDA保齡球國際大賽";
$default_oea["01689"]["CatCode"] = "3";
$default_oea["01689"]["CatName"] = "Physical Development";

$default_oea["01690"]["ItemCode"] = "01690";
$default_oea["01690"]["ItemName"] = "Peace Cup Painting and Calligraphy Artistic International Allied Exhibition  和平杯書畫藝術國際聯展青少年兒童書畫作品評奬";
$default_oea["01690"]["CatCode"] = "4";
$default_oea["01690"]["CatName"] = "Aesthetic Development";

$default_oea["01691"]["ItemCode"] = "01691";
$default_oea["01691"]["ItemName"] = "Peace Poster Contest";
$default_oea["01691"]["CatCode"] = "4";
$default_oea["01691"]["CatName"] = "Aesthetic Development";

$default_oea["01692"]["ItemCode"] = "01692";
$default_oea["01692"]["ItemName"] = "Peak ABA Championship (basketball)";
$default_oea["01692"]["CatCode"] = "3";
$default_oea["01692"]["CatName"] = "Physical Development";

$default_oea["01693"]["ItemCode"] = "01693";
$default_oea["01693"]["ItemName"] = "Pearl River Delta Amateur Fencing Tournament  粵港澳業餘擊劍邀請賽";
$default_oea["01693"]["CatCode"] = "3";
$default_oea["01693"]["CatName"] = "Physical Development";

$default_oea["01694"]["ItemCode"] = "01694";
$default_oea["01694"]["ItemName"] = "Pearl River Delta Children's Art Calligraphy Competition  中華少年兒童美術書法大賽";
$default_oea["01694"]["CatCode"] = "4";
$default_oea["01694"]["CatName"] = "Aesthetic Development";

$default_oea["01695"]["ItemCode"] = "01695";
$default_oea["01695"]["ItemName"] = "Pedestrian Safety Discovery Tour (School Team) - Hong Kong Road Safety Patrol";
$default_oea["01695"]["CatCode"] = "1";
$default_oea["01695"]["CatName"] = "Community Service";

$default_oea["01696"]["ItemCode"] = "01696";
$default_oea["01696"]["ItemName"] = "Pellegrino Cafe Bar Bistro International Tournament";
$default_oea["01696"]["CatCode"] = "2";
$default_oea["01696"]["CatName"] = "Career-related Experience";

$default_oea["01697"]["ItemCode"] = "01697";
$default_oea["01697"]["ItemName"] = "Personal Development Adventure Camp個人成長歷奇營(感受朝陽計劃)";
$default_oea["01697"]["CatCode"] = "2";
$default_oea["01697"]["CatName"] = "Career-related Experience";

$default_oea["01698"]["ItemCode"] = "01698";
$default_oea["01698"]["ItemName"] = "Perth International Cadet Championships (First-Aid)";
$default_oea["01698"]["CatCode"] = "1";
$default_oea["01698"]["CatName"] = "Community Service";

$default_oea["01699"]["ItemCode"] = "01699";
$default_oea["01699"]["ItemName"] = "Perth International First-aid Competition";
$default_oea["01699"]["CatCode"] = "1";
$default_oea["01699"]["CatName"] = "Community Service";

$default_oea["01700"]["ItemCode"] = "01700";
$default_oea["01700"]["ItemName"] = "Phase I - YPP 'Low Carbon Generation'";
$default_oea["01700"]["CatCode"] = "5";
$default_oea["01700"]["CatName"] = "Moral and Civic Education";

$default_oea["01702"]["ItemCode"] = "01702";
$default_oea["01702"]["ItemName"] = "Philip C Jessup International Law Moot Court Competition  Philip C Jessup國際法庭模擬賽";
$default_oea["01702"]["CatCode"] = "5";
$default_oea["01702"]["CatName"] = "Moral and Civic Education";

$default_oea["01703"]["ItemCode"] = "01703";
$default_oea["01703"]["ItemName"] = "Philippines Invitational Track & Field Athletics Meet  菲律賓田徑邀請賽";
$default_oea["01703"]["CatCode"] = "3";
$default_oea["01703"]["CatName"] = "Physical Development";

$default_oea["01704"]["ItemCode"] = "01704";
$default_oea["01704"]["ItemName"] = "Philippines Junior International Golf Championship  菲律賓青少年國際高球錦標賽";
$default_oea["01704"]["CatCode"] = "3";
$default_oea["01704"]["CatName"] = "Physical Development";

$default_oea["01705"]["ItemCode"] = "01705";
$default_oea["01705"]["ItemName"] = "Philippines-China-Japan international Fine Arts Competition  菲律賓-中國-日本國際書畫大賽";
$default_oea["01705"]["CatCode"] = "4";
$default_oea["01705"]["CatName"] = "Aesthetic Development";

$default_oea["01706"]["ItemCode"] = "01706";
$default_oea["01706"]["ItemName"] = "Philippines-China-Japan Youth Fine Arts Competition  菲律賓－中國－日本國際少年兒童書畫大賽";
$default_oea["01706"]["CatCode"] = "4";
$default_oea["01706"]["CatName"] = "Aesthetic Development";

$default_oea["01707"]["ItemCode"] = "01707";
$default_oea["01707"]["ItemName"] = "Photo-taking Competition for Secondary School Students - Food and Environmental Hygiene Department";
$default_oea["01707"]["CatCode"] = "4";
$default_oea["01707"]["CatName"] = "Aesthetic Development";

$default_oea["01701"]["ItemCode"] = "01701";
$default_oea["01701"]["ItemName"] = "PHE International Children's Art Calligraphy Competition for Primary and Secondary Students  PHE國際中小學生幼兒美術書法大賽";
$default_oea["01701"]["CatCode"] = "4";
$default_oea["01701"]["CatName"] = "Aesthetic Development";

$default_oea["01709"]["ItemCode"] = "01709";
$default_oea["01709"]["ItemName"] = "Pixar彼思動畫二十五週年展覽";
$default_oea["01709"]["CatCode"] = "4";
$default_oea["01709"]["CatName"] = "Aesthetic Development";

$default_oea["01708"]["ItemCode"] = "01708";
$default_oea["01708"]["ItemName"] = "PIA World Junior Men's Squash Championships";
$default_oea["01708"]["CatCode"] = "3";
$default_oea["01708"]["CatName"] = "Physical Development";

$default_oea["01710"]["ItemCode"] = "01710";
$default_oea["01710"]["ItemName"] = "Po Leung Kuk Art Quality Circle AAS保良局美術質素圈獎勵計劃";
$default_oea["01710"]["CatCode"] = "4";
$default_oea["01710"]["CatName"] = "Aesthetic Development";

$default_oea["01711"]["ItemCode"] = "01711";
$default_oea["01711"]["ItemName"] = "Po Leung Kuk Charity Art Competition保良局慈善藝術創作比賽";
$default_oea["01711"]["CatCode"] = "4";
$default_oea["01711"]["CatName"] = "Aesthetic Development";

$default_oea["01712"]["ItemCode"] = "01712";
$default_oea["01712"]["ItemName"] = "Po Leung Kuk Music Circle Award Scheme保良局音樂質素圈獎勵計劃";
$default_oea["01712"]["CatCode"] = "4";
$default_oea["01712"]["CatName"] = "Aesthetic Development";

$default_oea["01713"]["ItemCode"] = "01713";
$default_oea["01713"]["ItemName"] = "Po Leung Kuk Primary Mathematics World Contest  保良局小學數學世界邀請賽";
$default_oea["01713"]["CatCode"] = "2";
$default_oea["01713"]["CatName"] = "Career-related Experience";

$default_oea["01714"]["ItemCode"] = "01714";
$default_oea["01714"]["ItemName"] = "Po Leung Kuk Secondary Schools Aircraft Flying Competition保良局中學飛機飛行作品飛行比賽";
$default_oea["01714"]["CatCode"] = "2";
$default_oea["01714"]["CatName"] = "Career-related Experience";

$default_oea["01715"]["ItemCode"] = "01715";
$default_oea["01715"]["ItemName"] = "Po Leung Kuk Secondary Schools Joint Schools Swimming Gala: Group Prize保良局中學聯校水運會(團體獎)";
$default_oea["01715"]["CatCode"] = "3";
$default_oea["01715"]["CatName"] = "Physical Development";

$default_oea["01716"]["ItemCode"] = "01716";
$default_oea["01716"]["ItemName"] = "Po Leung Kuk Secondary Schools Physical Fitness Test Scheme保良局中學學生體適能測試計劃";
$default_oea["01716"]["CatCode"] = "3";
$default_oea["01716"]["CatName"] = "Physical Development";

$default_oea["01717"]["ItemCode"] = "01717";
$default_oea["01717"]["ItemName"] = "Po Leung Kuk Secondary Schools School Uniform Design Competition 保良局聯校校服設計比賽";
$default_oea["01717"]["CatCode"] = "4";
$default_oea["01717"]["CatName"] = "Aesthetic Development";

$default_oea["01718"]["ItemCode"] = "01718";
$default_oea["01718"]["ItemName"] = "Po Leung Kuk Swimming Gala保良局聯校游泳賽";
$default_oea["01718"]["CatCode"] = "3";
$default_oea["01718"]["CatName"] = "Physical Development";

$default_oea["01719"]["ItemCode"] = "01719";
$default_oea["01719"]["ItemName"] = "Po Leung Kuk X'mas Card Design Competition保良局聖誕咭設計比賽校內冠軍";
$default_oea["01719"]["CatCode"] = "4";
$default_oea["01719"]["CatName"] = "Aesthetic Development";

$default_oea["01720"]["ItemCode"] = "01720";
$default_oea["01720"]["ItemName"] = "Poetry Festival (詩節) (Please provide details in \"Description\" box.)";
$default_oea["01720"]["CatCode"] = "4";
$default_oea["01720"]["CatName"] = "Aesthetic Development";

$default_oea["01721"]["ItemCode"] = "01721";
$default_oea["01721"]["ItemName"] = "Poland International Wheelchair Fencing Competition  波蘭輪椅劍擊錦標賽";
$default_oea["01721"]["CatCode"] = "3";
$default_oea["01721"]["CatName"] = "Physical Development";

$default_oea["01722"]["ItemCode"] = "01722";
$default_oea["01722"]["ItemName"] = "Poland Wheelchair Fencing World Cup  波蘭輪椅劍擊世界盃";
$default_oea["01722"]["CatCode"] = "3";
$default_oea["01722"]["CatName"] = "Physical Development";

$default_oea["01723"]["ItemCode"] = "01723";
$default_oea["01723"]["ItemName"] = "Police Knowledge Training Course警隊知識訓練班";
$default_oea["01723"]["CatCode"] = "2";
$default_oea["01723"]["CatName"] = "Career-related Experience";

$default_oea["01724"]["ItemCode"] = "01724";
$default_oea["01724"]["ItemName"] = "PolyU Innovation and Entrepreneurship Global Student Challenge";
$default_oea["01724"]["CatCode"] = "2";
$default_oea["01724"]["CatName"] = "Career-related Experience";

$default_oea["01725"]["ItemCode"] = "01725";
$default_oea["01725"]["ItemName"] = "Port Dickson International Triathlon  波德申國際三項全能賽";
$default_oea["01725"]["CatCode"] = "3";
$default_oea["01725"]["CatName"] = "Physical Development";

$default_oea["01726"]["ItemCode"] = "01726";
$default_oea["01726"]["ItemName"] = "Portraits of Marine Life Art Competition  海洋生物面面觀 - 藝術創作比賽";
$default_oea["01726"]["CatCode"] = "4";
$default_oea["01726"]["CatName"] = "Aesthetic Development";

$default_oea["01727"]["ItemCode"] = "01727";
$default_oea["01727"]["ItemName"] = "Positive Family in Ma On Shan";
$default_oea["01727"]["CatCode"] = "5";
$default_oea["01727"]["CatName"] = "Moral and Civic Education";

$default_oea["01728"]["ItemCode"] = "01728";
$default_oea["01728"]["ItemName"] = "Poster Design廣告設計";
$default_oea["01728"]["CatCode"] = "4";
$default_oea["01728"]["CatName"] = "Aesthetic Development";

$default_oea["01729"]["ItemCode"] = "01729";
$default_oea["01729"]["ItemName"] = "Practical Accounting for SMEsTaster Program 中小企實用電腦會計導引課程";
$default_oea["01729"]["CatCode"] = "2";
$default_oea["01729"]["CatName"] = "Career-related Experience";

$default_oea["01730"]["ItemCode"] = "01730";
$default_oea["01730"]["ItemName"] = "Praise the parent slogan writing competition";
$default_oea["01730"]["CatCode"] = "4";
$default_oea["01730"]["CatName"] = "Aesthetic Development";

$default_oea["01732"]["ItemCode"] = "01732";
$default_oea["01732"]["ItemName"] = "Prefect Team";
$default_oea["01732"]["CatCode"] = "1";
$default_oea["01732"]["CatName"] = "Community Service";

$default_oea["01733"]["ItemCode"] = "01733";
$default_oea["01733"]["ItemName"] = "Preparation for heABWE Shing Yan Christian Social Service  Carnival  盛恩中心社區嘉年華籌備";
$default_oea["01733"]["CatCode"] = "1";
$default_oea["01733"]["CatName"] = "Community Service";

$default_oea["01734"]["ItemCode"] = "01734";
$default_oea["01734"]["ItemName"] = "President Cup International Open Bowling Championships  國際總督杯保齡球公開賽";
$default_oea["01734"]["CatCode"] = "3";
$default_oea["01734"]["CatName"] = "Physical Development";

$default_oea["01735"]["ItemCode"] = "01735";
$default_oea["01735"]["ItemName"] = "President's Cup Youth Team Championship  會長杯青年組錦標賽(世界)";
$default_oea["01735"]["CatCode"] = "3";
$default_oea["01735"]["CatName"] = "Physical Development";

$default_oea["01736"]["ItemCode"] = "01736";
$default_oea["01736"]["ItemName"] = "Prevention of Internet Bullying Workshop  ";
$default_oea["01736"]["CatCode"] = "5";
$default_oea["01736"]["CatName"] = "Moral and Civic Education";

$default_oea["01737"]["ItemCode"] = "01737";
$default_oea["01737"]["ItemName"] = "Primary Children's Painting Competition  全國少兒美術杯書畫大賽";
$default_oea["01737"]["CatCode"] = "4";
$default_oea["01737"]["CatName"] = "Aesthetic Development";

$default_oea["01738"]["ItemCode"] = "01738";
$default_oea["01738"]["ItemName"] = "Prime Gymnastics International Invitation Competition";
$default_oea["01738"]["CatCode"] = "3";
$default_oea["01738"]["CatName"] = "Physical Development";

$default_oea["01739"]["ItemCode"] = "01739";
$default_oea["01739"]["ItemName"] = "Prime International Gymnastics Competition  新加坡體操錦標賽";
$default_oea["01739"]["CatCode"] = "3";
$default_oea["01739"]["CatName"] = "Physical Development";

$default_oea["01740"]["ItemCode"] = "01740";
$default_oea["01740"]["ItemName"] = "Prince Cup Cross-Strait Calligraphy and Painting Exhibition  王子盃海峽兩岸書畫大展";
$default_oea["01740"]["CatCode"] = "4";
$default_oea["01740"]["CatName"] = "Aesthetic Development";

$default_oea["01741"]["ItemCode"] = "01741";
$default_oea["01741"]["ItemName"] = "Program For Future Scientists  明天小小科學家獎勵活動";
$default_oea["01741"]["CatCode"] = "2";
$default_oea["01741"]["CatName"] = "Career-related Experience";

$default_oea["01742"]["ItemCode"] = "01742";
$default_oea["01742"]["ItemName"] = "Project Lotus Talent Search";
$default_oea["01742"]["CatCode"] = "4";
$default_oea["01742"]["CatName"] = "Aesthetic Development";

$default_oea["01743"]["ItemCode"] = "01743";
$default_oea["01743"]["ItemName"] = "Project Show for Future Engineers, World Engineers' Convention  世界工程師大會-未來工程師聯展";
$default_oea["01743"]["CatCode"] = "2";
$default_oea["01743"]["CatName"] = "Career-related Experience";

$default_oea["01744"]["ItemCode"] = "01744";
$default_oea["01744"]["ItemName"] = "Protection of Endangered Species Sand Sculpture Competition - Agriculture, Fisheries & Conservation Department  保護瀕危動植物堆沙比賽 - 漁農自然護理署";
$default_oea["01744"]["CatCode"] = "4";
$default_oea["01744"]["CatName"] = "Aesthetic Development";

$default_oea["01731"]["ItemCode"] = "01731";
$default_oea["01731"]["ItemName"] = "PRC Youth Arts and Calligraphy Invitation Campaign";
$default_oea["01731"]["CatCode"] = "4";
$default_oea["01731"]["CatName"] = "Aesthetic Development";

$default_oea["01745"]["ItemCode"] = "01745";
$default_oea["01745"]["ItemName"] = "Public Speaking Contest (Senior Secondary Section) - Pui Ching Education Centre & Asian Association for Lifelong Learning";
$default_oea["01745"]["CatCode"] = "4";
$default_oea["01745"]["CatName"] = "Aesthetic Development";

$default_oea["01746"]["ItemCode"] = "01746";
$default_oea["01746"]["ItemName"] = "Pui Ching Invitational Mathematics Competition  培正數學邀請賽";
$default_oea["01746"]["CatCode"] = "2";
$default_oea["01746"]["CatName"] = "Career-related Experience";

$default_oea["01747"]["ItemCode"] = "01747";
$default_oea["01747"]["ItemName"] = "Puppet and shadow art show";
$default_oea["01747"]["CatCode"] = "4";
$default_oea["01747"]["CatName"] = "Aesthetic Development";

$default_oea["01748"]["ItemCode"] = "01748";
$default_oea["01748"]["ItemName"] = "Purchasing and merchadising-taster program 採購及營銷導引課程";
$default_oea["01748"]["CatCode"] = "2";
$default_oea["01748"]["CatName"] = "Career-related Experience";

$default_oea["01749"]["ItemCode"] = "01749";
$default_oea["01749"]["ItemName"] = "Pyramid Bowl International Junior All-Star Championships  金字塔保齡球國際青少年錦標賽";
$default_oea["01749"]["CatCode"] = "3";
$default_oea["01749"]["CatName"] = "Physical Development";

$default_oea["01750"]["ItemCode"] = "01750";
$default_oea["01750"]["ItemName"] = "Qingdao International Judo Tournament  青島國際柔道公開賽";
$default_oea["01750"]["CatCode"] = "3";
$default_oea["01750"]["CatName"] = "Physical Development";

$default_oea["01751"]["ItemCode"] = "01751";
$default_oea["01751"]["ItemName"] = "Quadrangular Wheelchair Meet  台港穗澳輪椅四角賽";
$default_oea["01751"]["CatCode"] = "3";
$default_oea["01751"]["CatName"] = "Physical Development";

$default_oea["01752"]["ItemCode"] = "01752";
$default_oea["01752"]["ItemName"] = "Quality Education Alliance 優質教育聯盟";
$default_oea["01752"]["CatCode"] = "2";
$default_oea["01752"]["CatName"] = "Career-related Experience";

$default_oea["01753"]["ItemCode"] = "01753";
$default_oea["01753"]["ItemName"] = "Quality Education Cup National Calligraphy and Painting Competition for Primary and Secondary Schools  素質教育杯全國中小學書畫大賽";
$default_oea["01753"]["CatCode"] = "4";
$default_oea["01753"]["CatName"] = "Aesthetic Development";

$default_oea["01754"]["ItemCode"] = "01754";
$default_oea["01754"]["ItemName"] = "Queensland Invitational Swimming Championships Brisbane, Australia  澳洲昆士蘭游泳錦標賽";
$default_oea["01754"]["CatCode"] = "3";
$default_oea["01754"]["CatName"] = "Physical Development";

$default_oea["01755"]["ItemCode"] = "01755";
$default_oea["01755"]["ItemName"] = "Queensland Open & Age Group State Championships  澳洲昆士蘭公開及分齡游泳錦標賽";
$default_oea["01755"]["CatCode"] = "3";
$default_oea["01755"]["CatName"] = "Physical Development";

$default_oea["01756"]["ItemCode"] = "01756";
$default_oea["01756"]["ItemName"] = "Queensland Open and Age Championships";
$default_oea["01756"]["CatCode"] = "3";
$default_oea["01756"]["CatName"] = "Physical Development";

$default_oea["01757"]["ItemCode"] = "01757";
$default_oea["01757"]["ItemName"] = "Queensland Orienteering Championship  澳洲昆士蘭野外定向錦標賽";
$default_oea["01757"]["CatCode"] = "3";
$default_oea["01757"]["CatName"] = "Physical Development";

$default_oea["01758"]["ItemCode"] = "01758";
$default_oea["01758"]["ItemName"] = "Quiksilver Beach Volleyball Teams Cup";
$default_oea["01758"]["CatCode"] = "3";
$default_oea["01758"]["CatName"] = "Physical Development";

$default_oea["01759"]["ItemCode"] = "01759";
$default_oea["01759"]["ItemName"] = "Red Ribbon Concert 2010 紅絲帶‧愛‧同行2010音樂會";
$default_oea["01759"]["CatCode"] = "1";
$default_oea["01759"]["CatName"] = "Community Service";

$default_oea["01760"]["ItemCode"] = "01760";
$default_oea["01760"]["ItemName"] = "Refined Collection Cup Chinese Calligraphy and Painting Competition  匯雅杯中國書畫大賽";
$default_oea["01760"]["CatCode"] = "4";
$default_oea["01760"]["CatName"] = "Aesthetic Development";

$default_oea["01761"]["ItemCode"] = "01761";
$default_oea["01761"]["ItemName"] = "Regional Painting Competition (China)";
$default_oea["01761"]["CatCode"] = "4";
$default_oea["01761"]["CatName"] = "Aesthetic Development";

$default_oea["01762"]["ItemCode"] = "01762";
$default_oea["01762"]["ItemName"] = "Relay race (Inter-school)友校接力邀請賽";
$default_oea["01762"]["CatCode"] = "3";
$default_oea["01762"]["CatName"] = "Physical Development";

$default_oea["01763"]["ItemCode"] = "01763";
$default_oea["01763"]["ItemName"] = "Relay Race in Regional Canoe Race";
$default_oea["01763"]["CatCode"] = "3";
$default_oea["01763"]["CatName"] = "Physical Development";

$default_oea["01764"]["ItemCode"] = "01764";
$default_oea["01764"]["ItemName"] = "Research on Historical Photos Writing Competition  歷史照片研究比賽";
$default_oea["01764"]["CatCode"] = "4";
$default_oea["01764"]["CatName"] = "Aesthetic Development";

$default_oea["01765"]["ItemCode"] = "01765";
$default_oea["01765"]["ItemName"] = "Respect Our Teachers - Card Design Competition敬師咭設計比賽";
$default_oea["01765"]["CatCode"] = "4";
$default_oea["01765"]["CatName"] = "Aesthetic Development";

$default_oea["01766"]["ItemCode"] = "01766";
$default_oea["01766"]["ItemName"] = "Retail Management-Taster Program 零售管理-認識零售營銷力量導引課程";
$default_oea["01766"]["CatCode"] = "2";
$default_oea["01766"]["CatName"] = "Career-related Experience";

$default_oea["01767"]["ItemCode"] = "01767";
$default_oea["01767"]["ItemName"] = "Rev. P. Wong Memorial Scholarship";
$default_oea["01767"]["CatCode"] = "2";
$default_oea["01767"]["CatName"] = "Career-related Experience";

$default_oea["01768"]["ItemCode"] = "01768";
$default_oea["01768"]["ItemName"] = "Rhythmic Gymnastics Age Group Competition 全港藝術體操分齡比賽";
$default_oea["01768"]["CatCode"] = "3";
$default_oea["01768"]["CatName"] = "Physical Development";

$default_oea["01769"]["ItemCode"] = "01769";
$default_oea["01769"]["ItemName"] = "Rhythmic Gymnastics China National Championship  全國藝術體操錦標賽";
$default_oea["01769"]["CatCode"] = "3";
$default_oea["01769"]["CatName"] = "Physical Development";

$default_oea["01770"]["ItemCode"] = "01770";
$default_oea["01770"]["ItemName"] = "Rich Heart Wealthy Mind (Life Experience Learning)";
$default_oea["01770"]["CatCode"] = "5";
$default_oea["01770"]["CatName"] = "Moral and Civic Education";

$default_oea["01771"]["ItemCode"] = "01771";
$default_oea["01771"]["ItemName"] = "River of Words International Environmental Poetry & Art Contest";
$default_oea["01771"]["CatCode"] = "4";
$default_oea["01771"]["CatName"] = "Aesthetic Development";

$default_oea["01772"]["ItemCode"] = "01772";
$default_oea["01772"]["ItemName"] = "Road Safety Drawing Competition道路安全填色及繪畫比賽";
$default_oea["01772"]["CatCode"] = "4";
$default_oea["01772"]["CatName"] = "Aesthetic Development";

$default_oea["01773"]["ItemCode"] = "01773";
$default_oea["01773"]["ItemName"] = "Road Safety Poster and Slogan Design Competition (Secondary School Section) - Sham Shui Po Road Safety Campaign";
$default_oea["01773"]["CatCode"] = "4";
$default_oea["01773"]["CatName"] = "Aesthetic Development";

$default_oea["01774"]["ItemCode"] = "01774";
$default_oea["01774"]["ItemName"] = "RoboCup Atlanta at the Georgia Institute of Technology";
$default_oea["01774"]["CatCode"] = "2";
$default_oea["01774"]["CatName"] = "Career-related Experience";

$default_oea["01775"]["ItemCode"] = "01775";
$default_oea["01775"]["ItemName"] = "RoboCup China Open  中國機器人大賽暨RoboCup公開賽";
$default_oea["01775"]["CatCode"] = "2";
$default_oea["01775"]["CatName"] = "Career-related Experience";

$default_oea["01776"]["ItemCode"] = "01776";
$default_oea["01776"]["ItemName"] = "RoboCup International Competition";
$default_oea["01776"]["CatCode"] = "2";
$default_oea["01776"]["CatName"] = "Career-related Experience";

$default_oea["01777"]["ItemCode"] = "01777";
$default_oea["01777"]["ItemName"] = "RoboCup World Championship";
$default_oea["01777"]["CatCode"] = "2";
$default_oea["01777"]["CatName"] = "Career-related Experience";

$default_oea["01778"]["ItemCode"] = "01778";
$default_oea["01778"]["ItemName"] = "RoboCupJunior Greater China Competition  青少年機器人世界杯大中華選拔大賽";
$default_oea["01778"]["CatCode"] = "2";
$default_oea["01778"]["CatName"] = "Career-related Experience";

$default_oea["01779"]["ItemCode"] = "01779";
$default_oea["01779"]["ItemName"] = "RoboCupJunior Greater China Open Competition  青少年機器人世界盃大中華區公開選拔大賽";
$default_oea["01779"]["CatCode"] = "2";
$default_oea["01779"]["CatName"] = "Career-related Experience";

$default_oea["01780"]["ItemCode"] = "01780";
$default_oea["01780"]["ItemName"] = "RoboCupJunior Hong Kong";
$default_oea["01780"]["CatCode"] = "2";
$default_oea["01780"]["CatName"] = "Career-related Experience";

$default_oea["01781"]["ItemCode"] = "01781";
$default_oea["01781"]["ItemName"] = "RoboCupJunior International Competition";
$default_oea["01781"]["CatCode"] = "2";
$default_oea["01781"]["CatName"] = "Career-related Experience";

$default_oea["01782"]["ItemCode"] = "01782";
$default_oea["01782"]["ItemName"] = "RoboCup機械人世界盃中國公開賽";
$default_oea["01782"]["CatCode"] = "2";
$default_oea["01782"]["CatName"] = "Career-related Experience";

$default_oea["01783"]["ItemCode"] = "01783";
$default_oea["01783"]["ItemName"] = "Robot Appearance Design Competition in Shanghai Interport Land-based Robot Olympic";
$default_oea["01783"]["CatCode"] = "2";
$default_oea["01783"]["CatName"] = "Career-related Experience";

$default_oea["01784"]["ItemCode"] = "01784";
$default_oea["01784"]["ItemName"] = "Robotic Olympic  機械奧運會";
$default_oea["01784"]["CatCode"] = "2";
$default_oea["01784"]["CatName"] = "Career-related Experience";

$default_oea["01785"]["ItemCode"] = "01785";
$default_oea["01785"]["ItemName"] = "Roller Skating Tournament - Guangdong-Hong Kong  粵港輪滑邀請賽";
$default_oea["01785"]["CatCode"] = "3";
$default_oea["01785"]["CatName"] = "Physical Development";

$default_oea["01786"]["ItemCode"] = "01786";
$default_oea["01786"]["ItemName"] = "Roller Speed Skating World Championships  世界速度輪滑錦標賽";
$default_oea["01786"]["CatCode"] = "3";
$default_oea["01786"]["CatName"] = "Physical Development";

$default_oea["01787"]["ItemCode"] = "01787";
$default_oea["01787"]["ItemName"] = "Rope Skipping Competition - Leisure and Cultural Services Department & Hong Kong Rope Skipping Association";
$default_oea["01787"]["CatCode"] = "3";
$default_oea["01787"]["CatName"] = "Physical Development";

$default_oea["01788"]["ItemCode"] = "01788";
$default_oea["01788"]["ItemName"] = "Rotary 10K Race";
$default_oea["01788"]["CatCode"] = "3";
$default_oea["01788"]["CatName"] = "Physical Development";

$default_oea["01789"]["ItemCode"] = "01789";
$default_oea["01789"]["ItemName"] = "Rotary Anti-Drug Festival扶輪禁毒盃";
$default_oea["01789"]["CatCode"] = "1";
$default_oea["01789"]["CatName"] = "Community Service";

$default_oea["01790"]["ItemCode"] = "01790";
$default_oea["01790"]["ItemName"] = "Rotary International District 3450 - Best Interactor Award / Youth Leadership Award";
$default_oea["01790"]["CatCode"] = "5";
$default_oea["01790"]["CatName"] = "Moral and Civic Education";

$default_oea["01791"]["ItemCode"] = "01791";
$default_oea["01791"]["ItemName"] = "Rover Scout Award  樂行童軍獎章";
$default_oea["01791"]["CatCode"] = "1";
$default_oea["01791"]["CatName"] = "Community Service";

$default_oea["01792"]["ItemCode"] = "01792";
$default_oea["01792"]["ItemName"] = "Royal Academy of Dancing";
$default_oea["01792"]["CatCode"] = "4";
$default_oea["01792"]["CatName"] = "Aesthetic Development";

$default_oea["01793"]["ItemCode"] = "01793";
$default_oea["01793"]["ItemName"] = "Royal Australian Chemical Institute Australian National Chemistry Quiz (Hong Kong Section)  澳洲皇家化學學院澳洲化學測試 (香港區)";
$default_oea["01793"]["CatCode"] = "2";
$default_oea["01793"]["CatName"] = "Career-related Experience";

$default_oea["01794"]["ItemCode"] = "01794";
$default_oea["01794"]["ItemName"] = "RS:X Asian Championships  RS:X亞洲滑浪風帆錦標賽";
$default_oea["01794"]["CatCode"] = "3";
$default_oea["01794"]["CatName"] = "Physical Development";

$default_oea["01795"]["ItemCode"] = "01795";
$default_oea["01795"]["ItemName"] = "RTHK Teen Time Recording Session";
$default_oea["01795"]["CatCode"] = "4";
$default_oea["01795"]["CatName"] = "Aesthetic Development";

$default_oea["02078"]["ItemCode"] = "02078";
$default_oea["02078"]["ItemName"] = "sportAct Award Scheme  sportACT獎勵計劃";
$default_oea["02078"]["CatCode"] = "3";
$default_oea["02078"]["CatName"] = "Physical Development";

$default_oea["01796"]["ItemCode"] = "01796";
$default_oea["01796"]["ItemName"] = "S1 Orientation Programme 中一新生指導日";
$default_oea["01796"]["CatCode"] = "5";
$default_oea["01796"]["CatName"] = "Moral and Civic Education";

$default_oea["01798"]["ItemCode"] = "01798";
$default_oea["01798"]["ItemName"] = "Sail Melbourne International Regatta  墨爾本國際風帆大賽";
$default_oea["01798"]["CatCode"] = "3";
$default_oea["01798"]["CatName"] = "Physical Development";

$default_oea["01800"]["ItemCode"] = "01800";
$default_oea["01800"]["ItemName"] = "Sam Ying Table Tennis Invitational Tournament  三英盃乒乓球邀請賽";
$default_oea["01800"]["CatCode"] = "3";
$default_oea["01800"]["CatName"] = "Physical Development";

$default_oea["01801"]["ItemCode"] = "01801";
$default_oea["01801"]["ItemName"] = "Samila Asian Beach Volleyball Tour";
$default_oea["01801"]["CatCode"] = "3";
$default_oea["01801"]["CatName"] = "Physical Development";

$default_oea["01802"]["ItemCode"] = "01802";
$default_oea["01802"]["ItemName"] = "Sand Building Competition  香港東區中、小學堆沙大賽";
$default_oea["01802"]["CatCode"] = "4";
$default_oea["01802"]["CatName"] = "Aesthetic Development";

$default_oea["01803"]["ItemCode"] = "01803";
$default_oea["01803"]["ItemName"] = "Sanitation Design Competition (HK)國際環境衛生年美術設計創作比賽";
$default_oea["01803"]["CatCode"] = "4";
$default_oea["01803"]["CatName"] = "Aesthetic Development";

$default_oea["01804"]["ItemCode"] = "01804";
$default_oea["01804"]["ItemName"] = "Say No to Drugs Game Stall Design Competition荃家全心躍動荃城最佳抗毒攤位遊戲比賽";
$default_oea["01804"]["CatCode"] = "4";
$default_oea["01804"]["CatName"] = "Aesthetic Development";

$default_oea["01797"]["ItemCode"] = "01797";
$default_oea["01797"]["ItemName"] = "SACAC(Singapore and American Community Action Council) Gymfest Open Championship  SACAC體操公開賽";
$default_oea["01797"]["CatCode"] = "3";
$default_oea["01797"]["CatName"] = "Physical Development";

$default_oea["01799"]["ItemCode"] = "01799";
$default_oea["01799"]["ItemName"] = "SALC Target English Programme";
$default_oea["01799"]["CatCode"] = "4";
$default_oea["01799"]["CatName"] = "Aesthetic Development";

$default_oea["01805"]["ItemCode"] = "01805";
$default_oea["01805"]["ItemName"] = "Scheme on Personal Growth and Development自愛自律成長教育計劃";
$default_oea["01805"]["CatCode"] = "5";
$default_oea["01805"]["CatName"] = "Moral and Civic Education";

$default_oea["01806"]["ItemCode"] = "01806";
$default_oea["01806"]["ItemName"] = "Scholarship of Service";
$default_oea["01806"]["CatCode"] = "2";
$default_oea["01806"]["CatName"] = "Career-related Experience";

$default_oea["01807"]["ItemCode"] = "01807";
$default_oea["01807"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Christmas Activities (Please provide details in \"Description\" box.)";
$default_oea["01807"]["CatCode"] = "4";
$default_oea["01807"]["CatName"] = "Aesthetic Development";

$default_oea["01808"]["ItemCode"] = "01808";
$default_oea["01808"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Class Association 班會 / Class Monitors/Class Prefect 班長 (Please provide details in \"Description\" box.)";
$default_oea["01808"]["CatCode"] = "1";
$default_oea["01808"]["CatName"] = "Community Service";

$default_oea["01809"]["ItemCode"] = "01809";
$default_oea["01809"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Cross Country Team (Please provide details in \"Description\" box.)";
$default_oea["01809"]["CatCode"] = "3";
$default_oea["01809"]["CatName"] = "Physical Development";

$default_oea["01810"]["ItemCode"] = "01810";
$default_oea["01810"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Debate Team辯論隊 (Please provide details in \"Description\" box.)";
$default_oea["01810"]["CatCode"] = "2";
$default_oea["01810"]["CatName"] = "Career-related Experience";

$default_oea["01811"]["ItemCode"] = "01811";
$default_oea["01811"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Debating Society辯論學會 (Please provide details in \"Description\" box)";
$default_oea["01811"]["CatCode"] = "2";
$default_oea["01811"]["CatName"] = "Career-related Experience";

$default_oea["01812"]["ItemCode"] = "01812";
$default_oea["01812"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Ambassadors (Please provide details in \"Description\" box.)";
$default_oea["01812"]["CatCode"] = "4";
$default_oea["01812"]["CatName"] = "Aesthetic Development";

$default_oea["01813"]["ItemCode"] = "01813";
$default_oea["01813"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Camp (Please provide details in \"Description\" box.)";
$default_oea["01813"]["CatCode"] = "4";
$default_oea["01813"]["CatName"] = "Aesthetic Development";

$default_oea["01814"]["ItemCode"] = "01814";
$default_oea["01814"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Debating Team  英文辯論比賽 / English Debating Team Training 英語辯論隊隊員培訓 (Please provide details in \"Description\" box.)";
$default_oea["01814"]["CatCode"] = "5";
$default_oea["01814"]["CatName"] = "Moral and Civic Education";

$default_oea["01815"]["ItemCode"] = "01815";
$default_oea["01815"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Drama Club (School)  英文戲劇學會 (Please provide details in \"Description\" box.)";
$default_oea["01815"]["CatCode"] = "4";
$default_oea["01815"]["CatName"] = "Aesthetic Development";

$default_oea["01816"]["ItemCode"] = "01816";
$default_oea["01816"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Drama Festival 英文戲劇節 (Please provide details in \"Description\" box.)";
$default_oea["01816"]["CatCode"] = "4";
$default_oea["01816"]["CatName"] = "Aesthetic Development";

$default_oea["01817"]["ItemCode"] = "01817";
$default_oea["01817"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English musical (Please provide details in \"Description\" box.)";
$default_oea["01817"]["CatCode"] = "4";
$default_oea["01817"]["CatName"] = "Aesthetic Development";

$default_oea["01818"]["ItemCode"] = "01818";
$default_oea["01818"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  English Speech Festival 英文演講節 (Please provide details in \"Description\" box.)";
$default_oea["01818"]["CatCode"] = "4";
$default_oea["01818"]["CatName"] = "Aesthetic Development";

$default_oea["01819"]["ItemCode"] = "01819";
$default_oea["01819"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Excursion Club見聞社 (Please provide details in \"Description\" box.)";
$default_oea["01819"]["CatCode"] = "4";
$default_oea["01819"]["CatName"] = "Aesthetic Development";

$default_oea["01820"]["ItemCode"] = "01820";
$default_oea["01820"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Fencing Class劍擊班 (Please provide details in \"Description\" box.)";
$default_oea["01820"]["CatCode"] = "3";
$default_oea["01820"]["CatName"] = "Physical Development";

$default_oea["01821"]["ItemCode"] = "01821";
$default_oea["01821"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Fencing School Teams  劍擊校隊 (Please provide details in \"Description\" box.)";
$default_oea["01821"]["CatCode"] = "3";
$default_oea["01821"]["CatName"] = "Physical Development";

$default_oea["01822"]["ItemCode"] = "01822";
$default_oea["01822"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Flag-raising Team升旗隊 (Please provide details in \"Description\" box.)";
$default_oea["01822"]["CatCode"] = "5";
$default_oea["01822"]["CatName"] = "Moral and Civic Education";

$default_oea["01823"]["ItemCode"] = "01823";
$default_oea["01823"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Football Team學校足球隊 (Please provide details in \"Description\" box.)";
$default_oea["01823"]["CatCode"] = "4";
$default_oea["01823"]["CatName"] = "Aesthetic Development";

$default_oea["01824"]["ItemCode"] = "01824";
$default_oea["01824"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  French Drama Festival 法文戲劇節 (Please provide details in \"Description\" box.)";
$default_oea["01824"]["CatCode"] = "4";
$default_oea["01824"]["CatName"] = "Aesthetic Development";

$default_oea["01825"]["ItemCode"] = "01825";
$default_oea["01825"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Handball Team 手球隊 (Please provide details in \"Description\" box.)";
$default_oea["01825"]["CatCode"] = "3";
$default_oea["01825"]["CatName"] = "Physical Development";

$default_oea["01826"]["ItemCode"] = "01826";
$default_oea["01826"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Handbell Ringers手鈴隊 (Please provide details in \"Description\" box.)";
$default_oea["01826"]["CatCode"] = "4";
$default_oea["01826"]["CatName"] = "Aesthetic Development";

$default_oea["01827"]["ItemCode"] = "01827";
$default_oea["01827"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Head Counseling Prefect (Please provide details in \"Description\" box.)";
$default_oea["01827"]["CatCode"] = "1";
$default_oea["01827"]["CatName"] = "Community Service";

$default_oea["01828"]["ItemCode"] = "01828";
$default_oea["01828"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Head Prefect (Please provide details in \"Description\" box.)";
$default_oea["01828"]["CatCode"] = "1";
$default_oea["01828"]["CatName"] = "Community Service";

$default_oea["01829"]["ItemCode"] = "01829";
$default_oea["01829"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Intellect House (Please provide details in \"Description\" box.)";
$default_oea["01829"]["CatCode"] = "1";
$default_oea["01829"]["CatName"] = "Community Service";

$default_oea["01830"]["ItemCode"] = "01830";
$default_oea["01830"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Liberal Studies  通識 (Please provide details in \"Description\" box.)";
$default_oea["01830"]["CatCode"] = "5";
$default_oea["01830"]["CatName"] = "Moral and Civic Education";

$default_oea["01831"]["ItemCode"] = "01831";
$default_oea["01831"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Librarian Team 圖書管理員 (Please provide details in \"Description\" box.)";
$default_oea["01831"]["CatCode"] = "1";
$default_oea["01831"]["CatName"] = "Community Service";

$default_oea["01832"]["ItemCode"] = "01832";
$default_oea["01832"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Long-distance Running Team (Please provide details in \"Description\" box.)";
$default_oea["01832"]["CatCode"] = "3";
$default_oea["01832"]["CatName"] = "Physical Development";

$default_oea["01833"]["ItemCode"] = "01833";
$default_oea["01833"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Mannequin Costume Creation (Please provide details in \"Description\" box.)";
$default_oea["01833"]["CatCode"] = "4";
$default_oea["01833"]["CatName"] = "Aesthetic Development";

$default_oea["01835"]["ItemCode"] = "01835";
$default_oea["01835"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Mentorship Scheme (Please provide details in \"Description\" box.)";
$default_oea["01835"]["CatCode"] = "2";
$default_oea["01835"]["CatName"] = "Career-related Experience";

$default_oea["01836"]["ItemCode"] = "01836";
$default_oea["01836"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Music Scheme校內音樂展能計劃 (Please provide details in \"Description\" box.)";
$default_oea["01836"]["CatCode"] = "4";
$default_oea["01836"]["CatName"] = "Aesthetic Development";

$default_oea["01837"]["ItemCode"] = "01837";
$default_oea["01837"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Netball Team 投球隊(Please provide details in \"Description\" box.)";
$default_oea["01837"]["CatCode"] = "3";
$default_oea["01837"]["CatName"] = "Physical Development";

$default_oea["01838"]["ItemCode"] = "01838";
$default_oea["01838"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Newspaper Team (Please provide details in \"Description\" box.)";
$default_oea["01838"]["CatCode"] = "2";
$default_oea["01838"]["CatName"] = "Career-related Experience";

$default_oea["01839"]["ItemCode"] = "01839";
$default_oea["01839"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  OLE Prefect (Please provide details in \"Description\" box.)";
$default_oea["01839"]["CatCode"] = "1";
$default_oea["01839"]["CatName"] = "Community Service";

$default_oea["01840"]["ItemCode"] = "01840";
$default_oea["01840"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Reading Scheme 閱讀計劃 (Please provide details in \"Description\" box.)";
$default_oea["01840"]["CatCode"] = "4";
$default_oea["01840"]["CatName"] = "Aesthetic Development";

$default_oea["01841"]["ItemCode"] = "01841";
$default_oea["01841"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  School Team校隊 (Please provide details in \"Description\" box.)";
$default_oea["01841"]["CatCode"] = "4";
$default_oea["01841"]["CatName"] = "Aesthetic Development";

$default_oea["01843"]["ItemCode"] = "01843";
$default_oea["01843"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Softball Team 壘球隊 (Please provide details in \"Description\" box.)";
$default_oea["01843"]["CatCode"] = "3";
$default_oea["01843"]["CatName"] = "Physical Development";

$default_oea["01844"]["ItemCode"] = "01844";
$default_oea["01844"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Squash School Teams  壁球校隊 (Please provide details in \"Description\" box.)";
$default_oea["01844"]["CatCode"] = "3";
$default_oea["01844"]["CatName"] = "Physical Development";

$default_oea["01845"]["ItemCode"] = "01845";
$default_oea["01845"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Student I.T. Team Leaders (Please provide details in \"Description\" box.)";
$default_oea["01845"]["CatCode"] = "1";
$default_oea["01845"]["CatName"] = "Community Service";

$default_oea["01846"]["ItemCode"] = "01846";
$default_oea["01846"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Symphonic Band管樂團 (Please provide details in \"Description\" box.)";
$default_oea["01846"]["CatCode"] = "4";
$default_oea["01846"]["CatName"] = "Aesthetic Development";

$default_oea["01847"]["ItemCode"] = "01847";
$default_oea["01847"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Talent Class(Please provide details in \"Description\" box.)";
$default_oea["01847"]["CatCode"] = "4";
$default_oea["01847"]["CatName"] = "Aesthetic Development";

$default_oea["01848"]["ItemCode"] = "01848";
$default_oea["01848"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Track-and-field School Team (Please provide details in \"Description\" box.)";
$default_oea["01848"]["CatCode"] = "3";
$default_oea["01848"]["CatName"] = "Physical Development";

$default_oea["01849"]["ItemCode"] = "01849";
$default_oea["01849"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  Virtue House (Please provide details in \"Description\" box.)";
$default_oea["01849"]["CatCode"] = "1";
$default_oea["01849"]["CatName"] = "Community Service";

$default_oea["01850"]["ItemCode"] = "01850";
$default_oea["01850"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  中文科中一伴讀員 (Please provide details in \"Description\" box.)";
$default_oea["01850"]["CatCode"] = "1";
$default_oea["01850"]["CatName"] = "Community Service";

$default_oea["01853"]["ItemCode"] = "01853";
$default_oea["01853"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  書友會 (Please provide details in \"Description\" box.)";
$default_oea["01853"]["CatCode"] = "4";
$default_oea["01853"]["CatName"] = "Aesthetic Development";

$default_oea["01854"]["ItemCode"] = "01854";
$default_oea["01854"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  書法學會 (Please provide details in \"Description\" box.)";
$default_oea["01854"]["CatCode"] = "4";
$default_oea["01854"]["CatName"] = "Aesthetic Development";

$default_oea["01861"]["ItemCode"] = "01861";
$default_oea["01861"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  獨唱練習 / 獨唱伴奏練習 (Please provide details in \"Description\" box.)";
$default_oea["01861"]["CatCode"] = "4";
$default_oea["01861"]["CatName"] = "Aesthetic Development";

$default_oea["01857"]["ItemCode"] = "01857";
$default_oea["01857"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  結他小組 Guitar Group (Please provide details in \"Description\" box.)";
$default_oea["01857"]["CatCode"] = "4";
$default_oea["01857"]["CatName"] = "Aesthetic Development";

$default_oea["01858"]["ItemCode"] = "01858";
$default_oea["01858"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  結他樂團 (Please provide details in \"Description\" box.)";
$default_oea["01858"]["CatCode"] = "4";
$default_oea["01858"]["CatName"] = "Aesthetic Development";

$default_oea["01859"]["ItemCode"] = "01859";
$default_oea["01859"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  舞台設計 (Please provide details in \"Description\" box.)";
$default_oea["01859"]["CatCode"] = "4";
$default_oea["01859"]["CatName"] = "Aesthetic Development";

$default_oea["01860"]["ItemCode"] = "01860";
$default_oea["01860"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  舞蹈班 (Please provide details in \"Description\" box.)";
$default_oea["01860"]["CatCode"] = "4";
$default_oea["01860"]["CatName"] = "Aesthetic Development";

$default_oea["01851"]["ItemCode"] = "01851";
$default_oea["01851"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  英詩集誦隊 (Please provide details in \"Description\" box.)";
$default_oea["01851"]["CatCode"] = "4";
$default_oea["01851"]["CatName"] = "Aesthetic Development";

$default_oea["01852"]["ItemCode"] = "01852";
$default_oea["01852"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  英語週 (Please provide details in \"Description\" box.)";
$default_oea["01852"]["CatCode"] = "4";
$default_oea["01852"]["CatName"] = "Aesthetic Development";

$default_oea["01855"]["ItemCode"] = "01855";
$default_oea["01855"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  訓導組 (Please provide details in \"Description\" box.)";
$default_oea["01855"]["CatCode"] = "5";
$default_oea["01855"]["CatName"] = "Moral and Civic Education";

$default_oea["01862"]["ItemCode"] = "01862";
$default_oea["01862"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  錄像課程  (Please provide details in \"Description\" box.)";
$default_oea["01862"]["CatCode"] = "4";
$default_oea["01862"]["CatName"] = "Aesthetic Development";

$default_oea["01863"]["ItemCode"] = "01863";
$default_oea["01863"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 -  魔術表演班 (Please provide details in \"Description\" box.)";
$default_oea["01863"]["CatCode"] = "4";
$default_oea["01863"]["CatName"] = "Aesthetic Development";

$default_oea["01864"]["ItemCode"] = "01864";
$default_oea["01864"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Accounting 會計 / Auditing 審計 / Business 商業 / Economics 經濟 / Finance 金融 / Commercial 商務 (Please provide details in \"Description\" box.)";
$default_oea["01864"]["CatCode"] = "4";
$default_oea["01864"]["CatName"] = "Aesthetic Development";

$default_oea["01865"]["ItemCode"] = "01865";
$default_oea["01865"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Adventure 歷奇探索 / War Game 野戰 (Please provide details in \"Description\" box.)";
$default_oea["01865"]["CatCode"] = "3";
$default_oea["01865"]["CatName"] = "Physical Development";

$default_oea["01866"]["ItemCode"] = "01866";
$default_oea["01866"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Aesthetic 藝術 / Philosophy 哲學 / Literature 文學 / Reading 閱讀  (Please provide details in \"Description\" box.)";
$default_oea["01866"]["CatCode"] = "4";
$default_oea["01866"]["CatName"] = "Aesthetic Development";

$default_oea["01867"]["ItemCode"] = "01867";
$default_oea["01867"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Art 藝術 / Ceramics/Pottery 陶藝 / Painting 繪畫 / Design設計 / Sculpture 雕塑 / Seal Engraving 篆刻 (Please provide details in \"Description\" box)";
$default_oea["01867"]["CatCode"] = "4";
$default_oea["01867"]["CatName"] = "Aesthetic Development";

$default_oea["01868"]["ItemCode"] = "01868";
$default_oea["01868"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Astronomy 天文 / Aerospace 航天 / Star Gazing 觀星 (Please provide details in \"Description\" box.)";
$default_oea["01868"]["CatCode"] = "2";
$default_oea["01868"]["CatName"] = "Career-related Experience";

$default_oea["01869"]["ItemCode"] = "01869";
$default_oea["01869"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Athletic Club田徑學會 / Sports 運動 / Swimming 游泳 (Please provide details in \"Description\" box.)";
$default_oea["01869"]["CatCode"] = "3";
$default_oea["01869"]["CatName"] = "Physical Development";

$default_oea["01870"]["ItemCode"] = "01870";
$default_oea["01870"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Badminton 羽毛球 / Volleyball 排球 / Basketball 籃球 / Basketball Instructor籃球指導員班 / Squash 壁球 / Table Tennis 乒乓球 / Football/Soccer 足球 / Snooker 英式桌球 / Billiard 美式桌球  (Please provide details in \"Description\" box.)";
$default_oea["01870"]["CatCode"] = "3";
$default_oea["01870"]["CatName"] = "Physical Development";

$default_oea["01871"]["ItemCode"] = "01871";
$default_oea["01871"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Bowling 保齡球 / Ice Skating 溜冰 (Please provide details in \"Description\" box.)";
$default_oea["01871"]["CatCode"] = "3";
$default_oea["01871"]["CatName"] = "Physical Development";

$default_oea["01872"]["ItemCode"] = "01872";
$default_oea["01872"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Bridge 橋牌 / Chess 國際象棋/ Chinese Chess中國象棋 (Please provide details in \"Description\" box.)";
$default_oea["01872"]["CatCode"] = "4";
$default_oea["01872"]["CatName"] = "Aesthetic Development";

$default_oea["01856"]["ItemCode"] = "01856";
$default_oea["01856"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Cheerleader 啦啦隊隊長 (Please provide details in \"Description\" box.)";
$default_oea["01856"]["CatCode"] = "3";
$default_oea["01856"]["CatName"] = "Physical Development";

$default_oea["01873"]["ItemCode"] = "01873";
$default_oea["01873"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Chemistry 化學 / Biology 生物 / Physics 物理(Please provide details in \"Description\" box.)";
$default_oea["01873"]["CatCode"] = "4";
$default_oea["01873"]["CatName"] = "Aesthetic Development";

$default_oea["01874"]["ItemCode"] = "01874";
$default_oea["01874"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Comic 漫畫 / Cartoon/Animation 卡通/動畫 (Please provide details in \"Description\" box.)";
$default_oea["01874"]["CatCode"] = "4";
$default_oea["01874"]["CatName"] = "Aesthetic Development";

$default_oea["01875"]["ItemCode"] = "01875";
$default_oea["01875"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Current Affairs Society 時事學會 / Public Affairs 公共事務 / Politics 政治 / Social Science 社會科學  (Please provide details in \"Description\" box.)";
$default_oea["01875"]["CatCode"] = "5";
$default_oea["01875"]["CatName"] = "Moral and Civic Education";

$default_oea["01897"]["ItemCode"] = "01897";
$default_oea["01897"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Dance Club 舞蹈學會 / Performing Arts 表現藝術 / Folk Dance 民族舞 / Ballet 芭蕾舞  / Jazz 爵士 / Modern Jazz 現代爵士 / Hip Hop 嘻哈  (Please provide details in \"Description\" box.)";
$default_oea["01897"]["CatCode"] = "4";
$default_oea["01897"]["CatName"] = "Aesthetic Development";

$default_oea["01876"]["ItemCode"] = "01876";
$default_oea["01876"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Drama 戲劇/ Stage Management 舞台管理 / Costume Design 舞台服飾設計 / Lighting 燈光 / Audio 音響 / Script Writing 劇本寫作 (Please provide details in \"Description\" box.)";
$default_oea["01876"]["CatCode"] = "4";
$default_oea["01876"]["CatName"] = "Aesthetic Development";

$default_oea["01877"]["ItemCode"] = "01877";
$default_oea["01877"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - English 英文 / Chinese 中文 / Putonghua 普通話 / French 法文 / German德文 / Italian 意大利文 / Spanish 西班牙文 / Other Languages 其他語言 (Please provide details in \"Description\" box.)";
$default_oea["01877"]["CatCode"] = "4";
$default_oea["01877"]["CatName"] = "Aesthetic Development";

$default_oea["01878"]["ItemCode"] = "01878";
$default_oea["01878"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Film Watching/Appreciation 電影觀賞/欣賞 / Film Critic 電影評論 / Chinese Seal Cutting Class 篆刻研習 (Please provide details in \"Description\" box.)";
$default_oea["01878"]["CatCode"] = "4";
$default_oea["01878"]["CatName"] = "Aesthetic Development";

$default_oea["01879"]["ItemCode"] = "01879";
$default_oea["01879"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Fun Fair / Promotion Day 推廣日/ Carnival 嘉年華會  (Please provide details in \"Description\" box.)";
$default_oea["01879"]["CatCode"] = "4";
$default_oea["01879"]["CatName"] = "Aesthetic Development";

$default_oea["01880"]["ItemCode"] = "01880";
$default_oea["01880"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Gardening/Horticulture 園藝  (Please provide details in \"Description\" box.)";
$default_oea["01880"]["CatCode"] = "4";
$default_oea["01880"]["CatName"] = "Aesthetic Development";

$default_oea["01881"]["ItemCode"] = "01881";
$default_oea["01881"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Interest Class 興趣班 / Handicraft 手工藝 / Philately 集郵 / Scrabble 拼字 / Magic 魔術 / Hiking 遠足  (Please provide details in \"Description\" box.)";
$default_oea["01881"]["CatCode"] = "4";
$default_oea["01881"]["CatName"] = "Aesthetic Development";

$default_oea["01898"]["ItemCode"] = "01898";
$default_oea["01898"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Interest Class手工藝 (Please provide details in \"Description\" box.)";
$default_oea["01898"]["CatCode"] = "4";
$default_oea["01898"]["CatName"] = "Aesthetic Development";

$default_oea["01882"]["ItemCode"] = "01882";
$default_oea["01882"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Judo 柔道 / Taekwondo 跆拳道學會  (Please provide details in \"Description\" box.)";
$default_oea["01882"]["CatCode"] = "3";
$default_oea["01882"]["CatName"] = "Physical Development";

$default_oea["01834"]["ItemCode"] = "01834";
$default_oea["01834"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Mechanical Engineering 機械工程 / Robot 機器人 / Engineering 工程 / Computer 電腦 / Information Technology 資訊科技  (Please provide details in \"Description\" box.)";
$default_oea["01834"]["CatCode"] = "2";
$default_oea["01834"]["CatName"] = "Career-related Experience";

$default_oea["01883"]["ItemCode"] = "01883";
$default_oea["01883"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Music Instrucment Class樂器班 (Please provide details in \"Description\" box.)";
$default_oea["01883"]["CatCode"] = "4";
$default_oea["01883"]["CatName"] = "Aesthetic Development";

$default_oea["01884"]["ItemCode"] = "01884";
$default_oea["01884"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Paper Cutting 剪紙學會 (Please provide details in \"Description\" box.)";
$default_oea["01884"]["CatCode"] = "4";
$default_oea["01884"]["CatName"] = "Aesthetic Development";

$default_oea["01885"]["ItemCode"] = "01885";
$default_oea["01885"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Photography 攝影 / Pinhole Photography 針孔攝影 (Please provide details in \"Description\" box.)";
$default_oea["01885"]["CatCode"] = "4";
$default_oea["01885"]["CatName"] = "Aesthetic Development";

$default_oea["01886"]["ItemCode"] = "01886";
$default_oea["01886"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Piano 鋼琴/ Violin小提琴/ Flute 長笛/ Harmonica 口琴 (Please provide details in \"Description\" box.)";
$default_oea["01886"]["CatCode"] = "4";
$default_oea["01886"]["CatName"] = "Aesthetic Development";

$default_oea["01887"]["ItemCode"] = "01887";
$default_oea["01887"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Practical Shooting 實用射擊 / Shuttlecock 毽子 / Slam Dunk 扣籃 / Sport Climbing 運動攀爬 / Table Soccer 足球機  (Please provide details in \"Description\" box.)";
$default_oea["01887"]["CatCode"] = "3";
$default_oea["01887"]["CatName"] = "Physical Development";

$default_oea["01888"]["ItemCode"] = "01888";
$default_oea["01888"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Press Society 編輯學會 / Editor 編輯/ School Magazine 學校雜誌/ School Newsletter 學校通訊 / School Bulletin 學校公告 / Public Relations 公共關係 / Advertising 廣告 / Other School Publications 其他學校出版刋物 (Please provide details in \"Description\" box.)";
$default_oea["01888"]["CatCode"] = "4";
$default_oea["01888"]["CatName"] = "Aesthetic Development";

$default_oea["01889"]["ItemCode"] = "01889";
$default_oea["01889"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Religion 宗教 / Pilgrimage 朝聖 / Moral Education 德育教育 / Civic Education 公民教育 / National Education 國民教育 (Please provide details in \"Description\" box.)";
$default_oea["01889"]["CatCode"] = "5";
$default_oea["01889"]["CatName"] = "Moral and Civic Education";

$default_oea["01890"]["ItemCode"] = "01890";
$default_oea["01890"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Rowing 划艇 (Please provide details in \"Description\" box.)";
$default_oea["01890"]["CatCode"] = "3";
$default_oea["01890"]["CatName"] = "Physical Development";

$default_oea["01842"]["ItemCode"] = "01842";
$default_oea["01842"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Science Society科學學會 / Mathematic 數學  (Please provide details in \"Description\" box.)";
$default_oea["01842"]["CatCode"] = "4";
$default_oea["01842"]["CatName"] = "Aesthetic Development";

$default_oea["01891"]["ItemCode"] = "01891";
$default_oea["01891"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Silver Band銀樂隊 / Pipe Band 風笛隊 / (Philharmonic) Orchestra 管絃樂團 / Pop Music Band 流行音樂樂隊 / Chinese Orchestra 中樂團  (Please provide details in \"Description\" box.)";
$default_oea["01891"]["CatCode"] = "4";
$default_oea["01891"]["CatName"] = "Aesthetic Development";

$default_oea["01892"]["ItemCode"] = "01892";
$default_oea["01892"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - Tea Ceremony 茶藝 / Home Economics 家政 / Needlework 針黹 / Cooking 烹飪 (Please provide details in \"Description\" box.)";
$default_oea["01892"]["CatCode"] = "4";
$default_oea["01892"]["CatName"] = "Aesthetic Development";

$default_oea["01893"]["ItemCode"] = "01893";
$default_oea["01893"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - TV Host and/or Programme Production 電視主持及/或製作 / Radio Host and/or Programme Production 電台主持及/或製作 / Multimedia Production 多媒體製作/ Website Production 網站製作 (Please provide details in \"Description\" box.)";
$default_oea["01893"]["CatCode"] = "4";
$default_oea["01893"]["CatName"] = "Aesthetic Development";

$default_oea["01896"]["ItemCode"] = "01896";
$default_oea["01896"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - 划艇學會 (Please provide details in \"Description\" box.)";
$default_oea["01896"]["CatCode"] = "3";
$default_oea["01896"]["CatName"] = "Physical Development";

$default_oea["01894"]["ItemCode"] = "01894";
$default_oea["01894"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - 牧笛及伴奏練習 / 牧笛獨奏練習 (Please provide details in \"Description\" box.)";
$default_oea["01894"]["CatCode"] = "4";
$default_oea["01894"]["CatName"] = "Aesthetic Development";

$default_oea["01895"]["ItemCode"] = "01895";
$default_oea["01895"]["ItemName"] = "School Club / Activity / Service 校內學會、活動及服務 - 體驗營 (Please provide details in \"Description\" box.)";
$default_oea["01895"]["CatCode"] = "4";
$default_oea["01895"]["CatName"] = "Aesthetic Development";

$default_oea["01899"]["ItemCode"] = "01899";
$default_oea["01899"]["ItemName"] = "School Competition 學校比賽 -  Archery Competition 射箭比賽 (Please provide details in \"Description\" box.)";
$default_oea["01899"]["CatCode"] = "3";
$default_oea["01899"]["CatName"] = "Physical Development";

$default_oea["01900"]["ItemCode"] = "01900";
$default_oea["01900"]["ItemName"] = "School Competition 學校比賽 -  Astronomy Project Competition (APC) (Please provide details in \"Description\" box.)";
$default_oea["01900"]["CatCode"] = "2";
$default_oea["01900"]["CatName"] = "Career-related Experience";

$default_oea["01901"]["ItemCode"] = "01901";
$default_oea["01901"]["ItemName"] = "School Competition 學校比賽 -  Badge Assessment - Cook  (Please provide details in \"Description\" box.)";
$default_oea["01901"]["CatCode"] = "4";
$default_oea["01901"]["CatName"] = "Aesthetic Development";

$default_oea["01902"]["ItemCode"] = "01902";
$default_oea["01902"]["ItemName"] = "School Competition 學校比賽 -  Badminton Competition 校內羽毛球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01902"]["CatCode"] = "3";
$default_oea["01902"]["CatName"] = "Physical Development";

$default_oea["01903"]["ItemCode"] = "01903";
$default_oea["01903"]["ItemName"] = "School Competition 學校比賽 -  Badminton Team學校羽毛球隊 (Please provide details in \"Description\" box.)";
$default_oea["01903"]["CatCode"] = "3";
$default_oea["01903"]["CatName"] = "Physical Development";

$default_oea["01904"]["ItemCode"] = "01904";
$default_oea["01904"]["ItemName"] = "School Competition 學校比賽 -  Basketball Competition (School)校內籃球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01904"]["CatCode"] = "3";
$default_oea["01904"]["CatName"] = "Physical Development";

$default_oea["01905"]["ItemCode"] = "01905";
$default_oea["01905"]["ItemName"] = "School Competition 學校比賽 -  Basketball Team籃球校隊 (Please provide details in \"Description\" box.)";
$default_oea["01905"]["CatCode"] = "3";
$default_oea["01905"]["CatName"] = "Physical Development";

$default_oea["01906"]["ItemCode"] = "01906";
$default_oea["01906"]["ItemName"] = "School Competition 學校比賽 -  Best Improved Student / Outstanding Student / Good Student 傑出學生選舉 (Please provide details in \"Description\" box.)";
$default_oea["01906"]["CatCode"] = "2";
$default_oea["01906"]["CatName"] = "Career-related Experience";

$default_oea["01907"]["ItemCode"] = "01907";
$default_oea["01907"]["ItemName"] = "School Competition 學校比賽 -  Bible Recitation Competition 聖經朗誦比賽 (Please provide details in \"Description\" box.)";
$default_oea["01907"]["CatCode"] = "5";
$default_oea["01907"]["CatName"] = "Moral and Civic Education";

$default_oea["01908"]["ItemCode"] = "01908";
$default_oea["01908"]["ItemName"] = "School Competition 學校比賽 -  Book Report Competition (Please provide details in \"Description\" box.)";
$default_oea["01908"]["CatCode"] = "4";
$default_oea["01908"]["CatName"] = "Aesthetic Development";

$default_oea["01909"]["ItemCode"] = "01909";
$default_oea["01909"]["ItemName"] = "School Competition 學校比賽 -  Bowling Competition 保齡球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01909"]["CatCode"] = "3";
$default_oea["01909"]["CatName"] = "Physical Development";

$default_oea["01910"]["ItemCode"] = "01910";
$default_oea["01910"]["ItemName"] = "School Competition 學校比賽 -  Bridge Competition 橋牌比賽 (Please provide details in \"Description\" box.)";
$default_oea["01910"]["CatCode"] = "4";
$default_oea["01910"]["CatName"] = "Aesthetic Development";

$default_oea["01911"]["ItemCode"] = "01911";
$default_oea["01911"]["ItemName"] = "School Competition 學校比賽 -  Chess Competition 國際象棋比賽 (Please provide details in \"Description\" box.)";
$default_oea["01911"]["CatCode"] = "4";
$default_oea["01911"]["CatName"] = "Aesthetic Development";

$default_oea["01912"]["ItemCode"] = "01912";
$default_oea["01912"]["ItemName"] = "School Competition 學校比賽 -  Chinese Billiards Competition  康樂棋比賽 (Please provide details in \"Description\" box.)";
$default_oea["01912"]["CatCode"] = "3";
$default_oea["01912"]["CatName"] = "Physical Development";

$default_oea["01913"]["ItemCode"] = "01913";
$default_oea["01913"]["ItemName"] = "School Competition 學校比賽 -  Chinese Calligraphy Competition書法比賽 (Please provide details in \"Description\" box.)";
$default_oea["01913"]["CatCode"] = "4";
$default_oea["01913"]["CatName"] = "Aesthetic Development";

$default_oea["01914"]["ItemCode"] = "01914";
$default_oea["01914"]["ItemName"] = "School Competition 學校比賽 -  Chinese Checkers Competition 波子棋比賽 (Please provide details in \"Description\" box.)";
$default_oea["01914"]["CatCode"] = "4";
$default_oea["01914"]["CatName"] = "Aesthetic Development";

$default_oea["01915"]["ItemCode"] = "01915";
$default_oea["01915"]["ItemName"] = "School Competition 學校比賽 -  Chinese Chess Competition 中國象棋比賽 (Please provide details in \"Description\" box.)";
$default_oea["01915"]["CatCode"] = "4";
$default_oea["01915"]["CatName"] = "Aesthetic Development";

$default_oea["01916"]["ItemCode"] = "01916";
$default_oea["01916"]["ItemName"] = "School Competition 學校比賽 -  Chinese Drum Competition 中國鼓樂比賽 (Please provide details in \"Description\" box.)";
$default_oea["01916"]["CatCode"] = "4";
$default_oea["01916"]["CatName"] = "Aesthetic Development";

$default_oea["01917"]["ItemCode"] = "01917";
$default_oea["01917"]["ItemName"] = "School Competition 學校比賽 -  Chinese Input Competition  校際中文輸入大賽 (Please provide details in \"Description\" box.)";
$default_oea["01917"]["CatCode"] = "4";
$default_oea["01917"]["CatName"] = "Aesthetic Development";

$default_oea["01918"]["ItemCode"] = "01918";
$default_oea["01918"]["ItemName"] = "School Competition 學校比賽 -  Chinese Painting and Calligraphy Competition國畫及書法比賽 (Please provide details in \"Description\" box.)";
$default_oea["01918"]["CatCode"] = "4";
$default_oea["01918"]["CatName"] = "Aesthetic Development";

$default_oea["01919"]["ItemCode"] = "01919";
$default_oea["01919"]["ItemName"] = "School Competition 學校比賽 -  Classroom Management Competition 班房管理比賽 (Please provide details in \"Description\" box.)";
$default_oea["01919"]["CatCode"] = "2";
$default_oea["01919"]["CatName"] = "Career-related Experience";

$default_oea["01920"]["ItemCode"] = "01920";
$default_oea["01920"]["ItemName"] = "School Competition 學校比賽 -  Cross Country Competition 校內越野賽 (Please provide details in \"Description\" box.)";
$default_oea["01920"]["CatCode"] = "3";
$default_oea["01920"]["CatName"] = "Physical Development";

$default_oea["01921"]["ItemCode"] = "01921";
$default_oea["01921"]["ItemName"] = "School Competition 學校比賽 -  CS Competition (Please provide details in \"Description\" box.)";
$default_oea["01921"]["CatCode"] = "1";
$default_oea["01921"]["CatName"] = "Community Service";

$default_oea["01922"]["ItemCode"] = "01922";
$default_oea["01922"]["ItemName"] = "School Competition 學校比賽 -  Cultural Programme 文化課程 (Please provide details in \"Description\" box.)";
$default_oea["01922"]["CatCode"] = "4";
$default_oea["01922"]["CatName"] = "Aesthetic Development";

$default_oea["01923"]["ItemCode"] = "01923";
$default_oea["01923"]["ItemName"] = "School Competition 學校比賽 -  Cycling Competition 單車比賽 (Please provide details in \"Description\" box.)";
$default_oea["01923"]["CatCode"] = "3";
$default_oea["01923"]["CatName"] = "Physical Development";

$default_oea["01924"]["ItemCode"] = "01924";
$default_oea["01924"]["ItemName"] = "School Competition 學校比賽 -  Dance Contest / Competition 舞蹈比賽 (Please provide details in \"Description\" box.)";
$default_oea["01924"]["CatCode"] = "4";
$default_oea["01924"]["CatName"] = "Aesthetic Development";

$default_oea["01925"]["ItemCode"] = "01925";
$default_oea["01925"]["ItemName"] = "School Competition 學校比賽 -  Dance Promotion Scheme校內舞蹈展能計劃 (Please provide details in \"Description\" box.)";
$default_oea["01925"]["CatCode"] = "4";
$default_oea["01925"]["CatName"] = "Aesthetic Development";

$default_oea["01926"]["ItemCode"] = "01926";
$default_oea["01926"]["ItemName"] = "School Competition 學校比賽 -  Debating Competition (Chinese) 粵語辯論比賽 (Please provide details in \"Description\" box)";
$default_oea["01926"]["CatCode"] = "2";
$default_oea["01926"]["CatName"] = "Career-related Experience";

$default_oea["01927"]["ItemCode"] = "01927";
$default_oea["01927"]["ItemName"] = "School Competition 學校比賽 -  Debating Competition (English) 英語辯論比賽 (Please provide details in \"Description\" box)";
$default_oea["01927"]["CatCode"] = "2";
$default_oea["01927"]["CatName"] = "Career-related Experience";

$default_oea["01928"]["ItemCode"] = "01928";
$default_oea["01928"]["ItemName"] = "School Competition 學校比賽 -  Drama Competition (Please provide details in \"Description\" box) 話劇比賽";
$default_oea["01928"]["CatCode"] = "4";
$default_oea["01928"]["CatName"] = "Aesthetic Development";

$default_oea["01929"]["ItemCode"] = "01929";
$default_oea["01929"]["ItemName"] = "School Competition 學校比賽 -  Drill Competition步操比賽  (Please provide details in \"Description\" box.)";
$default_oea["01929"]["CatCode"] = "4";
$default_oea["01929"]["CatName"] = "Aesthetic Development";

$default_oea["01930"]["ItemCode"] = "01930";
$default_oea["01930"]["ItemName"] = "School Competition 學校比賽 -  English Radio Drama Competition 英文廣播劇比賽 (Please provide details in \"Description\" box.)";
$default_oea["01930"]["CatCode"] = "4";
$default_oea["01930"]["CatName"] = "Aesthetic Development";

$default_oea["01931"]["ItemCode"] = "01931";
$default_oea["01931"]["ItemName"] = "School Competition 學校比賽 -  Fencing Competition 劍擊比賽 (Please provide details in \"Description\" box)";
$default_oea["01931"]["CatCode"] = "3";
$default_oea["01931"]["CatName"] = "Physical Development";

$default_oea["01932"]["ItemCode"] = "01932";
$default_oea["01932"]["ItemName"] = "School Competition 學校比賽 -  Football / Soccer Team / Competition 足球比賽 (Please provide details in \"Description\" box)";
$default_oea["01932"]["CatCode"] = "3";
$default_oea["01932"]["CatName"] = "Physical Development";

$default_oea["01933"]["ItemCode"] = "01933";
$default_oea["01933"]["ItemName"] = "School Competition 學校比賽 -  French Speech Competition 法文演講比賽 (Please provide details in \"Description\" box.)";
$default_oea["01933"]["CatCode"] = "4";
$default_oea["01933"]["CatName"] = "Aesthetic Development";

$default_oea["01934"]["ItemCode"] = "01934";
$default_oea["01934"]["ItemName"] = "School Competition 學校比賽 -  Golf Competition 哥爾夫球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01934"]["CatCode"] = "3";
$default_oea["01934"]["CatName"] = "Physical Development";

$default_oea["01935"]["ItemCode"] = "01935";
$default_oea["01935"]["ItemName"] = "School Competition 學校比賽 -  Gymnastics Competition 體育比賽 (Please provide details in \"Description\" box.)";
$default_oea["01935"]["CatCode"] = "3";
$default_oea["01935"]["CatName"] = "Physical Development";

$default_oea["01936"]["ItemCode"] = "01936";
$default_oea["01936"]["ItemName"] = "School Competition 學校比賽 -  Hockey Competition曲棍球 (Please provide details in \"Description\" box.)";
$default_oea["01936"]["CatCode"] = "3";
$default_oea["01936"]["CatName"] = "Physical Development";

$default_oea["01937"]["ItemCode"] = "01937";
$default_oea["01937"]["ItemName"] = "School Competition 學校比賽 -  Ice-skating Competition 溜冰比賽 (Please provide details in \"Description\" box.)";
$default_oea["01937"]["CatCode"] = "3";
$default_oea["01937"]["CatName"] = "Physical Development";

$default_oea["01938"]["ItemCode"] = "01938";
$default_oea["01938"]["ItemName"] = "School Competition 學校比賽 -  Judo Competition 柔道比賽 (Please provide details in \"Description\" box.)";
$default_oea["01938"]["CatCode"] = "3";
$default_oea["01938"]["CatName"] = "Physical Development";

$default_oea["01939"]["ItemCode"] = "01939";
$default_oea["01939"]["ItemName"] = "School Competition 學校比賽 -  Martial Arts Competition 中國武術比賽 (Please provide details in \"Description\" box.)";
$default_oea["01939"]["CatCode"] = "3";
$default_oea["01939"]["CatName"] = "Physical Development";

$default_oea["01940"]["ItemCode"] = "01940";
$default_oea["01940"]["ItemName"] = "School Competition 學校比賽 -  Mid-Autumn Lantern Design Competition  中秋綵燈設計比賽 (Please provide details in \"Description\" box.)";
$default_oea["01940"]["CatCode"] = "4";
$default_oea["01940"]["CatName"] = "Aesthetic Development";

$default_oea["01941"]["ItemCode"] = "01941";
$default_oea["01941"]["ItemName"] = "School Competition 學校比賽 -  Mid-Autumn Poster and Lantern Competition中秋節海報及花燈設計比賽  (Please provide details in \"Description\" box.)";
$default_oea["01941"]["CatCode"] = "4";
$default_oea["01941"]["CatName"] = "Aesthetic Development";

$default_oea["01942"]["ItemCode"] = "01942";
$default_oea["01942"]["ItemName"] = "School Competition 學校比賽 -  Painting Competition 繪畫比賽 (Please provide details in \"Description\" box.)";
$default_oea["01942"]["CatCode"] = "4";
$default_oea["01942"]["CatName"] = "Aesthetic Development";

$default_oea["01943"]["ItemCode"] = "01943";
$default_oea["01943"]["ItemName"] = "School Competition 學校比賽 -  Photography Competition 攝影比賽 (Please provide details in \"Description\" box.)";
$default_oea["01943"]["CatCode"] = "4";
$default_oea["01943"]["CatName"] = "Aesthetic Development";

$default_oea["01944"]["ItemCode"] = "01944";
$default_oea["01944"]["ItemName"] = "School Competition 學校比賽 -  Piano Competition 鋼琴比賽 (Please provide details in \"Description\" box.)";
$default_oea["01944"]["CatCode"] = "4";
$default_oea["01944"]["CatName"] = "Aesthetic Development";

$default_oea["01945"]["ItemCode"] = "01945";
$default_oea["01945"]["ItemName"] = "School Competition 學校比賽 -  Public Affairs and Administration Proposal Competition公共政策建議書比賽 (Please provide details in \"Description\" box.)";
$default_oea["01945"]["CatCode"] = "5";
$default_oea["01945"]["CatName"] = "Moral and Civic Education";

$default_oea["01946"]["ItemCode"] = "01946";
$default_oea["01946"]["ItemName"] = "School Competition 學校比賽 -  Putonghua Speech Competition 普通話比賽 (Please provide details in \"Description\" box.)";
$default_oea["01946"]["CatCode"] = "4";
$default_oea["01946"]["CatName"] = "Aesthetic Development";

$default_oea["01947"]["ItemCode"] = "01947";
$default_oea["01947"]["ItemName"] = "School Competition 學校比賽 -  Radio Drama Competition 播音劇比賽 (Please provide details in \"Description\" box.)";
$default_oea["01947"]["CatCode"] = "4";
$default_oea["01947"]["CatName"] = "Aesthetic Development";

$default_oea["01948"]["ItemCode"] = "01948";
$default_oea["01948"]["ItemName"] = "School Competition 學校比賽 -  Rowing Competition划船比賽 (Please provide details in \"Description\" box.)";
$default_oea["01948"]["CatCode"] = "3";
$default_oea["01948"]["CatName"] = "Physical Development";

$default_oea["01949"]["ItemCode"] = "01949";
$default_oea["01949"]["ItemName"] = "School Competition 學校比賽 -  School Handbell Competition手鈴比賽 (Please provide details in \"Description\" box.)";
$default_oea["01949"]["CatCode"] = "4";
$default_oea["01949"]["CatName"] = "Aesthetic Development";

$default_oea["01950"]["ItemCode"] = "01950";
$default_oea["01950"]["ItemName"] = "School Competition 學校比賽 -  Scrabble Competition 拼字比賽 (Please provide details in \"Description\" box.)";
$default_oea["01950"]["CatCode"] = "4";
$default_oea["01950"]["CatName"] = "Aesthetic Development";

$default_oea["01951"]["ItemCode"] = "01951";
$default_oea["01951"]["ItemName"] = "School Competition 學校比賽 -  Shuttlecock Competition 毽子比賽 (Please provide details in \"Description\" box.)";
$default_oea["01951"]["CatCode"] = "3";
$default_oea["01951"]["CatName"] = "Physical Development";

$default_oea["01952"]["ItemCode"] = "01952";
$default_oea["01952"]["ItemName"] = "School Competition 學校比賽 -  Squash Competition 壁球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01952"]["CatCode"] = "3";
$default_oea["01952"]["CatName"] = "Physical Development";

$default_oea["01953"]["ItemCode"] = "01953";
$default_oea["01953"]["ItemName"] = "School Competition 學校比賽 -  Swimming Competition游泳比賽 (Please provide details in \"Description\" box.)";
$default_oea["01953"]["CatCode"] = "3";
$default_oea["01953"]["CatName"] = "Physical Development";

$default_oea["01954"]["ItemCode"] = "01954";
$default_oea["01954"]["ItemName"] = "School Competition 學校比賽 -  Table-tennis Competition乒乓球賽 (Please provide details in \"Description\" box.)";
$default_oea["01954"]["CatCode"] = "3";
$default_oea["01954"]["CatName"] = "Physical Development";

$default_oea["01955"]["ItemCode"] = "01955";
$default_oea["01955"]["ItemName"] = "School Competition 學校比賽 -  Taekwondo Competition 跆拳道比賽 (Please provide details in \"Description\" box.)";
$default_oea["01955"]["CatCode"] = "3";
$default_oea["01955"]["CatName"] = "Physical Development";

$default_oea["01956"]["ItemCode"] = "01956";
$default_oea["01956"]["ItemName"] = "School Competition 學校比賽 -  Tennis Competition 網球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01956"]["CatCode"] = "3";
$default_oea["01956"]["CatName"] = "Physical Development";

$default_oea["01957"]["ItemCode"] = "01957";
$default_oea["01957"]["ItemName"] = "School Competition 學校比賽 -  Volleyball Competition 排球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01957"]["CatCode"] = "3";
$default_oea["01957"]["CatName"] = "Physical Development";

$default_oea["01958"]["ItemCode"] = "01958";
$default_oea["01958"]["ItemName"] = "School Competition 學校比賽 -  Western Dance competition 舞蹈比賽 (Please provide details in \"Description\" box.)";
$default_oea["01958"]["CatCode"] = "4";
$default_oea["01958"]["CatName"] = "Aesthetic Development";

$default_oea["01960"]["ItemCode"] = "01960";
$default_oea["01960"]["ItemName"] = "School Competition 學校比賽 -  利是封設計比賽 (Please provide details in \"Description\" box.)";
$default_oea["01960"]["CatCode"] = "4";
$default_oea["01960"]["CatName"] = "Aesthetic Development";

$default_oea["01959"]["ItemCode"] = "01959";
$default_oea["01959"]["ItemName"] = "School Competition 學校比賽 -  地理學會模擬城市設計比賽  (Please provide details in \"Description\" box.)";
$default_oea["01959"]["CatCode"] = "4";
$default_oea["01959"]["CatName"] = "Aesthetic Development";

$default_oea["01961"]["ItemCode"] = "01961";
$default_oea["01961"]["ItemName"] = "School Competition 學校比賽 -  沙灘手球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01961"]["CatCode"] = "3";
$default_oea["01961"]["CatName"] = "Physical Development";

$default_oea["01963"]["ItemCode"] = "01963";
$default_oea["01963"]["ItemName"] = "School Competition 學校比賽 -  舞蹈日 (Please provide details in \"Description\" box.)";
$default_oea["01963"]["CatCode"] = "4";
$default_oea["01963"]["CatName"] = "Aesthetic Development";

$default_oea["01962"]["ItemCode"] = "01962";
$default_oea["01962"]["ItemName"] = "School Competition 學校比賽 -  詩歌創作比賽 (Please provide details in \"Description\" box.)";
$default_oea["01962"]["CatCode"] = "4";
$default_oea["01962"]["CatName"] = "Aesthetic Development";

$default_oea["01964"]["ItemCode"] = "01964";
$default_oea["01964"]["ItemName"] = "School Competition 學校比賽 -  露營活動 攝影比賽 - 個人作品 / 攝影課程 / 工作坊 攝影比賽 - 個人作品 / 個人作品 (Please provide details in \"Description\" box.)";
$default_oea["01964"]["CatCode"] = "3";
$default_oea["01964"]["CatName"] = "Physical Development";

$default_oea["01965"]["ItemCode"] = "01965";
$default_oea["01965"]["ItemName"] = "School Competition 學校比賽 - Creative Design Contest 創意設計比賽  (Please provide details in \"Description\" box.)";
$default_oea["01965"]["CatCode"] = "4";
$default_oea["01965"]["CatName"] = "Aesthetic Development";

$default_oea["01966"]["ItemCode"] = "01966";
$default_oea["01966"]["ItemName"] = "School Competition 學校比賽 - Korfball Competition 合球比賽 (Please provide details in \"Description\" box.)";
$default_oea["01966"]["CatCode"] = "3";
$default_oea["01966"]["CatName"] = "Physical Development";

$default_oea["01967"]["ItemCode"] = "01967";
$default_oea["01967"]["ItemName"] = "School Competition 學校比賽 - Physics competition 物理比賽 (Please provide details in \"Description\" box.)";
$default_oea["01967"]["CatCode"] = "2";
$default_oea["01967"]["CatName"] = "Career-related Experience";

$default_oea["01968"]["ItemCode"] = "01968";
$default_oea["01968"]["ItemName"] = "School Competition 學校比賽 - Quiz 校內問答比賽 (Please provide details in \"Description\" box.)";
$default_oea["01968"]["CatCode"] = "4";
$default_oea["01968"]["CatName"] = "Aesthetic Development";

$default_oea["01969"]["ItemCode"] = "01969";
$default_oea["01969"]["ItemName"] = "School Competition 學校比賽 - School Competition 學校比賽 -  Anemometer Design Competition  風速表設計比賽 (Please provide details in \"Description\" box.)";
$default_oea["01969"]["CatCode"] = "4";
$default_oea["01969"]["CatName"] = "Aesthetic Development";

$default_oea["01970"]["ItemCode"] = "01970";
$default_oea["01970"]["ItemName"] = "School Competition 學校比賽 - 中文學會硬筆書法比賽 (Please provide details in \"Description\" box.)";
$default_oea["01970"]["CatCode"] = "4";
$default_oea["01970"]["CatName"] = "Aesthetic Development";

$default_oea["01971"]["ItemCode"] = "01971";
$default_oea["01971"]["ItemName"] = "School Creative Music showcase(EDB) 學校音樂創藝展";
$default_oea["01971"]["CatCode"] = "4";
$default_oea["01971"]["CatName"] = "Aesthetic Development";

$default_oea["01972"]["ItemCode"] = "01972";
$default_oea["01972"]["ItemName"] = "School Cultural Day - Cantonese Musical";
$default_oea["01972"]["CatCode"] = "4";
$default_oea["01972"]["CatName"] = "Aesthetic Development";

$default_oea["01973"]["ItemCode"] = "01973";
$default_oea["01973"]["ItemName"] = "School Dance Festival / Open Dance Competition / International Youth Dance Festival";
$default_oea["01973"]["CatCode"] = "4";
$default_oea["01973"]["CatName"] = "Aesthetic Development";

$default_oea["01974"]["ItemCode"] = "01974";
$default_oea["01974"]["ItemName"] = "School Interport Championship / School Interport Sports Competition  學界埠際錦標賽/學界體育比賽";
$default_oea["01974"]["CatCode"] = "3";
$default_oea["01974"]["CatName"] = "Physical Development";

$default_oea["01975"]["ItemCode"] = "01975";
$default_oea["01975"]["ItemName"] = "School Physical Fitness Award Scheme";
$default_oea["01975"]["CatCode"] = "3";
$default_oea["01975"]["CatName"] = "Physical Development";

$default_oea["01976"]["ItemCode"] = "01976";
$default_oea["01976"]["ItemName"] = "School Physical Fitness Award Scheme  學校體適能獎勵計劃";
$default_oea["01976"]["CatCode"] = "3";
$default_oea["01976"]["CatName"] = "Physical Development";

$default_oea["01977"]["ItemCode"] = "01977";
$default_oea["01977"]["ItemName"] = "School Uniform Groups Parade Day";
$default_oea["01977"]["CatCode"] = "4";
$default_oea["01977"]["CatName"] = "Aesthetic Development";

$default_oea["01978"]["ItemCode"] = "01978";
$default_oea["01978"]["ItemName"] = "School Writing Competition Write my world";
$default_oea["01978"]["CatCode"] = "4";
$default_oea["01978"]["CatName"] = "Aesthetic Development";

$default_oea["01979"]["ItemCode"] = "01979";
$default_oea["01979"]["ItemName"] = "School-Company Partnership Programme";
$default_oea["01979"]["CatCode"] = "2";
$default_oea["01979"]["CatName"] = "Career-related Experience";

$default_oea["01980"]["ItemCode"] = "01980";
$default_oea["01980"]["ItemName"] = "Schools Dance Festival Western Folk Dance學校舞蹈節西方舞組 (Please provide details in \"Description\" box.)";
$default_oea["01980"]["CatCode"] = "4";
$default_oea["01980"]["CatName"] = "Aesthetic Development";

$default_oea["01981"]["ItemCode"] = "01981";
$default_oea["01981"]["ItemName"] = "Schools Interport Swimming Championships  學界埠際游泳錦標賽";
$default_oea["01981"]["CatCode"] = "3";
$default_oea["01981"]["CatName"] = "Physical Development";

$default_oea["01982"]["ItemCode"] = "01982";
$default_oea["01982"]["ItemName"] = "Schools Interport Table Tennis Championships  學界埠際乒乓球錦標賽";
$default_oea["01982"]["CatCode"] = "3";
$default_oea["01982"]["CatName"] = "Physical Development";

$default_oea["01983"]["ItemCode"] = "01983";
$default_oea["01983"]["ItemName"] = "Schumann International Youth Piano Competition (Asia Pacific Region)  舒曼杯國際青少年鋼琴大賽(亞太地區選拔賽)";
$default_oea["01983"]["CatCode"] = "4";
$default_oea["01983"]["CatName"] = "Aesthetic Development";

$default_oea["01984"]["ItemCode"] = "01984";
$default_oea["01984"]["ItemName"] = "Schumann International Youth Piano Competition (Hong Kong Preliminary)  舒曼杯國際青少年鋼琴大賽(香港區選拔賽)";
$default_oea["01984"]["CatCode"] = "4";
$default_oea["01984"]["CatName"] = "Aesthetic Development";

$default_oea["01985"]["ItemCode"] = "01985";
$default_oea["01985"]["ItemName"] = "Science Enrichment Programme";
$default_oea["01985"]["CatCode"] = "2";
$default_oea["01985"]["CatName"] = "Career-related Experience";

$default_oea["01990"]["ItemCode"] = "01990";
$default_oea["01990"]["ItemName"] = "Scottish Junior Squash Open  蘇格蘭青少年壁球公開賽";
$default_oea["01990"]["CatCode"] = "3";
$default_oea["01990"]["CatName"] = "Physical Development";

$default_oea["01991"]["ItemCode"] = "01991";
$default_oea["01991"]["ItemName"] = "Scout Association of Hong Kong Kowloon Region President Cup Trophy Competition";
$default_oea["01991"]["CatCode"] = "1";
$default_oea["01991"]["CatName"] = "Community Service";

$default_oea["01992"]["ItemCode"] = "01992";
$default_oea["01992"]["ItemName"] = "Scout of the Year";
$default_oea["01992"]["CatCode"] = "1";
$default_oea["01992"]["CatName"] = "Community Service";

$default_oea["01993"]["ItemCode"] = "01993";
$default_oea["01993"]["ItemName"] = "Scouts童軍 (Please provide details in \"Description\" box.) ";
$default_oea["01993"]["CatCode"] = "1";
$default_oea["01993"]["CatName"] = "Community Service";

$default_oea["01994"]["ItemCode"] = "01994";
$default_oea["01994"]["ItemName"] = "Scrabble Championship";
$default_oea["01994"]["CatCode"] = "4";
$default_oea["01994"]["CatName"] = "Aesthetic Development";

$default_oea["01995"]["ItemCode"] = "01995";
$default_oea["01995"]["ItemName"] = "Scratch 學習體驗@Cyberport Scratch Learning Experience @Cyberpor";
$default_oea["01995"]["CatCode"] = "2";
$default_oea["01995"]["CatName"] = "Career-related Experience";

$default_oea["01996"]["ItemCode"] = "01996";
$default_oea["01996"]["ItemName"] = "Script Writing Competition";
$default_oea["01996"]["CatCode"] = "4";
$default_oea["01996"]["CatName"] = "Aesthetic Development";

$default_oea["01986"]["ItemCode"] = "01986";
$default_oea["01986"]["ItemName"] = "SCMP Hong Kong Student of the Year Award (Please provide details in \"Description\" box.)";
$default_oea["01986"]["CatCode"] = "5";
$default_oea["01986"]["CatName"] = "Moral and Civic Education";

$default_oea["01987"]["ItemCode"] = "01987";
$default_oea["01987"]["ItemName"] = "SCMP Newspaper Clipping Competition";
$default_oea["01987"]["CatCode"] = "4";
$default_oea["01987"]["CatName"] = "Aesthetic Development";

$default_oea["01988"]["ItemCode"] = "01988";
$default_oea["01988"]["ItemName"] = "SCMP SRP Student Reporter of the Year南華早報學生記者計劃—最佳記者";
$default_oea["01988"]["CatCode"] = "2";
$default_oea["01988"]["CatName"] = "Career-related Experience";

$default_oea["01989"]["ItemCode"] = "01989";
$default_oea["01989"]["ItemName"] = "SCMP Student Reporters Program南華早報學生記者計劃";
$default_oea["01989"]["CatCode"] = "2";
$default_oea["01989"]["CatName"] = "Career-related Experience";

$default_oea["01997"]["ItemCode"] = "01997";
$default_oea["01997"]["ItemName"] = "Sea Cadet (T.S. Nelson)";
$default_oea["01997"]["CatCode"] = "3";
$default_oea["01997"]["CatName"] = "Physical Development";

$default_oea["01999"]["ItemCode"] = "01999";
$default_oea["01999"]["ItemName"] = "Secondary 4 Career Education Certificate Program中四職業教育 證書課程";
$default_oea["01999"]["CatCode"] = "2";
$default_oea["01999"]["CatName"] = "Career-related Experience";

$default_oea["02000"]["ItemCode"] = "02000";
$default_oea["02000"]["ItemName"] = "Secondary Rescue Competition at RoboCupJunior Hong Kong Open Competition  青少年機械人世界盃香港區選拔賽(中學組)拯救比賽";
$default_oea["02000"]["CatCode"] = "2";
$default_oea["02000"]["CatName"] = "Career-related Experience";

$default_oea["02001"]["ItemCode"] = "02001";
$default_oea["02001"]["ItemName"] = "Secondary School Grafitti DesignCompetition中學塗鴉設計比賽";
$default_oea["02001"]["CatCode"] = "4";
$default_oea["02001"]["CatName"] = "Aesthetic Development";

$default_oea["02002"]["ItemCode"] = "02002";
$default_oea["02002"]["ItemName"] = "Secondary School Mock Trial Competition";
$default_oea["02002"]["CatCode"] = "2";
$default_oea["02002"]["CatName"] = "Career-related Experience";

$default_oea["02003"]["ItemCode"] = "02003";
$default_oea["02003"]["ItemName"] = "Secondary School Science Quiz Competition";
$default_oea["02003"]["CatCode"] = "2";
$default_oea["02003"]["CatName"] = "Career-related Experience";

$default_oea["02004"]["ItemCode"] = "02004";
$default_oea["02004"]["ItemName"] = "Secondary School Students Creative Visual Arts Work Competition中學生視覺藝術創作比賽";
$default_oea["02004"]["CatCode"] = "4";
$default_oea["02004"]["CatName"] = "Aesthetic Development";

$default_oea["02005"]["ItemCode"] = "02005";
$default_oea["02005"]["ItemName"] = "Secondary Schools and Uniform Group's PRC 60th Anniversary Ceremony中學制服團體國慶60周年升旗禮";
$default_oea["02005"]["CatCode"] = "5";
$default_oea["02005"]["CatName"] = "Moral and Civic Education";

$default_oea["02006"]["ItemCode"] = "02006";
$default_oea["02006"]["ItemName"] = "Seminar: Celebration for the 60th Anniversary of the People Republic of China (Seminar)建國六十年研討會";
$default_oea["02006"]["CatCode"] = "5";
$default_oea["02006"]["CatName"] = "Moral and Civic Education";

$default_oea["02007"]["ItemCode"] = "02007";
$default_oea["02007"]["ItemName"] = "Senior League Baseball Far East Tournament";
$default_oea["02007"]["CatCode"] = "3";
$default_oea["02007"]["CatName"] = "Physical Development";

$default_oea["02008"]["ItemCode"] = "02008";
$default_oea["02008"]["ItemName"] = "Senior Secondary Science and Mathematics Competitions (Chem)";
$default_oea["02008"]["CatCode"] = "2";
$default_oea["02008"]["CatName"] = "Career-related Experience";

$default_oea["02009"]["ItemCode"] = "02009";
$default_oea["02009"]["ItemName"] = "Sex Education Workshop / Sex Education Pioneer Training 性教育先鋒訓練";
$default_oea["02009"]["CatCode"] = "5";
$default_oea["02009"]["CatName"] = "Moral and Civic Education";

$default_oea["01998"]["ItemCode"] = "01998";
$default_oea["01998"]["ItemName"] = "SEARCC International Schools Software Competition";
$default_oea["01998"]["CatCode"] = "2";
$default_oea["01998"]["CatName"] = "Career-related Experience";

$default_oea["02010"]["ItemCode"] = "02010";
$default_oea["02010"]["ItemName"] = "Sham Shui Po District Outstanding Students Award (Senior Secondary Section) - Sham Ching Youth Association";
$default_oea["02010"]["CatCode"] = "2";
$default_oea["02010"]["CatName"] = "Career-related Experience";

$default_oea["02011"]["ItemCode"] = "02011";
$default_oea["02011"]["ItemName"] = "Sham Shui Po Road Safety Funfair Game Store Design Competition - Sham Shui Po Road Safety Council";
$default_oea["02011"]["CatCode"] = "4";
$default_oea["02011"]["CatName"] = "Aesthetic Development";

$default_oea["02012"]["ItemCode"] = "02012";
$default_oea["02012"]["ItemName"] = "Shanghai - Hong Kong Exchange Schools' Debating Competition";
$default_oea["02012"]["CatCode"] = "5";
$default_oea["02012"]["CatName"] = "Moral and Civic Education";

$default_oea["02013"]["ItemCode"] = "02013";
$default_oea["02013"]["ItemName"] = "Shanghai - Hong Kong Senior High School Students' English Debate Competition  滬港高中生英語辯論賽";
$default_oea["02013"]["CatCode"] = "5";
$default_oea["02013"]["CatName"] = "Moral and Civic Education";

$default_oea["02014"]["ItemCode"] = "02014";
$default_oea["02014"]["ItemName"] = "Shanghai Cultural Expedition";
$default_oea["02014"]["CatCode"] = "4";
$default_oea["02014"]["CatName"] = "Aesthetic Development";

$default_oea["02015"]["ItemCode"] = "02015";
$default_oea["02015"]["ItemName"] = "Shanghai International High School Students Chorus Festival";
$default_oea["02015"]["CatCode"] = "4";
$default_oea["02015"]["CatName"] = "Aesthetic Development";

$default_oea["02016"]["ItemCode"] = "02016";
$default_oea["02016"]["ItemName"] = "Shanghai International Youth Science and Technology Expo  上海國際青少年科技博覽會";
$default_oea["02016"]["CatCode"] = "2";
$default_oea["02016"]["CatName"] = "Career-related Experience";

$default_oea["02017"]["ItemCode"] = "02017";
$default_oea["02017"]["ItemName"] = "Shanghai Junior Robotic Olympic Competition  中國上海青少年機械奧運埠際賽";
$default_oea["02017"]["CatCode"] = "2";
$default_oea["02017"]["CatName"] = "Career-related Experience";

$default_oea["02018"]["ItemCode"] = "02018";
$default_oea["02018"]["ItemName"] = "Shanghai Normal University Attached Middle School Debate Competition";
$default_oea["02018"]["CatCode"] = "5";
$default_oea["02018"]["CatName"] = "Moral and Civic Education";

$default_oea["02022"]["ItemCode"] = "02022";
$default_oea["02022"]["ItemName"] = "Shanghai-HK Teenagers' Handball Summer Camp";
$default_oea["02022"]["CatCode"] = "3";
$default_oea["02022"]["CatName"] = "Physical Development";

$default_oea["02019"]["ItemCode"] = "02019";
$default_oea["02019"]["ItemName"] = "Shanghai, Hong Kong & Taiwan Rowing Championships";
$default_oea["02019"]["CatCode"] = "3";
$default_oea["02019"]["CatName"] = "Physical Development";

$default_oea["02020"]["ItemCode"] = "02020";
$default_oea["02020"]["ItemName"] = "Shanghai, Taipei, Hong Kong and Macau Youth Putonghua Recitation Competition";
$default_oea["02020"]["CatCode"] = "4";
$default_oea["02020"]["CatName"] = "Aesthetic Development";

$default_oea["02021"]["ItemCode"] = "02021";
$default_oea["02021"]["ItemName"] = "Shanghai,Taipei,Hong Kong and Macau Putonghua Speech Competition";
$default_oea["02021"]["CatCode"] = "4";
$default_oea["02021"]["CatName"] = "Aesthetic Development";

$default_oea["02023"]["ItemCode"] = "02023";
$default_oea["02023"]["ItemName"] = "Shankar's International Children's Competition  香拉克氏國際兒童繪畫比賽";
$default_oea["02023"]["CatCode"] = "4";
$default_oea["02023"]["CatName"] = "Aesthetic Development";

$default_oea["02024"]["ItemCode"] = "02024";
$default_oea["02024"]["ItemName"] = "Shanxi Taiyuan International Junior Open Championship  山西太原國際青少年公開賽";
$default_oea["02024"]["CatCode"] = "3";
$default_oea["02024"]["CatName"] = "Physical Development";

$default_oea["02025"]["ItemCode"] = "02025";
$default_oea["02025"]["ItemName"] = "Shatin Festival Half Marathon cum Christmas Fun Race";
$default_oea["02025"]["CatCode"] = "3";
$default_oea["02025"]["CatName"] = "Physical Development";

$default_oea["02027"]["ItemCode"] = "02027";
$default_oea["02027"]["ItemName"] = "Shenzhen - Hong Kong Exchange Students To Study Write-up Competition  深港中學生讀書交流活動之讀書隨筆寫作比賽";
$default_oea["02027"]["CatCode"] = "4";
$default_oea["02027"]["CatName"] = "Aesthetic Development";

$default_oea["02028"]["ItemCode"] = "02028";
$default_oea["02028"]["ItemName"] = "Shenzhen & Hong Kong Taekwondo Friendly Competition  深港跆拳道友誼賽";
$default_oea["02028"]["CatCode"] = "3";
$default_oea["02028"]["CatName"] = "Physical Development";

$default_oea["02029"]["ItemCode"] = "02029";
$default_oea["02029"]["ItemName"] = "Shenzhen Traditional Martial Arts Competition  深圳巿傳統武術比賽大會";
$default_oea["02029"]["CatCode"] = "3";
$default_oea["02029"]["CatName"] = "Physical Development";

$default_oea["02031"]["ItemCode"] = "02031";
$default_oea["02031"]["ItemName"] = "Shenzhen-Hong Kong-Guangzhou Football Cup";
$default_oea["02031"]["CatCode"] = "3";
$default_oea["02031"]["CatName"] = "Physical Development";

$default_oea["02030"]["ItemCode"] = "02030";
$default_oea["02030"]["ItemName"] = "Shenzhen, Hong Kong, Macao Children's Latin Dance Open  深、港、澳少兒拉丁舞公開賽";
$default_oea["02030"]["CatCode"] = "4";
$default_oea["02030"]["CatName"] = "Aesthetic Development";

$default_oea["02032"]["ItemCode"] = "02032";
$default_oea["02032"]["ItemName"] = "Shield Competition of Hong Kong Girl Guides Association (Dancing)";
$default_oea["02032"]["CatCode"] = "4";
$default_oea["02032"]["CatName"] = "Aesthetic Development";

$default_oea["02033"]["ItemCode"] = "02033";
$default_oea["02033"]["ItemName"] = "Shuang Long Cup National Calligraphy And Painting Competition";
$default_oea["02033"]["CatCode"] = "4";
$default_oea["02033"]["CatName"] = "Aesthetic Development";

$default_oea["02034"]["ItemCode"] = "02034";
$default_oea["02034"]["ItemName"] = "Shun Tak Fraternal Association Seaward Woo College Super 24  順德聯誼會胡兆熾中學Super 24邀請賽";
$default_oea["02034"]["CatCode"] = "2";
$default_oea["02034"]["CatName"] = "Career-related Experience";

$default_oea["02035"]["ItemCode"] = "02035";
$default_oea["02035"]["ItemName"] = "Shuttlecock League Match";
$default_oea["02035"]["CatCode"] = "3";
$default_oea["02035"]["CatName"] = "Physical Development";

$default_oea["02026"]["ItemCode"] = "02026";
$default_oea["02026"]["ItemName"] = "SHCC Athletics Team Winter Training Camp 2010-2011";
$default_oea["02026"]["CatCode"] = "3";
$default_oea["02026"]["CatName"] = "Physical Development";

$default_oea["02036"]["ItemCode"] = "02036";
$default_oea["02036"]["ItemName"] = "Siam Cup in Windsurfing IMCO Class";
$default_oea["02036"]["CatCode"] = "3";
$default_oea["02036"]["CatName"] = "Physical Development";

$default_oea["02038"]["ItemCode"] = "02038";
$default_oea["02038"]["ItemName"] = "Sign Language Training and Performance";
$default_oea["02038"]["CatCode"] = "4";
$default_oea["02038"]["CatName"] = "Aesthetic Development";

$default_oea["02039"]["ItemCode"] = "02039";
$default_oea["02039"]["ItemName"] = "Silvermine Bay Social Service Camp銀礦灣社區服務營";
$default_oea["02039"]["CatCode"] = "1";
$default_oea["02039"]["CatName"] = "Community Service";

$default_oea["02040"]["ItemCode"] = "02040";
$default_oea["02040"]["ItemName"] = "Sing Tao Debating Competition  星島全港校際辯論比賽";
$default_oea["02040"]["CatCode"] = "2";
$default_oea["02040"]["CatName"] = "Career-related Experience";

$default_oea["02041"]["ItemCode"] = "02041";
$default_oea["02041"]["ItemName"] = "Singapore Gymnastics Open Championships  新加坡藝術體操公開賽";
$default_oea["02041"]["CatCode"] = "3";
$default_oea["02041"]["CatName"] = "Physical Development";

$default_oea["02042"]["ItemCode"] = "02042";
$default_oea["02042"]["ItemName"] = "Singapore International Open Bowling Championships  星加坡國際保齡球公開賽";
$default_oea["02042"]["CatCode"] = "3";
$default_oea["02042"]["CatName"] = "Physical Development";

$default_oea["02043"]["ItemCode"] = "02043";
$default_oea["02043"]["ItemName"] = "Singapore International Rowing Championship";
$default_oea["02043"]["CatCode"] = "3";
$default_oea["02043"]["CatName"] = "Physical Development";

$default_oea["02044"]["ItemCode"] = "02044";
$default_oea["02044"]["ItemName"] = "Singapore Junior Squash Open  新加坡青少年壁球公開賽";
$default_oea["02044"]["CatCode"] = "3";
$default_oea["02044"]["CatName"] = "Physical Development";

$default_oea["02045"]["ItemCode"] = "02045";
$default_oea["02045"]["ItemName"] = "Singapore Open Sailing Championship  星加坡風帆公開賽";
$default_oea["02045"]["CatCode"] = "3";
$default_oea["02045"]["CatName"] = "Physical Development";

$default_oea["02046"]["ItemCode"] = "02046";
$default_oea["02046"]["ItemName"] = "Singapore Open Track & Field Championships";
$default_oea["02046"]["CatCode"] = "3";
$default_oea["02046"]["CatName"] = "Physical Development";

$default_oea["02047"]["ItemCode"] = "02047";
$default_oea["02047"]["ItemName"] = "Singapore Open Windsurfing Championship  星加坡滑浪風帆公開賽";
$default_oea["02047"]["CatCode"] = "3";
$default_oea["02047"]["CatName"] = "Physical Development";

$default_oea["02048"]["ItemCode"] = "02048";
$default_oea["02048"]["ItemName"] = "Singapore Open Youth Runner Up (Windsurfing)";
$default_oea["02048"]["CatCode"] = "3";
$default_oea["02048"]["CatName"] = "Physical Development";

$default_oea["02049"]["ItemCode"] = "02049";
$default_oea["02049"]["ItemName"] = "Singapore SACAC GYMFEST";
$default_oea["02049"]["CatCode"] = "3";
$default_oea["02049"]["CatName"] = "Physical Development";

$default_oea["02050"]["ItemCode"] = "02050";
$default_oea["02050"]["ItemName"] = "Singing Competition";
$default_oea["02050"]["CatCode"] = "4";
$default_oea["02050"]["CatName"] = "Aesthetic Development";

$default_oea["02051"]["ItemCode"] = "02051";
$default_oea["02051"]["ItemName"] = "Sino-Japanese International Calligraphy and Painting Competition  中國日本國際書畫大賽";
$default_oea["02051"]["CatCode"] = "4";
$default_oea["02051"]["CatName"] = "Aesthetic Development";

$default_oea["02052"]["ItemCode"] = "02052";
$default_oea["02052"]["ItemName"] = "Sir Edward Youde Memorial Prize (For Senior Secondary School Students)  尤德爵士紀念基金高中學生獎";
$default_oea["02052"]["CatCode"] = "5";
$default_oea["02052"]["CatName"] = "Moral and Civic Education";

$default_oea["02037"]["ItemCode"] = "02037";
$default_oea["02037"]["ItemName"] = "SICC Junior Invitational Golf Championship  SICC青少年高球邀請賽";
$default_oea["02037"]["CatCode"] = "3";
$default_oea["02037"]["CatName"] = "Physical Development";

$default_oea["02053"]["ItemCode"] = "02053";
$default_oea["02053"]["ItemName"] = "Skate Asia  亞洲冰上邀請賽";
$default_oea["02053"]["CatCode"] = "3";
$default_oea["02053"]["CatName"] = "Physical Development";

$default_oea["02054"]["ItemCode"] = "02054";
$default_oea["02054"]["ItemName"] = "Skate Malaysia";
$default_oea["02054"]["CatCode"] = "3";
$default_oea["02054"]["CatName"] = "Physical Development";

$default_oea["02055"]["ItemCode"] = "02055";
$default_oea["02055"]["ItemName"] = "SKIF (Shotokan Karate-do International Federation) World Championship  國際松濤館世界空手道大會";
$default_oea["02055"]["CatCode"] = "3";
$default_oea["02055"]["CatName"] = "Physical Development";

$default_oea["02056"]["ItemCode"] = "02056";
$default_oea["02056"]["ItemName"] = "Slam Dunk Challenge";
$default_oea["02056"]["CatCode"] = "3";
$default_oea["02056"]["CatName"] = "Physical Development";

$default_oea["02057"]["ItemCode"] = "02057";
$default_oea["02057"]["ItemName"] = "Slogan and /or Poster Design Competition";
$default_oea["02057"]["CatCode"] = "4";
$default_oea["02057"]["CatName"] = "Aesthetic Development";

$default_oea["02059"]["ItemCode"] = "02059";
$default_oea["02059"]["ItemName"] = "Smart Teen Challenge Project  多元智能挑戰計劃";
$default_oea["02059"]["CatCode"] = "2";
$default_oea["02059"]["CatName"] = "Career-related Experience";

$default_oea["02060"]["ItemCode"] = "02060";
$default_oea["02060"]["ItemName"] = "Smile action Program";
$default_oea["02060"]["CatCode"] = "1";
$default_oea["02060"]["CatName"] = "Community Service";

$default_oea["02061"]["ItemCode"] = "02061";
$default_oea["02061"]["ItemName"] = "Smirnoff International Fashion Awards  史美諾國際時裝獎";
$default_oea["02061"]["CatCode"] = "4";
$default_oea["02061"]["CatName"] = "Aesthetic Development";

$default_oea["02058"]["ItemCode"] = "02058";
$default_oea["02058"]["ItemName"] = "SMAP 中學數學網上學習計劃";
$default_oea["02058"]["CatCode"] = "2";
$default_oea["02058"]["CatName"] = "Career-related Experience";

$default_oea["02064"]["ItemCode"] = "02064";
$default_oea["02064"]["ItemName"] = "Soci-game: City of Choice";
$default_oea["02064"]["CatCode"] = "5";
$default_oea["02064"]["CatName"] = "Moral and Civic Education";

$default_oea["02062"]["ItemCode"] = "02062";
$default_oea["02062"]["ItemName"] = "Social Science Cross-Curriculum PBL Prizes";
$default_oea["02062"]["CatCode"] = "2";
$default_oea["02062"]["CatName"] = "Career-related Experience";

$default_oea["02063"]["ItemCode"] = "02063";
$default_oea["02063"]["ItemName"] = "Social Service Team";
$default_oea["02063"]["CatCode"] = "1";
$default_oea["02063"]["CatName"] = "Community Service";

$default_oea["02065"]["ItemCode"] = "02065";
$default_oea["02065"]["ItemName"] = "Solicitation of the Mascot, the Theme Song and the Poster for the Special Olympics World Games  上海世界特殊奧林匹克運動會吉祥物、主題歌、招貼畫徵集活動";
$default_oea["02065"]["CatCode"] = "4";
$default_oea["02065"]["CatName"] = "Aesthetic Development";

$default_oea["02066"]["ItemCode"] = "02066";
$default_oea["02066"]["ItemName"] = "Song Writing Competition";
$default_oea["02066"]["CatCode"] = "4";
$default_oea["02066"]["CatName"] = "Aesthetic Development";

$default_oea["02067"]["ItemCode"] = "02067";
$default_oea["02067"]["ItemName"] = "Soong Ching Ling Awards for Children's Invention  宋慶齡少年兒童發明獎";
$default_oea["02067"]["CatCode"] = "2";
$default_oea["02067"]["CatName"] = "Career-related Experience";

$default_oea["02068"]["ItemCode"] = "02068";
$default_oea["02068"]["ItemName"] = "South Africa Roller Speed Skating National Championships  南非全國速度滾軸溜冰錦標賽";
$default_oea["02068"]["CatCode"] = "3";
$default_oea["02068"]["CatName"] = "Physical Development";

$default_oea["02069"]["ItemCode"] = "02069";
$default_oea["02069"]["ItemName"] = "South China Athlete Association - Open Tennis Championship  南華體育會 - 網球公開賽";
$default_oea["02069"]["CatCode"] = "3";
$default_oea["02069"]["CatName"] = "Physical Development";

$default_oea["02070"]["ItemCode"] = "02070";
$default_oea["02070"]["ItemName"] = "South Pole Ambassadors";
$default_oea["02070"]["CatCode"] = "2";
$default_oea["02070"]["CatName"] = "Career-related Experience";

$default_oea["02071"]["ItemCode"] = "02071";
$default_oea["02071"]["ItemName"] = "Southern District Beach Run Competition";
$default_oea["02071"]["CatCode"] = "3";
$default_oea["02071"]["CatName"] = "Physical Development";

$default_oea["02072"]["ItemCode"] = "02072";
$default_oea["02072"]["ItemName"] = "Sowers Action Social Service Programme";
$default_oea["02072"]["CatCode"] = "1";
$default_oea["02072"]["CatName"] = "Community Service";

$default_oea["02073"]["ItemCode"] = "02073";
$default_oea["02073"]["ItemName"] = "Sowers Action Young Ambassador Program (SAYA)";
$default_oea["02073"]["CatCode"] = "1";
$default_oea["02073"]["CatName"] = "Community Service";

$default_oea["02074"]["ItemCode"] = "02074";
$default_oea["02074"]["ItemName"] = "Special Olympics World Summer Games  特殊奧運會夏季世界賽 (Please provide details in \"Description\" box.)";
$default_oea["02074"]["CatCode"] = "3";
$default_oea["02074"]["CatName"] = "Physical Development";

$default_oea["02075"]["ItemCode"] = "02075";
$default_oea["02075"]["ItemName"] = "Special Olympics World Winter Games  特殊冬季奧運會 (Please provide details in \"Description\" box.)";
$default_oea["02075"]["CatCode"] = "3";
$default_oea["02075"]["CatName"] = "Physical Development";

$default_oea["02076"]["ItemCode"] = "02076";
$default_oea["02076"]["ItemName"] = "Speed Skating Tournament - Shiqiao, Panyu  番禺市橋速度輪滑邀請賽";
$default_oea["02076"]["CatCode"] = "3";
$default_oea["02076"]["CatName"] = "Physical Development";

$default_oea["02077"]["ItemCode"] = "02077";
$default_oea["02077"]["ItemName"] = "Spinal Cord Injury Awareness Day脊髓損傷基金會脊髓損傷關注日- 感動人、人感動";
$default_oea["02077"]["CatCode"] = "1";
$default_oea["02077"]["CatName"] = "Community Service";

$default_oea["02079"]["ItemCode"] = "02079";
$default_oea["02079"]["ItemName"] = "Sportexcel-NSC-Junior Circuit  Sportexcel-NSC-青少年巡迴賽(世界)";
$default_oea["02079"]["CatCode"] = "3";
$default_oea["02079"]["CatName"] = "Physical Development";

$default_oea["02080"]["ItemCode"] = "02080";
$default_oea["02080"]["ItemName"] = "Sports Association(New Territories) 新界學界體育協進會  (Please provide details in \"Description\" box.)";
$default_oea["02080"]["CatCode"] = "3";
$default_oea["02080"]["CatName"] = "Physical Development";

$default_oea["02081"]["ItemCode"] = "02081";
$default_oea["02081"]["ItemName"] = "Sports Coaching and Management-Taster Program 運動管理與教練法導引課程";
$default_oea["02081"]["CatCode"] = "3";
$default_oea["02081"]["CatName"] = "Physical Development";

$default_oea["02082"]["ItemCode"] = "02082";
$default_oea["02082"]["ItemName"] = "Sports Day/ Carnival / Competition (School) 陸運會 (Please provide details in \"Description\" box.)";
$default_oea["02082"]["CatCode"] = "3";
$default_oea["02082"]["CatName"] = "Physical Development";

$default_oea["02083"]["ItemCode"] = "02083";
$default_oea["02083"]["ItemName"] = "Sports match commentary report體育賽事評論報告";
$default_oea["02083"]["CatCode"] = "4";
$default_oea["02083"]["CatName"] = "Aesthetic Development";

$default_oea["02084"]["ItemCode"] = "02084";
$default_oea["02084"]["ItemName"] = "Sri Lanka Women's 7's Tournament";
$default_oea["02084"]["CatCode"] = "3";
$default_oea["02084"]["CatName"] = "Physical Development";

$default_oea["02085"]["ItemCode"] = "02085";
$default_oea["02085"]["ItemName"] = "Stamp Exhibits Competition  校際郵集設計比賽";
$default_oea["02085"]["CatCode"] = "4";
$default_oea["02085"]["CatName"] = "Aesthetic Development";

$default_oea["02086"]["ItemCode"] = "02086";
$default_oea["02086"]["ItemName"] = "Standard Chartered 2010-2011 Colts 7s Tournament";
$default_oea["02086"]["CatCode"] = "3";
$default_oea["02086"]["CatName"] = "Physical Development";

$default_oea["02087"]["ItemCode"] = "02087";
$default_oea["02087"]["ItemName"] = "Standard Chartered ARTS IN THE PARK Mardi Gras  渣打藝趣嘉年華";
$default_oea["02087"]["CatCode"] = "4";
$default_oea["02087"]["CatName"] = "Aesthetic Development";

$default_oea["02088"]["ItemCode"] = "02088";
$default_oea["02088"]["ItemName"] = "Standard Chartered Hong Kong Marathon  渣打馬拉松";
$default_oea["02088"]["CatCode"] = "3";
$default_oea["02088"]["CatName"] = "Physical Development";

$default_oea["02089"]["ItemCode"] = "02089";
$default_oea["02089"]["ItemName"] = "Standard Chartered Speech Contest";
$default_oea["02089"]["CatCode"] = "2";
$default_oea["02089"]["CatName"] = "Career-related Experience";

$default_oea["02090"]["ItemCode"] = "02090";
$default_oea["02090"]["ItemName"] = "Stanford University Education Program for Gifted Youth (EPGY) Academic Talent Search  美國史丹福大學 EPGY(數學及英文寫作)資優生選拔大賽";
$default_oea["02090"]["CatCode"] = "2";
$default_oea["02090"]["CatName"] = "Career-related Experience";

$default_oea["02091"]["ItemCode"] = "02091";
$default_oea["02091"]["ItemName"] = "Star-Gazing Activity";
$default_oea["02091"]["CatCode"] = "4";
$default_oea["02091"]["CatName"] = "Aesthetic Development";

$default_oea["02092"]["ItemCode"] = "02092";
$default_oea["02092"]["ItemName"] = "StarRiver National Children FineArts Photo Contest星星河全國少年兒童美術書法攝影大賽";
$default_oea["02092"]["CatCode"] = "4";
$default_oea["02092"]["CatName"] = "Aesthetic Development";

$default_oea["02093"]["ItemCode"] = "02093";
$default_oea["02093"]["ItemName"] = "Starting the Successful Journey 成功之旅‧今日啟航";
$default_oea["02093"]["CatCode"] = "2";
$default_oea["02093"]["CatName"] = "Career-related Experience";

$default_oea["02096"]["ItemCode"] = "02096";
$default_oea["02096"]["ItemName"] = "Steinway & Sons International Youth Piano Competition (Hong Kong and Macau Region)  施坦威國際青少年鋼琴比賽(港澳賽區比賽)";
$default_oea["02096"]["CatCode"] = "4";
$default_oea["02096"]["CatName"] = "Aesthetic Development";

$default_oea["02097"]["ItemCode"] = "02097";
$default_oea["02097"]["ItemName"] = "Steinway Children and Youth Piano Competition (China Regional Competition)  施坦威青少年鋼琴比賽(中國賽區)";
$default_oea["02097"]["CatCode"] = "4";
$default_oea["02097"]["CatName"] = "Aesthetic Development";

$default_oea["02098"]["ItemCode"] = "02098";
$default_oea["02098"]["ItemName"] = "Storm Cup Korea International Open Tenpin Bowling Championships  韓國國際保齡球錦標賽";
$default_oea["02098"]["CatCode"] = "3";
$default_oea["02098"]["CatName"] = "Physical Development";

$default_oea["02099"]["ItemCode"] = "02099";
$default_oea["02099"]["ItemName"] = "Strait Children Calligraphy and Painting Competition";
$default_oea["02099"]["CatCode"] = "4";
$default_oea["02099"]["CatName"] = "Aesthetic Development";

$default_oea["02100"]["ItemCode"] = "02100";
$default_oea["02100"]["ItemName"] = "Student Ambassador Award Scheme";
$default_oea["02100"]["CatCode"] = "2";
$default_oea["02100"]["CatName"] = "Career-related Experience";

$default_oea["02101"]["ItemCode"] = "02101";
$default_oea["02101"]["ItemName"] = "Student behaviour Leap Project";
$default_oea["02101"]["CatCode"] = "5";
$default_oea["02101"]["CatName"] = "Moral and Civic Education";

$default_oea["02102"]["ItemCode"] = "02102";
$default_oea["02102"]["ItemName"] = "Student Concert For Lok Sin Tong Schools樂善堂音樂教育培育計劃";
$default_oea["02102"]["CatCode"] = "4";
$default_oea["02102"]["CatName"] = "Aesthetic Development";

$default_oea["02103"]["ItemCode"] = "02103";
$default_oea["02103"]["ItemName"] = "Student Environmental Protection Ambassador Merit Award - Environmental Campaign Committee";
$default_oea["02103"]["CatCode"] = "1";
$default_oea["02103"]["CatName"] = "Community Service";

$default_oea["02104"]["ItemCode"] = "02104";
$default_oea["02104"]["ItemName"] = "Student Environmental Protection Ambassador of the Schools Environmental Award Scheme";
$default_oea["02104"]["CatCode"] = "5";
$default_oea["02104"]["CatName"] = "Moral and Civic Education";

$default_oea["02105"]["ItemCode"] = "02105";
$default_oea["02105"]["ItemName"] = "Student Health Ambassador Training Programme 學生健康大使訓練計劃";
$default_oea["02105"]["CatCode"] = "1";
$default_oea["02105"]["CatName"] = "Community Service";

$default_oea["02106"]["ItemCode"] = "02106";
$default_oea["02106"]["ItemName"] = "Student Union - Chairperson of Houses / school activities";
$default_oea["02106"]["CatCode"] = "1";
$default_oea["02106"]["CatName"] = "Community Service";

$default_oea["02107"]["ItemCode"] = "02107";
$default_oea["02107"]["ItemName"] = "Student Union - Election of Students Organizations 學生組織選舉";
$default_oea["02107"]["CatCode"] = "2";
$default_oea["02107"]["CatName"] = "Career-related Experience";

$default_oea["02108"]["ItemCode"] = "02108";
$default_oea["02108"]["ItemName"] = "Student Union - Form Association/ Committee 級會 (Please provide details in \"Description\" box.)";
$default_oea["02108"]["CatCode"] = "1";
$default_oea["02108"]["CatName"] = "Community Service";

$default_oea["02109"]["ItemCode"] = "02109";
$default_oea["02109"]["ItemName"] = "Student Union - SMUD (St Mary's Union Day)";
$default_oea["02109"]["CatCode"] = "1";
$default_oea["02109"]["CatName"] = "Community Service";

$default_oea["02110"]["ItemCode"] = "02110";
$default_oea["02110"]["ItemName"] = "Student Union / Student Council (Please provide details in \"Description\" box.)";
$default_oea["02110"]["CatCode"] = "1";
$default_oea["02110"]["CatName"] = "Community Service";

$default_oea["02111"]["ItemCode"] = "02111";
$default_oea["02111"]["ItemName"] = "Student Voice: School Improvement 學生聲音:學校發展";
$default_oea["02111"]["CatCode"] = "4";
$default_oea["02111"]["CatName"] = "Aesthetic Development";

$default_oea["02113"]["ItemCode"] = "02113";
$default_oea["02113"]["ItemName"] = "Student's Blog Promotion Scheme 學生網誌寫作推廣計劃";
$default_oea["02113"]["CatCode"] = "4";
$default_oea["02113"]["CatName"] = "Aesthetic Development";

$default_oea["02112"]["ItemCode"] = "02112";
$default_oea["02112"]["ItemName"] = "Students as Learning Experience Designers Project";
$default_oea["02112"]["CatCode"] = "4";
$default_oea["02112"]["CatName"] = "Aesthetic Development";

$default_oea["02114"]["ItemCode"] = "02114";
$default_oea["02114"]["ItemName"] = "Students Indoor Volleyball Asia Championship";
$default_oea["02114"]["CatCode"] = "3";
$default_oea["02114"]["CatName"] = "Physical Development";

$default_oea["02115"]["ItemCode"] = "02115";
$default_oea["02115"]["ItemName"] = "Study trip (Please provide details in \"Description\" box.) ";
$default_oea["02115"]["CatCode"] = "2";
$default_oea["02115"]["CatName"] = "Career-related Experience";

$default_oea["02094"]["ItemCode"] = "02094";
$default_oea["02094"]["ItemName"] = "STCP Best Improved Team Member多元智能躍進計劃最佳進步獎";
$default_oea["02094"]["CatCode"] = "2";
$default_oea["02094"]["CatName"] = "Career-related Experience";

$default_oea["02095"]["ItemCode"] = "02095";
$default_oea["02095"]["ItemName"] = "STCP Best Team Member多元智能躍進計劃最佳學員";
$default_oea["02095"]["CatCode"] = "2";
$default_oea["02095"]["CatName"] = "Career-related Experience";

$default_oea["02116"]["ItemCode"] = "02116";
$default_oea["02116"]["ItemName"] = "Su Lin City Taekwondo Open  樹林巿長盃暨國際跆拳道公開錦標賽";
$default_oea["02116"]["CatCode"] = "3";
$default_oea["02116"]["CatName"] = "Physical Development";

$default_oea["02117"]["ItemCode"] = "02117";
$default_oea["02117"]["ItemName"] = "Suen King-sau Cup Hundred Cities Children's Story Teliing Invitational Competition  孫敬修杯百城兒童講故事邀請賽";
$default_oea["02117"]["CatCode"] = "4";
$default_oea["02117"]["CatName"] = "Aesthetic Development";

$default_oea["02118"]["ItemCode"] = "02118";
$default_oea["02118"]["ItemName"] = "Summer Internship (Please provide details in \"Description\" box.)";
$default_oea["02118"]["CatCode"] = "2";
$default_oea["02118"]["CatName"] = "Career-related Experience";

$default_oea["02119"]["ItemCode"] = "02119";
$default_oea["02119"]["ItemName"] = "Summer Poster Design暑期海報設計比賽";
$default_oea["02119"]["CatCode"] = "4";
$default_oea["02119"]["CatName"] = "Aesthetic Development";

$default_oea["02120"]["ItemCode"] = "02120";
$default_oea["02120"]["ItemName"] = "Summer Youth Olympic Games  夏季青年奧林匹克運動會 (Please provide details in \"Description\" box.)";
$default_oea["02120"]["CatCode"] = "3";
$default_oea["02120"]["CatName"] = "Physical Development";

$default_oea["02121"]["ItemCode"] = "02121";
$default_oea["02121"]["ItemName"] = "Super Sports Satellite Tournament Champion  吉隆坡壁球衛星賽";
$default_oea["02121"]["CatCode"] = "3";
$default_oea["02121"]["CatName"] = "Physical Development";

$default_oea["02122"]["ItemCode"] = "02122";
$default_oea["02122"]["ItemName"] = "Sustainable City Design Competition  可持續發展都市設計專題研習比賽";
$default_oea["02122"]["CatCode"] = "2";
$default_oea["02122"]["CatName"] = "Career-related Experience";

$default_oea["02124"]["ItemCode"] = "02124";
$default_oea["02124"]["ItemName"] = "Swimmer of the Year (Please provide details in \"Description\" box.)";
$default_oea["02124"]["CatCode"] = "3";
$default_oea["02124"]["CatName"] = "Physical Development";

$default_oea["02125"]["ItemCode"] = "02125";
$default_oea["02125"]["ItemName"] = "Swimming Gala (New Territories) 新界區際水運大會";
$default_oea["02125"]["CatCode"] = "3";
$default_oea["02125"]["CatName"] = "Physical Development";

$default_oea["02126"]["ItemCode"] = "02126";
$default_oea["02126"]["ItemName"] = "Swimming Training Course游泳訓練班";
$default_oea["02126"]["CatCode"] = "3";
$default_oea["02126"]["CatName"] = "Physical Development";

$default_oea["02123"]["ItemCode"] = "02123";
$default_oea["02123"]["ItemName"] = "SWAYDER Hong Kong Youth & Children Piano Competition  詩威德盃香港少年兒童鋼琴比賽";
$default_oea["02123"]["CatCode"] = "4";
$default_oea["02123"]["CatName"] = "Aesthetic Development";

$default_oea["02250"]["ItemCode"] = "02250";
$default_oea["02250"]["ItemName"] = "T-Net型人健康網絡教育計劃";
$default_oea["02250"]["CatCode"] = "4";
$default_oea["02250"]["CatName"] = "Aesthetic Development";

$default_oea["02257"]["ItemCode"] = "02257";
$default_oea["02257"]["ItemName"] = "T-shirt and Badge Design Competition";
$default_oea["02257"]["CatCode"] = "1";
$default_oea["02257"]["CatName"] = "Community Service";

$default_oea["02127"]["ItemCode"] = "02127";
$default_oea["02127"]["ItemName"] = "Table Tennis Umpire Training Course for NewTerritories.新界地域中學乒乓球初級裁判班";
$default_oea["02127"]["CatCode"] = "3";
$default_oea["02127"]["CatName"] = "Physical Development";

$default_oea["02129"]["ItemCode"] = "02129";
$default_oea["02129"]["ItemName"] = "Tai Po District Table Tennis Competition 大埔區乒乓球比賽";
$default_oea["02129"]["CatCode"] = "3";
$default_oea["02129"]["CatName"] = "Physical Development";

$default_oea["02130"]["ItemCode"] = "02130";
$default_oea["02130"]["ItemName"] = "Tai-Chi Competition";
$default_oea["02130"]["CatCode"] = "3";
$default_oea["02130"]["CatName"] = "Physical Development";

$default_oea["02131"]["ItemCode"] = "02131";
$default_oea["02131"]["ItemName"] = "Taichung World Youth Football Festival  台中世界青少年足球節";
$default_oea["02131"]["CatCode"] = "3";
$default_oea["02131"]["CatName"] = "Physical Development";

$default_oea["02132"]["ItemCode"] = "02132";
$default_oea["02132"]["ItemName"] = "Taipei Children's Arts Festival - Actors in Drama";
$default_oea["02132"]["CatCode"] = "4";
$default_oea["02132"]["CatName"] = "Aesthetic Development";

$default_oea["02133"]["ItemCode"] = "02133";
$default_oea["02133"]["ItemName"] = "Taipei City Teenage Baseball Tournament  台北國際城市青少年棒球對抗賽";
$default_oea["02133"]["CatCode"] = "3";
$default_oea["02133"]["CatName"] = "Physical Development";

$default_oea["02134"]["ItemCode"] = "02134";
$default_oea["02134"]["ItemName"] = "Taipei Fencing Open Competition cum United-Chin Fencing Cup (Taiwan)  台灣劍擊公開賽";
$default_oea["02134"]["CatCode"] = "3";
$default_oea["02134"]["CatName"] = "Physical Development";

$default_oea["02135"]["ItemCode"] = "02135";
$default_oea["02135"]["ItemName"] = "Taipei Open & United-Shin Fencing Cup";
$default_oea["02135"]["CatCode"] = "3";
$default_oea["02135"]["CatName"] = "Physical Development";

$default_oea["02136"]["ItemCode"] = "02136";
$default_oea["02136"]["ItemName"] = "Taipei Open International Judo Tournament  台北國際柔道公開賽";
$default_oea["02136"]["CatCode"] = "3";
$default_oea["02136"]["CatName"] = "Physical Development";

$default_oea["02137"]["ItemCode"] = "02137";
$default_oea["02137"]["ItemName"] = "Taipei Table Tennis Open For The Disabled";
$default_oea["02137"]["CatCode"] = "3";
$default_oea["02137"]["CatName"] = "Physical Development";

$default_oea["02138"]["ItemCode"] = "02138";
$default_oea["02138"]["ItemName"] = "Taiwan Cup: International Schools Handball Competition";
$default_oea["02138"]["CatCode"] = "3";
$default_oea["02138"]["CatName"] = "Physical Development";

$default_oea["02139"]["ItemCode"] = "02139";
$default_oea["02139"]["ItemName"] = "Taiwan Handbell Performance and Exchange Program";
$default_oea["02139"]["CatCode"] = "4";
$default_oea["02139"]["CatName"] = "Aesthetic Development";

$default_oea["02140"]["ItemCode"] = "02140";
$default_oea["02140"]["ItemName"] = "Taiwan Impression Postcard Design Competition印象台灣香港風采明信片設計比賽";
$default_oea["02140"]["CatCode"] = "4";
$default_oea["02140"]["CatName"] = "Aesthetic Development";

$default_oea["02141"]["ItemCode"] = "02141";
$default_oea["02141"]["ItemName"] = "Taiwan Junior Single Championship (17 yrs & under)  台灣青少年十七歲或以下錦標賽單打";
$default_oea["02141"]["CatCode"] = "3";
$default_oea["02141"]["CatName"] = "Physical Development";

$default_oea["02142"]["ItemCode"] = "02142";
$default_oea["02142"]["ItemName"] = "Taiwan Local Junior Ranking Championship  輝安盃全國青少年網球排名賽";
$default_oea["02142"]["CatCode"] = "3";
$default_oea["02142"]["CatName"] = "Physical Development";

$default_oea["02143"]["ItemCode"] = "02143";
$default_oea["02143"]["ItemName"] = "Taiwan National Tie Zhen Cup Handball Championship  台灣鐵砧杯手球錦標賽";
$default_oea["02143"]["CatCode"] = "3";
$default_oea["02143"]["CatName"] = "Physical Development";

$default_oea["02144"]["ItemCode"] = "02144";
$default_oea["02144"]["ItemName"] = "Taiwan Tien Jian Cup Handball Tournament";
$default_oea["02144"]["CatCode"] = "3";
$default_oea["02144"]["CatName"] = "Physical Development";

$default_oea["02145"]["ItemCode"] = "02145";
$default_oea["02145"]["ItemName"] = "Taiwan, China, Hong Kong and Macau Youth Canoe Competition";
$default_oea["02145"]["CatCode"] = "3";
$default_oea["02145"]["CatName"] = "Physical Development";

$default_oea["02146"]["ItemCode"] = "02146";
$default_oea["02146"]["ItemName"] = "Taiyuan International Junior Table Tennis Open Championship  太原國際青少年乒乓球公開賽";
$default_oea["02146"]["CatCode"] = "3";
$default_oea["02146"]["CatName"] = "Physical Development";

$default_oea["02147"]["ItemCode"] = "02147";
$default_oea["02147"]["ItemName"] = "Tak Sun Family Fun Day";
$default_oea["02147"]["CatCode"] = "4";
$default_oea["02147"]["CatName"] = "Aesthetic Development";

$default_oea["02128"]["ItemCode"] = "02128";
$default_oea["02128"]["ItemName"] = "TAFISA World Sport For All Games  世界全民運動大會";
$default_oea["02128"]["CatCode"] = "3";
$default_oea["02128"]["CatName"] = "Physical Development";

$default_oea["02148"]["ItemCode"] = "02148";
$default_oea["02148"]["ItemName"] = "TASA Development Fund of The Referee and Coach Age Group Swimming Meet";
$default_oea["02148"]["CatCode"] = "3";
$default_oea["02148"]["CatName"] = "Physical Development";

$default_oea["02149"]["ItemCode"] = "02149";
$default_oea["02149"]["ItemName"] = "Technology & Innovation Team 創新科技隊";
$default_oea["02149"]["CatCode"] = "2";
$default_oea["02149"]["CatName"] = "Career-related Experience";

$default_oea["02150"]["ItemCode"] = "02150";
$default_oea["02150"]["ItemName"] = "Technology & Renewable Energy Events (Wind Power Generator)  應用可再生能源設計暨競技大賽 - 風力發電機大賽";
$default_oea["02150"]["CatCode"] = "2";
$default_oea["02150"]["CatName"] = "Career-related Experience";

$default_oea["02151"]["ItemCode"] = "02151";
$default_oea["02151"]["ItemName"] = "Teen Entrepreneurs Competition  少年企業家大比拼";
$default_oea["02151"]["CatCode"] = "2";
$default_oea["02151"]["CatName"] = "Career-related Experience";

$default_oea["02152"]["ItemCode"] = "02152";
$default_oea["02152"]["ItemName"] = "Teen Team Challenge";
$default_oea["02152"]["CatCode"] = "1";
$default_oea["02152"]["CatName"] = "Community Service";

$default_oea["02153"]["ItemCode"] = "02153";
$default_oea["02153"]["ItemName"] = "Teen Time Radio Programme";
$default_oea["02153"]["CatCode"] = "4";
$default_oea["02153"]["CatName"] = "Aesthetic Development";

$default_oea["02154"]["ItemCode"] = "02154";
$default_oea["02154"]["ItemName"] = "Teenagers Judo Public Competition - The Judo Association of Hong Kong";
$default_oea["02154"]["CatCode"] = "3";
$default_oea["02154"]["CatName"] = "Physical Development";

$default_oea["02155"]["ItemCode"] = "02155";
$default_oea["02155"]["ItemName"] = "Tennis Competition: Boys分齡網球比賽：男子青少年組";
$default_oea["02155"]["CatCode"] = "4";
$default_oea["02155"]["CatName"] = "Aesthetic Development";

$default_oea["02156"]["ItemCode"] = "02156";
$default_oea["02156"]["ItemName"] = "Tennis Course網球課程";
$default_oea["02156"]["CatCode"] = "3";
$default_oea["02156"]["CatName"] = "Physical Development";

$default_oea["02157"]["ItemCode"] = "02157";
$default_oea["02157"]["ItemName"] = "Tennis Team網球隊";
$default_oea["02157"]["CatCode"] = "3";
$default_oea["02157"]["CatName"] = "Physical Development";

$default_oea["02158"]["ItemCode"] = "02158";
$default_oea["02158"]["ItemName"] = "Thai Airways International Mistral Youth And Junior Windsurfing World Championship";
$default_oea["02158"]["CatCode"] = "3";
$default_oea["02158"]["CatName"] = "Physical Development";

$default_oea["02159"]["ItemCode"] = "02159";
$default_oea["02159"]["ItemName"] = "Thailand International Judo Championship  泰國國際柔道錦標賽";
$default_oea["02159"]["CatCode"] = "3";
$default_oea["02159"]["CatName"] = "Physical Development";

$default_oea["02160"]["ItemCode"] = "02160";
$default_oea["02160"]["ItemName"] = "Thailand Open Fencing";
$default_oea["02160"]["CatCode"] = "3";
$default_oea["02160"]["CatName"] = "Physical Development";

$default_oea["02161"]["ItemCode"] = "02161";
$default_oea["02161"]["ItemName"] = "Thailand Swimming Meet";
$default_oea["02161"]["CatCode"] = "3";
$default_oea["02161"]["CatName"] = "Physical Development";

$default_oea["02162"]["ItemCode"] = "02162";
$default_oea["02162"]["ItemName"] = "Thailand Water Polo International Championship";
$default_oea["02162"]["CatCode"] = "3";
$default_oea["02162"]["CatName"] = "Physical Development";

$default_oea["02163"]["ItemCode"] = "02163";
$default_oea["02163"]["ItemName"] = "Thailand Windsurfing Championships";
$default_oea["02163"]["CatCode"] = "3";
$default_oea["02163"]["CatName"] = "Physical Development";

$default_oea["02164"]["ItemCode"] = "02164";
$default_oea["02164"]["ItemName"] = "Thammasat University International Swimming Championship  泰國總理杯法政大學國際游泳冠軍賽";
$default_oea["02164"]["CatCode"] = "3";
$default_oea["02164"]["CatName"] = "Physical Development";

$default_oea["02165"]["ItemCode"] = "02165";
$default_oea["02165"]["ItemName"] = "The ABWE Shing Yan Christian Social Service Carnival盛恩中心社區嘉年華";
$default_oea["02165"]["CatCode"] = "4";
$default_oea["02165"]["CatName"] = "Aesthetic Development";

$default_oea["02166"]["ItemCode"] = "02166";
$default_oea["02166"]["ItemName"] = "The All China Universities Students Fencing Championships  全國大學生劍擊錦標賽";
$default_oea["02166"]["CatCode"] = "3";
$default_oea["02166"]["CatName"] = "Physical Development";

$default_oea["02167"]["ItemCode"] = "02167";
$default_oea["02167"]["ItemName"] = "The All China University Student Badminton Championship  全國大學生羽毛球錦標賽";
$default_oea["02167"]["CatCode"] = "3";
$default_oea["02167"]["CatName"] = "Physical Development";

$default_oea["02168"]["ItemCode"] = "02168";
$default_oea["02168"]["ItemName"] = "The Anderson Memorial Sports Prize";
$default_oea["02168"]["CatCode"] = "3";
$default_oea["02168"]["CatName"] = "Physical Development";

$default_oea["02169"]["ItemCode"] = "02169";
$default_oea["02169"]["ItemName"] = "The Art Miles Mural Project  百里延綿壁畫行動";
$default_oea["02169"]["CatCode"] = "4";
$default_oea["02169"]["CatName"] = "Aesthetic Development";

$default_oea["02170"]["ItemCode"] = "02170";
$default_oea["02170"]["ItemName"] = "The Art of CUHK  中大藝術本科生畢業展";
$default_oea["02170"]["CatCode"] = "4";
$default_oea["02170"]["CatName"] = "Aesthetic Development";

$default_oea["02171"]["ItemCode"] = "02171";
$default_oea["02171"]["ItemName"] = "The ASME International Student Design Contest  ASME國際學生設計比賽";
$default_oea["02171"]["CatCode"] = "4";
$default_oea["02171"]["CatName"] = "Aesthetic Development";

$default_oea["02172"]["ItemCode"] = "02172";
$default_oea["02172"]["ItemName"] = "The Beijing National Speech Competition";
$default_oea["02172"]["CatCode"] = "2";
$default_oea["02172"]["CatName"] = "Career-related Experience";

$default_oea["02173"]["ItemCode"] = "02173";
$default_oea["02173"]["ItemName"] = "The Best Performance in ECA課外活動優良表現獎";
$default_oea["02173"]["CatCode"] = "4";
$default_oea["02173"]["CatName"] = "Aesthetic Development";

$default_oea["02175"]["ItemCode"] = "02175";
$default_oea["02175"]["ItemName"] = "The Boy's Brigade - The President's Badge / The Founder's Badge / The Queen's Badge  香港基督少年軍 - 會長章 / 創辦人章 / 女皇章";
$default_oea["02175"]["CatCode"] = "1";
$default_oea["02175"]["CatName"] = "Community Service";

$default_oea["02176"]["ItemCode"] = "02176";
$default_oea["02176"]["ItemName"] = "The Boy's Brigade 基督少年軍 (Please provide details in \"Description\" box.)";
$default_oea["02176"]["CatCode"] = "1";
$default_oea["02176"]["CatName"] = "Community Service";

$default_oea["02174"]["ItemCode"] = "02174";
$default_oea["02174"]["ItemName"] = "The Boys' and Girls' Clubs Association of Hong Kong (香港小童群益會)";
$default_oea["02174"]["CatCode"] = "1";
$default_oea["02174"]["CatName"] = "Community Service";

$default_oea["02178"]["ItemCode"] = "02178";
$default_oea["02178"]["ItemName"] = "The Caritas and Junior Chamber International";
$default_oea["02178"]["CatCode"] = "5";
$default_oea["02178"]["CatName"] = "Moral and Civic Education";

$default_oea["02179"]["ItemCode"] = "02179";
$default_oea["02179"]["ItemName"] = "The Carlton Trophy Competition  全港嘉爾頓錦標賽";
$default_oea["02179"]["CatCode"] = "3";
$default_oea["02179"]["CatName"] = "Physical Development";

$default_oea["02177"]["ItemCode"] = "02177";
$default_oea["02177"]["ItemName"] = "The CAA International Student Design Competition: An EcoFriendly Travellers Hotel  CAA國際學生設計比賽：環保旅館";
$default_oea["02177"]["CatCode"] = "4";
$default_oea["02177"]["CatName"] = "Aesthetic Development";

$default_oea["02180"]["ItemCode"] = "02180";
$default_oea["02180"]["ItemName"] = "The Centenary International Cadet Camp";
$default_oea["02180"]["CatCode"] = "5";
$default_oea["02180"]["CatName"] = "Moral and Civic Education";

$default_oea["02181"]["ItemCode"] = "02181";
$default_oea["02181"]["ItemName"] = "The Chinese Taipei DanceSport Championship  中國體育舞蹈錦標賽";
$default_oea["02181"]["CatCode"] = "4";
$default_oea["02181"]["CatName"] = "Aesthetic Development";

$default_oea["02182"]["ItemCode"] = "02182";
$default_oea["02182"]["ItemName"] = "The Chinese Teenagers Song Writing Competition";
$default_oea["02182"]["CatCode"] = "4";
$default_oea["02182"]["CatName"] = "Aesthetic Development";

$default_oea["02183"]["ItemCode"] = "02183";
$default_oea["02183"]["ItemName"] = "The Dandelion Organization Committee for Trials of Chinese Youngsters' Artistic Stars  蒲公英中國青少年藝術新人選拔大賽";
$default_oea["02183"]["CatCode"] = "4";
$default_oea["02183"]["CatName"] = "Aesthetic Development";

$default_oea["02184"]["ItemCode"] = "02184";
$default_oea["02184"]["ItemName"] = "The Duke of Edinburgh's Award in Austratlia";
$default_oea["02184"]["CatCode"] = "5";
$default_oea["02184"]["CatName"] = "Moral and Civic Education";

$default_oea["02185"]["ItemCode"] = "02185";
$default_oea["02185"]["ItemName"] = "The eXplora Challenge";
$default_oea["02185"]["CatCode"] = "5";
$default_oea["02185"]["CatName"] = "Moral and Civic Education";

$default_oea["02186"]["ItemCode"] = "02186";
$default_oea["02186"]["ItemName"] = "The First World Chinese Youth Forum Dragons in China";
$default_oea["02186"]["CatCode"] = "2";
$default_oea["02186"]["CatName"] = "Career-related Experience";

$default_oea["02187"]["ItemCode"] = "02187";
$default_oea["02187"]["ItemName"] = "The Girls' Brigade Hong Kong Flag Selling Day";
$default_oea["02187"]["CatCode"] = "1";
$default_oea["02187"]["CatName"] = "Community Service";

$default_oea["02188"]["ItemCode"] = "02188";
$default_oea["02188"]["ItemName"] = "The Girls' Brigade Hong Kong 香港基督女少年軍 (Please provide details in \"Description\" box.)";
$default_oea["02188"]["CatCode"] = "1";
$default_oea["02188"]["CatName"] = "Community Service";

$default_oea["02189"]["ItemCode"] = "02189";
$default_oea["02189"]["ItemName"] = "The Global Chinese Arts Festival";
$default_oea["02189"]["CatCode"] = "4";
$default_oea["02189"]["CatName"] = "Aesthetic Development";

$default_oea["02190"]["ItemCode"] = "02190";
$default_oea["02190"]["ItemName"] = "The Hong Kong Academy for Gifted Education (HKAGE) -  School Nomination (S.3 - S.5)";
$default_oea["02190"]["CatCode"] = "2";
$default_oea["02190"]["CatName"] = "Career-related Experience";

$default_oea["02191"]["ItemCode"] = "02191";
$default_oea["02191"]["ItemName"] = "The Hong Kong Association for Computer Education - \"Personal Social & Humanities Education Category\" of the Computer-Assisted Project Design Contest - ";
$default_oea["02191"]["CatCode"] = "2";
$default_oea["02191"]["CatName"] = "Career-related Experience";

$default_oea["02192"]["ItemCode"] = "02192";
$default_oea["02192"]["ItemName"] = "The Hong Kong Association for the Spastic Children(香港耀能協會) - 週年大會暨頒獎典禮(AGM)";
$default_oea["02192"]["CatCode"] = "1";
$default_oea["02192"]["CatName"] = "Community Service";

$default_oea["02193"]["ItemCode"] = "02193";
$default_oea["02193"]["ItemName"] = "The Hong Kong Award for Young People香港青年獎勵計劃";
$default_oea["02193"]["CatCode"] = "2";
$default_oea["02193"]["CatName"] = "Career-related Experience";

$default_oea["02194"]["ItemCode"] = "02194";
$default_oea["02194"]["ItemName"] = "The Hong Kong Bar Association Debating Competition  香港大律師公會辯論賽";
$default_oea["02194"]["CatCode"] = "5";
$default_oea["02194"]["CatName"] = "Moral and Civic Education";

$default_oea["02195"]["ItemCode"] = "02195";
$default_oea["02195"]["ItemName"] = "The Hong Kong Girl Guides Association";
$default_oea["02195"]["CatCode"] = "1";
$default_oea["02195"]["CatName"] = "Community Service";

$default_oea["02196"]["ItemCode"] = "02196";
$default_oea["02196"]["ItemName"] = "The Hong Kong Girl Guides Association - Chief Commissioner’s Guide Award -";
$default_oea["02196"]["CatCode"] = "1";
$default_oea["02196"]["CatName"] = "Community Service";

$default_oea["02197"]["ItemCode"] = "02197";
$default_oea["02197"]["ItemName"] = "The Hong Kong Girl Guides Association - Silver Service Flash";
$default_oea["02197"]["CatCode"] = "1";
$default_oea["02197"]["CatName"] = "Community Service";

$default_oea["02198"]["ItemCode"] = "02198";
$default_oea["02198"]["ItemName"] = "The Hong Kong Mathematics Creative Problem-Solving Competition for Secondary Schools  香港中學數學創意解難比賽";
$default_oea["02198"]["CatCode"] = "2";
$default_oea["02198"]["CatName"] = "Career-related Experience";

$default_oea["02199"]["ItemCode"] = "02199";
$default_oea["02199"]["ItemName"] = "The Hong Kong Venture Scout Competition  全港深資童軍錦標賽";
$default_oea["02199"]["CatCode"] = "1";
$default_oea["02199"]["CatName"] = "Community Service";

$default_oea["02200"]["ItemCode"] = "02200";
$default_oea["02200"]["ItemName"] = "The Hong Kong Youth Cultural and Arts Competitions - Couplets Competition (Student Section)";
$default_oea["02200"]["CatCode"] = "4";
$default_oea["02200"]["CatName"] = "Aesthetic Development";

$default_oea["02201"]["ItemCode"] = "02201";
$default_oea["02201"]["ItemName"] = "The Huayu Ziben Cup 6th All China Youth Fencing Championship";
$default_oea["02201"]["CatCode"] = "3";
$default_oea["02201"]["CatName"] = "Physical Development";

$default_oea["02202"]["ItemCode"] = "02202";
$default_oea["02202"]["ItemName"] = "The IET Innovation Awards";
$default_oea["02202"]["CatCode"] = "2";
$default_oea["02202"]["CatName"] = "Career-related Experience";

$default_oea["02203"]["ItemCode"] = "02203";
$default_oea["02203"]["ItemName"] = "The Inaugural FESPIC Youth Games  遠東及南太平洋區青少年傷殘人士運動會";
$default_oea["02203"]["CatCode"] = "3";
$default_oea["02203"]["CatName"] = "Physical Development";

$default_oea["02204"]["ItemCode"] = "02204";
$default_oea["02204"]["ItemName"] = "The Inter-city Online English Debating and Public Speaking Contest  網上雙城英語辯論及演講比賽";
$default_oea["02204"]["CatCode"] = "5";
$default_oea["02204"]["CatName"] = "Moral and Civic Education";

$default_oea["02205"]["ItemCode"] = "02205";
$default_oea["02205"]["ItemName"] = "The International Challenge 20/20 project, organized by the National Association of Independent Schools(NAIS)";
$default_oea["02205"]["CatCode"] = "5";
$default_oea["02205"]["CatName"] = "Moral and Civic Education";

$default_oea["02206"]["ItemCode"] = "02206";
$default_oea["02206"]["ItemName"] = "The International Children's Exhibition of Fine Arts LIDICE";
$default_oea["02206"]["CatCode"] = "4";
$default_oea["02206"]["CatName"] = "Aesthetic Development";

$default_oea["02207"]["ItemCode"] = "02207";
$default_oea["02207"]["ItemName"] = "The International Chinese Music and Arts Competition";
$default_oea["02207"]["CatCode"] = "4";
$default_oea["02207"]["CatName"] = "Aesthetic Development";

$default_oea["02208"]["ItemCode"] = "02208";
$default_oea["02208"]["ItemName"] = "The International Museum of Children's Art - Global Children and Youth Art Contest and Research Project";
$default_oea["02208"]["CatCode"] = "4";
$default_oea["02208"]["CatName"] = "Aesthetic Development";

$default_oea["02209"]["ItemCode"] = "02209";
$default_oea["02209"]["ItemName"] = "The International Music and Arts Competition  國際音樂藝術大賽";
$default_oea["02209"]["CatCode"] = "4";
$default_oea["02209"]["CatName"] = "Aesthetic Development";

$default_oea["02210"]["ItemCode"] = "02210";
$default_oea["02210"]["ItemName"] = "The International Public Speaking Competition (The English Speaking Union)";
$default_oea["02210"]["CatCode"] = "4";
$default_oea["02210"]["CatName"] = "Aesthetic Development";

$default_oea["02211"]["ItemCode"] = "02211";
$default_oea["02211"]["ItemName"] = "The JA New Leaders Programme 新領袖計劃";
$default_oea["02211"]["CatCode"] = "2";
$default_oea["02211"]["CatName"] = "Career-related Experience";

$default_oea["02212"]["ItemCode"] = "02212";
$default_oea["02212"]["ItemName"] = "The JAL Cup World Amateur Go Championship  JAL杯世界業餘GO錦標賽";
$default_oea["02212"]["CatCode"] = "4";
$default_oea["02212"]["CatName"] = "Aesthetic Development";

$default_oea["02213"]["ItemCode"] = "02213";
$default_oea["02213"]["ItemName"] = "The Joy of Music Festival 2010";
$default_oea["02213"]["CatCode"] = "4";
$default_oea["02213"]["CatName"] = "Aesthetic Development";

$default_oea["02214"]["ItemCode"] = "02214";
$default_oea["02214"]["ItemName"] = "The Law Society of Hong Kong (香港律師會) - Teen Forum 青Teen講場";
$default_oea["02214"]["CatCode"] = "5";
$default_oea["02214"]["CatName"] = "Moral and Civic Education";

$default_oea["02215"]["ItemCode"] = "02215";
$default_oea["02215"]["ItemName"] = "The Linguistics Wonderland : Exploring Human Language";
$default_oea["02215"]["CatCode"] = "4";
$default_oea["02215"]["CatName"] = "Aesthetic Development";

$default_oea["02216"]["ItemCode"] = "02216";
$default_oea["02216"]["ItemName"] = "The Lions Quest and Yodac Debating Competition";
$default_oea["02216"]["CatCode"] = "2";
$default_oea["02216"]["CatName"] = "Career-related Experience";

$default_oea["02217"]["ItemCode"] = "02217";
$default_oea["02217"]["ItemName"] = "The Meryl Innovation Award  Meryl創新大獎";
$default_oea["02217"]["CatCode"] = "2";
$default_oea["02217"]["CatName"] = "Career-related Experience";

$default_oea["02218"]["ItemCode"] = "02218";
$default_oea["02218"]["ItemName"] = "The National Outstanding Youth Children Fine Arts Exhibition of Calligraphic Works  全國青少年兒童優秀美術書法作品邀請展";
$default_oea["02218"]["CatCode"] = "4";
$default_oea["02218"]["CatName"] = "Aesthetic Development";

$default_oea["02219"]["ItemCode"] = "02219";
$default_oea["02219"]["ItemName"] = "The National Secondary and Primary School Fine Art and Calligraphy Competition  全國中小學生優秀美術書法作品邀請大賽";
$default_oea["02219"]["CatCode"] = "4";
$default_oea["02219"]["CatName"] = "Aesthetic Development";

$default_oea["02220"]["ItemCode"] = "02220";
$default_oea["02220"]["ItemName"] = "The National Tennis Championships  全國大學生網球錦標賽";
$default_oea["02220"]["CatCode"] = "3";
$default_oea["02220"]["CatName"] = "Physical Development";

$default_oea["02221"]["ItemCode"] = "02221";
$default_oea["02221"]["ItemName"] = "The National Youth Competition of Biology and Environmental Science";
$default_oea["02221"]["CatCode"] = "2";
$default_oea["02221"]["CatName"] = "Career-related Experience";

$default_oea["02222"]["ItemCode"] = "02222";
$default_oea["02222"]["ItemName"] = "The National Youth Drawing Competition (China CCTV)";
$default_oea["02222"]["CatCode"] = "4";
$default_oea["02222"]["CatName"] = "Aesthetic Development";

$default_oea["02223"]["ItemCode"] = "02223";
$default_oea["02223"]["ItemName"] = "The Olympic Sport and Literature Competition (International)";
$default_oea["02223"]["CatCode"] = "4";
$default_oea["02223"]["CatName"] = "Aesthetic Development";

$default_oea["02224"]["ItemCode"] = "02224";
$default_oea["02224"]["ItemName"] = "The OPEN Stage 打開戲流平台 (Please provide details in \"Description\" box.)";
$default_oea["02224"]["CatCode"] = "4";
$default_oea["02224"]["CatName"] = "Aesthetic Development";

$default_oea["02227"]["ItemCode"] = "02227";
$default_oea["02227"]["ItemName"] = "The Promotion of Proper Cantonese Pronunciation Ambassador - The Association for the Promotion of Proper Cantonese Pronunciation  學校粵語正音推廣大使";
$default_oea["02227"]["CatCode"] = "5";
$default_oea["02227"]["CatName"] = "Moral and Civic Education";

$default_oea["02225"]["ItemCode"] = "02225";
$default_oea["02225"]["ItemName"] = "The PRC All China University Games  全國大學生運動會";
$default_oea["02225"]["CatCode"] = "3";
$default_oea["02225"]["CatName"] = "Physical Development";

$default_oea["02226"]["ItemCode"] = "02226";
$default_oea["02226"]["ItemName"] = "The PRC National University Basketball Competition  全國大學生籃球錦標賽";
$default_oea["02226"]["CatCode"] = "3";
$default_oea["02226"]["CatName"] = "Physical Development";

$default_oea["02228"]["ItemCode"] = "02228";
$default_oea["02228"]["ItemName"] = "The Sarawak Chief Minister's Cup - Sarawak International Junior Golf Championship  Sarawak長官杯 - Sarawak國際青少年高球錦標賽";
$default_oea["02228"]["CatCode"] = "3";
$default_oea["02228"]["CatName"] = "Physical Development";

$default_oea["02229"]["ItemCode"] = "02229";
$default_oea["02229"]["ItemName"] = "The Schools Interport (Hong Kong & Macau) Sports Competition";
$default_oea["02229"]["CatCode"] = "3";
$default_oea["02229"]["CatName"] = "Physical Development";

$default_oea["02230"]["ItemCode"] = "02230";
$default_oea["02230"]["ItemName"] = "The Scout Voyager Award  童軍毅行獎章";
$default_oea["02230"]["CatCode"] = "1";
$default_oea["02230"]["CatName"] = "Community Service";

$default_oea["02231"]["ItemCode"] = "02231";
$default_oea["02231"]["ItemName"] = "The Shanghai China OM Association 20th Anniversary and Shanghai China Odyssey of the Mind Competition";
$default_oea["02231"]["CatCode"] = "2";
$default_oea["02231"]["CatName"] = "Career-related Experience";

$default_oea["02232"]["ItemCode"] = "02232";
$default_oea["02232"]["ItemName"] = "The Singapore Millennium International Open DanceSport Championships - Closed Asia Competition  新加坡千禧國際體育舞蹈錦標賽亞洲區";
$default_oea["02232"]["CatCode"] = "4";
$default_oea["02232"]["CatName"] = "Aesthetic Development";

$default_oea["02233"]["ItemCode"] = "02233";
$default_oea["02233"]["ItemName"] = "The Small Montmartre of Bitola  歐洲青少年蒙馬特繪畫比賽";
$default_oea["02233"]["CatCode"] = "4";
$default_oea["02233"]["CatName"] = "Aesthetic Development";

$default_oea["02234"]["ItemCode"] = "02234";
$default_oea["02234"]["ItemName"] = "The Tchaikovsky Piano Competition  柴可夫斯基鋼琴大賽";
$default_oea["02234"]["CatCode"] = "4";
$default_oea["02234"]["CatName"] = "Aesthetic Development";

$default_oea["02235"]["ItemCode"] = "02235";
$default_oea["02235"]["ItemName"] = "The United Nations Children's Fund International Environment Children's Painting Competition  聯合國兒童基金會國際環境兒童繪畫比賽";
$default_oea["02235"]["CatCode"] = "4";
$default_oea["02235"]["CatName"] = "Aesthetic Development";

$default_oea["02236"]["ItemCode"] = "02236";
$default_oea["02236"]["ItemName"] = "The Vladimir Horowitz International Competition for Young Pianists  霍洛維茲國際青少年鋼琴大賽";
$default_oea["02236"]["CatCode"] = "4";
$default_oea["02236"]["CatName"] = "Aesthetic Development";

$default_oea["02237"]["ItemCode"] = "02237";
$default_oea["02237"]["ItemName"] = "The World Peace Painting and Calligraphy Exhibition  世界和平書畫展";
$default_oea["02237"]["CatCode"] = "4";
$default_oea["02237"]["CatName"] = "Aesthetic Development";

$default_oea["02238"]["ItemCode"] = "02238";
$default_oea["02238"]["ItemName"] = "The World Taekwondo Festival &  International Taekwondo Korea Open Championship";
$default_oea["02238"]["CatCode"] = "3";
$default_oea["02238"]["CatName"] = "Physical Development";

$default_oea["02239"]["ItemCode"] = "02239";
$default_oea["02239"]["ItemName"] = "The World University Sailing Championships  世界大學生風帆錦標賽";
$default_oea["02239"]["CatCode"] = "3";
$default_oea["02239"]["CatName"] = "Physical Development";

$default_oea["02240"]["ItemCode"] = "02240";
$default_oea["02240"]["ItemName"] = "The Youth Learning Project on Pacific Economic Cooperation  太平洋區域經濟合作青年研習計劃";
$default_oea["02240"]["CatCode"] = "2";
$default_oea["02240"]["CatName"] = "Career-related Experience";

$default_oea["02241"]["ItemCode"] = "02241";
$default_oea["02241"]["ItemName"] = "Think Series Award Documentary Making Competition";
$default_oea["02241"]["CatCode"] = "4";
$default_oea["02241"]["CatName"] = "Aesthetic Development";

$default_oea["02242"]["ItemCode"] = "02242";
$default_oea["02242"]["ItemName"] = "ThinkQuest Internet Challenge  ThinkQuest國際教學網頁設計比賽";
$default_oea["02242"]["CatCode"] = "2";
$default_oea["02242"]["CatName"] = "Career-related Experience";

$default_oea["02243"]["ItemCode"] = "02243";
$default_oea["02243"]["ItemName"] = "Thousands of People Celebrating the Chinese Communist Party Shiliu Tai National Exhibition of Painting and Calligraphy Competition  慶祝中國共產黨十六大全國千人名家書畫邀請展賽";
$default_oea["02243"]["CatCode"] = "4";
$default_oea["02243"]["CatName"] = "Aesthetic Development";

$default_oea["02244"]["ItemCode"] = "02244";
$default_oea["02244"]["ItemName"] = "Tian Yan Cup International Comic Competition天眼盃中國國際少年兒童漫畫大賽";
$default_oea["02244"]["CatCode"] = "4";
$default_oea["02244"]["CatName"] = "Aesthetic Development";

$default_oea["02245"]["ItemCode"] = "02245";
$default_oea["02245"]["ItemName"] = "Tianjin China International Accordion Championship";
$default_oea["02245"]["CatCode"] = "4";
$default_oea["02245"]["CatName"] = "Aesthetic Development";

$default_oea["02246"]["ItemCode"] = "02246";
$default_oea["02246"]["ItemName"] = "Tianjin International Athletics Tournament  天津國際田徑邀請賽";
$default_oea["02246"]["CatCode"] = "3";
$default_oea["02246"]["CatName"] = "Physical Development";

$default_oea["02247"]["ItemCode"] = "02247";
$default_oea["02247"]["ItemName"] = "Tianjin International Children's Culture and Art Festival  天津國際少年兒童文化藝術節";
$default_oea["02247"]["CatCode"] = "4";
$default_oea["02247"]["CatName"] = "Aesthetic Development";

$default_oea["02248"]["ItemCode"] = "02248";
$default_oea["02248"]["ItemName"] = "Tiger Bowls World Invitations  老虎盃草地滾球世界邀請賽";
$default_oea["02248"]["CatCode"] = "3";
$default_oea["02248"]["CatName"] = "Physical Development";

$default_oea["02249"]["ItemCode"] = "02249";
$default_oea["02249"]["ItemName"] = "Tip Top Australian Age Championships  Tip Top澳洲分齡錦標賽";
$default_oea["02249"]["CatCode"] = "3";
$default_oea["02249"]["CatName"] = "Physical Development";

$default_oea["02252"]["ItemCode"] = "02252";
$default_oea["02252"]["ItemName"] = "TOYAMA Asian Youth Music Competition - Asian Championship  TOYAMA亞洲青少年音樂比賽";
$default_oea["02252"]["CatCode"] = "4";
$default_oea["02252"]["CatName"] = "Aesthetic Development";

$default_oea["02253"]["ItemCode"] = "02253";
$default_oea["02253"]["ItemName"] = "TOYAMA Asian Youth Music Competition - Hong Kong Championship  TOYAMA亞洲青少年音樂比賽 (香港賽區)";
$default_oea["02253"]["CatCode"] = "4";
$default_oea["02253"]["CatName"] = "Aesthetic Development";

$default_oea["02254"]["ItemCode"] = "02254";
$default_oea["02254"]["ItemName"] = "Tree Planting (Please provide details in \"Description\" box.)";
$default_oea["02254"]["CatCode"] = "5";
$default_oea["02254"]["CatName"] = "Moral and Civic Education";

$default_oea["02255"]["ItemCode"] = "02255";
$default_oea["02255"]["ItemName"] = "Trends of Industrial and Commercial Development in Hong Kong香港工商業發展大趨勢";
$default_oea["02255"]["CatCode"] = "2";
$default_oea["02255"]["CatName"] = "Career-related Experience";

$default_oea["02256"]["ItemCode"] = "02256";
$default_oea["02256"]["ItemName"] = "Trust House";
$default_oea["02256"]["CatCode"] = "1";
$default_oea["02256"]["CatName"] = "Community Service";

$default_oea["02258"]["ItemCode"] = "02258";
$default_oea["02258"]["ItemName"] = "Tsuen Wan District Summer Youth Programm荃灣區青少年暑期活動義工服務獎";
$default_oea["02258"]["CatCode"] = "1";
$default_oea["02258"]["CatName"] = "Community Service";

$default_oea["02259"]["ItemCode"] = "02259";
$default_oea["02259"]["ItemName"] = "Tsuen Wan District Winter Precaution Campaign荃灣區千人冬防滅罪運動";
$default_oea["02259"]["CatCode"] = "1";
$default_oea["02259"]["CatName"] = "Community Service";

$default_oea["02260"]["ItemCode"] = "02260";
$default_oea["02260"]["ItemName"] = "Tug of War Competition 拔河邀請賽";
$default_oea["02260"]["CatCode"] = "3";
$default_oea["02260"]["CatName"] = "Physical Development";

$default_oea["02261"]["ItemCode"] = "02261";
$default_oea["02261"]["ItemName"] = "TV Commercial Creation competition";
$default_oea["02261"]["CatCode"] = "2";
$default_oea["02261"]["CatName"] = "Career-related Experience";

$default_oea["02262"]["ItemCode"] = "02262";
$default_oea["02262"]["ItemName"] = "TVB兒童節兒童繪畫比賽";
$default_oea["02262"]["CatCode"] = "4";
$default_oea["02262"]["CatName"] = "Aesthetic Development";

$default_oea["02263"]["ItemCode"] = "02263";
$default_oea["02263"]["ItemName"] = "Two sides Four children in China Youth Fine Arts Competition  兩岸四地中國青少年兒童書畫大賽";
$default_oea["02263"]["CatCode"] = "4";
$default_oea["02263"]["CatName"] = "Aesthetic Development";

$default_oea["02264"]["ItemCode"] = "02264";
$default_oea["02264"]["ItemName"] = "Tygerberg International Eisteddfod";
$default_oea["02264"]["CatCode"] = "4";
$default_oea["02264"]["CatName"] = "Aesthetic Development";

$default_oea["02265"]["ItemCode"] = "02265";
$default_oea["02265"]["ItemName"] = "U16 World Chess Olympiad";
$default_oea["02265"]["CatCode"] = "4";
$default_oea["02265"]["CatName"] = "Aesthetic Development";

$default_oea["02266"]["ItemCode"] = "02266";
$default_oea["02266"]["ItemName"] = "UIA Architecture and Water International Ideas Competition  UIA建築及自來水國際意念比賽";
$default_oea["02266"]["CatCode"] = "4";
$default_oea["02266"]["CatName"] = "Aesthetic Development";

$default_oea["02267"]["ItemCode"] = "02267";
$default_oea["02267"]["ItemName"] = "UIAA World Youth Championship";
$default_oea["02267"]["CatCode"] = "3";
$default_oea["02267"]["CatName"] = "Physical Development";

$default_oea["02268"]["ItemCode"] = "02268";
$default_oea["02268"]["ItemName"] = "UK Intermediate Mathematical Challenge";
$default_oea["02268"]["CatCode"] = "2";
$default_oea["02268"]["CatName"] = "Career-related Experience";

$default_oea["02273"]["ItemCode"] = "02273";
$default_oea["02273"]["ItemName"] = "Under 21 Women's Asian Hockey Federation Cup  女子二十一歲以下曲棍球聯邦杯";
$default_oea["02273"]["CatCode"] = "3";
$default_oea["02273"]["CatName"] = "Physical Development";

$default_oea["02274"]["ItemCode"] = "02274";
$default_oea["02274"]["ItemName"] = "Under 25 Lawn Bowls Championship";
$default_oea["02274"]["CatCode"] = "3";
$default_oea["02274"]["CatName"] = "Physical Development";

$default_oea["02275"]["ItemCode"] = "02275";
$default_oea["02275"]["ItemName"] = "Understanding Financial Service-Taster Program 認識金融服務導引課程";
$default_oea["02275"]["CatCode"] = "2";
$default_oea["02275"]["CatName"] = "Career-related Experience";

$default_oea["02281"]["ItemCode"] = "02281";
$default_oea["02281"]["ItemName"] = "United Nations Academic Arts Stream International Youth's Painting Competition  聯合國教科文組國際青少年繪畫比賽";
$default_oea["02281"]["CatCode"] = "4";
$default_oea["02281"]["CatName"] = "Aesthetic Development";

$default_oea["02282"]["ItemCode"] = "02282";
$default_oea["02282"]["ItemName"] = "United Nations Environmental Planning Department World Environment Day Asian Region Art Competition  聯合國環境規劃署世界環境日亞太區美術比賽";
$default_oea["02282"]["CatCode"] = "4";
$default_oea["02282"]["CatName"] = "Aesthetic Development";

$default_oea["02283"]["ItemCode"] = "02283";
$default_oea["02283"]["ItemName"] = "United Nations International Youth's Poster Design Competition  聯合國國際青少年海報設計比賽";
$default_oea["02283"]["CatCode"] = "4";
$default_oea["02283"]["CatName"] = "Aesthetic Development";

$default_oea["02284"]["ItemCode"] = "02284";
$default_oea["02284"]["ItemName"] = "United States Open Taekwondo Championship";
$default_oea["02284"]["CatCode"] = "3";
$default_oea["02284"]["CatName"] = "Physical Development";

$default_oea["02269"]["ItemCode"] = "02269";
$default_oea["02269"]["ItemName"] = "UN International Children's Painting Competition  聯合國國際兒童環境繪畫比賽";
$default_oea["02269"]["CatCode"] = "4";
$default_oea["02269"]["CatName"] = "Aesthetic Development";

$default_oea["02270"]["ItemCode"] = "02270";
$default_oea["02270"]["ItemName"] = "UN International Year Art Competition  國際美術設計創作比賽";
$default_oea["02270"]["CatCode"] = "4";
$default_oea["02270"]["CatName"] = "Aesthetic Development";

$default_oea["02271"]["ItemCode"] = "02271";
$default_oea["02271"]["ItemName"] = "UN Peace Day Ceremony & Gandhi Peace Concert";
$default_oea["02271"]["CatCode"] = "4";
$default_oea["02271"]["CatName"] = "Aesthetic Development";

$default_oea["02272"]["ItemCode"] = "02272";
$default_oea["02272"]["ItemName"] = "UNCRC Child Ambassadors' Scheme  聯合國兒童權利公約兒童大使計劃";
$default_oea["02272"]["CatCode"] = "5";
$default_oea["02272"]["CatName"] = "Moral and Civic Education";

$default_oea["02276"]["ItemCode"] = "02276";
$default_oea["02276"]["ItemName"] = "UNDESD 香港獎勵計劃";
$default_oea["02276"]["CatCode"] = "4";
$default_oea["02276"]["CatName"] = "Aesthetic Development";

$default_oea["02277"]["ItemCode"] = "02277";
$default_oea["02277"]["ItemName"] = "UNESCO Children's Performing Arts Festival in East Asia  聯合國教科文組織東亞兒童藝術節";
$default_oea["02277"]["CatCode"] = "4";
$default_oea["02277"]["CatName"] = "Aesthetic Development";

$default_oea["02278"]["ItemCode"] = "02278";
$default_oea["02278"]["ItemName"] = "UNESCOWREN Architectural Design Awards for Practitioners and Idea Competition for Students/Young Designers Low Energy Urban Housing  UNESCOWREN青年學生設計師比賽建築設計大獎";
$default_oea["02278"]["CatCode"] = "4";
$default_oea["02278"]["CatName"] = "Aesthetic Development";

$default_oea["02279"]["ItemCode"] = "02279";
$default_oea["02279"]["ItemName"] = "UNICEF Young Envoys Programme - Pro. TECT Workshop";
$default_oea["02279"]["CatCode"] = "1";
$default_oea["02279"]["CatName"] = "Community Service";

$default_oea["02280"]["ItemCode"] = "02280";
$default_oea["02280"]["ItemName"] = "UNICEP Young EnvoysProgramme聯合國青年大使計劃";
$default_oea["02280"]["CatCode"] = "1";
$default_oea["02280"]["CatName"] = "Community Service";

$default_oea["02285"]["ItemCode"] = "02285";
$default_oea["02285"]["ItemName"] = "URA Comic Drawing Competition筆下樓情四格漫畫創作比賽";
$default_oea["02285"]["CatCode"] = "4";
$default_oea["02285"]["CatName"] = "Aesthetic Development";

$default_oea["02289"]["ItemCode"] = "02289";
$default_oea["02289"]["ItemName"] = "V-China視像中國";
$default_oea["02289"]["CatCode"] = "4";
$default_oea["02289"]["CatName"] = "Aesthetic Development";

$default_oea["02286"]["ItemCode"] = "02286";
$default_oea["02286"]["ItemName"] = "Valentine's Jewellery Design";
$default_oea["02286"]["CatCode"] = "4";
$default_oea["02286"]["CatName"] = "Aesthetic Development";

$default_oea["02287"]["ItemCode"] = "02287";
$default_oea["02287"]["ItemName"] = "Vancouver International Half Marathon  溫哥華國際半馬拉松";
$default_oea["02287"]["CatCode"] = "3";
$default_oea["02287"]["CatName"] = "Physical Development";

$default_oea["02288"]["ItemCode"] = "02288";
$default_oea["02288"]["ItemName"] = "VChina Story Competition (VChina Project, QEF, HKSAR and CUHK)  滬港故事創作大賽";
$default_oea["02288"]["CatCode"] = "4";
$default_oea["02288"]["CatName"] = "Aesthetic Development";

$default_oea["02290"]["ItemCode"] = "02290";
$default_oea["02290"]["ItemName"] = "Vehicle Licences Static Cling Sticker Design荃灣區議會運輸委員會交通安全教育推廣小組-汽車行車證靜電貼設計比賽";
$default_oea["02290"]["CatCode"] = "4";
$default_oea["02290"]["CatName"] = "Aesthetic Development";

$default_oea["02291"]["ItemCode"] = "02291";
$default_oea["02291"]["ItemName"] = "Venture Scout";
$default_oea["02291"]["CatCode"] = "1";
$default_oea["02291"]["CatName"] = "Community Service";

$default_oea["02292"]["ItemCode"] = "02292";
$default_oea["02292"]["ItemName"] = "Venture Scout Award  資深童軍獎章";
$default_oea["02292"]["CatCode"] = "1";
$default_oea["02292"]["CatName"] = "Community Service";

$default_oea["02293"]["ItemCode"] = "02293";
$default_oea["02293"]["ItemName"] = "VES Slogan Competition";
$default_oea["02293"]["CatCode"] = "4";
$default_oea["02293"]["CatName"] = "Aesthetic Development";

$default_oea["02294"]["ItemCode"] = "02294";
$default_oea["02294"]["ItemName"] = "VEX Asian & Pacific Championship  亞太地區VEX機器人工程挑戰賽";
$default_oea["02294"]["CatCode"] = "2";
$default_oea["02294"]["CatName"] = "Career-related Experience";

$default_oea["02295"]["ItemCode"] = "02295";
$default_oea["02295"]["ItemName"] = "VEX Robotics World Championship  世界機械人工程賽";
$default_oea["02295"]["CatCode"] = "2";
$default_oea["02295"]["CatName"] = "Career-related Experience";

$default_oea["02296"]["ItemCode"] = "02296";
$default_oea["02296"]["ItemName"] = "Vietnam Golden Racket Table Tennis International Tournament  越南金球拍乒乓球國際邀請賽";
$default_oea["02296"]["CatCode"] = "3";
$default_oea["02296"]["CatName"] = "Physical Development";

$default_oea["02297"]["ItemCode"] = "02297";
$default_oea["02297"]["ItemName"] = "Visual Arts Annual Show 2010";
$default_oea["02297"]["CatCode"] = "4";
$default_oea["02297"]["CatName"] = "Aesthetic Development";

$default_oea["02298"]["ItemCode"] = "02298";
$default_oea["02298"]["ItemName"] = "Visual Arts Exhibition at Hong Kong Cultural Centre";
$default_oea["02298"]["CatCode"] = "4";
$default_oea["02298"]["CatName"] = "Aesthetic Development";

$default_oea["02299"]["ItemCode"] = "02299";
$default_oea["02299"]["ItemName"] = "Visual Arts Tour 視界新色（導賞團）";
$default_oea["02299"]["CatCode"] = "4";
$default_oea["02299"]["CatName"] = "Aesthetic Development";

$default_oea["02300"]["ItemCode"] = "02300";
$default_oea["02300"]["ItemName"] = "Vivace International Choir Festival  Vivace國際合唱節";
$default_oea["02300"]["CatCode"] = "4";
$default_oea["02300"]["CatName"] = "Aesthetic Development";

$default_oea["02301"]["ItemCode"] = "02301";
$default_oea["02301"]["ItemName"] = "Vivocity (Singapore) International Student Design Competition  Vivocity (新加坡)國際學生設計大賽";
$default_oea["02301"]["CatCode"] = "4";
$default_oea["02301"]["CatName"] = "Aesthetic Development";

$default_oea["02303"]["ItemCode"] = "02303";
$default_oea["02303"]["ItemName"] = "Volleyball Tour cum Macau Pui Ching Middle School Macau Friendly  排球隊澳門交流暨澳門培正中學排球隊友誼賽";
$default_oea["02303"]["CatCode"] = "3";
$default_oea["02303"]["CatName"] = "Physical Development";

$default_oea["02304"]["ItemCode"] = "02304";
$default_oea["02304"]["ItemName"] = "Voluntary Services義工服務 (Please provide details in \"Description\" box.)";
$default_oea["02304"]["CatCode"] = "1";
$default_oea["02304"]["CatName"] = "Community Service";

$default_oea["02302"]["ItemCode"] = "02302";
$default_oea["02302"]["ItemName"] = "VOCATIONAL GRADED EXAMINATION IN DANCE";
$default_oea["02302"]["CatCode"] = "4";
$default_oea["02302"]["CatName"] = "Aesthetic Development";

$default_oea["02306"]["ItemCode"] = "02306";
$default_oea["02306"]["ItemName"] = "Walk-up Jardine House";
$default_oea["02306"]["CatCode"] = "3";
$default_oea["02306"]["CatName"] = "Physical Development";

$default_oea["02305"]["ItemCode"] = "02305";
$default_oea["02305"]["ItemName"] = "Walkathon";
$default_oea["02305"]["CatCode"] = "3";
$default_oea["02305"]["CatName"] = "Physical Development";

$default_oea["02307"]["ItemCode"] = "02307";
$default_oea["02307"]["ItemName"] = "Warwick Shire Australian Individual Championships (Australia)  沃裡克澳洲個人錦標賽";
$default_oea["02307"]["CatCode"] = "3";
$default_oea["02307"]["CatName"] = "Physical Development";

$default_oea["02309"]["ItemCode"] = "02309";
$default_oea["02309"]["ItemName"] = "Waste Management Project for Promoting Green Leaders屯門區廢物管理綠色領袖計劃";
$default_oea["02309"]["CatCode"] = "2";
$default_oea["02309"]["CatName"] = "Career-related Experience";

$default_oea["02310"]["ItemCode"] = "02310";
$default_oea["02310"]["ItemName"] = "Water Safety Poster Design Competition 水上安全海報設計比賽";
$default_oea["02310"]["CatCode"] = "3";
$default_oea["02310"]["CatName"] = "Physical Development";

$default_oea["02311"]["ItemCode"] = "02311";
$default_oea["02311"]["ItemName"] = "Watermate Swim League Age Group Swimming Championships";
$default_oea["02311"]["CatCode"] = "3";
$default_oea["02311"]["CatName"] = "Physical Development";

$default_oea["02312"]["ItemCode"] = "02312";
$default_oea["02312"]["ItemName"] = "Watson Group Hong Kong Student Sports Awards  屈臣氏集團香港學生運動員獎";
$default_oea["02312"]["CatCode"] = "3";
$default_oea["02312"]["CatName"] = "Physical Development";

$default_oea["02313"]["ItemCode"] = "02313";
$default_oea["02313"]["ItemName"] = "Watson's International Athletics Challenge";
$default_oea["02313"]["CatCode"] = "3";
$default_oea["02313"]["CatName"] = "Physical Development";

$default_oea["02308"]["ItemCode"] = "02308";
$default_oea["02308"]["ItemName"] = "WASBE International Youth Wind Orchestra";
$default_oea["02308"]["CatCode"] = "4";
$default_oea["02308"]["CatName"] = "Aesthetic Development";

$default_oea["02314"]["ItemCode"] = "02314";
$default_oea["02314"]["ItemName"] = "Web Development Invitational of the Imagine Cup Competition";
$default_oea["02314"]["CatCode"] = "2";
$default_oea["02314"]["CatName"] = "Career-related Experience";

$default_oea["02315"]["ItemCode"] = "02315";
$default_oea["02315"]["ItemName"] = "Wellesley College Book Award Program";
$default_oea["02315"]["CatCode"] = "4";
$default_oea["02315"]["CatName"] = "Aesthetic Development";

$default_oea["02316"]["ItemCode"] = "02316";
$default_oea["02316"]["ItemName"] = "Welsh Junior Open  威爾斯青少年壁球公開賽";
$default_oea["02316"]["CatCode"] = "3";
$default_oea["02316"]["CatName"] = "Physical Development";

$default_oea["02317"]["ItemCode"] = "02317";
$default_oea["02317"]["ItemName"] = "West Kowloon District Drill Competition - Hong Kong Road Safety Patrol";
$default_oea["02317"]["CatCode"] = "1";
$default_oea["02317"]["CatName"] = "Community Service";

$default_oea["02318"]["ItemCode"] = "02318";
$default_oea["02318"]["ItemName"] = "West Kowloon District Outstanding Patrol Member Election - Hong Kong Road Safety Patrol";
$default_oea["02318"]["CatCode"] = "1";
$default_oea["02318"]["CatName"] = "Community Service";

$default_oea["02319"]["ItemCode"] = "02319";
$default_oea["02319"]["ItemName"] = "West Kowloon District Road Safety Quiz - Hong Kong Road Safety Patrol";
$default_oea["02319"]["CatCode"] = "1";
$default_oea["02319"]["CatName"] = "Community Service";

$default_oea["02320"]["ItemCode"] = "02320";
$default_oea["02320"]["ItemName"] = "West Rail - Tongue Twister Competition西鐵-瞬間即達急口令";
$default_oea["02320"]["CatCode"] = "4";
$default_oea["02320"]["CatName"] = "Aesthetic Development";

$default_oea["02321"]["ItemCode"] = "02321";
$default_oea["02321"]["ItemName"] = "Western China Violin Competition";
$default_oea["02321"]["CatCode"] = "4";
$default_oea["02321"]["CatName"] = "Aesthetic Development";

$default_oea["02322"]["ItemCode"] = "02322";
$default_oea["02322"]["ItemName"] = "Wheelchair Fencing World Cup  世界盃輪椅劍擊賽";
$default_oea["02322"]["CatCode"] = "3";
$default_oea["02322"]["CatName"] = "Physical Development";

$default_oea["02323"]["ItemCode"] = "02323";
$default_oea["02323"]["ItemName"] = "Whole China Youth Science Competition";
$default_oea["02323"]["CatCode"] = "2";
$default_oea["02323"]["CatName"] = "Career-related Experience";

$default_oea["02324"]["ItemCode"] = "02324";
$default_oea["02324"]["ItemName"] = "Widening of Tolo Highway Painting on Hoardings Campaign";
$default_oea["02324"]["CatCode"] = "4";
$default_oea["02324"]["CatName"] = "Aesthetic Development";

$default_oea["02325"]["ItemCode"] = "02325";
$default_oea["02325"]["ItemName"] = "Wikimania Scholarship (Taiwan Chunghwa Telecom)";
$default_oea["02325"]["CatCode"] = "5";
$default_oea["02325"]["CatName"] = "Moral and Civic Education";

$default_oea["02326"]["ItemCode"] = "02326";
$default_oea["02326"]["ItemName"] = "Wisconsin High School Mock Trial";
$default_oea["02326"]["CatCode"] = "5";
$default_oea["02326"]["CatName"] = "Moral and Civic Education";

$default_oea["02327"]["ItemCode"] = "02327";
$default_oea["02327"]["ItemName"] = "Witman Publisher - Workplace Communication Writing Competition 商業寫作比賽 (Please provide details in \"Description\" box.)";
$default_oea["02327"]["CatCode"] = "4";
$default_oea["02327"]["CatName"] = "Aesthetic Development";

$default_oea["02328"]["ItemCode"] = "02328";
$default_oea["02328"]["ItemName"] = "Wofoo Hong Kong and Macau Chinese Typing Competition";
$default_oea["02328"]["CatCode"] = "2";
$default_oea["02328"]["CatName"] = "Career-related Experience";

$default_oea["02329"]["ItemCode"] = "02329";
$default_oea["02329"]["ItemName"] = "Wofoo Millennium Entrepreneurship Prgoramme  和富千禧企業家精神計劃";
$default_oea["02329"]["CatCode"] = "2";
$default_oea["02329"]["CatName"] = "Career-related Experience";

$default_oea["02330"]["ItemCode"] = "02330";
$default_oea["02330"]["ItemName"] = "Women's National Rugby Championships";
$default_oea["02330"]["CatCode"] = "3";
$default_oea["02330"]["CatName"] = "Physical Development";

$default_oea["02331"]["ItemCode"] = "02331";
$default_oea["02331"]["ItemName"] = "Women's Rugby World Cup";
$default_oea["02331"]["CatCode"] = "3";
$default_oea["02331"]["CatName"] = "Physical Development";

$default_oea["02332"]["ItemCode"] = "02332";
$default_oea["02332"]["ItemName"] = "Women's Sabre World Cup  女子佩劍世界杯";
$default_oea["02332"]["CatCode"] = "3";
$default_oea["02332"]["CatName"] = "Physical Development";

$default_oea["02333"]["ItemCode"] = "02333";
$default_oea["02333"]["ItemName"] = "Women's World Series (baseball)";
$default_oea["02333"]["CatCode"] = "3";
$default_oea["02333"]["CatName"] = "Physical Development";

$default_oea["02334"]["ItemCode"] = "02334";
$default_oea["02334"]["ItemName"] = "Women's Youth World Championship  世界女子青年手球錦標賽";
$default_oea["02334"]["CatCode"] = "3";
$default_oea["02334"]["CatName"] = "Physical Development";

$default_oea["02335"]["ItemCode"] = "02335";
$default_oea["02335"]["ItemName"] = "World Cadet and Junior Karate Championship";
$default_oea["02335"]["CatCode"] = "3";
$default_oea["02335"]["CatName"] = "Physical Development";

$default_oea["02336"]["ItemCode"] = "02336";
$default_oea["02336"]["ItemName"] = "World Championship of Triathlon";
$default_oea["02336"]["CatCode"] = "3";
$default_oea["02336"]["CatName"] = "Physical Development";

$default_oea["02337"]["ItemCode"] = "02337";
$default_oea["02337"]["ItemName"] = "World Championships of Marching Show Bands";
$default_oea["02337"]["CatCode"] = "4";
$default_oea["02337"]["CatName"] = "Aesthetic Development";

$default_oea["02338"]["ItemCode"] = "02338";
$default_oea["02338"]["ItemName"] = "World Children's Art Competition The Olympic Games";
$default_oea["02338"]["CatCode"] = "4";
$default_oea["02338"]["CatName"] = "Aesthetic Development";

$default_oea["02339"]["ItemCode"] = "02339";
$default_oea["02339"]["ItemName"] = "World Children's Art Exhibition";
$default_oea["02339"]["CatCode"] = "4";
$default_oea["02339"]["CatName"] = "Aesthetic Development";

$default_oea["02340"]["ItemCode"] = "02340";
$default_oea["02340"]["ItemName"] = "World Children's Day at McDonald's - Love for Our Future Drawing Competition  麥當勞世界兒童日 - 童心寄世界繪畫比賽";
$default_oea["02340"]["CatCode"] = "4";
$default_oea["02340"]["CatName"] = "Aesthetic Development";

$default_oea["02341"]["ItemCode"] = "02341";
$default_oea["02341"]["ItemName"] = "World Choir Games  世界合唱比賽";
$default_oea["02341"]["CatCode"] = "4";
$default_oea["02341"]["CatName"] = "Aesthetic Development";

$default_oea["02343"]["ItemCode"] = "02343";
$default_oea["02343"]["ItemName"] = "World Class Arena Elite Competition  世界數學測試精英邀請賽 (Please provide details in \"Description\" box.)";
$default_oea["02343"]["CatCode"] = "2";
$default_oea["02343"]["CatName"] = "Career-related Experience";

$default_oea["02344"]["ItemCode"] = "02344";
$default_oea["02344"]["ItemName"] = "World Council of YMCAs - 2010";
$default_oea["02344"]["CatCode"] = "1";
$default_oea["02344"]["CatName"] = "Community Service";

$default_oea["02345"]["ItemCode"] = "02345";
$default_oea["02345"]["ItemName"] = "World Cup International Garda Wheelchair Fencing Tournament  意大利輪椅劍擊錦標賽";
$default_oea["02345"]["CatCode"] = "3";
$default_oea["02345"]["CatName"] = "Physical Development";

$default_oea["02346"]["ItemCode"] = "02346";
$default_oea["02346"]["ItemName"] = "World Cup Taekwondo ITF  ITF跆拳道世界杯";
$default_oea["02346"]["CatCode"] = "3";
$default_oea["02346"]["CatName"] = "Physical Development";

$default_oea["02347"]["ItemCode"] = "02347";
$default_oea["02347"]["ItemName"] = "World Halloween Project Competition - The Commercial Press (Hong Kong)";
$default_oea["02347"]["CatCode"] = "2";
$default_oea["02347"]["CatName"] = "Career-related Experience";

$default_oea["02348"]["ItemCode"] = "02348";
$default_oea["02348"]["ItemName"] = "World Harmonica Festival  世界口琴節";
$default_oea["02348"]["CatCode"] = "4";
$default_oea["02348"]["CatName"] = "Aesthetic Development";

$default_oea["02349"]["ItemCode"] = "02349";
$default_oea["02349"]["ItemName"] = "World Heart Day Inter-Regional Drawing Competition  世界心臟日繪畫比賽";
$default_oea["02349"]["CatCode"] = "4";
$default_oea["02349"]["CatName"] = "Aesthetic Development";

$default_oea["02350"]["ItemCode"] = "02350";
$default_oea["02350"]["ItemName"] = "World Hong Kong Luminous Dragon Dance and Lion Dance Championships  世界香港夜光龍醒獅錦標賽";
$default_oea["02350"]["CatCode"] = "3";
$default_oea["02350"]["CatName"] = "Physical Development";

$default_oea["02351"]["ItemCode"] = "02351";
$default_oea["02351"]["ItemName"] = "World Indoor Cycling Championships  世界室內單車錦標賽";
$default_oea["02351"]["CatCode"] = "3";
$default_oea["02351"]["CatName"] = "Physical Development";

$default_oea["02352"]["ItemCode"] = "02352";
$default_oea["02352"]["ItemName"] = "World Junior and Cadet Fencing Championships";
$default_oea["02352"]["CatCode"] = "3";
$default_oea["02352"]["CatName"] = "Physical Development";

$default_oea["02353"]["ItemCode"] = "02353";
$default_oea["02353"]["ItemName"] = "World Junior Kurash Championship";
$default_oea["02353"]["CatCode"] = "3";
$default_oea["02353"]["CatName"] = "Physical Development";

$default_oea["02354"]["ItemCode"] = "02354";
$default_oea["02354"]["ItemName"] = "World Junior Squash Championship  世界青年壁球錦標賽";
$default_oea["02354"]["CatCode"] = "3";
$default_oea["02354"]["CatName"] = "Physical Development";

$default_oea["02355"]["ItemCode"] = "02355";
$default_oea["02355"]["ItemName"] = "World Junior Team Championship";
$default_oea["02355"]["CatCode"] = "3";
$default_oea["02355"]["CatName"] = "Physical Development";

$default_oea["02356"]["ItemCode"] = "02356";
$default_oea["02356"]["ItemName"] = "World Junior Tennis Competition  世界少年網球錦標賽";
$default_oea["02356"]["CatCode"] = "3";
$default_oea["02356"]["CatName"] = "Physical Development";

$default_oea["02357"]["ItemCode"] = "02357";
$default_oea["02357"]["ItemName"] = "World Junior Women's Squash Team Championships  世界女子青少年壁球團體賽";
$default_oea["02357"]["CatCode"] = "3";
$default_oea["02357"]["CatName"] = "Physical Development";

$default_oea["02358"]["ItemCode"] = "02358";
$default_oea["02358"]["ItemName"] = "World Junior Wushu Championships  世界青少年武術錦標賽";
$default_oea["02358"]["CatCode"] = "3";
$default_oea["02358"]["CatName"] = "Physical Development";

$default_oea["02359"]["ItemCode"] = "02359";
$default_oea["02359"]["ItemName"] = "World Juniors Badminton Championships";
$default_oea["02359"]["CatCode"] = "3";
$default_oea["02359"]["CatName"] = "Physical Development";

$default_oea["02360"]["ItemCode"] = "02360";
$default_oea["02360"]["ItemName"] = "World Kung-fu Gala  全世界功夫群英會";
$default_oea["02360"]["CatCode"] = "3";
$default_oea["02360"]["CatName"] = "Physical Development";

$default_oea["02361"]["ItemCode"] = "02361";
$default_oea["02361"]["ItemName"] = "World Leading Schools Football Championship  世界名中學足球友誼邀請賽";
$default_oea["02361"]["CatCode"] = "3";
$default_oea["02361"]["CatName"] = "Physical Development";

$default_oea["02362"]["ItemCode"] = "02362";
$default_oea["02362"]["ItemName"] = "World Lifesaving Championships";
$default_oea["02362"]["CatCode"] = "3";
$default_oea["02362"]["CatName"] = "Physical Development";

$default_oea["02363"]["ItemCode"] = "02363";
$default_oea["02363"]["ItemName"] = "World Memory Championship";
$default_oea["02363"]["CatCode"] = "2";
$default_oea["02363"]["CatName"] = "Career-related Experience";

$default_oea["02364"]["ItemCode"] = "02364";
$default_oea["02364"]["ItemName"] = "World Mind Sports Games  世界智力運動會";
$default_oea["02364"]["CatCode"] = "4";
$default_oea["02364"]["CatName"] = "Aesthetic Development";

$default_oea["02365"]["ItemCode"] = "02365";
$default_oea["02365"]["ItemName"] = "World Music Contest";
$default_oea["02365"]["CatCode"] = "4";
$default_oea["02365"]["CatName"] = "Aesthetic Development";

$default_oea["02366"]["ItemCode"] = "02366";
$default_oea["02366"]["ItemName"] = "World Music Fiesta Competition (Please provide details in \"Description\" box.)";
$default_oea["02366"]["CatCode"] = "4";
$default_oea["02366"]["CatName"] = "Aesthetic Development";

$default_oea["02367"]["ItemCode"] = "02367";
$default_oea["02367"]["ItemName"] = "World Music Fiesta Powerpoint Presentation Competition";
$default_oea["02367"]["CatCode"] = "4";
$default_oea["02367"]["CatName"] = "Aesthetic Development";

$default_oea["02368"]["ItemCode"] = "02368";
$default_oea["02368"]["ItemName"] = "World Outdoor Archery Championships";
$default_oea["02368"]["CatCode"] = "3";
$default_oea["02368"]["CatName"] = "Physical Development";

$default_oea["02369"]["ItemCode"] = "02369";
$default_oea["02369"]["ItemName"] = "World Painting and Calligraphy Contest for Overseas Chinese Youth and Children  世界華人少年兒童書畫大賽";
$default_oea["02369"]["CatCode"] = "4";
$default_oea["02369"]["CatName"] = "Aesthetic Development";

$default_oea["02370"]["ItemCode"] = "02370";
$default_oea["02370"]["ItemName"] = "World Peace Painting Competition  世界和平繪畫比賽";
$default_oea["02370"]["CatCode"] = "4";
$default_oea["02370"]["CatName"] = "Aesthetic Development";

$default_oea["02371"]["ItemCode"] = "02371";
$default_oea["02371"]["ItemName"] = "World Robot Olympiad  國際奧林匹克機械人競賽";
$default_oea["02371"]["CatCode"] = "2";
$default_oea["02371"]["CatName"] = "Career-related Experience";

$default_oea["02372"]["ItemCode"] = "02372";
$default_oea["02372"]["ItemName"] = "World Rope Skipping Championships  世界跳繩錦標賽";
$default_oea["02372"]["CatCode"] = "3";
$default_oea["02372"]["CatName"] = "Physical Development";

$default_oea["02373"]["ItemCode"] = "02373";
$default_oea["02373"]["ItemName"] = "World School Children's Art Exhibition China  世界兒童畫展";
$default_oea["02373"]["CatCode"] = "4";
$default_oea["02373"]["CatName"] = "Aesthetic Development";

$default_oea["02374"]["ItemCode"] = "02374";
$default_oea["02374"]["ItemName"] = "World Scout Jamboree";
$default_oea["02374"]["CatCode"] = "1";
$default_oea["02374"]["CatName"] = "Community Service";

$default_oea["02375"]["ItemCode"] = "02375";
$default_oea["02375"]["ItemName"] = "World Shitoryu Karatedo Championships  世界糸東流空手道大賽";
$default_oea["02375"]["CatCode"] = "3";
$default_oea["02375"]["CatName"] = "Physical Development";

$default_oea["02376"]["ItemCode"] = "02376";
$default_oea["02376"]["ItemName"] = "World Short Course Swimming Competition (Hong Kong)  世界短池游泳賽(香港)";
$default_oea["02376"]["CatCode"] = "3";
$default_oea["02376"]["CatName"] = "Physical Development";

$default_oea["02377"]["ItemCode"] = "02377";
$default_oea["02377"]["ItemName"] = "World Shuttlecock Championships  世界毽球錦標賽";
$default_oea["02377"]["CatCode"] = "3";
$default_oea["02377"]["CatName"] = "Physical Development";

$default_oea["02378"]["ItemCode"] = "02378";
$default_oea["02378"]["ItemName"] = "World Speed Skating Championships";
$default_oea["02378"]["CatCode"] = "3";
$default_oea["02378"]["CatName"] = "Physical Development";

$default_oea["02379"]["ItemCode"] = "02379";
$default_oea["02379"]["ItemName"] = "World Summer Taekwondo Festival Yangyang Korea  南韓蘘陽世界夏季跆拳道節";
$default_oea["02379"]["CatCode"] = "3";
$default_oea["02379"]["CatName"] = "Physical Development";

$default_oea["02380"]["ItemCode"] = "02380";
$default_oea["02380"]["ItemName"] = "World Taekwondo Championships  世界跆拳道錦標賽";
$default_oea["02380"]["CatCode"] = "3";
$default_oea["02380"]["CatName"] = "Physical Development";

$default_oea["02381"]["ItemCode"] = "02381";
$default_oea["02381"]["ItemName"] = "World Taekwondo Festival  世界跆拳道文化節";
$default_oea["02381"]["CatCode"] = "3";
$default_oea["02381"]["CatName"] = "Physical Development";

$default_oea["02382"]["ItemCode"] = "02382";
$default_oea["02382"]["ItemName"] = "World Taekwondo Festival & International Taekwondo Taiwan Changhua Open Championship  世界跆拳道文化節暨台灣彰化國際公開賽";
$default_oea["02382"]["CatCode"] = "3";
$default_oea["02382"]["CatName"] = "Physical Development";

$default_oea["02383"]["ItemCode"] = "02383";
$default_oea["02383"]["ItemName"] = "World Taekwondo Festival & Korea Classic Open  世界跆拳道節暨韓國跆拳道錦標賽";
$default_oea["02383"]["CatCode"] = "3";
$default_oea["02383"]["CatName"] = "Physical Development";

$default_oea["02384"]["ItemCode"] = "02384";
$default_oea["02384"]["ItemName"] = "World Telecommunication and Information Society Day - Hong Kong - Geo-caching Hunting Competition  香港世界電訊及資訊社會日 - 無線定位尋寶比賽";
$default_oea["02384"]["CatCode"] = "5";
$default_oea["02384"]["CatName"] = "Moral and Civic Education";

$default_oea["02385"]["ItemCode"] = "02385";
$default_oea["02385"]["ItemName"] = "World Traditional Martial Arts Festival - Traditional Martial Arts Tournament  世界傳統武術節武術競賽";
$default_oea["02385"]["CatCode"] = "3";
$default_oea["02385"]["CatName"] = "Physical Development";

$default_oea["02386"]["ItemCode"] = "02386";
$default_oea["02386"]["ItemName"] = "World Traditional Wushu Championships  世界傳統武術節";
$default_oea["02386"]["CatCode"] = "3";
$default_oea["02386"]["CatName"] = "Physical Development";

$default_oea["02387"]["ItemCode"] = "02387";
$default_oea["02387"]["ItemName"] = "World University Game (Volleyball)  世界大學生運動會(排球)";
$default_oea["02387"]["CatCode"] = "3";
$default_oea["02387"]["CatName"] = "Physical Development";

$default_oea["02388"]["ItemCode"] = "02388";
$default_oea["02388"]["ItemName"] = "World University Squash Championship  世界大學生壁球錦標賽";
$default_oea["02388"]["CatCode"] = "3";
$default_oea["02388"]["CatName"] = "Physical Development";

$default_oea["02389"]["ItemCode"] = "02389";
$default_oea["02389"]["ItemName"] = "World Water Day International Art Contest";
$default_oea["02389"]["CatCode"] = "4";
$default_oea["02389"]["CatName"] = "Aesthetic Development";

$default_oea["02390"]["ItemCode"] = "02390";
$default_oea["02390"]["ItemName"] = "World Wheelchair Fencing Championships  世界輪椅劍擊錦標賽";
$default_oea["02390"]["CatCode"] = "3";
$default_oea["02390"]["CatName"] = "Physical Development";

$default_oea["02391"]["ItemCode"] = "02391";
$default_oea["02391"]["ItemName"] = "World Xiangqi Championship  世界象棋錦標賽";
$default_oea["02391"]["CatCode"] = "4";
$default_oea["02391"]["CatName"] = "Aesthetic Development";

$default_oea["02392"]["ItemCode"] = "02392";
$default_oea["02392"]["ItemName"] = "World Year of Physics A & D Competition: Hong Kong世界物理年美術設計比賽：香港";
$default_oea["02392"]["CatCode"] = "4";
$default_oea["02392"]["CatName"] = "Aesthetic Development";

$default_oea["02393"]["ItemCode"] = "02393";
$default_oea["02393"]["ItemName"] = "World Year of Physics Art and Design Competition (international)  世界物理年美術設計創作比賽(世界賽)";
$default_oea["02393"]["CatCode"] = "4";
$default_oea["02393"]["CatName"] = "Aesthetic Development";

$default_oea["02394"]["ItemCode"] = "02394";
$default_oea["02394"]["ItemName"] = "World Youth and Children's Art Competition  世界青少年及兒童藝術比賽";
$default_oea["02394"]["CatCode"] = "4";
$default_oea["02394"]["CatName"] = "Aesthetic Development";

$default_oea["02395"]["ItemCode"] = "02395";
$default_oea["02395"]["ItemName"] = "World Youth Games  世界青年運動會";
$default_oea["02395"]["CatCode"] = "3";
$default_oea["02395"]["CatName"] = "Physical Development";

$default_oea["02396"]["ItemCode"] = "02396";
$default_oea["02396"]["ItemName"] = "World Youth GOE Championship  世界青年GOE錦標賽";
$default_oea["02396"]["CatCode"] = "3";
$default_oea["02396"]["CatName"] = "Physical Development";

$default_oea["02397"]["ItemCode"] = "02397";
$default_oea["02397"]["ItemName"] = "World Youth Hockey Festival";
$default_oea["02397"]["CatCode"] = "3";
$default_oea["02397"]["CatName"] = "Physical Development";

$default_oea["02398"]["ItemCode"] = "02398";
$default_oea["02398"]["ItemName"] = "World Youth Korfball Championship  世界青年合球錦標賽";
$default_oea["02398"]["CatCode"] = "3";
$default_oea["02398"]["CatName"] = "Physical Development";

$default_oea["02399"]["ItemCode"] = "02399";
$default_oea["02399"]["ItemName"] = "World Youth Team Championships (Bridge Tournament)  世界青年隊制橋藝錦標賽";
$default_oea["02399"]["CatCode"] = "3";
$default_oea["02399"]["CatName"] = "Physical Development";

$default_oea["02400"]["ItemCode"] = "02400";
$default_oea["02400"]["ItemName"] = "World Youth Tenpin Bowling Championships  世界青少年保齡球錦標賽";
$default_oea["02400"]["CatCode"] = "3";
$default_oea["02400"]["CatName"] = "Physical Development";

$default_oea["02401"]["ItemCode"] = "02401";
$default_oea["02401"]["ItemName"] = "World Youth Under 16 Olympiad";
$default_oea["02401"]["CatCode"] = "4";
$default_oea["02401"]["CatName"] = "Aesthetic Development";

$default_oea["02402"]["ItemCode"] = "02402";
$default_oea["02402"]["ItemName"] = "WTF World Junior Taekwondo Championships  世界青少年跆拳道錦標賽";
$default_oea["02402"]["CatCode"] = "3";
$default_oea["02402"]["CatName"] = "Physical Development";

$default_oea["02403"]["ItemCode"] = "02403";
$default_oea["02403"]["ItemName"] = "Wu Chien-Shiung Science Camp  吳健雄科學營";
$default_oea["02403"]["CatCode"] = "2";
$default_oea["02403"]["CatName"] = "Career-related Experience";

$default_oea["02404"]["ItemCode"] = "02404";
$default_oea["02404"]["ItemName"] = "Wu Yi Anglela Leong Cup Junior Golf Championship  五邑球會杯青少年高爾夫球錦標賽";
$default_oea["02404"]["CatCode"] = "3";
$default_oea["02404"]["CatName"] = "Physical Development";

$default_oea["02405"]["ItemCode"] = "02405";
$default_oea["02405"]["ItemName"] = "Xiao He Bei International Children's Calligraphy and Painting Match  小荷杯少年兒童國際書畫大賽";
$default_oea["02405"]["CatCode"] = "4";
$default_oea["02405"]["CatName"] = "Aesthetic Development";

$default_oea["02406"]["ItemCode"] = "02406";
$default_oea["02406"]["ItemName"] = "Xin Gai Nian Writing Competition  全國新概念作文大賽";
$default_oea["02406"]["CatCode"] = "4";
$default_oea["02406"]["CatName"] = "Aesthetic Development";

$default_oea["02407"]["ItemCode"] = "02407";
$default_oea["02407"]["ItemName"] = "Xin Miao Encouragement Award - China Development Foundation for Science and Technology  新苗鼓勵獎 - 中國科技館發展基金";
$default_oea["02407"]["CatCode"] = "2";
$default_oea["02407"]["CatName"] = "Career-related Experience";

$default_oea["02408"]["ItemCode"] = "02408";
$default_oea["02408"]["ItemName"] = "Xiwanbei International Mathematics Contest  希望杯國際數學競賽";
$default_oea["02408"]["CatCode"] = "2";
$default_oea["02408"]["CatName"] = "Career-related Experience";

$default_oea["02409"]["ItemCode"] = "02409";
$default_oea["02409"]["ItemName"] = "Xu Bei Hong Arts Competition for Young Adults & Children  徐悲鴻盃青少年兒童美術比賽 (Please provide details in \"Description\" box.)";
$default_oea["02409"]["CatCode"] = "4";
$default_oea["02409"]["CatName"] = "Aesthetic Development";

$default_oea["02410"]["ItemCode"] = "02410";
$default_oea["02410"]["ItemName"] = "Xu Bei Hong Arts Competition for Young Adults & Children (Hong Kong Region)  徐悲鴻盃青少年兒童美術比賽 (香港賽區)";
$default_oea["02410"]["CatCode"] = "4";
$default_oea["02410"]["CatName"] = "Aesthetic Development";

$default_oea["02412"]["ItemCode"] = "02412";
$default_oea["02412"]["ItemName"] = "Xunda Cup National Design Competition of China Architecture Students  迅達杯全國大學生建築設計競賽";
$default_oea["02412"]["CatCode"] = "4";
$default_oea["02412"]["CatName"] = "Aesthetic Development";

$default_oea["02413"]["ItemCode"] = "02413";
$default_oea["02413"]["ItemName"] = "Y Angle Short Film Competition of Hong Kong Art Centre  講、覺、導Y ANGLE青年錄像比賽";
$default_oea["02413"]["CatCode"] = "4";
$default_oea["02413"]["CatName"] = "Aesthetic Development";

$default_oea["02414"]["ItemCode"] = "02414";
$default_oea["02414"]["ItemName"] = "Yan Oi Tong Judo Competition 仁愛堂柔道大賽";
$default_oea["02414"]["CatCode"] = "3";
$default_oea["02414"]["CatName"] = "Physical Development";

$default_oea["02415"]["ItemCode"] = "02415";
$default_oea["02415"]["ItemName"] = "Yandou Award China Calligraphy and Painting Artists' Works Exhibition  燕都金像獎中國書畫藝術家作品展";
$default_oea["02415"]["CatCode"] = "4";
$default_oea["02415"]["CatName"] = "Aesthetic Development";

$default_oea["02416"]["ItemCode"] = "02416";
$default_oea["02416"]["ItemName"] = "Yangcheng Evening News Handwritten Newspaper Competition (Hong Kong Region)  《羊城晚報》小讀者暑期手抄報創作大賽 (香港賽區)";
$default_oea["02416"]["CatCode"] = "4";
$default_oea["02416"]["CatName"] = "Aesthetic Development";

$default_oea["02417"]["ItemCode"] = "02417";
$default_oea["02417"]["ItemName"] = "Yanhuang Art Museum - Concrete Yan Huang, juvenile spring line Exhibition of Contemporary Chinese Youth, National Youth Calligraphy and Painting  北京炎黃藝術館舉行之《具象炎黃‧春天少年行－中國當代少年畫展》";
$default_oea["02417"]["CatCode"] = "4";
$default_oea["02417"]["CatName"] = "Aesthetic Development";

$default_oea["02418"]["ItemCode"] = "02418";
$default_oea["02418"]["ItemName"] = "Yau Sang Cheong Cup HK Youth Art Competition友生昌盃全港青少年書畫大賽";
$default_oea["02418"]["CatCode"] = "1";
$default_oea["02418"]["CatName"] = "Community Service";

$default_oea["02419"]["ItemCode"] = "02419";
$default_oea["02419"]["ItemName"] = "Yau Tsim Mong District Girl Guides Shield Competition - The Hong Kong Girl Guides Association";
$default_oea["02419"]["CatCode"] = "1";
$default_oea["02419"]["CatName"] = "Community Service";

$default_oea["02420"]["ItemCode"] = "02420";
$default_oea["02420"]["ItemName"] = "YFS Arts Diary Cover design Competition";
$default_oea["02420"]["CatCode"] = "4";
$default_oea["02420"]["CatName"] = "Aesthetic Development";

$default_oea["02421"]["ItemCode"] = "02421";
$default_oea["02421"]["ItemName"] = "YMCA - Charity Cycling 青暉遠眺赤子情單車籌款";
$default_oea["02421"]["CatCode"] = "1";
$default_oea["02421"]["CatName"] = "Community Service";

$default_oea["02422"]["ItemCode"] = "02422";
$default_oea["02422"]["ItemName"] = "YMCA Girl's Volleyball Competition YMCA盃女子排球賽";
$default_oea["02422"]["CatCode"] = "3";
$default_oea["02422"]["CatName"] = "Physical Development";

$default_oea["02423"]["ItemCode"] = "02423";
$default_oea["02423"]["ItemName"] = "Yonex-Sunrise Hong Kong Open Badminton Super Series  YONEX-SUNRISE香港公開羽毛球超級賽";
$default_oea["02423"]["CatCode"] = "3";
$default_oea["02423"]["CatName"] = "Physical Development";

$default_oea["02424"]["ItemCode"] = "02424";
$default_oea["02424"]["ItemName"] = "Young Basketball Player Training Scheme, Youth Training Camp";
$default_oea["02424"]["CatCode"] = "3";
$default_oea["02424"]["CatName"] = "Physical Development";

$default_oea["02425"]["ItemCode"] = "02425";
$default_oea["02425"]["ItemName"] = "Young Entrepreneur Training Scheme: Lavender";
$default_oea["02425"]["CatCode"] = "2";
$default_oea["02425"]["CatName"] = "Career-related Experience";

$default_oea["02426"]["ItemCode"] = "02426";
$default_oea["02426"]["ItemName"] = "Young Folk Music Group Beijing Invitational  青少年民樂團隊北京邀請賽";
$default_oea["02426"]["CatCode"] = "4";
$default_oea["02426"]["CatName"] = "Aesthetic Development";

$default_oea["02427"]["ItemCode"] = "02427";
$default_oea["02427"]["ItemName"] = "Young Post :  Tuesday Cover  Award";
$default_oea["02427"]["CatCode"] = "2";
$default_oea["02427"]["CatName"] = "Career-related Experience";

$default_oea["02428"]["ItemCode"] = "02428";
$default_oea["02428"]["ItemName"] = "Young Samaritans Peer Support Programme Presentation";
$default_oea["02428"]["CatCode"] = "1";
$default_oea["02428"]["CatName"] = "Community Service";

$default_oea["02429"]["ItemCode"] = "02429";
$default_oea["02429"]["ItemName"] = "Youth 5.4 @FTU City Orienteering";
$default_oea["02429"]["CatCode"] = "3";
$default_oea["02429"]["CatName"] = "Physical Development";

$default_oea["02430"]["ItemCode"] = "02430";
$default_oea["02430"]["ItemName"] = "Youth Arts Festival (Young Artist Award)";
$default_oea["02430"]["CatCode"] = "4";
$default_oea["02430"]["CatName"] = "Aesthetic Development";

$default_oea["02431"]["ItemCode"] = "02431";
$default_oea["02431"]["ItemName"] = "Youth Chinese Orchestra (Chinese Cup) Invitation Competition";
$default_oea["02431"]["CatCode"] = "4";
$default_oea["02431"]["CatName"] = "Aesthetic Development";

$default_oea["02432"]["ItemCode"] = "02432";
$default_oea["02432"]["ItemName"] = "Youth Chinese Orchestra Competition";
$default_oea["02432"]["CatCode"] = "4";
$default_oea["02432"]["CatName"] = "Aesthetic Development";

$default_oea["02433"]["ItemCode"] = "02433";
$default_oea["02433"]["ItemName"] = "Youth Cup National Youth Competition Children's Painting  青春杯全國青少年書畫大賽";
$default_oea["02433"]["CatCode"] = "4";
$default_oea["02433"]["CatName"] = "Aesthetic Development";

$default_oea["02434"]["ItemCode"] = "02434";
$default_oea["02434"]["ItemName"] = "Youth Design Contest for Souvenir Cover on Asia's World City  青少年香港:亞洲國際都會紀念封設計比賽";
$default_oea["02434"]["CatCode"] = "4";
$default_oea["02434"]["CatName"] = "Aesthetic Development";

$default_oea["02435"]["ItemCode"] = "02435";
$default_oea["02435"]["ItemName"] = "Youth Docent Training Programme藝術館青年導賞員訓練課程";
$default_oea["02435"]["CatCode"] = "4";
$default_oea["02435"]["CatName"] = "Aesthetic Development";

$default_oea["02436"]["ItemCode"] = "02436";
$default_oea["02436"]["ItemName"] = "Youth Exchange Competition Dongguan, Guangdong";
$default_oea["02436"]["CatCode"] = "3";
$default_oea["02436"]["CatName"] = "Physical Development";

$default_oea["02437"]["ItemCode"] = "02437";
$default_oea["02437"]["ItemName"] = "Youth Learning Project (Hong Kong Federation of Youth Groups)";
$default_oea["02437"]["CatCode"] = "5";
$default_oea["02437"]["CatName"] = "Moral and Civic Education";

$default_oea["02438"]["ItemCode"] = "02438";
$default_oea["02438"]["ItemName"] = "Youth Literary Award  大學文學獎少年作家選拔比賽";
$default_oea["02438"]["CatCode"] = "4";
$default_oea["02438"]["CatName"] = "Aesthetic Development";

$default_oea["02439"]["ItemCode"] = "02439";
$default_oea["02439"]["ItemName"] = "Youth Literary Awards  青年文學獎徵文比賽";
$default_oea["02439"]["CatCode"] = "4";
$default_oea["02439"]["CatName"] = "Aesthetic Development";

$default_oea["02440"]["ItemCode"] = "02440";
$default_oea["02440"]["ItemName"] = "Youth Novice Lawn Bowls Competition";
$default_oea["02440"]["CatCode"] = "3";
$default_oea["02440"]["CatName"] = "Physical Development";

$default_oea["02441"]["ItemCode"] = "02441";
$default_oea["02441"]["ItemName"] = "Youth Rowing Age Group 青少年賽艇分齡賽";
$default_oea["02441"]["CatCode"] = "2";
$default_oea["02441"]["CatName"] = "Career-related Experience";

$default_oea["02442"]["ItemCode"] = "02442";
$default_oea["02442"]["ItemName"] = "Youth Science and Technology Fair and Exhibition";
$default_oea["02442"]["CatCode"] = "2";
$default_oea["02442"]["CatName"] = "Career-related Experience";

$default_oea["02443"]["ItemCode"] = "02443";
$default_oea["02443"]["ItemName"] = "Yuen Long Cup Two Straits Four Region Volleyball Performance Invitational Competition  元朗盃兩岸四地男女子排球表演邀請賽";
$default_oea["02443"]["CatCode"] = "3";
$default_oea["02443"]["CatName"] = "Physical Development";

$default_oea["02444"]["ItemCode"] = "02444";
$default_oea["02444"]["ItemName"] = "Y世代計劃禁毒滅罪中學校際籃球賽";
$default_oea["02444"]["CatCode"] = "3";
$default_oea["02444"]["CatName"] = "Physical Development";

$default_oea["02445"]["ItemCode"] = "02445";
$default_oea["02445"]["ItemName"] = "Zheng Cheng-gong Cup International Men's Basketball Tournament";
$default_oea["02445"]["CatCode"] = "3";
$default_oea["02445"]["CatName"] = "Physical Development";

$default_oea["02446"]["ItemCode"] = "02446";
$default_oea["02446"]["ItemName"] = "Zhonghua Cup Art Competition";
$default_oea["02446"]["CatCode"] = "4";
$default_oea["02446"]["CatName"] = "Aesthetic Development";

$default_oea["02447"]["ItemCode"] = "02447";
$default_oea["02447"]["ItemName"] = "Zhuangzedong Invitational Tournament";
$default_oea["02447"]["CatCode"] = "3";
$default_oea["02447"]["CatName"] = "Physical Development";

$default_oea["02448"]["ItemCode"] = "02448";
$default_oea["02448"]["ItemName"] = "Zhujiang Cup Baseball cum XLV Festival of Sport";
$default_oea["02448"]["CatCode"] = "3";
$default_oea["02448"]["CatName"] = "Physical Development";

$default_oea["02449"]["ItemCode"] = "02449";
$default_oea["02449"]["ItemName"] = "Zonta International - Young Women in Public Affairs Award";
$default_oea["02449"]["CatCode"] = "5";
$default_oea["02449"]["CatName"] = "Moral and Civic Education";

$default_oea["02450"]["ItemCode"] = "02450";
$default_oea["02450"]["ItemName"] = "《七十二家房客》舞台劇";
$default_oea["02450"]["CatCode"] = "4";
$default_oea["02450"]["CatName"] = "Aesthetic Development";

$default_oea["02452"]["ItemCode"] = "02452";
$default_oea["02452"]["ItemName"] = "《友里童行》街展活動";
$default_oea["02452"]["CatCode"] = "4";
$default_oea["02452"]["CatName"] = "Aesthetic Development";

$default_oea["02458"]["ItemCode"] = "02458";
$default_oea["02458"]["ItemName"] = "《基本法》頒布20周年 校際Tee恤設計比賽";
$default_oea["02458"]["CatCode"] = "4";
$default_oea["02458"]["CatName"] = "Aesthetic Development";

$default_oea["02451"]["ItemCode"] = "02451";
$default_oea["02451"]["ItemName"] = "《大圍顯徑街市廣場》聖誕嘉年華會";
$default_oea["02451"]["CatCode"] = "4";
$default_oea["02451"]["CatName"] = "Aesthetic Development";

$default_oea["02457"]["ItemCode"] = "02457";
$default_oea["02457"]["ItemName"] = "《家庭實況劇 一念之間》徵文比賽";
$default_oea["02457"]["CatCode"] = "5";
$default_oea["02457"]["CatName"] = "Moral and Civic Education";

$default_oea["02459"]["ItemCode"] = "02459";
$default_oea["02459"]["ItemName"] = "《愛．行動》- 愛滋病藝術教育推廣計劃";
$default_oea["02459"]["CatCode"] = "1";
$default_oea["02459"]["CatName"] = "Community Service";

$default_oea["02455"]["ItemCode"] = "02455";
$default_oea["02455"]["ItemName"] = "《施政報告》觀塘區師生論壇";
$default_oea["02455"]["CatCode"] = "5";
$default_oea["02455"]["CatName"] = "Moral and Civic Education";

$default_oea["02453"]["ItemCode"] = "02453";
$default_oea["02453"]["ItemName"] = "《百年中國》大型多媒體製作";
$default_oea["02453"]["CatCode"] = "4";
$default_oea["02453"]["CatName"] = "Aesthetic Development";

$default_oea["02454"]["ItemCode"] = "02454";
$default_oea["02454"]["ItemName"] = "《社會與我》攝影比賽";
$default_oea["02454"]["CatCode"] = "4";
$default_oea["02454"]["CatName"] = "Aesthetic Development";

$default_oea["02460"]["ItemCode"] = "02460";
$default_oea["02460"]["ItemName"] = "《藝無疆: 新晉展能藝術家大匯展2010》";
$default_oea["02460"]["CatCode"] = "4";
$default_oea["02460"]["CatName"] = "Aesthetic Development";

$default_oea["02461"]["ItemCode"] = "02461";
$default_oea["02461"]["ItemName"] = "《關懷感動五十載》全港勵志關懷語句大募集";
$default_oea["02461"]["CatCode"] = "4";
$default_oea["02461"]["CatName"] = "Aesthetic Development";

$default_oea["02456"]["ItemCode"] = "02456";
$default_oea["02456"]["ItemName"] = "《香港青年音樂匯演》中學組比賽";
$default_oea["02456"]["CatCode"] = "4";
$default_oea["02456"]["CatName"] = "Aesthetic Development";

$default_oea["02462"]["ItemCode"] = "02462";
$default_oea["02462"]["ItemName"] = "「I Mission」新高中廉潔大使全「誠」計劃 (Please provide details in \"Description\" box.)";
$default_oea["02462"]["CatCode"] = "5";
$default_oea["02462"]["CatName"] = "Moral and Civic Education";

$default_oea["02463"]["ItemCode"] = "02463";
$default_oea["02463"]["ItemName"] = "「I Mission」新高中廉潔大使全「誠」計劃: Card Design Competition (Please provide details in \"Description\" box.)";
$default_oea["02463"]["CatCode"] = "5";
$default_oea["02463"]["CatName"] = "Moral and Civic Education";

$default_oea["02464"]["ItemCode"] = "02464";
$default_oea["02464"]["ItemName"] = "「今年聖誕搞乜樹? 」搞乜樹?全港聖誕樹設計及製作比賽";
$default_oea["02464"]["CatCode"] = "4";
$default_oea["02464"]["CatName"] = "Aesthetic Development";

$default_oea["02469"]["ItemCode"] = "02469";
$default_oea["02469"]["ItemName"] = "一屋寶貝音樂劇";
$default_oea["02469"]["CatCode"] = "4";
$default_oea["02469"]["CatName"] = "Aesthetic Development";

$default_oea["02468"]["ItemCode"] = "02468";
$default_oea["02468"]["ItemName"] = "一念之間徵文比賽";
$default_oea["02468"]["CatCode"] = "4";
$default_oea["02468"]["CatName"] = "Aesthetic Development";

$default_oea["02470"]["ItemCode"] = "02470";
$default_oea["02470"]["ItemName"] = "一級運動攀登證書課程";
$default_oea["02470"]["CatCode"] = "3";
$default_oea["02470"]["CatName"] = "Physical Development";

$default_oea["02471"]["ItemCode"] = "02471";
$default_oea["02471"]["ItemName"] = "一起來, 更精彩 - 穗港澳台兩岸四地青少年同根同心喜迎亞運";
$default_oea["02471"]["CatCode"] = "4";
$default_oea["02471"]["CatName"] = "Aesthetic Development";

$default_oea["02489"]["ItemCode"] = "02489";
$default_oea["02489"]["ItemName"] = "三五知己學習時段 (Please provide details in \"Description\" box.)";
$default_oea["02489"]["CatCode"] = "4";
$default_oea["02489"]["CatName"] = "Aesthetic Development";

$default_oea["02490"]["ItemCode"] = "02490";
$default_oea["02490"]["ItemName"] = "三五知己學習時段-人生補時電影反思工作坊";
$default_oea["02490"]["CatCode"] = "5";
$default_oea["02490"]["CatName"] = "Moral and Civic Education";

$default_oea["02487"]["ItemCode"] = "02487";
$default_oea["02487"]["ItemName"] = "三人籃球賽";
$default_oea["02487"]["CatCode"] = "3";
$default_oea["02487"]["CatName"] = "Physical Development";

$default_oea["02488"]["ItemCode"] = "02488";
$default_oea["02488"]["ItemName"] = "三十週年標誌設計比賽";
$default_oea["02488"]["CatCode"] = "4";
$default_oea["02488"]["CatName"] = "Aesthetic Development";

$default_oea["02491"]["ItemCode"] = "02491";
$default_oea["02491"]["ItemName"] = "上海機械奧運會人型機械比賽";
$default_oea["02491"]["CatCode"] = "2";
$default_oea["02491"]["CatName"] = "Career-related Experience";

$default_oea["02507"]["ItemCode"] = "02507";
$default_oea["02507"]["ItemName"] = "不賭小福吉祥物設計比賽";
$default_oea["02507"]["CatCode"] = "4";
$default_oea["02507"]["CatName"] = "Aesthetic Development";

$default_oea["02508"]["ItemCode"] = "02508";
$default_oea["02508"]["ItemName"] = "不賭青年貼紙設計比賽";
$default_oea["02508"]["CatCode"] = "4";
$default_oea["02508"]["CatName"] = "Aesthetic Development";

$default_oea["02619"]["ItemCode"] = "02619";
$default_oea["02619"]["ItemName"] = "世界心臟日香港心臟基金會繪畫比賽";
$default_oea["02619"]["CatCode"] = "4";
$default_oea["02619"]["CatName"] = "Aesthetic Development";

$default_oea["02622"]["ItemCode"] = "02622";
$default_oea["02622"]["ItemName"] = "世界教師日師恩難忘作文比賽";
$default_oea["02622"]["CatCode"] = "4";
$default_oea["02622"]["CatName"] = "Aesthetic Development";

$default_oea["02620"]["ItemCode"] = "02620";
$default_oea["02620"]["ItemName"] = "世界水日";
$default_oea["02620"]["CatCode"] = "1";
$default_oea["02620"]["CatName"] = "Community Service";

$default_oea["02623"]["ItemCode"] = "02623";
$default_oea["02623"]["ItemName"] = "世界關顧自閉日短片製作比賽";
$default_oea["02623"]["CatCode"] = "4";
$default_oea["02623"]["CatName"] = "Aesthetic Development";

$default_oea["02621"]["ItemCode"] = "02621";
$default_oea["02621"]["ItemName"] = "世界防癆日防癆標語創作比賽";
$default_oea["02621"]["CatCode"] = "4";
$default_oea["02621"]["CatName"] = "Aesthetic Development";

$default_oea["02519"]["ItemCode"] = "02519";
$default_oea["02519"]["ItemName"] = "中國中學生作文大賽";
$default_oea["02519"]["CatCode"] = "4";
$default_oea["02519"]["CatName"] = "Aesthetic Development";

$default_oea["02520"]["ItemCode"] = "02520";
$default_oea["02520"]["ItemName"] = "中國歷史短片製作";
$default_oea["02520"]["CatCode"] = "4";
$default_oea["02520"]["CatName"] = "Aesthetic Development";

$default_oea["02509"]["ItemCode"] = "02509";
$default_oea["02509"]["ItemName"] = "中大樹木攝影比賽";
$default_oea["02509"]["CatCode"] = "4";
$default_oea["02509"]["CatName"] = "Aesthetic Development";

$default_oea["02544"]["ItemCode"] = "02544";
$default_oea["02544"]["ItemName"] = "中學校際手球比賽";
$default_oea["02544"]["CatCode"] = "3";
$default_oea["02544"]["CatName"] = "Physical Development";

$default_oea["02546"]["ItemCode"] = "02546";
$default_oea["02546"]["ItemName"] = "中學校際沙灘排球比賽";
$default_oea["02546"]["CatCode"] = "3";
$default_oea["02546"]["CatName"] = "Physical Development";

$default_oea["02545"]["ItemCode"] = "02545";
$default_oea["02545"]["ItemName"] = "中學校際田徑比賽";
$default_oea["02545"]["CatCode"] = "3";
$default_oea["02545"]["CatName"] = "Physical Development";

$default_oea["02547"]["ItemCode"] = "02547";
$default_oea["02547"]["ItemName"] = "中學校際科學常識問答比賽";
$default_oea["02547"]["CatCode"] = "2";
$default_oea["02547"]["CatName"] = "Career-related Experience";

$default_oea["02548"]["ItemCode"] = "02548";
$default_oea["02548"]["ItemName"] = "中學理財教育";
$default_oea["02548"]["CatCode"] = "2";
$default_oea["02548"]["CatName"] = "Career-related Experience";

$default_oea["02536"]["ItemCode"] = "02536";
$default_oea["02536"]["ItemName"] = "中學生作文大賽香港區賽事";
$default_oea["02536"]["CatCode"] = "4";
$default_oea["02536"]["CatName"] = "Aesthetic Development";

$default_oea["02533"]["ItemCode"] = "02533";
$default_oea["02533"]["ItemName"] = "中學生公開講座 讀書的風景與愛美的學問 (香港中文大學主辦)";
$default_oea["02533"]["CatCode"] = "4";
$default_oea["02533"]["CatName"] = "Aesthetic Development";

$default_oea["02538"]["ItemCode"] = "02538";
$default_oea["02538"]["ItemName"] = "中學生創作獎勵計劃";
$default_oea["02538"]["CatCode"] = "4";
$default_oea["02538"]["CatName"] = "Aesthetic Development";

$default_oea["02539"]["ItemCode"] = "02539";
$default_oea["02539"]["ItemName"] = "中學生單車安全壁畫設計比賽";
$default_oea["02539"]["CatCode"] = "4";
$default_oea["02539"]["CatName"] = "Aesthetic Development";

$default_oea["02535"]["ItemCode"] = "02535";
$default_oea["02535"]["ItemName"] = "中學生好書龍虎榜讀後感寫作比賽";
$default_oea["02535"]["CatCode"] = "4";
$default_oea["02535"]["CatName"] = "Aesthetic Development";

$default_oea["02534"]["ItemCode"] = "02534";
$default_oea["02534"]["ItemName"] = "中學生心理健康常識問答比賽";
$default_oea["02534"]["CatCode"] = "4";
$default_oea["02534"]["CatName"] = "Aesthetic Development";

$default_oea["02541"]["ItemCode"] = "02541";
$default_oea["02541"]["ItemName"] = "中學生數學比賽";
$default_oea["02541"]["CatCode"] = "2";
$default_oea["02541"]["CatName"] = "Career-related Experience";

$default_oea["02543"]["ItemCode"] = "02543";
$default_oea["02543"]["ItemName"] = "中學生橋樑模型創作比賽";
$default_oea["02543"]["CatCode"] = "2";
$default_oea["02543"]["CatName"] = "Career-related Experience";

$default_oea["02537"]["ItemCode"] = "02537";
$default_oea["02537"]["ItemName"] = "中學生統計習作比賽";
$default_oea["02537"]["CatCode"] = "2";
$default_oea["02537"]["CatName"] = "Career-related Experience";

$default_oea["02540"]["ItemCode"] = "02540";
$default_oea["02540"]["ItemName"] = "中學生視覺藝術創作展";
$default_oea["02540"]["CatCode"] = "4";
$default_oea["02540"]["CatName"] = "Aesthetic Development";

$default_oea["02542"]["ItemCode"] = "02542";
$default_oea["02542"]["ItemName"] = "中學生閱讀報告比賽";
$default_oea["02542"]["CatCode"] = "4";
$default_oea["02542"]["CatName"] = "Aesthetic Development";

$default_oea["02510"]["ItemCode"] = "02510";
$default_oea["02510"]["ItemName"] = "中小學機械人邀請賽";
$default_oea["02510"]["CatCode"] = "2";
$default_oea["02510"]["CatName"] = "Career-related Experience";

$default_oea["02511"]["ItemCode"] = "02511";
$default_oea["02511"]["ItemName"] = "中文口語模擬考試活動";
$default_oea["02511"]["CatCode"] = "2";
$default_oea["02511"]["CatName"] = "Career-related Experience";

$default_oea["02512"]["ItemCode"] = "02512";
$default_oea["02512"]["ItemName"] = "中文大學學習體驗活動";
$default_oea["02512"]["CatCode"] = "2";
$default_oea["02512"]["CatName"] = "Career-related Experience";

$default_oea["02514"]["ItemCode"] = "02514";
$default_oea["02514"]["ItemName"] = "中文學會 作家講座 李偉才博士 講題 從宇宙觀到人生觀";
$default_oea["02514"]["CatCode"] = "4";
$default_oea["02514"]["CatName"] = "Aesthetic Development";

$default_oea["02513"]["ItemCode"] = "02513";
$default_oea["02513"]["ItemName"] = "中文書法比賽";
$default_oea["02513"]["CatCode"] = "4";
$default_oea["02513"]["CatName"] = "Aesthetic Development";

$default_oea["02532"]["ItemCode"] = "02532";
$default_oea["02532"]["ItemName"] = "中樂團";
$default_oea["02532"]["CatCode"] = "4";
$default_oea["02532"]["CatName"] = "Aesthetic Development";

$default_oea["02531"]["ItemCode"] = "02531";
$default_oea["02531"]["ItemName"] = "中樂欣賞《薩滿與精靈》";
$default_oea["02531"]["CatCode"] = "4";
$default_oea["02531"]["CatName"] = "Aesthetic Development";

$default_oea["02518"]["ItemCode"] = "02518";
$default_oea["02518"]["ItemName"] = "中英劇團戲劇教育計劃初級班";
$default_oea["02518"]["CatCode"] = "4";
$default_oea["02518"]["CatName"] = "Aesthetic Development";

$default_oea["02517"]["ItemCode"] = "02517";
$default_oea["02517"]["ItemName"] = "中英文硬筆書法比賽";
$default_oea["02517"]["CatCode"] = "4";
$default_oea["02517"]["CatName"] = "Aesthetic Development";

$default_oea["02521"]["ItemCode"] = "02521";
$default_oea["02521"]["ItemName"] = "中華人民共和國建國六十週年問答比賽";
$default_oea["02521"]["CatCode"] = "5";
$default_oea["02521"]["CatName"] = "Moral and Civic Education";

$default_oea["02523"]["ItemCode"] = "02523";
$default_oea["02523"]["ItemName"] = "中華基督教會香港區會中學校長會校際三人籃球賽";
$default_oea["02523"]["CatCode"] = "3";
$default_oea["02523"]["CatName"] = "Physical Development";

$default_oea["02524"]["ItemCode"] = "02524";
$default_oea["02524"]["ItemName"] = "中華基督教會香港區會中學校長會體育學習領域工作小組主辦中學校際排球錦標賽";
$default_oea["02524"]["CatCode"] = "3";
$default_oea["02524"]["CatName"] = "Physical Development";

$default_oea["02522"]["ItemCode"] = "02522";
$default_oea["02522"]["ItemName"] = "中華文化．中國心計劃文化大使";
$default_oea["02522"]["CatCode"] = "4";
$default_oea["02522"]["CatName"] = "Aesthetic Development";

$default_oea["02525"]["ItemCode"] = "02525";
$default_oea["02525"]["ItemName"] = "中華經典誦讀大賽(其他地區)";
$default_oea["02525"]["CatCode"] = "5";
$default_oea["02525"]["CatName"] = "Moral and Civic Education";

$default_oea["02526"]["ItemCode"] = "02526";
$default_oea["02526"]["ItemName"] = "中華經典誦讀大賽(香港區)粵語組";
$default_oea["02526"]["CatCode"] = "5";
$default_oea["02526"]["CatName"] = "Moral and Civic Education";

$default_oea["02515"]["ItemCode"] = "02515";
$default_oea["02515"]["ItemName"] = "中西區保育文物建築導賞攝影比賽";
$default_oea["02515"]["CatCode"] = "4";
$default_oea["02515"]["CatName"] = "Aesthetic Development";

$default_oea["02516"]["ItemCode"] = "02516";
$default_oea["02516"]["ItemName"] = "中西區相樸機器人比賽";
$default_oea["02516"]["CatCode"] = "2";
$default_oea["02516"]["CatName"] = "Career-related Experience";

$default_oea["02549"]["ItemCode"] = "02549";
$default_oea["02549"]["ItemName"] = "中醫藥工作坊";
$default_oea["02549"]["CatCode"] = "2";
$default_oea["02549"]["CatName"] = "Career-related Experience";

$default_oea["02527"]["ItemCode"] = "02527";
$default_oea["02527"]["ItemName"] = "中銀香港第五十三屆體育節 青少年運動攀登錦標賽暨公開排名賽";
$default_oea["02527"]["CatCode"] = "3";
$default_oea["02527"]["CatName"] = "Physical Development";

$default_oea["02529"]["ItemCode"] = "02529";
$default_oea["02529"]["ItemName"] = "中銀香港體育節射箭錦標賽";
$default_oea["02529"]["CatCode"] = "3";
$default_oea["02529"]["CatName"] = "Physical Development";

$default_oea["02530"]["ItemCode"] = "02530";
$default_oea["02530"]["ItemName"] = "中銀香港體育節野外定向接力賽";
$default_oea["02530"]["CatCode"] = "3";
$default_oea["02530"]["CatName"] = "Physical Development";

$default_oea["02528"]["ItemCode"] = "02528";
$default_oea["02528"]["ItemName"] = "中銀香港體育節香港青少年柔道錦標賽";
$default_oea["02528"]["CatCode"] = "3";
$default_oea["02528"]["CatName"] = "Physical Development";

$default_oea["02624"]["ItemCode"] = "02624";
$default_oea["02624"]["ItemName"] = "主題學習周—從前的香港、今日的中國";
$default_oea["02624"]["CatCode"] = "5";
$default_oea["02624"]["CatName"] = "Moral and Civic Education";

$default_oea["03047"]["ItemCode"] = "03047";
$default_oea["03047"]["ItemName"] = "乘風航伙伴計劃";
$default_oea["03047"]["CatCode"] = "1";
$default_oea["03047"]["CatName"] = "Community Service";

$default_oea["02473"]["ItemCode"] = "02473";
$default_oea["02473"]["ItemName"] = "九龍中醫院聯網健康新一代健康領袖計劃";
$default_oea["02473"]["CatCode"] = "1";
$default_oea["02473"]["CatName"] = "Community Service";

$default_oea["02474"]["ItemCode"] = "02474";
$default_oea["02474"]["ItemName"] = "九龍中醫院聯網義工訓練課程";
$default_oea["02474"]["CatCode"] = "1";
$default_oea["02474"]["CatName"] = "Community Service";

$default_oea["02472"]["ItemCode"] = "02472";
$default_oea["02472"]["ItemName"] = "九龍中醫院聯網防感染培訓課程";
$default_oea["02472"]["CatCode"] = "2";
$default_oea["02472"]["CatName"] = "Career-related Experience";

$default_oea["02475"]["ItemCode"] = "02475";
$default_oea["02475"]["ItemName"] = "九龍城不毒的Teen空校園活動";
$default_oea["02475"]["CatCode"] = "5";
$default_oea["02475"]["CatName"] = "Moral and Civic Education";

$default_oea["02476"]["ItemCode"] = "02476";
$default_oea["02476"]["ItemName"] = "九龍城中學學長支援計劃";
$default_oea["02476"]["CatCode"] = "1";
$default_oea["02476"]["CatName"] = "Community Service";

$default_oea["02477"]["ItemCode"] = "02477";
$default_oea["02477"]["ItemName"] = "二零一零年香港校際柔道錦標賽";
$default_oea["02477"]["CatCode"] = "3";
$default_oea["02477"]["CatName"] = "Physical Development";

$default_oea["02811"]["ItemCode"] = "02811";
$default_oea["02811"]["ItemName"] = "亞洲女子手球錦標賽";
$default_oea["02811"]["CatCode"] = "3";
$default_oea["02811"]["CatName"] = "Physical Development";

$default_oea["02812"]["ItemCode"] = "02812";
$default_oea["02812"]["ItemName"] = "亞洲跳繩比賽　";
$default_oea["02812"]["CatCode"] = "3";
$default_oea["02812"]["CatName"] = "Physical Development";

$default_oea["02657"]["ItemCode"] = "02657";
$default_oea["02657"]["ItemName"] = "交通安全隊 Road Safety Patrol";
$default_oea["02657"]["CatCode"] = "1";
$default_oea["02657"]["CatName"] = "Community Service";

$default_oea["02658"]["ItemCode"] = "02658";
$default_oea["02658"]["ItemName"] = "交通銀行慈善基金中學生助學金";
$default_oea["02658"]["CatCode"] = "2";
$default_oea["02658"]["CatName"] = "Career-related Experience";

$default_oea["02813"]["ItemCode"] = "02813";
$default_oea["02813"]["ItemName"] = "京崑劇場";
$default_oea["02813"]["CatCode"] = "4";
$default_oea["02813"]["CatName"] = "Aesthetic Development";

$default_oea["02814"]["ItemCode"] = "02814";
$default_oea["02814"]["ItemName"] = "京港同心學生交流團－國際青年文化交流中心主辦";
$default_oea["02814"]["CatCode"] = "4";
$default_oea["02814"]["CatName"] = "Aesthetic Development";

$default_oea["02815"]["ItemCode"] = "02815";
$default_oea["02815"]["ItemName"] = "京滬港青少年手球錦標賽";
$default_oea["02815"]["CatCode"] = "3";
$default_oea["02815"]["CatName"] = "Physical Development";

$default_oea["02483"]["ItemCode"] = "02483";
$default_oea["02483"]["ItemName"] = "人樹共融 綠滿家園攝影比賽";
$default_oea["02483"]["CatCode"] = "4";
$default_oea["02483"]["CatName"] = "Aesthetic Development";

$default_oea["02479"]["ItemCode"] = "02479";
$default_oea["02479"]["ItemName"] = "人物專訪活動";
$default_oea["02479"]["CatCode"] = "4";
$default_oea["02479"]["CatName"] = "Aesthetic Development";

$default_oea["02478"]["ItemCode"] = "02478";
$default_oea["02478"]["ItemName"] = "人生積極為未來，家庭健康由我創嘉年華暨社區服務計劃嘉許禮";
$default_oea["02478"]["CatCode"] = "1";
$default_oea["02478"]["CatName"] = "Community Service";

$default_oea["02481"]["ItemCode"] = "02481";
$default_oea["02481"]["ItemName"] = "人與自然國際兒童書畫大賽";
$default_oea["02481"]["CatCode"] = "4";
$default_oea["02481"]["CatName"] = "Aesthetic Development";

$default_oea["02480"]["ItemCode"] = "02480";
$default_oea["02480"]["ItemName"] = "人間有情攝影比賽";
$default_oea["02480"]["CatCode"] = "4";
$default_oea["02480"]["CatName"] = "Aesthetic Development";

$default_oea["02482"]["ItemCode"] = "02482";
$default_oea["02482"]["ItemName"] = "人際關係話劇創作比賽";
$default_oea["02482"]["CatCode"] = "4";
$default_oea["02482"]["CatName"] = "Aesthetic Development";

$default_oea["02550"]["ItemCode"] = "02550";
$default_oea["02550"]["ItemName"] = "仁愛體育比賽，技巧體操-公開混合雙人跳箱";
$default_oea["02550"]["CatCode"] = "3";
$default_oea["02550"]["CatName"] = "Physical Development";

$default_oea["02551"]["ItemCode"] = "02551";
$default_oea["02551"]["ItemName"] = "仁濟二中魔力橋交流工作坊";
$default_oea["02551"]["CatCode"] = "4";
$default_oea["02551"]["CatName"] = "Aesthetic Development";

$default_oea["02552"]["ItemCode"] = "02552";
$default_oea["02552"]["ItemName"] = "今日Make in China企業環保研習行";
$default_oea["02552"]["CatCode"] = "2";
$default_oea["02552"]["CatName"] = "Career-related Experience";

$default_oea["02553"]["ItemCode"] = "02553";
$default_oea["02553"]["ItemName"] = "今日公益 明日領袖計劃";
$default_oea["02553"]["CatCode"] = "1";
$default_oea["02553"]["CatName"] = "Community Service";

$default_oea["02625"]["ItemCode"] = "02625";
$default_oea["02625"]["ItemName"] = "以區為本—長洲區非物質文化遺產探究課程";
$default_oea["02625"]["CatCode"] = "4";
$default_oea["02625"]["CatName"] = "Aesthetic Development";

$default_oea["02768"]["ItemCode"] = "02768";
$default_oea["02768"]["ItemName"] = "低碳環保在沙田-碳審計計劃";
$default_oea["02768"]["CatCode"] = "5";
$default_oea["02768"]["CatName"] = "Moral and Civic Education";

$default_oea["02767"]["ItemCode"] = "02767";
$default_oea["02767"]["ItemName"] = "何東學校數學日";
$default_oea["02767"]["CatCode"] = "2";
$default_oea["02767"]["CatName"] = "Career-related Experience";

$default_oea["02878"]["ItemCode"] = "02878";
$default_oea["02878"]["ItemName"] = "保育中環之《漫》遊中環《格仔舖》";
$default_oea["02878"]["CatCode"] = "4";
$default_oea["02878"]["CatName"] = "Aesthetic Development";

$default_oea["02879"]["ItemCode"] = "02879";
$default_oea["02879"]["ItemName"] = "保良局李城璧中學25週年校慶標誌設計比賽Logo Design Competition (Please provide details in \"Description\" box.)";
$default_oea["02879"]["CatCode"] = "4";
$default_oea["02879"]["CatName"] = "Aesthetic Development";

$default_oea["02880"]["ItemCode"] = "02880";
$default_oea["02880"]["ItemName"] = "保良局與香港外國記者會(FCC)合辦獎學金繪畫比賽";
$default_oea["02880"]["CatCode"] = "4";
$default_oea["02880"]["CatName"] = "Aesthetic Development";

$default_oea["02881"]["ItemCode"] = "02881";
$default_oea["02881"]["ItemName"] = "保良局體育質素圈體適能獎勵計劃 Po Leung Kuk Health Fitness Scheme";
$default_oea["02881"]["CatCode"] = "3";
$default_oea["02881"]["CatName"] = "Physical Development";

$default_oea["03102"]["ItemCode"] = "03102";
$default_oea["03102"]["ItemName"] = "做好自己徵文比賽";
$default_oea["03102"]["CatCode"] = "4";
$default_oea["03102"]["CatName"] = "Aesthetic Development";

$default_oea["03101"]["ItemCode"] = "03101";
$default_oea["03101"]["ItemName"] = "停一停．想一想──禱告．生活．正能量：向香港年青少年人宣揚拒毒信息";
$default_oea["03101"]["CatCode"] = "5";
$default_oea["03101"]["CatName"] = "Moral and Civic Education";

$default_oea["03103"]["ItemCode"] = "03103";
$default_oea["03103"]["ItemName"] = "健康人生世代學生徵文比賽";
$default_oea["03103"]["CatCode"] = "4";
$default_oea["03103"]["CatName"] = "Aesthetic Development";

$default_oea["03104"]["ItemCode"] = "03104";
$default_oea["03104"]["ItemName"] = "健康城市在北區－健身操比賽";
$default_oea["03104"]["CatCode"] = "3";
$default_oea["03104"]["CatName"] = "Physical Development";

$default_oea["03105"]["ItemCode"] = "03105";
$default_oea["03105"]["ItemName"] = "健康校園抗毒行動教育劇場";
$default_oea["03105"]["CatCode"] = "5";
$default_oea["03105"]["CatName"] = "Moral and Civic Education";

$default_oea["03106"]["ItemCode"] = "03106";
$default_oea["03106"]["ItemName"] = "健康校園齊創建計畫";
$default_oea["03106"]["CatCode"] = "5";
$default_oea["03106"]["CatName"] = "Moral and Civic Education";

$default_oea["03172"]["ItemCode"] = "03172";
$default_oea["03172"]["ItemName"] = "傑出中學生領袖選舉";
$default_oea["03172"]["CatCode"] = "2";
$default_oea["03172"]["CatName"] = "Career-related Experience";

$default_oea["03173"]["ItemCode"] = "03173";
$default_oea["03173"]["ItemName"] = "傑出中學運動員嘉許計劃";
$default_oea["03173"]["CatCode"] = "3";
$default_oea["03173"]["CatName"] = "Physical Development";

$default_oea["03174"]["ItemCode"] = "03174";
$default_oea["03174"]["ItemName"] = "傑出普通話大使";
$default_oea["03174"]["CatCode"] = "4";
$default_oea["03174"]["CatName"] = "Aesthetic Development";

$default_oea["03175"]["ItemCode"] = "03175";
$default_oea["03175"]["ItemName"] = "傑出義工選舉 2009";
$default_oea["03175"]["CatCode"] = "1";
$default_oea["03175"]["CatName"] = "Community Service";

$default_oea["03176"]["ItemCode"] = "03176";
$default_oea["03176"]["ItemName"] = "傑青禁毒大使計劃";
$default_oea["03176"]["CatCode"] = "5";
$default_oea["03176"]["CatName"] = "Moral and Civic Education";

$default_oea["03243"]["ItemCode"] = "03243";
$default_oea["03243"]["ItemName"] = "傳媒初體驗計劃模擬新聞採訪比賽";
$default_oea["03243"]["CatCode"] = "2";
$default_oea["03243"]["CatName"] = "Career-related Experience";

$default_oea["03244"]["ItemCode"] = "03244";
$default_oea["03244"]["ItemName"] = "傳揚天水圍北關愛家庭計劃暨標語創作比賽";
$default_oea["03244"]["CatCode"] = "4";
$default_oea["03244"]["CatName"] = "Aesthetic Development";

$default_oea["03245"]["ItemCode"] = "03245";
$default_oea["03245"]["ItemName"] = "傷健共融 (Please provide details in \"Description\" box.)";
$default_oea["03245"]["CatCode"] = "1";
$default_oea["03245"]["CatName"] = "Community Service";

$default_oea["03246"]["ItemCode"] = "03246";
$default_oea["03246"]["ItemName"] = "傷殘人士運動會";
$default_oea["03246"]["CatCode"] = "3";
$default_oea["03246"]["CatName"] = "Physical Development";

$default_oea["03447"]["ItemCode"] = "03447";
$default_oea["03447"]["ItemName"] = "優秀女童軍選舉、隊伍優秀女童軍選舉";
$default_oea["03447"]["CatCode"] = "1";
$default_oea["03447"]["CatName"] = "Community Service";

$default_oea["03448"]["ItemCode"] = "03448";
$default_oea["03448"]["ItemName"] = "優等生分享";
$default_oea["03448"]["CatCode"] = "2";
$default_oea["03448"]["CatName"] = "Career-related Experience";

$default_oea["02555"]["ItemCode"] = "02555";
$default_oea["02555"]["ItemName"] = "元朗初中學生最佳進步奬";
$default_oea["02555"]["CatCode"] = "2";
$default_oea["02555"]["CatName"] = "Career-related Experience";

$default_oea["02554"]["ItemCode"] = "02554";
$default_oea["02554"]["ItemName"] = "元朗屯門區中學辯論比賽";
$default_oea["02554"]["CatCode"] = "2";
$default_oea["02554"]["CatName"] = "Career-related Experience";

$default_oea["02558"]["ItemCode"] = "02558";
$default_oea["02558"]["ItemName"] = "元朗藝術節";
$default_oea["02558"]["CatCode"] = "4";
$default_oea["02558"]["CatName"] = "Aesthetic Development";

$default_oea["02557"]["ItemCode"] = "02557";
$default_oea["02557"]["ItemName"] = "元朗象棋比賽";
$default_oea["02557"]["CatCode"] = "4";
$default_oea["02557"]["CatName"] = "Aesthetic Development";

$default_oea["02556"]["ItemCode"] = "02556";
$default_oea["02556"]["ItemName"] = "元朗青年愛文化博覽計劃：最有特色的元朗文化導覽路線設計比賽";
$default_oea["02556"]["CatCode"] = "4";
$default_oea["02556"]["CatName"] = "Aesthetic Development";

$default_oea["02816"]["ItemCode"] = "02816";
$default_oea["02816"]["ItemName"] = "兒童權利與責任四格漫畫創作比賽";
$default_oea["02816"]["CatCode"] = "4";
$default_oea["02816"]["CatName"] = "Aesthetic Development";

$default_oea["02660"]["ItemCode"] = "02660";
$default_oea["02660"]["ItemName"] = "全國中小學師生榮辱觀個性化作文、文藝作品徵文比賽";
$default_oea["02660"]["CatCode"] = "4";
$default_oea["02660"]["CatName"] = "Aesthetic Development";

$default_oea["02663"]["ItemCode"] = "02663";
$default_oea["02663"]["ItemName"] = "全國美術考級試";
$default_oea["02663"]["CatCode"] = "4";
$default_oea["02663"]["CatName"] = "Aesthetic Development";

$default_oea["02665"]["ItemCode"] = "02665";
$default_oea["02665"]["ItemName"] = "全國華羅庚金杯少年數學邀請賽(香港賽區)";
$default_oea["02665"]["CatCode"] = "2";
$default_oea["02665"]["CatName"] = "Career-related Experience";

$default_oea["02664"]["ItemCode"] = "02664";
$default_oea["02664"]["ItemName"] = "全國速度輪滑錦標賽";
$default_oea["02664"]["CatCode"] = "3";
$default_oea["02664"]["CatName"] = "Physical Development";

$default_oea["02661"]["ItemCode"] = "02661";
$default_oea["02661"]["ItemName"] = "全國青少年民樂團隊比賽";
$default_oea["02661"]["CatCode"] = "4";
$default_oea["02661"]["CatName"] = "Aesthetic Development";

$default_oea["02662"]["ItemCode"] = "02662";
$default_oea["02662"]["ItemName"] = "全國青少年科學與藝術春節聯歡晚會比賽";
$default_oea["02662"]["CatCode"] = "4";
$default_oea["02662"]["CatName"] = "Aesthetic Development";

$default_oea["02659"]["ItemCode"] = "02659";
$default_oea["02659"]["ItemName"] = "全方位學習科野外定向(體育)";
$default_oea["02659"]["CatCode"] = "3";
$default_oea["02659"]["CatName"] = "Physical Development";

$default_oea["02677"]["ItemCode"] = "02677";
$default_oea["02677"]["ItemName"] = "全港中學中國歷史研習獎勵計劃";
$default_oea["02677"]["CatCode"] = "5";
$default_oea["02677"]["CatName"] = "Moral and Civic Education";

$default_oea["02678"]["ItemCode"] = "02678";
$default_oea["02678"]["ItemName"] = "全港中學中國歷史研習獎勵計劃-嘉許狀(高級組)";
$default_oea["02678"]["CatCode"] = "4";
$default_oea["02678"]["CatName"] = "Aesthetic Development";

$default_oea["02690"]["ItemCode"] = "02690";
$default_oea["02690"]["ItemName"] = "全港中學劍擊精英爭霸戰";
$default_oea["02690"]["CatCode"] = "3";
$default_oea["02690"]["CatName"] = "Physical Development";

$default_oea["02680"]["ItemCode"] = "02680";
$default_oea["02680"]["ItemName"] = "全港中學生中國象棋個人賽";
$default_oea["02680"]["CatCode"] = "4";
$default_oea["02680"]["CatName"] = "Aesthetic Development";

$default_oea["02681"]["ItemCode"] = "02681";
$default_oea["02681"]["ItemName"] = "全港中學生五大要貧窮要聞選舉暨要聞短評比賽";
$default_oea["02681"]["CatCode"] = "5";
$default_oea["02681"]["CatName"] = "Moral and Civic Education";

$default_oea["02679"]["ItemCode"] = "02679";
$default_oea["02679"]["ItemName"] = "全港中學生十大新聞選舉新聞評述比賽";
$default_oea["02679"]["CatCode"] = "2";
$default_oea["02679"]["CatName"] = "Career-related Experience";

$default_oea["02685"]["ItemCode"] = "02685";
$default_oea["02685"]["ItemName"] = "全港中學生廣告短片比賽頒獎典禮";
$default_oea["02685"]["CatCode"] = "4";
$default_oea["02685"]["CatName"] = "Aesthetic Development";

$default_oea["02682"]["ItemCode"] = "02682";
$default_oea["02682"]["ItemName"] = "全港中學生愛家人短片創作大賽";
$default_oea["02682"]["CatCode"] = "4";
$default_oea["02682"]["CatName"] = "Aesthetic Development";

$default_oea["02686"]["ItemCode"] = "02686";
$default_oea["02686"]["ItemName"] = "全港中學生數學短片創作比賽";
$default_oea["02686"]["CatCode"] = "4";
$default_oea["02686"]["CatName"] = "Aesthetic Development";

$default_oea["02683"]["ItemCode"] = "02683";
$default_oea["02683"]["ItemName"] = "全港中學生會計電腦軟件應用比賽";
$default_oea["02683"]["CatCode"] = "2";
$default_oea["02683"]["CatName"] = "Career-related Experience";

$default_oea["02684"]["ItemCode"] = "02684";
$default_oea["02684"]["ItemName"] = "全港中學生網頁設計邀請賽";
$default_oea["02684"]["CatCode"] = "2";
$default_oea["02684"]["CatName"] = "Career-related Experience";

$default_oea["02687"]["ItemCode"] = "02687";
$default_oea["02687"]["ItemName"] = "全港中學生藝術評賞徵文比賽";
$default_oea["02687"]["CatCode"] = "4";
$default_oea["02687"]["CatName"] = "Aesthetic Development";

$default_oea["02688"]["ItemCode"] = "02688";
$default_oea["02688"]["ItemName"] = "全港中學視藝精英作品邀請展暨全港十大傑出視覺藝術中學生選舉比賽";
$default_oea["02688"]["CatCode"] = "4";
$default_oea["02688"]["CatName"] = "Aesthetic Development";

$default_oea["02689"]["ItemCode"] = "02689";
$default_oea["02689"]["ItemName"] = "全港中學跳繩精英比賽";
$default_oea["02689"]["CatCode"] = "3";
$default_oea["02689"]["CatName"] = "Physical Development";

$default_oea["02675"]["ItemCode"] = "02675";
$default_oea["02675"]["ItemName"] = "全港中小學儒家德育及公民教育故事 (漫畫)創作大賽";
$default_oea["02675"]["CatCode"] = "5";
$default_oea["02675"]["CatName"] = "Moral and Civic Education";

$default_oea["02673"]["ItemCode"] = "02673";
$default_oea["02673"]["ItemName"] = "全港中小學普通話演講比賽 (Please provide details in \"Description\" box.)";
$default_oea["02673"]["CatCode"] = "4";
$default_oea["02673"]["CatName"] = "Aesthetic Development";

$default_oea["02672"]["ItemCode"] = "02672";
$default_oea["02672"]["ItemName"] = "全港中小學英文徵文比賽";
$default_oea["02672"]["CatCode"] = "4";
$default_oea["02672"]["CatName"] = "Aesthetic Development";

$default_oea["02676"]["ItemCode"] = "02676";
$default_oea["02676"]["ItemName"] = "全港中文書法比賽‧第一回合校內複賽";
$default_oea["02676"]["CatCode"] = "4";
$default_oea["02676"]["CatName"] = "Aesthetic Development";

$default_oea["02691"]["ItemCode"] = "02691";
$default_oea["02691"]["ItemName"] = "全港公開夜光龍錦標賽";
$default_oea["02691"]["CatCode"] = "3";
$default_oea["02691"]["CatName"] = "Physical Development";

$default_oea["02692"]["ItemCode"] = "02692";
$default_oea["02692"]["ItemName"] = "全港公開標準舞及拉丁舞大賽";
$default_oea["02692"]["CatCode"] = "4";
$default_oea["02692"]["CatName"] = "Aesthetic Development";

$default_oea["02669"]["ItemCode"] = "02669";
$default_oea["02669"]["ItemName"] = "全港十八區繪畫比賽 (慶祝中華人民共和國成立60周年)";
$default_oea["02669"]["CatCode"] = "4";
$default_oea["02669"]["CatName"] = "Aesthetic Development";

$default_oea["02704"]["ItemCode"] = "02704";
$default_oea["02704"]["ItemName"] = "全港原子筆中文書法比賽";
$default_oea["02704"]["CatCode"] = "4";
$default_oea["02704"]["CatName"] = "Aesthetic Development";

$default_oea["02670"]["ItemCode"] = "02670";
$default_oea["02670"]["ItemName"] = "全港大富翁比賽";
$default_oea["02670"]["CatCode"] = "2";
$default_oea["02670"]["CatName"] = "Career-related Experience";

$default_oea["02714"]["ItemCode"] = "02714";
$default_oea["02714"]["ItemName"] = "全港學生中國象棋比賽";
$default_oea["02714"]["CatCode"] = "4";
$default_oea["02714"]["CatName"] = "Aesthetic Development";

$default_oea["02715"]["ItemCode"] = "02715";
$default_oea["02715"]["ItemName"] = "全港學生公開攝影比賽";
$default_oea["02715"]["CatCode"] = "4";
$default_oea["02715"]["CatName"] = "Aesthetic Development";

$default_oea["02716"]["ItemCode"] = "02716";
$default_oea["02716"]["ItemName"] = "全港學生撐大運  為家國、為香港運動員打打氣口號創作比賽";
$default_oea["02716"]["CatCode"] = "5";
$default_oea["02716"]["CatName"] = "Moral and Civic Education";

$default_oea["02717"]["ItemCode"] = "02717";
$default_oea["02717"]["ItemName"] = "全港學生攝影比賽";
$default_oea["02717"]["CatCode"] = "4";
$default_oea["02717"]["CatName"] = "Aesthetic Development";

$default_oea["02720"]["ItemCode"] = "02720";
$default_oea["02720"]["ItemName"] = "全港學界保齡球比賽";
$default_oea["02720"]["CatCode"] = "3";
$default_oea["02720"]["CatName"] = "Physical Development";

$default_oea["02721"]["ItemCode"] = "02721";
$default_oea["02721"]["ItemName"] = "全港學界拯溺比賽";
$default_oea["02721"]["CatCode"] = "3";
$default_oea["02721"]["CatName"] = "Physical Development";

$default_oea["02723"]["ItemCode"] = "02723";
$default_oea["02723"]["ItemName"] = "全港學界普通話傳藝比賽";
$default_oea["02723"]["CatCode"] = "4";
$default_oea["02723"]["CatName"] = "Aesthetic Development";

$default_oea["02722"]["ItemCode"] = "02722";
$default_oea["02722"]["ItemName"] = "全港學界最佳進步獎";
$default_oea["02722"]["CatCode"] = "2";
$default_oea["02722"]["CatName"] = "Career-related Experience";

$default_oea["02727"]["ItemCode"] = "02727";
$default_oea["02727"]["ItemName"] = "全港學界籃球馬拉松比賽";
$default_oea["02727"]["CatCode"] = "3";
$default_oea["02727"]["CatName"] = "Physical Development";

$default_oea["02725"]["ItemCode"] = "02725";
$default_oea["02725"]["ItemName"] = "全港學界精英乒乓球比賽";
$default_oea["02725"]["CatCode"] = "3";
$default_oea["02725"]["CatName"] = "Physical Development";

$default_oea["02726"]["ItemCode"] = "02726";
$default_oea["02726"]["ItemName"] = "全港學界精英足球比賽";
$default_oea["02726"]["CatCode"] = "3";
$default_oea["02726"]["CatName"] = "Physical Development";

$default_oea["02718"]["ItemCode"] = "02718";
$default_oea["02718"]["ItemName"] = "全港學界羽毛球精英賽";
$default_oea["02718"]["CatCode"] = "3";
$default_oea["02718"]["CatName"] = "Physical Development";

$default_oea["02724"]["ItemCode"] = "02724";
$default_oea["02724"]["ItemName"] = "全港學界跆拳道比賽";
$default_oea["02724"]["CatCode"] = "3";
$default_oea["02724"]["CatName"] = "Physical Development";

$default_oea["02719"]["ItemCode"] = "02719";
$default_oea["02719"]["ItemName"] = "全港學界迎亞運嘉年華活動 (由扶輪社主辦)";
$default_oea["02719"]["CatCode"] = "3";
$default_oea["02719"]["CatName"] = "Physical Development";

$default_oea["02671"]["ItemCode"] = "02671";
$default_oea["02671"]["ItemName"] = "全港小學生關心祖國 - 齊顯愛國情 (第四屆) 比賽";
$default_oea["02671"]["CatCode"] = "5";
$default_oea["02671"]["CatName"] = "Moral and Civic Education";

$default_oea["02705"]["ItemCode"] = "02705";
$default_oea["02705"]["ItemName"] = "全港校際IT精英暨互聯網問答大賽";
$default_oea["02705"]["CatCode"] = "2";
$default_oea["02705"]["CatName"] = "Career-related Experience";

$default_oea["02706"]["ItemCode"] = "02706";
$default_oea["02706"]["ItemName"] = "全港校際即時演講比賽";
$default_oea["02706"]["CatCode"] = "2";
$default_oea["02706"]["CatName"] = "Career-related Experience";

$default_oea["02709"]["ItemCode"] = "02709";
$default_oea["02709"]["ItemName"] = "全港經典故事、小品、詩歌表演比賽";
$default_oea["02709"]["CatCode"] = "4";
$default_oea["02709"]["CatName"] = "Aesthetic Development";

$default_oea["02710"]["ItemCode"] = "02710";
$default_oea["02710"]["ItemName"] = "全港經濟精英杯爭霸戰";
$default_oea["02710"]["CatCode"] = "2";
$default_oea["02710"]["CatName"] = "Career-related Experience";

$default_oea["02711"]["ItemCode"] = "02711";
$default_oea["02711"]["ItemName"] = "全港詩詞創作大賽";
$default_oea["02711"]["CatCode"] = "4";
$default_oea["02711"]["CatName"] = "Aesthetic Development";

$default_oea["02712"]["ItemCode"] = "02712";
$default_oea["02712"]["ItemName"] = "全港跳繩精英賽";
$default_oea["02712"]["CatCode"] = "3";
$default_oea["02712"]["CatName"] = "Physical Development";

$default_oea["02708"]["ItemCode"] = "02708";
$default_oea["02708"]["ItemName"] = "全港通識教育校際議題問答比賽";
$default_oea["02708"]["CatCode"] = "5";
$default_oea["02708"]["CatName"] = "Moral and Civic Education";

$default_oea["02713"]["ItemCode"] = "02713";
$default_oea["02713"]["ItemName"] = "全港運動會開幕典禮";
$default_oea["02713"]["CatCode"] = "3";
$default_oea["02713"]["CatName"] = "Physical Development";

$default_oea["02693"]["ItemCode"] = "02693";
$default_oea["02693"]["ItemName"] = "全港青少年入樽服務計劃";
$default_oea["02693"]["CatCode"] = "3";
$default_oea["02693"]["CatName"] = "Physical Development";

$default_oea["02694"]["ItemCode"] = "02694";
$default_oea["02694"]["ItemName"] = "全港青少年入樽計劃藍球比賽";
$default_oea["02694"]["CatCode"] = "3";
$default_oea["02694"]["CatName"] = "Physical Development";

$default_oea["02695"]["ItemCode"] = "02695";
$default_oea["02695"]["ItemName"] = "全港青少年草地滾球賽";
$default_oea["02695"]["CatCode"] = "3";
$default_oea["02695"]["CatName"] = "Physical Development";

$default_oea["02696"]["ItemCode"] = "02696";
$default_oea["02696"]["ItemName"] = "全港青少年野外定向錦標賽";
$default_oea["02696"]["CatCode"] = "3";
$default_oea["02696"]["CatName"] = "Physical Development";

$default_oea["02697"]["ItemCode"] = "02697";
$default_oea["02697"]["ItemName"] = "全港青年中國古典詩詞朗誦比賽";
$default_oea["02697"]["CatCode"] = "4";
$default_oea["02697"]["CatName"] = "Aesthetic Development";

$default_oea["02701"]["ItemCode"] = "02701";
$default_oea["02701"]["ItemName"] = "全港青年學藝攝影比賽";
$default_oea["02701"]["CatCode"] = "4";
$default_oea["02701"]["CatName"] = "Aesthetic Development";

$default_oea["02699"]["ItemCode"] = "02699";
$default_oea["02699"]["ItemName"] = "全港青年學藝比賽(演講比賽)";
$default_oea["02699"]["CatCode"] = "4";
$default_oea["02699"]["CatName"] = "Aesthetic Development";

$default_oea["02700"]["ItemCode"] = "02700";
$default_oea["02700"]["ItemName"] = "全港青年學藝比賽之標語及海報創作比賽";
$default_oea["02700"]["CatCode"] = "4";
$default_oea["02700"]["CatName"] = "Aesthetic Development";

$default_oea["02702"]["ItemCode"] = "02702";
$default_oea["02702"]["ItemName"] = "全港青年繪畫大賽";
$default_oea["02702"]["CatCode"] = "4";
$default_oea["02702"]["CatName"] = "Aesthetic Development";

$default_oea["02703"]["ItemCode"] = "02703";
$default_oea["02703"]["ItemName"] = "全港青年繪畫比賽";
$default_oea["02703"]["CatCode"] = "4";
$default_oea["02703"]["CatName"] = "Aesthetic Development";

$default_oea["02698"]["ItemCode"] = "02698";
$default_oea["02698"]["ItemName"] = "全港青年象棋比賽";
$default_oea["02698"]["CatCode"] = "4";
$default_oea["02698"]["CatName"] = "Aesthetic Development";

$default_oea["02707"]["ItemCode"] = "02707";
$default_oea["02707"]["ItemName"] = "全港高中學生電子心意卡設計比賽工作坊";
$default_oea["02707"]["CatCode"] = "4";
$default_oea["02707"]["CatName"] = "Aesthetic Development";

$default_oea["02666"]["ItemCode"] = "02666";
$default_oea["02666"]["ItemName"] = "全球化專題研習報告比賽";
$default_oea["02666"]["CatCode"] = "2";
$default_oea["02666"]["CatName"] = "Career-related Experience";

$default_oea["02667"]["ItemCode"] = "02667";
$default_oea["02667"]["ItemName"] = "全球華人慶祝建國60週年系列及喜迎世博，共襄盛舉系列校際徵文比賽";
$default_oea["02667"]["CatCode"] = "5";
$default_oea["02667"]["CatName"] = "Moral and Civic Education";

$default_oea["02668"]["ItemCode"] = "02668";
$default_oea["02668"]["ItemName"] = "全球論語繪本比賽";
$default_oea["02668"]["CatCode"] = "5";
$default_oea["02668"]["CatName"] = "Moral and Civic Education";

$default_oea["02817"]["ItemCode"] = "02817";
$default_oea["02817"]["ItemName"] = "兩個名字，一個故事- 樂施會專題探究活動";
$default_oea["02817"]["CatCode"] = "1";
$default_oea["02817"]["CatName"] = "Community Service";

$default_oea["02559"]["ItemCode"] = "02559";
$default_oea["02559"]["ItemName"] = "公民問答比賽";
$default_oea["02559"]["CatCode"] = "5";
$default_oea["02559"]["CatName"] = "Moral and Civic Education";

$default_oea["02560"]["ItemCode"] = "02560";
$default_oea["02560"]["ItemName"] = "公民教育";
$default_oea["02560"]["CatCode"] = "5";
$default_oea["02560"]["CatName"] = "Moral and Civic Education";

$default_oea["02561"]["ItemCode"] = "02561";
$default_oea["02561"]["ItemName"] = "公民教育互動劇場";
$default_oea["02561"]["CatCode"] = "5";
$default_oea["02561"]["CatName"] = "Moral and Civic Education";

$default_oea["02562"]["ItemCode"] = "02562";
$default_oea["02562"]["ItemName"] = "公民教育劇巾幗揚眉";
$default_oea["02562"]["CatCode"] = "5";
$default_oea["02562"]["CatName"] = "Moral and Civic Education";

$default_oea["02563"]["ItemCode"] = "02563";
$default_oea["02563"]["ItemName"] = "公益少年團創奇先鋒領袖培訓";
$default_oea["02563"]["CatCode"] = "1";
$default_oea["02563"]["CatName"] = "Community Service";

$default_oea["02564"]["ItemCode"] = "02564";
$default_oea["02564"]["ItemName"] = "公益慈善便服清潔日及聖誕慶祝會";
$default_oea["02564"]["CatCode"] = "1";
$default_oea["02564"]["CatName"] = "Community Service";

$default_oea["02728"]["ItemCode"] = "02728";
$default_oea["02728"]["ItemName"] = "冰雪情國際師生書畫攝影作品交流展";
$default_oea["02728"]["CatCode"] = "4";
$default_oea["02728"]["CatName"] = "Aesthetic Development";

$default_oea["03407"]["ItemCode"] = "03407";
$default_oea["03407"]["ItemName"] = "凝音樂聚音樂會表演";
$default_oea["03407"]["CatCode"] = "4";
$default_oea["03407"]["CatName"] = "Aesthetic Development";

$default_oea["02565"]["ItemCode"] = "02565";
$default_oea["02565"]["ItemName"] = "分區中學校際乒乓球比賽";
$default_oea["02565"]["CatCode"] = "3";
$default_oea["02565"]["CatName"] = "Physical Development";

$default_oea["02568"]["ItemCode"] = "02568";
$default_oea["02568"]["ItemName"] = "分區中學校際排球比賽";
$default_oea["02568"]["CatCode"] = "3";
$default_oea["02568"]["CatName"] = "Physical Development";

$default_oea["02569"]["ItemCode"] = "02569";
$default_oea["02569"]["ItemName"] = "分區中學校際游泳比賽";
$default_oea["02569"]["CatCode"] = "3";
$default_oea["02569"]["CatName"] = "Physical Development";

$default_oea["02570"]["ItemCode"] = "02570";
$default_oea["02570"]["ItemName"] = "分區中學校際籃球比賽";
$default_oea["02570"]["CatCode"] = "3";
$default_oea["02570"]["CatName"] = "Physical Development";

$default_oea["02566"]["ItemCode"] = "02566";
$default_oea["02566"]["ItemName"] = "分區中學校際羽毛球比賽";
$default_oea["02566"]["CatCode"] = "3";
$default_oea["02566"]["CatName"] = "Physical Development";

$default_oea["02567"]["ItemCode"] = "02567";
$default_oea["02567"]["ItemName"] = "分區中學校際足球比賽";
$default_oea["02567"]["CatCode"] = "3";
$default_oea["02567"]["CatName"] = "Physical Development";

$default_oea["02571"]["ItemCode"] = "02571";
$default_oea["02571"]["ItemName"] = "分區分齡田徑比賽";
$default_oea["02571"]["CatCode"] = "3";
$default_oea["02571"]["CatName"] = "Physical Development";

$default_oea["02574"]["ItemCode"] = "02574";
$default_oea["02574"]["ItemName"] = "分區堆沙比賽";
$default_oea["02574"]["CatCode"] = "3";
$default_oea["02574"]["CatName"] = "Physical Development";

$default_oea["02577"]["ItemCode"] = "02577";
$default_oea["02577"]["ItemName"] = "分區學界乒乓球比賽";
$default_oea["02577"]["CatCode"] = "3";
$default_oea["02577"]["CatName"] = "Physical Development";

$default_oea["02583"]["ItemCode"] = "02583";
$default_oea["02583"]["ItemName"] = "分區學界劍擊比賽";
$default_oea["02583"]["CatCode"] = "3";
$default_oea["02583"]["CatName"] = "Physical Development";

$default_oea["02576"]["ItemCode"] = "02576";
$default_oea["02576"]["ItemName"] = "分區學界手球比賽";
$default_oea["02576"]["CatCode"] = "3";
$default_oea["02576"]["CatName"] = "Physical Development";

$default_oea["02580"]["ItemCode"] = "02580";
$default_oea["02580"]["ItemName"] = "分區學界排球比賽";
$default_oea["02580"]["CatCode"] = "3";
$default_oea["02580"]["CatName"] = "Physical Development";

$default_oea["02581"]["ItemCode"] = "02581";
$default_oea["02581"]["ItemName"] = "分區學界游泳比賽";
$default_oea["02581"]["CatCode"] = "3";
$default_oea["02581"]["CatName"] = "Physical Development";

$default_oea["02584"]["ItemCode"] = "02584";
$default_oea["02584"]["ItemName"] = "分區學界籃球比賽";
$default_oea["02584"]["CatCode"] = "3";
$default_oea["02584"]["CatName"] = "Physical Development";

$default_oea["02578"]["ItemCode"] = "02578";
$default_oea["02578"]["ItemName"] = "分區學界羽毛球比賽";
$default_oea["02578"]["CatCode"] = "3";
$default_oea["02578"]["CatName"] = "Physical Development";

$default_oea["02582"]["ItemCode"] = "02582";
$default_oea["02582"]["ItemName"] = "分區學界越野賽";
$default_oea["02582"]["CatCode"] = "3";
$default_oea["02582"]["CatName"] = "Physical Development";

$default_oea["02579"]["ItemCode"] = "02579";
$default_oea["02579"]["ItemName"] = "分區學界足球賽";
$default_oea["02579"]["CatCode"] = "3";
$default_oea["02579"]["CatName"] = "Physical Development";

$default_oea["02572"]["ItemCode"] = "02572";
$default_oea["02572"]["ItemName"] = "分區射箭比賽";
$default_oea["02572"]["CatCode"] = "3";
$default_oea["02572"]["CatName"] = "Physical Development";

$default_oea["02573"]["ItemCode"] = "02573";
$default_oea["02573"]["ItemName"] = "分區校際越野賽";
$default_oea["02573"]["CatCode"] = "3";
$default_oea["02573"]["CatName"] = "Physical Development";

$default_oea["02575"]["ItemCode"] = "02575";
$default_oea["02575"]["ItemName"] = "分區舞蹈比賽";
$default_oea["02575"]["CatCode"] = "4";
$default_oea["02575"]["CatName"] = "Aesthetic Development";

$default_oea["02585"]["ItemCode"] = "02585";
$default_oea["02585"]["ItemName"] = "分齡乒乓球比賽";
$default_oea["02585"]["CatCode"] = "3";
$default_oea["02585"]["CatName"] = "Physical Development";

$default_oea["02769"]["ItemCode"] = "02769";
$default_oea["02769"]["ItemName"] = "利民會銀禧紀念標語設計比賽";
$default_oea["02769"]["CatCode"] = "4";
$default_oea["02769"]["CatName"] = "Aesthetic Development";

$default_oea["03179"]["ItemCode"] = "03179";
$default_oea["03179"]["ItemName"] = "創協盃-泛珠三角智能機械及創意設計比賽";
$default_oea["03179"]["CatCode"] = "2";
$default_oea["03179"]["CatName"] = "Career-related Experience";

$default_oea["03180"]["ItemCode"] = "03180";
$default_oea["03180"]["ItemName"] = "創意領域新紀元－第五屆元朗屯門區青少年創意作品比賽及展覽";
$default_oea["03180"]["CatCode"] = "4";
$default_oea["03180"]["CatName"] = "Aesthetic Development";

$default_oea["03181"]["ItemCode"] = "03181";
$default_oea["03181"]["ItemName"] = "創新產品設計- 應用學習導引課程";
$default_oea["03181"]["CatCode"] = "4";
$default_oea["03181"]["CatName"] = "Aesthetic Development";

$default_oea["03369"]["ItemCode"] = "03369";
$default_oea["03369"]["ItemName"] = "劍樂會劍擊邀請賽";
$default_oea["03369"]["CatCode"] = "3";
$default_oea["03369"]["CatName"] = "Physical Development";

$default_oea["03370"]["ItemCode"] = "03370";
$default_oea["03370"]["ItemName"] = "劍樂會劍擊邀請賽女子重劍團體季軍";
$default_oea["03370"]["CatCode"] = "3";
$default_oea["03370"]["CatName"] = "Physical Development";

$default_oea["02484"]["ItemCode"] = "02484";
$default_oea["02484"]["ItemName"] = "力行盃足球比賽";
$default_oea["02484"]["CatCode"] = "3";
$default_oea["02484"]["CatName"] = "Physical Development";

$default_oea["03182"]["ItemCode"] = "03182";
$default_oea["03182"]["ItemName"] = "勞動歲月。相片展(導賞活動)";
$default_oea["03182"]["CatCode"] = "4";
$default_oea["03182"]["CatName"] = "Aesthetic Development";

$default_oea["03449"]["ItemCode"] = "03449";
$default_oea["03449"]["ItemName"] = "勵志音樂劇【我們的大舞台】";
$default_oea["03449"]["CatCode"] = "4";
$default_oea["03449"]["CatName"] = "Aesthetic Development";

$default_oea["02627"]["ItemCode"] = "02627";
$default_oea["02627"]["ItemName"] = "北京全國青少年兒童書畫攝影才藝展評活動";
$default_oea["02627"]["CatCode"] = "4";
$default_oea["02627"]["CatName"] = "Aesthetic Development";

$default_oea["02626"]["ItemCode"] = "02626";
$default_oea["02626"]["ItemName"] = "北京天津 - 文化古城探索之旅";
$default_oea["02626"]["CatCode"] = "4";
$default_oea["02626"]["CatName"] = "Aesthetic Development";

$default_oea["02628"]["ItemCode"] = "02628";
$default_oea["02628"]["ItemName"] = "北京學習交流團";
$default_oea["02628"]["CatCode"] = "2";
$default_oea["02628"]["CatName"] = "Career-related Experience";

$default_oea["02629"]["ItemCode"] = "02629";
$default_oea["02629"]["ItemName"] = "北區閱讀節";
$default_oea["02629"]["CatCode"] = "3";
$default_oea["02629"]["CatName"] = "Physical Development";

$default_oea["02630"]["ItemCode"] = "02630";
$default_oea["02630"]["ItemName"] = "北極虹青少年交流營";
$default_oea["02630"]["CatCode"] = "2";
$default_oea["02630"]["CatName"] = "Career-related Experience";

$default_oea["02729"]["ItemCode"] = "02729";
$default_oea["02729"]["ItemName"] = "匡智香港賽馬會全港中小學社區有機農耕比賽";
$default_oea["02729"]["CatCode"] = "4";
$default_oea["02729"]["CatName"] = "Aesthetic Development";

$default_oea["02485"]["ItemCode"] = "02485";
$default_oea["02485"]["ItemName"] = "十八區啦啦隊比賽";
$default_oea["02485"]["CatCode"] = "3";
$default_oea["02485"]["CatName"] = "Physical Development";

$default_oea["02486"]["ItemCode"] = "02486";
$default_oea["02486"]["ItemName"] = "十年前後徵文比賽";
$default_oea["02486"]["CatCode"] = "4";
$default_oea["02486"]["CatName"] = "Aesthetic Development";

$default_oea["02492"]["ItemCode"] = "02492";
$default_oea["02492"]["ItemName"] = "千人拼砌風車活動";
$default_oea["02492"]["CatCode"] = "4";
$default_oea["02492"]["CatName"] = "Aesthetic Development";

$default_oea["02586"]["ItemCode"] = "02586";
$default_oea["02586"]["ItemName"] = "午間音樂會";
$default_oea["02586"]["CatCode"] = "4";
$default_oea["02586"]["CatName"] = "Aesthetic Development";

$default_oea["02818"]["ItemCode"] = "02818";
$default_oea["02818"]["ItemName"] = "協青社健康動感校計劃領袖訓練歷奇日營";
$default_oea["02818"]["CatCode"] = "2";
$default_oea["02818"]["CatName"] = "Career-related Experience";

$default_oea["02882"]["ItemCode"] = "02882";
$default_oea["02882"]["ItemName"] = "南區草地滾球推廣日暨聯校比賽";
$default_oea["02882"]["CatCode"] = "3";
$default_oea["02882"]["CatName"] = "Physical Development";

$default_oea["02883"]["ItemCode"] = "02883";
$default_oea["02883"]["ItemName"] = "南華會全港學界田徑錦標賽";
$default_oea["02883"]["CatCode"] = "3";
$default_oea["02883"]["CatName"] = "Physical Development";

$default_oea["03183"]["ItemCode"] = "03183";
$default_oea["03183"]["ItemName"] = "博愛醫院啦啦隊比賽";
$default_oea["03183"]["CatCode"] = "3";
$default_oea["03183"]["CatName"] = "Physical Development";

$default_oea["02588"]["ItemCode"] = "02588";
$default_oea["02588"]["ItemName"] = "友出路師友計劃";
$default_oea["02588"]["CatCode"] = "5";
$default_oea["02588"]["CatName"] = "Moral and Civic Education";

$default_oea["02587"]["ItemCode"] = "02587";
$default_oea["02587"]["ItemName"] = "友心人成長小組";
$default_oea["02587"]["CatCode"] = "5";
$default_oea["02587"]["CatName"] = "Moral and Civic Education";

$default_oea["02590"]["ItemCode"] = "02590";
$default_oea["02590"]["ItemName"] = "友校數學比賽";
$default_oea["02590"]["CatCode"] = "2";
$default_oea["02590"]["CatName"] = "Career-related Experience";

$default_oea["02589"]["ItemCode"] = "02589";
$default_oea["02589"]["ItemName"] = "友校校慶開放活動";
$default_oea["02589"]["CatCode"] = "4";
$default_oea["02589"]["CatName"] = "Aesthetic Development";

$default_oea["02591"]["ItemCode"] = "02591";
$default_oea["02591"]["ItemName"] = "反吸食危害精神毒品名譽會長盃乒乓球比賽";
$default_oea["02591"]["CatCode"] = "3";
$default_oea["02591"]["CatName"] = "Physical Development";

$default_oea["02493"]["ItemCode"] = "02493";
$default_oea["02493"]["ItemName"] = "口腔健康標語海報創作及設計比賽";
$default_oea["02493"]["CatCode"] = "4";
$default_oea["02493"]["CatName"] = "Aesthetic Development";

$default_oea["02494"]["ItemCode"] = "02494";
$default_oea["02494"]["ItemName"] = "口腔衛生海報設計比賽";
$default_oea["02494"]["CatCode"] = "4";
$default_oea["02494"]["CatName"] = "Aesthetic Development";

$default_oea["02495"]["ItemCode"] = "02495";
$default_oea["02495"]["ItemName"] = "口語溝通觀摩比試";
$default_oea["02495"]["CatCode"] = "4";
$default_oea["02495"]["CatName"] = "Aesthetic Development";

$default_oea["02633"]["ItemCode"] = "02633";
$default_oea["02633"]["ItemName"] = "古典新世紀青年管弦樂團表演";
$default_oea["02633"]["CatCode"] = "4";
$default_oea["02633"]["CatName"] = "Aesthetic Development";

$default_oea["02631"]["ItemCode"] = "02631";
$default_oea["02631"]["ItemName"] = "可口可樂全港中小學跳繩比賽";
$default_oea["02631"]["CatCode"] = "3";
$default_oea["02631"]["CatName"] = "Physical Development";

$default_oea["02632"]["ItemCode"] = "02632";
$default_oea["02632"]["ItemName"] = "可持續發展都市設計專題研習比賽";
$default_oea["02632"]["CatCode"] = "4";
$default_oea["02632"]["CatName"] = "Aesthetic Development";

$default_oea["02739"]["ItemCode"] = "02739";
$default_oea["02739"]["ItemName"] = "各界慶祝60周年國慶嘉年華";
$default_oea["02739"]["CatCode"] = "5";
$default_oea["02739"]["CatName"] = "Moral and Civic Education";

$default_oea["02740"]["ItemCode"] = "02740";
$default_oea["02740"]["ItemName"] = "合唱團伴奏-代表學校參加校際音樂比賽";
$default_oea["02740"]["CatCode"] = "3";
$default_oea["02740"]["CatName"] = "Physical Development";

$default_oea["02741"]["ItemCode"] = "02741";
$default_oea["02741"]["ItemName"] = "合唱團副團長-代表學校參加校際音樂比賽";
$default_oea["02741"]["CatCode"] = "3";
$default_oea["02741"]["CatName"] = "Physical Development";

$default_oea["02742"]["ItemCode"] = "02742";
$default_oea["02742"]["ItemName"] = "合唱團團員-代表學校參加校際音樂比賽";
$default_oea["02742"]["CatCode"] = "3";
$default_oea["02742"]["CatName"] = "Physical Development";

$default_oea["02730"]["ItemCode"] = "02730";
$default_oea["02730"]["ItemName"] = "吉野家 Shabu Shabu來襲繪畫比賽";
$default_oea["02730"]["CatCode"] = "3";
$default_oea["02730"]["CatName"] = "Physical Development";

$default_oea["02731"]["ItemCode"] = "02731";
$default_oea["02731"]["ItemName"] = "同一天空下南亞裔共融活動";
$default_oea["02731"]["CatCode"] = "5";
$default_oea["02731"]["CatName"] = "Moral and Civic Education";

$default_oea["02736"]["ItemCode"] = "02736";
$default_oea["02736"]["ItemName"] = "同夢•同心兩岸四地青年學生交流營";
$default_oea["02736"]["CatCode"] = "2";
$default_oea["02736"]["CatName"] = "Career-related Experience";

$default_oea["02737"]["ItemCode"] = "02737";
$default_oea["02737"]["ItemName"] = "同夢同心兩岸四地交流營";
$default_oea["02737"]["CatCode"] = "2";
$default_oea["02737"]["CatName"] = "Career-related Experience";

$default_oea["02732"]["ItemCode"] = "02732";
$default_oea["02732"]["ItemName"] = "同心共印‧我國飛騰─戲劇組";
$default_oea["02732"]["CatCode"] = "4";
$default_oea["02732"]["CatName"] = "Aesthetic Development";

$default_oea["02733"]["ItemCode"] = "02733";
$default_oea["02733"]["ItemName"] = "同心結力抗毒大行動 2010 - 同心結製作";
$default_oea["02733"]["CatCode"] = "4";
$default_oea["02733"]["CatName"] = "Aesthetic Development";

$default_oea["02734"]["ItemCode"] = "02734";
$default_oea["02734"]["ItemName"] = "同根 Teen 地2010 之融會香江計劃";
$default_oea["02734"]["CatCode"] = "5";
$default_oea["02734"]["CatName"] = "Moral and Civic Education";

$default_oea["02735"]["ItemCode"] = "02735";
$default_oea["02735"]["ItemName"] = "同根同心香港初中及高小學生內地交流計畫";
$default_oea["02735"]["CatCode"] = "2";
$default_oea["02735"]["CatName"] = "Career-related Experience";

$default_oea["02738"]["ItemCode"] = "02738";
$default_oea["02738"]["ItemName"] = "吐露港10公里(青少年禁毒盃4公里)";
$default_oea["02738"]["CatCode"] = "3";
$default_oea["02738"]["CatName"] = "Physical Development";

$default_oea["02819"]["ItemCode"] = "02819";
$default_oea["02819"]["ItemName"] = "味不可言飲食藝術創作展覽";
$default_oea["02819"]["CatCode"] = "4";
$default_oea["02819"]["CatName"] = "Aesthetic Development";

$default_oea["02820"]["ItemCode"] = "02820";
$default_oea["02820"]["ItemName"] = "呼吸正氣－達致優質室內空氣質素電腦網頁設計比賽(中學組)";
$default_oea["02820"]["CatCode"] = "4";
$default_oea["02820"]["CatName"] = "Aesthetic Development";

$default_oea["02821"]["ItemCode"] = "02821";
$default_oea["02821"]["ItemName"] = "和富青年講場(觀塘區)";
$default_oea["02821"]["CatCode"] = "5";
$default_oea["02821"]["CatName"] = "Moral and Civic Education";

$default_oea["02822"]["ItemCode"] = "02822";
$default_oea["02822"]["ItemName"] = "和諧校園口號創作比賽 (Please provide details in \"Description\" box.)";
$default_oea["02822"]["CatCode"] = "4";
$default_oea["02822"]["CatName"] = "Aesthetic Development";

$default_oea["02824"]["ItemCode"] = "02824";
$default_oea["02824"]["ItemName"] = "和諧校園四格漫畫創作比賽";
$default_oea["02824"]["CatCode"] = "4";
$default_oea["02824"]["CatName"] = "Aesthetic Development";

$default_oea["02884"]["ItemCode"] = "02884";
$default_oea["02884"]["ItemName"] = "品德教育漫畫創作比賽";
$default_oea["02884"]["CatCode"] = "4";
$default_oea["02884"]["CatName"] = "Aesthetic Development";

$default_oea["03107"]["ItemCode"] = "03107";
$default_oea["03107"]["ItemName"] = "商務印書館中學生讀書報告比賽";
$default_oea["03107"]["CatCode"] = "4";
$default_oea["03107"]["CatName"] = "Aesthetic Development";

$default_oea["03108"]["ItemCode"] = "03108";
$default_oea["03108"]["ItemName"] = "商襄校園親親社群計劃";
$default_oea["03108"]["CatCode"] = "1";
$default_oea["03108"]["CatName"] = "Community Service";

$default_oea["03148"]["ItemCode"] = "03148";
$default_oea["03148"]["ItemName"] = "啟慧中國語文廣播劇創作暨演繹比賽";
$default_oea["03148"]["CatCode"] = "4";
$default_oea["03148"]["CatName"] = "Aesthetic Development";

$default_oea["03225"]["ItemCode"] = "03225";
$default_oea["03225"]["ItemName"] = "善言巧論：全港學生口語溝通大賽";
$default_oea["03225"]["CatCode"] = "4";
$default_oea["03225"]["CatName"] = "Aesthetic Development";

$default_oea["03185"]["ItemCode"] = "03185";
$default_oea["03185"]["ItemName"] = "喇沙會數學比賽";
$default_oea["03185"]["CatCode"] = "2";
$default_oea["03185"]["CatName"] = "Career-related Experience";

$default_oea["03186"]["ItemCode"] = "03186";
$default_oea["03186"]["ItemName"] = "喇沙會聯校數學比賽";
$default_oea["03186"]["CatCode"] = "2";
$default_oea["03186"]["CatName"] = "Career-related Experience";

$default_oea["03184"]["ItemCode"] = "03184";
$default_oea["03184"]["ItemName"] = "喇沙青年會 (Please provide details in \"Description\" box.)";
$default_oea["03184"]["CatCode"] = "1";
$default_oea["03184"]["CatName"] = "Community Service";

$default_oea["03325"]["ItemCode"] = "03325";
$default_oea["03325"]["ItemName"] = "嘉道理開心綠悠遊計劃";
$default_oea["03325"]["CatCode"] = "5";
$default_oea["03325"]["CatName"] = "Moral and Civic Education";

$default_oea["03408"]["ItemCode"] = "03408";
$default_oea["03408"]["ItemName"] = "器械健體班 (第一班)";
$default_oea["03408"]["CatCode"] = "3";
$default_oea["03408"]["CatName"] = "Physical Development";

$default_oea["02634"]["ItemCode"] = "02634";
$default_oea["02634"]["ItemName"] = "四十周年壁畫設計";
$default_oea["02634"]["CatCode"] = "4";
$default_oea["02634"]["CatName"] = "Aesthetic Development";

$default_oea["02635"]["ItemCode"] = "02635";
$default_oea["02635"]["ItemName"] = "四川好人音樂劇-學生專場";
$default_oea["02635"]["CatCode"] = "4";
$default_oea["02635"]["CatName"] = "Aesthetic Development";

$default_oea["02636"]["ItemCode"] = "02636";
$default_oea["02636"]["ItemName"] = "四方杯全國少兒書畫大賽";
$default_oea["02636"]["CatCode"] = "4";
$default_oea["02636"]["CatName"] = "Aesthetic Development";

$default_oea["02825"]["ItemCode"] = "02825";
$default_oea["02825"]["ItemName"] = "固體廢物處理及汽車能源參觀活動";
$default_oea["02825"]["CatCode"] = "5";
$default_oea["02825"]["CatName"] = "Moral and Civic Education";

$default_oea["03111"]["ItemCode"] = "03111";
$default_oea["03111"]["ItemName"] = "國情教育及交流計劃--暑期國情體驗計劃2010頒獎禮";
$default_oea["03111"]["CatCode"] = "2";
$default_oea["03111"]["CatName"] = "Career-related Experience";

$default_oea["03124"]["ItemCode"] = "03124";
$default_oea["03124"]["ItemName"] = "國慶標語創作比賽";
$default_oea["03124"]["CatCode"] = "4";
$default_oea["03124"]["CatName"] = "Aesthetic Development";

$default_oea["03121"]["ItemCode"] = "03121";
$default_oea["03121"]["ItemName"] = "國慶盃三人籃球精英邀請賽";
$default_oea["03121"]["CatCode"] = "3";
$default_oea["03121"]["CatName"] = "Physical Development";

$default_oea["03122"]["ItemCode"] = "03122";
$default_oea["03122"]["ItemName"] = "國慶盃足球賽(中學組)";
$default_oea["03122"]["CatCode"] = "3";
$default_oea["03122"]["CatName"] = "Physical Development";

$default_oea["03123"]["ItemCode"] = "03123";
$default_oea["03123"]["ItemName"] = "國慶節升旗禮司儀兼分享同學";
$default_oea["03123"]["CatCode"] = "5";
$default_oea["03123"]["CatName"] = "Moral and Civic Education";

$default_oea["03112"]["ItemCode"] = "03112";
$default_oea["03112"]["ItemName"] = "國旗飄揚 情繫中華";
$default_oea["03112"]["CatCode"] = "5";
$default_oea["03112"]["CatName"] = "Moral and Civic Education";

$default_oea["03109"]["ItemCode"] = "03109";
$default_oea["03109"]["ItemName"] = "國民教育 (Please provide details in \"Description\" box.) ";
$default_oea["03109"]["CatCode"] = "5";
$default_oea["03109"]["CatName"] = "Moral and Civic Education";

$default_oea["03110"]["ItemCode"] = "03110";
$default_oea["03110"]["ItemName"] = "國民教育領袖培訓課程";
$default_oea["03110"]["CatCode"] = "2";
$default_oea["03110"]["CatName"] = "Career-related Experience";

$default_oea["03113"]["ItemCode"] = "03113";
$default_oea["03113"]["ItemName"] = "國際天文年美術設計創作比賽";
$default_oea["03113"]["CatCode"] = "4";
$default_oea["03113"]["CatName"] = "Aesthetic Development";

$default_oea["03114"]["ItemCode"] = "03114";
$default_oea["03114"]["ItemName"] = "國際少兒藝術節舞蹈比賽";
$default_oea["03114"]["CatCode"] = "4";
$default_oea["03114"]["CatName"] = "Aesthetic Development";

$default_oea["03116"]["ItemCode"] = "03116";
$default_oea["03116"]["ItemName"] = "國際汽車工程師學會模型車設計 工作坊及比賽";
$default_oea["03116"]["CatCode"] = "4";
$default_oea["03116"]["CatName"] = "Aesthetic Development";

$default_oea["03115"]["ItemCode"] = "03115";
$default_oea["03115"]["ItemName"] = "國際生物多樣性年美術設計創作比賽";
$default_oea["03115"]["CatCode"] = "4";
$default_oea["03115"]["CatName"] = "Aesthetic Development";

$default_oea["03118"]["ItemCode"] = "03118";
$default_oea["03118"]["ItemName"] = "國際綜藝合家歡.差不多先生外傳";
$default_oea["03118"]["CatCode"] = "4";
$default_oea["03118"]["CatName"] = "Aesthetic Development";

$default_oea["03117"]["ItemCode"] = "03117";
$default_oea["03117"]["ItemName"] = "國際綜藝合家歡.看到真實";
$default_oea["03117"]["CatCode"] = "4";
$default_oea["03117"]["CatName"] = "Aesthetic Development";

$default_oea["03119"]["ItemCode"] = "03119";
$default_oea["03119"]["ItemName"] = "國際綜藝合家歡《沒有人造衛星相撞的夜空》";
$default_oea["03119"]["CatCode"] = "4";
$default_oea["03119"]["CatName"] = "Aesthetic Development";

$default_oea["03120"]["ItemCode"] = "03120";
$default_oea["03120"]["ItemName"] = "國際舞蹈日";
$default_oea["03120"]["CatCode"] = "4";
$default_oea["03120"]["CatName"] = "Aesthetic Development";

$default_oea["02744"]["ItemCode"] = "02744";
$default_oea["02744"]["ItemName"] = "在晴朗的一TEEN出發：城市歷奇比賽";
$default_oea["02744"]["CatCode"] = "2";
$default_oea["02744"]["CatName"] = "Career-related Experience";

$default_oea["02743"]["ItemCode"] = "02743";
$default_oea["02743"]["ItemName"] = "地理及通識教育科獨立專題探究比賽";
$default_oea["02743"]["CatCode"] = "2";
$default_oea["02743"]["CatName"] = "Career-related Experience";

$default_oea["02885"]["ItemCode"] = "02885";
$default_oea["02885"]["ItemName"] = "城市大學電腦科學系Robocode比賽";
$default_oea["02885"]["CatCode"] = "2";
$default_oea["02885"]["CatName"] = "Career-related Experience";

$default_oea["02886"]["ItemCode"] = "02886";
$default_oea["02886"]["ItemName"] = "城市規劃工作坊";
$default_oea["02886"]["CatCode"] = "2";
$default_oea["02886"]["CatName"] = "Career-related Experience";

$default_oea["02887"]["ItemCode"] = "02887";
$default_oea["02887"]["ItemName"] = "城市論壇";
$default_oea["02887"]["CatCode"] = "5";
$default_oea["02887"]["CatName"] = "Moral and Civic Education";

$default_oea["03127"]["ItemCode"] = "03127";
$default_oea["03127"]["ItemName"] = "培正中學- 《騰飛中國》文物展覽";
$default_oea["03127"]["CatCode"] = "4";
$default_oea["03127"]["CatName"] = "Aesthetic Development";

$default_oea["03125"]["ItemCode"] = "03125";
$default_oea["03125"]["ItemName"] = "基本法大使培訓計劃";
$default_oea["03125"]["CatCode"] = "5";
$default_oea["03125"]["CatName"] = "Moral and Civic Education";

$default_oea["03126"]["ItemCode"] = "03126";
$default_oea["03126"]["ItemName"] = "基本法盃中學辯論比賽";
$default_oea["03126"]["CatCode"] = "5";
$default_oea["03126"]["CatName"] = "Moral and Civic Education";

$default_oea["02637"]["ItemCode"] = "02637";
$default_oea["02637"]["ItemName"] = "外展(聯校)田徑章別挑戰日";
$default_oea["02637"]["CatCode"] = "3";
$default_oea["02637"]["CatName"] = "Physical Development";

$default_oea["02638"]["ItemCode"] = "02638";
$default_oea["02638"]["ItemName"] = "外展教練計劃5人足球比賽";
$default_oea["02638"]["CatCode"] = "3";
$default_oea["02638"]["CatName"] = "Physical Development";

$default_oea["02639"]["ItemCode"] = "02639";
$default_oea["02639"]["ItemName"] = "外展教練計劃排球挑戰賽";
$default_oea["02639"]["CatCode"] = "3";
$default_oea["02639"]["CatName"] = "Physical Development";

$default_oea["02745"]["ItemCode"] = "02745";
$default_oea["02745"]["ItemName"] = "多元文化校園訓練計劃";
$default_oea["02745"]["CatCode"] = "4";
$default_oea["02745"]["CatName"] = "Aesthetic Development";

$default_oea["02746"]["ItemCode"] = "02746";
$default_oea["02746"]["ItemName"] = "多元智能躍進計劃";
$default_oea["02746"]["CatCode"] = "2";
$default_oea["02746"]["CatName"] = "Career-related Experience";

$default_oea["02747"]["ItemCode"] = "02747";
$default_oea["02747"]["ItemName"] = "多媒體教育藝術計劃 (科藝組)";
$default_oea["02747"]["CatCode"] = "4";
$default_oea["02747"]["CatName"] = "Aesthetic Development";

$default_oea["02592"]["ItemCode"] = "02592";
$default_oea["02592"]["ItemName"] = "天水圍元朗區小學機械人種子計劃";
$default_oea["02592"]["CatCode"] = "2";
$default_oea["02592"]["CatName"] = "Career-related Experience";

$default_oea["02593"]["ItemCode"] = "02593";
$default_oea["02593"]["ItemName"] = "天藝盃中國象棋大賽";
$default_oea["02593"]["CatCode"] = "4";
$default_oea["02593"]["CatName"] = "Aesthetic Development";

$default_oea["02594"]["ItemCode"] = "02594";
$default_oea["02594"]["ItemName"] = "太陽能模型車大賽";
$default_oea["02594"]["CatCode"] = "2";
$default_oea["02594"]["CatName"] = "Career-related Experience";

$default_oea["02496"]["ItemCode"] = "02496";
$default_oea["02496"]["ItemName"] = "女童軍宣誓禮";
$default_oea["02496"]["CatCode"] = "1";
$default_oea["02496"]["CatName"] = "Community Service";

$default_oea["02826"]["ItemCode"] = "02826";
$default_oea["02826"]["ItemName"] = "姊妹學校同樂日";
$default_oea["02826"]["CatCode"] = "1";
$default_oea["02826"]["CatName"] = "Community Service";

$default_oea["02888"]["ItemCode"] = "02888";
$default_oea["02888"]["ItemName"] = "威尼斯建築雙年展香港展覽";
$default_oea["02888"]["CatCode"] = "4";
$default_oea["02888"]["CatName"] = "Aesthetic Development";

$default_oea["03128"]["ItemCode"] = "03128";
$default_oea["03128"]["ItemName"] = "婦女庇護中心 - 標誌設計比賽";
$default_oea["03128"]["CatCode"] = "4";
$default_oea["03128"]["CatName"] = "Aesthetic Development";

$default_oea["03412"]["ItemCode"] = "03412";
$default_oea["03412"]["ItemName"] = "學思盃辯論比賽";
$default_oea["03412"]["CatCode"] = "2";
$default_oea["03412"]["CatName"] = "Career-related Experience";

$default_oea["03422"]["ItemCode"] = "03422";
$default_oea["03422"]["ItemName"] = "學校保齡球代表隊";
$default_oea["03422"]["CatCode"] = "3";
$default_oea["03422"]["CatName"] = "Physical Development";

$default_oea["03420"]["ItemCode"] = "03420";
$default_oea["03420"]["ItemName"] = "學校升旗培訓課程";
$default_oea["03420"]["CatCode"] = "5";
$default_oea["03420"]["CatName"] = "Moral and Civic Education";

$default_oea["03424"]["ItemCode"] = "03424";
$default_oea["03424"]["ItemName"] = "學校商界跨接計劃";
$default_oea["03424"]["CatCode"] = "2";
$default_oea["03424"]["CatName"] = "Career-related Experience";

$default_oea["03423"]["ItemCode"] = "03423";
$default_oea["03423"]["ItemName"] = "學校室內賽艇代表隊";
$default_oea["03423"]["CatCode"] = "3";
$default_oea["03423"]["CatName"] = "Physical Development";

$default_oea["03425"]["ItemCode"] = "03425";
$default_oea["03425"]["ItemName"] = "學校教育劇場";
$default_oea["03425"]["CatCode"] = "4";
$default_oea["03425"]["CatName"] = "Aesthetic Development";

$default_oea["03421"]["ItemCode"] = "03421";
$default_oea["03421"]["ItemName"] = "學校文化日教育活動 - 都市中的遺跡";
$default_oea["03421"]["CatCode"] = "4";
$default_oea["03421"]["CatName"] = "Aesthetic Development";

$default_oea["03429"]["ItemCode"] = "03429";
$default_oea["03429"]["ItemName"] = "學校觀鳥網絡: 分享會及頒獎禮";
$default_oea["03429"]["CatCode"] = "4";
$default_oea["03429"]["CatName"] = "Aesthetic Development";

$default_oea["03430"]["ItemCode"] = "03430";
$default_oea["03430"]["ItemName"] = "學校觀鳥網絡: 水鳥普查訓練";
$default_oea["03430"]["CatCode"] = "4";
$default_oea["03430"]["CatName"] = "Aesthetic Development";

$default_oea["03426"]["ItemCode"] = "03426";
$default_oea["03426"]["ItemName"] = "學校越野跑代表隊";
$default_oea["03426"]["CatCode"] = "3";
$default_oea["03426"]["CatName"] = "Physical Development";

$default_oea["03427"]["ItemCode"] = "03427";
$default_oea["03427"]["ItemName"] = "學校體育推廣計劃-外展田徑訓練課程";
$default_oea["03427"]["CatCode"] = "3";
$default_oea["03427"]["CatName"] = "Physical Development";

$default_oea["03428"]["ItemCode"] = "03428";
$default_oea["03428"]["ItemName"] = "學校體育推廣計劃-田徑章別挑戰日";
$default_oea["03428"]["CatCode"] = "3";
$default_oea["03428"]["CatName"] = "Physical Development";

$default_oea["03409"]["ItemCode"] = "03409";
$default_oea["03409"]["ItemName"] = "學生演奏會";
$default_oea["03409"]["CatCode"] = "4";
$default_oea["03409"]["CatName"] = "Aesthetic Development";

$default_oea["03410"]["ItemCode"] = "03410";
$default_oea["03410"]["ItemName"] = "學生環境保護大使培訓工作坊 (Please provide details in \"Description\" box.)";
$default_oea["03410"]["CatCode"] = "5";
$default_oea["03410"]["CatName"] = "Moral and Civic Education";

$default_oea["03413"]["ItemCode"] = "03413";
$default_oea["03413"]["ItemName"] = "學界分區野外定向錦標賽";
$default_oea["03413"]["CatCode"] = "3";
$default_oea["03413"]["CatName"] = "Physical Development";

$default_oea["03415"]["ItemCode"] = "03415";
$default_oea["03415"]["ItemName"] = "學界划艇賽";
$default_oea["03415"]["CatCode"] = "3";
$default_oea["03415"]["CatName"] = "Physical Development";

$default_oea["03416"]["ItemCode"] = "03416";
$default_oea["03416"]["ItemName"] = "學界商界跨接計劃開幕禮及研討會";
$default_oea["03416"]["CatCode"] = "2";
$default_oea["03416"]["CatName"] = "Career-related Experience";

$default_oea["03414"]["ItemCode"] = "03414";
$default_oea["03414"]["ItemName"] = "學界田徑賽";
$default_oea["03414"]["CatCode"] = "3";
$default_oea["03414"]["CatName"] = "Physical Development";

$default_oea["03417"]["ItemCode"] = "03417";
$default_oea["03417"]["ItemName"] = "學界粵語正音大賽";
$default_oea["03417"]["CatCode"] = "4";
$default_oea["03417"]["CatName"] = "Aesthetic Development";

$default_oea["03418"]["ItemCode"] = "03418";
$default_oea["03418"]["ItemName"] = "學界魔術比賽";
$default_oea["03418"]["CatCode"] = "4";
$default_oea["03418"]["CatName"] = "Aesthetic Development";

$default_oea["03419"]["ItemCode"] = "03419";
$default_oea["03419"]["ItemName"] = "學苑盃全港中學生真情作文大賽";
$default_oea["03419"]["CatCode"] = "4";
$default_oea["03419"]["CatName"] = "Aesthetic Development";

$default_oea["03411"]["ItemCode"] = "03411";
$default_oea["03411"]["ItemName"] = "學長盃辯論賽";
$default_oea["03411"]["CatCode"] = "5";
$default_oea["03411"]["CatName"] = "Moral and Civic Education";

$default_oea["02827"]["ItemCode"] = "02827";
$default_oea["02827"]["ItemName"] = "定向 愛 父親節定向比賽";
$default_oea["02827"]["CatCode"] = "3";
$default_oea["02827"]["CatName"] = "Physical Development";

$default_oea["03048"]["ItemCode"] = "03048";
$default_oea["03048"]["ItemName"] = "家‧多點欣賞四格漫畫創作及心聲表達比賽";
$default_oea["03048"]["CatCode"] = "4";
$default_oea["03048"]["CatName"] = "Aesthetic Development";

$default_oea["03049"]["ItemCode"] = "03049";
$default_oea["03049"]["ItemName"] = "家和萬事好心意咭設計比賽";
$default_oea["03049"]["CatCode"] = "4";
$default_oea["03049"]["CatName"] = "Aesthetic Development";

$default_oea["03326"]["ItemCode"] = "03326";
$default_oea["03326"]["ItemName"] = "寧波歌舞團";
$default_oea["03326"]["CatCode"] = "4";
$default_oea["03326"]["CatName"] = "Aesthetic Development";

$default_oea["03371"]["ItemCode"] = "03371";
$default_oea["03371"]["ItemName"] = "寫出我天地徵文比賽中文組別";
$default_oea["03371"]["CatCode"] = "4";
$default_oea["03371"]["CatName"] = "Aesthetic Development";

$default_oea["02497"]["ItemCode"] = "02497";
$default_oea["02497"]["ItemName"] = "寸心大使委任禮暨綠色生活獨立短片創作比賽";
$default_oea["02497"]["CatCode"] = "4";
$default_oea["02497"]["CatName"] = "Aesthetic Development";

$default_oea["02498"]["ItemCode"] = "02498";
$default_oea["02498"]["ItemName"] = "寸心行動獨立短片比賽頒獎禮";
$default_oea["02498"]["CatCode"] = "4";
$default_oea["02498"]["CatName"] = "Aesthetic Development";

$default_oea["03187"]["ItemCode"] = "03187";
$default_oea["03187"]["ItemName"] = "尊師重道好少年選舉";
$default_oea["03187"]["CatCode"] = "5";
$default_oea["03187"]["CatName"] = "Moral and Civic Education";

$default_oea["03188"]["ItemCode"] = "03188";
$default_oea["03188"]["ItemName"] = "尋找敬師的故事徵文比賽";
$default_oea["03188"]["CatCode"] = "4";
$default_oea["03188"]["CatName"] = "Aesthetic Development";

$default_oea["02503"]["ItemCode"] = "02503";
$default_oea["02503"]["ItemName"] = "小棋聖盃全港學界棋藝挑戰賽";
$default_oea["02503"]["CatCode"] = "4";
$default_oea["02503"]["CatName"] = "Aesthetic Development";

$default_oea["02500"]["ItemCode"] = "02500";
$default_oea["02500"]["ItemName"] = "小玩意，大創意玩具設計比賽";
$default_oea["02500"]["CatCode"] = "4";
$default_oea["02500"]["CatName"] = "Aesthetic Development";

$default_oea["02502"]["ItemCode"] = "02502";
$default_oea["02502"]["ItemName"] = "小荷風采全國少兒舞蹈展演";
$default_oea["02502"]["CatCode"] = "4";
$default_oea["02502"]["CatName"] = "Aesthetic Development";

$default_oea["02501"]["ItemCode"] = "02501";
$default_oea["02501"]["ItemName"] = "小記者活動";
$default_oea["02501"]["CatCode"] = "2";
$default_oea["02501"]["CatName"] = "Career-related Experience";

$default_oea["02499"]["ItemCode"] = "02499";
$default_oea["02499"]["ItemName"] = "小豆苗愛心計劃";
$default_oea["02499"]["CatCode"] = "1";
$default_oea["02499"]["CatName"] = "Community Service";

$default_oea["02595"]["ItemCode"] = "02595";
$default_oea["02595"]["ItemName"] = "少年企業家大比拼";
$default_oea["02595"]["CatCode"] = "2";
$default_oea["02595"]["CatName"] = "Career-related Experience";

$default_oea["02596"]["ItemCode"] = "02596";
$default_oea["02596"]["ItemName"] = "少年警訊學校支會滅罪專題報告比賽";
$default_oea["02596"]["CatCode"] = "4";
$default_oea["02596"]["CatName"] = "Aesthetic Development";

$default_oea["02598"]["ItemCode"] = "02598";
$default_oea["02598"]["ItemName"] = "少林武術訓練營";
$default_oea["02598"]["CatCode"] = "3";
$default_oea["02598"]["CatName"] = "Physical Development";

$default_oea["02597"]["ItemCode"] = "02597";
$default_oea["02597"]["ItemName"] = "少林武術訓練班";
$default_oea["02597"]["CatCode"] = "3";
$default_oea["02597"]["CatName"] = "Physical Development";

$default_oea["02829"]["ItemCode"] = "02829";
$default_oea["02829"]["ItemName"] = "屈臣氏蒸餾水香港青少年精英田徑錦標賽";
$default_oea["02829"]["CatCode"] = "3";
$default_oea["02829"]["CatName"] = "Physical Development";

$default_oea["02830"]["ItemCode"] = "02830";
$default_oea["02830"]["ItemName"] = "屈臣氏蒸餾水香港青年分齡田徑賽及公開賽";
$default_oea["02830"]["CatCode"] = "3";
$default_oea["02830"]["CatName"] = "Physical Development";

$default_oea["02828"]["ItemCode"] = "02828";
$default_oea["02828"]["ItemName"] = "屈臣氏集團香港學生運動員獎";
$default_oea["02828"]["CatCode"] = "3";
$default_oea["02828"]["CatName"] = "Physical Development";

$default_oea["02599"]["ItemCode"] = "02599";
$default_oea["02599"]["ItemName"] = "屯門區校際初中數學比賽";
$default_oea["02599"]["CatCode"] = "2";
$default_oea["02599"]["CatName"] = "Career-related Experience";

$default_oea["02600"]["ItemCode"] = "02600";
$default_oea["02600"]["ItemName"] = "屯門康樂體育中心射箭比賽";
$default_oea["02600"]["CatCode"] = "3";
$default_oea["02600"]["CatName"] = "Physical Development";

$default_oea["02504"]["ItemCode"] = "02504";
$default_oea["02504"]["ItemName"] = "山林保育大挑戰主辦：香港攀山總會";
$default_oea["02504"]["CatCode"] = "3";
$default_oea["02504"]["CatName"] = "Physical Development";

$default_oea["03129"]["ItemCode"] = "03129";
$default_oea["03129"]["ItemName"] = "崑曲名家張繼青大師講座及折子戲欣賞";
$default_oea["03129"]["CatCode"] = "4";
$default_oea["03129"]["CatName"] = "Aesthetic Development";

$default_oea["03247"]["ItemCode"] = "03247";
$default_oea["03247"]["ItemName"] = "嵩山少林武術示演";
$default_oea["03247"]["CatCode"] = "3";
$default_oea["03247"]["CatName"] = "Physical Development";

$default_oea["02505"]["ItemCode"] = "02505";
$default_oea["02505"]["ItemName"] = "工展會優質生活創建更好明天攝影比賽";
$default_oea["02505"]["CatCode"] = "4";
$default_oea["02505"]["CatName"] = "Aesthetic Development";

$default_oea["02770"]["ItemCode"] = "02770";
$default_oea["02770"]["ItemName"] = "希望號奧運星搭載方案徵集活動";
$default_oea["02770"]["CatCode"] = "2";
$default_oea["02770"]["CatName"] = "Career-related Experience";

$default_oea["03051"]["ItemCode"] = "03051";
$default_oea["03051"]["ItemName"] = "師生伴讀計劃(到何東小學分享閱讀心得)";
$default_oea["03051"]["CatCode"] = "4";
$default_oea["03051"]["CatName"] = "Aesthetic Development";

$default_oea["03050"]["ItemCode"] = "03050";
$default_oea["03050"]["ItemName"] = "師生同樂日";
$default_oea["03050"]["CatCode"] = "4";
$default_oea["03050"]["CatName"] = "Aesthetic Development";

$default_oea["02748"]["ItemCode"] = "02748";
$default_oea["02748"]["ItemName"] = "年度聯校沙灘排球訓練課程";
$default_oea["02748"]["CatCode"] = "3";
$default_oea["02748"]["CatName"] = "Physical Development";

$default_oea["02749"]["ItemCode"] = "02749";
$default_oea["02749"]["ItemName"] = "年輕人藝術展才華";
$default_oea["02749"]["CatCode"] = "4";
$default_oea["02749"]["CatName"] = "Aesthetic Development";

$default_oea["03130"]["ItemCode"] = "03130";
$default_oea["03130"]["ItemName"] = "康文署戲劇培訓並公開演出";
$default_oea["03130"]["CatCode"] = "4";
$default_oea["03130"]["CatName"] = "Aesthetic Development";

$default_oea["03248"]["ItemCode"] = "03248";
$default_oea["03248"]["ItemName"] = "廉政互動劇場";
$default_oea["03248"]["CatCode"] = "4";
$default_oea["03248"]["CatName"] = "Aesthetic Development";

$default_oea["03249"]["ItemCode"] = "03249";
$default_oea["03249"]["ItemName"] = "廉政公署iTeen記者計劃";
$default_oea["03249"]["CatCode"] = "5";
$default_oea["03249"]["CatName"] = "Moral and Civic Education";

$default_oea["03372"]["ItemCode"] = "03372";
$default_oea["03372"]["ItemName"] = "廚餘化春泥計劃";
$default_oea["03372"]["CatCode"] = "5";
$default_oea["03372"]["CatName"] = "Moral and Civic Education";

$default_oea["03373"]["ItemCode"] = "03373";
$default_oea["03373"]["ItemName"] = "廣東省政協第七屆四洲盃 (粵港澳粵曲演唱大賽組委會)";
$default_oea["03373"]["CatCode"] = "4";
$default_oea["03373"]["CatName"] = "Aesthetic Development";

$default_oea["03531"]["ItemCode"] = "03531";
$default_oea["03531"]["ItemName"] = "廸士尼青少年學習系列";
$default_oea["03531"]["CatCode"] = "2";
$default_oea["03531"]["CatName"] = "Career-related Experience";

$default_oea["02890"]["ItemCode"] = "02890";
$default_oea["02890"]["ItemName"] = "建築環境與可持續發展";
$default_oea["02890"]["CatCode"] = "2";
$default_oea["02890"]["CatName"] = "Career-related Experience";

$default_oea["02889"]["ItemCode"] = "02889";
$default_oea["02889"]["ItemName"] = "建築素描寫意日比賽";
$default_oea["02889"]["CatCode"] = "4";
$default_oea["02889"]["CatName"] = "Aesthetic Development";

$default_oea["02640"]["ItemCode"] = "02640";
$default_oea["02640"]["ItemName"] = "弘揚孝道文化系列活動- 我最孝敬的人徵文比賽";
$default_oea["02640"]["CatCode"] = "4";
$default_oea["02640"]["CatName"] = "Aesthetic Development";

$default_oea["02831"]["ItemCode"] = "02831";
$default_oea["02831"]["ItemName"] = "弦樂團";
$default_oea["02831"]["CatCode"] = "4";
$default_oea["02831"]["CatName"] = "Aesthetic Development";

$default_oea["02832"]["ItemCode"] = "02832";
$default_oea["02832"]["ItemName"] = "弦韻傳情樂展能(音樂會)";
$default_oea["02832"]["CatCode"] = "4";
$default_oea["02832"]["CatName"] = "Aesthetic Development";

$default_oea["03131"]["ItemCode"] = "03131";
$default_oea["03131"]["ItemName"] = "彩虹戲劇培訓計劃";
$default_oea["03131"]["CatCode"] = "4";
$default_oea["03131"]["CatName"] = "Aesthetic Development";

$default_oea["03052"]["ItemCode"] = "03052";
$default_oea["03052"]["ItemName"] = "徐悲鴻盃國際青少年兒童美術比賽";
$default_oea["03052"]["CatCode"] = "4";
$default_oea["03052"]["CatName"] = "Aesthetic Development";

$default_oea["03135"]["ItemCode"] = "03135";
$default_oea["03135"]["ItemName"] = "從學習到服務 - 關顧貧窮 - 足球友誼賽";
$default_oea["03135"]["CatCode"] = "3";
$default_oea["03135"]["CatName"] = "Physical Development";

$default_oea["03132"]["ItemCode"] = "03132";
$default_oea["03132"]["ItemName"] = "從文本到舞台";
$default_oea["03132"]["CatCode"] = "4";
$default_oea["03132"]["CatName"] = "Aesthetic Development";

$default_oea["03133"]["ItemCode"] = "03133";
$default_oea["03133"]["ItemName"] = "從有教無類到素質教育 -山東古今的人才培訓";
$default_oea["03133"]["CatCode"] = "4";
$default_oea["03133"]["CatName"] = "Aesthetic Development";

$default_oea["03134"]["ItemCode"] = "03134";
$default_oea["03134"]["ItemName"] = "從辛亥革命看百年中國 - 音樂劇《旭日》";
$default_oea["03134"]["CatCode"] = "4";
$default_oea["03134"]["CatName"] = "Aesthetic Development";

$default_oea["03189"]["ItemCode"] = "03189";
$default_oea["03189"]["ItemName"] = "循道衛理楊震社會服務處何文田青少年綜合發展中心Smart Youth 跳出我天地";
$default_oea["03189"]["CatCode"] = "3";
$default_oea["03189"]["CatName"] = "Physical Development";

$default_oea["03374"]["ItemCode"] = "03374";
$default_oea["03374"]["ItemName"] = "德育劇場好事大追查";
$default_oea["03374"]["CatCode"] = "5";
$default_oea["03374"]["CatName"] = "Moral and Civic Education";

$default_oea["02603"]["ItemCode"] = "02603";
$default_oea["02603"]["ItemName"] = "心繫屯元徵文比賽";
$default_oea["02603"]["CatCode"] = "4";
$default_oea["02603"]["CatName"] = "Aesthetic Development";

$default_oea["02601"]["ItemCode"] = "02601";
$default_oea["02601"]["ItemName"] = "心肺復甦齊齊操 - 世界紀錄齊創造";
$default_oea["02601"]["CatCode"] = "3";
$default_oea["02601"]["CatName"] = "Physical Development";

$default_oea["02602"]["ItemCode"] = "02602";
$default_oea["02602"]["ItemName"] = "心誠盃射箭比賽";
$default_oea["02602"]["CatCode"] = "3";
$default_oea["02602"]["CatName"] = "Physical Development";

$default_oea["02604"]["ItemCode"] = "02604";
$default_oea["02604"]["ItemName"] = "心靈銀行大樓設計比賽";
$default_oea["02604"]["CatCode"] = "4";
$default_oea["02604"]["CatName"] = "Aesthetic Development";

$default_oea["02771"]["ItemCode"] = "02771";
$default_oea["02771"]["ItemName"] = "快樂好蒔田插秧佃和有機導賞活動";
$default_oea["02771"]["CatCode"] = "4";
$default_oea["02771"]["CatName"] = "Aesthetic Development";

$default_oea["02891"]["ItemCode"] = "02891";
$default_oea["02891"]["ItemName"] = "思定劇社";
$default_oea["02891"]["CatCode"] = "4";
$default_oea["02891"]["CatName"] = "Aesthetic Development";

$default_oea["02892"]["ItemCode"] = "02892";
$default_oea["02892"]["ItemName"] = "思高盃划艇賽";
$default_oea["02892"]["CatCode"] = "3";
$default_oea["02892"]["CatName"] = "Physical Development";

$default_oea["02893"]["ItemCode"] = "02893";
$default_oea["02893"]["ItemName"] = "恆生乒乓球學院章別獎勵計劃";
$default_oea["02893"]["CatCode"] = "3";
$default_oea["02893"]["CatName"] = "Physical Development";

$default_oea["03053"]["ItemCode"] = "03053";
$default_oea["03053"]["ItemName"] = "悅讀大本營";
$default_oea["03053"]["CatCode"] = "4";
$default_oea["03053"]["CatName"] = "Aesthetic Development";

$default_oea["03136"]["ItemCode"] = "03136";
$default_oea["03136"]["ItemName"] = "情牽父親顯愛心";
$default_oea["03136"]["CatCode"] = "4";
$default_oea["03136"]["CatName"] = "Aesthetic Development";

$default_oea["03137"]["ItemCode"] = "03137";
$default_oea["03137"]["ItemName"] = "情緒管理工作坊";
$default_oea["03137"]["CatCode"] = "4";
$default_oea["03137"]["CatName"] = "Aesthetic Development";

$default_oea["03138"]["ItemCode"] = "03138";
$default_oea["03138"]["ItemName"] = "情繫中國心寧、滬、港三地交流計劃";
$default_oea["03138"]["CatCode"] = "4";
$default_oea["03138"]["CatName"] = "Aesthetic Development";

$default_oea["03139"]["ItemCode"] = "03139";
$default_oea["03139"]["ItemName"] = "情繫城鄉 - 青年國內農村生活及交流體驗計劃";
$default_oea["03139"]["CatCode"] = "4";
$default_oea["03139"]["CatName"] = "Aesthetic Development";

$default_oea["03140"]["ItemCode"] = "03140";
$default_oea["03140"]["ItemName"] = "情繫黃土心嘉年華";
$default_oea["03140"]["CatCode"] = "4";
$default_oea["03140"]["CatName"] = "Aesthetic Development";

$default_oea["03260"]["ItemCode"] = "03260";
$default_oea["03260"]["ItemName"] = "愛‧和平行動2009我的成長故事徵文比賽";
$default_oea["03260"]["CatCode"] = "4";
$default_oea["03260"]["CatName"] = "Aesthetic Development";

$default_oea["03253"]["ItemCode"] = "03253";
$default_oea["03253"]["ItemName"] = "愛‧女孩攝影比賽";
$default_oea["03253"]["CatCode"] = "4";
$default_oea["03253"]["CatName"] = "Aesthetic Development";

$default_oea["03262"]["ItemCode"] = "03262";
$default_oea["03262"]["ItemName"] = "愛‧無界限繪畫比賽";
$default_oea["03262"]["CatCode"] = "4";
$default_oea["03262"]["CatName"] = "Aesthetic Development";

$default_oea["03261"]["ItemCode"] = "03261";
$default_oea["03261"]["ItemName"] = "愛家庭‧建社區家愛沙田壁畫創作比賽";
$default_oea["03261"]["CatCode"] = "4";
$default_oea["03261"]["CatName"] = "Aesthetic Development";

$default_oea["03255"]["ItemCode"] = "03255";
$default_oea["03255"]["ItemName"] = "愛心傳遞聖誕鹿車設計比賽暨愛心傳遞鹿車大暴走耐力賽";
$default_oea["03255"]["CatCode"] = "4";
$default_oea["03255"]["CatName"] = "Aesthetic Development";

$default_oea["03257"]["ItemCode"] = "03257";
$default_oea["03257"]["ItemName"] = "愛心滿鄉郊";
$default_oea["03257"]["CatCode"] = "4";
$default_oea["03257"]["CatName"] = "Aesthetic Development";

$default_oea["03256"]["ItemCode"] = "03256";
$default_oea["03256"]["ItemName"] = "愛心聖誕咭設計比賽";
$default_oea["03256"]["CatCode"] = "4";
$default_oea["03256"]["CatName"] = "Aesthetic Development";

$default_oea["03254"]["ItemCode"] = "03254";
$default_oea["03254"]["ItemName"] = "愛心送橽賀端陽";
$default_oea["03254"]["CatCode"] = "1";
$default_oea["03254"]["CatName"] = "Community Service";

$default_oea["03258"]["ItemCode"] = "03258";
$default_oea["03258"]["ItemName"] = "愛我中華第三屆青少年朗誦音樂藝術節";
$default_oea["03258"]["CatCode"] = "4";
$default_oea["03258"]["CatName"] = "Aesthetic Development";

$default_oea["03259"]["ItemCode"] = "03259";
$default_oea["03259"]["ItemName"] = "愛里有你 - 健康和諧愛心邨 (Please provide details in \"Description\" box.)";
$default_oea["03259"]["CatCode"] = "1";
$default_oea["03259"]["CatName"] = "Community Service";

$default_oea["03252"]["ItemCode"] = "03252";
$default_oea["03252"]["ItemName"] = "愛．中西區攝影比賽";
$default_oea["03252"]["CatCode"] = "4";
$default_oea["03252"]["CatName"] = "Aesthetic Development";

$default_oea["03250"]["ItemCode"] = "03250";
$default_oea["03250"]["ItemName"] = "慈青日";
$default_oea["03250"]["CatCode"] = "1";
$default_oea["03250"]["CatName"] = "Community Service";

$default_oea["03251"]["ItemCode"] = "03251";
$default_oea["03251"]["ItemName"] = "慈青曙暉信仰生活團";
$default_oea["03251"]["CatCode"] = "5";
$default_oea["03251"]["CatName"] = "Moral and Civic Education";

$default_oea["03375"]["ItemCode"] = "03375";
$default_oea["03375"]["ItemName"] = "慶祝中華人民共和國成立六十週年徵文比賽";
$default_oea["03375"]["CatCode"] = "4";
$default_oea["03375"]["CatName"] = "Aesthetic Development";

$default_oea["03376"]["ItemCode"] = "03376";
$default_oea["03376"]["ItemName"] = "慶祝建國六十周年全港中學生攝影比賽";
$default_oea["03376"]["CatCode"] = "4";
$default_oea["03376"]["CatName"] = "Aesthetic Development";

$default_oea["03380"]["ItemCode"] = "03380";
$default_oea["03380"]["ItemName"] = "慶祝新中國成立60周年中學生攝影比賽";
$default_oea["03380"]["CatCode"] = "4";
$default_oea["03380"]["CatName"] = "Aesthetic Development";

$default_oea["03381"]["ItemCode"] = "03381";
$default_oea["03381"]["ItemName"] = "慶祝離島節長洲太平清醮獲提名非物質文化遺產嘉年華會";
$default_oea["03381"]["CatCode"] = "4";
$default_oea["03381"]["CatName"] = "Aesthetic Development";

$default_oea["03377"]["ItemCode"] = "03377";
$default_oea["03377"]["ItemName"] = "慶祝香港回歸十周年中小學生慈善徵文比賽";
$default_oea["03377"]["CatCode"] = "4";
$default_oea["03377"]["CatName"] = "Aesthetic Development";

$default_oea["03378"]["ItemCode"] = "03378";
$default_oea["03378"]["ItemName"] = "慶祝香港回歸十周年中港澳劍擊邀請賽";
$default_oea["03378"]["CatCode"] = "3";
$default_oea["03378"]["CatName"] = "Physical Development";

$default_oea["03379"]["ItemCode"] = "03379";
$default_oea["03379"]["ItemName"] = "慶祝香港回歸祖國13周年泰山大型民族交響音畫";
$default_oea["03379"]["CatCode"] = "4";
$default_oea["03379"]["CatName"] = "Aesthetic Development";

$default_oea["03451"]["ItemCode"] = "03451";
$default_oea["03451"]["ItemName"] = "應用學習: 健康護理實務導引課程";
$default_oea["03451"]["CatCode"] = "4";
$default_oea["03451"]["CatName"] = "Aesthetic Development";

$default_oea["03450"]["ItemCode"] = "03450";
$default_oea["03450"]["ItemName"] = "應用學習:　多媒體科藝導引課程";
$default_oea["03450"]["CatCode"] = "4";
$default_oea["03450"]["CatName"] = "Aesthetic Development";

$default_oea["03452"]["ItemCode"] = "03452";
$default_oea["03452"]["ItemName"] = "應用學習導引課程: 美容學";
$default_oea["03452"]["CatCode"] = "4";
$default_oea["03452"]["CatName"] = "Aesthetic Development";

$default_oea["03513"]["ItemCode"] = "03513";
$default_oea["03513"]["ItemName"] = "戀愛總是平靜地意外身亡音樂劇";
$default_oea["03513"]["CatCode"] = "4";
$default_oea["03513"]["CatName"] = "Aesthetic Development";

$default_oea["02777"]["ItemCode"] = "02777";
$default_oea["02777"]["ItemName"] = "我們的驕傲-國旗飄揚、情繫中華圖片展覽";
$default_oea["02777"]["CatCode"] = "5";
$default_oea["02777"]["CatName"] = "Moral and Civic Education";

$default_oea["02780"]["ItemCode"] = "02780";
$default_oea["02780"]["ItemName"] = "我愛香港少年網徵文比賽";
$default_oea["02780"]["CatCode"] = "4";
$default_oea["02780"]["CatName"] = "Aesthetic Development";

$default_oea["02773"]["ItemCode"] = "02773";
$default_oea["02773"]["ItemName"] = "我的三城記：上海、香港、新加坡 (歷史講座)";
$default_oea["02773"]["CatCode"] = "4";
$default_oea["02773"]["CatName"] = "Aesthetic Development";

$default_oea["02774"]["ItemCode"] = "02774";
$default_oea["02774"]["ItemName"] = "我的世界—文化藝術認識及可持續發展的反思";
$default_oea["02774"]["CatCode"] = "4";
$default_oea["02774"]["CatName"] = "Aesthetic Development";

$default_oea["02775"]["ItemCode"] = "02775";
$default_oea["02775"]["ItemName"] = "我的學習目標";
$default_oea["02775"]["CatCode"] = "2";
$default_oea["02775"]["CatName"] = "Career-related Experience";

$default_oea["02776"]["ItemCode"] = "02776";
$default_oea["02776"]["ItemName"] = "我的警隊生涯";
$default_oea["02776"]["CatCode"] = "2";
$default_oea["02776"]["CatName"] = "Career-related Experience";

$default_oea["02778"]["ItemCode"] = "02778";
$default_oea["02778"]["ItemName"] = "我眼中的威爾斯親王醫院繪畫比賽";
$default_oea["02778"]["CatCode"] = "4";
$default_oea["02778"]["CatName"] = "Aesthetic Development";

$default_oea["02779"]["ItemCode"] = "02779";
$default_oea["02779"]["ItemName"] = "我眼中的濕地中學校際專題報道比賽";
$default_oea["02779"]["CatCode"] = "4";
$default_oea["02779"]["CatName"] = "Aesthetic Development";

$default_oea["02772"]["ItemCode"] = "02772";
$default_oea["02772"]["ItemName"] = "戒賭奇想漫畫創作比賽";
$default_oea["02772"]["CatCode"] = "4";
$default_oea["02772"]["CatName"] = "Aesthetic Development";

$default_oea["03453"]["ItemCode"] = "03453";
$default_oea["03453"]["ItemName"] = "戲劇欣賞及工作坊";
$default_oea["03453"]["CatCode"] = "4";
$default_oea["03453"]["CatName"] = "Aesthetic Development";

$default_oea["03455"]["ItemCode"] = "03455";
$default_oea["03455"]["ItemName"] = "戲劇藝術教育課程";
$default_oea["03455"]["CatCode"] = "4";
$default_oea["03455"]["CatName"] = "Aesthetic Development";

$default_oea["03454"]["ItemCode"] = "03454";
$default_oea["03454"]["ItemName"] = "戲劇表演工作坊";
$default_oea["03454"]["CatCode"] = "4";
$default_oea["03454"]["CatName"] = "Aesthetic Development";

$default_oea["02605"]["ItemCode"] = "02605";
$default_oea["02605"]["ItemName"] = "戶外活動日";
$default_oea["02605"]["CatCode"] = "4";
$default_oea["02605"]["CatName"] = "Aesthetic Development";

$default_oea["02606"]["ItemCode"] = "02606";
$default_oea["02606"]["ItemName"] = "手語中級課程/ 工作坊";
$default_oea["02606"]["CatCode"] = "2";
$default_oea["02606"]["CatName"] = "Career-related Experience";

$default_oea["02506"]["ItemCode"] = "02506";
$default_oea["02506"]["ItemName"] = "才華盡展樂滿FUN";
$default_oea["02506"]["CatCode"] = "4";
$default_oea["02506"]["CatName"] = "Aesthetic Development";

$default_oea["02787"]["ItemCode"] = "02787";
$default_oea["02787"]["ItemName"] = "扶輪環保盃辯論比賽";
$default_oea["02787"]["CatCode"] = "2";
$default_oea["02787"]["CatName"] = "Career-related Experience";

$default_oea["02785"]["ItemCode"] = "02785";
$default_oea["02785"]["ItemName"] = "扶輪社-標誌設計比賽";
$default_oea["02785"]["CatCode"] = "4";
$default_oea["02785"]["CatName"] = "Aesthetic Development";

$default_oea["02786"]["ItemCode"] = "02786";
$default_oea["02786"]["ItemName"] = "扶輪話劇比賽2011";
$default_oea["02786"]["CatCode"] = "4";
$default_oea["02786"]["CatName"] = "Aesthetic Development";

$default_oea["02788"]["ItemCode"] = "02788";
$default_oea["02788"]["ItemName"] = "批判性思維訓練";
$default_oea["02788"]["CatCode"] = "2";
$default_oea["02788"]["CatName"] = "Career-related Experience";

$default_oea["02781"]["ItemCode"] = "02781";
$default_oea["02781"]["ItemName"] = "抗日戰爭中國遠征軍戰士分享會";
$default_oea["02781"]["CatCode"] = "5";
$default_oea["02781"]["CatName"] = "Moral and Civic Education";

$default_oea["02784"]["ItemCode"] = "02784";
$default_oea["02784"]["ItemName"] = "抗毒宣傳創作室壁佈比賽";
$default_oea["02784"]["CatCode"] = "4";
$default_oea["02784"]["CatName"] = "Aesthetic Development";

$default_oea["02783"]["ItemCode"] = "02783";
$default_oea["02783"]["ItemName"] = "抗毒抗壓我做得到小組";
$default_oea["02783"]["CatCode"] = "5";
$default_oea["02783"]["CatName"] = "Moral and Civic Education";

$default_oea["02782"]["ItemCode"] = "02782";
$default_oea["02782"]["ItemName"] = "抗毒行動標語創作比賽";
$default_oea["02782"]["CatCode"] = "4";
$default_oea["02782"]["CatName"] = "Aesthetic Development";

$default_oea["02833"]["ItemCode"] = "02833";
$default_oea["02833"]["ItemName"] = "拉闊天空全接觸 - 校園和諧大使計劃";
$default_oea["02833"]["CatCode"] = "5";
$default_oea["02833"]["CatName"] = "Moral and Civic Education";

$default_oea["02894"]["ItemCode"] = "02894";
$default_oea["02894"]["ItemName"] = "挑戰海陸空：獨木舟一星章/二星章訓練課程";
$default_oea["02894"]["CatCode"] = "3";
$default_oea["02894"]["CatName"] = "Physical Development";

$default_oea["03054"]["ItemCode"] = "03054";
$default_oea["03054"]["ItemName"] = "捕捉永恆攝影比賽";
$default_oea["03054"]["CatCode"] = "4";
$default_oea["03054"]["CatName"] = "Aesthetic Development";

$default_oea["03142"]["ItemCode"] = "03142";
$default_oea["03142"]["ItemName"] = "接受香港電台訪問(長春社打穀機設計比賽冠軍)";
$default_oea["03142"]["CatCode"] = "2";
$default_oea["03142"]["CatName"] = "Career-related Experience";

$default_oea["03143"]["ItemCode"] = "03143";
$default_oea["03143"]["ItemName"] = "接待台灣六龜山地育幼院合唱團交流活動";
$default_oea["03143"]["CatCode"] = "4";
$default_oea["03143"]["CatName"] = "Aesthetic Development";

$default_oea["03141"]["ItemCode"] = "03141";
$default_oea["03141"]["ItemName"] = "控球在手欖球訓練";
$default_oea["03141"]["CatCode"] = "3";
$default_oea["03141"]["CatName"] = "Physical Development";

$default_oea["03144"]["ItemCode"] = "03144";
$default_oea["03144"]["ItemName"] = "推己人為環保 敬老扶幼建和諧文件夾封面設計比賽";
$default_oea["03144"]["CatCode"] = "4";
$default_oea["03144"]["CatName"] = "Aesthetic Development";

$default_oea["03190"]["ItemCode"] = "03190";
$default_oea["03190"]["ItemName"] = "揭開新一頁之全港原子筆中文書法比賽";
$default_oea["03190"]["CatCode"] = "4";
$default_oea["03190"]["CatName"] = "Aesthetic Development";

$default_oea["03383"]["ItemCode"] = "03383";
$default_oea["03383"]["ItemName"] = "撰寫影評技巧交流會(中國電影奇妙之旅放映會之後繼活動)";
$default_oea["03383"]["CatCode"] = "4";
$default_oea["03383"]["CatName"] = "Aesthetic Development";

$default_oea["03382"]["ItemCode"] = "03382";
$default_oea["03382"]["ItemName"] = "撰寫自述文章工作坊";
$default_oea["03382"]["CatCode"] = "4";
$default_oea["03382"]["CatName"] = "Aesthetic Development";

$default_oea["03495"]["ItemCode"] = "03495";
$default_oea["03495"]["ItemName"] = "攀登運動隊";
$default_oea["03495"]["CatCode"] = "3";
$default_oea["03495"]["CatName"] = "Physical Development";

$default_oea["03509"]["ItemCode"] = "03509";
$default_oea["03509"]["ItemName"] = "攜手躍動青年領袖訓練計劃";
$default_oea["03509"]["CatCode"] = "2";
$default_oea["03509"]["CatName"] = "Career-related Experience";

$default_oea["02607"]["ItemCode"] = "02607";
$default_oea["02607"]["ItemName"] = "支聯會-六四燭光晚會";
$default_oea["02607"]["CatCode"] = "5";
$default_oea["02607"]["CatName"] = "Moral and Civic Education";

$default_oea["02789"]["ItemCode"] = "02789";
$default_oea["02789"]["ItemName"] = "改革開放三十年展覽";
$default_oea["02789"]["CatCode"] = "5";
$default_oea["02789"]["CatName"] = "Moral and Civic Education";

$default_oea["02895"]["ItemCode"] = "02895";
$default_oea["02895"]["ItemName"] = "政經中國全港中學辯論比賽";
$default_oea["02895"]["CatCode"] = "5";
$default_oea["02895"]["CatName"] = "Moral and Civic Education";

$default_oea["03145"]["ItemCode"] = "03145";
$default_oea["03145"]["ItemName"] = "教育局九龍城區公益少年團主辦電影欣賞會暨徵文比賽";
$default_oea["03145"]["CatCode"] = "4";
$default_oea["03145"]["CatName"] = "Aesthetic Development";

$default_oea["03146"]["ItemCode"] = "03146";
$default_oea["03146"]["ItemName"] = "教育局公益少年團北區委員會2010-2011年度年終頒獎典禮";
$default_oea["03146"]["CatCode"] = "1";
$default_oea["03146"]["CatName"] = "Community Service";

$default_oea["03147"]["ItemCode"] = "03147";
$default_oea["03147"]["ItemName"] = "教育局課程發展處資優教育組馮漢柱資優教育中心增益試驗課程";
$default_oea["03147"]["CatCode"] = "2";
$default_oea["03147"]["CatName"] = "Career-related Experience";

$default_oea["03390"]["ItemCode"] = "03390";
$default_oea["03390"]["ItemName"] = "數學競賽培訓工作坊 - 幾何作圖";
$default_oea["03390"]["CatCode"] = "4";
$default_oea["03390"]["CatName"] = "Aesthetic Development";

$default_oea["03389"]["ItemCode"] = "03389";
$default_oea["03389"]["ItemName"] = "數學聯賽";
$default_oea["03389"]["CatCode"] = "2";
$default_oea["03389"]["CatName"] = "Career-related Experience";

$default_oea["03388"]["ItemCode"] = "03388";
$default_oea["03388"]["ItemName"] = "數學週";
$default_oea["03388"]["CatCode"] = "2";
$default_oea["03388"]["CatName"] = "Career-related Experience";

$default_oea["03384"]["ItemCode"] = "03384";
$default_oea["03384"]["ItemName"] = "數理優才育苗計劃";
$default_oea["03384"]["CatCode"] = "2";
$default_oea["03384"]["CatName"] = "Career-related Experience";

$default_oea["03386"]["ItemCode"] = "03386";
$default_oea["03386"]["ItemName"] = "數碼影音校園計劃 ─ 學界影片製作比賽";
$default_oea["03386"]["CatCode"] = "4";
$default_oea["03386"]["CatName"] = "Aesthetic Development";

$default_oea["03387"]["ItemCode"] = "03387";
$default_oea["03387"]["ItemName"] = "數碼攝影比賽";
$default_oea["03387"]["CatCode"] = "4";
$default_oea["03387"]["CatName"] = "Aesthetic Development";

$default_oea["03385"]["ItemCode"] = "03385";
$default_oea["03385"]["ItemName"] = "數碼混音課程";
$default_oea["03385"]["CatCode"] = "4";
$default_oea["03385"]["CatName"] = "Aesthetic Development";

$default_oea["02608"]["ItemCode"] = "02608";
$default_oea["02608"]["ItemName"] = "文千歲伉儷臨別香江粵曲分享會義工隊";
$default_oea["02608"]["CatCode"] = "1";
$default_oea["02608"]["CatName"] = "Community Service";

$default_oea["02609"]["ItemCode"] = "02609";
$default_oea["02609"]["ItemName"] = "文學之星頒獎禮及文學專題講座";
$default_oea["02609"]["CatCode"] = "4";
$default_oea["02609"]["CatName"] = "Aesthetic Development";

$default_oea["02610"]["ItemCode"] = "02610";
$default_oea["02610"]["ItemName"] = "文學散步";
$default_oea["02610"]["CatCode"] = "4";
$default_oea["02610"]["CatName"] = "Aesthetic Development";

$default_oea["02611"]["ItemCode"] = "02611";
$default_oea["02611"]["ItemName"] = "文藝節 - 名作家創作分享會";
$default_oea["02611"]["CatCode"] = "4";
$default_oea["02611"]["CatName"] = "Aesthetic Development";

$default_oea["02612"]["ItemCode"] = "02612";
$default_oea["02612"]["ItemName"] = "文藝節 - 舞台劇表演及分享";
$default_oea["02612"]["CatCode"] = "4";
$default_oea["02612"]["CatName"] = "Aesthetic Development";

$default_oea["03263"]["ItemCode"] = "03263";
$default_oea["03263"]["ItemName"] = "新一代藝術家計劃 (博弈-當代裝置藝術展覽)";
$default_oea["03263"]["CatCode"] = "4";
$default_oea["03263"]["CatName"] = "Aesthetic Development";

$default_oea["03264"]["ItemCode"] = "03264";
$default_oea["03264"]["ItemName"] = "新世紀杯全國中學生作文大賽";
$default_oea["03264"]["CatCode"] = "4";
$default_oea["03264"]["CatName"] = "Aesthetic Development";

$default_oea["03265"]["ItemCode"] = "03265";
$default_oea["03265"]["ItemName"] = "新亞愛心巡禮";
$default_oea["03265"]["CatCode"] = "1";
$default_oea["03265"]["CatName"] = "Community Service";

$default_oea["03267"]["ItemCode"] = "03267";
$default_oea["03267"]["ItemName"] = "新春團拜國術表演";
$default_oea["03267"]["CatCode"] = "4";
$default_oea["03267"]["CatName"] = "Aesthetic Development";

$default_oea["03266"]["ItemCode"] = "03266";
$default_oea["03266"]["ItemName"] = "新春暖洋洋老人服務";
$default_oea["03266"]["CatCode"] = "1";
$default_oea["03266"]["CatName"] = "Community Service";

$default_oea["03268"]["ItemCode"] = "03268";
$default_oea["03268"]["ItemName"] = "新春舞獅活動";
$default_oea["03268"]["CatCode"] = "4";
$default_oea["03268"]["CatName"] = "Aesthetic Development";

$default_oea["03270"]["ItemCode"] = "03270";
$default_oea["03270"]["ItemName"] = "新界區兒童公園定向比賽";
$default_oea["03270"]["CatCode"] = "3";
$default_oea["03270"]["CatName"] = "Physical Development";

$default_oea["03271"]["ItemCode"] = "03271";
$default_oea["03271"]["ItemName"] = "新界區學界大富翁比賽";
$default_oea["03271"]["CatCode"] = "2";
$default_oea["03271"]["CatName"] = "Career-related Experience";

$default_oea["03269"]["ItemCode"] = "03269";
$default_oea["03269"]["ItemName"] = "新界杯中國象棋賽";
$default_oea["03269"]["CatCode"] = "4";
$default_oea["03269"]["CatName"] = "Aesthetic Development";

$default_oea["03272"]["ItemCode"] = "03272";
$default_oea["03272"]["ItemName"] = "新界聯校辯論比賽";
$default_oea["03272"]["CatCode"] = "2";
$default_oea["03272"]["CatName"] = "Career-related Experience";

$default_oea["03277"]["ItemCode"] = "03277";
$default_oea["03277"]["ItemName"] = "新聞寫作坊";
$default_oea["03277"]["CatCode"] = "4";
$default_oea["03277"]["CatName"] = "Aesthetic Development";

$default_oea["03276"]["ItemCode"] = "03276";
$default_oea["03276"]["ItemName"] = "新聞與報道立體的人物訪問";
$default_oea["03276"]["CatCode"] = "4";
$default_oea["03276"]["CatName"] = "Aesthetic Development";

$default_oea["03275"]["ItemCode"] = "03275";
$default_oea["03275"]["ItemName"] = "新高中應用學習簡介(美客學基礎)";
$default_oea["03275"]["CatCode"] = "2";
$default_oea["03275"]["CatName"] = "Career-related Experience";

$default_oea["03274"]["ItemCode"] = "03274";
$default_oea["03274"]["ItemName"] = "新高中與可持續發展教育計劃";
$default_oea["03274"]["CatCode"] = "2";
$default_oea["03274"]["CatName"] = "Career-related Experience";

$default_oea["03273"]["ItemCode"] = "03273";
$default_oea["03273"]["ItemName"] = "新高中視覺藝術課程習作建構及評估、到校支援及觀課計劃";
$default_oea["03273"]["CatCode"] = "2";
$default_oea["03273"]["CatName"] = "Career-related Experience";

$default_oea["03055"]["ItemCode"] = "03055";
$default_oea["03055"]["ItemName"] = "旁聽高等法院聆訊";
$default_oea["03055"]["CatCode"] = "5";
$default_oea["03055"]["CatName"] = "Moral and Civic Education";

$default_oea["03327"]["ItemCode"] = "03327";
$default_oea["03327"]["ItemName"] = "旗手護旗章考核";
$default_oea["03327"]["CatCode"] = "1";
$default_oea["03327"]["CatName"] = "Community Service";

$default_oea["02613"]["ItemCode"] = "02613";
$default_oea["02613"]["ItemName"] = "日本語能力試 (Please provide details in \"Description\" box.)";
$default_oea["02613"]["CatCode"] = "2";
$default_oea["02613"]["CatName"] = "Career-related Experience";

$default_oea["02836"]["ItemCode"] = "02836";
$default_oea["02836"]["ItemName"] = "明愛飛一般義工訓練計劃";
$default_oea["02836"]["CatCode"] = "1";
$default_oea["02836"]["CatName"] = "Community Service";

$default_oea["02834"]["ItemCode"] = "02834";
$default_oea["02834"]["ItemName"] = "明日領航者計劃";
$default_oea["02834"]["CatCode"] = "2";
$default_oea["02834"]["CatName"] = "Career-related Experience";

$default_oea["02835"]["ItemCode"] = "02835";
$default_oea["02835"]["ItemName"] = "明日領袖高峰論壇 - 綠色救地球";
$default_oea["02835"]["CatCode"] = "2";
$default_oea["02835"]["CatName"] = "Career-related Experience";

$default_oea["02896"]["ItemCode"] = "02896";
$default_oea["02896"]["ItemName"] = "星星河全國少年兒童美術書法攝影大賽";
$default_oea["02896"]["CatCode"] = "4";
$default_oea["02896"]["CatName"] = "Aesthetic Development";

$default_oea["02897"]["ItemCode"] = "02897";
$default_oea["02897"]["ItemName"] = "星球探索比賽月球基地設計比賽";
$default_oea["02897"]["CatCode"] = "4";
$default_oea["02897"]["CatName"] = "Aesthetic Development";

$default_oea["03056"]["ItemCode"] = "03056";
$default_oea["03056"]["ItemName"] = "時事自由談";
$default_oea["03056"]["CatCode"] = "5";
$default_oea["03056"]["CatName"] = "Moral and Civic Education";

$default_oea["03058"]["ItemCode"] = "03058";
$default_oea["03058"]["ItemName"] = "時裝設計課程 / 工作坊";
$default_oea["03058"]["CatCode"] = "4";
$default_oea["03058"]["CatName"] = "Aesthetic Development";

$default_oea["03057"]["ItemCode"] = "03057";
$default_oea["03057"]["ItemName"] = "時間攝影比賽";
$default_oea["03057"]["CatCode"] = "4";
$default_oea["03057"]["CatName"] = "Aesthetic Development";

$default_oea["03191"]["ItemCode"] = "03191";
$default_oea["03191"]["ItemName"] = "普通話大使";
$default_oea["03191"]["CatCode"] = "4";
$default_oea["03191"]["CatName"] = "Aesthetic Development";

$default_oea["03194"]["ItemCode"] = "03194";
$default_oea["03194"]["ItemName"] = "普通話情景對話演繹比賽";
$default_oea["03194"]["CatCode"] = "4";
$default_oea["03194"]["CatName"] = "Aesthetic Development";

$default_oea["03192"]["ItemCode"] = "03192";
$default_oea["03192"]["ItemName"] = "普通話才藝表演日";
$default_oea["03192"]["CatCode"] = "4";
$default_oea["03192"]["CatName"] = "Aesthetic Development";

$default_oea["03193"]["ItemCode"] = "03193";
$default_oea["03193"]["ItemName"] = "普通話朗誦比賽 (校內比賽)";
$default_oea["03193"]["CatCode"] = "4";
$default_oea["03193"]["CatName"] = "Aesthetic Development";

$default_oea["03195"]["ItemCode"] = "03195";
$default_oea["03195"]["ItemName"] = "普通話週";
$default_oea["03195"]["CatCode"] = "4";
$default_oea["03195"]["CatName"] = "Aesthetic Development";

$default_oea["03198"]["ItemCode"] = "03198";
$default_oea["03198"]["ItemName"] = "智者耆樂為你加油計劃短片創作比賽";
$default_oea["03198"]["CatCode"] = "4";
$default_oea["03198"]["CatName"] = "Aesthetic Development";

$default_oea["03199"]["ItemCode"] = "03199";
$default_oea["03199"]["ItemName"] = "智能機械由我創比賽";
$default_oea["03199"]["CatCode"] = "4";
$default_oea["03199"]["CatName"] = "Aesthetic Development";

$default_oea["03197"]["ItemCode"] = "03197";
$default_oea["03197"]["ItemName"] = "暑期射箭對抗賽";
$default_oea["03197"]["CatCode"] = "3";
$default_oea["03197"]["CatName"] = "Physical Development";

$default_oea["03196"]["ItemCode"] = "03196";
$default_oea["03196"]["ItemName"] = "暑期青年領袖學校";
$default_oea["03196"]["CatCode"] = "2";
$default_oea["03196"]["CatName"] = "Career-related Experience";

$default_oea["02750"]["ItemCode"] = "02750";
$default_oea["02750"]["ItemName"] = "曲棍球班";
$default_oea["02750"]["CatCode"] = "3";
$default_oea["02750"]["CatName"] = "Physical Development";

$default_oea["02790"]["ItemCode"] = "02790";
$default_oea["02790"]["ItemName"] = "更生先鋒計劃";
$default_oea["02790"]["CatCode"] = "2";
$default_oea["02790"]["CatName"] = "Career-related Experience";

$default_oea["03177"]["ItemCode"] = "03177";
$default_oea["03177"]["ItemName"] = "最佳少年警訊中學學校支會獎-優異獎";
$default_oea["03177"]["CatCode"] = "1";
$default_oea["03177"]["CatName"] = "Community Service";

$default_oea["03178"]["ItemCode"] = "03178";
$default_oea["03178"]["ItemName"] = "最具創意及最感人戒煙短訊比賽";
$default_oea["03178"]["CatCode"] = "4";
$default_oea["03178"]["CatName"] = "Aesthetic Development";

$default_oea["03278"]["ItemCode"] = "03278";
$default_oea["03278"]["ItemName"] = "會考放榜初探";
$default_oea["03278"]["CatCode"] = "2";
$default_oea["03278"]["CatName"] = "Career-related Experience";

$default_oea["02753"]["ItemCode"] = "02753";
$default_oea["02753"]["ItemName"] = "有出路 - 師友計劃第二期";
$default_oea["02753"]["CatCode"] = "2";
$default_oea["02753"]["CatName"] = "Career-related Experience";

$default_oea["02752"]["ItemCode"] = "02752";
$default_oea["02752"]["ItemName"] = "有心學校計劃: 關懷無家者";
$default_oea["02752"]["CatCode"] = "1";
$default_oea["02752"]["CatName"] = "Community Service";

$default_oea["02751"]["ItemCode"] = "02751";
$default_oea["02751"]["ItemName"] = "有心計劃";
$default_oea["02751"]["CatCode"] = "1";
$default_oea["02751"]["CatName"] = "Community Service";

$default_oea["02755"]["ItemCode"] = "02755";
$default_oea["02755"]["ItemName"] = "有機大使培訓計劃";
$default_oea["02755"]["CatCode"] = "4";
$default_oea["02755"]["CatName"] = "Aesthetic Development";

$default_oea["02756"]["ItemCode"] = "02756";
$default_oea["02756"]["ItemName"] = "有機耕種講座《拉闊有機》";
$default_oea["02756"]["CatCode"] = "4";
$default_oea["02756"]["CatName"] = "Aesthetic Development";

$default_oea["02754"]["ItemCode"] = "02754";
$default_oea["02754"]["ItemName"] = "有風傳雅韻-獅子山古典詩歌朗誦會";
$default_oea["02754"]["CatCode"] = "4";
$default_oea["02754"]["CatName"] = "Aesthetic Development";

$default_oea["02641"]["ItemCode"] = "02641";
$default_oea["02641"]["ItemName"] = "本地公開音樂藝術比賽";
$default_oea["02641"]["CatCode"] = "4";
$default_oea["02641"]["CatName"] = "Aesthetic Development";

$default_oea["02837"]["ItemCode"] = "02837";
$default_oea["02837"]["ItemName"] = "東九龍46旅旅慶";
$default_oea["02837"]["CatCode"] = "1";
$default_oea["02837"]["CatName"] = "Community Service";

$default_oea["02838"]["ItemCode"] = "02838";
$default_oea["02838"]["ItemName"] = "東亞運動會北區分齡網球比賽";
$default_oea["02838"]["CatCode"] = "3";
$default_oea["02838"]["CatName"] = "Physical Development";

$default_oea["02840"]["ItemCode"] = "02840";
$default_oea["02840"]["ItemName"] = "東區學校書法比賽";
$default_oea["02840"]["CatCode"] = "4";
$default_oea["02840"]["CatName"] = "Aesthetic Development";

$default_oea["02839"]["ItemCode"] = "02839";
$default_oea["02839"]["ItemName"] = "東區文藝節青年視覺藝術展";
$default_oea["02839"]["CatCode"] = "4";
$default_oea["02839"]["CatName"] = "Aesthetic Development";

$default_oea["02841"]["ItemCode"] = "02841";
$default_oea["02841"]["ItemName"] = "東華三院吳祥川紀念中學步操樂團";
$default_oea["02841"]["CatCode"] = "4";
$default_oea["02841"]["CatName"] = "Aesthetic Development";

$default_oea["02842"]["ItemCode"] = "02842";
$default_oea["02842"]["ItemName"] = "東華三院聯校音樂匯演";
$default_oea["02842"]["CatCode"] = "4";
$default_oea["02842"]["CatName"] = "Aesthetic Development";

$default_oea["02900"]["ItemCode"] = "02900";
$default_oea["02900"]["ItemName"] = "柏立基爵士信託基金";
$default_oea["02900"]["CatCode"] = "2";
$default_oea["02900"]["CatName"] = "Career-related Experience";

$default_oea["02898"]["ItemCode"] = "02898";
$default_oea["02898"]["ItemName"] = "柔道校隊 Judo School Teams";
$default_oea["02898"]["CatCode"] = "3";
$default_oea["02898"]["CatName"] = "Physical Development";

$default_oea["02899"]["ItemCode"] = "02899";
$default_oea["02899"]["ItemName"] = "柔道總會40周年香港柔道錦標賽";
$default_oea["02899"]["CatCode"] = "3";
$default_oea["02899"]["CatName"] = "Physical Development";

$default_oea["03060"]["ItemCode"] = "03060";
$default_oea["03060"]["ItemName"] = "校內音樂會";
$default_oea["03060"]["CatCode"] = "4";
$default_oea["03060"]["CatName"] = "Aesthetic Development";

$default_oea["03059"]["ItemCode"] = "03059";
$default_oea["03059"]["ItemName"] = "校內音樂比賽";
$default_oea["03059"]["CatCode"] = "4";
$default_oea["03059"]["CatName"] = "Aesthetic Development";

$default_oea["03062"]["ItemCode"] = "03062";
$default_oea["03062"]["ItemName"] = "校園心晴大使計劃";
$default_oea["03062"]["CatCode"] = "1";
$default_oea["03062"]["CatName"] = "Community Service";

$default_oea["03063"]["ItemCode"] = "03063";
$default_oea["03063"]["ItemName"] = "校園私隱關注運動學生大使工作坊";
$default_oea["03063"]["CatCode"] = "5";
$default_oea["03063"]["CatName"] = "Moral and Civic Education";

$default_oea["03064"]["ItemCode"] = "03064";
$default_oea["03064"]["ItemName"] = "校園私隱關注運動學生大使計劃";
$default_oea["03064"]["CatCode"] = "1";
$default_oea["03064"]["CatName"] = "Community Service";

$default_oea["03065"]["ItemCode"] = "03065";
$default_oea["03065"]["ItemName"] = "校園私隱關注運動學生大使誓師大會";
$default_oea["03065"]["CatCode"] = "1";
$default_oea["03065"]["CatName"] = "Community Service";

$default_oea["03061"]["ItemCode"] = "03061";
$default_oea["03061"]["ItemName"] = "校長午宴";
$default_oea["03061"]["CatCode"] = "5";
$default_oea["03061"]["CatName"] = "Moral and Civic Education";

$default_oea["03067"]["ItemCode"] = "03067";
$default_oea["03067"]["ItemName"] = "校際朗誦節賽前預演欣賞";
$default_oea["03067"]["CatCode"] = "4";
$default_oea["03067"]["CatName"] = "Aesthetic Development";

$default_oea["03066"]["ItemCode"] = "03066";
$default_oea["03066"]["ItemName"] = "校際香港歷史文化考察報告比賽";
$default_oea["03066"]["CatCode"] = "4";
$default_oea["03066"]["CatName"] = "Aesthetic Development";

$default_oea["03068"]["ItemCode"] = "03068";
$default_oea["03068"]["ItemName"] = "根住唐樓 - 社區文化藝墟：社區藝術創作展示/示範攤位";
$default_oea["03068"]["CatCode"] = "4";
$default_oea["03068"]["CatName"] = "Aesthetic Development";

$default_oea["03069"]["ItemCode"] = "03069";
$default_oea["03069"]["ItemName"] = "桂港青年匯聚2010文化交流活動";
$default_oea["03069"]["CatCode"] = "4";
$default_oea["03069"]["CatName"] = "Aesthetic Development";

$default_oea["03279"]["ItemCode"] = "03279";
$default_oea["03279"]["ItemName"] = "業餘進修中心主辦少年西餅、蛋糕、甜品製作精選實習班";
$default_oea["03279"]["CatCode"] = "4";
$default_oea["03279"]["CatName"] = "Aesthetic Development";

$default_oea["03280"]["ItemCode"] = "03280";
$default_oea["03280"]["ItemName"] = "業餘進修中心法文初階";
$default_oea["03280"]["CatCode"] = "2";
$default_oea["03280"]["CatName"] = "Career-related Experience";

$default_oea["03328"]["ItemCode"] = "03328";
$default_oea["03328"]["ItemName"] = "榮獲世界胸肺年─香港胸肺基金會謝婉雯醫生紀念獎學金全港中學生公開徵文比賽";
$default_oea["03328"]["CatCode"] = "4";
$default_oea["03328"]["CatName"] = "Aesthetic Development";

$default_oea["03329"]["ItemCode"] = "03329";
$default_oea["03329"]["ItemName"] = "榮獲香港浸會大學第六屆文學獎";
$default_oea["03329"]["CatCode"] = "4";
$default_oea["03329"]["CatName"] = "Aesthetic Development";

$default_oea["03391"]["ItemCode"] = "03391";
$default_oea["03391"]["ItemName"] = "模型投石機大賽";
$default_oea["03391"]["CatCode"] = "2";
$default_oea["03391"]["CatName"] = "Career-related Experience";

$default_oea["03393"]["ItemCode"] = "03393";
$default_oea["03393"]["ItemName"] = "模擬放榜";
$default_oea["03393"]["CatCode"] = "2";
$default_oea["03393"]["CatName"] = "Career-related Experience";

$default_oea["03394"]["ItemCode"] = "03394";
$default_oea["03394"]["ItemName"] = "模擬法庭大賽計劃";
$default_oea["03394"]["CatCode"] = "2";
$default_oea["03394"]["CatName"] = "Career-related Experience";

$default_oea["03395"]["ItemCode"] = "03395";
$default_oea["03395"]["ItemName"] = "模擬面試活動";
$default_oea["03395"]["CatCode"] = "2";
$default_oea["03395"]["CatName"] = "Career-related Experience";

$default_oea["03392"]["ItemCode"] = "03392";
$default_oea["03392"]["ItemName"] = "模範生選舉";
$default_oea["03392"]["CatCode"] = "2";
$default_oea["03392"]["CatName"] = "Career-related Experience";

$default_oea["03431"]["ItemCode"] = "03431";
$default_oea["03431"]["ItemName"] = "樹景有情攝影比賽";
$default_oea["03431"]["CatCode"] = "4";
$default_oea["03431"]["CatName"] = "Aesthetic Development";

$default_oea["03435"]["ItemCode"] = "03435";
$default_oea["03435"]["ItemName"] = "機器人奧運會埠際賽";
$default_oea["03435"]["CatCode"] = "2";
$default_oea["03435"]["CatName"] = "Career-related Experience";

$default_oea["03432"]["ItemCode"] = "03432";
$default_oea["03432"]["ItemName"] = "機會之城";
$default_oea["03432"]["CatCode"] = "2";
$default_oea["03432"]["CatName"] = "Career-related Experience";

$default_oea["03434"]["ItemCode"] = "03434";
$default_oea["03434"]["ItemName"] = "機電安全香港通2010- 攝影故事比賽";
$default_oea["03434"]["CatCode"] = "4";
$default_oea["03434"]["CatName"] = "Aesthetic Development";

$default_oea["03433"]["ItemCode"] = "03433";
$default_oea["03433"]["ItemName"] = "機電工程處-海報設計";
$default_oea["03433"]["CatCode"] = "4";
$default_oea["03433"]["CatName"] = "Aesthetic Development";

$default_oea["03512"]["ItemCode"] = "03512";
$default_oea["03512"]["ItemName"] = "權兒之計口號設計比賽";
$default_oea["03512"]["CatCode"] = "4";
$default_oea["03512"]["CatName"] = "Aesthetic Development";

$default_oea["03516"]["ItemCode"] = "03516";
$default_oea["03516"]["ItemName"] = "欖球技術交流及同樂日";
$default_oea["03516"]["CatCode"] = "3";
$default_oea["03516"]["CatName"] = "Physical Development";

$default_oea["02642"]["ItemCode"] = "02642";
$default_oea["02642"]["ItemName"] = "正向心理四格漫畫設計比賽";
$default_oea["02642"]["CatCode"] = "4";
$default_oea["02642"]["CatName"] = "Aesthetic Development";

$default_oea["02643"]["ItemCode"] = "02643";
$default_oea["02643"]["ItemName"] = "正字大行動";
$default_oea["02643"]["CatCode"] = "5";
$default_oea["02643"]["CatName"] = "Moral and Civic Education";

$default_oea["02791"]["ItemCode"] = "02791";
$default_oea["02791"]["ItemName"] = "步行籌款";
$default_oea["02791"]["CatCode"] = "1";
$default_oea["02791"]["CatName"] = "Community Service";

$default_oea["03436"]["ItemCode"] = "03436";
$default_oea["03436"]["ItemName"] = "歷史文化考察報告比賽";
$default_oea["03436"]["CatCode"] = "4";
$default_oea["03436"]["CatName"] = "Aesthetic Development";

$default_oea["03484"]["ItemCode"] = "03484";
$default_oea["03484"]["ItemName"] = "歸來吧! 之潮飲潮食";
$default_oea["03484"]["CatCode"] = "4";
$default_oea["03484"]["CatName"] = "Aesthetic Development";

$default_oea["03396"]["ItemCode"] = "03396";
$default_oea["03396"]["ItemName"] = "毅力十二愛心跑";
$default_oea["03396"]["CatCode"] = "3";
$default_oea["03396"]["CatName"] = "Physical Development";

$default_oea["02644"]["ItemCode"] = "02644";
$default_oea["02644"]["ItemName"] = "母親的抉擇性教育工作坊﹕傳媒與性";
$default_oea["02644"]["CatCode"] = "5";
$default_oea["02644"]["CatName"] = "Moral and Civic Education";

$default_oea["02645"]["ItemCode"] = "02645";
$default_oea["02645"]["ItemName"] = "母親節念親恩  環保袋設計比賽";
$default_oea["02645"]["CatCode"] = "4";
$default_oea["02645"]["CatName"] = "Aesthetic Development";

$default_oea["02901"]["ItemCode"] = "02901";
$default_oea["02901"]["ItemName"] = "毒品齊SAY NO話劇比賽";
$default_oea["02901"]["CatCode"] = "4";
$default_oea["02901"]["CatName"] = "Aesthetic Development";

$default_oea["02614"]["ItemCode"] = "02614";
$default_oea["02614"]["ItemName"] = "水上歷奇活動";
$default_oea["02614"]["CatCode"] = "3";
$default_oea["02614"]["CatName"] = "Physical Development";

$default_oea["02615"]["ItemCode"] = "02615";
$default_oea["02615"]["ItemName"] = "水火箭50米定點打靶";
$default_oea["02615"]["CatCode"] = "3";
$default_oea["02615"]["CatName"] = "Physical Development";

$default_oea["02616"]["ItemCode"] = "02616";
$default_oea["02616"]["ItemName"] = "水運會";
$default_oea["02616"]["CatCode"] = "3";
$default_oea["02616"]["CatName"] = "Physical Development";

$default_oea["02647"]["ItemCode"] = "02647";
$default_oea["02647"]["ItemName"] = "永亨義人行_齊心結力共成長";
$default_oea["02647"]["CatCode"] = "1";
$default_oea["02647"]["CatName"] = "Community Service";

$default_oea["02646"]["ItemCode"] = "02646";
$default_oea["02646"]["ItemName"] = "永亨義人行(二)智放光芒:籌得一蚊，得一蚊義賣活動";
$default_oea["02646"]["CatCode"] = "1";
$default_oea["02646"]["CatName"] = "Community Service";

$default_oea["02649"]["ItemCode"] = "02649";
$default_oea["02649"]["ItemName"] = "永亨義人行學界義工計劃 - 織極人生";
$default_oea["02649"]["CatCode"] = "1";
$default_oea["02649"]["CatName"] = "Community Service";

$default_oea["02648"]["ItemCode"] = "02648";
$default_oea["02648"]["ItemName"] = "永亨義人行學界義工計劃分享會暨頒獎典禮";
$default_oea["02648"]["CatCode"] = "1";
$default_oea["02648"]["CatName"] = "Community Service";

$default_oea["02800"]["ItemCode"] = "02800";
$default_oea["02800"]["ItemName"] = "沙灘節 - 足球賽";
$default_oea["02800"]["CatCode"] = "3";
$default_oea["02800"]["CatName"] = "Physical Development";

$default_oea["02797"]["ItemCode"] = "02797";
$default_oea["02797"]["ItemName"] = "沙田傑出學生協會 幹事";
$default_oea["02797"]["CatCode"] = "2";
$default_oea["02797"]["CatName"] = "Career-related Experience";

$default_oea["02794"]["ItemCode"] = "02794";
$default_oea["02794"]["ItemName"] = "沙田區少年警訊5天廣州學術交流團";
$default_oea["02794"]["CatCode"] = "1";
$default_oea["02794"]["CatName"] = "Community Service";

$default_oea["02796"]["ItemCode"] = "02796";
$default_oea["02796"]["ItemName"] = "沙田區綠絲帶關愛鄰里行動家庭實況劇 一念之間徵文比賽";
$default_oea["02796"]["CatCode"] = "4";
$default_oea["02796"]["CatName"] = "Aesthetic Development";

$default_oea["02795"]["ItemCode"] = "02795";
$default_oea["02795"]["ItemName"] = "沙田區青少年獎勵計劃";
$default_oea["02795"]["CatCode"] = "2";
$default_oea["02795"]["CatName"] = "Career-related Experience";

$default_oea["02793"]["ItemCode"] = "02793";
$default_oea["02793"]["ItemName"] = "沙田城市定向日";
$default_oea["02793"]["CatCode"] = "2";
$default_oea["02793"]["CatName"] = "Career-related Experience";

$default_oea["02792"]["ItemCode"] = "02792";
$default_oea["02792"]["ItemName"] = "沙田污水處理廠開放日";
$default_oea["02792"]["CatCode"] = "4";
$default_oea["02792"]["CatName"] = "Aesthetic Development";

$default_oea["02799"]["ItemCode"] = "02799";
$default_oea["02799"]["ItemName"] = "沙田管樂團十週年音樂會";
$default_oea["02799"]["CatCode"] = "4";
$default_oea["02799"]["CatName"] = "Aesthetic Development";

$default_oea["02798"]["ItemCode"] = "02798";
$default_oea["02798"]["ItemName"] = "沙田節日燈飾攝影比賽";
$default_oea["02798"]["CatCode"] = "4";
$default_oea["02798"]["CatName"] = "Aesthetic Development";

$default_oea["02843"]["ItemCode"] = "02843";
$default_oea["02843"]["ItemName"] = "油尖旺道路安全運動海報設計比賽";
$default_oea["02843"]["CatCode"] = "4";
$default_oea["02843"]["CatName"] = "Aesthetic Development";

$default_oea["02903"]["ItemCode"] = "02903";
$default_oea["02903"]["ItemName"] = "活出人生—價值觀建立與鞏固";
$default_oea["02903"]["CatCode"] = "5";
$default_oea["02903"]["CatName"] = "Moral and Civic Education";

$default_oea["02905"]["ItemCode"] = "02905";
$default_oea["02905"]["ItemName"] = "活出健康之旅(東莞虎門)";
$default_oea["02905"]["CatCode"] = "5";
$default_oea["02905"]["CatName"] = "Moral and Civic Education";

$default_oea["02904"]["ItemCode"] = "02904";
$default_oea["02904"]["ItemName"] = "活出健康人生訊息分享計劃";
$default_oea["02904"]["CatCode"] = "5";
$default_oea["02904"]["CatName"] = "Moral and Civic Education";

$default_oea["02906"]["ItemCode"] = "02906";
$default_oea["02906"]["ItemName"] = "活在藝術中—毛筆書法及國畫初體驗";
$default_oea["02906"]["CatCode"] = "4";
$default_oea["02906"]["CatName"] = "Aesthetic Development";

$default_oea["02907"]["ItemCode"] = "02907";
$default_oea["02907"]["ItemName"] = "活在藝術中—藝術體操及舞蹈初體驗";
$default_oea["02907"]["CatCode"] = "4";
$default_oea["02907"]["CatName"] = "Aesthetic Development";

$default_oea["02908"]["ItemCode"] = "02908";
$default_oea["02908"]["ItemName"] = "活的傳承—中國崑曲博物館：崑曲折子戲展演";
$default_oea["02908"]["CatCode"] = "4";
$default_oea["02908"]["CatName"] = "Aesthetic Development";

$default_oea["02902"]["ItemCode"] = "02902";
$default_oea["02902"]["ItemName"] = "流行歌曲歌唱技巧";
$default_oea["02902"]["CatCode"] = "4";
$default_oea["02902"]["CatName"] = "Aesthetic Development";

$default_oea["03073"]["ItemCode"] = "03073";
$default_oea["03073"]["ItemName"] = "海下灣中學教育項目- 海岸生態學家";
$default_oea["03073"]["CatCode"] = "2";
$default_oea["03073"]["CatName"] = "Career-related Experience";

$default_oea["03077"]["ItemCode"] = "03077";
$default_oea["03077"]["ItemName"] = "海峽兩岸三家兒童醫院徵畫比賽";
$default_oea["03077"]["CatCode"] = "4";
$default_oea["03077"]["CatName"] = "Aesthetic Development";

$default_oea["03074"]["ItemCode"] = "03074";
$default_oea["03074"]["ItemName"] = "海洋公園之動感物理";
$default_oea["03074"]["CatCode"] = "2";
$default_oea["03074"]["CatName"] = "Career-related Experience";

$default_oea["03076"]["ItemCode"] = "03076";
$default_oea["03076"]["ItemName"] = "海洋公園報佳音 Christmas Carol in Ocean Park";
$default_oea["03076"]["CatCode"] = "1";
$default_oea["03076"]["CatName"] = "Community Service";

$default_oea["03075"]["ItemCode"] = "03075";
$default_oea["03075"]["ItemName"] = "海洋公園水質源保育座談高峰會";
$default_oea["03075"]["CatCode"] = "5";
$default_oea["03075"]["CatName"] = "Moral and Civic Education";

$default_oea["03072"]["ItemCode"] = "03072";
$default_oea["03072"]["ItemName"] = "浸會大學藝影共享";
$default_oea["03072"]["CatCode"] = "4";
$default_oea["03072"]["CatName"] = "Aesthetic Development";

$default_oea["03070"]["ItemCode"] = "03070";
$default_oea["03070"]["ItemName"] = "消防安全標語創作比賽";
$default_oea["03070"]["CatCode"] = "4";
$default_oea["03070"]["CatName"] = "Aesthetic Development";

$default_oea["03071"]["ItemCode"] = "03071";
$default_oea["03071"]["ItemName"] = "消除殘疾歧視．共建平等社會全港青少年徵文比賽";
$default_oea["03071"]["CatCode"] = "4";
$default_oea["03071"]["CatName"] = "Aesthetic Development";

$default_oea["03154"]["ItemCode"] = "03154";
$default_oea["03154"]["ItemName"] = "深圳青少年鋼琴藝術大獎賽";
$default_oea["03154"]["CatCode"] = "4";
$default_oea["03154"]["CatName"] = "Aesthetic Development";

$default_oea["03153"]["ItemCode"] = "03153";
$default_oea["03153"]["ItemName"] = "深水埗區家校才藝展光華";
$default_oea["03153"]["CatCode"] = "4";
$default_oea["03153"]["CatName"] = "Aesthetic Development";

$default_oea["03152"]["ItemCode"] = "03152";
$default_oea["03152"]["ItemName"] = "清新每一TEEN計劃 - 海報設計比賽";
$default_oea["03152"]["CatCode"] = "4";
$default_oea["03152"]["CatName"] = "Aesthetic Development";

$default_oea["03149"]["ItemCode"] = "03149";
$default_oea["03149"]["ItemName"] = "清晨廣播-我與藝術有約II";
$default_oea["03149"]["CatCode"] = "4";
$default_oea["03149"]["CatName"] = "Aesthetic Development";

$default_oea["03150"]["ItemCode"] = "03150";
$default_oea["03150"]["ItemName"] = "清晨廣播-珍愛生命教育頻道II";
$default_oea["03150"]["CatCode"] = "5";
$default_oea["03150"]["CatName"] = "Moral and Civic Education";

$default_oea["03151"]["ItemCode"] = "03151";
$default_oea["03151"]["ItemName"] = "清華大學國情教育交流計劃";
$default_oea["03151"]["CatCode"] = "5";
$default_oea["03151"]["CatName"] = "Moral and Civic Education";

$default_oea["03201"]["ItemCode"] = "03201";
$default_oea["03201"]["ItemName"] = "港專全港中學生歌唱比賽";
$default_oea["03201"]["CatCode"] = "4";
$default_oea["03201"]["CatName"] = "Aesthetic Development";

$default_oea["03200"]["ItemCode"] = "03200";
$default_oea["03200"]["ItemName"] = "港島東區中學即席專題演講比賽";
$default_oea["03200"]["CatCode"] = "4";
$default_oea["03200"]["CatName"] = "Aesthetic Development";

$default_oea["03202"]["ItemCode"] = "03202";
$default_oea["03202"]["ItemName"] = "港澳地區中小學普通話水平考試";
$default_oea["03202"]["CatCode"] = "2";
$default_oea["03202"]["CatName"] = "Career-related Experience";

$default_oea["03203"]["ItemCode"] = "03203";
$default_oea["03203"]["ItemName"] = "港燈電動車扮靚大賽";
$default_oea["03203"]["CatCode"] = "4";
$default_oea["03203"]["CatName"] = "Aesthetic Development";

$default_oea["03204"]["ItemCode"] = "03204";
$default_oea["03204"]["ItemName"] = "港鐵西港島綫 - 社區藝術創作計劃";
$default_oea["03204"]["CatCode"] = "4";
$default_oea["03204"]["CatName"] = "Aesthetic Development";

$default_oea["03281"]["ItemCode"] = "03281";
$default_oea["03281"]["ItemName"] = "溝通技巧昇華證書課程";
$default_oea["03281"]["CatCode"] = "4";
$default_oea["03281"]["CatName"] = "Aesthetic Development";

$default_oea["03532"]["ItemCode"] = "03532";
$default_oea["03532"]["ItemName"] = "滙豐愛心傳城義工大行動";
$default_oea["03532"]["CatCode"] = "5";
$default_oea["03532"]["CatName"] = "Moral and Civic Education";

$default_oea["03330"]["ItemCode"] = "03330";
$default_oea["03330"]["ItemName"] = "演藝學院戲劇工作坊";
$default_oea["03330"]["CatCode"] = "4";
$default_oea["03330"]["CatName"] = "Aesthetic Development";

$default_oea["03331"]["ItemCode"] = "03331";
$default_oea["03331"]["ItemName"] = "演辯技巧工作坊(高級組)";
$default_oea["03331"]["CatCode"] = "4";
$default_oea["03331"]["CatName"] = "Aesthetic Development";

$default_oea["03397"]["ItemCode"] = "03397";
$default_oea["03397"]["ItemName"] = "潮人聯會奮發圖強獎學金";
$default_oea["03397"]["CatCode"] = "2";
$default_oea["03397"]["CatName"] = "Career-related Experience";

$default_oea["03437"]["ItemCode"] = "03437";
$default_oea["03437"]["ItemName"] = "澳門 - 亞洲鋼琴公開比賽";
$default_oea["03437"]["CatCode"] = "2";
$default_oea["03437"]["CatName"] = "Career-related Experience";

$default_oea["03457"]["ItemCode"] = "03457";
$default_oea["03457"]["ItemName"] = "濕地教室";
$default_oea["03457"]["CatCode"] = "5";
$default_oea["03457"]["CatName"] = "Moral and Civic Education";

$default_oea["03456"]["ItemCode"] = "03456";
$default_oea["03456"]["ItemName"] = "濕地生態及有機耕種";
$default_oea["03456"]["CatCode"] = "4";
$default_oea["03456"]["CatName"] = "Aesthetic Development";

$default_oea["03517"]["ItemCode"] = "03517";
$default_oea["03517"]["ItemName"] = "灣仔藍屋拍攝活動";
$default_oea["03517"]["CatCode"] = "4";
$default_oea["03517"]["CatName"] = "Aesthetic Development";

$default_oea["02617"]["ItemCode"] = "02617";
$default_oea["02617"]["ItemName"] = "火炭藝術工作室開放計劃導賞活動";
$default_oea["02617"]["CatCode"] = "4";
$default_oea["02617"]["CatName"] = "Aesthetic Development";

$default_oea["02801"]["ItemCode"] = "02801";
$default_oea["02801"]["ItemName"] = "災難自我求生暨防火講座及遵理一叮才藝比賽";
$default_oea["02801"]["CatCode"] = "4";
$default_oea["02801"]["CatName"] = "Aesthetic Development";

$default_oea["02910"]["ItemCode"] = "02910";
$default_oea["02910"]["ItemName"] = "炮台攻防戰";
$default_oea["02910"]["CatCode"] = "2";
$default_oea["02910"]["CatName"] = "Career-related Experience";

$default_oea["02909"]["ItemCode"] = "02909";
$default_oea["02909"]["ItemName"] = "為青少年把把脈寫作比賽";
$default_oea["02909"]["CatCode"] = "4";
$default_oea["02909"]["CatName"] = "Aesthetic Development";

$default_oea["03205"]["ItemCode"] = "03205";
$default_oea["03205"]["ItemName"] = "無冷氣日";
$default_oea["03205"]["CatCode"] = "5";
$default_oea["03205"]["CatName"] = "Moral and Civic Education";

$default_oea["03206"]["ItemCode"] = "03206";
$default_oea["03206"]["ItemName"] = "無毒人人愛 - 禁毒口號創作比賽";
$default_oea["03206"]["CatCode"] = "4";
$default_oea["03206"]["CatName"] = "Aesthetic Development";

$default_oea["03207"]["ItemCode"] = "03207";
$default_oea["03207"]["ItemName"] = "無毒有我摯友營";
$default_oea["03207"]["CatCode"] = "5";
$default_oea["03207"]["CatName"] = "Moral and Civic Education";

$default_oea["03208"]["ItemCode"] = "03208";
$default_oea["03208"]["ItemName"] = "無毒社區標語創作比賽";
$default_oea["03208"]["CatCode"] = "4";
$default_oea["03208"]["CatName"] = "Aesthetic Development";

$default_oea["03210"]["ItemCode"] = "03210";
$default_oea["03210"]["ItemName"] = "無煙家居中文標語創作比賽";
$default_oea["03210"]["CatCode"] = "4";
$default_oea["03210"]["CatName"] = "Aesthetic Development";

$default_oea["03211"]["ItemCode"] = "03211";
$default_oea["03211"]["ItemName"] = "無線電視 - 激優一族 - 錄影活動";
$default_oea["03211"]["CatCode"] = "4";
$default_oea["03211"]["CatName"] = "Aesthetic Development";

$default_oea["03209"]["ItemCode"] = "03209";
$default_oea["03209"]["ItemName"] = "無限可能爆Seed升呢大行動 (Please provide details in \"Description\" box.)";
$default_oea["03209"]["CatCode"] = "4";
$default_oea["03209"]["CatName"] = "Aesthetic Development";

$default_oea["03438"]["ItemCode"] = "03438";
$default_oea["03438"]["ItemName"] = "燕京之星獎勵計劃";
$default_oea["03438"]["CatCode"] = "2";
$default_oea["03438"]["CatName"] = "Career-related Experience";

$default_oea["03439"]["ItemCode"] = "03439";
$default_oea["03439"]["ItemName"] = "燕京盃全港中小學校際圍棋錦標賽";
$default_oea["03439"]["CatCode"] = "4";
$default_oea["03439"]["CatName"] = "Aesthetic Development";

$default_oea["03078"]["ItemCode"] = "03078";
$default_oea["03078"]["ItemName"] = "特步香港青少年分齡賽";
$default_oea["03078"]["CatCode"] = "3";
$default_oea["03078"]["CatName"] = "Physical Development";

$default_oea["03282"]["ItemCode"] = "03282";
$default_oea["03282"]["ItemName"] = "獅藝學會";
$default_oea["03282"]["CatCode"] = "3";
$default_oea["03282"]["CatName"] = "Physical Development";

$default_oea["03440"]["ItemCode"] = "03440";
$default_oea["03440"]["ItemName"] = "獨木舟歷奇體驗";
$default_oea["03440"]["CatCode"] = "3";
$default_oea["03440"]["CatName"] = "Physical Development";

$default_oea["03458"]["ItemCode"] = "03458";
$default_oea["03458"]["ItemName"] = "獲選香港資優學院(領袖才幹)學員";
$default_oea["03458"]["CatCode"] = "2";
$default_oea["03458"]["CatName"] = "Career-related Experience";

$default_oea["02618"]["ItemCode"] = "02618";
$default_oea["02618"]["ItemName"] = "王仲銘中學中國象棋比賽";
$default_oea["02618"]["CatCode"] = "4";
$default_oea["02618"]["CatName"] = "Aesthetic Development";

$default_oea["03080"]["ItemCode"] = "03080";
$default_oea["03080"]["ItemName"] = "珠、澳傳統武術(套路)邀請賽";
$default_oea["03080"]["CatCode"] = "3";
$default_oea["03080"]["CatName"] = "Physical Development";

$default_oea["03079"]["ItemCode"] = "03079";
$default_oea["03079"]["ItemName"] = "班際合唱";
$default_oea["03079"]["CatCode"] = "4";
$default_oea["03079"]["CatName"] = "Aesthetic Development";

$default_oea["03159"]["ItemCode"] = "03159";
$default_oea["03159"]["ItemName"] = "現代盃全港中小學教師暨家校乒乓球賽";
$default_oea["03159"]["CatCode"] = "3";
$default_oea["03159"]["CatName"] = "Physical Development";

$default_oea["03155"]["ItemCode"] = "03155";
$default_oea["03155"]["ItemName"] = "理中有商：會財講座";
$default_oea["03155"]["CatCode"] = "2";
$default_oea["03155"]["CatName"] = "Career-related Experience";

$default_oea["03158"]["ItemCode"] = "03158";
$default_oea["03158"]["ItemName"] = "理想家園全港徵文比賽";
$default_oea["03158"]["CatCode"] = "4";
$default_oea["03158"]["CatName"] = "Aesthetic Development";

$default_oea["03156"]["ItemCode"] = "03156";
$default_oea["03156"]["ItemName"] = "理財 Easy Go計劃";
$default_oea["03156"]["CatCode"] = "2";
$default_oea["03156"]["CatName"] = "Career-related Experience";

$default_oea["03157"]["ItemCode"] = "03157";
$default_oea["03157"]["ItemName"] = "理財有道教育計劃講座";
$default_oea["03157"]["CatCode"] = "2";
$default_oea["03157"]["CatName"] = "Career-related Experience";

$default_oea["03459"]["ItemCode"] = "03459";
$default_oea["03459"]["ItemName"] = "環保大使計劃─全城起動互動講座";
$default_oea["03459"]["CatCode"] = "4";
$default_oea["03459"]["CatName"] = "Aesthetic Development";

$default_oea["03462"]["ItemCode"] = "03462";
$default_oea["03462"]["ItemName"] = "環保時裝設計比賽暨環保 Fashion Show";
$default_oea["03462"]["CatCode"] = "4";
$default_oea["03462"]["CatName"] = "Aesthetic Development";

$default_oea["03461"]["ItemCode"] = "03461";
$default_oea["03461"]["ItemName"] = "環保為公益 - 環保標語創作比賽";
$default_oea["03461"]["CatCode"] = "4";
$default_oea["03461"]["CatName"] = "Aesthetic Development";

$default_oea["03463"]["ItemCode"] = "03463";
$default_oea["03463"]["ItemName"] = "環保產品設計大賽";
$default_oea["03463"]["CatCode"] = "4";
$default_oea["03463"]["CatName"] = "Aesthetic Development";

$default_oea["03460"]["ItemCode"] = "03460";
$default_oea["03460"]["ItemName"] = "環保花燈設計比賽";
$default_oea["03460"]["CatCode"] = "4";
$default_oea["03460"]["CatName"] = "Aesthetic Development";

$default_oea["03465"]["ItemCode"] = "03465";
$default_oea["03465"]["ItemName"] = "環境保護大使計劃";
$default_oea["03465"]["CatCode"] = "1";
$default_oea["03465"]["CatName"] = "Community Service";

$default_oea["03467"]["ItemCode"] = "03467";
$default_oea["03467"]["ItemName"] = "環境與空間研習課程";
$default_oea["03467"]["CatCode"] = "2";
$default_oea["03467"]["CatName"] = "Career-related Experience";

$default_oea["03466"]["ItemCode"] = "03466";
$default_oea["03466"]["ItemName"] = "環境設計課程";
$default_oea["03466"]["CatCode"] = "4";
$default_oea["03466"]["CatName"] = "Aesthetic Development";

$default_oea["03464"]["ItemCode"] = "03464";
$default_oea["03464"]["ItemName"] = "環島行慈善行山比賽";
$default_oea["03464"]["CatCode"] = "3";
$default_oea["03464"]["CatName"] = "Physical Development";

$default_oea["02651"]["ItemCode"] = "02651";
$default_oea["02651"]["ItemName"] = "生命內涵操練比賽II-打破因循、更新生命";
$default_oea["02651"]["CatCode"] = "5";
$default_oea["02651"]["CatName"] = "Moral and Civic Education";

$default_oea["02652"]["ItemCode"] = "02652";
$default_oea["02652"]["ItemName"] = "生命教育課";
$default_oea["02652"]["CatCode"] = "5";
$default_oea["02652"]["CatName"] = "Moral and Civic Education";

$default_oea["02654"]["ItemCode"] = "02654";
$default_oea["02654"]["ItemName"] = "生態學習營";
$default_oea["02654"]["CatCode"] = "2";
$default_oea["02654"]["CatName"] = "Career-related Experience";

$default_oea["02653"]["ItemCode"] = "02653";
$default_oea["02653"]["ItemName"] = "生態精英班";
$default_oea["02653"]["CatCode"] = "5";
$default_oea["02653"]["CatName"] = "Moral and Civic Education";

$default_oea["02650"]["ItemCode"] = "02650";
$default_oea["02650"]["ItemName"] = "生死教育徵文比賽";
$default_oea["02650"]["CatCode"] = "4";
$default_oea["02650"]["CatName"] = "Aesthetic Development";

$default_oea["03160"]["ItemCode"] = "03160";
$default_oea["03160"]["ItemName"] = "畢業展覽(香港城市大學旅遊學系)";
$default_oea["03160"]["CatCode"] = "4";
$default_oea["03160"]["CatName"] = "Aesthetic Development";

$default_oea["03212"]["ItemCode"] = "03212";
$default_oea["03212"]["ItemName"] = "畫出健康生活- 流動課室車身圖案設計比賽";
$default_oea["03212"]["CatCode"] = "4";
$default_oea["03212"]["CatName"] = "Aesthetic Development";

$default_oea["02655"]["ItemCode"] = "02655";
$default_oea["02655"]["ItemName"] = "白理義先生野外定向紀念賽";
$default_oea["02655"]["CatCode"] = "3";
$default_oea["02655"]["CatName"] = "Physical Development";

$default_oea["02757"]["ItemCode"] = "02757";
$default_oea["02757"]["ItemName"] = "百年中國校際問答比賽";
$default_oea["02757"]["CatCode"] = "5";
$default_oea["02757"]["CatName"] = "Moral and Civic Education";

$default_oea["03081"]["ItemCode"] = "03081";
$default_oea["03081"]["ItemName"] = "益智遊戲競賽";
$default_oea["03081"]["CatCode"] = "4";
$default_oea["03081"]["CatName"] = "Aesthetic Development";

$default_oea["02911"]["ItemCode"] = "02911";
$default_oea["02911"]["ItemName"] = "看得見的希望視藝計劃";
$default_oea["02911"]["CatCode"] = "4";
$default_oea["02911"]["CatName"] = "Aesthetic Development";

$default_oea["03082"]["ItemCode"] = "03082";
$default_oea["03082"]["ItemName"] = "真光女子數學邀請賽";
$default_oea["03082"]["CatCode"] = "2";
$default_oea["03082"]["CatName"] = "Career-related Experience";

$default_oea["03468"]["ItemCode"] = "03468";
$default_oea["03468"]["ItemName"] = "瞬攝‧潮影‧新世代 - 攝影比賽";
$default_oea["03468"]["CatCode"] = "4";
$default_oea["03468"]["CatName"] = "Aesthetic Development";

$default_oea["02656"]["ItemCode"] = "02656";
$default_oea["02656"]["ItemName"] = "石景宜博士盃華夏書畫創作大賽";
$default_oea["02656"]["CatCode"] = "4";
$default_oea["02656"]["CatName"] = "Aesthetic Development";

$default_oea["02844"]["ItemCode"] = "02844";
$default_oea["02844"]["ItemName"] = "社區共融‧顯關愛心意卡設計比賽";
$default_oea["02844"]["CatCode"] = "4";
$default_oea["02844"]["CatName"] = "Aesthetic Development";

$default_oea["02845"]["ItemCode"] = "02845";
$default_oea["02845"]["ItemName"] = "社區同心另類學習經驗博覽";
$default_oea["02845"]["CatCode"] = "1";
$default_oea["02845"]["CatName"] = "Community Service";

$default_oea["02846"]["ItemCode"] = "02846";
$default_oea["02846"]["ItemName"] = "社會服務團訓練營";
$default_oea["02846"]["CatCode"] = "1";
$default_oea["02846"]["CatName"] = "Community Service";

$default_oea["02847"]["ItemCode"] = "02847";
$default_oea["02847"]["ItemName"] = "社會與我攝影比賽";
$default_oea["02847"]["CatCode"] = "4";
$default_oea["02847"]["CatName"] = "Aesthetic Development";

$default_oea["02848"]["ItemCode"] = "02848";
$default_oea["02848"]["ItemName"] = "社際歌唱比賽初賽欣賞";
$default_oea["02848"]["CatCode"] = "4";
$default_oea["02848"]["CatName"] = "Aesthetic Development";

$default_oea["03083"]["ItemCode"] = "03083";
$default_oea["03083"]["ItemName"] = "神舟七號載人航天代表團與香港學生真情對話";
$default_oea["03083"]["CatCode"] = "5";
$default_oea["03083"]["CatName"] = "Moral and Civic Education";

$default_oea["03283"]["ItemCode"] = "03283";
$default_oea["03283"]["ItemName"] = "禁毒 T-Shirt 設計比賽";
$default_oea["03283"]["CatCode"] = "4";
$default_oea["03283"]["CatName"] = "Aesthetic Development";

$default_oea["03284"]["ItemCode"] = "03284";
$default_oea["03284"]["ItemName"] = "禁毒先鋒大使計劃";
$default_oea["03284"]["CatCode"] = "5";
$default_oea["03284"]["CatName"] = "Moral and Civic Education";

$default_oea["02802"]["ItemCode"] = "02802";
$default_oea["02802"]["ItemName"] = "秀茂坪區會區慶";
$default_oea["02802"]["CatCode"] = "4";
$default_oea["02802"]["CatName"] = "Aesthetic Development";

$default_oea["02917"]["ItemCode"] = "02917";
$default_oea["02917"]["ItemName"] = "秋季藝術教育活動";
$default_oea["02917"]["CatCode"] = "4";
$default_oea["02917"]["CatName"] = "Aesthetic Development";

$default_oea["02916"]["ItemCode"] = "02916";
$default_oea["02916"]["ItemName"] = "秋季遠足(青年探知學會)";
$default_oea["02916"]["CatCode"] = "3";
$default_oea["02916"]["CatName"] = "Physical Development";

$default_oea["02912"]["ItemCode"] = "02912";
$default_oea["02912"]["ItemName"] = "科學日";
$default_oea["02912"]["CatCode"] = "2";
$default_oea["02912"]["CatName"] = "Career-related Experience";

$default_oea["02913"]["ItemCode"] = "02913";
$default_oea["02913"]["ItemName"] = "科學為民學生專題研習比賽";
$default_oea["02913"]["CatCode"] = "2";
$default_oea["02913"]["CatName"] = "Career-related Experience";

$default_oea["02915"]["ItemCode"] = "02915";
$default_oea["02915"]["ItemName"] = "科學競賽隊";
$default_oea["02915"]["CatCode"] = "2";
$default_oea["02915"]["CatName"] = "Career-related Experience";

$default_oea["02914"]["ItemCode"] = "02914";
$default_oea["02914"]["ItemName"] = "科學英才精進計劃 Science Enrichment Programme for Secondary 3-4";
$default_oea["02914"]["CatCode"] = "2";
$default_oea["02914"]["CatName"] = "Career-related Experience";

$default_oea["03398"]["ItemCode"] = "03398";
$default_oea["03398"]["ItemName"] = "穀機設計及製作比賽";
$default_oea["03398"]["CatCode"] = "4";
$default_oea["03398"]["CatName"] = "Aesthetic Development";

$default_oea["03441"]["ItemCode"] = "03441";
$default_oea["03441"]["ItemName"] = "積極人生、貢獻社群窗櫉展覽設計比賽";
$default_oea["03441"]["CatCode"] = "4";
$default_oea["03441"]["CatName"] = "Aesthetic Development";

$default_oea["03469"]["ItemCode"] = "03469";
$default_oea["03469"]["ItemName"] = "穗港鋼琴/管弦樂比賽";
$default_oea["03469"]["CatCode"] = "4";
$default_oea["03469"]["CatName"] = "Aesthetic Development";

$default_oea["02918"]["ItemCode"] = "02918";
$default_oea["02918"]["ItemName"] = "突破大使";
$default_oea["02918"]["CatCode"] = "1";
$default_oea["02918"]["CatName"] = "Community Service";

$default_oea["02920"]["ItemCode"] = "02920";
$default_oea["02920"]["ItemName"] = "突破機構我的城市、我的家繪石籌款活動";
$default_oea["02920"]["CatCode"] = "4";
$default_oea["02920"]["CatName"] = "Aesthetic Development";

$default_oea["02919"]["ItemCode"] = "02919";
$default_oea["02919"]["ItemName"] = "突破自己獎勵計劃";
$default_oea["02919"]["CatCode"] = "1";
$default_oea["02919"]["CatName"] = "Community Service";

$default_oea["03213"]["ItemCode"] = "03213";
$default_oea["03213"]["ItemName"] = "童享安全繪畫比賽";
$default_oea["03213"]["CatCode"] = "4";
$default_oea["03213"]["CatName"] = "Aesthetic Development";

$default_oea["03218"]["ItemCode"] = "03218";
$default_oea["03218"]["ItemName"] = "童軍會室內射箭公開賽";
$default_oea["03218"]["CatCode"] = "3";
$default_oea["03218"]["CatName"] = "Physical Development";

$default_oea["03214"]["ItemCode"] = "03214";
$default_oea["03214"]["ItemName"] = "童軍服務-ELC開幕";
$default_oea["03214"]["CatCode"] = "1";
$default_oea["03214"]["CatName"] = "Community Service";

$default_oea["03215"]["ItemCode"] = "03215";
$default_oea["03215"]["ItemName"] = "童軍服務-包糭子/老人中心送糭子";
$default_oea["03215"]["CatCode"] = "1";
$default_oea["03215"]["CatName"] = "Community Service";

$default_oea["03219"]["ItemCode"] = "03219";
$default_oea["03219"]["ItemName"] = "童軍歷奇挑戰營";
$default_oea["03219"]["CatCode"] = "4";
$default_oea["03219"]["CatName"] = "Aesthetic Development";

$default_oea["03216"]["ItemCode"] = "03216";
$default_oea["03216"]["ItemName"] = "童軍野外定向練習賽";
$default_oea["03216"]["CatCode"] = "3";
$default_oea["03216"]["CatName"] = "Physical Development";

$default_oea["03217"]["ItemCode"] = "03217";
$default_oea["03217"]["ItemName"] = "童軍野外定向錦標賽";
$default_oea["03217"]["CatCode"] = "3";
$default_oea["03217"]["CatName"] = "Physical Development";

$default_oea["03084"]["ItemCode"] = "03084";
$default_oea["03084"]["ItemName"] = "笑出彩虹計劃";
$default_oea["03084"]["CatCode"] = "1";
$default_oea["03084"]["CatName"] = "Community Service";

$default_oea["03161"]["ItemCode"] = "03161";
$default_oea["03161"]["ItemName"] = "第二十五屆籃球比賽";
$default_oea["03161"]["CatCode"] = "3";
$default_oea["03161"]["CatName"] = "Physical Development";

$default_oea["03221"]["ItemCode"] = "03221";
$default_oea["03221"]["ItemName"] = "筆可能《寫意樂園》寫作班";
$default_oea["03221"]["CatCode"] = "4";
$default_oea["03221"]["CatName"] = "Aesthetic Development";

$default_oea["03222"]["ItemCode"] = "03222";
$default_oea["03222"]["ItemName"] = "筆可能文學營 - 字在山水";
$default_oea["03222"]["CatCode"] = "4";
$default_oea["03222"]["CatName"] = "Aesthetic Development";

$default_oea["03223"]["ItemCode"] = "03223";
$default_oea["03223"]["ItemName"] = "筆尖創作班 《賞名篇‧學寫作》";
$default_oea["03223"]["CatCode"] = "4";
$default_oea["03223"]["CatName"] = "Aesthetic Development";

$default_oea["03220"]["ItemCode"] = "03220";
$default_oea["03220"]["ItemName"] = "等價交換";
$default_oea["03220"]["CatCode"] = "2";
$default_oea["03220"]["CatName"] = "Career-related Experience";

$default_oea["03324"]["ItemCode"] = "03324";
$default_oea["03324"]["ItemName"] = "筲箕灣譚公誕大巡遊";
$default_oea["03324"]["CatCode"] = "4";
$default_oea["03324"]["CatName"] = "Aesthetic Development";

$default_oea["03332"]["ItemCode"] = "03332";
$default_oea["03332"]["ItemName"] = "管弦樂報佳音";
$default_oea["03332"]["CatCode"] = "4";
$default_oea["03332"]["CatName"] = "Aesthetic Development";

$default_oea["03508"]["ItemCode"] = "03508";
$default_oea["03508"]["ItemName"] = "籃球體育事工籃球籌款賽";
$default_oea["03508"]["CatCode"] = "3";
$default_oea["03508"]["CatName"] = "Physical Development";

$default_oea["03506"]["ItemCode"] = "03506";
$default_oea["03506"]["ItemName"] = "籌款活動主禮人紀念品圖畫";
$default_oea["03506"]["CatCode"] = "4";
$default_oea["03506"]["CatName"] = "Aesthetic Development";

$default_oea["03507"]["ItemCode"] = "03507";
$default_oea["03507"]["ItemName"] = "籌辦通識尖尖尖──元朗小學校際通識尖子爭霸戰";
$default_oea["03507"]["CatCode"] = "2";
$default_oea["03507"]["CatName"] = "Career-related Experience";

$default_oea["03287"]["ItemCode"] = "03287";
$default_oea["03287"]["ItemName"] = "粵劇多面賞";
$default_oea["03287"]["CatCode"] = "4";
$default_oea["03287"]["CatName"] = "Aesthetic Development";

$default_oea["03288"]["ItemCode"] = "03288";
$default_oea["03288"]["ItemName"] = "粵劇欣賞—送戲到校園";
$default_oea["03288"]["CatCode"] = "4";
$default_oea["03288"]["CatName"] = "Aesthetic Development";

$default_oea["03286"]["ItemCode"] = "03286";
$default_oea["03286"]["ItemName"] = "粵劇示範工作坊";
$default_oea["03286"]["CatCode"] = "4";
$default_oea["03286"]["CatName"] = "Aesthetic Development";

$default_oea["03285"]["ItemCode"] = "03285";
$default_oea["03285"]["ItemName"] = "粵港澳朗誦大賽";
$default_oea["03285"]["CatCode"] = "4";
$default_oea["03285"]["CatName"] = "Aesthetic Development";

$default_oea["03334"]["ItemCode"] = "03334";
$default_oea["03334"]["ItemName"] = "精神健康大使培訓";
$default_oea["03334"]["CatCode"] = "1";
$default_oea["03334"]["CatName"] = "Community Service";

$default_oea["03333"]["ItemCode"] = "03333";
$default_oea["03333"]["ItemName"] = "精英學界越野賽";
$default_oea["03333"]["CatCode"] = "3";
$default_oea["03333"]["CatName"] = "Physical Development";

$default_oea["03399"]["ItemCode"] = "03399";
$default_oea["03399"]["ItemName"] = "糊塗戲班 《爆谷殺人夜》";
$default_oea["03399"]["CatCode"] = "4";
$default_oea["03399"]["CatName"] = "Aesthetic Development";

$default_oea["02803"]["ItemCode"] = "02803";
$default_oea["02803"]["ItemName"] = "系統管理學會";
$default_oea["02803"]["CatCode"] = "2";
$default_oea["02803"]["CatName"] = "Career-related Experience";

$default_oea["02849"]["ItemCode"] = "02849";
$default_oea["02849"]["ItemName"] = "糾正懶音證書課程";
$default_oea["02849"]["CatCode"] = "2";
$default_oea["02849"]["CatName"] = "Career-related Experience";

$default_oea["02922"]["ItemCode"] = "02922";
$default_oea["02922"]["ItemName"] = "紀律部隊影子工作計劃";
$default_oea["02922"]["CatCode"] = "2";
$default_oea["02922"]["CatName"] = "Career-related Experience";

$default_oea["02921"]["ItemCode"] = "02921";
$default_oea["02921"]["ItemName"] = "紀念五四運動九十一週年大埔區中學生辯論比賽";
$default_oea["02921"]["CatCode"] = "5";
$default_oea["02921"]["CatName"] = "Moral and Civic Education";

$default_oea["03085"]["ItemCode"] = "03085";
$default_oea["03085"]["ItemName"] = "素質教育杯全國中小學生作品大賽";
$default_oea["03085"]["CatCode"] = "4";
$default_oea["03085"]["CatName"] = "Aesthetic Development";

$default_oea["03086"]["ItemCode"] = "03086";
$default_oea["03086"]["ItemName"] = "索氣大作戰：一起追蹤健康空氣";
$default_oea["03086"]["CatCode"] = "5";
$default_oea["03086"]["CatName"] = "Moral and Civic Education";

$default_oea["03224"]["ItemCode"] = "03224";
$default_oea["03224"]["ItemName"] = "給學生的魔笛歌劇表演";
$default_oea["03224"]["CatCode"] = "4";
$default_oea["03224"]["CatName"] = "Aesthetic Development";

$default_oea["03289"]["ItemCode"] = "03289";
$default_oea["03289"]["ItemName"] = "經學要義演講系列 (香港浸會大學舉辦)";
$default_oea["03289"]["CatCode"] = "5";
$default_oea["03289"]["CatName"] = "Moral and Civic Education";

$default_oea["03335"]["ItemCode"] = "03335";
$default_oea["03335"]["ItemName"] = "綜藝晚會";
$default_oea["03335"]["CatCode"] = "4";
$default_oea["03335"]["CatName"] = "Aesthetic Development";

$default_oea["03341"]["ItemCode"] = "03341";
$default_oea["03341"]["ItemName"] = "綠星國際少年兒童美術書法攝影大賽";
$default_oea["03341"]["CatCode"] = "4";
$default_oea["03341"]["CatName"] = "Aesthetic Development";

$default_oea["03336"]["ItemCode"] = "03336";
$default_oea["03336"]["ItemName"] = "綠生活攝影比賽";
$default_oea["03336"]["CatCode"] = "4";
$default_oea["03336"]["CatName"] = "Aesthetic Development";

$default_oea["03337"]["ItemCode"] = "03337";
$default_oea["03337"]["ItemName"] = "綠色力量放駕一天減碳行動電子邀請卡設計比賽";
$default_oea["03337"]["CatCode"] = "4";
$default_oea["03337"]["CatName"] = "Aesthetic Development";

$default_oea["03339"]["ItemCode"] = "03339";
$default_oea["03339"]["ItemName"] = "綠色家庭在和樂";
$default_oea["03339"]["CatCode"] = "5";
$default_oea["03339"]["CatName"] = "Moral and Civic Education";

$default_oea["03340"]["ItemCode"] = "03340";
$default_oea["03340"]["ItemName"] = "綠色旅遊專題研習比賽";
$default_oea["03340"]["CatCode"] = "4";
$default_oea["03340"]["CatName"] = "Aesthetic Development";

$default_oea["03338"]["ItemCode"] = "03338";
$default_oea["03338"]["ItemName"] = "綠色生活攝影比賽";
$default_oea["03338"]["CatCode"] = "4";
$default_oea["03338"]["CatName"] = "Aesthetic Development";

$default_oea["03496"]["ItemCode"] = "03496";
$default_oea["03496"]["ItemName"] = "羅桂祥盃:中學射箭邀請賽";
$default_oea["03496"]["CatCode"] = "4";
$default_oea["03496"]["CatName"] = "Aesthetic Development";

$default_oea["03497"]["ItemCode"] = "03497";
$default_oea["03497"]["ItemName"] = "羅浮宮雕塑全接觸藝術教育展";
$default_oea["03497"]["CatCode"] = "4";
$default_oea["03497"]["CatName"] = "Aesthetic Development";

$default_oea["02758"]["ItemCode"] = "02758";
$default_oea["02758"]["ItemName"] = "羊城晚報手抄報創作大賽(全國決賽)";
$default_oea["02758"]["CatCode"] = "4";
$default_oea["02758"]["CatName"] = "Aesthetic Development";

$default_oea["02923"]["ItemCode"] = "02923";
$default_oea["02923"]["ItemName"] = "美術証書課程";
$default_oea["02923"]["CatCode"] = "4";
$default_oea["02923"]["CatName"] = "Aesthetic Development";

$default_oea["03293"]["ItemCode"] = "03293";
$default_oea["03293"]["ItemName"] = "義不容遲三響炮";
$default_oea["03293"]["CatCode"] = "1";
$default_oea["03293"]["CatName"] = "Community Service";

$default_oea["03290"]["ItemCode"] = "03290";
$default_oea["03290"]["ItemName"] = "義人行";
$default_oea["03290"]["CatCode"] = "1";
$default_oea["03290"]["CatName"] = "Community Service";

$default_oea["03294"]["ItemCode"] = "03294";
$default_oea["03294"]["ItemName"] = "義務工作50小時";
$default_oea["03294"]["CatCode"] = "1";
$default_oea["03294"]["CatName"] = "Community Service";

$default_oea["03296"]["ItemCode"] = "03296";
$default_oea["03296"]["ItemName"] = "義務工作嘉許金狀";
$default_oea["03296"]["CatCode"] = "1";
$default_oea["03296"]["CatName"] = "Community Service";

$default_oea["03295"]["ItemCode"] = "03295";
$default_oea["03295"]["ItemName"] = "義務工作發展局之長者義工技巧班";
$default_oea["03295"]["CatCode"] = "1";
$default_oea["03295"]["CatName"] = "Community Service";

$default_oea["03292"]["ItemCode"] = "03292";
$default_oea["03292"]["ItemName"] = "義工服務嘉許計劃";
$default_oea["03292"]["CatCode"] = "1";
$default_oea["03292"]["CatName"] = "Community Service";

$default_oea["03291"]["ItemCode"] = "03291";
$default_oea["03291"]["ItemName"] = "義工服務日與深化小姐";
$default_oea["03291"]["CatCode"] = "1";
$default_oea["03291"]["CatName"] = "Community Service";

$default_oea["03297"]["ItemCode"] = "03297";
$default_oea["03297"]["ItemName"] = "聖誕咭設計比賽";
$default_oea["03297"]["CatCode"] = "4";
$default_oea["03297"]["CatName"] = "Aesthetic Development";

$default_oea["03471"]["ItemCode"] = "03471";
$default_oea["03471"]["ItemName"] = "聯校創意旅遊企業訓練計劃";
$default_oea["03471"]["CatCode"] = "2";
$default_oea["03471"]["CatName"] = "Career-related Experience";

$default_oea["03475"]["ItemCode"] = "03475";
$default_oea["03475"]["ItemName"] = "聯校模型滑翔機飛行大賽";
$default_oea["03475"]["CatCode"] = "2";
$default_oea["03475"]["CatName"] = "Career-related Experience";

$default_oea["03473"]["ItemCode"] = "03473";
$default_oea["03473"]["ItemName"] = "聯校滑浪風帆比賽";
$default_oea["03473"]["CatCode"] = "3";
$default_oea["03473"]["CatName"] = "Physical Development";

$default_oea["03476"]["ItemCode"] = "03476";
$default_oea["03476"]["ItemName"] = "聯校籃球比賽";
$default_oea["03476"]["CatCode"] = "3";
$default_oea["03476"]["CatName"] = "Physical Development";

$default_oea["03470"]["ItemCode"] = "03470";
$default_oea["03470"]["ItemName"] = "聯校英語演說工作坊及比賽";
$default_oea["03470"]["CatCode"] = "4";
$default_oea["03470"]["CatName"] = "Aesthetic Development";

$default_oea["03472"]["ItemCode"] = "03472";
$default_oea["03472"]["ItemName"] = "聯校跆拳道錦標賽";
$default_oea["03472"]["CatCode"] = "3";
$default_oea["03472"]["CatName"] = "Physical Development";

$default_oea["03474"]["ItemCode"] = "03474";
$default_oea["03474"]["ItemName"] = "聯校運動會";
$default_oea["03474"]["CatCode"] = "3";
$default_oea["03474"]["CatName"] = "Physical Development";

$default_oea["03477"]["ItemCode"] = "03477";
$default_oea["03477"]["ItemName"] = "聯課活動之星";
$default_oea["03477"]["CatCode"] = "4";
$default_oea["03477"]["CatName"] = "Aesthetic Development";

$default_oea["03485"]["ItemCode"] = "03485";
$default_oea["03485"]["ItemName"] = "職安四格動手畫漫畫比賽";
$default_oea["03485"]["CatCode"] = "4";
$default_oea["03485"]["CatName"] = "Aesthetic Development";

$default_oea["03486"]["ItemCode"] = "03486";
$default_oea["03486"]["ItemName"] = "職業分享﹙機艙服務員、護士﹚";
$default_oea["03486"]["CatCode"] = "2";
$default_oea["03486"]["CatName"] = "Career-related Experience";

$default_oea["03487"]["ItemCode"] = "03487";
$default_oea["03487"]["ItemName"] = "職業安全健康局化學品安全網上遊戲設計比賽";
$default_oea["03487"]["CatCode"] = "4";
$default_oea["03487"]["CatName"] = "Aesthetic Development";

$default_oea["02760"]["ItemCode"] = "02760";
$default_oea["02760"]["ItemName"] = "自我健康管理課程-中醫秋冬食療保健湯水";
$default_oea["02760"]["CatCode"] = "4";
$default_oea["02760"]["CatName"] = "Aesthetic Development";

$default_oea["02761"]["ItemCode"] = "02761";
$default_oea["02761"]["ItemName"] = "自然足印低碳生活宣傳短片創作比賽";
$default_oea["02761"]["CatCode"] = "4";
$default_oea["02761"]["CatName"] = "Aesthetic Development";

$default_oea["02759"]["ItemCode"] = "02759";
$default_oea["02759"]["ItemName"] = "自由盃全港中學辯論比賽";
$default_oea["02759"]["CatCode"] = "5";
$default_oea["02759"]["CatName"] = "Moral and Civic Education";

$default_oea["02762"]["ItemCode"] = "02762";
$default_oea["02762"]["ItemName"] = "至叻環保小先鋒 活在美好地球中";
$default_oea["02762"]["CatCode"] = "5";
$default_oea["02762"]["CatName"] = "Moral and Civic Education";

$default_oea["03342"]["ItemCode"] = "03342";
$default_oea["03342"]["ItemName"] = "臺北國際柔道公開賽";
$default_oea["03342"]["CatCode"] = "3";
$default_oea["03342"]["CatName"] = "Physical Development";

$default_oea["03344"]["ItemCode"] = "03344";
$default_oea["03344"]["ItemName"] = "與作家會面－品讀城市講座：海外印象－我的浪遊隨筆";
$default_oea["03344"]["CatCode"] = "4";
$default_oea["03344"]["CatName"] = "Aesthetic Development";

$default_oea["03349"]["ItemCode"] = "03349";
$default_oea["03349"]["ItemName"] = "與劉慧郷議員談人權及法治";
$default_oea["03349"]["CatCode"] = "5";
$default_oea["03349"]["CatName"] = "Moral and Civic Education";

$default_oea["03343"]["ItemCode"] = "03343";
$default_oea["03343"]["ItemName"] = "與名人對談系列－透過藝術培育學生的健康成長分享會";
$default_oea["03343"]["CatCode"] = "5";
$default_oea["03343"]["CatName"] = "Moral and Civic Education";

$default_oea["03345"]["ItemCode"] = "03345";
$default_oea["03345"]["ItemName"] = "與校友對話，分享工作相關經驗";
$default_oea["03345"]["CatCode"] = "2";
$default_oea["03345"]["CatName"] = "Career-related Experience";

$default_oea["03346"]["ItemCode"] = "03346";
$default_oea["03346"]["ItemName"] = "與校長真情對話";
$default_oea["03346"]["CatCode"] = "5";
$default_oea["03346"]["CatName"] = "Moral and Civic Education";

$default_oea["03347"]["ItemCode"] = "03347";
$default_oea["03347"]["ItemName"] = "與眾同藝公眾教育計劃";
$default_oea["03347"]["CatCode"] = "5";
$default_oea["03347"]["CatName"] = "Moral and Civic Education";

$default_oea["03348"]["ItemCode"] = "03348";
$default_oea["03348"]["ItemName"] = "與鳥共舞雀鳥帽子設計比賽";
$default_oea["03348"]["CatCode"] = "4";
$default_oea["03348"]["CatName"] = "Aesthetic Development";

$default_oea["03226"]["ItemCode"] = "03226";
$default_oea["03226"]["ItemName"] = "舒曼知多少?";
$default_oea["03226"]["CatCode"] = "4";
$default_oea["03226"]["CatName"] = "Aesthetic Development";

$default_oea["03350"]["ItemCode"] = "03350";
$default_oea["03350"]["ItemName"] = "舞台劇《金鎖記》";
$default_oea["03350"]["CatCode"] = "4";
$default_oea["03350"]["CatName"] = "Aesthetic Development";

$default_oea["03351"]["ItemCode"] = "03351";
$default_oea["03351"]["ItemName"] = "舞戲劇場體驗日";
$default_oea["03351"]["CatCode"] = "4";
$default_oea["03351"]["CatName"] = "Aesthetic Development";

$default_oea["03352"]["ItemCode"] = "03352";
$default_oea["03352"]["ItemName"] = "舞蹈欣賞 — 童話舞林";
$default_oea["03352"]["CatCode"] = "4";
$default_oea["03352"]["CatName"] = "Aesthetic Development";

$default_oea["03087"]["ItemCode"] = "03087";
$default_oea["03087"]["ItemName"] = "航空新體驗";
$default_oea["03087"]["CatCode"] = "2";
$default_oea["03087"]["CatName"] = "Career-related Experience";

$default_oea["02764"]["ItemCode"] = "02764";
$default_oea["02764"]["ItemName"] = "色情文化對青少年成長的影響";
$default_oea["02764"]["CatCode"] = "5";
$default_oea["02764"]["CatName"] = "Moral and Civic Education";

$default_oea["02763"]["ItemCode"] = "02763";
$default_oea["02763"]["ItemName"] = "色級野外定向賽";
$default_oea["02763"]["CatCode"] = "3";
$default_oea["02763"]["CatName"] = "Physical Development";

$default_oea["02850"]["ItemCode"] = "02850";
$default_oea["02850"]["ItemName"] = "芭蕾舞欣賞";
$default_oea["02850"]["CatCode"] = "4";
$default_oea["02850"]["CatName"] = "Aesthetic Development";

$default_oea["02851"]["ItemCode"] = "02851";
$default_oea["02851"]["ItemName"] = "花卉繪畫工作坊";
$default_oea["02851"]["CatCode"] = "4";
$default_oea["02851"]["CatName"] = "Aesthetic Development";

$default_oea["02852"]["ItemCode"] = "02852";
$default_oea["02852"]["ItemName"] = "花旗集團財智星級爭霸戰";
$default_oea["02852"]["CatCode"] = "2";
$default_oea["02852"]["CatName"] = "Career-related Experience";

$default_oea["02853"]["ItemCode"] = "02853";
$default_oea["02853"]["ItemName"] = "花旗集團財智星級爭霸戰之少年投資精英杯";
$default_oea["02853"]["CatCode"] = "2";
$default_oea["02853"]["CatName"] = "Career-related Experience";

$default_oea["02925"]["ItemCode"] = "02925";
$default_oea["02925"]["ItemName"] = "苗圃行動";
$default_oea["02925"]["CatCode"] = "1";
$default_oea["02925"]["CatName"] = "Community Service";

$default_oea["02924"]["ItemCode"] = "02924";
$default_oea["02924"]["ItemName"] = "若瑟義工teen地";
$default_oea["02924"]["CatCode"] = "1";
$default_oea["02924"]["CatName"] = "Community Service";

$default_oea["02926"]["ItemCode"] = "02926";
$default_oea["02926"]["ItemName"] = "英文民歌小組";
$default_oea["02926"]["CatCode"] = "4";
$default_oea["02926"]["CatName"] = "Aesthetic Development";

$default_oea["02929"]["ItemCode"] = "02929";
$default_oea["02929"]["ItemName"] = "英語魔術表演";
$default_oea["02929"]["CatCode"] = "4";
$default_oea["02929"]["CatName"] = "Aesthetic Development";

$default_oea["03088"]["ItemCode"] = "03088";
$default_oea["03088"]["ItemName"] = "茶報（Trouble) 投搞者";
$default_oea["03088"]["CatCode"] = "4";
$default_oea["03088"]["CatName"] = "Aesthetic Development";

$default_oea["03089"]["ItemCode"] = "03089";
$default_oea["03089"]["ItemName"] = "荃情有您";
$default_oea["03089"]["CatCode"] = "1";
$default_oea["03089"]["CatName"] = "Community Service";

$default_oea["03090"]["ItemCode"] = "03090";
$default_oea["03090"]["ItemName"] = "荃葵青區傑出學生會辯論比賽";
$default_oea["03090"]["CatCode"] = "2";
$default_oea["03090"]["CatName"] = "Career-related Experience";

$default_oea["03227"]["ItemCode"] = "03227";
$default_oea["03227"]["ItemName"] = "華人少年作文比賽";
$default_oea["03227"]["CatCode"] = "4";
$default_oea["03227"]["CatName"] = "Aesthetic Development";

$default_oea["03230"]["ItemCode"] = "03230";
$default_oea["03230"]["ItemName"] = "華夏杯全國少兒書畫大賽";
$default_oea["03230"]["CatCode"] = "4";
$default_oea["03230"]["CatName"] = "Aesthetic Development";

$default_oea["03231"]["ItemCode"] = "03231";
$default_oea["03231"]["ItemName"] = "華廈盃朗誦比賽";
$default_oea["03231"]["CatCode"] = "4";
$default_oea["03231"]["CatName"] = "Aesthetic Development";

$default_oea["03228"]["ItemCode"] = "03228";
$default_oea["03228"]["ItemName"] = "華東師大二附中杯作文通訊全國中學生作文大賽";
$default_oea["03228"]["CatCode"] = "4";
$default_oea["03228"]["CatName"] = "Aesthetic Development";

$default_oea["03229"]["ItemCode"] = "03229";
$default_oea["03229"]["ItemName"] = "華苗薈《跨難》全港中學徵文比賽";
$default_oea["03229"]["CatCode"] = "4";
$default_oea["03229"]["CatName"] = "Aesthetic Development";

$default_oea["03302"]["ItemCode"] = "03302";
$default_oea["03302"]["ItemName"] = "葉脈書簽製作比賽";
$default_oea["03302"]["CatCode"] = "4";
$default_oea["03302"]["CatName"] = "Aesthetic Development";

$default_oea["03303"]["ItemCode"] = "03303";
$default_oea["03303"]["ItemName"] = "董建華先生與青年學生專題分享會";
$default_oea["03303"]["CatCode"] = "2";
$default_oea["03303"]["CatName"] = "Career-related Experience";

$default_oea["03299"]["ItemCode"] = "03299";
$default_oea["03299"]["ItemName"] = "葵青區中國象棋比賽";
$default_oea["03299"]["CatCode"] = "4";
$default_oea["03299"]["CatName"] = "Aesthetic Development";

$default_oea["03298"]["ItemCode"] = "03298";
$default_oea["03298"]["ItemName"] = "葵青區中文書法比賽";
$default_oea["03298"]["CatCode"] = "4";
$default_oea["03298"]["CatName"] = "Aesthetic Development";

$default_oea["03300"]["ItemCode"] = "03300";
$default_oea["03300"]["ItemName"] = "葵青區分齡游泳比賽";
$default_oea["03300"]["CatCode"] = "3";
$default_oea["03300"]["CatName"] = "Physical Development";

$default_oea["03301"]["ItemCode"] = "03301";
$default_oea["03301"]["ItemName"] = "葵青區青少年社區服務計劃比賽";
$default_oea["03301"]["CatCode"] = "1";
$default_oea["03301"]["CatName"] = "Community Service";

$default_oea["03478"]["ItemCode"] = "03478";
$default_oea["03478"]["ItemName"] = "薄扶林郊野公園及山頂環回步行徑 (盧吉道)遠足活動";
$default_oea["03478"]["CatCode"] = "3";
$default_oea["03478"]["CatName"] = "Physical Development";

$default_oea["03483"]["ItemCode"] = "03483";
$default_oea["03483"]["ItemName"] = "薈萃館藝術比賽";
$default_oea["03483"]["CatCode"] = "4";
$default_oea["03483"]["CatName"] = "Aesthetic Development";

$default_oea["03488"]["ItemCode"] = "03488";
$default_oea["03488"]["ItemName"] = "藍天一二大行動錄像製作活動及工作";
$default_oea["03488"]["CatCode"] = "4";
$default_oea["03488"]["CatName"] = "Aesthetic Development";

$default_oea["03489"]["ItemCode"] = "03489";
$default_oea["03489"]["ItemName"] = "藍天綠地在香港-全港學界海報設計比賽";
$default_oea["03489"]["CatCode"] = "4";
$default_oea["03489"]["CatName"] = "Aesthetic Development";

$default_oea["03490"]["ItemCode"] = "03490";
$default_oea["03490"]["ItemName"] = "藍天綠地在香港設計系列2009之全港學界板畫設計比賽";
$default_oea["03490"]["CatCode"] = "4";
$default_oea["03490"]["CatName"] = "Aesthetic Development";

$default_oea["03499"]["ItemCode"] = "03499";
$default_oea["03499"]["ItemName"] = "藝術家駐校計劃";
$default_oea["03499"]["CatCode"] = "4";
$default_oea["03499"]["CatName"] = "Aesthetic Development";

$default_oea["03500"]["ItemCode"] = "03500";
$default_oea["03500"]["ItemName"] = "藝術發展局18區id設計比賽";
$default_oea["03500"]["CatCode"] = "4";
$default_oea["03500"]["CatName"] = "Aesthetic Development";

$default_oea["03501"]["ItemCode"] = "03501";
$default_oea["03501"]["ItemName"] = "藝術發展計劃－戲劇班";
$default_oea["03501"]["CatCode"] = "4";
$default_oea["03501"]["CatName"] = "Aesthetic Development";

$default_oea["03498"]["ItemCode"] = "03498";
$default_oea["03498"]["ItemName"] = "藝術表演及工作坊";
$default_oea["03498"]["CatCode"] = "4";
$default_oea["03498"]["CatName"] = "Aesthetic Development";

$default_oea["03442"]["ItemCode"] = "03442";
$default_oea["03442"]["ItemName"] = "融之友聯校排球賽";
$default_oea["03442"]["CatCode"] = "3";
$default_oea["03442"]["CatName"] = "Physical Development";

$default_oea["03443"]["ItemCode"] = "03443";
$default_oea["03443"]["ItemName"] = "融融樂樂溫馨家庭攝影比賽";
$default_oea["03443"]["CatCode"] = "4";
$default_oea["03443"]["CatName"] = "Aesthetic Development";

$default_oea["02765"]["ItemCode"] = "02765";
$default_oea["02765"]["ItemName"] = "行行出狀元問答比賽";
$default_oea["02765"]["CatCode"] = "2";
$default_oea["02765"]["CatName"] = "Career-related Experience";

$default_oea["02855"]["ItemCode"] = "02855";
$default_oea["02855"]["ItemName"] = "表演藝術自僱系列—雜耍";
$default_oea["02855"]["CatCode"] = "4";
$default_oea["02855"]["CatName"] = "Aesthetic Development";

$default_oea["02854"]["ItemCode"] = "02854";
$default_oea["02854"]["ItemName"] = "表演賞.想舞蹈表演";
$default_oea["02854"]["CatCode"] = "4";
$default_oea["02854"]["CatName"] = "Aesthetic Development";

$default_oea["02766"]["ItemCode"] = "02766";
$default_oea["02766"]["ItemName"] = "西式食品製作課程";
$default_oea["02766"]["CatCode"] = "4";
$default_oea["02766"]["CatName"] = "Aesthetic Development";

$default_oea["03233"]["ItemCode"] = "03233";
$default_oea["03233"]["ItemName"] = "視聽器材組";
$default_oea["03233"]["CatCode"] = "4";
$default_oea["03233"]["CatName"] = "Aesthetic Development";

$default_oea["03232"]["ItemCode"] = "03232";
$default_oea["03232"]["ItemName"] = "視藝OLE";
$default_oea["03232"]["CatCode"] = "4";
$default_oea["03232"]["CatName"] = "Aesthetic Development";

$default_oea["03444"]["ItemCode"] = "03444";
$default_oea["03444"]["ItemName"] = "親親木偶－－演出暨互動工作坊";
$default_oea["03444"]["CatCode"] = "4";
$default_oea["03444"]["CatName"] = "Aesthetic Development";

$default_oea["03523"]["ItemCode"] = "03523";
$default_oea["03523"]["ItemName"] = "觀塘區國民教育電影欣賞會";
$default_oea["03523"]["CatCode"] = "5";
$default_oea["03523"]["CatName"] = "Moral and Civic Education";

$default_oea["03524"]["ItemCode"] = "03524";
$default_oea["03524"]["ItemName"] = "觀塘區國際復康日嘉年華";
$default_oea["03524"]["CatCode"] = "4";
$default_oea["03524"]["CatName"] = "Aesthetic Development";

$default_oea["03520"]["ItemCode"] = "03520";
$default_oea["03520"]["ItemName"] = "觀塘區小學中文演說比賽";
$default_oea["03520"]["CatCode"] = "4";
$default_oea["03520"]["CatName"] = "Aesthetic Development";

$default_oea["03521"]["ItemCode"] = "03521";
$default_oea["03521"]["ItemName"] = "觀塘區曲藝比賽少年組";
$default_oea["03521"]["CatCode"] = "4";
$default_oea["03521"]["CatName"] = "Aesthetic Development";

$default_oea["03522"]["ItemCode"] = "03522";
$default_oea["03522"]["ItemName"] = "觀塘區社區植樹日";
$default_oea["03522"]["CatCode"] = "1";
$default_oea["03522"]["CatName"] = "Community Service";

$default_oea["03519"]["ItemCode"] = "03519";
$default_oea["03519"]["ItemName"] = "觀塘海岸光‧影‧情短片比賽頒獎禮";
$default_oea["03519"]["CatCode"] = "4";
$default_oea["03519"]["CatName"] = "Aesthetic Development";

$default_oea["03525"]["ItemCode"] = "03525";
$default_oea["03525"]["ItemName"] = "觀塘節賀國慶";
$default_oea["03525"]["CatCode"] = "5";
$default_oea["03525"]["CatName"] = "Moral and Civic Education";

$default_oea["03526"]["ItemCode"] = "03526";
$default_oea["03526"]["ItemName"] = "觀塘變變變社區工作坊";
$default_oea["03526"]["CatCode"] = "1";
$default_oea["03526"]["CatName"] = "Community Service";

$default_oea["03518"]["ItemCode"] = "03518";
$default_oea["03518"]["ItemName"] = "觀星體驗營";
$default_oea["03518"]["CatCode"] = "4";
$default_oea["03518"]["CatName"] = "Aesthetic Development";

$default_oea["03530"]["ItemCode"] = "03530";
$default_oea["03530"]["ItemName"] = "觀瀾湖高爾夫球會維杰球場－觀瀾湖青少年系列賽";
$default_oea["03530"]["CatCode"] = "3";
$default_oea["03530"]["CatName"] = "Physical Development";

$default_oea["03527"]["ItemCode"] = "03527";
$default_oea["03527"]["ItemName"] = "觀賞中學校際籃球比賽";
$default_oea["03527"]["CatCode"] = "3";
$default_oea["03527"]["CatName"] = "Physical Development";

$default_oea["03528"]["ItemCode"] = "03528";
$default_oea["03528"]["ItemName"] = "觀賞學界足球比賽";
$default_oea["03528"]["CatCode"] = "3";
$default_oea["03528"]["CatName"] = "Physical Development";

$default_oea["03529"]["ItemCode"] = "03529";
$default_oea["03529"]["ItemName"] = "觀龍樓愛心大掃除暨環保為公益";
$default_oea["03529"]["CatCode"] = "1";
$default_oea["03529"]["CatName"] = "Community Service";

$default_oea["03162"]["ItemCode"] = "03162";
$default_oea["03162"]["ItemName"] = "設計出路2011設計及創意教育博覽";
$default_oea["03162"]["CatCode"] = "4";
$default_oea["03162"]["CatName"] = "Aesthetic Development";

$default_oea["03163"]["ItemCode"] = "03163";
$default_oea["03163"]["ItemName"] = "設計與科技興趣小組";
$default_oea["03163"]["CatCode"] = "4";
$default_oea["03163"]["CatName"] = "Aesthetic Development";

$default_oea["03164"]["ItemCode"] = "03164";
$default_oea["03164"]["ItemName"] = "設計與視覺傳意課程";
$default_oea["03164"]["CatCode"] = "4";
$default_oea["03164"]["CatName"] = "Aesthetic Development";

$default_oea["03304"]["ItemCode"] = "03304";
$default_oea["03304"]["ItemName"] = "試後活動 - 科學創意工作坊";
$default_oea["03304"]["CatCode"] = "4";
$default_oea["03304"]["CatName"] = "Aesthetic Development";

$default_oea["03305"]["ItemCode"] = "03305";
$default_oea["03305"]["ItemName"] = "試後活動 - 茶藝工作坊";
$default_oea["03305"]["CatCode"] = "4";
$default_oea["03305"]["CatName"] = "Aesthetic Development";

$default_oea["03306"]["ItemCode"] = "03306";
$default_oea["03306"]["ItemName"] = "試後活動 - 話劇欣賞及中文口語才藝比賽";
$default_oea["03306"]["CatCode"] = "4";
$default_oea["03306"]["CatName"] = "Aesthetic Development";

$default_oea["03307"]["ItemCode"] = "03307";
$default_oea["03307"]["ItemName"] = "試後活動《看到真實》互動舞蹈表演";
$default_oea["03307"]["CatCode"] = "4";
$default_oea["03307"]["CatName"] = "Aesthetic Development";

$default_oea["03308"]["ItemCode"] = "03308";
$default_oea["03308"]["ItemName"] = "話劇 《豆泥戰爭》";
$default_oea["03308"]["CatCode"] = "4";
$default_oea["03308"]["CatName"] = "Aesthetic Development";

$default_oea["03356"]["ItemCode"] = "03356";
$default_oea["03356"]["ItemName"] = "認識基本法‧齊頌祖國心徵文比賽";
$default_oea["03356"]["CatCode"] = "5";
$default_oea["03356"]["CatName"] = "Moral and Civic Education";

$default_oea["03357"]["ItemCode"] = "03357";
$default_oea["03357"]["ItemName"] = "認識愛滋病";
$default_oea["03357"]["CatCode"] = "5";
$default_oea["03357"]["CatName"] = "Moral and Civic Education";

$default_oea["03355"]["ItemCode"] = "03355";
$default_oea["03355"]["ItemName"] = "認識祖國、認識香港隊際問答比賽";
$default_oea["03355"]["CatCode"] = "5";
$default_oea["03355"]["CatName"] = "Moral and Civic Education";

$default_oea["03358"]["ItemCode"] = "03358";
$default_oea["03358"]["ItemName"] = "認識資歷架構下的課程及各行業的資訊";
$default_oea["03358"]["CatCode"] = "2";
$default_oea["03358"]["CatName"] = "Career-related Experience";

$default_oea["03354"]["ItemCode"] = "03354";
$default_oea["03354"]["ItemName"] = "認識香港法律 - 應用學習導引課程";
$default_oea["03354"]["CatCode"] = "2";
$default_oea["03354"]["CatName"] = "Career-related Experience";

$default_oea["03353"]["ItemCode"] = "03353";
$default_oea["03353"]["ItemName"] = "語常會初中辯論比賽";
$default_oea["03353"]["CatCode"] = "5";
$default_oea["03353"]["CatName"] = "Moral and Civic Education";

$default_oea["03359"]["ItemCode"] = "03359";
$default_oea["03359"]["ItemName"] = "說話工作坊：我也能成為說話高手";
$default_oea["03359"]["CatCode"] = "4";
$default_oea["03359"]["CatName"] = "Aesthetic Development";

$default_oea["03400"]["ItemCode"] = "03400";
$default_oea["03400"]["ItemName"] = "談Teen說性 - 交友與戀愛";
$default_oea["03400"]["CatCode"] = "5";
$default_oea["03400"]["CatName"] = "Moral and Civic Education";

$default_oea["03502"]["ItemCode"] = "03502";
$default_oea["03502"]["ItemName"] = "譚伯羽盃中學校際射箭錦標賽";
$default_oea["03502"]["CatCode"] = "3";
$default_oea["03502"]["CatName"] = "Physical Development";

$default_oea["03515"]["ItemCode"] = "03515";
$default_oea["03515"]["ItemName"] = "讓學生成為學習經歷設計者計劃";
$default_oea["03515"]["CatCode"] = "4";
$default_oea["03515"]["CatName"] = "Aesthetic Development";

$default_oea["03093"]["ItemCode"] = "03093";
$default_oea["03093"]["ItemName"] = "財富與人生錄像短片創作比賽";
$default_oea["03093"]["CatCode"] = "5";
$default_oea["03093"]["CatName"] = "Moral and Civic Education";

$default_oea["03091"]["ItemCode"] = "03091";
$default_oea["03091"]["ItemName"] = "財政司司長與青年真情對話";
$default_oea["03091"]["CatCode"] = "2";
$default_oea["03091"]["CatName"] = "Career-related Experience";

$default_oea["03092"]["ItemCode"] = "03092";
$default_oea["03092"]["ItemName"] = "財政預算案論壇";
$default_oea["03092"]["CatCode"] = "2";
$default_oea["03092"]["CatName"] = "Career-related Experience";

$default_oea["03094"]["ItemCode"] = "03094";
$default_oea["03094"]["ItemName"] = "財智Goal 飛";
$default_oea["03094"]["CatCode"] = "2";
$default_oea["03094"]["CatName"] = "Career-related Experience";

$default_oea["03095"]["ItemCode"] = "03095";
$default_oea["03095"]["ItemName"] = "財智Goal教師社區飛協作計劃之誰決定你的消費策略分析師生研討會";
$default_oea["03095"]["CatCode"] = "2";
$default_oea["03095"]["CatName"] = "Career-related Experience";

$default_oea["03165"]["ItemCode"] = "03165";
$default_oea["03165"]["ItemName"] = "貧富宴";
$default_oea["03165"]["CatCode"] = "1";
$default_oea["03165"]["CatName"] = "Community Service";

$default_oea["03309"]["ItemCode"] = "03309";
$default_oea["03309"]["ItemName"] = "資訊娛樂節目製作-應用學習導引課程";
$default_oea["03309"]["CatCode"] = "4";
$default_oea["03309"]["CatName"] = "Aesthetic Development";

$default_oea["03480"]["ItemCode"] = "03480";
$default_oea["03480"]["ItemName"] = "賽艇推廣計劃─星級賽艇課程";
$default_oea["03480"]["CatCode"] = "3";
$default_oea["03480"]["CatName"] = "Physical Development";

$default_oea["03479"]["ItemCode"] = "03479";
$default_oea["03479"]["ItemName"] = "賽馬會演藝推廣計劃之掌聲背後電影欣賞";
$default_oea["03479"]["CatCode"] = "4";
$default_oea["03479"]["CatName"] = "Aesthetic Development";

$default_oea["02807"]["ItemCode"] = "02807";
$default_oea["02807"]["ItemName"] = "走進廣東友和杯海峽兩岸書畫大展";
$default_oea["02807"]["CatCode"] = "4";
$default_oea["02807"]["CatName"] = "Aesthetic Development";

$default_oea["02804"]["ItemCode"] = "02804";
$default_oea["02804"]["ItemName"] = "走進美妙的數學花園中國青少年數學論壇 — 香港青少年數學比賽";
$default_oea["02804"]["CatCode"] = "2";
$default_oea["02804"]["CatName"] = "Career-related Experience";

$default_oea["02805"]["ItemCode"] = "02805";
$default_oea["02805"]["ItemName"] = "走進美妙的數學花園中國青少年數學論壇-發現之旅圖形計算器探究展示活動";
$default_oea["02805"]["CatCode"] = "2";
$default_oea["02805"]["CatName"] = "Career-related Experience";

$default_oea["02806"]["ItemCode"] = "02806";
$default_oea["02806"]["ItemName"] = "走進香港文學風景(一):與小思老師對談及中大校園文學景點賞覽";
$default_oea["02806"]["CatCode"] = "4";
$default_oea["02806"]["CatName"] = "Aesthetic Development";

$default_oea["03235"]["ItemCode"] = "03235";
$default_oea["03235"]["ItemName"] = "超級工商盃籃球賽";
$default_oea["03235"]["CatCode"] = "3";
$default_oea["03235"]["CatName"] = "Physical Development";

$default_oea["03234"]["ItemCode"] = "03234";
$default_oea["03234"]["ItemName"] = "越野長跑日";
$default_oea["03234"]["CatCode"] = "3";
$default_oea["03234"]["CatName"] = "Physical Development";

$default_oea["03360"]["ItemCode"] = "03360";
$default_oea["03360"]["ItemName"] = "趙聿修紀念中學初中數學邀請賽";
$default_oea["03360"]["CatCode"] = "2";
$default_oea["03360"]["CatName"] = "Career-related Experience";

$default_oea["03401"]["ItemCode"] = "03401";
$default_oea["03401"]["ItemName"] = "趣味科學比賽之深谷還珠";
$default_oea["03401"]["CatCode"] = "4";
$default_oea["03401"]["CatName"] = "Aesthetic Development";

$default_oea["03402"]["ItemCode"] = "03402";
$default_oea["03402"]["ItemName"] = "趣街拍檔分享小組";
$default_oea["03402"]["CatCode"] = "4";
$default_oea["03402"]["CatName"] = "Aesthetic Development";

$default_oea["03403"]["ItemCode"] = "03403";
$default_oea["03403"]["ItemName"] = "趣街拍檔外拍活動";
$default_oea["03403"]["CatCode"] = "4";
$default_oea["03403"]["CatName"] = "Aesthetic Development";

$default_oea["03236"]["ItemCode"] = "03236";
$default_oea["03236"]["ItemName"] = "跆拳道紅黑帶証書";
$default_oea["03236"]["CatCode"] = "3";
$default_oea["03236"]["CatName"] = "Physical Development";

$default_oea["03237"]["ItemCode"] = "03237";
$default_oea["03237"]["ItemName"] = "跆拳道隊";
$default_oea["03237"]["CatCode"] = "3";
$default_oea["03237"]["CatName"] = "Physical Development";

$default_oea["03313"]["ItemCode"] = "03313";
$default_oea["03313"]["ItemName"] = "跨境學童香港教育展";
$default_oea["03313"]["CatCode"] = "5";
$default_oea["03313"]["CatName"] = "Moral and Civic Education";

$default_oea["03310"]["ItemCode"] = "03310";
$default_oea["03310"]["ItemName"] = "跨校合作傷健共融計劃";
$default_oea["03310"]["CatCode"] = "1";
$default_oea["03310"]["CatName"] = "Community Service";

$default_oea["03311"]["ItemCode"] = "03311";
$default_oea["03311"]["ItemName"] = "跨校閱讀大使計劃暨師生伴讀計劃 (何東小學演出話劇)";
$default_oea["03311"]["CatCode"] = "4";
$default_oea["03311"]["CatName"] = "Aesthetic Development";

$default_oea["03312"]["ItemCode"] = "03312";
$default_oea["03312"]["ItemName"] = "跨越新teen地2009計劃";
$default_oea["03312"]["CatCode"] = "1";
$default_oea["03312"]["CatName"] = "Community Service";

$default_oea["03314"]["ItemCode"] = "03314";
$default_oea["03314"]["ItemName"] = "路德會包美達社區中心勇者 CEO訓練計劃";
$default_oea["03314"]["CatCode"] = "2";
$default_oea["03314"]["CatName"] = "Career-related Experience";

$default_oea["03315"]["ItemCode"] = "03315";
$default_oea["03315"]["ItemName"] = "跳繩強心校際花式跳繩比賽";
$default_oea["03315"]["CatCode"] = "3";
$default_oea["03315"]["CatName"] = "Physical Development";

$default_oea["03510"]["ItemCode"] = "03510";
$default_oea["03510"]["ItemName"] = "躍動不倒翁計劃 - 標語設計比賽";
$default_oea["03510"]["CatCode"] = "4";
$default_oea["03510"]["CatName"] = "Aesthetic Development";

$default_oea["03361"]["ItemCode"] = "03361";
$default_oea["03361"]["ItemName"] = "輕鬆暢談 80分";
$default_oea["03361"]["CatCode"] = "5";
$default_oea["03361"]["CatName"] = "Moral and Civic Education";

$default_oea["03404"]["ItemCode"] = "03404";
$default_oea["03404"]["ItemName"] = "輪里關愛聖誕心意咭設計比賽";
$default_oea["03404"]["CatCode"] = "4";
$default_oea["03404"]["CatName"] = "Aesthetic Development";

$default_oea["02808"]["ItemCode"] = "02808";
$default_oea["02808"]["ItemName"] = "辛亥百年認識與探究";
$default_oea["02808"]["CatCode"] = "5";
$default_oea["02808"]["CatName"] = "Moral and Civic Education";

$default_oea["02809"]["ItemCode"] = "02809";
$default_oea["02809"]["ItemName"] = "辛亥革命百周年紀念活動系列之學術研討";
$default_oea["02809"]["CatCode"] = "5";
$default_oea["02809"]["CatName"] = "Moral and Civic Education";

$default_oea["02931"]["ItemCode"] = "02931";
$default_oea["02931"]["ItemName"] = "迪士尼最愛校園開心事徵文比賽";
$default_oea["02931"]["CatCode"] = "4";
$default_oea["02931"]["CatName"] = "Aesthetic Development";

$default_oea["02930"]["ItemCode"] = "02930";
$default_oea["02930"]["ItemName"] = "迪士尼青少年奇妙學習系列之物理世界教育宣傳片拍攝活動";
$default_oea["02930"]["CatCode"] = "2";
$default_oea["02930"]["CatName"] = "Career-related Experience";

$default_oea["03096"]["ItemCode"] = "03096";
$default_oea["03096"]["ItemName"] = "退選的抉擇";
$default_oea["03096"]["CatCode"] = "5";
$default_oea["03096"]["CatName"] = "Moral and Civic Education";

$default_oea["03166"]["ItemCode"] = "03166";
$default_oea["03166"]["ItemName"] = "通利香港新青年樂團周年音樂會";
$default_oea["03166"]["CatCode"] = "4";
$default_oea["03166"]["CatName"] = "Aesthetic Development";

$default_oea["03168"]["ItemCode"] = "03168";
$default_oea["03168"]["ItemName"] = "通識專題研習";
$default_oea["03168"]["CatCode"] = "5";
$default_oea["03168"]["CatName"] = "Moral and Civic Education";

$default_oea["03167"]["ItemCode"] = "03167";
$default_oea["03167"]["ItemName"] = "通識音樂單元";
$default_oea["03167"]["CatCode"] = "4";
$default_oea["03167"]["CatName"] = "Aesthetic Development";

$default_oea["03238"]["ItemCode"] = "03238";
$default_oea["03238"]["ItemName"] = "週年隊制急救比賽";
$default_oea["03238"]["CatCode"] = "2";
$default_oea["03238"]["CatName"] = "Career-related Experience";

$default_oea["03239"]["ItemCode"] = "03239";
$default_oea["03239"]["ItemName"] = "進階步操考核";
$default_oea["03239"]["CatCode"] = "2";
$default_oea["03239"]["CatName"] = "Career-related Experience";

$default_oea["03319"]["ItemCode"] = "03319";
$default_oea["03319"]["ItemName"] = "遊戲大使訓練計劃";
$default_oea["03319"]["CatCode"] = "1";
$default_oea["03319"]["CatName"] = "Community Service";

$default_oea["03318"]["ItemCode"] = "03318";
$default_oea["03318"]["ItemName"] = "運動攀登公開賽(香港攀山總會舉辦)";
$default_oea["03318"]["CatCode"] = "3";
$default_oea["03318"]["CatName"] = "Physical Development";

$default_oea["03317"]["ItemCode"] = "03317";
$default_oea["03317"]["ItemName"] = "運動與多元智能發展";
$default_oea["03317"]["CatCode"] = "3";
$default_oea["03317"]["CatName"] = "Physical Development";

$default_oea["03316"]["ItemCode"] = "03316";
$default_oea["03316"]["ItemName"] = "運動要拼搏‧無需要賭博漫畫創作比賽";
$default_oea["03316"]["CatCode"] = "4";
$default_oea["03316"]["CatName"] = "Aesthetic Development";

$default_oea["03320"]["ItemCode"] = "03320";
$default_oea["03320"]["ItemName"] = "道路安全運動道路安全潛能創意盡表現比賽";
$default_oea["03320"]["CatCode"] = "4";
$default_oea["03320"]["CatName"] = "Aesthetic Development";

$default_oea["02933"]["ItemCode"] = "02933";
$default_oea["02933"]["ItemName"] = "郊野公園教育活動計劃體驗自然Nature in Touch郊野定向教育活動";
$default_oea["02933"]["CatCode"] = "3";
$default_oea["02933"]["CatName"] = "Physical Development";

$default_oea["02932"]["ItemCode"] = "02932";
$default_oea["02932"]["ItemName"] = "郊野小記者校際比賽";
$default_oea["02932"]["CatCode"] = "4";
$default_oea["02932"]["CatName"] = "Aesthetic Development";

$default_oea["03097"]["ItemCode"] = "03097";
$default_oea["03097"]["ItemName"] = "酒店營運課程";
$default_oea["03097"]["CatCode"] = "2";
$default_oea["03097"]["CatName"] = "Career-related Experience";

$default_oea["03491"]["ItemCode"] = "03491";
$default_oea["03491"]["ItemName"] = "醫院義工防感染培訓課程";
$default_oea["03491"]["CatCode"] = "1";
$default_oea["03491"]["CatName"] = "Community Service";

$default_oea["03363"]["ItemCode"] = "03363";
$default_oea["03363"]["ItemName"] = "銀章級野外鍛鍊課程";
$default_oea["03363"]["CatCode"] = "3";
$default_oea["03363"]["CatName"] = "Physical Development";

$default_oea["03362"]["ItemCode"] = "03362";
$default_oea["03362"]["ItemName"] = "銀章花式跳繩訓練課程";
$default_oea["03362"]["CatCode"] = "3";
$default_oea["03362"]["CatName"] = "Physical Development";

$default_oea["03511"]["ItemCode"] = "03511";
$default_oea["03511"]["ItemName"] = "鐵畫銀鈎話豪情硬筆書法比賽";
$default_oea["03511"]["CatCode"] = "4";
$default_oea["03511"]["CatName"] = "Aesthetic Development";

$default_oea["02856"]["ItemCode"] = "02856";
$default_oea["02856"]["ItemName"] = "長安、香港、澳門青少年象棋邀請賽";
$default_oea["02856"]["CatCode"] = "4";
$default_oea["02856"]["CatName"] = "Aesthetic Development";

$default_oea["02857"]["ItemCode"] = "02857";
$default_oea["02857"]["ItemName"] = "長洲官中開放日籃球邀請賽";
$default_oea["02857"]["CatCode"] = "3";
$default_oea["02857"]["CatName"] = "Physical Development";

$default_oea["03240"]["ItemCode"] = "03240";
$default_oea["03240"]["ItemName"] = "開心家庭齊躍動計劃繪畫創作及心聲表達比賽";
$default_oea["03240"]["CatCode"] = "4";
$default_oea["03240"]["CatName"] = "Aesthetic Development";

$default_oea["03405"]["ItemCode"] = "03405";
$default_oea["03405"]["ItemName"] = "閱讀嘉年華";
$default_oea["03405"]["CatCode"] = "4";
$default_oea["03405"]["CatName"] = "Aesthetic Development";

$default_oea["03503"]["ItemCode"] = "03503";
$default_oea["03503"]["ItemName"] = "關心祖國齊迎上海世博國民教育系列－專題研習獎勵計劃";
$default_oea["03503"]["CatCode"] = "5";
$default_oea["03503"]["CatName"] = "Moral and Civic Education";

$default_oea["03504"]["ItemCode"] = "03504";
$default_oea["03504"]["ItemName"] = "關愛校園口號創作比賽";
$default_oea["03504"]["CatCode"] = "4";
$default_oea["03504"]["CatName"] = "Aesthetic Development";

$default_oea["03505"]["ItemCode"] = "03505";
$default_oea["03505"]["ItemName"] = "關懷大使培訓課程";
$default_oea["03505"]["CatCode"] = "1";
$default_oea["03505"]["CatName"] = "Community Service";

$default_oea["02810"]["ItemCode"] = "02810";
$default_oea["02810"]["ItemName"] = "防止店舖盜竊中小學海報設計比賽";
$default_oea["02810"]["CatCode"] = "4";
$default_oea["02810"]["CatName"] = "Aesthetic Development";

$default_oea["03169"]["ItemCode"] = "03169";
$default_oea["03169"]["ItemName"] = "陳瑞祺(喇沙)書院游泳接力邀請賽";
$default_oea["03169"]["CatCode"] = "3";
$default_oea["03169"]["CatName"] = "Physical Development";

$default_oea["03171"]["ItemCode"] = "03171";
$default_oea["03171"]["ItemName"] = "陶瓷茶具創作比賽";
$default_oea["03171"]["CatCode"] = "4";
$default_oea["03171"]["CatName"] = "Aesthetic Development";

$default_oea["03170"]["ItemCode"] = "03170";
$default_oea["03170"]["ItemName"] = "陸運會攝影及寫生比賽";
$default_oea["03170"]["CatCode"] = "4";
$default_oea["03170"]["CatName"] = "Aesthetic Development";

$default_oea["03241"]["ItemCode"] = "03241";
$default_oea["03241"]["ItemName"] = "陽光先鋒多媒體製作展覽";
$default_oea["03241"]["CatCode"] = "4";
$default_oea["03241"]["CatName"] = "Aesthetic Development";

$default_oea["03493"]["ItemCode"] = "03493";
$default_oea["03493"]["ItemName"] = "雙長相因陶藝展";
$default_oea["03493"]["CatCode"] = "4";
$default_oea["03493"]["CatName"] = "Aesthetic Development";

$default_oea["03494"]["ItemCode"] = "03494";
$default_oea["03494"]["ItemName"] = "雞蛋撞地球比賽";
$default_oea["03494"]["CatCode"] = "2";
$default_oea["03494"]["CatName"] = "Career-related Experience";

$default_oea["03492"]["ItemCode"] = "03492";
$default_oea["03492"]["ItemName"] = "離島區環保嘉年華";
$default_oea["03492"]["CatCode"] = "4";
$default_oea["03492"]["CatName"] = "Aesthetic Development";

$default_oea["03322"]["ItemCode"] = "03322";
$default_oea["03322"]["ItemName"] = "電影及錄像課程";
$default_oea["03322"]["CatCode"] = "4";
$default_oea["03322"]["CatName"] = "Aesthetic Development";

$default_oea["03321"]["ItemCode"] = "03321";
$default_oea["03321"]["ItemName"] = "電視及電影配音課程";
$default_oea["03321"]["CatCode"] = "4";
$default_oea["03321"]["CatName"] = "Aesthetic Development";

$default_oea["02875"]["ItemCode"] = "02875";
$default_oea["02875"]["ItemName"] = "青協青年論壇";
$default_oea["02875"]["CatCode"] = "2";
$default_oea["02875"]["CatName"] = "Career-related Experience";

$default_oea["02859"]["ItemCode"] = "02859";
$default_oea["02859"]["ItemName"] = "青少年企業領袖訓練課程 Junior Entrepreneur Scholarship";
$default_oea["02859"]["CatCode"] = "2";
$default_oea["02859"]["CatName"] = "Career-related Experience";

$default_oea["02863"]["ItemCode"] = "02863";
$default_oea["02863"]["ItemName"] = "青少年創意產品創業競賽－創意產品工作坊";
$default_oea["02863"]["CatCode"] = "2";
$default_oea["02863"]["CatName"] = "Career-related Experience";

$default_oea["02861"]["ItemCode"] = "02861";
$default_oea["02861"]["ItemName"] = "青少年動物拯救隊";
$default_oea["02861"]["CatCode"] = "1";
$default_oea["02861"]["CatName"] = "Community Service";

$default_oea["02860"]["ItemCode"] = "02860";
$default_oea["02860"]["ItemName"] = "青少年合球錦標賽";
$default_oea["02860"]["CatCode"] = "3";
$default_oea["02860"]["CatName"] = "Physical Development";

$default_oea["02862"]["ItemCode"] = "02862";
$default_oea["02862"]["ItemName"] = "青少年排球比賽";
$default_oea["02862"]["CatCode"] = "3";
$default_oea["02862"]["CatName"] = "Physical Development";

$default_oea["02858"]["ItemCode"] = "02858";
$default_oea["02858"]["ItemName"] = "青少年田徑訓練班";
$default_oea["02858"]["CatCode"] = "3";
$default_oea["02858"]["CatName"] = "Physical Development";

$default_oea["02864"]["ItemCode"] = "02864";
$default_oea["02864"]["ItemName"] = "青少年發明家比賽";
$default_oea["02864"]["CatCode"] = "2";
$default_oea["02864"]["CatName"] = "Career-related Experience";

$default_oea["02865"]["ItemCode"] = "02865";
$default_oea["02865"]["ItemName"] = "青少年精神健康急救(證書)課程";
$default_oea["02865"]["CatCode"] = "1";
$default_oea["02865"]["CatName"] = "Community Service";

$default_oea["02866"]["ItemCode"] = "02866";
$default_oea["02866"]["ItemName"] = "青少年領袖訓練計劃";
$default_oea["02866"]["CatCode"] = "2";
$default_oea["02866"]["CatName"] = "Career-related Experience";

$default_oea["02867"]["ItemCode"] = "02867";
$default_oea["02867"]["ItemName"] = "青少盃排球賽";
$default_oea["02867"]["CatCode"] = "3";
$default_oea["02867"]["CatName"] = "Physical Development";

$default_oea["02872"]["ItemCode"] = "02872";
$default_oea["02872"]["ItemName"] = "青年匯舞林(舞蹈欣賞)";
$default_oea["02872"]["CatCode"] = "4";
$default_oea["02872"]["CatName"] = "Aesthetic Development";

$default_oea["02873"]["ItemCode"] = "02873";
$default_oea["02873"]["ItemName"] = "青年戲劇匯演";
$default_oea["02873"]["CatCode"] = "4";
$default_oea["02873"]["CatName"] = "Aesthetic Development";

$default_oea["02868"]["ItemCode"] = "02868";
$default_oea["02868"]["ItemName"] = "青年本色 愛校園校園抗毒計劃";
$default_oea["02868"]["CatCode"] = "5";
$default_oea["02868"]["CatName"] = "Moral and Civic Education";

$default_oea["02869"]["ItemCode"] = "02869";
$default_oea["02869"]["ItemName"] = "青年立法會—議政訓練証書課程";
$default_oea["02869"]["CatCode"] = "5";
$default_oea["02869"]["CatName"] = "Moral and Civic Education";

$default_oea["02874"]["ItemCode"] = "02874";
$default_oea["02874"]["ItemName"] = "青年藝術之友-話劇欣賞";
$default_oea["02874"]["CatCode"] = "4";
$default_oea["02874"]["CatName"] = "Aesthetic Development";

$default_oea["02870"]["ItemCode"] = "02870";
$default_oea["02870"]["ItemName"] = "青年起點《成功上班族儀容證書課程》";
$default_oea["02870"]["CatCode"] = "4";
$default_oea["02870"]["CatName"] = "Aesthetic Development";

$default_oea["02871"]["ItemCode"] = "02871";
$default_oea["02871"]["ItemName"] = "青年高峰會圓桌會議";
$default_oea["02871"]["CatCode"] = "2";
$default_oea["02871"]["CatName"] = "Career-related Experience";

$default_oea["02876"]["ItemCode"] = "02876";
$default_oea["02876"]["ItemName"] = "青苗 The Brains";
$default_oea["02876"]["CatCode"] = "2";
$default_oea["02876"]["CatName"] = "Career-related Experience";

$default_oea["02877"]["ItemCode"] = "02877";
$default_oea["02877"]["ItemName"] = "青苗排球計劃";
$default_oea["02877"]["CatCode"] = "3";
$default_oea["02877"]["CatName"] = "Physical Development";

$default_oea["03481"]["ItemCode"] = "03481";
$default_oea["03481"]["ItemName"] = "韓國濟州管樂節 Cheji Band Festival in Korea";
$default_oea["03481"]["CatCode"] = "4";
$default_oea["03481"]["CatName"] = "Aesthetic Development";

$default_oea["02934"]["ItemCode"] = "02934";
$default_oea["02934"]["ItemName"] = "音樂人生電影欣賞及反思會";
$default_oea["02934"]["CatCode"] = "4";
$default_oea["02934"]["CatName"] = "Aesthetic Development";

$default_oea["02939"]["ItemCode"] = "02939";
$default_oea["02939"]["ItemName"] = "音樂優秀演奏家比賽";
$default_oea["02939"]["CatCode"] = "4";
$default_oea["02939"]["CatName"] = "Aesthetic Development";

$default_oea["02936"]["ItemCode"] = "02936";
$default_oea["02936"]["ItemName"] = "音樂原住民《90%原音》學以致用計劃";
$default_oea["02936"]["CatCode"] = "4";
$default_oea["02936"]["CatName"] = "Aesthetic Development";

$default_oea["02935"]["ItemCode"] = "02935";
$default_oea["02935"]["ItemName"] = "音樂比賽";
$default_oea["02935"]["CatCode"] = "4";
$default_oea["02935"]["CatName"] = "Aesthetic Development";

$default_oea["02937"]["ItemCode"] = "02937";
$default_oea["02937"]["ItemName"] = "音樂綜藝表演";
$default_oea["02937"]["CatCode"] = "4";
$default_oea["02937"]["CatName"] = "Aesthetic Development";

$default_oea["02938"]["ItemCode"] = "02938";
$default_oea["02938"]["ItemName"] = "音樂錄像製作比賽 MTV Competition";
$default_oea["02938"]["CatCode"] = "4";
$default_oea["02938"]["CatName"] = "Aesthetic Development";

$default_oea["03323"]["ItemCode"] = "03323";
$default_oea["03323"]["ItemName"] = "頒獎日表演";
$default_oea["03323"]["CatCode"] = "4";
$default_oea["03323"]["CatName"] = "Aesthetic Development";

$default_oea["03364"]["ItemCode"] = "03364";
$default_oea["03364"]["ItemName"] = "領航員訓練計劃";
$default_oea["03364"]["CatCode"] = "2";
$default_oea["03364"]["CatName"] = "Career-related Experience";

$default_oea["03366"]["ItemCode"] = "03366";
$default_oea["03366"]["ItemName"] = "領袖生訓練營 Leadership Training Camp";
$default_oea["03366"]["CatCode"] = "2";
$default_oea["03366"]["CatName"] = "Career-related Experience";

$default_oea["03365"]["ItemCode"] = "03365";
$default_oea["03365"]["ItemName"] = "領袖生訓練計劃傑出表現獎 Prefect Training Scheme : Best Performance Award";
$default_oea["03365"]["CatCode"] = "2";
$default_oea["03365"]["CatName"] = "Career-related Experience";

$default_oea["02940"]["ItemCode"] = "02940";
$default_oea["02940"]["ItemName"] = "風帆及獨木舟旅程";
$default_oea["02940"]["CatCode"] = "3";
$default_oea["02940"]["CatName"] = "Physical Development";

$default_oea["02941"]["ItemCode"] = "02941";
$default_oea["02941"]["ItemName"] = "香島盃全港中學校際中國象棋賽";
$default_oea["02941"]["CatCode"] = "4";
$default_oea["02941"]["CatName"] = "Aesthetic Development";

$default_oea["02942"]["ItemCode"] = "02942";
$default_oea["02942"]["ItemName"] = "香港、泰國、澳門機械奧運會埠際賽";
$default_oea["02942"]["CatCode"] = "3";
$default_oea["02942"]["CatName"] = "Physical Development";

$default_oea["02955"]["ItemCode"] = "02955";
$default_oea["02955"]["ItemName"] = "香港中區歷史文物建築攝影比賽";
$default_oea["02955"]["CatCode"] = "4";
$default_oea["02955"]["CatName"] = "Aesthetic Development";

$default_oea["02960"]["ItemCode"] = "02960";
$default_oea["02960"]["ItemName"] = "香港中學彈網錦標賽";
$default_oea["02960"]["CatCode"] = "3";
$default_oea["02960"]["CatName"] = "Physical Development";

$default_oea["02948"]["ItemCode"] = "02948";
$default_oea["02948"]["ItemName"] = "香港中小學生創意作品比賽";
$default_oea["02948"]["CatCode"] = "4";
$default_oea["02948"]["CatName"] = "Aesthetic Development";

$default_oea["02949"]["ItemCode"] = "02949";
$default_oea["02949"]["ItemName"] = "香港中文大學化學系主辦現代化學講座";
$default_oea["02949"]["CatCode"] = "2";
$default_oea["02949"]["CatName"] = "Career-related Experience";

$default_oea["02951"]["ItemCode"] = "02951";
$default_oea["02951"]["ItemName"] = "香港中文大學博文公開講座系列人類基因的迷思";
$default_oea["02951"]["CatCode"] = "2";
$default_oea["02951"]["CatName"] = "Career-related Experience";

$default_oea["02952"]["ItemCode"] = "02952";
$default_oea["02952"]["ItemName"] = "香港中文大學學生文化大使";
$default_oea["02952"]["CatCode"] = "4";
$default_oea["02952"]["CatName"] = "Aesthetic Development";

$default_oea["02950"]["ItemCode"] = "02950";
$default_oea["02950"]["ItemName"] = "香港中文大學教育學院資優計劃資優學生培訓活動";
$default_oea["02950"]["CatCode"] = "2";
$default_oea["02950"]["CatName"] = "Career-related Experience";

$default_oea["02953"]["ItemCode"] = "02953";
$default_oea["02953"]["ItemName"] = "香港中文大學醫學院疫苗與癌症預防專題探究獎勵計劃";
$default_oea["02953"]["CatCode"] = "2";
$default_oea["02953"]["CatName"] = "Career-related Experience";

$default_oea["02954"]["ItemCode"] = "02954";
$default_oea["02954"]["ItemName"] = "香港中文大學體驗學習與青少年正向發展：學習．領導．生命轉化計劃";
$default_oea["02954"]["CatCode"] = "2";
$default_oea["02954"]["CatName"] = "Career-related Experience";

$default_oea["02957"]["ItemCode"] = "02957";
$default_oea["02957"]["ItemName"] = "香港中樂團 -《成吉思汗》";
$default_oea["02957"]["CatCode"] = "4";
$default_oea["02957"]["CatName"] = "Aesthetic Development";

$default_oea["02958"]["ItemCode"] = "02958";
$default_oea["02958"]["ItemName"] = "香港中樂團《香港指揮家節- 閻惠昌音樂會》";
$default_oea["02958"]["CatCode"] = "4";
$default_oea["02958"]["CatName"] = "Aesthetic Development";

$default_oea["02959"]["ItemCode"] = "02959";
$default_oea["02959"]["ItemName"] = "香港中樂團表演欣賞";
$default_oea["02959"]["CatCode"] = "4";
$default_oea["02959"]["CatName"] = "Aesthetic Development";

$default_oea["02956"]["ItemCode"] = "02956";
$default_oea["02956"]["ItemName"] = "香港中華文化總會 中華文化盃 - 我心中的祖國全港中文徵文大賽";
$default_oea["02956"]["CatCode"] = "4";
$default_oea["02956"]["CatName"] = "Aesthetic Development";

$default_oea["02962"]["ItemCode"] = "02962";
$default_oea["02962"]["ItemName"] = "香港五四青年節《五四青年大匯演》";
$default_oea["02962"]["CatCode"] = "4";
$default_oea["02962"]["CatName"] = "Aesthetic Development";

$default_oea["02961"]["ItemCode"] = "02961";
$default_oea["02961"]["ItemName"] = "香港五四青年節：同夢同心—兩地青年交流計劃接待安排";
$default_oea["02961"]["CatCode"] = "5";
$default_oea["02961"]["CatName"] = "Moral and Civic Education";

$default_oea["02968"]["ItemCode"] = "02968";
$default_oea["02968"]["ItemName"] = "香港交通安全隊新界南總區周年檢閱禮 步操比賽";
$default_oea["02968"]["CatCode"] = "2";
$default_oea["02968"]["CatName"] = "Career-related Experience";

$default_oea["02943"]["ItemCode"] = "02943";
$default_oea["02943"]["ItemName"] = "香港人口普查推廣";
$default_oea["02943"]["CatCode"] = "1";
$default_oea["02943"]["CatName"] = "Community Service";

$default_oea["02964"]["ItemCode"] = "02964";
$default_oea["02964"]["ItemName"] = "香港仔野外定向 Orienteering in Aberdeen";
$default_oea["02964"]["CatCode"] = "3";
$default_oea["02964"]["CatName"] = "Physical Development";

$default_oea["02965"]["ItemCode"] = "02965";
$default_oea["02965"]["ItemName"] = "香港代無伴奏合唱協會音樂會";
$default_oea["02965"]["CatCode"] = "4";
$default_oea["02965"]["CatName"] = "Aesthetic Development";

$default_oea["02969"]["ItemCode"] = "02969";
$default_oea["02969"]["ItemName"] = "香港任我行-學生普通話服務業大使培訓計劃";
$default_oea["02969"]["CatCode"] = "1";
$default_oea["02969"]["CatName"] = "Community Service";

$default_oea["03019"]["ItemCode"] = "03019";
$default_oea["03019"]["ItemName"] = "香港傑出學生選舉2010-2011 - 領袖培訓";
$default_oea["03019"]["CatCode"] = "2";
$default_oea["03019"]["CatName"] = "Career-related Experience";

$default_oea["02963"]["ItemCode"] = "02963";
$default_oea["02963"]["ItemName"] = "香港公共關係學會標語及海報創作比賽";
$default_oea["02963"]["CatCode"] = "4";
$default_oea["02963"]["CatName"] = "Aesthetic Development";

$default_oea["03021"]["ItemCode"] = "03021";
$default_oea["03021"]["ItemName"] = "香港創意思維活動襟章設計比賽";
$default_oea["03021"]["CatCode"] = "4";
$default_oea["03021"]["CatName"] = "Aesthetic Development";

$default_oea["02966"]["ItemCode"] = "02966";
$default_oea["02966"]["ItemName"] = "香港台灣青少年射箭友誼賽";
$default_oea["02966"]["CatCode"] = "3";
$default_oea["02966"]["CatName"] = "Physical Development";

$default_oea["02991"]["ItemCode"] = "02991";
$default_oea["02991"]["ItemName"] = "香港品牌‧締造優質生活全港中學生廣告短片創作比賽";
$default_oea["02991"]["CatCode"] = "4";
$default_oea["02991"]["CatName"] = "Aesthetic Development";

$default_oea["02993"]["ItemCode"] = "02993";
$default_oea["02993"]["ItemName"] = "香港品質保證局理想家園:徵文、攝影、繪畫及海報設計比賽 (Please provide details in \"Description\" box.)";
$default_oea["02993"]["CatCode"] = "4";
$default_oea["02993"]["CatName"] = "Aesthetic Development";

$default_oea["02992"]["ItemCode"] = "02992";
$default_oea["02992"]["ItemName"] = "香港品質保證局理想家園:海報設計比賽 (Please provide details in \"Description\" box.)";
$default_oea["02992"]["CatCode"] = "4";
$default_oea["02992"]["CatName"] = "Aesthetic Development";

$default_oea["03012"]["ItemCode"] = "03012";
$default_oea["03012"]["ItemName"] = "香港國家地質公園議題探究比賽 2010";
$default_oea["03012"]["CatCode"] = "2";
$default_oea["03012"]["CatName"] = "Career-related Experience";

$default_oea["03014"]["ItemCode"] = "03014";
$default_oea["03014"]["ItemName"] = "香港國際海報三年展";
$default_oea["03014"]["CatCode"] = "4";
$default_oea["03014"]["CatName"] = "Aesthetic Development";

$default_oea["03013"]["ItemCode"] = "03013";
$default_oea["03013"]["ItemName"] = "香港國際社會服務社";
$default_oea["03013"]["CatCode"] = "1";
$default_oea["03013"]["CatName"] = "Community Service";

$default_oea["02994"]["ItemCode"] = "02994";
$default_oea["02994"]["ItemName"] = "香港城市大學中樂團周年音樂會";
$default_oea["02994"]["CatCode"] = "4";
$default_oea["02994"]["CatName"] = "Aesthetic Development";

$default_oea["02944"]["ItemCode"] = "02944";
$default_oea["02944"]["ItemName"] = "香港大學射箭公開賽";
$default_oea["02944"]["CatCode"] = "3";
$default_oea["02944"]["CatName"] = "Physical Development";

$default_oea["03031"]["ItemCode"] = "03031";
$default_oea["03031"]["ItemName"] = "香港學生科學比賽";
$default_oea["03031"]["CatCode"] = "2";
$default_oea["03031"]["CatName"] = "Career-related Experience";

$default_oea["03033"]["ItemCode"] = "03033";
$default_oea["03033"]["ItemName"] = "香港學界劍擊比賽 (Please provide details in \"Description\" box.)";
$default_oea["03033"]["CatCode"] = "3";
$default_oea["03033"]["CatName"] = "Physical Development";

$default_oea["03032"]["ItemCode"] = "03032";
$default_oea["03032"]["ItemName"] = "香港學界游泳錦標賽";
$default_oea["03032"]["CatCode"] = "3";
$default_oea["03032"]["CatName"] = "Physical Development";

$default_oea["03034"]["ItemCode"] = "03034";
$default_oea["03034"]["ItemName"] = "香港學界辯論挑戰賽";
$default_oea["03034"]["CatCode"] = "2";
$default_oea["03034"]["CatName"] = "Career-related Experience";

$default_oea["03036"]["ItemCode"] = "03036";
$default_oea["03036"]["ItemName"] = "香港學界體育聯會乒乓球裁判服務";
$default_oea["03036"]["CatCode"] = "3";
$default_oea["03036"]["CatName"] = "Physical Development";

$default_oea["03035"]["ItemCode"] = "03035";
$default_oea["03035"]["ItemName"] = "香港學界體育聯會大埔及北區中學分會周年頒獎禮";
$default_oea["03035"]["CatCode"] = "3";
$default_oea["03035"]["CatName"] = "Physical Development";

$default_oea["02995"]["ItemCode"] = "02995";
$default_oea["02995"]["ItemName"] = "香港室內中樂團天華音樂之路音樂會";
$default_oea["02995"]["CatCode"] = "4";
$default_oea["02995"]["CatName"] = "Aesthetic Development";

$default_oea["03015"]["ItemCode"] = "03015";
$default_oea["03015"]["ItemName"] = "香港專上學院-青年高峰會";
$default_oea["03015"]["CatCode"] = "2";
$default_oea["03015"]["CatName"] = "Career-related Experience";

$default_oea["02947"]["ItemCode"] = "02947";
$default_oea["02947"]["ItemName"] = "香港小童群益會家庭樂開心家庭小貼示/小故事徵稿比賽";
$default_oea["02947"]["CatCode"] = "4";
$default_oea["02947"]["CatName"] = "Aesthetic Development";

$default_oea["02945"]["ItemCode"] = "02945";
$default_oea["02945"]["ItemName"] = "香港小童群益會扭轉逆境扭計骰版面設計比賽(高中組)";
$default_oea["02945"]["CatCode"] = "4";
$default_oea["02945"]["CatName"] = "Aesthetic Development";

$default_oea["02946"]["ItemCode"] = "02946";
$default_oea["02946"]["ItemName"] = "香港小童群益會青年服務計劃";
$default_oea["02946"]["CatCode"] = "1";
$default_oea["02946"]["CatName"] = "Community Service";

$default_oea["03038"]["ItemCode"] = "03038";
$default_oea["03038"]["ItemName"] = "香港應對氣候變化策略及行動綱領青年論壇";
$default_oea["03038"]["CatCode"] = "2";
$default_oea["03038"]["CatName"] = "Career-related Experience";

$default_oea["02996"]["ItemCode"] = "02996";
$default_oea["02996"]["ItemName"] = "香港拯溺總會元旦冬泳拯溺錦標大賽";
$default_oea["02996"]["CatCode"] = "3";
$default_oea["02996"]["CatName"] = "Physical Development";

$default_oea["03016"]["ItemCode"] = "03016";
$default_oea["03016"]["ItemName"] = "香港教師中心童言同心救地球徵文比賽";
$default_oea["03016"]["CatCode"] = "4";
$default_oea["03016"]["CatName"] = "Aesthetic Development";

$default_oea["03030"]["ItemCode"] = "03030";
$default_oea["03030"]["ItemName"] = "香港數理教育學會及香港科技大學聯合舉辦「數理遊蹤」";
$default_oea["03030"]["CatCode"] = "2";
$default_oea["03030"]["CatName"] = "Career-related Experience";

$default_oea["03002"]["ItemCode"] = "03002";
$default_oea["03002"]["ItemName"] = "香港旅遊文化發展協進會保育文化徵文比賽";
$default_oea["03002"]["CatCode"] = "4";
$default_oea["03002"]["CatName"] = "Aesthetic Development";

$default_oea["03020"]["ItemCode"] = "03020";
$default_oea["03020"]["ItemName"] = "香港最佳老友運動電能烹飪比賽";
$default_oea["03020"]["CatCode"] = "4";
$default_oea["03020"]["CatName"] = "Aesthetic Development";

$default_oea["02970"]["ItemCode"] = "02970";
$default_oea["02970"]["ItemName"] = "香港有格漫畫創作比賽";
$default_oea["02970"]["CatCode"] = "4";
$default_oea["02970"]["CatName"] = "Aesthetic Development";

$default_oea["02971"]["ItemCode"] = "02971";
$default_oea["02971"]["ItemName"] = "香港有機資源中心全城有機日";
$default_oea["02971"]["CatCode"] = "1";
$default_oea["02971"]["CatName"] = "Community Service";

$default_oea["02967"]["ItemCode"] = "02967";
$default_oea["02967"]["ItemName"] = "香港本地文化背包設計比賽";
$default_oea["02967"]["CatCode"] = "4";
$default_oea["02967"]["CatName"] = "Aesthetic Development";

$default_oea["02973"]["ItemCode"] = "02973";
$default_oea["02973"]["ItemName"] = "香港杯外交智識競賽";
$default_oea["02973"]["CatCode"] = "5";
$default_oea["02973"]["CatName"] = "Moral and Civic Education";

$default_oea["02972"]["ItemCode"] = "02972";
$default_oea["02972"]["ItemName"] = "香港東亞盃聯校柔道比賽";
$default_oea["02972"]["CatCode"] = "3";
$default_oea["02972"]["CatName"] = "Physical Development";

$default_oea["02997"]["ItemCode"] = "02997";
$default_oea["02997"]["ItemName"] = "香港柔道冠軍賽";
$default_oea["02997"]["CatCode"] = "3";
$default_oea["02997"]["CatName"] = "Physical Development";

$default_oea["03003"]["ItemCode"] = "03003";
$default_oea["03003"]["ItemName"] = "香港校際網上辨論比賽";
$default_oea["03003"]["CatCode"] = "2";
$default_oea["03003"]["CatName"] = "Career-related Experience";

$default_oea["03026"]["ItemCode"] = "03026";
$default_oea["03026"]["ItemName"] = "香港業餘田徑總會訓練";
$default_oea["03026"]["CatCode"] = "3";
$default_oea["03026"]["CatName"] = "Physical Development";

$default_oea["03022"]["ItemCode"] = "03022";
$default_oea["03022"]["ItemName"] = "香港殘疾人士週年硬地滾球錦標賽";
$default_oea["03022"]["CatCode"] = "3";
$default_oea["03022"]["CatName"] = "Physical Development";

$default_oea["03006"]["ItemCode"] = "03006";
$default_oea["03006"]["ItemName"] = "香港海洋公園智慧鯨訓練班－海洋公園工作實錄";
$default_oea["03006"]["CatCode"] = "2";
$default_oea["03006"]["CatName"] = "Career-related Experience";

$default_oea["03004"]["ItemCode"] = "03004";
$default_oea["03004"]["ItemName"] = "香港浸會大學中醫學院-中醫藥知識問答比賽";
$default_oea["03004"]["CatCode"] = "2";
$default_oea["03004"]["CatName"] = "Career-related Experience";

$default_oea["03005"]["ItemCode"] = "03005";
$default_oea["03005"]["ItemName"] = "香港浸會大學綠色有機健康跑活動";
$default_oea["03005"]["CatCode"] = "3";
$default_oea["03005"]["CatName"] = "Physical Development";

$default_oea["03023"]["ItemCode"] = "03023";
$default_oea["03023"]["ItemName"] = "香港游泳錦標賽 50米背泳";
$default_oea["03023"]["CatCode"] = "3";
$default_oea["03023"]["CatName"] = "Physical Development";

$default_oea["03007"]["ItemCode"] = "03007";
$default_oea["03007"]["ItemName"] = "香港特別行政區保安局副局長黎棟國先生分享會";
$default_oea["03007"]["CatCode"] = "4";
$default_oea["03007"]["CatName"] = "Aesthetic Development";

$default_oea["03008"]["ItemCode"] = "03008";
$default_oea["03008"]["ItemName"] = "香港特區授勳紀念畫冊儀式";
$default_oea["03008"]["CatCode"] = "5";
$default_oea["03008"]["CatName"] = "Moral and Civic Education";

$default_oea["03037"]["ItemCode"] = "03037";
$default_oea["03037"]["ItemName"] = "香港獨立短片及錄像比賽 (ifva)";
$default_oea["03037"]["CatCode"] = "4";
$default_oea["03037"]["CatName"] = "Aesthetic Development";

$default_oea["03017"]["ItemCode"] = "03017";
$default_oea["03017"]["ItemName"] = "香港理工大學工程學系-理工大學暑期夏令營";
$default_oea["03017"]["CatCode"] = "2";
$default_oea["03017"]["CatName"] = "Career-related Experience";

$default_oea["02998"]["ItemCode"] = "02998";
$default_oea["02998"]["ItemName"] = "香港盃全港中學校際中國象棋賽";
$default_oea["02998"]["CatCode"] = "4";
$default_oea["02998"]["CatName"] = "Aesthetic Development";

$default_oea["02999"]["ItemCode"] = "02999";
$default_oea["02999"]["ItemName"] = "香港盾射箭比賽";
$default_oea["02999"]["CatCode"] = "3";
$default_oea["02999"]["CatName"] = "Physical Development";

$default_oea["03009"]["ItemCode"] = "03009";
$default_oea["03009"]["ItemName"] = "香港真武跆拳道";
$default_oea["03009"]["CatCode"] = "3";
$default_oea["03009"]["CatName"] = "Physical Development";

$default_oea["03010"]["ItemCode"] = "03010";
$default_oea["03010"]["ItemName"] = "香港神託會Stewards - MR L.I.F.E. 生命掌舵人 - 認識工作世界";
$default_oea["03010"]["CatCode"] = "2";
$default_oea["03010"]["CatName"] = "Career-related Experience";

$default_oea["03011"]["ItemCode"] = "03011";
$default_oea["03011"]["ItemName"] = "香港神託會Stewards - MR L.I.F.E. 生命掌舵人-職前計劃工作坊";
$default_oea["03011"]["CatCode"] = "2";
$default_oea["03011"]["CatName"] = "Career-related Experience";

$default_oea["03001"]["ItemCode"] = "03001";
$default_oea["03001"]["ItemName"] = "香港科學館大飛機與中國的大飛機專項計劃";
$default_oea["03001"]["CatCode"] = "2";
$default_oea["03001"]["CatName"] = "Career-related Experience";

$default_oea["03000"]["ItemCode"] = "03000";
$default_oea["03000"]["ItemName"] = "香港科技大學-青少年機械人世界盃選拔賽";
$default_oea["03000"]["CatCode"] = "2";
$default_oea["03000"]["CatName"] = "Career-related Experience";

$default_oea["03024"]["ItemCode"] = "03024";
$default_oea["03024"]["ItemName"] = "香港童軍大匯操";
$default_oea["03024"]["CatCode"] = "1";
$default_oea["03024"]["CatName"] = "Community Service";

$default_oea["02974"]["ItemCode"] = "02974";
$default_oea["02974"]["ItemName"] = "香港花卉展覽攝影比賽";
$default_oea["02974"]["CatCode"] = "4";
$default_oea["02974"]["CatName"] = "Aesthetic Development";

$default_oea["03039"]["ItemCode"] = "03039";
$default_oea["03039"]["ItemName"] = "香港藝術發展局-18區ID設計比賽";
$default_oea["03039"]["CatCode"] = "4";
$default_oea["03039"]["CatName"] = "Aesthetic Development";

$default_oea["03040"]["ItemCode"] = "03040";
$default_oea["03040"]["ItemName"] = "香港藝術節青少年之友計劃- (Please provide details in \"Description\" box.)";
$default_oea["03040"]["CatCode"] = "4";
$default_oea["03040"]["CatName"] = "Aesthetic Development";

$default_oea["03041"]["ItemCode"] = "03041";
$default_oea["03041"]["ItemName"] = "香港藝術館-視界新色及獨立風骨參觀展覽";
$default_oea["03041"]["CatCode"] = "4";
$default_oea["03041"]["CatName"] = "Aesthetic Development";

$default_oea["03043"]["ItemCode"] = "03043";
$default_oea["03043"]["ItemName"] = "香港蘭藝會第三十六屆蘭花展覽";
$default_oea["03043"]["CatCode"] = "4";
$default_oea["03043"]["CatName"] = "Aesthetic Development";

$default_oea["03046"]["ItemCode"] = "03046";
$default_oea["03046"]["ItemName"] = "香港觀蝶及普查網絡(中學生)";
$default_oea["03046"]["CatCode"] = "4";
$default_oea["03046"]["CatName"] = "Aesthetic Development";

$default_oea["03045"]["ItemCode"] = "03045";
$default_oea["03045"]["ItemName"] = "香港觀鳥會生態導賞課程";
$default_oea["03045"]["CatCode"] = "4";
$default_oea["03045"]["CatName"] = "Aesthetic Development";

$default_oea["03042"]["ItemCode"] = "03042";
$default_oea["03042"]["ItemName"] = "香港警務署九龍城警區進展計劃培訓";
$default_oea["03042"]["CatCode"] = "2";
$default_oea["03042"]["CatName"] = "Career-related Experience";

$default_oea["03029"]["ItemCode"] = "03029";
$default_oea["03029"]["ItemName"] = "香港資優學院奧林匹克-物理競賽";
$default_oea["03029"]["CatCode"] = "2";
$default_oea["03029"]["CatName"] = "Career-related Experience";

$default_oea["03027"]["ItemCode"] = "03027";
$default_oea["03027"]["ItemName"] = "香港資優教育學院 - 卓越領導才能培訓";
$default_oea["03027"]["CatCode"] = "2";
$default_oea["03027"]["CatName"] = "Career-related Experience";

$default_oea["03028"]["ItemCode"] = "03028";
$default_oea["03028"]["ItemName"] = "香港資優教育學院 - 領導資優培訓";
$default_oea["03028"]["CatCode"] = "2";
$default_oea["03028"]["CatName"] = "Career-related Experience";

$default_oea["03025"]["ItemCode"] = "03025";
$default_oea["03025"]["ItemName"] = "香港跆拳道東龍會錦標賽";
$default_oea["03025"]["CatCode"] = "3";
$default_oea["03025"]["CatName"] = "Physical Development";

$default_oea["03018"]["ItemCode"] = "03018";
$default_oea["03018"]["ItemName"] = "香港野外定會幹事盃暨公開賽";
$default_oea["03018"]["CatCode"] = "3";
$default_oea["03018"]["CatName"] = "Physical Development";

$default_oea["02980"]["ItemCode"] = "02980";
$default_oea["02980"]["ItemName"] = "香港青少年創辦科技教育協會- Info Matrix 2010 in Romania";
$default_oea["02980"]["CatCode"] = "2";
$default_oea["02980"]["CatName"] = "Career-related Experience";

$default_oea["02976"]["ItemCode"] = "02976";
$default_oea["02976"]["ItemName"] = "香港青少年室內射箭公開賽";
$default_oea["02976"]["CatCode"] = "3";
$default_oea["02976"]["CatName"] = "Physical Development";

$default_oea["02977"]["ItemCode"] = "02977";
$default_oea["02977"]["ItemName"] = "香港青少年室外射箭錦標賽";
$default_oea["02977"]["CatCode"] = "3";
$default_oea["02977"]["CatName"] = "Physical Development";

$default_oea["02979"]["ItemCode"] = "02979";
$default_oea["02979"]["ItemName"] = "香港青少年射箭公開賽";
$default_oea["02979"]["CatCode"] = "3";
$default_oea["02979"]["CatName"] = "Physical Development";

$default_oea["02975"]["ItemCode"] = "02975";
$default_oea["02975"]["ItemName"] = "香港青少年弦樂大賽";
$default_oea["02975"]["CatCode"] = "4";
$default_oea["02975"]["CatName"] = "Aesthetic Development";

$default_oea["02981"]["ItemCode"] = "02981";
$default_oea["02981"]["ItemName"] = "香港青少年獎勵計劃 (Please provide details in \"Description\" box.)";
$default_oea["02981"]["CatCode"] = "2";
$default_oea["02981"]["CatName"] = "Career-related Experience";

$default_oea["02978"]["ItemCode"] = "02978";
$default_oea["02978"]["ItemName"] = "香港青少年軍事夏令營";
$default_oea["02978"]["CatCode"] = "2";
$default_oea["02978"]["CatName"] = "Career-related Experience";

$default_oea["02982"]["ItemCode"] = "02982";
$default_oea["02982"]["ItemName"] = "香港青少年鋼琴公開比賽";
$default_oea["02982"]["CatCode"] = "4";
$default_oea["02982"]["CatName"] = "Aesthetic Development";

$default_oea["02986"]["ItemCode"] = "02986";
$default_oea["02986"]["ItemName"] = "香港青年協會-渣打香港英語比賽";
$default_oea["02986"]["CatCode"] = "4";
$default_oea["02986"]["CatName"] = "Aesthetic Development";

$default_oea["02984"]["ItemCode"] = "02984";
$default_oea["02984"]["ItemName"] = "香港青年協會步行籌款";
$default_oea["02984"]["CatCode"] = "1";
$default_oea["02984"]["CatName"] = "Community Service";

$default_oea["02987"]["ItemCode"] = "02987";
$default_oea["02987"]["ItemName"] = "香港青年協會與中華全國青年聯合會《香港2000》領袖計劃";
$default_oea["02987"]["CatCode"] = "2";
$default_oea["02987"]["CatName"] = "Career-related Experience";

$default_oea["02988"]["ItemCode"] = "02988";
$default_oea["02988"]["ItemName"] = "香港青年協會與弱勢家庭旅行活動";
$default_oea["02988"]["CatCode"] = "1";
$default_oea["02988"]["CatName"] = "Community Service";

$default_oea["02989"]["ItemCode"] = "02989";
$default_oea["02989"]["ItemName"] = "香港青年協會關愛同行-搞搞笑-愛傳城服務計劃";
$default_oea["02989"]["CatCode"] = "1";
$default_oea["02989"]["CatName"] = "Community Service";

$default_oea["02985"]["ItemCode"] = "02985";
$default_oea["02985"]["ItemName"] = "香港青年協會青年專列直通上海世博";
$default_oea["02985"]["CatCode"] = "2";
$default_oea["02985"]["CatName"] = "Career-related Experience";

$default_oea["02983"]["ItemCode"] = "02983";
$default_oea["02983"]["ItemName"] = "香港青年大使計劃";
$default_oea["02983"]["CatCode"] = "1";
$default_oea["02983"]["CatName"] = "Community Service";

$default_oea["02990"]["ItemCode"] = "02990";
$default_oea["02990"]["ItemName"] = "香港青年領袖論壇";
$default_oea["02990"]["CatCode"] = "2";
$default_oea["02990"]["CatName"] = "Career-related Experience";

$default_oea["03044"]["ItemCode"] = "03044";
$default_oea["03044"]["ItemName"] = "香港體育節跆拳道錦標賽";
$default_oea["03044"]["CatCode"] = "3";
$default_oea["03044"]["CatName"] = "Physical Development";

$default_oea["03098"]["ItemCode"] = "03098";
$default_oea["03098"]["ItemName"] = "馬來西亞英語學習文化交流之旅";
$default_oea["03098"]["CatCode"] = "4";
$default_oea["03098"]["CatName"] = "Aesthetic Development";

$default_oea["03514"]["ItemCode"] = "03514";
$default_oea["03514"]["ItemName"] = "體適能課程";
$default_oea["03514"]["CatCode"] = "3";
$default_oea["03514"]["CatName"] = "Physical Development";

$default_oea["03099"]["ItemCode"] = "03099";
$default_oea["03099"]["ItemName"] = "高中組普通話經典新詩朗誦";
$default_oea["03099"]["CatCode"] = "4";
$default_oea["03099"]["CatName"] = "Aesthetic Development";

$default_oea["03100"]["ItemCode"] = "03100";
$default_oea["03100"]["ItemName"] = "高中藝術新體驗";
$default_oea["03100"]["CatCode"] = "4";
$default_oea["03100"]["CatName"] = "Aesthetic Development";

$default_oea["03406"]["ItemCode"] = "03406";
$default_oea["03406"]["ItemName"] = "髮型設計班";
$default_oea["03406"]["CatCode"] = "4";
$default_oea["03406"]["CatName"] = "Aesthetic Development";

$default_oea["03242"]["ItemCode"] = "03242";
$default_oea["03242"]["ItemName"] = "黑臉琵鷺藝術作品展";
$default_oea["03242"]["CatCode"] = "4";
$default_oea["03242"]["CatName"] = "Aesthetic Development";

$default_oea["03482"]["ItemCode"] = "03482";
$default_oea["03482"]["ItemName"] = "點對點社區藝術計劃";
$default_oea["03482"]["CatCode"] = "4";
$default_oea["03482"]["CatName"] = "Aesthetic Development";

$default_oea["03367"]["ItemCode"] = "03367";
$default_oea["03367"]["ItemName"] = "齊享健康資訊教育短劇";
$default_oea["03367"]["CatCode"] = "4";
$default_oea["03367"]["CatName"] = "Aesthetic Development";

$default_oea["03368"]["ItemCode"] = "03368";
$default_oea["03368"]["ItemName"] = "齊賞FTV電影電視學院畢業作品導賞";
$default_oea["03368"]["CatCode"] = "4";
$default_oea["03368"]["CatName"] = "Aesthetic Development";

$default_oea["03445"]["ItemCode"] = "03445";
$default_oea["03445"]["ItemName"] = "龍土鄉情音樂會";
$default_oea["03445"]["CatCode"] = "4";
$default_oea["03445"]["CatName"] = "Aesthetic Development";

$default_oea["03446"]["ItemCode"] = "03446";
$default_oea["03446"]["ItemName"] = "龍城看世界 (Please provide details in \"Description\" box.)";
$default_oea["03446"]["CatCode"] = "4";
$default_oea["03446"]["CatName"] = "Aesthetic Development";

?>