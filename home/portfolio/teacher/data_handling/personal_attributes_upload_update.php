<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$Folder = $PATH_WRT_ROOT."file/portfolio";
if (!file_exists($Folder))
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$lo = new libfilesystem();
	$lo->folder_new($intranet_root."/file/portfolio");
}

$result = "UpdateUnsuccess";
if (is_array($_FILES) && sizeof($_FILES)>0)
{
	foreach($_FILES AS $Key => $FileObj)
	{
		if ($FileObj["name"]!="" && $FileObj["tmp_name"]!="")
		{
			$splitArr = explode("_", $Key);
			$AcademicYearID = trim($splitArr[1]);
						
			# if exists, replace
			$FileCSV = $PATH_WRT_ROOT."file/portfolio/fywss_pa_".$AcademicYearID.".csv";

			# else move to
			move_uploaded_file($FileObj["tmp_name"], $FileCSV);
			
			$result = "UpdateSuccess";
		}
	}
}


intranet_closedb();

header("location: personal_attributes.php?xmsg=".$result);
?>