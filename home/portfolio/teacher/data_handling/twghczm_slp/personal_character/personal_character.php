<?php
$db_mode=true;
if($db_mode==true){
	header("Location: ".'personal_character_db_by_class.php');
	exit();
}

// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################
$libacademic_year = new academic_year();
$AllYears = $libacademic_year->Get_All_Year_List();

$Content = '<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">' .
		'<thead><tr class="tabletop"><td align="center" width="30%">'.$iPort["ole_report"]["school_year"].'</td><td width="20%" align="center" class="tabletopnolink">CSV</td><td width="20%" align="center" class="tabletopnolink">'.$ec_iPortfolio['lastModifiedDate'].'</td><td width="30%" align="center" class="tabletopnolink">&nbsp;</td></tr></thead><tbody>';
for ($i=0; $i<sizeof($AllYears); $i++)
{
	//debug($AllYears[$i]["AcademicYearID"]);
	$tr_class = ($i%2==1) ? "tablerow1" : "tablerow2";
	
	$FileCSV = $PATH_WRT_ROOT."file/portfolio/sptss_pc_".$AllYears[$i]["AcademicYearID"].".csv";
	if (file_exists($FileCSV))
	{
		//$CSV_file = "<a href=\"".$FileCSV."\">".$ec_iPortfolio['View']."</a>";
		$csv_file_path = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".getEncryptedText($intranet_root."/file/portfolio/sptss_pc_".$AllYears[$i]["AcademicYearID"].".csv");
		$CSV_file = $linterface->GET_BTN($ec_iPortfolio['View'], "button", $ParOnClick="self.location='{$csv_file_path}'");
		$LastModified = date("Y-m-d H:i:s",filemtime($FileCSV));
	} else
	{
		$CSV_file = "N/A";
		$LastModified = "--";
	}


	$CSV_Upload = $linterface->GET_BTN($iPort["upload"], "button", $ParOnClick="showUpload(".$AllYears[$i]["AcademicYearID"].")");

	$Content .= "<tr class='{$tr_class}'><td align='center'>".$AllYears[$i]["AcademicYearName"]."</td>" .
			"<td align='center'>{$CSV_file}</td>" .
			"<td align='center'>{$LastModified}</td>" .
			"<td align='center'><div style='display:none' id='upload_".$AllYears[$i]["AcademicYearID"]."'><input type='file' name='CSV_".$AllYears[$i]["AcademicYearID"]."' id='CSV_".$AllYears[$i]["AcademicYearID"]."' /></div><div id='uploadbtn_".$AllYears[$i]["AcademicYearID"]."'>{$CSV_Upload}</div></td></tr>\n";
}
$Content .= "</tbody></table>";
############  END Cotnent Here ######################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">
var _validFileExtensions = [".csv", ".txt", ".CSV", ".TXT"];    

function showUpload(AcademicYearID)
{
	if ($('#CSV_'+AcademicYearID).val().length<=0)
	{
		$('#upload_'+AcademicYearID).show();
	} else
	{
		fileName = $('#CSV_'+AcademicYearID).val();
		dots =  fileName.split('.');
		fileType = "." + dots[dots.length-1];
		
		if( ($.inArray( fileType, _validFileExtensions)) > -1 ){
			document.form1.submit();
		}
		else{
			alert('Wrong File type');
		}
	}
	
}
</SCRIPT>

<br />
<form method="POST" name="form1" id="form1" action="personal_character_update.php" enctype="multipart/form-data" >

<fieldset class="instruction_box_v30"><legend><?=$Lang['General']['Instruction']?></legend>
	<div><?=$Lang['iPortfolio']['twghczm_csv_upload']. $linterface->Get_CSV_Sample_Download_Link($PATH_WRT_ROOT."home/download_attachment.php?target_e=".getEncryptedText($intranet_root."/home/portfolio/teacher/data_handling/sptss/sample.csv"))?></div>
</fieldset>


<?=$Content?>
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
