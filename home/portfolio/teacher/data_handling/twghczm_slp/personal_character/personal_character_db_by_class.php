<?php

// Modifing by anna

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();
$ldb = new libdb();
if (!$sys_custom['iPf']['twghczm']['Report']['SLP']) {
	header("location: /home/");
	exit;
}


if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}
########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

### view tag
// $viewTabAry[] = array($displayLang, $onclickJs, $iconPath, $isSelectedTab);
// $curViewTab = 'view1';
// $viewTabAry = array();
// $viewTabAry[] = array(Get_Lang_Selection('學生','Student'), 'personal_character_db.php', '', $curViewTab=='view2');
// $viewTabAry[] = array(Get_Lang_Selection('班別','Class'), 'personal_character_db_by_class.php', '', $curViewTab=='view1');
// $htmlAry['viewTag'] = $linterface->GET_CONTENT_TOP_BTN($viewTabAry);

$libacademic_year = new academic_year();

# academic year selection
$academic_year_arr = $libacademic_year->Get_All_Year_List();
$academicYearID = (isset($academicYearID))? $academicYearID : Get_Current_Academic_Year_ID();
$select_academic_year = getSelectByArray($academic_year_arr, 'name="academicYearID" class="select_academic_year" onchange="select_academic_year_onchange();jSUBMIT_FORM();"', $academicYearID, 1, 1, $i_Attendance_AllYear, 2);



if($lpf->IS_IPF_ADMIN()){
	$ClassTeacherTable = "";
	$ClassTeacherConds = "";
}else{
	$ClassTeacherTable = "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER AS yct ON yct.YearClassID = yc.YearClassID";
	$ClassTeacherConds = "AND yct.UserID = '$UserID'";
	$isClassTeacher = true;
}
# class data
$cond = empty($yearClassID) ? "" : " AND yc.YearClassID = ".$yearClassID;
if($_SESSION["USER_BASIC_INFO"]["is_class_teacher"] && !$lpf->IS_IPF_ADMIN()){
	$cond .= " AND yc.YearClassID in (SELECT `YearClassID` FROM `YEAR_CLASS_TEACHER` WHERE UserID='".$UserID."')";
}
$sql =  "SELECT
            CONCAT('<a href=\"personal_character_db_by_class_update.php?a=',".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").",'&YearClassId=', yc.YearClassID, '&AcademicYearID=',yc.AcademicYearID,'\" class=\"tablelink\">', ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", '</a>') AS DisplayName,
            IF(MAX(cspc.ModifiedDate) IS NULL, '--', MAX(cspc.ModifiedDate)) AS latestModify,
			yc.YearClassID
          FROM
            {$intranet_db}.YEAR_CLASS AS yc
          LEFT JOIN
            {$eclass_db}.CUSTOM_STUDENT_PERSONAL_CHARACTER  AS cspc ON yc.YearClassID = cspc.YearClassID
			  $ClassTeacherTable 
		WHERE
            yc.AcademicYearID = ".$academicYearID."
            $cond
          $ClassTeacherConds
          GROUP BY
            yc.YearClassID
		  Order BY 
			yc.ClassTitleEN
        ";
      
            $TableData = $lpf->returnResultSet($sql);
          
// TABLE COLUMN
$header= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>
<thead><tr class='tabletop'>";
$header .= "<td  width='100' height='25' align='center' class=\"tabletopnolink\" >#</span></td>";
$header .= "<td nowrap='nowrap' width='200' >".$i_general_class."</td>";
$header .= "<td class=\"tabletopnolink\">".$ec_iPortfolio['last_update']."</td>";
$header .= "</tr></thead>";

$table_content = $header;
$table_content .= "<tbody>";
$YearClassIDAry = array();
for($i=0;$i<sizeof($TableData);$i++){
	$bgColor = $i%2 == '0'? '#FFFFFF':'#F3F3F3';
	$ClassName = $TableData[$i]['DisplayName'];
	$LastModified = $TableData[$i]['latestModify'];
	$YearID= $TableData[$i]['YearID'];
	$YearClassIDAry[] = $TableData[$i]['YearClassID'];
	 $table_content .= "<tr class='tablebottom' >
							<td bgcolor='$bgColor' class=\"tabletext\" height=\"20\" align=\"center\">".($i+1)."</td>
							<td bgcolor='$bgColor' class=\"tabletext\" height=\"20\">".$ClassName."</td>
							<td bgcolor='$bgColor' class=\"tabletext\" height=\"20\">".$LastModified."</td>
						</tr>";
	
}
$YearClassIDList = implode(',',$YearClassIDAry);

$table_content .= "</tbody>
</table>";

$import_btn_html ="<a href=\"import.php?academicYearID={$academicYearID}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import}</a>";
$export_btn_html ="<a href=\"export.php?academicYearID={$academicYearID}&YearClassID={$YearClassIDList}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_export}</a>" ;

if($isClassTeacher){
	$import_btn_html= "";
}

############  END Cotnent Here ######################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">
function jSUBMIT_FORM(){
  document.form1.submit();
}

function select_academic_year_onchange(){
	$('.select_class').val('');
	$('#pageNo').val('1');
}

function select_class_onchange(){
	$('#pageNo').val('1');
}

</SCRIPT>

<br />

<form name="form1" method="POST">
	<?php echo $select_academic_year; ?>
	<table>
		<td nowrap>
		<?php echo $import_btn_html;?>
		<?php echo $export_btn_html;?>
		</td>
	</table>
	<?php echo $table_content; ?>

	<input type="hidden" name="YearClassID" value="<?=$YearClassIDList?>">


</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
