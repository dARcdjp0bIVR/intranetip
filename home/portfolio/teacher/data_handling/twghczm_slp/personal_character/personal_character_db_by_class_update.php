<?php
/*
 * Change Log:
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();
$ldb = new libdb();
$libclass = new libclass();
if (!$sys_custom['iPf']['twghczm']['Report']['SLP']) {
	header("location: /home/");
	exit;
}

if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################

$allowed=false;
$canEdit = false;
$rows_class_teacher=$libclass->returnClassTeacherByClassID($YearClassId);

foreach((array)$rows_class_teacher as $row_class_teacher){
	if($row_class_teacher['UserID']==$UserID){		
		$allowed=true;	
	}
}

if($lpf->IS_IPF_ADMIN()){
	$allowed=true;
	$canEdit = true;
}
 
if(!$allowed){
	echo 'You have no priviledge to access this page.';
	exit();
}

$htmlAry['PersonalCharacter'] = $linterface->GET_NAVIGATION2_IP25($iPort['menu']['personal_attributes']);
$htmlAry['Remarks'] = $linterface->GET_NAVIGATION2_IP25($ec_iPortfolio['remark']);

$PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
if($_POST['cmd']=='SUBMIT'){
	$YearClassId = $_POST['YearClassId'];
	$AcademicYearID = $_POST['AcademicYearID'];
	$i=0; //i student 
	while(isset($_POST['StudentID_'.$i]) && $_POST['StudentID_'.$i]>-1){
		$PersonalCharacterID = $_POST['PersonalCharacterID_'.$i];
		$StudentID = $_POST['StudentID_'.$i];
		$StudentRemark = $_POST['Remark_'.$i];
		
		$SQL = "SELECT TypeID 
					FROM $eclass_db.CUSTOM_STUDENT_PERSONAL_CHARACTER
					WHERE StudentID = '$StudentID' and YearClassID='$YearClassId'";
		$TypeIDAry =  $ldb->returnVector($SQL);
		

		//personal character
		for($j=0;$j<sizeof($PersonalCharacter);$j++){   // j Character
			$CharacterName = $PersonalCharacter[$j];
			$CharacterNameAry = explode(' ',$CharacterName);
			$CharacterNameWithUnderScore = implode('_',$CharacterNameAry);
			$Grade = $_POST[$CharacterNameWithUnderScore.'_'.$i];
		
			if(!in_array($j,$TypeIDAry)){
				$sql='INSERT INTO '.$eclass_db.'.`CUSTOM_STUDENT_PERSONAL_CHARACTER`
						(AcademicYearID,YearClassID,StudentID,TypeID,Grade,ModifiedDate,ModifiedBy)' .
						' VALUES
					(\''.$AcademicYearID.'\',\''.$YearClassId.'\',\''.$StudentID.'\',\''.$j.'\',\''.$Grade.'\',NOW(),\''.$UserID.'\')';
				
				$ldb->db_db_query($sql);
			}else{
				$sql='UPDATE '.$eclass_db.'.`CUSTOM_STUDENT_PERSONAL_CHARACTER` 
					SET 
						Grade=\''.$Grade.'\',
						ModifiedDate=NOW(),
						ModifiedBy=\''.$UserID.'\' 
					WHERE 
						`StudentID`='.$StudentID.' and `TypeID` = \''.$j.'\' AND `YearClassID` = \''.$YearClassId.'\';';
				$ldb->db_db_query($sql);
			}
			
			// student remark
			$sql = "Select Remark From $eclass_db.CUSTOM_STUDENT_REMARKS 
                    WHERE StudentID = '$StudentID' and YearClassID='$YearClassId' ";
			$RemarkAry =  $ldb->returnVector($sql);
			if(empty($RemarkAry)){
			  
			    $sql='INSERT INTO '.$eclass_db.'.`CUSTOM_STUDENT_REMARKS`
						(AcademicYearID,YearClassID,StudentID,Remark,ModifiedDate,ModifiedBy)' .
						' VALUES
					(\''.$AcademicYearID.'\',\''.$YearClassId.'\',\''.$StudentID.'\',\''.$StudentRemark.'\',NOW(),\''.$UserID.'\')';
			    
			    $ldb->db_db_query($sql);
			    
			}else{
			    $sql='UPDATE '.$eclass_db.'.`CUSTOM_STUDENT_REMARKS`
					SET
						Remark =\''.$StudentRemark.'\',
						ModifiedDate=NOW(),
						ModifiedBy=\''.$UserID.'\'
					WHERE
						`StudentID`='.$StudentID.' AND `YearClassID` = \''.$YearClassId.'\';';
			    $ldb->db_db_query($sql);
			}
		}		
		$i++;
	}
	header("Location: ".'personal_character_db_by_class.php');
}

$sql =  "SELECT
            CONCAT(".getNameFieldByLang2("iu.").") AS DisplayName,
            ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." AS ClassTitle,
            ycu.ClassNumber AS ClassNumber,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
			yc.YearClassID,iu.UserID,yc.YearID,yc.AcademicYearID
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
         
          WHERE
            yc.YearClassID = ".$YearClassId."
          GROUP BY
            iu.UserID
          ORDER BY
            ycu.ClassNumber
        ";
$rowsStudentInfo=$ldb->returnArray($sql);
############  END Cotnent Here ######################

$YearID = empty($rowsStudentInfo)? '' :$rowsStudentInfo[0]['YearID'];

$objIpfPeriodSetting = new iportfolio_period_settings();
$PeriodSettingsArray = $objIpfPeriodSetting->getSettingsArray();
$PeriodSettingsArray = $PeriodSettingsArray['TWGHCZM'][$YearID];

$PeriodSettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$PeriodSettingsArray['StartDate'], (array)$PeriodSettingsArray['StartHour'], (array)$PeriodSettingsArray['StartMinute']);
$SubmissionPeriod_startTime = $PeriodSettingsArray['StartDateTime'][0];
$PeriodSettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$PeriodSettingsArray['EndDate'], (array)$PeriodSettingsArray['EndHour'], (array)$PeriodSettingsArray['EndMinute']);
$SubmissionPeriod_endTime = $PeriodSettingsArray['EndDateTime'][0];
$SubmissionPeriod_isAllowed = $PeriodSettingsArray['AllowSubmit'];


if(($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00") && ($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00")){
	$Period_Msg = $iPort['period_start_end']." ".$SubmissionPeriod_startTime." ".$iPort['to']." ".$SubmissionPeriod_endTime;
}else if($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00"){
	$Period_Msg = $iPort['period_start']." ".$SubmissionPeriod_startTime;
}else if($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00"){
	$Period_Msg = $iPort['period_end']." ".$SubmissionPeriod_endTime;
}else{
	$Period_Msg = "";
}

$display_period = "<tr><td class=\"tabletext\"><b><font color=\"red\"> ".$Period_Msg."</font></b></td></tr>";

$dateNow = date("Y-m-d H:i:s");
//debug_pr($studentOleConfigDataArray);

if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow && $SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime!='' && $SubmissionPeriod_endTime=='')
{
	$cond =($SubmissionPeriod_startTime<$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime=='' && $SubmissionPeriod_endTime!='')
{
	$cond =($SubmissionPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}

if($cond==true)
{
//	$PersonalCharacterNotes = "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithinSubmissionPeriod'] ."</td></tr>";
	$canEdit = true;
}
else
{
//	$new_btn = '';
	$canEdit = false;
//	$PersonalCharacterNotes= "<tr><td><br/>". $ec_iPortfolio['SLP']['OleJoinNotes']['WithoutSubmissionPeriod'] ."</td></tr>";
}

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
echo $linterface->Include_CopyPaste_JS_CSS('2016');
echo $linterface->Include_Excel_JS_CSS();
// echo $linterface->Include_CopyPaste_JS_CSS();
?>

<br />
<FORM name="form1" method="POST">
<table border="0" cellspacing="0" cellpadding="0">
	<?=$display_period?>
</table>
<div><?php echo $htmlAry['PersonalCharacter'];?></div>
<table class="common_table_list_v30 edit_table_list_v30">
	<tr>
		<th width="17">#</th>
		<th width="18"><?php echo $Lang['SysMgr']['FormClassMapping']['Class']; ?></th>
		<th width="19"><?php echo $Lang['SysMgr']['FormClassMapping']['ClassNo']; ?></th>
		<th width="81"><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Student']; ?></th>
		<?php for ($i=0;$i<sizeof($PersonalCharacter);$i++){ ?>
		<th width="100"><?php echo $PersonalCharacter[$i]?></th>	
		<? } ?>
	</tr>
	<?php $i=0; ?>
	<?php foreach((array)$rowsStudentInfo as $StudentInfo){ 
		$StudentID = $StudentInfo['UserID'];
		$YearClassID = $StudentInfo['YearClassID'];
		$SQL = "SELECT * FROM {$eclass_db}.CUSTOM_STUDENT_PERSONAL_CHARACTER WHERE StudentID='$StudentID' and YearClassID='$YearClassID'";
		$Student_personal_character = $ldb->returnResultSet($SQL);
		$Grade = array();
		for($j=0;$j<sizeof($Student_personal_character);$j++){
			$TypeID = $Student_personal_character[$j]['TypeID'];
			$Grade[$TypeID] = $Student_personal_character[$j]['Grade'];
		}
	
		?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $StudentInfo['ClassTitle']; ?></td>
			<td><?php echo $StudentInfo['ClassNumber']; ?></td>
			<td>
				<?php echo $StudentInfo['DisplayName']; ?>
				<input type="hidden" name="StudentID_<?php echo $i; ?>" value="<?php echo $StudentInfo['UserID']; ?>" >
				<input type="hidden" name="PersonalCharacterID_<?php echo $i; ?>" value="<?php echo $StudentInfo['PersonalCharacterID']; ?>" >
			</td>
			<?php for($j=0;$j<sizeof($PersonalCharacter);$j++){?>
				<?php $CharacterName = $PersonalCharacter[$j];
				  	  $CharacterNameAry = explode(' ',$CharacterName);
					$CharacterNameWithUnderScore = implode('_',$CharacterNameAry);
				?>
			<?php if($canEdit || $lpf->IS_IPF_ADMIN()){?>
						<td><input size="5" type="text" id="mark[<?php echo $i;?>][<?php echo $j;?>]" name="<?php echo $CharacterNameWithUnderScore.'_'.$i; ?>" value="<?php echo $Grade[$j];?>" maxlength="1"  ></input></td>		
					<? }else{ ?>
					<td><?php echo $Grade[$j];?></td>
					<? }?>
			<?}?>
		</tr>
		<?php $i++; ?>
	<?php } ?>
</table>

<div><?php echo $htmlAry['Remarks'];?></div>
<table class="common_table_list_v30 edit_table_list_v30">
	<tr>
		<th width="17">#</th>
		<th width="18"><?php echo $Lang['SysMgr']['FormClassMapping']['Class']; ?></th>
		<th width="19"><?php echo $Lang['SysMgr']['FormClassMapping']['ClassNo']; ?></th>
		<th width="81"><?php echo $Lang['iPortfolio']['SPTSS']['SPC']['Student']; ?></th>	
		<th width="810"><?php echo $ec_iPortfolio['remark'];?></th>	
	</tr>
	<?php $i=0; ?>
	<?php foreach((array)$rowsStudentInfo as $StudentInfo){ 
		$StudentID = $StudentInfo['UserID'];
		$YearClassID = $StudentInfo['YearClassID'];
	
		$SQL = "SELECT * FROM {$eclass_db}.CUSTOM_STUDENT_REMARKS WHERE StudentID='$StudentID' and YearClassID='$YearClassID'";
		$Student_remarks = $ldb->returnResultSet($SQL);
		
		$studentRemark = $Student_remarks[0]['Remark'];
		?>
		<tr>
			<td><?php echo ($i+1); ?></td>
			<td><?php echo $StudentInfo['ClassTitle']; ?></td>
			<td><?php echo $StudentInfo['ClassNumber']; ?></td>
			<td>
				<?php echo $StudentInfo['DisplayName']; ?>
				<input type="hidden" name="StudentID_<?php echo $i; ?>" value="<?php echo $StudentInfo['UserID']; ?>" >
			</td>
			<?php if($canEdit || $lpf->IS_IPF_ADMIN()){?>
					<td><input size="135" type="text" id="mark2[<?php echo $i;?>][0]" name="<?php echo 'Remark_'.$i; ?>" value="<?php echo $studentRemark;?>" ></input></td>				
					<? }else{ ?>
					<td><?php echo $studentRemark;?></td>
				<? }?>
		</tr>
		<?php $i++; ?>
	<?php } ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td height="1" class="dotline"><img src="<?php echo $image_path.'/'.$LAYOUT_SKIN; ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td style="text-align:center;">
			<?php if(count($rowsStudentInfo)<=0 || (!$canEdit && !$lpf->IS_IPF_ADMIN())){ ?>
			<?php }else{ ?>
				<?php echo $linterface->GET_ACTION_BTN($button_submit, "submit", ""); ?>
			<?php } ?>
			<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();"); ?>
		</td>
	</tr>
</table>

<input type="hidden" name="YearClassId" value="<?php echo $YearClassId; ?>" >
<input type="hidden" name="AcademicYearID" value="<?php echo $AcademicYearID; ?>" >
<input type="hidden" name="YearID" value="<?php echo $YearID; ?>" >
<input type="hidden" name="cmd" value="SUBMIT" >
<textarea id="text1" name="text1" style="display:none" cols="10" rows="4"></textarea>
</form>

<SCRIPT LANGUAGE="Javascript">
	var xno = "6";yno = "<?php echo $i; ?>";		// set table size
	var jsDefaultPasteMethod = "text";		// for copypaste.js
</SCRIPT>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
