<?php
// Modifing by  : 
/***********
* 2018-02-26 Anna
* added remark when export
*
***********/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

// # Page Authentication
// $EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
// iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();

if (!$sys_custom['iPf']['twghczm']['Report']['SLP']) {
	header("location: /home/");
	exit;
}


if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}
$lexport = new libexporttext();


$delimiter = "\t";
$valueQuote = "";

$Content .= $valueQuote.'WebSAMSRegNo'.$valueQuote.$delimiter;
$Content .= $valueQuote.'Class'.$valueQuote.$delimiter;
$Content .= $valueQuote.'ClassNumber'.$valueQuote.$delimiter;
$Content .= $valueQuote.'StudentName'.$valueQuote.$delimiter;


// $HeaderAry[] =$Lang['SysMgr']['FormClassMapping']['Class'];
// $HeaderAry[] =$Lang['SysMgr']['FormClassMapping']['ClassNo'];
// $HeaderAry[] =$Lang['iPortfolio']['SPTSS']['SPC']['Student'];
$PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
for ($i=0;$i<sizeof($PersonalCharacter);$i++){
// 	$HeaderAry[] = $PersonalCharacter[$i];
	$Content .= $valueQuote.$PersonalCharacter[$i].$valueQuote.$delimiter;
}
$Content .= $valueQuote.'Remarks'.$valueQuote.$delimiter;
$Content .= "\n";

$AcademicYearID = $_GET['academicYearID'];
$YearClassIDList = $_GET['YearClassID'];
$YearClassIDAry = explode(',',$YearClassIDList);
$YearClassIDConds = implode('\',\'',$YearClassIDAry);

$sql =  "SELECT
            CONCAT(".getNameFieldByLang2("iu.").") AS DisplayName,
            ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." AS ClassTitle,
            ycu.ClassNumber AS ClassNumber,
            CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', iu.ClassNumber) AS ClassInfo,
            yc.YearClassID,iu.UserID,yc.YearID,iu.WebSAMSRegNo
         FROM
            {$intranet_db}.INTRANET_USER AS iu
         INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON
            iu.UserID = ycu.UserID
         INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
            ON
            ycu.YearClassID = yc.YearClassID
            
         WHERE
         	yc.YearClassID IN ('".$YearClassIDConds."')  
         GROUP BY
            iu.UserID
         ORDER BY
             ycu.YearClassID,ycu.ClassNumber
        ";
            $rows_personal_character=$lpf->returnArray($sql);
            
            
foreach((array)$rows_personal_character as $row_personal_character){
	$StudentID = $row_personal_character['UserID'];
	$yearClassID = $row_personal_character['YearClassID'];
	$Content .= $valueQuote.$row_personal_character['WebSAMSRegNo'].$valueQuote.$delimiter;
	$Content .= $valueQuote.$row_personal_character['ClassTitle'].$valueQuote.$delimiter;
	$Content .= $valueQuote.$row_personal_character['ClassNumber'].$valueQuote.$delimiter;
	$Content .= $valueQuote.$row_personal_character['DisplayName'].$valueQuote.$delimiter;
	$SQL = "SELECT * FROM {$eclass_db}.CUSTOM_STUDENT_PERSONAL_CHARACTER WHERE StudentID='$StudentID' and YearClassID='$yearClassID'";
	$Student_personal_character = $lpf->returnResultSet($SQL);
	$Grade = array();
	for($j=0;$j<sizeof($Student_personal_character);$j++){
		$TypeID = $Student_personal_character[$j]['TypeID'];
		$Grade[$TypeID] = $Student_personal_character[$j]['Grade'];
	}
	
	for($j=0;$j<sizeof($PersonalCharacter);$j++){
			$Content .= $valueQuote.$Grade[$j].$valueQuote.$delimiter;
	}
	
	$SQL = "SELECT * FROM {$eclass_db}.CUSTOM_STUDENT_REMARKS WHERE StudentID='{$StudentID}' AND AcademicYearID = '{$AcademicYearID}'";
	$Student_RemarkAry = $lpf->returnResultSet($SQL);
	$Remark = $Student_RemarkAry[0]['Remark'];
	$Content .= $valueQuote.$Remark.$valueQuote.$delimiter;
	$Content .= "\n";
}

$Semester = ($IsAnnual==1) ? "" : $Semester;
$ExportContent = $lpf->returnServiceExportContent($Year, $Semester, $ClassName);

intranet_closedb();

$filename = "student_personal_character.csv";
$filename = iconv("UTF-8", "Big5", $filename);
$lexport->EXPORT_FILE($filename, $Content);

?>
