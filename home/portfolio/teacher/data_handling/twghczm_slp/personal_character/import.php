<?php
//using: anna
###################################### Change Log [Start] ######################################################
#
# 2018-02-26 Anna
# - added remark when import
#
###################################### Change Log [End] ######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");


intranet_auth();
intranet_opendb();


// Initializing classes
$LibUser = new libuser($UserID);
$linterface = new interface_html();
$lpf = new libpf_slp();
$ldb = new libdb();

// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

if (!$sys_custom['iPf']['twghczm']['Report']['SLP']) {
	header("location: /home/");
	exit;
}


if(!$_SESSION["USER_BASIC_INFO"]["is_class_teacher"]){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ADMIN_ACCESS_PAGE();
}
########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_CUST_DATA_CZM);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['iPortfolio']['twghczm_pesonalcharacter'],"personal_character_db_by_class.php");
$PAGE_NAVIGATION[] = array($button_import, "");
$academicYearID = $_GET['academicYearID'];
$Import_Guide = $iPort["ole_import_guide_int"];
$SampleFile = "twghczm_pc_sample.csv";

$ReferenceHTML = "<table width='80%' border='0' cellspacing='0' cellpadding='3'>";
$ReferenceHTML .= "<tr><td colspan='3'>".$Import_Guide."</td></tr>";
$ReferenceHTML .= "<tr><td valign='top'>".$Category_HTML."</td><td width='70'>&nbsp;</td><td valign='top'>".$ELE_HTML."</td></tr>";
$ReferenceHTML .= "</table>";

### data column
$DataColumnTitleArr = array();
$DataColumnTitleArr[] = $ec_iPortfolio['WebSAMSRegNo'];
$DataColumnTitleArr[] = $ec_iPortfolio['ClassName'];
$DataColumnTitleArr[] = $ec_iPortfolio['ClassNumber'];


$PersonalCharacter = $Lang['iPortfolio']['twghczm']['PersonalCharacter']['Type'];
for($i=0;$i<sizeof($PersonalCharacter);$i++){
	$DataColumnTitleArr[] = $PersonalCharacter[$i];#
}
$DataColumnTitleArr[] = $ec_iPortfolio['remark'];

$DataColumnPropertyArr = array(2,2,2,0,0,0,0,0,0,0,0,0,0);

$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);
?>

<script language="JavaScript">

</script>

<FORM name="form1" method="post" action="import_update.php" enctype="multipart/form-data">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
<td>
<?=$htmlAry['steps']?>
</td></tr>
<tr>
	<td colspan="2">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
   		<td>
       	<br />
		<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
			<td class="tabletext"> <?=$linterface->Get_CSV_Sample_Download_Link($PATH_WRT_ROOT."home/download_attachment.php?target_e=".getEncryptedText($intranet_root."/home/portfolio/teacher/data_handling/twghczm_slp/personal_character/$SampleFile"))?></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?> </td>
			<td class="tabletext"><?=$DataColumn?></td>
		</tr>
		</table>
		</td>
	</tr>      
	<tr>
		<td colspan="2">        
		<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<tr>
        	<td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
		</tr>	
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">^</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][7]?></td>
		</tr>	
		<tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
			</td>
		</tr>
        </table>                                
		</td>
	</tr> 
	</table>
	</td>
</tr>
</table>


<input type="hidden" name="academicYearID" value="<?=$academicYearID?>" >


</form>
<?=$RemarksLayer?>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

