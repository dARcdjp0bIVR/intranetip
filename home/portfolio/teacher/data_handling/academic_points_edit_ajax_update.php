<?php

// Modifing by 
/*
 * 2016-11-29	Omas
 * 		- Create the page
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$lpf = new libpf_slp();

if( ( $lpf->IS_IPF_ADMIN() ||$_SESSION['USER_BASIC_INFO']['is_class_teacher'])
	&& $_POST['Score'] != '' 
	&& $_POST['AcademicYearID'] != ''
	&& $_POST['TermNumber'] != ''
	&& $_POST['SubjectID'] != ''
	&& $_POST['UserID'] != ''
  ){
	$sql = "UPDATE $eclass_db.SLP_STUDENT_SUBJECT_POINTS SET DateModified = now(), SCORE = '".$_POST['Score']."' 
			WHERE 
			 AcademicYearID = '".$_POST['AcademicYearID']."'
			 AND TermID = '".$_POST['TermNumber']."'
			 AND SubjectID = '".$_POST['SubjectID']."'
			 AND StudentID = '".$_POST['UserID']."'";
	$lpf->db_db_query($sql);
	
	$sql = "SELECT SCORE FROM $eclass_db.SLP_STUDENT_SUBJECT_POINTS 
		WHERE 
		 AcademicYearID = '".$_POST['AcademicYearID']."'
		 AND TermID = '".$_POST['TermNumber']."'
		 AND SubjectID = '".$_POST['SubjectID']."'
		 AND StudentID = '".$_POST['UserID']."'";
	$newValue = $lpf->returnVector($sql);
	
	if($newValue[0] > 0){
		echo $newValue[0];
	}else{
		echo -999;
	}
}
?>