<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_STPAUL_ACADEMIC);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$CSV_Data = unserialize(urldecode($ImportData));


$counter = 0;
for ($i=0; $i<sizeof($CSV_Data); $i++)
{
	list($RowStudentID, $RowAcademicYearID, $RowTerm, $RowSubjectID, $RowScore) = $CSV_Data[$i];
	if ($RowStudentID!="" && $RowStudentID>0 && $RowAcademicYearID!="" && $RowAcademicYearID>0  && $RowTerm!="" && $RowTerm>0  && $RowSubjectID!="" && $RowSubjectID>0  && $RowScore!="" && $RowScore>0 )
	$sql = "INSERT INTO ".$eclass_db.".SLP_STUDENT_SUBJECT_POINTS (StudentID, AcademicYearID, TermID, SubjectID, SCORE, DateInput) " .
			" VALUES " .
			" ('{$RowStudentID}', '{$RowAcademicYearID}', '{$RowTerm}', '{$RowSubjectID}', '{$RowScore}', now())";
	$result = $lpf->db_db_query($sql);
	if ($result)
	{
		$counter ++;
	} else
	{
	}
}

if ($counter>0)
{
	$checked_result = $counter . " ".$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_success'];
} else
{
	$checked_result = $Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_result_failed'];
}


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<br />
<form name="form1" method="get" action="academic_points_import.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2" align="center" height="90">
			<?=$checked_result?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['ImportOtherRecords'], "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "self.location='./academic_points.php'") ?>
		</td>
	</tr>
</table>
</form>
<br />


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>