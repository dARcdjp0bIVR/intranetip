<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once("../websams_functions.php");
intranet_opendb();

$ldb = new libdb();

$sql =  "
          SELECT
            REPLACE(ps.WebSAMSRegNo, '#', '') AS WebSAMSRegNo,
            sas.Details
          FROM
            {$eclass_db}.SELF_ACCOUNT_STUDENT AS sas
          INNER JOIN
            {$eclass_db}.PORTFOLIO_STUDENT AS ps
          ON
            sas.UserID = ps.UserID
          WHERE
            INSTR(sas.DefaultSA, 'SLP')
        ";
$t_self_acc_arr = $ldb->returnArray($sql);

for($i=0; $i<count($t_self_acc_arr); $i++)
{
  $t_user_id = $t_self_acc_arr[$i]['WebSAMSRegNo'];
  
  $t_details = $t_self_acc_arr[$i]['Details'];
  $t_details = preg_replace("[\r\n]", "", $t_details);
  # Map for <P>
  $t_details = preg_replace("/<P>(.*)/U", "$1\n", $t_details);
  $t_details = preg_replace("/<P>(.*)<\/P>/U", "$1\n", $t_details);
  # Map for <LI>
  $t_details = preg_replace("/<LI>(.*)<\/LI>/U", "$1\n", $t_details);
  $t_details = preg_replace("/<LI>(.*)/U", "$1\n", $t_details);
  # Remove tags
  $t_details = str_replace(array("<BR>", "&nbsp;"), array("\n", " "), $t_details);
  $t_details = strip_tags($t_details);
  
  $self_acc_arr[$t_user_id] = $t_details;
}

if(is_array($self_acc_arr))
{
  $fs = new libfilesystem();

  $folder_prefix = $WebSAMS_FilesPath."/self_account";
	if (file_exists($folder_prefix))
	{
    $fs->folder_remove_recursive($folder_prefix);
  }
	$fs->folder_new($folder_prefix);

  foreach($self_acc_arr AS $regno => $t_content)
  {
    $t_filename = $regno.".txt";
    $t_fullpath = $folder_prefix."/".$regno.".txt";
  
    $fh=fopen($t_fullpath,"wb"); 
    fwrite($fh,$t_content); 
    fclose($fh);
  }
  
  $t_zipfilename = $WebSAMS_FilesPath."/"."selfaccount.zip";
  $fs->file_remove($t_zipfilename);
  
  # Not use filesystem zip
  chdir($folder_prefix);
  $t_zipfilename = OsCommandSafe($t_zipfilename);
  exec("zip -r $t_zipfilename *");
  
  header("Location: ".str_replace($intranet_root, "", $t_zipfilename));
}
else
{
  header("Location: ../index.php");
}

?>