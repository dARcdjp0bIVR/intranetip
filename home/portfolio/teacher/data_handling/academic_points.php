<?php

// Modifing by 
/*
 * 2016-11-29	Omas
 * 		-Modified to add view for ClassTeacher, Edit function for classteacher, Export for Admin
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$accessClassTeacher = $_SESSION['USER_BASIC_INFO']['is_class_teacher'] && !(strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth"));

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
if(!$accessClassTeacher){
	$lpf->ADMIN_ACCESS_PAGE();
}

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_STPAUL_ACADEMIC);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############  Cotnent Here ######################
if(!$accessClassTeacher){
$sql = "select sp.AcademicYearID, y.YearNameEN, sp.TermID, count(sp.RecordID) As TotalRecord " .
		" from ".$eclass_db.".SLP_STUDENT_SUBJECT_POINTS AS sp LEFT JOIN ".$intranet_db.".ACADEMIC_YEAR AS y ON y.AcademicYearID=sp.AcademicYearID " .
		" group by sp.academicyearid, sp.TermID order by y.Sequence DESC, y.AcademicYearID DESC, TermID ";
$rows = $lpf->returnArray($sql);

$Terms = array();
$YearRecords = array();
for ($i=0; $i<sizeof($rows); $i++)
{
	$YearRecords[$rows[$i]["AcademicYearID"]][$rows[$i]["TermID"]] = array($rows[$i]["YearNameEN"], $rows[$i]["TotalRecord"]);
	if (!in_array($rows[$i]["TermID"], $Terms))
	{
		$Terms[] = $rows[$i]["TermID"];
	}
}
sort($Terms);

for ($j=0; $j<sizeof($Terms); $j++)
{
	$ColumnsSemesters .= '<td class="tabletopnolink">'.$ec_iPortfolio['semester'].' '.($j+1).'</td>';
}


$Content = '<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">' .
		'<thead><tr class="tabletop"><td align="center" width="50%">'.$iPort["ole_report"]["school_year"].'</td>'.$ColumnsSemesters.'</tr></thead><tbody>';
$counter = 0;
if (sizeof($YearRecords)>0 && $Terms>0)
{
	foreach ($YearRecords AS $RowAcademicYearID => $TermObj)
	{
		$counter ++;
		$tr_class = ($counter%2==1) ? "tablerow1" : "tablerow2";
		$Content .= "<tr class='{$tr_class}'>";

		# find year name
		if (is_array($TermObj) && sizeof($TermObj)>0)
		{
			foreach ($TermObj AS $TermNumner => $RowObj)
			{
				if ($RowObj[0]!="")
				{
					$YearNameEN = $RowObj[0];
					break;
				}
			}
		}
		$Content .= "<td align='center'>".$YearNameEN."</td>";	


		for ($j=0; $j<sizeof($Terms); $j++)
		{
			$YearNameEN = $TermObj[$Terms[$j]][0];

			$RowTotal = (int) $TermObj[$Terms[$j]][1];
			$DelButton = ($RowTotal>0) ? $linterface->GET_LNK_DELETE("javascript:jsDeleteData('{$YearNameEN}','{$RowAcademicYearID}', '$Terms[$j]')", $iPort["btn"]["delete"]) : "";
			$downloadBtn = ($RowTotal>0) ? $linterface->GET_SMALL_BTN($Lang['Button']['Download'], "button", "jsDownloadData('$RowAcademicYearID', '$Terms[$j]')") : "";
			$Content .= "<td align='center'><div class=\"content_top_tool\">
						<div class=\"Conntent_tool\">{$RowTotal}</div>
						$downloadBtn
						<div class=\"Conntent_search\">{$DelButton}</div>
						<br style=\"clear:both\">
					</div></td>";
		}
		$Content .= "</tr>\n";
	}
}
$Content .= "</tbody></table>";
############  END Cotnent Here ######################
}else{
############# Class Teacher Content ################
	$sql = "SELECT yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5 FROM $intranet_db.YEAR_CLASS as yc INNER JOIN $intranet_db.YEAR_CLASS_TEACHER as yct on yc.YearClassID = yct.YearClassID  WHERE yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND UserID = '".$_SESSION['UserID']."'";
	$teacherClassIDAssoc = BuildMultiKeyAssoc($lpf->returnResultSet($sql), 'YearClassID', array('ClassTitleEN'), 1 );
	$validClassIDArr = array_keys($teacherClassIDAssoc);
	$yearClassID = $_GET['Class'] != '' && in_array($_GET['Class'],(array)$validClassIDArr) ? $_GET['Class'] : array_shift(array_keys($teacherClassIDAssoc));
	$classSelect = getSelectByAssoArray($teacherClassIDAssoc, 'id="YearClassID" name="YearClassID" onChange="javascipt:jsReloadPage(this.value)"', $yearClassID,0,1);
	
	$sql = "SELECT UserID FROM YEAR_CLASS_USER WHERE YearClassID = '$yearClassID'";
	$studentArr = $lpf->returnVector($sql);
	
	$fields = " sssp.StudentID, sssp.AcademicYearID, sssp.TermID, sssp.SubjectID, sssp.SCORE ";
	$sql = "SELECT $fields FROM 
			$eclass_db.SLP_STUDENT_SUBJECT_POINTS as sssp 
			INNER JOIN $intranet_db.YEAR_CLASS_USER as ycu on sssp.StudentID = ycu.UserID
			INNER JOIN $intranet_db.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = sssp.AcademicYearID
			WHERE sssp.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND UserID IN ('".implode("','",(array)$studentArr)."')
			ORDER BY ycu.ClassNumber";
	$data = $lpf->returnResultSet($sql);
	
	$studentDataAssocArr = BuildMultiKeyAssoc($data, array('StudentID', 'TermID', 'SubjectID'),array('SCORE'),1);
	$studentIDArr = array_unique(Get_Array_By_Key($data,'StudentID'));
	$subjectArr = array_unique(Get_Array_By_Key($data,'SubjectID'));
	
	$sql = "SELECT iu.UserID, iu.EnglishName, iu.ChineseName, iu.WebSAMSRegNo, yc.ClassTitleEN as ClassName, ycu.ClassNumber as ClassNumber  
	FROM $intranet_db.INTRANET_USER as iu
	INNER JOIN $intranet_db.YEAR_CLASS_USER as ycu on iu.UserID = ycu.UserID
	INNER JOIN $intranet_db.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
	WHERE iu.UserID IN ('".implode("','", (array)$studentIDArr)."')";
	
	$studentInfoArr = $lpf->returnResultSet($sql);
	$studentAssoc = BuildMultiKeyAssoc($studentInfoArr, 'UserID');
	
	// subject
	$sql = "select RecordID, EN_SNAME from ".$intranet_db.".ASSESSMENT_SUBJECT where RecordID IN ('".implode("','",(array)$subjectArr)."') order by RecordStatus";
	$subjectAssoc = BuildMultiKeyAssoc($lpf->returnResultSet($sql), 'RecordID', array('EN_SNAME'),1 );
	
	$termArr = array(1,2);
	// build content table
	$table = "$classSelect";
	$table .= '<table id="EditTable" width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">';
	$table .= '<thead>';
	$table .= '<tr class="tabletop">';
	$table .= '<th class="tabletopnolink" width="5%">'.$ec_iPortfolio['number'].'</th>';
	$table .= '<th class="tabletopnolink" width="15%">'.$Lang['SysMgr']['RoleManagement']['Name'].'</th>';
	$table .= '<th align="center" class="tabletopnolink" width="5%">'.$ec_iPortfolio['semester'].'</th>';
	$numCol = 3;
	foreach((array)$subjectAssoc as $_subjectID => $_subjectSName){
		$table .= '<th align="center" class="tabletopnolink" width="'.(count($subjectAssoc)>0? 75/count($subjectAssoc) :'75').'%">'.$_subjectSName.'</th>';
		$numCol++;
	}
	$table .= '</tr>';
	$table .= '</thead>';
	$table .= '<tbody>';
	if(!empty($studentDataAssocArr)){
		$i = 0;
		foreach((array)$studentDataAssocArr as $_studentID => $_termSubjectArr){
			$table .= '<tr class="tablerow'.($i%2+1).'">';
			$table .= '<td rowspan="2">'.$studentAssoc[$_studentID]['ClassName'].'-'.$studentAssoc[$_studentID]['ClassNumber'].'</td>';
			$table .= '<td rowspan="2">'.$studentAssoc[$_studentID][Get_Lang_Selection('ChineseName','EnglishName')].'</td>';
			foreach((array)$termArr as $__termID){
				if($__termID==2){
					$table .= '<tr class="tablerow'.($i%2+1).'">';
				}
				$__termName = $ec_iPortfolio['semester'].' '.$__termID;
				$table .= '<td align="center">'.$__termName.'</td>';
				foreach((array)$subjectAssoc as $___sujectID => $____subjectSName){
					if(isset($_termSubjectArr[$__termID][$___sujectID])){
						$displayScore = $_termSubjectArr[$__termID][$___sujectID];
						$linkId = 'EditLink_'.Get_Current_Academic_Year_ID().'_'.$__termID.'_'.$___sujectID.'_'.$_studentID;
						$editScoreLink = '<a id="'.$linkId.'" href="javascript:jsEditData(\''.Get_Current_Academic_Year_ID().'\',\''.$__termID.'\',\''.$___sujectID.'\',\''.$_studentID.'\')">'.$displayScore.'</a>';
					}else{
						$editScoreLink = Get_String_Display("");
					}
					$table .= '<td align="center">'.$editScoreLink.'</td>';
				}
				$table .= '</tr>';
			}
			$i ++;
		}
	}else{
		$table .= '<tr class="tablerow'.($i%2+1).'">';
		$table .= '<td align="center" colspan="'.$numCol.'">'.	$ec_iPortfolio['no_record_exist'].'</td>';
		$table .= '</tr>';
	}
	$table .= '</tbody>';
	$table .= '</table>';
	
	$Content = $table;
}
############# End Class Teacher Content ################

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>

<SCRIPT LANGUAGE="Javascript">
var globalTemp = -999;
function jsReloadPage(classID){
	self.location = "academic_points.php?Class="+classID;
}

function jsEditData(AcademicYearID, TermNumber, SubjectID, UserID)
{
	if($('#loading').length > 0){
		alert('The previous changes not completed, please wait for a moment!');
		return false;
	}
	
	var id = AcademicYearID + '_' + TermNumber + '_' + SubjectID + '_' + UserID;
	var parm = AcademicYearID + ', ' + TermNumber + ', ' + SubjectID + ', ' + UserID;

	jsUnsetEditing();

	$('a#EditLink_'+ id +'').hide();
	$('a#EditLink_'+ id +'').after('<input type="text" id="editValue" size="2" maxlength="3" value="' + $('a#EditLink_'+ id +'').text() + '">');
	var btn = '&nbsp;<input id="editBtn" type="button" class="formsmallbutton" onclick="jsEditSubmit('+parm+')" value="Submit" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'">';
	var cancelBtn = '&nbsp;<input id="editCancelBtn" type="button" class="formsmallbutton" onclick="jsUnsetEditing()" value="Cancel" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'">';
	$('#editValue').after('<div id="editDiv">'+btn+cancelBtn+'</div');
	globalTemp = $('a#EditLink_'+ id +'').text();
	$('#editValue').select();
	
	$(function(){
		$('#editValue').keypress(function (e) {
		 var key = e.which;
		 
		 if(key == 13)  // the enter key code
		  {
			 $('#editBtn').click();
		    return false;  
		  }
		});
	});
}

function jsUnsetEditing(){
	if( globalTemp != -999 && globalTemp != $('#editValue').val()){
		if(!confirm('Your changes have not been saved, are you sure to continue?')){
			return false;
		}
	}
	// reset all editing element
	globalTemp = -999;
	$("a[id^='EditLink_']").show();
	jsHideEditingElement();
}

function jsHideEditingElement(){
	$("#editValue").remove();
	$("#editBtn").remove();
	$("#editCancelBtn").remove();
	$("div#editDiv").remove();
}

function jsEditSubmit(AcademicYearID, TermNumber, SubjectID, UserID){

	globalTemp = -999;
	
	var id = AcademicYearID + '_' + TermNumber + '_' + SubjectID + '_' + UserID;
	var editScore = $('#editValue').val();

	if(editScore == ''){
		alert('Cannot submit empty value!');
		return false;
	}
	
	jsHideEditingElement();
	$('a#EditLink_'+ id +'').after('<div id="loading"><?php echo $linterface->Get_Ajax_Loading_Image(1)?></div>');
	
	$.ajax({
        type:"POST",
        url: 'academic_points_edit_ajax_update.php',
        data: {
            	"AcademicYearID" : AcademicYearID,
            	"TermNumber" : TermNumber,
            	"SubjectID" : SubjectID,
            	"UserID" : UserID,
            	"Score" : editScore,
              },
        success:function(data)
        {
            $('#loading').remove();
        	$('a#EditLink_'+ id +'').show();
        	if(data != -999){
        	$('a#EditLink_'+ id +'').html(data);
        	}
        	// for debug
        	//$('a#EditLink_'+ id +'').after('<div>'+data+'</div>');
        }
      })
}

function jsDownloadData(AcademicYearID, TermNumber)
{
	self.location = "academic_points_export.php?AcademicYearID="+AcademicYearID+"&TermNumber="+TermNumber;
}

function jsDeleteData(AcademicYearNameEN, AcademicYearID, TermNumber)
{
	var strMsg = "<?=$Lang['iPortfolio']['SLP_REPORT']['AcademicPerformance_Data_removal']?>";
	strMsg = strMsg.replace("[YEAR]",AcademicYearNameEN);
	strMsg = strMsg.replace("[TERM]",TermNumber);
	if (confirm(strMsg))
	{
		self.location = "academic_points_remove.php?Year="+AcademicYearID+"&Term="+TermNumber;
	}
	
}
</SCRIPT>

<br />

<!--form method="get" name="form1" id="form1" -->

<?php if(!$accessClassTeacher){?>
<fieldset class="instruction_box_v30"><legend><?=$Lang['General']['Instruction']?></legend>
	<div><?=$Lang['iPortfolio']['academic_performance_csv_upload']?></div>
</fieldset>

<div class="content_top_tool">
	<div class="Conntent_tool"><?= $linterface->GET_LNK_IMPORT("academic_points_import.php") ?></div>
	<div class="Conntent_search"></div>
	<br style="clear:both">
</div>
<?php }?>
<div class="Conntent_tool">
		
</div>


<?=$Content?>
<!--/form-->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
