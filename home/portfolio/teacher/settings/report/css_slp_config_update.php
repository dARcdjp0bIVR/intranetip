<?
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-04-20 Omas
 * - Create this page
 * 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_auth();
intranet_opendb();
$libgeneralsettings = new libgeneralsettings();

$TeacherListAry = $_POST['TeacherList']; // Head of School Array
$toGeneralSettingArr = array();
if(is_array($TeacherListAry)){
	foreach($TeacherListAry as $_yearId => $_userId){
		$toGeneralSettingArr['cust_CSS_slp_HeadOfSchoolConfig_'.$_yearId] = $_userId;
	}	
}

$libgeneralsettings->Save_General_Setting('iPortfolio',$toGeneralSettingArr);

intranet_closedb();

header("Location: css_slp_config.php?msg=update");
?>