<?php

// Mdoifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/html_editor_embed_lib.php");

//Extends iportfolio Class
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");

//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


$lpf = new libpf_report();

// load transcript settings
$transcript_content = $lpf->retrievePortfolioTranscriptConfig();
$school_description_html = $transcript_content["school_description"];

$school_address_html = $transcript_content["school_address"];
$PhotoConfig = $transcript_content["PhotoConfig"];

$school_badge_image = "<img src='http://$eclass_httppath/files/portfolio/transcript/template_0/images/badge.gif'/>";
$file_exist = file_exists($eclass_root."/files/portfolio/transcript/template_0/csv/data.csv");
$CSV_file_link = ($file_exist==1) ? "<a href='http://$eclass_httppath/files/portfolio/transcript/template_0/csv/data.csv' class='tablelink' target='_blank'><img src='http://$eclass_httppath/images/icon/resources/files/xls.gif' border='0' hspace='5' align='absmiddle' />data.csv</a>" : "-";


#################################################################

# define the navigation, page title and table size
// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_Reports";
$CurrentPageName = $iPort['menu']['reports'];

$luser = new libuser($UserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("full_report_config.php", $ec_iPortfolio['full_report_config'], 0);
$TabMenuArr[] = array("slp_config.php", $ec_iPortfolio['slp_config'], 0);
if ($sys_custom['iPf']['pkms']['Report']['SLP']){
	$TabMenuArr[] = array("plms_slp_config.php", $ec_iPortfolio['pkms_SLP_config'], 0);
}
if($sys_custom['iPf']['css']['Report']['SLP']){
	$TabMenuArr[] = array("css_slp_config.php", 'Student Development Progress Report', 0);
}
$TabMenuArr[] = array("student_transcript_config_edit.php", $ec_iPortfolio['transcript_config'], 1);

$linterface->LAYOUT_START();

?>
<script language="javascript">
function ChangeTemplate(para)
{
	if(para=="image")
	{
	var upload_image = document.getElementById('upload_image');
//	upload_image.innerHTML = "<table width=100%><tr><td><input type=file name=school_badge class=textboxtext /></td></tr><tr><td><input name=submit type=button class=formsubbutton onClick=ChangeTemplate('image_back');return document.MM_returnValue  onMouseOver=this.className='formsubbuttonon' onMouseOut=this.className='formsubbutton' value='Cancel Upload new file'></td></tr></table>";
	upload_image.innerHTML = "<table width=100%><tr><td><input type=file name=school_badge class=textboxtext /></td></tr><tr><td class='tabletextremark'><span class='tabletextrequire'>*</span><?=$ec_warning['overwrite_file']?></td></tr></table>";
	}else if(para=="xls"){
		
	var upload_xls = document.getElementById('upload_xls');
//	upload_xls.innerHTML = "<table width=100%><tr><td><input class=file type=file name='CSV_file' size='55' />&nbsp;<a href=http://<?=$eclass_httppath?>src/portfolio/profile/management/transcript_info_sample.csv class=contenttool target='new'> Download Sample</a></td></tr><tr><td><input name=submit type=button class=formsubbutton onClick=ChangeTemplate('xls_back');return document.MM_returnValue  onMouseOver=this.className='formsubbuttonon' onMouseOut=this.className='formsubbutton' value='Cancel Upload new file'></td></tr></table>";
		upload_xls.innerHTML = "<table width=100%><tr><td><input class=file type=file name='CSV_file' size='55' />&nbsp;<a href=http://<?=$eclass_httppath?>src/portfolio/profile/management/transcript_info_sample.csv class=contenttool target='new'> <?=$i_general_clickheredownloadsample?></a></td></tr><tr><td class='tabletextremark'><span class='tabletextrequire'>*</span><?=$ec_warning['overwrite_file']?></td></tr></table>";
	}
	
	if(para=="image_back")
	{
			var upload_image = document.getElementById('upload_image');
			upload_image.innerHTML = "<input name='button' class='formsubbutton' onclick=ChangeTemplate('image') onmouseover=this.className='formsubbuttonon' onmouseout=this.className='formsubbutton' value='Upload new file' type='button'>";

	}else if(para=="xls_back"){
		
			var upload_xls = document.getElementById('upload_xls');
			upload_xls.innerHTML = "<input name='button' class='formsubbutton' onclick=ChangeTemplate('xls') onmouseover=this.className='formsubbuttonon' onmouseout=this.className='formsubbutton' value='Upload new file' type='button'>";
	}

		
}
</script>
<FORM action="student_transcript_config_edit_update.php" method="POST" name="form1" enctype="multipart/form-data">
<?php if($msg==2) echo "<p align='left'><font color='green'>".$con_msg_update."</font></p>"; ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
				<td>
						<!-- Tab Start -->
  				<!--table border="0" cellpadding="5" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                    <td class="tab_underline">
                                    <div class="shadetabs">
                                    <ul>
                                      <li><a href="full_report_config.php"><//?=$ec_iPortfolio['full_report_config']?></a></li>
                                      <li><a href="slp_config.php"><//?=$ec_iPortfolio['slp_config']?></a></li>
                                      <//?php if ($sys_custom['iPf']['pkms']['Report']['SLP']){ ?>
                                      			<li><a href="plms_slp_config.php"><//?=$ec_iPortfolio['pkms_SLP_config']?></a></li>
									  <//?php	} ?>
                                      <li class="selected"><a href="#"><strong><//?=$ec_iPortfolio['transcript_config']?></strong></a></li>
                                      <!--<li><a href="customized_file_manage.php"><//?=$ec_iPortfolio['csv_report_config']?></a></li>-->
                                    <!--/ul>
                                    </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table-->	
                              <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
           <!-- Tab End -->
				</td>
		</tr>
		<tr>
		<td>
	<table align="center" border="0" cellpadding="5" cellspacing="0" width="95%">
        <tbody>
        <tr>
          <td width="10">&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_words['school_badge']?></td>
          <td valign="top">
          		
          		       <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                            <tbody>
                                              <tr>
                                                <td><?=$school_badge_image?></td>
                                              </tr>
                                              <tr>
                                                <td><div id="upload_image"><input name="button" class="formsubbutton" onclick="ChangeTemplate('image')" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="<?=$i_select_file?>" type="button"></div></td>
                                              </tr>
                                            </tbody>
                          </table>
          </td>
        </tr>
<!--
        <tr>
          <td width="10">&nbsp;</td>
          <td class="formfieldtitle" nowrap="nowrap" valign="top"><?=$ec_words['school_address']?></td>
          <td><span class="tabletext"><textarea name="school_address" cols="60" rows="3"><?=$school_address_html?></textarea></span></td>
        </tr>
-->
        <tr>
          <td width="10">&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['transcript_description']?></td>
          <td valign="top">
<?php
/*
	#html editior
	// Create the object
	$obj_name = "school_description";
	$contents = $school_description_html;
	$JS_Editor = new html_editor_embed_lib("contents", $is_included_before, "100%", "320px", "", $init_html_content);
	
	echo $JS_Editor->GET_HTML_EDITOR($obj_name, $contents);
*/

	$use_html_editor = true;
	if ($use_html_editor)
	{
		# Components size
		$msg_box_width = "100%";
		$msg_box_height = 200;
		$obj_name = "school_description";
		$editor_width = $msg_box_width;
		$editor_height = $msg_box_height;
		$init_html_content = $school_description_html;
		#$init_html_content = $preset_message;
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		
		$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
		$objHtmlEditor->Value = $init_html_content;
    $objHtmlEditor->Create();
	}
	else
	{
		echo "<textarea class=\"textboxtext\" name=\"school_description\" ID=\"school_description\" cols=\"80\" rows=\"16\" wrap=\"virtual\"></textarea>";
	}
?>
		 </td>
        </tr>
		<tr>
		  <td width="10">&nbsp;</td>
          <td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><?=$ec_iPortfolio['transcript_CSV_file']?></td>
          <td valign="top">
          		<table border="0" cellpadding="2" cellspacing="0" width="100%">
                       <tbody>
                             <tr>
                               <td><?=$CSV_file_link?></td>
                             </tr>
                            <tr>
                              <td><div id="upload_xls"><input name="button" class="formsubbutton" onclick="ChangeTemplate('xls')" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="<?=$i_select_file?>" type="button"></div></td>
                            </tr>
                        </tbody>
                   </table>
          </td>
        </tr>
		<tr>
		<td width="10">&nbsp;</td>
		 <td class="formfieldtitle" nowrap="nowrap" valign="top">&nbsp;</td>
		<td width="80%"><input type="checkbox" name="PhotoConfig" id="photo_1" value="1" <?=($PhotoConfig==1?"CHECKED":"")?> />&nbsp;<label for="photo_1"><?= $ec_iPortfolio['not_display_student_photo'];?></label></a></td>
		</tr>
		<tr>
		<td width="10">&nbsp;</td>
		 <td class="dotline" height="1"><img src="<?=$PATH_WRT_ROOT?>images/2007a/10x10.gif" height="1" width="10"></td>
	     <td class="dotline" height="1"><img src="<?=$PATH_WRT_ROOT?>images/2007a/10x10.gif" height="1" width="10"></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
      </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<input class="formbutton" type="button" value="<?=$ec_iPortfolio['preview']?>" onClick="newWindow('/home/portfolio/profile/report/transcript_print.php?StudentID=0', 10)" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
			<input class="formbutton" type="submit" value="<?=$button_submit?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
			<input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
			<!--<input class="formbutton" type="button" value="<?=$button_cancel?>" onClick="self.location='student_transcript_config.php'" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">-->

		</td>
		</tr>
	</table>
<input type=hidden name="recordID" value="<?=$recordID?>">
<input type=hidden name="codeID" value="<?=$codeID?>">
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
