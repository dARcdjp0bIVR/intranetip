<?php

/** [Modification Log] Modifying By: 
 * *******************************************
 * 2015-04-20 Omas
 * - Added CSS SLP Report Tab
 * 
 * 2014-07-15 Bill
 * - Added PKMS SLP Report Tab
 * 
 * 2010-06-04 Max (201006041431)
 * - Added setting for page-break
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();
$objIpfSetting = iportfolio_settings::getInstance();

$reportIsAllowed		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIsAllowed"]);
$reportIssueDate		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIssuesDate"]);
$reportLang				= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpLang"]);
$reportFormAllow		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"]);
$reportIsPrintIssueDate = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithIssuesDate"]);
$ShowSchoolHeaderImage  = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"]);
$AcademicScoreDisplayMode = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["academicScoreDisplayMode"]);
$DisplayFullMark 		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayFullMark"]);
$DisplayComponentSubject  = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayComponentSubject"]);

$ShowSchoolHeaderImage = ($ShowSchoolHeaderImage==1)? 'checked':'';

### $AcademicScoreDisplayMode=='' means that the client does not set the config.
if($AcademicScoreDisplayMode==$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade'] || $AcademicScoreDisplayMode=='')
{
	$markThenGrade_check = 'checked';
	$gradeThenMark_check = '';
}
else
{
	$markThenGrade_check = '';
	$gradeThenMark_check = 'checked';
}

### $DisplayFullMark=='' means that the client does not set the config.
if($DisplayFullMark==1 || $DisplayFullMark=='')
{
	$DisplayFullMark_YesCheck = 'checked';
	$DisplayFullMark_NoCheck = '';
}
else
{
	$DisplayFullMark_YesCheck = '';
	$DisplayFullMark_NoCheck = 'checked';
}


if($DisplayComponentSubject==1) {
	$DisplayComponentSubject_YesCheck = 'checked';
	$DisplayComponentSubject_NoCheck = '';
}
else {
	$DisplayComponentSubject_YesCheck = '';
	$DisplayComponentSubject_NoCheck = 'checked';
}



$reportLangInfo = explode(',',$reportLang);
$reportFormAllowInfo = explode(',',$reportFormAllow);

#Report Setting Name and Right
$report_setting = array("default",$ec_iPortfolio['academic_performance'], $iPort["ole_report"]["other_learning_experiences"], $ec_iPortfolio['list_award'], $iPort["external_ole_report"]["performance"], $ec_iPortfolio['student_self_account']);
$user_right_array = array("default","assessment_report","ole","award","ole","");

// load settings
$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";

if(file_exists($portfolio_report_config_file))
{
	list($filecontent, $OLESettings, $ExtSettings,$pageBreakSetting) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
	$config_array = unserialize($filecontent);
	$OLESettings = unserialize($OLESettings);
	$ExtSettings = unserialize($ExtSettings);
	$pageBreakSetting = unserialize($pageBreakSetting);
}
else
{
	for($j=1; $j<=count($report_setting)-1; $j++)
	{
		$config_array[$j][0] = $j;
		$config_array[$j][1] = 1;
	}
}

################################################
##	Student Details Config
$StudentDetailsConfig = !$config_array[6][0];
if ($StudentDetailsConfig) {
	$studentDetailsConfigChecked = " checked='checked' ";
} else {
	// do nothing
}
$sub_setting = "<a href='#TB_inline?height=330&width=400&inlineId=StudentDetailsSettings&modal=true' class='tablelink thickbox' style='display:none' id='sdc_thickBox'>".$i_general_settings."</a>";
$studentDetailsConfigHtml = <<<HTMLEND
				<tr class="tablerow1">
					<td class="row_on" align="center"><input type="checkbox" name="StudentDetailsConfig" id="details_1" onchange="setDisplaySDCSettings()" value="1" {$studentDetailsConfigChecked} /></td>
					<td class="row_on" align="center">--</td>
					<td class="row_on tabletext">{$ec_iPortfolio['no_student_details']}</td>
					<td class="row_on tabletext">&nbsp;&nbsp;&nbsp;{$sub_setting}</td>
				</tr>
HTMLEND;
## Thick Box
$StudentDetailsConfigDetails = $config_array[6][1];
$StudentDetailsConfigSettingStr = "";
$studentDetailsConfigArray = array(
									"StudentName"=>$i_UserStudentName,
									"DateOfBirth"=>$i_UserDateOfBirth,
									"EducationInstitution"=>$ec_iPortfolio['institute'],
									"AdmissionDate"=>$ec_iPortfolio['admission_date'],
									"SchoolAddress"=>$ec_words['school_address'],
									"SchoolPhone"=>$ec_iPortfolio['school_phone'],
									"HKID"=>$i_HKID,
									"Gender"=>$i_UserGender,
									"SchoolCode"=>$ec_iPortfolio['school_code'],
								);
foreach($studentDetailsConfigArray as $key => $value) {
	$checked = "";
	if ($StudentDetailsConfigDetails[$key]) {
		$checked = " checked='checked' ";
	} else {
		// do nothing
	}
	$StudentDetailsConfigSettingStr .=	<<<HTMLEND
	    		<tr>
	    			<td align="center" class="tabletext"><input type="checkbox" id="sdc_{$key}" value="1" name="sdc_{$key}" $checked /><label for="sdc_{$key}">{$value}</label></td>
	    		</tr>
HTMLEND;
}
################################################

// order selection
for($j=1; $j<=count($report_setting)-1; $j++)
{
	$t_order = $config_array[$j][0];
	$t_display = $config_array[$j][1];
	$disabled = ($t_display==1) ? " " : "disabled";

	${"order_select".$j} = $t_order;
	
	$check = ($t_display==1) ? "CHECKED" : "";
	${"display_check".$j} = "<input type='checkbox' name='display$j' onClick='set_disable($j)' value=1 $check>";
}

/**
 * Generate a check box for setting page break
 * @param INT $ParIdentifer the number of $config_array[0] <== the number of modules
 * @param BOOLEAN $ParChecked used to set the checkbox checked
 * @return HTML a check box element
 */
function getCheckboxForSettingPageBreak($ParIdentifier, $ParChecked=false) {
	$checked = ($ParChecked?" checked=\"checked\" ":"");
	$HTML = <<<HTMLEND
	<input type="checkbox" value="1" id="pageBreakSet_{$ParIdentifier}" name="pageBreakSet_{$ParIdentifier}" $checked /><label for="pageBreakSet_{$ParIdentifier}">Page Break Before This Item</label>
HTMLEND;
	return $HTML;
}
//debug_r($config_array);
# setting rows


for($i=1;$i<=count($report_setting)-1;$i++)
{
	$t_display = $config_array[$i][1];
	$bclass = ($t_display==1) ? "class='row_on'" : "class='row_off'";
	
	if($report_setting[$i] == $iPort["ole_report"]["other_learning_experiences"])
		$sub_setting = "<a href='#TB_inline?height=250&width=400&inlineId=OLESettings&modal=true' class='tablelink thickbox'>".$i_general_settings."</a>";
	else if($report_setting[$i] == $iPort["external_ole_report"]["performance"])
		$sub_setting = "<a href='#TB_inline?height=250&width=400&inlineId=ExtSettings&modal=true' class='tablelink thickbox'>".$i_general_settings."</a>";
	else
		$sub_setting = "&nbsp;";
	
	if($user_right_array[$i] == "" || strstr($ck_user_rights_ext, $user_right_array[$i]))
	{
		$template_row[$i] =	"
													<tr class='tablerow1'>
														<td>
															<table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'>
																<tbody>
																	<tr>
																		<td id='td".$i."n1' $bclass align='center'>".${'display_check'.$i}."<input type='hidden' value='$i' name='order[]'/></td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td>
															<table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'>
																<tbody>
																	<tr>
																		<td id='td".$i."n2' $bclass align='center'>
																			<table border='0' cellpadding='2' cellspacing='0'>
																				<tbody>
																					<tr>
																						<td><a href='javascript:;' onClick='swapRowUp(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif' border='0' height='13' width='13'></a></td>
																					</tr>
																					<tr>
																						<td><a href='javascript:;' onClick='swapRowDown(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode)'><img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif' border='0' height='13' width='13'></a></td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td>
															<table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'>
																<tbody>
																	<tr>
																		<td id='td".$i."n3' $bclass><span class='tabletext'>" . getCheckboxForSettingPageBreak($i,$pageBreakSetting[$i]["IS_SET_PAGE_BREAK"]) . "<br/>".$report_setting[$i]."</span></td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td>
															<table border='0' cellpadding='0' cellspacing='0' width='100%' height='40'>
																<tbody>
																	<tr>
																		<td id='td".$i."n4' $bclass>".$sub_setting."</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												";
	}
}


# define the navigation, page title and table size
// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_Reports";
$CurrentPageName = $iPort['menu']['reports'];

$lpf = new libportfolio2007();

// check iportfolio admin user
if (!$lpf->IS_ADMIN_USER($UserID) && !$lpf->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);
$ClassLevelArr = $lpf->GET_CLASSLEVEL_LIST();

// OLE selected record setting
$OLESettingStr = "";
for($i=0; $i<count($ClassLevelArr); $i++)
{
	$TeacherCheck = ($OLESettings[$ClassLevelArr[$i]][1] == 1) ? "checked" : "";
	$StudentCheck = ($OLESettings[$ClassLevelArr[$i]][2] == 1) ? "checked" : "";

	$OLESettingStr .=	"
      								<tr>
      									<td class=\"tabletext\"><input type=\"hidden\" name=\"ClassLevel[]\" value=\"".$ClassLevelArr[$i]."\">".$ClassLevelArr[$i].":</td>
      									<td align=\"center\" class=\"tabletext\"><input type=\"text\" name=\"RecordsAllowed[]\" size=\"3\" value=\"".$OLESettings[$ClassLevelArr[$i]][0]."\" /></td>
      									<td align=\"center\" class=\"tabletext\"><input type=\"checkbox\" name=\"TeacherAllowed[]\" value=\"".$ClassLevelArr[$i]."\" ".$TeacherCheck." /></td>
      									<td align=\"center\" class=\"tabletext\"><input type=\"checkbox\" name=\"StudentAllowed[]\" value=\"".$ClassLevelArr[$i]."\" ".$StudentCheck." /></td>
      								</tr>
      							";
}
$NoPrintToLimit = $OLESettings["NoPrintToLimit"] ? "checked=\"checked\"" : "";

// External Performance selected record setting
$ExtSettingStr = "";
for($i=0; $i<count($ClassLevelArr); $i++)
{
	$TeacherCheck = ($ExtSettings[$ClassLevelArr[$i]][1] == 1) ? "checked" : "";
	$StudentCheck = ($ExtSettings[$ClassLevelArr[$i]][2] == 1) ? "checked" : "";

	$ExtSettingStr .=	"
							<tr>
								<td class=\"tabletext\"><input type=\"hidden\" name=\"ExtClassLevel[]\" value=\"".$ClassLevelArr[$i]."\">".$ClassLevelArr[$i].":</td>
								<td align=\"center\" class=\"tabletext\"><input type=\"text\" name=\"ExtRecordsAllowed[]\" size=\"3\" value=\"".$ExtSettings[$ClassLevelArr[$i]][0]."\" /></td>
								<td align=\"center\" class=\"tabletext\"><input type=\"checkbox\" name=\"ExtTeacherAllowed[]\" value=\"".$ClassLevelArr[$i]."\" ".$TeacherCheck." /></td>
								<td align=\"center\" class=\"tabletext\"><input type=\"checkbox\" name=\"ExtStudentAllowed[]\" value=\"".$ClassLevelArr[$i]."\" ".$StudentCheck." /></td>
							</tr>
						";
}
$ExtNoPrintToLimit = $ExtSettings["NoPrintToLimit"] ? "checked=\"checked\"" : "";


$h_formChk = _getFormAllowCheckBoxUI($reportFormAllowInfo);


$h_slpSettingStyle = 'style="display:none"';
$h_allowStudentPrintSLPChecked = '';

if($reportIsAllowed == 1){
	//setting is allow to student to print report
	$h_slpSettingStyle = '';
	$h_allowStudentPrintSLPChecked = ' checked ';
}

### Upload photo fail

if($UploadPhotoFail==1)
{
	$failMsg = $ec_iPortfolio['upload_photo_fail'];
}

/********* Read School Image  *******/
$SchoolImgDir = $intranet_root.$ipf_cfg['SLPArr']['SLPFolderPath'];
 
if(is_dir($SchoolImgDir)) {

	$dh = opendir($SchoolImgDir);
 
	$LogofileName = $ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'];
	$ExistFileName = '';
	 
	while (($file = readdir($dh)) !== false)
	{		
		$pos = strpos($file,$LogofileName);
		
		if($pos === false) {
			 // string needle NOT found in $file
		}
		else {
		 	$ExistFileName = $file;
		}
	}
	closedir($dh);

	$SchoolImgFile = $SchoolImgDir.$ExistFileName;
	if(file_exists($SchoolImgFile))
	{
		$RandNo = time();
		//$SchoolImageFile_HTTP = get_website().$ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;
		//$SchoolImageFile_HTTP = 'http://'.$HTTP_SERVER_VARS["HTTP_HOST"].$ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;
		$SchoolImageFile_HTTP = $ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;
		//'http://'.$HTTP_SERVER_VARS["HTTP_HOST"].
		$thisBorder =  'border-left: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black;';
		$schoolHeaderImage = ($SchoolImgFile != '') ? '<img src="'.$SchoolImageFile_HTTP.'?rand='.$RandNo.'" style="'.$thisBorder.'"></img>' : '&nbsp;';
	
		$pageURL = curPageURL(0,0);

		$SchoolImageFile_ImageSize_HTTPpath = $intranet_root.$ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;

		$sizeArr = getimagesize($SchoolImageFile_ImageSize_HTTPpath); 

							
		$thisWidth = $sizeArr[0];
		$thisHeight = $sizeArr[1];

		$SchoolImageRow = '	<tr id="SchoolHeaderImageRow" >
								<td class="" style="" nowrap="nowrap" valign="top"><span class="tabletext">'.$ec_iPortfolio['SchoolHeaderImage'].'</span></td>
								<td>
								'.$schoolHeaderImage.'
								</td>
							</tr>
							<tr id="ActualImageSizeRow" >
								<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext">&nbsp;</span></td>
								<td>
								<font color="#3333CC">'.$ec_iPortfolio['SchoolHeaderImageSizeActual'].'<br/>'.$ec_iPortfolio['Width'].': '.$thisWidth.'px;      '.$ec_iPortfolio['Height'].': '.$thisHeight.'px;</font>
								</td>
							</tr>';
	   
	}
}
$SuggestImageSizeRow_html = $ec_iPortfolio['SchoolHeaderImageSizeSuggest'].'<br/>'.$ec_iPortfolio['Width'].': 756px(~20cm);      '.$ec_iPortfolio['Height'].': 151px(~4cm)';
/********* END Read School Image  *******/

// Tab Menu Settings
$TabMenuArr = array();
$TabMenuArr[] = array("full_report_config.php", $ec_iPortfolio['full_report_config'], 0);
$TabMenuArr[] = array("slp_config.php", $ec_iPortfolio['slp_config'], 1);
if ($sys_custom['iPf']['pkms']['Report']['SLP']){
	$TabMenuArr[] = array("plms_slp_config.php", $ec_iPortfolio['pkms_SLP_config'], 0);
}
if($sys_custom['iPf']['css']['Report']['SLP']){
	$TabMenuArr[] = array("css_slp_config.php", 'Student Development Progress Report', 0);
}
$TabMenuArr[] = array("student_transcript_config_edit.php", $ec_iPortfolio['transcript_config'], 0);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">

function checkAllForm (flag){

   if (flag == 0)
   {
      $("input[id^='div_formAllowed_']").attr('checked', false);
   }
   else
   {
      $("input[id^='div_formAllowed_']").attr('checked', true);
   }

	return;
}

function checkform(obj)
{
	var i, j, order_value, display, exist, null_exist;
	var orderArr = new Array(12);

	for(i=1; i<=5; i++)
	{
		order_obj = eval("obj.order"+i);
		display_obj = eval("obj.display"+i);

		if(typeof(order_obj)!="undefined" && typeof(order_obj)!="undefined" && display_obj.checked==true)
		{
			exist = 0;
			null_exist = 0;
			order_value = order_obj.value;
			for(j=1; j<=5; j++)
			{
				if(order_value=="")
				{
					null_exist=1;
					break
				}
				else if(orderArr[j]==order_value)
				{
					exist=1;
					break;
				}
			}
		
			if (null_exist==1)
			{
				alert("<?=$ec_warning['order_required']?>");
				return false;
			}
			else if(exist==1)
			{
				alert("<?=$ec_warning['order_repeat']?>");
				return false;
			}
			else
			{
				orderArr[i] = order_value;
			}
		}	
	}
	
	RecordsAllowedArr = document.getElementsByName("RecordsAllowed[]");
	for(i=0; i<RecordsAllowedArr.length; i++)
	{
		if(RecordsAllowedArr[i].value != "" && isNaN(RecordsAllowedArr[i].value) && Math.floor(RecordsAllowedArr[i].value) == RecordsAllowedArr[i].value && RecordsAllowedArr[i].value <= 0)
		{
			alert("<?=$ec_warning['please_enter_pos_integer']?>");
			RecordsAllowedArr[i].focus();
			return false;
		}
	}

	//start check for SLP report setting
	if($('#div_allowStudentPrintSLP').attr('checked')){
		//ALLOW STUDENT PRINT REPORT IS CHECKED

		//CHECK FORM AT LEAST ONE FORM IS SELECTED
		var formChecked = $("input[name='r_formAllowed[]']:checked").size(); 				
		if (formChecked == 0) { 
			alert("<?=$Lang['iPortfolio']['OEA']['ApplicableFormsEmpty']?>");
			return false; 
		} 


		//CHECK DATE INPUT
		if($('#div_isPrintIssueDate').attr('checked')){
			//DISPLAY ISSUEDATE IN THE REPORT
			if(!check_date(document.form1.issuedate, "<?=$i_invalid_date?>")){
				return false;
			}
		}


		//CHECK LANG
		var lang_checked = $("input[name='r_slpReportLang[]']:checked").size(); 				
		if (lang_checked == 0) { 
			alert("<?=$w_alert['Invalid_Lang']?>");
			return false; 
		} 

	}
	//end for SLP report setting
	return true;
}


function displaySlpForStudentSetting(checkedValue){
	if (checkedValue=="1")
	{		
		$('#div_studentSLPSetting').show();
	} else
	{		
		$('#div_studentSLPSetting').hide();
	}
}
function set_disable(id)
{
	var formObj = document.form1;
	var display, set_value;

	display = eval("formObj.display"+id+".checked");
	set_value = (display==false) ? "true" : "false";
	
	for(i=1;i<=4;i++)
	{
		new_id= "td"+id+"n"+i;
		if(display==false)
		{	
			document.getElementById(new_id).className="row_off";
		}else{

		    document.getElementById(new_id).className="row_on";	
		}
	}
	
}


function swapRowUp(chosenRow) {

	var mainTable = document.getElementById("mainTable"); 
	// Explain chosenRow
	//var test = chosenRow.innerHTML;
	//alert(test);
	//var test = chosenRow.rowIndex;
	//alert(test);
 if (chosenRow.rowIndex != 0 && chosenRow.rowIndex != 1) {
   moveRow(chosenRow, chosenRow.rowIndex-1); 
 }
} 
function swapRowDown(chosenRow) {
	var mainTable = document.getElementById("mainTable"); 
	//var test = mainTable.rows.length;
	//alert(test);
 if (chosenRow.rowIndex != mainTable.rows.length-1) {
   moveRow(chosenRow, chosenRow.rowIndex+1); 
 }
} 
//moves the target row object to the input row index 
function moveRow(targetRow, newIndex) {
//since we are not actually swapping 
//but simulating a swap, have to "skip over" 
//the current index 
//alert(targetRow.cells[0].innerHTML);
 if (newIndex > targetRow.rowIndex) {
   newIndex++; 
 }
//establish proper reference to the table 
 var mainTable = document.getElementById('mainTable'); 
//insert a new row at the new row index 
 var theCopiedRow = mainTable.insertRow(newIndex); 
//copy all the cells from the row to move 
//into the new row 
 for (var i=0; i<targetRow.cells.length; i++) {
		
	 	var oldCell = targetRow.cells[i]; 
   		var newCell = document.createElement("TD"); 
   		
   		if(i==0 || i==1){newCell.align="center";}
   		//newCell.ClassName="row_on";
   		//alert(targetRow.cells[i].innerHTML);
		newCell.innerHTML = oldCell.innerHTML; 
		theCopiedRow.appendChild(newCell); 
		copyChildNodeValues(targetRow.cells[i], newCell);	

 } 
//delete the old row 
 mainTable.deleteRow(targetRow.rowIndex); 
} 

function copyChildNodeValues(sourceNode, targetNode) {
	var mainTable = document.getElementById("mainTable"); 
 for (var i=0; i < sourceNode.childNodes.length; i++) {
   try{
     targetNode.childNodes[i].value = sourceNode.childNodes[i].value;
   }
   catch(e){

   }
 }
}

function setDisplaySDCSettings() {
	if (!$("#details_1").attr("checked")) {
		$("#sdc_thickBox").show();
	} else {
		$("#sdc_thickBox").hide();
	}
}
$(document).ready(function() {
	setDisplaySDCSettings();
});
</script>

<!-- ===================================== Body Contents ============================= -->
<form name="form1" method="post" action="slp_config_update.php" onSubmit="return checkform(this)" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <!-- Tab Start -->
      <?=$lpf->GET_TAB_MENU($TabMenuArr);?>
      <!-- Tab End -->
    </td>
  </tr>
	<tr><td><?=$linterface->GET_SYS_MSG($msg,$failMsg)?></td></tr>
	<tr>
		<td align="center">
    	<!-- Cap Setting Start -->
			<table width="100%" border="0" cellspacing="0" cellpadding="6">
				<tr class="tabletop">
					<td class="tabletopnolink" width="5%"><?=$iPort["display"]?></td>
					<td class="tabletopnolink" width="5%"><?=$iPort["order"]?></td>
					<td class="tabletopnolink" width="60%">&nbsp;</td>
					<td width="30%">&nbsp;</td>
				</tr>
				<?=$studentDetailsConfigHtml?>
				<?/*<tr class="tablerow1">
					<td class="row_on" align="center"><input type="checkbox" name="StudentDetailsConfig" id="details_1" value="1" <?=($StudentDetailsConfig==1?"CHECKED":"")?> /></td>
					<td class="row_on" align="center">--</td>
					<td class="row_on tabletext"><?=$ec_iPortfolio['no_student_details']?></td>
					<td class="row_on tabletext">&nbsp;</td>
				</tr>
				*/?>
	<!--
	<tr class="tabletop">
         	<td class="tabletopnolink" width="5%">&nbsp;</td>
         	<td class="tabletopnolink" align="center" width="5%">&nbsp;</td>
         	<td width="60%">&nbsp;</td>
        	<td width="30%">&nbsp;</td>
    </tr>
    -->
			</table>
   
   <!--	<tr><td colspan="4"> -->
			<table id="mainTable" border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr class="tabletop" height="25">
					<td class="tabletopnolink" width="6%">&nbsp;</td>
					<td class="tabletopnolink" align="center" width="7%">&nbsp;</td>
					<td width="60%">&nbsp;</td>
					<td width="27%">&nbsp;</td>
				</tr>
<?
	for($i=0;$i<=count($report_setting)-1;$i++)
	{
		for($j=0;$j<=count($report_setting)-1;$j++)
		{
			if($i==${"order_select".$j})
			{
				echo $template_row[$j];
			}
		}
	}
?>
			</table> 
			<!-- Cap Setting End -->
			<br />
<?
//start allow student to print SLP report setting//
?>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
	
	<tr class="tabletop">
		<td colspan = "2"><?=$Lang['iPortfolio']['SLP_REPORT']['AllowForStudent']?>&nbsp;</td>
	</tr>

	<tr>
	<td width = "145" valign = "top"><?=$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrint']?></td>
	<td>
			<input type = "checkbox" value="1" name="r_allowStudentPrintSLP" id = "div_allowStudentPrintSLP" onclick="displaySlpForStudentSetting(this.checked)" <?=$h_allowStudentPrintSLPChecked?>><label for="div_allowStudentPrintSLP" ><?=$Lang['iPortfolio']['SLP_REPORT']['AllowForStudentToPrintYesNo']?></label>
			<div id="div_studentSLPSetting" <?=$h_slpSettingStyle?>>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr >
						<td  width="15%" valign="top"><?=$ec_iPortfolio['SLP']['JoinableYear']?>&nbsp;</td>
						<td>
<?php
	echo $h_formChk;
?>&nbsp;
						</td>
					</tr>
				
					<tr id='issueDateRow' >
						<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['date_issue']?></span></td>
						<td>
						<input name="r_issuedate" id="issuedate" type="text" class="tabletext" value="<?=$reportIssueDate?>" />
								<?=$linterface->GET_CALENDAR("form1", "issuedate");?>
<?
$isPrintIssueDateChecked = ($reportIsPrintIssueDate == 1)? ' checked ': '';
?>
								<br/><div id="printIssueDate"> <input type = "checkbox" name="r_isPrintIssueDate" value = "1" id ="div_isPrintIssueDate" <?=$isPrintIssueDateChecked?>><label for="div_isPrintIssueDate" ><?=$Lang['iPortfolio']['print_date_issue']?></label></div>
						</td>
					</tr>
					<tr id='bi_lang'>
						<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['print_slp_lang']?></span></td>
						<td>
							<?
								$_b5Checked = in_array('b5',$reportLangInfo) ?  ' checked ':''  ;
								$_enChecked = in_array('en',$reportLangInfo) ?  ' checked ':''  ;
							?>
							<input type = "checkbox" name="r_slpReportLang[]" value = "b5" id ="slpReportLang_B5" <?=$_b5Checked?>><label for="slpReportLang_B5" ><?=$Lang['iPortfolio']['print_slp_lang_b5']?></label>
							<input type = "checkbox" name="r_slpReportLang[]" value = "en" id ="slpReportLang_En" <?=$_enChecked?>><label for="slpReportLang_En" ><?=$Lang['iPortfolio']['print_slp_lang_en']?></label>
						</td>
					</tr>	
					
					<tr id='academicScoreDisplayModeRow' style=''>
						<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$Lang['iPortfolio']['AcademicResult']?></span></td>
						<td>
							<?=$Lang['iPortfolio']['DisplayOptions']?>:
							<br />
							<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>" <?=$markThenGrade_check?> /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']?>"><?=$Lang['iPortfolio']['MarkThenGrade']?></label>
							<br />
							<input type="radio" id="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>" name="academicScoreDisplayMode" value="<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>" <?=$gradeThenMark_check?> /> <label for="academicScoreDisplayMode_<?=$ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']?>"><?=$Lang['iPortfolio']['GradeThenMark']?></label>
							<br />
							<br />
							<?=$Lang['iPortfolio']['DisplayFullMark']?>:
							<br />
							<input type="radio" id="displayFullMark_Yes" name="displayFullMark" value="1" <?=$DisplayFullMark_YesCheck?> /> <label for="displayFullMark_Yes"><?=$Lang['General']['Yes']?></label>
							<input type="radio" id="displayFullMark_No" name="displayFullMark" value="0" <?=$DisplayFullMark_NoCheck?>/> <label for="displayFullMark_No"><?=$Lang['General']['No']?></label>
							<br />
							<br />
							<?=$Lang['iPortfolio']['DisplayComponentSubject']?>:
							<br />
							<input type="radio" id="displayComponentSubject_Yes" name="displayComponentSubject" value="1" <?=$DisplayComponentSubject_YesCheck?> /> <label for="displayComponentSubject_Yes"><?=$Lang['General']['Yes']?></label>
							<input type="radio" id="displayComponentSubject_No" name="displayComponentSubject" value="0" <?=$DisplayComponentSubject_NoCheck?> /> <label for="displayComponentSubject_No"><?=$Lang['General']['No']?></label>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	
	
	<tr class="tabletop">
		<td colspan = "2"><?=$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImagePrint']?>&nbsp;</td>
	</tr>
	<tr>
	<td width = "145" valign = "top"><?=$Lang['iPortfolio']['SLP_REPORT']['SchoolHeaderImageDetail']?></td>
		<td>	
			<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<?=$SchoolImageRow?>
					<tr id='filePathRow' >
						<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['SchoolHeaderImageUpload']?></span>						
						</td>
						<td>
						<input name="uploadFile" id="uploadFile" type="file" class="tabletext" size="40" /><br/>
						<?=$SuggestImageSizeRow_html?>
						</td>
					</tr>
					

					
					<tr id='IsShowImageRow' >
						<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['ShowSchoolHeaderImage']?></span></td>
						<td>
						<input name="IsShowImage" id="IsShowImage" type="checkbox" <?=$ShowSchoolHeaderImage?>/>
						</td>
					</tr>
			</table>
		</td>
	</tr>
</table>
<?
//end allow student to print SLP report setting//
?>
			<table width="95%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<input class="formbutton" type="submit" value="<?=$button_submit?>" name="btn_submit" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
						<input name="Reset" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<div id="OLESettings" style="display:none">
  <div class="edit_pop_board edit_pop_board_simple">
    <h1><span><?=$ec_iPortfolio['ole']?></span></h1>
    <div>
      <table class="form_table inside_form_table">
    		<tr>
    			<td class="tabletext"><?=$i_ClassLevel?></td>
    			<td align="center" class="tabletext"><?=$ec_iPortfolio['number_of_record']?></td>
    			<td align="center" class="tabletext"><?=$i_general_Teacher?></td>
    			<td align="center" class="tabletext"><?=$i_identity_student?></td>
    		</tr>
        <?=$OLESettingStr?>
      </table>
    </div>
    <div class="edit_bottom">
      <input name="NoPrintToLimit" id="NoPrintToLimit" type="checkbox" value="1" <?=$NoPrintToLimit?> /> <label for="NoPrintToLimit"><?=$ec_iPortfolio['NoPrintToLimit']?></label>
      <p class="spacer"></p>
      <input type="button" class="formbutton" onclick="tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_confirm?>" />
      <p class="spacer"></p>
    </div>
  </div>
</div>

<div id="ExtSettings" style="display:none">
  <div class="edit_pop_board edit_pop_board_simple">
    <h1><span><?=$iPort["external_ole_report"]["performance"]?></span></h1>
    <div>
      <table class="form_table inside_form_table">
    		<tr>
    			<td class="tabletext"><?=$i_ClassLevel?></td>
    			<td align="center" class="tabletext"><?=$ec_iPortfolio['number_of_record']?></td>
    			<td align="center" class="tabletext"><?=$i_general_Teacher?></td>
    			<td align="center" class="tabletext"><?=$i_identity_student?></td>
    		</tr>
        <?=$ExtSettingStr?>
      </table>
    </div>
    <div class="edit_bottom">
      <input name="ExtNoPrintToLimit" id="ExtNoPrintToLimit" type="checkbox" value="1" <?=$ExtNoPrintToLimit?> /> <label for="ExtNoPrintToLimit"><?=$ec_iPortfolio['NoPrintToLimit']?></label>
      <p class="spacer"></p>
      <input type="button" class="formbutton" onclick="tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_confirm?>" />
      <p class="spacer"></p>
    </div>
  </div>
</div>





<div id="StudentDetailsSettings" style="display:none">
  <div class="edit_pop_board edit_pop_board_simple">
    <h1><span>Student Details Configuration</span></h1>
    <div>
      <table class="form_table inside_form_table">
        <?=$StudentDetailsConfigSettingStr?>
      </table>
    </div>
    <div class="edit_bottom">
      <input type="button" class="formbutton" onclick="tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_confirm?>" />
      <p class="spacer"></p>
    </div>
  </div>
</div>

</form>
<!-- ===================================== Body Contents (END) ============================= -->

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

function _getFormAllowCheckBoxUI($reportFormAllowInfo = array()){
	$FormPerRow = 3;
	$libfcm = new form_class_manage();
	$FormInfoArr = $libfcm->Get_Form_List($GetMappedClassInfo=false, $ActiveOnly=1);

	$h_formChk = '';
			$h_formChk .= '<table border="0" cellpadding="2" cellspacing="2">'."\n";
	for ($i = 0,$i_max = count($FormInfoArr); $i < $i_max; $i++){
				$_YearID =  $FormInfoArr[$i]['YearID'];
		$_YearName =  $FormInfoArr[$i]['YearName'];
		$_checked = in_array($_YearID,$reportFormAllowInfo) ?  ' checked ' : '' ;
		$_divID = 'div_formAllowed_'.$_YearID;

		if ($i % $FormPerRow == 0) {
			$h_formChk .= '<tr>'."\n";
		}

		$h_formChk .= '<td >&nbsp;</td>'."\n";
		$h_formChk .= '<td >'."\n";
		$h_formChk .= '<input type ="checkbox" name="r_formAllowed[]" value="'.$_YearID.'" '.$_checked.' id='.$_divID.'><label for ="'.$_divID.'">'.$_YearName.'</label><br/>'."\n";
		$h_formChk .= '</td>'."\n";
						
		if ( (($i+1) % $FormPerRow == 0) || ($i==$i_max-1)) {
			$h_formChk .= '</tr>'."\n";
		}



	}
	$h_formChk .= '</table>';
	$h_formChk = '<input type = "checkbox" name="checkFormAll" value ="1" id="div_checkFormAll" onclick="checkAllForm(this.checked)"><label for ="div_checkFormAll">Check All</label><br/>'.$h_formChk;
	return $h_formChk;
}


?>
