<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$linterface = new interface_html("popup.html");

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$sql = "SELECT DISTINCT y.YearName, yc.YearClassID, yc.ClassTitleEN ";
$sql .= "FROM {$eclass_db}.PORTFOLIO_STUDENT ps ";
$sql .= "LEFT JOIN {$lpf->course_db}.user_group ug ";
$sql .= "ON ps.CourseUserID = ug.user_id AND ug.group_id = {$group_id} ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ";
$sql .= "ON ps.UserID = iu.UserID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ";
$sql .= "ON iu.UserID = ycu.UserID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ";
$sql .= "ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
$sql .= "INNER JOIN {$intranet_db}.YEAR y ";
$sql .= "ON yc.YearID = y.YearID ";
$sql .= "WHERE ug.user_id IS NULL ";
$sql .= "ORDER BY y.Sequence, yc.Sequence";
$class_arr = $lpf->returnArray($sql);

$curYearName = "";
$class_selection = "";
$html_class_selection = "<select name=\"YearClassID\" >\n";
$html_class_selection .= "<option value=\"\"></option>\n";
for($i=0, $i_max=count($class_arr); $i<$i_max; $i++)
{
  $_y_name = $class_arr[$i]["YearName"];
  $_yc_id = $class_arr[$i]["YearClassID"];
  $_yc_title = $class_arr[$i]["ClassTitleEN"];
  
  if($curYearName != $_y_name)
  {
    $html_class_selection .= "<optgroup label=\"$_y_name\">\n";
    $curYearName = $_y_name;
  }
  
  $selected = ($_yc_id == $YearClassID) ? "SELECTED" : "";
  $html_class_selection .= "<option value=\"{$_yc_id}\" {$selected}>{$_yc_title}</option>\n";
  
  if(isset($class_arr[$i+1]) && $curYearName != $class_arr[$i+1]["YearName"])
  {
    $html_class_selection .= "</optgroup>\n";
  }
}
$html_class_selection .= "</select>";

if($YearClassID != ""){
  $sql = "SELECT ps.CourseUserID, iu.ChineseName ";
  $sql .= "FROM {$eclass_db}.PORTFOLIO_STUDENT ps ";
  $sql .= "LEFT JOIN {$lpf->course_db}.user_group ug ";
  $sql .= "ON ps.CourseUserID = ug.user_id AND ug.group_id = {$group_id} ";
  $sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ";
  $sql .= "ON ps.UserID = iu.UserID ";
  $sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ";
  $sql .= "ON iu.UserID = ycu.UserID ";
  $sql .= "WHERE ug.user_id IS NULL AND ycu.YearClassID = {$YearClassID} ";
  $sql .= "ORDER BY ycu.ClassNumber";
  $student_arr = $lpf->returnArray($sql);

  $html_student_selection = "<select size=\"15\" name=\"targetID[]\" multiple >\n";
  for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
  {
    $_uid = $student_arr[$i]["CourseUserID"];
    $_s_name = $student_arr[$i]["ChineseName"];
        
    $html_student_selection .= "<option value=\"{$_uid}\">{$_s_name}</option>\n";
  }
  $html_student_selection .= "</select>";
}

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="JavaScript">

$(document).ready(function() {
  $("select[name=YearClassID]").change(function(){
    document.form1.submit();
  });
  
  $("input[name=add_btn]").click(function(){
    var cnt_checked = $("select[name^=targetID]").find("option:selected").length;

    if(cnt_checked > 0)
    {
      $.post(
        "group_student_assign_update.php",
        $("form[name=form1]").serialize(),
        function(data){
          window.opener.location.reload();
          self.close();
        }
      );
    }  
  });
  
  $("input[name=sel_all_btn]").click(function(){
    $("select[name^=targetID]").find("option").each(function(){
      $(this).attr("selected", "selected");
    });
  
  });

});

</script>

<form name="form1" METHOD="POST">

  <table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
    <tr>
      <td align="center">
      
        <table width="95%" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td>
              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td valign="top" nowrap="nowrap"><span class="tabletext"><?= $iDiscipline['class']?>:</span></td>
                  <td width="80%"><?=$html_class_selection?></td>
                </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                </tr>		
<?php if($YearClassID != "") { ?>
                <tr>
                  <td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
                </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                </tr>
                <tr>
                  <td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $iDiscipline['students']?>: </span></td>
                  <td width="80%" style="align: left">
                    <table border="0" cellpadding="0" cellspacing="0" align="left">
                      <tr> 
                        <td>
                        <?=$html_student_selection ?>
                        </td>
                        <td style="vertical-align:bottom">        
                          <table width="100%" border="0" cellspacing="0" cellpadding="6">
                            <tr> 
                              <td align="left"> 
                                <?= $linterface->GET_BTN($button_add, "button", "", "add_btn")?>
                              </td>
                            </tr>
                            <tr> 
                              <td align="left"> 
                                <?= $linterface->GET_BTN($button_select_all, "button", "", "sel_all_btn")?>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
<?php } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
          <tr>
            <td class="dotline">
              <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
            </td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <input type="hidden" name="group_id" value="<?=$group_id?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>