<?php
# editing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_TeacherComment";
$CurrentPageName = $Lang['iPortfolio']['TeacherComment']." ".$iPort['menu']['settings'];

$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Group List
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getTcSettingsTags($currentPageIndex);




$pageSizeChangeEnabled = true;

# TABLE SQL
$keyword = trim($keyword);
if($field == "") $field = 0;

$name_field = getNameFieldByLang("b.");
$conds = "WHERE (g.group_name like '%".str_replace(Array("%","_"),Array("\\%","\\_"),htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%')";

$sql =  "
          SELECT DISTINCT
            g.group_id,
            g.group_name,
            GROUP_CONCAT(um_t.firstname SEPARATOR ', '),
            GROUP_CONCAT(um_s.firstname SEPARATOR ', '),
            CONCAT('<a class=\"tablelink\" href=\"group_user_list.php?group_id=', g.group_id, '\">', count(DISTINCT ug.user_id), '</a>'),
            g.inputdate
          FROM
            grouping g
          INNER JOIN mgt_grouping_function mgf
            ON g.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:TeacherComment') > 0
          LEFT JOIN user_group ug
            ON g.group_id = ug.group_id
          LEFT JOIN usermaster um_t
            ON ug.user_id = um_t.user_id AND um_t.memberType = 'T'
          LEFT JOIN usermaster um_s
            ON ug.user_id = um_s.user_id AND um_s.memberType = 'S'
          {$conds}
          GROUP BY
            ug.group_id
        ";

// echo htmlspecialchars($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("group_id", "group_name", "count(DISTINCT ug.user_id)", "inputdate");
$li->db = $lpf->course_db;
$li->sql = $sql;
$li->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th width='5%' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(0, $Lang['iPortfolio']['GroupID'])."</th>\n";
$li->column_list .= "<th width='25%' class='tabletop' style='vertical-align:middle'>".$li->column(1, $Lang['iPortfolio']['GroupName'])."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop tabletopnolink' style='vertical-align:middle'>{$Lang['iPortfolio']['TeacherTutor']}</th>\n";
$li->column_list .= "<th width='20%' class='tabletop tabletopnolink' style='vertical-align:middle'>{$iPort['usertype_s']}</th>\n";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$li->column(2, $i_eClassNumUsers)."</th>\n";
$li->column_list .= "<th width='15%' class='tabletop' style='vertical-align:middle'>".$li->column(3, $i_eClassInputdate)."</th>\n";

// TABLE FUNCTION BAR

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$searchTag  	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag 	.= "</tr></table>";

$html_instruction = $linterface->Get_Warning_Message_Box($i_Discipline_System_Conduct_Instruction, $Lang['iPortfolio']['GroupList_instruction']);



### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get" action="group_list.php">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td>
    </tr>
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                </tr>
                <tr>
                  <td><?=$html_instruction?></td>
                </tr>
                <tr><td align="right"><?=$searchTag?></td></tr>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="pageNo" value="<?=$li->pageNo ?>" />
  <input type="hidden" name="order" value="<?=$li->order ?>">
  <input type="hidden" name="field" value="<?=$li->field ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change ?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size ?>" />

</form>



<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>