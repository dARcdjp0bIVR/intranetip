<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_TeacherComment";
$CurrentPageName = $Lang['iPortfolio']['TeacherComment']." ".$iPort['menu']['settings'];

//$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Group List
$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getTcSettingsTags($currentPageIndex);


$student_ole_config_file = "$eclass_root/files/teacher_comment_config.txt";
$filecontent = trim(get_file_content($student_ole_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<script language="JavaScript">

function checkform(obj){

	if (typeof(obj.starttime)!="undefined")
	{
		if (obj.starttime.value!="")
		{
			if(!check_date(obj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			isst = true;
		}
	}
	if (typeof(obj.endtime)!="undefined")
	{
		if (obj.endtime.value!="")
		{
			if(!check_date(obj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(obj.starttime, obj.sh, obj.sm, obj.endtime, obj.eh, obj.em)) {
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
				if(obj.starttime.value==obj.endtime.value && obj.sh.value==obj.eh.value && obj.sm.value==obj.em.value){
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}

  return true;
}

</script>

<form name="form1" method="POST" action="config_update.php" onSubmit="return checkform(this)">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td>
  </tr>
  <tr>
    <td align="center">
      <table width="90%" cellspacing="0" cellpadding="5" border="0">
        <tr>
          <td>
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">
                  <span class="tabletext"><?=$iPort["options_select"]?></span>
                </td>
                <td width="80%" valign="top">
                  <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                      <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">
                        <span class="tabletext"><?=$iPort["start_time"]?></span>
                      </td>
                      <td valign="top">
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td><input name="starttime" type="text" class="tabletext" value="<?=$starttime?>" /></td>
                            <td width="25" align="center"><?=$linterface->GET_CALENDAR("form1", "starttime")?></td>
                            <td align="center">
                              <?=returnHour('sh', $sh, 0)?> : <?=returnMinute('sm', $sm, 0)?>
                            </td>
                            <td width="25" align="center"></td>
                          </tr>
                        </table>
                        <?=$iPort["msg"]["start_time_teacher_comment"]?>
                      </td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;">
                        <span class="tabletext"><?=$iPort["end_time"]?></span>
                      </td>
                      <td valign="top">
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td><input name="endtime" type="text" class="tabletext" value="<?=$endtime?>" /></td>
                            <td width="25" align="center"><?=$linterface->GET_CALENDAR("form1", "endtime")?></td>
                            <td align="center">
                              <?=returnHour('eh', $eh, 0)?> : <?=returnMinute('em', $em, 0)?>
                            </td>
                            <td width="25" align="center"></td>
                          </tr>
                        </table>
                        <?=$iPort["msg"]["end_time_teacher_comment"]?>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
        <tr>
          <td align="right">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">
                  <?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "submit", "", "btnSubmit")?>
                  &nbsp;
                  <?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "reset", "", "btnReset")?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>

</table>

</td></tr></table>
</td></tr></table>

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>