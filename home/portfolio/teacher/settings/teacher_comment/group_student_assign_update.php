<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
intranet_auth();
intranet_opendb();

$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

if(isset($group_id) && isset($targetID))
{
  for($i=0, $i_max=count($targetID); $i<$i_max; $i++)
  {
    $_uid = $targetID[$i];
  
    $field_val[] = "({$group_id}, {$_uid})";
  }
}

if(count($field_val) > 0)
{
  $field_val_str = implode(", ", $field_val);

  $sql = "INSERT IGNORE INTO {$lpf->course_db}.user_group (group_id, user_id) VALUES ".$field_val_str;
  $lpf->db_db_query($sql);
}

intranet_closedb();
?>