<?php

$gfunction = !isset($gfunction) ? array() : $gfunction;

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupFunctionRight($gfunction);
$lpf_mgmt_group->UPDATE_GROUP_MGMT_RIGHT();

$lpf_mgmt_group->UPDATE_GROUP_COMPONENT($ELE_ID, $UserID);

$msg = "update";

header("Location: index.php?group_type={$group_type}&task=editGroupRight&group_id={$group_id}&msg={$msg}");

?>