<?php
## Modifying By
##
## Date : 2018-01-17 Omas modified sql to exclude suspended teachers


$course_db = $lpf->course_db;
$t_namefield = getNameFieldByLang("iu.");
$s_namefield = getNameFieldWithClassNumberByLang("iu.");


$group_type = intval($group_type);

$SelectedMemberListSelection = "<select name=\"uID[]\" id=\"SelectedUser\" size=\"10\" style=\"width:100%\" multiple>";
$SelectedMemberListSelection .= "</select>";

$sub_sql = "SELECT ";
$sub_sql .= "um.user_id ";
$sub_sql .= "FROM {$course_db}.user_group ug ";
$sub_sql .= "INNER JOIN {$course_db}.usermaster um ON ug.user_id = um.user_id ";
$sub_sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserEmail = um.user_email ";
$sub_sql .= "WHERE ug.group_id = {$GroupID} AND (um.status IS NULL OR um.status <> 'deleted')";

$sql = "SELECT ";
$sql .= "uc.user_id, {$t_namefield} AS DisplayName ";
$sql .= "FROM {$eclass_db}.user_course uc ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserEmail = uc.user_email ";
$sql .= "WHERE uc.course_id = {$ck_course_id} AND iu.RecordType = 1 ";
$sql .= "AND uc.user_id NOT IN ({$sub_sql}) AND iu.RecordStatus = 1";
//$sql .= " order by displayName";
$sql .= " ORDER BY iu.EnglishName";
$teacher_arr = $lpf->returnArray($sql);
//debug_r($sql);


if($group_type == $ipf_cfg['GroupSetting']['isStudentTypeGroup']){
	$sql = "SELECT ";
	$sql .= "uc.user_id, {$s_namefield} AS DisplayName ";
	$sql .= "FROM {$eclass_db}.user_course uc ";
	$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserEmail = uc.user_email ";
	$sql .= "WHERE uc.course_id = {$ck_course_id} AND iu.RecordType = 2 ";
	$sql .= "AND uc.user_id NOT IN ({$sub_sql}) ";
	$sql .= "ORDER BY iu.ClassName, iu.ClassNumber,iu.EnglishName";
	//$sql .= "ORDER BY iu.EnglishName";
	$student_arr = $lpf->returnArray($sql);
	//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
}


//debug_r($sql);
//hdebug_r($sql);
$MemberListSelection = "<select id=\"User2Select\" size=\"10\" style=\"width:100%\" multiple>";
$MemberListSelection .= "<optgroup label=\"{$Lang['Identity']['TeachingStaff']}\">";
for($i=0, $i_max=count($teacher_arr); $i<$i_max; $i++)
{
	$t_uid = $teacher_arr[$i]["user_id"];
	$t_uname = $teacher_arr[$i]["DisplayName"];

	$MemberListSelection .= "<option value=\"{$t_uid}\">{$t_uname}</option>";
}
$MemberListSelection .= "</optgroup>";

//ALLOW STUDENT IS SELECTED ONLY WHEN GROUP TYPE IS A STUDENT TYPE
if($group_type == $ipf_cfg['GroupSetting']['isStudentTypeGroup']){
	$MemberListSelection .= "<optgroup label=\"{$Lang['Identity']['Student']}\">";
	for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
	{

		$t_uid = $student_arr[$i]["user_id"];
		$t_uname = $student_arr[$i]["DisplayName"];

		$MemberListSelection .= "<option value=\"{$t_uid}\">{$t_uname}</option>";

	}
	$MemberListSelection .= "</optgroup>";
}
$MemberListSelection .= "</select>";

$WarningMsg = $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $Lang['iPortfolio']['onlyPortfolioUser']);

include_once("template/newMember.tmpl.php");
?>