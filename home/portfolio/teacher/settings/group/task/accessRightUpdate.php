<?php
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
switch (strtoupper($intranet_session_language)){
	case 'B5' : $lang30 = 'chib5'; break;
	case 'GB' : $lang30 = 'chigb'; break;
	case 'EN' : $lang30 = 'eng'; break;
	default : $lang30 = 'eng'; 
}
include_once("$eclass_filepath/system/settings/lang/$lang30.php");
include_once("accessRightFunction.php");

# include eClass's language file
$ck_room_type = "iPORTFOLIO";

eclass_opendb();

# eClass
$course_id = getEClassRoomID($iPFRoomType = 4);
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

// maybe set by checking what rights are allowed to be set
$right_ids = "1,2";

$lg = new libgroups($eclass_prefix."c$course_id");

$access_config = getFormValue($portfolio_functions);

$portfolio_setting_file = "$eclass_filepath/files/portfolio_right.txt";

$li_fm = new fileManager($eclass_db, 0, 0);

$li_fm->writeFile(serialize($access_config), $portfolio_setting_file);

eclass_closedb();

header("Location: index.php?group_type=3&task=accessRight&msg=update");
?>