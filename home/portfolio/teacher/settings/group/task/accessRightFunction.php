<?php
/*
 * 	2018-01-25 Cameron
 * 		- add reading_records in $portfolio_functions
 */
 
# generate pull-down select
function getSelectField($my_category, $my_function, $my_config){

	global $usertype_ct, $usertype_st, $ec_iPortfolio, $iportfolio_version;

	//Pull-down 2, for T
	if($my_function=="student_info")
	{
    if($iportfolio_version == "" || $iportfolio_version == "2.0")
      $select_options = array($ec_iPortfolio['right_any_teacher']);
    else
      $select_options = array($ec_iPortfolio['right_any_teacher'],$usertype_ct,($usertype_ct." / ".$usertype_st));
	}
	else
	{
	 $select_options = ($my_function!="ole") ? array($ec_iPortfolio['right_no'],$ec_iPortfolio['right_any_teacher'],$usertype_ct,($usertype_ct." / ".$usertype_st)) : array($ec_iPortfolio['right_no'],$ec_iPortfolio['right_any_teacher'],$usertype_ct);
	}

	//Get corresponding config value and put in $cur_value
	$cur_value = $my_config[$my_category][$my_function];

	if ($my_category=="T")
	{
		$rx = "<select name=\"{$my_category}_{$my_function}\" >\n";
		// Generate teacher column
		for ($i=0;$i<sizeof($select_options);$i++)
		{
	 				  if($iportfolio_version == "" || $iportfolio_version == "2.0")
		    $j = $i;
		  else
		    $j = ($my_function=="student_info") ? $i+1 : $i;
		
	 		//Select pull down value
			if ($cur_value == $j)
	        {
	        	$rx = $rx."<option value=\"$j\" selected>$select_options[$i]</option>\n";
	        } else
	        {
				$rx = $rx."<option value=\"$j\" >$select_options[$i]</option>\n";
			}
	    }
		$rx = $rx."</select>\n";
	} else
	{
		if($my_function=="student_info")
		{
			$rx .= "<input type='checkbox' CHECKED DISABLED/>\n";
			$rx .= "<input type='hidden' name=\"{$my_category}_{$my_function}\" value=1/>\n";
		}
		else
		{
			// Generate other columns
			$checked = ($cur_value==1) ? "checked" : "false";
			$rx .= "<input type='checkbox' name=\"{$my_category}_{$my_function}\" $checked />\n";
		}
	}

	return $rx;
}

function getAccessConfigForm($my_functions, $my_config) {

	//Language variables
	global $ec_iPortfolio, $usertype_admin, $usertype_t, $usertype_s, $usertype_p;
	global $portfolio_config;
	global $course_id, $iportfolio_version;
	
	# Growth Scheme wording
	$ec_iPortfolio["growth_scheme"] = iportfolio_getSBSTitle($course_id);
	
	# Skip some functions for parent & student in ipf 25 or above
	if($iportfolio_version != "" && $iportfolio_version != "2.0")
	{
	    $skip_functions['S'][] = "manage_student_learning_sharing";
	    $skip_functions['S'][] = "print_report";
	    
	    $skip_functions['P'][] = "manage_student_learning_sharing";
	    $skip_functions['P'][] =  "print_report";
	}
		
	//$portfolio_config['school_based_scheme']
	$rx .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	// table header
	$rx .= "<tr class='tabletop'><td>&nbsp;</td>\n";
	$rx .= "<td class='num_check' align='center'>".$usertype_t."</td>\n";
	$rx .= "<td align='center'>".$usertype_s."</td>\n";
	$rx .= "<td align='center'>".$usertype_p."</td></tr>\n";
	
	for ($i=0; $i<sizeof($my_functions); $i++)
	{
		if(($my_functions[$i]=="learning_sharing" || $my_functions[$i]=="manage_student_learning_sharing") && $portfolio_config['learning_portfolio']!=1)
		{
			continue;
		}
		else if($my_functions[$i]=="growth_scheme" && $portfolio_config['school_based_scheme']!=1)
		{
			continue;
		}
		
		$_displayText = $my_functions[$i]=='ole'?'ole_record':$my_functions[$i];
		
		// each row setting
		$row_color = ($i%2==1) ? "#F3F3F3" : "#FFFFFF";
		$rx .= "<tr bgcolor='$row_color' class='tabletext'><td align='center'>".$ec_iPortfolio[$_displayText]."</td>\n";
		$rx .= "<td align='center'>".getSelectField('T', $my_functions[$i], $my_config)."</td>\n";
		$rx .= (!is_array($skip_functions['S']) || !in_array($my_functions[$i], $skip_functions['S'])) ? "<td align='center' class='$row_css' >".getSelectField('S', $my_functions[$i], $my_config)."</td>\n" : "<td class='$row_css'>&nbsp;</td>\n";
		$rx .= (!is_array($skip_functions['P']) || !in_array($my_functions[$i], $skip_functions['P'])) ? "<td align='center' class='$row_css' >".getSelectField('P', $my_functions[$i], $my_config)."</td>\n" : "<td class='$row_css'>&nbsp;</td>\n";
		$rx .= "</tr>\n";
	}
	$rx .= "</table>\n";

	return $rx;
}


# get pull-down value of the form and update the records
function getFieldValue($my_category,$my_functions) {

	global ${$my_category.'_'.$my_functions};
	$post_value = ${$my_category.'_'.$my_functions};

	if ($post_value=="2" || $post_value=="3")
	{
		$rx = $post_value;
	} else
	{
		$rx = ($post_value) ? "1" : "0";
	}

	return $rx;
}

function getFormValue($my_functions) {

	# get all values from the form

	for ($i=0; $i<sizeof($my_functions); $i++)
	{
		$access_config['admin'][$my_functions[$i]] = getFieldValue('admin', $my_functions[$i]);
		$access_config['T'][$my_functions[$i]] = getFieldValue('T', $my_functions[$i]);
		$access_config['S'][$my_functions[$i]] = getFieldValue('S', $my_functions[$i]);
		$access_config['P'][$my_functions[$i]] = getFieldValue('P', $my_functions[$i]);
	}

	return $access_config;
}

function IsCheck($field_name, $field_value, $function_list){
	if(strstr($function_list,$field_value)){
		$x = "<input type=checkbox name=".$field_name." value=\"".$field_value."\" checked>";
	} else {
		$x =  "<input type=checkbox name=".$field_name." value=\"".$field_value."\">";
	}
	return $x;
}	

function IsAllCheck($id){
	$x = "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
	return $x;
}

// function list
$portfolio_functions = array(
							'student_info',
							'merit',
							'assessment_report',
							'activity',
							'award',
							'reading_records',
							'teacher_comment',
							'attendance',
							'service',
							'ole',
							'print_report',
							'learning_sharing',
							'growth_scheme',
							'manage_student_learning_sharing'
						);
$portfolio_setting_file = "$eclass_filepath/files/portfolio_right.txt";

$student_profile_functions = array(
							array('Profile:Subject', $ec_iPortfolio['management_subject']),
							array('Profile:Student', $ec_iPortfolio['management_student']),
							array('Profile:ImportData', $ec_iPortfolio['data_import']),
							array('Profile:ExportData', $ec_iPortfolio['export_slp_data']),
							array('Profile:OLR', $ec_iPortfolio['student_ole_manage']),
							array('Profile:ReportSetting', $ec_iPortfolio['student_report_setting']),
							array('Profile:ReportPrint', $ec_iPortfolio['student_report_printing']),
							array('Profile:Stat', $ec_iPortfolio['assessment_stat_report']),
							array('Profile:SelfAccount', $ec_iPortfolio['self_account'])
						);
// Customized for Chung Wing Kwong
if($sys_custom['cwk_SLP']) $student_profile_functions[] = array('Profile:TeacherComment', $ec_iPortfolio['teacher_comment']);

$sharing_functions = array(
							array('Sharing:Content', $ec_iPortfolio['learning_portfolio_content']),
							array('Sharing:Template', $ec_iPortfolio['manage_templates']),
							array('Sharing:PeerReview', $ec_iPortfolio['peer_review_setting']),
							array('Sharing:CDBurning', $ec_iPortfolio['prepare_cd_burning'])
						);


?>
