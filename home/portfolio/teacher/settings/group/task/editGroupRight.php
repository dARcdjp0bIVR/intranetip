<?php

$portfolio_config_file = "$eclass_filepath/system/settings/portfolio.php";
if(file_exists($portfolio_config_file))
{
	include_once($portfolio_config_file);
}

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();
$lpf_mgmt_group->setGroupFunctionRight();
$GroupName = $lpf_mgmt_group->getGroupName();

// assign the group function right to a temp array for remove end of ","
$gf_arrTemp = $lpf_mgmt_group->getGroupFunctionRight(true);

$gf_arr = array();
for($i = 0, $i_max = sizeof($gf_arrTemp);$i< $i_max; $i++)
{
	//for safe, if the element of "$gf_arr" end with "," , there will error in the program. 
	//So, remove the end of "," for every element of $gf_arrTemp and assign to $gf_arr
//	$gf_arr[] = str_replace(",", "", $gf_arrTemp[$i]);
	$gf_arr[] = preg_replace("/,$/", "", $gf_arrTemp[$i]);
}

// Section for school records
$html_function_table = "<div class=\"table_board\">";
$html_function_table .= "<div class=\"form_sub_title_v30\"><em>- <span class=\"field_title\"> {$ec_iPortfolio['school_profile']} </span>-</em>";
$html_function_table .= "<p class=\"spacer\"></p>";
$html_function_table .= "</div>";
$html_function_table .= "<table class=\"form_table_v30\">";
for($i=0, $i_max=count($student_profile_functions); $i<$i_max; $i++)
{
	$_function_code = $student_profile_functions[$i][0];
	$_function_name = $student_profile_functions[$i][1];
	
	$_checked = in_array($_function_code, $gf_arr) ? "checked" : "";
	
	// OLE Component selection
	$html_ole_component_selection = "";
	if($_function_code == "Profile:OLR")
	{
		$ele_arr = $lpf->GET_ELE();
		$ele_mgmt_arr = $lpf_mgmt_group->getGroupComponent($group_id);

		$_display_ele_table = in_array($_function_code, $gf_arr) ? "" : "display:none";
		$_all_ele_checked = count($ele_mgmt_arr) > 0 ? "" : "checked";
		$_restrict_ele_checked = count($ele_mgmt_arr) > 0 ? "checked" : "";
		$_ele_restrict_disabled = count($ele_mgmt_arr) > 0 ? "" : "disabled";
	
		$html_ole_component_selection = "<table width=\"80%\" id=\"ele_table\" style=\"{$_display_ele_table}\">";
		$html_ole_component_selection .= "<col width=\"30%\" /><col width=\"50%\" />";
		$html_ole_component_selection .= "<tr><td><input type=\"radio\" name=\"ele_mgmt\" id=\"ele_mgmt_1\" value=\"1\" {$_all_ele_checked} /><label for=\"ele_mgmt_1\">{$Lang['iPortfolio']['allRecord']}</label></td><td>&nbsp;</td></tr>";
		$html_ole_component_selection .= "<tr><td><input type=\"radio\" name=\"ele_mgmt\" id=\"ele_mgmt_0\" value=\"0\" {$_restrict_ele_checked} /><label for=\"ele_mgmt_0\">{$Lang['iPortfolio']['restrictToComponent']}</label></td><td>";
		$html_ole_component_selection .= "<div>";
		foreach($ele_arr AS $ele_code => $ele_name)
		{
			$_ele_checked = in_array($ele_code, $ele_mgmt_arr) ? "checked" : "";
		
			$html_ole_component_selection .= "<input type=\"checkbox\" name=\"ELE_ID[]\" id=\"ele_{$ele_code}\" value=\"{$ele_code}\" {$_ele_checked} {$_ele_restrict_disabled} /> <label for=\"ele_{$ele_code}\">{$ele_name}</label><br />";
		}
		$html_ole_component_selection .= "</div>";
		$html_ole_component_selection .= "</td></tr>";
		$html_ole_component_selection .= "</table>";
	}
	
	$html_function_table .= "<tr valign='top'>";
	$html_function_table .= "<td class=\"field_title\">{$_function_name}</td>";
	$html_function_table .= "<td>";
	$html_function_table .= "<input type=\"checkbox\" name=\"gfunction[]\" value=\"{$_function_code}\" {$_checked} />";
	$html_function_table .= $html_ole_component_selection;
	$html_function_table .= "</td>";
	$html_function_table .= "</tr>";
}
$html_function_table .= "</table>";

// Section for learning portfolio
if($portfolio_config['learning_portfolio']==1)
{
	$html_function_table .= "<div class=\"form_sub_title_v30\"><em>- <span class=\"field_title\"> {$ec_iPortfolio['learning_sharing']} </span>-</em>";
	$html_function_table .= "<p class=\"spacer\"></p>";
	$html_function_table .= "</div>";
	$html_function_table .= "<table class=\"form_table_v30\">";
	for($i=0, $i_max=count($sharing_functions); $i<$i_max; $i++)
	{
		$_function_code = $sharing_functions[$i][0];
		$_function_name = $sharing_functions[$i][1];
		
		$_checked = in_array($_function_code, $gf_arr) ? "checked" : "";
	
		$html_function_table .= "<tr valign='top'>";
		$html_function_table .= "<td class=\"field_title\">{$_function_name}</td>";
		$html_function_table .= "<td><input type=\"checkbox\" name=\"gfunction[]\" value=\"{$_function_code}\" {$_checked} /></td>";
		$html_function_table .= "</tr>";
	}
	$html_function_table .= "</table>";
}

// Section for SBS
if($portfolio_config['school_based_scheme']==1)
{
	$_checked = in_array("Growth", $gf_arr) ? "checked" : "";

	$html_function_table .= "<div class=\"form_sub_title_v30\"><em>- <span class=\"field_title\"> {$iPort['menu']['school_based_scheme']} </span>-</em>";
	$html_function_table .= "<p class=\"spacer\"></p>";
	$html_function_table .= "</div>";
	$html_function_table .= "<table class=\"form_table_v30\">";
	$html_function_table .= "<tr>\n";
	$html_function_table .= "<td class=\"field_title\">{$iPort['menu']['school_based_scheme']}</td>";
	$html_function_table .= "<td><input type=\"checkbox\" name=\"gfunction[]\" value=\"Growth\" {$_checked} /></td>";
	$html_function_table .= "</tr>";
	$html_function_table .= "</table>";
}

// Section for alumni
$_checked = in_array("Alumni", $gf_arr) ? "checked" : "";
$html_function_table .= "<div class=\"form_sub_title_v30\"><em>- <span class=\"field_title\"> {$ec_iPortfolio['view_alumni']} </span>-</em>";
$html_function_table .= "<p class=\"spacer\"></p>";
$html_function_table .= "</div>";
$html_function_table .= "<table class=\"form_table_v30\">";
$html_function_table .= "<tr>\n";
$html_function_table .= "<td class=\"field_title\">{$ec_iPortfolio['view_alumni']}</td>";
$html_function_table .= "<td><input type=\"checkbox\" name=\"gfunction[]\" value=\"Alumni\" {$_checked} /></td>";
$html_function_table .= "</tr>";
$html_function_table .= "</table>";

// Section for alumni

$html_function_table .= "<div class=\"form_sub_title_v30\"><em>- <span class=\"field_title\"> {$Lang['iPortfolio']['OEA']['JUPAS']} </span>-</em>";
$html_function_table .= "<p class=\"spacer\"></p>";
$html_function_table .= "</div>";
$html_function_table .= "<table class=\"form_table_v30\">";
$html_function_table .= "<tr>\n";
for($i =0,$i_max = count($jupas_functions);$i< $i_max;$i++){
	$_function_code = $jupas_functions[$i][0];
	$_function_name = $jupas_functions[$i][1];

	$_checked = in_array($_function_code, $gf_arr) ? "checked" : "";

	$html_function_table .= "<td class=\"field_title\">".$_function_name."</td>";
	$html_function_table .= "<td><input type=\"checkbox\" name=\"gfunction[]\" value=\"".$_function_code."\" {$_checked} /></td>";
}
$html_function_table .= "</tr>";
$html_function_table .= "</table>";

# tab menu
$TabMenuArr = libpf_tabmenu::getGroupSettingsTags($group_type, 1);
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

# navigation
$MenuArr = array();
$MenuArr[] = array($Lang['iPortfolio']['GroupList'], "index.php?group_type=".$group_type);
$MenuArr[] = array($GroupName, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

$linterface->LAYOUT_START();
include_once("template/updateGroupRight.tmpl.php");
$linterface->LAYOUT_STOP();
?>