<?php
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once("accessRightFunction.php");
switch (strtoupper($intranet_session_language)){
	case 'B5' : $lang30 = 'chib5'; break;
	case 'GB' : $lang30 = 'chigb'; break;
	case 'EN' : $lang30 = 'eng'; break;
	default : $lang30 = 'eng'; 
}
include_once("$eclass_filepath/system/settings/lang/$lang30.php");

$ck_room_type = "iPORTFOLIO";
$portfolio_config_file = "$eclass_filepath/system/settings/portfolio.php";
if(file_exists($portfolio_config_file))
{
	include_once($portfolio_config_file);
}

# eClass
$course_id = getEClassRoomID($iPFRoomType = 4);
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

// maybe set by checking what rights are allowed to be set
$right_ids = "1,2";

// load settings
$portfolio_setting_file = "$eclass_filepath/files/portfolio_right.txt";
$filecontent = trim(get_file_content($portfolio_setting_file));

$access_config = unserialize($filecontent);

$access_right_table = getAccessConfigForm($portfolio_functions, $access_config);

$linterface->LAYOUT_START();
include_once("template/accessRight.tmpl.php");
$linterface->LAYOUT_STOP();
?>