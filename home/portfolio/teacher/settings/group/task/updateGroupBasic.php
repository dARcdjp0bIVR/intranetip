<?php

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupName($group_name);
$lpf_mgmt_group->setGroupDesc($group_desc);
$lpf_mgmt_group->setGroupStatus(1);
$lpf_mgmt_group->setGroupHasRight($has_right);

if(!empty($group_id))
{
  $lpf_mgmt_group->setGroupID($group_id);

  $lpf_mgmt_group->UPDATE_GROUP();
  $msg = "update";
}
else
{
	$lpf_mgmt_group->ADD_GROUP();
	$msg = "add";
}

header("Location: index.php?group_type={$group_type}&task=editGroupBasic&group_id={$lpf_mgmt_group->getGroupID()}&msg={$msg}");

?>