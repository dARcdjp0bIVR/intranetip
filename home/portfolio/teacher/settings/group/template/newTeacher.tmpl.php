<!-- Modifying By  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/jquery-1.3.2.min.js"></script>

<script language="JavaScript">

function checkform(formObj){
	$("#SelectedUser option").attr("selected", true);
	
	return true;
}

function resizeSelect(){
	$("#User2Select").width($("#User2Select").parent().width());
	$("#SelectedUser").width($("#SelectedUser").parent().width());
}

$(document).ready(function(){
	$("#AddAll").click(function(){
		$("#User2Select option").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Add").click(function(){
		$("#User2Select option:selected").remove().appendTo("#SelectedUser");
		resizeSelect();
	});
	
	$("#Remove").click(function(){
		$("#SelectedUser option:selected").remove().appendTo("#User2Select");
		resizeSelect();
	});
	
	$("#RemoveAll").click(function(){
		$("#SelectedUser option").remove().appendTo("#User2Select");
		resizeSelect();
	});
});

</script>

</head>

<body>
<form name="form1" action="index.php" method="post" onSubmit="return checkform(this)">

<div class="edit_pop_board" style="height: 360px;">
	<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height: 290px;" align="center">
		<?php
			#$WarningMsg
		?>
		<table class="form_table">
			<tr><td></td></tr>
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="5" cellspacing="0">
									<col style="width:50%">
									<col style="width:40px">
									<col style="width:50%">
									<tr>
										<td bgcolor="#eeeeee"><?=$Lang['SysMgr']['RoleManagement']['Users']?> </td>
										<td></td>
										<td class="steptitletext" bgcolor="#effee2"><?=$Lang['SysMgr']['RoleManagement']['SelectedUser']?> </td>
									</tr>
									<tr>
										<td align="center" bgcolor="#eeeeee">
											<?=$MemberListSelection?>
											<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
										</td>
										<td>
											<input id="AddAll" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width: 40px;" title="Add All" type="button"><br>
											<input id="Add" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width: 40px;" title="Add Selected" type="button"><br><br>
											<input id="Remove" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width: 40px;" title="Remove Selected" type="button"><br>
											<input id="RemoveAll" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width: 40px;" title="Remove All" type="button">
										</td>
										<td id="SelectedUserCell" bgcolor="#effee2"><?=$SelectedMemberListSelection?></td>
									</tr>
								</table>
								<p class="spacer"></p>
							</td>
						</tr>
					</table>
					<br>
				</td>
			</tr>
		</table>
	</div>
	<div class="edit_bottom">
		<span> </span>
		<p class="spacer"></p>
		<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" type="submit">
		<input name="AddMemberCancelBtn" id="AddMemberCancelBtn" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" type="button">
		<p class="spacer"></p>
	</div>
</div>



<!--
<div class="edit_pop_board edit_pop_board_simple">
  <table class="form_table inside_form_table">
    <col class="field_title" />
    <col  class="field_c" />
    <tr>
      <td><?=$ec_iPortfolio['SLP']['Name']?></td>
      <td>:</td>
      <td><?=$MemberListSelection?></td>
    </tr>
  </table>
  <div class="edit_bottom">
    <p class="spacer"></p>
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
    <input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
    <p class="spacer"></p>
  </div>
</div>
-->
<input type="hidden" name="task" value="updateTeacher" />
<input type="hidden" name="course_id" value="<?=$lpf->course_id?>" />
</form>
</body>
</html>