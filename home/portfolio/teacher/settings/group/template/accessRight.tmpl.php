<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
function index_reload_page(msg){
  this.tb_remove();     //Close thickbox
  window.location.reload();      //Refresh page
}

function save_access_right(){
	$('#task').val('accessRightUpdate');
	$('#form1').submit();
}
</script>



<form name="form1" id="form1" action="index.php" method="post">
<?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $Lang['iPortfolio']['GroupSettingRemark'], "")?>
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom">
    <!-- ###### Tabs Start ######-->
    <?=$TabMenuDisplay?>&nbsp;
    <!-- ###### Tabs End ######-->					
    </td>
  </tr>
	<tr><td><?=$linterface->GET_SYS_MSG($msg); ?></td></tr>
  <tr> 
    <td class="main_content">
      <div class="navigation">
      </div>
      <div class="content_top_tool">
      
        
        <br style="clear:both" />
      </div>
      <div class="table_board">
		<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom"><div class="table_filter"></div></td>
            <td valign="bottom"><div class="common_table_tool">
            </div></td>
          </tr>
        </table>
        <?=$access_right_table?>
        <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td align="center">
        <br/>
        <input onclick="save_access_right()" class="formbutton" type="button" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" value="<?=$Lang['Btn']['Save']?>">
        &nbsp;&nbsp;
        <input onclick="$('#form1')[0].reset()" class="formbutton" type="button" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" value="<?=$Lang['Btn']['Reset']?>">
        </tr></td>
        </table>
      </div>
    </td>
  </tr>
</table>
<!--###### Content Board End ######-->

<input type="hidden" name="order" value=<?=$order?> />
<input type="hidden" name="pageNo" value=<?=$li->pageNo?> />
<input type="hidden" name="field" value=<?=$field?> />
<input type="hidden" name="page_size_change" value=<?=$page_size_change?> />
<input type="hidden" name="numPerPage" value=<?=$li->page_size?> />

<input type="hidden" name="group_type" value="<?=$group_type?>" />
<input type="hidden" name="task" id="task" value="accessRight" />
<input type="hidden" name="status" />
</form>