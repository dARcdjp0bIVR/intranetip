<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
function index_reload_page(msg){
	if(msg=='update'){
		//$("input[name='teacher_id[]']:checked").each(function(){$(this).attr('checked',false);});
		$("#msg").val(msg);
	}
  this.tb_remove();     //Close thickbox
  //window.location.reload();      //Refresh page
  $('#form1').submit();
}

function delete_teacher(obj,element){
	var check_size = $("input[name='"+element+"']:checked").length;
	var selectedTeacher = new Array();

  if(check_size==0)
  {
  		document.getElementById('delete_link').href ='javascript:void(0)';
		alert(globalAlertMsg2);
	}
  else{
    if(confirm(globalAlertMsg3)){
		$("input[name='"+element+"']:checked").each(function(){selectedTeacher.push($(this).val());});
		//$(".delete_link").attr("href"."index.php?st="+selectedTeacher+"&task=deleteTeacher&KeepThis=true&TB_iframe=true&height=420&width=750");
		document.getElementById('delete_link').href = 'index.php?st='+selectedTeacher+'&task=deleteTeacher&KeepThis=true&TB_iframe=true&height=420&width=750';
		document.getElementById('delete_link').click();
		//obj.task.value = 'deleteTeacher';                
		//obj.submit();		          
    }
  }

}
</script>

<form name="form1" id="form1" action="index.php" method="get">
<!--###### Content Board Start ######-->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom">
    <!-- ###### Tabs Start ######-->
    <?=$TabMenuDisplay?>
    <!-- ###### Tabs End ######-->					
    </td>
  </tr>
	<tr><td><?=$linterface->GET_SYS_MSG($msg); ?></td></tr>
  <tr> 
    <td class="main_content">
      <div class="navigation">
      </div>
      <div class="content_top_tool">
        <div class="Conntent_tool"> <a href="index.php?task=newTeacher&KeepThis=true&TB_iframe=true&height=350&width=750" class="new thickbox" title="<?=$button_new?>"> <?=$button_new?></a></div>
        <div class="Conntent_search">
          <input name="search_text" value="<?=$search_text?>" type="text"/>
        </div>
        <br style="clear:both" />
      </div>
      <div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="bottom"><div class="table_filter"></div></td>
            <td valign="bottom"><div class="common_table_tool">
              <a id="" onclick="delete_teacher(document.form1,'teacher_id[]')" href="javascript:void(0)" class="tool_delete" title="<?=$ec_iPortfolio['delete']?>"><?=$ec_iPortfolio['delete']?></a>
              <a style="display:none" id="delete_link" onclick="" href="javascript:void(0)" class="tool_delete thickbox" title="<?=$ec_iPortfolio['delete']?>"><?=$ec_iPortfolio['delete']?></a>
            </div></td>
          </tr>
        </table>
			        
        <?=$MemberListTable?>
      </div>
    </td>
  </tr>
</table>
<!--###### Content Board End ######-->
<input type="hidden" name="order" value=<?=$order?> />
<input type="hidden" name="pageNo" value=<?=$li->pageNo?> />
<input type="hidden" name="field" value=<?=$field?> />
<input type="hidden" name="page_size_change" value=<?=$page_size_change?> />
<input type="hidden" name="numPerPage" value=<?=$li->page_size?> />

<input type="hidden" name="group_type" value="<?=$group_type?>" />
<input type="hidden" name="group_id" value="<?=$group_id?>" />
<input type="hidden" name="task" id="task" value="listTeacher" />
<input type="hidden" name="msg" id="msg" value="" />
<input type="hidden" name="status" />
</form>