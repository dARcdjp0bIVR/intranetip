<?php
## Using By : Siuwan
##############################################
##	Modification Log:
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/dynReport/JSON.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
// temp lang
//include_once("temp_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Default show management group
$group_type = !isset($group_type) ? IPF_CFG_GROUP_SETTING_MGMT : $group_type;
$group_type = $_SESSION["platform"]=="KIS"?IPF_CFG_GROUP_SETTING_STD : $group_type;
switch($group_type)
{
	case IPF_CFG_GROUP_SETTING_ACCESS_RIGHT:
		$has_right = 3;
		break;
	case IPF_CFG_GROUP_SETTING_TEACHER:
		$has_right = 2;
		break;
	case IPF_CFG_GROUP_SETTING_MGMT:
		$has_right = 1;
		break;
	case IPF_CFG_GROUP_SETTING_STD:
		$has_right = 0;
		break;
}



$task = trim($task);
$task = ($task == "")? "listGroup" : $task;
// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_Grouping";

$lpf = new libpf_slp();
$lpf_ui = new libportfolio_ui();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

############  Content Here ######################
// page number settings

// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr = getTableActionArr();

#################################################

### Title ###
if($_SESSION["platform"]!="KIS")$TAGS_OBJ = libpf_tabmenu::getGroupSettingsTopTags($group_type);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//echo "ttt ".$task."<br/>";
/*debug_r($_POST);
echo "ttt ".$task."<br/>";

exit();*/
switch($task)
{
/*
	case 'listCat': //LIST CATEGORY
		$linterface->LAYOUT_START();
		include_once("template/listCat.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'listSubCat': //LIST SUB CATEGORY
	  $PAGE_NAVIGATION[] = array("Category List"); // tmp
    $PAGE_NAVIGATION[] = array("Cat 1"); // tmp
		$linterface->LAYOUT_START();
		include_once("template/listSubCat.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'listProg': //LIST PROGRAM
	  $PAGE_NAVIGATION[] = array("Category List"); // tmp
    $PAGE_NAVIGATION[] = array("Cat 1 Sub Cat List"); // tmp
    $PAGE_NAVIGATION[] = array("Sub Cat"); // tmp    
		$linterface->LAYOUT_START();
		include_once("template/listProg.tmpl.php");
		$linterface->LAYOUT_STOP();
	break;

	case 'cat_update':
		saveCat();
		break;
*/
  case "ajax":
    $task_script = "ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		//$task_script = "task/" . str_replace("../", "", $task) . ".php";
		$task_script = "task/" . $task . ".php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}
intranet_closedb();
exit();



//////////////////
//FUNCTION LIST //
//////////////////
/*
function saveCat()
{

 # DO SAVE 
 //....
 
 
 
 
 echo "
 <script>
    parent.index_reload_page('msg'); 
 </script>
 " ;
}
*/

function getTableActionArr()
{
	global $PATH_WRT_ROOT,$image_path,$LAYOUT_SKIN,$ec_iPortfolio;

	$TableActionArr = array();
	$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=activate', 1, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_approve.gif", "imgPublic", $ec_iPortfolio['activate']);
	$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=deactivate', 2, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_reject.gif", "imgPrivate", $ec_iPortfolio['deactive']);
	$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["remove_default"]."')) checkRemove(document.form1,'RecordID[]','update_ele.php?Action=remove')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete']);
	$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["edit_default"]."')) checkEdit(document.form1,'RecordID[]','edit_ele.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit']);
	return $TableActionArr;
}


?>

