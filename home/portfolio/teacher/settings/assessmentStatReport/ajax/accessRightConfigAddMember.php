<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$lpf = new libportfolio2007();

if (!$lpf->IS_ADMIN_USER($UserID) && !$lpf->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

######## Add Member START ########
$result = array();
$redirect = $_REQUEST['redirect'];
$AccessType = $_REQUEST['AccessType'];

switch($AccessType){
	case 'admin':
		#### Get Old User START ####
		$selectedUserArr = (array)IntegerSafe($_REQUEST['selectedCategory']);
		$selectedUserList = implode("','", $selectedUserArr);
		$sql = "SELECT
			UserID 
		FROM
			ASSESSMENT_ACCESS_RIGHT
		WHERE
			AccessType = '0'";
		$oldUser = $lpf->returnVector($sql);
		#### Get Old User END ####
		
		#### Delete User START ####
		$deleteUserArr = array_diff($oldUser, $selectedUserArr);
		$deleteUserList = implode("','", (array)$deleteUserArr);
		$sql = "DELETE FROM
			ASSESSMENT_ACCESS_RIGHT
		WHERE
			UserID IN ('{$deleteUserList}')";
		$result[] = $lpf->db_db_query($sql);
		#### Delete User END ####
		
		#### Add User START ####
		foreach($selectedUserArr as $uid){
			if(!in_array($uid, $oldUser)){
				$sql = "INSERT INTO ASSESSMENT_ACCESS_RIGHT (
					UserID, 
					AccessType, 
					InputDate, 
					CreatedBy
				) VALUES (
					'{$uid}',
					'0',
					CURRENT_TIMESTAMP,
					'{$UserID}'
				)";
				$result[] = $lpf->db_db_query($sql);
			}
		}
		#### Add User END ####
		
		if(in_array(false, $result)){
			$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		}else{
			$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
		}
		break;
	case 'subjectPanel':
		$subjectID = IntegerSafe($_REQUEST['subjectID']);
		$selectedUserArr = (array)IntegerSafe($_REQUEST['teacherID']);
		$yearID = (array)IntegerSafe($_REQUEST['yearId']);
		#### Get Old User START ####
		$selectedUserList = implode("','", $selectedUserArr);
		$sql = "SELECT
			* 
		FROM
			ASSESSMENT_ACCESS_RIGHT
		WHERE
			AccessType = '1'
		AND
			SubjectID = '{$subjectID}'";
		$rs = $lpf->returnResultSet($sql);
		
		$oldUser = array();
		foreach($rs as $r){
			$oldUser[ $r['UserID'] ][ $r['YearID'] ] = 1;
		}
		#### Get Old User END ####
		
		
		#### Add User START ####
		foreach($selectedUserArr as $uid){
			foreach($yearID as $year){
				if(!$oldUser[$uid][$year]){
					$sql = "INSERT INTO ASSESSMENT_ACCESS_RIGHT (
						UserID, 
						AccessType, 
						SubjectID,
						YearID,
						InputDate, 
						CreatedBy
					) VALUES (
						'{$uid}',
						'1',
						'{$subjectID}',
						'{$year}',
						CURRENT_TIMESTAMP,
						'{$UserID}'
					)";
					$result[] = $lpf->db_db_query($sql);
				}
			}
		}
		#### Add User END ####
		
		if(in_array(false, $result)){
			$Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
		}else{
			$Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
		}
		break;
}

header("Location: {$redirect}?Msg={$Msg}");
?>