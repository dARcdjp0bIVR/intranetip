<?php


$PATH_WRT_ROOT = '../../../../../';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$obj = new libportfolio2007();
$linterface = new interface_html();

if (!$obj->IS_ADMIN_USER($UserID) && !$obj->IS_TEACHER())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);

######### Get Subject Selection START #########
$objSubject = new subject();
$subjectList = $objSubject->Get_Subject_List();
//$subjectList = BuildMultiKeyAssoc($subjectList, array('RecordID') , array( Get_Lang_Selection('CH_DES', 'EN_DES') ), $SingleValue=1);
$SubjectInput = '<select name="subjectID">';
foreach($subjectList as $subject){
	$name = $subject[ Get_Lang_Selection('CH_DES', 'EN_DES') ];
	$SubjectInput .= "<option value=\"{$subject['RecordID']}\">{$name}</option>";
}
$SubjectInput .= '</select>';
######### Get Subject Selection END #########


######### Get Form Selection START #########
$objYear = new Year();
$yearList = $objYear->Get_All_Year_List();
$FormInput = '';
foreach($yearList as $index=>$year){
	$FormInput .= <<<HTML
	<div style="width:100px;display:inline-block">
		<input type="checkbox" id="yearId_{$index}" name="yearId[]" class="yearId" value="{$year['YearID']}" />
		<label for="yearId_{$index}">{$year['YearName']}</label>
	</div>
HTML;
	if(($index+1) % 3 == 0){
		$FormInput .= '<br />';
	}
}
######### Get Form Selection END #########


######### Get Teacher Selection START #########
$deselectedGroup = array();
$nameField = getNameFieldByLang2("IU.");
$_UserType = USERTYPE_STAFF;
$sql = "SELECT
	IU.UserID,
	{$nameField} As Name
FROM
	INTRANET_USER IU
WHERE
	RecordType = '{$_UserType}'
AND
	RecordStatus = '1'
AND
	UserID NOT IN ('{$selectedIdList}')
ORDER BY
	EnglishName
";
$rs = $obj->returnResultSet($sql);
foreach($rs as $r){
	$deselectedGroup[] = array(
		'value' => $r['UserID'], 
		'name' => $r['Name']
	);
}

$settings = array(
	'deselectedHeader' => $Lang['SysMgr']['RoleManagement']['Users'],
	'deselectedSelectionName' => 'deselectedCategory',
	'deselectedList' => $deselectedGroup,
	'selectedHeader' => $Lang['SysMgr']['RoleManagement']['SelectedUser'],
	'selectedSelectionName' => 'teacherID',
	'selectedList' => array()
);
$GroupMemberInput = $linterface->generateUserSelection($settings);
######### Get Teacher Selection END #########


######### UI START #########
$CurrentPage = "Settings_AssessmentStatisticReport";
$TAGS_OBJ[] = array($ec_iPortfolio['admin_accessRight'], "accessRightConfigAdmin.php", 0);
$TAGS_OBJ[] = array($ec_iPortfolio['subjectPanel_accessRight'], "", 1);
$MODULE_OBJ = $obj->GET_MODULE_OBJ_ARR('Teacher');

$linterface->LAYOUT_START($Msg);
?>

<form id="form1" action="ajax/accessRightConfigAddMember.php" method="post">
	<input type="hidden" name="redirect" value="../accessRightConfigSubjectPanel.php" />
	<input type="hidden" name="AccessType" value="subjectPanel" />
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	  <tr>
	    <td class="navigation"><?=$navigation_html ?><span style="float:right"><?=$linterface->GET_SYS_MSG($msg)?></span></td>
	  </tr>
	  <tr>
	    <td align="center">
	      <?=$tab_menu_html?>
	  	</td>
	  </tr>
	  <tr>
	    <td>
		    <table class="form_table inside_form_table">
		      <col class="field_title" />
		      <col  class="field_c" />
		      
		      
		      <tr>
		        <td><?=$ec_iPortfolio['SAMS_subject']?></td>
		        <td>:</td>
		        <td><?=$SubjectInput?></td>
		      </tr>
		      
		      <tr>
		        <td><?=$ec_iPortfolio['form']?></td>
		        <td>:</td>
		        <td><?=$FormInput?></td>
		      </tr>
		      
		      <tr>
		        <td><?=$Lang['iPortfolio']['GroupMemberName']?></td>
		        <td>:</td>
		        <td><?=$GroupMemberInput?></td>
		      </tr>
		      
		      
		    </table>
		  </td>
		</tr>
	</table>
	<div class="edit_bottom">
	  <p class="spacer"></p>
	  <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
	  <input type="button" class="formbutton" onclick="window.location='accessRightConfigSubjectPanel.php'" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
	  <p class="spacer"></p>
	</div>
	
</form>

<script>
	$('#subjectID > option:first').attr('selected', 'selected');
	
	
	$('#AddAll').click(function(){
		$('#teacherID').append( $('#deselectedCategory').find('option') );
	});
	$('#Add').click(function(){
		$('#teacherID').append( $('#deselectedCategory').find('option:selected') );
	});
	
	$('#Remove').click(function(){
		$('#teacherID > option:selected').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});
	$('#RemoveAll').click(function(){
		$('#teacherID > option').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});
	
	$('#form1').submit(function(){
		$('#teacherID > option').attr('selected', 'selected');
		
		if( $('.yearId:checked').length == 0){
			alert('<?=$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']?>');
			return false;
		}
		if( $('#teacherID > option').length == 0){
			alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
			return false;
		}
		
		return true;
	});
</script>

<?php 
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>
