<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-09 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$values = '';
$count = count($award);
for($i=0; $i<$count; $i++){
	$start = ($award_spt[$i] == '')? '' : intval($award_spt[$i]);
	$end = ($award_ept[$i] == '')? '' : intval($award_ept[$i]);
	
	$values .= $award[$i].'###'.$award_en[$i].'###'.$start.'###'.$end;
	if($i < ($count-1)){
		$values .= ';;';
	}
}

$SASsettings = new studentInfo('', $yearID);
$success = $SASsettings->setSASSettings('award', $values);

$msg = ($success == 1)? 'update' : 'add_failed';

intranet_closedb();
header("Location: award.php?msg=$msg&yearID=$yearID");

?>