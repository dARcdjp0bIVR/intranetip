<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-05 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Template for student page
$linterface = new interface_html();

// Page title
$CurrentPage = "Settings_SAS";

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Tag Object ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Category_Credit_Point'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/category_credit_point.php", 0);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Award'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/award.php", 0);
$TAGS_OBJ[] = array($Lang['iPortfolio']['SAS_Settings']['Release_Student'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/release_students.php.php", 1);

$today = date('Y-m-d');

# Class level
$sql = 'Select YearID as ClassLevelID, YearName as LevelName From YEAR Order By Sequence';
$classArr = $lpf->returnArray($sql,2);

# Get SAS Settings
$SAS = new studentInfo('', Get_Current_Academic_Year_ID());
$data_Ary = $SAS->getSASSettings('release');
$data_Ary = $data_Ary['release'];

$SAS_settings = array();
if(count($data_Ary) > 0)
{
    $classArr = BuildMultiKeyAssoc($classArr, "ClassLevelID");
	$settings = explode(';;', $data_Ary['SettingValue']);
	
	for($i = 0; $i<count($settings); $i++){
		$stored_value = explode('###', $settings[$i]);
		
		$class_name = $classArr[$stored_value[0]]['LevelName'];
		$target_startdate = explode(' ', $stored_value[1]);
		$target_enddate = explode(' ', $stored_value[2]);
		
		$SAS_settings[] = array($stored_value[0], $class_name, $target_startdate[0], $target_enddate[0]);
	}
} else {
	# Use $classArr as default value
	$SAS_settings = $classArr;
}

$content = '';

# Header
$content .= "<table class=\"common_table_list_v30 edit_table_list_v30\" cellspacing=\"0\" cellpadding=\"5\">
				<tr>
					<th>".$i_ClassLevelName."</th>
					<th>".$iPort["start_time"]."</th>
					<th>".$iPort["end_time"]."</th>
				</tr>
				<tr class=\"edit_table_head_bulk\">
	               	<th>&nbsp;</th>
	               	<th>
						<div style=\"float:left;\">".
							$linterface->GET_DATE_PICKER("All_release_starttime", $today, "", "yy-mm-dd").
						"</div>".
						$linterface->Get_Apply_All_Icon("javascript:jSetValue(document.form1.All_release_starttime.value, document.form1,'release_starttime[]')")."
					</th>
	                <th>
						<div style=\"float:left;\">".
							$linterface->GET_DATE_PICKER("All_release_endtime", $today, "", "yy-mm-dd").
						"</div>".
						$linterface->Get_Apply_All_Icon("javascript:jSetValue(document.form1.All_release_endtime.value, document.form1,'release_endtime[]')")."
	                    </th>
					</tr>";

# Content
$i = 0;
foreach($SAS_settings as $form_SAS)
{
	$content .= "<tr>
					<td>
						<span>".$form_SAS[1]."</span>
						<input type=\"hidden\" name=\"LevelID[]\" value=".$form_SAS[0].">
					</td>
				    <td>".
						$linterface->GET_DATE_PICKER("release_starttime[]", $form_SAS[2], "", "yy-mm-dd", "", "", "", "", "release_starttime_$i", 0, 1)."
					</td>
				 	<td>".
						$linterface->GET_DATE_PICKER("release_endtime[]", $form_SAS[3], "", "yy-mm-dd", "", "", "", "", "release_endtime_$i", 0, 1)."
					</td>
				 </tr>";
	$i++;
}
$content .= "</table>";

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">
function jSetValue(val, obj, element_name)
{
	len = obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) 
	{
	    if (obj.elements[i].name==element_name)
	    obj.elements[i].value=val;
	}
}

function checkform()
{
	var submits = false;
	$("#btnSubmit").attr("disabled", true);
	
	var obj = document.form1;
	var start_times = document.getElementsByName('release_starttime[]');
	var end_times = document.getElementsByName('release_endtime[]');
	
	var classCount = start_times.length;
	
	if (classCount > 0){
		submit = true;
		
		for (var i=0; i<classCount; i++){
			var current_start = start_times[i];
			var current_end = end_times[i];
			
			if (typeof(current_start)=="undefined" || current_start.value=="")
			{
				current_start.focus();
				alert("<? echo $assignments_alert_msg9 ?>");
				submit = false;
				break;
			} 
			else if(typeof(current_end)=="undefined" || current_end.value=="")
			{
				current_end.focus();
				alert("<? echo $assignments_alert_msg9 ?>");
				submit = false;
				break;
			}
			else if (typeof(current_start)!="undefined" && current_start.value!="" && typeof(current_end)!="undefined" && current_end.value!="") 
			{
				if(!check_date(current_start, "<?php echo $assignments_alert_msg9; ?>")) {
					current_start.focus();
					submit = false;
					break;
				}
					
				if(!check_date(current_end, "<?php echo $assignments_alert_msg9; ?>")) {
					current_end.focus();
					submit = false;
					break;
				}
				
				if(!compare_date(current_start, current_end, "<?php echo $w_alert['start_end_time2']; ?>")) {
					current_start.focus();
					submit = false;
					break;
				}
			}
		}
	}
	
	if(submit){
		obj.submit();
	} else {
		$("#btnSubmit").attr("disabled", false);
		return false;
	}

}
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>

<form name="form1" id="form1" action="release_student_update.php" method="post">

<?=$linterface->GET_NAVIGATION2($Lang['iPortfolio']['SAS_Settings']['Release_Student'])?>

<br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
	</tr>
</table>

<br>

<!-- Content Here -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr valign="top">
		<td width="65%"><?=$content?></td>
	</tr>
</table>

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">

<?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "checkform();", "btnSubmit")?>
&nbsp;
<?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "reset", "", "btnReset")?>

</td></tr>
</table>

</form>

<!-- ENd Content Here -->
</td></tr></table>
</td></tr></table>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
