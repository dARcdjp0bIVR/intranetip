<?php
## Using By :  

/******************************************************************************
 * Modification log:
 * 2015-01-09 (Bill)
 * - Create file
 *****************************************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/ngwah/libpf-slp-ngwah.php");

intranet_auth();
intranet_opendb();

$lpf = new libportfolio2007();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$values = '';
$count = count($LevelID);
for($i=0; $i<$count; $i++){
	$start = ($release_starttime[$i] == '')? '' : $release_starttime[$i].' 00:00:00';
	$end = ($release_endtime[$i] == '')? '' : $release_endtime[$i].' 23:59:59';
	
	$values .= $LevelID[$i].'###'.$start.'###'.$end;
	if($i < ($count-1)){
		$values .= ';;';
	}
}

$SASsettings = new studentInfo('', Get_Current_Academic_Year_ID());
$success = $SASsettings->setSASSettings('release', $values);

$msg = ($success == 1)? 'update' : 'add_failed';

intranet_closedb();
header("Location: release_students.php?msg=$msg&yearID=$yearID");

?>