<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$title = htmlspecialchars($title);

if($Action == "activate")
{
	$fields_values = "RecordStatus = '1'";
	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '0'";
	$li->db_db_query($sql);
	$fields_values = "RecordStatus = '2'";
	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '3'";
	$li->db_db_query($sql);
	$msg = 'activate';
}
else if($Action == "deactivate")
{
	$fields_values = "RecordStatus = '0'";
	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '1'";
	$li->db_db_query($sql);
	$fields_values = "RecordStatus = '3'";
	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".implode(",", $RecordID).") AND RecordStatus = '2'";
	$li->db_db_query($sql);
	$msg = 'suspend';
}
else
{
	if($RecordID!="")
	{
		$fields_values = "ChiTitle = '$ChiTitle', ";
		$fields_values .= "EngTitle = '$EngTitle', ";
		$fields_values .= "RecordStatus = '$status', ";
		$fields_values .= "ModifiedDate = now()";
		$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID = '$RecordID'";
		$li->db_db_query($sql);
		$msg = 'update';
	}
	else
	{
		$fields = "ChiTitle, EngTitle, RecordStatus, ModifiedDate, InputDate";
		$values = "'$ChiTitle', '$EngTitle', '$status', now(), now()";
		$sql = "INSERT INTO {$eclass_db}.OLE_CATEGORY ($fields) VALUES ($values) ";
		$li->db_db_query($sql);
		$msg = 'add';
	}
}

intranet_closedb();

header("Location: category.php?msg=$msg");
?>
