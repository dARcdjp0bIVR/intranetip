<?php

/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");

intranet_auth();
intranet_opendb();
//debug_r($approvalSettings);die;
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

//$ole_approval_file = "$eclass_root/files/portfolio_olr_approval_setting.txt";
$ole_approval_file = "$eclass_root/files/portfolio_olr_approval_setting_splited.txt";
$li_fs = new libfilesystem();
//if ($ApprovalSetting == $ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"]) {
//	if (empty($allowEdit)) {
//		$allowEdit = 0;
//	}
//} else {
//	$allowEdit = 0;
//}
//$ApprovalSetting = $ApprovalSetting.",".$allowEdit;

$li_fs->file_write(serialize($approvalSettings), $ole_approval_file);
$msg = "update";

intranet_closedb();

header("Location: record_approval.php?msg=$msg");
?>
