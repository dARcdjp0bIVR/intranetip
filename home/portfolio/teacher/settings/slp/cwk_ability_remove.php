<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$sql = "DELETE FROM {$eclass_db}.CWK_COMMON_ABILITY ";
$sql .= "WHERE AbilityID IN (".implode(",", $AbilityID).")";
$li->db_db_query($sql);
$msg = "delete";

intranet_closedb();
header("Location: cwk_common_ability.php?msg=$msg");
?>