<?php
// Modifing by anna

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$lpf = new libportfolio();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$objIpfSetting = new iportfolio_period_settings();
$success = $objIpfSetting->updatePersonalCharacterPeriodSetting($_POST);

$msg = $success? "update" : "import_failed";

intranet_closedb();

header("Location: personal_character_period.php?msg=$msg");
?>
