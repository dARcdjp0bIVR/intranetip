<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";
$CurrentPageName = $iPort['menu']['ole']." ".$iPort['menu']['settings'];

$luser = new libuser($UserID);
$lpf = new libportfolio();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

$RecordID = (is_array($RecordID)) ? $RecordID[0] : $RecordID;
if($RecordID!="")
{
	$sql = "SELECT ChiTitle, EngTitle, RecordStatus FROM {$eclass_db}.OLE_CATEGORY WHERE RecordID = '{$RecordID}'";
	$row = $lpf->returnArray($sql);

	list($ChiTitle, $EngTitle, $RecordStatus) = $row[0];
	${"checked_".$RecordStatus} = "CHECKED='CHECKED'";
	
} else
{
	$checked_1 = "CHECKED='CHECKED'";
	
}

?>

<script language="JavaScript">
function checkform(formObj){
	// YuEn: check date and title!
	if(formObj.EngTitle.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		formObj.EngTitle.focus();
		return false;
	}
	if(formObj.ChiTitle.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		formObj.ChiTitle.focus();
		return false;
	}
	
	formObj.submit();
}

</script>

<form name="form1" action="category_update.php" method="POST">
	<br/>
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
			<table id="body_frame1" width="100%" border="0" cellspacing="0" cellpadding="4" align="center">
				<br />
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="15%">
						<?=$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_eng'].")"?>
						<span class="tabletextrequire">*</span>
					</td>
					<td valign="top" class="tabletext"><input class="text" type="text" name="EngTitle" size="40" maxlength="255" value="<?=$EngTitle?>">
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$ec_iPortfolio['title']." (".$ec_iPortfolio['lang_chi'].")"?>
						<span class="tabletextrequire">*</span>
					</td>
					<td valign="top" class="tabletext"><input class="text" type="text" name="ChiTitle" size="40" maxlength="255" value="<?=$ChiTitle?>">
				</tr>
				<tr>
					<td valign="top"width="120" class="formfieldtitle tabletext" nowrap="nowrap">
						<?= $qbank_status ?>
						<span class="tabletextrequire">*</span>
					</td>
			        <td >
						<table width="365" border="0" cellspacing="0" cellpadding="0">
			                <tr>
			                  	<td width="100%" class="tabletext">
									<input type="radio" name="status" id="status_1" value="1" <?=$checked_1?>><label for="status_1"><?= $button_public ?></label>&nbsp;&nbsp;
									<input type="radio" name="status" id="status_0" value="0" <?=$checked_0?>><label for="status_0"><?= $button_private ?></label>
								</td>
			            	</tr>
			        	</table>
			        </td>
				</tr>
					
				</tr>
			</table>
		</td></tr>
	</table>
	
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td class="tabletextremark" colspan="6"><?=$ec_iPortfolio['mandatory_field_description']?></td>
		</tr>
		<tr>
			<td align="center" colspan="6">
				<div style="padding-top: 5px">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button","javascript: checkform(document.form1);")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
				</div>
			</td>
		</tr>
	</table>

	<input type="hidden" name="RecordID" value="<?=$RecordID?>" />

</form>
<!-- ENd Content Here -->


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>

