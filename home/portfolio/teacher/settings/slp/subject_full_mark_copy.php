<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- create this page
##
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = stripslashes($_POST['AcademicYearID']);

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$lpf = new libpf_slp();
$libfcm_ui = new form_class_manage_ui();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################
// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Academic Result Type Display (for student)
$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_SR, $currentPageIndex);

// Academic Year Selection
$FromYearSelection = getSelectAcademicYear('FromYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, '');
$ToYearSelection = getSelectAcademicYear('ToYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value);"', $noFirst=1, $noPastYear=0, $AcademicYearID);

// Save Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Go_Submit();", $id="Btn_Save");
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();", $id="Btn_Cancel");

// hidden
$hidden = '';
$hidden .= $linterface->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);
$hidden .= $linterface->GET_HIDDEN_INPUT('YearID', 'YearID', $YearID);

##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_SR);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>
<script>
var canSubmit = true;
function js_Go_Submit(){
	if(canSubmit){
		$('div.warnMsgDiv').hide();
		
		if( $('#FromYearID').val() == $('#ToYearID').val() ){
	// 		alert('Can not copy to same year');
			$('div.warnMsgDiv').show();
			return false;	
		}
		canSubmit = false;
		$('#form1').submit();
	}
	else{
		return false;
	}
}

function js_Go_Back(){
// 	window.location = 'subject_full_mark.php';
	$('#form1').attr('action','subject_full_mark.php').submit();
}
</script>
<form id="form1" name="form1" action="subject_full_mark_copy_update.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td><?=$lpf->GET_TAB_MENU($TabMenuArr)?></td></tr>
		<tr>
			<td style="padding-left:7px;">
				<table class="form_table_v30" width="90%" align="center">
					<tr>	
						<td class="field_title" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear']?></td>
						<td class="tabletext">
							<?php echo $FromYearSelection?>
							<?php echo $linterface->Get_Form_Warning_Msg('sameYearWarnDiv', $Lang['iPortfolio']['CannotSameYear'], $Class='warnMsgDiv')."\n" ?>
						</td>
					</tr>
					<tr>
						<td class="field_title" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?> </td>
						<td class="tabletext">
							<?php echo $ToYearSelection?>
							<?php echo $linterface->Get_Form_Warning_Msg('sameYearWarnDiv', $Lang['iPortfolio']['CannotSameYear'], $Class='warnMsgDiv')."\n" ?>
						</td>
					</tr>
				</table>
				
				<div class="edit_bottom_v30">
					<?=$SaveBtn?>
					<?=$BackBtn?>
				</div>
			</td>
		</tr>
	</table>
	<?php echo $hidden?>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>