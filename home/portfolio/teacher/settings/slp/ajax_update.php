<?php
## Using By : ivan 
##############################################
##	Modification Log:
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");

$libpf = new libportfolio();
$libpf_academic = new libpf_academic();

$Action = stripslashes($_REQUEST['Action']);
if ($Action == 'Save_Subject_FullMark')
{
	$AcademicYearID = $_POST['AcademicYearID'];
	$FullMarkInfoArr = $_POST['FullMarkInfoArr'];
	
	// Get Current Full Mark Info to determine "insert" or "update"
	$OriginalFullMarkArr = $libpf_academic->Get_Subject_Full_Mark($AcademicYearID);
	
	# Insert Record
	$libpf_academic->Start_Trans();
		
	$SuccessArr = array();
	$InsertValueArr = array();
	foreach ((array)$FullMarkInfoArr as $thisYearID => $thisYearFullMarkInfoArr)
	{
		foreach ((array)$thisYearFullMarkInfoArr as $thisSubjectID => $thisYearSubjectFullMarkInfoArr)
		{
			if (isset($OriginalFullMarkArr[$AcademicYearID][$thisYearID][$thisSubjectID])) {
				// update
				$thisFullMarkID = $OriginalFullMarkArr[$AcademicYearID][$thisYearID][$thisSubjectID]['FullMarkID'];
				$SuccessArr['Update'][$thisYearID][$thisSubjectID] = $libpf_academic->Update_Subject_Full_Mark($thisFullMarkID, $thisYearSubjectFullMarkInfoArr);
			}
			else {
				// insert
				$InsertValueArr[] = array(	'AcademicYearID' => $AcademicYearID,
											'YearID' => $thisYearID,
											'SubjectID' => $thisSubjectID,
											'FullMarkInt' => $thisYearSubjectFullMarkInfoArr['FullMarkInt'],
											'PassMarkInt' => $thisYearSubjectFullMarkInfoArr['PassMarkInt'],
											'FullMarkGrade' => $thisYearSubjectFullMarkInfoArr['FullMarkGrade'],
										);
			}
			$SuccessArr['UpdatePassTotal'][$thisYearID][$thisSubjectID] = $libpf->UpdatePassTotal(
				$AcademicYearID,
				$thisSubjectID, 
				$thisYearID, 
				$thisYearSubjectFullMarkInfoArr['FullMarkInt'], 
				$thisYearSubjectFullMarkInfoArr['PassMarkInt']
			);
		}
	}

	if (count($InsertValueArr) > 0) {
		$SuccessArr['Insert'] = $libpf_academic->Insert_Subject_Full_Mark($InsertValueArr);
	}
	
	if (in_multi_array(false, $SuccessArr)) {
		$libpf_academic->RollBack_Trans();
		echo '0';
	}
	else {
		$libpf_academic->Commit_Trans();
		echo '1';
	}
}
?>