<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaward.php");
include_once($PATH_WRT_ROOT."includes/libservice.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

//$luser = new libuser($UserID);
$lpf = new libpf_slp();
$lpf_report = new libpf_report();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

###############################################
############  Functions #######################
###############################################

function getYearTerm($ParType)
{
  global $intranet_db, $eclass_db;

  $li = new libdb();
  
  switch($ParType)
  {
    case "activity":
      $sql = "SELECT DISTINCT Year, Semester FROM {$eclass_db}.ACTIVITY_STUDENT UNION SELECT DISTINCT Year, Semester FROM {$intranet_db}.PROFILE_STUDENT_ACTIVITY ORDER BY Year DESC, Semester";
      break;
    case "award":
      $sql = "SELECT DISTINCT Year, Semester FROM {$eclass_db}.AWARD_STUDENT UNION SELECT DISTINCT Year, Semester FROM {$intranet_db}.PROFILE_STUDENT_AWARD ORDER BY Year DESC, Semester";
      break;
    case "service":
      $sql = "SELECT DISTINCT Year, Semester FROM {$eclass_db}.SERVICE_STUDENT UNION SELECT DISTINCT Year, Semester FROM {$intranet_db}.PROFILE_STUDENT_SERVICE ORDER BY Year DESC, Semester";
      break;
    case "merit":
      $sql = "SELECT DISTINCT Year, Semester FROM {$eclass_db}.MERIT_STUDENT UNION SELECT DISTINCT Year, Semester FROM {$intranet_db}.PROFILE_STUDENT_MERIT ORDER BY Year DESC, Semester";
      break;
    case "assessment":
      $sql = "SELECT DISTINCT Year, Semester FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD UNION SELECT DISTINCT Year, Semester FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD ORDER BY Year DESC, Semester";
      break;
  }

  $resArr = $li->returnArray($sql);

  $returnArr = array();
  for($i=0; $i<count($resArr); $i++)
  {
    if(!empty($resArr[$i][0]))
    {
      $returnArr[$resArr[$i][0]][] = $resArr[$i][1];
    }
  } 
  return $returnArr;
}

function getClassHistory()
{
  global $intranet_db;

  $li = new libdb();
  
  $sql = "SELECT DISTINCT AcademicYear FROM {$intranet_db}.PROFILE_CLASS_HISTORY ORDER BY AcademicYear DESC";
  $returnArr = $li->returnVector($sql);
  
  return $returnArr;
}

###############################################
###############################################
###############################################

##################################################
############  Cotnent Here #######################
##################################################
// Tab Menu Settings
//*** current page index*****
// 0 -   * Year Syn
// 1 -   * Academic Result Type Display (for student)
$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_SR, $currentPageIndex);

//// load approval settings
//$ApprovalSetting = $lpf->GET_OLR_APPROVAL_SETTING();
//$ParArr["ApprovalSetting"] = $ApprovalSetting;

$Content = "";
$Content .= $lpf->GET_TAB_MENU($TabMenuArr);
$Content .= $linterface->GET_SYS_MSG($msg);
##################################################
##################################################
##################################################

# Class history year
$YearArray['ClassHistory'] = getClassHistory();
# Activity year
$YearSemArray['activity'] = getYearTerm("activity");
$YearArray['activity'] = array_keys($YearSemArray['activity']);
# Assessment year
$YearSemArray['assessment_report'] = getYearTerm("assessment");
$YearArray['assessment_report'] = array_keys($YearSemArray['assessment_report']);
# Attendance year
$lpf_att = new libpf_attendance();
// [2020-0521-1400-38206]
//$YearSemArray['attendance'] = $lpf_att->getAttendanceYearTerm();
$YearSemArray['attendance'] = $lpf_att->getAttendanceYearTerm($groupByYearTerm=true);
$YearArray['attendance'] = array_keys($YearSemArray['attendance']);
# Award year
$YearSemArray['award'] = getYearTerm("award");
$YearArray['award'] = array_keys($YearSemArray['award']);
# Teacher comment year
$lpf_tc = new libpf_teacher_comment();
$YearSemArray['teacher_comment'] = $lpf_tc->getTeacherCommentYearTerm();
$YearArray['teacher_comment'] = array_keys($YearSemArray['teacher_comment']);
# Merit year
$YearSemArray['merit'] = getYearTerm("merit");
$YearArray['merit'] = array_keys($YearSemArray['merit']);
# Service year
$YearSemArray['service'] = getYearTerm("service");
$YearArray['service'] = array_keys($YearSemArray['service']);

$lay = new academic_year();
$SysYearArray = $lay->Get_All_Year_List("en");
$year_selection_html = getSelectByArray($SysYearArray, "name=\"ay_id[]\"", "", 0, 0, "", 2);

foreach($YearArray AS $module_title => $year_array) {

  if(count($year_array) > 0)
  {
    $t_module_title = isset($ec_iPortfolio[$module_title]) ? $ec_iPortfolio[$module_title] : (isset(${"i_Profile_".$module_title}) ? ${"i_Profile_".$module_title} : $module_title);
  
    $x .= "<tr>";
    $x .= "<td height=\"25\" bgcolor=\"#FFFFFF\" valign=\"top\"><a href=\"javascript:displayTable('table_$module_title')\" class=\"tablelink\">+ ".$t_module_title."</a></td>";
    $x .= "</tr>";
    
    $x .= "<tr>";
    $x .= "<td>";
    
    $x .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:none\" id=\"table_$module_title\">";
    $x .= "<tr>";
    $x .= "<td width=\"25%\" height=\"25\" class=\"tabletop\">".$ec_iPortfolio['existing_format']."</td>";
    $x .= "<td width=\"25%\" height=\"25\" class=\"tabletop\">".$ec_iPortfolio['convert_to']."</td>";
    $x .= "<td width=\"25%\" height=\"25\" class=\"tabletop\">".$ec_iPortfolio['existing_format']."</td>";
    $x .= "<td width=\"25%\" height=\"25\" class=\"tabletop\">".$ec_iPortfolio['convert_to']."</td>";
    $x .= "</tr>";
    
    for($i=0; $i<count($year_array); $i++)
    {
      $t_year = $year_array[$i];
      $t_sem_arr = $YearSemArray[$module_title][$t_year];
      $t_sem_cnt = count($t_sem_arr) > 0 ? count($t_sem_arr) : 1;
    
      $tdstyle = ($i%2 == 0) ? "bgcolor=\"#FFFFFF\"" : "bgcolor=\"#F3F3F3\"";
    
      $x .= "<tr>";
      $x .= "<td height=\"25\" rowspan=\"".$t_sem_cnt."\" $tdstyle>".$t_year;
      $x .= "<input type=\"hidden\" name=\"module[]\" value=\"".$module_title."\" />";
      $x .= "<input type=\"hidden\" name=\"old_year[]\" value=\"".$t_year."\" />";
      $x .= "</td>";
      $x .= "<td height=\"25\" rowspan=\"".$t_sem_cnt."\" $tdstyle>";
      $x .= $year_selection_html;
      $x .= "</td>";
      
      if(count($t_sem_arr) > 0)
      {
        for($j=0; $j<count($t_sem_arr); $j++)
        {
          $t_sem = htmlspecialchars($t_sem_arr[$j]);
		  $d_sem = 	$t_sem; // $d_sem caption for display semester

		  //20110630 For empty term imply "whole year" only apply to 'award','service','activity' (May change later if needed)
		  $wholeYearRecord = array('award','service','activity');
		  if(in_array($module_title,$wholeYearRecord)){
			  $d_sem = ($d_sem == '') ? $Lang['General']['WholeYear'] : $t_sem;
		  }

          $x .= ($j==0) ? "" : "<tr>";
          $x .= "<td height=\"25\" $tdstyle>".$d_sem;
          $x .= "<input type=\"hidden\" name=\"old_semester[".$module_title."][".$t_year."][]\" value=\"".$t_sem."\" />";
          $x .= "</td>";
          $x .= "<td height=\"25\" $tdstyle name=\"".$module_title."_".$t_year."\"><select name=\"YearTermID[".$module_title."][".$t_year."][]\"><option value=\"\">-- ".$button_select." --</option></select></td>";
        }
      }
      else
      {
        $x .= "<td height=\"25\" $tdstyle>--</td>";
        $x .= "<td height=\"25\" $tdstyle>--</td>";
      }
      
      $x .= "</tr>";
      
      $j++;
    }
    
    $x .= "</table>";
    
    $x .= "</td>";
    $x .= "</tr>";
  }
}

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_SR);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<script language="JavaScript">
var module_title_arr = new Array();
module_title_arr["ClassHistory"] = "<?=$i_Profile_ClassHistory?>";
module_title_arr["activity"] = "<?=$ec_iPortfolio["activity"]?>";
module_title_arr["assessment_report"] = "<?=$ec_iPortfolio["assessment_report"]?>";
module_title_arr["attendance"] = "<?=$ec_iPortfolio["attendance"]?>";
module_title_arr["award"] = "<?=$ec_iPortfolio["award"]?>";
module_title_arr["teacher_comment"] = "<?=$ec_iPortfolio["teacher_comment"]?>";
module_title_arr["merit"] = "<?=$ec_iPortfolio["merit"]?>";
module_title_arr["service"] = "<?=$ec_iPortfolio["service"]?>";

var curr_table_style = "none";

function checkform(formObj)
{
  var rec_str = "";
  var prev_module = "";

  $("select[name=ay_id[]]").each(function(i)
    {
      if($(this).val() != "")
      {
        var t_module_title = $("input[name=module[]]:eq("+i+")").val();
        var t_old_year = $("input[name=old_year[]]:eq("+i+")").val();
        
        if(prev_module != t_module_title)
        {
          rec_str = rec_str + "\n" + module_title_arr[t_module_title] + ":\n";
        }
        
        rec_str = rec_str + "<?=$ec_iPortfolio['year']?> " + t_old_year + " <?=$iPort['to']?> <?=$ec_iPortfolio['year']?> " + $(this).find("option:selected").text() + "\n";
        
        prev_module = t_module_title;
      }
    }
  );
  
  if(prev_module == "")
  {
    return false;
  }
  else
  {
    return confirm("<?=$iPort["msg"]["confirm_make_change"]?>\n"+rec_str);
  }
}

function displayAllTable(){

  var style2change = (curr_table_style == "") ? "none" : "";
  var btn_text = (curr_table_style == "") ? "+ <?=$i_general_expand_all?>" : "+ <?=$i_general_collapse_all?>";

  if($("#table_ClassHistory").length > 0) displayTable("table_ClassHistory", style2change);
  if($("#table_activity").length > 0) displayTable("table_activity", style2change);
  if($("#table_assessment_report").length > 0) displayTable("table_assessment_report", style2change);
  if($("#table_attendance").length > 0) displayTable("table_attendance", style2change);
  if($("#table_award").length > 0) displayTable("table_award", style2change);
  if($("#table_teacher_comment").length > 0) displayTable("table_teacher_comment", style2change);
  if($("#table_merit").length > 0) displayTable("table_merit", style2change);
  if($("#table_service").length > 0) displayTable("table_service", style2change);
  
  $("#table_control_btn").html(btn_text);
  
  curr_table_style = style2change;
}

$(document).ready( function(){
  $("select[name=ay_id[]]").change( function(){
    var t_module = $(this).parent().parent().find("input[name=module[]]").val();
    var t_oldyear = $(this).parent().parent().find("input[name=old_year[]]").val();
    var target_td = $(this).parent().parent().parent().find("td[name="+t_module+"_"+t_oldyear+"]");

    if(target_td.size() > 0)
    {
      if($(this).val() == "")
      {
        target_td.each(function(){
          $(this).html("<select name=\"YearTermID["+t_module+"]["+t_oldyear+"][]\"><option value=\"\">-- <?=$button_select?> --</option></select>");
        });
      }
      else
      {    
        $.ajax({
          url: '../../../ajax/ajax_get_yearterm_opt.php',
          data: "ay_id="+$(this).val()+"&requestLang=en",
          success: function(data) {
            target_td.each(function(){
              var select_text = data.replace("YearTermID", "YearTermID["+t_module+"]["+t_oldyear+"][]");
            
              $(this).html(select_text);
            });
          }
        });
      }
    }
  });
});

</script>

<form name="form1" method="POST" action="sr_year_list_update.php" onSubmit="return checkform(this)">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?=$Content?>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td>
      <?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['convert_year_instruction'], "")?>
    </td>
  </tr>
  <tr>
    <td align="center">
      <table width="90%" cellspacing="0" cellpadding="5" border="0">
        <tr>
          <td>
            <table width="100%" cellspacing="0" cellpadding="5" border="0">
              <tr>
                <td colspan="2">
                  <a href="javascript:displayAllTable()" id="table_control_btn">+ <?=$i_general_expand_all?></a>
                </td>
              </tr>
              <tr>
                <td width="30">&nbsp;</td>
                <td>
                  <table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor='#cccccc'>
                    <?=$x?>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
        <tr>
          <td align="right">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">
                  <?=$linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "submit", "", "btnSubmit")?>
                  &nbsp;
                  <?=$linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "reset", "", "btnReset")?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>

</table>

</td></tr></table>
</td></tr></table>

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>