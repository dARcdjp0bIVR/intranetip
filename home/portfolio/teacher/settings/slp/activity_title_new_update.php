<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

intranet_auth();
intranet_opendb();


$luser = new libuser($UserID);
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

if (isset($temp_type))
{
	$Title = trim($Title);
	if(is_array($ele) && !empty($ele))
	{
		$ELEList = implode(",", $ele);
		$Title = $Title."\t".trim($ELEList);
	}

	$lf = new libwordtemplates_ipf(3);
	$data = $lf->getTemplatesContent($temp_type);
	
	$NewData = "";
	$DataArray = explode("\n", $data);
	if(!empty($DataArray))
	{
		for($k=0; $k<sizeof($DataArray); $k++)
		{
			$t_title = ($TitleID!="" && $k==$TitleID) ? $Title : intranet_undo_htmlspecialchars(addslashes(trim($DataArray[$k])));
			if($t_title!="") {
				$NewData .= $t_title."\n";
			}
		}
		if($TitleID=="") {
			$NewData .= $Title."\n";
		}
	}

	$lf->updateTemplatesContent($temp_type, stripslashes(intranet_htmlspecialchars($NewData)));
	$msg = ($TitleID=="") ? "add" : "update";
}

intranet_closedb();

header("Location: activity_title_template.php?msg=$msg&temp_type=$temp_type");
?>