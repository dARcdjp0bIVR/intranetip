<?php
## Using By : Max
##############################################
##	Modification Log:
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

##################################################
############  Cotnent Here #######################
##################################################
$Content = "";

// Tab Menu Settings
//*** current page index*****
// 0 -   * Management Group
// 1 -   * Components of OLE
// 2 -   * Programme Category
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 4;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);

$student_ole_config_file = "$eclass_root/files/student_ole_config.txt";
$filecontent = trim(get_file_content($student_ole_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em, $check_int, 
$check_ext,$ex_starttime,$ex_sh,$ex_sm,$ex_endtime,$ex_eh,$ex_em) = unserialize($filecontent);


if($check_int == "on")
$check_int = "checked";

if($check_ext == "on")
$check_ext = "checked";

$ParArr["starttime"] = $starttime;
$ParArr["sh"] = $sh;
$ParArr["sm"] = $sm;
$ParArr["endtime"] = $endtime;
$ParArr["eh"] = $eh;
$ParArr["em"] = $em;
$ParArr["check_int"] = $check_int;

$ParArr["ex_starttime"] = $ex_starttime;
$ParArr["ex_sh"] = $ex_sh;
$ParArr["ex_sm"] = $ex_sm;
$ParArr["ex_endtime"] = $ex_endtime;
$ParArr["ex_eh"] = $ex_eh;
$ParArr["ex_em"] = $ex_em;
$ParArr["check_ext"] = $check_ext;

$Content = "";
$Content .= $lpf->GET_TAB_MENU($TabMenuArr);
$Content .= $linterface->GET_SYS_MSG($msg);
$Content .= $lpf->GET_SUBMISSION_PERIOD_FORM2($ParArr);
##################################################
##################################################
##################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(obj)
{
	var isst = false;
	if (typeof(obj.starttime)!="undefined")
	{
		if (obj.starttime.value!="")
		{
			if(!check_date(obj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			isst = true;
		}
	}
	if (typeof(obj.endtime)!="undefined")
	{
		if (obj.endtime.value!="")
		{
			if(!check_date(obj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(obj.starttime, obj.sh, obj.sm, obj.endtime, obj.eh, obj.em)) {
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
				if(obj.starttime.value==obj.endtime.value && obj.sh.value==obj.eh.value && obj.sm.value==obj.em.value){
					obj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}
	
	if ((obj.endtime.value=="" && obj.starttime.value=="") && (obj.check_int.checked || obj.check_ext.checked))
	{
		alert("<? echo $assignments_alert_msg9 ?>");
		return false;
	}
/*
  # Eric Yip (20090719): Not necessary to force check
	if(!obj.check_int.checked && !obj.check_ext.checked)
	{
		alert("<? echo $iPort["msg"]["select_submit_type"] ?>");
		return false;
	}
*/
	return true;
}

function jReset()
{
	obj = document.form1;
	obj.reset();
}

function jSubmit()
{
	obj = document.form1;
	if(checkform(obj))
	obj.submit();
}

</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
