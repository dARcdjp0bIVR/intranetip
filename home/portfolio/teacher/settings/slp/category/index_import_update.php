<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-subCategory.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/oleCategory/libpf-program-title.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dynReport/JSON.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

unset($error_data);
$count_success = 0;
if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

$uploaderUserId = $UserID;  // variable from session

intranet_opendb();
$lpf = new libpf_slp();
$lpf_ole = new libpf_ole();

$lpf->CHECK_ACCESS_IPORTFOLIO();
	
$CurrentPage = "Settings_OLE";
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Tab Menu Settings ###
$TabMenuArr  = getTabMenuArr();

//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval

$currentPageIndex = 1;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);
$TabMenuDisplay = $lpf->GET_TAB_MENU($TabMenuArr);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$li = new libdb();

$libCategory = new Category();
$libSubCategory = new subCategory();	
$libProgram = new libpf_program_title();
	
$li->Start_Trans();

// All ELE Component
$ELEKeyArr = array_keys($lpf->GET_ELE());
$ele_list = $lpf->GET_OLE_ELE_LIST();
$ELETitleArr = Get_Array_By_Key($ele_list, 'DefaultID');
$ELEIDArr = Get_Array_By_Key($CategoryInfoArr, 'RecordID');

// Step display
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$import_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
$import_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td>".$Lang['iPortfolio']['cat_name_eng']."</td><td>".$Lang['iPortfolio']['sub_cat_name_eng']."</td><td>".$Lang['iPortfolio']['program_name']."</td><td>".$Lang['iPortfolio']['old_compo']."</td></tr>\n";
$css_color = "#F3F3F3";

// Get data from temp 
$sql = "SELECT * FROM {$eclass_db}.TEMP_OLE_PROGRAM_IMPORT WHERE UserID = '$uploaderUserId'";				
$q_result = $li->returnArray($sql);

$rowcount = 0;
$numOfResult = sizeof($q_result);
for ($i=0; $i<$numOfResult; $i++)	//	This data get the data from import file
{		
	
	// All existing catagory
	$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false);
	// Catagory data
	$CategoryTitleArr = Get_Array_By_Key($CategoryInfoArr, 'EngTitle');
	$CategoryIDArr = Get_Array_By_Key($CategoryInfoArr, 'RecordID');
	
	list($tid, $uid, $row, $t_pro_name, $t_cat_name, $t_subcat_name, $t_ELE, $date) = $q_result[$i];
	
	$cat_exist = 0;
	$sub_cat_ids = 0; 
	
	// Add new category if requried
	if(!in_array($t_cat_name, $CategoryTitleArr, true)){
		$libCategory->SET_CLASS_VARIABLE('chi_title', $t_cat_name);
		$libCategory->SET_CLASS_VARIABLE('eng_title', $t_cat_name);
		$libCategory->SET_CLASS_VARIABLE('record_status', '1');
		$libCategory->ADD_CATEGORY();

		$cat_exist = $libCategory->GET_CLASS_VARIABLE('category_id');
						
	} else {
		$cat_exist=$CategoryIDArr[array_search($t_cat_name, $CategoryTitleArr)];
	}

	// Add new subcategory if requried
	if($t_subcat_name != NULL){
		
		$libSubCategory->SET_CLASS_VARIABLE('category_id', $cat_exist);
		$SubCategoryInfoArr = $libSubCategory->GET_SUBCATEGORY_LIST($ParShowInactive=false);
					
		if (!empty($SubCategoryInfoArr)){
			
			$SubCategoryTitleArr = Get_Array_By_Key($SubCategoryInfoArr, 'EngTitle');
			$SubCategoryIDArr = Get_Array_By_Key($SubCategoryInfoArr, 'SubCatID');

			if(!in_array($t_subcat_name, $SubCategoryTitleArr, true)){
							
				$libSubCategory->SET_CLASS_VARIABLE('chi_title', $t_subcat_name);
				$libSubCategory->SET_CLASS_VARIABLE('eng_title', $t_subcat_name);
				$libSubCategory->SET_CLASS_VARIABLE('record_status', '1');
	
				$libSubCategory->ADD_SUBCATEGORY();
				$sub_cat_ids = $libSubCategory->GET_CLASS_VARIABLE('subcategory_id');
							
			} else {
				$sub_cat_ids = $SubCategoryIDArr[array_search($t_subcat_name, $SubCategoryTitleArr)];
			}
						
		} else {
			$libSubCategory->SET_CLASS_VARIABLE('chi_title', $t_subcat_name);
			$libSubCategory->SET_CLASS_VARIABLE('eng_title', $t_subcat_name);
			$libSubCategory->SET_CLASS_VARIABLE('record_status', '1');
	
			$libSubCategory->ADD_SUBCATEGORY();
			$sub_cat_ids = $libSubCategory->GET_CLASS_VARIABLE('subcategory_id');
		}
	} else {
		$t_subcat_name = NULL;
	}
	
	// Handle empty subcategory
	if ($sub_cat_ids == 0){
		$sub_cat_ids = NULL;
	}
	
	// Add program 
	$libProgram->SET_CLASS_VARIABLE('category_id', $cat_exist);
	$libProgram->SET_CLASS_VARIABLE('sub_category_id', $sub_cat_ids);
	$libProgram->SET_CLASS_VARIABLE('chi_title', $li->Get_Safe_Sql_Query($t_pro_name));
	$libProgram->SET_CLASS_VARIABLE('eng_title', $li->Get_Safe_Sql_Query($t_pro_name));
	$libProgram->SET_CLASS_VARIABLE('record_status', '1');
	$libProgram->SET_CLASS_VARIABLE('component',explode(",",$t_ELE));
	$libProgram->ADD_PROGRAM_TITLE();
	
	$rowcount++;
	
	$import_table .= "<tr bgcolor='$css_color'><td class='tabletext'>".$t_cat_name."</td><td class='tabletext'>".$t_subcat_name."</td><td class='tabletext'>".$t_pro_name."</td><td class='tabletext'>".$t_ELE."</td></tr>\n";
}

// Error
if ($rowcount < count($q_result)) {
	$li->RollBack_Trans();
	header("Location: index_import.php");
	exit;
} else {
	$li->Commit_Trans();
}

function getTabMenuArr()
{
	global $iPort;
	$TabMenuArr = array();
	$TabMenuArr[] = array("ele.php", $iPort["ele"], 0);
	$TabMenuArr[] = array("category_index.php", $iPort["category"], 1);
	$TabMenuArr[] = array("tag.php", $iPort["tag"], 0);
	$TabMenuArr[] = array("activity_title_template.php", $iPort["activity_title_template"], 0);
	$TabMenuArr[] = array("activity_role_template.php", $iPort["activity_role_template"], 0);
	$TabMenuArr[] = array("student_submission_period.php", $iPort["student_submission_period"], 0);
	$TabMenuArr[] = array("record_approval.php", $iPort["record_approval"], 0);

	return $TabMenuArr;
}
?>
<?=$TabMenuDisplay?>
<table align="left" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_import?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
</table>
	<br/>
	<br />
	<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	<?= $import_table ?>
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<br/>
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<div align="center" style="padding-top:5px;">
	<?= $linterface->GET_ACTION_BTN($iPort["btn"]["ole_finish"], "button", "self.location='index.php'");  ?>
	<?= $linterface->GET_ACTION_BTN($iPort["btn"]["import_another"] , "button", "self.location='index_import.php'"); ?>
	</div>

	<input type="hidden" name="modes" value="1" >
	<input type="hidden" name="userfile" value="<?= $filepath ?>" >
	<input type="hidden" name="userfile_name" value="<?= $filename ?>" >
	
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>