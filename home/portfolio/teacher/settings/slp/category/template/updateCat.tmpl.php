<!-- Modifying By  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>

<script language="JavaScript">

$(document).ready( function() {	
	$('input#eng_title').focus();
});

function checkform()
{
	if (Trim($('input#eng_title').val()) == '')
	{
		alert('<?=$Lang['General']['JS_warning']['InputEnglishName']?>');
		$('input#eng_title').focus();
		return false;
	}
	
	if (Trim($('input#chi_title').val()) == '')
	{
		alert('<?=$Lang['General']['JS_warning']['InputChineseName']?>');
		$('input#chi_title').focus();
		return false;
	}
	
	$('form#form1').submit();
}

</script>

</head>

<body>
<form id="form1" name="form1" action="index.php" method="post">
<div class="edit_pop_board edit_pop_board_simple">
  <h1><span><?=$iPort["new"].$iPort["category"]?></span></h1>
  <div class="edit_pop_board_write">
    <table class="form_table inside_form_table">
      <col class="field_title" />
      <col  class="field_c" />
      <tr>
        <td><span class='tabletextrequire'>*</span> <?=$ec_iPortfolio['SLP']['Name'].": (".$ec_iPortfolio['lang_eng'].")"?></td>
        <td>:</td>
        <td><?=$CatEngNameInput?></td>
      </tr>
      <tr>
        <td><span class='tabletextrequire'>*</span> <?=$ec_iPortfolio['SLP']['Name'].": (".$ec_iPortfolio['lang_chi'].")"?></td>
        <td>:</td>
        <td><?=$CatChiNameInput?></td>
      </tr>
      <tr>
        <td><span class='tabletextrequire'>*</span> <?=$ec_iPortfolio['status']?></td>
        <td>:</td>
        <td><?=$CatStatusInput?></td>
      </tr>
    </table>
  </div>
  <div class="edit_bottom">
  	<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span>
    <p class="spacer"></p>
    <input type="button" class="formbutton" onclick="checkform()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
    <input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
    <p class="spacer"></p>
  </div>
</div>
<input type="hidden" name="task" value="updateCat" />
<input type="hidden" name="category_id" value="<?=$CatID?>" />
</form>
</body>
</html>