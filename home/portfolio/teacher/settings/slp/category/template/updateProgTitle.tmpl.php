<!-- Modifying By  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/script.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/lang/script.<?=$intranet_session_language?>.js"></script>

<script language="JavaScript">

function getSubCat(){
  var cat_id = $("[name=category_id]").val();

  $.ajax({
    type: "POST",
    url: "index.php",
    data: "task=ajax&script=get_subcat&CatID="+cat_id,
    dataType: "json",
    success: function(data){
      $("[name=subcategory_id]").children().remove().end().append($("<option></option>").val("").html("--"));
      
      $.each(data, function(){
        $("[name=subcategory_id]").append( 
          $("<option></option>").val(this.SubCatID).html(<?=Get_Lang_Selection("this.ChiTitle", "this.EngTitle")?>)
        ); 
      });
    }
  });
}

function checkform(formObj)
{
  if($("[name=category_id]").val() == "")
  {
    alert(globalAlertMsg18);
    $("[name=category_id]").focus();
    return false;
  }

  return true;  
}

</script>

</head>

<body>
<form name="form1" action="index.php" method="post" onSubmit="return checkform(this)">
<div class="edit_pop_board edit_pop_board_simple" style="height:100%">
  <h1><span><?=$ec_iPortfolio['NewPresetProgrammeName']?></span></h1>
  <div class="edit_pop_board_write" style="height:100%">
    <table class="form_table inside_form_table">
      <col class="field_title" />
      <col  class="field_c" />
      <tr>
        <td><?=$ec_iPortfolio['SLP']['Name']?></td>
        <td>:</td>
        <td><?=$ProgTitleEngNameInput?></td>
      </tr>
      <tr>
        <td><?=$ec_iPortfolio['category']?></td>
        <td>:</td>
        <td><?=$ProgTitleCatInput?></td>
      </tr>
      <tr>
        <td><?=$ec_iPortfolio['sub_category']?></td>
        <td>:</td>
        <td><?=$ProgTitleSubCatInput?></td>
      </tr>
      <tr>
        <td><?=$ec_iPortfolio['status']?></td>
        <td>:</td>
        <td><?=$ProgTitleStatusInput?></td>
      </tr>
      <tr>
        <td><?=$iPort["ele"]?></td>
        <td>:</td>
        <td><?=$ProgTitleComponentInput?></td>
      </tr>
    </table>
  </div>
  <div class="edit_bottom">
    <p class="spacer"></p>
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
    <input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
    <p class="spacer"></p>
  </div>
</div>
<input type="hidden" name="task" value="updateProgTitle" />
<input type="hidden" name="prog_title_id" value="<?=$ProgTitleID?>" />
<?=$ProgTitleChiNameInput?>
</form>
</body>
</html>