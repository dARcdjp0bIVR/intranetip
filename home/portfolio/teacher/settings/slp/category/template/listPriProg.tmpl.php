<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
function index_reload_page(msg){
  this.tb_remove();     //Close thickbox
  window.location.reload();      //Refresh page
}
function edit_category(obj,cat_id){
  //tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");
  tb_show("","index.php?task=editCat&category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");
}
function edit_prog_title(obj,element){
  if(countChecked(obj,element)==1) {
    var prog_title_obj = document.getElementsByName(element);
    
    for(var i=0; i<prog_title_obj.length; i++)
    {
      if(prog_title_obj[i].checked)
      {
        var prog_title_id = prog_title_obj[i].value;
        break;
      }
    }
  
    //tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");tb_show("","task/editCat.php?category_id="+cat_id+"&KeepThis=true&TB_iframe=true&height=200&width=500","");
    tb_show("","index.php?task=editProgTitle&ProgTitleID="+prog_title_id+"&KeepThis=true&TB_iframe=true&height=347&width=500","");
  } else {
    alert(globalAlertMsg1);
  }
}
function change_progtitle_status(obj,element,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
    var msg = (status == 'active') ? globalAlertActivate : globalAlertMsg5;
  
    if(confirm(msg)){
      obj.status.value = status;
      obj.task.value = 'changeProgTitleStatus';
      obj.submit();
    }
  }
}
function delete_prog_title(obj,element){
  if(countChecked(obj,element)==0){
    alert(globalAlertMsg2);
  }
  else{
    if(confirm(globalAlertMsg3)){	            
      obj.task.value = 'deleteProgTitle';                
      obj.submit();				             
    }
  }
}
function move_prog_title(obj,element){
  if(countChecked(obj,element)==0){
    alert(globalAlertMsg2);
  }
  else{
    tb_show("","index.php?task=moveProgTitle&CatID=<?=$CatID?>&KeepThis=true&TB_iframe=true&height=200&width=500","");				             
  }
}
</script>
<form name="form1" id="form1" action="index.php" method="post">
<!--###### Content Board Start ######-->
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="bottom" background="images/2007a/content_02_bg.gif">
        <?=$TabMenuDisplay?>
        <!-- ###### Tabs End ######-->
      </td>
    </tr>
    <tr> 
      <td class="main_content">
        <div class="navigation">
          <?=$PageNavigationDisplay?>
        </div>
        <div class="detail_title_box"><span><?=$ec_iPortfolio['title'].": (".$ec_iPortfolio['lang_eng'].")"?>   <strong><?=$CatEngName?></strong>&nbsp;&nbsp;&nbsp;&nbsp;</span> <span>&nbsp;&nbsp;&nbsp;&nbsp;<?="(".$ec_iPortfolio['lang_chi'].")"?>  <strong><?=$CatChiName?></strong></span><span class="table_row_tool"><a href="javascript:edit_category(document.form1,<?=$CatID?>)" class="edit_dim" title="Edit Category"></a></span><br style="clear:both" />
          <span><?=$ec_iPortfolio['status']?> :     <strong><?=$CatRecStatus?></strong></span><!--<span class="table_row_tool"><a href="#" class="edit_dim" title="Edit Access Right"></a></span>--><br style="clear:both" />
        </div>
        <br />
        <div class="content_top_tool">
          <div class="Conntent_tool"><a href="index.php?task=newProgTitle&CatID=<?=$CatID?>&KeepThis=true&TB_iframe=true&height=347&width=500" class="new thickbox"> <?=$ec_iPortfolio['NewPresetProgrammeName']?></a></div>
          <div class="Conntent_search">
          	<input name="prog_searching_keywords" type="text" value="<?= $prog_searching_keywords ?>"/>
          </div>
          <br style="clear:both" />
        </div>
        <div class="table_board">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr class="table-action-bar">
              <td valign="bottom"><div class="table_filter"><?=$disp_type_select_html?></div></td>
              <td valign="bottom"><div class="common_table_tool">
                <a href="javascript:change_progtitle_status(document.form1,'record_id[]','deactive')" class="tool_reject"><?=$ec_iPortfolio['deactive']?></a>
                <a href="javascript:change_progtitle_status(document.form1,'record_id[]','active')" class="tool_approve"><?=$ec_iPortfolio['activate']?></a>
                <a href="javascript:move_prog_title(document.form1,'record_id[]')" class="tool_move"><?=$ec_iPortfolio['Categorize']?></a>
                <a href="javascript:edit_prog_title(document.form1,'record_id[]')" class="tool_edit" title="Edit"><?=$ec_iPortfolio['edit']?></a>
                <a href="javascript:delete_prog_title(document.form1,'record_id[]')" class="tool_delete" title="Delete"><?=$ec_iPortfolio['delete']?></a>
              </div></td>
            </tr>
          </table>
          <?=$ProgTitleListTable?>
        </div>
      </td>
    </tr>
  </table>
<!--###### Content Board End ######-->
<input type="hidden" name="order" value=<?=$order?> />
<input type="hidden" name="pageNo" value=<?=$li->pageNo?> />
<input type="hidden" name="field" value=<?=$field?> />
<input type="hidden" name="page_size_change" value=<?=$page_size_change?> />
<input type="hidden" name="numPerPage" value=<?=$li->page_size?> />

<input type="hidden" name="task" value="listPriProg" />
<input type="hidden" name="from" value="listPriProg" />
<input type="hidden" name="status" />
<input type="hidden" name="CatID" value=<?=$CatID?> />
</form>