<?php

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libdbtable2007($field, $order, $pageNo);

$keywords = null;
if($cat_searching_keywords != NULL){
	$keywords = "WHERE oc.EngTitle LIKE '%".$cat_searching_keywords."%'";
}

$sql =  "
          SELECT
            CONCAT('<a href=\"index.php?task=listPriProg&CatID=', oc.RecordID, '\" class=\"tablelink\">', oc.EngTitle, '</a>'),
            CONCAT('<a href=\"index.php?task=listPriProg&CatID=', oc.RecordID, '\" class=\"tablelink\">', oc.ChiTitle, '</a>'),
			oc.RecordID,
            IF(osc.subCatCount IS NULL, '0', osc.subCatCount) AS subCatCount,
            IF(opt.progTitleCount IS NULL, '0', opt.progTitleCount) AS progTitleCount,
            CASE oc.RecordStatus
              WHEN 0 THEN '{$button_private}'
              WHEN 1 THEN '{$button_public}'
              WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
              WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
            END,
            DATE_FORMAT(oc.ModifiedDate, '%Y-%m-%d') AS ModifiedDate,
            CONCAT('<input type=\"checkbox\" name=\"record_id[]\" value=\"', oc.RecordID, '\">')
          FROM
            {$eclass_db}.OLE_CATEGORY AS oc
          LEFT JOIN
            (
              SELECT
                CatID,
                count(*) AS subCatCount
              FROM
                {$eclass_db}.OLE_SUBCATEGORY
              GROUP BY
                CatID
            ) AS osc
          ON
            oc.RecordID = osc.CatID
          LEFT JOIN
            (
              SELECT
                CategoryID,
                count(*) AS progTitleCount
              FROM
                {$eclass_db}.OLE_PROGRAM_TITLE
              GROUP BY
                CategoryID
            ) AS opt
          ON
            oc.RecordID = opt.CategoryID $keywords
        ";

$LibTable->sql = $sql;
$LibTable->field_array = array("oc.EngTitle", "oc.ChiTitle", "oc.RecordID", "subCatCount", "progTitleCount", "oc.RecordStatus", "oc.ModifiedDate");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
// if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 9;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_alt = array("", "");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 3, 3, 0, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$ec_iPortfolio['SLP']['Name']." (".$ec_iPortfolio['lang_eng'].")")."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['SLP']['Name']." (".$ec_iPortfolio['lang_chi'].")")."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(2,$ec_iPortfolio['category_code'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(3,$ec_iPortfolio['NoOfSubcategory'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(4,$ec_iPortfolio['NoOfPresetProgrammeName'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(5,$ec_iPortfolio['status'])."</th>\n";
$LibTable->column_list .= "<th nowrap>".$LibTable->column(6,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->check("record_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['category'], "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);

ob_start();
$LibTable->displayPlain();
$CategoryListTable = ob_get_contents();
ob_end_clean();
$CategoryListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $CategoryListTable .= "<tr class='tablebottom'>";
  $CategoryListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $CategoryListTable .= $LibTable->navigation(1);
  $CategoryListTable .= "</td>";
  $CategoryListTable .= "</tr>";
}
$CategoryListTable .= "</table>";

$linterface->LAYOUT_START();
include_once("template/listCat.tmpl.php");
$linterface->LAYOUT_STOP();
?>