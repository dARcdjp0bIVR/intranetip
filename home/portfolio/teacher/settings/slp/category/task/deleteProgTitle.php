<?php

$lpf_prog_title = new libpf_program_title();
$lpf_prog_title->DELETE_PROGRAM_TITLE($record_id);

$from = trim($from);
switch ($from) {
	case "listPriProg":
		$redirectLocation = "index.php?task=listPriProg&CatID=".$CatID;
		break;
	case "listProg":
		$redirectLocation = "index.php?task=listProg&SubCatID=".$SubCatID;
		break;
	default:
		break;
}

header("Location: ".$redirectLocation);

?>