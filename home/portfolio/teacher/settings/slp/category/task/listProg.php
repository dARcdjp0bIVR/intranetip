<?php
## Modifying By: 
$lpf_sub_cat = new subCategory();
$lpf_sub_cat->SET_CLASS_VARIABLE("subcategory_id", $SubCatID);
$lpf_sub_cat->SET_SUBCATEGORY_PROPERTY();
$CatID = $lpf_sub_cat->GET_CLASS_VARIABLE("category_id");
$SubCatChiName = $lpf_sub_cat->GET_CLASS_VARIABLE("chi_title");
$SubCatEngName = $lpf_sub_cat->GET_CLASS_VARIABLE("eng_title");
$t_SubCatRecStatus = $lpf_sub_cat->GET_CLASS_VARIABLE("record_status");
switch($t_SubCatRecStatus)
{
  case 0:
    $SubCatRecStatus = $button_private;
    break;
  case 1:
    $SubCatRecStatus = $button_public;
    break;
  case 2:
    $SubCatRecStatus = $ec_iPortfolio['default']." - ".$button_public;
    break;
  case 3:
    $SubCatRecStatus = $ec_iPortfolio['default']." - ".$button_private;
    break;
}

$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE("category_id", $CatID);
$lpf_cat->SET_CATEGORY_PROPERTY();
$CatChiName = $lpf_cat->GET_CLASS_VARIABLE("chi_title");
$CatEngName = $lpf_cat->GET_CLASS_VARIABLE("eng_title");

//get the list

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libdbtable2007($field, $order, $pageNo);

$sql =  "
          SELECT
            EngTitle,
            IF(ELE IS NULL OR ELE = '', '--', ELE) AS ELE,
            CASE RecordStatus
              WHEN 0 THEN '{$button_private}'
              WHEN 1 THEN '{$button_public}'
              WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
              WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
            END,
            DATE_FORMAT(ModifiedDate, '%Y-%m-%d') AS ModifiedDate,
            CONCAT('<input type=\"checkbox\" name=\"record_id[]\" value=\"', ProgramTitleID, '\">')
          FROM
            {$eclass_db}.OLE_PROGRAM_TITLE
          WHERE
            SubCategoryID = '".$SubCatID."'
        ";

$LibTable->sql = $sql;
$LibTable->field_array = array("EngTitle", "RecordStatus", "ModifiedDate");
$LibTable->db = $intranet_db;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 6;
$LibTable->noNumber = false;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 0, 0, 0, 0);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<th class='num_check'>#</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(0,$ec_iPortfolio['title'])."</th>\n";
$LibTable->column_list .= "<th>" . $iPort["component"] . "</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(1,$ec_iPortfolio['status'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->column(2,$ec_iPortfolio['SAMS_last_update'])."</th>\n";
$LibTable->column_list .= "<th>".$LibTable->check("record_id[]")."</th>\n";
$LibTable->column_list .= "</tr>\n";	

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['Category'], "index.php?task=listCat");
$MenuArr[] = array($CatChiName." (".$CatEngName.")", "index.php?task=listSubCat&CatID=".$CatID);
$MenuArr[] = array($SubCatChiName." (".$SubCatEngName.")", "");
$PageNavigationDisplay = $linterface->GET_NAVIGATION($MenuArr);

ob_start();
$LibTable->displayPlain();
$ProgTitleListTable = ob_get_contents();
ob_end_clean();
$ProgTitleListTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
if ($LibTable->navigationHTML!="") {
  $ProgTitleListTable .= "<tr class='tablebottom'>";
  $ProgTitleListTable .= "<td  class=\"tabletext\" align=\"right\">";
  $ProgTitleListTable .= $LibTable->navigation(1);
  $ProgTitleListTable .= "</td>";
  $ProgTitleListTable .= "</tr>";
}
$ProgTitleListTable .= "</table>";

$linterface->LAYOUT_START();
include_once("template/listProg.tmpl.php");
$linterface->LAYOUT_STOP();
?>