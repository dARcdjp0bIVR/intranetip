<?php

$lpf_cat = new Category();
$lpf_cat->SET_CLASS_VARIABLE('category_id', $category_id);
$lpf_cat->SET_CATEGORY_PROPERTY();

$CatID = $category_id;
$CatChiName = $lpf_cat->GET_CLASS_VARIABLE("chi_title");
$CatEngName = $lpf_cat->GET_CLASS_VARIABLE("eng_title");
$CatRecStatus = $lpf_cat->GET_CLASS_VARIABLE("record_status");

if($CatRecStatus == 2 || $CatRecStatus == 3)
{
  $CatEngNameInput = $CatEngName."<input name=\"eng_title\" type=\"hidden\" value=\"".$CatEngName."\" id=\"eng_title\" class=\"textbox\" />";
  $CatChiNameInput = $CatChiName."<input name=\"chi_title\" type=\"hidden\" value=\"".$CatChiName."\" id=\"chi_title\" class=\"textbox\" />";
  
  $CatStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"2\" ".($CatRecStatus==2?"checked=\"checked\"":"")." /> <label for=\"status_active\">" . $ec_iPortfolio['Active'] . "</label>";
  $CatStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"3\" ".($CatRecStatus==3?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">" . $ec_iPortfolio['Suspend'] . "</label>";
}
else
{
  $CatEngNameInput = "<input name=\"eng_title\" type=\"text\" value=\"".$CatEngName."\" id=\"eng_title\" class=\"textbox\"/>";
  $CatChiNameInput = "<input name=\"chi_title\" type=\"text\" value=\"".$CatChiName."\" id=\"chi_title\" class=\"textbox\"/>";
  
  $CatStatusInput = "<input type=\"radio\" name=\"record_status\" id=\"status_active\" value=\"1\" ".($CatRecStatus==1?"checked=\"checked\"":"")." /> <label for=\"status_active\">" . $ec_iPortfolio['Active'] . "</label>";
  $CatStatusInput .= "<input type=\"radio\" name=\"record_status\" id=\"status_suspend\" value=\"0\" ".($CatRecStatus==0?"checked=\"checked\"":"")." /> <label for=\"status_suspend\">" . $ec_iPortfolio['Suspend'] . "</label>";
}


include_once("template/updateCat.tmpl.php");
?>