<?php

$lpf_mgmt_group = new libpf_mgmt_group();
$lpf_mgmt_group->setGroupID($group_id);
$lpf_mgmt_group->setGroupProperty();

$BoxTitle = $button_edit;
$GroupTitle = $lpf_mgmt_group->getTitle();
$GroupTitle = htmlentities($GroupTitle, ENT_COMPAT, "UTF-8");
$GroupID = $lpf_mgmt_group->getGroupID();

$GroupNameInput = "<input name=\"title\" type=\"text\" id=\"title\" value=\"{$GroupTitle}\" class=\"textbox\"/>";

include_once("template/updateGroup.tmpl.php");
?>