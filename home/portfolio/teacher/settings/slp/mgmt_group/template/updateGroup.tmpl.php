<!-- Modifying By  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Text</title>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">

<script language="JavaScript">

function checkform(formObj)
{
  var title = document.getElementById("title");
	
	if(title.value == "")
	{
		alert("<?=$Lang['Group']['EmptyGroupName']?>");
		title.focus();
		return false;
	}

	return true;  
}

</script>

</head>

<body>
<form name="form1" action="index.php" method="post" onSubmit="return checkform(this)">
<div class="edit_pop_board edit_pop_board_simple">
  <div class="edit_pop_board_write">
    <table class="form_table inside_form_table">
      <col class="field_title" />
      <col  class="field_c" />
      <tr>
        <td><?=$ec_iPortfolio['SLP']['Name']?></td>
        <td>:</td>
        <td><?=$GroupNameInput?></td>
      </tr>
    </table>
  </div>
  <div class="edit_bottom">
    <p class="spacer"></p>
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" />
    <input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" />
    <p class="spacer"></p>
  </div>
</div>
<input type="hidden" name="task" value="updateGroup" />
<input type="hidden" name="group_id" value="<?=$GroupID?>" />
</form>
</body>
</html>