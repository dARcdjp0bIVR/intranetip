<?php
## Using By :  
##############################################
##	Modification Log:
##	2016-05-20 Omas
##	- removed empty object $LibTable  
##
##	2010-02-18 Max (201002180941)
##	- Get the tag menu from includes/portfolio25/libpf-tabmenu.php instead of slpsettingcommonfunction.php

##	2010-01-20 Max(201001201615)
##	- Generate the Tag Menu By function call 
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Settings_OLE";
//$CurrentPageName = $iPort['menu']['ole']." ".$iPort['menu']['settings'];

$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

############  Cotnent Here ######################
// page number settings
if ($order=="") $order=0;
if ($field=="") $field=2;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

# Get default ELE
$sql =	"
					SELECT
						RecordID
					FROM
						{$eclass_db}.OLE_ELE
					WHERE 
						RecordStatus = 2 OR RecordStatus = 3
				";
$DefaultELE = $lpf->returnVector($sql);

// original query , default ele cannot deactivate
/*
$sql = "SELECT
			EngTitle,
			ChiTitle,
			if(RecordStatus=2, '{$ec_iPortfolio['default']}', if(RecordStatus=0, '{$button_private}', '{$button_public}')),
            ModifiedDate,
          	if(RecordStatus=2, '<img src=\"".$image_path."/red_checkbox.gif\" />', CONCAT('<input type=checkbox name=\"RecordID[]\" value=', RecordID ,'>'))
        FROM 
        	{$eclass_db}.OLE_ELE
		";
*/

// new query, default ele can deactivate
$sql = "SELECT
			EngTitle,
			ChiTitle,
			CASE RecordStatus
				WHEN 0 THEN '{$button_private}'
				WHEN 1 THEN '{$button_public}'
				WHEN 2 THEN '{$ec_iPortfolio['default']} - {$button_public}'
				WHEN 3 THEN '{$ec_iPortfolio['default']} - {$button_private}'
			END,
            ModifiedDate,
          	CONCAT('<input type=checkbox name=\"RecordID[]\" value=', RecordID ,'>')
        FROM 
        	{$eclass_db}.OLE_ELE
		";

$li = new libpf_dbtable($field, $order, $pageNo);
$li->field_array = array("EngTitle", "ChiTitle", "RecordStatus", "ModifiedDate");
$li->sql = $sql;
$li->no_col = 6;
$li->table_tag = "<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
//$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_alt = array("", "");
$li->row_height = 25;
$li->plain_column = 1;
$li->plain_column_css = "tablelink";
// empty object $LibTable ? 
// $LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
//$li->record_num_mode = "iportfolio"; // for school based scheme total record display
//$li->IsColOff = 2;
$li->title = $iPort['table']['records'];

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$ec_iPortfolio['SLP']['Name']." (".$ec_iPortfolio['lang_eng'].")", 1)."</td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(1,$ec_iPortfolio['SLP']['Name']." (".$ec_iPortfolio['lang_chi'].")", 1)."</td>\n";
$li->column_list .= "<td width='10%' nowrap='nowrap'>".$li->column(2,$ec_iPortfolio['status'], 1)."</td>\n";
$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(3,$ec_iPortfolio['SAMS_last_update'], 1)."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";
$li->column_array = array(10,10,0,0,0,0);


// Tab Menu Settings
//*** current page index*****
// 0 -   * Components of OLE
// 1 -   * Programme Category
// 2 -   * Management Group
// 3 -   * Preset Participation Role
// 4 -   * Submission Period
// 5 -   * Record Approval
$currentPageIndex = 0;
$TabMenuArr = libpf_tabmenu::getSlpSettingsTags(IPF_CFG_SLP_SETTING_OLE, $currentPageIndex);


// Table Action Button (JS, Image, Image_name, title)
$TableActionArr = array();
$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=activate', 1, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_approve.gif", "imgPublic", $ec_iPortfolio['activate'], "tool_approve");
$TableActionArr[] = array("javascript:jCheckPick(document.form1,'RecordID[]','update_ele.php?Action=deactivate', 2, 1)", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_reject.gif", "imgPrivate", $ec_iPortfolio['deactive'], "tool_reject");
$TableActionArr[] = array("javascript:if(jCHECK_OP('".$ec_warning["remove_default"]."')) checkRemove(document.form1,'RecordID[]','update_ele.php?Action=remove')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif", "imgDelete", $ec_iPortfolio['delete'], "tool_delete");
$TableActionArr[] = array("javascript:checkEdit(document.form1,'RecordID[]','edit_ele.php')", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_edit.gif", "imgEdit", $ec_iPortfolio['edit'], "tool_edit");

##################################################


### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpSettingsTopTags(IPF_CFG_SLP_SETTING_OLE);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
	globalAlertDeactivate = '<?=$iPort["msg"]["confirm_deactivate"]?>';	
	defaultELE = new Array(<?=implode(',',$DefaultELE)?>)
	
	function jCHECK_OP(jParErrorMsg)
	{
		OP_allow = true;
		for(i=0; i<document.form1.elements.length; i++)
		{
			if(document.form1.elements[i].name == "RecordID[]" && document.form1.elements[i].checked)
			{
				for(j=0; j<defaultELE.length; j++)
				{
					if(document.form1.elements[i].value == defaultELE[j])
					{
						OP_allow = false;
						break;
					}
				}
			}
			
			if(!OP_allow)
			{
				alert(jParErrorMsg);
				break;
			}
		}			
		
		return OP_allow;
	}

</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- Content Here -->
<?
echo "<form name=\"form1\" id=\"form1\" action=\"ele.php\" method=\"post\">";
echo $lpf->GET_TAB_MENU($TabMenuArr);
echo $linterface->GET_SYS_MSG($msg);
echo $lpf->GET_NEW_BUTTON("new_ele.php", $iPort["new"]);
echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
echo "<tr><td align=\"right\" valign=\"bottom\">";
echo $lpf->GET_TABLE_ACTION_BTN($TableActionArr);
echo "</td></tr></table>";

echo $li->displayPlain();
//$li->display();

if ($li->navigationHTML!="")
{
	echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	echo "<tr class='tablebottom'>";
	echo "<td class=\"tabletext\" align=\"right\">";
	echo $li->navigationHTML;
	echo "</td>";
	echo "</tr>";
	echo "</table>";
}

echo "<input type=\"hidden\" name=\"order\" value=\"".$li->order."\" />";
echo "<input type=\"hidden\" name=\"pageNo\" value=\"".$li->pageNo."\" />";
echo "<input type=\"hidden\" name=\"field\" value=\"".$li->field."\" />";
echo "<input type=\"hidden\" name=\"page_size_change\" value=\"".$page_size_change."\" />";
echo "<input type=\"hidden\" name=\"numPerPage\" value=\"".$li->page_size."\" />";
echo "</form>";
?>
<!-- ENd Content Here -->
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
