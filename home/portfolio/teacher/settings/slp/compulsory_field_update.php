<?php
## Using By : 
##############################################
##	Modification Log:
## 	2015-10-19 Omas
## -create this page
##
##############################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");

intranet_auth();
intranet_opendb();

// set the current page title
$CurrentPage = "Settings_OLE";

# Get all period settings from DB
//$objIpfSetting = iportfolio_settings::getInstance();
$objIpfSetting = new iportfolio_settings();

$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

//save setting
$settingName = $ipf_cfg["Student_OLE_CompulsoryFields"]["SettingName"];
$delimeter = ',';
$settingValue = implode($delimeter,(array)$_POST['Student_OLE_Compulsory']);
$objIpfSetting->updateSetting($settingName,$settingValue);

$settingName = $ipf_cfg["Student_OLEExt_CompulsoryFields"]["SettingName"];
$delimeter = ',';
$settingValue = implode($delimeter,(array)$_POST['Student_OLEExt_Compulsory']);
$objIpfSetting->updateSetting($settingName,$settingValue);

header("Location: compulsory_field.php?msg=$msg");
intranet_closedb();
?>
