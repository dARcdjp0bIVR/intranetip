<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:PeerReview") || !strstr($ck_user_rights, ":web:")); Editied by Eva

intranet_auth();
intranet_opendb();

$lpf = new libportfolio();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$lp_config_file = "$eclass_root/files/lp_config.txt";
$li_fs = new libfilesystem();

$config_array = unserialize($li_fs->file_read($lp_config_file));
$config_array["disable_reset"] = empty($disable_reset) ? 0 : 1;
$config_array["disable_facebook"] = (get_client_region()=='zh_CN')?0:(empty($disable_facebook) ? 0 : 1);
$li_fs->file_write(serialize($config_array), $lp_config_file);

intranet_closedb();
header("Location: learning_portfolio_setting.php?msg=update");
?>