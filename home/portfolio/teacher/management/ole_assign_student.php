<?php
// Modifing by 
####################
# Modification Log:
#	Date:	2017-07-11	(Villa)
#	Improved: no userid can be selected twice to the selected box
#
#	Date:	2016-12-22	(Villa) - N110026 
#	Improved: change the autocomplete to search
#
#	Date:	2016-03-21	(Omas) - E94129 
#	Improved: support searching quited student (follow manual add logic)
#
#	Date:	2015-05-26	(Omas)	
#	Improved: autocomplete div will being blocked by the input field by add left:162 to statescontainer
#
#	Date:	2015-04-14	(Omas)	
# 	Fixed: #define yui array part if student name have special character, seach is not work
#
####################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();


// Initializing classes
//$LibUser = new libuser($UserID);
$LibPortfolio = new libportfolio2007();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $LibPortfolio->GET_TAB_MENU($TabMenuArr);


$student = $_POST['student'];
$flag = $_POST['flag'];
$targetClass = $_POST['targetClass'];
$targetNum = $_POST['targetNum'];
$student_login = $_POST['student_login'];

# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name='targetClass' onChange='changeClass(this.form)'", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Number
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber, CONCAT(ClassNumber, ' (', ".getNamefieldByLang().", ')') FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $classnum = $lc->returnArray($sql, 2);
    $select_classnum = $linterface->GET_SELECTION_BOX($classnum, "name='targetNum' ","",$targetNum);
    $select_classnum .= $linterface->GET_BTN($button_add, "button", "javascript:addByClassNum()");
}

if ($flag == 1 && $student_login != '')           # Login
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND (UserLogin = '$student_login' OR CardID = '$student_login')";
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    # sort by class and class number
    $sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list) ORDER BY ClassName ASC, ClassNumber ASC";
    $array_students = $lc->returnArray($sql,2);
}

$student_selected = $linterface->GET_SELECTION_BOX($array_students, "name='student[]' ID='student[]' class='select_studentlist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");

### start for searching ###
#get required data
$data_ary = array();
$sql = "SELECT YearClassID,ClassTitleEN FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."' ORDER BY ClassTitleEN";
$result = $lc->returnArray($sql,2);
for ($i = 0; $i < sizeof($result); $i++) 
{
	list($this_classid, $this_classname) = $result[$i];
	
	$name_field = getNameFieldByLang();
// #E94129 
// 	$sql1 = "SELECT UserID, $name_field, ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$this_classname."' ORDER BY ClassNumber";
	$sql1 = "SELECT UserID, CONCAT( $name_field, IF(RecordStatus=1,'',IF(RecordStatus=3,' [".$Lang['Status']['Left']."]', ' [".$Lang['Status']['Suspend']."]')) ), ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '".$this_classname."' ORDER BY ClassNumber";
	$result1 = $lc->returnArray($sql1,4);
	for ($j = 0; $j < sizeof($result1); $j++) 
	{
		list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
		$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
	}
}

if(!empty($data_ary))
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;

		//($i == 0) ? $liList = "<li class=\"\" style=\"display: none;\">". $temp_str ."</li>\n" : $liList = "<li style=\"display: none;\">". $temp_str ."</li>\n";
		$liArr .= "[\"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str) ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_userid ."\"]";
		//($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
		//$liArr .= ",\n";
		($i == (sizeof($data_ary)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
//echo $liArr;
	foreach ($data_ary as $key => $row)
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);

	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;

		$liArr2 .= "[\"". $this_userlogin ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($data_ary)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}


########################################################
# Layout Display
########################################################
$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "ole.php?IntExt=".$IntExt);
$MenuArr[] = array($Lang['iPortfolio']['OLE']['NewStudent'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

$STEPS_OBJ[] = array($i_Discipline_System_Select_Student, 1);
$STEPS_OBJ[] = array($i_Discipline_System_Add_Record, 0);

$linterface->LAYOUT_START();

?>

<script src="<?= $PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="<?= $PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">
$().ready(function(){
		$('#search1').autocomplete(
			"ole_assign_student_ajax_autocomplete.php",
			 {
				onItemSelect: function(li) {
					Add_Selected_User(li.extra[1], li.selectValue,'search1');
				},
// 					formatItem: function(row) {
// 						//alert(row);
// 						return row[0];
// 					},
				delay:0,
				maxItemsToShow: 30,
				minChars:2,
				matchContains:1,
				extraParams: {'ajaxFieldName':'ClassName'},
				autoFill:false,
				overflow_y: 'auto',
				overflow_x: 'hidden',
				maxHeight: '200px'
			}
		);
		$('#search2').autocomplete(
			"ole_assign_student_ajax_autocomplete.php",
			 {
				onItemSelect: function(li) {
					Add_Selected_User(li.extra[1], li.selectValue,'search2');
				},
				delay:0,
				maxItemsToShow: 30,
				minChars:1,
				matchContains:1,
				extraParams: {'ajaxFieldName':'UserLogin'},
				autoFill:false,
				overflow_y: 'auto',
				overflow_x: 'hidden',
				maxHeight: '200px'
			}
		);
});

function Add_Selected_User(UserID, UserName, SelectID){
	UserID = UserID.replace("U","");
	var studentIDArr = [];
	$('select[name="student[]"] option').each(function(){
		studentIDArr.push( $(this).val() );
	});
	if(studentIDArr.indexOf(UserID)=='-1'){
		$('select[name="student[]"]').append("<option value="+ UserID +">"+ UserName +"</option>");
	}
	$('#'+SelectID).val('');
	
}

function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'ole_student_new2.php?IntExt=<?=$IntExt?>';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
    obj = document.form1;
    
    if (obj.flag.value==1)
    {
        if (addByLogin())
        {
	    	obj.submit();    
        }
        else
        {
	        return false;
        }
    }

        if(obj.elements["student[]"].length != 0)
        {
            if (formSubmit(obj))
            {
            	obj.submit();
        	}
        	else
        	{
	        	return false;
        	}            	
		}
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
}

</SCRIPT>

<form name="form1" method="POST" enctype="multipart/form-data" action="ole_student_new2.php?IntExt=<?=$IntExt?>" >

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?=$tab_menu_html?></td>
	</tr>
	<tr>
		<td class="navigation"><?=$navigation_html?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="tabletext" width="40%"><?=$i_general_choose_student?></td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td class="tabletext" width="60%"><?=$i_general_selected_students; ?> <span class="tabletextrequire">*</span></td>
		</tr>
	      	  
		<tr>
			<td class="tablerow2" valign="top">
			<table width="100%" border="0" cellpadding="3" cellspacing="0">
			<tr>
				<td class="tabletext"><?=$i_general_from_class?></td>
			</tr>
			<tr>
				<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('choose/index.php?fieldname=student[]', 16)")?></td>
			</tr>
			<tr>
				<td class="tabletext"><i><?=$i_general_or?></i></td>
			</tr>
			<tr>
				<td class="tabletext">
				<?=$i_general_search_by_inputformat?><br \>
				<div id="statesautocomplete">
					<input type="text" class="tabletext" name="search1" ID="search1">
				</div>
				</td>
			</tr>					
			<tr>
				<td class="tabletext"><i><?=$i_general_or?></i></td>
			</tr>
			<tr>
				<td class="tabletext">
				<?=$i_general_search_by_loginid?><br \>
				<div id="statesautocomplete">
					<input type="text" class="tabletext" name="search2" ID="search2">
				</div>
				</td>
			</tr>			
			</table>	
			
			</td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td align="left" valign="top">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td align="left"><?= $student_selected ?></td>
			</tr>
			<tr>
				<td align="right"><?=$button_remove_html?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	    </td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	    <tr>
	    	<td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td>
	    </tr>
	    <tr>
	    	<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	    </tr>
	    <tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button", "this.form.flag.value=3;checkForm()") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
			</td>
	    </tr>
		</table>
		</td>
	</tr>
</table>

<br />
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="type" value="<?=$type?>" />
<input type="hidden" name="EventID" value="<?=$EventID?>" />

</form>
<?php
    $linterface->LAYOUT_STOP();
?>
