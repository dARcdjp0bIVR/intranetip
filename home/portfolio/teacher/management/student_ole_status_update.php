<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$lpf_mgmt_grp = new libpf_mgmt_group();
$LibPortfolio = new libpf_slp();


//receive variable
$rejectType = trim($rejectType);
$IntExt = trim($IntExt);
$IntExtStr = ($IntExt == "") ? "int": "ext";

########################################################
# Data Pre-processing : Start
########################################################
$mgmt_ele_arr = array();
if($IntExt == 0){ // IT IS A INT PROGRAM
	// for the component group , it support with INT only
	//CHECK user in which group
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

	//get USER's OLE component
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}

$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExtStr);
//debug_r($IntExt);
//debug_r($LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2);
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
	$allowClassTeacherApprove = 1;	
}
//debug_r($allowClassTeacherApprove);
$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);
$RecordList = (is_array($RecordArr)) ? implode($RecordArr, ",") : $record_id;
########################################################
# Data Pre-processing : End
########################################################


$sql = "CREATE TEMPORARY TABLE tempApprovableRecord (RecordID int(11), PRIMARY KEY (RecordID)) ";
//echo $sql."<Br/>";
$ldb->db_db_query($sql);

$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);
if($isAdmin)
{

	$sql = "INSERT INTO tempApprovableRecord (RecordID) SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE ProgramID IN ($RecordList)";
//echo $sql."<Br/>";
	$ldb->db_db_query($sql);
}
else
{
	// Class Teacher
	if($allowClassTeacherApprove == 1){
		$sql = "INSERT IGNORE INTO tempApprovableRecord (RecordID) ";
		$sql .= "SELECT DISTINCT os.RecordID ";
		$sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
		$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
		$sql .= "WHERE yct.UserID = {$UserID} AND os.ProgramID IN ($RecordList) ";
//echo "teacher ==> ".$sql."<Br/>";
		$ldb->db_db_query($sql);
	}
	
	// Requested approver
	$sql = "INSERT IGNORE INTO tempApprovableRecord (RecordID) ";
	$sql .= "SELECT DISTINCT os.RecordID ";
	$sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
	$sql .= "WHERE os.RequestApprovedBy = {$UserID} AND os.ProgramID IN ($RecordList)";
//echo $sql."<Br/>";
	$ldb->db_db_query($sql);
	
	// Programme Creator
	$sql = "INSERT IGNORE INTO tempApprovableRecord (RecordID) ";
	$sql .= "SELECT DISTINCT os.RecordID ";
	$sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
	$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM op ON os.ProgramID = op.ProgramID ";
	$sql .= "WHERE op.CreatorID = {$UserID} AND os.ProgramID IN ($RecordList)";
//echo $sql."<Br/>";
	$ldb->db_db_query($sql);
	
	// Group Admin (with specific components)
	$eleConds = "";
	if(!empty($mgmt_ele_arr))
	{
		$conds = "";
		for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
		{
			$mgmt_ele = $mgmt_ele_arr[$i];
			
			$eleConds .= ($eleConds == "") ? "" : " or ";
			$eleConds .= " INSTR(op.ELE, '{$mgmt_ele}') > 0 ";

		}
		$eleConds =  "(".$eleConds.")";

		$sql = "INSERT IGNORE INTO tempApprovableRecord (RecordID) ";
		$sql .= "SELECT DISTINCT os.RecordID ";
		$sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
		$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM op ON os.ProgramID = op.ProgramID ";
		$sql .= "WHERE os.ProgramID IN ($RecordList) AND {$eleConds}";
//echo $sql."<Br/>";
		$ldb->db_db_query($sql);	
	}
}

//$approve = (!isset($approve)) ? 2 : $approve;
$approve = (!isset($approve)) ? $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] : $approve;


$sql = ""; // reset the SQL 

# RecordStatus = 1 (only pending record can be approved/rejected)
if (strstr($ck_function_rights, "Profile:OLR"))
{
  # Eric Yip (20100429): Can reject all the records, disregarding record status
  if($approve == $ipf_cfg["OLE_STUDENT_RecordStatus"]["rejected"])  //reject action
  {
    $sql = "UPDATE {$eclass_db}.OLE_STUDENT os ";
		$sql .= "INNER JOIN tempApprovableRecord tar ON os.RecordID = tar.RecordID ";
		$sql .= "SET os.RecordStatus = '$approve', os.ApprovedBy = '$UserID', os.ProcessDate = now(), os.ModifiedDate = now() ";
		$sql .= ($rejectType == "pending") ? "WHERE os.RecordStatus = 1" : "";

  }
  else
  {
    $sql = "UPDATE {$eclass_db}.OLE_STUDENT os ";
    $sql .= "INNER JOIN tempApprovableRecord tar ON os.RecordID = tar.RecordID ";
    $sql .= "SET os.RecordStatus = '$approve', os.ApprovedBy = '$UserID', os.ProcessDate = now(), os.ModifiedDate = now() ";

	//suppose no need to concern the original status
//		$sql .= "WHERE os.RecordStatus = ".;  

  }
}
else
{
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT os ";
	$sql .= "INNER JOIN tempApprovableRecord tar ON os.RecordID = tar.RecordID ";
	$sql .= "SET os.RecordStatus = '$approve', os.ApprovedBy = '$UserID', os.ProcessDate = now(), os.ModifiedDate = now() ";

	//suppose no need to concern the original status
//	$sql .= "WHERE os.RecordStatus = 1"; 

}

if(trim($sql) != ""){ // $sql has value
//	echo $sql."<Br/>";
	$ldb->db_db_query($sql);
}

intranet_closedb();

//exit();
header("Location: ole.php?msg=".($approve==2?"approve":"reject")."&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");

//if ($FromPage == "program")
//	header("Location: ole.php?msg=".($approve==2?"approve":"reject")."&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
//else	
//	header("Location: ole_student.php?msg=update&StudentID=$StudentID&page_size_change=$page_size_change&numPerPage=$numPerPage&pageNo=$pageNo&order=$order&field=$field&IntExt=$IntExt");


?>

