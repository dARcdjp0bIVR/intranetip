<?php
// Modifing by 
##########################################################
## Modification Log
##	2015-01-15 Omas
##	- added $sys_custom['iPf']['displayIneRC'] feature - #U77580
##########################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();


$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);


$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);
if (is_array($RecordArr))
{
	$RecordList = implode($RecordArr, ",");
}
else
{
	$RecordList = $record_id;
}
if($eRCBatch){
	$sql= "SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE  ProgramID IN ($RecordList)";
	$recordIDArr = $LibPortfolio->returnVector($sql);
	if(count($recordIDArr)>0){
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET displayIneRC = {$displayIneRC} ,ModifiedDate = now() WHERE RecordID IN ('".implode("','",$recordIDArr)."')";
	$LibPortfolio->db_db_query($sql);
	}
}
else{
	if ($displayIneRC==='1' || $displayIneRC ==='0'){
	$sql = "UPDATE {$eclass_db}.OLE_STUDENT SET displayIneRC = {$displayIneRC} ,ModifiedDate = now() WHERE RecordID IN ($RecordList)";
	$LibPortfolio->db_db_query($sql);
	}
	else{
		// do noth
	}		
}
intranet_closedb();

if ($FromPage == "program" && !$eRCBatch)
	header("Location: ole_event_studentview.php?msg=2&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
else
	header("Location: ole.php?page_size_change=$page_size_change&numPerPage=$numPerPage&pageNo=$pageNo&order=$order&field=$field&IntExt=$IntExt");
//	header("Location: ole.php?msg=".($approve==2?"approve":"reject")."&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
?>



?>
