<?php
## Using By: 
###########################################
##	Modification Log:
##	
##	2016-07-15 Henry HM
##	Replaced all split() to explode() for PHP 5.4 
##	
###########################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

# uploaded file information
intranet_opendb();
$li_pf = new libportfolio();
$li_pf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$li_pf->ACCESS_CONTROL("ole");
$li = new libdb();
$fs = new libfilesystem();

$count_new = 0;
$count_updated = 0;
$display_content = "";

$is_attachment = false;
for ($i=1; $i<=$attachment_size; $i++)
{
	if (trim(${"ole_file".$i})!="")
	{
		$is_attachment = true;
	}
}

if($is_attachment == false && $attachment_size_current <= 0)
{
	intranet_closedb();
	header("Location: ole_upload.php?StudentID=$StudentID&EventID=$EventID&RecordID=$RecordID");
}
else
{
	// use previous session_id if exists
	if ($attachment_size_current>0)
	{
		$tmp_arr = explode("/", ${"file_current_0"});
		$SessionID = trim($tmp_arr[0]);
	}
	if ($SessionID=="")
	{
		$SessionID = session_id();
	}
	//$folder_prefix = $eclass_root."/files/portfolio/ole/r".$RecordID."/".$SessionID;
	//$fs->folder_remove_recursive($eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID);
	
	$fs->folder_new($eclass_root."/files/portfolio/ole");
	$fs->folder_new($eclass_root."/files/portfolio/ole/tmp");
	$fs->folder_new($eclass_root."/files/portfolio/ole/tmp/t_".$UserID);
	$fs->folder_new($eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID);
	$folder_prefix = $eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID."/".$SessionID;
	
	# remove unwanted files
	for ($i=0; $i<$attachment_size_current; $i++)
	{
		$attach_file = ${"file_current_".$i};
		$all_attachments .= (($all_attachments=="")?"":":") . $attach_file;
		
		if (${"is_need_".$i})
		{	
			$attachments .= (($attachments=="")?"":":") . $attach_file;
			$fileArr[] = $attach_file;
		} 
		else
		{
			// store remove file & item id
			$remove_attachments .= (($remove_attachments=="")?"":":") . $attach_file;
			$remove_item_id .= (($remove_item_id=="")?"":":") . $i;
		}
	}
	
	if($is_attachment)
	{
		$fs->folder_new($folder_prefix);

		# copy the files
		for ($i=1; $i<=$attachment_size; $i++)
		{
			$loc = ${"ole_file".($i)};
			$name_hidden = ${"ole_file".$i."_hidden"};
			$filename = (trim($name_hidden)!="") ? $name_hidden : ${"ole_file".($i)."_name"};
			if ($loc!="none" && file_exists($loc))
			{
				if(strpos($filename, ".")==0)
				{ 
					// Fail
					$isOk = false;
				} 
				else
				{ 
					// Success
					$fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
					$tmp_attach = $SessionID."/".$filename;
					$exist_flag=0;
					for($j=0; $j<sizeof($fileArr); $j++)
					{
						if($tmp_attach==$fileArr[$j])
						{
							$exist_flag = 1;
							break;
						}
					}
					if($exist_flag!=1)
					{
						$all_attachments .= (($all_attachments=="")?"":":") . $tmp_attach;
						$attachments .= (($attachments=="")?"":":") . $tmp_attach;
						$fileArr[] = $tmp_attach;
					}
				}
			} // enf if file exist
		} // end for loop attachment
	} // end if check is attachment
	$attachments = (trim($attachments)=="") ? "" : "$attachments";
} // end if is attachment
intranet_closedb();
$remove_attachments = (trim($remove_attachments)=="") ? "" : "$remove_attachments";
$all_attachments = (trim($all_attachments)=="") ? "" : "$all_attachments";
//debug_r($remove_attachments);
//debug_r($attachments);
//debug_r($remove_item_id);
?>

<SCRIPT LANGUAGE="Javascript">
window.opener.jChangeToUploadStatus("<?=$StudentID?>", "<?=$RecordID?>", "<?=$all_attachments?>", "<?=$attachments?>", "<?=$remove_attachments?>", "<?=$remove_item_id?>", "<?=$original_file_number?>");
self.location.href = "ole_upload.php?StudentID=<?=$StudentID?>&EventID=<?=$EventID?>&RecordID=<?=$RecordID?>";
</SCRIPT>
