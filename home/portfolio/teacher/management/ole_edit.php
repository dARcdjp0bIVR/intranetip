<?php
# *remark 1 - please do not use variable name $role directly, since this will collapse with the session variable
/** [Modification Log] Modifying By: 
 * *******************************************
 * 2016-10-25 Omas
 * - change basename() to get_file_basename() for php5.4
 * 
 * 2015-05-12 Bill
 * - add "Hours / Marks" checking to ensure positive value input
 * 
 * 2013-11-11 Ivan [2013-1106-1649-45071]
 * - change award to textarea for consistancy and added preset function
 * 
 * 2010-11-22 Max (201011191622)
 * - Change the approved_by to requestApprovedBy
 * 
 * 2010-11-22 Max (201011191622)
 * - Change the approved_by to requestApprovedBy
 * 
 * 2010-06-30 Max (201006301329)
 * - changed variable name $role to $var_role: refer to remark 1
 * - added id from title field and role field for GetPresetText to identify
 * - js check field "attachment_size" exist or not before js operation on attachment_size
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");
intranet_auth();
intranet_opendb();


// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libpf_slp(); // for libwordtemplates_ipf
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$lpf->ACCESS_CONTROL("ole");


//CHECK user in which group
$LibPortfolio = new libpf_slp();
$lpf_mgmt_grp = new libpf_mgmt_group();
$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

//get USER's OLE component
$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);

//1) the teacher is a OLE ADMIN and
//2a) the teacher is big admin "strstr($ck_function_rights, ":manage:")" OR
//2b) the teacher in a group without ELE component restriction "sizeof($mgmt_ele_arr) == 0"
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);
$editProgramList = array();
if(!$isAdmin){
	//handle the program access right
	$editProgramList = getProgramCanBeEdit($UserID);
}


// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;
$TAGS_OBJ = $TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

###############################################
$SelectedStatus = $status;
$SelectedELE = $ELE;
$SelectedYear = $Year;

$record_id = (is_array($record_id)) ? $record_id[0] : $record_id;

# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;
# template

# template drop down list
for ($f=0; $f<sizeof($LibWord->file_array); $f++) 
{
	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	unset($WordListArr);
	$tempArr = $LibWord->getWordList($id);
	$WordListArr[] = array("", " --- ".$button_select." --- ");
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$WordListArr[] = array(str_replace("&amp;", "&", $p_title), undo_htmlspecialchars($p_title));
	}
	$WordListSelection[] = array($id, returnSelection($WordListArr , "", "WordList", "onChange=\"document.form1.title.value = this.value \""));
}
# template drop down list

# float list arr
for ($f=0; $f<sizeof($LibWord->file_array); $f++) {

	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	$tempArr = $LibWord->getWordList($id);
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$ListArr[$id][] = str_replace("&amp;", "&", undo_htmlspecialchars($p_title));

		# preset ELE of the corresponding title
		$ListELEArr[$id][] = $TempTitleArray[1];
	}
}


###############################################

###############################################
# template for activity role
$LibWord2 = new libwordtemplates_ipf(4);

# float list arr
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
	$RoleListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
}
$js_role_arr = ConvertToJSArray($RoleListArr, "jRoleArr1");

# template
###############################################


###############################################
# template for activity awards
 $LibWordAward = new libwordtemplates_ipf(5);

# float list arr
 $awards_array = $LibWordAward->getWordList(0);
 for ($i=0; $i<sizeof($awards_array); $i++) {
       $AwardsListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($awards_array[$i]));
 }
 print ConvertToJSArray($AwardsListArr, "jAwardsArr1");

# template
###############################################



//list($ClassName, $StudentID) = $lpf->GET_STUDENT_ID($ck_user_id);

$programIsEditable = 1;

$attach_count = 0;
if($record_id!="")
{
	$DataArr = $lpf->RETURN_OLE_RECORD_BY_RECORDID($record_id);
// 	debug_r($DataArr);
	list($startdate, $enddate, $title, $category, $ELE, $var_role, $hours, $organization, $achievement, $details, $ole_file, $approved_by, $remarks, $process_date, $status, $user_id, $_intext, $_programid, $_subcategoryID, $_maxHours, $_compulsoryFields, $requestApprovedBy) = $DataArr;

	//the program does not in the $editProgramList, --> it cannot be edited by the teacher
	if(!in_array($_programid,$editProgramList)){
		if(!$isAdmin){
			// not a admin , should editable to false
			$programIsEditable = 0;
		}
	}

	# load school remarks from OLE_PROGRAM
	$sql = "SELECT op.SchoolRemarks FROM {$eclass_db}.OLE_PROGRAM op INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID WHERE os.RecordID = '{$record_id}'";
	$SchoolRemarks = $lpf->returnVector($sql);
	$SchoolRemarks = $SchoolRemarks[0];

	# Get all categories if record category is disabled
	$OLE_Cat = $lpf->get_OLR_Category();
	if(!array_key_exists($category, $OLE_Cat))
		$file_array = $LibWord->setFileArr(true);

	# get ELE Array
	$ELEArr = explode(",", $ELE);
	$ELEArr = array_map("trim", $ELEArr);

	$JScriptID = "jArr".$category;
	$JELEScriptID = "jELEArr".$category;

	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_edit, "")
		);
} else
{
	$NewCategoryArray = $lpf->get_OLR_Category();
	// get the least category id
	foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
	{
		$TmpCategoryID = $CategoryID;
		break;
	}

	$JScriptID = "jArr".$TmpCategoryID;
	$JELEScriptID = "jELEArr".$TmpCategoryID;
	if($ELE!="")
	{
		$ELEArr = array($ELE);
	}
	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_new, "")
	);
}
//debug_r("programIsEditable ".$programIsEditable);

//$h_disbaled = ($programIsEditable)? "" :" readonly ";
$h_disbaled = ($programIsEditable)? "" :" DISABLED ";


$category = (!isset($category) || $category=="") ? 1 : $category;


# build ELE list
$DefaultELEArray = $lpf->GET_ELE();
foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
{
	$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";
	$ELEList .= "<INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' {$checked} {$h_disbaled}><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
}


if($startdate!="" && $enddate!="")
{
	$enddate_table_style = "display:block;";
	$add_link_style = "display:none;";
}
else
{
	$enddate_table_style = "display:none;";
	$add_link_style = "display:block;";
}

if($SubmitType == "")
{
	if($IntExt == 1)
	$SubmitType = "EXT";
	else
	$SubmitType = "INT";
}

// submit type (internal / external)
if($SubmitType == "EXT") 
{
	$DisplayStyle = "style='display:none;'";
	$OrganizationText = $iPort["external_ole_report"]["organization"];
}
else
{
	$DisplayStyle = "";
	$OrganizationText = $ec_iPortfolio['organization'];
}

$sumit_type_array = array();
$sumit_type_array[] = array("INT", $iPort["internal_record"]);
$sumit_type_array[] = array("EXT", $iPort["external_record"]);
$SubmissionType = $linterface->GET_SELECTION_BOX($sumit_type_array, "name ='SubmitType' onChange=\" jChangeSubmitType(this); \" ","", $SubmitType);



# approval load settings
//$ApprovalSetting = $lpf->GET_OLR_APPROVAL_SETTING();
//if ($ApprovalSetting==$ipf_cfg["RECORD_APPROVAL_SETTING"]["studentSelectApprovalTeacher"])
//{
//	
//	# teacher selection
//	$teacher_selection = $lpf->GET_TEACHER_SELECTION($requestApprovedBy);
//	$TeacherSelectionRow = "<tr valign=\"middle\" class=\"chi_content_12\">
//								<td valign=\"top\" nowrap=\"nowrap\">".$ec_iPortfolio['approved_by']." :</td>
//								<td>".$teacher_selection."</td>
//							</tr>
//							";
//}

$approvalSettings = $lpf->GET_OLR_APPROVAL_SETTING_DATA2($SubmitType);
//if ($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self'])
//{
	
	# teacher selection
	$teacher_selection = $lpf->GET_TEACHER_SELECTION($requestApprovedBy);
	$TeacherSelectionRow = "<tr valign=\"middle\" class=\"chi_content_12\">
								<td valign=\"top\" nowrap=\"nowrap\">".$Lang['iPortfolio']['preferred_approver']." :</td>
								<td>".$teacher_selection."</td>
							</tr>
							";
//}



$linterface->LAYOUT_START();

for ($f=0; $f<sizeof($LibWord->file_array); $f++) 
{
	$id = $LibWord->file_array[$f][0];
	$js_list_arr .= ConvertToJSArray($ListArr[$id], "jArr".$id);
	$js_ele_arr .= ConvertToJsArray($ListELEArr[$id], "jELEArr".$id);
}

$attach_count = 0;
// $tmp_arr = explode("\:", $ole_file);
$tmp_arr = explode(":", $ole_file);
$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$record_id;
$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$record_id;
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
	$attach_file = $tmp_arr[$i];
	if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
	{
		$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
		$attachments_html .= "<a href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count' class='tablelink' >".get_file_basename($attach_file)." ($file_size)</a>";
		$attachments_html .= "<input type='checkbox' name='is_need_$attach_count' onClick='triggerFile(document.form1, \"\", ".$attach_count.")' CHECKED>\n";
		$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
		$attach_count ++;
	}
}

?>

<?=$js_role_arr?>
<?=$js_list_arr?>
<?=$js_ele_arr?>

<script language="JavaScript">
var jCurrentArr = eval("<?=$JScriptID?>");
var jCurrentELEArr = eval("<?=$JELEScriptID?>");
var jPresetELE = 1;

//onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value);";
onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value); jCurrentELEArr=eval('jELEArr'+this.value);";
</script>

<script language="JavaScript">

onChangeStr1 = "document.getElementById('title_selection_";
onChangeStr3 = "').style.display = 'block'";

function triggerFile(formObj, my_file, my_index)
{
	var is_remove = (eval("formObj.is_need_"+my_index+".checked==false"));
	var link = document.getElementById('a_'+my_index);
	link.style.textDecorationLineThrough = is_remove;
}

function checkform(formObj)
{
	// YuEn: check date and title!
	if(formObj.title.value=="")
	{
		alert("<?=$ec_warning['title']?>");
		return false;
	}
	
	if(formObj.startdate.value=="")
	{
		formObj.startdate.focus();
		alert("<?=$ec_warning['date']?>");
		return false;
	}
	else
	{
		if(!check_date(formObj.startdate,"<?=$w_alert['Invalid_Date']?>"))
		{
			return false;
		}
		else if(formObj.enddate.value!="")
		{
			if(!check_date(formObj.enddate,"<?=$w_alert['Invalid_Date']?>"))
				return false;
			else if(formObj.startdate.value>formObj.enddate.value)
			{
				formObj.enddate.focus();
				alert("<?=$w_alert['start_end_time2']?>");
				return false;
			}
		}
	}

    if (isNaN(formObj.hours.value)) {
    	alert("<?=$ec_iPortfolio['SLP']['PleaseEnterANumber']?>");
    	return false;
    } else if(Number(formObj.hours.value) < 0){
    	alert("<?=$Lang['iPortfolio']['PleaseInputPositiveNumber']?>");
    	return false;
    }
    
	//CHeck Attachment Path format
	var attachment_size_element = document.getElementById('attachment_size');
	if (attachment_size_element != null) {
		var attachment_size = attachment_size_element.value;
		for (var x = 1; x <= attachment_size; x++)
	   {
			file_path = "ole_file"+x;
			if (document.getElementById(file_path) != undefined)
			{
				var path = document.getElementById(file_path).value;
				if(path!=""){
					if((path.charAt(0) != "\\" && path.charAt(1) != "\\") && (path.charAt(0) != "/" && path.charAt(1) != "/"))
					{
		
						if(!path.charAt(0).match(/^[a-zA-z]/))
						{
							document.getElementById(file_path).focus();
							alert("<?=$ec_iPortfolio['upload_error']?>");
							return false;
						}
						if(!path.charAt(1).match(/^[:]/) || !path.charAt(2).match(/^[\/\\]/))
						{
							document.getElementById(file_path).focus();
							alert("<?=$ec_iPortfolio['upload_error']?>");
							return false;
						}
					}
				}
			}
		}
	}


	// handle special Chinese filename
	//var attachment_size = document.getElementById('attachment_size').value;
	Big5FileUploadHandler(formObj, 'ole_file', attachment_size);

	return true;
}

function jPRESET_ELE(i_pos, j_pos)
{
	var formObj = document.form1;
	var ELEObj = formObj.elements["ele[]"];
	var len = ELEObj.length;
	var jPresetELEList = jCurrentELEArr[i_pos][j_pos];

	for(var m=0; m<len; m++)
	{
		if(typeof(jPresetELEList)!="undefined" && jPresetELEList.indexOf(ELEObj[m].value)!=-1) {
			ELEObj[m].checked = 1;
		}
		else {
			ELEObj[m].checked = 0;
		}
	}
}

function jChangeSubmitType(obj)
{
	var row1 = document.getElementById("row_ele");
	var div1 = document.getElementById("div_org");
	
	// external
	if(obj.value == "EXT")
	{
		row1.style.display = "none";
		div1.innerHTML = "<?=$iPort["external_ole_report"]["organization"]?>";
	}
	else
	{
		// internal
		row1.style.display = "block";
		div1.innerHTML =  "<?=$ec_iPortfolio['organization']?>";
	}
}

$(document).ready(function(){
	$("form[name=form1] input[type=text]:not(:disabled)").eq(0).focus();
});

</script>

<FORM name="form1" method="post" action="ole_student_update2.php" enctype="multipart/form-data" onSubmit="return checkform(this)">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td >
	<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_edit?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
	</table>
		
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
	<tr>
		<td valign="top" >
		<!-- CONTENT HERE -->
				
					
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			
			<!-- Type -->
			<!--
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $iPort['submission_type']; ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><?=$SubmissionType?></td>
				</tr>
				</table>
				</td>
			</tr>
			-->
			
			<!-- cat -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['category']?></span>
				</td>
				<td>
				<?=$linterface->GET_SELECTION_BOX($file_array, "name ='category' onChange=\" eval(onChangeStr) \" {$h_disbaled}","",$category);?>						
				</td>
			</tr>
			
			<!-- title -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $ec_iPortfolio['title']; ?>:<span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><input id="title" name="title" type="text" size="50" value="<?=$title?>" class="textboxtext"  maxlength="255" <?=$h_disbaled?>></td>
					<td width="5">&nbsp;</td>
					<td width="25" ><div id="change_preset">
					<? if($programIsEditable){
						echo GetPresetText("jCurrentArr", 1, "title", "jPRESET_ELE()");
						}
					?></div></td>
				</tr>
				</table>
				</td>
			</tr>
			
			<!-- date -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['date']?>:<span class="tabletextrequire">*</span></span></td>
				<td>				

				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap='nowrap'>
					<input type="text" name="startdate" size="11" maxlength="10" class="tabletext" value="<?=$startdate?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}" <?=$h_disbaled?> />
					<?php if($programIsEditable){
						echo $linterface->GET_CALENDAR("form1", "startdate");
						}
					?>
					</td>
					<td nowrap='nowrap' id="table_enddate" style="<?=$enddate_table_style?>">&nbsp;<?=$profiles_to?>&nbsp;<input class="tabletext" type="text" name="enddate" size="11" maxlength="10" value="<?=$enddate?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}" <?=$h_disbaled?> />
					<?php if($programIsEditable){
						$linterface->GET_CALENDAR("form1", "enddate");
						}
					?>
				</tr>
				<?php if($programIsEditable){ ?>
				<tr id="add_link" style="<?=$add_link_style?>"><td height="25"><a href="javascript:displayTable('add_link', 'none');displayTable('table_enddate', 'block');" class='tablelink'><?=$ec_iPortfolio['add_enddate']?></a></td></tr>
				<?php } ?>
				</table>								
				</td>
			</tr>
			
			<!-- ele -->
			<tr id="row_ele" valign="top" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['ele']?><span class="tabletextrequire">*</span></span>
				</td>
				<td><?=$ELEList?></td>
			</tr>
			
			<!-- Hours -->
			<tr id="row_hour" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['hours']?></span></td>
				<td><input name="hours" type="text" size="10" value="<?=$hours?>" class="tabletext" ></td>
			</tr>
			
			<!-- role -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $ec_iPortfolio['ole_role']; ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><input id="role2" name="role2" type="text" size="50" value="<?=$var_role?>" class="textboxtext"  maxlength="255"></td>
					<td width="5">&nbsp;</td>
					<td width="25" ><div id="change_preset2"><?= GetPresetText("jRoleArr1", 2, "role2", "");?></div></td>
				</tr>
				</table>
				</td>
			</tr>

			<!-- organization -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$OrganizationText?></div></span></td>
				<td><input name="organization" type="text" value="<?=$organization?>" maxlength="256" class="textboxtext" <?=$h_disbaled?>></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['achievement']?></span></td>
				<!--td><input name="achievement" type="text" size="50" value="<?=$achievement?>" maxlength="256" class="textboxtext" ></td-->
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="60%">
					<tr>
						<td width="100%"><?=$linterface->GET_TEXTAREA("achievement", $achievement, 70, 5)?></td>
						<td width="5">&nbsp;</td>
						<td width="25" style="vertical-align:top;"><div id="change_preset3" style="float:left;"><?= GetPresetText("jAwardsArr1", 3, "achievement", "");?></div></td>
					</tr>
					</table>		
				</td>
			</tr>			
			
			<!-- attachment -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['attachment']?></span></td>
				<td valign="top">
				<?=$attachments_html?>
				<table border="0" cellpadding="0" cellspacing="0" id="tableAttachment" style="display:none" >
				<tr>
					<td>&nbsp;</td>
				</tr>
				</table>
				<a class="tablelink" href="javascript:document.getElementById('tableAttachment').style.display='block';jAddMoreFileField(this.form, 'tableAttachment', 'attachment_size', 'ole_file')" ><?=$button_more_file?></a>
				</td>				
			</tr>

			<!-- detail -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?></span></td>
				<td><?=$linterface->GET_TEXTAREA("details", $details,70,5,'',0,$h_disbaled);?></td>
			</tr>
			
			<!-- school remark -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remarks']?>:</span></td>
				<td><?=$linterface->GET_TEXTAREA("SchoolRemarks", $SchoolRemarks,70,5,'',0,$h_disbaled);?></td>
			</tr>
			
			<!-- Teacher Selection -->
			<?=$TeacherSelectionRow?>
			</table>
			</td>
		</tr>
		<tr>
			<td height="1" class="dotline" colspan="2"  ><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		
		<tr>
			<td  colspan="2" align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletextremark"><?=$ec_iPortfolio['mandatory_field_description']?></td>
				<td align="right">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>				
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>								
				</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>


<input type="hidden" id="ClassName" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" id="StudentID" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" id="attachment_size" name="attachment_size" value="0" >
<input type="hidden" id="RecordID" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" id="attachment_size_current" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" id="ApprovalSetting" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" id="SelectedStatus" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" id="SelectedELE" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" id="SelectedYear" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" id="FromPage" name="FromPage" value="<?=$FromPage?>" >
<input type="hidden" id="IntExt" name="IntExt" value="<?=$IntExt?>" >
<input type="hidden" id="programIsEditable" name="programIsEditable" value="<?=$programIsEditable?>" >

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

//start TEST
function getProgramCanBeEdit($teacherID){
	global $eclass_db , $ipf_cfg; 
	$lpf_mgmt_grp = new libpf_mgmt_group();
	$LibPortfolio = new libpf_slp();
	$libdb =  new libdb();
	//CHECK user in which group
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($teacherID);
	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

	//get USER's OLE component
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
	
	$_com_progamList = array();
	$com_programListStr = "";
	if(!empty($group_id_arr))
	{
		$conds = "";
		for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
		{
			$mgmt_ele = $mgmt_ele_arr[$i];
		
			$conds .= "OR INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
		}
		

		$sql = "SELECT DISTINCT op.ProgramID FROM {$eclass_db}.OLE_PROGRAM op ";
		$sql .= "WHERE 0 {$conds}";
//debug_r(" component ===>".$sql);	
		$_com_progamList = $libdb->returnVector($sql);	
		
		if(sizeof($_com_progamList) > 0 && is_array($_com_progamList)){
			$com_programListStr = implode(",",$_com_progamList);
			$com_programListStr = " or op.ProgramID in ({$com_programListStr}) ";
		}
	}


	$sql = "";
	$sql .= "SELECT distinct os.userid as 'userid' FROM {$eclass_db}.OLE_PROGRAM op ";
	$sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
	$sql .= "WHERE yct.UserID = {$teacherID} ";
//echo $sql."<Br/>";
	$classStudentRS = $libdb->returnVector($sql);

	$classStudentStr = "";
	$classStudentSql = "";
	if(is_array($classStudentRS) && sizeof($classStudentRS) > 0 ){
		$classStudentStr = implode(",",$classStudentRS);
	}
	if($classStudentStr  != ""){
		// $classStudentStr has value
		$classStudentSql = " or ( os.Userid in({$classStudentStr}) and op.ComeFrom = ".$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"].") ";
	}

	$sql = "Select distinct op.ProgramID
				from {$eclass_db}.OLE_STUDENT as os
				inner join {$eclass_db}.OLE_PROGRAM as op on os.Programid = op.programid
				where 
				(
					(os.RequestApprovedBy = {$teacherID} and op.ComeFrom = ".$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"].")
						or 
					op.CreatorID = {$teacherID}
					{$classStudentSql}
					{$com_programListStr}
				)
		   ";
	$programRS = $libdb->returnVector($sql);
//echo "function sql ".$sql."<br/>";
/*
	$programListStr = "";
	if(is_array($programRS) && sizeof($programRS) > 0){
		$programListStr = implode(",",$programRS);
	}
	*/
	return $programRS;

}
//END test

?>

