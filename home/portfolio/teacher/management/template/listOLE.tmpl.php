<script>
function jSUBMIT_FORM(){
  
//  document.searchForm.submit();
document.form1.submit();
}
function showRecordDetails(recoredID){

	var curr_status = $("#showAction_"+recoredID).attr("class");

	if(curr_status == "zoom_in"){
		$("#studentDetails_"+recoredID).show();
		$("#programDetails_"+recoredID).show();
		$("#showAction_"+recoredID).attr("class","zoom_out");
	}else{
		$("#studentDetails_"+recoredID).hide();
		$("#programDetails_"+recoredID).hide();
		$("#showAction_"+recoredID).attr("class","zoom_in");
	}



}
function js_editItem(){
	var selectProgramID = 0;	
	var passChecking = true;
	$("input[name='record_id[]']").each(function(){			
			if($(this).attr('checked')){
				var _programID = $(this).attr("pId");
				if(selectProgramID == 0){
					selectProgramID =_programID; 
					$("#EventID").val(selectProgramID);
				}else{				
					if(_programID != selectProgramID){						
						passChecking = false;
					}
				}				
			}		
		});
		if(passChecking == true){
			checkEditMultiple(document.form1,'record_id[]','ole_student_edit2.php',0);
		}else{
			alert("<?=$iPort["msg"]["batchEditWarning"]?>");
		}		
}
function js_changeStudentItemStatus(status){
	$("#hidChangeStatus").val(status);
	if(status == 2){
		checkApprove(document.form1,'record_id[]','oleIndex.php?task=approveAction');
	}else{
		checkRejectNoDel(document.form1,'record_id[]','oleIndex.php?task=approveAction');
	}
}
function moreProgram(programID){
	$.post(
		"oleIndex.php",{
			programID:programID,
			task:"ProgramDetails"
		},
		function(ReturnData){
				$(".programMoreDetails_"+programID).replaceWith(ReturnData);
		}
	);
}
function expandAll(){

	var curr_status = $("#expandControl").attr("class");
	var displayText = "";
	var _tmpElementID = "";
	var _recordID = "";

	$("a[id^='showAction']").each(function(){
			_recordID = "";
			_tmpElementID = $(this).attr('id')
			var _array = _tmpElementID.split('_');
			_recordID = _array[1];
			showRecordDetails(_recordID);
	});

	if(curr_status == "collapse"){
		displayText = " - <?=$i_general_collapse_all?>";
		$("#expandControl").attr("class","expand");
	}else{
		displayText = " + <?=$i_general_expand_all?>";
		$("#expandControl").attr("class","collapse");
	}
	$("#expandControl").html(displayText);
}
</script>
<FORM name="form1" method="post" action="oleIndex.php">
<div class="content_top_tool">
<br/>

<table width = "100%">
<tr><td width = "30%"><?=$h_slt_oleType?></td><td align = "right" width = "*"><?=$h_returnMsg?></td></tr>
</table>
<?=$h_sltYear?>
<?=$h_slt_RequestType?>
<?=$h_sltStuatType?>
  <div class="Conntent_search">
    <input name="searchText" type="text" value="<?=$h_searchText?>" />
  </div>
</div>
<br/>

<?
	echo $h_expandAll;
	echo $h_actionBar;
	echo $h_tableContent;
?>


<input type ="hidden" name="EventID" value="" id="EventID">
<input type ="hidden" name="fromAction" value="approveStudentRecord" id="fromAction">
<input type ="hidden" name="hidChangeStatus" value="" id="hidChangeStatus"> <?//store value for approve / reject record?>
<input type ="hidden" name="IntExt" value="<?=$IntExt?>" id="IntExt">

<input type="hidden" name="order" value="<?php echo $li->order;?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>