<?php
/**
 * [Modification Log] Modifying By: 
 * ************************************************
 * 2015-01-15 Omas
 * 	- added $sys_custom['iPf']['displayIneRC'] feature - #U77580
 * 2010-05-06 Max (201005061355)
 * - More eye catching Link
 * ************************************************
 */
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">
var editableProgramID = <?=$html_js_editable_programme?>
var preventDef =	function(event){
										event.preventDefault();
									}

function doExport(my_type){
	newWindow("../../profile/"+my_type+"/data_export.php", 27);
}

function index_reload_page(msg){
  this.tb_remove();     //Close thickbox
  window.location = "ole.php?IntExt=<?=$IntExt?>&msg="+msg;      //Refresh page
}

function jSUBMIT_FORM(){
  var search_text = document.getElementById("search_text");
  
  if(search_text.value == '<?=$ec_iPortfolio['enter_title_name']?>'){
    search_text.value = '';
  }
  
  document.form1.submit();
}

function jSET_SEARCH_TEXT(jParFocus){
  var search_text = document.getElementById("search_text");

  if(jParFocus)
  {
    if(search_text.value == '<?=$ec_iPortfolio['enter_title_name']?>'){
      search_text.value = '';
    }
  }
  else
  {
    if(search_text.value == ''){
      search_text.value = '<?=$ec_iPortfolio['enter_title_name']?>';
    }
  }
}

function jBATCH_CHANGE_YEAR(){
  if(countChecked(document.form1, 'record_id[]') > 0)
  {
    tb_show("","ole_change_year.php?KeepThis=true&TB_iframe=true&height=200&width=500","");
  }
  else
  {
    alert(globalAlertMsg2);
  }
}

function jAPPROVE_PROGRAMME(){
  $("input[name=approve]").val(2);
  checkApprove(document.form1,'record_id[]','student_ole_status_update.php');
}

function jREJECT_PROGRAMME(jParRejectType){
  $("input[name=approve]").val(3);
  $("input[name=rejectType]").val(jParRejectType);
  checkRejectNoDel(document.form1,'record_id[]','student_ole_status_update.php');
}

function checkMerge() {
	if(countChecked(document.form1, 'record_id[]') > 0)
  {
		$("form[name=form1]").attr("action","ole_new.php");
		$("input[name=Action]").val("Merge");
		$("form[name=form1]").submit();
	}
  else
  {
    alert(globalAlertMsg2);
  }
}


$(document).ready(function() {
  $('#rejectStatusShow').click(function() {
    $('#status_list').css({
      "top": ($(this).offset().top + 20)+"px",
      "left": $(this).offset().left+"px"
    });
    $('#status_list').show();
  });
  $('#rejectStatusHide').click(function() {
    $('#status_list').hide();
  });
  
  $("input[name='record_id[]']").click(function(){
	  handleActionBar();
	});
	$("input[name='checkmaster']").click(function(){	
	  handleActionBar();			
	});
});
function handleActionBar(){
	  	var editable = true;
  
		$("input[name='record_id[]']:checked").each(function(){
			var programID = parseInt($(this).val());
			if($.inArray(programID, editableProgramID) == -1)
			{
				editable = false;
				return false;
			}
		});
		
		if(editable)
		{
			$("#edit_toolbar").find("a").css("color", "");
			$("#edit_toolbar").find("a").unbind("click", preventDef);
		}
		else
		{
			$("#edit_toolbar").find("a").css("color", "#CCCCCC");
			$("#edit_toolbar").find("a").bind("click", preventDef);
		}

}
<? if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='') ){ ?>
function displayeRC(obj,element,page,status){
  if(countChecked(obj,element)==0)
    alert(globalAlertMsg2);
  else{
      obj.action=page;
      obj.displayIneRC.value=status;
      obj.submit();
  }
}
<?}?>
</script>

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td align="center">
      <?=$tab_menu_html?>
  	</td>
  </tr>
  <?=$op_result?>
  <tr>
  	<td><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $ec_warning['oleMgmt_instruction'], "")?></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
  			<tr>
                <td nowrap>
                <?=$new_btn_html?>
  				<?=$import_btn_html?>
  				<?=$export_btn_html?>
  				<?=$comment_html?>
                </td>
      		<td width="100%" align="right" valign="bottom" class="thumb_list">
    				<span class="tabletext">
    				  <?=$searching_html?>
    				</span>
    			</td>
  			</tr>
			</table>
    </td>
  </tr>
  <tr>
    <td>
      <?=$ay_selection_html?>
      <?=$ele_selection_html?>
      <?=$record_selection_html?><br/>
      <div style="margin-top:5px;"><?=$category_selection_html?>&nbsp;&nbsp;<?=$subcategory_selection_html?>&nbsp;&nbsp;<?=$IsSAS_selection_html?>&nbsp;&nbsp;<?=$InsideOutSide_selection_html?></div>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="50%" valign="bottom">
            <table border="0" cellpadding="3" cellspacing="0">
    					<tr>					
    						<td class="navigation">
    						<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
    						<?=$ec_iPortfolio['program_list']?>				
    						</td>
    					</tr>					
  					</table>
					</td>
        </tr>
        <tr>
          <td align="right" valign="bottom">
<!--action bar -->          	
<?if($action != $ipf_cfg["OLE_ACTION"]["view"]){?>
            <div class="common_table_tool">
                <a href="javascript:jAPPROVE_PROGRAMME()" class="tool_approve"><?=$button_approve?></a>
                <a id="rejectStatusShow" href="javascript:;" class="tool_reject"><?=$button_reject?></a>
                <div id="status_list" style="position:absolute; width:170px; z-index:1; display: none;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td height="19">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5" height="19"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_01.gif" width="5" height="19"></td>
                                        <td height="19" valign="middle" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_02.gif">&nbsp;</td>
                                        <td width="19" height="19" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_02.gif"><a id="rejectStatusHide" href="javascript:;" class="none"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_close_off.gif" name="pre_close22" width="19" height="19" border="0" id="pre_close22" onMouseOver="MM_swapImage('pre_close22','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_04.gif" width="5" height="19"></td>
                                        <td bgcolor="#FFFFF7">
                                            <table width="98%" border="0" cellpadding="0" cellspacing="3">
                                                <tr><td><a href="javascript:jREJECT_PROGRAMME('all')" class="none"><?=$i_status_all?> <?=$ec_iPortfolio['record']?></a></td></tr>
                                                <tr><td><a href="javascript:jREJECT_PROGRAMME('pending')" class="none"><?=$i_status_waiting?> <?=$ec_iPortfolio['record']?></a></td></tr>
                                            </table>
                                        </td>
                                        <td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_06.gif" width="6" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_07.gif" width="5" height="6"></td>
                                        <td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_08.gif" width="5" height="6"></td>
                                        <td width="6" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/can_board_09.gif" width="6" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <? if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='') ):?>
                    <a href="javascript:displayeRC(document.form1,'record_id[]','display_eRC_update.php', 1)" class="tool_approve"><?=$Lang['iPortfolio']['OLE']['DisplayIneRC']?></a>
                    <a href="javascript:displayeRC(document.form1,'record_id[]','display_eRC_update.php', 0)" class="tool_reject"><?=$Lang['iPortfolio']['OLE']['HideIneRC']?></a>
                <? endif;?>
                &nbsp;
                |
                &nbsp;
                <a href="javascript:jBATCH_CHANGE_YEAR()" class="tool_edit"><?=$ec_iPortfolio['edit']?> <?=$ec_iPortfolio['year']?></a>
                <a href="javascript:checkEdit(document.form1,'record_id[]','ole_new.php',0)" class="tool_edit"><?=$ec_iPortfolio['edit']?></a>
                <a href="javascript:checkMerge()" class="tool_edit"><?=$ec_iPortfolio['Merge']?></a>
                <a href="javascript:checkRemove(document.form1,'record_id[]','ole_remove.php')" class="tool_delete"><?=$ec_iPortfolio['delete']?></a>

            </div>

<?}?>
<!-- end of action bar -->            
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <?=$table_content?>
      		</td>
      	</tr>
      </table>
    </td>
  </tr>
</table>
<input type="hidden" name="Action" />
<input type="hidden" name="approve" />
<input type="hidden" name="rejectType" />
<input type="hidden" name="pageNo" value="<?= $pageNo ?>" />
<input type="hidden" name="order" value="<?= $order ?>" />
<input type="hidden" name="field" value="<?= $field ?>" />
<input type="hidden" name="numPerPage" value="<?= $numPerPage ?>" />
<input type="hidden" name="page_size_change" />
<input type="hidden" name="FromPage" value="program" />
<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

<? if($sys_custom['iPf']['displayIneRC'] && ($IntExt==0 || $IntExt=='') ){ ?>
<input type="hidden" name="displayIneRC" />
<input type="hidden" name="eRCBatch" value="1" />
<? } ?>
</FORM>