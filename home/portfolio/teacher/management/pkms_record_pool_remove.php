<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

// Default: no effect on processing OLE Record
$IntExt = 0;

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$OLERecArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt, 1);
$OLERecArrNew = array_values(array_diff($OLERecArr, $record_id));

$LibPortfolio->SET_OLE_RECORD_ORDER_SLP($StudentID, $OLERecArrNew, $IntExt, 1);

intranet_closedb();

header("Location: pkms_record_pool.php?msg=delete&IntExt=".$IntExt."&StudentID=".$StudentID);
?>