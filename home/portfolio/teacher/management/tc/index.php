<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

/**********************************************************************
 * This page is mainly divided into THREE parts ==> INPUT, PROCESS AND OUTPUT.
 * Please put the code in corresponding parts for clearance.
 **********************************************************************/
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	get POST data
//~ data for libdbtable settings
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

//~ other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$YearClassID = $_POST["YearClassID"];
###############################################



/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PROCESS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
if(strstr($ck_user_rights, ":manage:"))	//	iportfolio admin
{
	$IS_MANAGE = true;
} else {
	$IS_MANAGE = false;
}
###############################################
##	HTML - class_selection
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
if($IS_MANAGE)	//	iportfolio admin
{
//	$groupIds = $libpf_slp->getUserGroupsInTeacherComment();
} else {
	$groupIds = $libpf_slp->getUserGroupsInTeacherComment($UserID);
}
//debug_r($groupIds);
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
//debug_r($userClassInfo);
$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);
$html["class_selection"] = $libportfolio_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' id='YearClass' onChange='reload();'",$YearClassID);
###############################################


###############################################
##	HTML - keyword
$html["keyword"] = stripslashes(trim($keyword));
###############################################


###############################################
##	Construct the display table
//~ Table basic settings	--------------------------||
$libdbtable2007 = new libdbtable2007($field, $order, $pageNo);
$libdbtable2007->page_size = $numPerPage;	//	the constructor does not provide setting page size, which is the number of records display per page

//~ Table headers			--------------------------||
$columnCount=0;
$libdbtable2007->column_list .= "<th width='1'>#</th>\n";
$libdbtable2007->column_list .= "<th nowrap>".$libdbtable2007->column($columnCount++, $i_UserClassName." - ".$i_UserClassNumber)."</th>\n";	//	column($fieldIndex,$columnTitle)
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $i_UserName)."</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $ec_iPortfolio['teacher_comment'])."</th>\n";
$libdbtable2007->column_list .= "<th nowrap>".$libdbtable2007->column($columnCount++, $Lang['General']['LastModifiedBy'])."</th>\n";
$libdbtable2007->column_list .= "<th>".$libdbtable2007->column($columnCount++, $Lang['General']['LastModified'])."</th>\n";
$libdbtable2007->column_list .= "<th width='1'>".$libdbtable2007->check("YearClassUserID[]")."</th>\n";


//~	Get submission period	--------------------------||
$student_ole_config_file = "$eclass_root/files/teacher_comment_config.txt";
$filecontent = trim(get_file_content($student_ole_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);
$startTimestamp = strtotime($starttime." ".$sh.":".$sm.":"."00");
$endTimestamp = strtotime($endtime." ".$eh.":".$em.":"."00");
$currentTimestamp = time();

//~	Setting content			--------------------------||
$keyword = addslashes($keyword);
$coursedb = classNamingDB($ck_course_id);
$currentAcademicYearId = Get_Current_Academic_Year_ID();
$userNameField = getNameFieldByLang("IU.");
$lastUpdateByField = getNameFieldByLang("IU2.");
$ClassNumberField = getClassNumberField("YCU.");
$classAndNumber = " CONCAT(YC.CLASSTITLE".strtoupper($intranet_session_language).", ' - ', $ClassNumberField) ";

if (!empty($YearClassID)) {
	$cond .= " AND YC.YEARCLASSID = $YearClassID ";
}
if (!empty($groupIds)) {
	$groupsCond = "AND ug.group_id IN (".implode(",",$groupIds).")";
	$groupsJoinCond = "
INNER JOIN {$coursedb}.user_group ug 
	ON um.user_id = ug.user_id
INNER JOIN {$coursedb}.mgt_grouping_function mgf 
	ON ug.group_id = mgf.group_id AND INSTR(mgf.function_right, 'Profile:TeacherComment') > 0
	";
} else {
	
}

$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"YearClassUserID[]\" value=\"', YCU.YEARCLASSUSERID,'\">')";


if (!empty($starttime)) {
	$html["out_of_period"] = $iPort['period_start_end']." ".date("Y-m-d H:i:s",$startTimestamp);
}
if (!empty($endtime)) {
	$html["out_of_period"] .= " - ".date("Y-m-d H:i:s",$endTimestamp);
}

$sql = "
SELECT
	$classAndNumber AS CLASSANDNUMBER,
	$userNameField AS USERNAME,
	REPLACE(/* replacing the new line character */
		IF (SC.COMMENT IS NULL || SC.COMMENT = '','--',SC.COMMENT),'\\n','<br/>'
	) AS COMMENT,
	IF (SC.LASTUPDATEBY IS NULL || SC.LASTUPDATEBY = '','--',$lastUpdateByField) AS LASTUPDATEBY,
	IF (SC.MODIFIEDDATE IS NULL || SC.MODIFIEDDATE = '','--',SC.MODIFIEDDATE) AS MODIFIEDDATE,
	$checkBoxField
FROM {$intranet_db}.INTRANET_USER IU
INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON IU.USERID = PS.USERID AND PS.ISSUSPEND = 0 
LEFT JOIN {$eclass_db}.STUDENT_COMMENT SC
	ON SC.STUDENTID = IU.USERID
LEFT JOIN {$intranet_db}.INTRANET_USER IU2
	ON SC.LASTUPDATEBY = IU2.USERID
INNER JOIN {$intranet_db}.YEAR_CLASS_USER YCU
	ON YCU.USERID = IU.USERID
INNER JOIN {$intranet_db}.YEAR_CLASS YC
	ON YCU.YEARCLASSID = YC.YEARCLASSID
INNER JOIN {$intranet_db}.YEAR Y
	ON YC.YEARID = Y.YEARID
INNER JOIN {$eclass_db}.user_course uc 
	ON uc.user_email = IU.USEREMAIL
INNER JOIN {$coursedb}.usermaster um 
	ON um.user_id = uc.user_id 
$groupsJoinCond
WHERE YC.ACADEMICYEARID = $currentAcademicYearId
	AND (IU.RECORDTYPE=".TYPE_STUDENT." AND IU.RECORDSTATUS = ".STATUS_APPROVED.")
	$cond
	$groupsCond
	AND uc.course_id = $ck_course_id
	AND ($classAndNumber LIKE '%$keyword%'
		OR $userNameField LIKE '%$keyword%'
		OR SC.COMMENT LIKE '%".htmlspecialchars($keyword)."%'
		OR $lastUpdateByField LIKE '%$keyword%'
		OR SC.MODIFIEDDATE LIKE '%$keyword%')
";
//DEBUG_R(htmlspecialchars($sql));
$libdbtable2007->sql = $sql;

//~ Control Display			--------------------------||
$libdbtable2007->IsColOff = "IP25_table";	//	choosing layout
$libdbtable2007->field_array = array("CLASSANDNUMBER","USERNAME","SC.COMMENT", "SC.LASTUPDATEBY", "SC.MODIFIEDDATE");	//	set the sql field being display
$libdbtable2007->no_col = 1/* row number */+count($libdbtable2007->field_array)+1/* checkbox */;	//	set the number of fields to be displayed, at least 2, becoz the 1st row is used to display the number of rows
$libdbtable2007->column_array = array(0,0,0,0,0);	//	use to control the text style

//~ Get the HTML			--------------------------||
$displayTableHtml = $libdbtable2007->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$libdbtable2007->pageNo}">
<input type="hidden" name="order" value="{$libdbtable2007->order}">
<input type="hidden" name="field" value="{$libdbtable2007->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$libdbtable2007->page_size}">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################


###############################################
##	HTML - display_table (containing the hidden form fields for the table settings)
$html["display_table"] = $displayTableHtml;
###############################################


/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~OUTPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	Preparation before start a new page
$libportfolio = new libportfolio();
$CurrentPage = "Teacher_TeacherComment";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $ec_iPortfolio['teacher_comment'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

###############################################
##	Create the page
$PAGE_NAVIGATION[] = array($CurrentPageName);
$linterface = new interface_html();	//	cannot use $interface_html as variable(i.e. $interface_html = new interface_html();), will cause error, reason: unknown
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
###############################################

intranet_closedb();
?>
