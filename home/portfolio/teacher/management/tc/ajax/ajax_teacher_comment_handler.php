<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");


$Action = $_POST["Action"];
$yearClassUserIds = explode(",",$_POST["yearClassUserIds"]);
$recordId = $_POST["recordId"];
$recordStatus = $_POST["recordStatus"];
$elements = $_POST["details"];
$approvedBy = $_POST["approvedBy"];


$libpf_slp = new libpf_slp();
switch(strtoupper($Action)) {
	default:
	case "LOAD":
		$teacher_comments = $libpf_slp->getTeacherCommentsByYearClassUserIds($yearClassUserIds);
//		debug_r($teacher_comments);die;
		if (is_array($teacher_comments)) {
			$IS_CDATA = true;
			foreach($teacher_comments as $key=>$element) {
				$selfAccount = array();
				$selfAccount[] = simpleXMLembed("studentid",$element["STUDENTID"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("recordid",$element["RECORDID"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("yearclassuserid",$element["YEARCLASSUSERID"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("classtitle",$element["CLASSTITLE"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("classnumber",$element["CLASSNUMBER"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("username",$element["USERNAME"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("comment",$element["COMMENT"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("modifiedDate",$element["MODIFIEDDATE"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("modifiedBy",$element["LASTUPDATEBY"],$IS_CDATA);
				$xmlArray[] = array("teacherComment", implode("",$selfAccount));
			}
		}
		$XML = generate_XML(
					$xmlArray, false, false
				);
//		debug_r($xmlArray);
		break;
	case "UPDATE":
		$updateResult = $libpf_slp->saveTeacherComment($studentId, $comment, $UserID);
		$XML = generate_XML(
						array(
							array("result", ($updateResult?1:0) )
						)
				);
		break;
}

header("Content-Type:   text/xml");

echo $XML;
?>