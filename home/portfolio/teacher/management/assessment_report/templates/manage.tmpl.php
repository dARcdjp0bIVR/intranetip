<script>

function checkform(formObj){
	if($('#title').val() == ""){
		alert('<?=$assignments_alert_msg7?>');
		return false;
	}
	if(formObj.releasedate.value=="" ){
		alert('<?=$iPort['Assessment']['Error']['ReleaseDate']?>');
		return false;
	}
	return true;
}

function updateYearClassSelection(obj){
	if(obj.value==""){
		$('#YearClassID').val('all');
	}else{
		$('#YearClassID').val(obj.value);
	}
}
</script>

<form name="form1" method="post" action="assessment_report_manage_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">
	<?= $html["navigation"]?>
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5"
		align="center">
		<tbody>
			<tr>
				<td valign="top">
					<!-- CONTENT HERE -->

					<div class="table_board">
						<span class="sectiontitle_v30"> <?=$iPort['menu']['assessment_report']?></span><br>

						<table align="center" width="100%" border="0" cellpadding="5"
							cellspacing="0">
							<tbody>
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle"><span
										class="tabletext"><?=$iPort['Assessment']['Title']?><span class="tabletextrequire">*</span></span>
									</td>
									<td width="80%" valign="top">
										<table border="0" cellpadding="0" cellspacing="0" width="60%">
											<tbody>
												<tr>
													<td width="100%">
														<input autocomplete="off" id="title" name="title" type="text" size="50" value="<?=$title?>" class="textboxtext ac_input" maxlength="255">
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>

								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle"><span
										class="tabletext"><?=$iPort['Assessment']['ReleaseDate']?><span class="tabletextrequire">*</span></span></td>
									<td>

										<table border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<?= $linterface->GET_DATE_PICKER("releasedate", $releasedate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1); ?>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<span class="tabletext"><?=$iPort['Assessment']['Target'] ?></span></td>
									<td>

										<table border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<?=$html["class_selection"]?>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								
								<tr>
									<td colspan="2" >
										<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
										</table>

										<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
											<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$ec_iPortfolio['mandatory_field_description']?></td></tr>
											<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
											<tr>
												<td align="center">
													<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
													<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<!-- End of CONTENT -->
	</div>
	</td>
	</tr>
	</tbody>
	</table>

	<input type="hidden" name="action" value="<?=$html["action"]?>">
	<input type="hidden" name="assessmentId" value="<?=$assessment_id?>">
	<input type="hidden" name="callback" value="<?=$callback?>">
</form>
