<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.form.js"></script>
<form name="form1" action="" method="POST">
	<?= $html["navigation"] ?>
	<br style="clear: both;" />	
	<div class="content_top_tool">
		<div>
			<table>
			<tr>
			<td>
				<b><?=$html["title"]?></b>
			</td>
			<td nowrap="">
				<a href="assessment_report_manage.php?assessment_id[]=<?=$assessment_id?>&callback=detail" class="contenttool">
				<img src="../../../../../images/2009a/icon_edit.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_edit?>
				</a>
			</td>
			</tr>
			<tr>
			<td>
			<?=$iPort['Assessment']['ReleaseDate']?>: <?=$html['ReleaseDate']?>
			</td>
			</tr>
			</table>
		</div>
		<div class="Conntent_search"><input type="text" name="keyword" value="<?= $html["keyword"] ?>"></div><br style="clear: both;" />
	</div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			
		</tr>
		<tr>
			<td valign="bottom">                                 
				<div class="table_filter"><?= $html["class_selection"] ?></div>
				<div class="table_filter"><?= $html["status_selection"] ?></div>
				<p class="spacer" />
				<br style="clear:both">
			</td>
			<td valign="bottom">
				
			</td>
		</tr>
	</table>
	<?= $html["display_table"] ?>
</form>
<form id="filuploadForm" action="assessment_file_manage.php" method="POST" enctype='multipart/form-data' style="display:none;">
	<input type='hidden' name='assessment_id' value='<?=$assessment_id?>'>
	<input type='hidden' id='upload_form_student_id' name='student_id' value=''>
	<input type='hidden' name='action' value='uploadFile'>
	<input type='file' name='upload_file' id='fileinput'/>
</form>
<script>
var assessment_id = '<?=$assessment_id?>';
var sizeToChange = $('.common_table_list_v30  tr').size();
var uploader = new Array(sizeToChange);
var studentList = new Array(sizeToChange);
for(var i=1; i < sizeToChange; i++){
	var obj = $('.common_table_list_v30  tr')[i].getElementsByTagName('td')[2];
	var data = obj.getElementsByTagName('data')[0].innerHTML.split('?=?');
	var view = obj.getElementsByTagName('view')[0].innerHTML;
	var upload = obj.getElementsByTagName('upload')[0].innerHTML;
	var link = "assessment_file_manage.php?action=readFile&assessmentId="+assessment_id+"&studentId="+data[1];
	
	if(view=='show'){
		view = "display:block;";		
	}else{
		view = "display:none;";
	}
	if(upload=='show'){
		upload = "display:block;";
	}else{
		upload = "display:none;";
	}
	
	var content = "<span id='view"+data[1]+"' class='view_attachment' style='"+view+"'>";
	content += "<a href='"+link+"' class='file_attachment'><?=$ec_iPortfolio['view_analysis']?></a>";
	content += "<div class='table_row_tool'><a href='#' class='delete_dim' title='<?=$legend_desc3?>' value='"+data[1]+"'></a></div></span>";
	content +=	"<div id='uploader_container_"+data[1]+"'><input type='button' class='assessment_upload_btn formsmallbutton' value='<?=$iPort["upload"]?>' id='uploader_button_"+data[1]+"' onclick='fileUpload("+data[1]+")' style='position: relative; z-index: 0;"+upload+"'></div>";
	obj.innerHTML = content;
	studentList[i] = data[1];
}
$('.delete_dim').click(function(e){
	var student_id=$(this).attr('value');
	$.post(
		'assessment_file_manage.php',
		{
			'action': 'removeFile',
			'student_id': student_id,
			'assessment_id': assessment_id
		},
		function(feedback){
			$('#view'+student_id).css('display','none');
			$('#uploader_button_'+student_id).css('display','block');
			$('#date_'+student_id).html('');
		}
	);
});
$(document).ready(function() { 
	$('#filuploadForm').ajaxForm({
		complete: function(xhr){
			var callback = xhr.responseText.replace(/<\/?[^>]+(>|$)/g, "");
			var result = callback.split('@=@');
			var student_id = $('#upload_form_student_id').val();
			if(result[0]=='false'){
				alert('fail to upload: '+result[1]);
			}else if(result[0]=='done'){
				$('#view'+student_id).css('display','block');
				$('#uploader_button_'+student_id).css('display','none');
				var currentdate = new Date();
				if(currentdate.getDate() < 10){
					var dateWithZero = "0" + currentdate.getDate();
				}else{
					var dateWithZero = currentdate.getDate();
				}
				$('#date_'+student_id).html(currentdate.getFullYear()+"-"+(((currentdate.getMonth()+1) < 10)?"0":"")+(currentdate.getMonth()+1)+"-"+dateWithZero);
			}
			$("#filuploadForm").resetForm();
		}
	});
	$('#fileinput').change(function(){		
		$('#filuploadForm').submit();
	});
});
function fileUpload(student_id){
	$('#upload_form_student_id').val(student_id);
	$('#fileinput').click();
}
</script>
<style>
.view_attachment a.file_attachment{
	float:left;
}
</style>