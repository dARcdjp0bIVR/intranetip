<?php

// Modified by Paul

/********************** Change Log ***********************/

/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-assessment-report.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
iportfolio_auth("T");
intranet_opendb();

// Collect Parameters
$assessment_id = isset($_REQUEST['assessment_id'])?$_REQUEST['assessment_id'][0]:'NEW';
$callback = isset($_REQUEST['callback'])?$_REQUEST['callback']:'';
$lpf = new libportfolio();
$libpf_ar = new libpf_assessment_report();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();

###############################################


# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_AssessmentReport";
$CurrentPageName = ($assessment_id=='NEW')?$iPort['Assessment']['NewAssessment']:$iPort['Assessment']['EditAssessment'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['assessment_report'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Retrieve Edit Information #########
if($assessment_id == 'NEW'){
	$YearClassID = "";
	$title = "";
	$releasedate = "";
}else{
	$assessment_info = $libpf_ar->getAssessmentInfo($assessment_id);
	$YearClassID = $assessment_info[2];
	$title = $assessment_info[0];
	$releasedate = date('Y-m-d',strtotime($assessment_info[1]));
}

######## Display #########
$PAGE_NAVIGATION[] = array($CurrentPageName);
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
if($IS_MANAGE)	//	iportfolio admin
{
	//	$groupIds = $libpf_slp->getUserGroupsInTeacherComment();
} else {
	$groupIds = $libpf_slp->getUserGroupsInTeacherComment($UserID);
}
//debug_r($groupIds);
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);
$isDisable = ($assessment_id=='NEW')?'':'disabled'; 
$html["class_selection"] = $libportfolio_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' id='YearClass' ".$isDisable,$YearClassID, true, false, array('all', $iPort['Assessment']['AllClasses']));

$html["action"] = ($assessment_id=='NEW')?'addReport':'editReport';

$linterface->LAYOUT_START();
include_once("templates/manage.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
intranet_closedb();
?>