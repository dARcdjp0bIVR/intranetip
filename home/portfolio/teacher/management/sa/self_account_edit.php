<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

/**********************************************************************
 * This page is mainly divided into THREE parts ==> INPUT, PROCESS AND OUTPUT.
 * Please put the code in corresponding parts for clearance.
 **********************************************************************/
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~INPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	get POST data
//~ other parameters
$YearClassUserID = $_POST["YearClassUserID"];

###############################################



/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PROCESS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	JS - student_id_to_load
if (is_array($YearClassUserID)) {
	$id_to_load = implode(",",$YearClassUserID);
}
$js["year_class_user_id_to_load"] = "var year_class_user_id_to_load = [".$id_to_load."];";
###############################################


###############################################
##	JS - recordStatusInputBox and approvalDetailsField
$libportfolio = new libportfolio();
if ($libportfolio->isUseApprovalInSelfAccount()) {
	$js["recordStatusInputBox"] = <<<JSEND
						\$recordStatusInputBox = \$("<input/>").attr("type","checkbox").attr("id","selfAccount_"+studentid+"_recordstatus").val(1)
						if (Number(recordstatus)) {
							\$recordStatusInputBox.attr("checked","checked");
						}
JSEND;


	$approvedByWord = $ec_iPortfolio['approved_by'].": ";
	$approvedDateWord = $ec_iPortfolio['process_date'].": ";
	$approvedBySpan = '.append($("<span></span>").attr("id","selfAccount_"+studentid+"_approvedBy").text(approvedby))';
	$approvedDateSpan = '.append($("<span></span>").attr("id","selfAccount_"+studentid+"_approvedDate").text(approveddate))';
	$inputBoxAppend = <<<JSEND
														.append(\$recordStatusInputBox)
														.append(\$("<label></label>").attr("for","selfAccount_"+studentid+"_recordstatus").html("<b>$button_approve</b>"))
JSEND;
} else {
	$approvedByWord = "&nbsp;";
	$approvedDateWord = "&nbsp;";
}
	$js["approvalDetailsField"] = <<<JSEND
$inputBoxAppend
											).append(\$("<td></td>").attr("nowrap","nowrap").attr("width","30%").html("<b>{$approvedByWord}</b>"){$approvedBySpan})
											.append(\$("<td></td>").attr("nowrap","nowrap").attr("width","25%").html("<b>{$approvedDateWord}</b>"){$approvedDateSpan})
JSEND;
###############################################

###############################################
##	JS - 
###############################################

###############################################
##	JS - 
###############################################

###############################################
##	Construct FCKeditor Template
# Components size
$msg_box_width = "100%";
$msg_box_height = 200;
$obj_name = "{{{ OBJ_NAME }}}";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;
$init_html_content = "{{{ CONTENT }}}";

$page_break_note = ($sys_custom["ywgs_ole_report"]) ? $Lang['iPortfolio']['page_break_ywgs'] : "&nbsp;";

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');

$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
$objHtmlEditor->Value = $init_html_content;
ob_start();
$objHtmlEditor->Create(); 
$fckeditorTemplate = addslashes(ob_get_contents());
ob_end_clean();
###############################################

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~OUTPUT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
###############################################
##	Preparation before start a new page
$CurrentPage = "Teacher_SelfAccount";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $button_edit.(strtoupper($intranet_session_language)=="EN"?" ":"").$ec_iPortfolio['self_account'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

###############################################
##	Create the page
$PAGE_NAVIGATION[] = array($ec_iPortfolio['self_account'], "index.php");
$PAGE_NAVIGATION[] = array($CurrentPageName);
$linterface = new interface_html();	//	cannot use $interface_html as variable(i.e. $interface_html = new interface_html();), will cause error, reason: unknown
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$linterface->LAYOUT_START();
include_once("templates/self_account_edit.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
###############################################

intranet_closedb();
?>