<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-15	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
$libportfolio = new libportfolio();
$limport = new libimporttext();
$lfs = new libfilesystem();

#################################### CSV Checking(Start) ####################################
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lfs->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_step1.php?xmsg=import_failed");
	exit();
}

### move to temp folder first for others validation
$folderPrefix = $intranet_root."/file/import_temp/iportfolio/self_account";
if (!file_exists($folderPrefix)) {
	$lfs->folder_new($folderPrefix);
}
$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folderPrefix."/".$targetFileName);
$successAry['MoveCsvFileToTempFolder'] = $lfs->lfs_move($csvfile, $targetFilePath);

$columnTitleAry = $libpf_slp->getImportSelfAccountHeader();
$columnPropertyAry = $libpf_slp->getImportSelfAccountColumnProperty();
$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $columnTitleAry, $columnPropertyAry);
$csvColName = array_shift($csvData);
$numOfData = count($csvData);
#################################### CSV Checking(End) ####################################

### iFrame for validation
$thisSrc = "ajax_task.php?task=validateSelfAccountImport&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($ec_iPortfolio['self_account'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);

### Block UI Msg
$processingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfData, $Lang['General']['ImportArr']['RecordsValidated']);

### Buttons
$htmlAry['importBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "goImport();", 'ImportBtn', '', $Disabled=1);
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");

### Top Info Table
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td><div id="SuccessCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;

###############################################
##	Preparation before start a new page
$CurrentPage = "Teacher_SelfAccount";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $ec_iPortfolio['self_account'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################
$linterface->LAYOUT_START($ReturnMsg);
?>
<script type="text/javascript">
$(document).ready( function() {
	Block_Document('<?=$processingMsg?>');
});

function goCancel() {
	window.location = 'index.php';
}

function goBack() {
	window.location = 'import_step2.php';
}

function goImport() {
	$('form#form1').attr('action', 'import_step3.php').submit();
}

</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<?=$htmlAry['iframe']?>
		<div id="ErrorTableDiv"></div>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['importBtn']?>
		<?=$htmlAry['backBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>