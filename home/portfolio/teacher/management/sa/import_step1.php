<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-15	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
$libportfolio = new libportfolio();

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($ec_iPortfolio['self_account'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);

### Import Format
$ColumnTitleArr[] = $ec_iPortfolio['ClassName'];
$ColumnTitleArr[] = $ec_iPortfolio['export']['ClassNumber'];
$ColumnTitleArr[] = $ec_iPortfolio['WebSAMSRegNo'];
$ColumnTitleArr[] = $i_general_title;
$ColumnTitleArr[] = $ec_iPortfolio['self_account'];
$ColumnPropertyArr = array(3,3,3,1,1);

$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$htmlAry['columnRemarks'] = $ImportPageColumn;

### Get CSV Button
$csvFile = "<a class='tablelink' href='". 'import_template.php' ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### form table
$x = '';
$x .= '<table id="html_body_frame" width="100%">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
					$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
					$x .= '<td>'.$building_selection.'<a class="tablelink" href="javascript:goDownloadSample();">'. $Lang['General']['ClickHereToDownloadSample'] .'</a></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $htmlAry['columnRemarks']."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tabletextremark" colspan="2">'."\n";
						$x .= '<p>'.$Lang['General']['ImportArr']['ClassNameClassNum_WebSAMS'].'</p>';
						$x .= $linterface->MandatoryField();
						
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['continueBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Continue'], "button", "goSubmit()", 'sumbitBtn');
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');


###############################################
##	Preparation before start a new page
$CurrentPage = "Teacher_SelfAccount";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $ec_iPortfolio['self_account'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	if(checkForm()==true){
		$('form#form1').attr('action', 'import_step2.php').submit();	
	}
}

function goDownloadSample() {
	$('form#form1').attr('action', 'import_template.php').submit();
}

function checkForm(){
	if($("#csvfile").val() == ''){
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	else{
		return true;
	}
	
}
</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['continueBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>