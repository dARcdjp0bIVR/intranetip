<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-15	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();
$libportfolio = new libportfolio();

$importDataAry = $libpf_slp->getSelfAccountImportTempData();

foreach($importDataAry as $_studentData){
	$_studentID = $_studentData['StudentID'];

	$ParSelfAccountArr['UserID'] = $_studentID;
	$ParSelfAccountArr['Title'] = $libpf_slp->Get_Safe_Sql_Query($_studentData['Title']);
	$ParSelfAccountArr['Details'] = $libpf_slp->Get_Safe_Sql_Query($_studentData['Details']);
	
	$haveDefaultSA = $libpf_slp->GET_SELF_ACCOUNT_LIST($_studentID,1);

	$RecordID = '';
	if($haveDefaultSA){
		// Update
		$ParSelfAccountArr['RecordID'] = $haveDefaultSA[0]['RecordID'];
		$successArr[] = $libpf_slp->EDIT_SELF_ACCOUNT($ParSelfAccountArr);
	}
	else{
		// Add New
		$successArr[] = $libpf_slp->ADD_SELF_ACCOUNT($ParSelfAccountArr);
		$RecordID = $libpf_slp->db_insert_id();
		$libpf_slp->SET_SELF_ACCOUNT_DEFAULTSA($_studentID,$RecordID);
	}

}

if(in_array(false,$successArr)){
	$numSuccess = 0;
}
else{
	$numSuccess = count($successArr);
}

###############################################
##	Preparation before start a new page
$CurrentPage = "Teacher_SelfAccount";	//	set the current page in the $MODULE_OBJ
$MODULE_OBJ = $libportfolio->GET_MODULE_OBJ_ARR("Teacher");	//	Get the MODULE_OBJ in specific module (in this case get from libportfolio), which is used in $linterface to show the left menu

$CurrentPageName = $ec_iPortfolio['self_account'];	//	set the current page name  
$TAGS_OBJ[] = array($CurrentPageName,"");	//	Put the current page name to the $TAGS_OBJ, which is used in $linterface to show the page name
###############################################

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($ec_iPortfolio['self_account'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "goCancel();");

$linterface->LAYOUT_START();
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function goCancel() {
	window.location = 'index.php';
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$numSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>