<?php
/** [Modification Log] Modifying By: 
 * ************************************************************
 * 2015-01-26 (Bill): replace urlencode() by rawurldecode() after using js encodeURIComponent() 
 * 2015-01-09 (Omas):  is removed by the script which lead to js error
 * 2014-12-12 (Bill): return record ID if insert new records
 * ************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");


$Action = $_POST["Action"];
$yearClassUserIds = explode(",",$_POST["yearClassUserIds"]);
$recordId = $_POST["recordId"];
$recordStatus = $_POST["recordStatus"];
$title = $_POST["title"];
$details = $_POST["details"];
$approvedBy = $_POST["approvedBy"];
$studentId = $_POST["studentId"];
$isSetAsDefaultValue = $_POST["isSetAsDefaultValue"];

$libpf_slp = new libpf_slp();
switch(strtoupper($Action)) {
	default:
	case "LOAD":
		$self_accounts = $libpf_slp->getSelfAccountsByYearClassUserIds($yearClassUserIds);

//		debug_r($self_accounts);die;
		if (is_array($self_accounts)) {
			$IS_CDATA = true;
			foreach($self_accounts as $key=>$element) {
				$otherSelfAccount = 0;
				$selfAccount = array();
				$selfAccount[] = simpleXMLembed("studentid",$key,$IS_CDATA);
				$detail = $element["ELEMENT"];
				

				$selfAccount[] = simpleXMLembed("yearclassuserid",$detail["YEARCLASSUSERID"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("classtitle",$detail["CLASSTITLE"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("classnumber",$detail["CLASSNUMBER"],$IS_CDATA);
				$selfAccount[] = simpleXMLembed("username",$detail["USERNAME"],$IS_CDATA);
				
				
				$selfAccountsForThisStudent = $libpf_slp->GET_SELF_ACCOUNT_ALL_DATA('',$key);
				if(count($selfAccountsForThisStudent) > 0){
					for($i = 0, $i_max= count($selfAccountsForThisStudent); $i < $i_max; $i++){
						$_defaultSA = $selfAccountsForThisStudent[$i]['DefaultSA'];
						if(strtoupper($_defaultSA) == 'SLP'){
							//do nothing
						}else{
							//this is not a default self account
							$otherSelfAccount = $otherSelfAccount +1;
						}
					}

				}

				//pass back there is how many self account beside "DEFAULT"
				$selfAccount[] = simpleXMLembed("otherSelfAccount",$otherSelfAccount,$IS_CDATA);

				if ($element["HAS_DEFAULT"]) {
					$selfAccount[] = simpleXMLembed("recordid",$detail["RECORDID"],$IS_CDATA);
					$selfAccount[] = simpleXMLembed("has_default",$element["HAS_DEFAULT"],$IS_CDATA);
					$selfAccount[] = simpleXMLembed("recordstatus",$detail["RECORDSTATUS"],$IS_CDATA);
					if ($detail["RECORDSTATUS"]) {
						if (strtoupper($intranet_session_language)=="EN") {
							$nameField = "ENGLISHNAME";
						} else {
							$nameField = "CHINESENAME";
						}
						$nameField = getNameFieldByLang();
						$sql = "SELECT $nameField FROM {$intranet_db}.INTRANET_USER WHERE USERID = '".$detail["APPROVEDBY"]."'";
						$approvedByName = current($libpf_slp->returnVector($sql));
						$approvedDate = $detail["APPROVEDDATE"];
					} else {
						$approvedByName = "--";
						$approvedDate = "--";
					}
					$thisDetails = str_replace('</p>',"\n",$detail["DETAILS"]);
					$thisDetails = str_replace('<br />',"",$thisDetails);	
					$thisDetails = str_replace("\x0B","\n",$thisDetails);
					$thisDetails = str_replace("","",$thisDetails);		
						
					$thisDetails = strip_tags($thisDetails);
			//		$detail["DETAILS"]  = html_entity_decode($thisDetails,ENT_QUOTES);
					$detail["DETAILS"]  = $thisDetails;
					
					$IS_SPC_CHAR = true;
					$selfAccount[] = simpleXMLembed("approvedby",$approvedByName,$IS_CDATA);
					$selfAccount[] = simpleXMLembed("approveddate",$approvedDate,$IS_CDATA);
					$selfAccount[] = simpleXMLembed("title",$detail["TITLE"],$IS_CDATA,$IS_SPC_CHAR);
					$selfAccount[] = simpleXMLembed("details",trim($detail["DETAILS"]),$IS_CDATA,$IS_SPC_CHAR);
					
				}
				$xmlArray[] = array("selfAccount", implode("",$selfAccount));
//				$xmlArray[] = array("faifai", 4);
			}
		}
		$XML = generate_XML(
					$xmlArray, false, false
				);
//		debug_r($xmlArray);
		break;
	case "UPDATE":
	$details = nl2br($details);
	// $details = urldecode($details);
	// 2015-02-03 - use rawurldecode()
	$title = rawurldecode($title);
	$details = rawurldecode($details);
//	str_replace("%26","&",$details)
		$updateResult = $libpf_slp->saveSelfAccount($recordId,$studentId,$recordStatus,$title,$details, $approvedBy,$isSetAsDefaultValue);
		
		# Handle new self account by teacher
		if(empty($recordId)){
			# Get new self account record ID
			$new_RecordID = $libpf_slp->db_insert_id();
			$XML = generate_XML(
							array(
								array("result", ($updateResult? 1:0) ),
								array("new_recordID", $new_RecordID )
							)
					);
		}
		else {
			$XML = generate_XML(
							array(
								array("result", ($updateResult? 1:0) )
							)
					);
		}
		break;
}

header("Content-Type: text/xml");

echo $XML;
?>