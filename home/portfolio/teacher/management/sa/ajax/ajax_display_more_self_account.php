<?php
/** [Modification Log] Modifying By: fai
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


$studentID = $_POST["studentID"];

$linterface = new interface_html();

$libpf_slp = new libpf_slp();
$SelfAccArr = $libpf_slp ->getSelfAccounts_noDefault($studentID);

$html = '';

$html .= '<table width="100%">';


for($i=0,$i_MAX=count($SelfAccArr);$i<$i_MAX;$i++)
{
	$RecordID = $SelfAccArr[$i]["RECORDID"];
	$Title = $SelfAccArr[$i]["TITLE"]; 
	$Details = $SelfAccArr[$i]["DETAILS"]; 
	$Details = strip_tags($Details);
	
	$ApproveBy = $SelfAccArr[$i]["APPROVEDBY"];
	$ApproveDate = $SelfAccArr[$i]["APPROVEDDATE"];

	
	$ApproveBy = ($ApproveBy=='')? '--':$ApproveBy;
	$ApproveDate = ($ApproveDate=='')? '--':$ApproveDate;
	
	$SaveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', "UpdateSelfAccountByRecordId('$RecordID', '$studentID');", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="");
	
	$bottom_left_html ='';
	
	$bottom_left_html .='<table width="100%">
							<tr>
								<td>
									<input type="radio" name="radioDefault_'.$studentID.'"  id="radioDefault_'.$RecordID.'" value="1"/> <label for="radioDefault_'.$RecordID.'"><b>'.$iPort["select_default_sa_teacher"].'</b></label>
								</td>
								<!--td><input type="checkbox" value="" name="Approve_'.$RecordID.'" id="Approve_'.$RecordID.'" ></input> <label for="Approve_'.$RecordID.'"><b>Approve</b></label></td>

								<td><b>Approved By:</b>'.$ApproveBy.'</td>

								<td><b>Process Date:</b>'.$ApproveDate.'</td-->
							</tr>
						</table>';
	
	
	$html .= '<tr>
				<td colspan="2">

				<span id="selfAccount_Status_'.$RecordID.'" style="float:right"></span>

				<div id="default_div_student_'.$studentID.'_'.$RecordID.'"></div>
					'.$i_general_title.' :
					<input type="text" value="'.intranet_htmlspecialchars($Title).'" name="title_'.$RecordID.'" id="title_'.$RecordID.'" style="width:80%"></input>
				</td>
			  </tr>
			  <tr>
				<td colspan="2">
					<textarea width="100%" name="content_'.$RecordID.'" id = "content_'.$RecordID.'"  cols="145" rows="16" wrap="virtual">'.$Details.'</textarea>
				</td>
			  </tr>
			  <tr>
				<td>
					'.$bottom_left_html.'
				</td>
				<td align="right">
					'.$SaveButton.'
				</td>
			  </tr>';
	$html .='<tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
			  </tr>';
}

$html .= '</table>';

echo $html;
?>