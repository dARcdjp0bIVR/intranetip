<?php
// Modifing by: Connie
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");



# Page Authentication
$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Profile:ExportData"));
iportfolio_auth("T");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-slp.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();

$lpf_selfAcc = new libpf_self_account();

$lexport = new libexporttext();

$YearClassID = $_POST['YearClassID'];
//$Selection = $_POST['Selection'];  //Selected or All
$groupsIDArr = $_POST['groupIds'];


$recordsPerLoop = 10000;
$startIndex = 0;

//debug_r($YearClassID);

# print the report title				
$filename = "Self_Account".$YearClassID.".csv";
$isXLS = false;		

$TitleArr = array();
$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['RegNo'];
$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['StdName'];
$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassName'] ;
$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['ClassNo'];
$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAccPrimary']; 

$MaxSA_count = $lpf_selfAcc->getSelfAccounts_MaxCount($YearClassID,$groupsIDArr);

for($i=1;$i<$MaxSA_count;$i++)
{
	$TitleArr[] = $Lang['iPortfolio']['SelfAccountArr']['exportArr']['SelfAcc']." ".($i+1);
}


$DataArray = $lpf_selfAcc->getSelfAccounts_Export($YearClassID,$groupsIDArr);


$ExportContent = implode("\t",(array)$TitleArr);
$ExportContent .="\n";
//debug_r('here'); 
$lexport->EXPORT_FILE($filename, $ExportContent, $isXLS, $ignoreLength=true);	



$DisplayArr = array();

if(count($DataArray)>0)
{
	$_UserID='begin';
}

$thisNo = 0;
for($i=0,$i_MAX=count($DataArray);$i<$i_MAX;$i++)
{		
	$_detail = strip_tags($DataArray[$i]['Details']);
	$_detail = (string)str_replace(array("\r", "\r\n", "\n","&nbsp;"), ' ', $_detail);
	
	if($DataArray[$i]['UserID']==$_UserID)
	{
		
		if($DataArray[$i]['DefaultSA']=='')
		{
			$DisplayArr[$thisNo]['SelfAccOther'][] = $_detail;
		}
		else
		{
			$DisplayArr[$thisNo]['SelfAccPrimary'] = $_detail;
		}
		
	}
	else
	{			
		$DisplayArr[$i]['RegNo'] = $DataArray[$i]['RegNo'];	
		$userNameField = Get_Lang_Selection($DataArray[$i]['ChiName'],$DataArray[$i]['EngName']);
		$DisplayArr[$i]['StdName'] =$userNameField;
		$DisplayArr[$i]['ClassName'] = $DataArray[$i]['ClassName'];
		$DisplayArr[$i]['ClassNo'] = $DataArray[$i]['ClassNo'];
		$DisplayArr[$i]['SelfAccPrimary'] = '';
			
		if($DataArray[$i]['DefaultSA']=='')
		{
			$DisplayArr[$i]['SelfAccOther'][] =$_detail;
		}
		else
		{
			$DisplayArr[$i]['SelfAccPrimary'] = $_detail;
		}
		$thisNo = $i;
	}

	$_UserID=$DataArray[$i]['UserID'];
}

//debug_r($DisplayArr);

foreach((array)$DisplayArr as $key=>$DisplayInfoArr)
{
	$SelfAccOtherContent = implode("\t",(array) $DisplayInfoArr['SelfAccOther']);
		
	$ExportContent ='';
	$ExportContent .=$DisplayInfoArr['RegNo']."\t";
	$ExportContent .=$DisplayInfoArr['StdName']."\t";
	$ExportContent .=$DisplayInfoArr['ClassName']."\t";
	$ExportContent .=$DisplayInfoArr['ClassNo']."\t";
	$ExportContent .=$DisplayInfoArr['SelfAccPrimary']."\t";
	$ExportContent .=$SelfAccOtherContent;
	
	$ExportContent .="\n";
	$ExportContent = mb_convert_encoding($ExportContent,'UTF-16LE','UTF-8');
	print($ExportContent);
}

	
intranet_closedb();


?>
