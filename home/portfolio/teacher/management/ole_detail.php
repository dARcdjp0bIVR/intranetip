<?php

// Modifing by 
##############
#	Date 20161024 Omas
#	Fix php 5.4 can't show Chinese attachment name problem . changed basename() to get_file_basename()
#############
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

// Initializing classes
$LibPortfolio = new libpf_slp();
$LibUser = new libuser($UserID);

$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";

### Title ###
$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;
$TAGS_OBJ = $TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

$data = $LibPortfolio->RETURN_OLE_RECORD_BY_RECORDID($record_id);
list($startdate, $enddate, $title, $category, $ele, $role, $hours, $organization, $achievement, $details, $ole_file, $approved_by, $remark, $process_date, $status, $user_id, $p_intext, $programid, $subcat_id, $maxhours, $compulfields, $request_approve_by, $IsSAS, $IsOutsideSchool) = $data;


$IsSASText = ($IsSAS) ? $ec_iPortfolio['SLP']['Yes'] : $ec_iPortfolio['SLP']['No'];
$IsOutsideSchoolText = ($IsOutsideSchool) ? $Lang['iPortfolio']['OLE']['Outside_School'] : $Lang['iPortfolio']['OLE']['Inside_School'];



# get ELE Array
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";

$statusArr[1] = $ec_iPortfolio['pending'];
$statusArr[2] = $ec_iPortfolio['approved'];
$statusArr[3] = $ec_iPortfolio['rejected'];

# get category display
$CategoryArray = $LibPortfolio->GET_OLR_CATEGORY();
$category_display = $CategoryArray[trim($category)];

if($startdate=="" || $startdate=="0000-00-00")
	$date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
	$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
	$date_display = $startdate;

$attach_count = 0;
// $tmp_arr = split("\:", $ole_file);
// $tmp_arr = explode("\:", $ole_file);
$tmp_arr = explode(":", $ole_file);
$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$record_id;
$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$record_id;
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
	$attach_file = $tmp_arr[$i];
	if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
	{
		# Eric Yip (20090522): Handle chinese-character file name
		$attach_file_url = urlencode($attach_file);
		$attach_file_url = str_replace(array("%2F", "%26", "+"), array("/", "&", " "), $attach_file_url);
	
		$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
		//$attachments_html .= "<a href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count' class='tablelink' >".basename($attach_file)." ($file_size)</a>";
		//$attachments_html .= "<a href=\"".$folder_url."/".$attach_file_url."\" target='_blank' id='a_$attach_count' class='tablelink' >".basename($attach_file)." ($file_size)</a>";
		$attachments_html .= "<a href=\"".$folder_url."/".$attach_file_url."\" target='_blank' id='a_$attach_count' class='tablelink' >".get_file_basename($attach_file)." ($file_size)</a>";
		$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
		$attach_count ++;
	}
}

# load approval settings
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();
if (empty($IntExt)) {
	$INT_EXT_String = "INT";
} else {
	$INT_EXT_String = "EXT";
}
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($INT_EXT_String);
$flagDisplayHours = (strtoupper($INT_EXT_String) == 'INT') ? true: false;

$LibUserStudent = new libuser($StudentID);
if ($intranet_session_language == "en")
{
	$StudentInfo = $LibUserStudent->EnglishName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}
else
{
	$StudentInfo = $LibUserStudent->ChineseName." (".$LibUserStudent->ClassName."-".$LibUserStudent->ClassNumber.")";
}

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['student_list'], "ole_studentview.php?IntExt=".$IntExt);
$MenuArr[] = array($StudentInfo, "ole_student.php?IntExt=$IntExt&StudentID=$StudentID");
$MenuArr[] = array($title, "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">
</SCRIPT>

<FORM name="form1" method="GET">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation"><?=$navigation_html?></td>				 
			</tr>
			</table>
			</td>
		</tr>
		
		<tr>
			<td align="right">		
						
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr >
					<td nowrap='nowrap' class='formfieldtitle' ><span class="tabletext"><?php echo $ec_iPortfolio['title']; ?></span></td>
					<td class="tabletext" width="80%" ><span class="tabletext"><?=$title?></span></td>
				</tr >
				<tr >
					<td nowrap='nowrap' class='formfieldtitle' ><span class="tabletext"><?=$ec_iPortfolio['date']?> </span></td>
					<td class="tabletext" ><span class="tabletext"><?=$date_display?></span></td>
				</tr>				
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' class='formfieldtitle' ><?php echo $ec_iPortfolio['category']; ?> </td>
					<td><?=$category_display?></td>
				</tr>
				
				<? 	if ($IntExt) {
						// hide component of ole for external program
					} else {
				?>
					<tr valign="middle" class="tabletext">
						<td nowrap='nowrap' valign='top' class='formfieldtitle' ><?php echo $ec_iPortfolio['ele']; ?> </td>
						<td nowrap="nowrap"><?=$ele_display?></td>
					</tr>
				<? } ?>
				
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['ole_role']?> </td>
					<td><?=($role==""?"--":$role)?></td>
				</tr>
<?
if($flagDisplayHours){
?>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['hours']?> </td>
				    <td><?=($hours==""?"--":$hours."&nbsp;<font class='text_11_grey'>".$ec_iPortfolio['unit_hour']."</font>")?></td>
				</tr>
<?
}
?>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['organization']?></td>
				    <td><?=($organization==""?"--":$organization)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap'><?=$ec_iPortfolio['achievement']?></td>
				    <td><?=($achievement==""?"--":$achievement)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' valign="top" class='formfieldtitle' ><?=$ec_iPortfolio['attachment']?></td>
					<td><?=($attachments_html==""?"--":$attachments_html)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['details']?></td>
				    <td><?=($details==""?"--":nl2br($details))?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['status']?></td>
					<td><?=($status!='4'?$statusArr[$status]:$ec_iPortfolio['teacher_submit_record'])?></td>
				</tr>
				<?php
				if($approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Self'])
				{?>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$Lang['iPortfolio']['preferred_approver']?></td>
					<td><?=(($request_approve_by=="" || $request_approve_by==0)?"--":$LibPortfolio->GET_TEACHER_NAME($request_approve_by))?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$Lang['iPortfolio']['approver']?></td>
					<td><?=(($approved_by=="" || $approved_by==0 || $status=="4")?"--":$LibPortfolio->GET_TEACHER_NAME($approved_by))?></td>
				</tr>
				<?php
				}
				?>
				<?php
				if($process_date != "" && $status != 1)
				{?>
				<tr valign="middle" class="tabletext" class='formfieldtitle' >
					<td nowrap='nowrap'  class="formfieldtitle"><?=$ec_iPortfolio['approval_date']?></td>
					<td><?=$process_date?></td>
				</tr>
				<?php
				}
				?>
				<?php
				if($ck_memberType=="T")
				{
				?>
					<tr valign="middle" class="tabletext">
						<td valign="top" nowrap='nowrap'  class="formfieldtitle"><?=$ec_iPortfolio['school_remark']?> </td>
						<td><?=($remark==""?"--":nl2br($remark))?></td>
					</tr>
				<?php
				}
				?>
			<!-- HKUGAC Customization -->
<?php if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
	
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['SAS_with_link']?></span></td>
				<td><?=$IsSASText?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['iPortfolio']['OLE']['In_Outside_School']?></span></td>
				<td><?=$IsOutsideSchoolText?></td>
			</tr>
<?php } ?>
				<tr>
				    <td colspan=2 align=center>
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "history.back();") ?>					
					</td>
				</tr>
				
				</table>
					
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
