<?php

# modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();


$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");


# RecordStatus = 1 (only pending record can be deleted)
$ProgramArr = (is_array($record_id)) ? $record_id : array($record_id);
$ProgramList = implode($ProgramArr, ",");

# Delete related student records
$LibFS = new libfilesystem();

$sql = "SELECT DISTINCT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE ProgramID IN ({$ProgramList})";
$RecordArr = $LibPortfolio->returnVector($sql);
# remove the attachment folders
for ($i=0; $i<sizeof($RecordArr); $i++)
{
	$rid = $RecordArr[$i];
	$folder_attachment = $eclass_filepath."/files/portfolio/ole/r".$rid;
	$LibFS->folder_remove_recursive($folder_attachment);
}
$sql = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE ProgramID IN ({$ProgramList})";
$LibPortfolio->db_db_query($sql);

# Delete programs
$sql = "DELETE FROM {$eclass_db}.OLE_PROGRAM WHERE ProgramID IN ({$ProgramList})";
$LibPortfolio->db_db_query($sql);

intranet_closedb();

if ($FromPage == "program")
	header("Location: ole.php?msg=delete&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
else	
	header("Location: ole.php?msg=delete&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");


?>
