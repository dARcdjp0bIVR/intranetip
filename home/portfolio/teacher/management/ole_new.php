<?php
# using: 
## Remark: all variables with prefix cnecc_ are for the customization of cnec Christian Collage

/** 
 * *******************************************
 * 2017-08-09 Simon
 * - Add flag for giving a default value as school name in new page/Partner Organizations
 * 
 * 2016-07-15 Henry HM
 * - Added data checking for "Default Hours" (same checking as Max Hours)
 * 
 * 2016-05-23 Henry HM [2016-0415-1449-22214]
 * - Added field of "Default Hours"
 * 
 * 2015-05-12 Bill	[2015-0324-1420-39164]
 * - Display order and add spacing of "Component of OLE"
 * - Hide "Allow Participation Reporting", "Map to OEA", "Partner Organizations", "Details / Name of Activities", "Student Remark"
 * - Add Field "Teacher-in-charge"
 * - Fixed: input Hours/Marks must be positive value
 * 2015-02-05 Omas
 * - Add Message Box showing merge program when merge
 * 2015-01-12 Bill
 * - Add SAS Category and SAS Credit Point for Ng Wah Cust
 * 2014-08-19 Bill	(2014-08-29)
 * - Double the width of preset program window
 * 2014-08-19 Bill	(20140819)
 * - Improve: autocomplete of title
 * 2010-10-13 Max 	(201010131146)
 * - Hide listContent_1 also when ajax get preset program name
 * *******************************************
 */
##########################################################
## Modification Log
## 2010-02-22: Max (201002181147)
## - Add config for cnecc

## 2010-02-01: Max (201002011713)
## - Indent the allow student to join's options
## - fix the bug that chosen allow student to join but saved as not allowed
## - alert box language for not selecting form with allowed to join

## 2010-01-26: Max (201001261820)
## - Show all titles in a Category when a category is chosen

## 2009-12-28: Max (200912281012)
## - Change the sub-category according to category by AJAX
## - Change the selection layer for title according to sub-category
##
## 2009-12-08: Max (200912081532)
## - retrieving data from function PROGRAM_BY_PROGRAMID_MAX
## - fix the "Component of OLE" displacement when change submission type
##########################################################
/**
 * Type			: Enhancement
 * Date 		: 200911230911
 * Description	: Teacher role
 * 				  1C) Add fields [Allow student to join], [Join period]
 * 				  Student role
 * 				  2) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
 * By			: Max Wong
 * Case Number	: 200911230911MaxWong
 * C=CurrentIssue 
 * ----------------------------------
 * Type			: Depression
 * Date 		: 200911201021
 * Description	: 1) Depress the enhancement 200911131142MaxWong, 200911121040MaxWong
 * 						-aC) comment out Chinese Input fields in the [New] page
 * 						-b) fallback the corresponding Download Sample to previous version
 * 						    that is not supporting ChiTitle and ChiDetails only
 * 							and modify back ole_import_update.php to not importing chinese fields
 * 
 * 						    fallback the current version Download Sample student_olr_sample.csv:
 * 						    student_olr_sample.csv.200911201044 -> student_olr_sample.csv.20091113
 * 						
 * 						-c) comment out the selection of English, Chinese and Bilingual for Report Type -> Student Learning Profile
 * 						    in [Student Report Printing] page
 * Case Number	: 200911201021MaxWong
 * C=CurrentIssue
 * ----------------------------------
 * Type			: Enhancement
 * Date 		: 200911131142
 * Description	: 1C) Enhance the form to New an OLE with chinese title and chinese details
 * C=CurrentIssue 2) The import page is required to support chinese title, chinese details and add school remarks
 * Case Number	: 200911131142MaxWong
 *
 * 2009-12-15: FAI (REMOVE the function for switch to other TYPE (INT/EXT) while editing the program details)
 * 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-ole-program-tag.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."/includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."/includes/portfolio25/oea/liboea.php");
include_once($PATH_WRT_ROOT."/includes/portfolio25/oea/liboea_item.php");
include_once($PATH_WRT_ROOT."/includes/portfolio25/oea/liboea_setting.php");
//include_once($PATH_WRT_ROOT."/includes/portfolio25/oea/liboea_setting.php");

intranet_auth();
intranet_opendb();

//debug_pr($_REQUEST);

// Initializing classes
$LibUser = new libuser($UserID);
$lpf = new libpf_slp(); // for libwordtemplates_ipf




$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$lpf->ACCESS_CONTROL("ole");
$lpf_ole_program_tag = new libpf_ole_program_tag();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";

### Title ###
$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;
$TAGS_OBJ = $TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

###############################################
$SelectedStatus = $status;
$SelectedELE = $ELE;
$SelectedYear = $Year;


$CHECKED_STR = " checked = 'checked' ";
$DISPLAY_STR = " style = 'display:none;' ";
################################## record_id below are program id indeed ################################



##########################
# handling merge program
if (strtoupper($Action) == "MERGE") {
	$MergeProgramIDs = $record_id;
	/////////////////////////////////////////////
	// 20101109 Max Temp. Comment out
	/////////////////////////////////////////////
//	$mergeStatement = str_replace("{{{ REPLACE }}}",(empty($IntExt)?$iPort["internal_record"]:$iPort["external_record"]),$ec_iPortfolio['SLP']['ProgramMergeStatement1'])
//						.'<br/>'
//						.str_replace("{{{ REPLACE }}}",(empty($IntExt)?$iPort["internal_record"]:$iPort["external_record"]),$ec_iPortfolio['SLP']['ProgramMergeStatement2']);
	$OLERecordAry = $lpf->RETURN_OLE_PROGRAM_ARRAY_BY_PROGRAMID_ARRAY($MergeProgramIDs);
	$OLETitleAry = Get_Array_By_Key($OLERecordAry,'Title',1);
	$mergeStatement = $linterface->Get_Warning_Message_Box($Lang['iPortfolio']['OLE']['MergeProgramMessageBoxTitle'], $OLETitleAry, "");
	/////////////////////////////////////////////
	// 20101109 Max Temp. Comment out
	/////////////////////////////////////////////
	$record_id = "";	// Set to new a program
	$h_navigationWord = $iPort['MergeProgramme'];
# handling edit program
} else {
	if(isset($record_id))
	{
		$record_id = (is_array($record_id)) ? $record_id[0] : $record_id;
		$h_navigationWord = $button_edit;
	}
	else
	{
		$h_navigationWord = $button_new;
	}
}

##########################



# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;

# template

/*
$lf = new libwordtemplates_ipf(3);
if (!isset($temp_type))
{
	$temp_type = 1;
}
$data = $lf->getTemplatesContent($temp_type);

debug_r($data);
*/

# template drop down list
for ($f=0; $f<sizeof($LibWord->file_array); $f++)
{
	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	unset($WordListArr);
	$tempArr = $LibWord->getWordList($id);
	$WordListArr[] = array("", " --- ".$button_select." --- ");
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$WordListArr[] = array(str_replace("&amp;", "&", $p_title), undo_htmlspecialchars($p_title));
	}
	$WordListSelection[] = array($id, returnSelection($WordListArr , "", "WordList", "onChange=\"document.form1.title.value = this.value \""));
}
# template drop down list

# float list arr
for ($f=0; $f<sizeof($LibWord->file_array); $f++) {

	$id = $LibWord->file_array[$f][0];
	
	unset($tempArr);
	$tempArr = $LibWord->getWordList($id);
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$ListArr[$id][] = str_replace("&amp;", "&", undo_htmlspecialchars($p_title));

		# preset ELE of the corresponding title
		$ListELEArr[$id][] = $TempTitleArray[1];
	}
}


###############################################

###############################################
# template for activity role
$LibWord2 = new libwordtemplates_ipf(4);

# float list arr
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
	$RoleListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
}
print ConvertToJSArray($RoleListArr, "jRoleArr1");

$defaultSelectArray = array('-9','--','');   //insert this array for default select category "--"

# template
###############################################

$attach_count = 0;
if($record_id!="")
{
	$DataArr = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($record_id);
	
	if (is_array($DataArr)) {
		$startdate = $DataArr['StartDate'];
		$enddate = $DataArr['EndDate'];
		$engTitle = $DataArr['Title'];
		$chiTitle = $DataArr['TitleChi'];
		$category = $DataArr['Category'];
		$subCategoryID = $DataArr['SubCategoryID'];
		$ELE = $DataArr['ELE'];
		$organization = $DataArr['Organization'];
		$engDetails = $DataArr['Details'];
		$chiDetails = $DataArr['DetailsChi'];
		$SchoolRemarks = $DataArr['SchoolRemarks'];
		$input_date = $DataArr['InputDate'];
		$modified_date = $DataArr['ModifiedDate'];
		$period = $DataArr['Period'];
		$int_ext = $DataArr['IntExt'];
		$canJoin = $DataArr['CanJoin'];
		$canJoinStartDate = $DataArr['CanJoinStartDate'];
		$canJoinEndDate = $DataArr['CanJoinEndDate'];
		$userName = $DataArr['UserName'];
		$autoApprove = $DataArr['AUTOAPPROVE'];
		$ayID = $DataArr['AcademicYearID'];
		$ytID = $DataArr['YearTermID'];
		$joinableYear = $DataArr['JoinableYear'];
		$maximumHours = $DataArr['MaximumHours'];
		$defaultHours = $DataArr['DefaultHours'];
		$compulsoryFields = $DataArr['CompulsoryFields'];
		$defaultApprover = $DataArr['DefaultApprover'];
		$IsSAS = $DataArr['IsSAS'];
		$IsOutsideSchool = $DataArr['IsOutsideSchool'];
		// Added: 2015-01-12
		$SASCategory = $DataArr['SASCategory'];
		$SASPoint = $DataArr['SASPoint'];
	}
	
	// [2015-0324-1420-39164] get Teacher-In-Charge
	if($sys_custom['iPortfolio_ole_record_tic']){
		$teacher_PIC = $lpf->GET_OLE_PROGRAM_TEACHER_IN_CHARGE_BY_RECORDID($record_id);
	}

	# Get all categories if record category is disabled
	$OLE_Cat = $lpf->get_OLR_Category();
	$file_array = $LibWord->setFileArr();

	
	array_unshift($file_array, $defaultSelectArray);  //insert this array for default select category "--"

	if(!array_key_exists($category, $OLE_Cat))
	{
		$file_array = $LibWord->setFileArr(true);
	}
	
	# get ELE Array
	$ELEArr = explode(",", $ELE);
	$ELEArr = array_map("trim", $ELEArr);

	$JScriptID = "jArr".$category;
	$JELEScriptID = "jELEArr".$category;

	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php?IntExt=$IntExt"),
		Array($button_edit, "")
		);
} else
{
	$NewCategoryArray = $lpf->get_OLR_Category();
	
	$file_array = $LibWord->setFileArr();
	
	array_unshift($file_array, $defaultSelectArray); //insert this array for default select category "--"
	// get the least category id
	if (is_array($NewCategoryArray)) {
		foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
		{
			$TmpCategoryID = $CategoryID;
			break;
		}
	}
	
	$JScriptID = "jArr".$TmpCategoryID;
	$JELEScriptID = "jELEArr".$TmpCategoryID;
	if($ELE!="")
	{
		$ELEArr = array($ELE);
	}
	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php?IntExt=$IntExt"),
		Array($button_new, "")
	);
}

//$category = (!isset($category) || $category=="") ? 1 : $category;   // do not default as category = 1 fai 20091215;
$html_oleProgramType = ($IntExt == 1) ? $iPort["external_record"] : $iPort["internal_record"];

# build ELE list
$DefaultELEArray = $lpf->GET_ELE();
foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
{
	$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";
	$ELEList .= "<INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked onClick=\"jsCheckMultiple(this)\"><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
}

# [2015-0324-1420-39164] Sort ELE checkbox in required order
if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && $IntExt==0){
	global $eclass_db;
	
	// Get RecordID of "Responsibilities & Duties" and "Awards & Achievements"
	$sql = "SELECT RecordID FROM {$eclass_db}.OLE_ELE where EngTitle='Responsibilities &amp; Duties'";
	$rows = $lpf->returnVector($sql);
	$ELE_RD = $rows[0];		
	$sql = "SELECT RecordID FROM {$eclass_db}.OLE_ELE where EngTitle='Awards &amp; Achievements'";
	$rows = $lpf->returnVector($sql);
	$ELE_AA = $rows[0];
	
	// Required order
	$ELE_Order = array("[AD]", "[PD]", "[MCE]", "[CS]", "[CE]", "[$ELE_RD]", "[$ELE_AA]");
	
	$ELEList = "";
	foreach($ELE_Order as $ELE_ID){
		$ELE_Name = $DefaultELEArray[$ELE_ID];
		$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr))? "CHECKED" : "";
		
		$ELEList .= ($ELE_ID=="[$ELE_RD]" || $ELE_ID=="[$ELE_AA]")? "<br>" : "";
		$ELEList .= "<INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked onClick=\"jsCheckMultiple(this)\"><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
	}
}

if($startdate!="" && $enddate!="")
{
	$enddate_table_style = "display:block;";
	$add_link_style = "display:none;";
}
else
{
	$enddate_table_style = "display:none;";
	$add_link_style = "display:block;";
}

//HANDLE FOR THE PROGRAM TYPE 
$SubmitType = ($IntExt == 1) ? "EXT" : "INT" ;

// submit type (internal / external)
if($SubmitType == "EXT") 
{
	$DisplayStyle = "style='display:none;'";
	$OrganizationText = $iPort["external_ole_report"]["organization"];
}
else
{
	$DisplayStyle = "";
	$OrganizationText = $ec_iPortfolio['organization'];
}

$sumit_type_array = array();
$sumit_type_array[] = array("INT", $iPort["internal_record"]);
$sumit_type_array[] = array("EXT", $iPort["external_record"]);

# Get academic year
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='get_yearterm_opt()'", $ayID, 0, 0, "", 2);
if($record_id=="")
{
  $academic_yearterm_arr = array();
}
else
{
  $lay->AcademicYearID = $ayID;
  $academic_yearterm_arr = $lay->Get_Term_List(false);
}
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $ytID, 0, 0, "", 2);

# Program Tag
$lpf_ole_program_tag->SET_CLASS_VARIABLE("program_id", $record_id);
$lpf_ole_program_tag->SET_PROGRAM_TAG_PROPERTY();
$tagID = $lpf_ole_program_tag->GET_CLASS_VARIABLE("tag_id");
$tmp_arr = $lpf_ole_program_tag->GET_PROGRAM_TAG_LIST();
$titlefield = ($intranet_session_language=="en") ? "EngTitle" : "ChiTitle";
foreach($tmp_arr as $i=>$values)
{
  $program_tag_arr[$i] = array($values["TagID"], $values[$titlefield]);
}

$program_tag_html = getSelectByArrayTitle($program_tag_arr, "name ='tagID' onChange=\" eval(onChangeStr) \" ", $i_general_please_select, $tagID, false, 2);


$IsSASChecked = ($IsSAS) ? "checked='checked'" : "";
if (!isset($IsOutsideSchool))
{
	$IsOutsideSchool = 0;
}
$IsOutsideSchoolChecked[$IsOutsideSchool] = true;

# Prepare allow to join

$canJoinCheckedYes = "";
$canJoinCheckedNo = "";
$joinPeriodDisplayStatus = "";
if ($canJoin) {
	$canJoinCheckedYes = $CHECKED_STR;
} else {
	$canJoinCheckedNo = $CHECKED_STR;
	$joinOptionsDisplayStatus = $DISPLAY_STR;
}
if ($autoApprove) {
	$autoApproveCheckedYes = $CHECKED_STR;
} else {
	$autoApproveCheckedNo = $CHECKED_STR;
}


// Checking the compulsory fields
if (!empty($compulsoryFields)) {
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]) !== false) {
		$cpsry_hours_checked = $CHECKED_STR;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]) !== false) {
		$cpsry_role_checked = $CHECKED_STR;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]) !== false) {
		$cpsry_achievement_checked = $CHECKED_STR;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]) !== false) {
		$cpsry_attachment_checked = $CHECKED_STR;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]) !== false) {
		$cpsry_details_checked = $CHECKED_STR;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]) !== false) {
		$cpsry_approved_by_checked = $CHECKED_STR;
	}
}


$fcm = new form_class_manage();
$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_arr = array();
for($i=0; $i<count($yearClassArr); $i++)
{
  $year_arr[] = array($yearClassArr[$i]["YearID"], $yearClassArr[$i]["YearName"]);
}
$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr))));
$year_selection_html = "<input type=\"checkbox\" name=\"yearMaster\" id=\"yearMaster\" onClick=\"jCHECK_ALL_YEAR(this)\" /> <label for=\"yearMaster\">".$i_general_all."</label><br />\n";
for($i=0; $i<count($year_arr); $i++)
{
  $t_year_id = $year_arr[$i][0];
  $t_year_name = $year_arr[$i][1];
  $t_id = "year_".$i;
  
  $t_checked = (strpos($joinableYear, $t_year_id) !== false) ? $CHECKED_STR : "";

  $year_selection_html .= "<img src=\"$image_path/spacer.gif\" width=\"20\" height=\"1\" /><input type=\"checkbox\" name=\"Year[]\" id=\"".$t_id."\" value=\"".$t_year_id."\" ".$t_checked."> <label for=\"".$t_id."\">".$t_year_name."</label>\n";
  if ($i%4==3) {
  	$year_selection_html .= "<br />";
  } else {
  	// do nothing
  }
}

### Preferred Approver Teacher ###
//$teacherRecordStatus = 1;
$teacher_selection = $lpf->GET_TEACHER_SELECTION_IPF(); //bbbb

$OLE_OUTSIDE_TITLE = ($IntExt) ? $Lang['iPortfolio']['OLE_Outside']['Information'] : $Lang['iPortfolio']['OLE']['Information'];

///////////////////////// CNECC Cust : START /////////////////////////
if($sys_custom['cnecc_SS'] && $IntExt == 0)
{
	include_once($PATH_WRT_ROOT."includes/portfolio25/customize/cnecc/libpf-slp-cnecc.php");
	$libpf_slp_cnecc = new libpf_slp_cnecc();
	$cnecc_customization = $libpf_slp_cnecc->getOleNewCustomization($record_id);
}
///////////////////////// CNECC Cust : END /////////////////////////

### Ng Wah Cust [Start] - Create SAS Category Drop Down List ###
 
if($sys_custom['NgWah_SAS']){
	$SAS_Cat = array();
	
	$count = 0;
	$allCat = $Lang['iPortfolio']['NgWah_SAS_Category'];
	if(count($allCat) > 0){
		$SAS_Cat[] = array(0, "--");
		foreach($allCat as $key => $cat_name){
			$count++;
			$SAS_Cat[] = array($count, $cat_name);
		}
	}
}
 
### Ng Wah Cust [End] - Create SAS Category Drop Down List ###

########### OEA

# refer: home/portfolio/oea/student/task/oea_new.php!!!!

$liboea_setting = new liboea_setting();
$_objOEA_ITEM = new liboea_item();
$liboea = new liboea();

if ($record_id!="")
{
	$_objOEA_ITEM->setEditMode(1);
	$_objOEA_ITEM->setNewRecord($isNewRecord=1);
	$IsOEAMapped = $_objOEA_ITEM->loadRecordFromOLEMapping($record_id);
	if ($IsOEAMapped && $_objOEA_ITEM->getOEA_ProgramCode()==$oea_cfg["OEA_Default_Category_Others"])
	{
		$_objOEA_ITEM->setTitle( $Lang['iPortfolio']['OEA']['UseOleName']. " (".$engTitle.")");
	}
	$OEAMappingChecked = ($IsOEAMapped) ? " checked='checked' " : "";
	
} else
{
	$_objOEA_ITEM->setNewRecord($isNewRecord=1);
}
$h_result = $_objOEA_ITEM->getOeaItemTable(true, $engTitle);

# prepare js for display of OEA category
//$default_oea_all = $liboea_setting->get_OEA_Item();
//if(sizeof($default_oea_all)>0) {
//	$js_default_item_array = "var itemAry = new Array();\n";
//	foreach($default_oea_all as $_key=>$_ary) {
//		$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
//	}
//}
//$h_js = $js_default_item_array."\n";
$h_js = $liboea->getOeaItemAryForJs();
	
	
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"currentRecordID\" id=\"currentRecordID\" value=\"$_id\">";
$h_HiddenOLE_Item .= "<input type=\"hidden\" name=\"oea_title_temp\" id=\"oea_title_temp\" value=\"\">";

############ OEA END



?>

<script type="text/javascript" src="/templates/2009a/js/ipf_jupas.js"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />


<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="JavaScript">
//var jCurrentArr = eval("<?=$JScriptID?>");
//var jCurrentELEArr = eval("<?=$JELEScriptID?>");
var jPresetELE = 1;

//onChangeStr = "document.getElementById('listContent').style.display = 'none'; jCurrentArr=eval('jArr'+this.value);";
onChangeStr = "document.getElementById('listContent').style.display='none';if(this.value == -9){jCurrentArr=new Array(0);jCurrentELEArr=new Array(0);}else{jCurrentArr=eval('jArr'+this.value); jCurrentELEArr=eval('jELEArr'+this.value);}";
</script>

<script language="JavaScript">

onChangeStr1 = "document.getElementById('title_selection_";
onChangeStr3 = "').style.display = 'block'";

function triggerFile(formObj, my_file, my_index)
{
	var is_remove = (eval("formObj.is_need_"+my_index+".checked==false"));
	var link = document.getElementById('a_'+my_index);
	link.style.textDecorationLineThrough = is_remove;
}

function checkform(formObj)
{	
	if ($("select[name='category']").val() == "-9") {
		alert("<?=$ec_warning['category']?>");
		return false;
	}
	// YuEn: check date and title!
	if(formObj.englishTitle.value=="")
	{
		alert("<?=$ec_warning['engTitle']?>");
		return false;
	}	
	if(formObj.startdate.value=="")
	{
		formObj.startdate.focus();
		alert("<?=$ec_warning['date']?>");
		return false;
	}
	if(formObj.category.options[formObj.category.selectedIndex].value == -9)
	{
		formObj.category.focus();
		alert("<?=$ec_warning['date']?>");
		return false;
	}
	else
	{
		if(!check_date(formObj.startdate,"<?=$w_alert['Invalid_Date']?>"))
		{
			return false;
		}
		else if(formObj.enddate.value!="")
		{
			if(!check_date(formObj.enddate,"<?=$w_alert['Invalid_Date']?>"))
				return false;
			else if(formObj.startdate.value>formObj.enddate.value)
			{
				formObj.enddate.focus();
				alert("<?=$w_alert['start_end_time2']?>");
				return false;
			}
		}
	}
	
	if(formObj.academicYearID.value=="")
	{
		formObj.academicYearID.focus();
		alert(globalAlertMsg18);
		return false;
  	}
 
  	if($("input:radio[name=allowStudentsToJoin]:checked").val() == "1")
  	{
		// check join period start date
		if(formObj.jp_periodStart.value != "") {
			if ( check_date(formObj.jp_periodStart, "<?= $ec_iPortfolio['SLP']['InvalidPeriodStart'] ?>") ) {
			// check join period end date
				if(formObj.jp_periodEnd.value != "") {
					if ( check_date(formObj.jp_periodEnd, "<?= $ec_iPortfolio['SLP']['InvalidPeriodEnd'] ?>") ) {
						if ( compareDate(formObj.jp_periodEnd.value, formObj.jp_periodStart.value) > -1 ) {
							
						} else {
							alert("<?= $ec_iPortfolio['SLP']['PeriodStart_LT_PeriodEnd'] ?>");
							return false;
						}
					} else {
						return false;
					}
				} else {
					alert("<?= $ec_iPortfolio['SLP']['FillTogether'] ?>");
					return false;
				}
			} else {
				return false;
			}		
		} else {
			if(formObj.jp_periodEnd.value != "") {
				alert("<?= $ec_iPortfolio['SLP']['FillTogether'] ?>");
				return false;
			}
		}

	  	if(!check_checkbox(formObj,"Year[]"))
	  	{
	    	alert("<?=$ec_iPortfolio['SLP']['ChooseAtLeastOneYear']?>");
	    	return false;
	    }
	    
	    if (isNaN(formObj.maximumHours.value)) {
	    	alert("<?=$ec_iPortfolio['SLP']['PleaseEnterANumber']?>[<?=$ec_iPortfolio['SLP']['MaximumHours']?>]");
	    	return false;
	    } 
	    // add negative checking - Maximum Hours of "Allow Participation Reporting"
	    else if(Number(formObj.maximumHours.value) < 0){
	    	alert("<?=$Lang['iPortfolio']['PleaseInputPositiveNumber']?> [<?=$ec_iPortfolio['SLP']['MaximumHours']?>]");
	    	return false;
	    }
	    
	    if (isNaN(formObj.defaultHours.value)) {
	    	alert("<?=$ec_iPortfolio['SLP']['PleaseEnterANumber']?>[<?=$ec_iPortfolio['SLP']['DefaultHours']?>]");
	    	return false;
	    } 
	    // add negative checking - Default Hours of "Allow Participation Reporting"
	    else if(Number(formObj.defaultHours.value) < 0){
	    	alert("<?=$Lang['iPortfolio']['PleaseInputPositiveNumber']?> [<?=$ec_iPortfolio['SLP']['DefaultHours']?>]");
	    	return false;
	    }
  	}
  
	if ($('input#IsOEAMappingChk').attr('checked')) {
		if ($('input#ItemCode').val().Trim() == '') {
			alert('<?=$Lang['iPortfolio']['OEA']['jsWarning']['ChooseOeaProgram']?>');
			return false;
		}
	}
  	
  	<?php if($sys_custom['iPortfolio_ole_record_tic']) { ?>
  		if(formObj.elements["teacher_PIC[]"].length != 0)
		{
			checkOptionAll(formObj.elements["teacher_PIC[]"]);
		}
  	<?php } ?>
  	
  	<?php 
  		# SAS Credit Point Checking
		if($sys_custom['NgWah_SAS']){
  	?>
	  	var input_pt = $('input#SASPoint').val();
	  	if($('select#SASCategory').val() != 0 && (input_pt < 0 || !(/^\+?(0|[1-9]\d*)$/.test(input_pt)))){
	  		alert('<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>');
	  		$('input#SASPoint').focus();
	  		return false;
	  	}
  	<?php } ?>
  	
	return true;
}

function jPRESET_ELE(i_pos, j_pos)
{
	var formObj = document.form1;
	var ELEObj = formObj.elements["ele[]"];
	var len = ELEObj.length;
	var jPresetELEList = jCurrentELEArr[i_pos][j_pos];

	for(var m=0; m<len; m++)
	{
		if(typeof(jPresetELEList)!="undefined" && jPresetELEList.indexOf(ELEObj[m].value)!=-1) {
			ELEObj[m].checked = 1;
		}
		else {
			ELEObj[m].checked = 0;
		}
	}
}

function jChangeSubmitType(obj)
{
	var row1 = document.getElementById("row_ele");
	var div1 = document.getElementById("div_org");
	
	// external
	if(obj.value == "EXT")
	{
		row1.style.display = "none";
		div1.innerHTML = "<?=$iPort["external_ole_report"]["organization"]?>";
	}
	else
	{
		// internal
		row1.style.display = "";
		div1.innerHTML =  "<?=$ec_iPortfolio['organization']?>";
	}
}

function jCHECK_ALL_YEAR(jParcheckObj)
{
  var checked = jParcheckObj.checked;
  setChecked(checked, document.form1, "Year[]");
}

function changeDisplay(obj) {
	
	objID = obj.id;
	switch (objID)
	{
		case 'astj_yes':
			show('joinOptions');
			break;
		case 'astj_no':
			hide('joinOptions');
			break;
	}
}
function show(objID) {
	document.getElementById(objID).style.display = '';
}
function hide(objID) {
	document.getElementById(objID).style.display = 'none';
}

function changeSubCategory() {
	$("[id*=listContent]").hide();
	$("#subCategorySelectionField").html("<?=$iPort['loading']?>...");
	var catID = $("select[name='category']").val();
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_reload_slp.php",
		async: false,
		data: "ActionType=Change_Sub_Category&CategoryID="+catID+"&SubCategoryID=<?=$subCategoryID?>",
		success: function (xml) {
			$("#subCategorySelectionField").html($(xml).find("subCategorySel").text());
		}
	});
	// reset the current array for preset programme names and the corresponding ele array
	jCurrentArr = new Array(0);
	jCurrentELEArr = new Array(0);
}

// Add variable - parTitle to get required ELE components - autocomplete only
function setTitleLayer(parTitle) {
	var parTitle = parTitle || '';
	
	$("[id*=listContent]").hide();
	$("#change_preset").hide();
	$("#change_preset_loading").show();
	var catID = $("select[name='category']").val();
	var subCatID = $("select[name='subCategory']").val();
	var urlString = "../../ajax/ajax_reload_slp.php";
	var dataString = "";
	var target_count = "";
	if (subCatID == null || subCatID == -9) {
		dataString = "ActionType=Get_Program_Name_And_Ele&CategoryID="+catID;
	} else {
		dataString = "ActionType=Get_Program_Name_And_Ele&SubCategoryID="+subCatID;
	}
//	alert(urlString+"?"+dataString);
	$.ajax({
		type: "GET",
		url: urlString,
		data: dataString,
		success: function (xml) {
			// parent node is <elements>
			child = $(xml).find("programNameElements");
			
			// initialize the current array for preset programme names and the corresponding ele array
			jCurrentArr = new Array(child.size());
			jCurrentELEArr = new Array(child.size());
			
			arr_pos_count = 0;
			child.each(function() {
				var title = $(this).find("title").text();

				var ele = $(this).find("ele").text();
				
				jCurrentArr[arr_pos_count] = new Array(1);
				jCurrentELEArr[arr_pos_count] = new Array(1);
				
				jCurrentArr[arr_pos_count][0] = title;
				jCurrentELEArr[arr_pos_count][0] = ele;
				
				// Return ELE Component index when parTitle equal to obtained title 
				if(parTitle == title){
					target_count = arr_pos_count;	
				}
				
				arr_pos_count = arr_pos_count + 1;
			});
			
			$("#change_preset").show();
			$("#change_preset_loading").hide();
			setDivVisible(false, 'listContent_1', 'lyrShim');
			document.getElementById('listContent_1').style.display= 'none';
		}
	});
	
	return target_count;
}

function check_academic_year()
{
  var startdate = $("[name=startdate]").val();
  if(startdate == "") return;

	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_check_yearID.php",
		data: "startdate="+startdate,
		success: function (msg) {
      if(msg != "")
      {
        var ay_id = msg;
        $("[name='academicYearID']").val(ay_id);
        get_yearterm_opt();
      }
		}
	});
}

function get_yearterm_opt()
{
  var ay_id = $("[name=academicYearID]").val();
  var startdate = $("[name=startdate]").val();
  if(ay_id == ""){
    $("#ayterm_cell").html('');
    return;
  }
  
	$.ajax({
		type: "GET",
		url: "../../ajax/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id+"&startdate="+startdate,
		success: function (msg) {
      if(msg != "")
      {
        $("#ayterm_cell").html(msg);
      }
		}
	});
}

function getDefaultApprover()
{
	$('select[name="request_approved_by"]').children().each(function(){
    if ($(this).val()=="<?=$defaultApprover?>"){
        $(this).attr("selected","true"); 
    }
});
	//alert('<?=$defaultApprover?>');
}

function js_EnableDefaultApprover()
{
	$('select[name=request_approved_by]').attr("disabled","");
}


function js_DisableDefaultApprover()
{
	$('select[name=request_approved_by]').attr("disabled","disabled");

}

function displayOEAMapping(myValue)
{
	if (myValue=="1")
	{
		$('#TableDiv').show();
		$('#RemoveOEAMappingDiv').hide();
	} else
	{
		$('#TableDiv').hide();
<?php if ($IsOEAMapped) { ?>
		$('#RemoveOEAMappingDiv').show();
<?php } ?>
	}
}

$(document).ready(function(){
	// initialize sub category
	changeSubCategory();
	setTitleLayer();
	
	AutoApprovedYes = "<?=$autoApproveCheckedYes?>";
	AutoApprovedNo = "<?=$autoApproveCheckedNo?>";
	
	if(AutoApprovedYes!='')
	{
		js_DisableDefaultApprover();
	}
	else if (AutoApprovedNo!='')
	{
		js_EnableDefaultApprover();
	}
	
	getDefaultApprover();
	
	
	$.ajaxSetup({	
		cache:false, async:false
	});
	
	$('input').keydown(function(e) {
		if (event.which == 13) {
			event.preventDefault();
			return false;
		}
	});
	
	if($("#englishTitle").length > 0){
		// Autocomplete of title 
		$("#englishTitle").autocomplete(
	      "ajax_ole_title_suggestion.php",
	      {
	      	
	      		onItemSelect: function(li){
	      			
	      			// Data: Title, Category, SubCategory
	      			var $dataAry1 = $("#englishTitle").val().split('<?=' ( '. $ec_iPortfolio['SLP']['Category'].' : '?>');
	      			var $dataAry2 = $dataAry1[1].split('<?=' - '. $ec_iPortfolio['SLP']['SubCategory'].' : '?>');
	      			var $title = Trim($dataAry1[0]);
	      			if(typeof $dataAry2[1] === "undefined"){
	      				var $catSelect = Trim(Trim($dataAry2[0]).slice(0,-1));
	      				var $subCatSelect = null;
	      			} else {
	      				var $catSelect = Trim($dataAry2[0]);
	      				var $subCatSelect = Trim(Trim($dataAry2[1]).slice(0,-1));
	      			}
	      			
	      			// Response
	      			// Textfield: Title
	      			$("#englishTitle").val($title);
	      			
	      			// Drop Down List: Category
	      			$("select[name=category] option").filter(function() {
    					if(this.text == $catSelect){
    						// Selected if equal to $catSelect
    						this.selected=true;
    					}
					});
					// Refresh
					changeSubCategory(); 
					setTitleLayer();
					
					// Drop Down List: SubCategory
					$("select[name=subCategory] option").filter(function() {	
    					if(this.text == $subCatSelect){
    						this.selected=true;
    					}
					});
					// Refreah + Get related ELE Component
					var locatCount = setTitleLayer($title);
					if(typeof(jPresetELE)!='undefined'){
						jPRESET_ELE(locatCount.toString(), '0');
					}
	      			
	      		}, 
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px'
	  		}
	    );
	}
	
<?php if($record_id=="") { ?>
	check_academic_year();
<?php } ?>
});


	
function Show_Advance_Search()
{
	var currentRecordID = $('input#currentRecordID').val();
	var ole_title = $('input#englishTitle').val();
	$.post(
		"../../oea/student/index.php", 
		{ 
			currentRecordID: currentRecordID,
			IsForOLEEdit: 1,
			ole_title: ole_title,
			task: "ajax",
			script: "ajax_advance_search"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
		}
	);
}

function Show_SuggestionList()
{
	var ole_title = $('input#englishTitle').val();
	if (ole_title=="")
	{
		alert("<?=$Lang['iPortfolio']['OEA']['MissingOLETitle']?>");
		document.form1.IsOEAMapping.checked = false;
		document.form1.englishTitle.focus();
		return ;
	} else
	{
		$('#HiddenClickTB').click();
	}	
	
	var currentRecordID = $('input#currentRecordID').val();
	$.post(
		"../../oea/student/index.php", 
		{ 
			currentRecordID: currentRecordID,
			ole_title: ole_title,
			IsForOLEEdit: 1,
			task: "ajax",
			script: "ajax_get_suggestion_list"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);	
}


function js_show_layer(layername) {
	$('#'+layername).show('slow');
}

function checkOEAcategory(itemCode) {
	document.getElementById('span_OEA_Category').innerHTML = itemAry[itemCode][1];
	document.getElementById('CatCode').value =  itemAry[itemCode][0];
}

function CopyOthers() {
	var titleText = "<b><?=$Lang['iPortfolio']['OEA']['UseOleName']?></b> ";
	var ole_title = $('input#englishTitle').val();
	if (ole_title!="") {
		titleText += "("+ $('input#englishTitle').val() +")";
	}
	document.getElementById('selectedItemSpan').innerHTML = titleText;
	//document.getElementById('oea_title_temp').focus();
	$('input#ItemCode').val('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	checkOEAcategory('<?=$oea_cfg["OEA_Default_Category_Others"]?>');
	$('#MapOptions').hide('slow');
	$('#divPencil').show();
}


function jsCheckMultiple(obj)
{
<?php if ($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']) { ?>
		//if(countChecked(document.form1, 'ele[]') > 1)
		//{
		//	alert("<?=$msg_check_at_most_one?>");
		//	obj.checked = false;
		//}
		len=document.form1.elements.length;
		for(i=0; i<len; i++) {
			if(document.form1.elements[i].name=='ele[]' && document.form1.elements[i].checked && document.form1.elements[i]!=obj){
				document.form1.elements[i].checked = false;
			}
        }
<?php } ?>
}
</script>

<FORM name="form1" method="post" action="ole_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">

<br />


<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td >
	<table border="0" cellspacing="0" cellpadding="5" width="95%">
	<tr>
		<td>
		<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" border="0" align="absmiddle"> <?=$h_navigationWord?> </a>
		</td>
		<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
	</tr>
	<tr>
		<td style="text-align:center;" colspan="2">
		<?=$mergeStatement?>
		</td>
	</tr>
	</table>

	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center" >
	<tr>
		<td valign="top" >
		<!-- CONTENT HERE -->

<div class="table_board"><span class="sectiontitle_v30"> <?=$OLE_OUTSIDE_TITLE ?></span><br />

		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			
			<!-- type -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?php echo $iPort['submission_type']; ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><?=$html_oleProgramType?>
					<input type ="Hidden" value="<?=$SubmitType?>" name ="SubmitType">
					</td>
				</tr>
				</table>
				</td>
			</tr>
			
			<!-- cateogry -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['category']?><span class="tabletextrequire">*</span></span>
				</td>
				<td>
				<?//=$linterface->GET_SELECTION_BOX($file_array, "name ='category' onChange=\" eval(onChangeStr) \" ","",$category);
				?>
				<?=$linterface->GET_SELECTION_BOX($file_array, "name ='category' onChange=\" changeSubCategory(); setTitleLayer(); \" ","",$category);
				?>
				</td>
			</tr>
			
			<!-- sub category -->
			<? //sub category ?>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['sub_category']?></span>
				</td>
				<td id='subCategorySelectionField'>
				<?//=$linterface->GET_SELECTION_BOX($file_array, "name ='subCategory' onChange=\" eval(onChangeStr) \" ","",$category);?>
				</td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?= $ec_iPortfolio['title'] ?><span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><input autocomplete="off" id='englishTitle' name="englishTitle"  type="text" size="50" value="<?=htmlentities($engTitle, ENT_COMPAT, "utf-8")?>" class="textboxtext"  maxlength="255"></td>
					<td width="5">&nbsp;</td>
					<td width="25" ><div id="change_preset_loading" style="display: none;"><img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif"></div><div id="change_preset"><?= GetPresetText("jCurrentArr", 1, "englishTitle", "jPRESET_ELE()", '', '400');?></div></td>
				</tr>
				</table>
				</td>
			</tr>
			<?/*200911201021MaxWong<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?= $ec_iPortfolio['chinese'] . ($intranet_session_language == "en" ? " " : "") . $ec_iPortfolio['title'] ?></span>
				</td>
				<td width="80%" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="60%" >
				<tr>
					<td width="100%" ><input name="chineseTitle" id="chineseTitle" type="text" size="50" value="<?=$chiTitle?>" class="textboxtext"  maxlength="255"></td>
					<td width="5">&nbsp;</td>
					<td width="25" ><div id="change_preset"><?= GetPresetText("jCurrentArr", 1, "title", "jPRESET_ELE()");?></div></td>
				</tr>
				</table>
				</td>
			</tr>*/?>
			<!-- 200911131142MaxWong -->
			<!-- date -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['date']?><span class="tabletextrequire">*</span></span></td>
				<td>

				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap='nowrap'>
					
					<!--
					<input type="text" name="startdate" size="11" maxlength="10" class="tabletext" value="<?=$startdate?>" onChange="check_academic_year()" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>'; check_academic_year();}" />
					<?=$linterface->GET_CALENDAR("form1", "startdate", "check_academic_year()")?>
					-->
					<?= $linterface->GET_DATE_PICKER("startdate", $startdate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="check_academic_year();",$ID="",$SkipIncludeJS=0, $CanEmptyField=1); ?>
					</td>
					<td nowrap='nowrap' id="table_enddate" style="<?=$enddate_table_style?>">&nbsp;<?=$profiles_to?>&nbsp;
					<!--
					<input class="tabletext" type="text" name="enddate" size="11" maxlength="10" value="<?=$enddate?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}">
					<?=$linterface->GET_CALENDAR("form1", "enddate")?>
					-->
					<?= $linterface->GET_DATE_PICKER("enddate", $enddate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1); ?>
				</tr>
				<tr id="add_link" style="<?=$add_link_style?>"><td height="25"><a href="javascript:displayTable('add_link', 'none');displayTable('table_enddate', 'block');" class='tablelink'><?=$ec_iPortfolio['add_enddate']?></a></td></tr>
				</table>
				</td>
			</tr>
			
      <!-- year term -->
      <tr>
      	<td valign="top" nowrap="nowrap" class="formfieldtitle">
      	  <span class="tabletext"><?=$ec_iPortfolio['year']?><span class="tabletextrequire">*</span></span></td>
      	<td>
      
      	<table border="0" cellspacing="3" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap'><?=$ay_selection_html?></td>
      			<td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
      		</tr>
      	</table>
      	</td>
      </tr>
			
			<!-- ele -->
			<tr id="row_ele" valign="top" <?=$DisplayStyle?> >
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['ele']?></span>
				</td>
				<td><?=$ELEList?></td>
			</tr>
			
<?php		
	if($sys_custom['NgWah_SAS']){
?>			
			<!-- Ng Wah Customization -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$Lang['iPortfolio']['NgWah_SAS_Settings']['Category']?></span>
				</td>
				<td><?=$linterface->GET_SELECTION_BOX($SAS_Cat, "name ='SASCategory' id='SASCategory'", "", $SASCategory);?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$Lang['iPortfolio']['NgWah_SAS_Settings']['Credit_pt']?></span>
				</td>
				<td><input type="text" class="textboxnum" name="SASPoint" id="SASPoint" value="<?=(($SASCategory == 0)? '': $SASPoint)?>" maxlength="4"></td>
			</tr>
<?php } ?>
			
			<!-- allow student to join -->
<?php
if($IntExt != "1" && $SubmitType =="INT" && !$sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'])
{
			//allow student to join only support to inside school program
?>
			<tr id="joinSelectField">
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['SLP']['AllowStudentsToJoin']?></span>
				</td>
				<td>
					<label for="astj_yes"><?=$ec_iPortfolio['SLP']['Yes']?></label><input id="astj_yes" name="allowStudentsToJoin" value="1" type="radio" <?= $canJoinCheckedYes ?> onClick="changeDisplay(this);"/>
					<label for="astj_no"><?=$ec_iPortfolio['SLP']['No']?></label><input id="astj_no" name="allowStudentsToJoin" value="0" type="radio" <?= $canJoinCheckedNo ?> onClick="changeDisplay(this);" />
				</td>
			</tr>
			
			<!-- join options -->
			<tr id="joinOptions" <?= $joinOptionsDisplayStatus ?>>
				<td>&nbsp;</td>
				<td>
					<table class="tablerow2" align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
						<tbody>
						<tr id="joinPeriod">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$ec_iPortfolio['SLP']['JoinPeriod']?></span>
							</td>
							<td>
								<!--
								<label for="jp_periodStart"><?=$ec_iPortfolio['SLP']['PeriodStart']?></label> <input size="11" maxlength="10" class="tabletext" id="jp_periodStart" name="jp_periodStart" type="text" value="<?= ($canJoinStartDate == "0000-00-00" ? "" : $canJoinStartDate) ?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}" /> <?=$linterface->GET_CALENDAR("form1", "jp_periodStart")?>
								<label for="jp_periodEnd"><?=$ec_iPortfolio['SLP']['PeriodEnd']?></label> <input size="11" maxlength="10" class="tabletext" id="jp_periodEnd" name="jp_periodEnd" type="text" value="<?= ($canJoinEndDate == "0000-00-00" ? "" : $canJoinEndDate) ?>" onFocus="if(this.value==''){this.value='<?=date("Y-m-d")?>';}" /> <?=$linterface->GET_CALENDAR("form1", "jp_periodEnd")?>
								-->
								<label for="jp_periodStart"><?=$ec_iPortfolio['SLP']['PeriodStart']?></label> <?= $linterface->GET_DATE_PICKER("jp_periodStart", ($canJoinStartDate == "0000-00-00" ? "" : $canJoinStartDate), $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1); ?>
								<label for="jp_periodEnd"><?=$ec_iPortfolio['SLP']['PeriodEnd']?><?= $linterface->GET_DATE_PICKER("jp_periodEnd", ($canJoinEndDate == "0000-00-00" ? "" : $canJoinEndDate), $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1); ?>
							</td>
						</tr>
						
						<!-- joinable year -->
						<tr id="joinableYear">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$ec_iPortfolio['SLP']['JoinableYear']?><span class="tabletextrequire">*</span></span>
							</td>
							<td>
								<?=$year_selection_html?>
							</td>
						</tr>
						
						<!-- auto approve -->
						<tr id="autoApproveSection">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$ec_iPortfolio['SLP']['AutoApprove']?></span>
							</td>
							<td>
								<label for="auto_approve_yes"><?=$ec_iPortfolio['SLP']['Yes']?></label><input id="auto_approve_yes" onclick="js_DisableDefaultApprover();"  name="autoApprove" value="1" type="radio" <?= $autoApproveCheckedYes ?> />
								<label for="auto_approve_no"><?=$ec_iPortfolio['SLP']['No']?></label><input id="auto_approve_no" onclick="js_EnableDefaultApprover();"  name="autoApprove" value="0" type="radio" <?= $autoApproveCheckedNo ?> /><!--font size="1">Approve setting base on system settings</font-->
							</td>
						</tr>
						
						<!-- max hour : Student cannot set hour larger then this number -->
						<tr id="maximumHoursBox">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$ec_iPortfolio['SLP']['MaximumHours']?></span>
							</td>
							<td>
								<input id="maximumHours" name="maximumHours" size="10" value="<?=$maximumHours?>" class="tabletext" type="text">
							</td>
						</tr>
						
						<!-- default hour : Student will see this default hour in the submission hours -->
						<tr id="defaultHoursBox">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?php echo $ec_iPortfolio['SLP']['DefaultHours']; ?></span>
							</td>
							<td>
								<input id="defaultHours" name="defaultHours" size="10" value="<?php echo $defaultHours; ?>" class="tabletext" type="text">
							</td>
						</tr>
						
						<!-- compulsory fields -->
						<tr id="compulsoryFieldsBox">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$ec_iPortfolio['SLP']['CompulsoryFields']?></span>
							</td>
							<td>
								<input type="checkbox" name="compulsoryFields[hours]" id="cpsry_hours" <?=$cpsry_hours_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]?>" /><label for="cpsry_hours"><?=$ec_iPortfolio['hours']?></label>
								<input type="checkbox" name="compulsoryFields[role]" id="cpsry_role" <?=$cpsry_role_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]?>" /><label for="cpsry_role"><?=$ec_iPortfolio['ole_role']?></label>
								<input type="checkbox" name="compulsoryFields[achievement]" id="cpsry_achievement" <?=$cpsry_achievement_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]?>" /><label for="cpsry_achievement"><?=$ec_iPortfolio['achievement']?></label>
								<input type="checkbox" name="compulsoryFields[attachment]" id="cpsry_attachment" <?=$cpsry_attachment_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]?>" /><label for="cpsry_attachment"><?=$ec_iPortfolio['attachment']?></label>
								<?/*<input type="checkbox" name="compulsoryFields[details]" id="cpsry_details" <?=$cpsry_details_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]?>" /><label for="cpsry_details"><?=$ec_iPortfolio['details']?></label>*/?>
								<input type="checkbox" name="compulsoryFields[approved_by]" id="cpsry_approved_by" <?=$cpsry_approved_by_checked?> value="<?=$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]=5?>" /><label for="cpsry_approved_by"><?=$ec_iPortfolio['approved_by']?></label>
							</td>
						</tr>
						
						<!-- Default Approver fields -->
						<tr id="approvedTeacherBox">
							<td valign="top" nowrap="nowrap" class="formfieldtitle">
							<span class="tabletext"><?=$Lang['iPortfolio']['default_approver']?></span>
							</td>
							<td>
								<?=$teacher_selection?>
							</td>
						</tr>
					</tbody>
					</table>
				</td>
			</tr>
<?
} // end if($IntExt != "1" && $SubmitType =="INT") , allow student to join only support to inside school program
?>
			
			<!-- editable fields -->
			<?php/* may be release later
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle">
				<span class="tabletext"><?=$ec_iPortfolio['SLP']['EditableFields']?></span>
				</td>
				<td>
					<label for="ef_organization"><?=$OrganizationText?></label><input id="ef_organization" name="editableFields" type="checkbox" />
					<label for="ef_englishDetails"><?= $ec_iPortfolio['english'] . ($intranet_session_language == "en" ? " " : "") .$ec_iPortfolio['details']?></label><input id="ef_englishDetails" name="editableFields" type="checkbox" />
					<label for="ef_schoolRemarks"><?=$ec_iPortfolio['school_remarks']?></label><input id="ef_schoolRemarks" name="editableFields" type="checkbox" />
				</td>
			</tr>*/?>
			
		<?php if(!$sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] || ($IntExt!=0 && $IntExt!="")) { ?>
			<!-- organization -->
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$OrganizationText?></div></span></td>
				<td>
						<!-- add flag to get the default school name -->
						<?php 
							if($sys_custom['iPf']['OLE']['GetDefaultSchoolName'] && is_null($organization) && empty($organization)){
								$organization = GET_SCHOOL_NAME();
							}
						?>
						<input name="organization" type="text" value="<?=$organization?>" maxlength="256" class="textboxtext" >
				</td>
			</tr>
			
			<!-- 200911131142MaxWong -->
			<?/*<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?></span></td>
				<td><?=$linterface->GET_TEXTAREA("details", $details);?></td>
			</tr>*/?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['details']?></span></td>
	     		<td><?=$linterface->GET_TEXTAREA("englishDetails", $engDetails);?> <div class="tabletextremark"><?=$ec_iPortfolio['details_sp_remark']?></div></td>	
			</tr>
			<?/*200911201021MaxWong<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?= $ec_iPortfolio['chinese'] . ($intranet_session_language == "en" ? " " : "") .$ec_iPortfolio['details']?></span></td>
				<td><?=$linterface->GET_TEXTAREA("chineseDetails", $chiDetails);?></td>
			</tr>*/?>
			<!-- 200911131142MaxWong -->
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remarks']?></span></td>
				<td><?=$linterface->GET_TEXTAREA("SchoolRemarks", $SchoolRemarks);?></td>
			</tr>
		<?php } ?>
		
		<?php if($sys_custom['iPortfolio_ole_record_tic']) { ?>
			<!-- Teacher-in-charge -->
			<tr valign='top'>
				<td nowrap class='field_title'><?=$Lang['iPortfolio']['OLE']['TeacherInCharge']?></td>
				<td><table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><?=$linterface->GET_SELECTION_BOX($teacher_PIC, "id='teacher_PIC' name='teacher_PIC[]' size='6' multiple", "");?></td>
							<td valign="bottom">
								<?=$linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=teacher_PIC[]&CatID=-99&permitted_type=1&Disable_AddGroup_Button=1', 16)");?>
								<br>
								<?=$linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['teacher_PIC[]'])");?>
							</td>
						</tr>
				</table></td>
			</tr>
		<?php } ?>
		
			<!-- come from -->
			<?=$h_setComeFrom?>
			
			<?=$cnecc_customization?>
			
			<!-- HKUGAC Customization -->
<?php if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) { ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$Lang['iPortfolio']['OLE']['SAS_with_link']?></div></span></td>
				<td valign="top"><input id="IsSAS" name="IsSAS" type="checkbox" value="1" <?=$IsSASChecked?> /></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$Lang['iPortfolio']['OLE']['In_Outside_School']?></div></span></td>
				<td valign="top"><?= $linterface->Get_Radio_Button("InsideSchool", "InOutSideSchool", "0", $IsOutsideSchoolChecked[0], "", $Lang['iPortfolio']['OLE']['Inside_School'] ) ?>
				<?= $linterface->Get_Radio_Button("OutsideSchool", "InOutSideSchool", "1", $IsOutsideSchoolChecked[1], "", $Lang['iPortfolio']['OLE']['Outside_School'] ) ?></td>
			</tr>
<?php } ?>
			
			<!-- OEA Mapping -->
<?php if ($liboea->isEnabledOEAItem() && (!$sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] || ($IntExt!=0 && $IntExt!=""))) { ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><div id="div_org"><?=$Lang['iPortfolio']['OEA']['PreMapping']?></div></span><br /><div class='tabletextremark'><?= $Lang['iPortfolio']['OEA']['PreMappingNotes'] ?></div></td>
				<td valign="top"><input id="IsOEAMappingChk" name="IsOEAMapping" type="checkbox" value="1" onClick="displayOEAMapping(this.checked)" <?=$OEAMappingChecked?> />
				<div id="RemoveOEAMappingDiv" style="display:none;"><font color="red"><?=$Lang['iPortfolio']['OEA']['PreMappingRemove']?></font></div>
				
				
	<?php
echo $h_result ;
echo $h_HiddenOLE_Item;

 ?>

				</td>
			</tr>
<?php } ?>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" >
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$ec_iPortfolio['mandatory_field_description']?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
			</td>
		</tr>
		</table>
		<br>
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<script>
<?=$h_js?>

<?php if ($IsOEAMapped) { ?>
	displayOEAMapping(1);
<?php } ?>



	
	
</script>
<input type="hidden" name="Action" value="<?=$Action?>" >
<input type="hidden" name="MergeProgramIDs" value='<?=serialize($MergeProgramIDs)?>' >
<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" name="attachment_size" value="0" >
<input type="hidden" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" name="FromPage" value="<?=$FromPage?>" >
<input type="hidden" name="IntExt" value="<?=$IntExt?>" >
<input type ="hidden" name="passBackParameter" value="<?=$passBackParameter?>">
<input type ="hidden" name="passBackURL" value="<?=$passBackURL?>">
</form>

<?php
//echo $linterface->FOCUS_ON_LOAD("form1.title");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

