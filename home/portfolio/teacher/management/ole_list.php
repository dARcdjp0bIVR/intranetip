<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

$LibPortfolio = new libpf_slp();

########################################################
# Tab Menu : Start
########################################################

$TabIndex = ($IntExt == 1) ? "OUTSIDE" : "OLE";
$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex);

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $LibPortfolio->GET_TAB_MENU($TabMenuArr);

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='jSUBMIT_FORM()'", $academicYearID, 1, 0, $i_Attendance_AllYear, 2);

# component selection
$ele_selection_html = ($IntExt == 1) ? "" : $LibPortfolio->GET_ELE_SELECTION("name='ELE' onChange='jSUBMIT_FORM()'", $ELE, 1);

# cateogry selection
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='jSUBMIT_FORM()'", $category, 1);

if(strstr($ck_function_rights, "Profile:OLR"))
{
	$RecordArray[] = array("all", $ec_iPortfolio['all_programmes']);
	$RecordArray[] = array("my", $ec_iPortfolio['my_programmes']);
	$RecordArray[] = array("teacher", $ec_iPortfolio['SLP']['TeacherCreatedProgram']);
	$RecordArray[] = array("student", $ec_iPortfolio['SLP']['StudentCreatedProgram']);
	$record_own = (!isset($record_own)) ? "all" : $record_own;
	$record_selection_html = getSelectByArray($RecordArray, "name='record_own' onChange='jSUBMIT_FORM()'", $record_own, 0, 1, "", 2);
}

# title search
$search_text = ($search_text == $ec_iPortfolio['enter_title_name']) ? "" : $search_text;
$searching_html = "<input name='search_text' id='search_text' value=\"".($search_text==""?$ec_iPortfolio['enter_title_name']:stripslashes($search_text))."\" onFocus=\"jSET_SEARCH_TEXT(true)\" onBlur=\"jSET_SEARCH_TEXT(false)\" >&nbsp;<input class='button_g' onClick=\"jSUBMIT_FORM()\" type='button' value=\"".$button_search."\">";

# import button
$import_btn_html = strstr($ck_function_rights, "ImportData") ? "<td nowrap><a href=\"ole_import.php?IntExt={$IntExt}&FromPage=pview\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import}</a></td>" : "";

# export button
$export_btn_html = strstr($ck_function_rights, "ExportData") ? "<td nowrap><a href=\"javascript:doExport('ole')\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_export}</a></td>" : "";

# LaSalle customarization: comment input button
$comment_html = ($LibPortfolio->GET_CUSTOMARIZE_SCHOOL_SLP() == "LaSalle") ? "<td nowrap><a href=\"ole_comment.php\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_comment.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_advice']}</a></td>" : "";
########################################################
# Operations : End
########################################################

########################################################
# Table content : Start
########################################################
$ELEArray = $LibPortfolio->GET_ELE();
$ELECount = ($IntExt == 1) ? 0 : count($ELEArray);
$rowStyle = ($IntExt == 1) ? "" : "rowspan=\"2\"";

$pageSizeChangeEnabled = true;
$checkmaster = true;

# Filter conditions
$cond = empty($academicYearID) ? "" : " AND ay.AcademicYearID = ".$academicYearID;
$cond .= empty($ELE) ? "" : " AND INSTR(op.ELE, '{$ELE}') > 0";
$cond .= empty($category) ? "" : " AND op.Category = ".$category;
$cond .= empty($search_text) ? "" : " AND op.Title = '".$search_text."'";
switch ($record_own)
{
	case "my": # Select of My Programmes
		$cond .= " AND (op.CreatorID = ".$UserID." OR op.ProgramID IN (SELECT DISTINCT ProgramID FROM {$eclass_db}.OLE_STUDENT WHERE ApprovedBy = ".$UserID."))";
		break;
	case "teacher": # Select of Teacher Created Programmes
		$cond .= " AND op.ProgramType = 'T'";
		break;
	case "student": # Select of Student Created Programmes
		$cond .= " AND op.ProgramType = 'S'";
		break;
	default:
		break;
}

# Main query
if ($order=="") $order=0;
if ($field=="") $field=3;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT
            CONCAT('<a class=\"tablelink\" href=\"ole_event_studentview.php?IntExt={$IntExt}&EventID=', op.ProgramID, '&Year={$Year}&FromPage=program\">', op.Title, '</a>'),
            IF(ay.AcademicYearID IS NULL, '--', ".($intranet_session_language=="b5"?"ay.YearNameB5":"ay.YearNameEN")."),
            ".($intranet_session_language=="b5"?"oc.ChiTitle":"oc.EngTitle").",
        ";
if ($IntExt != 1)
{
  foreach($ELEArray as $ELECode => $ELETitle)
	{
    $sql .= "IF(INSTR(op.ELE, '".$ELECode."') > 0, '<img src=\"/images/2009a/icon_tick_green.gif\" />', ''),";
	}
}
$sql .= "
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
            IF(stuCountTable.stuCount IS NULL, 0, stuCountTable.stuCount) AS stuCount,
            IF(pendingCountTable.pendingCount IS NULL, 0, pendingCountTable.pendingCount) AS pendingCount,
            CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', op.ProgramID ,'>')
          FROM
            {$eclass_db}.OLE_PROGRAM AS op
          LEFT JOIN
            {$intranet_db}.ACADEMIC_YEAR AS ay
          ON
            op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN
            {$eclass_db}.OLE_CATEGORY AS oc
          ON
            op.Category = oc.RecordID
          LEFT JOIN
            (
              SELECT
                ProgramID,
                count(*) AS stuCount
              FROM
                {$eclass_db}.OLE_STUDENT
              WHERE
                IntExt = '".($IntExt==1?"EXT":"INT")."'
              GROUP BY
                ProgramID
            ) AS stuCountTable
          ON
            op.ProgramID = stuCountTable.ProgramID
          LEFT JOIN
            (
              SELECT
                ProgramID,
                count(*) AS pendingCount
              FROM
                {$eclass_db}.OLE_STUDENT
              WHERE
                IntExt = '".($IntExt==1?"EXT":"INT")."' AND
                RecordStatus = 1
              GROUP BY
                ProgramID
            ) AS pendingCountTable
          ON
            op.ProgramID = pendingCountTable.ProgramID
          WHERE
            op.IntExt = '".($IntExt==1?"EXT":"INT")."'
            $cond
        ";

// TABLE INFO
$LibTable->field_array = array("op.Title", "op.AcademicYearID", "op.Category", "op.StartDate", "stuCount", "pendingCount");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 8 + $ELECount;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' $rowStyle class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' $rowStyle width='100' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle class=\"tabletopnolink\">".$LibTable->column(1,$ec_iPortfolio['by_year'], 1)."</td>";
$LibTable->column_list .= "<td $rowStyle class=\"tabletopnolink\">".$LibTable->column(2,$ec_iPortfolio['category'], 1)."</td>";
$LibTable->column_list .= ($IntExt != 1) ? "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n" : "";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(3,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(4,$i_ClubsEnrollment_NumOfStudent, 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(5,$ec_iPortfolio['waiting_approval'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->check("record_id[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";

if ($IntExt != 1)
{
	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
		$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
	}
	$LibTable->column_list .= "</tr>";
  
  $LibTable->column_array = array_merge(array(0,0,3), array_fill(0, $ELECount, 3), array(0,3,3,3));				
}
else
{
  $LibTable->column_array = array(0,0,3,0,3,3,3);
}

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
include_once("template/ole_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>