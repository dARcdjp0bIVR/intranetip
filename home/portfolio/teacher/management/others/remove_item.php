<?php

// Modifing by 
/*
 * 	Purpose: remove item(s) of other reports in iportfolio 
 * 	
 * 	Log
 * 
 * 	Date:	2015-11-16 [Cameron] create this file 
 * 	
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreportitem.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libportfolio2007();
$lf = new libfilesystem();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$error = array();
$RecordID = $_POST['RecordID'];

if (count($RecordID) > 0) {
	foreach ((array)$RecordID as $id) {
		$lpfori = new libportfoliootherreportitem($id);
		$ReportFile = $lpfori->getReportFile();
		$IsDir = $lpfori->getIsDir();
		$target = $intranet_root."/file/iportfolio/other_report/".$ReportFile;
		
		// delete file
		$ret = $lf->file_remove($target);
		if ($IsDir) {
			$start = strpos($ReportFile,'/');
			$end = strpos($ReportFile,'/',$start+1);
			$target_dir = substr($ReportFile,$start+1,$end-$start-1);
			
			$record_id_dir = substr($ReportFile,0,$start);		
			$del_dir = $intranet_root."/file/iportfolio/other_report/".$record_id_dir."/".$target_dir;
			
			$error[] = $lf->deleteDirectory($del_dir);
		}
		else {
			$start = strpos($ReportFile,'/');
			$end = strrpos($ReportFile,'/');
			$target_dir = substr($ReportFile,$start+1,$end-$start-1);
			
			$record_id_dir = substr($ReportFile,0,$start);		
			$del_dir = $intranet_root."/file/iportfolio/other_report/".$record_id_dir."/".$target_dir;
		
		 	
			// delete directory if there's no file in it 
	        if (file_exists($del_dir) && is_dir($del_dir) ) {
	        	$counter = 0;
		        foreach (scandir($del_dir) as $item) {
		            if ($item != '.' && $item != '..') {
		            	$counter++;
		            }
		        }
		        if ($counter == 0) {
		        	$error[] = $lf->deleteDirectory($del_dir);
		        }
	        }    
		}
						
		$error[] = $ret;
		if ($ret) {
			$error[] = $lpfori->deleteRecord();
		}
		unset($lpfori);				
	}
}

if (!in_array(false,$error)) {
	header("Location: $PATH_WRT_ROOT/home/portfolio/teacher/management/others/item_list.php?xmsg=DeleteSuccess&OtherReportID=$OtherReportID");
}
else {
	header("Location: $PATH_WRT_ROOT/home/portfolio/teacher/management/others/item_list.php?xmsg=DeleteUnsuccess&OtherReportID=$OtherReportID");
}
intranet_closedb();

?>
