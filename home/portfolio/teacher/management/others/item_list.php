<?php

// Modifing by 
/*
 * 	Note:	ClassClassNumber displays student's class name and class number in current academic year,
 * 			therefore, alumni won't show record
 * 	
 * 	Log
 * 
 * 	Date:	2015-11-13 [Cameron] create this file 
 * 	
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreport.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

//$lpf = new libpf_slp();
//$luser = new libuser($UserID);
$lpf = new libportfolio2007();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_OTHER_REPORT);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################


if ($order=="") $order=0;		// desc
if ($field=="") $field=1;		// InputDate

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
$pageNo = 1;

// table tool
$table_tool = "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove_item.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

$OtherReportID = $_REQUEST['OtherReportID'];
$lpfor = new libportfoliootherreport($OtherReportID);
$ReportTitle = $lpfor->getReportTitle();

$currentAcademicYear = Get_Current_Academic_Year_ID();

if ($intranet_session_language == "en") {
	$sql_UserName = "u.EnglishName";
	$sql_ClassClassNumber = "CONCAT(yc.ClassTitleEN,' - ',ycu.ClassNumber)";
	$sql_ClassName = "yc.ClassTitleEN";
	
}
else {
	$sql_UserName = "u.ChineseName";
	$sql_ClassClassNumber = "CONCAT(yc.ClassTitleB5,' - ',ycu.ClassNumber)";
	$sql_ClassName = "yc.ClassTitleB5";
}

$sql = "SELECT 	$sql_UserName AS StudentName,
				$sql_ClassClassNumber AS ClassClassNubmer,
				CONCAT('<a href=\"javascript: view_report(\'',i.ReportItemID,'\',\'',i.IsDir,'\')\" class=\"tablelink\">','$ReportTitle','_',$sql_UserName,'_',$sql_ClassName,'_',ycu.ClassNumber,'</a>')
				AS ReportFile,
				i.DateInput,
				CONCAT('<input type=checkbox name=\"RecordID[]\" value=', i.ReportItemID ,'>'),
				i.ReportItemID				
		FROM    {$eclass_db}.PORTFOLIO_OTHER_REPORT_ITEM i
		INNER JOIN {$intranet_db}.INTRANET_USER u
			ON  u.UserID=i.StudentID
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu 
			ON  ycu.UserID=u.UserID
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON 	yc.YearClassID=ycu.YearClassID
			AND yc.AcademicYearID='$currentAcademicYear' 
		WHERE 	i.OtherReportID='$OtherReportID'";
		
$li = new libpf_dbtable($field, $order, $pageNo);
$li->field_array = array("StudentName", "ClassClassNubmer", "ReportFile", "DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->table_tag = "<table bgcolor='#cccccc' border='0' cellpadding='4' cellspacing='1' width='100%'>";
$li->row_alt = array("#FFFFFF", "#F3F3F3");
$li->row_height = 25;
$li->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink' height='25' align='center'>#</span></td>\n";
$li->column_list .= "<td width='35%' nowrap='nowrap'>".$li->column(0,$iPort['student_name'])."</td>\n";
$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(1,$iPort['class'].'-'.$iPort['class_no'])."</td>\n";
$li->column_list .= "<td width='20%' nowrap='nowrap'>".$li->column(2,$Lang['iPortfolio']['OtherReports']['ReportFile'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap='nowrap'>".$li->column(3,$Lang['iPortfolio']['OtherReports']['CreationDate'])."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">
function link_to_upload()
{
	newWindow("upload.php?OtherReportID=" + document.form1.OtherReportID.value, 27);
}

function view_report(reportItemID, isDir) {
	if (isDir == '1') {
		document.form1.target = '_blank';
	}
	document.form1.action = 'view_report.php';
	document.form1.ReportItemID.value = reportItemID;
	document.form1.submit();
	document.form1.target = '_self';
	document.form1.action = '';
}
</SCRIPT>

<br />
<form method="POST" name="form1" id="form1" action="">

	<table border="0" cellspacing="0" cellpadding="5" width="95%">
		<tbody>
			<tr>
				<td>
					<img src="../../../../../images/2009a/nav_arrow.gif" border="0" align="absmiddle"> <?=$ReportTitle?> 
				</td>
			</tr>
			<tr>
				<td style="text-align:center;" colspan="2"></td>
			</tr>
		</tbody>
	</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
  			<tbody><tr>
  				<td nowrap=""><a href="javascript: link_to_upload();" class="contenttool"><img src="../../../../../images/2009a/icon_upload.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_upload?> </a></td>
  				<td width="100"></td>
  			</tr>
			</tbody></table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?=$li->displayPlain()?>
<?
if ($li->navigationHTML!="")
{
	echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
	echo "<tr class='tablebottom'>";
	echo "<td class=\"tabletext\" align=\"right\">";
	echo $li->navigationHTML;
	echo "</td>";
	echo "</tr>";
	echo "</table>";
}
?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="ReportItemID" id="ReportItemID" value="">
<input type="hidden" name="OtherReportID" id="OtherReportID" value="<?=$OtherReportID?>">
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
