<?php
/*
 * 	Log
 * 
 *	Date:	2015-11-13 [Cameron] create this file 	
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreport.php");

intranet_auth();
intranet_opendb();
 
$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

if(sizeof($_POST)==0) {
	header("Location: edit.php");	
	exit;
}

$OtherReportID = $_POST['OtherReportID'];
$lpfor = new libportfoliootherreport($OtherReportID);
$lpfor->setReportTitle($_POST['ReportTitle']);
$lpfor->setDescription($_POST['ReportDescription']);
$OtherReportID = $lpfor->save();

$flag = $OtherReportID ? "UpdateSuccess" : "UpdateUnsuccess";

intranet_closedb();

header("Location: index.php?xmsg=$flag");
 
?>