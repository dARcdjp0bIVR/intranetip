<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreport.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libpf_slp();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

########################################################
# Tab Menu : Start
########################################################

### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_OTHER_REPORT);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

if (count($_POST['RecordID']) <= 0) {
	exit;
}
$OtherReportID = $_POST['RecordID'][0];
$lpfor = new libportfoliootherreport($OtherReportID);

$val_ReportTitle = $lpfor->getReportTitle();
$val_Description = $lpfor->getDescription();


$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE="Javascript">

function checkform(FormObj)
{
	if (Trim(FormObj.ReportTitle.value)=="")
	{
		alert("<?=$Lang['iPortfolio']['OtherReports']['AlertInputTitle']?>");
		FormObj.ReportTitle.focus();
		return false;
	}
	
	return true;
}
</SCRIPT>

<br />
<form method="POST" name="form1" id="form1" action="edit_update.php" onsubmit="return checkform(this)">
<input type="hidden" name="OtherReportID" id="OtherReportID" value="<?=$OtherReportID?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tbody><tr>
	<td>
	<table border="0" cellspacing="0" cellpadding="5" width="95%">
	<tbody><tr>
		<td>
		<img src="../../../../../images/2009a/nav_arrow.gif" border="0" align="absmiddle"> <?=$button_edit?> 
		</td>
		
	</tr>
	<tr>
		<td style="text-align:center;" colspan="2">
				</td>
	</tr>
	</tbody></table>

	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tbody><tr>
		<td valign="top">
		<!-- CONTENT HERE -->

<div class="table_board"><span class="sectiontitle_v30"><?=$Lang['iPortfolio']['OtherReports']['ReportInformation']?></span><br>

		
<table class="form_table_v30">
<tr valign='top'>
	<td nowrap class='field_title'><span class='tabletextrequire'>*</span><?=$Lang['iPortfolio']['OtherReports']['Title']?> <br />(<?=$Lang['iPortfolio']['OtherReports']['TitleNotes']?>)</td>
	<td><?= $linterface-> GET_TEXTBOX_NAME("ReportTitle", "ReportTitle", $val_ReportTitle) ?>
	</td>
</tr>
<tr valign='top'>
	<td nowrap class='field_title'><?=$Lang['iPortfolio']['OtherReports']['Description']?></td>
	<td><?= $linterface->GET_TEXTAREA("ReportDescription", $val_Description)?></td>
</tr>

</table>


<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
    	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
</div>	


			</td>
		</tr>
		</tbody></table>
</tr>
</tbody></table>
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>


