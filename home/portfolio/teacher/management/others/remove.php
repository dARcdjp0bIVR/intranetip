<?php

// Modifing by 
/*
 * 	Purpose: remove other report scheme in iportfolio 
 * 	
 * 	Log
 * 
 * 	Date:	2015-11-16 [Cameron] create this file 
 * 	
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio_others/libportfoliootherreport.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

$lpf = new libportfolio2007();
$lf = new libfilesystem();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ADMIN_ACCESS_PAGE();

$RecordID = $_POST['RecordID'];
$nrRec = count($RecordID);
$nrSuccess = 0;
$xmsg = '';
if ($nrRec > 0) {
	foreach ((array)$RecordID as $id) {
		$lpfor = new libportfoliootherreport($id);		
		$isItemExist = $lpfor->isReportItemExist();
		if ($isItemExist) {
			$xmsg = 'NonEmptyItems';
		}
		else {
			$ret = $lpfor->deleteRecord();			
			if ($ret) {
				$nrSuccess++;	
			} 			
		}
		unset($lpfor);				
	}
}

if ($nrSuccess > 0 && $nrSuccess < $nrRec) {
	$xmsg = 'DeletePartiallySuccess';
}
else if ( $nrSuccess > 0 && $nrSuccess == $nrRec) {
	$xmsg = 'DeleteSuccess';	
}
else if ( $nrSuccess == 0) {
	$xmsg = $xmsg ? $xmsg : 'DeleteUnsuccess';
}


header("Location: $PATH_WRT_ROOT/home/portfolio/teacher/management/others/index.php?xmsg=$xmsg");

intranet_closedb();

?>
