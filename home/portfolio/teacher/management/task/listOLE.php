<?
### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ole_management_listOLE_oleType", "oleType");	
$arrCookies[] = array("ck_ole_management_listOLE_searchText", "searchText");	
$arrCookies[] = array("ck_ole_management_listOLE_requestType", "requestType");	
$arrCookies[] = array("ck_ole_management_listOLE_status", "status");	
$arrCookies[] = array("ck_ole_management_listOLE_academicYearID", "academicYearID");	


if(isset($clearCoo) && $clearCoo == 1){
	clearCookies($arrCookies);
}
else{
	updateGetCookies($arrCookies);
}

$oleType = trim($oleType);
$searchText = trim($searchText);
$requestType = trim($requestType);
$status = trim($status);
if (isset($academicYearID)) {
	$academicYearID = trim($academicYearID);
}
else {
	$academicYearID = Get_Current_Academic_Year_Id();
}


$oleType = ($oleType == "")? $ipf_cfg["OLE_TYPE_STR"]["INT"] : $oleType;


$objOLERecord = new OLETeacherRecord($UserID);
$objOLERecord->setOLEType($oleType);
$objOLERecord->createRecordTable();
$oleRecordTable = $objOLERecord->getOLETeacherRecordTable();
$IntExt = ($oleType == $ipf_cfg["OLE_TYPE_STR"]["INT"])? $ipf_cfg["OLE_TYPE_INT"]["INT"]: $ipf_cfg["OLE_TYPE_INT"]["EXT"];


$requestCond = _handleSearchCriteria($requestType,$searchText,$status,$oleType,$academicYearID);

$objDB = new libdb();

$sql_checkBox = " CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=\"checkbox\" pId= \"',op.Programid,'\"name=\"record_id[]\" value=\"', os.recordid ,'\">') as 'checkbox'";

$approverNameField = getNameFieldByLang("iu2.");
$studendDetails = "<table class=\"common_table_list\" border=\"0\">";
$studendDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['hours'].":</td> <td> ',os.Hours,'&nbsp;</td></tr>";
$studendDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['ole_role'].":</td> <td> ',os.role,'&nbsp;</td></tr>";
$studendDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['achievement'].":</td> <td> ',os.Achievement,'&nbsp;</td></tr>";
$studendDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$Lang['iPortfolio']['preferred_approver'].":</td> <td> ',ifnull({$approverNameField},''),'&nbsp;</td></tr>";
$studendDetails .= "</table>";

$categoryField = Get_Lang_Selection('oc.ChiTitle','oc.EngTitle');
$programDetails = "";
$programDetails .= "<table class=\"common_table_list\" border=\"0\">";
$programDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['date'] ." / ". $ec_iPortfolio['year_period'].": </td> <td> ',DATE_FORMAT(op.StartDate, '%Y-%m-%d'),if(op.EndDate is null or DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00','',concat(' -- ',DATE_FORMAT(op.EndDate, '%Y-%m-%d'))),'&nbsp;</td></tr>";
$programDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['category'].":</td> <td> ',{$categoryField},'&nbsp;</td></tr>";
$programDetails .= " <tr class=\"sub_row\"><td width=\"25%\">".$ec_iPortfolio['organization'].":</td> <td> ',op.Organization,'&nbsp;</td></tr>";
$programDetails .= " <tr class=\"sub_row programMoreDetails_',op.programid,'\"><td>&nbsp;</td> <td align=\"right\"><a href = \"javascript: void(0)\" onClick=\"moreProgram(',op.programid,')\">".$Lang['General']['More']."</a></td></tr>";
$programDetails .= "</table>";
//$programDetails = "";
$namefield = getNameFieldByLang("iu.");

$sql =  "SELECT
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as 'clsName',
			ycu.ClassNumber as 'clsNo',
			concat({$namefield},'<span class=\"row_link_tool\" style=\"display:inline-block;\"><a id=\"showAction_',os.recordid,'\" href = \"javascript: void(0)\"  class=\"zoom_in\" onClick=\"showRecordDetails(',os.recordid,'); return false;\"></a></span><div id=\"studentDetails_',os.recordid,'\" style=\"display:none;\">{$studendDetails}</div>') as 'studentName',
			concat(op.Title,'<span class=\"row_link_tool\" style=\"display:inline-block;\"><a class=\"zoom_in\" href=\"javascript: void(0)\" style=\"visibility:hidden;\"></a></span><div id =\"programDetails_',os.recordid,'\" style=\"display:none;\">{$programDetails}</div>') as 'ProgramTitle',
			DATE_format(os.InputDate, '%Y-%m-%d') as 'InputDate',
			CASE os.RecordStatus
						WHEN 1 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif\" title=\"{$ec_iPortfolio['pending']}\">'
						WHEN 2 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif\" title=\"{$ec_iPortfolio['approved']}\">'
						WHEN 3 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif\" title=\"{$ec_iPortfolio['rejected']}\">'
						WHEN 4 THEN '<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_teacher_input.gif\" title=\"{$ec_iPortfolio['teacher_submit_record']}\">'
					ELSE '--' END as 'studentRecordStatus',

			{$sql_checkBox}
		from 
			{$oleRecordTable} as tempT
		inner join 
			{$eclass_db}.OLE_PROGRAM as op on op.programid = tempT.programid
		left join 
			{$eclass_db}.OLE_CATEGORY as oc on op.Category = oc.RecordID
		inner join
			{$eclass_db}.OLE_STUDENT as os on os.recordid = tempT.recordid
		inner join 
			{$intranet_db}.INTRANET_USER as iu on iu.userid = tempT.userid
		left join
			{$intranet_db}.INTRANET_USER as iu2 on iu2.userid = os.RequestApprovedBy
		inner join 
			YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID) 
		inner JOIN 
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
		inner  JOIN
			YEAR y ON (y.YearID=yc.YearID)
		where 1
		and yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." 
		/*and op.title like 'tt%'*/
		";
$sql .= $requestCond;
//echo $sql."<br/>";
//$ary  = $objDB->returnArray($sql); //<-- for debug only
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$order = ($order == "") ? 0: $order;
$field = ($field == "") ? 4: $field;

$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$li->sql = $sql;
$li->field_array = array("clsName", "clsNo", "studentName","ProgramTitle","InputDate","studentRecordStatus");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='5%'>".$li->column($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th width='7%'>".$li->column($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='30%'>".$li->column($pos++, $iPort['student_name'])."</th>\n";
$li->column_list .= "<th width='43%'>".$li->column($pos++, $ec_iPortfolio['title'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $iPort['submitted_date'])."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column($pos++, $ec_iPortfolio['status'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("record_id[]")."</th>\n";

$h_tableContent = $li->display();

/**************/
/* SEARCH BOX */
/**************/
$h_searchText = htmlentities($searchText, ENT_QUOTES, "UTF-8");


/**************/
/* ACTION BAR */
/**************/
$h_ApprovalBtnDivID = 'ApprovalBtnDiv_abc';

$BtnArr[] = array('approve', "javascript:js_changeStudentItemStatus(".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"].")");
$BtnArr[] = array('reject', "javascript:js_changeStudentItemStatus(".$ipf_cfg["OLE_STUDENT_RecordStatus"]["rejected"].")");
$BtnArr[] = array('edit', "javascript:js_editItem();", '', $h_ApprovalBtnDivID);

$h_actionBar .= '<div id="'.$h_ActionBtnDivID.'">'.$linterface->Get_DBTable_Action_Button_IP25($BtnArr).'</div>'."\n";

/*******************/
/* SLT REQUST TYPE */
/*******************/

$RecordArray[$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromIndividual"]]	= $Lang['iPortfolio']['oleSourceByIndividualStudent'];
$RecordArray[$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromClass"]]		= $Lang['iPortfolio']['oleSourceFromMyClass'];
$RecordArray[$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromGroup"]]		= $Lang['iPortfolio']['oleSourceFromMyGroup'];
$requstTypeArray[$Lang['iPortfolio']['oleSourceByApprovalRequest']] = $RecordArray;

$RecordArray = array();
$RecordArray[$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["selfCreated"]]		= $Lang['iPortfolio']['oleOwn'];
$RecordArray[$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]]	= $ec_iPortfolio['SLP']['TeacherCreatedProgram'];
$RecordArray[$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]]	= $ec_iPortfolio['SLP']['StudentCreatedProgram'];
$requstTypeArray[$Lang['iPortfolio']['oleByCreator']] = $RecordArray;




$h_slt_RequestType  = getSelectByAssoArray($requstTypeArray,"name='requestType' onChange='jSUBMIT_FORM()'", $requestType, 1, 0, $ec_iPortfolio['all_programmes'], 2);
/**********************/
/* SLT REQUEST STATUS */
/**********************/
$status_selection = "<SELECT name='status' id='status' onChange='jSUBMIT_FORM()'>";
$status_selection .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
$status_selection .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection .= "</SELECT>";
$h_sltStuatType = $status_selection;

/***********************************************/
/* SLT ACADEMIC YEAR for OLE PROGRAM START DATE*/
/***********************************************/
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$h_sltYear = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='jSUBMIT_FORM()'", $academicYearID, 1, 0, $i_Attendance_AllYear, 2);

/****************/
/* SLT OLE TYPE */
/****************/

$oleTypeArray[] = array($ipf_cfg["OLE_TYPE_STR"]["INT"],$iPort["internal_record"]);
$oleTypeArray[] = array($ipf_cfg["OLE_TYPE_STR"]["EXT"],$iPort["external_record"]);
$h_slt_oleType = getSelectByArray($oleTypeArray, "name='oleType' onChange='jSUBMIT_FORM()'", $oleType, 0, 1, "", 2);

/*********************/
/* EXPAND ALL ACTION */
/*********************/
$h_expandAll = "<a href=\"javascript: void(0)\" onClick=\"expandAll()\" class=\"collapse tablelink\" id=\"expandControl\"> + ".$i_general_expand_all."</a>";

/**************/
/* RETURN MSG */
/**************/
switch($msg)
{
  case 1:
    $msg = "add";
    break;
  case 2:
    $msg = "update";
    break;
  case 3:
    $msg = "delete";
    break;
  case 4:
  	$msg = "update_failed";
  	break;
  default:
    $msg = "";
    break;
}
$h_returnMsg = ($msg == "") ? "" : $linterface->GET_SYS_MSG($msg);


/*******/
/* TAB */
/*******/
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_APPROVE_RECORD);
$linterface->LAYOUT_START();
include_once($template_script);
$linterface->LAYOUT_STOP();
exit();
///////////////////////////////
/////// FUNCTION LIST//////////
///////////////////////////////

function _handleSearchCriteria($requestType,$searchText,$oleSTUDENTRecordStatus,$oleType,$academicYearID){
	global $ipf_cfg;

	$returnCond  = "";
	switch (strtolower($requestType)) {

		case $ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["selfCreated"]:
			$returnCond  = " and selfCreated = ".$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["selfCreated"];
			break;

		case $ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromIndividual"]:
			$returnCond  = " and fromIndividual = ".$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromIndividual"];
			break;

		case $ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromClass"]:
			$returnCond  = " and fromClass = ".$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromClass"];
			break;

		case $ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromGroup"]:
			$returnCond  = " and fromGroup = ".$ipf_cfg["TEACHER_OLE_FILTER_RECORD"]["fromGroup"];
			break;

		case strtolower($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]):
			$returnCond = " AND op.ProgramType = '".$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]."'";
			break;

		case strtolower($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]):
			$returnCond = " AND op.ProgramType = '".$ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]."'";
			break;

		default :
			$returnCond  = "";
			break;
	}

	if($oleSTUDENTRecordStatus != ""){
		$returnCond .= " and (os.RecordStatus = {$oleSTUDENTRecordStatus}) ";
	}

	if($searchText != ""){
		$returnCond .= " and ( op.Title like '%{$searchText}%' ";
		$returnCond .= " or iu.EnglishName like '%{$searchText}%' ";
		$returnCond .= " or iu.ChineseName like '%{$searchText}%' ";
		$returnCond .= " or yc.ClassTitleB5 like '%{$searchText}%' ";
		$returnCond .= " or yc.ClassTitleEN like '%{$searchText}%' )";
	}


	if($oleType != ""){
		$returnCond .= " and op.intext = '{$oleType}'";
	}	

	if($academicYearID != ""){
		$returnCond .= " and op.AcademicYearID = {$academicYearID}";
	}

	return $returnCond;
}

?>
