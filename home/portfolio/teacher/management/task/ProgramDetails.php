<?

include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
$programID	= trim($programID);

$objProgram = new ole_program($programID);
$eleDetails = $objProgram->getEleDetails();

$h_ele = "";

if(strtolower($objProgram->getIntExt()) == strtolower($ipf_cfg["OLE_TYPE_STR"]["INT"])){
	//DISPLAY ELE FOR INT PROGRAM ONLY
	$h_ele = "<tr class=\"sub_row\"><td>".$ec_iPortfolio['ele'].":</td><td>";

	for($i = 0,$i_max = sizeof($eleDetails);$i< $i_max; $i++){
		$h_ele .= " - ".$eleDetails[$i]."<br/>";
	}
	$h_ele .= "</td></tr>";
}


$h_details = "<tr class=\"sub_row\"><td>".$ec_iPortfolio['details'].":</td><td>".$objProgram->getDetails()."</td></tr>";
$h_remarks = "<tr class=\"sub_row\"><td>".$ec_iPortfolio['school_remarks'].":</td><td>".$objProgram->getSchoolRemarks()."</td></tr>";

$subCatName = Get_Lang_Selection($objProgram->getSubCatChiTitle(),$objProgram->getSubCatEngTitle());
$h_subCatName = "<tr class=\"sub_row\"><td>".$ec_iPortfolio['sub_category'].":</td><td>".$subCatName."</td></tr>";

$h_period = "<tr class=\"sub_row\"><td>".$ec_iPortfolio['year'].":</td><td>".$objProgram->getPeriod()."</td></tr>";

$h_return = $h_ele.$h_period.$h_subCatName.$h_remarks.$h_details;

echo $h_return;
?>