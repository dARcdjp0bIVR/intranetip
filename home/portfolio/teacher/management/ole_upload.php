<?php
// Modifing by 
##########################################################
## Modification Log
##	2020-01-08 	Philips [DM#3733]
##	Use $btn_close instead of $btn_cancel
##  2016-07-15  Henry HM
##  Replaced all split() to explode() for PHP 5.4 
##
##	2015-05-11	Bill	[Defect List #2866]
##	Change Edit Button display from Edit to Update
##
##########################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$lpf->ACCESS_CONTROL("ole");

$CurrentPage = "OLE_Upload_File";
$title = $iPort['OLE_Upload_File'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

/////////  Pre submit form - pass js variable to php ////////////
if(!$IS_PASS_FROM_JS)
{
	$x = "<form name=\"form_js\" enctype=\"multipart/form-data\" action=\"ole_upload.php\" method=\"POST\">\n";	
	$x .= "<input type=\"hidden\" name=\"StudentID\" value=\"".$StudentID."\" >\n";
	$x .= "<input type=\"hidden\" name=\"EventID\" value=\"".$EventID."\" >\n";
	$x .= "<input type=\"hidden\" name=\"RecordID\" value=\"".$RecordID."\" >\n";
	$x .= "<input type=\"hidden\" name=\"IS_PASS_FROM_JS\" value=\"1\" >\n";
	$x .= "<input type=\"hidden\" name=\"AttachmentFileStr\" value=\"\" >\n";
	$x .= "</form>";
	
	$x .= "<SCRIPT LANGUAGE=Javascript>";
	$x .= "var TmpAttachmentStr = window.opener.jGet_AllAttachment(".$StudentID.");";
	$x .= "document.form_js.AttachmentFileStr.value = TmpAttachmentStr;";
	$x .= "document.form_js.submit();";
	$x .= "</SCRIPT>";
	echo $x;
}
/////////  end Pre submit form - pass js variable to php ////////////

////////////////////// Attahcment html /////////////////////////////////////////////
$attach_count = 0;
$original_file_number = 0;
if($RecordID!="")
{
	$DataArr = $lpf->returnOLERecordByRecordID($RecordID);
	
	list($startdate, $enddate, $title, $category, $ELE, $role, $hours,$organization, $achievement, $details, $ole_file, $approved_by, $remark, $process_date, $record_status, $user_id) = $DataArr;

	$namefield = ($ck_default_lang=="chib5") ? "EnglishName" : "ChineseName";
	# Get student name
	$StudentInfo = $lpf->returnUserNameAndClass($user_id);
	$NameHTML = "<tr valign='top' class='tabletext'>
					<td nowrap='nowrap'>".$ec_iPortfolio['growth_student_name']." :</td>
					<td>".$StudentInfo["EnglishName"]." (".$StudentInfo["ChineseName"].")"."</td>
				</tr>";
	$NameHTML .= "<tr valign='top' class='tabletext'>
					<td nowrap='nowrap'>".$ec_iPortfolio['class_and_number']." :</td>
					<td>".$StudentInfo["ClassName"]." - ".$StudentInfo["ClassNumber"]."</td>
				</tr>";

	//# get ELE Array
	//$ELEArr = explode(",", $ELE);
	//$ELEArr = array_map("trim", $ELEArr);
	
	$tmp_arr = explode(":", $ole_file);
	$original_file_number = sizeof($tmp_arr);
	$folder_prefix0 = $eclass_root."/files/portfolio/ole/r".$RecordID;
	$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$RecordID;
	
	// Original exist file
	for($i=0; $i<sizeof($tmp_arr); $i++)
	{
		$attach_file = $tmp_arr[$i];
		
		if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
		{
			$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
			$attachments_html .= "<input type='checkbox' name='is_need_$attach_count' checked onClick=\"triggerFile(this.form, '".addslashes($attach_file)."', $attach_count)\">&nbsp;";
			$attachments_html .= "<a class=\"contenttool\" href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count'>".get_file_basename($attach_file)." ($file_size)</a>";
			$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
			$attach_count ++;
		}
	}
	
	// Temp store folder file
	$folder_prefix1 = $eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID;
	$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID;
	
	$tmp_arr = explode(":", $AttachmentFileStr);
	for($i=0; $i<sizeof($tmp_arr); $i++)
	{
		$attach_file = $tmp_arr[$i];
		if (trim($attach_file)!="" && file_exists($folder_prefix1."/".$attach_file))
		{
			$file_size = ceil(filesize($folder_prefix1."/".$attach_file)/1024) . $file_kb;
			$attachments_html .= "<input type='checkbox' name='is_need_$attach_count' checked onClick=\"triggerFile(this.form, '".addslashes($attach_file)."', $attach_count)\">&nbsp;";
			$attachments_html .= "<a class=\"contenttool\" href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count'>".get_file_basename($attach_file)." ($file_size)</a>";
			$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
			$attach_count ++;
		}
	}
} // end if check is RecordID exist
else
{
	// Temp store folder file
	$folder_prefix1 = $eclass_root."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID;
	$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/tmp/t_".$UserID."/s_".$StudentID;
	
	$tmp_arr = explode(":", $AttachmentFileStr);
	for($i=0; $i<sizeof($tmp_arr); $i++)
	{
		$attach_file = $tmp_arr[$i];
		if (trim($attach_file)!="" && file_exists($folder_prefix1."/".$attach_file))
		{
			$file_size = ceil(filesize($folder_prefix1."/".$attach_file)/1024) . $file_kb;
			$attachments_html .= "<input type='checkbox' name='is_need_$attach_count' checked onClick=\"triggerFile(this.form, '".addslashes($attach_file)."', $attach_count)\">&nbsp;";
			$attachments_html .= "<a class=\"contenttool\" href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count'>".get_file_basename($attach_file)." ($file_size)</a>";
			$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
			$attach_count ++;
		}
	}
} // end if check is RecordID not exist


?>

<!-- Content Here -->
<SCRIPT LANGUAGE=Javascript>
function triggerFile(formObj, my_file, my_index){
	var is_remove = (eval("formObj.is_need_"+my_index+".checked==false"));
	var link = document.getElementById('a_'+my_index);
	link.style.textDecoration = is_remove ? 'line-through' : 'none';
}

////////// From Script JS /////////////////////////////
function jInsertTableRow(jTableName, jFileNumber){
	// first argument is jTableName
	// other arguments after will be td contents

	var row = jTableName.insertRow(jFileNumber);

	for(var i=2; i<arguments.length; i++){
        cell = row.insertCell(i-2);
		cell.innerHTML = arguments[i];
    }
}

function jAddMoreFileField(jFormObj, jTableName, jFileNumberName, jFileFieldName){
	// prepare td contents
	// manipulate jInsertTableRow()
	
	if (document.all || document.getElementById)
	{
		var table = document.all ? document.all[jTableName] : document.getElementById(jTableName);
		
		var file_no = parseInt(document.getElementById(jFileNumberName).value);
	
		// No need to check document.all, otherwise, not work in Firefox.
//		if (document.all)
		{
			var td = "<input class=file type=file name='" + jFileFieldName + (file_no + 1) + "' size='40' /><input type=hidden name='" + jFileFieldName + (file_no + 1) + "_hidden' />";
			jInsertTableRow(table, file_no, td);
			document.getElementById(jFileNumberName).value = file_no + 1;
		}
	}
}
/////////////////////////////////////////////////////////////////
</SCRIPT>

<FORM enctype="multipart/form-data" action="ole_upload_update.php" method="POST" name="form1" onSubmit="this.btn_upload.disabled=true;this.btn_edit.disabled=true;">
	<table width="450" border="0" cellspacing="0" cellpadding="0">
		<tr>
          <td height="10" colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="top" class="tabletext"><?=$iPort["upload_file"]?>:</td>
          <td>
          <?= $attachments_html ?>
          <?= $lpf->GetFileUpload("tableAttachment", "attachment_size", "ole_file")?>
          </td>
        </tr>
    </table>
    <br />
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_upload?>" name="btn_upload">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_update?>" name="btn_edit">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_close?>" onClick="self.close()">
		</td>
	</tr>
	</table>
	<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
	<input type="hidden" name="EventID" value="<?=$EventID?>" >
	<input type="hidden" name="RecordID" value="<?=$RecordID?>" >
	<input type="hidden" name="attachment_size" id="attachment_size" value="1">
	<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>">
	<input type="hidden" name="original_file_number" value="<?=$original_file_number?>">
</FORM>

<SCRIPT LANGUAGE=Javascript>
//var a_Str = window.opener.jGet_AllAttachment(<?=$StudentID?>);
//alert(a_Str);

// set the tmp file list, to uncheck the remove item
var RemoveItemIDStr = window.opener.jGet_RemoveItemID(<?=$StudentID?>);
var ItemIDArr = new Array();
ItemIDArr = RemoveItemIDStr.split(":");

for(var i=0; i < ItemIDArr.length; i++)
{
	var k = ItemIDArr[i];
	if(k != "" && k >= 0)
	{
		var check_obj = eval("document.form1.is_need_"+k);
		check_obj.click();
	}
}
</SCRIPT>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
