<?php
include_once($intranet_root."/includes/libimporttext.php");
include ($intranet_root.'/includes/import/importDataManager.php');
include ($intranet_root.'/includes/import/importData.php');
include ($intranet_root.'/includes/import/importDataDefinition.php');
include ($intranet_root.'/includes/portfolio25/import/oleImportData.php');
include ($intranet_root.'/includes/portfolio25/import/oleLasalleImportData.php');
include_once($intranet_root."/includes/libfilesystem.php");

//skip to handle data with "

$importObj = new libimporttext();


$objOleImportData = new oleImportData();

if($r_intext == $ipf_cfg["OLE_TYPE_INT"]["EXT"]){
	$objOleImportData->setOLEIntExt($ipf_cfg["OLE_TYPE_STR"]["EXT"]);
}else{
	$objOleImportData->setOLEIntExt($ipf_cfg["OLE_TYPE_STR"]["INT"]);
}

$objLasalleImportData = new oleLasalleImportData();
$objOleImportData->setExtraImportData($objLasalleImportData);

$libfs = new libfilesystem();

$DefaultCsvHeaderArr = $objOleImportData->getDataDefaultCsvHeaderArr();
$ColumnPropertyArr = $objOleImportData->getDataColumnPropertyArr();


$fileName = $_FILES['csvfile']['name'];

$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('iportfolio/ole/', $csvfile, $fileName);
$objOleImportData->setBatchNo($TargetFilePath);




$dataArray = $importObj->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
$encodeTargetFilePath = base64_encode($TargetFilePath);


$arrayWithDataOnly = $objOleImportData->dataHandle($dataArray , $DefaultCsvHeaderArr, $ColumnPropertyArr);
//debug_r($arrayWithDataOnly);

$objImportDataManager = new importDataManager($objOleImportData);
$objImportDataManager->setImportDataSource($arrayWithDataOnly);

$objImportDataManager->checkImportData();


$h_error = $objImportDataManager->getDisplayErrorRecord();
$h_SuccessfulRecord = $objImportDataManager->getNoOfValidRecord();

$noOfInvalidRecord = $objImportDataManager->getNoOfInvalidRecord();
$h_FailureRecord = $noOfInvalidRecord;
$h_StepTable = $linterface->GET_IMPORT_STEPS($CurrStep=2);

$Disabled  = 0;

if($noOfInvalidRecord>0){
	$Disabled = 1;
}
$ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '',$Disabled);
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");


$template_script = 'templates/import/lasalleImportStep1.tmpl.php';
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>

<?php



?>