<?
include_once ($intranet_root.'/includes/import/importDataManager.php');
include_once ($intranet_root.'/includes/import/importData.php');
include_once ($intranet_root.'/includes/import/importDataDefinition.php');
include_once ($intranet_root.'/includes/portfolio25/import/oleImportData.php');
include_once ($intranet_root.'/includes/portfolio25/import/oleLasalleImportData.php');
include_once($intranet_root.'/includes/portfolio25/oleCategory/libpf-category.php');
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
$objOleImportData = new oleImportData();


$objLasalleImportData = new oleLasalleImportData();
$objOleImportData->setExtraImportData($objLasalleImportData);

$objImportDataManager = new importDataManager($objOleImportData);
$h_guide = $objImportDataManager->getDisplayGuide();

//$aaa = $objOleImportData->getDataDefinition();

### Steps Table
$h_StepTable = $linterface->GET_IMPORT_STEPS($CurrStep=1);

### Buttons

$h_ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();",'ContinueBtn');
$h_CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");

$h_MandatoryRemarks = $linterface->MandatoryField();
//$h_ReferenceRemarks = $linterface->ReferenceField();

$h_fileURL = 'FileName=ole_import_sample.csv&Path=/home/portfolio/teacher/management/import/';
$h_fileURL = base64_encode($h_fileURL);

$h_download  = '<a class="contenttool" href="/home/portfolio/teacher/management/download.php?r_value='.$h_fileURL.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'.$import_csv['download'].'</a>';


$libCategory = new Category();
$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false, $orderByField='RecordID');

$Category_HTML = "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$Category_HTML .= "<tr><td nowrap='nowrap'><u>".$ec_iPortfolio['category_code']."</u></td></tr>";
//for($i=0; $i<sizeof($CategoryArray); $i++)
for($i=0; $i<sizeof($CategoryInfoArr); $i++)
{
	//$cat_id = $CategoryArray[$i][0];
	//$cat_name = $CategoryArray[$i][1];
	$cat_id = $CategoryInfoArr[$i]['RecordID'];
	$cat_name = Get_Lang_Selection($CategoryInfoArr[$i]['ChiTitle'], $CategoryInfoArr[$i]['EngTitle']);
	$Category_HTML .= "<tr><td>".$cat_id." - ".$cat_name."</td></tr>";
}
$Category_HTML .= "</table>";


$libpf_ole = new libpf_ole();
$ELEArray = $libpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]), 'RecordID');
$ELE_HTML = "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
	$ELE_HTML .= "<tr><td nowrap='nowrap'><u>".$ec_iPortfolio['ele_code']."</u></td></tr>";
	
	$numOfELE = count($ELEArray);
	for($i=0; $i<$numOfELE; $i++) {
		// 20110902 Commented by Ivan [2011-0826-0949-55066]
		//$ELE_HTML .= "<tr><td>".($i+1)." - ".$ELEArray[$i]."</td></tr>";
		
		$_ELE_ID = $ELEArray[$i]['RecordID'];
		$_ELE_Name = Get_Lang_Selection($ELEArray[$i]['ChiTitle'], $ELEArray[$i]['EngTitle']);
		$ELE_HTML .= "<tr><td>".$_ELE_ID." - ".$_ELE_Name."</td></tr>";
	}
	$ELE_HTML .= "</table>";
	

$ReferenceHTML = "<table width='80%' border='0' cellspacing='0' cellpadding='3'>";
//$ReferenceHTML .= "<tr><td colspan='3'>".$Import_Guide."</td></tr>";
$ReferenceHTML .= "<tr><td valign='top'>".$Category_HTML."</td><td width='70'>&nbsp;</td><td valign='top'>".$ELE_HTML."</td></tr>";
$ReferenceHTML .= "</table>";


$template_script = 'templates/import/lasalle.tmpl.php';
$linterface->LAYOUT_START($ReturnMsg);
include_once($template_script);
$linterface->LAYOUT_STOP();
?>


