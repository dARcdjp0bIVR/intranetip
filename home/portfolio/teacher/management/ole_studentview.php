<?php
/**
 * [Modification Log] Modifying By: yat
 * ************************************************
 * Date:	2013-08-28	Ivan [2013-0805-0935-09073]
 * 			Improved: Show activated iPortfolio account student only
 * 
 * Date:	2013-06-19	YatWoon
 *			Improved: Retreive data with academic year filter, no need to check the program start date within the academic year [Case#2013-0614-1109-35167]
 * ************************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-mgmt-group.php");

# set cookies
$arrCookies = array();
$arrCookies[] = array("iPf_Mgmt_Slp_StudnetView_Keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$Keyword = '';
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$LibPortfolio = new libpf_slp();
$lpf_ui = new libportfolio_ui();
$lpf_mgmt_grp = new libpf_mgmt_group();
$linterface = new interface_html();


########################################################
# Data Preprocessing : Start
########################################################
//GET PASSING VARIABLE
$IntExtType = ($IntExt == 1) ? "EXT" : "INT";

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;
$no_of_col = ($isReadOnly) ? 4 : 5;
define ("STANDARD_NO_OF_COLUMN", $no_of_col);

$currentAcademicYear = Get_Current_Academic_Year_ID();

$approvalSettings  = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($IntExtType);
if($approvalSettings["Elements"]["ClassTeacher"] == 1){
	$allowClassTeacherApprove = 1;	
}

$mgmt_ele_arr = array();
if($IntExtType == "INT"){
	// for the component group , it support with INT only
	//CHECK user in which group
	$ec_uID = $LibPortfolio->IP_USER_ID_TO_EC_USER_ID($UserID);
	$group_id_arr = $lpf_mgmt_grp->GET_USER_IN_GROUP($ec_uID);

	//get USER's OLE component
	$mgmt_ele_arr = $lpf_mgmt_grp->getGroupComponent($group_id_arr);
}
$isAdmin = $LibPortfolio->isOLEMasterAdmin($mgmt_ele_arr);	
// Group Component
$groupComponentConds = "";
if(!empty($group_id_arr))
{

	for($i=0, $i_max=count($mgmt_ele_arr); $i<$i_max; $i++)
	{
		$mgmt_ele = $mgmt_ele_arr[$i];
		$_tmp = " INSTR(op.ELE, '{$mgmt_ele}') > 0 ";
		$groupComponentConds .= $groupComponentConds == "" ? $_tmp : "OR ".$_tmp;
	}

	$groupComponentConds = $groupComponentConds == "" ? "": " OR ({$groupComponentConds})";
}
if ($allowClassTeacherApprove == 1){
// class student
$classStudentSql = "SELECT DISTINCT UserID ";
$classStudentSql .= "FROM {$intranet_db}.YEAR_CLASS_USER ";
$classStudentSql .= "WHERE YearClassID IN (";
$classStudentSql .= "SELECT yc.YearClassID FROM {$intranet_db}.YEAR_CLASS yc INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON yc.YearClassID = yct.YearClassID WHERE yct.UserID = {$UserID} AND yc.AcademicYearID = {$currentAcademicYear}";
$classStudentSql .= ")";
}
// Temporary table for accessible student
$sql = "CREATE TEMPORARY TABLE tempStudentList (StudentID int(11), YearClassID int(8), PRIMARY KEY (StudentID))";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);
//debug_r($ck_function_rights);
//debug_r("count ".count($mgmt_ele_arr));
// Handle admin
if(
	(
		//strstr($ck_function_rights, "Profile:OLR") && count($mgmt_ele_arr) == 0		// OLE admin
		$isAdmin == true // OLE admin
		&& (!isset($student_own) || $student_own == "all")		// all student display
	) || $isReadOnly)
{
	$sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
	$sql .= "SELECT DISTINCT UserID ";
	$sql .= "FROM {$intranet_db}.INTRANET_USER ";
	$sql .= " WHERE RecordType = 2";
//echo $sql."<Br/>";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
	$ldb->db_db_query($sql);
}
// Handle group teacher
else if($groupComponentConds != "")
{
	$sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
	$sql .= "SELECT DISTINCT os.UserID ";
	$sql .= "FROM {$eclass_db}.OLE_STUDENT os ";
	$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM op ON op.ProgramID = os.ProgramID ";
	$sql .= "WHERE 0 {$groupComponentConds}";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
	$ldb->db_db_query($sql);
}
// Handle class teacher
if($allowClassTeacherApprove == 1)
{
	$sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
	$sql .= $classStudentSql;
//error_log($sql."\n", 3, "/tmp/aaa.txt");
	$ldb->db_db_query($sql);
}
// Handle approve teacher
$sql = "INSERT IGNORE INTO tempStudentList (StudentID) ";
$sql .= "SELECT DISTINCT UserID ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT ";
$sql .= "WHERE ApprovedBy = {$UserID} OR RequestApprovedBy = {$UserID}";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);

// Set current YearClassID for students
$sql = "UPDATE tempStudentList tsl ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON tsl.StudentID = ycu.UserID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = {$currentAcademicYear} ";
$sql .= "SET tsl.YearClassID = yc.YearClassID";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);

$ELEArray = $LibPortfolio->GET_ELE();
$ELECount = ($IntExt == 1) ? 0 : count($ELEArray);


########################################################
# Data Preprocessing : End
########################################################

########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 1);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

########################################################
# Operations : Start
########################################################
# tab menu
$tab_menu_html = $lpf_ui->GET_TAB_MENU($TabMenuArr);

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$academicYearID = (isset($academicYearID))? $academicYearID : Get_Current_Academic_Year_ID();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' onChange='jSUBMIT_FORM()'", $academicYearID, 1, 0, $i_Attendance_AllYear, 2);

# Switch all students / my students
if(strstr($ck_function_rights, "Profile:OLR") && count($mgmt_ele_arr) == 0)
{
	$StudentArray[] = array("all", $ec_iPortfolio['all_students']);
	$StudentArray[] = array("my", $ec_iPortfolio['my_students']);
	$student_own = (!isset($student_own)) ? $StudentArray[0][0] : $student_own;
	$student_selection_html = getSelectByArray($StudentArray, "name='student_own' onChange='jSUBMIT_FORM()'", $student_own, 0, 1, "", 2);
}

/*
# year class selection
if(!strstr($ck_function_rights, "Profile:OLR") || $student_own!="all")
{
  $sql =  "
            SELECT DISTINCT
              yc.YearClassID
            FROM
              {$intranet_db}.YEAR_CLASS_TEACHER AS yct
            INNER JOIN
              {$intranet_db}.YEAR_CLASS AS yc
            ON
              yc.YearClassID = yct.YearClassID
            WHERE
              yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." AND
              yct.UserID = ".$UserID."
          ";
  $auth_class_arr = $ldb->returnVector($sql);
}

$lfcm = new form_class_manage();
$t_year_class_arr = $lfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_class_arr = array();
for($i=0; $i<count($t_year_class_arr); $i++)
{
  $t_year_class_id = $t_year_class_arr[$i]['YearClassID'];
  $t_year_class_title = Get_Lang_Selection($t_year_class_arr[$i]['ClassTitleB5'],$t_year_class_arr[$i]['ClassTitleEN']);
  
  if(is_array($auth_class_arr) && !in_array($t_year_class_id, $auth_class_arr))
    continue;
  
  $year_class_arr[] = array($t_year_class_id, $t_year_class_title);
}
$yc_selection_html = getSelectByArray($year_class_arr, "name='yearClassID' onChange='jSUBMIT_FORM()'", $yearClassID, 1, 0, $i_general_all_classes, 2);
*/

// class selection
$sql = "SELECT DISTINCT yc.YearClassID, IF(IFNULL(".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN").", '') = '', ".Get_Lang_Selection("yc.ClassTitleEN", "yc.ClassTitleB5").", ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN").") ";
$sql .= "FROM tempStudentList tsl ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON tsl.YearClassID = yc.YearClassID ";
$sql .= "INNER JOIN {$intranet_db}.YEAR y ON y.YearID = yc.YearID ";
$sql .= "ORDER BY y.Sequence, yc.Sequence";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$year_class_arr = $ldb->returnArray($sql);
$yc_selection_html = getSelectByArray($year_class_arr, "name='yearClassID' onChange='jSUBMIT_FORM()'", $yearClassID, 1, 0, $i_general_all_classes, 2);

if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")){	
	# new button
//	$new_btn_html= "<a href=\"ole_new.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">{$button_new}</a>";
	$new_btn_html= '<div class="Conntent_tool">
              			<a href="ole_new.php?IntExt='.$IntExt.'" class="new">'.$Lang['Btn']['New'].'</a>
					</div>';
}

if(!$isReadOnly && strstr($ck_function_rights, "Profile:OLR")){
	# import button
//	$import_btn_html = strstr($ck_function_rights, "ImportData") ? "<a href=\"ole_import.php?IntExt={$IntExt}&FromPage=pview\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_import}</a>" : "";
	$import_btn_html = strstr($ck_function_rights, "ImportData") ? '<div class="Conntent_tool">
              			<a href="ole_import.php?IntExt='.$IntExt.'&FromPage=pview" class="import">'.$Lang['Btn']['Import'].'</a>
					</div>' : '';
}

if($isReadOnly && strstr($ck_function_rights, "ExportData")){
	# export button
//	$export_btn_html = "<a href=\"javascript:doExport('ole')\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_export}</a>";
	$export_btn_html= '<div class="Conntent_tool">
              			<a href="javascript:doExport(\'ole\')" class="export">'.$Lang['Btn']['Export'].'</a>
					</div>';
}


// Search box
$Keyword = $_GET['Keyword'];
$OthersPara = 'onkeypress="js_Changed_Keyword(event);"';
$searchbox_html = $linterface->Get_Search_Box_Div('Keyword', $Keyword, $OthersPara);
$conds_Keyword = " And (iu.EnglishName Like '%".$ldb->Get_Safe_Sql_Like_Query($Keyword)."%' Or iu.ChineseName Like '%".$ldb->Get_Safe_Sql_Like_Query($Keyword)."%') ";
########################################################
# Operations : End
########################################################

########################################################
# Table content : Start
########################################################
$rowStyle = ($IntExt == 1) ? "" : "rowspan=\"2\"";

$pageSizeChangeEnabled = true;
$checkmaster = true;

# Filter conditions
$ttable_extra_table = empty($academicYearID) ? "" : "{$intranet_db}.ACADEMIC_YEAR AS ay, {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt, ";
//$ttable_cond = empty($academicYearID) ? "" : " AND (op.StartDate BETWEEN ayt.TermStart and ayt.TermEnd AND ayt.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID = ".$academicYearID.")";
//$ttable_cond = empty($academicYearID) ? "" : " AND (op.AcademicYearID= ".$academicYearID." and ayt.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID = ".$academicYearID.")";
$ttable_cond = empty($academicYearID) ? "" : " AND (op.AcademicYearID= ".$academicYearID.")";
$ttable_cond .= ($IntExt == "1") ? " AND op.IntExt = 'EXT'" : " AND op.IntExt = 'INT'";

$cond = empty($yearClassID) ? "" : " AND yc.YearClassID = ".$yearClassID;
/*
if($student_own != "all")
{
	// Class Teacher
	$sql = "SELECT yc.YearClassID FROM {$intranet_db}.YEAR_CLASS_TEACHER AS yct INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON yc.YearClassID = yct.YearClassID WHERE yct.UserID = '".$UserID."' AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID();
	$ycID_arr = $ldb->returnVector($sql);
	$ycID_str = empty($ycID_arr) ? "0" : implode(", ", $ycID_arr);

	$cond .= " AND yc.YearClassID IN ({$ycID_str})";
}

if($allowClassTeacherApprove == 1){
// handle for class teacher
	$sql = "";
	$sql .= "SELECT distinct os.userID as 'userid' FROM {$eclass_db}.OLE_PROGRAM op ";
	$sql .= "INNER JOIN {$eclass_db}.OLE_STUDENT os ON op.ProgramID = os.ProgramID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON os.UserID = ycu.UserID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_TEACHER yct ON ycu.YearClassID = yct.YearClassID ";
	$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON yc.YearClassID = yct.YearClassID AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()." ";
	$sql .= "WHERE yct.UserID = {$UserID} ";
//echo " class ==>".$sql."<Br/>";
	$classStudentRS = $ldb->returnVector($sql);

	$classStudentStr = "";
	$classStudentSql = "";
	if(is_array($classStudentRS) && sizeof($classStudentRS) > 0 ){
		$classStudentStr = implode(",",$classStudentRS);
	}
	if($classStudentStr  != ""){
		// $classStudentStr has value
		$classStudentSql = " or os.Userid in({$classStudentStr}) ";
	}
}
*/
// end of handle class teacher

//debug_r("classStudentRS ==>".$classStudentRS);
# Temp table for OLE records
$sql =  "
          CREATE TEMPORARY TABLE tempRecCount(
            UserID int(11),
        ";
if ($IntExt != 1)
{
	foreach($ELEArray as $ELECode => $ELETitle)
	{
    $temp_field_name = "t_".str_replace(array("[", "]"), "", $ELECode);
    $sql .= $temp_field_name." int(8) DEFAULT 0, ";
	}
}
$sql .= "
						editRecCount int(8),
						recCount int(8),
						pendingCount int(8),
						totalPendingCount int(8),
						PRIMARY KEY (UserID)
          )
        ";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);

// rec count of individual component
if ($IntExt != 1)
{
	foreach($ELEArray as $ELECode => $ELETitle)
	{
		$temp_field_name = "t_".str_replace(array("[", "]"), "", $ELECode);
    $sql = "INSERT INTO tempRecCount (UserID, {$temp_field_name}) ";
    /*
    $sql .= "SELECT os.UserID, count(*) ";
    $sql .= "FROM {$ttable_extra_table} {$eclass_db}.OLE_STUDENT AS os ";
    $sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
    $sql .= "WHERE INSTR(op.ELE, '".$ELECode."') > 0 {$ttable_cond} ";
    $sql .= "GROUP BY os.UserID ";
    */
    $sql .= "SELECT os.UserID, count(*) ";
    $sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
    $sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
    $sql .= "WHERE INSTR(op.ELE, '".$ELECode."') > 0 {$ttable_cond} ";
    $sql .= "GROUP BY os.UserID ";
    
    $sql .= "ON DUPLICATE KEY UPDATE {$temp_field_name} = VALUES({$temp_field_name})";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
//debug_pr($sql);
    $ldb->db_db_query($sql);
	}
}

// total rec count of student 
$sql = "INSERT INTO tempRecCount (UserID, recCount) ";
/*
$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$ttable_extra_table} {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE 1 {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
*/
$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE 1 {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE recCount = VALUES(recCount)";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);

// total pending rec count of student
$sql = "INSERT INTO tempRecCount (UserID, totalPendingCount) ";
/*
$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$ttable_extra_table} {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
*/
$sql .= "SELECT os.UserID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE totalPendingCount = VALUES(totalPendingCount)";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
$ldb->db_db_query($sql);

// pending rec count of student which can be approved by user
$sql = "INSERT INTO tempRecCount (UserID, pendingCount) ";
/*
$sql .= "SELECT os.USERID, count(*) ";
$sql .= "FROM {$ttable_extra_table} {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} ";
//$sql .= "AND (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$classStudentSql} {$groupComponentConds}) {$ttable_cond} ";
$sql .= "AND (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$groupComponentConds} ";
if($allowClassTeacherApprove == 1){ 
	$sql .= "OR os.UserID IN ($classStudentSql)";
}
$sql .= ") {$ttable_cond} ";

$sql .= "GROUP BY os.UserID ";
*/
$sql .= "SELECT os.USERID, count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE os.RecordStatus = {$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]} ";
$sql .= "AND (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$groupComponentConds} ";
if($allowClassTeacherApprove == 1){ 
	$sql .= "OR os.UserID IN ($classStudentSql)";
}
$sql .= ") {$ttable_cond} ";
$sql .= "GROUP BY os.UserID ";
$sql .= "ON DUPLICATE KEY UPDATE pendingCount = values(pendingCount)"; 
//error_log($sql."\n", 3, "/tmp/aaa.txt");
//echo $sql."<Br/>"; 
$ldb->db_db_query($sql);

// rec count of student which can be edited by user
$sql = "INSERT INTO tempRecCount (UserID, editRecCount) ";
/*
$sql .= "SELECT os.USERID,count(*) ";
$sql .= "FROM {$ttable_extra_table} {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
//$sql .= "WHERE (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$classStudentSql} {$groupComponentConds}) {$ttable_cond} ";
$sql .= "WHERE (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$groupComponentConds} ";

if($allowClassTeacherApprove == 1){
	$sql .= "OR os.UserID IN ($classStudentSql)";
}

$sql .= ") {$ttable_cond} ";

$sql .= "GROUP BY os.USERID ";
*/
$sql .= "SELECT os.USERID,count(*) ";
$sql .= "FROM {$eclass_db}.OLE_STUDENT AS os ";
$sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
$sql .= "WHERE (op.CreatorID = {$UserID} OR os.RequestApprovedBy = {$UserID} OR os.ApprovedBy = {$UserID} {$groupComponentConds} ";

if($allowClassTeacherApprove == 1){
	$sql .= "OR os.UserID IN ($classStudentSql)";
}

$sql .= ") {$ttable_cond} ";

$sql .= "GROUP BY os.USERID ";
$sql .= "ON DUPLICATE KEY UPDATE editRecCount = values(editRecCount)";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
//echo $sql."<Br/>";

$ldb->db_db_query($sql);

# Main query
if ($order=="") $order=1;
if ($field=="") $field=1;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql =  "
          SELECT
            ".getNameFieldByLang2("iu.")." AS DisplayName,
            CONCAT(".libportfolio::GetYearClassTitleFieldByLang("yc.").", ' - ', iu.ClassNumber),
        ";
if ($IntExt != 1)
{
  foreach($ELEArray as $ELECode => $ELETitle)
	{
		$temp_field_name = "t_".str_replace(array("[", "]"), "", $ELECode);
    $sql .= "IF(tempRecCount.".$temp_field_name." IS NULL, '0<img style=\"display:none\" onload=\"this.parentNode.title=\'".$ELETitle."\'\" src=\"/images/2009a/icon_tick_green.gif\" />', CONCAT(tempRecCount.".$temp_field_name.",'<img style=\"display:none\" onload=\"this.parentNode.title=\'".$ELETitle."\'\" src=\"/images/2009a/icon_tick_green.gif\" />')),";
	}
}

//since 1) For "View only" and "Admin" , 2) "normal teacher" login will have different layout handle
if(strstr($ck_function_rights, "Profile:OLR") || $isReadOnly){
	$sql_displayField1 = " recCount ";
	$sql_extraInfo1 = "";

	$sql_displayField2 = " totalPendingCount ";
	$sql_extraInfo2 = "";
}else{
	$sql_displayField1 = " editRecCount ";
	$sql_extraInfo1 = ",'&nbsp;(',IF(tempRecCount.recCount IS NULL,0,tempRecCount.recCount),')'";

	$sql_displayField2 = " pendingCount ";
	$sql_extraInfo2 = ",'&nbsp;(',IF(tempRecCount.totalPendingCount IS NULL,0,tempRecCount.totalPendingCount),')'";
}
$sql .= "
			concat(
				IF(tempRecCount.{$sql_displayField1} IS NULL, 
					CONCAT('<a class=\"tablelink\" href=\"ole_student.php?Year={$academicYearID}&IntExt={$IntExt}&StudentID=', iu.UserID, '\">0</a>'), 
					CONCAT('<a class=\"tablelink\" href=\"ole_student.php?Year={$academicYearID}&IntExt={$IntExt}&StudentID=', iu.UserID, '\">', tempRecCount.{$sql_displayField1}, '</a>')
				){$sql_extraInfo1}
			),
";
if($isReadOnly){
	$sql .= " CONCAT('&nbsp;'), ";
}
else{
	$sql .= " CONCAT(
				IF(
					tempRecCount.{$sql_displayField2} IS NULL, 
					CONCAT('<a class=\"tablelink\" href=\"ole_student.php?Year={$academicYearID}&IntExt={$IntExt}&StudentID=', iu.UserID, '&status=1\">0</a>'), 
					CONCAT('<a class=\"tablelink\" href=\"ole_student.php?Year={$academicYearID}&IntExt={$IntExt}&StudentID=', iu.UserID, '&status=1\">', tempRecCount.{$sql_displayField2}, '</a>')
				){$sql_extraInfo2}
			),
";
}

$testSQL = " and (iu.UserLogin like '%eric%' or iu.UserLogin like '%henry%')";

$sql .= "
						CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', LPAD(iu.ClassNumber, 2, '0')) AS ClassNumber,
            IF(tempRecCount.recCount IS NULL, 0, tempRecCount.recCount) AS recCount,
            IF(tempRecCount.totalPendingCount IS NULL, 0, tempRecCount.totalPendingCount) AS totalPendingCount
          FROM
          	tempStudentList tsl
          INNER JOIN {$intranet_db}.INTRANET_USER AS iu
          	ON iu.UserID = tsl.StudentID 
		  INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
          	ON iu.UserID = ps.UserID 
          INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
          	ON iu.UserID = ycu.UserID
          INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
						ON ycu.YearClassID = yc.YearClassID
					LEFT JOIN tempRecCount
						ON iu.UserID = tempRecCount.UserID
          WHERE
            yc.AcademicYearID = {$currentAcademicYear}
			And ps.IsSuspend = 0
            $cond
			$conds_Keyword
			/* $testSQL */
        ";
//error_log($sql."\n", 3, "/tmp/aaa.txt");
//debug_r($sql);
// TABLE INFO
$LibTable->field_array = array("DisplayName", "CONCAT(iu.ClassName, LPAD(iu.ClassNumber, 2 ,'0'))", "recCount", "totalPendingCount");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = STANDARD_NO_OF_COLUMN + $ELECount;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' $rowStyle class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' $rowStyle width='100' >".$LibTable->column(0,$i_general_name, 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle class=\"tabletopnolink\">".$LibTable->column(1,$i_general_class."-".$i_ClassNumber, 1)."</td>";
//$LibTable->column_list .= "<td $rowStyle class=\"tabletopnolink\">".$LibTable->column(2,$ec_iPortfolio['category'], 1)."</td>";
$LibTable->column_list .= ($IntExt != 1) ? "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n" : "";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(2,$ec_iPortfolio['no_of_records'], 1)."</td>\n";

if($isReadOnly){
	//change the waiting of approval column to empty
	$h_waiting_apprroval_col = "";
}else
{
	$h_waiting_apprroval_col = "<td $rowStyle >".$LibTable->column(3,$ec_iPortfolio['waiting_approval'], 1)."</td>\n";
}
$LibTable->column_list .= $h_waiting_apprroval_col;


$LibTable->column_list .= "</tr>\n";

if ($IntExt != 1)
{
	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
		$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span id='oletitle_".$ELECode."' title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
	}
	$LibTable->column_list .= "</tr>";
  
  $LibTable->column_array = array_merge(array(0,0), array_fill(0, $ELECount, 3), array(3,3));				
}
else
{
  $LibTable->column_array = array(0,0,3,0,3,3,3);
}

$table_content = $LibTable->displayPlain();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigation(1)."</td></tr>" : "";
$table_content .= "</table>";

########################################################
# Table content : End
########################################################

########################################################
# Layout Display
########################################################
// template for teacher page
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";

### Title ###
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
include_once("template/ole_student_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
