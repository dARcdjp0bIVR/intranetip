<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();


$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");

$LibFS = new libfilesystem();

//$li_pf->accessControl("ole");

$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);
# remove the attachment folders
for ($i=0; $i<sizeof($RecordArr); $i++)
{
	$rid = $RecordArr[$i];
	$folder_attachment = $eclass_filepath."/files/portfolio/ole/r".$rid;
	$LibFS->folder_remove_recursive($folder_attachment);
}
$RecordList = implode($RecordArr, ",");

# RecordStatus = 1 (only pending record can be deleted)
//$sql = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ({$RecordList}) AND RecordStatus = '1'";
# Any records can be deleted
$sql = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ({$RecordList})";
$LibPortfolio->db_db_query($sql);

intranet_closedb();

if ($FromPage == "program")
	header("Location: ole_event_studentview.php?msg=3&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");
else	
	header("Location: ole_event_studentview.php?msg=3&EventID=$EventID&Year=$Year&FromPage=$FromPage&IntExt=$IntExt");


?>
