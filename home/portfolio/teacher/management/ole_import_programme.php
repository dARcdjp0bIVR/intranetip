<?php

// Modifing by Kenneth
##################################### Change Log [Start] #####################################################
#
#   2016-05-27 Henry HM
# 	Added field of "Default Hours"
#
#   2016-03-15 Kenneth
# 	Created
#
###################################### Change Log [End] ######################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");


intranet_auth();
intranet_opendb();


// Initializing classes
$LibUser = new libuser($UserID);
$libpf_ole = new libpf_ole();
$LibPortfolio = new libpf_slp();
$libCategory = new Category();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);



// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";

########################################################
# Tab Menu : Start
########################################################

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

############################################################################
# template for activity title
$LibWord = new libwordtemplates_ipf(1);
$file_array = $LibWord->file_array;
# template

$linterface->LAYOUT_START();

if($IntExt==1){
	$PAGE_NAVIGATION[] = array($iPort['external_record'],"ole.php?IntExt=1");
}else{
	$PAGE_NAVIGATION[] = array($iPort['menu']['ole'],"ole.php?IntExt=0");
}
$PAGE_NAVIGATION[] = array($button_import, "");

// get the reference html
$ELEArray = $libpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]), 'RecordID');
$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false, $orderByField='RecordID');



$Import_Guide = $iPort["ole_import_guide_int"];
if($IntExt==0){
	if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
		$SampleFile = "student_olr_sample3_sas.csv";
	}else{
		$SampleFile = "student_olr_sample3.csv";
	}
}else if($IntExt==1){
	if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
		$SampleFile = "student_olr_sample4_sas.csv";
	}else{
		$SampleFile = "student_olr_sample4.csv";
	}
}
$SubCatInfoArr = $libCategory-> GET_SUBCATEGORY_LIST(false,'CatID');

$ReferenceHTML = "<table width='80%' border='0' cellspacing='0' cellpadding='3'>";
$ReferenceHTML .= "<tr><td colspan='3'>".$Import_Guide."</td></tr>";
$ReferenceHTML .= "<tr><td valign='top'>".$Category_HTML."</td><td width='70'>&nbsp;</td><td valign='top'>".$ELE_HTML."</td></tr>";
$ReferenceHTML .= "</table>";

### data column
$DataColumnTitleArr = array();
//$DataColumnTitleArr[] = $ec_iPortfolio['ClassName'];
//$DataColumnTitleArr[] = $ec_iPortfolio['number'];
//$DataColumnTitleArr[] = $Lang['General']['UserLogin'];
//$DataColumnTitleArr[] = $ec_iPortfolio['WebSAMSRegNo'];
$DataColumnTitleArr[] = $Lang['General']['StartDate'];#
$DataColumnTitleArr[] = $Lang['General']['EndDate'];#
// $DataColumnTitleArr[] = $ec_iPortfolio['activity_name'];#
$DataColumnTitleArr[] = $ec_iPortfolio['title'];
$DataColumnTitleArr[] = $Lang['General']['SchoolYear'];//empty then get current
$DataColumnTitleArr[] = $Lang['General']['Term'];//empty then get current
$DataColumnTitleArr[] = $ec_iPortfolio['export']['Details'];
$DataColumnTitleArr[] = $ec_iPortfolio['export']['Category'];#

$DataColumnTitleArr[] = $ec_iPortfolio['sub_category']	;

//$DataColumnTitleArr[] = $Lang['iPortfolio']['VolunteerArr']['Role'];
//if($IntExt!=1)
//$DataColumnTitleArr[] = $ec_iPortfolio['export']['Hours'];
//$DataColumnTitleArr[] = $Lang['iPortfolio']['OEA']['Achievement'];
// $DataColumnTitleArr[] = $ec_iPortfolio['remark'];
$DataColumnTitleArr[] = $ec_iPortfolio['ProgramRemarks'];
$DataColumnTitleArr[] = $iPort['external_ole_report']['organization'];
if($IntExt!=1)
// $DataColumnTitleArr[] = $ec_iPortfolio['export']['ELE'];
	$DataColumnTitleArr[] = $ec_iPortfolio['ComponentCode'];

if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
	$DataColumnTitleArr[] = $Lang['iPortfolio']['OLE']['In_Outside_School'];
	$DataColumnTitleArr[] = $Lang['iPortfolio']['OLE']['SAS'];
}
if($IntExt!=1){
// 	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['AllowStudentsToJoin'];
	$DataColumnTitleArr[] = $ec_iPortfolio['AllowStudentsToJoin'] ;
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['JoinPeriod']. ' '. $Lang['General']['StartDate'];;
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['JoinPeriod']. ' '. $Lang['General']['EndDate'];;
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['JoinableYear'];
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['AutoApprove'];
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['MaximumHours'];
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['DefaultHours'];
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['CompulsoryFields'];
	$DataColumnTitleArr[] = $ec_iPortfolio['SLP']['DefaultApprover'];
}
//$DataColumnPropertyArr = array(2,2,2,2,1,1,1,0,0,0,1);
if($IntExt==0){
	if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
		$DataColumnPropertyArr = array(1,0,1,0,0,0,1,0,0,0,1,1,1,1,4,4,2,2,0,0,0,0);
	}else{
		$DataColumnPropertyArr = array(1,0,1,0,0,0,1,0,0,0,1,1,4,4,2,2,0,0,0,0);
	}
}else{
	if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){
		$DataColumnPropertyArr = array(1,0,1,0,0,0,1,0,0,0,1,1);
	}else{
		$DataColumnPropertyArr = array(1,0,1,0,0,0,1,0,0,0);
	}
}
$DataColumnRemarksArr = array();
//$DataColumnRemarksArr[3] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][3].')</span>';
$DataColumnRemarksArr[0] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
$DataColumnRemarksArr[1] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
$DataColumnRemarksArr[3] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][1].')</span>';
$DataColumnRemarksArr[4] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][2].')</span>';
$DataColumnRemarksArr[6] = '<a id="remarkBtn_category" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'category\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][5].']</a>';
if($IntExt!=1)
$DataColumnRemarksArr[10] = '<a id="remarkBtn_component" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'component\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][6].']</a>&nbsp;'.'<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][4].')</span>';
$DataColumnRemarksArr[7] = '<a id="remarkBtn_subcategory" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'subcategory\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][8].']</a>';
if($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']){

		$DataColumnRemarksArr[11] = '<a id="remarkBtn_inside_outside" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'inside_outside\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][9].']</a>';
		$DataColumnRemarksArr[12] = '<a id="remarkBtn_isSAS" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'isSAS\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][10].']</a>';
		//$DataColumnRemarksArr[16] = '<a id="remarkBtn_allowJoin" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allowJoin\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11].']</a>';
		$DataColumnRemarksArr[13] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][18].')</span>';
		$DataColumnRemarksArr[14] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
		$DataColumnRemarksArr[15] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
		$DataColumnRemarksArr[16] = '<a id="remarkBtn_allForm" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allForm\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][12].']</a>&nbsp;'.'<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][13].')</span>';
		//$DataColumnRemarksArr[20] = '<a id="remarkBtn_allowAutoApproval" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allowAutoApproval\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11].']</a>';
		$DataColumnRemarksArr[17] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][20].')</span>';
		$DataColumnRemarksArr[21] = '<a id="remarkBtn_compulsoryField" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'compulsoryField\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][15].']</a>';
		$DataColumnRemarksArr[22] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][16].')</span>';
}else{

		//$DataColumnRemarksArr[14] = '<a id="remarkBtn_allowJoin" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allowJoin\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11].']</a>';
		$DataColumnRemarksArr[11] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][18].')</span>';
		$DataColumnRemarksArr[12] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
		$DataColumnRemarksArr[13] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
		$DataColumnRemarksArr[14] = '<a id="remarkBtn_allForm" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allForm\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][12].']</a>&nbsp;'.'<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][13].')</span>';
		//$DataColumnRemarksArr[15] = '<a id="remarkBtn_allowAutoApproval" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'allowAutoApproval\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][11].']</a>';
		$DataColumnRemarksArr[15] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][20].')</span>';
		$DataColumnRemarksArr[18] = '<a id="remarkBtn_compulsoryField" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'compulsoryField\')">['.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][15].']</a>' . '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][21].')</span>';
		$DataColumnRemarksArr[19] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][16].')</span>';

}
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);




##Remarks Layer
$RemarksLayer .= '';
$thisRemarksType = 'category';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['Category'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$CategoryInfoArr as $CategoryInfo){
											$cat_id = $CategoryInfo['RecordID'];
											$cat_name = Get_Lang_Selection($CategoryInfo['ChiTitle'],$CategoryInfo['EngTitle']);
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$cat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'component';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['ELE'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$ELEArray as $ELE){
											$cat_id = $ELE['RecordID'];
											$cat_name = Get_Lang_Selection($ELE['ChiTitle'],$ELE['EngTitle']);
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$cat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
			
			
$thisRemarksType = 'subcategory';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['Category'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['sub_category']	.'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$SubCatInfoArr as $SubCategoryInfo){
											$subcat_id = $SubCategoryInfo['SubCatID'];
											$subcat_name = Get_Lang_Selection($SubCategoryInfo['ChiTitle'],$SubCategoryInfo['EngTitle']);
											$cat_name = Get_Lang_Selection($SubCategoryInfo['CategoryChiTitle'],$SubCategoryInfo['CategoryEngTitle']);
											$cat_id = $SubCategoryInfo['CatID'];
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$subcat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.' ('.$cat_id.')'.'</td>'."\n";
												$RemarksLayer .= '<td>'.$subcat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";


$thisRemarksType = 'inside_outside';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$Lang['iPortfolio']['OLE']['In_Outside_School'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>I</td>'."\n";
												$RemarksLayer .= '<td>'.$Lang['iPortfolio']['OLE']['Inside_School'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>O</td>'."\n";
												$RemarksLayer .= '<td>'.$Lang['iPortfolio']['OLE']['Outside_School'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'isSAS';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$Lang['iPortfolio']['OLE']['SAS'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'allowJoin';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['AllowStudentsToJoin'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";

###  GET all Form from this ay
$fcm = new form_class_manage();
$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_arr = array();
for($i=0; $i<count($yearClassArr); $i++)
{
  $year_arr[] = array($yearClassArr[$i]["YearID"], $yearClassArr[$i]["YearName"]);
}
$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr))));

$thisRemarksType = 'allForm';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												//$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['JoinableYear'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach($year_arr as $yearInfo){
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												//$RemarksLayer .= '<td>'.$yearInfo['0'].'</td>'."\n";
												$RemarksLayer .= '<td>'.$yearInfo['1'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";	
										}	
														
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'allowAutoApproval';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['AutoApprove'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";


$thisRemarksType = 'compulsoryField';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['CompulsoryFields'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>1</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['hours'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>2</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['ole_role'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>3</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['achievement'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>4</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['attachment'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>5</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['approved_by'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);


?>

<script language="JavaScript">
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
isFiltered = false;
function onclickFilter(obj){
		var CategoryName = obj.innerText;
		filterByCategoryName(CategoryName);

}
function filterByCategoryName(CategoryName){
	if(isFiltered){
		$('.custFilter').show();
		isFiltered = false;
	}else{
		$('.custFilter').hide();
		$('.custFilterTarget').filter(function(){
			return $(this).text()==CategoryName;
		}).parent(".custFilter").show();
		isFiltered = true;
	}
}
function onmouseover(obj){
	alert();
	obj.style.backgroundColor = "red";
}
</script>

<FORM name="form1" method="post" action="ole_import_programme_step1.php" enctype="multipart/form-data">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
<td>
<?=$htmlAry['steps']?>
</td></tr>
<tr>
	<td colspan="2">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
   		<td>
       	<br />
		<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
			<td class="tabletext"><a class="contenttool" href="download.php?FileName=<?=$SampleFile?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?> </td>
			<td class="tabletext"><?=$DataColumn?></td>
		</tr>
		</table>
		</td>
	</tr>      
	<tr>
		<td colspan="2">        
		<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<tr>
        	<td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
		</tr>
		<?php if($IntExt==0){ ?>	
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">^</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][17]?></td>
		</tr>	
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">#</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][19]?></td>
		</tr>
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">@</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][22]?></td>
		</tr>
		<?php } ?>	
		<tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
			</td>
		</tr>
        </table>                                
		</td>
	</tr> 
	</table>
	</td>
</tr>
</table>


<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" name="attachment_size" value="0" >
<input type="hidden" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" name="FromPage" value="<?=$FromPage?>" >
<input type="hidden" name="IntExt" value="<?=$IntExt?>" >

</form>
<?=$RemarksLayer?>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

