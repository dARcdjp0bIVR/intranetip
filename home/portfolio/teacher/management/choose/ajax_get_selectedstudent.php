<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT iu.UserID, CONCAT(".getNameFieldByLang2("iu.").", ' (', ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN").", ' - ', ycu.ClassNumber, ')') AS DisplayName ";
$sql .= "FROM {$intranet_db}.YEAR y ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu ON ycu.YearClassID = yc.YearClassID ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON ycu.UserID = iu.UserID ";
switch($add_type)
{
	case "year":
		$sql .= "WHERE y.YearID IN (".implode(", ", $YearID).") ";
		break;
	case "yearclass":
		$sql .= "WHERE ycu.YearClassID IN (".implode(", ", $YearClassID).") ";
		break;
	case "student":
		$sql .= "WHERE iu.UserID IN (".implode(", ", $StudentID).") ";
		break;
}
$sql .= "ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
$student_arr = $li->returnArray($sql);

for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{	
	if($student_arr[$i]["DisplayName"] != null || $student_arr[$i]["DisplayName"] != ''){
		$_uID = $student_arr[$i]["UserID"];
		$_uName = $student_arr[$i]["DisplayName"];
//		if(strpos($_uName,'&') !== false){
		// Prevent error occurred when '&' exist in string
		$_uName = str_replace('&','&amp;',$_uName);
//		}	
	
		$html_selected_student .= "<option value=\"{$_uID}\">{$_uName}</option>";
	}
}

header("Content-Type:   text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
echo "<options>";
echo $html_selected_student;
echo "</options>";

intranet_closedb();
?>