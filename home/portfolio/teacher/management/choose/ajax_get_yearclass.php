<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." AS ClassTitle ";
$sql .= "FROM {$intranet_db}.YEAR y ";
$sql .= "INNER JOIN {$intranet_db}.YEAR_CLASS yc ON y.YearID = yc.YearID ";
$sql .= "WHERE yc.YearID IN (".implode(", ", $YearID).") ";
$sql .= "AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
$sql .= "ORDER BY y.Sequence, yc.Sequence";
$sql .= 
$yc_arr = $li->returnArray($sql);

$html_yearclass_selection = "<select name=\"YearClassID[]\" multiple=\"multiple\" size=\"7\" style=\"width:100%\">";
for($i=0, $i_max=count($yc_arr); $i<$i_max; $i++)
{
	$_ycID = $yc_arr[$i]["YearClassID"];
	$_ycName = $yc_arr[$i]["ClassTitle"];

	$html_yearclass_selection .= "<option value=\"{$_ycID}\">{$_ycName}</option>";
}
$html_yearclass_selection .= "</select>";

echo $html_yearclass_selection;

intranet_closedb();
?>