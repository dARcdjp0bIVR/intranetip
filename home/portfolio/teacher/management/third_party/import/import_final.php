<?php
# using: 

/*	Remark:	!important to read for ADDING NEW FIELDS in csv
			Bookmark: note_1
			-	the array position should be maintain carefully whenever added fields in the csv,
				the array number will be shifted if adding csv fields before the SchoolYear field
			-	it will trigger an error whenever the SchoolYear is not set to trigger auto detect SchoolYear							
**/
/** [Modification Log] Modifying By: 
 * *******************************************
 * 	Modification Log
 * 2017-02-22 Villa
 * - P113388  - Fix cannot import with apostrophe 
 * 
 * 2017-01-13 Omas
 * - improve logic to follow old logic in ole_import_update.php #F111438
 * - Program with same name, date, category will be count as same program
 * 
 * 2016-06-28 Omas
 * - modified getProgramID - fix Import Int become Ext record problem #L98030
 * 
 * 2016-05-27 Henry HM
 * - Added field of "Default Hours"
 * 
 * 2016-03-15 Kenneth
 * - add new params
 * - fix layout issue
 * 
 * 2016-02-12 Kenneth
 * - Created
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("T");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");

intranet_opendb();
$lpf = new libpf_slp();
$lpf->CHECK_ACCESS_IPORTFOLIO();

$CurrentPage = "Teacher_ThirdParty";
$CurrentPageName = $iPort['menu']['third_party'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface = new interface_html();

$li = new libdb();
//$lo = new libfilesystem();
$limport = new libimporttext();
$libCategory = new Category();
$lpf_tp = new libpf_third_party();

$loginUserId = $_SESSION['UserID'];

### Retireve and handle data
$success = 0;
$import_data = isset($import_data)?$import_data:array();
if(!empty($import_data)){
	foreach($import_data as $data){
		$isSuccess = true;
		$d = unserialize(stripslashes($data));
		$sql = "SELECT RecordID FROM {$eclass_db}.THIRD_PARTY_REPORT WHERE Title='".$d['Title']."' AND StartDate='".$d['StartDate']."'";
		$RecordID = current($li->returnVector($sql));
		if(empty($RecordID)){
			$sql = "INSERT INTO {$eclass_db}.THIRD_PARTY_REPORT 
					(Title, Description, StartDate, EndDate, InputDate, Createdby) 
					VALUES 
					('".$d['Title']."','".$d['Description']."','".$d['StartDate']."','".$d['EndDate']."',NOW(),'".$loginUserId."')";
			$result = $li->db_db_query($sql);
			if($result==false){
				$isSuccess = false;
			}
			$RecordID = $li->db_insert_id();
		}
		$sql = "INSERT INTO {$eclass_db}.THIRD_PARTY_REPORT_RECORD
				(ReportID, UserID, Result, Remark, InputDate, Createdby)
				VALUES
				('".$RecordID."','".$d['UserID']."','".$d['Result']."','".$d['Remarks']."',NOW(),'".$loginUserId."')";
		$result = $li->db_db_query($sql);
		if($result==false){
			$isSuccess = false;
		}
		
		if($isSuccess){
			$success++;
		}
	}
}

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "self.location='../index.php'");

$linterface->LAYOUT_START();
?>

<form name="form1" id="form1" method="POST">

	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$success?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>

</form>	
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>