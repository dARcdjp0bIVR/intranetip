<?php

// Modifing by Kenneth
##################################### Change Log [Start] #####################################################
#
#   2016-05-27 Henry HM
# 	Added field of "Default Hours"
#
#   2016-03-15 Kenneth
# 	Created
#
###################################### Change Log [End] ######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libpf-ole.php");
include_once("$intranet_root/includes/portfolio25/oleCategory/libpf-category.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
iportfolio_auth("T");
intranet_opendb();

// Initializing classes

$lpf = new libportfolio();
$LibUser = new libuser($UserID);
$libpf_ole = new libpf_ole();
$lpf_tp = new libpf_third_party();
$LibPortfolio = new libpf_slp();
$libCategory = new Category();
//$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);



// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_ThirdParty";
$CurrentPageName = $iPort['menu']['third_party'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['third_party'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");



############################################################################
# template for activity title
$LibWord = new libwordtemplates_ipf(1);
$file_array = $LibWord->file_array;
# template



$PAGE_NAVIGATION[] = array($CurrentPageName, '../index.php');
$PAGE_NAVIGATION[] = array($button_import, "");

// get the reference html
$ELEArray = $libpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]), 'RecordID');
$CategoryInfoArr = $libCategory->GET_CATEGORY_LIST($ParShowInactive=false, $orderByField='RecordID');



$Import_Guide = $iPort["ole_import_guide_int"];
$SampleFile = "third_party/sample.csv";
$SubCatInfoArr = $libCategory-> GET_SUBCATEGORY_LIST(false,'CatID');

$ReferenceHTML = "<table width='80%' border='0' cellspacing='0' cellpadding='3'>";
$ReferenceHTML .= "<tr><td colspan='3'>".$Import_Guide."</td></tr>";
$ReferenceHTML .= "<tr><td valign='top'>".$Category_HTML."</td><td width='70'>&nbsp;</td><td valign='top'>".$ELE_HTML."</td></tr>";
$ReferenceHTML .= "</table>";

### data column
$DataColumnTitleArr = array();

$DataColumnTitleArr[] = $ec_iPortfolio['activity_name'];
$DataColumnTitleArr[] = $Lang['iPortfolio']['OtherReports']['Description'];
$DataColumnTitleArr[] = $Lang['General']['StartDate'];
$DataColumnTitleArr[] = $Lang['General']['EndDate'];
$DataColumnTitleArr[] = $Lang['General']['UserLogin'];
$DataColumnTitleArr[] = $Lang['iPortfolio']['ThirdParty']['Result'];
$DataColumnTitleArr[] = $ec_iPortfolio['remark'];


//$DataColumnPropertyArr = array(2,2,2,2,1,1,1,0,0,0,1);
$DataColumnPropertyArr = array(1,0,0,4,1,1,0);

$DataColumnRemarksArr = array();
//$DataColumnRemarksArr[3] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][3].')</span>';
$DataColumnRemarksArr[2] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
$DataColumnRemarksArr[3] = '<span class="tabletextremark">('.$Lang['iPortfolio']['IMPORT']['OLE']['Description'][0].')</span>';
$DataColumnRemarksArr[5] = '<span class="tabletextremark">('.$Lang['iPortfolio']['ThirdParty']['ResultRemark'].')</span>';

$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);


/*

##Remarks Layer
$RemarksLayer .= '';
$thisRemarksType = 'category';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['Category'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$CategoryInfoArr as $CategoryInfo){
											$cat_id = $CategoryInfo['RecordID'];
											$cat_name = Get_Lang_Selection($CategoryInfo['ChiTitle'],$CategoryInfo['EngTitle']);
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$cat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'component';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['ELE'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$ELEArray as $ELE){
											$cat_id = $ELE['RecordID'];
											$cat_name = Get_Lang_Selection($ELE['ChiTitle'],$ELE['EngTitle']);
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$cat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
			
			
$thisRemarksType = 'subcategory';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['export']['Category'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['sub_category']	.'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ((array)$SubCatInfoArr as $SubCategoryInfo){
											$subcat_id = $SubCategoryInfo['SubCatID'];
											$subcat_name = Get_Lang_Selection($SubCategoryInfo['ChiTitle'],$SubCategoryInfo['EngTitle']);
											$cat_name = Get_Lang_Selection($SubCategoryInfo['CategoryChiTitle'],$SubCategoryInfo['CategoryEngTitle']);
											$cat_id = $SubCategoryInfo['CatID'];
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$subcat_id.'</td>'."\n";
												$RemarksLayer .= '<td>'.$cat_name.' ('.$cat_id.')'.'</td>'."\n";
												$RemarksLayer .= '<td>'.$subcat_name.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";


$thisRemarksType = 'inside_outside';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$Lang['iPortfolio']['OLE']['In_Outside_School'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>I</td>'."\n";
												$RemarksLayer .= '<td>'.$Lang['iPortfolio']['OLE']['Inside_School'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>O</td>'."\n";
												$RemarksLayer .= '<td>'.$Lang['iPortfolio']['OLE']['Outside_School'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'isSAS';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$Lang['iPortfolio']['OLE']['SAS'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'allowJoin';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['AllowStudentsToJoin'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";

###  GET all Form from this ay
$fcm = new form_class_manage();
$yearClassArr = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
$year_arr = array();
for($i=0; $i<count($yearClassArr); $i++)
{
  $year_arr[] = array($yearClassArr[$i]["YearID"], $yearClassArr[$i]["YearName"]);
}
$year_arr = array_values(array_map("unserialize", array_unique(array_map("serialize", $year_arr))));

$thisRemarksType = 'allForm';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												//$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['JoinableYear'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach($year_arr as $yearInfo){
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												//$RemarksLayer .= '<td>'.$yearInfo['0'].'</td>'."\n";
												$RemarksLayer .= '<td>'.$yearInfo['1'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";	
										}	
														
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
$thisRemarksType = 'allowAutoApproval';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['AutoApprove'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>Y</td>'."\n";
												$RemarksLayer .= '<td>Yes</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>N</td>'."\n";
												$RemarksLayer .= '<td>No</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";


$thisRemarksType = 'compulsoryField';
			$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['General']['Code'].'</th>'."\n";
												$RemarksLayer .= '<th>'.$ec_iPortfolio['SLP']['CompulsoryFields'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>1</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['hours'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";		
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>2</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['ole_role'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>3</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['achievement'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>4</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['attachment'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
											$RemarksLayer .= '<tr class="custFilter">'."\n";
												$RemarksLayer .= '<td>5</td>'."\n";
												$RemarksLayer .= '<td>'.$ec_iPortfolio['approved_by'].'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
															
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
*/				
### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);
$linterface->LAYOUT_START();
include_once("../templates/import.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
intranet_closedb();
?>