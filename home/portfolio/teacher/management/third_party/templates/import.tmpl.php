<script language="JavaScript">
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
isFiltered = false;
function onclickFilter(obj){
		var CategoryName = obj.innerText;
		filterByCategoryName(CategoryName);

}
function filterByCategoryName(CategoryName){
	if(isFiltered){
		$('.custFilter').show();
		isFiltered = false;
	}else{
		$('.custFilter').hide();
		$('.custFilterTarget').filter(function(){
			return $(this).text()==CategoryName;
		}).parent(".custFilter").show();
		isFiltered = true;
	}
}
function onmouseover(obj){
	alert();
	obj.style.backgroundColor = "red";
}
</script>

<FORM name="form1" method="post" action="import_validate.php" enctype="multipart/form-data">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
<td>
<?=$htmlAry['steps']?>
</td></tr>
<tr>
	<td colspan="2">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
   		<td>
       	<br />
		<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
			<td class="tabletext"><a class="contenttool" href="../../download.php?FileName=<?=$SampleFile?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$import_csv['download']?></a></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['ImportArr']['DataColumn']?> </td>
			<td class="tabletext"><?=$DataColumn?></td>
		</tr>
		</table>
		</td>
	</tr>      
	<tr>
		<td colspan="2">        
		<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<tr>
        	<td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
		</tr>
		<!-- 
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">^</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][17]?></td>
		</tr>
		 -->	
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">#</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][19]?></td>
		</tr>
		<!-- 
		<tr>
			<td align="left" class="tabletextremark"><span class="tabletextrequire">@</span><?=$Lang['iPortfolio']['IMPORT']['OLE']['Description'][22]?></td>
		</tr>
		 -->
		<tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
			</td>
		</tr>
        </table>                                
		</td>
	</tr> 
	</table>
	</td>
</tr>
</table>
<!--

<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
<input type="hidden" name="StudentID" value="<?=$StudentID?>" >
<input type="hidden" name="attachment_size" value="0" >
<input type="hidden" name="RecordID" value="<?=$record_id?>" >
<input type="hidden" name="attachment_size_current" value="<?=$attach_count?>" >
<input type="hidden" name="ApprovalSetting" value="<?=$ApprovalSetting?>" >
<input type="hidden" name="SelectedStatus" value="<?=$SelectedStatus?>" >
<input type="hidden" name="SelectedELE" value="<?=$SelectedELE?>" >
<input type="hidden" name="SelectedYear" value="<?=$SelectedYear?>" >
<input type="hidden" name="FromPage" value="<?=$FromPage?>" >
<input type="hidden" name="IntExt" value="<?=$IntExt?>" >
  -->
</form>
<!--<?=$RemarksLayer?>-->