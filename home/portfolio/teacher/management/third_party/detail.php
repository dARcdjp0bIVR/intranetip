<?php

// Modified by Paul

/********************** Change Log ***********************
*  2018-12-03 (Pun) [ip.2.5.10.1.1]
*  	- Modified getStudentSubmisstionInfo(), added NCS cust
/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-thirdparty.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
iportfolio_auth("T");
intranet_opendb();

// Search Parameters
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$classId = isset($_POST["YearClassID"])?$_POST["YearClassID"]:'all';
$status = isset($_POST["status"])?$_POST["status"]:'all';
$assessment_id = isset($_GET["assessment_id"])?$_GET["assessment_id"]:'';

if($assessment_id==''){
	header('Location: index.php');
}

$lpf = new libportfolio();
$lpf_tp = new libpf_third_party();
$libportfolio_ui = new libportfolio_ui();
$libpf_slp = new libpf_slp();

// Table setup
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;	//	the constructor does not provide setting page size, which is the number of records display per page

//~ Table headers			--------------------------||
$columnCount=0;
$pfTable->column_list .= "<th width='1'>#</th>\n";
$pfTable->column_list .= "<th nowrap style='width: 200px;'>".$pfTable->column($columnCount++, $Lang['Identity']['Student'])."</th>\n";	//	column($fieldIndex,$columnTitle)
if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    $pfTable->column_list .= "<th style='width: 200px;'>".$pfTable->column($columnCount++, $Lang['AccountMgmt']['LoginID'])."</th>\n";
}else{
    $pfTable->column_list .= "<th style='width: 200px;'>".$pfTable->column($columnCount++, $Lang['SysMgr']['FormClassMapping']['StudentClassNumber'])."</th>\n";
}
$pfTable->column_list .= "<th style='width: 200px;'>".$pfTable->column($columnCount++, $Lang['iPortfolio']['ThirdParty']['Result'])."</th>\n";
$pfTable->column_list .= "<th>".$pfTable->column($columnCount++, $ec_iPortfolio['remark'])."</th>\n";

$pfTable->sql = $lpf_tp->getStudentSubmisstionInfo($assessment_id,$keyword,$status);

//~ Control Display			--------------------------||
$pfTable->IsColOff = "IP25_table";	//	choosing layout
$pfTable->field_array = array("UserName","Class","Result", "Remark");	//	set the sql field being display
$pfTable->no_col = count($pfTable->field_array)+1/* checkbox */;	//	set the number of fields to be displayed, at least 2, becoz the 1st row is used to display the number of rows
$pfTable->column_array = array(0,0,0,0);	//	use to control the text style

//~ Get the HTML			--------------------------||
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################


###############################################
##	HTML - display_table (containing the hidden form fields for the table settings)
$html["display_table"] = $displayTableHtml;

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_ThirdParty";
$CurrentPageName = $iPort['menu']['third_party'];
### Title ###
$TAGS_OBJ[] = array($iPort['menu']['third_party'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
$PAGE_NAVIGATION[] = array($CurrentPageName, 'index.php');
$PAGE_NAVIGATION[] = array($lpf_tp->getAssessmentReportName($assessment_id),'');
$html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$html["title"] = $lpf_tp->getAssessmentReportName($assessment_id);

if($IS_MANAGE)	//	iportfolio admin
{
	//	$groupIds = $libpf_slp->getUserGroupsInTeacherComment();
} else {
	$groupIds = $libpf_slp->getUserGroupsInTeacherComment($UserID);
}
//debug_r($groupIds);
$userClassInfo = $libpf_slp->getClassInfoByGroupIds($groupIds);
//$class_arr = $libpf_slp->refineUserClassInfo($userClassInfo);
$class_arr = $lpf_tp->getClassInfo();
$html["status_selection"] = '
							<select id="status" name="status" class="auto_submit" onchange="document.form1.submit();">';
$html["status_selection"] .= '<option value="all" '.(($status=="all")?"selected":"").'>'.$Lang['Btn']['All'] .'</option>';
foreach($class_arr as $c){
	$cname = ($intranet_session_language=='en')?$c['class_name_en']:$c['class_name_b5'];
	$html["status_selection"] .= '<option value="'.$c['class_id'].'" '.(($status==$c['class_id'])?"selected":"").'>'.$cname.'</option>';
}
$html["status_selection"] .='</select>
							';

$html["keyword"] = $keyword;

$linterface->LAYOUT_START();
include_once("templates/detail.tmpl.php");	//	the content besides the new page layout
$linterface->LAYOUT_STOP();
intranet_closedb();
?>