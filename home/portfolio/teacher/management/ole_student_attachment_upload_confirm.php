<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$action = $_SESSION["SESS_OLE_ACTION"];
$isReadOnly = ($action == $ipf_cfg["OLE_ACTION"]["view"]) ? 1 : 0 ;

$ldb = new libdb();
$lpf = new libpf_slp();
$luser = new libuser();
$lfs = new libfilesystem();
$laccount = new libaccountmgmt();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

########################################################
# HIDE COL FOR SIS 20140319 TEMPLATE SETTINGS 
########################################################

// set current tab name for sys_custom 
/* Please do not change the Value 
 * Unless you have understood how hide column works as well as INT/EXT means */

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = ($isReadOnly) ? "Teacher_OLEView" : "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

#step
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0], 0);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1], 1);
// $STEPS_OBJ[] = array($Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2], 0);
// $StepObj = $linterface->GET_STEPS($STEPS_OBJ);
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][0];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][1];
$stepAry[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['StepArr'][2];
$StepObj = $linterface->GET_IMPORT_STEPS($CurrStep=2, $stepAry);

$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

$TabMenuArr = libpf_tabmenu::getSlpMgmtTabs($TabIndex, 0);
### Title ###
$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($EventID);
if (is_array($data)) {
	$eng_title_html = $data['Title'];
}

#nagigation
$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['program_list'], "ole.php?IntExt=".$IntExt);
$MenuArr[] = array($eng_title_html, "ole_event_studentview.php?IntExt=".$IntExt."&EventID=".$EventID."&Year=".$Year."&FromPage=program");
$MenuArr[] = array($Lang['iPortfolio']['OLE']['UploadButton'], "");
$navigation_html = $linterface->GET_NAVIGATION($MenuArr);


# upload file handling start
$filename = $_FILES["UploadFile"]["name"];
$tmpfilepath = $_FILES["UploadFile"]["tmp_name"];
$fileext = $lfs->file_ext($filename);

$ole_portfolio_path= $eclass_root."/files/portfolio/ole";
//$base_img_path = "/file/portfolio";
$tmpfolder = $ole_portfolio_path."/tmp/attachment/t_".$_SESSION['UserID'];

if(file_exists($tmpfolder))
{
	//$lfs->folder_remove_recursive(file_exists($tmpfolder));
	$lfs->folder_remove_recursive($tmpfolder);
}
### generate tmp filename (prevent Chinease filename coding problem)
$tmpfilename = "TMP_".$_SESSION['UserID'].$fileext;
//$tmpfile = $tmpfolder."/$filename";
$tmpfile = $tmpfolder."/$tmpfilename";
# create a tmp file
$lfs->folder_new($tmpfolder);

if(strtolower($fileext)==".zip")
{
	$tmpfilelist = '';
//	clearstatcache();
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
//	if($lfs->file_unzip_ext($tmpfile,$tmpfolder))
 		if($lfs->file_unzip($tmpfile,$tmpfolder))
	{
		$lfs->lfs_remove($tmpfile);
		$lfs->set_subfolder_recursive(true);
		$tmpfilelist= $lfs->return_folderlist($tmpfolder,true);
	}
}
// debug_pr($tmpfilelist);

//get Student class/name
$sql = "SELECT  a.RecordID,
				b.UserLogin,
				b.WebSAMSRegNo,
  				CONCAT(b.ClassName,'_', LPAD(b.ClassNumber, 2, '0')) as ClassNumber
		FROM  {$eclass_db}.OLE_STUDENT as a
		INNER JOIN {$intranet_db}.INTRANET_USER AS b ON a.UserID = b.UserID
 		WHERE a.ProgramID = '$EventID'
";

$StudentInfo  = $lpf->returnArray($sql);
$StudentInfoByClassNumberAry = BuildMultiKeyAssoc($StudentInfo,'ClassNumber');
$StudentInfoByWebSAMSAry = BuildMultiKeyAssoc($StudentInfo,'WebSAMSRegNo');

for($j=0;$j<count($StudentInfo);$j++){
	$StudentClassNumberAry[] = $StudentInfo[$j]['ClassNumber'];
	$StudentUserLogin[]=$StudentInfo[$j]['UserLogin'];
	$WebSAMSRegNoAry[] = $StudentInfo[$j]['WebSAMSRegNo'];
}
//  debug_pr($tmpfilelist);
$userIdAry = array();

if(count($tmpfilelist)>0)
{
	$NoOfUploadSuccess = 0;
	$UserLoginAry = array();
	for($i=0; $i< count($tmpfilelist); $i++)
	{
		$WarnMsg = array();
		$thisUserInfo = '';
			$thisrawext = substr($tmpfilelist[$i], strrpos($tmpfilelist[$i],".")); // no strtolower

			$thisUserName = basename($tmpfilelist[$i],$thisrawext);	
			
			if(in_array($thisUserName,$StudentClassNumberAry)){
				$UserLogin = $StudentInfoByClassNumberAry[$thisUserName]['UserLogin'];
				$RecordID =$StudentInfoByClassNumberAry[$thisUserName]['RecordID'];
			}elseif(in_array($thisUserName,$WebSAMSRegNoAry)){
				$UserLogin = $StudentInfoByWebSAMSAry[$thisUserName]['UserLogin'];
				$RecordID =$StudentInfoByWebSAMSAry[$thisUserName]['RecordID'];
			}else{
				$WarnMsg[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['UnmappingName'];
				$UserLogin = '';
			}
			
			$thisUserInfo = $laccount->getUserInfoByUserLogin($UserLogin);
			if(is_dir($tmpfilelist[$i]) == 1){
				$ThisFolderList = $lfs->return_folderlist($tmpfilelist[$i],true);
				if(empty($ThisFolderList)){
					$WarnMsg[] =$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['EmptyFolder'];
				}
				for($j=0;$j<count($ThisFolderList);$j++){
					if(is_dir($ThisFolderList[$j]) == 1){
						$WarnMsg[] =$Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['FolderInside'] ;
					}
				}
			}	
			if(in_array($UserLogin,$UserLoginAry)){
				if($UserLogin != ''){
				$WarnMsg[] = $Lang['iPortfolio']['OLE']['UploadAttachment']['WarnMsg']['RepeatStudent'];
				}
			}			
			if(empty($WarnMsg)){
				$NoOfUploadSuccess++;
				$UploadSuccess = true;
			}else{
				$UploadSuccess = false;
			}
			
			$red_css = !$UploadSuccess?" red ":"";
			
			$rowcss = " class='".($key++%2==0? "tablebluerow2":"tablebluerow1")." $red_css' ";
			
			# gen table row
			$tr.= '<tr '.$rowcss.'>'."\n";
			$tr.= '<td>'.basename($tmpfilelist[$i]).'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$UserLogin]["ClassName"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$UserLogin]["ClassNumber"]:"-").'</td>'."\n";
			$tr.= '<td>'.($thisUserInfo?$thisUserInfo[$UserLogin]["Name"]:"-").'</td>'."\n";
			if($WarnMsg !== ''){
				$tr.= '<td'.$red_css.'>'.implode("<br>",$WarnMsg).'</td>'."\n";
			}else{
				$tr.= '<td>---</td>'."\n";
			}
			$tr.= '</tr>'."\n";
			
			$UserLoginAry[] = $UserLogin;
			$RecordIDAry[] = $RecordID;
			$SuccessfulAttachmentPathAry[] =$tmpfilelist[$i];
		
	}
	$display.= '<table cellpadding=5 cellspacing=0 border=0 width="100%">'."\n";
	$display.= '<tr class="tablebluetop tabletopnolink">'."\n";
	$display.= '<td>'.$Lang['AccountMgmt']['File'].'</td>'."\n";
	$display.= '<td>'.$Lang['AccountMgmt']['Class'].'</td>'."\n";
	$display.= '<td>'.$Lang['AccountMgmt']['ClassNo'].'</td>'."\n";
	$display.= '<td>'.$Lang['AccountMgmt']['StudentName'].'</td>'."\n";
// 	if(!$UploadSuccess){
		$display.= '<td>'.$Lang['iPortfolio']['OLE']['UploadAttachment']['Error'].'</td>'."\n";
	
	$display.= '</tr>'."\n";
	$display.= $tr;
	$display.= '</table>'."\n";
	
}

$UploadStat.= '<table cellpadding="5" cellspacing="0" border="0" width="100%">'."\n";
$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadNumber'] .'</td><td class="tabletext">'.count($tmpfilelist).'</td></tr>'."\n";
//$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['AccountMgmt']['UploadSuccess'].'</td><td class="tabletext">'.$NoOfUploadSuccess.'</td></tr>'."\n";
$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['iPortfolio']['OLE']['UploadAttachment']['UploadUncessfulNumber'].'</td><td class="tabletext">'.(count($tmpfilelist)-$NoOfUploadSuccess).'</td></tr>'."\n";
$UploadStat.= '</table>'."\n";


# gen button
$Disabled=0;
if(count($tmpfilelist) != $NoOfUploadSuccess){
	$Disabled=1;
}
//$uploadBtn = $linterface->GET_ACTION_BTN($button_submit, "button", "window.location.href='ole_student_attachment_upload_update.php'", "uploadBtn");
$uploadBtn = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OLE']['UploadAttachment']['UploadButton'], "button", "goImport();", 'ImportBtn','',$Disabled);
//$backBtn = $linterface->GET_ACTION_BTN($Lang['AccountMgmt']['BackToPhotoMgmt'], "button", "window.location.href='index.php'", "backBtn");
$backBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button",  "window.history.back();");

$linterface->LAYOUT_START();
?>
<br>
<form name="form1" id="form1" method="POST">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><?=$navigation_html?></td>
						</tr>
					</table>
				</td>
				<td align="right"></td>
			</tr>
	</table>
	<br>
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?=$StepObj?>
						</td>
					</tr>
				</table><br>
				<div style="width:90%;margin:0 auto;"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<?=$UploadStat?>
						</td>
						<td align="right"></td>
					</tr>
				</table>
				<br>
				<?=$display?>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<?=$uploadBtn?>
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
				<?= $backBtn ?>
				<?= $cancelBtn?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="tmpfile" value="<?=implode(",",$SuccessfulAttachmentPathAry)?>">
	<input type="hidden" name="UserLogin" value="<?=implode(",",$UserLoginAry)?>">
	<input type="hidden" name="RecordID" value="<?=implode(",",$RecordIDAry)?>">
	<input type="hidden" name="EventID" value="<?= $EventID?>">
</form>
<br><br>
<script>

$(document).ready(function(){

});
function goImport(){

	$('form#form1').attr('action', 'ole_student_attachment_upload_update.php').submit();
}
</script>
<?	
$linterface->LAYOUT_STOP();
intranet_closedb();
?>