<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");
intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Init Library ###
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);
$luser = new libuser($UserID);

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
$ParArr["Role"] = "TEACHER";
$ParArr["assignment_id"] = $assignment_id;
$ParArr["parent_id"] = $assignment_id;
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);
$ParArr["SchemeData"] = $SchemeData;
$SchemeTitle = $SchemeData[0]["title"];

$GroupListOutData = $lo->getGroupsListOut_DataValue($assignment_id, "A");
$GroupListInData = $lo->getGroupsListIn_DataValue($assignment_id, "A");

// get the group list array
/*
$GroupIDs = array();
for($i = 0; $i < count($GroupListInData); $i++)
{
	$GroupIDs[] = $GroupListInData[$i]["group_id"];
}
$SelectedClassList = $lo->getClassListByGroupIDList($GroupIDs, $assignment_id);
*/


$ParArr["GroupListIn"] = $lo->getGroupsListIn($assignment_id, "A");
$ParArr["GroupListOut"] = $lo->getGroupsListOut($assignment_id, "A");


$MenuArr = array();
$MenuArr[] = array($iPort["edit_scheme"], "");
$MenuArr[] = array($SchemeTitle, "");

$Content = "";

$Content .= "<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr><td align=\"center\">";
$Content .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"navigation\">";
$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= "</td></tr></table>";
$Content .= $lpf_ui->GET_SCHEME_FORM($ParArr);
$Content .= "</td></tr></table>";

//////////////////////////////////////////////////////
//////////// END  GET THE PAGE CONTENT  //////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	if (!check_text(myObj.title, "<?=$ec_warning['growth_title']?>")) return false;
	
	checkOption(myObj.elements["target[]"]);
	
	for(var i=0; i< myObj.elements["target[]"].length; i++)
	{
		myObj.elements["target[]"].options[i].selected = true;
	}
	
	myObj.submit();
}

function jSubmit()
{
	var obj = document.form1;
	checkform(obj);
}

function jCancel()
{
	location.href = "index.php";
}

function jReset()
{
	var formObj = document.form1;
	formObj.reset();
	
	var TargetObj = document.form1.elements['target[]'];
	removeAllOption(TargetObj);
	addDefaultOption(TargetObj, 1);
	
	var SourceObj = document.form1.elements['source[]'];
	removeAllOption(SourceObj);
	addDefaultOption(SourceObj, 0);
}

/////////////////////////////////////////////////////////////
///////////  check select group /////////////////////////////
/////////////////////////////////////////////////////////////
var GroupListOut = new Array();
var GroupListIn = new Array();

function initGroupList()
{
	<?
	$x = "";
	for ($i = 0; $i < count($GroupListOutData); $i++)
	{
		$x .= "GroupListOut[".$i."] = new Array(); \n";
		$x .= "GroupListOut[".$i."][0] = \"".$GroupListOutData[$i][0]."\"; \n";
		$x .= "GroupListOut[".$i."][1] = \"".$GroupListOutData[$i][1]."\"; \n";
	}
	echo $x;
	
	$x = "";
	for ($i = 0; $i < count($GroupListInData); $i++)
	{
		$x .= "GroupListIn[".$i."] = new Array(); \n";
		$x .= "GroupListIn[".$i."][0] = \"".$GroupListInData[$i][0]."\"; \n";
		$x .= "GroupListIn[".$i."][1] = \"".$GroupListInData[$i][1]."\"; \n";
	}
	echo $x;
	?>
}

function removeAllOption(obj)
{
	for(var i = (obj.length - 1) ; i >= 0; i--)
	{
		obj.remove(i);
	}
}

function addDefaultOption(obj, action)
{
	if(action == 0)
	{
		// source group
		for(var i = 0; i < GroupListOut.length; i++)
		{
			obj.options[obj.length] = new Option(GroupListOut[i][1], GroupListOut[i][0], false, false);
		}
	}
	else if(action == 1)
	{
		// target group
		for(var i = 0; i < GroupListIn.length; i++)
		{
			obj.options[obj.length] = new Option(GroupListIn[i][1], GroupListIn[i][0], false, false);
		}
	}
}

function jSelectGroup(obj, action)
{
	if(action == 0)
	{
		// deselect
		check(document.form1.elements['target[]'],document.form1.elements['source[]']);
	}
	else if(action == 1)
	{
		// select
		check(document.form1.elements['source[]'],document.form1.elements['target[]']);
	}
	else
	{
		//alert("Invalid Parameter");
	}
}

initGroupList();
/////////////////////////////////////////////////////////////
///////////  end check select group /////////////////////////
/////////////////////////////////////////////////////////////
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>

</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
