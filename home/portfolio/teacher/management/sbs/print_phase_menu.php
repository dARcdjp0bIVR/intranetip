<?php

// Modifing by

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$CurrentPage = "eClass_update_activity";
$title = $ec_iPortfolio['print_result'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

$lgs = new growth_scheme();
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

# get the period
$period_result = $lgs->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);

if($period_result!="FUTURE")
{
	$WarningMsg = ($period_result=="IN") ? "<tr><td class='tabletext' colspan='2'>".$ec_iPortfolio['phase_in_processing_state']."</td></tr>" : "";

	$lo = new libportfolio_group($lpf->course_db);

	// get goups
	$sql  = "SELECT 
				a.group_id, 
				a.group_name 
			FROM 
				grouping AS a, 
				grouping_function AS b 
			WHERE 
				a.group_id = b.group_id 
				AND b.function_id = '{$assignment_id}' 
				AND b.function_type = 'A' 
			ORDER BY 
				a.group_name
			";
	$GroupArray = $lo->returnArray($sql);
	if(sizeof($GroupArray)==0)
	{
		$GroupArray = $lo->getGroups();
	}

	$GroupList = "<SELECT name='GroupID'>";
	for($i=0; $i<sizeof($GroupArray); $i++)
	{
		$GroupList .= "<OPTION value='".$GroupArray[$i][0]."'>".$GroupArray[$i][1]."</OPTION>";
	}
	$GroupList .= "</SELECT>";

	if($ParUserID != "")
	{
		$DisplayContent = "";
	}
	else
	{
		$DisplayContent = $ec_iPortfolio['growth_group'].": ".$GroupList;
	}
	
	$PrintButton = $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "document.form1.submit();", "btn_submit");
}
else
{
	$DisplayContent = $ec_warning['no_view_now'];
}

if($ParUserID != "")
$ActionLink = "print_phase.php";
else
$ActionLink = "print_phase_class.php";


///UI Related , some client request default check  some item, do it my turn on flag//
$h_chkNoPhotoDefaultChecked = ($sys_custom['iPf']['SBS']['print_defaultNoPhoto']) ? ' checked ':'';
$h_chkNoStudentInfoChecked = ($sys_custom['iPf']['SBS']['print_defaultNoStudentInfo']) ? ' checked ':'';
$h_chkNoRelatedPhaseChecked = ($sys_custom['iPf']['SBS']['print_defaultNoRelatedPhase']) ? ' checked ':'';

?>

<FORM enctype="multipart/form-data" action="<?=$ActionLink?>" method="POST" name="form1">
	<table width="420" border="0" cellspacing="0" cellpadding="5">
		<?=$WarningMsg?>
		 <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class='tabletext'><?=$DisplayContent?></td>
        </tr>
		<tr><td class='tabletext'><input type="checkbox" name="NoPhoto" id="photo_1" value="1" <?=$h_chkNoPhotoDefaultChecked?>/>&nbsp;<label for="photo_1"><?=$ec_iPortfolio['not_display_student_photo']?></label></td></tr>
		<tr><td class='tabletext'><input type="checkbox" name="NoStudentInfo" id="details_1" value="1" <?=$h_chkNoStudentInfoChecked?>/>&nbsp;<label for="details_1"><?=$ec_iPortfolio['no_student_info']?></label></td></tr>
		<tr><td class='tabletext'><input type="checkbox" name="NoRelatedPhase" id="related_phase_1" value="1" <?=$h_chkNoRelatedPhaseChecked?>/>&nbsp;<label for="related_phase_1"><?=$ec_iPortfolio['no_related_phase']?></label></td></tr>
      </table>
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		<?=$PrintButton?>
		<?=$linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "self.close();", "btn_cancel");?>
		</td>
	</tr>
	</table>
<input type="hidden" name="phase_id" value="<?=$phase_id?>" />
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>" />
<input type="hidden" name="ParUserID" value="<?=$ParUserID?>" />
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>