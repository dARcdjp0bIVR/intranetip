<?php

// Modifing by Key

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

intranet_auth();
intranet_opendb();

$answersheet = HTMLtoDB($qStr);

// update if submitted from edit.php
$li = new libdb();
$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

if ($notes_id!="")
{
	$sql = "UPDATE $lpf->course_db.assignment SET answersheet='$answersheet' WHERE worktype=8 AND parent_id='$notes_id' ";
	$li->db_db_query($sql);
	//debug($sql);
	$sql = "UPDATE $lpf->course_db.notes SET modified=now() WHERE notes_id='$notes_id' ";
	$li->db_db_query($sql);
	//debug($sql);
}

intranet_closedb();

header("Location: view_form.php?notes_id=$notes_id");
?>