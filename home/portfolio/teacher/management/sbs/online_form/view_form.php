<?php

// Modifing by Key

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

intranet_auth();
intranet_opendb();

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

$li = new libdb();
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$sql = "SELECT $lpf->course_db.assignment_id, sheettype, answersheet FROM assignment WHERE parent_id=$notes_id AND worktype=8 ";
$row = $li->returnArray($sql);
$form_obj = $row[0];

?>

<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/old/style_iPortfolio_purple.css" rel='stylesheet' type='text/css'>
      
<form name="ansForm" method="post" action="update.php">
	<input type=hidden name="qStr" value="<?=$form_obj["answersheet"]?>">
	<input type=hidden name="aStr" value="">
</form>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/tooltip.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>


<script language="Javascript">
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";
answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";


background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$form_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;
document.write(editPanel());

</script>

<form name="form1" method="post" action="update_phase.php" onSubmit="return checkform(this)">
<input type="hidden" name="phase_id" value="<?=$phase_id?>">
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>">
<input type="hidden" name="answersheet" value="<?=$form_obj["answersheet"]?>">
<input type="hidden" name="fieldname" value="answersheet">
<input type="hidden" name="formname" value="form1">
</form>

</body>
</html>

<?php
intranet_closedb();
?>