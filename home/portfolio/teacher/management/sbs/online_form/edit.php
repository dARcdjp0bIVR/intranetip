<?php

// Modifing by Key

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

### Init Library ###
$linterface = new interface_html();
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

//$EC_BL_ACCESS_HIGH = (!strstr($ck_function_rights, "Sharing:Content") || !strstr($ck_user_rights, ":web:"));

$answersheet_js = stripslashes(str_replace('"', '&quot;', $answersheet));

if ($formname=="ansForm")
{
	$style_color_used = "purple";
	$btn_save_action = "saveFormTemplate()";
	$btn_cancel_action = "self.location='view_form.php?notes_id=$notes_id'";
} 
else
{
	$style_color_used = "green";
	$btn_save_action = "returnToParent()";
	$btn_cancel_action = "self.close()";
}
?>

<html>
<head>
<?=returnHtmlMETA()?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/brwsniff.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/old/style_iPortfolio_<?=$style_color_used?>.css" rel='stylesheet' type='text/css'>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/tooltip.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/iportfolio.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/iportfolio_icon_css.php" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?= $LAYOUT_SKIN ?>/css/iportfolio/content.css" rel="stylesheet" type="text/css" />
</head>
<body>

<form name="ansForm" method="post" action="update.php">
	<input type="hidden" name="qStr" value="<?=$answersheet_js?>">
	<input type="hidden" name="aStr" value="">	
	<input type="hidden" name="notes_id" value="<?=$notes_id?>">
</form>



<script language="Javascript">
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";
answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

operator_move_up = "<?=$ec_form_word['move_up_item']?>";
operator_move_down = "<?=$ec_form_word['move_down_item']?>";
operator_delete = "<?=$ec_form_word['delete_item']?>";
operator_rename = "<?=$ec_form_word['rename_item']?>";

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";


background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.mode = 0;	// 0:edit 1:fill in application
sheet.displaymode = <?=$sheettype?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;
document.write(editPanel());

<?php if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){ ?>
    var qType = document.addForm.qType;
    qType.addEventListener("change", function(){
    	if (this.selectedIndex==2||this.selectedIndex==3){
    		var extraONums = document.getElementsByClassName('extraONum');
    		for(var i=0;i<extraONums.length;i++){
    			extraONums[i].style.display = 'block';
    		}
    	}else{
    		var extraONums = document.getElementsByClassName('extraONum');
    		for(var i=0;i<extraONums.length;i++){
    			extraONums[i].style.display = 'none';
    		}
    	}
    });
<?php } ?>

function returnToParent(){
	finish();
	var qStr = document.ansForm.qStr.value;
	opener.document.<?=$formname?>.<?=$fieldname?>.value = qStr.replace(/\"/g, '&quot;');
	self.close();
}

function saveFormTemplate(){
	var objForm = document.ansForm;
	finish();
	objForm.action = "form_edit_update.php";
	objForm.submit();	
}
</script>

<div align="center">
<?
echo $linterface->GET_ACTION_BTN($iPort["btn"]["save"], "button", $btn_save_action, "imgSave");
echo "&nbsp";
echo $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", $btn_cancel_action, "cancel");
?>
</div>

</body>
</html>