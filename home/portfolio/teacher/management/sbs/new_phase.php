<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/html_editor_embed_lib.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Init Library ###
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);
$luser = new libuser($UserID);
$lgs = new growth_scheme();
$JS_Editor = new html_editor_embed_lib("contents", $is_included_before, "100%", "320px", "", $init_html_content);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
$ParArr["Role"] = "TEACHER";
$ParArr["assignment_id"] = $assignment_id;
$ParArr["parent_id"] = $assignment_id;
$parent_id = $assignment_id;
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);
$ParArr["SchemeData"] = $SchemeData;

// store the list option for reset use
$PhaseListOutData = $lgs->GET_NON_SELECTED_RELEVANT_PHASE($parent_id, $phase_id, $relevant_id, "array");
$PhaseListInData = $lgs->GET_SELECTED_RELEVANT_PHASE($parent_id, $phase_id, $relevant_id, "array");

$MenuTitle = " <strong>".$SchemeData[0]["title"]."</strong>";

$MenuArr = array();
$MenuArr[] = array($iPort["new_phase_for"].$MenuTitle, "");

$Content = "";

$Content .= "<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr><td align=\"center\">";
$Content .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"navigation\">";
$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= "</td></tr></table>";
$Content .= $lpf_ui->GET_ADMIN_PHASE_FORM($ParArr);
$Content .= "</td></tr></table>";

//////////////////////////////////////////////////////
//////////// END  GET THE PAGE CONTENT  //////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	var isst = false;
	if (!check_text(myObj.title, "<?=$ec_warning['growth_phase_title']?>")) return false;

	if(myObj.starttime.value=="" || myObj.endtime.value=="")
	{
		alert("<?=$ec_iPortfolio['please_fill_start_end_date']?>");
		return false;
	}

	if (typeof(myObj.starttime)!="undefined")
	{
		if (myObj.starttime.value!="")
		{
			if(!check_date(myObj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			{
				isst = true;
			}
		}
	}
	
	if (typeof(myObj.endtime)!="undefined")
	{
		if (myObj.endtime.value!="")
		{
			if(!check_date(myObj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTimeInput(myObj.starttime, myObj.sh, myObj.sm, myObj.endtime, myObj.eh, myObj.em)) 
				{
					myObj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}
	
	checkOption(myObj.elements["relevant_phase[]"]);
	for(var i=0; i<myObj.elements["relevant_phase[]"].length; i++)
	{
		myObj.elements["relevant_phase[]"].options[i].selected = true;
	}
	
	return true;
} // end function check form

function jCancel()
{
	self.location.href = "index.php";
}

function jReset()
{
	obj = document.form1;
	obj.reset();
	
	var TargetObj = document.form1.elements['relevant_phase[]'];
	removeAllOption(TargetObj);
	addDefaultOption(TargetObj, 1);
	
	var SourceObj = document.form1.elements['src_phase[]'];
	removeAllOption(SourceObj);
	addDefaultOption(SourceObj, 0);
}

function jSubmitFinish()
{
	var obj = document.form1;
	if(checkform(obj))
	{
		obj.NextAction.value = "Finish";
		obj.submit();	
	}
}

function jSubmitPhase()
{
	var obj = document.form1;
	if(checkform(obj))
	{
		obj.NextAction.value = "NewPhase";
		obj.submit();
	}
}

function jSelectPhase(obj, action)
{
	if(action == 0)
	{
		// deselect
		check(document.form1.elements['relevant_phase[]'],document.form1.elements['src_phase[]']);
	}
	else if(action == 1)
	{
		// select
		check(document.form1.elements['src_phase[]'],document.form1.elements['relevant_phase[]']);
	}
}

function editOnlineForm()
{
	postInstantForm(document.form1, "online_form/edit.php", "post", 10, "intranet_popup10");
}

/////////////////////////////////////////////////////////////
///////////  check select phase /////////////////////////////
/////////////////////////////////////////////////////////////
var PhaseListOut = new Array();
var PhaseListIn = new Array();

function initPhaseList()
{
	<?
	$x = "";
	for ($i = 0; $i < count($PhaseListOutData); $i++)
	{
		$x .= "PhaseListOut[".$i."] = new Array(); \n";
		$x .= "PhaseListOut[".$i."][0] = \"".$PhaseListOutData[$i][0]."\"; \n";
		$x .= "PhaseListOut[".$i."][1] = \"".$PhaseListOutData[$i][1]."\"; \n";
	}
	echo $x;
	
	$x = "";
	for ($i = 0; $i < count($PhaseListInData); $i++)
	{
		$x .= "PhaseListIn[".$i."] = new Array(); \n";
		$x .= "PhaseListIn[".$i."][0] = \"".$PhaseListInData[$i][0]."\"; \n";
		$x .= "PhaseListIn[".$i."][1] = \"".$PhaseListInData[$i][1]."\"; \n";
	}
	echo $x;
	?>
}

function removeAllOption(obj)
{
	for(var i = (obj.length - 1) ; i >= 0; i--)
	{
		obj.remove(i);
	}
}

function addDefaultOption(obj, action)
{
	if(action == 0)
	{
		// source phase
		for(var i = 0; i < PhaseListOut.length; i++)
		{
			obj.options[obj.length] = new Option(PhaseListOut[i][1], PhaseListOut[i][0], false, false);
		}
	}
	else if(action == 1)
	{
		// target phase
		for(var i = 0; i < PhaseListIn.length; i++)
		{
			obj.options[obj.length] = new Option(PhaseListIn[i][1], PhaseListIn[i][0], false, false);
		}
	}
}

function jSelectPhase(obj, action)
{
	if(action == 0)
	{
		// deselect
		check(document.form1.elements['relevant_phase[]'],document.form1.elements['src_phase[]']);
	}
	else if(action == 1)
	{
		// select
		check(document.form1.elements['src_phase[]'],document.form1.elements['relevant_phase[]']);
	}
}

initPhaseList();
/////////////////////////////////////////////////////////////
///////////  end check select Phase /////////////////////////
/////////////////////////////////////////////////////////////

</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
