<?php

// Modifing by
/*
 * Modification Log
 * - 2013-12-20 Ivan (Case#B57070)
 * 	 Change to use "page-break-after" with a div to control the page break
 * 
 * - 2011-01-06 Thomas
 * 	 Remove JS script 'layer.js'
 *   Move <body> tag to the beginning of the html document	
 * */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_opendb();
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$lgs = new growth_scheme();
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

$assignment_id  = $phase_obj["parent_id"];
$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];
	
$relevant_phase_html = ($NoRelatedPhase==1) ? "" : $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], "", 1);

// get handed-in users
$UserArr = $lgs->getHandedinUser($phase_id, $GroupID);


if(sizeof($UserArr)!=0)
{
	?>
	<body bgcolor=transparent>
	<!--
	<link rel='stylesheet' href='<?=$eclass_root?>/system/skins/chemistry/style_new.css'>
	<link rel='stylesheet' href='<?=$eclass_root?>/src/includes/css/style_calendar.css'>
	<link href='<?=$eclass_root?>/src/includes/css/style_portfolio.css' rel='stylesheet' type='text/css'>
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_link.css" rel="stylesheet" type="text/css">
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_content.css" rel="stylesheet" type="text/css">
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_table.css" rel="stylesheet" type="text/css">
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_button.css" rel="stylesheet" type="text/css">
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_body.css" rel="stylesheet" type="text/css">
	<link href="<?=$eclass_root?>/src/includes/css/style_portfolio_test.css" rel="stylesheet" type="text/css">
	-->
	<style type="text/css">
	html, body { margin: 0px; padding: 0px; } 
	BODY {background-color: #FFFFFF;}
	.style11 {font-size: 11px; }
	.style16 {font-size: 12px; }
	.style29 {font-weight: bold; font-size: 12px;}

	TD {
		FONT-SIZE: 14px; FONT-FAMILY: Times New Roman, Verdana, Arial, Helvetica, Mingliu, Sans-Serif; COLOR: black;
	}
	
	.form_title {
	font-family: "Arial", "Verdana", "Helvetica", "sans-serif";
	font-size: 20px;
	font-weight: bold;
	color: black;
	}
	
	</style>
	<script language="Javascript">
		var form_title_main = "<?=$phase_obj['title']?>";
		var form_description = "<?=preg_replace('(\r\n|\n)', '<br>', $phase_obj['instruction'])?>";
		var form_reference = "<?=$relevant_phase_html?>";
		var print_report = 1;
		var txtStr = '';
	</script>
	<STYLE TYPE='text/css'>
		P.breakhere {page-break-before: always}
	</STYLE>
	<?php
	if($phase_obj["answer_display_mode"]==1)
	{?>
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>
		<script language="Javascript">
			answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
			answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
			answersheet_header = "<?=$ec_form_word['question_title']?>";
			answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
			no_options_for = "<?=$ec_form_word['no_options_for']?>";
			pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
			pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
			chg_title = "<?=$ec_form_word['change_heading']?>";
			chg_template = "<?=$ec_form_word['confirm_to_template']?>";

			answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
			answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
			answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
			answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
			answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
			answersheet_option = "<?=$answersheet_option?>";
			answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";
			answer_display_mode = "<?=$phase_obj['answer_display_mode']?>";
			
/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

			button_submit = " <?=$button_submit?> ";
			button_add = " <?=$button_add?> ";
			button_cancel = " <?=$button_cancel?> ";
			button_update = " <?=$button_update?> ";

			background_image = "";

			var submit_action = " onSubmit=\"return false\"";
			var display_answer_form = 1;
			var image_path = "<?=$image_path?>";
		</script>
	<?php
	}
	else
	{
		$phase_obj["answersheet"] = str_replace("\"", "\\\"", undo_htmlspecialchars($phase_obj["answersheet"]));
		$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $phase_obj["answersheet"])) : preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $phase_obj["answersheet"]));
		?>
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_view.js"></script>
		<script language="Javascript">
/************************************************************/
answer_display_mode = "<?=$phase_obj['answer_display_mode']?>";
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";

/************************************************************/

			var myQue = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $phase_obj['answersheet']))?>";
			var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
			var displaymode = <?=$phase_obj["sheettype"]?>;
		</script>
	<?php
	}

	for($i=0; $i<sizeof($UserArr); $i++)
	{
		$user_id = $UserArr[$i];
		
		$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
		if(!is_null($handin_obj))
		{
			$TempAnswer = $handin_obj["answer"];

			$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
			$phase_obj["status"] = $handin_obj["status"];

			$handin_obj["answer"] = preg_replace('(\r\n|\n)', "<br>", $handin_obj["answer"]);
			$handin_obj["answer"] = str_replace(" ", "&nbsp;", $handin_obj["answer"]);	
		}
		
		# get student info table
		$luser = new libuser($user_id, $ck_course_id);
		$StudentInfoTable = ($NoStudentInfo!=1) ? $lgs->getUserInfoTable($user_id, $NoPhoto, $NoStudentInfo) : "";
		if($NoPhoto!=1)
		{
			$StudentPhoto = $lgs->getStudentPhoto($user_id);
			$user_photo_js = "<table border=0><tr>";
			$user_photo_js .= "<td>".str_replace('"', '\"', $StudentPhoto)."</td>";
			$user_photo_js .= ($StudentInfoTable!="") ? "<td>".$StudentInfoTable."</td>" : "";
			$user_photo_js .= "</tr></table>";
		}
		else
			$user_photo_js = $StudentInfoTable;

		?>
		<script language="Javascript">
			var form_user_photo = "<?=$user_photo_js?>";
		</script>
		<?php
		if($phase_obj["answer_display_mode"]==1)
		{
			$form_name = "ansForm".$i;
			$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);
		?>
			<form name="<?=$form_name?>" method="post">
				<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
				<input type="hidden" name="aStr" value="<?=str_replace("&nbsp;", " ", $handin_obj["answer"])?>">
			</form>
			<script language="Javascript">
				var sheet = new Answersheet();
				var i = "<?=$i?>";

				// attention: MUST replace '"' to '&quot;'
				var tmpStr = eval("document.ansForm"+i+".qStr.value");
				var tmpStrA = eval("document.ansForm"+i+".aStr.value");
				//var tmpStr = document.ansForm.qStr.value;
				//var tmpStrA = document.ansForm.aStr.value;
				sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
				sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
				sheet.mode = 1;	// 0:edit 1:fill in application
				sheet.displaymode = <?=$phase_obj["sheettype"]?>;
				sheet.answer = sheet.sheetArr();
				
				// temp
				var form_templates = new Array();
				
				sheet.templates = form_templates;
				txtStr += '<div style="page-break-after:always;">\n';
				txtStr += sheet.writeSheet();
				txtStr += '</div>\n';
				//txtStr += '<p class="breakhere"></p>\n';
				//txtStr += '<p style="page-break-after:always;"></p>\n';
			</script>
		<?php
		}
		else
		{
			$handin_obj['answer'] = str_replace("&nbsp;", " ", $handin_obj['answer']);
			?>
			<script language="Javascript">
				var myAns = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $handin_obj['answer']))?>";
				txtStr += '<div style="page-break-after:always;">\n';
				txtStr += viewForm(myQue, myAns);
				txtStr += '</div>\n';
				//txtStr += '<p class="breakhere"></p>\n';
				//txtStr += '<p style="page-break-after:always;"></p>\n';
			</script>
		<?php
		}
	}
	?>
	<!--DIV ID="blockInput" STYLE="background-color: transparent; width:100%; visibility:visible;"-->
	<script language="javascript">
		document.write(txtStr);
	</script>
	<!--/DIV-->
	</body>
	<?php
}
else
{
	$linterface = new interface_html("popup.html");
	
	$CurrentPage = "eClass_update_activity";
	$title = $ec_iPortfolio['print_result'];
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ["title"] = $title;

	$linterface->LAYOUT_START();

	$Content .= "<table align='center'>";
	$Content .= "<tr><td height='100'>$no_record_msg<td></tr>";
	$Content .= "<tr><td height='15' align='center'>";
	$Content .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "history.back();", "btn_cancel");
	$Content .= "<td></tr>";
	$Content .= "</table>";
	echo $Content;
	
	$linterface->LAYOUT_STOP();
}

intranet_closedb();
?>