<?php

// Modifing by key

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

// resolve the assignment_id to array
$tmp_phase_id = array();
$tmp_phase_id[] = $phase_id;
$phase_id = $tmp_phase_id;

if (is_array($phase_id))
{
	$assignment_id_str = implode(",", $phase_id);

	# delete handins
	$sql = "DELETE FROM $lpf->course_db.handin WHERE assignment_id IN ($assignment_id_str) ";
	$li->db_db_query($sql);

	# delete assignments
	$sql = "DELETE FROM $lpf->course_db.assignment WHERE assignment_id IN ($assignment_id_str) ";
	$li->db_db_query($sql);

	$msg = 3;
}

header("Location: index.php?msg=$msg");

intranet_closedb();
?>
