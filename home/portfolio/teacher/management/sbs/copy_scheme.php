<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

iportfolio_auth("TA");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$assignment_fields = "title, instruction, html_opt, status, attachment, answer, starttime, deadline, answersheet, modelanswer, worktype, parent_id, percentage, marking_scheme, sheettype, modified, inputdate";

function getSqlValues($myObj, $my_parent_id=null, $title_postfix="")
{
	$rx = "'".addslashes($myObj["title"]).$title_postfix."', ";
	$rx .= "'".addslashes($myObj["instruction"])."', ";
	$rx .= "'".$myObj["html_opt"]."', ";
	$rx .= "'".$myObj["status"]."', ";
	$rx .= "'".addslashes($myObj["attachment"])."', ";
	$rx .= "'".addslashes($myObj["answer"])."', ";
	$rx .= ($myObj["starttime"]!="") ? "'".$myObj["starttime"]."', " : "NULL, ";
	$rx .= ($myObj["deadline"]!="") ? "'".$myObj["deadline"]."', " : "NULL, ";
	$rx .= ($myObj["answersheet"]!="") ? "'".addslashes($myObj["answersheet"])."', " : "NULL, ";
	$rx .= ($myObj["modelanswer"]!="") ? "'".addslashes($myObj["modelanswer"])."', " : "NULL, ";
	$rx .= "'".$myObj["worktype"]."', ";
	$rx .= ($my_parent_id!=null) ? "'".$my_parent_id."', " : "NULL, ";
	$rx .= "'".$myObj["percentage"]."', ";
	$rx .= ($myObj["marking_scheme"]!="") ? "'".$myObj["marking_scheme"]."', " : "NULL, ";
	$rx .= "'".$myObj["sheettype"]."', ";
	$rx .= "now(), ";
	$rx .= "now() ";

	return $rx;
}


$li = new libdb();

# copy main scheme (worktype==6, assignment)
$sql = "SELECT $assignment_fields FROM $lpf->course_db.assignment WHERE assignment_id='$assignment_id' ";
$row = $li->returnArray($sql);
$scheme_obj = $row[0];
$scheme_obj["title"] = $iPort["copy_of"].$scheme_obj["title"];

$values_sql = getSqlValues($scheme_obj, null, (" ".$status_copy));
$sql = "INSERT INTO $lpf->course_db.assignment ($assignment_fields)
		VALUES ($values_sql) ";
$li->db_db_query($sql);

$new_assignment_id = $li->db_insert_id();


# copy group permission
$sql = "SELECT group_id FROM $lpf->course_db.grouping_function WHERE function_type='A' AND function_id='$assignment_id' ";
$groups_arr = $li->returnVector($sql);
for ($i=0; $i<sizeof($groups_arr); $i++)
{
	$group_id = $groups_arr[$i];
	$sql = "INSERT INTO $lpf->course_db.grouping_function (group_id, function_id, function_type) VALUES ($group_id, $new_assignment_id, 'A')";
	$li->db_db_query($sql);
}


# find and copy child phases (worktype==7)
$sql = "SELECT $assignment_fields FROM $lpf->course_db.assignment WHERE parent_id='$assignment_id' AND worktype=7 ORDER BY assignment_id ";
$phases_arr = $li->returnArray($sql);

for ($i=0; $i<sizeof($phases_arr); $i++)
{
	$phase_obj = $phases_arr[$i];
	$values_sql = getSqlValues($phase_obj, $new_assignment_id, "");
	$sql = "INSERT INTO $lpf->course_db.assignment ($assignment_fields)
		VALUES ($values_sql) ";
	$li->db_db_query($sql);
}

$msg = 2;
header("Location: index.php?msg=$msg");

intranet_closedb();
?>
