<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Init Library ###
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$luser = new libuser($UserID);

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
$ParArr["Role"] = "TEACHER";
$ParArr["parent_id"] = $assignment_id;

$GroupListIn = $lo->getGroupsListIn_DataValue($assignment_id, "A");
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$ParArr["GroupListIn"] = $GroupListIn;
$ParArr["SchemeData"] = $SchemeData;

if($msg == 1)
$Message = "<span class=\"tabletextrequire\">&nbsp;(".$iPort["msg"]["record_add"].")</span>";

$MenuArr = array();
$MenuArr[] = array($iPort["new_scheme"].$Message, "");

$Content = "";

$Content .= "<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\"><tr><td align=\"center\">";
$Content .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"navigation\">";
$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= "</td></tr></table>";
$Content .= $lpf_ui->GET_SCHEME_SUMMARY($ParArr);
$Content .= "</td></tr></table>";

//////////////////////////////////////////////////////
//////////// END  GET THE PAGE CONTENT  //////////////
//////////////////////////////////////////////////////

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
function jGoCreatePhase()
{
	self.location.href = "new_phase.php?assignment_id=<?=$assignment_id?>";
}

function jGoSchemeList()
{
	self.location.href = "index.php";
}
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>

</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
