<?php

// Modifing by kit

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("TA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_opendb();

$linterface = new interface_html("popup.html");
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$linterface->LAYOUT_START();

$lgs = new growth_scheme();
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$assignment_id = $phase_obj["parent_id"];

$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];

$QuestionOnly = 1;

# relevant phase preparation
if($ParUserID == "")
$relevant_phase_html = $lgs->getRelevantPhaseView($phase_obj["answer"]);
else
{
	$handin_obj = $lgs->getStudentPhaseHandin($ParUserID, $phase_obj);
	$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $ParUserID);
	$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
	$phase_obj["status"] = $handin_obj["status"];
}

# relevant person preparation
$subject_name = ($phase_obj["teacher_subject_id"]!="") ? $lgs->getSubjectName($phase_obj["teacher_subject_id"]) : "";
switch ($phase_obj["person_response"])
{
	case "CT":
			$target_person_html = $usertype_ct;
			break;
	case "P":
			$target_person_html = $usertype_p;
			break;
	case "ST":
			$target_person_html = $usertype_st." (".$subject_name.")";
			break;
	case "S":
	DEFAULT:
			$target_person_html = $usertype_s;
			break;
}

?>

<table id="html_body_frame" width=100% border="0" cellpadding="0" cellspacing="0">
<tr><td>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr><td>
      <table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_phase_title'] ?>:</td>
          <td class="tabletext"><?=$phase_obj["title"]?></td>
        </tr>
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_description'] ?>:</td>
            <td class="tabletext"><?=nl2br($phase_obj["instruction"])?></td>
        </tr>
        <tr>
          <td valign="top" nowrap class="tabletext"><?= $ec_iPortfolio['growth_target_person'] ?>:</td>
          <td class="tabletext"><?=$target_person_html?></td>
        </tr>
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_phase_period'] ?>:</td>
          <td class="tabletext"><?= $phase_obj["starttime"] . " {$profiles_to} " . $phase_obj["deadline"] ?></td>
        </tr>
        <!--<tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_relevant_phase'] ?>:</td>
          <td class="tabletext"><?=$relevant_phase_html?></td>
        </tr>-->
        <tr>
          <td colspan="2" valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_online_form'] ?>:

<form name="ansForm" method="post" action="update.php">
	<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
<!--
	<input type="hidden" name="aStr" value="<?=preg_replace('(\r\n|\n)', "<br />", $handin_obj["answer"])?>">
-->
	<input type="hidden" name="aStr" value="<?=$handin_obj["answer"]?>">
	<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>">
	<input type="hidden" name="phase_id" value="<?=$phase_id?>">
	<input type="hidden" name="assignment_id" value="<?=$parent_id?>">
	<input type="hidden" name="answersheet" value="<?=$phase_obj["answersheet"]?>">
	<input type="hidden" name="fieldname" value="answersheet">
	<input type="hidden" name="formname" value="ansForm">
</form>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>

<?php
if($QuestionOnly)
echo "<script language=\"javascript\" src=\"".$PATH_WRT_ROOT."templates/online_form_edit.js\"></script>";
else
echo "<script language=\"javascript\" src=\"".$PATH_WRT_ROOT."templates/online_form_view.js\"></script>";
//echo "<script language=\"javascript\" src=\"".$PATH_WRT_ROOT."home/portfolio/js/online_form_view.js\"></script>";
?>

<script language="Javascript">
function jClose()
{
	self.close();	
}

function jPrint(phase_id, assignment_id)
{
	var url = "print_phase.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id+"&ParUserID=<?=$ParUserID?>";
	
	newWindow(url, 28);
}



function editOnlineForm()
{
	postInstantForm(document.form1, "online_form/edit.php", "post", 10, "ec_popup10");
}


<?php
if($QuestionOnly)
{
?>
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";

answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

//added on 20090213
/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";

background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;
var tmpStrA = document.ansForm.aStr.value;

//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$phase_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;

document.write(editPanel());
<?php
}else{
?>

var myQue = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $phase_obj['answersheet']))?>";
var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
var displaymode = <?=$phase_obj["sheettype"]?>;

var myAns = "<?=preg_replace('(\r\n|\n)', '<br>', str_replace('&amp;', '&', $handin_obj['answer']))?>";
document.write(viewForm(myQue, myAns));
<?php
}
?>
</script>

</td></tr></table>

</td></tr>
<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<tr><td>
<?
$x = "";
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td align=\"center\">";
$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["print"], "button", "jPrint($phase_id, $assignment_id);", "btn_print");
$x .= "&nbsp;";
$x .= $linterface->GET_ACTION_BTN($button_close, "button", "jClose();", "btn_close");
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";

echo $x;
?>
</td></tr>

</td></tr></table>

<form name="form1" method="post" action="phase_update.php" onSubmit="return checkform(this)">
<input type="hidden" name="phase_id" value="<?=$phase_id?>">
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>">
<input type="hidden" name="answersheet" value="<?=$phase_obj["answersheet"]?>">
<input type="hidden" name="fieldname" value="answersheet">
<input type="hidden" name="formname" value="form1">
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>