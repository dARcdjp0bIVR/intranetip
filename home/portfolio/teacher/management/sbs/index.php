<?php

# Modifing by Pun
/**
 * Change Log:
* 2016-03-16 Pun
*  - Modified jPrint(), added hartspreschool cust report
*/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

### Get the library ###
$lgs = new growth_scheme();
$luser = new libuser($UserID);
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);

// check iportfolio admin user
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

//////////////////////////////////////////////////////
////////////   GET THE PAGE CONTENT  /////////////////
//////////////////////////////////////////////////////
// page number settings
$order = ($order == 1) ? 1 : 0;

if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($pageNo == "")
	$pageNo = 1;

	if(!isset($Status))
		$Status = 1;

		// ParArr settings
		$ParArr["Role"] = "TEACHER";

		$ParArr["SchoolYear"] = $SchoolYear;
		//$ParArr["SchoolYear"] = $lpf->GET_ACADEMIC_YEAR();

		$KIS_edit_role = true;
		if($_SESSION["platform"]=="KIS"){
			if($_SESSION["SSV_USER_ACCESS"]["other-iPortfolio"]){
				$user['KIS_teacher_type'] = 'admin';
			}else {
				$sql = "SELECT count(*)
				FROM
				{$intranet_db}.ASSESSMENT_ACCESS_RIGHT AAR
				WHERE
				AccessType = '0'
				AND SubjectID IS NULL
				AND AAR.UserID = '{$UserID}'";
					
				if(current($lpf->returnVector($sql))>0){
					$user['KIS_teacher_type'] = 'CoursePanel';
				}else{
						
					$KIS_edit_role = false;
						
					$sql = "SELECT YearID
					FROM
					{$intranet_db}.ASSESSMENT_ACCESS_RIGHT AAR
					WHERE
					AccessType = '1'
					AND SubjectID IS NULL
					AND AAR.UserID = '{$UserID}'
					";

					$formIdAry = $lpf->returnArray($sql);
						
					$sql = "Select  a.YearID, a.YearClassID, a.ClassTitleEN
					From
					{$intranet_db}.YEAR_CLASS a
					INNER JOIN
					{$intranet_db}.YEAR_CLASS_TEACHER c
					ON
					a.YearClassID = c.YearClassID
					WHERE a. AcademicYearID = '$ck_current_academic_year_id' AND c.UserID = '{$UserID}'
					";
					$classDetail = $lpf->returnArray ( $sql );
						
					if(sizeof($formIdAry) > 0 && sizeof($classDetail) > 0){
						$user['KIS_teacher_type'] = 'FormClassTeacher';
						$user ['KIS_class_info'] = $classDetail;
						$user ['KIS_specific_form'] = $formIdAry;
					}else{
						if(sizeof($formIdAry) > 0){
							$user['KIS_teacher_type'] = 'FormTeacher';
							$user ['KIS_specific_form'] = $formIdAry;
						}else if(sizeof($classDetail) > 0){
							$user['KIS_teacher_type'] = 'ClassTeacher';
							$user ['KIS_class_info'] = $classDetail;

						}else{
							$user['KIS_teacher_type'] = 'NormalTeacher';
						}
					}
						
				}
			}
			$ParArr["KIS_Detail"] = $user;
		}

		if($user['KIS_teacher_type'] == 'FormTeacher'|| $user['KIS_teacher_type'] == 'ClassTeacher' || $user['KIS_teacher_type'] == 'FormClassTeacher' || $user['KIS_teacher_type'] == 'NormalTeacher'){
			$ParArr["Role"] = "KIS_TEACHER";
		}

		$ParArr["KIS_teacher_type"] = $user['KIS_teacher_type'];
		$keyword = trim($keyword);
		$ParArr["keyword"] = $keyword;
		$ParArr["NumOfCol"] = 5; // default phase number display in one row
		$ParArr["page_size"] = $page_size;
		$ParArr["pageNo"] = $pageNo;
		$ParArr["Status"] = $Status;
		$ParArr["KIS_CONDITION"] = $Status;

		$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

		$ParArr["SchemeData"] = $SchemeData;

		$SchemeRecordArr = $lpf_ui->GET_SCHEME_TABLE($ParArr);
		$SchemeRecord = $SchemeRecordArr["content"];
		$SchemeRecordCount = $SchemeRecordArr["count"];

		# TABLE SQL
		$ParArr["selectType"] = "count";


		$sql = $lpf->GET_SCHOOL_BASED_SCHEME_QUERY($ParArr);

		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("Num");
		$li->sql = $sql;
		$li->no_col = 1;
		$li->record_num_mode = "iportfolio";

		$li->IsColOff = "iPortfolioSchoolBasedScheme";
		$li->title = $iPort['table']['records'];

		$Content = "";
		$Content .= $lpf_ui->GET_RETURN_MESSAGE($msg);
		if($KIS_edit_role){
			$Content .= $lpf_ui->GET_NEW_SCHEME_BUTTON();
		}
		$Content .= $lpf_ui->GET_SELECTION_SEARCH_TABLE($ParArr);
		$Content .= $li->display($SchemeRecord, $SchemeRecordCount);
		$Content .= "<br /><br /><br />";
		//////////////////////////////////////////////////////
		////////////   END GET THE PAGE CONTENT  /////////////
		//////////////////////////////////////////////////////

		$linterface->LAYOUT_START();
		?>

<SCRIPT LANGUAGE="Javascript">
<!--
function jChangeFilterStatus(obj)
{
	document.form1.submit();
}

function jChangeSchoolYear(obj)
{
	document.form1.pageNo.value = 1;
	document.form1.SchoolYear.value = obj.value;
	document.form1.submit();
}

function jSubmitForm()
{
	document.form1.submit();
}

function jChangeStatus(curStatus, id, msg)
{
	var page = "#scheme_"+id;
	
	if(curStatus == 1)
	{
		// to deactivate
		var x = confirm(msg);
		
		if(x)
		{
			self.location.href = "update_scheme.php?assignment_id="+id+"&UpdateSatus=0&page="+page;
		}
	}
	else
	{
		// to activate
		var x = confirm(msg);
		
		if(x)
		{
			self.location.href = "update_scheme.php?assignment_id="+id+"&UpdateSatus=1&page="+page;
		}
	}
}

function jCheckRemoveScheme(msg, id)
{
	var x = confirm(msg);
	
	if(x)
	{
		self.location.href = "remove_scheme.php?assignment_id="+id;
	}
}

function jCheckRemovePhase(msg, id)
{
	var x = confirm(msg);
	
	if(x)
	{
		self.location.href = "remove_phase.php?phase_id="+id;
	}
}

function jPrint(phase_id, assignment_id)
{
	<?php if($sys_custom['iPf']['hartspreschool']['SBS']){ ?>
		var url = "../../../student/sbs/print_pdf_hartspreschool.php?phase_id="+phase_id+"&assignment_id="+assignment_id+"&print_all=1";
	<?php }else{ ?>
		var url = "print_phase_menu.php?phase_id="+phase_id+"&assignment_id="+assignment_id;
	<?php } ?>
	
	newWindow(url, 28);
}

function jExport(phase_id, assignment_id)
{
	var url = "export_phase.php";
	
	document.form1.phase_id.value = phase_id;
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = "index.php";
}

function jExportAll(assignment_id)
{
	var url = "export_all_phases.php";
	
	document.form1.parent_id.value = assignment_id;
	document.form1.action = url;
	document.form1.submit();
	document.form1.action = "index.php";
}

//-->
</SCRIPT>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<form name="form1" id="form1" action="index.php" method="post">
<?=$Content?>

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="phase_id" value="" />
<input type="hidden" name="parent_id" value="" />
</form>


</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
