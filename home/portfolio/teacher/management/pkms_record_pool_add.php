<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// Default: no effect on processing OLE Record
$IntExt = 0;

// Initializing classes
$LibPortfolio = new libpf_slp();
$LibUser = new libuser($StudentID);

$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("popup.html");
// set the current page title
$CurrentPage = "Teacher_OLE";
//$CurrentPageName = $IntExt == 1 ? "<font size='-2'>".$iPort["external_record"]."</font>" : $iPort['menu']['ole'];
$CurrentPageName = $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

//# load approval setting
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

if ($order=="") $order=0;
if ($field=="") $field=1;
$LibTable = new libdbtable2007($field, $order, $pageNo);
$LibTable->field_array = array("c.Title","OLEDate",  "c.Category", "a.Hours");

$conds = " AND a.RecordStatus IN ('2','4')";
// Set condition to null PKMS SLP order
$conds .= " AND a.PKMSSLPOrder IS NULL";

$namefield = getNameFieldWithClassNumberByLang("b.");

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("c.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";

//if($IntExt == 1)
//	$conds .= " AND c.IntExt = 'EXT'";
//else
// Set condition to all records (internal and external)
$conds .= " AND (c.IntExt = 'INT' || c.IntExt = 'EXT')";

$sql = "	SELECT
              	c.Title,
              	IF ((c.StartDate IS NULL OR c.StartDate='0000-00-00'),'--',IF(c.EndDate IS NULL OR c.EndDate='0000-00-00',c.StartDate,CONCAT(c.StartDate,'<br />$profiles_to<br />',c.EndDate))) as OLEDate,
			  	$SqlCategoryField
				a.Hours,
				CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'),
				IF ((Month(a.StartDate)>8), Year(a.StartDate), Year(a.StartDate)-1) AS StartYear
			FROM 
				{$eclass_db}.OLE_STUDENT as a
			LEFT JOIN 
				{$intranet_db}.INTRANET_USER as b
			ON 
				a.ApprovedBy = b.UserID
			LEFT JOIN
				{$eclass_db}.OLE_PROGRAM as c
			ON
				a.ProgramID = c.ProgramID
        	WHERE 
				a.UserID = '$StudentID'
				$conds
			";
				
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 6;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center'>#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='100' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td>".$LibTable->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(2,$ec_iPortfolio['category'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(3,$ec_iPortfolio['hours'], 1)."</td>\n";
$LibTable->column_list .= "<td>".$LibTable->check("record_id[]")."</td>\n";

$LibTable->column_array = array(10,10,0,10,0,10);
	
//////////////////////////////////////////////

// load settings
//$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
//$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt, 1);
//$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);
//
//if(is_array($OLESettings))
//{
//	if($OLESettings[$StudentLevel[1]][0] != "")
//		$RecordsAllowed = $OLESettings[$StudentLevel[1]][0]-count($OLEAssignedArr);
//	else
//		$RecordsAllowed = "inf";
//}

// Access checking
$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
if(!file_exists($portfolio__plms_report_config_file) || get_file_content($portfolio__plms_report_config_file) == null){
	intranet_closedb();
	header("Location: ole_student.php?IntExt=$IntExt&StudentID=$StudentID");
//	exit();
} 
else 
{
	list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
	$totalRecordsAllowed = unserialize($noOfRecord);
}

// Get number of records available for adding
$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($StudentID, $IntExt, 1);

if(is_array($OLEAssignedArr)){
	$RecordsAllowed = $totalRecordsAllowed - count($OLEAssignedArr);
}
//debug_pr($RecordsAllowed);
$rec_allowed_msg = (is_int($RecordsAllowed)) ? str_replace("<!--NoRec-->", $RecordsAllowed, $ec_iPortfolio['ole_no_rec_add_to_pool']) : "";

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">

function checkform(jParFormObj)
{
<?php if(is_int($RecordsAllowed)) { ?>
	if(countChecked(jParFormObj, "record_id[]") > <?=$RecordsAllowed?>)
	{
		alert("<?=$ec_warning['over_limit']?>");
	}
	else
	{
		if(countChecked(jParFormObj, "record_id[]") > 0)
		{
			jParFormObj.action = "pkms_record_pool_add_update.php";
			jParFormObj.submit();
		}
		else
		{
			alert("<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>");
		}
	}
<?php } else { ?>
  jParFormObj.action = "pkms_record_pool_add_update.php";
  jParFormObj.submit();
<?php } ?>
}

</SCRIPT>

<FORM name="form1" method="POST">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<?=$xmsg?>	
	<tr>
		<td>
		<!-- CONTENT HERE -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php if($rec_allowed_msg != "") { ?>
		<tr>
			<td><?=$rec_allowed_msg?></td>
		</tr>
<?php } ?>
		<tr>
			<td align="right">		

						
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>				
					<td align="right" valign="bottom">
			
					<?
					if ($ck_memberType!="P"){
					?>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
					

							<table border="0" cellpadding="0" cellspacing="2">
							<tbody>
							<tr>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkform(document.form1)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_new.gif"  name="imgNew" align="absmiddle" border="0" ><?=$button_add ?> </a></td>
							</tr>
							</tbody>
							</table>
							
						</td>
						<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
					
					</tr>
					</tbody>
					</table>
					<?
					}
					?>
					
					</td>
				</tr>
				</table>
					
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
	          		<?php 
	          			$LibTable->displayPlain() ;
	          		?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
					<?php
					if ($LibTable->navigationHTML!="")
					{
					?>
						<tr class='tablebottom'>
						<td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
						</tr>
					<?php
					}
					?>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="IntExt" value="<?=$IntExt?>" />
<input type="hidden" name="StudentID" value="<?=$StudentID?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>