<?php
/**
 * Modifying By: 
 * *************************************
 * Modification Log:
 * 2015-06-10 Bill	[2015-0324-1420-39164]
 * - fixed auto complete problem when using firefox
 * 2015-05-12 Bill	[2015-0324-1420-39164]
 * - Display of preset "Role of Participation"
 * - Disable the input field when input records for different OLE component
 * - keyword search function of "Details / Name of Activities" and "Role of Participation"
 * - Fixed: Hours / Marks must be positive value
 * 2015-01-13 Bill
 * - Add remarks for Ng Wah SAS Cust
 * 2014-10-20 Bill
 * - Add autocomplete to Role and Achievements
 * - Pass parameter to double preset window width
 * 2010-03-23 Max (201003231511)
 * - Pass paramenter to GEN_ASSIGN_OLE_STUDENT_TABLE() distinguish IntExt Program to roll out different table
 * *************************************
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");

intranet_auth();
intranet_opendb();

// Initializing classes
$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
//$LibPortfolio->ACCESS_CONTROL("ole");
$fs = new libfilesystem();

// template for teacher page
$linterface = new interface_html();
// set the current page title
$CurrentPage = "Teacher_OLE";
$CurrentPageName = $iPort['menu']['ole'];

#######################
# GET THE PASSING VARIABLE 
# 1) From Previous Page
# 2) From Session
#######################

//1) From Previous Page
$EventID = trim($EventID);
//$record_id[] is a array

if($passBackParameter != ""){
	$passBackParameter = base64_decode($passBackParameter);
	foreach(explode("&",$passBackParameter) as $pKey => $parameter) {
		$paraNameValuePair = explode("=",$parameter);
		$paraName = $paraNameValuePair[0];
		$parVar = $paraNameValuePair[1];
		$parameters[$paraName] = $parVar;
	}
	$EventID = $parameters["EventID"];
	if($parameters["record_id"] != ""){
		$record_id = explode(",",$parameters["record_id"]);
	}
	$IntExt = $parameters["IntExt"];
}

#####################
# GET THE PROGRAM DETAILS 
#####################

$objOLEProgram = new ole_program($EventID);
$programTitle = $objOLEProgram->getTitle();

$programPeriod = $objOLEProgram->getPeriod();
$programEleDetails = $objOLEProgram->getEleDetails();

$h_ele = "";

if(strtolower($objOLEProgram->getIntExt()) == strtolower($ipf_cfg["OLE_TYPE_STR"]["INT"])){
	//DISPLAY ELE FOR INT PROGRAM ONLY
	$h_ele = "<tr ><td class=\"field_title\">".$ec_iPortfolio['ele'].":</td><td>";

	for($i = 0,$i_max = sizeof($programEleDetails);$i< $i_max; $i++){
		$h_ele .= " - ".$programEleDetails[$i]."<br/>";
	}
	$h_ele .= "</td></tr>";
}


$programCatTitle = Get_Lang_Selection($objOLEProgram->getCatChiTitle(),$objOLEProgram->getCatEngTitle());
$programSubCatTitle = Get_Lang_Selection($objOLEProgram->getSubCatChiTitle(),$objOLEProgram->getSubCatEngTitle());
$programDetails = $objOLEProgram->getDetails();
$programSchoolRemarks = $objOLEProgram->getSchoolRemarks();

$eventType = -1;
// [2015-0324-1420-39164] get OLE component of current program
if($sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && ($IntExt==0 || $IntExt=="")){
		
	// Get event ELE code
	$programELE = $objOLEProgram->getELE();
	
	// Get RecordID of R&D and A&A 
	$sql = "SELECT RecordID FROM {$eclass_db}.OLE_ELE where EngTitle='Responsibilities &amp; Duties'";
	$rows = $LibPortfolio->returnVector($sql);
	$ELE_RD = "[".$rows[0]."]";
	$sql = "SELECT RecordID FROM {$eclass_db}.OLE_ELE where EngTitle='Awards &amp; Achievements'";
	$rows = $LibPortfolio->returnVector($sql);
	$ELE_AA = "[".$rows[0]."]";
	
	$eventType = 0;
	$eventType = $programELE==$ELE_RD? 1 : $eventType;
	$eventType = $programELE==$ELE_AA? 2 : $eventType;
}

########################################################
# Tab Menu : Start
########################################################
### Title ###
$TabIndex = empty($IntExt) ? IPF_CFG_SLP_MGMT_OLE : IPF_CFG_SLP_MGMT_OUTSIDE;

if($fromAction == "approveStudentRecord"){
	$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags(IPF_CFG_SLP_MGMT_APPROVE_RECORD);
}else{
	$TAGS_OBJ = libpf_tabmenu::getSlpMgmtTopTags($TabIndex);
}
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR("Teacher");

########################################################
# Tab Menu : End
########################################################

### Clear previous temp file ###
$fs->folder_remove_recursive($eclass_root."/files/portfolio/ole/tmp/t_".$UserID);
################################################################

###############################################
# template for activity role
$LibWord2 = new libwordtemplates_ipf(4);

# float list arr
$role_array = $LibWord2->getWordList(0);
for ($i=0; $i<sizeof($role_array); $i++) {
	// [2015-0324-1420-39164] filter role list for St.Paul Cust
	if($eventType==0 && ($role_array[$i]!="Participant" && $role_array[$i]!="Competitor")) continue;
	if($eventType==1 && ($role_array[$i]=="Participant" || $role_array[$i]=="Competitor")) continue;
	$RoleListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
}
$jsRoleArr1 = ConvertToJSArray($RoleListArr, "jRoleArr1");

# template
###############################################

###############################################
# template for activity awards
 $LibWordAward = new libwordtemplates_ipf(5);

# float list arr
 $awards_array = $LibWordAward->getWordList(0);
 for ($i=0; $i<sizeof($awards_array); $i++) {
       $AwardsListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($awards_array[$i]));
 }
 print ConvertToJSArray($AwardsListArr, "jAwardsArr1");

# template
###############################################




$STEPS_OBJ[] = array($i_Discipline_System_Select_Student, 0);
$STEPS_OBJ[] = array($i_Discipline_System_Add_Record, 1);


	

if (count($record_id) > 0)
{
	$RecordImplode = implode(",",$record_id);
	$Sql = " SELECT 
					UserID
				FROM
					{$eclass_db}.OLE_STUDENT	
				WHERE
					RecordID IN (".$RecordImplode.")	
			 ";	
	$student = $LibPortfolio->returnVector($Sql);			 
}
$StudentArray = $LibPortfolio->GET_STUDENT_OLE_INFO($EventID, $student);

// [2015-0324-1420-39164] pass $eventType
$ContentHTML = $LibPortfolio->GEN_ASSIGN_OLE_STUDENT_TABLE($EventID, $StudentArray, $IntExt, $doublewidth=true, $eventType);


$linterface->LAYOUT_START();
?>

<?=$jsRoleArr1?>

<script type="text/javascript" src="/templates/2009a/js/ipf_jupas.js"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />


<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<SCRIPT LANGUAGE=Javascript>
function jChangeToUploadStatus(id, r_id, all_file, ole_file, remove_file, remove_item_id, original_file_number)
{
	var obj = document.getElementById("UploadBy_"+id);
	var AttachmentImgPath = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_attachment.gif";
	var AttachmentImg = "<a target='_blank' href='ole_attach.php?StudentID="+id+"&RecordID="+r_id+"&ViewMode=new'><img border='0' src='"+AttachmentImgPath+"' /></a>";
	var UploadLink = "<a href='javascript:jUploadFile(\""+id+"\",\""+r_id+"\");' class='tablelink'><?=$iPort["upload"]?></a>";
	
	//if(r_id != null && r_id != "" && original_file_number > 0)
	//obj.innerHTML = AttachmentImg + "&nbsp;&nbsp;&nbsp;" + UploadLink;
	//else
	//obj.innerHTML = UploadLink;
	
	var file_obj = document.getElementById("UserAttachment_"+id);
	file_obj.value = ole_file;
	
	var remove_file_obj = document.getElementById("UserRemoveAttachment_"+id);
	remove_file_obj.value = remove_file;
	
	var remove_item_obj = document.getElementById("RemoveItemID_"+id);
	remove_item_obj.value = remove_item_id;
	
	var all_file_obj = document.getElementById("AllAttachment_"+id);
	all_file_obj.value = all_file;
}

function jGet_AllAttachment(id)
{
	var obj = document.getElementById("AllAttachment_"+id);
	return obj.value;
}

function jGet_Attachment(id)
{
	var obj = document.getElementById("UserAttachment_"+id);
	return obj.value;
}

function jGet_RemoveAttachment(id)
{
	var obj = document.getElementById("UserRemoveAttachment_"+id);
	return obj.value;
}

function jGet_RemoveItemID(id)
{
	var obj = document.getElementById("RemoveItemID_"+id);
	return obj.value;
}

function jUploadFile(id, r_id)
{
	var EventID = "<?=$EventID?>";
	
	newWindow('ole_upload.php?StudentID='+id+'&EventID='+EventID+'&RecordID='+r_id, 27);
}

function viewCurrentRecord(id)
{
         newWindow('viewcurrent.php?StudentID='+id, 8);
}
function changeCat()
{
         var item_select = document.form1.elements["ItemID"];
         var selectedCatID = document.form1.elements["CatID"].value;
         while (item_select.options.length > 0)
         {
                item_select.options[0] = null;
         }
         if (selectedCatID == '')
         {
             item_select.options[0] = new Option('<?=$gen_i_select?>',0);

<?
$curr_cat_id = "";
$pos = 1;
for ($i=0; $i<sizeof($items); $i++)
{
     list($r_catID, $r_itemID, $r_itemName) = $items[$i];
     $r_itemName = intranet_undo_htmlspecialchars($r_itemName);
     $r_itemName = str_replace('"', '\"', $r_itemName);
     $r_itemName = str_replace("'", "\'", $r_itemName);
     /*
     if (substr($r_itemName, strlen($r_itemName)-1, 1) == "\\")
     {
             $r_itemName = $r_itemName . ' ';
     }
     */

     //$r_itemName = addslashes($r_itemName);
     if ($r_catID != $curr_cat_id)
     {
         $pos = 1;
?>
         }
         else if (selectedCatID == "<?=$r_catID?>")
         {
              item_select.options[0] = new Option('<?=$gen_i_select?>',0);
     <?
         $curr_cat_id = $r_catID;
     }
     ?>
             item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);

<?
}
?>
         }
}

function changeItem(value)
{
         var i;
         var meritType = document.form1.elements["MeritType"];
         var meritNum = document.form1.elements["MeritNum"];
         var conductScore = document.form1.elements["ConductScore"];
         <? if ($ldiscipline->use_sub_score1) { ?>
         var subScore1 = document.form1.elements["SubScore1"];
         <? } ?>
         <?
         if ($type==-1) {
         ?>
         var detention = document.form1.elements["DetentionMinutes"];
         var punishment = document.form1.elements["SpecialPunish"];
<?
         }
for ($i=0; $i<sizeof($items); $i++)
{
     list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
     ?>
     if (value=="<?=$r_itemID?>")
     {
         // Merit Type
         for (i=0; i<meritType.options.length; i++)
         {
              if (meritType.options[i].value=="<?=$r_meritType?>")
              {
                  meritType.selectedIndex = i;
                  break;
              }
         }
         // Merit Num
         for (i=0; i<meritNum.options.length; i++)
         {
              if (meritNum.options[i].value=="<?=$r_meritNum?>")
              {
                  meritNum.selectedIndex = i;
                  break;
              }
         }

         // Conduct Score
         for (i=0; i<conductScore.options.length; i++)
         {
              if (conductScore.options[i].value=="<?=$r_conduct?>")
              {
                  conductScore.selectedIndex = i;
                  break;
              }
         }

         <? if ($ldiscipline->use_sub_score1) { ?>
         // SubScore 1
         for (i=0; i<subScore1.options.length; i++)
         {
              if (subScore1.options[i].value=="<?=$r_subscore1?>")
              {
                  subScore1.selectedIndex = i;
                  break;
              }
         }
         <? } ?>



         <?
         if ($type==-1) {
         ?>
         // Punishment
                 /*
         for (i=0; i<punishment.options.length; i++)
         {
              if (punishment.options[i].value=="<?=$r_punish?>")
              {
                  punishment.selectedIndex = i;
                  break;
              }
         }
                */

         // Detention
         for (i=0; i<detention.options.length; i++)
         {
              if (detention.options[i].value=="<?=$r_detention?>")
              {
                  detention.selectedIndex = i;
                  break;
              }
         }
         <? } ?>
     }
        <?
}
?>
}

function checkForm()
{
}
</SCRIPT>

<script language="javascript">
/*
* General JS function(s)
*/
function jSetValue(val, obj, element_name)
{
	len=obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) 
	{
	    if (obj.elements[i].name==element_name)
	    obj.elements[i].value=val;
	}
}

function jSetSelectedndex(val, obj, element_name)
{
	len=obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) 
	{
	    if (obj.elements[i].name==element_name)
	    obj.elements[i].selectedIndex =val;
	}
}

$(document).ready(function(){

	$('input').keydown(function(e) {
		var inputCoding = (window.event) ? event.which : e.keyCode;
		if (inputCoding == 13) {
			event.preventDefault();
			return false;
		}
	});
	
	if($("#AllAchievement").length > 0){
		// Autocomplete of achievement 
		$("#AllAchievement").autocomplete(
	      "ajax_ole_award_suggestion.php",
	      	{	
	      		onItemSelect: function(li){
	      		}, 
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width: '300'
	  		}
	    );
	}
	
	if($("#AllRole").length > 0){
		// Autocomplete of role 
		$("#AllRole").autocomplete(
	      "ajax_ole_role_suggestion.php",
	      	{
	      		onItemSelect: function(li){
	      		}, 
	  			delay:3,
	  			minChars:1,
	  			matchContains:1,
	  			formatItem: function(row){ return row[0]; },
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width: '300'
	  		<?php if($eventType==0 || $eventType==1) { ?>
	  			,extraParams: { oletargettype : "<?=$eventType?>" }
  			<?php } ?>
	      	}
	    );
	}
	
	<?php if($eventType==2) { ?>
		if($("#AllStudentDetails").length > 0){
			// Autocomplete of "Details / Name of Activities"
			$("#AllStudentDetails").autocomplete(
		    	"ajax_ole_activity_name_suggestion.php",
		      	{
		      		onItemSelect: function(li){
		      			var selected_title = $("#AllStudentDetails").val();
		      			selected_title = selected_title.replace(/<br>/gi, "");
		      			$("#AllStudentDetails").val(selected_title);
		      		},
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			formatItem: function(row){ return row[0]; },
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px',
		  			width: '200'
		  		}
		    );
		}
		
		<?php for($count=0; $count<count($StudentArray); $count++) { ?>
			if($("#StudentDetails_<?=$count?>").length > 0){
				// Autocomplete of "Details / Name of Activities"
				$("#StudentDetails_<?=$count?>").autocomplete(
			    	"ajax_ole_activity_name_suggestion.php",
			      	{
			      		onItemSelect: function(li){
			      			var selected_title = $("#StudentDetails_<?=$count?>").val();
			      			selected_title = selected_title.replace(/<br>/gi, "");
			      			$("#StudentDetails_<?=$count?>").val(selected_title);
			      		},
			  			delay:3,
			  			minChars:1,
			  			matchContains:1,
			  			formatItem: function(row){ return row[0]; },
			  			autoFill:false,
			  			overflow_y: 'auto',
			  			overflow_x: 'hidden',
			  			maxHeight: '200px',
			  			width: '200'
			  		}
			    );
			}
	<?php
		}
	} ?>
});

/* Global */

function formcheck(){
	var negative_num = false;
	var hours = document.getElementsByName('Hours[]');
	
	for (var i = 0; i < hours.length; i++) {
		if (isNaN(hours[i].value)) {
    		negative_num = true;
			alert("<?=$ec_iPortfolio['SLP']['PleaseEnterANumber']?>.");
			break;
		} else if(Number(hours[i].value) < 0){
    		negative_num = true;
    		alert("<?=$Lang['iPortfolio']['PleaseInputPositiveNumber']?>");
			break;
		}
	}
	
	if(negative_num == true){
		return false;
	} else {
		return true;
	}
}
</script>
<?//link to edit the program
//$editURL = " &nbsp;<a href =\"javascript: document.formEditProgram.submit()\">[".$Lang['Btn']['Edit']."]</a>"; //comment on 20110504
$editURL = ""; 
$passBackParameter = "EventID={$EventID}&record_id={$RecordImplode}&IntExt={$IntExt}";
$passBackParameter = base64_encode($passBackParameter);

?>


<!--- Students Listing --->
<!--<table width=80% border=0 cellpadding=2 cellspacing=0>-->
<form name="formEditProgram" ACTION="ole_new.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
        <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
        </td>
</tr>
<!--- form input --->
<tr>
<td>
<br style='clear:both'>
	<div class='this_form' style='width:550'>
	<table class='form_table_v30'>
	<tr>
		<td class='field_title'><?=$ec_iPortfolio['title']?></td>
		<td><?=$programTitle?><?=$editURL?></td>
	</tr>
	<tr>
		<td class='field_title'><?=$ec_iPortfolio['date'] ." / ". $ec_iPortfolio['year_period']?></td>
		<td><?=$programPeriod?></td>
	</tr>
	<tr>
		<td class='field_title'><?=$ec_iPortfolio['category']?></td>
		<td><?=$programCatTitle?></td>
	</tr>
	<tr>
		<td class='field_title'><?=$ec_iPortfolio['sub_category']?></td>
		<td><?=$programSubCatTitle?></td>
	</tr>
	<?= $h_ele?>
	</table>
</td>
</tr>
</table>
<input type ="hidden" name="record_id" value="<?=$EventID?>">
<input type ="hidden" name="IntExt" value="<?=$IntExt?>">
<input type ="hidden" name="passBackParameter" value="<?=$passBackParameter?>">
<input type ="hidden" name="passBackURL" value="/home/portfolio/teacher/management/ole_student_edit2.php">
</form>
<!-- onsubmit="javascript:checkForm();" -->
<br/>
<form name="form1" ACTION="ole_student_update.php" method="POST" onsubmit="return formcheck()">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
        <td align="center"><?=$ContentHTML?></td>
</tr>

<?php if($sys_custom['NgWah_SAS']){ ?>
<tr><td>
	<span class=\"tabletextremark\"> <?=$Lang['iPortfolio']['NgWah_SAS_Settings']['Remarks']?></span>
</td></tr>
<?php } ?>

<tr>
        <td>
        <table width="95%" border="0" cellpadding="2" cellspacing="0" align="center">
        <tr>
			<td colspan="2" class="dotline" height="10" valign="top"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
        <tr>
        	<td align="center">
            <div style="padding-top: 3px">
            <?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
            <?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
            <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();") ?>
            </div>
        	</td>
        </tr>
        </table><br/>
        </td>
</tr>
</table>

<?php
for ($i=0; $i<sizeof($array_students); $i++)
{
     list($id, $name) = $array_students[$i];
?>
<input type="hidden" name="student[]" value="<?=$id?>" />
<?
}
?>
<input type="hidden" name="type" value="<?=$type?>" />
<input type="hidden" name="EventID" value="<?=$EventID?>" />
<input type="hidden" name="IntExt" value="<?=$IntExt?>" />
<input type="hidden" name="Year" value="<?=$Year?>" />
<input type="hidden" name="fromAction" value="<?=$fromAction?>" />

</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
