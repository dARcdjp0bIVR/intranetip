<?php
/*
 * 	Log
 * 	
 * 	Purpose: cancel medal award handling
 *
 *  Date:   2019-07-05 [Cameron]
 *          - change RecordID to RecordRow to avoid casting it to Integer in lib.php [case #J164347]
 *      
 *  Date:   2019-07-05 [Cameron]
 *          - fix: should initialize $successAry
 *          
 * 	Date:	2016-12-19 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

$json['success'] = false;
if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$lpf = new libportfolio();

$RecordRow = $_POST['RecordRow'];
$successAry = array();

if (count($RecordRow)) {
	$insertAry = array();
	$lpf->Start_Trans();
	foreach((array)$RecordRow as $rid) {
		list($userID,$academicYearID) = explode("^~",$rid);
		if ($userID && $academicYearID) {
			$sql = "SELECT UserID FROM ".$eclass_db.".PORTFOLIO_JINYING_AWARD WHERE UserID='".$userID."' AND AcademicYearID='".$academicYearID."'";
			$rs = $lpf->returnResultSet($sql);
			if (count($rs)) {
				$sql = "DELETE FROM ".$eclass_db.".PORTFOLIO_JINYING_AWARD 
						WHERE UserID='".$userID."' 
						AND	AcademicYearID='".$academicYearID."'";
				$successAry[] = $lpf->db_db_query($sql);
			}
		}
	}
	
	if (!in_array(false,(array)$successAry)) {
		$lpf->Commit_Trans();
		$json['success'] = true;
	}
	else {
		$lpf->RollBack_Trans();
	}
	
}

$x = '';
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();
?>