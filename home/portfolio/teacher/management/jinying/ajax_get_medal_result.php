<?php
/*
 * 	Log
 * 	
 * 	Purpose: return medal assignment result
 *
 *  Date:   2020-07-13 [Cameron]
 *          - pass argument NrScopeMet to getStudentWithMedal() [case #J189433]
 *
 *  Date:   2019-12-10 [Cameron]
 *          - add column TargetMetScopeNumber
 *
 *  Date:   2019-12-04 [Cameron]
 *          - pass argument $medal to getStudentWithMedal()
 *          - assign medal button is shown based on medal filter selection
 *
 *  Date:   2019-07-05 [Cameron]
 *          - change RecordID to RecordRow to avoid casting it to Integer in lib.php [case #J164347]
 *      
 *	Date:	2017-10-24 [Cameron]
 *			- add Export function
 *
 *	Date:	2017-01-13 [Cameron]
 *			- set ClassLevel to comma separated string
 *
 * 	Date:	2016-12-14 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

$json['success'] = false;
if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
if (!$AcademicYearID) {
	return '';	
}
$ClassLevel = $_POST['ClassLevel'];
if (!$ClassLevel) {
	return '';	
}

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);
$ljy->setClassLevel(implode(',',(array)$ClassLevel));
$ljy->setAwarded($Awarded);
$ljy->setNotAwarded($NotAwarded);

$filter['BSQ_GP'] = $_POST['BSQ_GP'];
$filter['OFS_GP'] = $_POST['OFS_GP'];
$filter['BTP_GP'] = $_POST['BTP_GP'];
$filter['BSQ_TM'] = $_POST['BSQ_TM'];
$filter['EPW_TM'] = $_POST['EPW_TM'];
$filter['BTP_TM'] = $_POST['BTP_TM'];
$medal = $_POST['PresetMedalCriteria'];
$nrScopeMet = $_POST['NrScopeMet'];
$rs = $ljy->getStudentWithMedal($filter, $medal, $nrScopeMet);

$x = '
<style>
	.align_center {text-align:center !important;}
</style>
<form name="form2" id="form2" method="POST">
	<table width="98%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">
		<tr>
			<th class="sub_row_top align_center" rowspan="2">#</th>
			<th class="sub_row_top align_center" rowspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['ClassName'].'</th>
			<th class="sub_row_top align_center" rowspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['ClassNumber'].'</th>
			<th class="sub_row_top align_center" rowspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['StudentName'].'</th>';
			
	foreach((array)$Lang['iPortfolio']['JinYing']['Scope'] as $scope=>$item) {
		$x .= '<th class="sub_row_top align_center"'.(($scope == 'BSQ' || $scope == 'BTP') ? ' colspan="2"' : '').'>'.$item.'</th>';
	}
	$x .= '	<th class="sub_row_top align_center" rowspan="2">'.$Lang['iPortfolio']['JinYing']['Medal']['TargetMetScopeNumber'].'</th>';
    $x .= '	<th class="sub_row_top align_center" rowspan="2">'.$Lang['iPortfolio']['JinYing']['Medal']['Award'].'</th>';
	$x .= '	<th width="1" rowspan="2">
				<input type=checkbox name="checkmaster" id="checkmaster" onClick=(this.checked)?setChecked(1,this.form,"RecordRow[]"):setChecked(0,this.form,"RecordRow[]")>		
			</th>		
		</tr>
		<tr>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'].'</th>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['TargetMet'].'</th>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['TargetMet'].'</th>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'].'</th>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint'].'</th>
			<th class="sub_row_top align_center">'.$Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood'].'</th>
		</tr>';
	
	$i = 0;
	foreach((array)$rs as $r=>$v) {
		++$i;
		$x .= '	<tr '.($v['Medal'] ? 'class="green"' : '').'><td class="align_center">'.$i.'</td>
					<td class="align_center">'.$v['ClassName'].'</td>
					<td class="align_center">'.$v['ClassNumber'].'</td>
					<td class="align_center">'.$v['StudentName'].'</td>
					<td class="align_center">'.$v['GoodPoint_BSQ'].'</td>
					<td class="align_center">'.$v['TargetMet_BSQ'].'</td>
					<td class="align_center">'.$v['TargetMet_EPW'].'</td>
					<td class="align_center">'.$v['GoodPoint_OFS'].'</td>
					<td class="align_center">'.$v['GoodPoint_BTP'].'</td>
					<td class="align_center">'.$v['TargetMet_BTP'].'</td>					
					<td class="align_center">'.$v['TargetMetScopeNumber'].'</td>
					<td class="align_center medal">'.($v['Medal'] ? $Lang['iPortfolio']['JinYing']['Medal'][$v['Medal']] : '-').'</td>
					<td class="align_center">'.$v['CheckBox'].'</td>
				</tr>';
	}
	$x .= '</table>';

	$linterface = new interface_html();
	$x .= '
		<div class="edit_bottom_v30">
			<p class="spacer"></p>';
	switch ($medal){
        case 'Gold':
            $x .= $linterface->GET_ACTION_BTN($Lang['iPortfolio']['JinYing']['Medal']['AssignGold'], "button", "AssignMedalAward('Gold')", "assign_gold").'&nbsp;&nbsp;';
            break;
        case 'Silver':
            $x .= $linterface->GET_ACTION_BTN($Lang['iPortfolio']['JinYing']['Medal']['AssignSilver'], "button", "AssignMedalAward('Silver')", "assign_silver").'&nbsp;&nbsp;';
            break;
        case 'Bronze':
            $x .= $linterface->GET_ACTION_BTN($Lang['iPortfolio']['JinYing']['Medal']['AssignBronze'], "button", "AssignMedalAward('Bronze')", "assign_bronze").'&nbsp;&nbsp;';
            break;
    }
    $x .= $linterface->GET_ACTION_BTN($Lang['iPortfolio']['JinYing']['Medal']['CancelAward'], "button", "CancelMedalAward()", "cancel_award").'&nbsp;&nbsp;'.
			$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "Export()", "export_award").'
			<input type="hidden" name="MedalType" id="MedalType" value="">
			<p class="spacer"></p>
		</div>

</form>';

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();
?>