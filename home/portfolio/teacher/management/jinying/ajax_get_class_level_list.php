<?php
/*
 * 	Log
 * 	
 * 	Purpose: return class level list according to selected academic year
 *
 * 	Date:	2017-10-25 [Cameron]
 * 			- add check all class level check box
 * 			- add label to enclose checkbox for easy select
 * 
 * 	Date:	2017-01-13 [Cameron]
 * 			- show class level in checkbox so that user can select multiple class levels
 * 
 * 	Date:	2016-12-14 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/json.php");
iportfolio_auth("T");	// teacher
intranet_opendb();

$json['success'] = false;
if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
if (!$AcademicYearID) {
	return '';	
}

$lfcm = new form_class_manage();
$formList = $lfcm->Get_Form_List(true,1,$AcademicYearID);
//$x = getSelectByArray($formList, 'name="ClassLevel" id="ClassLevel"', '', 'all');
$x = '<label><input type="checkbox" name="AllClassLevel" id="AllClassLevel" value="1" onClick="CheckAll(this)">'.$Lang['Btn']['SelectAll'].'</label><br>';
foreach((array)$formList as $k=>$v) {
	$x .='<label><input type="checkbox" name="ClassLevel[]" id="ClassLevel[]" value="'.$v['YearID'].'">'.$v['YearName'].'</label><br>';
}

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

$jsonObj = new JSON_obj();
echo $jsonObj->encode($json);

intranet_closedb();
?>