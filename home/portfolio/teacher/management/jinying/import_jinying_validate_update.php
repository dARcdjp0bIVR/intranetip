<?php
/*
 * Log
 *
 * Purpose: validate and import jinying scheme data to temp table for update purpose
 *
 * Date: 2019-12-02 [Cameron]
 * add 'EPW-PW', 'EPW-OC', 'BTP-TP', 'BTP-IS' [case #J161484]
 *
 * Date: 2018-04-23 [Cameron]
 * fix bug: disable submit button after submit to prevent input duplicate record
 *
 * Date: 2017-09-13 [Cameron]
 * add two fields: OrganizationNameEng & OrganizationNameChi
 *
 * Date: 2017-08-28 [Cameron]
 * change RecordID position ($recordIDPos) because of adding WebSAMS after class number in excel
 *
 * Date: 2017-05-04 [Cameron]
 * change to use PHPExcel/IOFactory.php to read excel file as simplexlsx.class.php cannot read xlsx file
 * created by PHPExcel without process (save) by Microsoft Excel
 *
 * Date: 2017-04-28 [Cameron]
 * create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once ($PATH_WRT_ROOT . "home/portfolio/teacher/management/jinying/import_common.php");
include_once ($PATH_WRT_ROOT . "includes/phpxls/Classes/PHPExcel/IOFactory.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    header("Location: /home/portfolio/school_records.php");
    exit();
}

$lpf = new libportfolio();
if (! $lpf->IS_IPF_SUPERADMIN() && ! $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) { // only allow iPortfolio admin to access
    header("Location: /home/portfolio/");
    exit();
}

$Item = $_POST['Item']; // ItemCode
if (! $Item) {
    header("Location: import_jinying.php?returnMsgKey=MissingItem");
    exit();
}

// get preset excel column header
$ljy = new libJinYing();
$presetHeader = $ljy->getPresetHeader($Item, '');

// get excel data
$fileName = $_FILES['userfile']['tmp_name'];

try {
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
} catch (Exception $e) {
    die($Lang['iPortfolio']['JinYing']['ImportError']['LoadingFile'] . pathinfo($fileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

// Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

if ((PHPExcel_Cell::columnIndexFromString($highestColumn) > 1) && $sheet->getCell($highestColumn . '1')->getValue() == '') {
    $highestColumn = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($highestColumn) - 2); // stringFromColumnIndex is zero based
}

$data = array();
// Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row ++) {
    // Read a row of data into an array
    $row_data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    $row_data = current($row_data);
    $data[] = $row_data;
}

if (count($data) <= 1) {
    header("Location: import_jinying.php?returnMsgKey=CSVFileNoData");
    exit();
}
// check if header column match
$header = array_shift($data); // remove header
for ($i = 0, $iMax = count($header); $i < $iMax; $i ++) {
    if ($header[$i] != $presetHeader[$i]) {
        header("Location: import_jinying.php?returnMsgKey=WrongExcelHeader");
        exit();
    }
}

// remove(ignore) empty row
foreach ((array) $data as $k => $v) {
    $row_empty = true;
    foreach ((array) $v as $cv) {
        if ($cv) {
            $row_empty = false;
            continue;
        }
    }
    if ($row_empty) {
        unset($data[$k]);
    }
}
sort($data);

// ################################################
// process data
// ## delete old temp data
$temp_table = $eclass_db . ".PORTFOLIO_JINYING_SCHEME_IMPORT_UPDATE";
$table_name = $eclass_db . '.PORTFOLIO_JINYING_SCHEME';
$sql = "Delete From " . $temp_table . " Where BatchID = '" . $_SESSION['UserID'] . "'";
$successAry['deleteOldTempData'] = $lpf->db_db_query($sql);

$numOfData = count($data);
$errorMsgAssoAry = array();
$insertAry = array();
switch ($Item) {
    case 'BSQ-HW':
    case 'BSQ-PC':
    case 'BSQ-AD':
    case 'BSQ-AI':
    case 'BSQ-RC':
    case 'BSQ-RE':
    case 'EPW-CA':
        $rs = validate_item_4update_standard($data, $Item);
        break;
    
    case 'BSQ-EC':
    case 'BSQ-TA':
    case 'EPW-TA':
        $rs = validate_item_4update_with_activity($data, $Item);
        break;
    
    case 'EPW-PY':
    case 'EPW-PJ':
    case 'EPW-PS':
    case 'EPW-PW':
    case 'EPW-OC':
    case 'BTP-TP':
    case 'BTP-IS':
        $rs = validate_item_4update_standard($data, $Item, $recordIDPos = 7);
        break;

    case 'OFS-SI':
        $rs = validate_OFS_SI_4update($data, $Item);
        break;
    case 'OFS-SO':
        $rs = validate_OFS_SO_4update($data, $Item);
        break;
    
    case 'BTP-EA':
        $rs = validate_BTP_EA_4update($data, $Item);
        break;
    case 'BTP-IC':
        $rs = validate_BTP_IC_4update($data, $Item);
        break;
} // end switch

if ($rs) {
    $errorMsgAssoAry = $rs[0];
    $insertAry = $rs[1];
}
// ################################################

// ## simple statistics
$numOfErrorRow = count($errorMsgAssoAry);
$numOfSuccessRow = $numOfData - $numOfErrorRow;

// ## insert excel data to temp table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
    $insertChunkAry = array_chunk($insertAry, 1000);
    $numOfChunk = count($insertChunkAry);
    
    for ($i = 0; $i < $numOfChunk; $i ++) {
        $_insertAry = $insertChunkAry[$i];
        
        $sql = "Insert Into $temp_table
					(BatchID,RecordID,PerformanceCode,Performance,
					ActivityDate,ActivityNameEng,ActivityNameChi,AwardNameEng,AwardNameChi,RecommendMerit,OrganizationNameEng,OrganizationNameChi,ServiceHours,InputDate,InputBy)
				Values " . implode(', ', (array) $_insertAry);
        $successAry['insertData'][] = $lpf->db_db_query($sql);
    }
}

// validation result to display
if ($numOfErrorRow > 0) {
    $numOfErrorDisplay = '<span class="tabletextrequire">' . $numOfErrorRow . '</span>';
} else {
    $numOfErrorDisplay = 0;
}

list ($scope, $item) = explode('-', $Item);

$x = '';
$x .= '<table class="form_table_v30">' . "\r\n";
$x .= '<tr>' . "\n";
$x .= '<td class="field_title">' . $Lang['iPortfolio']['JinYing']['Item'][$scope][$item] . '</td>' . "\n";
$x .= '<td></td>' . "\n";
$x .= '</tr>' . "\n";

$x .= '<tr>' . "\n";
$x .= '<td class="field_title">' . $Lang['General']['SuccessfulRecord'] . '</td>' . "\n";
$x .= '<td>' . $numOfSuccessRow . '</td>' . "\n";
$x .= '</tr>' . "\n";
$x .= '<tr>' . "\n";
$x .= '<td class="field_title">' . $Lang['General']['FailureRecord'] . '</td>' . "\n";
$x .= '<td>' . $numOfErrorDisplay . '</td>' . "\n";
$x .= '</tr>' . "\n";
$x .= '</table>' . "\r\n";
$htmlAry['importInfoTbl'] = $x;

// ################################################
// error display
$x = '';
if ($numOfErrorRow > 0) {
    switch ($Item) {
        case 'BSQ-HW':
        case 'BSQ-PC':
        case 'BSQ-AD':
        case 'BSQ-AI':
        case 'BSQ-RC':
        case 'BSQ-RE':
        case 'EPW-CA':
            $x .= get_error_table_item_4update_standard($data, $errorMsgAssoAry, $Item);
            break;
        case 'BSQ-EC':
        case 'BSQ-TA':
        case 'EPW-TA':
            $x .= get_error_table_item_4update_with_activity($data, $errorMsgAssoAry, $Item);
            break;
        
        case 'EPW-PY':
        case 'EPW-PJ':
        case 'EPW-PS':
        case 'EPW-PW':
        case 'EPW-OC':
        case 'BTP-TP':
        case 'BTP-IS':
            $x .= get_error_table_item_4update_standard($data, $errorMsgAssoAry, $Item, $recordIDPos = 7);
            break;
        
        case 'OFS-SI':
            $x .= get_error_table_OFS_SI_4update($data, $errorMsgAssoAry, $Item);
            break;
        case 'OFS-SO':
            $x .= get_error_table_OFS_SO_4update($data, $errorMsgAssoAry, $Item);
            break;
        
        case 'BTP-EA':
            $x .= get_error_table_BTP_EA_4update($data, $errorMsgAssoAry, $Item);
            break;
        case 'BTP-IC':
            $x .= get_error_table_BTP_IC_4update($data, $errorMsgAssoAry, $Item);
            break;
    } // switch
}
$htmlAry['errorTbl'] = $x;
// ################################################

// Page heading setting
$linterface = new interface_html();

// tag information
$CurrentPage = "Teacher_JinYing_Scheme";

$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

// page navigation (leave the array empty if no need)
$PAGE_NAVIGATION = $ljy->getImportPageNavigation();
$PAGE_NAVIGATION[] = array(
    $Lang['iPortfolio']['JinYing']['Import']['Replace'],
    ""
);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

// handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

// ## step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep = 2, array_values($Lang['iPortfolio']['JinYing']['ImportStepArr']));

// ## action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute = "", ($numOfErrorRow > 0) ? true : false, $ParClass = "", $ParExtraClass = "actionBtn");
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">

function goSubmit() {
	$('#submitBtn').attr('disabled', 'disabled');
	$('form#form1').attr('action', 'import_jinying_confirm_update.php').submit();
}

function goBack() {
	window.location = 'import_jinying.php';
}

function goCancel() {
	self.location = "index.php";
}

</script>
<form id="form1" name="form1" method="POST">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board" style="width: 100%;">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear: both;" />

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>

	<input type="hidden" name="Item" value="<?=$Item?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>