<?php
// using:
/*
 *
 * Date: 2019-12-12 [Cameron]
 * - add function getPerformanceExportTableGoodPassFailNotImport(), getPerformanceExportTableGoodPassNoData()
 * - add case EPW-PW, EPW-OC, BTP-IS, BTP-TP
 *
 * 2018-05-14 [Cameron]
 * - fix typo $space
 *
 * 2018-04-26 [Cameron]
 * - fix swap column of good and pass in getPerformanceExportTableGoodPassFail()
 *
 * 2018-04-19 Cameron
 * - create this file
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', '256M');

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}
$item = $_POST['Item'];
if (! $item) {
    return '';
}
$semester = $_POST['Semester'];

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);
$classLevelAry = $ljy->getAllClassLevel();
if (count($classLevelAry)) {
    $ljy->setClassLevel(implode(',', $classLevelAry)); // set ClassLevel because some functions use it in $ljy, e.g. setGoodPoint_BSQ
}

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID), 0, 4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID), 0, 4);

$stat = $ljy->getMedalStatByItem($item, $semester);

list ($scope, $subItem) = explode('-', $item);

$itemName = str_replace('#', '', $Lang['iPortfolio']['JinYing']['Item'][$scope][$subItem]);
if ($semester) {
    $semesterName = $ljy->getSemesterName($semester);
} else {
    $semesterName = '';
}
$space = $intranet_session_language == 'en' ? ' ' : '';

switch ($item) {
    
    case 'BSQ-HW':
    case 'BSQ-PC':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableGoodPassFail($title, $stat);
        break;
    
    case 'BSQ-AD':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableGoodFail($title, $stat);
        break;
    
    case 'BSQ-AI':
    case 'BSQ-RC':
    case 'BSQ-RE':
    case 'EPW-CA':
    case 'EPW-OC':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTablePassFail($title, $stat);
        break;
    
    case 'EPW-PY':
    case 'EPW-PJ':
    case 'EPW-PS':
    case 'BTP-TP':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTablePassFail($title, $stat);
        break;
    
    case 'BSQ-EC':
    case 'BSQ-TA':
    case 'EPW-TA':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableGoodPassFailNotJoin($title, $stat);
        break;

    case 'EPW-PW':
        $itemName .= '-' . $semesterName;
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableGoodPassFailNotImport($title, $stat);
        break;

    case 'OFS-SI':
    case 'OFS-SO':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableService($title, $stat);
        break;
    
    case 'BTP-EA':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableExtracurricularActivities($title, $stat);
        break;
    
    case 'BTP-IC':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableExternalCompetitions($title, $stat);
        break;

    case 'BTP-IS':
        $title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItem'], $academic_start_year, $academic_end_year, $itemName);
        $result = getPerformanceExportTableGoodPassNoData($title, $stat);
        break;
}

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($result['data'], $result['column'], "\t", "\r\n", "\t", 0, '11');

intranet_closedb();

$filename = $title . "_" . date("Ymd") . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

// ############################ start functions
function getPerformanceExportTableGoodPassFail($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    } else {
        $isNoData = false;
    }
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        if ($isNoData) {
            $tmpRow[] = $v['NumNoData'];
            $tmpRow[] = $v['PercentNoData'];
        }
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableGoodFail($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    } else {
        $isNoData = false;
    }
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        if ($isNoData) {
            $tmpRow[] = $v['NumNoData'];
            $tmpRow[] = $v['PercentNoData'];
        }
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTablePassFail($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    } else {
        $isNoData = false;
    }
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        if ($isNoData) {
            $tmpRow[] = $v['NumNoData'];
            $tmpRow[] = $v['PercentNoData'];
        }
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableGoodPassFailNotJoin($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        $tmpRow[] = $v['NumNoData'];
        $tmpRow[] = $v['PercentNoData'];
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableService($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumJoin'];
        $tmpRow[] = $v['PercentJoin'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumNoData'];
        $tmpRow[] = $v['PercentNoData'];
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableExtracurricularActivities($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        $tmpRow[] = $v['NumNoData'];
        $tmpRow[] = $v['PercentNoData'];
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableExternalCompetitions($title, $stat)
{
    global $Lang, $space;
    
    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        $tmpRow[] = $v['NumJoin'];
        $tmpRow[] = $v['PercentJoin'];
        $tmpRow[] = $v['NumNoMerit'];
        $tmpRow[] = $v['PercentNoMerit'];
        $tmpRow[] = $v['NumMerit'];
        $tmpRow[] = $v['PercentMerit'];
        $tmpRow[] = $v['NumNoData'];
        $tmpRow[] = $v['PercentNoData'];
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }
    
    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableGoodPassFailNotImport($title, $stat)
{
    global $Lang, $space;

    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];

    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        $tmpRow[] = $v['NumNoData'];
        $tmpRow[] = $v['PercentNoData'];
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }

    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

function getPerformanceExportTableGoodPassNoData($title, $stat)
{
    global $Lang, $space;

    $exportColumn = array();
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    } else {
        $isNoData = false;
    }

    $result_data = array();
    foreach ((array) $stat as $r => $v) {
        $tmpRow = array();
        $tmpRow[] = $v['Name'];
        $tmpRow[] = $v['NumStudent'];
        $tmpRow[] = $v['NumGood'];
        $tmpRow[] = $v['PercentGood'];
        $tmpRow[] = $v['NumPass'];
        $tmpRow[] = $v['PercentPass'];
        $tmpRow[] = $v['NumFail'];
        $tmpRow[] = $v['PercentFail'];
        if ($isNoData) {
            $tmpRow[] = $v['NumNoData'];
            $tmpRow[] = $v['PercentNoData'];
        }
        $result_data[] = $tmpRow;
        unset($tmpRow);
    }

    return array(
        'column' => $exportColumn,
        'data' => $result_data
    );
}

// ############################ end functions

?>