<?php

// Modified by 
/*
 *  2019-12-03 [Cameron]
 *      - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 *
 * 	2016-11-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/");
	exit;
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if(!$lpf->IS_IPF_SUPERADMIN() && !$_SESSION['SSV_USER_ACCESS']['other-iPortfolio']){	// only allow iPortfolio admin to access
	header("Location: /home/portfolio/");
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'] ? $_REQUEST['AcademicYearID'] : Get_Current_Academic_Year_ID();
$currentYearTerm = getCurrentAcademicYearAndYearTerm();
$Semester = $_REQUEST['Semester'] ? $_REQUEST['Semester'] : $currentYearTerm['YearTermID'];
 
# Acadermic Year Selection        	
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="changeAcademicYear();"', 1, 0, $AcademicYearID);

# Acadermic Term Selection
$semesterCss = in_array($Item, (array)$conf['JinYingItemCodeWithTerm']) ? '' : 'none';
$semesterFilter = getSelectSemester2('name="Semester" id="Semester" style="display:'.$semesterCss.'" onChange="changeAcademicTerm();"',$Semester, $ParQuoteValue=1, $AcademicYearID);

# Build Item List
$itemList_array = array();
$itemList_array[] = array('',$Lang['iPortfolio']['JinYing']['SelectItem']); 
foreach($Lang['iPortfolio']['JinYing']['Item'] as $scope=>$item) {
	$itemList_array[] = array('OPTGROUP',$Lang['iPortfolio']['JinYing']['Scope'][$scope]);	// (value, name)
	foreach($item as $k=>$v) {
		$code = "$scope-$k";
		$itemList_array[] = array($code,$v);	
	}
}
$itemFilter = returnSelection($itemList_array, $Item, 'Item', 'ID="Item" onChange="changeItem();"');


# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";
$CurrentPageName = $Lang['iPortfolio']['JinYing']['JinYingScheme'];
### Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$PAGE_NAVIGATION = $ljy->getDeletePageNavigation();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
	.hideBottomBorder{border-bottom: none !important;}
	.align_center {text-align:center !important;}
</style>

<script src="/templates/jquery/jquery-1.12.1.min.js"></script>

<script language="JavaScript">

$(document).ready(function(){
	
	$(document).on("change","#FormID\\[\\]", function(){
		$(this).parent().parent().find('input[data-class="'+$(this).val()+'"]').prop('checked',$(this).prop('checked'));
	});
	
	getFormClassListWithNumRec();
	
});

function changeAcademicYear() {
	getFormClassListWithNumRec();
}

function changeAcademicTerm() {
	getFormClassListWithNumRec();	
}

function changeItem() {
    <?php echo "var codeWithTermAry = ['".implode("','",$conf['JinYingItemCodeWithTerm'])."'];"; ?>

    var currentItem = $('#Item').val();
    if (codeWithTermAry.indexOf(currentItem) != -1) {
        $('#Semester').css("display","");
        $('#SemesterRow').css("display","");
    }
    else {
        $('#Semester').css("display","none");
        $('#SemesterRow').css("display","none");
    }
	getFormClassListWithNumRec();
}

function getFormClassListWithNumRec() {
  	$('#form_class_table').html('');
  	$('#DeleteBtn').css("display","none");
  	if ($('#Item').val() != '') {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_numrec_by_item.php',
			data : $("#form1").serialize(),		  
			success: load_form_class_layout,
			error: show_ajax_error
		});
  	}
	
}

function checkRemove(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
        if(confirm(alertConfirmRemove)){
	        obj.action=page;
	        obj.method="post";
	        obj.submit();
        }
    }
}

function load_form_class_layout(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#form_class_table').html(ajaxReturn.html);
		$('#semester_filter').html(ajaxReturn.semesterFilter);
	}
}	

function show_ajax_error() {
	alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
}


</script>

<form name="form1" id="form1" method="POST">
	<div class="content_top_tool">
		<div><?=$htmlAry['navigation']?></div>
		<br style="clear:both" />
	</div>

	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
				<td><?=$yearFilter?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['ActivityItems']?></td>
				<td><?=$itemFilter?></td>
			</tr>
			<tr id="SemesterRow" style="display:<?=$semesterCss?>">
				<td class="field_title"><?=$Lang['General']['Term']?></td>
				<td id="semester_filter"><?=$semesterFilter?></td>
			</tr>
		</table>	
	</div>

	<div id="form_class_table">
	
	</div>

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>