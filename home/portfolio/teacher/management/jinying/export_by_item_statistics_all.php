<?php
// using:
/*
 * Date: 2019-12-12 [Cameron]
 * - add function getPerformanceExportAllGoodPassFailNotImport(), getPerformanceExportAllGoodPassNoData()
 * - add case EPW-PW, EPW-OC, BTP-IS, BTP-TP
 *
 * 2018-05-14 Cameron
 * - create this file
 */
@SET_TIME_LIMIT(216000);
ini_set('memory_limit', '256M');

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/config.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'];
if (! $AcademicYearID) {
    return '';
}

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);
$classLevelAry = $ljy->getAllClassLevel();
if (count($classLevelAry)) {
    $ljy->setClassLevel(implode(',', $classLevelAry)); // set ClassLevel because some functions use it in $ljy, e.g. setGoodPoint_BSQ
}

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID), 0, 4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID), 0, 4);

$semesterAry = getSemesters($AcademicYearID, $ReturnAsso=1);


$space = $intranet_session_language == 'en' ? ' ' : '';
$exportColumn = array();    // column title
$result_data = array();     // export data

// column title
$exportColumn[0][0] = ''; 
$exportColumn[0][1] = '';
$exportColumn[1][0] = '';
$exportColumn[1][1] = '';
$exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
$exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];


$itemAry = array();
foreach($Lang['iPortfolio']['JinYing']['Item'] as $_scopeCode => $_scope) {
    foreach($_scope as $_itemCode => $_itemName) {
        $itemAry["{$_scopeCode}-{$_itemCode}"] = str_replace('#', '', $_itemName); 
    }
}

foreach($itemAry as $_itemCode => $_itemName) {
    switch ($_itemCode) {
    
        case 'BSQ-HW':
        case 'BSQ-PC':
            $i = 0;
            foreach($semesterAry as $_semesterID => $_semesterName) {
                $stat = $ljy->getMedalStatByItem($_itemCode, $_semesterID);
                if (($_itemCode == 'BSQ-HW') && ($i == 0)) {
                    $showClassLevel = true;
                }
                else {
                    $showClassLevel = false;
                }
                getPerformanceExportAllGoodPassFail($stat, $exportColumn, $result_data, $_itemName, $_semesterName, $showClassLevel);
                $i++;
            }
            break;
            
        case 'BSQ-AD':
            foreach($semesterAry as $_semesterID => $_semesterName) {
                $stat = $ljy->getMedalStatByItem($_itemCode, $_semesterID);
                getPerformanceExportAllGoodFail($stat, $exportColumn, $result_data, $_itemName, $_semesterName);
            }
            break;
            
        case 'BSQ-AI':
        case 'BSQ-RC':
        case 'BSQ-RE':
        case 'EPW-CA':
        case 'EPW-OC':
            foreach($semesterAry as $_semesterID => $_semesterName) {
                $stat = $ljy->getMedalStatByItem($_itemCode, $_semesterID);
                getPerformanceExportAllPassFail($stat, $exportColumn, $result_data, $_itemName, $_semesterName);
            }
            break;
            
        case 'EPW-PY':
        case 'EPW-PJ':
        case 'EPW-PS':
        case 'BTP-TP':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllPassFail($stat, $exportColumn, $result_data, $_itemName, $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear']);
            break;
            
        case 'BSQ-EC':
        case 'BSQ-TA':
        case 'EPW-TA':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllGoodPassFailNotJoin($stat, $exportColumn, $result_data, $_itemName);
            break;

        case 'EPW-PW':
            foreach($semesterAry as $_semesterID => $_semesterName) {
                $stat = $ljy->getMedalStatByItem($_itemCode, $_semesterID);
                getPerformanceExportAllGoodPassFailNotImport($stat, $exportColumn, $result_data, $_itemName, $_semesterName);
            }
            break;

        case 'OFS-SI':
        case 'OFS-SO':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllService($stat, $exportColumn, $result_data, $_itemName);
            break;
            
        case 'BTP-EA':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllExtracurricularActivities($stat, $exportColumn, $result_data, $_itemName);
            break;
            
        case 'BTP-IC':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllExternalCompetitions($stat, $exportColumn, $result_data, $_itemName);
            break;

        case 'BTP-IS':
            $stat = $ljy->getMedalStatByItem($_itemCode);
            getPerformanceExportAllGoodPassNoData($stat, $exportColumn, $result_data, $_itemName);
            break;
            
    }   // end swich
    
}   // end foreach


$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT(array_values($result_data), $exportColumn, "\t", "\r\n", "\t", 0, '11');

intranet_closedb();

$title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByItemAll'], $academic_start_year, $academic_end_year);
$filename = $title . "_" . date("Ymd") . ".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

// ############################ start functions
function getPerformanceExportAllGoodPassFail($stat, &$exportColumn, &$result_data, $title, $semesterName, $showClassLevel=false)
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title; 
    $exportColumn[1][] = $semesterName;
    for ($i=0; $i<5; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['MeritExcludeTargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
        for ($i=0; $i<2; $i++) {
            $exportColumn[0][] = '';
            $exportColumn[1][] = '';
        }
    } else {
        $isNoData = false;
    }
    
    foreach ((array) $stat as $r => $v) {
        if ($showClassLevel) {
            $result_data[$r][] = $v['Name'];
            $result_data[$r][] = $v['NumStudent'];
        }
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        if ($isNoData) {
            $result_data[$r][] = $v['NumNoData'];
            $result_data[$r][] = $v['PercentNoData'];
        }
    }
}

function getPerformanceExportAllGoodFail($stat, &$exportColumn, &$result_data, $title, $semesterName)
{
    global $Lang, $space;

    $exportColumn[0][] = $title;
    $exportColumn[1][] = $semesterName;
    for ($i=0; $i<3; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
        for ($i=0; $i<2; $i++) {
            $exportColumn[0][] = '';
            $exportColumn[1][] = '';
        }
    } else {
        $isNoData = false;
    }
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        if ($isNoData) {
            $result_data[$r][] = $v['NumNoData'];
            $result_data[$r][] = $v['PercentNoData'];
        }
    }
}

function getPerformanceExportAllPassFail($stat, &$exportColumn, &$result_data, $title, $semesterName='')
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title;
    $exportColumn[1][] = $semesterName;
    for ($i=0; $i<3; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
        for ($i=0; $i<2; $i++) {
            $exportColumn[0][] = '';
            $exportColumn[1][] = '';
        }
    } else {
        $isNoData = false;
    }
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        if ($isNoData) {
            $result_data[$r][] = $v['NumNoData'];
            $result_data[$r][] = $v['PercentNoData'];
        }
    }
}

function getPerformanceExportAllGoodPassFailNotJoin($stat, &$exportColumn, &$result_data, $title)
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title;
    $exportColumn[1][] = $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'];
    for ($i=0; $i<7; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        $result_data[$r][] = $v['NumNoData'];
        $result_data[$r][] = $v['PercentNoData'];
    }
}

function getPerformanceExportAllService($stat, &$exportColumn, &$result_data, $title)
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title;
    $exportColumn[1][] = $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'];
    for ($i=0; $i<7; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Participation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['Merit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoMeritWithParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipation'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumJoin'];
        $result_data[$r][] = $v['PercentJoin'];
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumNoData'];
        $result_data[$r][] = $v['PercentNoData'];
    }
}

function getPerformanceExportAllExtracurricularActivities($stat, &$exportColumn, &$result_data, $title)
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title;
    $exportColumn[1][] = $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'];
    for ($i=0; $i<7; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEA'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        $result_data[$r][] = $v['NumNoData'];
        $result_data[$r][] = $v['PercentNoData'];
    }
}

function getPerformanceExportAllExternalCompetitions($stat, &$exportColumn, &$result_data, $title)
{
    global $Lang, $space;
    
    $exportColumn[0][] = $title;
    $exportColumn[1][] = $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'];
    for ($i=0; $i<13; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMetWithParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['ParticipationWithoutPrize'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithoutMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['PrizeWithMerit'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoParticipationEC'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    
    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        $result_data[$r][] = $v['NumJoin'];
        $result_data[$r][] = $v['PercentJoin'];
        $result_data[$r][] = $v['NumNoMerit'];
        $result_data[$r][] = $v['PercentNoMerit'];
        $result_data[$r][] = $v['NumMerit'];
        $result_data[$r][] = $v['PercentMerit'];
        $result_data[$r][] = $v['NumNoData'];
        $result_data[$r][] = $v['PercentNoData'];
    }
}

function getPerformanceExportAllGoodPassFailNotImport($stat, &$exportColumn, &$result_data, $title, $semesterName)
{
    global $Lang, $space;

    $exportColumn[0][] = $title;
    $exportColumn[1][] = $semesterName;
    for ($i=0; $i<5; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetNotMet'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    if ($stat['junior']['NumNoData'] > 0 || $stat['high']['NumNoData'] > 0) {
        $isNoData = true; // whether there's student that hasn't imported data or not
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
        $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
        for ($i=0; $i<2; $i++) {
            $exportColumn[0][] = '';
            $exportColumn[1][] = '';
        }
    } else {
        $isNoData = false;
    }

    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        if ($isNoData) {
            $result_data[$r][] = $v['NumNoData'];
            $result_data[$r][] = $v['PercentNoData'];
        }
    }
}


function getPerformanceExportAllGoodPassNoData($stat, &$exportColumn, &$result_data, $title)
{
    global $Lang, $space;

    $exportColumn[0][] = $title;
    $exportColumn[1][] = $Lang['iPortfolio']['JinYing']['Statistics']['WholeYear'];
    for ($i=0; $i<7; $i++) {
        $exportColumn[0][] = '';
        $exportColumn[1][] = '';
    }

    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetWithGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['TargetMetExcludeGoodPerformance'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Count'];
    $exportColumn[2][] = $Lang['iPortfolio']['JinYing']['Statistics']['ByItem']['NoData'] . $space . $Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];

    foreach ((array) $stat as $r => $v) {
        $result_data[$r][] = $v['NumGood'];
        $result_data[$r][] = $v['PercentGood'];
        $result_data[$r][] = $v['NumPass'];
        $result_data[$r][] = $v['PercentPass'];
        $result_data[$r][] = $v['NumFail'];
        $result_data[$r][] = $v['PercentFail'];
        $result_data[$r][] = $v['NumNoData'];
        $result_data[$r][] = $v['PercentNoData'];
    }
}
// ############################ end functions

?>