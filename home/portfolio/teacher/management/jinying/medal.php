<?php

// Modified by
/*
 * 2020-11-10 [Cameron]
 *  - use $Lang['iPortfolio']['JinYing']['Report']['Index'] in item [case #J200351]
 *
 * 2020-07-13 [Cameron]
 * - change number of scope met from label to selection list [case #J189433]
 *
 * 2019-12-04 [Cameron]
 * - revise filter medal layout to meet the new requirement [case #J161484]
 *
 * 2019-07-05 [Cameron]
 * - change RecordID to RecordRow to avoid casting it to Integer in lib.php [case #J164347]
 *      
 * 2018-04-17 [Cameron]
 * - add mandatory remark
 * - use config setting for medal award requirement
 *
 * 2017-10-25 [Cameron]
 * - add check all class level check box
 * - add label to enclose checkbox for easy select
 *
 * 2017-10-24 [Cameron]
 * - add export function
 *
 * 2017-02-14 [Cameron]
 * - add preset medal award criteria
 *
 * 2017-01-13 [Cameron]
 * - show class level in checkbox so that user can select multiple class levels
 *
 * 2016-12-14 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T"); // teacher
intranet_opendb();

if (! $sys_custom['iPf']['JinYingScheme']) {
    header("Location: /home/portfolio/");
    exit();
}

$lpf = new libportfolio();
$lfcm = new form_class_manage();

if (! $lpf->IS_IPF_SUPERADMIN() && ! $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) { // only allow iPortfolio admin to access
    header("Location: /home/portfolio/");
    exit();
}

$AcademicYearID = $_POST['AcademicYearID'] ? $_POST['AcademicYearID'] : Get_Current_Academic_Year_ID();

// Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);

// Class Level (Form) selection
$formList = $lfcm->Get_Form_List(true, 1, $AcademicYearID);
// $formFilter = getSelectByArray($formList, 'name="ClassLevel" id="ClassLevel"', '', 'all');
$formFilter = '<label><input type="checkbox" name="AllClassLevel" id="AllClassLevel" value="1" onClick="CheckAll(this)">' . $Lang['Btn']['SelectAll'] . '</label><br>';
foreach ((array) $formList as $k => $v) {
    $formFilter .= '<label><input type="checkbox" name="ClassLevel[]" id="ClassLevel[]" value="' . $v['YearID'] . '">' . $v['YearName'] . '</label><br>';
}

// Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_JinYing_Scheme";

// ## Title ###
$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('Medal');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<style>
.green {
	color: #00b33c;
}
</style>

<script type="text/JavaScript" language="JavaScript">
	function hideOptionLayer()
	{
		$('#form1').attr('style', 'display: none');
		$('.spanHideOption').attr('style', 'display: none');
		$('.spanShowOption').attr('style', '');
	}
	
	function showOptionLayer()
	{
		$('#form1').attr('style', '');
		$('.spanShowOption').attr('style', 'display: none');
		$('.spanHideOption').attr('style', '');
	}

	$(document).ready(function(){
		$('#AcademicYearID').change(function(e){
			e.preventDefault();
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_get_class_level_list.php',
				data : 'AcademicYearID='+$(this).val(),		  
				success: load_classlevel_layout,
				error: show_ajax_error
			});
		});
		
		$('#submit_result').click(function(){
			
			if ($('#ClassLevel\\[\\]:checked').length == 0) {
				alert("<?=$Lang['iPortfolio']['JinYing']['MedalError']['EmptyClassLevel']?>");
				return;					
			}
			
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_get_medal_result.php',
				data : $("#form1").serialize(),		  
				success: load_medal_result_layout,
				error: show_ajax_error
			});
		});

		$('#PresetMedalCriteria').change(function(e){
			SetMedalAwardCriteria();
		});

	});
	
	function load_classlevel_layout(ajaxReturn) {
		if (ajaxReturn != null && ajaxReturn.success){
		  	$('#FormListFilter').html(ajaxReturn.html);
		}
	}	

	function load_medal_result_layout(ajaxReturn) {
		if (ajaxReturn != null && ajaxReturn.success){
			$('#form1').attr('style','display: none');
			$('#form_result').html(ajaxReturn.html);
			$('#spanShowOption').show();
			$('#spanHideOption').hide();
			$('#report_show_option').addClass("report_option report_hide_option");
			window.scrollTo(0, 0);
		}
	}	
	
	function show_ajax_error() {
		alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
	}
	
	function AssignMedalAward(medal){
		var msg;
		if ($('#RecordRow\\[\\]:checked').length == 0) {
			alert(globalAlertMsg2);
			return;					
		}
		switch(medal) {
			case 'Bronze':
				msg = '<?=$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignBronze']?>';
				$('#MedalType').val('Bronze');
				break;
			case 'Gold':
				msg = '<?=$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignGold']?>';
				$('#MedalType').val('Gold');
				break;
			case 'Silver':
				msg = '<?=$Lang['iPortfolio']['JinYing']['Medal']['ConfirmAssignSilver']?>';
				$('#MedalType').val('Silver');
				break;
		}
        if(confirm(msg)){
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_assign_medal_award.php',
				data : $("#form2").serialize(),		  
				success: show_medal_result,
				error: show_ajax_error
			});
        }
	}

	function CancelMedalAward() {
		var msg;
		if ($('#RecordRow\\[\\]:checked').length == 0) {
			alert(globalAlertMsg2);
			return;					
		}
		msg = '<?=$Lang['iPortfolio']['JinYing']['Medal']['ConfirmCancelAward']?>';
        if(confirm(msg)){
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_cancel_medal_award.php',
				data : $("#form2").serialize(),		  
				success: show_cancel_medal_result,
				error: show_ajax_error
			});
        }
	}

	function show_medal_result(ajaxReturn) {
		var medal;
		if (ajaxReturn != null && ajaxReturn.success){
			switch($('#MedalType').val()) {
				case 'Bronze':
					medal = '<?=$Lang['iPortfolio']['JinYing']['Medal']['Bronze']?>';
					break;
				case 'Gold':
					medal = '<?=$Lang['iPortfolio']['JinYing']['Medal']['Gold']?>';
					break;
				case 'Silver':
					medal = '<?=$Lang['iPortfolio']['JinYing']['Medal']['Silver']?>';
					break;
			}
			$('#RecordRow\\[\\]:checked').parent().parent().removeClass('green').addClass('green');
			$('#RecordRow\\[\\]:checked').parent().parent().find('.medal').html(medal);
			Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			$('#RecordRow\\[\\]').attr('checked',false);
			$('#checkmaster').attr('checked',false);
		}
	}	

	function show_cancel_medal_result(ajaxReturn) {
		var medal;
		if (ajaxReturn != null && ajaxReturn.success){
			$('#RecordRow\\[\\]:checked').parent().parent().removeClass('green');
			$('#RecordRow\\[\\]:checked').parent().parent().find('.medal').html('-');
			Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			$('#RecordRow\\[\\]').attr('checked',false);
			$('#checkmaster').attr('checked',false);
		}
	}
	
	function SetMedalAwardCriteria() {
		switch($('#PresetMedalCriteria').val()) {
			case 'Gold':
			    $('#NrScopeMet').val('4');
/*
					$('#BSQ_GP').val('<?php echo $conf['MedalRequirement']['Gold']['BSQ']['GoodPoint'];?>');
					$('#BSQ_TM').val('<?php echo $conf['MedalRequirement']['Gold']['BSQ']['TargetMet'];?>');
					$('#EPW_TM').val('<?php echo $conf['MedalRequirement']['Gold']['EPW']['TargetMet'];?>');
					$('#OFS_GP').val('<?php echo $conf['MedalRequirement']['Gold']['OFS']['GoodPoint'];?>');
					$('#BTP_TM').val('<?php echo $conf['MedalRequirement']['Gold']['BTP']['TargetMet'];?>');
					$('#BTP_GP').val('<?php echo $conf['MedalRequirement']['Gold']['BTP']['GoodPoint'];?>');
*/
				break;
			case 'Silver':
                $('#NrScopeMet').val('3');
/*
					$('#BSQ_GP').val('<?php echo $conf['MedalRequirement']['Silver']['BSQ']['GoodPoint'];?>');
					$('#BSQ_TM').val('<?php echo $conf['MedalRequirement']['Silver']['BSQ']['TargetMet'];?>');
					$('#EPW_TM').val('<?php echo $conf['MedalRequirement']['Silver']['EPW']['TargetMet'];?>');
					$('#OFS_GP').val('<?php echo $conf['MedalRequirement']['Silver']['OFS']['GoodPoint'];?>');
					$('#BTP_TM').val('<?php echo $conf['MedalRequirement']['Silver']['BTP']['TargetMet'];?>');
					$('#BTP_GP').val('<?php echo $conf['MedalRequirement']['Silver']['BTP']['GoodPoint'];?>');
*/
				break;
			case 'Bronze':
                $('#NrScopeMet').val('2');
/*
					$('#BSQ_GP').val('<?php echo $conf['MedalRequirement']['Bronze']['BSQ']['GoodPoint'];?>');
					$('#BSQ_TM').val('<?php echo $conf['MedalRequirement']['Bronze']['BSQ']['TargetMet'];?>');
					$('#EPW_TM').val('<?php echo $conf['MedalRequirement']['Bronze']['EPW']['TargetMet'];?>');
					$('#OFS_GP').val('<?php echo $conf['MedalRequirement']['Bronze']['OFS']['GoodPoint'];?>');
					$('#BTP_TM').val('<?php echo $conf['MedalRequirement']['Bronze']['BTP']['TargetMet'];?>');
					$('#BTP_GP').val('<?php echo $conf['MedalRequirement']['Bronze']['BTP']['GoodPoint'];?>');
*/
				break;
            default:
                $('#NrScopeMet').val('0');
                break;
		}
	}	
	
	function Export() {
		if ($('#ClassLevel\\[\\]:checked').length == 0) {
			alert("<?=$Lang['iPortfolio']['JinYing']['MedalError']['EmptyClassLevel']?>");
			return;					
		}

		var original_action = document.form1.action;
		var url = "export_medal_data.php";
		document.form1.action = url;
		document.form1.submit();
		document.form1.action = original_action;
	}
	
	function CheckAll(obj) {
		$('#ClassLevel\\[\\]').attr('checked', $(obj).attr('checked'));
	}
</script>

<div id="report_show_option">

	<span id="spanShowOption" class="spanShowOption" style="display: none">
		<a href="javascript:showOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['ShowOption']?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['iPortfolio']['JinYing']['Medal']['HideOption']?></a>
	</span>


	<form name="form1" method="post" id="form1">

		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['General']['SchoolYear']?><span class=tabletextrequire>*</span></td>
				<td><?=$yearFilter?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['Medal']['Form']?><span
					class=tabletextrequire>*</span></td>
				<td id="FormListFilter"><?=$formFilter?></td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['Medal']['PresetCriteria']?></td>
				<td><select name="PresetMedalCriteria" id="PresetMedalCriteria">
						<option value="">-- <?=$button_select?> --</option>
						<option value="Gold"><?=$Lang['iPortfolio']['JinYing']['Medal']['Gold']?></option>
						<option value="Silver"><?=$Lang['iPortfolio']['JinYing']['Medal']['Silver']?></option>
						<option value="Bronze"><?=$Lang['iPortfolio']['JinYing']['Medal']['Bronze']?></option>
				    </select>
                    <lable><?php echo $Lang['iPortfolio']['JinYing']['Medal']['TargetMetScopeNumber']; ?></lable>&nbsp;

                    <select name="NrScopeMet" id="NrScopeMet">
                    <?php for ($i=0; $i<=4; $i++): ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php endfor;?>
                    </select>
                </td>
			</tr>
            <tr>
                <td class="field_title"><?php echo $Lang['iPortfolio']['JinYing']['Medal']['ScopeRequirement'];?></td>
                <td id="FormListFilter">&nbsp;</td>
            </tr>
<?
$numberAry = array();
for ($i = 0; $i <= 20; $i ++) {
    $numberAry[] = array(
        $i,
        $i
    );
}

// Scope Selection
$i=0;
foreach ((array) $Lang['iPortfolio']['JinYing']['Report']['Scope'] as $scope => $item) :
    $item = sprintf($item, $Lang['iPortfolio']['JinYing']['Report']['Index'][$i++]);
    ?>
			<tr>
				<td class="field_title"><?=$item?></td>
				<td>
					<table border="0">
					<? if ($scope != 'EPW'):?>
						<tr>
							<td><?=$Lang['iPortfolio']['JinYing']['Medal']['GoodPoint']?></td>
							<td><?=getSelectByArray($numberAry, 'name="'.$scope.'_GP" id="'.$scope.'_GP"', $conf['ScopeRequirement'][$scope]['GoodPoint']).$Lang['iPortfolio']['JinYing']['Medal']['Point'].$Lang['iPortfolio']['JinYing']['Medal']['OrAbove']?></td>
						</tr>
					<? endif;?>
                    <?php if ($scope == 'BTP') echo "<tr><td colspan='2'>".$Lang['General']['Or']."</td></tr>"; ?>
					<? if ($scope != 'OFS'):?>
						<tr>
							<td><?=(($scope == 'BTP') ? $Lang['iPortfolio']['JinYing']['Medal']['TargetMetWiGood'] : $Lang['iPortfolio']['JinYing']['Medal']['TargetMet'])?></td>
							<td><?=getSelectByArray($numberAry, 'name="'.$scope.'_TM" id="'.$scope.'_TM"', $conf['ScopeRequirement'][$scope]['TargetMet']).$Lang['iPortfolio']['JinYing']['Medal']['Times'].$Lang['iPortfolio']['JinYing']['Medal']['OrAbove']?></td>
						</tr>
					<? endif;?>
					</table>
				</td>
			</tr>
	
<?  endforeach;?>	

			<tr>
				<td class="field_title"><?=$Lang['iPortfolio']['JinYing']['Medal']['AwardStatus']?></td>
				<td><label><input type="checkbox" name="NotAwarded" id="NotAwarded"
						value="NotAwarded" checked><?=$Lang['iPortfolio']['JinYing']['Medal']['NotAwarded']?></label>&nbsp;
					<label><input type="checkbox" name="Awarded" id="Awarded"
						value="Awarded"><?=$Lang['iPortfolio']['JinYing']['Medal']['Awarded']?></label></td>
			</tr>

			<tr>
				<td colspan=2 class="tabletextremark"><?php echo $Lang['iPortfolio']['JinYing']['ImportRemarks']['Mandatory'];?></td>
			</tr>
		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submit_result")?>
			<p class="spacer"></p>
		</div>
	</form>
</div>
<div id="form_result"></div>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>