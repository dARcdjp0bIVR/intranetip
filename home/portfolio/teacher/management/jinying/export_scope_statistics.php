<?php
// using:    
/*
 *  2018-04-18 Cameron
 * 		- create this file
 */

@SET_TIME_LIMIT(216000);
ini_set('memory_limit','256M');

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

iportfolio_auth("T");	// teacher
intranet_opendb();


if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
if (!$AcademicYearID) {
	return '';	
}
$scope = $_POST['Scope'];
if (!$scope) {
    return '';
}
$filter['Scope'] = $scope;
$filter['GoodPointGold'] = $_POST['GoodPointGold'];
$filter['GoodPointSilver'] = $_POST['GoodPointSilver'];
$filter['GoodPointBronze'] = $_POST['GoodPointBronze'];
$filter['TargetMetGold'] = $_POST['TargetMetGold'];
$filter['TargetMetSilver'] = $_POST['TargetMetSilver'];
$filter['TargetMetBronze'] = $_POST['TargetMetBronze'];

$ljy = new libJinYing();

$ljy->setAcademicYearID($AcademicYearID);
$classLevelAry = $ljy->getAllClassLevel();
if (count($classLevelAry)) {
    $ljy->setClassLevel(implode(',',$classLevelAry));       // set ClassLevel because some functions use it in $ljy, e.g. setGoodPoint_BSQ
}

$academic_start_year = substr(getStartDateOfAcademicYear($AcademicYearID),0,4);
$academic_end_year = substr(getEndDateOfAcademicYear($AcademicYearID),0,4);

$stat = $ljy->getMedalStatByScope($filter);
$column_one = $Lang['iPortfolio']['JinYing']['Statistics']['Form'];
$title = sprintf($Lang['iPortfolio']['JinYing']['StatisticsByScope'],$academic_start_year,$academic_end_year,$Lang['iPortfolio']['JinYing']['Scope'][$scope]);

$space = $intranet_session_language == 'en' ? ' ' : '';

$exportColumn = array();
$exportColumn[0][] = $column_one;
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['Total'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Gold'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Gold'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Silver'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Silver'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Bronze'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['Bronze'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['None'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Count'];
$exportColumn[0][] = $Lang['iPortfolio']['JinYing']['Statistics']['MedalLevel']['None'].$space.$Lang['iPortfolio']['JinYing']['Statistics']['Percentage'];

$lexport = new libexporttext();

$result_data = array();
foreach((array)$stat as $r=>$v) {
	$tmpRow = array();
	$tmpRow[] = $v['Name'];
	$tmpRow[] = $v['NumStudent'];
	$tmpRow[] = $v['NumGold'];
	$tmpRow[] = $v['PercentGold'].'%';
	$tmpRow[] = $v['NumSilver'];
	$tmpRow[] = $v['PercentSilver'].'%';
	$tmpRow[] = $v['NumBronze'];
	$tmpRow[] = $v['PercentBronze'].'%';
    $tmpRow[] = $v['NumMedalNone'];
    $tmpRow[] = $v['PercentMedalNone'].'%';
	$result_data[] = $tmpRow;
	unset($tmpRow);
}

$export_content = $lexport->GET_EXPORT_TXT($result_data, $exportColumn, "\t", "\r\n", "\t", 0, '11');

intranet_closedb();

$filename = $title."_".date("Ymd").".csv";

$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");
?>