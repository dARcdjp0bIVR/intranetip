<?php
/*
 * 	Log
 * 	
 * 	Purpose: return form-class layout with number of imported records according to selected item
 *
 *  2019-12-03 [Cameron]
 *      - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 *      - change RecordID to RecordInfo to avoid converting it to integer as it's string
 *
 * 	Date:	2016-12-19 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

$json['success'] = false;
if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$lfcm = new form_class_manage();
$linterface = new interface_html();

# Class List
$classTitle = Get_Lang_Selection('ClassTitleB5','ClassTitleEN');
$classList = $lfcm->Get_Class_List_By_Academic_Year($AcademicYearID);
$formList = BuildMultiKeyAssoc($classList,'YearID','YearName',1);
$classList = BuildMultiKeyAssoc($classList,array('YearID','YearClassID'),array('YearID','YearClassID',$classTitle));

$maxNumClass = 0;	// get max number of class in a form for showing columns
foreach((array)$classList as $form=>$class) {
	if (count($class) > $maxNumClass) {
		$maxNumClass =  count($class);
	}
}

## show result
$result = array();
$itemList_array = array();
$semesterCss = 'none';
$semesterFilter = '';

if ($AcademicYearID && $Item) {
	$terms = getAllSemesterByYearID($AcademicYearID);
	$Semester = $_POST['Semester'] ? $_POST['Semester'] : (count($terms) ? $terms[0]['YearTermID'] : 0);
	list($scope,$item) = explode('-',$Item);
	$itemName = $Lang['iPortfolio']['JinYing']['Item'][$scope][$item];
	if (in_array($Item,$conf['JinYingItemCodeWithTerm'])) {
		$termCond = "AND p.YearTermID='".$Semester."'";
		$termCode = $Semester;
		$semesterCss = '';
		$layt = new academic_year_term($Semester);
		$termName = $layt->Get_Year_Term_Name();
	}
	else {
		$termCond = '';
		$termCode = '0';
		$termName = '';
	}

	$semesterFilter = getSelectSemester2('name="Semester" id="Semester" style="display:'.$semesterCss.'" onChange="changeAcademicTerm();"',$Semester, 1, $AcademicYearID);

	$sql = "SELECT yc.YearClassID, 
			IF(COUNT(DISTINCT p.UserID)>0,CONCAT('<input type=\'checkbox\' name=\'RecordInfo[]\' id=\'RecordInfo[]\' data-class=\'',yc.`YearID`,'\' value=\'', yc.`YearClassID`,'^~','$AcademicYearID','^~','$termCode','^~','$Item','\'>',$classTitle),$classTitle) AS ClassName,
 			COUNT(DISTINCT p.UserID) AS NrRec
			FROM 	YEAR_CLASS yc
			LEFT JOIN YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID
			LEFT JOIN {$eclass_db}.PORTFOLIO_JINYING_SCHEME p  
					ON p.UserID=ycu.UserID 
					".$termCond." 
					AND p.PerformanceCode LIKE '".$Item."%'
					AND p.AcademicYearID='".$AcademicYearID."' 
			WHERE yc.AcademicYearID='".$AcademicYearID."'
			GROUP BY yc.YearClassID";
	$rs = $lfcm->returnResultSet($sql);
	
	if (count($rs)) {
		$result = BuildMultiKeyAssoc($rs,'YearClassID',array('ClassName','NrRec'));
	}
}
else {
	$termCode = '0';
}


$x = '';
if ($AcademicYearID && $Item):
	$x .= '<table width="95%" border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
	foreach((array)$formList as $formID=>$formName):
		$i=0;
		$x .= '<tr>
				<td rowspan="2" class="align_center"><input type="checkbox" name="FormID[]" id="FormID[]" value="'.$formID.'">'.$formName.'</td>';
		if ($classList[$formID]):				
			foreach((array)$classList[$formID] as $classID=>$class):
				$i++;
				$x .= '<td class="align_center hideBottomBorder">'.($result[$classID]['ClassName'] ? $result[$classID]['ClassName'] : '-').'</td>';
			endforeach;
		endif;
	
		for($j=$i;$j<$maxNumClass;$j++):
			$x .= '<td class="hideBottomBorder">&nbsp;</td>';
		endfor;
		$x .= '</tr>';

		$x .= '<tr>';
		if ($classList[$formID]):				
			foreach((array)$classList[$formID] as $classID=>$class):
				$x .= '<td class="align_center">'.($result[$classID]['NrRec'] ? $result[$classID]['NrRec'] : '0').'</td>';
			endforeach;
		endif;
	
		for($j=$i;$j<$maxNumClass;$j++):
			$x .= '<td>&nbsp;</td>';
		endfor;
		$x .= '</tr>';
	endforeach;

	$x .='</table>';
	
	$x .= '<div>'.$Lang['iPortfolio']['JinYing']['Remark'].'</div>';

	$x .= '<div class="edit_bottom_v30">
			<p class="spacer"></p>';
	$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], "button", "javascript:checkRemove(document.form1,'RecordInfo[]','remove.php')", "DeleteBtn");
	$x .= '<p class="spacer"></p>
		   </div>';
endif;	

$json['success'] = true;
$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;
$y = remove_dummy_chars_for_json($semesterFilter);
$json['semesterFilter'] = $y;

$jsonObj = new JSON_obj();

echo $jsonObj->encode($json);

intranet_closedb();
?>