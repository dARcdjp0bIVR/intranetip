<?php
/*
 * 	Log
 * 	
 * 	Purpose: import jinying scheme data
 *
 *  Date:   2020-09-17 [Cameron]
 *          - add AcademicYear selection so that it can import records of past year [case #J193735]
 *
 *  Date:   2019-12-03 [Cameron]
 *          - modify logic related to $conf['JinYingItemCodeWithTerm'] as the codes cover scope [case #J161484]
 *
 *	Date:	2017-05-04 [Cameron]
 *			add version checking, don't allow to access this page if php version < 5.2
 *
 *	Date:	2017-04-28 [Cameron]
 *			- add update / delete function
 *
 * 	Date:	2016-11-22 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");

iportfolio_auth("T");	// teacher
intranet_opendb();

if (!$sys_custom['iPf']['JinYingScheme']) {
	header("Location: /home/portfolio/school_records.php");
	exit;
}

if (phpversion_compare('5.2') == 'ELDER') {
	header("Location: index.php");
	exit;
}

# Build Item List
$itemList_array = array();
$itemList_array[] = array('',$Lang['iPortfolio']['JinYing']['SelectItem']); 
foreach($Lang['iPortfolio']['JinYing']['Item'] as $scope=>$item) {
	$itemList_array[] = array('OPTGROUP',$Lang['iPortfolio']['JinYing']['Scope'][$scope]);	// (value, name)
	foreach($item as $k=>$v) {
		$code = "$scope-$k";
		$itemList_array[] = array($code,$v);	
	}
}
$itemFilter = returnSelection($itemList_array, '', 'Item', 'ID="Item"');

$AcademicYearID = Get_Current_Academic_Year_ID();	// default current year
// Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);

$currentYearTerm = getCurrentAcademicYearAndYearTerm();
$currentTerm = $currentYearTerm['YearTermID'];
 
# Acadermic Term Selection
$semesterFilter = getSelectSemester2('name="Semester" id="Semester" style="display:none"',$currentTerm, $ParQuoteValue=1, $AcademicYearID);

$lpf = new libportfolio();

# Page heading setting
$linterface = new interface_html();

# tag information	
$CurrentPage = "Teacher_JinYing_Scheme";

$ljy = new libJinYing();
$TAGS_OBJ = $ljy->getJinYingTab('JinYing');

$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION = $ljy->getImportPageNavigation();
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

# handle return message
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
if ($returnMsgKey == 'MissingItem') {
	$returnMsg = $Lang['iPortfolio']['JinYing']['ReturnMessage']['MissingItem'];
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1,array_values($Lang['iPortfolio']['JinYing']['ImportStepArr']));

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="style=\"display:none;\"", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>

<script type="text/JavaScript" language="JavaScript">
    var isLoading = true;
    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goCancel() {
	self.location = "index.php";
}

function goSubmit() {
	var canSubmit = true;
	
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		canSubmit = false;
		alert('<?=$Lang['General']['PleaseSelectFiles']?>');
		obj.elements["userfile"].focus();
	}
	
	if (canSubmit) {
		if ($("input:radio[name='ActionType']:checked").val() == '1') {
		    if ($('#AcademicYearID').val() != '<?php echo $AcademicYearID;?>') {
		        confirm = window.confirm("<?php echo $Lang['iPortfolio']['JinYing']['ImportWarning']['NonCurrentYear'];?>");
                if (confirm) {
                    obj.action = "import_jinying_validate.php";
                    obj.submit();
                }
            }
		    else {
                obj.action = "import_jinying_validate.php";
                obj.submit();
		    }
		}
		else {
			if ($("input:radio[name='ActionSubtype']:checked").val() == '1') {
				obj.action = "import_jinying_validate_update.php";
				obj.submit();	
			}
			else {
				var confirm = window.confirm("<?=$i_Discipline_System_alert_remove_record?>");
				if (confirm) {
					obj.action = "import_jinying_validate_delete.php";
					obj.submit();	
				}
			}			
		}
	}
}

$(document).ready(function(){
	$('#Item').change(function(e) {
		changeColumnLayout(e);
	});
	
	$("input:radio[name='ActionType']").change(function(e) {
		changeColumnLayout(e);
	});

	$("input:radio[name='ActionSubtype']").change(function(e) {
		changeColumnLayout(e);
	});
	
	$("input:radio[name='ActionType']").click(function() {
		if ($(this).val() == '2') {
			$('#ActionSubtypeRow').css("display","");
            $('#AcademicYearRow').css("display","none");
		}
		else {
			$('#ActionSubtypeRow').css("display","none");
            $('#AcademicYearRow').css("display","");
		}
	});
	
    $('#AcademicYearID').change(function(){
        isLoading = true;
        $('#semesterSpan').html(loadingImg);

        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax_get_academic_year_terms.php',
            data : {'AcademicYearID': $('#AcademicYearID').val(),
                'Item': $('#Item').val()
            },
            success: function(ajaxReturn) {
                if (ajaxReturn != null && ajaxReturn.success){
                    $('#semesterSpan').html(ajaxReturn.html);
                    showOrHideTerm($('#Item').val());
                    isLoading = false;
                }
            },
            error: show_ajax_error
        });
    });

});	// end $(document).ready()

function showOrHideTerm(itemCode)
{
    <?php echo "var codeWithTermAry = ['".implode("','",$conf['JinYingItemCodeWithTerm'])."'];"; ?>
    if (codeWithTermAry.indexOf(itemCode) != -1) {
        $('#Semester').css("display","");
    }
    else {
        $('#Semester').css("display","none");
    }
}

function changeColumnLayout(event) {
  	event.preventDefault();
  	$('#import_table_div').html('');
  	$('#submitBtn').css("display","none");
  	$('#Semester').css("display","none");
  	if ($('#Item').val() != '') {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_get_item_details.php',
			data : { 
					'Item': $('#Item').val(),
					'ActionType': $("input:radio[name='ActionType']:checked").val(),
					'ActionSubtype': $("input:radio[name='ActionSubtype']:checked").val()
				},
			success: load_import_layout,
			error: show_ajax_error
		});
  	}
}

function load_import_layout(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('#import_table_div').html(ajaxReturn.html);
	  	$('#submitBtn').css("display","");
        <?php echo "var codeWithTermAry = ['".implode("','",$conf['JinYingItemCodeWithTerm'])."'];"; ?>
        var currentItem = $('#Item').val();
        if (codeWithTermAry.indexOf(currentItem) != -1) {
            if ($("input:radio[name='ActionType']:checked").val() == '1') {
                $('#Semester').css("display","");
            }
            else {
                $('#Semester').css("display","none");
            }
        }
        else {
            $('#Semester').css("display","none");
        }
	}
}	


function show_ajax_error() {
	alert('<?=$Lang['iPortfolio']['JinYing']['ajaxError']?>');
}

</script>
<form id="form1" name="form1" action="import_jinying_validate.php" method="POST" enctype="multipart/form-data">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board" style="width:100%; padding-left:6px;">
		<table border="0" cellspacing="0" cellpadding="0" class="form_table_v30" style="width: 85%; margin-left: 7%;">
            <tr>
		    	<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['JinYing']['Import']['ActionType']?></td>
                <td class="tabletext" width="80%">
                	<?=$linterface->Get_Radio_Button("ActionAdd", "ActionType", "1",1,"",$Lang['iPortfolio']['JinYing']['Import']['AddNew']);?> &nbsp;
                	<?=$linterface->Get_Radio_Button("ActionUpdate", "ActionType", "2",0,"",$Lang['iPortfolio']['JinYing']['Import']['Update']);?><span class="tabletextremark">(<?=$Lang['iPortfolio']['JinYing']['ImportRemarks']['Update']?>)</span>
    			</td>
            </tr>
            <tr id="ActionSubtypeRow" style="display:none">
		    	<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['JinYing']['Import']['ActionSubType']?></td>
                <td class="tabletext" width="80%">
                	<?=$linterface->Get_Radio_Button("ActionReplace", "ActionSubtype", "1",1,"",$Lang['iPortfolio']['JinYing']['Import']['Replace']);?> &nbsp;
                	<?=$linterface->Get_Radio_Button("ActionDelete", "ActionSubtype", "2",0,"",$Lang['iPortfolio']['JinYing']['Import']['Delete']);?>
    			</td>
            </tr>
            <tr id="AcademicYearRow">
                <td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
                <td><?=$yearFilter?></td>
            </tr>
			<tr>
		    	<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['JinYing']['ActivityItems']?></td>
                <td class="tabletext" width="80%">
					<?=$itemFilter?>
                    <span id="semesterSpan"><?= $semesterFilter ?></span>
					<?=$Lang['iPortfolio']['JinYing']['Remark']?>						
<!--					<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="--><?//=$AcademicYearID?><!--">-->
				</td>
			</tr>
		</table>
	</div>
	<br>
	<div class="table_board" style="width:85%;">
		<div id="import_table_div">
		
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>