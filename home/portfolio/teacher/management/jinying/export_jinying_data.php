<?php
// using:    
/*
 * 	Note:	Please edit in utf-8
 *
 *  2019-12-02 Cameron
 *      - add case BTP-TP, BTP-IS, EPW-PW, EPW-OC [case #J161484]
 *
 * 	2017-04-25 Cameron
 * 		- create this file
 */

@SET_TIME_LIMIT(216000);
ini_set('memory_limit','512M');
date_default_timezone_set('Asia/Hong_Kong');

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/config.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
include_once($PATH_WRT_ROOT."home/portfolio/teacher/management/jinying/export_common.php");

iportfolio_auth("T");	// teacher
intranet_opendb();


if (!$sys_custom['iPf']['JinYingScheme']) {
	exit;
}

$AcademicYearID = $_POST['AcademicYearID'];
if (!$AcademicYearID) {
	return '';	
}
$Item = $_POST['Item'];
if (!$Item) {
	return '';	
}


## Step 1: get filters
$academic_start_year = date('Y',getStartOfAcademicYear($AcademicYearID));		// for partial of export file name
$semesters = getAllSemesterByYearID($AcademicYearID);
$semesters = BuildMultiKeyAssoc($semesters,'YearTermID','TermTitle',1);			// single value

$filter = array();
$filter['AcademicYearID'] = $AcademicYearID;
$filter['Item'] = $Item;
$filter['Semester'] = $_POST['Semester'];
$filter['YearClassID'] = $_POST['YearClassID'];


## Step 2: get header columns
$ljy = new libJinYing();
$presetHeader = $ljy->getPresetHeader($Item,'');
$nrCol = count($presetHeader);


## Step 3: get export data
$ljy->setAcademicYearID($AcademicYearID);
$data = $ljy->getJinYingData($filter);


## Step 4: output excel file
$objPHPExcel = new PHPExcel();	// Create new PHPExcel object

list($main_code,$sub_code) = explode('-',$Item);
$export_subject = str_replace('#','',$Lang['iPortfolio']['JinYing']['Item'][$main_code][$sub_code]);
$file_name = $academic_start_year.'_'.$export_subject.'_'.date('Ymd').'.xlsx';

// Set properties
$objPHPExcel->getProperties()->setCreator("eClass")
							 ->setLastModifiedBy("eClass")
							 ->setTitle("iPortfolio Student Profile")
							 ->setSubject($export_subject)
							 ->setDescription($export_subject)
							 ->setKeywords("iPortfolio JinYing")
							 ->setCategory("iPortfolio");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('新細明體');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);

// Create a first sheet
$objPHPExcel->setActiveSheetIndex(0);
$activeSheet = $objPHPExcel->getActiveSheet();

// build header row
for($i=0;$i<$nrCol;$i++) {
	$activeSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($i).'1', $presetHeader[$i]);
}
$activeSheet->getStyle('A1:'.(PHPExcel_Cell::stringFromColumnIndex($nrCol-1)).'1')->getFont()->setBold(true);

// function in export_common.php
switch($Item) {
	case 'BSQ-EC':
	case 'BSQ-TA':
	case 'EPW-TA':
		Export_With_ActivityDate($Item);
		break;
	case 'EPW-PY':
	case 'EPW-PJ':
	case 'EPW-PS':
    case 'BTP-TP':
    case 'BTP-IS':
		Export_Physical($Item);             // no term from standard
		break;
    case 'EPW-PW':
    case 'EPW-OC':
        Export_StandardWoAward($Item);      // no award from standard
        break;
	case 'OFS-SI':
		Export_OFS_SI();
		break;
	case 'OFS-SO':
		Export_OFS_SO();
		break;
	case 'BTP-EA':
		Export_BTP_EA();
		break;
	case 'BTP-IC':
		Export_BTP_IC();
		break;
	default:
		Export_Standard($Item);
		break;
}

// freeze first two column and top row
$activeSheet->freezePane('C2');	

// Rename sheet
$activeSheet->setTitle($Lang['iPortfolio']['JinYing']['Export']['PIC'][$Item]);


intranet_closedb();

// output file
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

exit;

?>