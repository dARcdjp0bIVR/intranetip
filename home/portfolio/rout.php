<?php
// Modifing by 
/*
 * 20190418 Tiffany
 * Modified: support logic of "iPortfolioFromApp" to both teacher and parent app
 */
/*
 * 20180110 Ivan [ip.2.5.9.3.1]
 * Modified: support logic of "iPortfolioFromApp" which means login from student app
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-lp2.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li_pf = new libportfolio();

	switch($ck_memberType)
	{
	  case "T":
            if ($_SESSION['iPortfolioFromApp']) {
              session_unregister_intranet("iPortfolioFromApp");
              header("Location: ../eClassApp/common/iPortfolio/index.php");
            }
			else if($li_pf->HAS_RIGHT("student_info")){
			  header(libpf_lp2::$is_competition? "Location: ./learning_portfolio_v2/": "Location: school_records.php");
			}
			else if(strstr($ck_function_rights, "Profile:ImportData")){
			  header("Location: teacher/data_handling/update_info.php");
			}
			else if($li_pf->HAS_RIGHT("ole") || strstr($ck_function_rights, "OLR")){
			  header("Location: teacher/management/ole.php");
			}
			else if(strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing")){
			  header($iportfolio_lp_version == 2? "Location: ./learning_portfolio_v2/": "Location: contents_admin/index_scheme.php");
			}
			else if(strstr($ck_user_rights, ":growth:") && $li_pf->HAS_RIGHT("growth_scheme")){
			  header("Location: teacher/management/sbs/index.php");
			}
			else if($li_pf->HAS_RIGHT("print_report")){
			  header("Location: profile/report/report_printing_menu.php");
			}
			else if($li_pf->HAS_RIGHT("ole")){
			  header("Location: profile/ole/index_stat.php");
			}
			else if(strstr($ck_function_rights, "Profile:Stat")){
			  header("Location: profile/management/score_list.php");
			}
			else{
				$msg = libportfolio::backToIPHome();
				echo $msg;
			}
		break;
	  case "P":
		if (libpf_lp2::isFromKIS()){
		    echo 'success';
		}else{
            if ($_SESSION['iPortfolioFromApp']) {
                session_unregister_intranet("iPortfolioFromApp");
                header("Location: ../eClassApp/common/iPortfolio/index.php");
            }
            else {
                header("Location: profile/student_info_parent.php");
            }
		}
		break;
	  case "S":
		if (libpf_lp2::$is_competition && $iportfolio_lp_version == 2){
		  header("Location: ./learning_portfolio_v2/");
		}else{
			if ($_SESSION['iPortfolioFromApp']) {
				session_unregister_intranet("iPortfolioFromApp");
				header("Location: ../eClassApp/common/iPortfolio/index.php");
				die();
			}
			else {
				header("Location: profile/student_info_student.php");
			}
		}
		
		break;
	  default:
		  $msg = libportfolio::backToIPHome();
		  echo $msg;
		break;
	}


?>
