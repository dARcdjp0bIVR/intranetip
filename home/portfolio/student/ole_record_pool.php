<?php
// Modifing by 

/*
##### Change Log [Start] #####
#
#	Date:	2015-03-06	Omas
# 		Fix :Disable swap_btn when updating on db(by ajax)		
#
###### Change Log [End] ######
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// Initializing classes
$LibUser = new libuser($UserID);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$gray = '#C0C0C0';
$_borderTop = 'border-top: 1px solid '.$gray.';';
$_borderLeft = 'border-left: 1px solid '.$gray.';';
$_borderRight = 'border-right: 1px solid '.$gray.';';
$_borderBottom = 'border-bottom: 1px solid '.$gray.';';

$IntExt_Setting = $IntExt;
if ($sys_custom['IPF_OLE_SELECT']['INCLUDE_OUTSIDE_RECORD'])
{
	$IntExt = 3;
	$IntExt_Setting = 0;
}

// load settings
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($UserID, $IntExt);
$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt_Setting);

if(is_array($OLESettings))
{
	if($OLESettings[$StudentLevel[1]][0] != "")
	{
		$RecordsAllowed = $OLESettings[$StudentLevel[1]][0]-count($OLEAssignedArr);

		if($RecordsAllowed < 0)
			$LibPortfolio->RESET_OVERSET_RECORD_SLP($UserID, abs($RecordsAllowed), $IntExt);
	}
	else
		$RecordsAllowed = "inf";
}

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
if($IntExt == 1)
	$CurrentPage = "Student_ExtOLE";
else
	$CurrentPage = "Student_OLE";
$CurrentPageName = $IntExt == 1 ? "<font size='-2'>".$iPort["external_record"]."</font>" : $iPort['menu']['ole'];


if ($sys_custom['IPF_OLE_SELECT']['INCLUDE_OUTSIDE_RECORD'])
{
	$CurrentPageName = "<font size='-2'>". $iPort['menu']['ole'] . " &". $iPort["external_record"]."</font>";
}


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

//# load approval setting
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();

$vars = "ClassName=$ClassName&StudentID=$StudentID";
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else 
{
	header("Location ../index.php");
}

#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";
####################################

$ELEArray = $LibPortfolio->GET_ELE();
$ELECodeArray = array_keys($ELEArray);
if (count($ELECodeArray)>0)
{
	$ELEImplode = implode(",",$ELECodeArray);
}
$ELECount = count($ELECodeArray);

####################################

$swap_control =	"
					CONCAT('
					&nbsp;
					<table cellpadding=0 border=0 cellspacing=0>
						<tr><td><a class=\"swap_btn\" clickable=\"1\" href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', a.RecordID,', -1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
						<tr><td><a class=\"swap_btn\" clickable=\"1\" href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', a.RecordID,', 1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
					</table>
					')
				";

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

$LibTable = new libdbtable2007(0, 1, $pageNo);
$LibTable->field_array = array("a.SLPOrder");

if(!empty($OLEAssignedArr))
	$conds = " AND a.RecordID IN (".implode(",",$OLEAssignedArr).")";
else {
	// IMPORTANT : no records displayed when no SLP records is selected [Case #A75039]
	$conds = " AND 1 = 2";
}

$namefield = getNameFieldWithClassNumberByLang("b.");

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("a.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";



if($IntExt == 1)
	$conds .= " AND c.IntExt = 'EXT'";
elseif($IntExt == 3)
	$conds .= " AND (c.IntExt = 'INT' OR c.IntExt = 'EXT')";
else
	$conds .= " AND c.IntExt = 'INT'";


$sql = "	SELECT
				'{$ELECount}', 
				'{$ELEImplode}',
              	c.Title,
              	IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,'<br />$profiles_to<br />',a.EndDate))) as OLEDate,
              	a.ELE,
			  	$SqlCategoryField			  	
				{$swap_control},
				CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'),
				IF ((Month(a.StartDate)>8), Year(a.StartDate), Year(a.StartDate)-1) AS StartYear
			FROM 
				{$eclass_db}.OLE_STUDENT as a
			LEFT JOIN 
				{$intranet_db}.INTRANET_USER as b
			ON 
				a.ApprovedBy = b.UserID
			LEFT JOIN
				{$eclass_db}.OLE_PROGRAM as c
			ON
				a.ProgramID = c.ProgramID
        	WHERE 
				a.UserID = '$StudentID'
				$conds
			";
//debug($sql);			
			
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 9;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->IsColOff = "iPortfolioOLEPool";
$LibTable->additionalCols = count($ELECodeArray);


$ELEArray = $LibPortfolio->GET_ELE();		

// TABLE COLUMN
$row_span = ($IntExt == 0 || $IntExt == 3) ? 2 : 1;


$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' align='center' rowspan='$row_span' >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center' rowspan='$row_span' width='100' >".$ec_iPortfolio['title']."</td>\n";
$LibTable->column_list .= "<td rowspan='$row_span' align='center' >".$ec_iPortfolio['date']."/".$ec_iPortfolio['period']."</td>\n";
if($IntExt == 0 || $IntExt == 3)
	$LibTable->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
$LibTable->column_list .= "<td rowspan='$row_span' align='center'  nowrap='nowrap'>".$ec_iPortfolio['category']."</td>\n";
//$LibTable->column_list .= "<td class='tabletop' >".$LibTable->column(5,$ec_iPortfolio['attachment'], 1)."</td>\n";
$LibTable->column_list .= "<td rowspan='$row_span' align='center'  nowrap='nowrap'>&nbsp;</td>\n";
$LibTable->column_list .= ($ck_memberType=="S") ? "<td rowspan='$row_span' align='center' >".$LibTable->check("record_id[]")."</td>\n" : "";

$LibTable->column_list .= "</tr>\n";


if (($IntExt == 0 || $IntExt == 3) && count($ELECodeArray) > 0)
{
	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
//		$CodePos = array_search($ELECode, $ELECodeArray);
//		$TargetField = $CodePos+1;
		$ELEDisplay = $ELETitle;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b>".$ELEDisplay."</b></td>";
	}

	$LibTable->column_list .= "</tr>";				
}
$LibTable->column_array = array(10,10,0,10,0,0,10,0);
	
$ELEArray = $LibPortfolio->GET_ELE();
//////////////////////////////////////////////

$linterface->LAYOUT_START();


if ($msg == "3")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("delete")."</td>";
} 
else if ($msg == "1")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("add")."</td>";
} 
else 
{
	$xmsg = "";
}

# Get SLP period settings data from DB
$objIpfPeriodSetting = new iportfolio_period_settings();
$SLPSettingsArray = $objIpfPeriodSetting->getSettingsArray("SLP", $StudentLevel['YearID']);

# Set start and end date format
$SLPSettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SLPSettingsArray['StartDate'], (array)$SLPSettingsArray['StartHour'], (array)$SLPSettingsArray['StartMinute']);
$SLPSettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SLPSettingsArray['EndDate'], (array)$SLPSettingsArray['EndHour'], (array)$SLPSettingsArray['EndMinute']);
$SetSLPPeriod_startTime = $SLPSettingsArray['StartDateTime'][0];
$SetSLPPeriod_endTime = $SLPSettingsArray['EndDateTime'][0];
$SubmissionPeriod_isAllowed = $SLPSettingsArray['AllowSubmit'];

# Display message
//if(($OLESettingsArray['StartDateTime']!="" && $OLESettingsArray['StartDateTime']!="0000-00-00 00:00:00") && ($OLESettingsArray['EndDateTime']!="" && $OLESettingsArray['EndDateTime']!="0000-00-00 00:00:00")) 
//	{
//		$SubmissionPeriod_Msg = $OLESettingsArray['StartDateTime']." ".$iPort['to']." ".$OLESettingsArray['EndDateTime'];
//	} 
//	else if($OLESettingsArray['StartDateTime']!="" && $OLESettingsArray['StartDateTime']!="0000-00-00 00:00:00")
//	{
//		$SubmissionPeriod_Msg = $OLESettingsArray['StartDateTime'];
//	}	 
//	else if($OLESettingsArray['EndDateTime']!="" && $OLESettingsArray['EndDateTime']!="0000-00-00 00:00:00")
//	{
//		$SubmissionPeriod_Msg = $OLESettingsArray['EndDateTime'];
//	}	  
//	else
//	{
//		$SubmissionPeriod_Msg = "";
//	}
	
	if(($SetSLPPeriod_startTime && $SetSLPPeriod_startTime!="0000-00-00 00:00:00") && ($SetSLPPeriod_endTime!="" && $SetSLPPeriod_endTime!="0000-00-00 00:00:00")) 
	{
		$SetSLPPeriod_Msg = $SetSLPPeriod_startTime." ".$iPort['to']." ".$SetSLPPeriod_endTime;
	} 
	else if($SetSLPPeriod_startTime!="" && $SetSLPPeriod_startTime!="0000-00-00 00:00:00")
	{
		$SetSLPPeriod_Msg = $SetSLPPeriod_startTime;
	}	 
	else if($SetSLPPeriod_endTime!="" && $SetSLPPeriod_endTime!="0000-00-00 00:00:00")
	{
		$SetSLPPeriod_Msg = $SetSLPPeriod_endTime;
	}	  
	else
	{
		$SetSLPPeriod_Msg = "";
	}

//if($LibPortfolio->isStudentSubmitOLESetting($IntExt, $ck_memberType))
//{
//	$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();debug_pr($studentOleConfigDataArray);
//	if(($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")) 
//	{
//	 //	$Period_Msg = $iPort['period_start_end']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
//		$SubmissionPeriod_Msg = $studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
//	} 
//	else if($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	{
//	//	$Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"];
//		$SubmissionPeriod_Msg = $studentOleConfigDataArray["STARTTIME_TO_MINS"];
//	}	 
//	else if($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	{
//	//	$Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
//		$SubmissionPeriod_Msg = $studentOleConfigDataArray["ENDTIME_TO_MINS"];
//	}	  
//	else
//	{
//		$SubmissionPeriod_Msg = "";
//	}
//	
//	if(($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")) 
//	{
//		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
//	} 
//	else if($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	{
//		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"];
//	}	 
//	else if($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	{
//		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
//	}	  
//	else
//	{
//		$SetSLPPeriod_Msg = "";
//	}

//  debug_r($tmp);
//	strtotime("2011-01-07")
 
//	$display_period = "<tr><td class=\"tabletext\"><b>".$Period_Msg."</b></td></tr>";
	
	$display_period = '<tr><td><table id="periodTableID" style="border:1px solid '.$gray.';" width="1000px" border="0" cellspacing="0" cellpadding="5">
						  <col width="20%"/>
						  <col width="40%"/>
						  <col width="40%"/>
						  <tr class="tabletop"></tr>
						  <tr class="tabletop">
							<td style="'.$_borderBottom.$_borderRight.'">&nbsp;</td>
							<td style="'.$_borderBottom.'">'.$iPort["ReportPeriod"].'</td>
						  </tr>
						  <tr>
							<td style="'.$_borderRight.'">'.$iPort["period"].'</td>
							<td style="">'.$SetSLPPeriod_Msg.'</td>
						  </tr>
					   </table></td></tr>
					  ';
	
//	$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();

	/************* Old Version ****************/		  
//	debug_pr($SLPSettingsArray);
	if(($SetSLPPeriod_startTime!="" && $SetSLPPeriod_startTime!="0000-00-00 00:00:00") && ($SetSLPPeriod_endTime!="" && $SetSLPPeriod_endTime!="0000-00-00 00:00:00"))
	  $Period_Msg = $iPort['SLPperiod_start_end']." ".$SetSLPPeriod_startTime." ".$iPort['to']." ".$SetSLPPeriod_endTime;
	else if($SetSLPPeriod_startTime!="" && $SetSLPPeriod_startTime!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['SLPperiod_start']." ".$SetSLPPeriod_startTime;
	else if($SetSLPPeriod_endTime!="" && $SetSLPPeriod_endTime!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['SLPperiod_end']." ".$SetSLPPeriod_endTime;
	else
	  $Period_Msg = "";
	
//	if(($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00"))
//	  $Period_Msg = $iPort['SLPperiod_start_end']." ".$studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
//	else if($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['SLPperiod_start']." ".$studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"];
//	else if($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['SLPperiod_end']." ".$studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
//	else
//	  $Period_Msg = "";
	$display_period = "<tr><td class=\"tabletext\"><b>".$Period_Msg."</b></td></tr>";

//	$SetSLPPeriod_isAllowed = $studentOleConfigDataArray["SetSLPPeriod_ON"];
//	$SetSLPPeriod_isAllowed = ($SetSLPPeriod_isAllowed=='on')? true:$SetSLPPeriod_isAllowed;
	
	/************* END Old Version ****************/
//}

$newButton ="<a href=\"javascript:newWindow('ole_record_pool_add.php?IntExt=$IntExt', 1)\" class=\"contenttool\">
					<img src=\"$PATH_WRT_ROOT$image_path/$LAYOUT_SKIN/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> $button_new </a>";

$dateNow = date("Y-m-d H:i:s");
//debug_pr($studentOleConfigDataArray);

if($SetSLPPeriod_startTime!='' && $SetSLPPeriod_endTime!='')
{
	$cond =($SetSLPPeriod_startTime<$dateNow && $SetSLPPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SetSLPPeriod_startTime!='' && $SetSLPPeriod_endTime=='')
{
	$cond =($SetSLPPeriod_startTime<$dateNow)&& $SubmissionPeriod_isAllowed==true;
}
else if($SetSLPPeriod_startTime=='' && $SetSLPPeriod_endTime!='')
{
	$cond =($SetSLPPeriod_endTime>$dateNow)&& $SubmissionPeriod_isAllowed==true;
}

if($cond==true)
{
}
else
{
	$newButton = '';
}


?>

<SCRIPT LANGUAGE="Javascript">

function swap_row(jParRowObj, jParRecordID, jParDir)
{
	if(!$('.swap_btn').attr('clickable'))
		return;
	
	StartRowIndex = <?=($IntExt==0)?2:1?>;

	var ParentTable = jParRowObj.parentNode;
	var nRows = ParentTable.rows;
	var SourceRowIndex = jParRowObj.rowIndex;
	var TargetRowIndex = SourceRowIndex + jParDir;
	
	// Check outrange
	if(TargetRowIndex >= nRows.length || TargetRowIndex < StartRowIndex)
		return;

	// Disable button
	$('.swap_btn').removeAttr('clickable');
	
  $.ajax({
    method: "get",
    url: "../ajax/swap_ole_row_ajax.php",
    data: "SourceRecordID="+jParRecordID+"&Direction="+jParDir+"&IntExt=<?=$IntExt?>",
    success: function(html) {
			// Swap row
			for(i=1; i<jParRowObj.cells.length; i++)
			{
				SourceRowCellHTML = jParRowObj.cells[i].innerHTML;
				ParentTable.rows[SourceRowIndex].cells[i].innerHTML = ParentTable.rows[TargetRowIndex].cells[i].innerHTML;
				ParentTable.rows[TargetRowIndex].cells[i].innerHTML = SourceRowCellHTML;
				$('.swap_btn').attr('clickable',true);
			}
    },
    error: function() {
			alert("<?=$ec_warning['update_fail']?>");
		}
  });
}
</SCRIPT>

<FORM name="form1" method="GET">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
<?php if ((strcmp($RecordsAllowed, "inf") == 0 || $RecordsAllowed > 0) && $ck_memberType!="P") { ?>
				<td class="tabletext">
				<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td>
					  <?=$display_period?>
					</td>
				</tr>
				<tr>
					<td>
						<?=$newButton?>
					</td>
					<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
				</tr>
				</table>
				</td>
				<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
<?php } ?>
<?php if($xmsg != "") {echo $xmsg;} ?>
			</tr>
			</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation">
				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
				<!--<a href="<?=(($IntExt==0)?"ole_record.php":"ole_record_ext.php")?>"><?=$ec_iPortfolio['overall_summary']?></a>-->
				<a href="ole_record.php?IntExt=<?=$IntExt?>"><?=$ec_iPortfolio['overall_summary']?></a>
				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle"></span>
				<span class="tabletext"><?=$ec_iPortfolio['ole_set_pool_record']?></span>
				</td>				 
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align="right">		

						
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>				
					<td align="right" valign="bottom">
			
<?php if ($ck_memberType!="P") { ?>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
					

							<table border="0" cellpadding="0" cellspacing="2">
							<tbody>
							<tr>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','ole_record_pool_remove.php')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete'] ?> </a></td>
							</tr>
							</tbody>
							</table>
							
						</td>
						<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
					
					</tr>
					</tbody>
					</table>
<?php } ?>
					</td>
				</tr>
				</table>
					
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
	          		<?php 
	          			$LibTable->display($IntExt) ;
	          		?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
					<?php
					if ($LibTable->navigationHTML!="")
					{
					?>
						<tr class='tablebottom'>
						<td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
						</tr>
					<?php
					}
					?>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>">
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />

<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
