<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();


// Initializing classes
$LibUser = new libuser($UserID);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
if ($ck_memberType=="S")
	$CurrentPage = "Student_ExtOLE";
else if ($ck_memberType=="P")
	$CurrentPage = "Parent_ExtOLE";
$CurrentPageName = "<font size='-2'>".$iPort["external_record"]."</font>";

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

$data = $LibPortfolio->RETURN_OLE_RECORD_BY_RECORDID($record_id);
list($startdate, $enddate, $title, $category, $ele, $role, $hours, $organization, $achievement, $details, $ole_file, $approved_by, $remark, $process_date, $status, $user_id, $submit_type) = $data;

$submit_type = $iPort["external_record"];

# get ELE Array
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";

$statusArr[1] = $ec_iPortfolio['pending'];
$statusArr[2] = $ec_iPortfolio['approved'];
$statusArr[3] = $ec_iPortfolio['rejected'];

# get category display
$CategoryArray = $LibPortfolio->GET_OLR_CATEGORY(0, true);
$category_display = $CategoryArray[trim($category)];

if($startdate=="" || $startdate=="0000-00-00")
	$date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
	$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
	$date_display = $startdate;

$attach_count = 0;
$tmp_arr = explode("\:", $ole_file);
$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$record_id;
$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$record_id;
for ($i=0; $i<sizeof($tmp_arr); $i++)
{
	$attach_file = $tmp_arr[$i];
	if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
	{
		$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
		$attachments_html .= "<a href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count' class='tablelink' >".basename($attach_file)." ($file_size)</a>";
		$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
		$attach_count ++;
	}
}

# load approval settings
$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();


$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">
</SCRIPT>

<FORM name="form1" method="GET">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation">
				<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle"></span>
				<a href="javascript:history.back()"><?=$AllRecords?></a> 
				<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle"></span>
				<span class="tabletext"><?=$title?></span>
				</td>				 
			</tr>
			</table>
			</td>
		</tr>
		
		<tr>
			<td align="right">		
						
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr >
					<td nowrap='nowrap' class='formfieldtitle' ><span class="tabletext"><?php echo $iPort['submission_type']; ?> :</span></td>
					<td class="tabletext" width="80%" ><span class="tabletext"><?=$submit_type?></span></td>
				</tr >
				
				<tr >
					<td nowrap='nowrap' class='formfieldtitle' ><span class="tabletext"><?php echo $ec_iPortfolio['title']; ?> :</span></td>
					<td class="tabletext" width="80%" ><span class="tabletext"><?=$title?></span></td>
				</tr >
				<tr >
					<td nowrap='nowrap' class='formfieldtitle' ><span class="tabletext"><?=$ec_iPortfolio['date']?> :</span></td>
					<td class="tabletext" ><span class="tabletext"><?=$date_display?></span></td>
				</tr>				
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' class='formfieldtitle' ><?php echo $ec_iPortfolio['category']; ?> :</td>
					<td><?=$category_display?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['ole_role']?> :</td>
					<td><?=($role==""?"--":$role)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['organization']?> :</td>
				    <td><?=($organization==""?"--":$organization)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap'><?=$ec_iPortfolio['achievement']?> :</td>
				    <td><?=($achievement==""?"--":$achievement)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td nowrap='nowrap' valign="top" class='formfieldtitle' ><?=$ec_iPortfolio['attachment']?> :</td>
					<td><?=($attachments_html==""?"--":$attachments_html)?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['details']?> :</td>
				    <td><?=($details==""?"--":nl2br($details))?></td>
				</tr>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['status']?> :</td>
					<td><?=($status!='4'?$statusArr[$status]:$ec_iPortfolio['teacher_submit_record'])?></td>
				</tr>
				<?php
				if($ApprovalSetting==1)
				{?>
				<tr valign="middle" class="tabletext">
					<td valign="top" nowrap='nowrap' class='formfieldtitle' ><?=$ec_iPortfolio['approved_by']?> :</td>
					<td><?=(($approved_by==""||$status=="4")?"--":$LibPortfolio->GET_TEACHER_NAME($approved_by))?></td>
				</tr>
				<?php
				}
				?>
				<?php
				if($process_date != "" && $status != 1)
				{?>
				<tr valign="middle" class="tabletext" class='formfieldtitle' >
					<td nowrap='nowrap'><?=$ec_iPortfolio['approval_date']?> :</td>
					<td><?=$process_date?></td>
				</tr>
				<?php
				}
				?>
				<?php
				if($ck_memberType=="T")
				{
				?>
					<tr valign="middle" class="tabletext">
						<td valign="top" nowrap='nowrap'><?=$ec_iPortfolio['school_remark']?> :</td>
						<td><?=($remark==""?"--":nl2br($remark))?></td>
					</tr>
				<?php
				}
				?>
				<tr>
				    <td colspan=2 align=center>
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "history.back();") ?>					
					</td>
				</tr>
				
				</table>
					
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
