<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// Default: no effect on processing OLE Record
$IntExt = 0;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// Initializing classes
$LibUser = new libuser($UserID);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$gray = '#C0C0C0';
$_borderTop = 'border-top: 1px solid '.$gray.';';
$_borderLeft = 'border-left: 1px solid '.$gray.';';
$_borderRight = 'border-right: 1px solid '.$gray.';';
$_borderBottom = 'border-bottom: 1px solid '.$gray.';';

//$IntExt_Setting = $IntExt;
//if ($sys_custom['IPF_OLE_SELECT']['INCLUDE_OUTSIDE_RECORD'])
//{
//	$IntExt = 3;
//	$IntExt_Setting = 0;
//}

// load settings
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($UserID, $IntExt, 1);

// Get PKMS Config data
$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
if(file_exists($portfolio__plms_report_config_file))
{
	list($r_formAllowed, $startdate, $enddate,$allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfRecord) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
	$startdate = trim(unserialize($startdate));
	$enddate = trim(unserialize($enddate));
//	$allowStudentSLP = unserialize($allowStudentPrintPKMSSLP);
	$formArray = unserialize($r_formAllowed);
	
	$new_rec_allow = unserialize($noOfRecord);
	$RecordsAllowed = $new_rec_allow;
	
	$today = date("Y-m-d");
	if($startdate != null && $enddate != null && $startdate <= $enddate){
		$Period_Msg = "<b>".$ec_iPortfolio['ole_pkms_rec_start_from']." ".$startdate." ".$iPort['to']." ".$enddate."</b>";
		
		// check if within edit date range
		if($startdate <= $today && $enddate >= $today){
			$targetRange = true;
		}
		
	}
	
	// check if form is allowed 
	if(!(is_array($formArray) && in_array($StudentLevel[0], $formArray))){
		$notallow = true;
	}
} else {
	$notallow = true;
}

// Access denied
if($notallow){
	intranet_closedb();
	header("Location: ole.php");
}

// Get number of records allowed
if(is_array($OLEAssignedArr)){
	$new_rec_allow = $RecordsAllowed - count($OLEAssignedArr);
	
	if($new_rec_allow < 0){
		$LibPortfolio->RESET_OVERSET_RECORD_SLP($UserID, $RecordsAllowed, $IntExt, 1);
		$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($UserID, $IntExt, 1);
		
		$new_rec_allow = 0;
	}
}




$linterface = new interface_html("iportfolio_default.html");

// set the current page title
//if($IntExt == 1)
//	$CurrentPage = "Student_ExtOLE";
//else

$CurrentPage = "Student_OLE";
//$CurrentPageName = $IntExt == 1 ? "<font size='-2'>".$iPort["external_record"]."</font>" : $iPort['menu']['ole'];
$CurrentPageName = $iPort['menu']['ole'];

//
//if ($sys_custom['IPF_OLE_SELECT']['INCLUDE_OUTSIDE_RECORD'])
//{
//	$CurrentPageName = "<font size='-2'>". $iPort['menu']['ole'] . " &". $iPort["external_record"]."</font>";
//}


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

//# load approval setting
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();
//debug_pr($StudentID);
$vars = "ClassName=$ClassName&StudentID=$UserID";
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else 
{
	header("Location ../index.php");
}

#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";
####################################

$ELEArray = $LibPortfolio->GET_ELE();
$ELECodeArray = array_keys($ELEArray);
if (count($ELECodeArray)>0)
{
	$ELEImplode = implode(",",$ELECodeArray);
}
$ELECount = count($ELECodeArray);

####################################

$swap_control =	"
					CONCAT('
					&nbsp;
					<table cellpadding=0 border=0 cellspacing=0>
						<tr><td><a href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', a.RecordID,', -1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
						<tr><td><a href=\"javascript:;\" onClick=\"swap_row(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode,', a.RecordID,', 1)\"><img src=\"/images/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" border=\"0\" width=\"13\" height=\"13\" /></a></td></tr>
					</table>
					')
				";

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

$LibTable = new libdbtable2007(0, 1, $pageNo);
// Set to PKMS SLP Order
$LibTable->field_array = array("a.PKMSSLPOrder");

if(!empty($OLEAssignedArr))
	$conds = " AND a.RecordID IN (".implode(",",$OLEAssignedArr).")";
else
	$conds = " AND 1 = 2";

$namefield = getNameFieldWithClassNumberByLang("b.");

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("a.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";


//
//if($IntExt == 1)
//	$conds .= " AND c.IntExt = 'EXT'";
//elseif($IntExt == 3)

// Set condition to all recors
$conds .= " AND (c.IntExt = 'INT' OR c.IntExt = 'EXT')";

//else
//	$conds .= " AND c.IntExt = 'INT'";


$sql = "	SELECT
				'{$ELECount}', 
				'{$ELEImplode}',
              	c.Title,
              	IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,'<br />$profiles_to<br />',a.EndDate))) as OLEDate,
              	a.ELE,
			  	$SqlCategoryField			  	
				{$swap_control},
				CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'),
				IF ((Month(a.StartDate)>8), Year(a.StartDate), Year(a.StartDate)-1) AS StartYear
			FROM 
				{$eclass_db}.OLE_STUDENT as a
			LEFT JOIN 
				{$intranet_db}.INTRANET_USER as b
			ON 
				a.ApprovedBy = b.UserID
			LEFT JOIN
				{$eclass_db}.OLE_PROGRAM as c
			ON
				a.ProgramID = c.ProgramID
        	WHERE 
				a.UserID = '$StudentID'
				$conds
			";
//debug($sql);			
			
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = $targetRange? 9:7;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->IsColOff = "iPortfolioOLEPool";
$LibTable->additionalCols = count($ELECodeArray);

$ELEArray = $LibPortfolio->GET_ELE();		

// TABLE COLUMN
//$row_span = ($IntExt == 0 || $IntExt == 3) ? 2 : 1;
$row_span = 2;

$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' align='center' rowspan='$row_span' >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center' rowspan='$row_span' width='100' >".$ec_iPortfolio['title']."</td>\n";
$LibTable->column_list .= "<td rowspan='$row_span' align='center' >".$ec_iPortfolio['date']."/".$ec_iPortfolio['period']."</td>\n";
//if($IntExt == 0 || $IntExt == 3)
$LibTable->column_list .= "<td colspan=\"".$ELECount."\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n";
$LibTable->column_list .= "<td rowspan='$row_span' align='center'  nowrap='nowrap'>".$ec_iPortfolio['category']."</td>\n";
//$LibTable->column_list .= "<td class='tabletop' >".$LibTable->column(5,$ec_iPortfolio['attachment'], 1)."</td>\n";
if($targetRange){
	$LibTable->column_list .= "<td rowspan='$row_span' align='center'  nowrap='nowrap'>&nbsp;</td>\n";
	$LibTable->column_list .= ($ck_memberType=="S") ? "<td rowspan='$row_span' align='center' >".$LibTable->check("record_id[]")."</td>\n" : "";
}
$LibTable->column_list .= "</tr>\n";

//if (($IntExt == 0 || $IntExt == 3) && count($ELECodeArray) > 0)
if (count($ELECodeArray) > 0)
{

	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
//		$CodePos = array_search($ELECode, $ELECodeArray);
//		$TargetField = $CodePos+1;
		$ELEDisplay = $ELETitle;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b>".$ELEDisplay."</b></td>";
	}

	$LibTable->column_list .= "</tr>";				
}
$LibTable->column_array = array(10,10,0,10,0,0,10,0);
	
$ELEArray = $LibPortfolio->GET_ELE();

//////////////////////////////////////////////

$linterface->LAYOUT_START();


if ($msg == "3")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("delete")."</td>";
} 
else if ($msg == "1")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("add")."</td>";
} 
else 
{
	$xmsg = "";
}

$display_period = $Period_Msg;

$newButton ="<a href=\"javascript:newWindow('pkms_record_pool_add.php?IntExt=$IntExt', 1)\" class=\"contenttool\">
					<img src=\"$PATH_WRT_ROOT$image_path/$LAYOUT_SKIN/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> $button_new </a>";

// Show add button or not
if($new_rec_allow > 0 && $targetRange){
//$RecordsAllowed = 10 - count($OLEAssignedArr);
//$new_rec_allow = (strcmp($RecordsAllowed, "inf") == 0 || $RecordsAllowed > 0);
	$cond = true;
}

if($cond==true)
{
}
else
{
	$newButton = '';
}

?>

<SCRIPT LANGUAGE="Javascript">

function swap_row(jParRowObj, jParRecordID, jParDir)
{

	StartRowIndex = <?=($IntExt==0)?2:1?>;

	var ParentTable = jParRowObj.parentNode;
	var nRows = ParentTable.rows;
	var SourceRowIndex = jParRowObj.rowIndex;
	var TargetRowIndex = SourceRowIndex + jParDir;
	
	// Check outrange
	if(TargetRowIndex >= nRows.length || TargetRowIndex < StartRowIndex)
		return;

  $.ajax({
    method: "get",
    url: "../ajax/swap_pkms_ole_row_ajax.php",
    data: "SourceRecordID="+jParRecordID+"&Direction="+jParDir+"&IntExt=<?=$IntExt?>",
    success: function(html) {
			// Swap row
			for(i=1; i<jParRowObj.cells.length; i++)
			{
				SourceRowCellHTML = jParRowObj.cells[i].innerHTML;
				ParentTable.rows[SourceRowIndex].cells[i].innerHTML = ParentTable.rows[TargetRowIndex].cells[i].innerHTML;
				ParentTable.rows[TargetRowIndex].cells[i].innerHTML = SourceRowCellHTML;
			}
    },
    error: function() {
			alert("<?=$ec_warning['update_fail']?>");
		}
  });
}
</SCRIPT>

<FORM name="form1" method="GET">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
<?php if ($ck_memberType!="P") { ?>
				<td class="tabletext">
				<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td>
					  <?=$display_period?>
					</td>
				</tr>
				<tr>
					<td>
						<?=$newButton?>
					</td>
					<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
				</tr>
				</table>
				</td>
				<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
<?php } ?>
<?php if($xmsg != "") {echo $xmsg;} ?>
			</tr>
			</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation">
				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle">
				<a href="ole_record.php?IntExt=<?=$IntExt?>"><?=$ec_iPortfolio['overall_summary']?></a>
				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle"></span>
				<span class="tabletext"><?=$ec_iPortfolio['ole_pkms_set_pool_record']?></span>
				</td>				 
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align="right">		

						
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>				
					<td align="right" valign="bottom">
			
<?php if ($ck_memberType!="P" && $targetRange) { ?>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
					

							<table border="0" cellpadding="0" cellspacing="2">
							<tbody>
							<tr>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','pkms_record_pool_remove.php')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete'] ?> </a></td>
							</tr>
							</tbody>
							</table>
							
						</td>
						<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
					
					</tr>
					</tbody>
					</table>
<?php } ?>
					</td>
				</tr>
				</table>
					
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
	          		<?php 
	          			$LibTable->display($IntExt) ;
	          		?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
					<?php
					if ($LibTable->navigationHTML!="")
					{
					?>
						<tr class='tablebottom'>
						<td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
						</tr>
					<?php
					}
					?>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $LibTable->order; ?>">
<input type="hidden" name="field" value="<?php echo $LibTable->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$LibTable->page_size?>" />

<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
