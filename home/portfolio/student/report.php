<?php
/** [Modification Log] Using By : 
 * *******************************************************
 * 2015-01-13 Bill
 * 				add Ng Wah SAS Report Tab
 * 
 * 2014-07-23 Bill
 * 				display Pui Kui SLP Report Tab
 * 
 * 2013-10-03 Yuen
 * 				display the customization report to student for the customization of YING WA GIRLS' SCHOOL
 * 
 * *******************************************************
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-tabmenu.php");

intranet_auth();
intranet_opendb();

$li_pf = new libpf_sturec();
$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("iportfolio_default.html");

$CurrentPage = "Student_Report"; // fai need to updat
//$CurrentPageName = $iPort['menu']['slp'];
$CurrentPageName = $iPort['menu']['reports'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();


//check teacher setting to avoid student key in the link directly
$objIpfSetting = iportfolio_settings::getInstance();

$luser = new libuser($UserID);
$objiPf = new libportfolio();

$reportFormAllow = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"]);
$showSLPReportPrintingForStduent = $objiPf->showSLPReportPrintingForStduent($luser);
if($showSLPReportPrintingForStduent && $reportFormAllow){
	// do nothing 
}else{
	$showDynamicReportPrintingForStudent = $objiPf->showDynamicReportPrintingForStudent($luser);
	if ($showDynamicReportPrintingForStudent) {
		header("Location: dynamicReport.php");
	}
	else {
		header("Location: /home/portfolio/profile/student_info_student.php");
		exit();
	}
}



//$showSLPReportPrintingForStduent = $objiPf->showSLPReportPrintingForStduent($luser);
//if($showSLPReportPrintingForStduent){
//	// do nothing
//}else{
//	header("Location: /home/portfolio/profile/student_info_student.php");
//	exit();
//}

$TabMenuArr = libpf_tabmenu::getReportTags("StudentLearningProfile");

# Check if student has right to print Pui Kiu SLP report: Display the tab
if($sys_custom['iPf']['pkms']['Report']['SLP'])
{
	$PKMSAccess = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		list($r_formAllowed, $startdate, $enddate, $allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecords) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
		$formArray = unserialize($r_formAllowed);
		$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
		$StudentLevel = $LibPortfolio->getClassLevel($luser->ClassName);
		
		if(is_array($formArray) && in_array($StudentLevel[0], $formArray) && $allowStudentPrint == 1){
			// Allow student to print
			$PKMSAccess = true;
		}
	}
}

if($sys_custom['iPf']['pkms']['Report']['SLP'] && $PKMSAccess){
	// Add Pui Kiu Report Tab
	array_push($TabMenuArr, array("/home/portfolio/student/pkms_report.php",$ec_iPortfolio['pkms_SLP_config'], 0));
}

# Ng Wah SAS Cust & within release period
if($sys_custom['NgWah_SAS'] && $LibPortfolio->showNgWahSASReport($luser)){
//	global $intranet_db, $eclass_db;
//		
//	# Get Class Name
//	$className = trim($luser->ClassName);
//		
//	$sql = "Select YearID From {$intranet_db}.YEAR_CLASS Where ClassTitleEN = '".$className."' AND AcademicYearId = '".Get_Current_Academic_Year_ID()."'";
//	$resultAry = $objiPf->returnVector($sql);
//		
//	# Get Form
//	$currentForm = $resultAry[0];
//		
//	$sql = "Select SettingValue From {$eclass_db}.OLE_SAS_SETTINGS Where SettingName = 'release' AND AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
//	$resultAry = $objiPf->returnVector($sql);
//		
//	$sas_release_date = explode(';;', $resultAry[0]);
//	if(count($sas_release_date) > 0)
//	{
//		for($i = 0; $i<count($sas_release_date); $i++){
//			$stored_value = explode('###', $sas_release_date[$i]);
//				
//			if($currentForm != $stored_value[0])
//			{
//				continue;
//			} 
//			# within allow submit period
//			else 
//			{
//				$today = date('Y-m-d');
//				$target_startdate = explode(' ', $stored_value[1]);
//				$target_enddate = explode(' ', $stored_value[2]);
//					
//				# return
//				if(validatePeriod($stored_value[1], $stored_value[2])){
					// Add Ng Wah SAS Report Tab
					array_push($TabMenuArr, array("/home/portfolio/student/ngwah_sas_report.php", $Lang['iPortfolio']['NgWah_SAS_Report'], 0));
//				}				
//			}
//		}
//	} 
}

$note_to_student = ($sys_custom["ywgs_ole_report"]) ? $ec_warning['reportPrinting_instruction_for_student_ywgs'] : $ec_warning['reportPrinting_instruction_for_student'];


# generate class history table
$StudentID = $UserID;
$class_history_Arr = $li_pf->GET_STUDENT_CLASS_HISTORY($StudentID);

$HistoryArr = array();
for($i=0,$i_max=count($class_history_Arr);$i<$i_max;$i++)
{
	$thisYearID = $class_history_Arr[$i]["AcademicYearID"];
	$HistoryArr[] = $thisYearID;
}
//debug_pr($HistoryArr);

# Generate multiple year checkboxes
$libacademic_year = new academic_year();
$ay_arr = $libacademic_year->Get_All_Year_List($intranet_session_language);
$ay_selection_html = "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
if (!$sys_custom["ywgs_ole_report"])
{
	$ay_selection_html .= "<tr id=\"AYCheckMasterRow\"><td><input type=\"checkbox\" name=\"checkYearAll\" id=\"AYCheckMaster\" value=\"\" onclick=\"checkAllForm(this.checked)\"/> <label for=\"AYCheckMaster\">{$i_Attendance_AllYear}</label></td><td>&nbsp;</td><td>&nbsp;</td></tr>";
}

for($i=0; $i<ceil(count($ay_arr)/3)*3; $i++)
{
  if($i % 3 == 0) {
    $ay_selection_html .= "<tr>";
  }
  
  if(isset($ay_arr[$i])){
  	
  	$thisYear = $ay_arr[$i]['AcademicYearID'];
  	
  	if ($sys_custom["ywgs_ole_report"])
  	{
  		$thisOption = "<td width=\"150\"><input type=\"radio\" name=\"r_academicYearID\" id=\"div_academicYearID_".$ay_arr[$i][0]."\" value=\"".$ay_arr[$i][0]."\" /> <label for=\"div_academicYearID_".$ay_arr[$i][0]."\">".$ay_arr[$i][1]."</label></td>";
  	} else
  	{
  		$thisOption = "<td width=\"150\"><input type=\"checkbox\" name=\"r_academicYearID[]\" id=\"div_academicYearID_".$ay_arr[$i][0]."\" value=\"".$ay_arr[$i][0]."\" /> <label for=\"div_academicYearID_".$ay_arr[$i][0]."\">".$ay_arr[$i][1]."</label></td>";
  	}
  	
  	if($sys_custom['iPf']['SLP_Report']['DisplayAcademicYearInStudentClassHistoryOnly']) {
  		// Student Class History exists in "School Year" settings	
		if(in_array($thisYear,$HistoryArr))
	  	{
	  	    $ay_selection_html .= $thisOption;
	  	}  
  	} else {
  		
  	     $ay_selection_html .= $thisOption;
  	}
  }
  else {
    $ay_selection_html .= "<td width=\"150\">&nbsp;</td>";
  }
  
  if($i % 3 == 2) {
    $ay_selection_html .= "</tr>";
  }
}
$ay_selection_html .= "</table>";


$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkAllForm (flag){

   if (flag == 0)
   {
      $("input[id^='div_academicYearID_']").attr('checked', false);
   }
   else
   {
      $("input[id^='div_academicYearID_']").attr('checked', true);
   }

	return;
}

function checkSubmit()
{
	//r_academicYearID[]
	
	var isCheck = false;
	$("input[name^='r_academicYearID']").each(function(){	 

			isCheck = $(this).is(":checked");
			if(isCheck == true){
				return false;
			}

	});


	if(isCheck==false)
	{
		alert('<?=$Lang['iPortfolio']['Student']['Report']['PlsSelectAcademicYear']?>');
		return false;
	}else{
		return true;
	}	
}

function printCustReport()
{
	var origin_action = document.form1.action;
	document.form1.action = "report_print_cust.php";
	document.form1.submit();
	document.form1.action = origin_action;
}

</script>
<FORM action="report_print.php" method="POST" id="form1" name="form1" target="_blank" onSubmit="return checkSubmit();">
	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr>
			<td colspan="2"> <?=$LibPortfolio->GET_TAB_MENU($TabMenuArr);?>
			</td>
			</tr>
			
			<tr>
			
				<td width="6%">&nbsp;</td>
				<td><br/>
				<?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $note_to_student, "")?>
				<br/>
				
					<table border="0" cellpadding="5" cellspacing="0" width="100%" align="center">
									  <tr>
											<td class="formfieldtitle" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
											<td valign="top"><?=$ay_selection_html?></td>
										</tr>
										<tr>
											<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
											<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
										</tr>
					</table>
					<br/>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr>
							<td align="center">
<?php if ($sys_custom["ywgs_ole_report"]) {?>
								<input class="formbutton" type="submit" value="<?=$Lang['Btn']['Preview']?>" name="btn_print_pdf" id="btn_print_pdf" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">&nbsp;</td>
<?php } else {?>
	
								<input class="formbutton" type="submit" value="<?=$ec_iPortfolio['SLP']['ExportAsPdf']?>" name="btn_print_pdf" id="btn_print_pdf" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">&nbsp;</td>
<?php } ?>

						</tr>
					</table>
				</td>
				</tr>
				
		</tbody>
	</table>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>