<?php

// Modifing by 
/**
 * ##Used by CD Burning
 * $bypass	-- A int to control the accessControl, if the burn if 1(true)
 * it will bypass the accessControl (auth("STP") & $pf->accessControl("growth_scheme")) to get the page content.
 */
$bypass = FALSE;
if($burn == 1)
{
	$bypass = TRUE;

}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_opendb();

?>

</style>
<?php
$lpf = new libpf_sbs();

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);

if(!$bypass){
	$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
	$lpf->ACCESS_CONTROL("growth_scheme");
}

if($ck_memberType == "P")
{
	$user_id = $lpf->getCourseUserID($ck_current_children_id);
}

if($user_id=="")
{
	$user_id = $lpf->getCourseUserID($UserID);
}

$lgs = new growth_scheme();

if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}

$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

$assignment_id  = $phase_obj["parent_id"];
$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];

$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
if(!is_null($handin_obj))
{
	$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
	$phase_obj["status"] = $handin_obj["status"];

	$handin_obj["answer"] = preg_replace('(\r\n|\n)', "<br>", $handin_obj["answer"]);
	$handin_obj["answer"] = str_replace(" ", "&nbsp;", $handin_obj["answer"]);	
}

if($person_right=="NO" || $ViewOnly==1)
{
	$relevant_phase_html = ($NoRelatedPhase==1) ? "" : $lgs->getRelevantPhaseView($phase_obj["answer"], 1);
	$subject_name = ($phase_obj["teacher_subject_id"]!="") ? $lgs->getSubjectName($phase_obj["teacher_subject_id"]) : "";
	switch ($phase_obj["person_response"])
	{
		case "CT":
				$target_person_html = $usertype_ct;
				break;
		case "P":
				$target_person_html = $usertype_p;
				break;
		case "ST":
				$target_person_html = $usertype_st." (".$subject_name.")";
				break;
		case "S":
		DEFAULT:
				$target_person_html = $usertype_s;
				break;
	}
	
	?>
	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr><td colspan="3" align="center" class="16Gray"><?=$student_name_html?></td></tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		<br><table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_phase_title'] ?>:</td>
          <td><?=$phase_obj["title"]?></td>
        </tr>
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_description'] ?>:</td>
            <td><?=nl2br($phase_obj["instruction"])?></td>
        </tr>
        <tr>
          <td valign="top" nowrap class="tabletext"><?= $ec_iPortfolio['growth_target_person'] ?>:</td>
          <td><?=$target_person_html?>
          </td>
        </tr>
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_phase_period'] ?>:</td>
          <td><?= $phase_obj["starttime"] . " {$profiles_to} " . $phase_obj["deadline"] ?>
	    </td>
        </tr>
        <tr>
          <td valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_relevant_phase'] ?>:</td>
          <td><?=$relevant_phase_html?>
          </td>
        </tr>
        <tr>
          <td colspan="2" valign="top" class="tabletext" nowrap><?= $ec_iPortfolio['growth_online_form'] ?>:<br>
		<form name="ansForm" method="post" action="update.php">
			<input type=hidden name="qStr" value="<?=$phase_obj["answersheet"]?>">
			<input type=hidden name="aStr" value="">
		</form>
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>

		<script language="Javascript">
		answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
		answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
		answersheet_header = "<?=$ec_form_word['question_title']?>";
		answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
		no_options_for = "<?=$ec_form_word['no_options_for']?>";
		pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
		pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
		chg_title = "<?=$ec_form_word['change_heading']?>";
		chg_template = "<?=$ec_form_word['confirm_to_template']?>";

		answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
		answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
		answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
		answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
		answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
		answersheet_option = "<?=$answersheet_option?>";
		answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";
		
		/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

		button_submit = " <?=$button_submit?> ";
		button_add = " <?=$button_add?> ";
		button_cancel = " <?=$button_cancel?> ";
		button_update = " <?=$button_update?> ";
		background_image = "";

		// added by KELLY
		submit_action = " onSubmit=\"return false\"";

		var sheet = new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		var tmpStr = document.ansForm.qStr.value;
		//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
		sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
		sheet.mode = 1;	// 0:edit 1:fill in application
		sheet.displaymode = <?=$phase_obj["sheettype"]?>;	// 0:edit 1:fill in application
		sheet.answer = sheet.sheetArr();

		// temp
		var form_templates = new Array();

		sheet.templates = form_templates;
		document.write(editPanel());
		</script>
		</td></tr>
        </table>
<?php
}
else
{
	$relevant_phase_html = ($NoRelatedPhase==1) ? "" : $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $user_id, 1);
	
	# Eric Yip (20090918) : Show student info disregarding user type
//	if ($ck_memberType!="S")
//	{		# get student info table
		$StudentInfoTable = ($NoStudentInfo!=1) ? $lgs->getUserInfoTable($user_id, $NoPhoto, $NoStudentInfo) : "";
		if($NoPhoto!=1)
		{
			$StudentPhoto = $lgs->getStudentPhoto($user_id);
			$user_photo_js = "<table border=0><tr>";
			$user_photo_js .= "<td>".str_replace('"', '\"', $StudentPhoto)."</td>";
			$user_photo_js .= ($StudentInfoTable!="") ? "<td>".$StudentInfoTable."</td>" : "";
			$user_photo_js .= "</tr></table>";
		}
		else
			$user_photo_js = $StudentInfoTable;	
//	}
	?>
	<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>
	<script language="Javascript">
		var form_title_main = "<?=$phase_obj['title']?>";
		var form_description = "<?=preg_replace('(\r\n|\n)', '<br>', $phase_obj['instruction'])?>";
		var form_reference = "<?=$relevant_phase_html?>";
		var form_user_photo = "<?=$user_photo_js?>";
		var print_report = 1;
		var question_scale = "<?=$ec_form_word['question_scale']?>";
		var question_question = "<?=$ec_form_word['question_question']?>";
	</script>
	<?php
	if($phase_obj["answer_display_mode"]==1)
	{
		$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);
		?>
		<form name="ansForm" method="post">
			<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
			<input type="hidden" name="aStr" value="<?=str_replace("&nbsp;", " ", $handin_obj["answer"])?>">
		</form>
			
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>

		<script language="Javascript">
		answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
		answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
		answersheet_header = "<?=$ec_form_word['question_title']?>";
		answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
		no_options_for = "<?=$ec_form_word['no_options_for']?>";
		pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
		pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
		chg_title = "<?=$ec_form_word['change_heading']?>";
		chg_template = "<?=$ec_form_word['confirm_to_template']?>";

		answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
		answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
		answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
		answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
		answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
		answersheet_option = "<?=$answersheet_option?>";
		answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";
		
/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

		button_submit = " <?=$button_submit?> ";
		button_add = " <?=$button_add?> ";
		button_cancel = " <?=$button_cancel?> ";
		button_update = " <?=$button_update?> ";

		background_image = "";

		// added by KELLY
		var submit_action = " onSubmit=\"return false\"";
		var display_answer_form = 1;
		var image_path = "<?=$image_path?>";

		var sheet = new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		var tmpStr = document.ansForm.qStr.value;
		var tmpStrA = document.ansForm.aStr.value;
		//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
		//sheet.aString = tmpStrA.replace(/\"/g, "&quot;");
		sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
		sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
		sheet.mode = 1;	// 0:edit 1:fill in application
		sheet.displaymode = <?=$phase_obj["sheettype"]?>;
		sheet.answer = sheet.sheetArr();

		// temp
		var form_templates = new Array();

		sheet.templates = form_templates;
		document.write(editPanel());
		</script>
	<?php
	}
	else
	{
		$phase_obj["answersheet"] = str_replace("\"", "\\\"", undo_htmlspecialchars($phase_obj["answersheet"]));
		$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $phase_obj["answersheet"])) : preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $phase_obj["answersheet"]));

		$handin_obj['answer'] = str_replace("&nbsp;", " ", $handin_obj['answer']);
		?>
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_view.js"></script>
		<script language="Javascript">
			var myQue = "<?=$phase_obj["answersheet"]?>";
			var myAns = "<?=preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $handin_obj["answer"]))?>";
			var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
			var answer_display_mode = "<?=$phase_obj["answer_display_mode"]?>";

			var displaymode = <?=$phase_obj["sheettype"]?>;
			document.write(viewForm(myQue, myAns));
		</script>
	<?php
	}
}

intranet_closedb();
?>