<?php
// Using: Pun

ini_set('memory_limit','128M');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

include_once($PATH_WRT_ROOT.'kis/init.php');
$libkis_iportfolio = $libkis->loadApp('iportfolio');


intranet_opendb();

######## Init START ########
$lpf = new libpf_sbs();

$mPDF = new mPDF('','A4',0,'',15,15,5,5);

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);

if($ck_memberType == "P")
{
	$user_id = $lpf->getCourseUserID($ck_current_children_id);
}

if($user_id=="")
{
	$user_id = $lpf->getCourseUserID($UserID);
}
######## Init END ########


######## Access Right START ########
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");


$lgs = new growth_scheme();

if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}
######## Access Right END ########


######## Student Info START ########
$StudentIDArr = array();
$StudentIDArr[] = $user_id;
$UserArr = $lgs->getHandedinUserInfo($StudentIDArr);

$nameSql = getNameFieldByLang2("iu.");
$sql = "SELECT 
	iu.UserID,
	{$nameSql} as Name,
	iu.ClassName,
	iu.ClassNumber,
	DATE(iu.DateOfBirth) as birthday,
	iu.Gender as gender
FROM 
	{$lpf->course_db}.usermaster um
INNER JOIN 
	{$intranet_db}.INTRANET_USER iu
ON 
	um.user_email = iu.UserEmail
WHERE
	um.user_id = '{$user_id}'
	";
$rs = 	$lpf->returnResultSET($sql);
$user = $rs[0];

$studentId = $user['UserID'];
$studentName = $user['Name'];
$studentClass = $user['ClassName'];
$studentNumber = $user['ClassNumber'];
$studentDOB = date("Y/m/d", strtotime($user['birthday']));
$studentGender = $user['gender'];
######## Student Info END ########


######## Student Portfolio Info START ########
$data = array();
$data['studentId'] = $studentId;
$data['school_year_id'] = $school_year_id;
$data['school_year_term_id'] = $school_year_term_id;
$portfolioInfo = $libkis_iportfolio->getPortfolioRecord($data);

$semesterArr = getSemesters($school_year_id);
$semesterIdArr = array_keys($semesterArr);
$isLastSemester = ($school_year_term_id == $semesterIdArr[ count($semesterIdArr)-1 ]);

if($printDate){
	$printDate = date("Y/m/d", strtotime($printDate));
	$printDateWeekDay = date("w", strtotime($printDate));
}
######## Student Portfolio Info END ########

######## Set empty string width START ########
if(count($portfolioInfo) == 0){
	$portfolioInfo['year_name'] = //
	$portfolioInfo['StartAgeYear'] = //
	$portfolioInfo['StartAgeMonth'] = //
	$portfolioInfo['StartWeight'] = //
	$portfolioInfo['StartHeight'] = //
	$portfolioInfo['EndAgeYear'] = //
	$portfolioInfo['EndAgeMonth'] = //
	$portfolioInfo['EndWeight'] = //
	$portfolioInfo['EndHeight'] = //
	$portfolioInfo['TotalSchoolDay'] = //
	$portfolioInfo['PresentDay'] = //
	$portfolioInfo['SickLeaveDay'] = //
	$portfolioInfo['OtherLeaveDay'] = //
	$portfolioInfo['LateDay'] = //
	$portfolioInfo['EarlyLeaveDay'] = $emptyStr;
}else{
	foreach($portfolioInfo as $key=>$value){
		if($value == ''){
			$portfolioInfo[$key] = $emptyStr;
		}
	}
}
######## Set empty string width END ########

######## Get data START ########
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
######## Get data END ########


######## Decode Ansersheet START ########
$answersheetArr = explode('#QUE#', $phase_obj['answersheet']);
array_shift($answersheetArr);
$questionArr = array();
$categoryArr = array();
$subcategoryArr = array();

$lastCategoryId = -1;
$lastSubCategoryId = -1;
for($i=0,$iMax=count($answersheetArr);$i<$iMax;$i++){
	$_questions = explode('||', $answersheetArr[$i]);
	$typeNum = explode(',', $_questions[0]); // $typeNum[0]: Question Type , $typeNum[>0]: Number of options
	$title = $_questions[1];
	$options = $_questions[2];
	
	if($typeNum[0] == '8'){ // Category
		$lastCategoryId++;
		$categoryArr[$lastCategoryId] = $title;
		continue;
	}else if($typeNum[0] == '6'){ // Sub-Category, Options
		$lastSubCategoryId++;
		$subcategoryArr[$lastSubCategoryId] = $title;
		$optionsArr = explode('#OPT#', $options);
		array_shift($optionsArr); // Remove the first(empty) item
		
		$itmArr = array();
		$optArr = array();
		for($j=0, $jMax=count($optionsArr);$j<$jMax;$j++){ // item
			if( ($j) < $typeNum[1]){
				$itmArr[] = $optionsArr[$j];
			}else{
				$optArr[] = $optionsArr[$j];
			}
		}
		
		$questionArr[$lastCategoryId][$lastSubCategoryId] = array(
			'items' => $itmArr,
			'options' => $optArr,
		);
	}
}
######## Decode Ansersheet END ########


######## Decode Anser START ########
$_answerArr = explode('#ANS#', $handin_obj['answer']);
array_shift($_answerArr);
$answerArr = array();

$lastCategoryId = -1;
$lastSubCategoryId = -1;
for($i=0,$iMax=count($_answerArr);$i<$iMax;$i++){
	if($_answerArr[$i] == ''){
		$lastCategoryId++;
		continue;
	}else{
		$lastSubCategoryId++;
		
		if(strcasecmp($_answerArr[$i], 'NULL') == 0){
			$optionCount = count($questionArr[$lastCategoryId][$lastSubCategoryId]['items']);
			for($j=0;$j<$optionCount;$j++){
				$answerArr[$lastCategoryId][$lastSubCategoryId][] = -1;
			}
		}else{
			$answerArr[$lastCategoryId][$lastSubCategoryId] = explode(',', $_answerArr[$i]);
		}
	}
}
######## Decode Anser END ########


######## Output START ########
ob_start();
?>
<style>
	@page{
		/*background-image: url("/file/test/PortfolioData.jpg");*/
	}
	body{
		font-family: msjh !important;
	}
	
	.portfolioTable{
		border-collapse:collapse;
	}
	/*.portfolioTable td { border: 0px solid red; }
	.portfolioTable td 
	{
	    text-align:center; 
	    vertical-align:middle;
	}*/
	
	.td_title{
		 text-align:left; 
	}
	.td_content{
		 vertical-align:bottom;
	}
	.td_l{
		border-left:0.1px solid black;
	}
	.td_t{
		border-top:0.1px solid black;
	}
	.td_r{
		border-right:0.1px solid black;
	}
	.td_b{
		border-bottom:0.1px solid black;
	}
</style>

&nbsp;
<div style="text-align:center; margin-top: 95mm; font-size: 25px"><?=$portfolioInfo['year_name']?></div>


<table class="portfolioTable" style="margin-top: 25mm; margin-left: 0mm;">
	<tr>
		<td class="td_b td_title" style="width: 17mm; vertical-align:top;height: 10mm;font-size: 12px;">學生姓名</td>
		<td class="td_b" colspan="7" style="font-size: 20px;vertical-align:bottom;"><?=$studentName ?>&nbsp;</td>
	</tr>
	
	<tr>
		<td class="td_b td_title" style="width: 15mm; vertical-align:center;font-size: 12px;">班別</td>
		<td class="td_b td_r td_content" style="width: 29mm;height: 7mm;"><?=$studentClass ?>&nbsp;</td>
		
		<td class="td_b td_title" style="width: 15mm; vertical-align:center;font-size: 12px;">學號</td>
		<td class="td_b td_r td_content" style="width: 30mm"><?=$studentNumber ?>&nbsp;</td>
		
		<td class="td_b td_title" style="width: 15mm; vertical-align:center;font-size: 12px;">性別</td>
		<td class="td_b td_r td_content" style="width: 30mm">
			<?php
			if($studentGender == 'M'){
				echo $Lang['General']['Male'];
			}else if($studentGender == 'F'){
				echo $Lang['General']['Female'];
			}
			?>&nbsp;
		</td>
		
		<td class="td_b td_title" style="width: 21mm; vertical-align:top;font-size: 12px;">出生日期</td>
		<td class="td_b td_content" style="width: 22mm; "><?=$studentDOB ?>&nbsp;</td>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 17mm; margin-left: 0mm;">
	<tr>
		<td class="td_b td_r td_title" style="width: 90mm; text-align: center;font-size: 12px;" colspan="3">開學</td>
		<td class="td_b td_title" style="width: 90mm; text-align: center;font-size: 12px;" colspan="3">散學</td>
	</tr>
	
	<tr>
		<td class="td_title" style="width: 30mm; text-align: center;font-size: 12px;">年齡</td>
		<td class="td_title" style="width: 30mm; text-align: center;font-size: 12px;">體重</td>
		<td class="td_r td_title" style="width: 30mm; text-align: center;font-size: 12px;">體高</td>
		<td class="td_title" style="width: 30mm; text-align: center;font-size: 12px;">年齡</td>
		<td class="td_title" style="width: 30mm; text-align: center;font-size: 12px;">體重</td>
		<td class="td_title" style="width: 30mm; text-align: center;font-size: 12px;">體高</td>
	</tr>
	
	<tr>
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['StartAgeYear'] ?>&nbsp;歲&nbsp;<?=$portfolioInfo['StartAgeMonth'] ?>&nbsp;個月
		</td>
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['StartWeight'] ?>&nbsp;千克
		</td>
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['StartHeight'] ?>&nbsp;厘米
		</td>
		
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['EndAgeYear'] ?>&nbsp;歲&nbsp;<?=$portfolioInfo['EndAgeMonth'] ?>&nbsp;個月
		</td>
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['EndWeight'] ?>&nbsp;千克
		</td>
		<td class="td_l td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			<?=$portfolioInfo['EndHeight'] ?>&nbsp;厘米
		</td>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 4mm; margin-left: 0mm;">
	<tr>
		<td class="td_l td_b td_r td_title" style="width: 36mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			本學期授課&nbsp;<?=$portfolioInfo['TotalSchoolDay'] ?>&nbsp;天
		</td>
		<td class="td_l td_b td_r td_title" style="width: 35mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			該生上課&nbsp;<?=$portfolioInfo['PresentDay'] ?>&nbsp;天
		</td>
		<td class="td_l td_b td_r td_title" style="width: 27mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			病假&nbsp;<?=$portfolioInfo['SickLeaveDay'] ?>&nbsp;天
		</td>
		<td class="td_l td_b td_r td_title" style="width: 27mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			事假&nbsp;<?=$portfolioInfo['OtherLeaveDay'] ?>&nbsp;天
		</td>
		<td class="td_l td_b td_r td_title" style="width: 27mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			遲到&nbsp;<?=$portfolioInfo['LateDay'] ?>&nbsp;天
		</td>
		<td class="td_l td_b td_r td_title" style="width: 27mm;height: 25px; text-align: center;vertical-align: bottom;font-size: 12px;">
			早退&nbsp;<?=$portfolioInfo['EarlyLeaveDay'] ?>&nbsp;天
		</td>
	</tr>
</table>


<table class="portfolioTable" style="width: 100%;margin-top: 4mm;width:100%">
	<tr>
		<?php if($isLastSemester){ ?>
			<td class="td_l td_b" style="text-align:left;vertical-align: bottom;width: 10mm; font-size: 12px;">&nbsp;&nbsp;獎項</td>
			<td class="td_b td_r td_title" style="height: 26px; text-align: center;vertical-align: bottom;font-size: 15px;">
				<?=$portfolioInfo['Award']?>&nbsp;
			</td>
		<?php }else{ ?>
			<td style="text-align:center;">&nbsp;</td>
		<?php } ?>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 4mm; margin-left: 0mm;width:100%">
	<tr>
		<td class="td_l td_b" style="text-align:left;vertical-align: bottom;width: 10mm;height: 12mm; font-size: 12px;">&nbsp;&nbsp;特錄</td>
		<td class="td_b td_r td_title" style="height: 25px; text-align: center;vertical-align: bottom;font-size: 15px;">
			<?=$portfolioInfo['Remark']?>&nbsp;
		</td>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 14mm; margin-left: 0mm; width:100%">
	<tr>
		<td class="td_t" style="text-align:left;vertical-align: top;width: 45mm; font-size: 12px;">校監</td>
		<td class="td_t" style="text-align:left;vertical-align: top;width: 45mm; font-size: 12px;">校長</td>
		<td class="td_t" style="text-align:left;vertical-align: top; font-size: 12px;">老師</td>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 13mm; margin-left: 0mm; width:100%">
	<tr>
		<!--td class="td_b td_r" style="text-align:left;vertical-align: top;width: 12mm; font-size: 3px;">&nbsp;</td>
		<td class="td_b td_r" style="text-align:left;vertical-align: top;width: 24mm; font-size: 3px;">&nbsp;</td>
		<td class="td_b td_r" style="text-align:left;vertical-align: top; font-size: 3px;">&nbsp;</td-->
		<td class="td_b" style="font-size: 3px;">&nbsp;</td>
	</tr>
</table>


<table class="portfolioTable" style="margin-top: 1mm; margin-left: 0mm; width:100%;">
	<tr>
		<td class="td_b" style="height:10mm;text-align:left;vertical-align: top;width: 20mm; font-size: 13px;">日期</td>
		<td class="td_b" style="text-align:left;vertical-align: bottom; font-size: 20px;">
			<?php
			if($printDate){
				echo "{$printDate} ({$Lang['SysMgr']['Homework']['WeekDay'][$printDateWeekDay]})";
			}
			?>&nbsp;
		</td>
		<td class="td_b" style="text-align:left;vertical-align: top;width: 60mm; font-size: 13px;">家長簽署</td>
	</tr>
</table>


<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
######## PDF Content-Page Settings START ########
// $margin_top = '7';
$mPDF->mirrorMargins = 1;
$mPDF->SetColumns(2);

$header = "
	學生姓名：<span style=\"text-decoration: underline;\">&nbsp;{$studentName}&nbsp;</span>&nbsp;&nbsp;&nbsp;
	班別：<span style=\"text-decoration: underline;\">&nbsp;{$studentClass}&nbsp;</span>&nbsp;&nbsp;&nbsp;
	學號：<span style=\"text-decoration: underline;\">&nbsp;{$studentNumber}&nbsp;</span>
";
$mPDF->SetHTMLHeader("<div style=\"width:100%; text-align:center;\">{$header}</div>",'E');
$mPDF->SetHTMLHeader("<div style=\"width:100%; text-align:center;\">{$header}</div>",'O');

$mPDF->AddPage();

$footer = '(1) 未能做到&nbsp;&nbsp;&nbsp;(2) 偶爾能做到&nbsp;&nbsp;&nbsp;(3) 間中能做到&nbsp;&nbsp;&nbsp;(4) 經常能做到&nbsp;&nbsp;&nbsp;(5) 完全做到';

$mPDF->SetHTMLFooter("
	<div style=\"width:90%; text-align:center;float:left;\">{$footer}</div>
	<span style=\"float:right\">第 {PAGENO} 頁</span>
",'O');
$mPDF->SetHTMLFooter("
	<div style=\"width:90%; text-align:center;float:left;\">{$footer}</div>
	<span style=\"float:right\">第 {PAGENO} 頁</span>
",'E');
######## PDF Content-Page Settings END ########


######## Output START ########
$row = -1;
$rowPerPage = 39;
$hasNewColumn = false;

ob_start();
?>

<style>
	body{
		font-family: msjh !important;
	}
	.alignLeft{
		width: 80mm;
	}
	
	.categoryRow{
		border-bottom: 2 solid black;
	}
	.category{
		font-weight: bold;
	}
	
	.subCategoryRow{
		width: 100%;
		padding-left: 3mm;
		border-bottom: 1 solid black;
	}
	
	.itemRow{
		padding-left: 6mm;
		border-bottom: 1 solid black;
	}
</style>

<br />
<?php 
foreach ($questionArr as $CategoryId=>$d1){ 
	if($row++ == $rowPerPage){
		echo '<columnbreak /><br /><br />';
		$row = 1;
		$hasNewColumn = true;
	}else{
		echo '<br />';
		$row++;
	}
?>

	<!-- ######## Category START ######## -->
	<table class="categoryRow">
		<tr>
			<td class="alignLeft category"><?=$categoryArr[$CategoryId]?></td>
			<td class="alignRight" style="white-space: nowrap">表現</td>
		</tr>
	</table>
	<!-- ######## Category END ######## -->

	<!-- ######## SubCategory START ######## -->
	<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			if($row++ == $rowPerPage){
				echo '<columnbreak /><br /><br />';
				$row = 1;
				$hasNewColumn = true;
			}
			$items = $question['items'];
			$options = $question['options'];
	?>
		<table class="subCategoryRow">
			<tr>
				<td>
					<div><?=$subcategoryArr[$SubCategoryId]?></div>
				</td>
			</tr>
		</table>
	<!-- ######## SubCategory END ######## -->
	
		<?php 
		foreach ($items as $index=>$item){ 
			if($row++ == $rowPerPage){
				echo '<columnbreak /><br /><br />';
				$row = 1;
				$hasNewColumn = true;
			}
		?>
		<!-- ######## Item START ######## -->
			<table class="itemRow">
				<tr>
					<td class="alignLeft"><?=$item?></td>
					<td class="alignRight">
					<?php 
					if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
						echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
					}else{
						echo '';
					}
					?>
					</td>
				</tr>
			</table>
		<!-- ######## Item END ######## -->
		
		<?php 
		} 
		?>
	<?php 
	} 
	?>
<?php 
} 

if(!$hasNewColumn){ // If don't echo these <br .>, the rows will balanced. 
	for($i=0;$i<$rowPerPage;$i++){
		echo '<br />&nbsp;';
	}
}

?>



<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();


exit;
####################### END ########################
?>

........................THE CODE BELOW ARE OLD CODE AND WILL NOT USE......................



<style>
	div{
		width: 100%;
		border: 1;
	}
	.alignLeft{
		color:blue;
		float:left;
		width: 70%;
	}
	.alignRight{
		color:red;
		float:right;
		width: 20%;
	}
</style>

<?php foreach ($questionArr as $CategoryId=>$d1){ ?>

	<!-- ######## Category START ######## -->
	<div class="categoryRow">
		<div class="alignLeft"><?=$categoryArr[$CategoryId]?></div>
		<div class="alignRight">abc</div>
	</div>
	<!-- ######## Category END ######## -->

	<!-- ######## SubCategory START ######## -->
	<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			$items = $question['items'];
			$options = $question['options'];
	?>
		<div class="subCategoryRow">
			<div><?=$subcategoryArr[$SubCategoryId]?></div>
		</div>
	<!-- ######## SubCategory END ######## -->
	
		<?php foreach ($items as $index=>$item){ ?>
		<!-- ######## Item START ######## -->
			<div class="itemRow">
				<div class="alignLeft"><?=$item?></div>
				<div class="alignRight">
					<?php 
					if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
						echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
					}else{
						echo '';
					}
					?>
				</div>
			</div>
		<!-- ######## Item END ######## -->
		
		<?php } ?>
	<?php } ?>
<?php } ?>

<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();


exit;
####################### END ########################
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		.subCatRow > td > span{
			color:red;
		}
	</style>
</head>
<body>
<?php foreach ($questionArr as $CategoryId=>$d1){ ?>
	<table border="1" style="width: 100%">
		<tr>
			<th>
				<span><?=$categoryArr[$CategoryId]?></span>
			</th>
			<th style="width: 15mm">
				<span>&nbsp;</span>
			</th>
		</tr>
		
		<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			$items = $question['items'];
			$options = $question['options'];
		?>
			<tr class="subCatRow">
				<td>
					<span><?=$subcategoryArr[$SubCategoryId]?></span>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<?php foreach ($items as $index=>$item){ ?>
				<tr class="itemRow">
					<td>
						<span><?=$item?></span>
					</td>
					<td>
						<span>
							<?php 
							if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
								echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
							}else{
								echo '';
							}
							?>
						</span>
					</td>
				</tr>
			<?php } ?>
		<?php 
		} 
		?>
	</table>
<?php } ?>
</body>
</html>
<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();
?>