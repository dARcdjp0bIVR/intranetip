<?php

// Modifing by

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("STP");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_opendb();

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);

$lpf = new libpf_sbs();
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lgs = new growth_scheme();
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$assignment_id = $phase_obj["parent_id"];

$assignment_obj = $lgs->getSchemeInfo($assignment_id);
$assignment_obj = $assignment_obj[0];

$QuestionOnly = 1;

// Student ID
//$ParUserID = $UserID;
$ParUserID = $lpf->IP_USER_ID_TO_EC_USER_ID($UserID);
# relevant phase preparation
if($ParUserID == "")
$relevant_phase_html = $lgs->getRelevantPhaseView($phase_obj["answer"]);
else
{
	$handin_obj = $lgs->getStudentPhaseHandin($ParUserID, $phase_obj);
	$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $ParUserID);
	$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
	$phase_obj["status"] = $handin_obj["status"];
}

$phase_obj["answersheet"] = str_replace('src="/files/', "", $phase_obj["answersheet"]);

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

$WarningMsg = ($person_right=="VIEW_DOING") ? "<tr><td valign='top'>&nbsp;</td><td class='text_12_red' colspan='2'>".$ec_iPortfolio['phase_in_processing_state']."</td></tr>" : "";

//debug_r($relevant_phase_html);

# define photo showing 
if($ck_memberType=="T")
{
	$StudentPhoto = $lgs->getStudentPhoto($ParUserID);
	$user_photo_js = str_replace('"', '\"', $StudentPhoto)."<br>".str_replace("'", "\'", $student_name_html);
	$student_name_html = "";
}
else
{
	$student_name_html = "<tr><td colspan='3' align='center' class='16Gray' height='30'>".$student_name_html."</td></tr>";
}

?>

<script language="Javascript">
function jClose()
{
	self.close();	
}

function jPrint(phase_id, assignment_id)
{
	var url = "print_phase.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id;
	
	newWindow(url, 28);
}

function viewPhaseAnswer(phase_id, user_id)
{
	newWindow("<?=$PATH_WRT_ROOT?>home/portfolio/student/sbs/view_phase_popup.php?user_id="+user_id+"&phase_id="+phase_id, 27);
}

function jRedo()
{
	
}

</script>


<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
<?=$WarningMsg?>
<?=$student_name_html?>
<tr>
<td>&nbsp;</td>
<td>
<br>
<?php
if ($person_right=="VIEW" || $person_right=="VIEW_DOING")
{
	$handin_obj["answer"] = preg_replace('(\r\n|\n)', "<br>", $handin_obj["answer"]);
	$handin_obj["answer"] = str_replace(" ", "&nbsp;", $handin_obj["answer"]);	
?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>home/portfolio/js/layer.js"></script>

<script language="Javascript">
	var form_title_main = "<?=$phase_obj['title']?>";
	var form_description = "<?=preg_replace('(\r\n|\n)', "<br>", $phase_obj['instruction'])?>";
	var form_reference = "<?=addslashes($relevant_phase_html);?>";
	var form_user_photo = "<?=$user_photo_js?>";
</script>

	<?php
	if($phase_obj["answer_display_mode"]==1)
	{
		$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);
		?>
		<form name="ansForm" method="post">
			<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
			<input type="hidden" name="aStr" value="<?=str_replace("&nbsp;", " ", $handin_obj["answer"])?>">
		</form>
		
		<script language="javascript" src="<?=$PATH_WRT_ROOT?>home/portfolio/js/online_form_edit.js"></script>

		<script language="Javascript">
		answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
		answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
		answersheet_header = "<?=$ec_form_word['question_title']?>";
		answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
		no_options_for = "<?=$ec_form_word['no_options_for']?>";
		pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
		pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
		chg_title = "<?=$ec_form_word['change_heading']?>";
		chg_template = "<?=$ec_form_word['confirm_to_template']?>";

		answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
		answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
		answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
		answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
		answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
		answersheet_option = "<?=$answersheet_option?>";
		answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

		button_submit = " <?=$button_submit?> ";
		button_add = " <?=$button_add?> ";
		button_cancel = " <?=$button_cancel?> ";
		button_update = " <?=$button_update?> ";

		background_image = "";

		// added by KELLY
		var submit_action = " onSubmit=\"return false\"";
		var display_answer_form = 1;
		var image_path = "<?=$image_path?>";

		var sheet = new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		var tmpStr = document.ansForm.qStr.value;
		var tmpStrA = document.ansForm.aStr.value;
		//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
		//sheet.aString = tmpStrA.replace(/\"/g, "&quot;");
		sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
		sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
		sheet.mode = 1;	// 0:edit 1:fill in application
		sheet.displaymode = <?=$phase_obj["sheettype"]?>;
		sheet.answer = sheet.sheetArr();

		// temp
		var form_templates = new Array();

		sheet.templates = form_templates;
		document.write(editPanel());
		</script>

	<?php
	}
	else
	{
		$phase_obj["answersheet"] = str_replace("\"", "\\\"", undo_htmlspecialchars($phase_obj["answersheet"]));
		$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $phase_obj["answersheet"])) : preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $phase_obj["answersheet"]));
		?>

		<script language="javascript" src="<?=$PATH_WRT_ROOT?>home/portfolio/js/online_form_view.js"></script>
		
		<script language="Javascript">
		var myQue = "<?=$phase_obj["answersheet"]?>";
		var myAns = "<?=preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $handin_obj["answer"]))?>";
		var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";

		var displaymode = <?=$phase_obj["sheettype"]?>;
		document.write(viewForm(myQue, myAns));
		</script>
	<?php
	}
} else
{
	# block if not in fill in status
	echo $ec_warning['no_view_now']."<p></p>";
}
?>
</td>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%">
<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<form name="redoForm" method="post" action="phase_redo_accept.php">
<table>
<tr>
    <td>&nbsp;</td>
    <td align="center">
			<?=$linterface->GET_ACTION_BTN($iPort["btn"]["print"], "button", "jPrint($phase_id, $assignment_id);", "btn_print");?>
			<?=$linterface->GET_ACTION_BTN($button_close, "button", "jClose();", "btn_close");?>
    </td>
    <td>&nbsp;</td>
</tr>
</table>

<input type="hidden" name="ParUserID" value="<?=$ParUserID?>" />
<input type="hidden" name="ParStudentID" value="<?=$StudentID?>" />
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>" />
<input type="hidden" name="phase_id" value="<?=$phase_id?>" />
<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>" />
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>