<?php

# Modifing by Siuwan

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_auth();
intranet_opendb();

// template for student page
$linterface = new interface_html("iportfolio_default4.html");
// set the current page title
$CurrentPage = "Student_SchoolBasedScheme";
$CurrentPageName = $iPort['menu']['school_based_scheme'];

$assignment_id = IntegerSafe($assignment_id);
$parent_id = IntegerSafe($parent_id);


### set Library ###
$lgs = new growth_scheme();
$luser = new libuser($UserID);
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lo = new libportfolio_group($lpf->course_db);

if($ck_memberType == "P")
$user_id = $lpf->getCourseUserID($ck_current_children_id);
else
$user_id = $lpf->getCourseUserID($UserID);

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

if (!in_array($user_id, $lgs->getSchemeUsers($parent_id))) {
	No_Access_Right_Pop_Up();
}


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

////////////////// Gen Content //////////////////
$phase_obj = $lgs->getPhaseInfo($assignment_id); // assignment_id also is phase_id
$phase_obj = $phase_obj[0];
$person_response = $phase_obj[8];
$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

$JSans_mode = $phase_obj["answer_display_mode"];
//debug_r($person_right);

$AlertMsg = "";
if($person_right == "NO")
{
	$phaseDoBy = $lpf_ui->GET_ROLE_TYPE($person_response);
	$AlertMsg .= $iPort["alert"]["this_phase_is_filled_by"]." ".$phaseDoBy." ".$iPort["alert"]["filled_in"];
	
	// in student phase
	if($person_response == "S")
	{
		$AlertMsg .= $iPort["alert"]["no_right_to_do"];
		//$AlertMsg .= $iPort["alert"]["only_preview"];
	}
	else
	{
		$AlertMsg .= $iPort["alert"]["no_right_to_do"];
	}
}
$ParArr["AlertMsg"] = $AlertMsg;

# get handin object
$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
$relevant_phase_html = $lgs->getRelevantPhaseViewAnswer($phase_obj["answer"], $user_id);

$phase_obj["correctiondate"] = $handin_obj["correctiondate"];
$phase_obj["status"] = $handin_obj["status"];

$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

if($Mode == "edit" && $person_right == "VIEW")
$phase_obj["answer_display_mode"] = 1;
else if($Mode == "view" && $person_right == "VIEW")
$phase_obj["answer_display_mode"] = 0;
else if($Mode == "view" && $person_right == "NO")
$phase_obj["answer_display_mode"] = 1;
else if($Mode == "view")
$phase_obj["answer_display_mode"] = 0;
else if($Mode == "edit")
$phase_obj["answer_display_mode"] = 1;

# preset parameter array
$ParArr["person_right"] = $person_right;
$ParArr["StudentID"] = $UserID;
$ParArr["Role"] = "STUDENT";
$ParArr["Mode"] = $Mode;
$ParArr["assignment_id"] = $assignment_id;
$ParArr["parent_id"] = $parent_id;
$phase_id = $assignment_id;

$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);

$PhaseTitle = $PhaseData[0]["title"];
$SchemeTitle = $SchemeData[0]["title"];

$MenuArr = array();
$MenuArr[] = array($iPort["scheme_list"], "index.php");
$MenuArr[] = array($SchemeTitle, "");
$MenuArr[] = array($PhaseTitle, "");
/////////////// END Gen Content ////////////////////

$linterface->LAYOUT_START();
?>

<form name="ansForm" method="post" action="phase_fill_update.php" onSubmit="return checkform(this)">
	<input type="hidden" name="qStr" value="<?=$phase_obj["answersheet"]?>">
<?php
if($phase_obj["answer_display_mode"]==1){
?>
	<input type="hidden" name="aStr" value="<?=$handin_obj["answer"]?>">
<?php
} else {
?>
	<input type="hidden" name="aStr" value="<?=preg_replace('(\r\n|\n)', "<br />", $handin_obj["answer"])?>">
<?php
}
?>
	<input type="hidden" name="handin_id" value="<?=$handin_obj["handin_id"]?>">
	<input type="hidden" name="phase_id" value="<?=$phase_id?>">
	<input type="hidden" name="assignment_id" value="<?=$parent_id?>">
	<input type="hidden" name="answersheet" value="<?=$phase_obj["answersheet"]?>">
	<input type="hidden" name="fieldname" value="answersheet">
	<input type="hidden" name="formname" value="ansForm">
	<input type="hidden" name="user_id" value="<?=$user_id?>">
</form>

<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/layer.js"></script>

<?php
if($phase_obj["answer_display_mode"]==1){
?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_edit.js"></script>
<?php
} else {
?>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/online_form_view.js"></script>
<?php
}
?>

<SCRIPT LANGUAGE="Javascript">
function checkform(myObj)
{
	var isst = false;
	if (!check_text(myObj.title, "<?=$ec_warning['growth_phase_title']?>")) return false;
	if (typeof(myObj.starttime)!="undefined")
	{
		if (myObj.starttime.value!="")
		{
			if(!check_date(myObj.starttime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			{
				isst = true;
			}
		}
	}
	if (typeof(myObj.endtime)!="undefined")
	{
		if (myObj.endtime.value!="")
		{
			if(!check_date(myObj.endtime, "<?php echo $assignments_alert_msg9; ?>")) return false;
			if (isst)
			{
				if(!compareTime(myObj.starttime, myObj.sh, myObj.sm, myObj.endtime, myObj.eh, myObj.em)) {
					myObj.starttime.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
	}
	
	/*
	checkOption(myObj.elements["relevant_phase[]"]);
	for(var i=0; i<myObj.elements["relevant_phase[]"].length; i++)
	{
		myObj.elements["relevant_phase[]"].options[i].selected = true;
	}
	*/
	
	return true;
}

function editOnlineForm()
{
	postInstantForm(document.form1, "online_form/edit.php", "post", 10, "ec_popup10");
}

function jSubmitForm()
{
	finish();
	var obj = document.ansForm;
	obj.submit();
}

function jCancelForm()
{
	history.back();
}

function jPrint(phase_id, assignment_id, is_menu)
{
	if(is_menu == 1)
	var url = "print_phase_menu.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id;
	else
	var url = "print_phase.php?QuestionOnly=1&phase_id="+phase_id+"&assignment_id="+assignment_id;
	
	newWindow(url, 28);
}

<?php
if($phase_obj["answer_display_mode"]==1){
$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", $phase_obj["answersheet"]) : preg_replace('(\r\n|\n)', "", $phase_obj["answersheet"]);
?>
// answer form
answer_sheet = "<span class=title><?=$ec_form_word['growth_scheme_form']?></span>";
answersheet_template = "<?=$ec_form_word['answersheet_template']?>";
answersheet_header = "<?=$ec_form_word['question_title']?>";
answersheet_type = "<?=$ec_form_word['fill_in_method']?>";
no_options_for = "<?=$ec_form_word['no_options_for']?>";
pls_specify_type = "<?=$ec_form_word['fill_in_type']?>";
pls_fill_in = "<?=$ec_form_word['fill_in_content']?>";
chg_title = "<?=$ec_form_word['change_heading']?>";
chg_template = "<?=$ec_form_word['confirm_to_template']?>";

answersheet_tf = "<?=$ec_form_word['answersheet_tf']?>";
answersheet_mc = "<?=$ec_form_word['answersheet_mc']?>";
answersheet_mo = "<?=$ec_form_word['answersheet_mo']?>";
answersheet_sq1 = "<?=$ec_form_word['answersheet_sq1']?>";
answersheet_sq2 = "<?=$ec_form_word['answersheet_sq2']?>";
answersheet_option = "<?=$answersheet_option?>";

answersheet_not_applicable = "<?=$ec_form_word['answersheet_not_applicable']?>";

/************************************************************/
answersheet_likert = "<?=$ec_form_word['answersheet_ls']?>";
answersheet_table = "<?=$ec_form_word['answersheet_tl']?>";
answersheet_question = "<?=$ec_form_word['answersheet_no_ques']?>";
no_questions_for = "<?=$ec_form_word['answersheet_no_select_ques']?>";
question_scale = "<?=$ec_form_word['question_scale']?>";
question_question = "<?=$ec_form_word['question_question']?>";
/************************************************************/

button_submit = " <?=$button_submit?> ";
button_add = " <?=$button_add?> ";
button_cancel = " <?=$button_cancel?> ";
button_update = " <?=$button_update?> ";


background_image = "";

var sheet = new Answersheet();
// attention: MUST replace '"' to '&quot;'
var tmpStr = document.ansForm.qStr.value;

<? 
if($person_right != "NO")
echo "var tmpStrA = document.ansForm.aStr.value;";
else
echo "var tmpStrA = '';";
?>

//sheet.qString = tmpStr.replace(/\"/g, "&quot;");
sheet.qString = tmpStr.replace(/\&quot;/g, "\"");
sheet.aString = tmpStrA.replace(/\&quot;/g, "\"");
sheet.mode = 1;	// 0:edit 1:fill in application
sheet.displaymode = <?=$phase_obj["sheettype"]?>;	// 0:edit 1:fill in application
sheet.answer = sheet.sheetArr();

// temp
var form_templates = new Array();

sheet.templates = form_templates;

<?php
} else {
$phase_obj["answersheet"] = str_replace("\"", "\\\"", undo_htmlspecialchars($phase_obj["answersheet"]));
$phase_obj["answersheet"] = ($phase_obj["sheettype"]!=2) ? preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $phase_obj["answersheet"])) : preg_replace('(\r\n|\n)', "", str_replace("&amp;", "&", $phase_obj["answersheet"]));
?>
var myQue = "<?=$phase_obj["answersheet"]?>";
var myAns = "<?=preg_replace('(\r\n|\n)', "<br>", str_replace("&amp;", "&", $handin_obj["answer"]))?>";
var msg_not_answered = "<i><font color=gray>&lt;<?=$ec_iPortfolio['form_not_answered']?>&gt;</font></i>";
var displaymode = <?=$phase_obj["sheettype"]?>;
var answer_display_mode = <?=$JSans_mode?>;
question_scale = "<?=$ec_form_word['question_scale']?>";
<?php
}
?>

</SCRIPT>

<?php
if($phase_obj["answer_display_mode"]==1)
$ParArr["SheetData"] = "<script language='javascript'>document.write(editPanel());</script>"; 
else
$ParArr["SheetData"] = "<script language='javascript'>document.write(viewForm(myQue, myAns));</script>"; 

$Content .= $lpf_ui->GET_NAVIGATION($MenuArr);
$Content .= $lpf_ui->GET_PHASE_FORM($ParArr);
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<!-- CONTENT HERE -->
<?=$Content?>
</td></tr></table>
</td></tr></table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
