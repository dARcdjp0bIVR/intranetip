<?php

// Modifing by Pun

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
include_once($PATH_WRT_ROOT."includes/libportfolio_group.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once('print_pdf_chiuchunkg.config.php');

intranet_opendb();


######## Init START ########
$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);
$school_year_id = ($school_year_id)? IntegerSafe($school_year_id) : Get_Current_Academic_Year_ID();

$linterface = new interface_html("popup.html");
$lpf = new libpf_sbs();
$lpf_ui = new libpf_sbs();
$lgs = new growth_scheme();
$libenroll = new libclubsenrol($school_year_id);


$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$CurrentPage = "eClass_update_activity";
$title = $ec_iPortfolio['print_result'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;
######## Init END ########

$linterface->LAYOUT_START();

######## AccessRight START ########
if($ck_memberType == "P") {
	$user_id = $lpf->getCourseUserID($ck_current_children_id);
}
if($user_id=="") {
	$user_id = $lpf->getCourseUserID($UserID);
}
if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}
######## AccessRight END ########


######## Get Phase Info START ########
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

# get the period
$period_result = $lgs->getPhasePeriod($phase_obj["starttime"], $phase_obj["deadline"]);

if($period_result!="FUTURE")
{
	$WarningMsg = ($period_result=="IN") ? "<tr><td class='tabletext' colspan='2'>".$ec_iPortfolio['phase_in_processing_state']."</td></tr>" : "";

	$PrintButton = $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "submit", "", "btn_submit");
}
else
{
	//$DisplayContent = $ec_warning['no_view_now'];
}
######## Get Phase Info END ########


######## Get UI Element START ########
$academicYearHTML = getSelectAcademicYear("school_year_id", "",1,"",$school_year_id);


$termHTML = $libenroll->Get_Term_Selection('school_year_term_id', $school_year_id, $SelectedSemester='', $term_onchange='', $NoFirst=1, $NoPastTerm=0, $withWholeYear=0);
######## Get UI Element END ########


######## UI START ########
?>

<FORM enctype="multipart/form-data" action="print_pdf_chiuchunkg_print.php" method="POST" id="form1" name="form1">
	<table width="420" border="0" cellspacing="0" cellpadding="5">
		<?=$WarningMsg?>
		 <tr>
          <td>&nbsp;</td>
        </tr>
		<tr>
			<td class='tabletext'>
				<label for="school_year_id"><?=$ec_iPortfolio['school_year']?></label>
			</td>
			<td class='tabletext'>
				<?=$academicYearHTML ?>
			</td>
		</tr>
		<tr>
			<td class='tabletext'>
				<label for="school_year_term_id"><?=$ec_iPortfolio['term']?></label>
			</td>
			<td class='tabletext'>
				<?=$termHTML ?>
			</td>
		</tr>
		<tr>
			<td class='tabletext'>
				<label for="printDate"><?=$i_general_print_date ?></label>
			</td>
			<td class='tabletext'>
				<input name="printDate" id="printDate" value="<?=date('Y-m-d') ?>"/>
				<?=$linterface->GET_CALENDAR("form1", "printDate"); ?>
			</td>
		</tr>
		<tr>
			<td class='tabletext'>
				<label for="printDate"><?=$Lang['iPortfolio']['ChiuChunKG']['SBS']['rowCount'] ?></label>
			</td>
			<td class='tabletext'>
				<input name="rowCount" id="rowCount" value="<?=$pdfConfig['ReportDefaultRowCount'] ?>"/>
			</td>
		</tr>
		<tr style="display:none;">
			<td class='tabletext'>
				<label for="printVersion"><?=$Lang['iPortfolio']['ChiuChunKG']['SBS']['printVersion'] ?></label>
			</td>
			<td class='tabletext'>
				<select id="printVersion" name="printVersion">
					<option>1</option>
					<option selected>2</option>
				</select>
			</td>
		</tr>
		<!--tr id="reportTitleRow">
			<td class='tabletext'>
				<label for="reportTitle"><?=$Lang['iPortfolio']['ChiuChunKG']['SBS']['reportTitle'] ?></label>
			</td>
			<td class='tabletext'>
				<input id="reportTitle" name="reportTitle" value="<?=$pdfConfig['ReportTitle'] ?>" />
			</td>
		</tr-->
      </table>
	<br />

	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
		<?=$PrintButton?>
		<?=$linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "self.close();", "btn_cancel");?>
		</td>
	</tr>
	</table>
<input type="hidden" name="user_id" value="<?=$user_id?>" />
<input type="hidden" name="phase_id" value="<?=$phase_id?>" />
<input type="hidden" name="assignment_id" value="<?=$assignment_id?>" />
</FORM>


<script>
var isRefresh = false;
$('#school_year_id').change(function(){
	isRefresh = true;
	$('#form1').attr('action', '').submit();
});

$('#printVersion').change(function(){
	if($(this).val() == '1'){
		$('#reportTitleRow').hide();
	}else{
		$('#reportTitleRow').show();
	}
});

$('#form1').submit(function(){
	if($('#printVersion').val() == '2'){
		$(this).attr('action', 'print_pdf_chiuchunkg_print2.php');
	}else{
		$(this).attr('action', 'print_pdf_chiuchunkg_print.php');
	}

	if (!isRefresh && $('#printDate').val() !="")
	{
		if(!check_date($('#printDate')[0], "<?php echo $assignments_alert_msg9; ?>")){
			return false;
		}
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
