<?php
// Using: Pun

ini_set('memory_limit','128M');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
include_once('print_pdf_chiuchunkg.config.php');

include_once($PATH_WRT_ROOT.'kis/init.php');
$libkis_iportfolio = $libkis->loadApp('iportfolio');


intranet_opendb();

######## Init START ########
$lpf = new libpf_sbs();

$mPDF = new mPDF(
    $mode='',
    $format='A4',
    $default_font_size=0,
    $default_font='msjh',
    $marginLeft=0,
    $marginRight=0,
    $marginTop=0,
    $marginBottom=0,
    $marginHeader=0,
    $marginFooter=0,
    $orientation='P'
);

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);
$rowCount = IntegerSafe($rowCount);

if($ck_memberType == "P")
{
	$user_id = $lpf->getCourseUserID($ck_current_children_id);
}

if($user_id=="")
{
	$user_id = $lpf->getCourseUserID($UserID);
}

$imgBase = "{$PATH_WRT_ROOT}file/iportfolio/cust/chiuchunkg/";

//define('REPORT_ROW_P1', 38);
//define('REPORT_ROW_P2', 43);
define('REPORT_ROW_P1', $rowCount-5);
define('REPORT_ROW_P2', $rowCount);
######## Init END ########


######## Access Right START ########
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");


$lgs = new growth_scheme();

if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}
######## Access Right END ########


######## Student Info START ########
$StudentIDArr = array();
$StudentIDArr[] = $user_id;
$UserArr = $lgs->getHandedinUserInfo($StudentIDArr);

$sql = "SELECT 
	iu.UserID,
	CONCAT(iu.ChineseName, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', iu.EnglishName) as Name,
	iu.ClassName,
	iu.ClassNumber,
	DATE(iu.DateOfBirth) as birthday,
	iu.Gender as gender,
	iu.STRN,
	y.YearName,
	yc.ClassTitleB5,
	yc.ClassTitleEN
FROM 
	{$lpf->course_db}.usermaster um
INNER JOIN 
	{$intranet_db}.INTRANET_USER iu
ON 
	um.user_email = iu.UserEmail
INNER JOIN
	YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
INNER JOIN
	YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID					
INNER JOIN
	YEAR y ON yc.YearID = y.YearID				
WHERE 
	yc.AcademicYearID = '".$libkis_iportfolio->schoolyear."'
AND
	um.user_id = '{$user_id}'
	";
$rs = 	$lpf->returnResultSET($sql);
$user = $rs[0];

$studentId = $user['UserID'];
$studentName = $user['Name'];
$studentYear = $user['YearName'];
$studentClass = Get_Lang_Selection($user['ClassTitleB5'],$user['ClassTitleEN']);
$studentNumber = $user['STRN']; // $user['ClassNumber'];
$studentDOB = date("Y/m/d", strtotime($user['birthday']));
$studentGender = $user['gender'];
######## Student Info END ########


######## Student Portfolio Info START ########
$data = array();
$data['studentId'] = $studentId;
$data['school_year_id'] = $school_year_id;
$data['school_year_term_id'] = $school_year_term_id;
$portfolioInfo = $libkis_iportfolio->getPortfolioRecord($data);

$semesterArr = getSemesters($school_year_id);
$semesterIdArr = array_keys($semesterArr);
$isLastSemester = ($school_year_term_id == $semesterIdArr[ count($semesterIdArr)-1 ]);

$academicYearName = getAYNameByAyId($school_year_id);
$semesterName = $semesterArr[$school_year_term_id];

if($printDate){
	$printDate = date("Y/m/d", strtotime($printDate));
	$printDateWeekDay = date("w", strtotime($printDate));
}
######## Student Portfolio Info END ########

######## Set empty string width START ########
if(count($portfolioInfo) == 0){
	$portfolioInfo['year_name'] = //
	$portfolioInfo['StartAgeYear'] = //
	$portfolioInfo['StartAgeMonth'] = //
	$portfolioInfo['StartWeight'] = //
	$portfolioInfo['StartHeight'] = //
	$portfolioInfo['EndAgeYear'] = //
	$portfolioInfo['EndAgeMonth'] = //
	$portfolioInfo['EndWeight'] = //
	$portfolioInfo['EndHeight'] = //
	$portfolioInfo['TotalSchoolDay'] = //
	$portfolioInfo['PresentDay'] = //
	$portfolioInfo['SickLeaveDay'] = //
	$portfolioInfo['OtherLeaveDay'] = //
	$portfolioInfo['LateDay'] = //
	$portfolioInfo['EarlyLeaveDay'] = '&nbsp;&nbsp;';
}else{
	foreach($portfolioInfo as $key=>$value){
		if($value == ''){
			$portfolioInfo[$key] = '&nbsp;&nbsp;';
		}
	}
}
######## Set empty string width END ########

######## Get data START ########
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
######## Get data END ########


######## Decode Ansersheet/Answer START ########
$answersheetArr = explode('#QUE#', $phase_obj['answersheet']);
array_shift($answersheetArr);

$answerArr = explode('#ANS#', $handin_obj['answer']);
array_shift($answerArr);

$emptyRow = array(
    'class' => '',
    'title' => '&nbsp;',
    'value' => '&nbsp;',
);

$row = array();
for($i=0,$iMax=count($answersheetArr);$i<$iMax;$i++){
    list($questionInfo, $title, $optionsInfo) = explode('||', $answersheetArr[$i]);
    list($questionType, $questionCount, $optionCount) = explode(',', $questionInfo);

    if($questionType == '8'){ // Category
        if(count($row)){
            $row[] = array(
                'class' => 'empty',
                'title' => '&nbsp;',
                'value' => '&nbsp;',
            );
        }
        $row[] = array(
            'class' => 'main-category',
            'title' => $title,
            'value' => '表現',
        );
        continue;
    }else if($questionType == '6'){
        $row[] = array(
            'class' => 'sub-category',
            'title' => $title,
            'value' => '&nbsp;',
        );

        $options = explode('#OPT#', $optionsInfo);
        array_shift($options);

        $studentAnswer = explode(',', $answerArr[$i]);
        for($j=0;$j<$questionCount;$j++){
            $answer = $studentAnswer[$j];

            if( $answer !== null && $answer !== 'null' ){
                $row[] = array(
                    'class' => 'question',
                    'title' => $options[$j],
                    'value' => $options[$answer+$questionCount],
                );
            }else{
                $row[] = array(
                    'class' => 'question',
                    'title' => $options[$j],
                    'value' => '&nbsp;'
                );
            }

        }
    }
}
$rowChunk = array();
for($i=0;$i<REPORT_ROW_P1;$i++){
    $data = $row[$i];

    if(empty($data)){
        $rowChunk[0][] = $emptyRow;
    }else{
        $rowChunk[0][] = $data;
    }
}
for($i=0;$i<REPORT_ROW_P1;$i++){
    $data = $row[$i+REPORT_ROW_P1];

    if(empty($data)){
        $rowChunk[1][] = $emptyRow;
    }else{
        $rowChunk[1][] = $data;
    }
}
for($i=0;$i<REPORT_ROW_P2;$i++){
    $data = $row[$i+REPORT_ROW_P1*2];

    if(empty($data)){
        $rowChunk[2][] = $emptyRow;
    }else{
        $rowChunk[2][] = $data;
    }
}
for($i=0;$i<REPORT_ROW_P2;$i++){
    $data = $row[$i+REPORT_ROW_P1*2+REPORT_ROW_P2];

    if(empty($data)){
        $rowChunk[3][] = $emptyRow;
    }else{
        $rowChunk[3][] = $data;
    }
}
######## Decode Ansersheet/Answer END ########


######## Output START ########
$mPDF->SetHTMLHeader('<img src="'.$imgBase.'header.jpg" />');
$mPDF->SetHTMLFooter('<img src="'.$imgBase.'footer.jpg" />');


#################################### Page 2 START ####################################
ob_start();
?>

<style>
	@apage{
		margin: 0;
		padding: 0;
		margin-top:-6mm;

		/*margin-header: 5mm;
		margin-footer: 5mm;
		header: html_myHTMLHeaderOdd;
		footer: html_myHTMLFooterOdd;*/

	    /*background-image: url("<?=$imgBase ?>full.jpg");
        background-repeat: no-repeat;
        background-image-resize: 6;*/
	}
	body{
		font-family: msjh !important;
	}

	.portfolioTable{
		border-collapse:collapse;
		width:100%;
	}

	.portfolioTable tr td{
		border-width: 1px;
	}

	.td_l{
		border-left:0.1px solid black;
	}
	.td_t{
		border-top:0.1px solid black;
	}
	.td_r{
		border-right:0.1px solid black;
	}
	.td_b{
		border-bottom:0.1px solid black;
	}

	.td_title{
	    font-size: 4mm;
	}

	.td_content{
	    font-size: 5mm;
	}


    .main-category{
        padding-left: 1mm;
    }
    .main-category, .main-category-answer{
        font-size: 3.6mm;
        font-weight: bold;
    }
    .sub-category{
        font-size: 3.2mm;
        font-weight: bold;
        padding-left: 5mm;
    }
    .question{
        padding-left: 10mm;
    }
    .question, .question-answer{
        font-size: 3.2mm;
    }
    .empty, .empty-answer{
        border-bottom: 0;
    }
</style>

&nbsp;
<table class="portfolioTable" border="0" style="text-align:center; margin-top: 32mm;">
	<tr>
    	<td style="padding: 0;height: 8mm;font-size: 5mm;vertical-align:bottom;">
    		&nbsp;
    		<?=$academicYearName ?> 年度
    		<?=$semesterName ?>
    		<?=$studentYear ?>
    		&nbsp;
    	</td>
	</tr>
	<tr><td style="padding: 0;font-size: 5mm;"><?=$phase_obj['instruction'] ?></td></tr>
</table>



<table class="portfolioTable" border="0" style="margin-top: 0mm;">
	<tr>
		<td></td>
		<td class="td_b td_title" style="height: 7mm;padding-left:3mm;font-size: 4mm;">學生姓名</td>
		<td class="td_b td_content" colspan="7" style="padding-left:3mm;font-size: 4mm;"><?=$studentName ?>&nbsp;</td>
		<td></td>
	</tr>

	<tr>
		<td></td>

		<td class="td_b td_title" style="height: 7mm;padding-left:3mm;font-size: 4mm;">班別</td>
		<td class="td_b td_r td_content" style="padding-left:3mm;font-size: 4mm;"><?=$studentClass ?>&nbsp;</td>

		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">學號</td>
		<td class="td_b td_r td_content" style="text-align:center;font-size: 4mm;">&nbsp;<?=$studentNumber ?>&nbsp;</td>

		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">性別</td>
		<td class="td_b td_r td_content" style="text-align:center;font-size: 4mm;">
			&nbsp;<?php
			if($studentGender == 'M'){
				echo $Lang['General']['Male'];
			}else if($studentGender == 'F'){
				echo $Lang['General']['Female'];
			}
			?>&nbsp;
		</td>

		<td class="td_b td_title" style="text-align:right;font-size: 4mm;">出生日期</td>
		<td class="td_b td_content" style="text-align:center;font-size: 4mm;">&nbsp;<?=$studentDOB ?>&nbsp;</td>

		<td></td>
	</tr>
	<tr>
		<td style="width:10mm;"></td>
		<td style="width:23.75mm;"></td>
		<td style="width:23.75mm;"></td>
		<td style="width:12mm;"></td>
		<td style="width:35.5mm;"></td>
		<td style="width:12mm;"></td>
		<td style="width:35.5mm;"></td>
		<td style="width:20mm;"></td>
		<td style="width:27.5mm;"></td>
		<td style="width:10mm;"></td>
	</tr>
</table>

<table class="portfolioTable" border="0" style="margin-top:3mm;">
	<?php
	for($i=0;$i<REPORT_ROW_P1;$i++){
	    $lRowInfo = $rowChunk[0][$i];
	    $rRowInfo = $rowChunk[1][$i];
	?>
    	<tr>
    		<td></td>
    		<td class="td_b <?=$lRowInfo['class'] ?>" style=""><?=$lRowInfo['title'] ?></td>
    		<td class="td_b <?=$lRowInfo['class'] ?>-answer" style="text-align: center;"><?=$lRowInfo['value'] ?></td>
    		<td></td>
    		<td class="td_b <?=$rRowInfo['class'] ?>" style=""><?=$rRowInfo['title'] ?></td>
    		<td class="td_b <?=$rRowInfo['class'] ?>-answer" style="text-align: center;"><?=$rRowInfo['value'] ?></td>
    		<td></td>
    	</tr>
	<?php
	}
	?>
	<tr>
		<td style="width:10mm"></td>
		<td style="width:80mm"></td>
		<td style="width:10mm"></td>
		<td style="width:10mm"></td>
		<td style="width:80mm"></td>
		<td style="width:10mm"></td>
		<td style="width:10mm"></td>
	</tr>
</table>



<div style="position: fixed;bottom: 15mm;">
    <table class="portfolioTable" border="0" style="text-align:center;">
    	<tr>
    		<td></td>
    		<td></td>
    		<td style="font-size: 3mm;"><?=$pdfConfig['ReportFooter'] ?></td>
    		<td style="font-size: 3mm;text-align:right;">接後頁</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td style="width:10mm"></td>
    		<td style="width:15mm"></td>
    		<td style="width:160mm"></td>
    		<td style="width:15mm"></td>
    		<td style="width:10mm"></td>
    	</tr>
    </table>
</div>



<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->AddPage();

#################################### Page 3 START ####################################
ob_start();
?>

&nbsp;
<table class="portfolioTable" border="0" style="margin-top:32mm;">
	<?php
	for($i=0;$i<REPORT_ROW_P2;$i++){
	    $lRowInfo = $rowChunk[2][$i];
	    $rRowInfo = $rowChunk[3][$i];
	?>
    	<tr>
    		<td></td>
    		<td class="td_b <?=$lRowInfo['class'] ?>" style=""><?=$lRowInfo['title'] ?></td>
    		<td class="td_b <?=$lRowInfo['class'] ?>-answer" style="text-align: center;"><?=$lRowInfo['value'] ?></td>
    		<td></td>
    		<td class="td_b <?=$rRowInfo['class'] ?>" style=""><?=$rRowInfo['title'] ?></td>
    		<td class="td_b <?=$rRowInfo['class'] ?>-answer" style="text-align: center;"><?=$rRowInfo['value'] ?></td>
    		<td></td>
    	</tr>
	<?php
	}
	?>
	<tr>
		<td style="width:10mm"></td>
		<td style="width:80mm"></td>
		<td style="width:10mm"></td>
		<td style="width:10mm"></td>
		<td style="width:80mm"></td>
		<td style="width:10mm"></td>
		<td style="width:10mm"></td>
	</tr>
</table>

<div style="position: fixed;bottom: 15mm;">
    <table class="portfolioTable" border="0" style="text-align:center;">
    	<tr>
    		<td></td>
    		<td class="td_title" style="vertical-align:bottom;padding-bottom:0;">家長簽署：</td>
    		<td class="td_b"></td>
    		<td></td>
    		<td class="td_title" style="vertical-align:bottom;padding-bottom:0;text-align: left;">日期：</td>
    		<td class="td_b">
    			&nbsp;<?php
    			if($printDate){
    				//echo "{$printDate} ({$Lang['SysMgr']['Homework']['WeekDay'][$printDateWeekDay]})";
    				echo $printDate;
    			}
    			?>&nbsp;
    		</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td style="width:10mm"></td>
    		<td style="width:20mm"></td>
    		<td style="width:70mm"></td>
    		<td style="width:10mm"></td>
    		<td style="width:15mm"></td>
    		<td style="width:75mm"></td>
    		<td style="width:10mm"></td>
    	</tr>
    </table>
</div>

<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);

$mPDF->Output();
exit;
