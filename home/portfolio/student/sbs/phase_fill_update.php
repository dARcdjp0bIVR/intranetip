<?php

# Modifing by Key

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");

intranet_auth();
intranet_opendb();

$lgs = new growth_scheme();
$lpf = new libpf_sbs();

$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);


if($ck_memberType == "P") {
	$tmp_user_id = $lpf->getCourseUserID($ck_current_children_id);
}
if($tmp_user_id=="") {
	$tmp_user_id = $lpf->getCourseUserID($UserID);
}
if ($tmp_user_id != $user_id || !in_array($tmp_user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}

$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];
$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj);

if ($person_right=="DO" || $person_right="REDO")
{
	$answer = HTMLtoDB($aStr);

	$li = new libdb();
	$update_status = ($person_right=="REDO") ? "LR" : "L";
	if ($handin_id!="")
	{
		$marked_by_sql = ($ck_memberType=="T" || $ck_memberType=="P") ? ", marked_by = '$ck_user_id'" : "";
		$sql = "UPDATE $lpf->course_db.handin SET answer='$answer', filename=NULL, inputdate=now(), status='$update_status', comment=NULL $marked_by_sql WHERE handin_id='$handin_id' and user_id='$user_id'";
		$msg = 2;
	} 
	else
	{
		$newStatus = "NULL";
		$marked_by_sql = ($ck_memberType=="T" || $ck_memberType=="P") ? "'$ck_user_id'" : "NULL";
		$sql = "INSERT INTO $lpf->course_db.handin (assignment_id, user_id, answer, comment, marked_by, status, inputdate, type) VALUES ('$phase_id', '$user_id', '$answer', '$newStatus', $marked_by_sql, '$update_status', now(), 'sbs')";
		$msg = 1;
	}
	$li->db_db_query($sql);
} else
{
	$msg = "";
}

intranet_closedb();

header("Location: index.php?{$user_id_par}assignment_id={$assignment_id}&msg=$msg");

?>
