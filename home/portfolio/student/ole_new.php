<?php
/** [Modification Log] Using By : 
 *********************************************************
 * 2016-07-15 Henry HM
 * - Update split() function to explode() function since split() function was DEPRECATED in PHP 5.3.0, and REMOVED in PHP 7.0.0.
 * 
 * 2016-05-23 Henry HM [2016-0415-1449-22214]
 * - display default hours to the hours field when new record from teacher OLE programme 
 * 
 * 2016-05-20 Omas
 * - fixed OLE_PROGRAM_CompulsoryFields > Approved by problem 
 * 
 * 2015-10-19 Omas
 * - added compulsory setting - #S79668
 * 
 * 2014-12-23: Bill
 * - pass $SettingsArray to isStudentSubmitOLESetting() for checking
 * 
 * 2013-02-21: YatWoon
 * - allow user view the details instead of redirect to main page if user no permission to edit record
 * * 2011-10-24: Connie 
 * - modified teacher_selection and added $hiddenField_Approver
 *
 * 2010-05-04: FAI 
 * - do not check isStudentSubmitOLESetting for OLE program which is created by teacher (self join program) otherwise it does allow student to join the *   program if the setting is not allow student to report OLE program

 * 2010-04-21: Max (201004211152)
 * - Allow student to edit if allow edit is set in RecordApproval
 * 
 * 2010-02-12: Max (201002121147)
 * - Setting display varibles for ole_new.tmpl.php

 * 2010-01-27: Max (201001271450)
 * - Modify the getting of DataArr is not by CAPITAL letter as RETURN_OLE_RECORD_BY_RECORDID is modified
 * 
 * 2009-12-08: Max (200912081532)
 * - retrieving data from function PROGRAM_BY_PROGRAMID_MAX

 * Type			: Enhancement
 * Date 		: 200911230911
 * Description	: Teacher role
 * 				  1) Add fields [Allow student to join], [Join period]
 * 				  Student role
 * 				  2C) Allow students to see the list of Student Learning Profile(SLP) to join, show within editable period and type == 1
 * By			: Max Wong
 * Case Number	: 200911230911MaxWong
 * C=CurrentIssue 
 */ 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();


$DefaultApprover_IsSelectedbyTeacher=false;
################ 20101124
if ($IntExt) {
	$INT_EXT_String = "EXT";	
} else {
	$IntExt = "";
	$INT_EXT_String = "INT";	
}
$lpf = new libpf_slp();
$approvalSettings = $lpf->GET_OLR_APPROVAL_SETTING_DATA2($INT_EXT_String);
################ 20101124

//get variable
$action = trim($action);  //GET THE ACTION FOR THIS PAGE
$SelectedStatus = IntegerSafe(trim($status));
$SelectedELE = trim($ELE);
$SelectedYear = IntegerSafe(trim($AcademicYearID));
$record_id = (is_array($record_id)) ? IntegerSafe($record_id[0]) : IntegerSafe($record_id);

$p_type = trim($p_type);  // variable p_type (Program Type) to indicate received OLE PROGRAM is student created or teacher created

$ole_student_sql = "select ComeFrom, ProgramID, UserID from $eclass_db.OLE_STUDENT where RecordID='$record_id'";
$ole_student_result_tmp = $lpf->returnArray($ole_student_sql);
$ole_student_result = $ole_student_result_tmp[0];

$programID  = trim($programID) ? IntegerSafe(trim($programID)) : $ole_student_result['ProgramID'];

$canAccess = false;

if ($record_id !='' && $ck_memberType == 'S' && $ole_student_result['UserID'] == $_SESSION['UserID']) {
	$canAccess = true;
}
else if ($record_id !='' && $ck_memberType == 'P' && $ole_student_result['UserID'] == $ck_current_children_id) {
	$canAccess = true;
}
if (!$canAccess) {
	//No_Access_Right_Pop_Up();
}


if(!$p_type)
{
	$p_type = $ole_student_result[0]==$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"] ? "sr":"";
}

if($p_type!="sr" && $action!="new")
{
	$warning_msg[] = "no_permission";	
}
	
// Initializing classes
$LibUser = new libuser($UserID);
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$displaySchoolRemark = 0; // default do not display the SCHOOL REMARK  ==> 0

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPage = $IntExt == 1 ? "Student_ExtOLE" : "Student_OLE";
$CurrentPageName = $IntExt == 1 ? "<font size='-2'>".$iPort["external_record"]."</font>" : $iPort['menu']['ole'];

### Title ###
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();

###############################################
/*
if($p_type == "sr" && ($programID != "" || $record_id != "")){
	debug_pr("in this");
		//IF THE RECEIVED PROGRAM IS CREATE BY TEACHER "SR"--> "STUDENT REPORT" AND EITHER $programID OR $record_id HAS VALUE, BY PASS THE SUBMIT OLE SETTING CHECKING
		// DO nothing
}
else
{
	// BELOW CHECKING DOES NOT APPLY TO TEACHER CREATED PROGRAM THAT SET TO STUDENT JOIN 
	//////// get the submission period & allow submit type ////////////////////////
	if(!$lpf->isStudentSubmitOLESetting($IntExt, $ck_memberType))
	{
		$warning_msg[] = "expired";	
		//NOT ALLOW TO SUBMIT THE OLE PROGRAM ACCORDING TO THE TEACHER SETTINGS , REDIRECT TO ole.php , PROGRAM END
// 		header("Location: ole.php");
// 		exit;
	}
	////////////////////////////////////////////////////////////////////////////////
}
*/

# Get period settings from DB
$objIpfPeriodSetting = new iportfolio_period_settings();
$StudentLevel = $lpf->getClassLevel($LibUser->ClassName);
$SettingsArray = $objIpfPeriodSetting->getSettingsArray(($IntExt==1? "EXT" : "OLE"), $StudentLevel['YearID']);

# Set start and end date format
$SettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SettingsArray['StartDate'], (array)$SettingsArray['StartHour'], (array)$SettingsArray['StartMinute']);
$SettingsArray['StartDateTime'] = $SettingsArray['StartDateTime'][0];
$SettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SettingsArray['EndDate'], (array)$SettingsArray['EndHour'], (array)$SettingsArray['EndMinute']);
$SettingsArray['EndDateTime'] = $SettingsArray['EndDateTime'][0];

//if(!$lpf->isStudentSubmitOLESetting($IntExt, $ck_memberType, $programID) && $p_type=="sr")
if(!$lpf->isStudentSubmitOLESetting($IntExt, $ck_memberType, $programID, (array)$SettingsArray) && $p_type=="sr")
{
	$warning_msg[] = "without submission perdiod";	
	//NOT ALLOW TO SUBMIT THE OLE PROGRAM ACCORDING TO THE TEACHER SETTINGS , REDIRECT TO ole.php , PROGRAM END
// 		header("Location: ole.php");
// 		exit;
}
$isAllowEditOLE = checkAllowEditOLE($record_id,$approvalSettings);
if(!$isAllowEditOLE)	$warning_msg[] = "already approved";
$isAllowEditOLE = ($isAllowEditOLE) && (!sizeof($warning_msg));

if(sizeof($warning_msg))
{
// 	$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'];
	//$WarningMsg = implode('<br />', $warning_msg);
	$WarningMsg = $Lang['iPortfolio']['OLE']['ViewModeReason'];
	// $WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Warning'].'</font>', $WarningMsg, $others="");
	$WarningBox = "<fieldset class='instruction_box_v30'><legend>". $Lang['General']['Note'] ."</legend> <font color='#666666'>". $WarningMsg ."</font></fieldset>";
}


/*
if($isAllowEditOLE == 0)
{
	//NOT ALLOW TO EDIT THE OLE PROGRAM , REDIRECT TO ole_record.php , PROGRAM END
	//header("Location: ole_record.php");
	header("Location: ole_detail.php?record_id=".$record_id);
	exit();
}
*/

# template for activity title
$LibWord = new libwordtemplates_ipf(3);
$file_array = $LibWord->file_array;

# template

# template drop down list
for ($f=0; $f<sizeof($LibWord->file_array); $f++)
{
	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	unset($WordListArr);
	$tempArr = $LibWord->getWordList($id);
	
	$WordListArr[] = array("", " --- ".$button_select." --- ");
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$WordListArr[] = array(str_replace("&amp;", "&", $p_title), undo_htmlspecialchars($p_title));
	}
	$WordListSelection[] = array($id, returnSelection($WordListArr , "", "WordList", "onChange=\"document.form1.title.value = this.value \""));
}
# template drop down list

# float list arr
for ($f=0; $f<sizeof($LibWord->file_array); $f++) {

	$id = $LibWord->file_array[$f][0];
	unset($tempArr);
	$tempArr = $LibWord->getWordList($id);
	
	for ($i=0; $i<sizeof($tempArr); $i++) {
		$TempTitleArray = explode("\t", $tempArr[$i]);
		$p_title = $TempTitleArray[0];
		$ListArr[$id][] = str_replace("&amp;", "&", undo_htmlspecialchars($p_title));

		# preset ELE of the corresponding title
		$ListELEArr[$id][] = $TempTitleArray[1];
	}
}

###############################################

###############################################
# template for activity role

$RoleListArr = getActivityRoleList();

# template
###############################################

###############################################
# template for activity award

$AwardsListArr = getActivityAwardsList();

# template
###############################################



list($ClassName, $StudentID) = $lpf->GET_STUDENT_ID($ck_user_id);

$attach_count = 0;

if($record_id!="")  //IE OLE_STUDENT EXIST
{
	$DataArr = $lpf->RETURN_OLE_RECORD_BY_RECORDID($record_id);
	$startdate = $DataArr["StartDate"];
	$enddate = $DataArr["EndDate"];
	$title = $DataArr["Title"];
	$category = $DataArr["Category"];
	$ELE = $DataArr["ELE"];
	$ele = explode(",",$ELE);

	$ole_role = $DataArr["Role"];
	$hours = $DataArr["Hours"];
	$organization = $DataArr["Organization"]; 
	$achievement = $DataArr["Achievement"];
	$details = $DataArr["Details"];
	$ole_file = $DataArr["Attachment"];
	$approved_by = $DataArr["ApprovedBy"];
	$remark = $DataArr["remark"];
	$process_date = $DataArr["ProcessDate"];
	$record_status = $DataArr["RecordStatus"];
	$user_id = $DataArr["UserID"];
	$int_ext = $DataArr["IntExt"];
	$programID = $DataArr["ProgramID"];
	$subCategoryID = $DataArr["SubCategoryID"];
	$maximumHours = $DataArr["MaximumHours"];
	$compulsoryFields = $DataArr['CompulsoryFields'];
	$requestApprovedBy = $DataArr['RequestApprovedBy'];
	$IsSAS = $DataArr['IsSAS'];
	$IsOutsideSchool = $DataArr['IsOutsideSchool'];

	# Get all categories if record category is disabled
	$OLE_Cat = $lpf->get_OLR_Category();
	$file_array = $LibWord->setFileArr();
	if(!array_key_exists($category, $OLE_Cat))
		$file_array = $LibWord->setFileArr(true);

	$SubmitType = $int_ext;
	
	# get ELE Array
	$ELEArr = explode(",", $ELE);
	$ELEArr = array_map("trim", $ELEArr);
// 	$tmp_arr = explode("\:", $ole_file);
	$tmp_arr = explode(":", $ole_file);
	$folder_prefix0 = $eclass_filepath."/files/portfolio/ole/r".$record_id;
	$folder_url = "http://".$eclass_httppath."/files/portfolio/ole/r".$record_id;

	for ($i=0; $i<sizeof($tmp_arr); $i++)
	{
		$attach_file = $tmp_arr[$i];
		$filename = get_file_basename($attach_file);

		if (trim($attach_file)!="" && file_exists($folder_prefix0."/".$attach_file))
		{
			$file_size = ceil(filesize($folder_prefix0."/".$attach_file)/1024) . $file_kb;
			if($isAllowEditOLE)
				$attachments_html .= "<input type='checkbox' name='is_need_$attach_count' checked onClick=\"triggerFile(this.form, '".addslashes($attach_file)."', $attach_count)\">&nbsp;";
			$attachments_html .= "<a href=\"".$folder_url."/".$attach_file."\" target='_blank' id='a_$attach_count' class='tablelink' >".$filename." ($file_size)</a>";
			$attachments_html .= "<input type='hidden' name='file_current_$attach_count' value=\"$attach_file\"><br>\n";
			$attach_count ++;
		}
	}
	$JScriptID = "jArr".$category;
	$JELEScriptID = "jELEArr".$category;

	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_edit, "")
		);
		
	$h_navigationWord = $button_edit;
	
	
} 
else
{

	$NewCategoryArray = $lpf->get_OLR_Category();
	$file_array = $LibWord->setFileArr();
	
	// get the least category id
	foreach($NewCategoryArray as $CategoryID => $CategoryTitle)
	{
		$TmpCategoryID = $CategoryID;
		break;
	}

	$JScriptID = "jArr".$TmpCategoryID;
	$JELEScriptID = "jELEArr".$TmpCategoryID;
	if($ELE!="")
	{
		$ELEArr = array($ELE);
	}
	# define the navigation
	$template_pages = Array(
		Array($ec_iPortfolio['ole'], "index.php"),
		Array($button_new, "")
	);
	
	$h_navigationWord = $button_new;
}
$defaultSelectArray = array('-9','--','');   //insert this array for default select category "--"
array_unshift($file_array, $defaultSelectArray);  //insert this array for default select category "--"
$category = (!isset($category) || $category=="") ? 1 : $category;

# Get submit type
$DefaultSubmitType = "";
if(($check_int == "on" && $check_ext == "") || $IntExt == "")
{
	$SubmissionType = $iPort["internal_record"];
	$DefaultSubmitType = "INT";
}
else if(($check_int == "" && $check_ext == "on") || $IntExt == 1)
{
	$SubmissionType = $iPort["external_record"];
	$DefaultSubmitType = "EXT";
}

if($DefaultSubmitType == "EXT") 
{
	$DisplayStyle = "style='display:none;'";
	$OrganizationText = $iPort["external_ole_report"]["organization"];
}
else
{
	$DisplayStyle = "";
	$OrganizationText = $ec_iPortfolio['organization'];
}

# build ELE list
$DefaultELEArray = $lpf->GET_ELE();
foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
{
	$checked = (is_array($ELEArr) && in_array($ELE_ID, $ELEArr)) ? "CHECKED" : "";
	$ELEList .= "<INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked onClick=\"jsCheckMultiple(this)\" ><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
}





if($startdate!="" && $enddate!="")
{
	$enddate_table_style = "display:block;";
	$add_link_style = "display:none;";
}
else
{
	$enddate_table_style = "display:none;";
	$add_link_style = "display:block;";
}


# Retrieve the OLE PROGRAM
$fieldsSetStyle = 0; # default new record enable all the input

$data = $lpf->RETURN_OLE_PROGRAM_BY_PROGRAMID_MAX($programID);
$createID = $data['CreatorID'];

$luser = new libuser($createID);

$isStudent = $luser->isStudent();
$CreateIDType = ($isStudent==true)?"student":"teacher";

if($isStudent==false)
{
	$isTeacher = $luser->isTeacherStaff();
	$CreateIDType = ($isTeacher==true)?"teacher":"";
}

if (isset($record_id) && !empty($record_id)) { # student already had a record (get from OLE_STUDENT)
	$li = new libdb();
	$sql = "
	SELECT
	  PROGRAMTYPE
	FROM
	  $eclass_db.OLE_PROGRAM
	WHERE PROGRAMID = (SELECT PROGRAMID FROM $eclass_db.OLE_STUDENT WHERE RECORDID = '$record_id')
	";
	$row = $li->returnVector($sql);
	
	$fieldsSetStyle = 0;
		
	//handling old data, for some old data without "PROGRAMTYPE" (is null or empty) , for safe , set it to "T", created by teacher
	$_programType  = (trim($row[0]) == "") ? "T": trim($row[0]);

	if ($_programType  == 'T') { # if the program is initially created by teacher, disable some fields
		$fieldsSetStyle = 1;
	}

//debug_r($requestApprovedBy);

	if (is_array($data) && $CreateIDType=='teacher')
	{				
		if($requestApprovedBy!=0 && $requestApprovedBy!=-1)   // -1 means no need to approve
		{
			$DefaultApprover_IsSelectedbyTeacher=true;	
		}
	}
	
} else if ($programID != "") { # join a record created by teacher  (get from OLE_PROGRAM)
	$programID = $_GET['programID'];
	$fieldsSetStyle = 1;

	if (is_array($data)) {
		$startdate = $data['StartDate'];
		$enddate = $data['EndDate'];
		$title = $data['Title'];
		$chiTitle = $data['TitleChi'];
		$category = $data['Category'];
		$subCategoryID = $data['SubCategoryID'];
		$ele = $data['ELE'];
		$organization = $data['Organization'];
		$details = $data['Details'];
		$chiDetails = $data['DetailsChi'];
		$remark = $data['SchoolRemarks'];
		$input_date = $data['InputDate'];
		$modified_date = $data['ModifiedDate'];
		$period = $data['Period'];
		$int_ext = $data['IntExt'];
		$canJoin = $data['CanJoin'];
		$canJoinStartDate = $data['CanJoinStartDate'];
		$canJoinEndDate = $data['CanJoinEndDate'];
		$userName = $data['UserName'];
		$autoApprove = $data['AUTOAPPROVE'];
		$maximumHours = $data['MaximumHours'];
		$defaultHours = $data['DefaultHours'];
		$compulsoryFields = $data['CompulsoryFields'];
		$requestApprovedBy = $data['DefaultApprover'];
		$IsSAS = $data['IsSAS'];
		$IsOutsideSchool = $data['IsOutsideSchool'];

		if($requestApprovedBy <=0 )  {
			//do nothing 
		}else{  // if the teacher selected DefaultApprover
			$DefaultApprover_IsSelectedbyTeacher=true;
		}
		
		if($record_id=="" && $hours==''){
			$hours=$defaultHours;
		}
	}	
	# build ELE list
	$DefaultELEArray = $lpf->GET_ELE();
	$ele = explode(",", $ele);
	$ELEList = "";
	foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
	{
		$checked = (is_array($ele) && in_array($ELE_ID, $ele)) ? "CHECKED" : "";
		$ELEList .= "<INPUT type='checkbox' name='ele[]' value='".$ELE_ID."' id='".$ELE_ID."' $checked><label for='".$ELE_ID."'>".$ELE_Name."</label><br />";
	}
}

# setting fields
$libpf_slp = new libpf_slp();
$subCategoryArray = $lpf->getSubCategory($category);
if ($fieldsSetStyle==0) { # new

	# submission type
	$SubmissionType = $SubmissionType;
	
	# category
	if($isAllowEditOLE)
	{
		$categoryFieldDisplay = $linterface->GET_SELECTION_BOX($file_array, "name ='category' onChange=\" changeSubCategory(); setTitleLayer();\" ","",$category);;
	}
	else
	{
		# category
		while($elements = current($file_array)) 
		{
			if ($elements[0] == $category) {
				$categoryFieldDisplay = $elements[1];
			}
			next($file_array);
		}
		reset($file_array);	
	}
	
	# subcategory
	if($isAllowEditOLE)
	{
		$subCategoryFieldDisplay = "";
	}
	else
	{
		while($elements = current($subCategoryArray)) 
		{
			if ($elements[0] == $subCategoryID) {
				$subCategoryFieldDisplay = $elements[1];
			}
			next($subCategoryArray);
		}
		reset($subCategoryArray);
	}
	
	# title
	$titleFieldDisplay = getTitleField();

	# date
	$dateFieldDisplay = getDateField();
	
	# ELE list
	if($isAllowEditOLE)
	{
		$ELEList = $ELEList;
	} 
	else
	{
		foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
		{
			if (is_array($ele) && in_array($ELE_ID, $ele)) {
				$ELEListTemp[] .= $ELE_Name;
			} else {
				continue;
			}
		}
		if (count($ELEListTemp)<1) {
			$ELEList = "--";
		} else {
			$ELEList = implode("<br />", $ELEListTemp);
		}
	}
	
	# organization
	$organizationFieldDisplay = $isAllowEditOLE ? "<input name=\"organization\" type=\"text\" value=\"{$organization}\" maxlength=\"256\" class=\"textboxtext\">" : $organization;

	# hours
	$hoursFieldDisplay = $isAllowEditOLE ? "<input name='hours' type='text' size='10' value='$hours' class='tabletext'>" : $hours;
	
	# role
	$roleFieldDisplay = getRoleField();
	
	# achievement
//	$achievementFieldDisplay = "<input name='achievement' type='text' size='50' value='$achievement' maxlength='256' class='textboxtext'>";
//	$achievementFieldDisplay = "<input name=\"achievement\" type=\"text\" size=\"50\" value=\"{$achievement}\" maxlength=\"256\" class=\"textboxtext\">";
//	$achievementFieldDisplay = "<input name='achievement' type='text' size='50' value='$achievement' maxlength='256' class='textboxtext'>";
	$achievementFieldDisplay = getAchievementField();

	# attachment
	$attachmentFieldDisplay = getAttachmentField();
	
	# details
	$detailFieldDisplay = $isAllowEditOLE ? $linterface->GET_TEXTAREA("details", $details) : nl2br($details);

	# school remark
	$schoolRemarkFieldDisplay = $remark;

} else if ($fieldsSetStyle==1) {  //$fieldsSetStyle==1 --> disable some input field
	# submission type
	$SubmissionType = $SubmissionType;
	
	# category
	while($elements = current($file_array)) {
		if ($elements[0] == $category) {
			$categoryFieldDisplay = $elements[1];
		}
		next($file_array);
	}
	reset($file_array);
	
	# subcategory
	if ($subCategoryID==-9 || trim($subCategoryFieldDisplay)=="") {
		$subCategoryFieldDisplay = "--";
	} else {
		while($elements = current($subCategoryArray)) {
			if ($elements[0] == $subCategoryID) {
				$subCategoryFieldDisplay = $elements[1];
			}
			next($subCategoryArray);
		}
		reset($subCategoryArray);
	}
	
	# title
	$titleFieldDisplay = $title;	
	
	# date
	$dateFieldDisplay = (($startdate == '0000-00-00'|| empty($startdate)) ? "--" : $startdate)."&nbsp;".$profiles_to."&nbsp;".(($enddate == '0000-00-00' || empty($enddate)) ? "--" : $enddate);
	
	# ELE list
	foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
	{
		if (is_array($ele) && in_array($ELE_ID, $ele)) {
			$ELEListTemp[] .= $ELE_Name;
		} else {
			continue;
		}
	}
	if (count($ELEListTemp)<1) {
		$ELEList = "--";
	} else {
		$ELEList = implode("<br />", $ELEListTemp);
	}
	
	# organization
	$organizationFieldDisplay = (empty($organization) ? "--":$organization);
	
	# hours
	if (!empty($maximumHours)) {
		$h_maximumHours = $ec_iPortfolio['SLP']['MaximumHours'].": ".$maximumHours;
	}
	$hoursFieldDisplay = $isAllowEditOLE ? "<input name='hours' type='text' size='10' value='$hours' class='tabletext'> ".$h_maximumHours : $hours; 
	
	# role
	$roleFieldDisplay = getRoleField();
	
	# achievement
//	$achievementFieldDisplay = "<input name='achievement' type='text' size='50' value='$achievement' maxlength='256' class='textboxtext'>";
	$achievementFieldDisplay = getAchievementField();

	# attachment
	$attachmentFieldDisplay = getAttachmentField();
	
	# details
	if (empty($details)) {
		$detailFieldDisplay = "--";
	} else {
		$detailFieldDisplay = nl2br($details);
	}

	# school remark
	$displaySchoolRemark = 1;
	$schoolRemarkFieldDisplay = nl2br($remark);

} else {
	// do nothing
}

$IsSASChecked = ($IsSAS) ? "checked='checked'" : "";
if (!isset($IsOutsideSchool))
{
	$IsOutsideSchool = 0;
}
$IsOutsideSchoolChecked[$IsOutsideSchool] = true;



$redStar = '<span class="tabletextrequire">*</span>';
// compulsory fields elements setting
if (!empty($compulsoryFields)) {
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Hours"]) !== false) {
		$cpsry_hours_star = $redStar;
		$cpsry_hours_jsChecking = <<<JSEND
	if ( Number(\$("input[name=hours]").val())==0 ) {
		alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['hours']}");
		return false;
	}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["RoleOfParticipation"]) !== false) {
		$cpsry_role_star = $redStar;
		$cpsry_role_jsChecking = <<<JSEND
		if ( \$("input[name=ole_role]").val()=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['ole_role']}");
			return false;
		}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Achievement"]) !== false) {
		$cpsry_achievement_star = $redStar;
		$cpsry_achievement_jsChecking = <<<JSEND
		if ( \$("input[name=achievement]").val()=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['achievement']}");
			return false;
		}
JSEND;
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Attachment"]) !== false) {
		$cpsry_attachment_star = $redStar;
		$cpsry_attachment_jsChecking = <<<JSEND
		\$targetFileUpload = \$("input[name^=ole_file]");
		var allFileString = "";
		\$targetFileUpload.each(function() {
			allFileString = allFileString + $(this).val();
		});
		if ( allFileString=="" ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['attachment']}");
			return false;
		}
JSEND;
	}
		
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["Details"]) !== false) {
		$cpsry_details_star = $redStar;
		$cpsry_details_jsChecking = "";// do nothing in this moment
	}
	if (strpos($compulsoryFields,(string)$ipf_cfg["OLE_PROGRAM_CompulsoryFields"]["ApprovedBy"]) !== false) {
		$cpsry_approved_by_star = $redStar;
		$cpsry_approved_by_jsChecking = <<<JSEND
		if ( Number(\$("select[name=approved_by]").val())==0 ) {
			alert("{$ec_iPortfolio['SLP']['PleaseFillIn']} {$ec_iPortfolio['approved_by']}");
			return false;
		}
JSEND;
	}
}
############## 20101124
 // S79668
$requreJSArr = array();
if(empty($programID)){
	$objIpfSetting = new iportfolio_settings();
	$requiredField = $redStar;
	if($IntExt != 1){
		// OLE
		$CompulsorySettingArr = explode(',',$objIpfSetting->findSetting($ipf_cfg["Student_OLE_CompulsoryFields"]["SettingName"]));
	}
	else{
		// Performance / Awards and Key Participation Outside School
		$CompulsorySettingArr = explode(',',$objIpfSetting->findSetting($ipf_cfg["Student_OLEExt_CompulsoryFields"]["SettingName"]));
	}
	foreach((array)$CompulsorySettingArr as $fieldname){
		$requiredSetting[$fieldname] = $requiredField;
		$requreJSArr[] = $fieldname;
	}
}
// $DefaultApprover_IsSelectedbyTeacher means the program is created by teacher and Approver is selected by the teacher
if (($approvalSettings["IsApprovalNeed"] && $approvalSettings["Elements"]["Self"]) || $DefaultApprover_IsSelectedbyTeacher==true) {
	# teacher selection
	$teacherRecordStatus = 1; //get those active teacher account only
	$teacher_selection = $lpf->GET_TEACHER_NAME($requestApprovedBy); 	
	$hiddenField_Approver = '<input type="hidden" name="request_approved_by" value="'.$requestApprovedBy.'" >';
	
	if($isAllowEditOLE || $teacher_selection=='' || $CreateIDType=='student' || $action=="new")
	{
		$teacher_selection = $lpf->GET_TEACHER_SELECTION_IPF($requestApprovedBy); 
		$hiddenField_Approver='';
	}
	
	$TeacherSelectionRow = "<tr valign=\"middle\" class=\"chi_content_12\">
							<td valign=\"top\" nowrap=\"nowrap\">".$Lang['iPortfolio']['preferred_approver']."$cpsry_approved_by_star".$requiredSetting['preferred_approver']." :</td>
							<td>";
	if($isAllowEditOLE || $action=="new")
		$TeacherSelectionRow.= $teacher_selection;
	else
		$TeacherSelectionRow.= $lpf->GET_TEACHER_NAME($requestApprovedBy);
	$TeacherSelectionRow.= "</td>";
	$TeacherSelectionRow.= "</tr>";
} 
else {
	
	# default teacher: class teacher
	if($approved_by=="")
	{
		$ClassTeacher = $lpf->GET_CLASS_TEACHER($ClassName);
		$approved_by = $ClassTeacher[0];
	}
}


# Eric Yip (20100201): set back page
//$back_url = ($IntExt == 1 || isset($AcademicYearID)) ? "ole_record.php?IntExt={$IntExt}&AcademicYearID={$AcademicYearID}&ELE={$ELE}&status={$status}" : "ole.php";
//$back_url = ($IntExt == 1 || isset($AcademicYearID)) ? "ole_record.php?IntExt={$IntExt}&AcademicYearID={$AcademicYearID}&ELE={$ELE}&status={$status}" : "ole_record.php?AcademicYearID={$AcademicYearID}";
$back_url = "ole_record.php?IntExt={$IntExt}";

$linterface->LAYOUT_START();
include_once("template/ole_new.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

///////////////////////////////////
//////////// function /////////////
///////////////////////////////////
function getAttachmentField() {
	global $attachments_html, $button_more_file, $isAllowEditOLE, $action;
	
	$table .= $attachments_html;
	
	if($isAllowEditOLE || $action=="new")
	{
		$table .= "
		<table border='0' cellpadding='0' cellspacing='0' id='tableAttachment' style='display:none' >
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<a class='tablelink' href='javascript:document.getElementById(\"tableAttachment\").style.display=\"block\";jAddMoreFileField(this.form, \"tableAttachment\", \"attachment_size\", \"ole_file\")' >" . $button_more_file . "</a>
		";
	}
	return $table;
}
function getRoleField() {
	global $ole_role, $isAllowEditOLE, $action;
	$table .= "
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"60%\" >
	<tr>
		<td width=\"100%\" > ";
	if($isAllowEditOLE || $action=="new")
		$table .= "<input id=\"ole_role\" name=\"ole_role\" type=\"text\" size=\"50\" value=\"{$ole_role}\" class=\"textboxtext\"  maxlength=\"255\">";
	else
		$table .= $ole_role;
	$table .= "</td>";

	if($isAllowEditOLE || $action=="new")
	{
		$table .= "<td width=\"5\">&nbsp;</td>
		<td width=\"25\" ><div id=\"change_preset2\">" .  GetPresetText("jRoleArr1", 2, "ole_role", "", '', '390') . "</div></td>";
	}
	$table .="</tr>
</table>
";
	return $table;
}


function getAchievementField() {
	global $achievement, $isAllowEditOLE, $action;
	$table .= "
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"60%\" >
	<tr>
		<td width=\"100%\" >";
	if($isAllowEditOLE || $action=="new")
		$table .= "<input id=\"achievement\" name=\"achievement\" type=\"text\" size=\"50\" value=\"{$achievement}\" class=\"textboxtext\"  maxlength=\"255\">";
	else
		$table .= $achievement;
	$table .= "</td>";
	
	if($isAllowEditOLE || $action=="new")
	{
		$table .= "<td width=\"5\">&nbsp;</td>
		<td width=\"25\" ><div id=\"change_preset2\">" .  GetPresetText("jAwardsArr1", 3, "achievement", "", '', '390') . "</div></td>";
	}

	$table .= "</tr>
</table>
";
	return $table;
}

function getTitleField() {
	global $title, $PATH_WRT_ROOT, $isAllowEditOLE;
	$table .= "
				<table border='0' cellpadding='0' cellspacing='0' width='60%' >
				<tr>
					<td width='100%' >";
	if($isAllowEditOLE)
		$table .= "<input id='title' name='title' type='text' size='50' value='" . intranet_htmlspecialchars($title) . "' class='textboxtext'  maxlength='255'>";
	else
		$table .= intranet_htmlspecialchars($title);
	
	$table .= "</td>";
	
	if($isAllowEditOLE)
	{
		$table .="<td width='5'>&nbsp;</td>
					<td width='25' ><div id='change_preset_loading' style='display: none;'><img src='" . $PATH_WRT_ROOT . "images/2009a/indicator.gif'></div><div id='change_preset'>" .  GetPresetText("jCurrentArr", 1, "title", "jPRESET_ELE()", '', '390')  . "</div></td>";
	}
	$table .= "</tr>
				</table>
";


	return $table;
}
function getDateField() {
	global $startdate, $enddate_table_style, $add_link_style, $profiles_to, $enddate, $ec_iPortfolio, $linterface, $isAllowEditOLE;
	
	if($isAllowEditOLE)
	{
		$table .= "
					<table border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td nowrap='nowrap'>
						<input type='text' name='startdate' size='11' maxlength='10' class='tabletext' value='" . ($startdate == '0000-00-00' ? "" : $startdate) . "' onFocus='if(this.value==\"\"){this.value=\"" . date("Y-m-d"). "\";}' />
						<span id='startdatecal'>" . $linterface->GET_CALENDAR("form1", "startdate") . "</span>
						</td>
						<td nowrap='nowrap' id='table_enddate' style='" . $enddate_table_style. "'>&nbsp;" . $profiles_to. "&nbsp;<input class='tabletext' type='text' name='enddate' size='11' maxlength='10' value='" . ($enddate == '0000-00-00' ? "" : $enddate) . "' onFocus='if(this.value==\"\"){this.value=\"" . date("Y-m-d"). "\";}'>
						<span id='enddatecal'>" . $linterface->GET_CALENDAR("form1", "enddate"). "</span>
					</tr>
					<tr id='add_link' style='" . $add_link_style. "'><td height='25'><a href='javascript:displayTable(\"add_link\", \"none\");displayTable(\"table_enddate\", \"block\");' class='tablelink'>" . $ec_iPortfolio['add_enddate'] ."</a></td></tr>
					</table>
		";
	}
	else
	{
		$table .= "
					<table border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td nowrap='nowrap'>". $startdate . 
						($enddate ? "&nbsp;" . $profiles_to. "&nbsp; " : "" ) .
						$enddate ."</td>
					</tr>
					</table>
		";
	}
return $table;
}

############ 20101124
function checkAllowEditOLE($record_id,$approvalSettings) {
	global $eclass_db,$ipf_cfg,$lpf;
	### References diagram (logic derived from old function)
	### /addon/script/max/doc/References/checkAllowEditOLE.jpeg
	### Refined Logic
	### /addon/script/max/doc/References/checkAllowEditOLE2.jpeg
	############################################################
	if (!empty($record_id)) {
		$sql = "SELECT RecordStatus FROM {$eclass_db}.OLE_STUDENT WHERE RecordID = '$record_id'";
		$status = current($lpf->returnVector($sql));
	}

	if (
		empty($record_id)
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] || $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"])
		|| (isset($status) && $status==$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]) && (!$approvalSettings['IsApprovalNeed'] && $approvalSettings['Elements']['Editable'])
		) {
		$isAllow = 1;
	} else {
		$isAllow = 0;
	}
	return $isAllow;
}

function getActivityRoleList()
{
	$RoleListArr = array();
	$LibWord2 = new libwordtemplates_ipf(4);

	# float list arr
	$role_array = $LibWord2->getWordList(0);
	for ($i=0; $i<sizeof($role_array); $i++) {
		$RoleListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($role_array[$i]));
	}
	return $RoleListArr;
}

function getActivityAwardsList(){
 $LibWordAward = array();
 $LibWordAward = new libwordtemplates_ipf(5);

# float list arr
 $awards_array = $LibWordAward->getWordList(0);
 for ($i=0; $i<sizeof($awards_array); $i++) {
       $AwardsListArr[] = str_replace("&amp;", "&", undo_htmlspecialchars($awards_array[$i]));
 }

	return $AwardsListArr;


}
?>
