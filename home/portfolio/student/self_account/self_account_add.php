<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * Date : 2016-01-12 Omas
 * 		  modified js_Submit() - add 'setDefault' value as hidden field instead of using GET- #J91331 
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";

# Define name of textarea
define("TEXTAREA_OBJ_NAME", "Details");

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$StudentID = $_SESSION['UserID'];
$RecordID = IntegerSafe(intval($_POST['RecordID']));

$li_pf = new libpf_slp();

$sa_config_file = $eclass_filepath."/files/portfolio_self_account.txt";
$word_limit = (int) get_file_content($sa_config_file);

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

$redirect_link = 'self_account_add_update.php';
if($RecordID > 0)
{
	# Get self account information
	$self_acc = $li_pf->GET_SELF_ACCOUNT($RecordID);
	# Escape special characters
	$self_acc['Title'] = intranet_htmlspecialchars($self_acc['Title']);
	
	if ($self_acc['UserID'] != $_SESSION['UserID']) {
		No_Access_Right_Pop_Up();
		die();
	}
	
	$NavigationButton =$button_edit;
}
else
{	
	$NavigationButton =$button_new;
}

$selfAccListArr = $li_pf->GET_SELF_ACCOUNT_LIST($StudentID);
$count_selfAccListArr = count($selfAccListArr);

$showConfirmBox = 1;
if($count_selfAccListArr==0)
{
	$showConfirmBox = 0;
}


// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPage = "Student_SelfAccount";
$CurrentPageName = $iPort['menu']['self_account'];

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">
	
function jADD_ATTACHMENT_ROW()
{
	var attachment_list = document.getElementById('attachment_list');
	var new_row = attachment_list.insertRow(attachment_list.rows.length);
	var new_cell = new_row.insertCell(0);
	
	new_cell.innerHTML =	"<input type='file' name='attachment[]' />";
	
	window.location.href = "#attachment_list_anchor";
}

function jINSERT_HTML(jParHTML)
{
//  var oEditor = FCKeditorAPI.GetInstance('<?=TEXTAREA_OBJ_NAME?>');
  
//  oEditor.InsertHtml(jParHTML);

  $("#Details").append(jParHTML);
}

// function for view records
function jDISPLAY_RECORDS(jParRecordType)
{
  $.ajax({
		type: "GET",
		url: "../../ajax/get_"+jParRecordType+"_html_ajax.php",
		data: "objname=<?=TEXTAREA_OBJ_NAME?>",
		success: function (data) {
			$("#RecordsLayer").html(data);
		}
	});
}

/*
functions for delete attachments
1. jDELETE_ATTACHMENT
2. CompleteDeleteAttachment
*/
/*
function jDELETE_ATTACHMENT(jParAttachment)
{
	if(confirm("<?=$ec_iPortfolio['delete_attachment_confirm']?>"))
	{
	  xmlHttp_attachment = GetXmlHttpObject();
	  if (xmlHttp_attachment == null)
	  {
	    alert ("Your browser does not support AJAX!");
	    return;
	  }
	
	  // Modify Layer Content
	  var url="./ajax/delete_attachment_ajax.php";
	  url=url+"?attachment=" + jParAttachment;
	  url=url+"&path=<?=base64_encode($dir_path)?>";
	  url=url+"&RecordID=<?=$RecordID?>";
	  url=url+"&sid="+Math.random();
	
	  xmlHttp_attachment.onreadystatechange = CompleteDeleteAttachment;
	  xmlHttp_attachment.open("GET",url,true);
	  xmlHttp_attachment.send(null);
	}
}

function CompleteDeleteAttachment() 
{ 
  if (xmlHttp_attachment.readyState==4)
  {
    var Attachment_HTML = xmlHttp_attachment.responseText;
    var AttachmentLayer = document.getElementById("AttachmentLayer");
    AttachmentLayer.innerHTML = Attachment_HTML;
  }
}
*/


function jCOUNT_WORD() {
  var oEditor = FCKeditorAPI.GetInstance('<?=TEXTAREA_OBJ_NAME?>');
  var matches = oEditor.GetData().replace(/<[^<|>]+?>|&nbsp;/gi,' ').match(/\b/g);
  var count = 0;
  if(matches) {
      count = matches.length/2;
  }
  count = $('#word_count').wordCount();
  return count;
}

function jUPDATE_WORD_COUNT() {
  count = jCOUNT_WORD();
  $("#word_count").text(count);
}

function FCKeditor_OnComplete( editorInstance ) {
  jUPDATE_WORD_COUNT();
  if (document.all) {      
    // IE
    editorInstance.EditorDocument.attachEvent("onkeyup", jUPDATE_WORD_COUNT);
  } else {
    // other browser
    editorInstance.EditorDocument.addEventListener( 'keyup', jUPDATE_WORD_COUNT, true );
  }   
}

function js_Submit()
{

	var showConfirmBox = <?=$showConfirmBox?>;
	var inputContent = $('#Details').val();
	var checkPass = 1;

	//disable the save button to prevent double submit
	$('#id_Save').disabled=true;

	if($.trim(inputContent) == ''){
		alert('<?=$Lang['iPortfolio']['selfAccountIsEmpty']?>');
		checkPass = 0;
		$('#id_Save').disabled=false;  // reactive the save button
		$('#Details').focus();
	}

	if(checkPass == 1){
		$('input#id_Save').attr('disabled', 'disabled');
		
		if(showConfirmBox==1)
		{
			if(confirm("<?=$ec_iPortfolio['setThisDefaultSA'] ?>"))
			{
//				document.form1.action += '?setDefault=1';	
				var input = document.createElement("input");
				input.setAttribute("type", "hidden");
				input.setAttribute("name", "setDefault");
				input.setAttribute("value", "1");
				document.getElementById("form1").appendChild(input);
			}
		}
		else
		{
//			document.form1.action += '?setDefault=1';	
			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "setDefault");
			input.setAttribute("value", "1");
			document.getElementById("form1").appendChild(input);
		}
		
		document.form1.submit();
	}
}

function textCounter() {

    var number = 0;
    var matches = $('#Details').val().match(/\b/g);
    if(matches) {
        number = matches.length/2;
    }
    $('#word_count').text( number);

}

$(document).ready(function() {
  textCounter();
  $("form[name='form1']").submit(function() {
    count = jCOUNT_WORD();
    if(<?=$word_limit?> && count > <?=$word_limit?>)
    {
      alert("<?=str_replace("<!--WordLimit-->", $word_limit, $Lang['iPortfolio']['alertExceedWordLimit'])?>");
      return false;
    }

    return true;
  });
});


</SCRIPT>

<form id="form1" name="form1" method="POST" enctype="multipart/form-data" action="<?=$redirect_link?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="self_account.php"><?=$ec_iPortfolio['self_account']?></a> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$NavigationButton?> </td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="400" align="center" valign="top" class="stu_info_log tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0" style="border:dashed 1px #999999">
										<tr>
											<td>
												<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif" width="20" height="20" align="absmiddle">
												<span class="tabletextremark"><?=$ec_iPortfolio['view_my_record']?><br></span>
												<table border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('activity')"> <?=$ec_iPortfolio['activity']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_5"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('award')"> <?=$ec_iPortfolio['award']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_4"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('comment')"> <?=$ec_iPortfolio['title_teacher_comments']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_5"></a> </td>
														<td align="center" nowrap><a href="#" class="tablelink" onClick="jDISPLAY_RECORDS('ole')"><?=$ec_iPortfolio['ole']?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_7"></a></td>
														<!--<td align="center" nowrap><a href="#" class="tablelink"><?=$i_general_others?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/leftmenu/btn_arrow_more_off.gif" width="15" height="30" border="0" align="absmiddle" id="lb_8"></a> </td>-->
													</tr>
												</table>
												<div id="RecordsLayer" style="overflow:auto;" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td class="tabletext" width="30"><strong><?=$i_general_title?></strong></td>
											<td class="tabletext">
												<input type="text" name="Title" class="tabletext" style="width:100%" value="<?=$self_acc['Title']?>" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
<?php
//	$use_html_editor = true;
	$use_html_editor = false;
	if ($use_html_editor)
	{
		# Components size
		$msg_box_width = "100%";
		$msg_box_height = 200;
		$obj_name = TEXTAREA_OBJ_NAME;
		$editor_width = $msg_box_width;
		$editor_height = $msg_box_height;
		$init_html_content = $self_acc['Details'];
		
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		
		$objHtmlEditor = new FCKeditor($obj_name, $msg_box_width, $editor_height);
		$objHtmlEditor->Value = $init_html_content;
		$objHtmlEditor->Create();
	}
	else
	{
//error_log("thisDetails [".$self_acc['Details']."] <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		$thisDetails = str_replace('</p>',"\n",$self_acc['Details']);
		$thisDetails = strip_tags($thisDetails);
//		$thisDetails = html_entity_decode($thisDetails);
//		$thisDetails = $self_acc['Details'];

//error_log("thisDetails [".$thisDetails."] <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		echo "<textarea class=\"textboxtext\" name=\"Details\" ID=\"Details\" cols=\"80\" rows=\"16\" wrap=\"virtual\" onKeyUp=\"textCounter();\" >".$thisDetails."</textarea>";
	}
?>

								</td>
							</tr>
							<tr>
                <td>
                  <table cellspacing="1" width="100%" border="0">
                    <tr>
                      <td nowrap="nowrap"><?=$Lang['iPortfolio']['selfAccountWordCount'][0]?></td>
                      <td id="word_count" width="1%">&nbsp;</td>
                      <td nowrap="nowrap"><?=$Lang['iPortfolio']['selfAccountWordCount'][1]?></td>
					<td align="right" width="100%">					
					<?= (($sys_custom["ywgs_ole_report"]) ?  $Lang['iPortfolio']['page_break_ywgs'] : "&nbsp;") ?>
					</td>
                    </tr>
                  </table>
                </td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<input type="button" class="formbutton" onClick="js_Submit();" value="<?=$button_save?>"  id="id_Save" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'">
									<!-- <input type="button" class="formbutton" onClick="js_Submit();" value="<?=$button_save?>"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'">-->
									<input type="button" class="formbutton" onClick="this.disabled=true;self.location='self_account.php'"  onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
<input type="hidden" name="RecordID" value="<?=$RecordID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
