<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

//include_once($PATH_WRT_ROOT."includes/libportfolio-eva.php");

intranet_opendb();

$StudentID = $_SESSION['UserID'];

$li_pf = new libpf_slp();

# Set photo in the left menu
$luser = new libuser($StudentID);
$luser->PhotoLink = $li_pf->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentID);
$luser->PhotoLink = str_replace($intranet_root, "", $luser->PhotoLink[0]);

// $self_acc_display = $li_pf->GEN_SELF_ACCOUNT_LIST_TABLE($StudentID);
$DefaultSA_display = $li_pf->GEN_DEFAULT_SELF_ACCOUNT_TABLE($StudentID,$showLastModifiedDate=true);


// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
$CurrentPage = "Student_SelfAccount";
$CurrentPageName = $iPort['menu']['self_account'];

## Show instruction box
$msg = $_GET['msg'];

if($msg==1)
{
	$instruction_html =' <tr>
						  	<td>'. $linterface->Get_Warning_Message_Box('<font color="red">'.$eDiscipline["Instruction"].'</font>', '<font color="red">'.$ec_warning['DefaultSA_by_ModifiedDate'].'</font>', "").'</td>
						  </tr>';
}


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $li_pf->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE="Javascript">

// Using post to go to edit instead of using get (security)
function jEDIT_SA(jParRecordID){
	document.form1.RecordID.value = jParRecordID;
	document.form1.action = "self_account_edit.php";
	document.form1.submit();
}

function jUNCHECK_RECHECK(jParObjName, jParValueCheck){

	len = document.form1.elements.length;
	var i=0;
	for(i=0; i<len; i++) {
		if(document.form1.elements[i].name == jParObjName)
		{
			var CheckStatus = document.form1.elements[i].checked;
			if(document.form1.elements[i].value != jParValueCheck)
				CheckStatus = false;
		
			document.form1.elements[i].checked = CheckStatus;
		}
  }
}

</SCRIPT>

<form name="form1" method="POST" action="self_account_set_default_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

	<?=$instruction_html?>
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tabletext">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td>
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><a href="self_account.php" class="contenttool"> <?=$iPort['menu']['self_account']?> </a>
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle"><?=$iPort["select_default_sa"]?>
								</td>
								<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
							</tr>
						</table>
					</td>
					<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
				</tr>
			</table>
			<?=$DefaultSA_display?>
			<table width='100%' border='0' cellpadding='5' cellspacing='0'>
				<tr>
					<td align='right'>
						<?=$linterface->GET_BTN($button_submit, "submit")?>
						<?=$linterface->GET_BTN($button_cancel, "button", "this.disabled=true;self.location='self_account.php'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" value="<?=$StudentID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
