<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("S");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_slp();

if ($StudentID != $_SESSION['UserID']) {
	No_Access_Right_Pop_Up();
	die();
}

if(is_array($_POST))
{
	foreach($_POST as $key => $option)
	{
		if ($key == 'StudentID') {
			continue;
		}
		list($type, $value) = explode("_", $key);
		if($type == 'SLP')
			$DefaultSA[$option][] = 'SLP';
		else
			$DefaultSA[$option][] = $value;
	}
}

$li_pf->SET_DEFAULT_SELF_ACCOUNT($StudentID, $DefaultSA);

intranet_closedb();
header("Location: ./self_account.php");

?>
