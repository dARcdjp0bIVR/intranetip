<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


// Initializing classes
$LibUser = new libuser($UserID);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
if ($ck_memberType=="S")
	$CurrentPage = "Student_ExtOLE";
else if ($ck_memberType=="P")
	$CurrentPage = "Parent_ExtOLE";
$CurrentPageName = "<font size='-2'>".$iPort["external_record"]."</font>";

### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

# load approval setting
$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();

$vars = "ClassName=$ClassName&StudentID=$StudentID";
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else if ($ck_memberType=="P")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($LibPortfolio->IP_USER_ID_TO_EC_USER_ID($ck_current_children_id));

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else 
{
	header("Location ../index.php");
}

#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";
####################################

// load certificate submission period settings
$student_award_config_file = "$eclass_root/files/student_ole_config.txt";
$filecontent = trim(get_file_content($student_award_config_file));	
list($starttime, $sh, $sm, $endtime, $eh, $em) = unserialize($filecontent);

$starttime = (trim($starttime)!="") ? $starttime." $sh:$sm:00" : "";
$endtime = (trim($endtime)!="") ? $endtime." $eh:$em:59" : "";

$ValidPeriod = (($starttime!="" && $starttime!="0000-00-00 00:00:00") || ($endtime!="" && $endtime!="0000-00-00 00:00:00")) ? validatePeriodByTimeStamp($starttime, $endtime) : 1;

// Create a status filter
$status_selection = "<SELECT name='status' onChange='this.form.submit()'>";
$status_selection .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
//$status_selection .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection .= "</SELECT>";
$status_selection_html = $ec_iPortfolio['status']." : ".$status_selection;

# cateogry selection
//$category_selection = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1, true);
$category_selection = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='this.form.submit()'", $category, 1);
//$category_selection_html = $ec_iPortfolio['category']." : ".$category_selection;
$category_selection_html = $category_selection;


$ELEArray = $LibPortfolio->GET_ELE();
$ELECodeArray = array_keys($ELEArray);
if (count($ELECodeArray)>0)
{
	$ELEImplode = implode(",",$ELECodeArray);
}
$ELECount = count($ELECodeArray);

# Searching box
$searching_html = "<input name='search_text' value='".$search_text."' >&nbsp;<input class='button_g' onClick=\"this.form.submit();\" type='button' value=\"".$button_search."\">";

$YearArray = $LibPortfolio->GET_OLE_DISTINCT_YEAR($UserID);
$YearSelectionHTML = $linterface->GET_SELECTION_BOX($YearArray,"name='Year' onChange='this.form.submit()'", $i_general_all, $Year);

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

if ($order=="") $order=0;
if ($field=="") $field=2;
$LibTable = new libdbtable2007($field, $order, $pageNo);
$LibTable->field_array = array("a.Title","OLEDate",  "a.ELE", "a.Role","a.Achievement","a.Category", "a.Hours", "a.Attachment", "a.RecordStatus");

$conds = ($status=="") ? "" : (($status=="2") ? " AND a.RecordStatus IN ('2','4')" : " AND a.RecordStatus = '$status'");
$conds .= ($category=="") ? "" : " AND a.Category = '$category'";
$conds .= " AND c.IntExt = 'EXT'";
if($Year!="")
{
	$SplitArray = explode("-", $Year);
	if(sizeof($SplitArray)>1)
	{
		$LowerDate = date("Y-m-d", mktime(0, 0, 0, 9, 1, trim($SplitArray[0])));
		$UpperDate = date("Y-m-d", mktime(0, 0, 0, 8, 31, trim($SplitArray[1])));
	}
	else
	{
		$LowerDate = date("Y-m-d", mktime(0, 0, 0, 9, 1, $Year));
		$UpperDate = date("Y-m-d", mktime(0, 0, 0, 8, 31, $Year+1));
	}
	$conds .= " AND a.StartDate BETWEEN '".$LowerDate."' AND '".$UpperDate."'";
}

$namefield = getNameFieldWithClassNumberByLang("b.");

if($ValidPeriod == 1)
$edit_field = ($ck_memberType=="S") ? " , if(a.RecordStatus = 1, CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'), '')" : "";
else
$edit_field == "";

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("a.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";

if($search_text!="")
{
	$search_cond = " AND (a.Title LIKE '%".$search_text."%' OR a.Details LIKE '%".$search_text."%' OR a.Role LIKE '%".$search_text."%'  OR a.Achievement LIKE '%".$search_text."%')";
}

//$SqlELEFields
$sql = "	SELECT              
				a.RecordStatus,
				'{$ELECount}',	
				'{$ELEImplode}',
              	CONCAT('<a class=\"tablelink\" href=\"ole_detail_ext.php?record_id=', a.RecordID, '\">', a.Title, '</a>'),
              	IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,'<br />$profiles_to<br />',a.EndDate))) as OLEDate,
              	a.ELE,
              	a.Role,
              	a.Achievement,
			  	$SqlCategoryField			  	
			  	IF ((a.Attachment!='' AND a.Attachment IS NOT NULL), CONCAT('<a href=\"attach.php?RecordID=', a.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '&nbsp;')				  	
		";
//$sql .= ($ApprovalSetting==1) ? " ,IF (a.ApprovedBy='' OR a.ApprovedBy IS NULL OR a.RecordStatus='4', '--', $namefield) as ApprovedBy" : "";
$sql .= " 	,CONCAT(CASE a.RecordStatus
				WHEN 1 THEN '".$ec_iPortfolio['pending']."'
                WHEN 2 THEN '".$ec_iPortfolio['approved']."'
                WHEN 3 THEN '".$ec_iPortfolio['rejected']."'
				WHEN 4 THEN '".$ec_iPortfolio['teacher_submit_record']."'
              	ELSE '--' END, IF (a.ProcessDate,CONCAT('<br>',DATE_FORMAT(a.ProcessDate,'%Y-%m-%d')),''))
			  	$edit_field	  	
			FROM 
				{$eclass_db}.OLE_STUDENT as a
			LEFT JOIN 
				{$intranet_db}.INTRANET_USER as b
			ON 
				a.ApprovedBy = b.UserID
			LEFT JOIN
				{$eclass_db}.OLE_PROGRAM as c
			ON
				a.ProgramID = c.ProgramID
        	WHERE 
				a.UserID = '$StudentID'
				$conds
				$search_cond
			";
//hdebug($sql);			
			
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;

if($ck_memberType=="S")
	$LibTable->no_col = 13;
else
	$LibTable->no_col = ($ApprovalSetting==1) ? 13 : 12;
	
if($ValidPeriod == 1)
{}
else
$LibTable->no_col = $LibTable->no_col - 1;
	
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->IsColOff = "iPortfolioStudentOLE1_EXT";
$LibTable->additionalCols = count($ELECodeArray);
	

// TABLE COLUMN

$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' align='center' >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='100' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td >".$LibTable->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
$LibTable->column_list .= "<td >".$LibTable->column(3,$ec_iPortfolio['ole_role'], 1)."</td>\n";
$LibTable->column_list .= "<td >".$LibTable->column(4,$ec_iPortfolio['achievement'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(5,$ec_iPortfolio['category'], 1)."</td>\n";
//$LibTable->column_list .= "<td class='tabletop' >".$LibTable->column(5,$ec_iPortfolio['attachment'], 1)."</td>\n";
$LibTable->column_list .= "<td  >&nbsp;</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(7,$ec_iPortfolio['status']."/<br>".$ec_iPortfolio['process_date'], 1)."</td>\n";

if($ValidPeriod == 1)
$LibTable->column_list .= ($ck_memberType=="S") ? "<td >".$LibTable->check("record_id[]")."</td>\n" : "";

$LibTable->column_list .= "</tr>\n";


//$LibTable->column_list .= 

if($ck_memberType=="S")
	$LibTable->column_array = array(10,10,0,10,0,0,10,0);
else
	$LibTable->column_array = ($ApprovalSetting==1) ? array(10,10,0,10,0,0,10,10) : array(10,10,0,10,0,0,10);
	
//////////////////////////////////////////////

//check if submission period is over
$student_ole_config_file = "$eclass_root/files/student_ole_config.txt";
$filecontent = trim(get_file_content($student_ole_config_file));
list($starttime, $sh, $sm, $endtime, $eh, $em, $check_int, $check_ext) = unserialize($filecontent);

$startSubmissionDate = strtotime($starttime." ".$sh.":".$sm);
$startSubmissionDate = ($startSubmissionDate == -1) ? false : $startSubmissionDate;
$endSubmissionDate = strtotime($endtime." ".$eh.":".$em);
$endSubmissionDate = ($endSubmissionDate == -1) ? false : $endSubmissionDate;
$nowDate = time();
if ((!$startSubmissionDate || $startSubmissionDate <= $nowDate) && (!$endSubmissionDate || $endSubmissionDate >= $nowDate) && $check_ext == "on")
{
	$overSubmission = 0;
}
else
{
	$overSubmission = 1;
}

$starttime = (trim($starttime)!="") ? sprintf($starttime." %02s:%02s:00", $sh, $sm) : "";
$endtime = (trim($endtime)!="") ? sprintf($endtime." %02s:%02s:00", $eh, $em) : "";

if(($starttime!="" && $starttime!="0000-00-00 00:00:00") && ($endtime!="" && $endtime!="0000-00-00 00:00:00"))
  $Period_Msg = $iPort['period_start_end']." ".$starttime." ".$iPort['to']." ".$endtime;
else if($starttime!="" && $starttime!="0000-00-00 00:00:00")
  $Period_Msg = $iPort['period_start']." ".$starttime;
else if($endtime!="" && $endtime!="0000-00-00 00:00:00")
  $Period_Msg = $iPort['period_start']." ".$endtime;
else
  $Period_Msg = "";

# Check if student has right to set OLE records for SLP report
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);

if(is_array($OLESettings))
	$SettingAllowed = $OLESettings[$StudentLevel[1]][2];


$linterface->LAYOUT_START();


if ($msg == "3")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("delete")."</td>";
} 
else if ($msg == "1")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("add")."</td>";
} 
else if ($msg == "2")
{
	$xmsg = "<td align='right' >".$linterface->GET_SYS_MSG("update")."</td>";
} 
else 
{
	$xmsg = "";
}


?>

<SCRIPT LANGUAGE="Javascript">
</SCRIPT>

<FORM name="form1" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td>
		<!-- CONTENT HERE -->
<?php if (!$overSubmission && $ck_memberType!="P") {	//hide the new icon if submission period is over ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td class="tabletext"><b><?=$Period_Msg?></b></td>
			</tr>
			</table>
			<br />
<?php } ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabletext">
				<table border="0" cellspacing="0" cellpadding="2">
				<tr>
<?php if (!$overSubmission && $ck_memberType!="P") {	//hide the new icon if submission period is over ?>
					<td>
					<a href="ole_new.php?IntExt=1" class="contenttool">
					<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?> </a>
					</td>
					<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
<?php } ?>
<?php if ($SettingAllowed) { ?>
					<td>
					<a href="ole_record_pool.php?IntExt=1" class="contenttool">
					<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_assign.gif" width="12" height="12" border="0" align="absmiddle"> <?=$ec_iPortfolio['ole_set_pool_record']?> </a>
					</td>
<?php } ?>
				</tr>
				</table>
				</td>
				<td align="right" valign="bottom" class="tabletext">&nbsp;</td>
			</tr>
			</table>		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation">
				<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" align="absmiddle"></span>
				<span class="tabletext"><?=$AllRecords?></span>
				</td>
				<?=$xmsg?>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align="right">		

						
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>				
					<td>
					<table border="0" cellpadding="3" cellspacing="0">
					<tr>					
						<td><?=$YearSelectionHTML?></td>
						<td><?=$ele_selection_html?></td>
						<td><?=$category_selection_html?></td>						
						<td><?=$status_selection?></td>
					</tr>
					</table>						
					</td>
					
					<td align="right" valign="bottom">
			
					<?
					if ($ValidPeriod==1 && $ck_memberType!="P"){
					?>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
					

							<table border="0" cellpadding="0" cellspacing="2">
							<tbody>
							<tr>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'record_id[]','ole_new.php',0)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_edit.gif" name="imgEdit" align="absmiddle" border="0" ><?=$ec_iPortfolio['edit'] ?></a></td>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','ole_remove.php')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete'] ?> </a></td>
							</tr>
							</tbody>
							</table>
							
						</td>
						<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
					
					</tr>
					</tbody>
					</table>
					<?
					}
					?>
					</td>
				</tr>
				</table>
					
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
	          		<?php 
	          			$LibTable->display() ;
	          		?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
					<?php
					if ($LibTable->navigationHTML!="")
					{
					?>
						<tr class='tablebottom'>
						<td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
						</tr>
					<?php
					}
					?>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		</table>			
			
		
		<!-- End of CONTENT -->
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="IntExt" value="1" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
