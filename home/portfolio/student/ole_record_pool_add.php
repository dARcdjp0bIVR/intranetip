<?php
// Modifing by Bill
/*
 * 2014-12-23 (Bill)
 * - Check SLP period settings of forms in DB
 * 
 * 2013-12-13 (Ivan) [B56704]
 * 	Fixed: empty form action when form submit => cannot submit form
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");


intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// Initializing classes
$LibUser = new libuser($UserID);

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("popup.html");
// set the current page title
if($IntExt == 1)
	$CurrentPage = "Student_ExtOLE";
else
	$CurrentPage = "Student_OLE";
$CurrentPageName = $IntExt == 1 ? "<font size='-2'>".$iPort["external_record"]."</font>" : $iPort['menu']['ole'];

$IntExt_Setting = $IntExt;
if ($sys_custom['IPF_OLE_SELECT']['INCLUDE_OUTSIDE_RECORD'])
{
	$IntExt = 3;
	$IntExt_Setting = 0;
	$CurrentPageName = "<font size='-2'>". $iPort['menu']['ole'] . " &". $iPort["external_record"]."</font>";
}


### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

//# load approval setting
//$ApprovalSetting = $LibPortfolio->GET_OLR_APPROVAL_SETTING();

$vars = "ClassName=$ClassName&StudentID=$StudentID";
if ($ck_memberType=="S")
{
	list($ClassName, $StudentID) = $LibPortfolio->GET_STUDENT_ID($ck_user_id);

	# define the navigation
	$template_pages = 	Array(
								Array($ec_iPortfolio['ole'], "")
									);
}
else 
{
	header("Location ../index.php");
}

#####################################
if($displayBy=="")
	$displayBy = "Record";

# define the buttons according to the variable $displayBy
$bcolor_record = ($displayBy=="Record") ? "#CFE6FE" : "#FFD49C";
$bcolor_stat = ($displayBy=="Stat") ? "#CFE6FE" : "#FFD49C";
//$bcolor_analysis = ($displayBy=="Analysis") ? "#CFE6FE" : "#FFD49C";

$gif_record = ($displayBy=="Record") ? "l" : "o";
$gif_stat = ($displayBy=="Stat") ? "l" : "o";
//$gif_analysis = ($displayBy=="Analysis") ? "l" : "o";

$link_record = ($displayBy=="Record") ? $ec_iPortfolio['olr_display_record'] : "<a href='index.php?$vars&displayBy=Record' class='link_a'>".$ec_iPortfolio['olr_display_record']."</a>";
$link_stat = ($displayBy=="Stat") ? $ec_iPortfolio['olr_display_stat'] : "<a href='index_stat.php?$vars&displayBy=Stat' class='link_a'>".$ec_iPortfolio['olr_display_stat']."</a>";
//$link_analysis = ($displayBy=="Analysis") ? $ec_iPortfolio['olr_analysis'] : "<a href='index_analysis.php?$vars&displayBy=Analysis' class='link_a'>".$ec_iPortfolio['olr_display_analysis']."</a>";

///////////////////////////////////////////////////////
///// TABLE SQL
///////////////////////////////////////////////////////
$pageSizeChangeEnabled = true;
$checkmaster = true; 

if ($order=="") $order=0;
if ($field=="") $field=1;
$LibTable = new libdbtable2007($field, $order, $pageNo);
$LibTable->field_array = array("a.Title","OLEDate",  "a.Category", "a.Hours");

$conds = " AND a.RecordStatus IN ('2','4')";
$conds .= " AND a.SLPOrder IS NULL";

$namefield = getNameFieldWithClassNumberByLang("b.");

# get Category field
$CategoryField = $LibPortfolio->GET_OLR_Category_Field_For_Record("c.", true);
$SqlCategoryField = ($CategoryField == "") ? "'-'," : "$CategoryField,";

if($IntExt == 1)
	$conds .= " AND c.IntExt = 'EXT'";
elseif($IntExt == 3)
	$conds .= " AND (c.IntExt = 'INT' OR c.IntExt = 'EXT')";
else
	$conds .= " AND c.IntExt = 'INT'";

$sql = "	SELECT
              	c.Title,
              	IF ((a.StartDate IS NULL OR a.StartDate='0000-00-00'),'--',IF(a.EndDate IS NULL OR a.EndDate='0000-00-00',a.StartDate,CONCAT(a.StartDate,'<br />$profiles_to<br />',a.EndDate))) as OLEDate,
			  	$SqlCategoryField
				a.Hours,
				CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', a.RecordID ,'>'),
				IF ((Month(a.StartDate)>8), Year(a.StartDate), Year(a.StartDate)-1) AS StartYear
			FROM 
				{$eclass_db}.OLE_STUDENT as a
			LEFT JOIN 
				{$intranet_db}.INTRANET_USER as b
			ON 
				a.ApprovedBy = b.UserID
			LEFT JOIN
				{$eclass_db}.OLE_PROGRAM as c
			ON
				a.ProgramID = c.ProgramID
        	WHERE 
				a.UserID = '$StudentID'
				$conds
			";
	
// TABLE INFO
$LibTable->sql = $sql;
$LibTable->db = $intranet_db;
$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($ck_page_size=="") ? $page_size : $ck_page_size;
if ($page_size_change!="") $li->page_size = $numPerPage;
$LibTable->no_col = 6;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center'>#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' width='100' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td>".$LibTable->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(2,$ec_iPortfolio['category'], 1)."</td>\n";
$LibTable->column_list .= "<td  nowrap='nowrap'>".$LibTable->column(3,$ec_iPortfolio['hours'], 1)."</td>\n";
$LibTable->column_list .= ($ck_memberType=="S") ? "<td>".$LibTable->check("record_id[]")."</td>\n" : "";

$LibTable->column_array = array(10,10,0,10,0,10);
	
//////////////////////////////////////////////

// load settings
$StudentLevel = $LibPortfolio->getClassLevel($LibUser->ClassName);
$OLEAssignedArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($UserID, $IntExt);
$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt_Setting);

if(is_array($OLESettings))
{
	if($OLESettings[$StudentLevel[1]][0] != "")
		$RecordsAllowed = $OLESettings[$StudentLevel[1]][0]-count($OLEAssignedArr);
	else
		$RecordsAllowed = "inf";
}

//$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();

# Get SLP period settings data from DB
$objIpfPeriodSetting = new iportfolio_period_settings();
$SLPSettingsArray = $objIpfPeriodSetting->getSettingsArray("SLP", $StudentLevel['YearID']);

# Set start and end date format
$SLPSettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SLPSettingsArray['StartDate'], (array)$SLPSettingsArray['StartHour'], (array)$SLPSettingsArray['StartMinute']);
$SLPSettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SLPSettingsArray['EndDate'], (array)$SLPSettingsArray['EndHour'], (array)$SLPSettingsArray['EndMinute']);
$SetSLPPeriod_startTime = $SLPSettingsArray['StartDateTime'][0];
$SetSLPPeriod_endTime = $SLPSettingsArray['EndDateTime'][0];
$SetSLPPeriod_isAllowed = $SLPSettingsArray['AllowSubmit'];

//$SetSLPPeriod_isAllowed = $studentOleConfigDataArray["SetSLPPeriod_ON"];
//$SetSLPPeriod_isAllowed = ($SetSLPPeriod_isAllowed=='on')? true:$SetSLPPeriod_isAllowed;

$rec_allowed_msg = (is_int($RecordsAllowed)) ? str_replace("<!--NoRec-->", $RecordsAllowed, $ec_iPortfolio['ole_no_rec_add_to_pool']) : "";

$dateNow = date("Y-m-d H:i:s");

if($SetSLPPeriod_startTime!='' && $SetSLPPeriod_endTime!='')
{
	$cond =($SetSLPPeriod_startTime<$dateNow && $SetSLPPeriod_endTime>$dateNow)&& $SetSLPPeriod_isAllowed==true;
}
else if($SetSLPPeriod_startTime!='' && $SetSLPPeriod_endTime=='')
{
	$cond =($SetSLPPeriod_startTime<$dateNow)&& $SetSLPPeriod_isAllowed==true;
}
else if($SubmissionPeriod_startTime=='' && $SetSLPPeriod_endTime!='')
{
	$cond =($SetSLPPeriod_endTime>$dateNow)&& $SetSLPPeriod_isAllowed==true;
}

//$SetSLPPeriod_isAllowed = $studentOleConfigDataArray["SetSLPPeriod_ON"];
//$SetSLPPeriod_isAllowed = ($SetSLPPeriod_isAllowed=='on')? true:$SetSLPPeriod_isAllowed;

$isAllowed = false;


if($cond==true && $SetSLPPeriod_isAllowed==true)
{
	$isAllowed = true;
}
else
{
	$isAllowed = false;
}



$accessDeniedStr = $ec_warning['no_permission'];

$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE="Javascript">

function checkform(jParFormObj)
{
<?php if(is_int($RecordsAllowed)) { ?>
	if(countChecked(jParFormObj, "record_id[]") > <?=$RecordsAllowed?>)
	{
		alert("<?=$ec_warning['over_limit']?>");
	}
	else
	{
		if(countChecked(jParFormObj, "record_id[]") > 0)
		{
			jParFormObj.action = 'ole_record_pool_add_update.php';
			jParFormObj.submit();
		}
		else
		{
			alert("<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>");
		}
	}
<?php } else  { ?>
	jParFormObj.action = 'ole_record_pool_add_update.php';
	jParFormObj.submit();
<? } ?>
}

</SCRIPT>

<FORM name="form1" method="POST">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="8">
	<?=$xmsg?>	
	<tr>
		<td style="text-align:center;">
		<?php 
		if($isAllowed)
		{			
		?>
		
		<!-- CONTENT HERE -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php if($rec_allowed_msg != "") { ?>
		<tr>
			<td><?=$rec_allowed_msg?></td>
		</tr>
<?php } ?>
		<tr>
			<td align="right">		

						
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>				
					<td align="right" valign="bottom">
			
					<?
					if ($ck_memberType!="P"){
					?>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
					

							<table border="0" cellpadding="0" cellspacing="2">
							<tbody>
							<tr>
								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
								<td nowrap="nowrap"><a href="javascript:checkform(document.form1)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_new.gif"  name="imgNew" align="absmiddle" border="0" ><?=$button_add ?> </a></td>
							</tr>
							</tbody>
							</table>
							
						</td>
						<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
					
					</tr>
					</tbody>
					</table>
					<?
					}
					?>
					
					</td>
				</tr>
				</table>
					
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
			<tr>
	          	<td align="center" valign="middle">
	          	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	          	<tr>
	          		<td>
	          		<?php 
	          			$LibTable->displayPlain() ;
	          		?>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
					<?php
					if ($LibTable->navigationHTML!="")
					{
					?>
						<tr class='tablebottom'>
						<td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
						</tr>
					<?php
					}
					?>
					</table>
					</td>
				</tr>
				</table>
				</td>
			</tr>
			</table>			
			</td>
		</tr>
		</table>			
		
		<!-- End of CONTENT -->
		
		<?php
		}
		else
		{
		?>
			<?=$accessDeniedStr?>
		<?php
		}
		?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
