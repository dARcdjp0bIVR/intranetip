<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();


$LibPortfolio = new libportfolio();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$LibFS = new libfilesystem();

//$li_pf->accessControl("ole");

$RecordArr = (is_array($record_id)) ? $record_id : array($record_id);
$RecordArr = IntegerSafe($RecordArr);
$AcademicYearID = IntegerSafe($AcademicYearID);
$category = IntegerSafe($category);
$status = IntegerSafe($status);


$sql = "select UserID from ".$eclass_db.".OLE_STUDENT WHERE RecordID IN ('".implode("','", (array)$RecordArr)."')";
$recordInfoAry = $LibPortfolio->returnResultSet($sql);
$numOfRecord = count($recordInfoAry);
$allBelongsToStudent = true;
for ($i=0; $i<$numOfRecord; $i++) {
	$_userId = $recordInfoAry[$i]['UserID'];
	
	if ($ck_memberType == 'S' && $_userId != $_SESSION['UserID']) {
		$allBelongsToStudent = false;
	}
	else if ($ck_memberType == 'P' && $_userId != $ck_current_children_id) {
		$allBelongsToStudent = false;
	}
}

if ($numOfRecord==0 || !$allBelongsToStudent) {
	No_Access_Right_Pop_Up();
}


# remove the attachment folders
for ($i=0; $i<sizeof($RecordArr); $i++)
{
	$rid = $RecordArr[$i];
	$folder_attachment = $eclass_filepath."/files/portfolio/ole/r".$rid;
	$LibFS->folder_remove_recursive($folder_attachment);
}
$RecordList = implode($RecordArr, ",");

if(trim($RecordList) == "")
{
	//INVALID ACCESS OF THIS PAGE
	//STOP THE PAGE
	exit();
}

$libpf_slp = new libpf_slp();
$cond_RecordStatus_Editable = "";
//$ApprovalSettingData = $libpf_slp->GET_OLR_APPROVAL_SETTING_DATA();
if ($ParIntExt) {
	$INT_EXT_String = "EXT";
} else {
	$INT_EXT_String = "INT";
}
$approvalSettings = $libpf_slp->GET_OLR_APPROVAL_SETTING_DATA2($INT_EXT_String);

/*
if ($ApprovalSettingData["APPROVAL_SETTING"]==$ipf_cfg["RECORD_APPROVAL_SETTING"]["noApprovalNeeded"] && $ApprovalSettingData["EDITABLE_SETTING"]) {
	$cond_RecordStatus_Editable = " OR (RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." AND ComeFrom=".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"].") ";
}

# RecordStatus = 1 (only pending record can be deleted)
$sql = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ({$RecordList}) AND (RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]."$cond_RecordStatus_Editable) ";
*/


//ALLOW DELETE RECORD FOR PENDING APPROVAL RECORD ONLY (BY DEFAULT)
$extraDeleteCondition = " (RecordStatus = '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"]."' or RecordStatus = '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["rejected"]."')";


if (!$approvalSettings['IsApprovalNeed'] && $approvalSettings["Elements"]['Editable']) {
	//Since school setting is "SELT EDITABLE" is true, 
	//Student can delete those record although approved. (so, do not consider $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"])
	//overide $extraDeleteCondition
	$extraDeleteCondition = " ( ComeFrom = '".$ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"]."') ";
}


$sqlDelete = 'select programid from '.$eclass_db.'.OLE_STUDENT WHERE RecordID IN ('.$RecordList.') AND '.$extraDeleteCondition;

$deleteProgramAry = $LibPortfolio->returnVector($sqlDelete);


$sql = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ({$RecordList}) AND ".$extraDeleteCondition;
$LibPortfolio->db_db_query($sql);

//Delete those program is create by the student and the program without associated with other student if have any
$deleteResult = $LibPortfolio->deleteStudentCreatedProgram($deleteProgramAry,$UserID);

intranet_closedb();
if($IntExt==1)
{
	header("Location: ole_record.php?msg=delete&IntExt=1&AcademicYearID=$AcademicYearID&category=$category&status=$status");
}
else
{
	header("Location: ole_record.php?msg=delete&AcademicYearID=$AcademicYearID&category=$category&status=$status&ELE=$ELE");
}

?>