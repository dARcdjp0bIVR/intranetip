<?php

// Modifing by 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

intranet_auth();
intranet_opendb();

// Default: no effect on processing OLE Record
$IntExt = 0;

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$record_id = IntegerSafe($record_id);

$sql = "select UserID from ".$eclass_db.".OLE_STUDENT WHERE RecordID IN ('".implode("','", (array)$record_id)."')";
$recordInfoAry = $LibPortfolio->returnResultSet($sql);
$numOfRecord = count($recordInfoAry);
$allBelongsToStudent = true;
for ($i=0; $i<$numOfRecord; $i++) {
	$_userId = $recordInfoAry[$i]['UserID'];
	
	if ($_userId != $_SESSION['UserID']) {
		$allBelongsToStudent = false;
	}
}

if ($numOfRecord==0 || !$allBelongsToStudent) {
	No_Access_Right_Pop_Up();
}

$OLERecArr = $LibPortfolio->GET_OLE_RECORD_ASSIGNED($UserID, $IntExt, 1);
$OLERecArrNew = array_merge($OLERecArr, $record_id);

$LibPortfolio->SET_OLE_RECORD_ORDER_SLP($UserID, $OLERecArrNew, $IntExt, 1);

intranet_closedb();

?>

<script language="JavaScript">
	window.opener.location.href = "pkms_record_pool.php?IntExt=<?=$IntExt?>&msg=1";
	window.close();
</script>