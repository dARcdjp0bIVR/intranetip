<?php

// using: 

/**************************************************
 * Date:	2013-03-21	(Rita) 
 * Details:	Create this page
 **************************************************/
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-slp.php");


intranet_auth();
intranet_opendb();

$LibPortfolio = new libpf_student();
$libpf = new libportfolio();

$recordId = IntegerSafe(trim($_POST['recordId']));
$reportTemplateId = IntegerSafe(trim($_POST['templateId']));


# Check if same user retrieves this record
$sql = $LibPortfolio->Get_Student_DynamicReport_DBTable_Sql($_SESSION['UserID'], $recordId);
$resultArray = $LibPortfolio->db->returnArray($sql);
$numOfResultArray = count($resultArray);

if($numOfResultArray==0){
	echo 'noRecordFound';
}
else
{

	$filePath = '';
	$fileName = '';
	
	$filePath .= $ipf_cfg['dynReport']['studentReportPath'];
	$filePath .= '/'.$reportTemplateId;

	
	$fileName = $libpf->getEncryptedTextWithNoTimeChecking($_SESSION['UserID'], $ipf_cfg['dynReport']['encryptKey']).'.pdf';	
	$encryptedPath = getEncryptedText($filePath."/".$fileName, $ipf_cfg['dynReport']['encryptKey']);
	
	echo $encryptedPath;
}

intranet_closedb();

?>