<?php
## Using By : anna
##########################################################
## Modification Log
##  2018-05-11 Anna
##  - Added studentAcademicYearID, only show year which student alreay in school#139189
##
## 2014-07-21: Bill
## - Add PKMS SLP Setting Link
##
## 2014-01-14: Ivan [Case#2014-0113-1506-59073]
## - Fixed: If student is not allowed to submit records for report, hide the "set records for SLP report" button now 
##
## 2013-02-21: YatWoon 
## - Add flag checking for mouseover $sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']
##
## 2010-02-01: Max (201002011713)
## - Add a button to the join page instead of with new and join in this page
##########################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

intranet_auth();
intranet_opendb();

$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

$StudentID = ($ck_memberType == "P") ? $ck_current_children_id : $UserID;

$gray = '#C0C0C0';
$_borderTop = 'border-top: 1px solid '.$gray.';';
$_borderLeft = 'border-left: 1px solid '.$gray.';';
$_borderRight = 'border-right: 1px solid '.$gray.';';
$_borderBottom = 'border-bottom: 1px solid '.$gray.';';

$lpf_acc = new libpf_account_student();
$lpf_acc->SET_CLASS_VARIABLE("user_id", $StudentID);
$lpf_acc->SET_STUDENT_PROPERTY();


// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
switch($ck_memberType)
{
  case "S":
    $CurrentPage = "Student_OLE";
    break;
  case "P":
    $CurrentPage = "Parent_OLE";
    break;
}
$CurrentPageName = $iPort['menu']['ole'];
### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['overall_summary'], "");

########################################################
# Operations : Start
########################################################
# Display by status
$StatusArr[] = array("1", $ec_iPortfolio['pending']);
$StatusArr[] = array("2,4", $ec_iPortfolio['approved']);
$StatusArr[] = array("3", $ec_iPortfolio['rejected']);
if(!isset($status)) $status = "2,4";
switch($status)
{
  case "1":
    $DisplayStatus = $ec_iPortfolio['pending'];
    break;
  case "2,4":
    $DisplayStatus = $ec_iPortfolio['approved'];
    break;
  case "3":
    $DisplayStatus = $ec_iPortfolio['rejected'];
    break;
}
$StatusSelection = getSelectByArray($StatusArr, 'name="status" onChange="document.form1.submit()"', $status, 0, 1, "", 2);
$t_status = ($status=="2,4") ? 2 : $status;   # status value used in URL

if($LibPortfolio->isStudentSubmitOLESetting($IntExt, $ck_memberType))
{
	$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();

	/************* Old Version ****************/		  

	if(($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00"))
	  $Period_Msg = $iPort['period_start_end']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
	else if($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"];
	else if($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
	  $Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
	else
	  $Period_Msg = "";
	$display_period = "<tr><td class=\"tabletext\"><b>".$Period_Msg."</b></td></tr>";
	
	/************* END Old Version ****************/
	
	if(($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")) 
	{
	 //	$Period_Msg = $iPort['period_start_end']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
		$SubmissionPeriod_Msg = $studentOleConfigDataArray["STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
	} 
	else if($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
	{
	//	$Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["STARTTIME_TO_MINS"];
		$SubmissionPeriod_Msg = $studentOleConfigDataArray["STARTTIME_TO_MINS"];
	}	 
	else if($studentOleConfigDataArray["ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
	{
	//	$Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray["ENDTIME_TO_MINS"];
		$SubmissionPeriod_Msg = $studentOleConfigDataArray["ENDTIME_TO_MINS"];
	}	  
	else
	{
		$SubmissionPeriod_Msg = "";
	}
	
	if(($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")) 
	{
		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
	} 
	else if($studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
	{
		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"];
	}	 
	else if($studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
	{
		$SetSLPPeriod_Msg = $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];
	}	  
	else
	{
		$SetSLPPeriod_Msg = "";
	}

//  debug_r($tmp);
//	strtotime("2011-01-07")
 
//	$display_period = "<tr><td class=\"tabletext\"><b>".$Period_Msg."</b></td></tr>";
	
	$display_period = '<tr><td><table id="periodTableID" style="border:1px solid '.$gray.';" width="1000px" border="0" cellspacing="0" cellpadding="5">
						  <col width="20%"/>
						  <col width="40%"/>
						  <col width="40%"/>
						  <tr class="tabletop"></tr>
						  <tr class="tabletop">
							<td style="'.$_borderBottom.$_borderRight.'">&nbsp;</td>
							<td style="'.$_borderBottom.$_borderRight.'">'.$Lang['iPortfolio']['OEA']['SubmissionPeriod'].'</td> 
							<td style="'.$_borderBottom.'">'.$iPort["ReportPeriod"].'</td>
						  </tr>
						  <tr>
							<td style="'.$_borderRight.'">'.$iPort["period"].'</td>
							<td style="'.$_borderRight.'">'.$SubmissionPeriod_Msg.'</td>
							<td style="">'.$SetSLPPeriod_Msg.'</td>
						  </tr>
					   </table></td></tr>
					  ';
					  
	$display_period = '';			  

}


$dateNow = date("Y-m-d H:i");
//debug_pr($studentOleConfigDataArray);

$SubmissionPeriod_startTime = $studentOleConfigDataArray["STARTTIME_TO_MINS"];
$SubmissionPeriod_endTime =  $studentOleConfigDataArray["ENDTIME_TO_MINS"];

$SetSLPPeriod_startTime = $studentOleConfigDataArray["SetSLPPeriod_STARTTIME_TO_MINS"];
$SetSLPPeriod_endTime =  $studentOleConfigDataArray["SetSLPPeriod_ENDTIME_TO_MINS"];

$join_btn = ($IntExt!=1 && $ck_memberType!="P") ? "<td><a href=\"ole_join.php?IntExt_p={$IntExt}&AcademicYearID_p={$AcademicYearID}&ELE_p={$ELE}&status_p={$status}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['SLP']['ReportOtherActivities']} </a></td>" : "";

$t_classname = $lpf_acc->GET_CLASS_VARIABLE("class_name");
$StudentLevel = $LibPortfolio->getClassLevel($t_classname);
$OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);
/*
if(is_array($OLESettings))
	$SettingAllowed = (aaa && bbb) ? 1 : 0;
*/

// 2014-0113-1506-59073
if(is_array($OLESettings)) {
	$SettingAllowed = $OLESettings[$StudentLevel[1]][2];
}

$set_print_rec_btn = '';
if ($SettingAllowed) {
	$set_print_rec_btn = "<td><a href=\"ole_record_pool.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_assign.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_set_pool_record']} </a></td>";
}

####  For Pui Kui SLP Customization
if($sys_custom['iPf']['pkms']['Report']['SLP']){
	
	$PKMSAllowed = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		$PKMSAllowed = true;
		
		list($r_formAllowed, $startdate, $enddate,$allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecords) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
//		$allowStudentPrint = unserialize($allowStudentPrintPKMSSLP);
		$formAllowed = unserialize($r_formAllowed);
//		$startdate = trim(unserialize($startdate));
//		$enddate = trim(unserialize($enddate));
		
		// Student allow to print
//		if(!$allowStudentPrint){
//			$PKMSAllowed = false;
//		}
		
		// Student not in allow forms
		if(!in_array($StudentLevel[0], (array)$formAllowed)){
			$PKMSAllowed = false;
		}
		
//		// Today is not within preset date range
//		$todayFormat = date("Y-m-d");
//		if($startdate == null || $enddate == null || $todayFormat < $startdate || $todayFormat > $enddate){
//			$PKMSAllowed = false;
//		}
		
	}
	
	$set_print_pkmsrec_btn = '';
	if ($PKMSAllowed) {
		$set_print_pkmsrec_btn = "<td><a href=\"pkms_record_pool.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_assign.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_pkms_set_pool_record']} </a></td>";
	}
	
}

////////////////////////////////////////////////////////////////////////////////

if($SubmissionPeriod_startTime<$dateNow && $SubmissionPeriod_endTime>$dateNow)
{
}
else
{
	//$join_btn = '';
}

if($SetSLPPeriod_startTime<$dateNow && $SetSLPPeriod_endTime>$dateNow)
{
}
else
{
	//$set_print_rec_btn = '';
}

//debug_r($OLESettings);

########################################################
# Operations : End
########################################################



########################################################
# Table content : Start
########################################################
$ELEArray = $LibPortfolio->GET_ELE();
$ELECount = count($ELEArray);

if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libpf_dbtable($field, $order, $pageNo);
$sql = "SELECT AcademicYearID
        FROM  {$intranet_db}.YEAR_CLASS AS yc
        INNER JOIN  {$intranet_db}.YEAR_CLASS_USER as ycu ON (yc.YearClassID = ycu.YearClassID)
        WHERE  ycu.UserID =  '{$StudentID}'";
$studentAcademicYearIDAry = $LibPortfolio->returnVector($sql);
$studentAcademicYearIDList = implode('\',\'',$studentAcademicYearIDAry);


$sql =  "
          SELECT
            ".Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN").",
        ";
foreach($ELEArray as $ELECode => $ELETitle)
{
	if($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile'])
		$mouse_over = "CONCAT(' ELEHyperLink\" onmouseover=\"showActivityDetails(\'{$ELECode}\',',ay.AcademicYearID,',',IFNULL(os.UserID,0),',$(this))\"')";
	else
		$mouse_over = "' \"'";
	
  $sql .= "CONCAT('<a class=\"tablelink',IF(TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM ROUND(SUM(IF(INSTR(op.ELE, '".$ELECode."') > 0, IF(os.Hours IS NULL, 0, os.Hours), -1)), 2))) = 0, ' ELEORecord\"', ". $mouse_over."),' href=\"ole_record.php?AcademicYearID=', ay.AcademicYearID, '&ELE={$ELECode}&status={$t_status}\">', TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM ROUND(SUM(IF(INSTR(op.ELE, '".$ELECode."') > 0, IF(os.Hours IS NULL, 0, os.Hours), 0)), 2))), '</a>'),";
}
$sql .=  "
            CONCAT('<a href=\"ole_record.php?AcademicYearID=', ay.AcademicYearID, '&status={$t_status}\" class=\"tablelink\">', TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM ROUND(SUM(IF(os.Hours IS NULL, 0, os.Hours)), 2))), '</a>'),
            CONCAT('<a href=\"ole_record.php?AcademicYearID=', ay.AcademicYearID, '&status={$t_status}\" class=\"tablelink\">', count(DISTINCT os.RecordID), '</a>')
          ,os.Hours
          FROM
            {$intranet_db}.ACADEMIC_YEAR AS ay
          INNER JOIN
            {$eclass_db}.OLE_PROGRAM AS op
          ON
            op.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID IN ('{$studentAcademicYearIDList}')
          LEFT JOIN
            {$eclass_db}.OLE_STUDENT AS os
          ON
            os.ProgramID = op.ProgramID AND
            os.RecordStatus IN ({$status}) AND
            os.UserID = '{$StudentID}'
          WHERE
            op.IntExt = 'INT'
            AND op.AcademicYearID IN ('{$studentAcademicYearIDList}')
          GROUP BY
            op.AcademicYearID
        "; 
              
// TABLE INFO
$LibTable->field_array = array("ay.Sequence");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
//$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 3 + $ELECount;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->noNumber = true;

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td rowspan=\"2\">&nbsp;</td>\n";
$LibTable->column_list .= "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']." (".$ec_iPortfolio['hours'].")</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" align='center'>".$ec_iPortfolio["total_hours"]."</td>\n";
$LibTable->column_list .= "<td rowspan=\"2\" align='center'>".$ec_iPortfolio['total_records']."<br />(".$DisplayStatus.")</td>\n";
$LibTable->column_list .= "</tr>\n";

$LibTable->column_list .= "<tr class=\"tabletop\">";
foreach($ELEArray as $ELECode => $ELETitle)
{
	$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
	$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
	$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
}
$LibTable->column_list .= "</tr>";

$LibTable->column_array = array_merge(array(0), array_fill(0, $ELECount, 3), array(3,3));				

$table_content = $LibTable->displayOLEStudentSummaryTable();

########################################################
# Table content : End
########################################################

$linterface->LAYOUT_START();
?>

<?php
//Customization for St.Paul, a mouseover popup box showing the awards detail
if($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']){
?>
<SCRIPT>
var request;
$(document).ready(function(){
	$('.ELEHyperLink').each(function(){
		$(this).closest('td').mouseover($(this).attr('onmouseover'));
		$(this).attr('onmouseover','');
		$(this).closest('td').hover(
			function(e){
				$('#ActivityLayer').html('<table style="border:1px dotted #CCE6FF; background:#690; color:white; z-index:12; font-weight:bold"><tr><td><?=$linterface->Get_Ajax_Loading_Image(1)?></td></tr></table>');
				$('#ActivityLayer').css({ "top": (e.pageY+10)+'px', "left": (e.pageX+10)+'px' });
				$('#ActivityLayer').show();
			},
			function(){
				$('#ActivityLayer').hide();
			}
		)
	});
	$('.ELEORecord').each(function(){
		$(this).closest('td').mouseover($(this).attr('onmouseover'));
		$(this).attr('onmouseover','');
		$(this).closest('td').hover(
			function(e){
				if(request){
					request.abort();
				}
				$('#ActivityLayer').html('<table style="border:1px dotted #CCE6FF; background:#690; color:white; z-index:12; font-weight:bold"><tr><td>-</td></tr></table>');
				$('#ActivityLayer').css({ "top": (e.pageY+10)+'px', "left": (e.pageX+10)+'px' });
				$('#ActivityLayer').show();
			},
			function(){
				$('#ActivityLayer').hide();
			}
		)
	});
});

function showActivityDetails(ELECode, AcademicYearID, userID, obj){
	if(request){
		request.abort();
	}
	request = $.ajax({
		url:'ajax_loader.php?func=showActivityDetails',
		data: { UserID: userID, AcademicYearID: AcademicYearID, ELE:ELECode },
		success: function (data){
			if(data){
				$('#ActivityLayer').html('<table style="border:1px dotted #CCE6FF; background:#690; color:white; opacity:0.9; filter:alpha(opacity=90); z-index:12; font-weight:bold"><tr><td></td><td>Title</td><td>Awards</td></tr>'+data+'</table>');
			}
		}
	})
	
}

</SCRIPT>
<?php
}
?>
<div id='ActivityLayer' style='position:absolute;display:none'></div>
<FORM name="form1" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
  	<td align="center">
    	<table width="96%" border="0" cellspacing="0" cellpadding="8">
      	<tr>
      		<td>
      		<!-- CONTENT HERE -->
      			<table border="0" cellspacing="5" cellpadding="0">
              <?=$display_period?>
        			<tr>
      					<td>
		                  <table border="0" cellspacing="0" cellpadding="0">
		                    <tr>
		                      <?=$join_btn?>
		                      <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
		                      <?=$set_print_rec_btn?>
		                      <?php if($sys_custom['iPf']['pkms']['Report']['SLP']){ ?>
			                      <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
			                      <?=$set_print_pkmsrec_btn?>
		                      <?php } ?>
		              		</tr>
		            	  </table>
            		</td>
            	</tr>
            </table>
      			<br />

        		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<tr>
          			<td>
            			<table width="100%" border="0" cellspacing="0" cellpadding="5">
              			<tr>
              				<td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
              				<td align="right">
              					<?=$StatusSelection?>
              				</td>				
              			</tr>
            			</table>
          			</td>
          		</tr>
          		<tr>
          			<td>		
          			 <?=$table_content ?>
          			</td>
          		</tr>
        		</table>
      		
      		<!-- End of CONTENT -->
      		</td>
      	</tr>
    	</table>
  	</td>
  </tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>