<?
//this file copy from /home/web/eclass40/intranetIP25/home/portfolio/profile/report/report_print.php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");


if ($sys_custom["ywgs_ole_report"])
{
	include_once("../profile/report/ywgs_ole_report.php");
 	//debug($r_academicYearID);
 	die();
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
//include_once("report_lib/common.php");
//include_once("report_lib/full_report.php");
//include_once("report_lib/transcript.php");

include_once($PATH_WRT_ROOT."lang/slp_report_lang_bilingual.php");

include_once($PATH_WRT_ROOT."/home/portfolio/profile/report/report_lib/slp_report.php");
include_once($PATH_WRT_ROOT."/home/portfolio/profile/report/report_lib/slp_report_data.php");
include_once($PATH_WRT_ROOT."/home/portfolio/profile/report/report_lib/slp_report_html.php");
include_once($PATH_WRT_ROOT."/home/portfolio/profile/report/report_lib/slp_report_pdf.php");

intranet_auth();
intranet_opendb();

$PrintType = 'pdf'; // hard code to PDF

$objIpfSetting			= iportfolio_settings::getInstance();
$issuedate	= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIssuesDate"]);
$reportLang				= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpLang"]);
$printIssueDate = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithIssuesDate"]);
$academicScoreDisplayMode = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["academicScoreDisplayMode"]);
$displayFullMark = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayFullMark"]);
$displayComponentSubject = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["displayComponentSubject"]);



### if the client does not set the config, $displayFullMark would be set 1, the report would display full mark
$displayFullMark = ($displayFullMark=='')? 1:$displayFullMark;
//debug_r($displayFullMark);

$reportLangInfo = explode(',',$reportLang);

for($i =0,$i_max = sizeof($reportLangInfo);$i < $i_max; $i++){
	$_tmpLang = $reportLangInfo[$i];
	$slpRequestLang = strtoupper($_tmpLang);
}
if(sizeof($reportLangInfo)>1 && is_array($reportLangInfo))
{
	//element of $reportLangInfo > 1 , that mean user select with two lang --> set the lang to BI	
	$slpRequestLang = "BI";
}
$slpRequestLang = ($slpRequestLang == "")?"BI":$slpRequestLang;


$thisStudentId = $_SESSION['UserID'];
$StudentArr = array($thisStudentId);

$lpf_slp = new libpf_slp();

$academicYearID = in_array("", $r_academicYearID) ? array() : IntegerSafe($r_academicYearID);
    

//for safe if more than one student is printed out , or a student print other one report, abort this action
if(sizeof($StudentArr) != 1 || $thisStudentId != $_SESSION['UserID']){
	exit();
}


# Data retrieval
$lpf_slp_data = new slp_report_data();
$lpf_slp_data->SET_DATA_ARRAY($StudentArr, $academicYearID, '', $displayComponentSubject);
$yearName = $lpf_slp_data->GET_YearName();


# Report printing object selection
switch($PrintType)
{
	case "html":
    case "word":
		$lib_slp_report = new slp_report_html();
	break;
    case "pdf":
		$lib_slp_report = new slp_report_pdf();
	break;
}

# Set data to report printing object
$lib_slp_report->SET_SPArr($lpf_slp_data->GET_SPArr());
$lib_slp_report->SET_SubjArr($lpf_slp_data->GET_SubjArr());
$lib_slp_report->SET_YearArr($lpf_slp_data->GET_YearArr());
$lib_slp_report->SET_APArr($lpf_slp_data->GET_APArr());
$lib_slp_report->SET_AwardArr($lpf_slp_data->GET_AwardArr());
$lib_slp_report->SET_ActivityArr($lpf_slp_data->GET_ActivityArr());
$lib_slp_report->SET_OLEArr($lpf_slp_data->GET_OLEArr());
$lib_slp_report->SET_ExtPerformArr($lpf_slp_data->GET_ExtPerformArr());
$lib_slp_report->SET_SelfAccountArr($lpf_slp_data->GET_SelfAccountArr());
$lib_slp_report->SET_SubjectInfoOrderedArr($lpf_slp_data->GET_SubjectInfoOrderedArr());
$lib_slp_report->SET_SubjectFullMarkArr($lpf_slp_data->GET_SubjectFullMarkArr());

$lib_slp_report->setReportLang($slpRequestLang);


// For performance tunning info
//debug_pr(convert_size(memory_get_usage()));
//debug_pr(convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//$lib_slp_report->db_show_debug_log();
//die();


# Report printing
$report_html = $lib_slp_report->GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($StudentArr, $issuedate, $yearName, $printIssueDate, $IsZip,$academicScoreDisplayMode,$displayFullMark,$displayComponentSubject);




intranet_closedb();


switch($PrintType)
{
  # pdf case should not be entered as pdf has been outputted in previous function 
  case "pdf":
    DIE();
    break;
  case "word":
	//do nothing
	 break;
  case "html":
	  //do nothing
	 break;
}
?>