<?php
// using By : Bill
/*
 * [Modification Log]
 * 
 * *******************************************************
 * 
 * 2013-02-20 (YatWoon)
 * - add hyperlink to the program title
 * - hide the submission period
 * 2012-03-07 (Henry Chow)
 * - check $SettingAllowed with "Max. Quota can be applied" also
 * 
 * 2010-06-30: Max (201006300902)
 * - Added Subcategory Filter
 * 
 * 2010-04-21: Max (201004211152)
 * - Allow student to edit if allow edit is set in RecordApproval
 * - Check submission period by function isStudentSubmitOLESetting
 * **********************************
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-student.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_period_settings.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$status = IntegerSafe($status);
$category = IntegerSafe($category);
$subcategory = IntegerSafe($subcategory);
$IsOutsideSchool = IntegerSafe($IsOutsideSchool);

if ($IntExt) {
	$INT_EXT_String = "EXT";
} else {
	$INT_EXT_String = "INT";
}
$LibPortfolio = new libpf_slp();
$LibPortfolio->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);

// template for student page
$linterface = new interface_html("iportfolio_default.html");
// set the current page title
switch($ck_memberType)
{
  case "S":
    $CurrentPage = ($IntExt == 1) ? "Student_ExtOLE" : "Student_OLE";
    $StudentID = $UserID;
    break;
  case "P":
    $CurrentPage = ($IntExt == 1) ? "Parent_ExtOLE" : "Parent_OLE";
    $StudentID = $ck_current_children_id;
    break;
}
$CurrentPageName = ($IntExt == 1) ? $iPort["external_record"] : $iPort['menu']['ole'];
### Title ###
$TAGS_OBJ[] = array($ip20TopMenu['iPortfolio'],"");
$MODULE_OBJ = $LibPortfolio->GET_MODULE_OBJ_ARR();

$lpf_acc = new libpf_account_student();
$lpf_acc->SET_CLASS_VARIABLE("user_id", $StudentID);
$lpf_acc->SET_STUDENT_PROPERTY();

$MenuArr = array();
if($IntExt != 1) $MenuArr[] = array($ec_iPortfolio['overall_summary'], "ole.php");
$MenuArr[] = array($AllRecords, "");

$msg_row = isset($msg) ? "<tr><td align='right' >".$linterface->GET_SYS_MSG($msg)."</td></tr>" : "";

########################################################
# Operations : Start
########################################################
# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='AcademicYearID' onChange='document.form1.submit()'", $AcademicYearID, 1, 0, $i_Attendance_AllYear, 2);

# status selection
$status_selection_html = "<SELECT name='status' onChange='document.form1.submit()'>";
$status_selection_html .= "<OPTION value='' ".($status==0?"SELECTED":"").">".$ec_iPortfolio['all_status']."</OPTION>";
$status_selection_html .= "<OPTION value='1' ".($status==1?"SELECTED":"").">".$ec_iPortfolio['pending']."</OPTION>";
$status_selection_html .= "<OPTION value='2' ".($status==2?"SELECTED":"").">".$ec_iPortfolio['approved']."</OPTION>";
$status_selection_html .= "<OPTION value='3' ".($status==3?"SELECTED":"").">".$ec_iPortfolio['rejected']."</OPTION>";
//$status_selection .= "<OPTION value='4' ".($status==4?"SELECTED":"").">".$ec_iPortfolio['teacher_submit_record']."</OPTION>";
$status_selection_html .= "</SELECT>";

# component selection
$ele_selection_html = ($IntExt == 1) ? "" : $LibPortfolio->GET_ELE_SELECTION("name='ELE' onChange='document.form1.submit()'", $ELE, 1);

# cateogry selection
$category_selection_html = $LibPortfolio->GET_OLE_CATEGORY_SELECTION("name='category' onChange='document.form1.submit()'", $category, 1);

# subcategory selection
$subcategory_selection_html = $LibPortfolio->GET_SUBCATEGORY_SELECTION("name='subcategory' onChange='document.form1.submit()'", $subcategory, 1, $category);

$t_classname = $lpf_acc->GET_CLASS_VARIABLE("class_name");
$StudentLevel = $LibPortfolio->getClassLevel($t_classname);

# New button / Join button
# check if submission period is over
//if ($IntExt==1) {
//	$EX = "EX_";
//}

# Get period settings data from DB
$objIpfPeriodSetting = new iportfolio_period_settings();
$SettingsArray = $objIpfPeriodSetting->getSettingsArray(($IntExt==1? "EXT" : "OLE"), $StudentLevel['YearID']);

# Set start and end date format
$SettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SettingsArray['StartDate'], (array)$SettingsArray['StartHour'], (array)$SettingsArray['StartMinute']);
$SettingsArray['StartDateTime'] = $SettingsArray['StartDateTime'][0];
$SettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$SettingsArray['EndDate'], (array)$SettingsArray['EndHour'], (array)$SettingsArray['EndMinute']);
$SettingsArray['EndDateTime'] = $SettingsArray['EndDateTime'][0];

if($LibPortfolio->isStudentSubmitOLESetting($IntExt, $ck_memberType, "", $SettingsArray))
{
	$SubmissionPeriod_startTime = $SettingsArray['StartDateTime'];
	$SubmissionPeriod_endTime = $SettingsArray['EndDateTime'];
	
//	$studentOleConfigDataArray = $LibPortfolio->getDataFromStudentOleConfig();
//	if(($studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"]!="0000-00-00 00:00:00") && ($studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"]!="0000-00-00 00:00:00"))
//	  $Period_Msg = $iPort['period_start_end']." ".$studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"]." ".$iPort['to']." ".$studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"];
//	else if($studentOleConfigDataArray["STARTTIME_TO_MINS"]!="" && $studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"];
//	else if($studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"]!="" && $studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"]!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_start']." ".$studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"];
//	else
//	  $Period_Msg = "";

//	if(($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00") && ($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00"))
//	  $Period_Msg = $iPort['period_start_end']." ".$SubmissionPeriod_startTime." ".$iPort['to']." ".$SubmissionPeriod_endTime;
//	else if($SubmissionPeriod_startTime!="" && $SubmissionPeriod_startTime!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_start']." ".$SubmissionPeriod_startTime;
//	else if($SubmissionPeriod_endTime!="" && $SubmissionPeriod_endTime!="0000-00-00 00:00:00")
//	  $Period_Msg = $iPort['period_start']." ".$SubmissionPeriod_endTime;
//	else
	  $Period_Msg = "";

// 	$display_period = "<tr><td class=\"tabletext\"><b>".$Period_Msg."</b></td></tr>";
	if ($IntExt == 1) {
		$new_btn = "<td><a href=\"ole_new.php?action=new&IntExt={$IntExt}&AcademicYearID={$AcademicYearID}&ELE={$ELE}&status={$status}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$button_new} </a></td>
					<td><img src='" . $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/10x10.gif'></td>";
	}
}
if ($IntExt == 1) {
	$join_btn = ($IntExt!=1 && $ck_memberType!="P") ? "<td><a href=\"ole_join.php\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['SLP']['button_Join']} </a></td>" : "";
} else {
	$join_btn = ($IntExt!=1 && $ck_memberType!="P") ? "<td><a href=\"ole_join.php?IntExt_p={$IntExt}&AcademicYearID_p={$AcademicYearID}&ELE_p={$ELE}&status_p={$status}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['SLP']['ReportOtherActivities']} </a></td>" : "";
}

//$ValidPeriod = $LibPortfolio->isValidPeriod($studentOleConfigDataArray[$EX."STARTTIME_TO_MINS"],$studentOleConfigDataArray[$EX."ENDTIME_TO_MINS"]);
if($SubmissionPeriod_startTime != '' && $SubmissionPeriod_startTime != ''){
	$ValidPeriod = $LibPortfolio->isValidPeriod($SubmissionPeriod_startTime, $SubmissionPeriod_endTime);
}

# Check if student has right to set OLE records for SLP report
// 20160920 Omas change to use db setting value
// $OLESettings = $LibPortfolio->GET_OLE_SETTINGS_SLP($IntExt);
// if(is_array($OLESettings))
// 	$SettingAllowed = ($OLESettings[$StudentLevel[1]][2] && $OLESettings[$StudentLevel[1]][0]>0) ? 1 : 0;
// 	$set_print_rec_btn = ($SettingAllowed && $ck_memberType!="P") ? "<td><a href=\"ole_record_pool.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_assign.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_set_pool_record']} </a></td>" : "";

// checking for setting SLP period
$slpSettingsArray = $objIpfPeriodSetting->getSettingsArray( 'SLP', $StudentLevel['YearID']);
$slpSettingsArray['StartDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$slpSettingsArray['StartDate'], (array)$slpSettingsArray['StartHour'], (array)$slpSettingsArray['StartMinute']);
$slpSettingsArray['StartDateTime'] = $slpSettingsArray['StartDateTime'][0];
$slpSettingsArray['EndDateTime'] = $objIpfPeriodSetting->returnDBTimeFormat((array)$slpSettingsArray['EndDate'], (array)$slpSettingsArray['EndHour'], (array)$slpSettingsArray['EndMinute']);
$slpSettingsArray['EndDateTime'] = $slpSettingsArray['EndDateTime'][0];
if(is_array($slpSettingsArray)){
	$allowToSubmit = $slpSettingsArray['AllowSubmit'];
	$validTime = $LibPortfolio->isValidPeriod($slpSettingsArray['StartDateTime'], $slpSettingsArray['EndDateTime']);
	$SettingAllowed = ($allowToSubmit && $validTime);
	$set_print_rec_btn = ($SettingAllowed && $ck_memberType!="P") ? "<td><a href=\"ole_record_pool.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_assign.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_set_pool_record']} </a></td>" : "";
}


####  For Pui Kui SLP Customization
if($sys_custom['iPf']['pkms']['Report']['SLP']){
	$PKMSAllowed = false;
	
	$portfolio__plms_report_config_file = "$eclass_root/files/portfolio_plms_slp_config.txt";
	if(file_exists($portfolio__plms_report_config_file) && get_file_content($portfolio__plms_report_config_file) != null)
	{	
		$PKMSAllowed = true;
		
		list($r_formAllowed, $startdate, $enddate,$allowStudentPrintPKMSSLP,$issuedate,$r_isPrintIssueDate,$noOfPKMSRecords) = explode("\n", trim(get_file_content($portfolio__plms_report_config_file)));
		$formAllowed = unserialize($r_formAllowed);
		
		// Student not in allow forms
		if(!in_array($StudentLevel[0], (array)$formAllowed)){
			$PKMSAllowed = false;
		}
		
	}
	
	$set_print_pkmsrec_btn = '';
	if ($PKMSAllowed) {
		$set_print_pkmsrec_btn = "<td><a href=\"pkms_record_pool.php?IntExt={$IntExt}\" class=\"contenttool\"><img src=\"{$PATH_WRT_ROOT}{$image_path}/{$LAYOUT_SKIN}/icon_assign.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> {$ec_iPortfolio['ole_pkms_set_pool_record']} </a></td>";
	}
}


########################################################
# Operations : End
########################################################

########################################################
# Table content : Start
########################################################
$ELEArray = $LibPortfolio->GET_ELE();
$ELECount = ($IntExt == 1) ? 0 : count($ELEArray);
$rowStyle = ($IntExt == 1) ? "" : "rowspan=\"2\"";

$pageSizeChangeEnabled = true;
$checkmaster = true;

if (isset($sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE']) && $sys_custom['iPf']['HKUGAC']['OLE']['SAS_N_INOUTSIDE'])
{
	$IsSAS_selection_html = "SAS";
	$IsSASArr[] = array("", $Lang['iPortfolio']['OLE']['SAS_ALL']);
	$IsSASArr[] = array("0", $Lang['iPortfolio']['OLE']['SAS_NOT']);
	$IsSASArr[] = array("1", $Lang['iPortfolio']['OLE']['SAS']);
	$IsSAS_selection_html = $linterface->GET_SELECTION_BOX($IsSASArr, "onChange='this.form.submit()' name='IsSAS'", "", $IsSAS);
	
	$InOutsideSchoolArr[] = array("", $Lang['iPortfolio']['OLE']['In_Outside_School']);
	$InOutsideSchoolArr[] = array("0", $Lang['iPortfolio']['OLE']['Inside_School']);
	$InOutsideSchoolArr[] = array("1", $Lang['iPortfolio']['OLE']['Outside_School']);
	$InsideOutSide_selection_html = $linterface->GET_SELECTION_BOX($InOutsideSchoolArr, "onChange='this.form.submit()' name='IsOutsideSchool'", "", $IsOutsideSchool);
}

# Get Record Approval Setting Data
//$ApprovalSettingData = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA();
$approvalSettings = $LibPortfolio->GET_OLR_APPROVAL_SETTING_DATA2($INT_EXT_String);

# Main query
if ($order=="") $order=0;
if ($field=="") $field=1;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

# Filter conditions
$cond = empty($AcademicYearID) ? "" : " AND ay.AcademicYearID = ".$AcademicYearID;
$cond .= empty($status) ? "" : (($status == 2) ? " AND os.RecordStatus IN (2,4)" : " AND os.RecordStatus = ".$status);
$cond .= empty($ELE) ? "" : " AND INSTR(op.ELE, '{$ELE}') > 0";
$cond .= empty($category) ? "" : " AND op.Category = '".$category."'";
$cond .= empty($subcategory) ? "" : " AND op.SubCategoryID = '".$subcategory."'";



$sql =  "
          SELECT
            concat('<a href=\"ole_new.php?record_id=', os.RecordID ,'&IntExt=', if(os.IntExt='EXT','1','0') ,'\">',op.Title,'</a>'),
            IF(DATE_FORMAT(op.EndDate, '%Y-%m-%d') = '0000-00-00', IF(DATE_FORMAT(op.StartDate, '%Y-%m-%d') = '0000-00-00', '--', DATE_FORMAT(op.StartDate, '%Y-%m-%d')), CONCAT(DATE_FORMAT(op.StartDate, '%Y-%m-%d'), ' {$profiles_to} ', DATE_FORMAT(op.EndDate, '%Y-%m-%d'))) AS period,
        ";
if ($IntExt != 1)
{
  foreach($ELEArray as $ELECode => $ELETitle)
	{
    $sql .= "IF(INSTR(op.ELE, '".$ELECode."') > 0, '<img src=\"/images/2020a/icon_tick_green.gif\" />', ''),";
	}
}


if ($IsSAS=="")
{
	# for all records
} elseif ($IsSAS=="1")
{
	$cond .= " AND op.IsSAS='1' ";
} else
{
	$cond .= " AND op.IsSAS='0' ";
}


if ($IsOutsideSchool=="")
{
	# for all records
} elseif ($IsOutsideSchool=="1")
{
	$cond .= " AND op.IsOutsideSchool='1' ";
} else
{
	$cond .= " AND op.IsOutsideSchool='0' ";
}

$sql .= "
            IF(os.Role IS NULL, '--', os.Role) AS Role,
            IF(os.Achievement IS NULL, '--', os.Achievement) AS Achievement,
            ".Get_Lang_Selection("oc.ChiTitle","oc.EngTitle").",
        ";
$sql .= ($IntExt != 1) ? "IF(os.Hours IS NULL, '0', os.Hours) AS Hours," : "";        
$sql .= "
            IF ((os.Attachment IS NOT NULL AND os.Attachment != ''), CONCAT('<a href=\"attach.php?RecordID=', os.RecordID, '\" target=\"_blank\"><img src=\"$image_path/icon/attachment.gif\" border=0></a>'), '&nbsp;'),
            CASE os.RecordStatus
              WHEN 1 THEN '".$ec_iPortfolio['pending']."'
              WHEN 2 THEN CONCAT('".$ec_iPortfolio['approved']."', IF(os.ProcessDate IS NULL OR DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') = '0000-00-00', '', CONCAT('<br />', DATE_FORMAT(os.ProcessDate,'%Y-%m-%d'))))
              WHEN 3 THEN CONCAT('".$ec_iPortfolio['rejected']."', IF(os.ProcessDate IS NULL OR DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') = '0000-00-00', '', CONCAT('<br />', DATE_FORMAT(os.ProcessDate,'%Y-%m-%d'))))
              WHEN 4 THEN CONCAT('".$ec_iPortfolio['teacher_submit_record']."', IF(os.ProcessDate IS NULL OR DATE_FORMAT(os.ProcessDate,'%Y-%m-%d') = '0000-00-00', '', CONCAT('<br />', DATE_FORMAT(os.ProcessDate,'%Y-%m-%d'))))
              ELSE '--'
            END,
        ".($ck_memberType=="P" ? "" : "IF(
											os.RecordStatus = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["pendingForApprove"] . "
											OR (
												" . (!$approvalSettings['IsApprovalNeed']?1:0) . "=1
												AND " .  ($approvalSettings['Elements']['Editable']?1:0) . "=1
												AND os.ComeFrom = " . $ipf_cfg["OLE_STUDENT_COMEFROM"]["studentInput"] . "
												
												)
											, CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"record_id[]\" value=', os.RecordID,'>'), '&nbsp;'),")."
            os.RecordStatus
          FROM
            {$eclass_db}.OLE_STUDENT AS os
          INNER JOIN
            {$eclass_db}.OLE_PROGRAM AS op
          ON
            os.ProgramID = op.ProgramID
          LEFT JOIN
            {$intranet_db}.INTRANET_USER AS iu
          ON
            op.CreatorID = iu.UserID
          LEFT JOIN
            {$intranet_db}.ACADEMIC_YEAR AS ay
          ON
            op.AcademicYearID = ay.AcademicYearID
          LEFT JOIN
            {$eclass_db}.OLE_CATEGORY AS oc
          ON
            op.Category = oc.RecordID
          WHERE
            op.IntExt = '".($IntExt==1?"EXT":"INT")."' AND
            os.UserID = '{$StudentID}'
            $cond
        ";
//debug_r($sql);
// TABLE INFO
$LibTable->field_array = array("op.Title", "op.StartDate", "os.Role", "os.Achievement", "op.Category", "os.Hours", "os.RecordStatus");
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = ($ck_memberType=="P" ? 8 : 9) + ($IntExt=="1" ? 0 : 1) + $ELECount;
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_alt = array("", "");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td height='25' align='center' $rowStyle class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' $rowStyle width='100' >".$LibTable->column(0,$ec_iPortfolio['title'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle class=\"tabletopnolink\">".$LibTable->column(1,$ec_iPortfolio['date']."/".$ec_iPortfolio['period'], 1)."</td>";
$LibTable->column_list .= ($IntExt != 1) ? "<td colspan=\"".$ELECount."\" class=\"tabletopnolink\" align=\"center\" nowrap='nowrap'>".$ec_iPortfolio['ele']."</td>\n" : "";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(2,$ec_iPortfolio['ole_role'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle >".$LibTable->column(3,$ec_iPortfolio['achievement'], 1)."</td>\n";
$LibTable->column_list .= "<td $rowStyle nowrap='nowrap'>".$LibTable->column(4,$ec_iPortfolio['category'], 1)."</td>\n";
$LibTable->column_list .= ($IntExt != 1) ? "<td $rowStyle nowrap='nowrap'>".$LibTable->column(5,$ec_iPortfolio['hours'], 1)."</td>\n" : "";
$LibTable->column_list .= "<td $rowStyle >&nbsp;</td>\n";
$LibTable->column_list .= "<td $rowStyle nowrap='nowrap'>".$LibTable->column(6,$ec_iPortfolio['status']."/<br>".$ec_iPortfolio['process_date'], 1)."</td>\n";
$LibTable->column_list .= ($ck_memberType=="P") ? "" : "<td $rowStyle >".$LibTable->check("record_id[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";

if ($IntExt != 1)
{
	$LibTable->column_list .= "<tr class=\"tabletop\">";
	foreach($ELEArray as $ELECode => $ELETitle)
	{
		$ELEDisplay = str_replace(array("[", "]"), "", $ELECode);
		$ELEDisplay = is_numeric($ELEDisplay) ? $ELETitle : $ELEDisplay;
		$LibTable->column_list .= "<td align='center' class='tabletopnolink'><b><span title='".$ELETitle."'>".$ELEDisplay."</span></b></td>";
	}
	$LibTable->column_list .= "</tr>";
  
  $LibTable->column_array = array_merge(array(0,3), array_fill(0, $ELECount, 3), array(3,3,3,3,3,3,3));				
}
else
{
  $LibTable->column_array = array(0,3,3,3,3,3,3,3,3);
}

$table_content = $LibTable->displayOLEStudentRecordTable();
$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$table_content .= "</table>";				

########################################################
# Table content : End
########################################################

$linterface->LAYOUT_START();
?>
<FORM name="form1" action="ole_record.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
  	<td align="center">
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
      	<?=$msg_row?>	
      	<tr>
      		<td>
      		<!-- CONTENT HERE -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table border="0" cellspacing="0" cellpadding="5">
                    <?=$display_period?>
                    <tr>
                      <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <?=$new_btn?>
                            <?=$join_btn?>
                            <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
                            <?=$set_print_rec_btn?>
                            <?php if($sys_custom['iPf']['pkms']['Report']['SLP']){ ?>
			                      <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
			                      <?=$set_print_pkmsrec_btn?>
		                    <?php } ?>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  	<tr>
                      <td><?=$ay_selection_html?>&nbsp;&nbsp;&nbsp;<?=$ele_selection_html?>&nbsp;&nbsp;&nbsp;<?=$status_selection_html?></td>
                  	</tr>
                  	<tr>
                  		<td><?=$category_selection_html?>&nbsp;&nbsp;&nbsp;<?=$subcategory_selection_html?>&nbsp;&nbsp;<?=$IsSAS_selection_html?>&nbsp;&nbsp;<?=$InsideOutSide_selection_html?></td>
                  	</tr>
                    <tr>
                      <td align="right" valign="bottom">
<?php if ($ValidPeriod==1 && $ck_memberType!="P") { ?>
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
                              <td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
                                <table border="0" cellpadding="0" cellspacing="2">
                                  <tbody>
                                    <tr>
                                      <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                                      <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'record_id[]','ole_new.php',0)" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_edit.gif" name="imgEdit" align="absmiddle" border="0" ><?=$ec_iPortfolio['edit'] ?></a></td>
                                      <td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                                      <td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'record_id[]','ole_remove.php?ParIntExt=<?=$IntExt?>')" class="tabletool"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete'] ?> </a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                              <td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
                            </tr>
                          </tbody>
                        </table>
<?php } ?>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_b">
                    <tr>
                      <td align="center" valign="middle">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <?=$table_content?>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>			
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="pageNo" value="<?= $pageNo ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="numPerPage" value="<?= $numPerPage ?>">
<input type="hidden" name="page_size_change"  value="<?=$page_size_change?>">

<input type="hidden" name="IntExt" value="<?=$IntExt?>" />

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>