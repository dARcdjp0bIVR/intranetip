<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

$requestLang = trim($requestLang);

$lay = new academic_year($ay_id);
$academic_yearterm_arr = $lay->Get_Term_List(false,$requestLang);

if(!empty($startdate))
{
  list($yt_id, $yt_name) = getAcademicYearAndYearTermByDate($startdate);
}

$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 0, "", 2);
echo $ayterm_selection_html;

?>