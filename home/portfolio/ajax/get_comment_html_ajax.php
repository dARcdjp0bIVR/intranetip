<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio-ericyip.php");
//include_once($PATH_WRT_ROOT."includes/libportfolio2007a-ericyip.php");

intranet_opendb();

$li_pf = new libpf_slp();

$CommentList = $li_pf->GET_STUDENT_COMMENT_LIST($_SESSION['UserID']);

?>

<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" id="RecordsTable">
	<tr class="tabletop">
		<td align="center" valign="top" class="tabletopnolink"><?=$ec_iPortfolio['year']?></td>
		<td valign="top" class="tabletopnolink"><?=$ec_iPortfolio['semester']?></td>
		<td align="left" valign="top" class="tabletopnolink"><?=$ec_iPortfolio['conduct_grade']?></td>
		<td align="left" valign="top" class="tabletopnolink"><?=$ec_iPortfolio['title_teacher_comments']?></td>
	</tr>
<?php
	for($i=0; $i<count($CommentList); $i++)
	{
		$RowStyle = ($i%2==0)?"tablerow1":"tablerow2";
		echo	"
						<tr class=\"".$RowStyle."\">\n
							<td align=\"center\" valign=\"top\" class=\"tabletext\">".$CommentList[$i]['Year']."</td>\n
							<td valign=\"top\" class=\"tabletext\">".$CommentList[$i]['Semester']."</td>
							<td align=\"left\" valign=\"top\" class=\"tabletext\">".$CommentList[$i]['ConductGradeChar']."</td>
							<td align=\"left\" valign=\"top\"><a href=\"javascript:jINSERT_HTML('".htmlspecialchars($CommentList[$i]['Comment'],ENT_QUOTES)."')\" class=\"tablelink\">".$CommentList[$i]['Comment']." <img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_update.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"></a></td>
						</tr>
					";
	}
	
	if(count($CommentList) == 0)
		echo	"
						<tr class=\"tablerow1\">\n
							<td align=\"center\" valign=\"top\" class=\"tabletext\" colspan=\"5\">".$i_no_record_exists_msg."</td>\n
						</tr>\n
					";
?>
</table>

<?php
intranet_closedb();
?>
