<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$rec_cnt = 0;
if(count($ClassNames) > 0)
{
  $table = ($report_type == "0") ? "{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD" : "{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD";

  $conds = ($YearTermID == "") ? "AND IFNULL(YearTermID, 0) = 0 " : "AND YearTermID = {$YearTermID} ";
  $conds .= "AND ClassName IN ('".implode("','", $ClassNames)."') ";
  
  $sql =  "SELECT count(*) ";
  $sql .= "FROM {$table} asr ";
  $sql .= "INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps ON asr.UserID = ps.UserID ";
  $sql .= "WHERE AcademicYearID = {$academicYearID} ";
  $sql .= $conds;
  $rec_cnt = current($li->returnVector($sql));
}

echo $rec_cnt;

intranet_closedb();
?>