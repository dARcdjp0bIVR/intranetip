<?php
/*
 * Modification Log:
 * 2017-03-22 Omas
 *  - modified sql order by ClassTitleEN instead of Sequence
 * 2016-04-11 Omas
 *  - added support to client using SDAS without iPo
 * 2012-02-20 Ivan [2012-0215-1452-19071]
 * 	- added parameter $returnClassWithStudentOnly
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$returnClassWithStudentOnly = (!isset($_REQUEST['returnClassWithStudentOnly']))? 1 : $_REQUEST['returnClassWithStudentOnly'];

$table = ($report_type == "0") ? "{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD" : "{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD";

$li = new libdb();

// if ($returnClassWithStudentOnly) {
// 	$sql =  "
//           SELECT DISTINCT
//             assr.ClassName
//           FROM
//             {$table} assr
//           INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
//             ON assr.UserID = ps.UserID
//           INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
//             ON ycu.UserID = ps.UserID
//           INNER JOIN {$intranet_db}.YEAR_CLASS yc
//             ON  ycu.YearClassID = yc.YearClassID AND
//                 (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
//           WHERE
//             assr.AcademicYearID = {$ay_id}
//             {$conds}
//           ORDER BY
//             yc.Sequence
//         ";
// }
// else {
// 	$sql =  "
//           SELECT DISTINCT
//             assr.ClassName
//           FROM
//             {$table} assr
//           INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT ps
//             ON assr.UserID = ps.UserID
//           INNER JOIN {$intranet_db}.YEAR_CLASS yc
//             ON  (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
// 				and assr.AcademicYearID = yc.AcademicYearID
//           WHERE
//             assr.AcademicYearID = {$ay_id}
//             {$conds}
//           ORDER BY
//             yc.Sequence
//         ";
// }

// $class_arr = $li->returnVector($sql);

// fix for SDAS without iPo school
// if( count($class_arr) == 0 && $plugin['StudentDataAnalysisSystem']){
	if ($returnClassWithStudentOnly) {
		$sql =  "
		SELECT DISTINCT
			assr.ClassName
		FROM
			{$table} assr
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
			ON ycu.UserID = assr.UserID
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON  ycu.YearClassID = yc.YearClassID AND
				(assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
		WHERE
			assr.AcademicYearID = {$ay_id}
			{$conds}
		ORDER BY
			yc.ClassTitleEN
		";
	}
	else {
		$sql =  "
		SELECT DISTINCT
			assr.ClassName
		FROM
			{$table} assr
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON  (assr.ClassName = yc.ClassTitleEN OR assr.ClassName = yc.ClassTitleB5)
				and assr.AcademicYearID = yc.AcademicYearID
		WHERE
			assr.AcademicYearID = {$ay_id}
			{$conds}
		ORDER BY
			yc.ClassTitleEN
		";
	}
// }
	$class_arr = $li->returnVector($sql);

$class_selection_html = "<table width=\"360\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
$class_selection_html .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"YCCheckMaster\" /> <label for=\"YCCheckMaster\">{$button_check_all}</label></td></tr>";
for($i=0; $i<ceil(count($class_arr)/3)*3; $i++)
{
  $_class = $class_arr[$i];

  if($i % 3 == 0) {
    $class_selection_html .= "<tr>";
  }
  
  if(!empty($_class)){
    $class_selection_html .= "<td width=\"120\"><input type=\"checkbox\" id=\"class_{$i}\" name=\"ClassNames[]\" value=\"{$_class}\" /> <label for=\"class_{$i}\">{$_class}</label></td>";
  }
  else {
    $class_selection_html .= "<td width=\"120\">&nbsp;</td>";
  }
  
  if($i % 3 == 2) {
    $class_selection_html .= "</tr>";
  }
}
$class_selection_html .= "</table>";

echo $class_selection_html;

intranet_closedb();
?>