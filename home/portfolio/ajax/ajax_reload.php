<?php
// using :
/**
 * Change Log:
 * 2020-07-22 Philips [#L188901]
 * -	Modified Student_Selection, add flag $sys_custom['SDAS']['StudentPerformanceTracking']['ShowActiveOnly']
 * 2018-07-30 Bill
 * -    Modified Student_Selection, to support multiple year class
 * 2016-01-26 Pun
 * 	-	Modified Subject_Selection, added $OnFocus, $FilterSubjectWithoutSG, $IsMultiple, $IncludeSubjectIDArr, $ExtrudeSubjectIDArr
 * 	-	Added 'Subject_Teacher_Selection'
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == "Subject_Selection")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$SelectionID = cleanHtmlJavascript($_REQUEST['SelectionID']);
	$OnChange = cleanHtmlJavascript($_REQUEST['OnChange']);
	$NoFirst = $_REQUEST['NoFirst'];
	$FirstTitle = trim(urldecode(stripslashes($_REQUEST['FirstTitle'])));
	$OnFocus = $_REQUEST['OnFocus'];
	$FilterSubjectWithoutSG = $_REQUEST['FilterSubjectWithoutSG'];
	$IsMultiple = $_REQUEST['IsMultiple'];
	$ExtrudeSubjectIDArr = $_REQUEST['ExtrudeSubjectIDArr'];
	
	$IncludeSubjectIDArr = array_filter((array)$_REQUEST['IncludeSubjectIDArr']);
	$IncludeSubjectIDArr = (empty($IncludeSubjectIDArr))? '' : $IncludeSubjectIDArr;
	
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
	
	$scm_ui = new subject_class_mapping_ui();
	echo $scm_ui->Get_Subject_Selection($SelectionID, $SubjectID, $OnChange, $NoFirst, $FirstTitle, $YearTermID, $OnFocus, $FilterSubjectWithoutSG, $IsMultiple, $IncludeSubjectIDArr, $ExtrudeSubjectIDArr);
}
else if ($Action == "Class_Selection")
{
	include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
	include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
	$fcm_ui = new form_class_manage_ui();
	
	$AcademicYearID = $_POST['AcademicYearID'];
	$YearID = $_POST['YearID'];
	$SelectedYearClassID = $_POST['SelectedYearClassID'];
	$SelectionID = cleanHtmlJavascript($_POST['SelectionID']);
	$OnChange = cleanHtmlJavascript($_POST['OnChange']);
	$IsMultiple = stripslashes($_POST['IsMultiple']);
	$NoFirst = stripslashes($_POST['NoFirst']);
	$IsAll = $_POST['IsAll'];
	$TeachingOnly = $_POST['TeachingOnly'];
	echo $fcm_ui->Get_Class_Selection($AcademicYearID, $YearID, $SelectionID, $SelectedYearClassID, $OnChange, $NoFirst, $IsMultiple, $IsAll, $TeachingOnly);
}
else if ($Action == "Student_Selection")
{
	include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
	include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
	$fcm_ui = new form_class_manage_ui();
	
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$SelectedYearClassID = $_REQUEST['SelectedYearClassID'];
	$SelectedYearClassID = is_array($SelectedYearClassID)? $SelectedYearClassID : array($SelectedYearClassID);
	$SelectedStudentID = $_REQUEST['SelectedStudentID'];
	$SelectionID =cleanHtmlJavascript($_REQUEST['SelectionID']);
	$OnChange = cleanHtmlJavascript($_REQUEST['OnChange']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsAll = $_REQUEST['IsAll'];
	
	if($sys_custom['SDAS']['StudentPerformanceTracking']['ShowActiveOnly']){
		echo $fcm_ui->Get_Student_Selection($SelectionID, $SelectedYearClassID, $SelectedStudentID, $OnChange, $NoFirst, $IsMultiple, $IsAll, '', '', $activeOnly = true);
	} else {
		echo $fcm_ui->Get_Student_Selection($SelectionID, $SelectedYearClassID, $SelectedStudentID, $OnChange, $NoFirst, $IsMultiple, $IsAll);
	}
}
else if ($Action == "Term_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libSCM_ui = new subject_class_mapping_ui();
	
	$SelectionID = cleanHtmlJavascript($_REQUEST['SelectionID']);
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$OnChange = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['OnChange']))));
	$NoFirst = $_REQUEST['NoFirst'];
	$DisplayAll = $_REQUEST['DisplayAll'];
	$FirstTitle = trim(urldecode(stripslashes($_REQUEST['FirstTitle'])));
	
	echo $libSCM_ui->Get_Term_Selection($SelectionID, $AcademicYearID, $YearTermID, $OnChange, $NoFirst, $NoPastTerm=0, $DisplayAll, $FirstTitle);
}
else if ($Action == "Term_Assessment_Selection")
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	//$libSCM_ui = new subject_class_mapping_ui();
	include_once($PATH_WRT_ROOT."includes/libportfolio.php");
	$libportfolio = new libportfolio();
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html();
	
	$SelectionID = cleanHtmlJavascript($_REQUEST['SelectionID']);
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$OnChange = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['OnChange']))));
	$NoFirst = $_REQUEST['NoFirst'];
	$DisplayAll = $_REQUEST['DisplayAll'];
	$FirstTitle = trim(urldecode(stripslashes($_REQUEST['FirstTitle'])));
	$OnlyHasRecord = $_REQUEST['OnlyHasRecord'];
	$showOverAll = $_REQUEST['showOverAll'];
	
	$TermAndAssessments = $libportfolio->Get_Term_Assessment_Selection($AcademicYearID, $OnlyHasRecord, $showOverAll);
	
	echo $linterface->GET_SELECTION_BOX($TermAndAssessments, "name='{$SelectionID}' id='{$SelectionID}'", "", $ParSelected="");
}
else if ($Action == "Subject_Teacher_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html();
	
	$TeacherID = $_REQUEST['TeacherID'];
	$SelectionID = cleanHtmlJavascript($_REQUEST['SelectionID']);
	$SubjectIdArr = (array)$_REQUEST['SubjectIdArr'];
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$AddTeacherID = $_REQUEST['AddTeacherID'];

	$objSubject = new subject();
	$scm = new subject_class_mapping();
	$objYear = new academic_year($AcademicYearID);


	$teacherList = array();
	
	#### Add teacher START ####
	$AddTeacherIDList = implode("','", (array)$AddTeacherID);
	$NameField = getNameFieldByLang();
	$sql = "SELECT 
		UserID,
		{$NameField} AS NAME
	FROM
		INTRANET_USER 
	WHERE
		UserID IN ('{$AddTeacherIDList}')";
	$rs = $scm->returnResultSet($sql);
	
	foreach ($rs as $r){
		$teacherList[ $r['UserID'] ] = array($r['UserID'], $r['NAME']);
	}
	#### Add teacher END ####
	
	#### Get year term START ####
	$yearTerm = $objYear->Get_Term_List();
	$yearTermIdArr = array_keys((array)$yearTerm);
	#### Get year term END ####
	
	#### Get teacher by year term and subject START ####
	foreach($SubjectIdArr as $SubjectID){
		$rs = $scm->Get_Subject_Group_List($yearTermIdArr, $SubjectID);
		$SubjectGroupIdArr = array_keys((array)$rs['SubjectGroupList']);
		
		$rs = $scm->Get_Subject_Group_Teacher_ListArr((array)$SubjectGroupIdArr);
		foreach ($rs as $d1) {
			foreach ($d1 as $d2) {
				$teacherList[ $d2['UserID'] ] = array($d2['UserID'], $d2['TeacherName']);
			}
		}
	}
	$teacherList = array_values($teacherList);
	#### Get teacher by year term and subject END ####
	
	echo $linterface->GET_SELECTION_BOX($teacherList, "name='{$SelectionID}' id='{$SelectionID}'", "", $ParSelected=$TeacherID);
}

intranet_closedb();
?>