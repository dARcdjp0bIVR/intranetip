<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$without_background = $_REQUEST['without_background'];
if ($without_background) {
	$table_bg = '';
	$td_border = ' style="border:0px;" ';
}
else {
	$table_bg = 'background: #EEEEEE';
	$td_border = '';
}

$lay = new academic_year($ay_id);
$ayt_arr = $lay->Get_Term_List(false);

$ayterm_selection_html = "<table id=\"YearTermSelectTable\" width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"$table_bg\">";
$ayterm_selection_html .= "<tr><td $td_border colspan=\"3\"><input type=\"checkbox\" id=\"YTCheckMaster\" onClick=\"jCHECK_YEARTERM(this)\" /> <label for=\"YTCheckMaster\">".$Lang['iPortfolio']['wholeYearIncludeNullSem']."</label></td></tr>";
for($i=0; $i<ceil(count($ayt_arr)/3)*3; $i++)
{
  if($i % 3 == 0) {
    $ayterm_selection_html .= "<tr>";
  }
  
  if(isset($ayt_arr[$i])){
    $ayterm_selection_html .= "<td $td_border width=\"150\"><input type=\"checkbox\" name=\"YearTermIDs[]\" id=\"yt_".$ayt_arr[$i][0]."\" value=\"".$ayt_arr[$i][0]."\" /> <label for=\"yt_".$ayt_arr[$i][0]."\">".$ayt_arr[$i][1]."</label></td>";
  }
  else {
    $ayterm_selection_html .= "<td $td_border width=\"150\">&nbsp;</td>";
  }
  
  if($i % 3 == 2) {
    $ayterm_selection_html .= "</tr>";
  }
}
$ayterm_selection_html .= "</table>";

echo $ayterm_selection_html;

?>