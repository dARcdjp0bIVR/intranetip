<?php
// Modified by anna

/********************** Change Log ***********************/
#       Date:   2018-09-07 [Anna]
#               revise sql when insert into tempUpdatedSRRecord
#
#		Date:	2017-11-30 [Carlos] $sys_custom['LivingHomeopathy'] - changed search input default display [Enter student name or Login ID].
#
#		Date:	2017-11-07 [Bill]	[2017-0403-1552-04240]
#				HOY Group Member - View all forms	($sys_custom['iPf']['HKUGA_eDis_Tab'])
#
#		Date:   2016-04-15 [Omas]
#			 	fix subject teacher fail get any class problem -#U94994
#
#		Date:	2014-11-18 Pun
#				Remove alumni list select	
#
#		Date:	2012-08-03	Bill Mak
#				Disable import button	
#
#       Date:   2010-08-24      [fai]
#				Remove upload offical photo function in iPortfolio
#
/********************** Change Log ***********************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();

########################################################
# Temp table for updated school record: Start
########################################################

$sql =  "
          SELECT
            MAX(inputdate)
          FROM
            {$eclass_db}.course_count
          WHERE
            DATE_FORMAT(inputdate, '%Y-%m-%d') < DATE_FORMAT(NOW(), '%Y-%m-%d') AND
            course_id = {$ck_course_id} AND
            user_id = {$ck_user_id}
        ";
$last_login = current($lpf->returnVector($sql));

$SRModuleArr = array(
											"MERIT_STUDENT",
											"ASSESSMENT_STUDENT_SUBJECT_RECORD",
											"ASSESSMENT_STUDENT_MAIN_RECORD",
											"ACTIVITY_STUDENT",
											"AWARD_STUDENT",
											"CONDUCT_STUDENT",
											"ATTENDANCE_STUDENT",
											"SERVICE_STUDENT",
											"PORTFOLIO_STUDENT",
											"GUARDIAN_STUDENT"
										);

$sql =  "
          CREATE TEMPORARY TABLE tempUpdatedSRRecord(
            YearClassID int(11),
            RecSum int(11),
            PRIMARY KEY (YearClassID)
          )
        ";
$lpf->db_db_query($sql);

$sql = "Select YearClassID From {$intranet_db}.YEAR_CLASS as yc Where yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
$yearClassIdAry = $lpf->returnVector($sql);

$sql = "Select UserID From INTRANET_USER Where RecordStatus = 1 AND RecordType = 2 ";
$studentIdAry = $lpf->returnVector($sql);


$sql = "
            SELECT
                YearClassID,UserID
            FROM
                YEAR_CLASS_USER
            WHERE
                YearClassID In ('".implode("','", (array)$yearClassIdAry)."')
            AND
                UserID In ('".implode("','", (array)$studentIdAry)."')
              
            ";
$YearClassStudentAry= $lpf->returnArray($sql);

$YearClassStudentAssocAry= BuildMultiKeyAssoc($YearClassStudentAry,array('YearClassID','UserID'));

$SelectedYearClassID = array();
$YearClassUserIDAry= array();
foreach($YearClassStudentAssocAry as $YearClassID => $YearClassInfoAry){
    $SelectedYearClassID[] = $YearClassID;   
    foreach($YearClassInfoAry as $StudentID => $StudentInfo){
        $YearClassUserIDAry[$YearClassID][] = $StudentID;
   }

}

for($i=0; $i<count($SRModuleArr); $i++)
{
//	$sql =	"
//            INSERT INTO
//              tempUpdatedSRRecord
//                (YearClassID, RecSum)
//						SELECT
//							yc.YearClassID,
//							count(a.UserID)
//						FROM
//							{$intranet_db}.INTRANET_USER as iu
//						INNER JOIN
//							{$eclass_db}.".$SRModuleArr[$i]." as a
//						ON
//							a.UserID = iu.UserID AND
//							iu.RecordStatus = 1 AND
//							iu.RecordType = 2
//						INNER JOIN
//						  {$intranet_db}.YEAR_CLASS_USER as ycu
//						ON
//						  iu.UserID = ycu.UserID
//						INNER JOIN
//						  {$intranet_db}.YEAR_CLASS as yc
//						ON
//						  ycu.YearClassID = yc.YearClassID AND
//						  yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
//						WHERE
//							a.ModifiedDate > '".$last_login."'
//						GROUP BY
//							yc.YearClassID
//						ON DUPLICATE KEY UPDATE
//						  RecSum = RecSum + VALUES(RecSum)
//					";

  
    
//     $sql =	"
//                 INSERT INTO
//                 tempUpdatedSRRecord
//                     (YearClassID, RecSum)
//                         SELECT
//                             ycu.YearClassID,
//                             count(a.UserID)
//                         FROM
//                             {$intranet_db}.INTRANET_USER as iu
//                         INNER JOIN
//                             {$eclass_db}.".$SRModuleArr[$i]." as a
//                         ON
//                             a.UserID = iu.UserID
//                         INNER JOIN
//                             {$intranet_db}.YEAR_CLASS_USER as ycu
//                         ON
//                             iu.UserID = ycu.UserID and
//                             iu.RecordStatus = 1 AND
//                             iu.RecordType = 2 
//                         WHERE                        
//                             a.ModifiedDate > '".$last_login."'
//                         AND ycu.YearClassID In ('".implode("','", (array)$yearClassIdAry)."')                               
// 						GROUP BY
// 							ycu.YearClassID
// 						ON DUPLICATE KEY UPDATE
// 						  RecSum = RecSum + VALUES(RecSum)
// 					";

  
    foreach ($YearClassUserIDAry as $YearClassID => $StudentIDAry){
    	$sql =	" INSERT INTO
                  tempUpdatedSRRecord
                    (YearClassID, RecSum)
    						SELECT
    							".$YearClassID.",
    							count(a.UserID)
    						FROM
    							{$eclass_db}.".$SRModuleArr[$i]." as a
    						
    						WHERE						  
                                a.ModifiedDate > '".$last_login."'  
                                AND a.UserID IN ('".implode("','", (array)$StudentIDAry)."')                                                 					
    						ON DUPLICATE KEY UPDATE
    						  RecSum = RecSum + VALUES(RecSum)
    					";
    }

					  
	$lpf->db_db_query($sql);
}

########################################################
# Temp table for updated school record: End
########################################################

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$cond = "";
if(!strstr($ck_user_rights, ":manage:"))
{
  # Class teacher view class
  if(strstr($ck_user_rights_ext, "student_info:form_t"))
  {
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
    $class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
    //$classlevel_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASSLEVEL();
    $classlevel_teach_arr = array();
    $cond .= " AND yc.YearClassID IN ('".implode("','", $class_teach_arr)."')";
  }
  # Subject teacher view class
  else if(strstr($ck_user_rights_ext, "student_info:form_subject_t"))
  {
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
    //$class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
    $class_teach_arr = array_merge($lpf_acc->GET_CLASS_TEACHER_CLASS(), $lpf_acc->GET_SUBJECT_TEACHER_CLASS());
    //$classlevel_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASSLEVEL();
    $classlevel_teach_arr = array();
    $cond .= " AND yc.YearClassID IN ('".implode("','", $class_teach_arr)."')";
  }
}

// [2017-0403-1552-04240] HOY Group Member - View all forms
if($sys_custom['iPf']['HKUGA_eDis_Tab'] && $lpf->IS_HOY_GROUP_MEMBER_IPO()) {
	$class_teach_arr = "";
	$classlevel_teach_arr = "";
	$cond = "";
}

$cond .= ($year_id == "") ? "" : " AND yc.YearID = '".$year_id."'";
# count(uc.user_config_id) : total number of lp published
# count(ptl.portfolio_tracking_id) : number of lp read by user
# (count(uc.user_config_id) - count(ptl.portfolio_tracking_id)) : number of lp unread by user
$sql =  "
          SELECT
            CONCAT('<a href=\"school_records_class.php?YearClassID=', yc.YearClassID,'\" class=\"tablelink\">', ".libportfolio::GetYearClassTitleFieldByLang("yc.").", '</a>'),
            IF(t_SRRec.RecSum IS NULL, 0, t_SRRec.RecSum) AS NewSRCount,
            (count(uc.user_config_id) - count(ptl.portfolio_tracking_id)) AS NewLPCount,
            count(DISTINCT ps.UserID) AS ActivatedCount,
            count(DISTINCT ycu.YearClassUserID) AS StudentCount
          FROM
            {$intranet_db}.YEAR_CLASS AS yc
          LEFT JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON  yc.YearClassID = ycu.YearClassID
          LEFT JOIN {$intranet_db}.INTRANET_USER AS iu
            ON  ycu.UserID = iu.UserID
          LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
            ON  iu.UserID = ps.UserID
          LEFT JOIN ".$lpf->course_db.".user_config AS uc
            ON  ps.CourseUserID = uc.user_id AND
                uc.notes_published IS NOT NULL
          LEFT JOIN ".$lpf->course_db.".portfolio_tracking_last AS ptl
            ON  uc.user_id = ptl.student_id AND
                uc.web_portfolio_id = ptl.web_portfolio_id AND
                ptl.teacher_id = {$ck_user_id}
          LEFT JOIN tempUpdatedSRRecord AS t_SRRec
            ON  yc.YearClassID = t_SRRec.YearClassID
          WHERE
            yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND
			yc.YearID != 0 AND
            iu.RecordType = 2 AND
            iu.RecordStatus = 1
            $cond
          GROUP BY
            yc.YearClassID
        ";

$LibTable->sql = $sql;
$LibTable->field_array = array("ClassTitleEN", "NewLPCount", "ActivatedCount", "StudentCount");
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 6;
	
//$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
//$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
$LibTable->row_alt = array("", "");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 3, 3, 3, 3);

// TABLE COLUMN
$LibTable->column_list .= "<tr class='tabletop'>\n";

$LibTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$LibTable->column_list .= "<td>".$LibTable->column(0,$ec_iPortfolio['class'])."</td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(1,$ec_iPortfolio['heading']['school_record_updated'])."</td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(2,$ec_iPortfolio['heading']['lp_updated'])."</td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(3,$ec_iPortfolio['heading']['no_lp_active_stu'])."</td>\n";
$LibTable->column_list .= "<td align='center'>".$LibTable->column(4,$ec_iPortfolio['heading']['no_stu'])."</td>\n";
$LibTable->column_list .= "</tr>\n";

$lpf_fc = new libpf_formclass();
$t_form_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
if(is_array($classlevel_teach_arr))
{
  for($i=0; $i<count($t_form_arr); $i++)
  {
    $t_form_id = $t_form_arr[$i][0];
    if(in_array($t_form_id, $classlevel_teach_arr))
    {
      $form_arr[] = $t_form_arr[$i];
    }
  }
}
else
{
  $form_arr = $t_form_arr;
}
$year_selection_html = getSelectByArray($form_arr, "name='year_id' onChange='jCHANGE_FIELD()'", $year_id, 1, 0, "", 2);

$search_field_default_text = $sys_custom['LivingHomeopathy']? $ec_iPortfolio['enter_student_name_or_userlogin'] : $ec_iPortfolio['enter_student_name'];

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];
### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();

?>

<script language="JavaScript">

var SearchTextFocus = false;

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(){
	document.form1.action = "school_records.php";
	document.form1.submit();
}

// Change to student activation page
function jSTUDENT_ACTIVATE(){
	document.form1.action = "school_records_class.php";
	document.form1.submit();
}

$(document).ready(function(){
  $("input[name=search_name]").keypress(function(event){
    if(event.keyCode == 13)
    {
    	document.form1.action = "school_records_class.php";
    	document.form1.submit();
    }
  });
});

</script>

<form name="form1" method="POST">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" bgcolor="#FFFFFF">
			<table width="98%" border="0" cellspacing="3" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>

<?php 
			if(strstr($ck_function_rights, "Profile:Student")&&$_SESSION['platform']!='KIS') { ?>
											<td><a href="#" onClick="jSTUDENT_ACTIVATE()" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_wait_approve.gif" width="20" height="20" border="0" align="absmiddle"><?=$ec_iPortfolio['import_activation']?></a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
<?php }/* 	#Disabled, Import Student moved to Account Management

			if(strstr($ck_function_rights, "Profile:ImportData")) { ?>
											<td><a href="javascript:newWindow('profile/student_data/import_sams.php', 27)" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_import.gif" width="20" height="20" border="0" align="absmiddle"><?=$iPort["btn"]["import_student_info"]?></a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
<?php }*/
			if(strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing:CDBurning")) { ?>
											<td><a href="learning_portfolio/contents_admin/prepare_CDburning.php" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_cd.gif" width="20" height="20" border="0" align="absmiddle"><?=$iPort['menu']['prepare_cd_rom_burning']?></a></td>
											<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
<?php } ?>
										</tr>
									</table>
								</td>
								<td align="right" valign="bottom" class="thumb_list">
									<span class="tabletext">
										<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$search_field_default_text:stripslashes($search_name))?>" onFocus="SearchTextFocus=true;if(this.value=='<?=$search_field_default_text?>'){this.value=''}" onBlur="SearchTextFocus=false;if(this.value==''){this.value='<?=$search_field_default_text?>'}"  title="<?=$search_field_default_text?>" />
										<!--<input type="button" class="formsubbutton" value="<?=$button_search?>"  onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'" onClick="jSUBMIT_SEARCH()" />-->
									</span>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="3" width="100%">
										<tr>
											<td>
												<?=$year_selection_html ?>
											</td>
<?php if(strstr($ck_function_rights, "Alumni") && !$sys_custom['iPortfolio_alumni_report']) { ?>
											<td align="right" valign="middle" class="thumb_list">
												<span> <?=$i_identity_student?> </span> | <a href="school_records_alumni.php"><?=$i_identity_alumni?></a>
											</td>
<?php } ?>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<?=$LibTable->displayPlain(); ?>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($LibTable->navigationHTML!="") { ?>
                    <tr class='tablebottom'>
                      <td  class="tabletext" align="right"><?=$LibTable->navigationHTML?></td>
                    </tr>
<?php } ?>
                  </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
<input type="hidden" name="order" value="<?= $order ?>">
<input type="hidden" name="field" value="<?= $field ?>">
<input type="hidden" name="page_size_change" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>