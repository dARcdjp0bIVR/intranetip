<?php
/**
 * [Modification Log] Modifying By:
 * ************************************************
 *  Date:   2019-03-19 [Bill] always show "School Record" icon if has right [2017-1207-0959-51277]    ($sys_custom['iPf']['DBSTranscript_SkipActivateChecking'])
 *  Date:   2018-05-28 [Anna] Added SearchByParentOccupation cust #140045
 *  Date:   2018-05-21 [Anna] added $sys_custom['iPf']['SearchByParentName'] #139773
 * 	Date:	2018-02-13 [Pun] added search UserLogin for NCS cust
 * 	Date:	2018-01-31 [Isaac]$sys_custom['LivingHomeopathy']- added export students' profile function
 * 	Date:	2017-11-07 [Bill] $sys_custom['iPf']['HKUGA_eDis_Tab'] - HOY Group Member can view all classes
 *  Date:	2017-08-15 [Carlos] $sys_custom['LivingHomeopathy'] - added can search with userlogin. 
 *	Date:   2016-04-18 [Omas]
 *		 - class selection hide first option
 * ************************************************
 */

ini_set('memory_limit', '256M');

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-accountmanager.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libportfolio-ui.php");

include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

global $sys_custom;

$lpf = new libportfolio();
$lpf->ACCESS_CONTROL("student_info");

$lpf_acm = new libpf_accountmanager();
$lpf_acm->SET_LICENSE_KEYS();

if(!strstr($ck_user_rights, ":manage:"))
{
  # Class teacher view class
  if(strstr($ck_user_rights_ext, "student_info:form_t"))
  {
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
    $class_teach_arr = $lpf_acc->GET_CLASS_TEACHER_CLASS();
    $cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
  }
  # Subject teacher view class
  else if(strstr($ck_user_rights_ext, "student_info:form_subject_t"))
  {
    $lpf_acc = new libpf_account_teacher();
    $lpf_acc->SET_CLASS_VARIABLE("user_id", $UserID);
    $class_teach_arr = array_merge($lpf_acc->GET_CLASS_TEACHER_CLASS(), $lpf_acc->GET_SUBJECT_TEACHER_CLASS());
    $cond .= " AND yc.YearClassID IN (".implode(",", $class_teach_arr).")";
  }
}

$has_sr_access_right = $lpf->HAS_SCHOOL_RECORD_VIEW_RIGHT() ? 1 : 0;
$has_lp_access_right = (strstr($ck_user_rights, ":web:") && $lpf->HAS_RIGHT("learning_sharing")) ? 1 : 0;
$has_sbs_access_right = (strstr($ck_user_rights, ":growth:") && $lpf->HAS_RIGHT("growth_scheme")) ? 1 : 0;

// [2017-0403-1552-04240] HOY Group Member - View all clesses
if($sys_custom['iPf']['HKUGA_eDis_Tab'] && $lpf->IS_HOY_GROUP_MEMBER_IPO())
{
	$class_teach_arr = "";
	$cond = "";
	$has_sr_access_right = 1;
}

// [2017-1207-0959-51277] skip activated account checking logic
$is_skip_activated_checking = false;
if($sys_custom['iPf']['DBSTranscript_SkipActivateChecking'] && $has_sr_access_right) {
    $is_skip_activated_checking = true;
}

# Define display style
if(!isset($DisplayType) || $DisplayType == "" ) $DisplayType = "list";
if($search_name == $ec_iPortfolio['enter_student_name'])
{
	$search_name = "";
}
else
{
	if(trim($search_name) == '') {
		// do nothing
	} else {
		$initialFieldName = getInitialNameField("iu.");
	  
		$searchUserLogin = ($sys_custom['LivingHomeopathy'] || (isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""));
		$cond .=  " AND (iu.ChineseName LIKE '%".$search_name."%' OR iu.EnglishName LIKE '%".$search_name."%' OR $initialFieldName LIKE '%".$search_name."%' ".($searchUserLogin?" OR iu.UserLogin LIKE '%".$search_name."%' ":"");
		if($sys_custom['iPf']['SearchByParentName']){
		    $cond .= " OR gs.EnName LIKE '%".$search_name."%' OR gs.ChName LIKE '%".$search_name."%'";
		}
		if($sys_custom['iPf']['SearchByParentOccupation']){
		    $cond .= " OR gse1.Occupation LIKE '%".$search_name."%' ";
		}
		$cond .=")";
	}
}

if ($order=="") $order=1;
if ($field=="") $field=0;
$pageSizeChangeEnabled = true;
$LibTable = new libpf_dbtable($field, $order, $pageNo);

$img_activated = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_active_ipf.gif\" width=\"16\" height=\"20\" border=\"0\" align=\"absmiddle\">";
$img_pi = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_stu_info.gif\" alt=\"".$ec_iPortfolio['heading']['student_info']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_sr = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_school_record.gif\" alt=\"".$ec_iPortfolio['heading']['student_record']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_lp = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_learning_portfolio.gif\" alt=\"".$ec_iPortfolio['heading']['learning_portfolio']."\" width=\"20\" height=\"20\" border=\"0\">";
$img_sbs = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/icon_sbs.gif\" alt=\"".$iPort['menu']['school_based_scheme']."\" width=\"20\" height=\"20\" border=\"0\">";

// create a temp table to store the whether a student has update any LP
$studentHaveUpdate = 1;
$studentHaveNotUpdate  = 0;

$tmpSql = "create temporary  table temp_newLPForStudent
			(
			`StudentID` int(8)  NULL,
			`haveNew` tinyint(1) ,
			KEY `StudentID` (`StudentID`),
			UNIQUE KEY `u_StudentID` (`StudentID`)
			)";
$lpf->db_db_query($tmpSql);

$lpStudentIdCond = '';
$lpStudentIdList = '';
if(is_numeric($YearClassID)){
	$tmpSql = 'select UserID as \'UserID\' from '.$intranet_db.'.YEAR_CLASS_USER where YearClassID = '.$YearClassID;
	$tmpResult = $lpf->returnResultSet($tmpSql);
	
	if(count($tmpResult) > 0){
		$tmpAry = Get_Array_By_Key($tmpResult, 'UserID');
		$lpStudentIdList = implode(',',$tmpAry);
		if($lpStudentIdList != ''){
			$lpStudentIdCond = ' and ps.UserID in ('.$lpStudentIdList.')';
		}
	}
}

//insert record to temp table 
//only one student will have one record in table temp_newLPForStudent
//for ONE student has more than one LP, 
//If any LP 1) student_publish_time is larger than teacher_view_time or 2) student has publish the first time and teacher has not viewed , 
//it will set haveNew to $studentHaveUpdate. (ON DUPLICATE KEY UPDATE haveNew = if(haveNew < values(haveNew) , values(haveNew),haveNew))
//Teacher will see a "new" in the list
$insertCheckNewSql = "
		insert into 
			temp_newLPForStudent(StudentID,haveNew)
		select 
			ptl.student_id,
			if(
				(UNIX_TIMESTAMP(ptl.student_publish_time)>UNIX_TIMESTAMP(ptl.teacher_view_time))
					or
				(ptl.student_publish_time is not null and ptl.teacher_view_time is null),
				{$studentHaveUpdate},
				{$studentHaveNotUpdate}
			) as 'haveUpdate'
		from 
			{$eclass_db}.PORTFOLIO_STUDENT as ps  inner join  
			".$lpf->course_db.".portfolio_tracking_last as ptl on ps.CourseUserID = student_id
			inner join ".$lpf->course_db.".web_portfolio as wp on wp.web_portfolio_id =  ptl.web_portfolio_id
		where 
		teacher_id = {$ck_user_id} 
		and wp.status = 1
		{$lpStudentIdCond}
		ON DUPLICATE KEY UPDATE 
		haveNew = if(haveNew < values(haveNew) , values(haveNew),haveNew)
	";
$lpf->db_db_query($insertCheckNewSql);
//debug_r($insertCheckNewSql);
//$aa = "select * from temp_newLPForStudent";
//debug_r($lpf->returnArray($aa));

if($sys_custom['iPf']['SearchByParentName']){
    $joinTable= " LEFT  JOIN {$eclass_db}.GUARDIAN_STUDENT AS gs
                ON (gs.UserID =iu.UserID )";    
}
if($sys_custom['iPf']['SearchByParentOccupation']){
    $joinTable .=" LEFT JOIN {$eclass_db}.GUARDIAN_STUDENT_EXT_1 AS gse1 ON (iu.UserID  = gse1.UserID )";
}
$cond .= ($YearClassID == "") ? "" : " AND ycu.YearClassID = ".$YearClassID;

$sql =  " SELECT DISTINCT
            ".libportfolio::GetYearClassTitleFieldByLang("yc.")." AS ClassTitle,
            ycu.ClassNumber,
            '<!--StudentPhoto-->',
            ".getNameFieldByLang2("iu.")." AS UserName,
            CONCAT(Replace(BINARY iu.WebSAMSRegNo,'#',''), '&nbsp;', IF(ps.RecordID IS NULL, '', '".$img_activated."')) AS WebSAMSRegNo,
            CONCAT('<a href=\"javascript:jTO_INFO(', iu.UserID,', ',yc.YearClassID,')\">".$img_pi."</a>') AS PILink,
            IF(".$has_sr_access_right." ".($is_skip_activated_checking? "" : "AND ps.RecordID IS NOT NULL AND ps.IsSuspend = 0").", CONCAT('<a href=\"javascript:jTO_SR(', iu.UserID,')\">".$img_sr."</a>'), '&nbsp;') AS SRLink,
            IF(".$has_lp_access_right." AND ps.RecordID IS NOT NULL AND ps.IsSuspend = 0, CONCAT('<a href=\"javascript:jTO_LP(', iu.UserID,')\" class=\"new_alert\">".$img_lp."', IF(lpStatus.haveNew = {$studentHaveUpdate}, 'new', ''), '</a>'), '&nbsp;') AS LPLink,
            IF(".$has_sbs_access_right." AND ps.RecordID IS NOT NULL AND ps.IsSuspend = 0, CONCAT('<a href=\"javascript:jTO_SBS(', iu.UserID,')\">".$img_sbs."</a>'), '&nbsp;') AS SBSLink,
            CONCAT('<input type=\"checkbox\" name=\"user_id[]\" value=\"', iu.UserID, '\">'),
            iu.UserID,
            IF(ps.RecordID IS NULL, 0, 1) AS Activated,
            IF(ps.RecordID IS NOT NULL AND ps.IsSuspend = 1, 1, 0) AS Suspended
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          LEFT JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
            ON  ycu.UserID = iu.UserID
          LEFT JOIN {$intranet_db}.YEAR_CLASS AS yc
            ON yc.YearClassID = ycu.YearClassID
          LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
            ON  iu.UserID = ps.UserID
          LEFT JOIN ".$lpf->course_db.".user_config AS uc
            ON  ps.CourseUserID = uc.user_id AND
                uc.notes_published IS NOT NULL
          LEFT JOIN ".$lpf->course_db.".portfolio_tracking_last AS ptl
            ON  uc.user_id = ptl.student_id AND
                uc.web_portfolio_id = ptl.web_portfolio_id AND
                ptl.teacher_id = {$ck_user_id}
          LEFT JOIN temp_newLPForStudent as lpStatus
            ON  lpStatus.StudentID = ps.CourseUserID
          $joinTable
          WHERE
            iu.RecordType = 2 AND
            iu.RecordStatus = 1 AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()."
            $cond
          GROUP BY
            iu.UserID
        ";

//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
$LibTable->sql = $sql;
$LibTable->field_array = array("ClassTitle", "ClassNumber", "UserName", "WebSAMSRegNo");
$LibTable->fieldorder2 = ", ClassNumber ASC";
$LibTable->db = $intranet_db;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($num_per_page=="") ? $page_size : $num_per_page;
if ($page_size_change!="") $LibTable->page_size = $numPerPage;
$LibTable->no_col = 10;
$LibTable->noNumber = true;

$LibTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$LibTable->row_alt = array("#FFFFFF", "F3F3F3");
//$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";
$LibTable->column_array = array(0, 3, 3, 0, 0, 3, 3, 3, 3, 3);

// TABLE COLUMN
$LibTable->column_list = "<tr class='tabletop'>\n";
$LibTable->column_list .= "<td nowrap='nowrap' >".$LibTable->column(0,$i_UserClassName)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$LibTable->column(1,$i_UserClassNumber)."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center'>&nbsp;</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap'>".$LibTable->column(2,$ec_iPortfolio['heading']['student_name'])."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap'>".$LibTable->column(3,$ec_iPortfolio['heading']['student_regno'])."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['student_info']."</td>\n";
$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['student_record']."</td>\n";
$LibTable->column_list .= $has_lp_access_right ? "<td nowrap='nowrap' align='center'>".$ec_iPortfolio['heading']['learning_portfolio']."</td>\n" : "<td>&nbsp;</td>";
$LibTable->column_list .= $has_sbs_access_right ? "<td nowrap='nowrap' align='center'>".$iPort['menu']['school_based_scheme']."</td>\n" : "<td>&nbsp;</td>";
$LibTable->column_list .= "<td nowrap='nowrap' align='center'>".$LibTable->check("user_id[]")."</td>\n";
$LibTable->column_list .= "</tr>\n";	

//$t_classlevel_arr = $lpf_fc->GET_CLASSLEVEL_LIST();
//for($i=0; $i<count($t_classlevel_arr); $i++)
//{
//  $t_classlevel_id = $t_classlevel_arr[$i]["YearID"];
//  $t_classlevel_name = $t_classlevel_arr[$i]["YearName"];
//
//  $lpf_fc->SET_CLASS_VARIABLE("YearID", $t_classlevel_id);
//  $t_class_arr = $lpf_fc->GET_CLASS_LIST();
//  
//  for($j=0; $j<count($t_class_arr); $j++)
//  {
//    $t_yc_id = $t_class_arr[$j][0];
//    $t_yc_title = Get_Lang_Selection($t_class_arr[$j]['ClassTitleB5'], $t_class_arr[$j]['ClassTitleEN']);
//    if(!is_array($class_teach_arr) || in_array($t_yc_id, $class_teach_arr))
//    {
//      $class_arr[$t_classlevel_name][] = array($t_yc_id, $t_yc_title);
//    }
//    
//    if($t_yc_id == $YearClassID)
//    {
//      $class_name = $t_yc_title;
//    }
//  }
//}
$class_name = $i_general_WholeSchool;
$lpf_fc = new libpf_formclass();
$class_arr = $lpf_fc->getClassListArray($class_teach_arr);
if(is_array($class_arr))
{
  foreach($class_arr as $value)
  {
  	foreach($value as $one){
  		
  		list($t_yc_id, $t_yc_title) = $one;
  		
  		if($t_yc_id == $YearClassID)
  	    {
  	      $class_name = $t_yc_title;
  	      break;
  	    }
  	}
  	unset($one);
  }
  unset($value);
}

// Generate class selection with optgroup
//$class_selection_html = getSelectByArray($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, 1, 0, "", 2);
$lpf_ui = new libportfolio_ui();
//$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, true);
$class_selection_html = $lpf_ui->GEN_CLASS_SELECTION_OPTGROUP($class_arr, "name='YearClassID' onChange='jCHANGE_FIELD()'", $YearClassID, true, false);

# Generate class selection drop-down list
$lpf_fc->SET_CLASS_VARIABLE("YearClassID", $YearClassID);
$lpf_fc->SET_CLASS_VARIABLE("YearID", '');
$act_student_arr = $lpf_fc->GET_ACTIVATED_STUDENT_LIST($ActiveStudentOnly=1);
$student_arr = $lpf_fc->GET_STUDENT_LIST($activatedPorfolioOnly=false, $haveClassOnly=true);
//debug_pr($act_student_arr);
//debug_pr($student_arr);
$StatusStr = str_replace("<!--PortfolioNo-->", count($act_student_arr), $ec_iPortfolio['portfolio_status']);
$StatusStr = str_replace("<!--TotalStudentNo-->", count($student_arr), $StatusStr);

$RepStr = $DisplayType == "list" ? "" : $ec_iPortfolio['green_frame'];
$StatusStr = str_replace("<!--GreenFrame-->", $RepStr, $StatusStr);

$search_field_default_text = $sys_custom['LivingHomeopathy']? $ec_iPortfolio['enter_student_name_or_userlogin'] : $ec_iPortfolio['enter_student_name'];

$MenuArr = array();
$MenuArr[] = array($ec_iPortfolio['class_list'], "school_records.php");
$MenuArr[] = array($class_name, "");

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_StudentAccount";
$CurrentPageName = $iPort['menu']['student_account'];

### Title ###
$TAGS_OBJ[] = array($ec_iPortfolio['student_account'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

$linterface->LAYOUT_START();
echo $linterface->Include_JS_CSS();

# define sspacing between words
$space = ($intranet_session_language == "en") ? ' ' : '' ;

# build thickbox
if($sys_custom['LivingHomeopathy']) {
$thickBoxHeight = 300;
$thickBoxWidth = 600;

$exportProfileButton = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "",
    $iPort["Export"].$space.$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'], "", 'divSectionExport', $Content='', 'exportProfileButton');

# thickbox layout
$thickboxContent = '';
$thickboxContent .= '<div id="divSectionExport" style="display:none">';
$thickboxContent .= '<form method="POST" name="exportSessionForm" action="profile/export_students_profile.php">';
$thickboxContent .= '<table class="form_table_v30">';
$thickboxContent .= '<tr>';
$thickboxContent .= '<td class="field_title"><span>'.$iPort["Selected"].$space.$ec_iPortfolio['export']['Category'].'</span></td>';
$thickboxContent .= "<td>".$linterface->Get_Checkbox('selectAll', 'selectAll', '', '', '', $langpf_lp2['admin']['print']['selectall'],'check_select_all();')."<br/>";
$thickboxContent .= $linterface->Get_Checkbox("基本資料", "基本資料", "基本資料", 1, "", "基本資料","", true).'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("學歷", "sections[]", "學歷", "", "", "學歷","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("訓練及進修", "sections[]", "訓練及進修", "", "", "訓練及進修","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("工作經歷", "sections[]", "工作經歷", "", "", "工作經歷","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("社會工作", "sections[]", "社會工作", "", "", "社會工作","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("語文能力", "sections[]", "語文能力", "", "", "語文能力","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("專長", "sections[]", "專長", "", "", "專長","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("專業技術", "sections[]", "專業技術", "", "", "專業技術","check_category_select();").'<br/>';
$thickboxContent .= $linterface->Get_Checkbox("其他", "sections[]", "其他", "", "", "其他","check_category_select();").'其他<br/>';
$thickboxContent .= '<input type="hidden" name="user_id"><br/>';
$thickboxContent .= '</td>';
$thickboxContent .= '</tr>';
$thickboxContent .= '</table>';
$thickboxContent .= '<br/>';
$thickboxContent .= '<div align="center">';
$thickboxContent .= $linterface->GET_ACTION_BTN($iPort["Export"].$space.$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile'], "button", "jExport_StuProfile()");
$thickboxContent .= '</div>';
$thickboxContent .= '</form>';
$thickboxContent .= '</div>';
}
?>

<script language="JavaScript">

function js_show_thickbox()
{
	if (!$("input[name='user_id[]']").is(':checked')){
		alert("<?php echo $ec_warning['select_student'];?>");
	} else {
		$('a#exportProfileButton').click();
	}
}

// Change hidden field "FieldChanged" for keeping track which selection list is used, and reload the page
function jCHANGE_FIELD(jParField)
{
/*
	if(jParField == 'classlevel')
	{
		jCHANGE_CLASSLIST();
	}
	else if(jParField == 'thumbnail' || jParField == 'list')
	{
		document.form1.DisplayType.value = jParField;
		document.form1.action = "school_records_class.php";
		document.form1.submit();
	}
	else
	{
		document.form1.FieldChanged.value = jParField;
		document.form1.action = "school_records_class.php";
		document.form1.submit();
	}
*/
  if(jParField == 'thumbnail' || jParField == 'list')
	{
		document.form1.DisplayType.value = jParField;
	}

  document.form1.action = "school_records_class.php";
  document.form1.submit();
}

// Change page to display student detail information
function jTO_INFO(jParStudentID, jParYearClassID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.StudentYearClassID.value = jParYearClassID;
	document.form1.action = "profile/student_info_teacher.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_LP(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "profile/learning_portfolio_teacher<?=$iportfolio_lp_version == 2?'_v2':''?>.php";
	document.form1.submit();
}

// Change page to display student detail information
function jTO_SR(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "profile/school_record_teacher.php";
	document.form1.submit();
}

// Change page to display school base scheme
function jTO_SBS(jParStudentID)
{
	document.form1.StudentID.value = jParStudentID;
	document.form1.action = "profile/sbs/index.php";
	document.form1.submit();
}

// Activate students
function jSTUDENT_ACTIVATE(){
	if(countChecked(document.form1, 'user_id[]') > 0)
	{
		document.form1.action = "profile/management/student_activate.php";
		document.form1.submit();
	}
	else
		alert("<?=$msg_check_at_least_one?>");
}

// Suspend students
function jSTUDENT_SUSPEND(){
	if(countChecked(document.form1, 'user_id[]') > 0)
	{
		if(confirm("<?=$ec_iPortfolio['suspend_account_confirm']?>"))
		{
			document.form1.action = "profile/management/student_suspend.php";
			document.form1.submit();
		}
	}
	else
		alert("<?=$msg_check_at_least_one?>");
}
// export student 
function jExport_StuProfile(){
		document.exportSessionForm.user_id.value = $("input[name='user_id[]']:checked").map(function() {
		    return this.value;
		}).get();

// 	document.form1.action = "profile/export_students_profile.php";
// 	document.exportSessionForm.action = "profile/export_students_profile.php";
// 	document.form1.submit();
	document.exportSessionForm.submit();
}

function check_select_all(){
	if ($("input[name='selectAll']").is(':checked')) {
		$("input[name='sections[]']").attr("checked", true);
	}else if (!$("input[name='selectAll']").is(':checked')){
		$("input[name='sections[]']").attr("checked", false);
	}
}

function check_category_select(){
	var totalCheckboxes = $("input[name='sections[]']:checkbox").length;
	var checkedCheckboxes = $("input[name='sections[]']:checked").length;

	if(checkedCheckboxes == totalCheckboxes){
		$("input[name='selectAll']").attr("checked", true);		
	} else {
		$("input[name='selectAll']").attr("checked", false);
	}
}

/*
functions for showing / hiding photos
1. getMouseXY
2. jSHOW_PHOTO
3. jHIDE_PHOTO
*/
function getMouseXY(e) {
  var IE = document.all?true:false

  if (IE) { // grab the x-y pos.s if browser is IE
    tempX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    tempY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop - 10;
  } else {  // grab the x-y pos.s if browser is NS
    tempX = e.pageX
    tempY = e.pageY
  }  
  // catch possible negative values in NS4
  if (tempX < 0){tempX = 0}
  if (tempY < 0){tempY = 0}  
  // show the position values in the form named Show
  // in the text fields named MouseX and MouseY
  
  pos = new Array(tempX,tempY);
  return pos;
}

function jSHOW_PHOTO(e, jParPhotoURL){
	jHIDE_PHOTO();
	
	// Move Layer
	if (!e) var e = window.event;
	mousePos = getMouseXY(e);
	xValue = mousePos[0]; 
	yValue = mousePos[1];
	
	var PhotoLayer = document.getElementById("PhotoLayer"); 
	
	PhotoLayer.style.top = ( yValue + 10 ) + "px"; 
	PhotoLayer.style.left = ( xValue + 10 ) + "px";    

  PhotoLayer.innerHTML = "<img src=\""+jParPhotoURL+"\" width=\"100\" height=\"130\" class=\"photo_border\" />";
	PhotoLayer.style.visibility = 'visible';
}

function jHIDE_PHOTO(){
	if ( document.getElementById("PhotoLayer").style.visibility == 'visible') 
	{ 
		document.getElementById("PhotoLayer").style.visibility = 'hidden'; 
	}
}

<?php if($sys_custom['iPf']['Mgmt']['activatedLicenseSummary']){ ?>
	function js_Show_Activated_License_Summary(){
		$.post(
			"ajax_get_activated_account_summary.php", 
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
<?php } ?>

$(document).ready(function(){
  $("input[name=search_name]").keypress(function(event){
    if(event.keyCode == 13)
    {
    	document.form1.action = "school_records_class.php";
    	document.form1.submit();
    }
  });

});
</script>

<form method='POST' name='form1' action='school_records_class.php'>
  <table width="100%" border="0" cellpadding="0" cellspacing="10">
  	<tr>
  		<td>
  			<table width="100%" border="0" cellspacing="0" cellpadding="0">
  				<tr>
  					<td>
  						<table border="0" cellspacing="0" cellpadding="2">
  							<tr>
<?php if(strstr($ck_function_rights, "Profile:Student")) { ?>
                                  <td><div class="Conntent_tool"><a href="profile/management/import_regno.php?YearClassID=<?=$YearClassID?>" class="import"><?=$ec_iPortfolio['import_regno']?></a></div></td>
								  <td width="20px"></td>
	<?php if($sys_custom['iPf']['Mgmt']['activatedLicenseSummary']){ ?>
								  <td><?= $linterface->Get_Thickbox_Link(350, 600, "contenttool", $ec_iPortfolio['Activated_Liense_Summary'], "js_Show_Activated_License_Summary(); return false;", "FakeLayer", '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/icon_summary.gif" width="20" height="20" border="0" align="absmiddle">'.$ec_iPortfolio['Activated_Liense_Summary']); ?></td>
<?php 
		}
	} ?>
  							</tr>
  						</table>
  					</td>
  					<td align="right" valign="bottom" class="thumb_list">
  						<span class="tabletext">
  							<input name="search_name" type="text" class="tabletext" value="<?=($search_name==""?$search_field_default_text:intranet_htmlspecialchars(stripslashes($search_name)))?>" onFocus="if(this.value=='<?=$search_field_default_text?>'){this.value=''}" onBlur="if(this.value==''){this.value='<?=$search_field_default_text?>'}" title="<?=$search_field_default_text?>" />
  						</span>
  					</td>
  				</tr>
  			</table>
  			<table width="100%" border="0" cellspacing="0" cellpadding="3">
  				<tr>
  					<td><?=$class_selection_html?></td>
  					<td align="right" valign="bottom" class="tabletextremark"><?=(strstr($ck_function_rights, "Profile:Student") && $DisplayType=="list"?$ec_iPortfolio['available_portfolio_acc'].": ".$lpf_acm->GET_NUMBER_FREE_LICENSE():"&nbsp;")?></td>
  				</tr>
  				<tr>
  					<td>
  						<table width="100%" border="0" cellspacing="0" cellpadding="5">
  							<tr>
                  <td class="navigation"><?=$linterface->GET_NAVIGATION($MenuArr) ?></td>
  							</tr>
  						</table>
  					</td>
  					<td align="right" valign="bottom" class="thumb_list">
<?php
  switch($DisplayType) {
    case "list":
      echo "<a href=\"javascript:jCHANGE_FIELD('thumbnail')\">".$ec_iPortfolio['thumbnail']."</a>";
      echo " | ";
      echo "<span>".$ec_iPortfolio['list']."</span>";
      break;
    case "thumbnail":
      echo "<span>".$ec_iPortfolio['thumbnail']."</span>";
      echo " | ";
      echo "<a href=\"javascript:jCHANGE_FIELD('list')\">".$ec_iPortfolio['list']."</a>";
      break;
  }
?>
            </td>
  				</tr>
  			</table>
  			<table width="100%" border="0" cellspacing="0" cellpadding="0">
  				<tr>
<?php if($DisplayType == "thumbnail") { ?>
  					<td class="student_list_top student_list_top_thumb tabletext">
  						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" width="20" height="20" align="absmiddle"> <?=$StatusStr?>
<?php } else { ?>
  					<td align="right" class="tabletext">
  						<table width="100%" border="0" cellspacing="0" cellpadding="0">
  							<tr class="table-action-bar">
  								<td class="tabletextremark" nowrap>
  									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/iPortfolio/icon_learning_portfolio.gif" width="20" height="20" align="absmiddle"> <?=$StatusStr?>
  								</td>
<?php if(strstr($ck_function_rights, "Profile:Student")) { ?>
  								<td align="right" valign="bottom">
  									<table border="0" cellspacing="0" cellpadding="0">
  										<tr>
  											<td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_01.gif" width="21" height="23"></td>
  											<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_02.gif">
  												<table border="0" cellspacing="0" cellpadding="2">
  													<tr>
  														<?php if($sys_custom['LivingHomeopathy']) { ?>
															<td nowrap>
															<a href="#" onClick="js_show_thickbox()" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_export.gif" width="12" height="12" border="0" align="absmiddle"><?=$iPort["Export"].$space.$Lang['iPortfolio']['LivingHomeopathy']['PersonalProfile']?> </a></td>
                                                 			
                                                         <?php echo $exportProfileButton; } ?>
  														<td nowrap><a href="javascript:jSTUDENT_ACTIVATE()" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_activate_iPortfolio?> </a></td>
  														<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5"></td>
  														<td nowrap><a href="javascript:jSTUDENT_SUSPEND()" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_reject.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_deactivate?> </a></td>
  													</tr>
  												</table>
  											</td>
  											<td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/table_tool_03.gif" width="6" height="23"></td>
  										</tr>
  									</table>
  								</td>
<?php } ?>
  							</tr>
  						</table>
<?php } ?>
    				</td>
    			</tr>
    			<tr>
    				<td class="student_thumb_content">
  						<?=($DisplayType == "list" ? $LibTable->displayStudentList_ListView() : $LibTable->displayStudentList_ThumbnailView())?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="h-13-black">
<?php if ($LibTable->navigationHTML!="") { ?>
                <tr class='tablebottom'>
                  <td  class="tabletext" align="right"><?=$LibTable->navigation(1)?></td>
                </tr>
<?php } ?>
              </table>
  					</td>
  				</tr>
  			</table>
  		</td>
  	</tr>
  </table>
  <div id='PhotoLayer' style='position:absolute;height:130px;width:100px;visibility:hidden;'></div>

  <input type="hidden" name="DisplayType" value="<?=$DisplayType?>" />
  <input type="hidden" name="StudentID" />
  <input type="hidden" name="StudentYearClassID" />
  
  <input type="hidden" name="pageNo" value="<?php echo $LibTable->pageNo; ?>">
  <input type="hidden" name="order" value="<?= $order ?>">
  <input type="hidden" name="field" value="<?= $field ?>">
  <input type="hidden" name="page_size_change" />
  <input type="hidden" name="numPerPage" />

</form>
<?=$thickboxContent?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>