<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-30 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf_dbs = new libpf_dbs_transcript();
$result = $lpf_dbs->updateIBGradeMapping($_POST['subjectGrade']);

$msg = $result? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: index.php?msg='.$msg);
?>