<style type="text/css">
.table_board .common_table_list .tabletopnolink { text-align: center; }
.table_board .common_table_list .tabletext { text-align: center; }
</style>

<script type="text/javascript">
function goBack() {
	$('.edit_table').hide();
	$('.edit_button').hide();
	
	$('.view_table').show();
	$('.display_button').show();
}

function goEdit() {
	$('.view_table').hide();
	$('.display_button').hide();

	$('.edit_table').show();
	$('.edit_button').show();
}

function goSubmit() {
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="update.php">
	<div class="table_board">
		<br />
		
		<p class="spacer"></p>
		<br />
		
		<div>
			<table width='100%'>
    			<tr>
    				<td width='20%' valign='top'>
            			<?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['AllSubject']?> <br/>
            			<table id='all_subject_view_table' class='common_table_list view_table'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='50%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='50%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Grade']?></th>
							</tr>
                			<?=$htmlAry['AllSubjectTable']['View']?>
            			</table>
            			<table id='all_subject_edit_table' class='common_table_list edit_table' style='display: none'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='50%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='50%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Grade']?></th>
							</tr>
                			<?=$htmlAry['AllSubjectTable']['Edit']?>
            			</table>
    				</td>
    				<td width='1%'>&nbsp;</td>
    				
    				<td width='40%' valign='top'>
            			<?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Math']?> <br/>
            			<table id='math_subject_view_table' class='common_table_list view_table'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathStudies']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathSL']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathHL']?></th>
							</tr>
                			<?=$htmlAry['MathSubjectTable']['View']?>
            			</table>
            			<table id='math_subject_edit_table' class='common_table_list edit_table' style='display: none'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathStudies']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathSL']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressMathHL']?></th>
							</tr>
                			<?=$htmlAry['MathSubjectTable']['Edit']?>
            			</table>
    				</td>
    				<td width='1%'>&nbsp;</td>
    				
    				<td width='40%' valign='top'>
            			<?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Chinese']?> <br/>
            			<table id='chi_subject_view_table' class='common_table_list view_table'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLit']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLangLit']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseB']?></th>
							</tr>
                			<?=$htmlAry['ChiSubjectTable']['View']?>
            			</table>
            			<table id='chi_subject_edit_table' class='common_table_list edit_table' style='display: none'>
							<tr>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['IBGrade']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLit']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseLangLit']?></th>
    							<th class='tablegreentop tabletopnolink' width='25%'><?=$Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['ProgressChineseB']?></th>
							</tr>
                			<?=$htmlAry['ChiSubjectTable']['Edit']?>
            			</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<br/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="display_button">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["ViewButton"]?></td></tr>
	  		</table>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="edit_button" style="display: none">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["EditButton"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>