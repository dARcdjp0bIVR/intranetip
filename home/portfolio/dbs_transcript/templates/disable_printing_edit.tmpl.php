<form name="form1" action="update.php" method="POST">
	<?=$html["navigation"]?>
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td>
    			<table width="96%" border="0" cellpadding="5" cellspacing="0">
    				<tr align="left">
    					<td>
    						<table class="form_table_v30">
    							<?php if($is_edit) { ?>
                            		<tr>
        								<td valign="top" nowrap="nowrap" class="field_title"><?=$i_general_target?></td>
										<td><?=$disable_student['StudentName']?> (<?=$disable_student['ClassName']?>)</td>
									</tr>
    							<?php } else { ?>
                            		<tr>
        								<td valign="top" nowrap="nowrap" class="field_title" rowspan="2">
        									<?=$i_general_target?> <span class="tabletextrequire">*</span>
        								</td>
                            			<td>
                            				<input type="radio" name="target_type" id="target_type_active" value='1' onclick="changeStudentType(1)" <?=(!$is_edit? 'checked' : '')?>/>
                            				<label for="target_type_active"> <?=$iPort['CurrentStudent']?> </label>
                            				<input type="radio" name="target_type" id="target_type_archive" value="0" onclick="changeStudentType(0)" <?=(true? '' : 'checked')?>/>
                            				<label for="target_type_archive"> <?=$Lang['Identity']['Alumni']?> </label>
                            			</td>
                            		</tr>
        							<tr valign="top">
    									<td>
    										<div id='active_div' <?=(!$is_edit? '' : ' style="display: none" ')?>>
            									<table border="0">
            										<tr>
            											<td>
            												<div id='DivRankTargetDetail'></div>
            												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('rankTargetDetail[]')){Select_All_Options('rankTargetDetail[]', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
            											</td>
            										</tr>
            									</table>
            									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID', false);?>
            									<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
        									</div>
    										<div id='archive_div' <?=(true? ' style="display: none" ' : '')?>>
            									<table border="0">
            										<tr>
            											<td><?=$html["year_selection"]?></td>
        											</tr>
            										<tr>
            											<td>
            												<div id='DivRankTargetDetail_Year'></div>
            												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('rankTargetDetail_Year[]')){Select_All_Options('rankTargetDetail_Year[]', true);}return false;","selectAllTargetBtn2",' style="display:none;"')?>
            											</td>
            										</tr>
            									</table>
            									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID_Year', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID_Year', false);?>
            									<div class="tabletextremark" id="DivSelectAllRemark_Year" style="display:none;">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
        									</div>
        								</td>
    								</tr>
    							<?php } ?>
                        		<tr>
                        			<td class="field_title">
                        				<?=$Lang['General']['Reason']?>
                        			</td>
                        			<td>
                    					<textarea wrap="virtual" onfocus="this.rows=5;" rows="2" cols="90" id="Reason" name="Reason" class="tabletext requiredField"><?=$disable_student['Reason']?></textarea>
                        			</td>
                        		</tr>
                        		<tr>
                        			<td class="field_title">
                        				<span class="tabletextrequire">*</span>
                        				<?=$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Status']?>
                        			</td>
                        			<td>
                        				<input type="radio" name="status" id="status_locked" value='1' <?=($disable_student['RecordStatus']? 'checked' : '')?>/>
                        				<label for="status_locked"> <?=$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Locked']?> </label>
                        				<input type="radio" name="status" id="status_unlocked" value="0" <?=($disable_student['RecordStatus']? '' : 'checked')?>/>
                        				<label for="status_unlocked"> <?=$Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Unlocked']?> </label>
                        			</td>
                        		</tr>
    						</table>
    						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
    							<!-- <tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>-->
    							<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
    							<tr>
    								<td align="center">
    									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit()", "submitBtn")?>
										<?= $linterface->GET_ACTION_BTN($Lang["Btn"]["Back"], "button", "history.back();")?>
    								</td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
	</table>
	
	<?=$html["display_table"]?>
</form>

<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

$(document).ready(function() {
	targetTypeChanged();
});

function changeStudentType(type)
{
	if(type == 1) {
		$('div#active_div').show();
		$('div#archive_div').hide();
	}
	else {
		$('div#active_div').hide();
		$('div#archive_div').show();
	}
}

function targetTypeChanged()
{
	var studentType = $("input[name='target_type']:checked").val();
	if(studentType == 1)
	{
		var targetDiv = 'DivRankTargetDetail';
		var targetYearId = academicYearId;
		var targetClassField = 'studentTargetID';
		var targetStudentField = 'TargetID';
	}
	else
	{
		var targetDiv = 'DivRankTargetDetail_Year';
		var targetYearId = $('#targetYearID').val();
		var targetClassField = 'studentTargetID_Year';
		var targetStudentField = 'TargetID_Year';
	}

	if(studentType != 1 && targetYearId == '')
	{
		$('#'+targetDiv).html('');
	}
	else
	{
    	$('#'+targetDiv).html(loadingImg).load(
    		'../ajax/ajax_reload.php',
    		{
    			'target': 'student',
    			'academicYearId': targetYearId,
    			'fieldId': targetClassField,
    			'fieldName': targetClassField+'[]',
    			'onchange': 'getStudentSelection()',
    			'divStudentSelection': targetStudentField,
    			'allCurriculum': 1
    		},
    		function(data){
    			$('#selectAllTargetBtn').hide();
    			$('#DivSelectAllRemark').show();
    			getStudentSelection();
    		}
    	);
	}
}

function getStudentSelection()
{
	var studentType = $("input[name='target_type']:checked").val();
	if(studentType == 1)
	{
		var targetDiv = 'DivRankTargetDetail';
		var targetYearId = academicYearId;
		var targetClassField = 'studentTargetID';
		var targetStudentField = 'TargetID';
		var targetArchived = '0';
	}
	else
	{
		var targetDiv = 'DivRankTargetDetail_Year';
		var targetYearId = $('#targetYearID').val();
		var targetClassField = 'studentTargetID_Year';
		var targetStudentField = 'TargetID_Year';
		var targetArchived = '1';
	}
	
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById(targetClassField);
	for(var i=0; i<yearClassObj.options.length; i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#'+targetStudentField).html(loadingImg);
	$.post(
		'../ajax/ajax_reload.php',
		{
			'target': 'student2ndLayer',
			'academicYearId': targetYearId,
			'fieldId': targetClassField,
			'fieldName': targetClassField+'[]',
			'studentFieldName': targetStudentField+'[]',
			'studentFieldId': targetStudentField,
			'YearClassID[]': selectedYearClassId,
			'includeInactive': 1,
			'isArchived': targetArchived,
			'excludeCurriculumChecking': 1
		},
		function(data){
			$('#'+targetStudentField).html(data);
		}
	);
}

function loadYearStudentList(yearId)
{
	targetTypeChanged();
}

function checkForm(formObj)
{
	var isValid = true;

	<?php if(!$is_edit) { ?>
		var studentType = $("input[name='target_type']:checked").val();
    	if(studentType == 1)
    	{
    		var targetStudentField = 'TargetID';
    		var targetWarningDiv = 'WarnTargetID';
    	}
    	else
    	{
    		var targetStudentField = 'TargetID_Year';
    		var targetWarningDiv = 'WarnTargetID_Year';
    	}
    	
    	var targetIdObj = $('#'+targetStudentField+' option:selected');
    	if(targetIdObj.length == 0){
    		$('#'+targetWarningDiv).show();
    		isValid = false;
    	} else {
    		$('#'+targetWarningDiv).hide();
    	}
	<?php } ?>
	
	if(isValid) {
		document.form1.submit();
	}
}

function goSubmit(){
	$('.warnMsgDiv').hide();
	if(checkForm()) {
		form1.submit();
	}
}
</script>