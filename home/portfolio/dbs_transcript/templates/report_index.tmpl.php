<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">
<style type="text/css">
    #studentsmod {position:relative;}
    #activestudentsautocomplete, #archivestudentautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #activestudentsautocomplete, #archivestudentautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #studentsinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */

    <?php foreach((array)$liArr as $this_Id => $_liArr) { ?>
        #studentscontainer_<?=$this_Id?>, {position:absolute;top:0.3em;width:100%}
        #studentscontainer_<?=$this_Id?> .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
        #studentscontainer_<?=$this_Id?> .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
        #studentscontainer_<?=$this_Id?> ul {padding:5px 0;width:100%;}
        #studentscontainer_<?=$this_Id?> li {padding:0 5px;cursor:default;white-space:nowrap;}
        #studentscontainer_<?=$this_Id?> li.yui-ac-highlight {background:#bbbbbb;}
        #studentscontainer_<?=$this_Id?> li.yui-ac-prehighlight {background:#FFFFFF;}
    <?php } ?>

    <?php foreach((array)$liArr2 as $this_Id => $_liArr) { ?>
        #studentscontainerCC_<?=$this_Id?>, {position:absolute;top:0.3em;width:100%}
        #studentscontainerCC_<?=$this_Id?> .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
        #studentscontainerCC_<?=$this_Id?> .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
        #studentscontainerCC_<?=$this_Id?> ul {padding:5px 0;width:100%;}
        #studentscontainerCC_<?=$this_Id?> li {padding:0 5px;cursor:default;white-space:nowrap;}
        #studentscontainerCC_<?=$this_Id?> li.yui-ac-highlight {background:#bbbbbb;}
        #studentscontainerCC_<?=$this_Id?> li.yui-ac-prehighlight {background:#FFFFFF;}
    <?php } ?>

    #studentsmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
    #studentsmod table{border-collapse:collapse;border-spacing:0;}
    #studentsmod fieldset,img{border:0;}
    #studentsmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
    #studentsmod ol,ul {list-style:none;}
    #studentsmod caption,th {text-align:left;}
    #studentsmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
    #studentsmod q:before,q:after{content:'';}
    #studentsmod abbr,acronym {border:0;}
    #studentsmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script type="text/javascript" language="JavaScript">
    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
    var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';
    var currentTargetType = '';

    $(document).ready(function() {
        reloadReportFields();
        currentTargetType = $("input[name='target_type']:checked").val();
    });

    function reloadReportFields()
    {
        var studentType = $("input[name='target_type']:checked").val();
        if(studentType == 1) {
            targetTypeChanged(document.form1.TargetType);
        }
        else {
            targetArchiveYearChanged(document.form1.targetYearID);
        }
        reloadReportImage();

        $('#WarnClassLevel').hide();

        $('#TargetFormStart').removeAttr('disabled');
        $('#TargetFormStart option').show();
        $('#TargetFormEnd').removeAttr('disabled');
        $('#TargetFormEnd option').show();

        // Handle default form range for different curriculum
        var selectedCurriculum = $("#curriculumID option:selected").text().toLowerCase();
        if(selectedCurriculum == 'ib' || selectedCurriculum == 'ibdp') {
            $('#TargetFormStart').attr('disabled', true);
            $('#TargetFormStart').val(9);

            if($('#TargetFormEnd').val() == '7' || $('#TargetFormEnd').val() == '8' || $('#TargetFormEnd').val() == '13') {
                $('#TargetFormEnd').val(12);
            }
            $("#TargetFormEnd option[value='7']").hide();
            $("#TargetFormEnd option[value='8']").hide();
            $("#TargetFormEnd option[value='13']").hide();
        } else if(selectedCurriculum == 'hkdse') {
            if($('#TargetFormStart').val() == '13') {
                $('#TargetFormStart').val(12);
            }
            $("#TargetFormStart option[value='13']").hide();

            if($('#TargetFormEnd').val() == '13') {
                $('#TargetFormEnd').val(12);
            }
            $("#TargetFormEnd option[value='13']").hide();
        } else {
            // do nothing
        }
    }

    function reloadReportImage()
    {
        $('#headerSelection').html(loadingImg);
        $('#backCoverSelection').html(loadingImg);
        $('#schoolSealSelection').html(loadingImg);
        $('#leftSignatureSelection').html(loadingImg);
        $('#rightSignatureSelection').html(loadingImg);
        $('#leftSignature2Selection').html(loadingImg);
        $('#rightSignature2Selection').html(loadingImg);

        var curriculumID = document.form1.curriculumID.value;
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'headerSelection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#headerSelection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'backCoverSelection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#backCoverSelection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'schoolSealSelection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#schoolSealSelection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'leftSignatureSelection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#leftSignatureSelection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'rightSignatureSelection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#rightSignatureSelection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'leftSignature2Selection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#leftSignature2Selection').html(data);
            }
        );
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'rightSignature2Selection',
                'CurriculumID': curriculumID
            },
            function(data){
                $('#rightSignature2Selection').html(data);
            }
        );
    }

    function updateSignatureRow(reportType, isChecked)
    {
        var rowObjArr = [];
        if(reportType == 'PG') {
            rowObjArr = $('tr.predicted_grade_row');
        } else {
            rowObjArr = $('tr.transcript_row');
        }

        if(rowObjArr != [])
        {
            $(rowObjArr).each(function() {
                if(isChecked) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    }

    function changeStudentType(type)
    {
        // Get current target type
        if(type == 'type') {
            var type = $("input[name='target_type']:checked").val();
        }

        // if target type changed > clear search by name multiselection
        if(type != currentTargetType) {
            checkOptionClear(document.form1.elements['search_by_name[]']);
        }

        var findType = $("input[name='findTarget']:checked").val();
        if(findType == 1) {
            if(type == 1) {
                $('div#active_div').show();
                $('div#archive_div').hide();
            }
            else {
                $('div#active_div').hide();
                $('div#archive_div').show();
            }
        }
        else {
            if(type == 1) {
                $('div#activestudentsautocomplete').show();
                $('div#archivestudentautocomplete').hide();
            }
            else {
                $('div#activestudentsautocomplete').hide();
                $('div#archivestudentautocomplete').show();
            }
        }
        changeFindTypeMethod(findType);
    }

    function targetTypeChanged(obj)
    {
        var curriculumID = document.form1.curriculumID.value;
        var selectedTargetType = obj.value;
        if(selectedTargetType == 'form' || selectedTargetType == 'class') {
            $('#DivRankTargetDetail').html(loadingImg).load(
                '../ajax/ajax_reload.php',
                {
                    'target': selectedTargetType,
                    'academicYearId': academicYearId,
                    'fieldId': 'TargetID',
                    'fieldName': 'TargetID[]',
                    'CurriculumID': curriculumID
                },
                function(data){
                    $('#selectAllTargetBtn').show();
                    $('#DivSelectAllRemark').show();
                }
            );
        } else if(selectedTargetType == 'student') {
            $('#DivRankTargetDetail').html(loadingImg).load(
                '../ajax/ajax_reload.php',
                {
                    'target': 'student',
                    'academicYearId': academicYearId,
                    'fieldId': 'studentTargetID',
                    'fieldName': 'studentTargetID[]',
                    'onchange': 'getStudentSelection()',
                    'divStudentSelection': 'DivStudentSelection',
                    'CurriculumID': curriculumID
                },
                function(data){
                    $('#selectAllTargetBtn').hide();
                    $('#DivSelectAllRemark').show();
                    getStudentSelection();
                }
            );
        } else {
            $('#DivRankTargetDetail').html('');
            $('#selectAllTargetBtn').hide();
            $('#DivSelectAllRemark').hide();
        }
    }

    function getStudentSelection()
    {
        var selectedYearClassId = [];
        var yearClassObj = document.getElementById('studentTargetID');
        for(var i=0; i<yearClassObj.options.length; i++) {
            if(yearClassObj.options[i].selected) {
                selectedYearClassId.push(yearClassObj.options[i].value);
            }
        }

        $('#DivStudentSelection').html(loadingImg);
        $.post(
            '../ajax/ajax_reload.php',
            {
                'target': 'student2ndLayer',
                'academicYearId': academicYearId,
                'fieldId': 'studentTargetID',
                'fieldName': 'studentTargetID[]',
                'studentFieldName': 'TargetID[]',
                'studentFieldId': 'TargetID',
                'YearClassID[]': selectedYearClassId,
                'curriculumID': $('#curriculumID').val()
            },
            function(data){
                $('#DivStudentSelection').html(data);
            }
        );
    }

    function targetArchiveYearChanged(obj)
    {
        var curriculumID = document.form1.curriculumID.value;
        var selectedYearID = obj.value;

        if(selectedYearID == '')
        {
            $('#DivRankTargetDetail_Year').html('');
        }
        else
        {
            $('#DivRankTargetDetail_Year').html(loadingImg).load(
                '../ajax/ajax_reload.php',
                {
                    'target': 'student',
                    'academicYearId': selectedYearID,
                    'fieldId': 'studentTargetID_Year',
                    'fieldName': 'studentTargetID_Year[]',
                    'onchange': 'getArchiveStudentSelection()',
                    'divStudentSelection': 'TargetID_Year',
                    'CurriculumID': curriculumID
                },
                function(data){
                    $('#selectAllTargetBtn2').hide();
                    $('#DivSelectAllRemark_Year').show();
                    getArchiveStudentSelection();
                }
            );
        }
    }

    function getArchiveStudentSelection()
    {
        var selectedYearClassId = [];
        var yearClassObj = document.getElementById('studentTargetID_Year');
        for(var i=0; i<yearClassObj.options.length; i++) {
            if(yearClassObj.options[i].selected) {
                selectedYearClassId.push(yearClassObj.options[i].value);
            }
        }
        var academicYearId = $('#targetYearID').val();

        $('#TargetID_Year').html(loadingImg).load(
            '../ajax/ajax_reload.php',
            {
                'target': 'student2ndLayer',
                'academicYearId': academicYearId,
                'fieldId': 'studentTargetID_Year',
                'fieldName': 'studentTargetID_Year[]',
                'studentFieldName': 'TargetID_Year[]',
                'studentFieldId': 'TargetID_Year',
                'YearClassID[]': selectedYearClassId,
                'curriculumID': $('#curriculumID').val(),
                'isArchived': 1,
            }
        );
    }

    function changeFindTypeMethod(method)
    {
        if(method == 1) {
            $('tr#selectTargetRow').show();
            $('tr#searchTargetRow').hide();
        }
        else {
            $('tr#selectTargetRow').hide();
            $('tr#searchTargetRow').show();
        }
    }

    function checkForm(formObj)
    {
        var isValid = true;

        var isReportTypeValid = countChecked(document.form1, 'ReportType[]') > 0;
        if(isReportTypeValid) {
            $('#WarnReportType').hide();
        } else {
            $('#WarnReportType').show();
        }
        if(!isReportTypeValid) {
            isValid = false;
        }

        var formStartNum = parseInt($("#TargetFormStart").val());
        var formEndNum = parseInt($("#TargetFormEnd").val());
        if(formStartNum > formEndNum)
        {
            isValid = false;
            $('#WarnClassLevel').show();
            $('#WarnClassLevel').html('<span class="tabletextrequire">* <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange']?></span>');
        }
        else
        {
            $('#WarnClassLevel').hide();

            var selectedCurriculum = $("#curriculumID option:selected").text().toLowerCase();
            if(selectedCurriculum == 'ib' || selectedCurriculum == 'ibdp') {
                if(formEndNum < 9 || formEndNum > 12) {
                    isValid = false;
                    $('#WarnClassLevel').show();
                    $('#WarnClassLevel').html('<span class="tabletextrequire">* <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange']?> <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeIB']?></span>');
                }
            } else if(selectedCurriculum == 'hkdse') {
                if(formStartNum < 7 || formStartNum > 12 || formEndNum < 7 || formEndNum > 12) {
                    isValid = false;
                    $('#WarnClassLevel').show();
                    $('#WarnClassLevel').html('<span class="tabletextrequire">* <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange']?> <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeDBS']?></span>');
                }
            } else {
                if(formStartNum < 7 || formStartNum > 13 || formEndNum < 7 || formEndNum > 13) {
                    isValid = false;
                    $('#WarnClassLevel').show();
                    $('#WarnClassLevel').html('<span class="tabletextrequire">* <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRange']?> <?=$Lang['iPortfolio']['DBSTranscript']['WarningArr']['PleaseInputValidRangeOthers']?></span>');
                }
            }
        }

        var findStudentType = $("input[name='findTarget']:checked").val();
        if(findStudentType == 1)
        {
            var studentType = $("input[name='target_type']:checked").val();
            if(studentType == 1)
            {
                var targetIdObj = $('#TargetID option:selected');
                if(targetIdObj.length == 0){
                    $('#WarnTargetID').show();
                    isValid = false;
                } else {
                    $('#WarnTargetID').hide();
                }
            }
            else
            {
                var targetIdObj = $('#TargetID_Year option:selected');
                if(targetIdObj.length == 0){
                    $('#WarnTargetID_Year').show();
                    isValid = false;
                } else {
                    $('#WarnTargetID_Year').hide();
                }
            }
        }
        else
        {
            checkOptionAll(document.form1.elements["search_by_name[]"]);

            var targetIdObj = $('#search_by_name\\[\\] option:selected');
            if(targetIdObj.length == 0){
                $('#WarnTargetID_Search').show();
                isValid = false;
            } else {
                $('#WarnTargetID_Search').hide();
            }
        }

        if(isValid) {
            document.form1.submit();
        }
    }

    function goSubmit(){
        $('.warnMsgDiv').hide();
        if(checkForm()) {
            form1.submit();
        }
    }

    function checkCR(evt) {
        var evt  = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
    }
    document.onkeypress = checkCR;
</script>

<form name="form1" action="report.php" method="POST" target="_blank">
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
    		<td>
    			<table width="96%" border="0" cellpadding="5" cellspacing="0">
    				<tr align="left">
    					<td>
    						<table class="form_table_v30">
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['ReportType']?>
    								 	<span class="tabletextrequire">*</span>
    								</td>
    								<td>
    									<?=$html["reportTypeSelection"]?>
    									<?=$linterface->Get_Form_Warning_Msg('WarnReportType', $Lang['iPortfolio']['DBSTranscript']['WarningArr']['SelectWarning'].$Lang['iPortfolio']['DBSTranscript']['ReportType'], 'warnMsgDiv', false);?>
    								</td>
    							</tr>
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['Curriculums']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td><?=$html["curriculumSelection"]?></td>
    							</tr>
                                <tr>
                                    <td valign="top" nowrap="nowrap" class="field_title">
                                        <?=$Lang['iPortfolio']['DBSTranscript']['DisplayForm']?> <span class="tabletextrequire">*</span>
                                    </td>
                                    <td>
                                        <?=$html["classLevelSelection"]?>
                                        <?=$linterface->Get_Form_Warning_Msg('WarnClassLevel', '', 'WarnClassLevel', false);?>
                                    </td>
                                </tr>
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title" rowspan="3">
    									<?=$i_general_target?> <span class="tabletextrequire">*</span>
    								</td>
    								<td>
                        				<input type="radio" name="target_type" id="target_type_active" value='1' onclick="changeStudentType(1)" checked/>
                        				<label for="target_type_active"> <?=$iPort['CurrentStudent']?> </label>
                        				<input type="radio" name="target_type" id="target_type_archive" value="0" onclick="changeStudentType(0)"/>
                        				<label for="target_type_archive"> <?=$Lang['Identity']['Alumni']?> </label>
    								</td>
                        		</tr>
                                <tr>
                                    <td>
                                        <input type="radio" name="findTarget" id="findTargetSelect" value='1' onclick="changeStudentType('type')" checked/>
                                        <label for="findTargetSelect"> <?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SelectStudent']?> </label>
                                        <input type="radio" name="findTarget" id="findTargetSearch" value="0" onclick="changeStudentType('type')"/>
                                        <label for="findTargetSearch"> <?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentName']?> </label>
                                    </td>
                                </tr>
                                <tr id="selectTargetRow">
    								<td valign="top">
    									<table border="0">
    										<tr>
    											<td>
													<div id='active_div'>
                    									<table border="0">
                    										<tr>
                    											<td>
                    												<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
                    													<option value="form"><?=$Lang['General']['Form']?></option>
                    													<option value="class"><?=$Lang['General']['Class']?></option>
                    													<option value="student"><?=$Lang['General']['StudentName']?></option>
                    												</select>
                    											</td>
                    											<td>
                    												<div id='DivRankTargetDetail'></div>
                    												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('rankTargetDetail[]')){Select_All_Options('rankTargetDetail[]', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
                    											</td>
                    										</tr>
                    									</table>
                    									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID', false);?>
                    									<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
                									</div>
            										<div id='archive_div' style="display: none">
                    									<table border="0">
                    										<tr>
                    											<td><?=$html["yearSelection"]?></td>
                											</tr>
                    										<tr>
                    											<td>
                    												<div id='DivRankTargetDetail_Year'></div>
                    												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('rankTargetDetail_Year[]')){Select_All_Options('rankTargetDetail_Year[]', true);}return false;","selectAllTargetBtn2",' style="display:none;"')?>
                    											</td>
                    										</tr>
                    									</table>
                    									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID_Year', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID_Year', false);?>
                    									<div class="tabletextremark" id="DivSelectAllRemark_Year" style="display:none;">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
                									</div>
            									</td>
    										</tr>
    									</table>
    								</td>
    							</tr>
                                <tr id="searchTargetRow" style="display: none">
                                    <td valign="top">
                                        <table border="0">
                                            <tr>
                                                <td class="tabletext"><?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['SearchByStudentNameFormat']?><br />
                                                    <div id="activestudentsautocomplete">
                                                        <input type="text" class="tabletext" name="search1" ID="search1">
                                                        <?php foreach((array)$liArr as $this_Id => $_liArr) { ?>
                                                            <div id="studentscontainer_<?=$this_Id?>" style=" left:142px; top:0px;" class="student_container">
                                                                <div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
                                                                    <div style="display: none;" class="yui-ac-hd"></div>
                                                                    <div class="yui-ac-bd"></div>
                                                                    <div style="display: none;" class="yui-ac-ft"></div>
                                                                </div>
                                                                <div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div id="search1_loading_div"></div>
                                                    </div>
                                                    <div id="archivestudentautocomplete" style="display: none">
                                                        <input type="text" class="tabletext" name="search2" ID="search2">
                                                        <?php foreach((array)$liArr2 as $this_Id => $_liArr) { ?>
                                                            <div id="studentscontainerCC_<?=$this_Id?>" style=" left:142px; top:0px;" class="student_container">
                                                                <div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
                                                                    <div style="display: none;" class="yui-ac-hd"></div>
                                                                    <div class="yui-ac-bd"></div>
                                                                    <div style="display: none;" class="yui-ac-ft"></div>
                                                                </div>
                                                                <div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div id="search2_loading_div"></div>
                                                    </div>
                                                </td>
                                                <td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="5" cellspacing="0">
                                                        <tr>
                                                            <td align="left"><?=$linterface->GET_SELECTION_BOX(array(), "name='search_by_name[]' ID='search_by_name[]' class='select_studentlist' size='15' multiple='multiple'", "") ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right"><?=$linterface->GET_BTN($button_remove_selected_student, "button", "javascript:checkOptionRemove(document.form1.elements['search_by_name[]'])")?></td>
                                                        </tr>
                                                    </table>
                                                    <?=$linterface->Get_Form_Warning_Msg('WarnTargetID_Search', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID_Search', false);?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Header']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="headerSelection">
    									<?=$html["headerSelection"]?>
									</td>
    							</tr>
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['BackCover']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="backCoverSelection">
    									<?=$html["backCoverSelection"]?>
									</td>
    							</tr>
    							<tr>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SchoolSeal']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="schoolSealSelection">
    									<?=$html["schoolSealSelection"]?>
									</td>
    							</tr>
    							<tr class='predicted_grade_row'>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeLeftSignature']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="leftSignature2Selection">
    									<?=$html["leftSignature2Selection"]?>
									</td>
    							</tr>
    							<tr class='predicted_grade_row'>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['PredictedGradeRightSignature']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="rightSignature2Selection">
    									<?=$html["rightSignature2Selection"]?>
									</td>
    							</tr>
    							<tr class='transcript_row'>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptLeftSignature']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="leftSignatureSelection">
    									<?=$html["leftSignatureSelection"]?>
									</td>
    							</tr>
    							<tr class='transcript_row'>
    								<td valign="top" nowrap="nowrap" class="field_title">
    									<?=$Lang['iPortfolio']['DBSTranscript']['PrintReport']['TranscriptRightSignature']?> <span class="tabletextrequire">*</span>
    								</td>
    								<td id="rightSignatureSelection">
    									<?=$html["rightSignatureSelection"]?>
									</td>
    							</tr>
    						</table>
    						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
    							<!-- <tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>-->
    							<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
    							<tr>
    								<td align="center">
    									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit()", "submitBtn")?>
    								</td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
	</table>
	
	<?=$html["display_table"]?>
</form>

<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
    var activeStudentsArray = [];
    <?php foreach((array)$liArr as $this_Id => $_liArr) { ?>
        activeStudentsArray[<?=$this_Id?>] = [
            <?=$_liArr?>
        ];
    <?php } ?>

    var archiveStudentsArray = [];
    <?php foreach((array)$liArr2 as $this_Id => $_liArr) { ?>
        archiveStudentsArray[<?=$this_Id?>] = [
            <?=$_liArr?>
        ];
    <?php } ?>

    var delimArray = [
        ";"
    ];
</script>
<!-- In-memory JS array ends-->

<script type="text/javascript">
    YAHOO.example.ACJSArray = function() {
        var oACDS, oAutoComp;
        return {
            init: function() {
                // Instantiate first JS Array DataSour
                var firstCurriculumID = document.form1.curriculumID.value;
                oACDS = new YAHOO.widget.DS_JSArray(activeStudentsArray[parseInt(firstCurriculumID)]);

                // Instantiate first AutoComplete
                oAutoComp = new YAHOO.widget.AutoComplete('search1', 'search_by_name[]', 'studentscontainer_' + parseInt(firstCurriculumID), oACDS);
                oAutoComp.queryDelay = 0;
                oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
                oAutoComp.useShadow = true;
                oAutoComp.minQueryLength = 0;

                oACDS2 = new YAHOO.widget.DS_JSArray(archiveStudentsArray[parseInt(firstCurriculumID)]);
                oAutoComp2 = new YAHOO.widget.AutoComplete('search2', 'search_by_name[]', 'studentscontainerCC_' + parseInt(firstCurriculumID), oACDS2);
                oAutoComp2.queryDelay = 0;
                oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
                oAutoComp2.useShadow = true;
                oAutoComp2.minQueryLength = 0;
            },

            reload: function() {
                // reset
                $(".yui-ac-bd").empty();
                $(".student_container").hide();

                // Instantiate first JS Array DataSour
                var firstCurriculumID = document.form1.curriculumID.value;
                oACDS = new YAHOO.widget.DS_JSArray(activeStudentsArray[parseInt(firstCurriculumID)]);

                // Instantiate first AutoComplete
                oAutoComp = new YAHOO.widget.AutoComplete('search1', 'search_by_name[]', 'studentscontainer_' + parseInt(firstCurriculumID), oACDS);
                oAutoComp.queryDelay = 0;
                oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
                oAutoComp.useShadow = true;
                oAutoComp.minQueryLength = 0;

                $( "#studentscontainer_" + parseInt(firstCurriculumID)).show();

                oACDS2 = new YAHOO.widget.DS_JSArray(archiveStudentsArray[parseInt(firstCurriculumID)]);
                oAutoComp2 = new YAHOO.widget.AutoComplete('search2', 'search_by_name[]', 'studentscontainerCC_' + parseInt(firstCurriculumID), oACDS2);
                oAutoComp2.queryDelay = 0;
                oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
                oAutoComp2.useShadow = true;
                oAutoComp2.minQueryLength = 0;

                $( "#studentscontainerCC_" + parseInt(firstCurriculumID)).show();

                // add loading logic
                document.getElementById("search1").disabled = true;
                document.getElementById("search2").disabled = true;
                $('#search1_loading_div').html(loadingImg);
                $('#search2_loading_div').html(loadingImg);
                setTimeout(function(){
                    document.getElementById("search1").disabled = false;
                    document.getElementById("search2").disabled = false;
                    $('#search1_loading_div').html('');
                    $('#search2_loading_div').html('');
                }, 1000);

            },

            validateForm: function() {
                // Validate form inputs here
                return false;
            }
        };
    }();

    YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
    YAHOO.util.Event.addListener(this.form1.curriculumID,'click',YAHOO.example.ACJSArray.reload);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
    dp.SyntaxHighlighter.HighlightAll('code');
</script>