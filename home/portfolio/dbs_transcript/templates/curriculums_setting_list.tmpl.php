<form name="form1" action="" method="POST">
	<?=$html["navigation"]?>
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td nowrap>
				<a href="javascript:void(0);" onclick="goNew()" class="contenttool">
					<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_new?>
				</a>
			</td>
		</tr>
		<tr>
			<td valign="bottom">
				<div class="common_table_tool">
                    <a class="tool_approve" href="javascript:void(0);" onClick="checkEditMultiple(document.form1, 'curriculumIds[]', 'activate.php',0);"><?=$iPort['activate']?></a>
                    <a class="tool_reject" href="javascript:void(0);" onClick="checkEditMultiple(document.form1, 'curriculumIds[]', 'deactivate.php',0);"><?=$iPort['deactivate']?></a>
                    <a class="tool_edit" href="javascript:void(0);" onClick="checkEdit(document.form1, 'curriculumIds[]', 'edit.php');"><?=$button_edit?></a>
                    <a class="tool_delete" href="javascript:void(0);" onClick="checkRemove(document.form1,'curriculumIds[]','delete.php');"><?=$button_delete?></a>
				</div> 
			</td>
		</tr>
	</table>
	
	<?=$html["display_table"]?>
</form>

<script type="text/javascript" language="JavaScript">
function goNew(){
	document.form1.isNew.value = 1;
	document.form1.action = 'edit.php';
	document.form1.submit();
}
</script>