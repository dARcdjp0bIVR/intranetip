<form name="form1" action="" method="POST">
	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td nowrap>
				<?php if($isPortfolioSuperAdmin) { ?>
    				<a href="javascript:void(0);" onclick="goImport()" class="contenttool">
    					<img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?>
    				</a>
				<?php } ?>
			</td>
		</tr>
		<tr>
    		<td>
				<table class="form_table_v30">
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DBSTranscript']['Curriculums']?></td>
						<td><?=$html["curriculumSelection"]?></td> 
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$ec_iPortfolio["form"]?></td>
						<td id='formSelection'><?=$html["formSelection"]?></td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$ec_iPortfolio["class"]?></td>
						<td id='classSelection'><?=$html["classSelection"]?></td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$iPort["Subject"]?></td>
						<td><?=$html["subjectSelection"]?></td> 
					</tr>
					<?php if(!$isIBTranscriptReport && $isSubjectTeacher) { ?>
						<?php if($html["adjustmentStartDate"] != '') { ?>
        					<tr>
        						<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentStartDate']?></td>
        						<td id='startDateDisplay'><?=$html["adjustmentStartDate"]?> (<?= ($isAllowEditPeriod1? $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1'] : $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2']) ?>)</td>
        					</tr>
    					<?php } ?>
    					<?php if($html["adjustmentEndDate"] != '') { ?>
        					<tr>
        						<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentEndDate']?></td>
        						<td id='endDateDisplay'><?=$html["adjustmentEndDate"]?> (<?= ($isAllowEditPeriod1? $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment1'] : $Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Amendment2']) ?>)</td>
        					</tr>
						<?php } ?>
					<?php } ?>
				</table>
    		</td>
    	</tr>
	</table>
	
	<div id='table_content'><?= $html["display_table_fields"] ?></div>
	<br/>
	
	<?php if (!$isIBTranscriptReport && $isSubjectTeacher) { ?>
		<div id='saveDraftRemarksDiv'><?= $Lang['iPortfolio']['DBSTranscript']['RemarkArr']['GradeSaveAsDraft'] ?></div>
	<?php } ?>
	<?php if ($lastModifiedDisplay != '') { ?>
		<div class='tabletextremark'><?= $lastModifiedDisplay ?></div>
	<?php } ?>
	<input type="hidden" value="0" id="saveAsDraft" name="saveAsDraft">
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php if($isPortfolioSuperAdmin) { ?>
		<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<?php } else if (!$isIBTranscriptReport && $isSubjectTeacher) { ?>
		<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
		<input type="button" value="<?=	$button_save_as_draft?>" class="formbutton_v30 print_hide " onclick="goSaveDraft()" id="saveDraftBtn" name="saveDraftBtn">
	<?php } ?>
	<p class="spacer"></p>
</div>

<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

$(document).ready(function() {
	<?php if(!$curriculumID) { ?>
		document.form1.submit();
	<?php } else { ?>
		//if($("input[name^='canSaveDraft\\\[']").length == 0) {
		if($("input.save_draft_inputs").length == 0) {
			if($("#saveDraftBtn")) {
				$("#saveDraftBtn").attr("disabled", true);
				$("#saveDraftBtn").removeClass("formbutton_v30").addClass("formbutton_disable_v30");
			}
			if($("#saveDraftRemarksDiv")) {
				$("#saveDraftRemarksDiv").hide();
			}
		}
		//if($("input[name^='predict\\\[']").length == 0) {
		if($("select.predict_inputs").length == 0) {
			if($("#submitBtn")) {
				$("#submitBtn").hide();
			}
			if($("#saveDraftBtn")) {
				$("#saveDraftBtn").hide();
			}
		}
	<?php } ?>
});

function goSaveDraft(){
	$('#saveAsDraft').val(1);
	goSubmit();
}

function goSubmit(){
	//$('.warnMsgDiv').hide();
	//if(checkForm()) {
	//}
	document.form1.action = 'update.php';
	document.form1.submit();
}

function goImport(){
	document.form1.action = 'import.php';
	document.form1.submit();
}
</script>