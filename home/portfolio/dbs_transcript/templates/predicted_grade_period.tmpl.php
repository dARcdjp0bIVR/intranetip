<form name="form1" action="" method="POST">
	<br />
	
	<div id='table_content'><?= $htmlAry["PeriodTable"]?></div>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<p class="spacer"></p>
</div>

<script type="text/javascript" language="JavaScript">

$(document).ready(function() {
	
});

function goSubmit() {
	var periodArr = [];
	for (var i=1; i<=2; i++)
	{
		var period_start = document.getElementById('start_date_' + i);
		var period_end = document.getElementById('end_date_' + i);
		
		// Checking for start date
		if (typeof(period_start) != "undefined" && period_start.value != "")
		{
			if(!check_date(period_start, "<?php echo $assignments_alert_msg9; ?>")) {
				period_start.focus();
				return false;
			}
			isst = true;
		}
		
		// Checking for end date
		if (typeof(period_end) != "undefined" && period_end.value!="")
		{
			if(!check_date(period_end, "<?php echo $assignments_alert_msg9; ?>")) {
				period_end.focus();
				return false;
			}
			
			if (isst)
			{
				// Checking for end date < start date
				if(compareDate(period_start.value, period_end.value) == 1) {
					period_start.focus();
					alert("<?= $w_alert['start_end_time2'] ?>");
					return false;
				}
			}
		}
		
		periodArr[i] = [];
		periodArr[i]['start'] = period_start.value;
		periodArr[i]['end'] = period_end.value;
	}

	// Checking for 2 period range
	if((periodArr[1]['start'] >= periodArr[2]['start'] && periodArr[1]['end'] <= periodArr[2]['end']) || (periodArr[2]['start'] >= periodArr[1]['start'] && periodArr[2]['end'] <= periodArr[1]['end']) ||
		(periodArr[1]['start'] <= periodArr[2]['start'] && periodArr[1]['end'] >= periodArr[2]['start']) || (periodArr[2]['start'] <= periodArr[1]['start'] && periodArr[2]['end'] >= periodArr[1]['start']) || 
		(periodArr[1]['start'] <= periodArr[2]['end'] && periodArr[1]['end'] >= periodArr[2]['end']) || (periodArr[2]['start'] <= periodArr[1]['end'] && periodArr[2]['end'] >= periodArr[1]['end']))
	{
		document.getElementById('start_date_1').focus();
		alert("<?= $Lang['iPortfolio']['DBSTranscript']['WarningArr']['PeriodCannotOverlap']?>");
		return false;
	}
	
	document.form1.action = 'period_update.php';
	document.form1.submit();
}
</script>