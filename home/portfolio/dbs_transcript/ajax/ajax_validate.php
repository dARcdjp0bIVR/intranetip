<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lpf_dbs = new libpf_dbs_transcript();

$returnString = '';
$type = trim($_REQUEST['type']);
if ($type == "checkCurriculums")
{
    $CurriculumID = trim($_REQUEST['CurriculumID']);
    $Code = trim($_REQUEST['Code']);
    $NameEn = trim($_REQUEST['NameEn']);
    $NameCh = trim($_REQUEST['NameCh']);
    
    $duplicateCodeAry = $lpf_dbs->getCurriculumSetting('', $Code, '', '', '', false, $CurriculumID);
    $duplicateNameEnAry = $lpf_dbs->getCurriculumSetting('', '', $NameEn, '', '', false, $CurriculumID);
    $duplicateNameChAry = $lpf_dbs->getCurriculumSetting('', '', '', $NameCh, '', false, $CurriculumID);
    if(!empty($duplicateCodeAry)) {
        $returnString = 'Code';
    } else if(!empty($duplicateNameEnAry)) {
        $returnString = 'NameEn';
    } else if(!empty($duplicateNameChAry)) {
        $returnString = 'NameCh';
    }
}
else if ($type == "checkImages")
{
    $ImageID = trim($_REQUEST['ImageID']);
    $ImageType = $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"][trim($_REQUEST['ImageType'])];
    $CurriculumID = trim($_REQUEST['CurriculumID']);
    $ReportTypeID = trim($_REQUEST['ReportTypeID']);
    $Description = trim($_REQUEST['Description']);
    $SignatureName = trim($_REQUEST['SignatureName']);
    
    if($Description != '') {
        $duplicateDescriptionAry = $lpf_dbs->getReportImage($ImageType, '', $CurriculumID, '', $Description, '', '', false, $ImageID);
    }
    if($SignatureName != '') {
        $duplicateSignatureNameAry = $lpf_dbs->getReportImage($ImageType, '', $CurriculumID, $ReportTypeID, '', $SignatureName, '', false, $ImageID);
    }
    if(!empty($duplicateDescriptionAry)) {
        $returnString = 'Description';
    } else if(!empty($duplicateSignatureNameAry)) {
        $returnString = 'SignatureName';
    }
}

echo $returnString;
?>