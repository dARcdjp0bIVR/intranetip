<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$lu = new libuser();
$fcm = new form_class_manage();
$scm = new subject_class_mapping();
$subject = new subject();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

// Get Curriculum
$Curriculum = $lpf_dbs->getCurriculumSetting($curriculumID, '', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$CurriculumCode = $Curriculum[0]['Code'];

// Get CSV data from temp table
if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP")
{
    $sql = "SELECT * FROM ".$eclass_db.".temp_dbs_predicted_grade_min_max WHERE TempImportUserID = '". $_SESSION["UserID"]."'";
    $data = $lpf->returnResultSet($sql);
}
else
{
    $sql = "SELECT * FROM ".$eclass_db.".temp_dbs_predicted_grade WHERE TempImportUserID = '". $_SESSION["UserID"]."'";
    $data = $lpf->returnResultSet($sql);
}

//$error_occured = 0;
$successCount = 0;
for($i=0; $i<sizeof($data); $i++)
{
    ### Save Data
    unset($data[$i]['TempImportUserID']);
    if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP") {
        $result = $lpf_dbs->insertUpdateStudentAdjustMinMax($data[$i], $id='', $data[$i]['StudentID'], $data[$i]['SubjectID']);
    } else {
        $result = $lpf_dbs->insertUpdateStudentAdjustment($data[$i], $id='', $data[$i]['StudentID'], $data[$i]['SubjectID'], $data[$i]['PredictPhase']);
    }
    $successCount += $result? 1 : 0;
}

### Page ###
$CurrentPage = "Teacher_Transcript_PredictedGrade";

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getPredictedGradeTab('index');
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Import Step ###
$ImportStep = 3;
$ImportTargetUrl = 'index.php';

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, $ImportStep==1? 1 : 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, $ImportStep==2? 1 : 0);
$STEPS_OBJ[] = array($i_general_complete, $ImportStep==3? 1 : 0);

$linterface->LAYOUT_START();

include_once("../templates/predicted_grade_import.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>