<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

// Get DSE ID
$DSECurriculum = $lpf_dbs->getCurriculumSetting('', $code='HKDSE', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$DSECurriculumID = $DSECurriculum[0]['CurriculumID'];

// Get IB ID
$IBCurriculum = $lpf_dbs->getCurriculumSetting('', $code=array('IB', 'IBDP'), '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$IBCurriculumID = $IBCurriculum[0]['CurriculumID'];

// Curriculum Selection
$curriculumID = $_POST['curriculumID']? $_POST['curriculumID'] : $_GET['curriculumID'];
$html["curriculumSelection"] = $lpf_dbs_ui->getCurriculumSelection('curriculumID', $curriculumID, $onchange='updatedCurriculum(this.value)', $noFirst=1, $firstTitleText='', $useCode=true, $forPredictedGrade=true);

### Page ###
$CurrentPage = "Teacher_Transcript_PredictedGrade";

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getPredictedGradeTab('index');
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Import Step ###
$ImportStep = 1;
$ImportTargetUrl = 'import_result.php';

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, $ImportStep==1? 1 : 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, $ImportStep==2? 1 : 0);
$STEPS_OBJ[] = array($i_general_complete, $ImportStep==3? 1 : 0);

$linterface->LAYOUT_START();

include_once("../templates/predicted_grade_import.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>