<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

//$error_occured = 0;
$returnAry = array();
$start_period = $_POST['start_date'];
$end_period = $_POST['end_date'];
foreach($start_period as $predict_phase => $phase_start)
{
    $phase_end = $end_period[$predict_phase];
        
    ### Save Data
    if ($phase_start != "" && $phase_end != "") {
        $valid_data = array();
        $valid_data['PeriodStart'] = $phase_start;
        $valid_data['PeriodEnd'] = $phase_end;
        $valid_data['PredictPhase'] = $predict_phase;
        $returnAry[] = $lpf_dbs->insertUpdateAdjustmentPeriodSettings($valid_data, $predict_phase);
    }
}

$msg = !in_array(false, $returnAry)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: period.php?msg='.$msg);
?>