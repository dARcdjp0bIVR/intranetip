<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

iportfolio_auth("T");
intranet_opendb();

$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

// Get Curriculum
$Curriculum = $lpf_dbs->getCurriculumSetting($_POST['curriculumID'], '', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$CurriculumCode = $Curriculum[0]['Code'];

//$error_occured = 0;
$returnAry = array();
$data = $_POST['predict'];
foreach($data as $predict_phase => $predict_data)
{
    foreach($predict_data as $student_user_id => $predict_grade)
    {
        $valid_data = array();
        $valid_data['StudentID'] = $student_user_id;
        $valid_data['SubjectID'] = $_POST['subjectID'];
        //$valid_data['SubjectGroupID'] = $subject_group_id;
        if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP") {
            $valid_data['PredictedGrade'] = $predict_grade;
        }
        else {
            $valid_data['PredictedGrade'] = $predict_grade;
            $valid_data['PredictPhase'] = $predict_phase;
            $valid_data['PredictType'] = $_POST['saveAsDraft'];
        }
        
        ### Save Data
        if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP") {
            $returnAry[] = $lpf_dbs->insertUpdateStudentAdjustMinMax($valid_data, $id='', $valid_data['StudentID'], $valid_data['SubjectID']);
        } else {
            $returnAry[] = $lpf_dbs->insertUpdateStudentAdjustment($valid_data, $id='', $valid_data['StudentID'], $valid_data['SubjectID'], $valid_data['PredictPhase'], $valid_data['PredictType']);
        }
    }
}
$msg = !in_array(false, $returnAry)? 'UpdateSuccess' : 'UpdateUnsuccess';

$parms .= 'curriculumID='.$_POST['curriculumID'].'&formID='.$_POST['formID'].'&subjectID='.$_POST['subjectID'];
header('Location: index.php?'.$parms.'&msg='.$msg);
?>