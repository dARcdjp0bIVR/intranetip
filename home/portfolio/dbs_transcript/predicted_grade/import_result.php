<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-10-15 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

iportfolio_auth("T");
intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$linterface = new interface_html();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$lu = new libuser();
$fcm = new form_class_manage();
$scm = new subject_class_mapping();
$subject = new subject();

// Check if Super Admin
$isPortfolioSuperAdmin = $lpf->IS_IPF_SUPERADMIN();

// Check file type
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if($ext != ".CSV" && $ext != ".TXT")
{
    header("location: import.php?curriculumID=$curriculumID&xmsg=import_failed");
    exit();
}

// Get Curriculum
$Curriculum = $lpf_dbs->getCurriculumSetting($curriculumID, '', '', '', $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
$CurriculumCode = $Curriculum[0]['Code'];

// Get CSV Header
$file_format = $ipf_cfg["DSBTranscript"]["PREDICT_GRADE_IMPORT"][$CurriculumCode];
if(empty($file_format)) {
    header("location: import.php?curriculumID=$curriculumID&xmsg=import_failed");
    exit();
}

// Get Import Data
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data)) {
    $col_name = array_shift($data);
}

// Check CSV Header Format
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
    if ($col_name[$i] != $file_format[$i]) {
        $format_wrong = true;
        break;
    }
}
if($format_wrong)
{
    header("location: import.php?curriculumID=$curriculumID&$xmsg=wrong_header");
    exit();
}

// Get Subject Code
$allSubjectCodeList = $subject->Get_Subject_List();
$allSubjectCodeList = BuildMultiKeyAssoc($allSubjectCodeList, 'CODEID', 'RecordID', 1, 0);

// Get Curriculum related classes
$allCurriculumClassList = $lpf_dbs->getCurriculumClassList($curriculumID);
$allCurriculumClassList = Get_Array_By_Key($allCurriculumClassList, 'YearClassID');

// Get Class Student
$allCurriculumStudentList = $fcm->Get_Student_By_Class($allCurriculumClassList);
$allCurriculumStudentList = Get_Array_By_Key($allCurriculumStudentList, 'UserID');

if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP")
{
    $UserLoginColIndex = 2;
    $SubjectCodeColIndex = 0;
}
else
{
    $UserLoginColIndex = 0;
    $SubjectCodeColIndex = 4;
}

//$error_occured = 0;
$valid_data_ary = array();
$previous_data_ary = array();
$error_data_ary = array();
$error_indicate_ary = array();
for($i=0; $i<sizeof($data); $i++)
{
    $isErrorData = false;
    $errorRowIndex = sizeof($error_data_ary);
    
    ### Get Data
    $student_userlogin = $data[$i][$UserLoginColIndex];
    $subject_code = $data[$i][$SubjectCodeColIndex];
    
    // Check empty data (both UserLogin and SubjectCode)
    $student_userlogin = trim($student_userlogin);
    $subject_code = trim($subject_code);
    if($student_userlogin == '' || $subject_code == '') {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        continue;
    }
    
    // Check valid student
    $student_user_id = $previous_data_ary['user_id'][$student_userlogin];
    $student_user_id = $student_user_id? $student_user_id : $lu->getUserIDByUserLogin($student_userlogin);
    if($student_user_id == '') {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        $error_indicate_ary[$errorRowIndex][$UserLoginColIndex] = true;
        continue;
    } else {
        $previous_data_ary['user_id'][$student_userlogin] = $student_user_id;
    }
    
    // Check Student Curriculum
    if(!in_array($student_user_id, $allCurriculumStudentList)) {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        $error_indicate_ary[$errorRowIndex][$UserLoginColIndex] = true;
        continue;
    }
    
    // Check student study any subjects
    $subject_id_ary = $previous_data_ary['subject_id'][$student_user_id];
    $subject_group_id_ary = $previous_data_ary['subject_group_id'][$student_user_id];
    if(empty($subject_id_ary)) {
        $subject_groups = $scm->getSubjectGroupIDByStudentID($student_user_id);
        $subject_id_ary = array_keys((array)$subject_groups);
        $previous_data_ary['subject_id'][$student_user_id] = $subject_id_ary;
        
        $subject_group_id_ary = $subject_groups;
        $previous_data_ary['subject_group_id'][$student_user_id] = $subject_group_id_ary;
    }
    if(empty($subject_id_ary)) {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        $error_indicate_ary[$errorRowIndex][$UserLoginColIndex] = true;
        continue;
    }
    
    // Check valid subject
    $subject_id = $allSubjectCodeList[$subject_code];
    if($subject_id == '') {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        $error_indicate_ary[$errorRowIndex][$SubjectCodeColIndex] = true;
        continue;
    }
    
    // Check student study this subject
    if(!in_array($subject_id, $subject_id_ary)) {
        array_unshift($data[$i], ($i + 1));
        $error_data_ary[] = $data[$i];
        $error_indicate_ary[$errorRowIndex][$SubjectCodeColIndex] = true;
        continue;
    }
    $subject_group_id = $subject_group_id_ary[$subject_id];
    
    $valid_data = array();
    $valid_data['StudentID'] = $student_user_id;
    $valid_data['SubjectID'] = $subject_id;
    $valid_data['SubjectGroupID'] = $subject_group_id;
    if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP") {
        $valid_data['PredictedGradeMax'] = $data[$i][4];
        $valid_data['PredictedGradeMin'] = $data[$i][5];
    }
    else {
        $valid_data['PredictedGrade'] = $data[$i][10];
        $valid_data['PredictPhase'] = $importPhase;
    }
    $valid_data_ary[] = $valid_data;
}

$valid_data_count = count($valid_data_ary);
if($valid_data_count > 0)
{
    if ($CurriculumCode == "IB" || $CurriculumCode == "IBDP")
    {
        $sql = "CREATE TABLE IF NOT EXISTS ".$eclass_db.".temp_dbs_predicted_grade_min_max
            	(
            		 StudentID int(11),
            		 SubjectID int(11),
            		 SubjectGroupID int(11),
            		 PredictedGradeMin varchar(100),
            		 PredictedGradeMax varchar(100),
                     TempImportUserID int(11)
            	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $lpf->db_db_query($sql) or die(mysql_error());
        
        $sql = "DELETE FROM ".$eclass_db.".temp_dbs_predicted_grade_min_max where TempImportUserID = ".$_SESSION["UserID"];
        $lpf->db_db_query($sql) or die(mysql_error());
        
        $insertSql = array();
        foreach($valid_data_ary as $this_data) {
            $insertSql[] = " ('".$this_data['StudentID']."', '".$this_data['SubjectID']."', '".$this_data['SubjectGroupID']."', '".$this_data['PredictedGradeMax']."', '".$this_data['PredictedGradeMin']."', '".$_SESSION["UserID"]."') ";
        }
        if(count($insertSql) > 0)
        {
            $sql = "INSERT INTO ".$eclass_db.".temp_dbs_predicted_grade_min_max
                        (StudentID, SubjectID, SubjectGroupID, PredictedGradeMin, PredictedGradeMax, TempImportUserID) 
                    VALUES
                        ".implode(", ", $insertSql)."";
            $lpf->db_db_query($sql);
        }
    }
    else
    {
        $sql = "CREATE TABLE IF NOT EXISTS ".$eclass_db.".temp_dbs_predicted_grade
            	(
            		 StudentID int(11),
            		 SubjectID int(11),
            		 SubjectGroupID int(11),
            		 PredictedGrade varchar(100),
                     PredictPhase int(8), 
                     TempImportUserID int(11)
            	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $lpf->db_db_query($sql) or die(mysql_error());
        
        $sql = "DELETE FROM ".$eclass_db.".temp_dbs_predicted_grade where TempImportUserID = ".$_SESSION["UserID"];
        $lpf->db_db_query($sql) or die(mysql_error());
        
        $insertSql = array();
        foreach($valid_data_ary as $this_data) {
            $insertSql[] = " ('".$this_data['StudentID']."', '".$this_data['SubjectID']."', '".$this_data['SubjectGroupID']."', '".$this_data['PredictedGrade']."', '".$this_data['PredictPhase']."', '".$_SESSION["UserID"]."') ";
        }
        if(count($insertSql) > 0)
        {
            $sql = "INSERT INTO ".$eclass_db.".temp_dbs_predicted_grade
                        (StudentID, SubjectID, SubjectGroupID, PredictedGrade, PredictPhase, TempImportUserID)
                    VALUES
                        ".implode(", ", $insertSql)."";
            $lpf->db_db_query($sql);
        }
    }
    
    $canSubmit = true;
}

$error_data_count = count($error_data_ary);
if($error_data_count > 0)
{
    $x = "";
    $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"common_table_list_v30 view_table_list_v30\">";
    $x .= "<tr>";
        $x .= "<th class=\"field_title\">".$Lang['General']['ImportArr']['Row']	."</td>";
        for($i=0; $i<sizeof($file_format); $i++) {
            $x .= "<th class=\"field_title\">".$file_format[$i]."</td>";
        }
    $x .= "</tr>";
    
    foreach($error_data_ary as $this_data_count => $this_error_data)
    {
        $x .= "<tr class=\"tablebluerow".($this_data_count % 2 + 1)."\">";
        foreach($this_error_data as $this_data_index => $this_data) {
            $needIndicator = $error_indicate_ary[$this_data_count][($this_data_index - 1)];
            $css = $needIndicator? "red" : "tabletext";
            
            $x .= "<td class=\"$css\">".$this_data."</td>";
        }
        $x .= "</tr>";
    }
    $x .= "</table>";
}
$htmlAry["ErrorTable"] = $x;

### Page ###
$CurrentPage = "Teacher_Transcript_PredictedGrade";

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getPredictedGradeTab('index');
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

### Import Step ###
$ImportStep = 2;
$ImportTargetUrl = 'import_update.php';

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, $ImportStep==1? 1 : 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, $ImportStep==2? 1 : 0);
$STEPS_OBJ[] = array($i_general_complete, $ImportStep==3? 1 : 0);

$linterface->LAYOUT_START();

include_once("../templates/predicted_grade_import.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>