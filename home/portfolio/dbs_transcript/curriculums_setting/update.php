<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$dataAry = array();
$dataAry['Code'] = $_POST['Code'];
$dataAry['NameEn'] = $_POST['NameEn'];
$dataAry['NameCh'] = $_POST['NameCh'];
$dataAry['Status'] = $_POST['status'];
$dataAry['SubjectLevel'] = preg_replace("/[\r\n]+/", "\n", $_POST['SubjectLevel']);
$dataAry['SubjectLevel'] = implode("\n",array_unique(array_filter(Array_Trim(explode("\n",$dataAry['SubjectLevel'])))));

$lpf_dbs = new libpf_dbs_transcript();
$result = $lpf_dbs->insertUpdateCurriculumSetting($dataAry, $_POST['CurriculumID']);

$parms = 'pageNo='.$_POST['pageNo'].'&order='.$_POST['order'].'&field='.$_POST['field'].'&page_size_change='.$_POST['page_size_change'].'&numPerPage='.$_POST['numPerPage'];

$msg = $result? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: index.php?'.$parms.'&msg='.$msg);
?>