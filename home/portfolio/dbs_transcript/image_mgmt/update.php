<?php
// Modifying By:
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

iportfolio_auth("T");
intranet_opendb();

$lfs = new libfilesystem();
$lpf_dbs = new libpf_dbs_transcript();

$dataAry = array();
$dataAry['ImageID'] = $_POST['ImageID'];
$dataAry['ImageType'] = $ipf_cfg["DSBTranscript"]["IMAGE_TYPE"][$_POST['ImageType']];
$dataAry['CurriculumID'] = $_POST['CurriculumID'];
$dataAry['ReportTypeID'] = $_POST['ReportTypeID'];
$dataAry['Description'] = $_POST['Description'];
$dataAry['SignatureName'] = $_POST['SignatureName'];
$dataAry['SignatureDetails'] = $_POST['SignatureDetails'];
$dataAry['Status'] = $ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"];
$result = $lpf_dbs->insertUpdateImageSetting($dataAry, $_POST['ImageID']);

$imageID = $_POST['ImageID']? $_POST['ImageID'] : $result;
$ImageLink = $_POST["ImageLink"];

$target_dir = $PATH_WRT_ROOT.$ipf_cfg["DSBTranscript"]["IMAGE_DIRECTORY"];
if($_FILES["fileToUpload"]["tmp_name"]) {
    $lfs->folder_new($target_dir);
    
    ### Delete Photo
    if($ImageLink != '') {
        $target_file = $target_dir.$ImageLink;
        $lfs->file_remove($target_file);
    }
    
    $file_ext = $lfs->file_ext($_FILES["fileToUpload"]["name"]);
    $ImageLink = basename($imageID).$file_ext;
}

if($_FILES["fileToUpload"]["tmp_name"] && $imageID > 0)
{
    $dataAry = array();
    $dataAry['ImageLink'] = $ImageLink;
    $lpf_dbs->insertUpdateImageSetting($dataAry, $imageID);
    
    $max_width = 300;
    $max_height = 300;
    
    ### File [Start]
    $uploadOk = 1;
    $target_file = $target_dir.basename($imageID).$file_ext;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $UploadError[] = "File is not an image.";
        $uploadOk = 0;
    }
    
    // Define as update process and delete the orginal one
    if (file_exists($target_file)) {
        $indexVar["libfilesystem"]->file_remove($target_file);
        // unlink($target_file);
    }
    
    // Check file size
//     if ($_FILES["fileToUpload"]["size"] > 500000) {		// if oversize => resize the image size
//         if($imageFileType=="jpg") {
//             $image = imagecreatefromjpeg($_FILES["fileToUpload"]["tmp_name"]);
//         }
//         $old_width  = imagesx($image);
//         $old_height = imagesy($image);
//         $scale      = min($max_width/$old_width, $max_height/$old_height);
//         $new_width  = ceil($scale*$old_width);
//         $new_height = ceil($scale*$old_height);
        
//         // Create new empty image
//         $new = imagecreatetruecolor($new_width, $new_height);
        
//         // Keep the transparent background for png
//         imagealphablending($new, false);
//         imagesavealpha($new, true);
//         $transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
//         imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);
        
//         // Resize old image into new
//         imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
        
//         // Catch the imagedata
//         imagepng($new, $_FILES["fileToUpload"]["tmp_name"]); //update the fileToupload with the resize image
        
//         // Destroy resources
//         imagedestroy($image);
//         imagedestroy($new);
//     }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        $UploadError[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $UploadError[] = "Sorry, your file was not uploaded.";
    }
    // if everything is ok, try to upload file
    else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        }
        else{
            // $uploadOk = 0;
            $UploadError[] = "Sorry, there was an error uploading your file.";
        }
    }
    ### File [End]
}

$parms = 'pageNo='.$_POST['pageNo'].'&order='.$_POST['order'].'&field='.$_POST['field'].'&page_size_change='.$_POST['page_size_change'].'&numPerPage='.$_POST['numPerPage'];
$parms .= '&curriculumID='.$_POST['CurriculumID'].'&reportTypeID='.$_POST['ReportTypeID'];

$msg = $result? 'UpdateSuccess' : 'UpdateUnsuccess';

$target_page = $_POST['ImageType'].'.php';
header('Location: '.$target_page.'?'.$parms.'&msg='.$msg);
?>