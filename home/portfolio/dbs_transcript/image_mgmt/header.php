<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2018-08-16 (Bill)
 *      - Create file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$image_type = 'header';
$curriculumID = isset($_POST['curriculumID'])? $_POST['curriculumID'] : $_GET['curriculumID'];

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();

$html["selection"] = $lpf_dbs_ui->getCurriculumSelection('curriculumID', $curriculumID, $onchange='document.form1.submit()', $noFirst=0, $firstTitleText=$Lang['General']['All'], $useCode=true);

// Table settings
$field = isset($_POST["field"])? $_POST["field"] : $_GET["field"];
$order = isset($_POST["order"])? $_POST["order"] : (isset($_GET["order"])? $_GET["order"] : 1);
$pageNo = isset($_POST["pageNo"])? $_POST["pageNo"] : $_GET["pageNo"];
$page_size_change = isset($_POST["page_size_change"])? $_POST["page_size_change"] : $_GET["page_size_change"];
$numPerPage = !empty($_POST["numPerPage"])? $_POST["numPerPage"] : (!empty($_GET["numPerPage"])? $_GET["numPerPage"] : $page_size);

// Table setups
$pfTable = new libpf_dbtable($field, $order, $pageNo);
$pfTable->page_size = $numPerPage;

// Table headers
$columnCount = 0;
$pfTable->column_list .= "<th width='2%'>#</th>\n";
$pfTable->column_list .= "<th width='10%'>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['Curriculums'])."</th>\n";
$pfTable->column_list .= "<th width='26%'>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Description'])."</th>\n";
$pfTable->column_list .= "<th width='60%'>".$pfTable->column($columnCount++, $Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Image'])."</th>\n";
$pfTable->column_list .= "<th width='2%'>".$pfTable->check("imageIds[]")."</th>\n";

// Table controls
$pfTable->IsColOff = "IP25_table";
$pfTable->field_array = array("Code", "Description", "ImageLink");
$pfTable->no_col = 1/* row number */+count($pfTable->field_array)+1/* checkbox */;
$pfTable->column_array = array(0,0,0,0,0);

$pfTable->sql = $lpf_dbs->getReportImage($ipf_cfg["DSBTranscript"]["IMAGE_TYPE"][$image_type], '', $curriculumID, $reportTypeID='', $description='', $signatureName='', $status='', $returnSQL=true);

// Get table
$displayTableHtml = $pfTable->display();
$table_hidden = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$pfTable->pageNo}">
<input type="hidden" name="order" value="{$pfTable->order}">
<input type="hidden" name="field" value="{$pfTable->field}">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="{$pfTable->page_size}">
<input type="hidden" name="imageType" value="$image_type">
<input type="hidden" name="isNew" value="0">
HTMLEND;
$displayTableHtml .= $table_hidden;
###############################################

###############################################
###	HTML - display table
$html["display_table"] = $displayTableHtml;

### Page ###
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_ImageManagement";
$CurrentPageName = $Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title'];

### Title ###
$TAGS_OBJ = $lpf_dbs_ui->getImageMmgtTab($image_type);
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");

######## Display #########
// $PAGE_NAVIGATION[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title']);
// $html["navigation"] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/image.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>