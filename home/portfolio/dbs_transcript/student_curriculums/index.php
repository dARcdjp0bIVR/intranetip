<?php
// Modified by 

/********************** Change Log ***********************/
#
/********************** Change Log ***********************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-formclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-account-teacher.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-dbtable.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript_ui.php");

iportfolio_auth("T");
intranet_opendb();

$lpf = new libportfolio();
$lpf_dbs = new libpf_dbs_transcript();
$lpf_dbs_ui = new libpf_dbs_transcript_ui();
$lfcm_ui = new form_class_manage_ui();

$isAlumniAccess = $_POST['isAlumni']? $_POST['isAlumni'] : $_GET['isAlumni'];

$targetYearID = $targetYearID? $targetYearID : Get_Current_Academic_Year_ID();
$html["yearSelection"] = $lpf_dbs_ui->getAcademicYearClassHistorySelection('targetYearID', $targetYearID, $onChange='jCHANGE_FIELD()', $noFirst=0, $firstTitleText='');

$sql = "SELECT
            y.YearID, y.YearName
        FROM
            YEAR_CLASS yc
            INNER JOIN YEAR y ON (y.YearID = yc.YearID)
        WHERE
            yc.AcademicYearID = '".$targetYearID."'
        GROUP BY
            y.YearID
        ORDER BY
            y.Sequence, yc.Sequence";
$result = $lpf->returnArray($sql,2);
$htmlAry["formSelection"] = getSelectByArray($result, ' id="YearID" name="YearID" onChange="jCHANGE_FIELD()" ', $YearID, 0, 0, $ec_iPortfolio['all_classlevel']);

/*
IF(
    (
        COUNT(DISTINCT sc.StudentID) = 0 OR
        COUNT(DISTINCT ycu.YearClassUserID) > COUNT(DISTINCT sc.StudentID)
        )
    AND yc.CurriculumID = '1',
    COUNT(ycu.YearClassUserID),
    SUM(IF(sc.CurriculumID = '1', 1, 0))
    )
*/
$fieldAry = array();
$fieldSqlAry = array();
$CurriculumArr = $lpf_dbs->getCurriculumSetting();
foreach($CurriculumArr as $thisCurriculumInfo)
{
    $fieldAry[] = $thisCurriculumInfo['Code'];
    $fieldSqlAry[] = " IF(  
                            COUNT(DISTINCT sc.StudentID) = 0 AND yc.CurriculumID = '".$thisCurriculumInfo['CurriculumID']."', 
                            COUNT(ycu.YearClassUserID),
                            SUM(
                                IF(
                                    sc.CurriculumID = '".$thisCurriculumInfo['CurriculumID']."' OR (sc.StudentID IS NULL AND yc.CurriculumID = '".$thisCurriculumInfo['CurriculumID']."'), 1, 0
                                )
                            )
                        ) as '".$thisCurriculumInfo['Code']."'";
}
$fieldStr = implode(", ", $fieldSqlAry);

if ($order=="") $order = 1;
if ($field=="") $field = 0;
$pageSizeChangeEnabled = true;
$libTable = new libpf_dbtable($field, $order, $pageNo);

$user_table = $isAlumniAccess? "INTRANET_ARCHIVE_USER" : "INTRANET_USER";
$cond = ($YearID == "") ? "" : " AND yc.YearID = '".$YearID."'";

$sql = "SELECT
            CONCAT('<a href=\"edit.php?YearClassID=', yc.YearClassID,'\" class=\"tablelink\">', ".libportfolio::GetYearClassTitleFieldByLang("yc.").", '</a>'),
            $fieldStr,
            COUNT(DISTINCT ycu.YearClassUserID) as StudentCount
        FROM
            {$intranet_db}.YEAR_CLASS AS yc
            INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON  yc.YearClassID = ycu.YearClassID
            INNER JOIN {$intranet_db}.$user_table AS iu ON  ycu.UserID = iu.UserID
            LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON iu.UserID = ps.UserID
            LEFT JOIN {$eclass_db}.DBS_STUDENT_CURRICULUM AS sc ON iu.UserID = sc.StudentID
        WHERE
            yc.AcademicYearID = '".$targetYearID."' AND
			yc.YearID != 0 AND
            iu.RecordType = 2
            /* AND iu.RecordStatus = 1*/
            $cond
        GROUP BY
            yc.YearClassID ";
$libTable->sql = $sql;

$libTable->field_array = array_merge(array("ClassTitleEN"), $fieldAry, array("StudentCount"));
$libTable->db = $intranet_db;
$libTable->no_msg = $no_record_msg;
$libTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$libTable->no_col = count($fieldAry) + 3;

$libTable->table_tag = "<table width='100%' border='0' cellpadding='4' cellspacing='1'>";
//$libTable->row_alt = array("#FFFFFF", "F3F3F3");
$libTable->row_alt = array("", "");
$libTable->sort_link_style = "class='tbheading'";
$libTable->row_valign = "top";
$libTable->column_array = array(0);
foreach($fieldAry as $thisField) {
    $libTable->column_array[] = 3;
}
$libTable->column_array[] = 3;

// TABLE COLUMN
$fieldNum = 1;
$libTable->column_list .= "<tr class='tabletop'>\n";
$libTable->column_list .= "<td height='25' align='center' class=\"tabletopnolink\" >#</span></td>\n";
$libTable->column_list .= "<td>".$libTable->column(0,$ec_iPortfolio['class'])."</td>\n";
foreach($fieldAry as $thisFieldName) {
    $libTable->column_list .= "<td align='center'>".$libTable->column($fieldNum++,$thisFieldName)."</td>\n";
}
$libTable->column_list .= "<td align='center'>".$libTable->column($fieldNum,$ec_iPortfolio['heading']['no_stu'])."</td>\n";
$libTable->column_list .= "</tr>\n";

// Get table
$html["display_table"] = $libTable->displayPlain();
$html["table_hidden_field"] = <<<HTMLEND
<input type="hidden" name="pageNo" value="{$libTable->pageNo}">
<input type="hidden" name="order" value="{$order}">
<input type="hidden" name="field" value="{$field}">
<input type="hidden" name="page_size_change"/>
<input type="hidden" name="numPerPage" value="{$numPerPage}" />
HTMLEND;

if($isAlumniAccess)
{
$html["table_hidden_field"] .= <<<HTMLEND
<input type="hidden" name="isAlumni" value="1" />
HTMLEND;
}
###############################################

###############################################
###	HTML - display table

// $lfcm_ui = new form_class_manage_ui();
// $htmlAry["classSelection"] = $lfcm_ui->Get_Class_Selection(Get_Current_Academic_Year_ID(), '', 'YearClassID', $YearClassID, 'jCHANGE_FIELD()', 0, 0, 0, 0, $iPort['Assessment']['AllClasses']);

# Page heading setting
$linterface = new interface_html();
$CurrentPage = "Teacher_Transcript_StudentCurriculumsMapping";

### Title ###
$TAGS_OBJ[] = array($Lang['iPortfolio']['DBSTranscript']['StudentCurriculumsMapping']['Title'],"");
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR("Teacher");
$html["view_student"] = true;

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

include_once("../templates/student_curriculums_setting_list.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>