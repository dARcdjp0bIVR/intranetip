<?php
# using: yat

### this page will be called by:
# 
# /home/admin/smartcard/take/takeAM.php
# /home/admin/smartcard/take/takePM.php
#
# PPC:
# /ppc/home/attendance/take/takeAM.php
# /ppc/home/attendance/take/takePM.php

#################################################
#
#	Date:	2013-04-10	YatWoon
#			Fixed: Missed the declare the $PageLoadTime that failed to take attendance more than 1 time
#
#################################################

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../includes/libadminjob.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$isGroupType = false;
if($class_name == "" && $group_id != "" && $period<>"")
{
	$isGroupType = true;
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     	header ("Location: /");
     intranet_closedb();
     exit();
}

$lu = new libuser($UserID);
$isStudentHelper = $lu->isStudent();

$LIDB = new libdb();
$LICLASS = new libclass();
$lcardattend = new libcardstudentattend2();

$TargetDate=date('Y-m-d');
$isPPC = $_SESSION['client_is_ppc'];
$PageLoadTime = $PageLoadTime ? $PageLoadTime : time();
if(sizeof($user_id) > 0)
{
	//CHECK RECORD UPDATED AFTER LOAD THE SUBMIT PAGE, CHECK DATA CONSISTENCE
	if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
		$msg = 99;  // data outdated

		$return_url = $isPPC? "/ppc/home/attendance/take/" : "";
		$switch_status = $isPPC? "&switch_status=".urlencode($switch_status):"";
		$return_url .= ($period=="AM")?"takeAM.php":"";
		$return_url .= ($period=="PM")?"takePM.php":"";

		if($isGroupType)
		{
			$return_url .= "?group_id=".urlencode($group_id)."&period=".urlencode($period)."&msg=$msg".$switch_status;
		}else
		{
			$return_url .= "?class_name=".urlencode($class_name)."&period=".urlencode($period)."&msg=$msg".$switch_status;
		}

		header("Location: $return_url");
		exit();
	}
}

### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".date("Y")."_".date("m");

$my_record_date = date("Y-m-d");

if($bug_tracing['smartcard_student_attend_status']){
	include_once("../../../../includes/libfilesystem.php");
	if(!is_dir($file_path."/file/log_student_attend_status")){
		$lf = new libfilesystem();
		$lf->folder_new($file_path."/file/log_student_attend_status");
	}
	$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".date("Ymd").".txt";
}


########### PPC Version #####################

if($isPPC){
	
	$remark_list_to_remove=array();
	$remark_list_to_insert=array();
	$remark_list_to_update=array();
	
	if(sizeof($user_id)>0){
			$user_id = IntegerSafe($user_id);
			$student_list = implode(",",$user_id);
			
			## Get Remark list
	        $sql = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='$my_record_date' AND StudentID IN ( $student_list)";
	        $r = $LIDB->returnArray($sql,2);
	        for($i=0;$i<sizeof($r);$i++){
	                $rid = $r[$i][0];
	                $student_id = $r[$i][1];
	                $remarkResult[$student_id]['RecordID']= $rid;
	        }
	        
	        ## Get Preset Absence List
			$my_period = $period=="AM"?2:3;
			$sql="SELECT RecordID,StudentID,Reason FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID IN($student_list) AND RecordDate='$my_record_date' AND DayPeriod='$my_period'";
			$tt = $LIDB->returnArray($sql,3);
			for($i=0;$i<sizeof($tt);$i++){
				list($tt_id,$tt_sid,$tt_reason)=$tt[$i];
				$preset_absence[$tt_sid]['reason']=$tt_reason;
			}
	        
	}

	
	for($i=0; $i<sizeOf($user_id); $i++){
        $my_user_id = $user_id[$i];
        $my_day = date("d");
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];    
		$remark_record_id = $remarkResult[$my_user_id]['RecordID'];
		
		//eval("\$my_reason=\$reason$i;");
		$my_reason = $_POST['reason'.$i];
		
		if($switch_status!=1){ ## show all mode, teacher cannot input the remark
			if( $my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE && $remark_record_id!=""){
	                        # remove Remarks if :
	                        # 1. not absent/late
	                        $remark_list_to_remove[] =$remark_record_id;
	        }else if( ($my_drop_down_status==CARD_STATUS_ABSENT) && $remark_record_id==""){ 
		       				 // Late / Absent AND No Remark
		        			 // 1. Set Preset Absent as Remark ?
		        			 $preset_absence_reason = $preset_absence[$my_user_id]['reason'];
		        			 if($preset_absence_reason!="")
		        			 	$remark_list_to_insert[] ="('$my_user_id','$my_record_date','$preset_reason')";
		    }
	    }else{ ## show late / absent / outing mode, teacher can input the reason
		    if($my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE && $remark_record_id!=""){
	                        # remove Remarks if :
	                        # 1. not absent/late
	                        $remark_list_to_remove[] =$remark_record_id;
			}else if($my_reason!="" && !$isStudentHelper){
		                        # insert
		                        if($remark_record_id==""){
		                                $remark_list_to_insert[] ="('$my_user_id','$my_record_date','$my_reason')";
		                        }
		                        # update
		                        else{
		                                $updateRemarkSQL ="UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$my_reason' WHERE RecordID='$remark_record_id'";
		                                $LIDB->db_db_query($updateRemarkSQL);
		                        }
		
		     }else if( ( ($my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE) 
		                			|| !$isStudentHelper) && $remark_record_id!=""){
		                        # remove Remarks if :
		                        # 1. not absent/late
		                        # 2. remark is null , and not student helper
		                        $remark_list_to_remove[]=$remark_record_id;
		     }
		}
	            
        if( $period == "AM"){

                # insert if not exist
                if($my_record_id==""){
                        $sql = "INSERT INTO $card_log_table_name
                                       (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
                                       ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
                                                ";

                        $LIDB->db_db_query($sql);

                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE)           # empty -> present
                        {
	                        $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
                        }
                }
                # update if exist
                else{
                     # Grab original status
                     $sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
                     $temp = $LIDB->returnArray($sql,3);
                     list($old_status, $old_inTime, $old_record_id) = $temp[0];
                     if ($old_status != $my_drop_down_status)
                     {
                         # Check bad actions
                         if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                         {
	                         $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
	                          if($old_inTime!=""){ # with time but absent
	                               # Has card record but not present in classroom
	                               $lcardattend->addBadActionFakedCardAM($my_user_id,$my_record_date,$old_inTime);
                               }
                         }
                         
                         # Absent -> Late / Present
                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                         {
                               $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
	                               # forgot to bring card
	                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
                         }

                         
                         # Update Daily table
                        $sql = "UPDATE $card_log_table_name SET AMStatus='$my_drop_down_status', DateModified = now() WHERE RecordID='$my_record_id'";

                        $LIDB->db_db_query($sql);
                     }

                     # Try to remove profile records if applicable
                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
                     {
                         # Remove Previous Absent Record
                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                              AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                         $lcardattend->db_db_query($sql);
                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                              AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                         $lcardattend->db_db_query($sql);
                     }

                }
        }
        else if( $period == "PM"){
                # insert if not exist
                if($my_record_id==""){
                        $sql = "INSERT INTO $card_log_table_name
                                       (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
                                       ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
                        {
	                        $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
                        }
                }
                # update if exist
                else{
                     # Grab original status
                     $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
                     $temp = $LIDB->returnArray($sql,5);
                     list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
                     
                    ## get old time
			        if ($lcardattend->attendance_mode==1){ # PM only
						$bad_action_time_field = $old_inTime;
					}else{
						$bad_action_time_field = $old_lunchBackTime;
					}
                     
                     if ($old_status != $my_drop_down_status)
                     {
	                      if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                         {
	                         
                               $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
								if($bad_action_time_field!=""){ # has time but absent
	                               # Has card record but not present in classroom
	                               $lcardattend->addBadActionFakedCardPM($my_user_id,$my_record_date,$bad_action_time_field);
                               }
                         }
                         
                         # Absent -> Late / Present
                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                         {
                               $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
                               # forgot to bring card
                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$bad_action_time_field);
                         }

                         # Update Daily table
                         $sql = "UPDATE $card_log_table_name SET PMStatus='$my_drop_down_status', DateModified=now() WHERE RecordID='$my_record_id'";
                         $LIDB->db_db_query($sql);
                     }

                     # Try to remove profile records if applicable
                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
                     {
                         # Remove Previous Absent Record
                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                              AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                         $lcardattend->db_db_query($sql);
                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                              AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                         $lcardattend->db_db_query($sql);
                     }
                }
        }
        
	}
    ## Remove Remark
    if(sizeof($remark_list_to_remove)>0){
        $r_list = implode(",",$remark_list_to_remove);
        $sql = "DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID IN ($r_list)";
        $lcardattend->db_db_query($sql);
    }
    ## Insert Remark
    if(sizeof($remark_list_to_insert)>0){
        $r_list = implode(",",$remark_list_to_insert);
        $sql ="INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,Remark) VALUES $r_list ";
        
        $lcardattend->db_db_query($sql);
    }

	
} // End of PPC Version

else{ ##################### PC Version #########################

		if(sizeof($user_id)>0){
				# get Preset Absence
				if($isStudentHelper){
					$my_period = $period=="AM"?2:3;
					$s_list = implode(",",$user_id);
					$sql="SELECT RecordID,StudentID,Reason FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID IN($s_list) AND RecordDate='$my_record_date' AND DayPeriod='$my_period'";
					$tt = $LIDB->returnArray($sql,3);
					for($i=0;$i<sizeof($tt);$i++){
						list($tt_id,$tt_sid,$tt_reason)=$tt[$i];
						$preset_absence[$tt_sid]['reason']=$tt_reason;
					}
				}
				
		        $sqlRemark = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='$my_record_date' AND StudentID IN (";
		        for($i=0;$i<sizeof($user_id);$i++)
		                $sqlRemark.=$user_id[$i].",";
		        $sqlRemark=substr($sqlRemark,0,strlen($sqlRemark)-1).")";
		        $r = $LIDB->returnArray($sqlRemark,2);
		        for($i=0;$i<sizeof($r);$i++){
		                $rid = $r[$i][0];
		                $student_id = $r[$i][1];
		                $remarkResult[$student_id]['RecordID']= $rid;
		        }
		        $insertRemarkSQL="INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,Remark)VALUES";
		}
		
		$removeRemarkSQL = "DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID IN( ";
		$insertRemark = false;
		$removeRemark = false;
		for($i=0; $i<sizeOf($user_id); $i++){
		        $my_user_id = $user_id[$i];
		        $my_day = date("d");
		        $my_drop_down_status = $drop_down_status[$i];
		        $my_record_id = $record_id[$i];
		
		                $remark_record_id = $remarkResult[$my_user_id]['RecordID'];
		
		                //eval("\$my_reason=\$reason$i;");
		                $my_reason = $_POST['reason'.$i];
						$my_reason = intranet_htmlspecialchars($my_reason);
					
		                # insert / update CARD_STUDENT_DAILY_REMARK
		                
		                # if Student Helper, and No Remark, then use Preset Absence reason for the Remark
		                if($isStudentHelper && $my_drop_down_status==CARD_STATUS_ABSENT && $remark_record_id==""){
			                $preset_reason = $preset_absence[$my_user_id]['reason'];
			                if($preset_reason!=""){
		                            $insertRemark = true;
		                            $insertRemarkSQL .="('$my_user_id','$my_record_date','$preset_reason'),";
				            }
			            }
			            
		                if($my_reason!="" && !$isStudentHelper){
		                        # insert
		                        if($remark_record_id==""){
		                                $insertRemark = true;
		                                $insertRemarkSQL .="('$my_user_id','$my_record_date','$my_reason'),";
		                        }
		                        # update
		                        else{
		                                $updateRemarkSQL ="UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$my_reason' WHERE RecordID='$remark_record_id'";
		                                $LIDB->db_db_query($updateRemarkSQL);
		                        }
		
		                }else if( ( ($my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE) 
		                			|| !$isStudentHelper) && $remark_record_id!=""){
		                        # remove Remarks if :
		                        # 1. not absent/late
		                        # 2. remark is null , and not student helper
		                        $removeRemark=true;
		                        $removeRemarkSQL .=$remark_record_id.",";
		                }
			            
		                
		                /*
		                if($my_reason!=""){
		                        # insert
		                        if($remark_record_id==""){
		                                $insertRemark = true;
		                                $insertRemarkSQL .="('$my_user_id','$my_record_date','$my_reason'),";
		                        }
		                        # update
		                        else{
		                                $updateRemarkSQL ="UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$my_reason' WHERE RecordID='$remark_record_id'";
		                                $LIDB->db_db_query($updateRemarkSQL);
		                        }
		
		                }else if($my_drop_down_status!=CARD_STATUS_ABSENT && $remark_record_id!=""){
		                        # remove Remarks if not absent
		                        $removeRemark=true;
		                        $removeRemarkSQL .=$remark_record_id.",";
		                }
		                */
		
		        if( $period == "AM"){
		                # insert if not exist
		                if($my_record_id==""){
		                        $sql = "INSERT INTO $card_log_table_name
		                                       (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
		                                       ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
		                                                ";
		                        $LIDB->db_db_query($sql);
		                        
                        ## Bug Tracing
                        if($bug_tracing['smartcard_student_attend_status']){
	                        $log_date = date('Y-m-d H:i:s');
	                        $log_target_date = $my_record_date;
	                        $log_student_id = $my_user_id;
	                        $log_old_status = "";
	                        $log_new_status = $my_drop_down_status;
	                        $log_sql = $sql;
	                        $log_admin_user = $UserID;
	                        
             				$log_page = 'take_update.php';
				            $log_content = get_file_content($log_filepath);
				            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				            $log_content .= $log_entry;
				            write_file_content($log_content, $log_filepath);	                        
	                    }		                        
		
		                        # Check Bad actions
		                        if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
		                        {
			                        $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
		                            # Forgot to bring card
		                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
		                        }
		                }
		                # update if exist
		                else{
		                     # Grab original status
		                     $sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
		                     $temp = $LIDB->returnArray($sql,3);
		                     list($old_status, $old_inTime, $old_record_id) = $temp[0];
		                     if ($old_status != $my_drop_down_status)
		                     {
		                         # Check bad actions
		                         if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
		                         {
			                         $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
			                         if($old_inTime!=""){
			                               # Has card record but not present in classroom
			                               $lcardattend->addBadActionFakedCardAM($my_user_id,$my_record_date,$old_inTime);
		                               }
		                         }
		                         
				                 # Absent -> Late / Present
		                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
		                         {
		                               $lcardattend->removeBadActionFakedCardAM($my_user_id,$my_record_date);
			                               # forgot to bring card
			                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$old_inTime);
		                         }
		                         # Update Daily table
		                        $sql = "UPDATE $card_log_table_name SET AMStatus='$my_drop_down_status', DateModified = now() WHERE RecordID='$my_record_id'";
		                        $LIDB->db_db_query($sql);
		                        
		                       ## Bug Tracing
		                        if($bug_tracing['smartcard_student_attend_status']){
			                        $log_date = date('Y-m-d H:i:s');
			                        $log_target_date = $my_record_date;
			                        $log_student_id = $my_user_id;
			                        $log_old_status = $old_status;
			                        $log_new_status = $my_drop_down_status;
			                        $log_sql = $sql;
			                        $log_admin_user = $UserID;
			                        
		             				$log_page = 'take_update.php';
						            $log_content = get_file_content($log_filepath);
						            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
						            $log_content .= $log_entry;
						            write_file_content($log_content, $log_filepath);	                        
			                    }		                     		                        
		                        
		                        
		                     }
		
		                     # Try to remove profile records if applicable
		                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
		                     {
		                         # Remove Previous Absent Record
		                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
		                                              AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
		                         $lcardattend->db_db_query($sql);
		                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
		                                              AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
		                         $lcardattend->db_db_query($sql);
		                     }
		
		                }
		        }
		        else if( $period == "PM"){
		                # insert if not exist
		                if($my_record_id==""){
		                        $sql = "INSERT INTO $card_log_table_name
		                                       (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
		                                       ('$my_user_id','$my_day','$my_drop_down_status',NOW(),NOW())
		                                                ";
		                        $LIDB->db_db_query($sql);
		                        
		                       ## Bug Tracing
		                        if($bug_tracing['smartcard_student_attend_status']){
			                        $log_date = date('Y-m-d H:i:s');
			                        $log_target_date = $my_record_date;
			                        $log_student_id = $my_user_id;
			                        $log_old_status = "";
			                        $log_new_status = $my_drop_down_status;
			                        $log_sql = $sql;
			                        $log_admin_user = $UserID;
			                        
		             				$log_page = 'take_update.php';
						            $log_content = get_file_content($log_filepath);
						            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
						            $log_content .= $log_entry;
						            write_file_content($log_content, $log_filepath);	                        
			                    }		                     		                        
		                        
		                        
		                        # Check Bad actions
		                        if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
		                        {
			                        $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
		                            # Forgot to bring card
		                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
		                        }
		                }
		                # update if exist
		                else{
		                     # Grab original status
		                     $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
		                     $temp = $LIDB->returnArray($sql,5);
		                     list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
		                     
			                    ## get old time
						        if ($lcardattend->attendance_mode==1){ # PM only
									$bad_action_time_field = $old_inTime;
								}else{
									$bad_action_time_field = $old_lunchBackTime;
								}		                     
		                     
		                     
		                     if ($old_status != $my_drop_down_status)
		                     {
			                      if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
		                         {
			                         
		                               $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
										if($bad_action_time_field!=""){ # has time but absent
			                               # Has card record but not present in classroom
			                               $lcardattend->addBadActionFakedCardPM($my_user_id,$my_record_date,$bad_action_time_field);
		                               }
		                         }
		                         
		                         # Absent -> Late / Present
		                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
		                         {
		                               $lcardattend->removeBadActionFakedCardPM($my_user_id,$my_record_date);
		                               # forgot to bring card
		                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$bad_action_time_field);
		                         }		
		                         # Update Daily table
		                         $sql = "UPDATE $card_log_table_name SET PMStatus='$my_drop_down_status', DateModified=now() WHERE RecordID='$my_record_id'";
		                         $LIDB->db_db_query($sql);
		                         
				                ## Bug Tracing
		                        if($bug_tracing['smartcard_student_attend_status']){
			                        $log_date = date('Y-m-d H:i:s');
			                        $log_target_date = $my_record_date;
			                        $log_student_id = $my_user_id;
			                        $log_old_status = $old_status;
			                        $log_new_status = $my_drop_down_status;
			                        $log_sql = $sql;
			                        $log_admin_user = $UserID;
			                        
		             				$log_page = 'take_update.php';
						            $log_content = get_file_content($log_filepath);
						            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
						            $log_content .= $log_entry;
						            write_file_content($log_content, $log_filepath);	                        
			                    }		                     
		                         
		                     }
		
		                     # Try to remove profile records if applicable
		                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
		                     {
		                         # Remove Previous Absent Record
		                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
		                                              AND AttendanceDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		                         $lcardattend->db_db_query($sql);
		                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
		                                              AND RecordDate = '$my_record_date' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
		                         $lcardattend->db_db_query($sql);
		                     }
		                }
		        }
		}
		if($insertRemark){
		        $insertRemarkSQL = substr($insertRemarkSQL,0,strlen($insertRemarkSQL)-1);
		        $LIDB->db_db_query($insertRemarkSQL);
		}
		if($removeRemark){
		        $removeRemarkSQL =substr($removeRemarkSQL,0,strlen($removeRemarkSQL)-1).")";
		        $LIDB->db_db_query($removeRemarkSQL);
		}
} // End of PC Version

### for class / group confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".date("Y")."_".date("m");
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".date("Y")."_".date("m");

if( $confirmed_id <> "" ){
        # update if record exist
		$sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID='$UserID', DateModified = NOW() WHERE RecordID = '$confirmed_id'";
		if($isGroupType)
		{
			$sql = "UPDATE $card_student_daily_group_confirm SET ConfirmedUserID='$UserID', DateModified = NOW() WHERE RecordID = '$confirmed_id'";
		}
        $LIDB->db_db_query($sql);
        $msg = 2;
}
else{
        # insert if record not exist
        $class_id = $LICLASS->getClassID($class_name);

        switch($period){
                case "AM": $DayType = 2; break;
                case "PM": $DayType = 3; break;
                default : $DayType = 2; break;
        }

        $sql = "INSERT INTO $card_student_daily_class_confirm
                                        (
                                                ClassID,
                                                ConfirmedUserID,
                                                DayNumber,
                                                DayType,
                                                DateInput,
                                                DateModified
                                        ) VALUES
                                        (
                                                '$class_id',
                                                '$UserID',
                                                '".date("d")."',
                                                '$DayType',
                                                NOW(),
                                                NOW()
                                        )
                                        ";
		if($isGroupType)
		{
	        $sql = "INSERT INTO $card_student_daily_group_confirm
					(
						GroupID,
                        ConfirmedUserID,
                        DayNumber,
                        DayType,
                        DateInput,
                        DateModified
					) 
						VALUES
                    (
                        '$group_id',
                        '$UserID',
                        '".date("d")."',
                        '$DayType',
                        NOW(),
                        NOW()
					)
                    ";
		}
        $LIDB->db_db_query($sql);
        $msg = 1;
}

$return_url = $isPPC? "/ppc/home/attendance/take/" : "";
$switch_status = $isPPC? "&switch_status=".urlencode($switch_status):"";
$return_url .= ($period=="AM")?"takeAM.php":"";
$return_url .= ($period=="PM")?"takePM.php":"";


if($isGroupType)
{
	$return_url .= "?group_id=".urlencode($group_id)."&period=".urlencode($period)."&msg=$msg".$switch_status;
}else
{
	$return_url .= "?class_name=".urlencode($class_name)."&period=".urlencode($period)."&msg=$msg".$switch_status;
}

//echo "return_url [".$return_url."]<Br>";
header( "Location: $return_url");
?>
