<?php
/*
 *  Using:      
 * 
 * 	Please use UTF-8 to edit
 * 
 * 	Purpose: customized portal to replace home/index.php
 * 	Staff have specific module admin permission can access the management menu
 *
 *  2020-09-07 [Bill]   [2020-0604-1821-16170]
 *      - updated session checking for eNotice
 *
 *  2020-04-28 [Cameron]
 *      - disable DemoLMS, let it use StandardLMS configure
 *      
 *  2020-04-17 [Cameron]
 *      - don't show organization logo if it hasn't setup
 *      - don't show organization_info menu for Amway, Oaks and CEM because they are already customized before
 *      
 *  2020-03-31 [Cameron]
 *      - add standard LMS 
 *      
 *  2020-03-26 [Cameron]
 *      - add Menu item organization_info
 *      
 *  2020-02-06 [Cameron]
 *      - fix layout for DemoLMS
 *      
 *  2018-11-12 [Cameron]
 *      - change flag from $sys_custom['project'] to $sys_custom['project']['CourseTraining']
 *      - add case $sys_custom['project']['CourseTraining']['CEM']
 *      
 *	2017-12-27 [Cameron]
 *		- show default personal photo if it's empty
 *
 *	2017-12-18 [Cameron]
 *		- add customization for Oaks
 *
 *	2016-01-13 [Cameron]
 *		- pass ParentWindowLevel in sign()
 *		- change window name from 10 to 25 in allNotice()
 *  
 * 	2015-12-09 [Cameron]
 * 		- create this file
 * 
 */
 
$PATH_WRT_ROOT = "../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libannounce2007a.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

//debug_pr($sys_custom['project']);
//debug_pr($_SERVER);
//debug_pr($_SESSION);

$li = new libuser($UserID); 
$lp = new libportal();
$la = new libannounce2007();
$announcement = $la->returnSchoolAnnouncement($UserID);
$no_announcement = count($announcement);

$ln = new libnotice();
if ($_SESSION['UserType']==USERTYPE_STAFF) {
	$eNotice = $ln->getCusteNoticeForStaff(true);	// count unexpired eNotice
	$no_eNotice = $eNotice[0]['NumOfRec'];
	$DisplayName = $li->UserNameLang(0);
}
else {
	$no_eNotice = $lp->getStudentNoticeUnsignedCount();	// count unsigned eNotice
	$UserLogin = $li->UserLogin;
	$DisplayName = $li->UserNameLang(0)." ($UserLogin)";
}
$PersonalPhotoLink = $li->PersonalPhotoLink;
if(empty($PersonalPhotoLink) || !file_exists($intranet_root.$PersonalPhotoLink)) {
	$PersonalPhotoLink = $li->returnDefaultPersonalPhotoPath();
}

if ($sys_custom['project']['CourseTraining']['StandardLMS']) {
    $cus_image = '';
    $ls = new libgeneralsettings();
    $settingAry = $ls->Get_General_Setting('OrganizationInfo',array("'logo'"));
    
    if (count($settingAry)) {
        $logo = "/file/standard_lms/". $settingAry['logo'];
        
        if (file_exists($intranet_root.$logo)) {
            $cus_image = $logo;
        }
    }    
    $cus_css = "/templates/2009a/css/customized/content_standard_lms.css";
    $user_eclass_layout = $lp->custDisplayUserEClass_Amway($li->UserEmail);
}

// else if ($sys_custom['project']['CourseTraining']['DemoLMS'] && $sys_custom['project']['CourseTraining']['Amway']) {
//     $cus_css = "../images/customization/lms/css/content_lms.css";
//     $cus_image = "../images/customization/lms/images/module/lms/lms_logo.png";
//     $user_eclass_layout = $lp->custDisplayUserEClass_Amway($li->UserEmail);
// }
else if ($sys_custom['project']['CourseTraining']['Amway']) {
	$cus_css = "../images/customization/amway/css/content_Amway.css";
	$cus_image = "../images/customization/amway/images/module/Amway/Amway_logo.png";
	$user_eclass_layout = $lp->custDisplayUserEClass_Amway($li->UserEmail); 	
}
else if ($sys_custom['project']['CourseTraining']['Oaks']) {
	$cus_css = "../images/customization/oaks/css/content_TheOaks.css";	
	$cus_image = "../images/customization/oaks/images/module/TheOaks/TheOaks_login_logo.png";
	$user_eclass_layout = $lp->custDisplayUserEClass_Oaks($li->UserEmail);
}
else if ($sys_custom['project']['CourseTraining']['CEM']) {
    $cus_css = "/templates/2009a/css/customized/content_cem.css";
    $cus_image = "/images/customization/cem/module/cem/cem_logo.png";
    $user_eclass_layout = $lp->custDisplayUserEClass_CEM($li->UserEmail);
}
else {
	$cus_css = "";
	$cus_image = "";
	$user_eclass_layout = ""; 
}

### Customized portal
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<title><?=$Lang['PageTitle']?></title>
<link href="<?=$cus_css?>" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?= $intranet_session_language?>.js"></script>
<script>
function moreNews(type)
{
	newWindow('moreannouncement.php?type='+type,1);
}

function sign(id,studentID)
{
    newWindow('../home/eService/notice/student_notice/sign.php?NoticeID='+id+'&StudentID='+studentID+'&ParentWindowLevel=1',10);
}

function allNotice() {
	newWindow('../home/eService/notice/student_notice/index.php',35);	// window name is different from 10 so that it won't loop back when sign notice 
}

$(document).ready(function(){
	$('#AnnouncementTab a').click(function(){
		$('#eNoticeTab').hasClass('current'); {
			$('#eNoticeTab').removeClass('current');
			$('#AnnouncementTab').addClass('current');
		}
		$('.main_content_annoucement').css('display', 'block');
		$('.main_content_enotice').css('display', 'none');
	});
	$('#eNoticeTab a').click(function(){
		$('#AnnouncementTab').hasClass('current'); {
			$('#AnnouncementTab').removeClass('current');
			$('#eNoticeTab').addClass('current');
		}
		$('.main_content_annoucement').css('display', 'none');
		$('.main_content_enotice').css('display', 'block');
	});
	
});

function logout()
{
	if(confirm(globalAlertMsg16))
	{
    	window.location="/logout.php";
	}
}

</script>
</head>

<body>

<?php if ($sys_custom['project']['CourseTraining']['CEM']):?>
<div class="bg_decor"></div>
<?php endif;?>

<div id="container">
    <div class="top_banner">
    
    <?php if ($sys_custom['project']['CourseTraining']['CEM']):?>
    	<div class="top_banner_decor"></div>
    <?php endif;?>
    
<?php if ($cus_image):?>    
    	<div class="school_logo"><a href="/home/index.php"><img src="<?php echo $cus_image;?>" alt="<?php echo $Lang['SysMgr']['OrganizationInfo']['Logo'];?>" /></a></div>
<?php endif;?>
    	
        <div class="top_menu">
			<ul <?php if ($sys_custom['project']['CourseTraining']['CEM']) echo 'class="menu_items"';?>>
            	<li class="<?php if (!$sys_custom['project']['CourseTraining']['CEM']) echo 'current';?>"><a href="/home/index.php"><?=$Lang['Header']['Menu']['Home']?></a></li>
<?php if ($_SESSION['UserType']==USERTYPE_STAFF): ?>            	
				<li class="<?php if ($sys_custom['project']['CourseTraining']['CEM']) echo 'current';?>"><a href="#"><?=$Lang['Portal']['Menu']['Management']?></a>
					<ul <?php if ($sys_custom['project']['CourseTraining']['CEM']) echo 'class="sub_items"';?>>
<?php
	if (($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {					
		echo "<li><a href=\"../home/eAdmin/GeneralMgmt/schoolnews/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['schoolNews']."</span></a></li>\n";
	}
?>	
                        <li><a href="../home/eLearning/eclass/" target="_blank"><span><?=$Lang['Header']['Menu']['eClass']?></span></a></li>
<?php
    // [2020-0604-1821-16170]
	//if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
    if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"])) {
		echo "<li><a href=\"../home/eAdmin/StudentMgmt/notice/student_notice/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['eNotice']."</span></a></li>\n";
	}                        

	if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]) {
		echo "<li><a href=\"../home/system_settings/form_class_management/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['Class']."</span></a></li>\n";
	}
	
	if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"]) {          
		echo "<li><a href=\"../home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1\" target=\"_blank\"><span>".$Lang['Header']['Menu']['User']."</span></a></li>\n";
	}
	
	if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
		echo "<li><a href=\"../home/system_settings/role_management/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['Role']."</span></a></li>\n";		
	}

	if($sys_custom['project']['CourseTraining']['IsEnable'] && (!$sys_custom['project']['CourseTraining']['Amway'] && !$sys_custom['project']['CourseTraining']['Oaks'] && !$sys_custom['project']['CourseTraining']['CEM']) && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"])) {
	    echo "<li><a href=\"../home/system_settings/organization_info/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['OrganizationInfo']."</span></a></li>\n";
	}
	
//	if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]) && ($_SESSION['EnglishName'] == 'broadlearning'))
//	{
//		echo "<li><a href=\"../home/system_settings/school_calendar/\" target=\"_blank\"><span>".$Lang['Header']['Menu']['SchoolCalendar']."</span></a></li>\n";
//	}

//	if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && ($_SESSION['EnglishName'] == 'broadlearning')) {
//		echo "<li><a href=\"../home/system_settings/security/password_policy.php\" target=\"_blank\"><span>".$Lang['SysMgr']['Security']['Title']."</span></a></li>\n";
//	}	                 
?>                        
					</ul>                
              	</li>
<?php endif; ?>              
			</ul>
      </div>
        <div class="top_banner_right">
		  <div class="top_user">
				<a href="#" class="top_user_group"><span class="selectbox_arrow"></span><span class="top_user_name"><?=$DisplayName?></span><div class="top_user_photo"><img src="<?=$PersonalPhotoLink?>"/></div></a>
			  <ul>
				  <li><a href="../home/iaccount/account/index.php" target="_blank"><span><?=$Lang['Header']['Menu']['MyProfile']?></span></a></li>
				  <li><a href="../home/iaccount/account/login_password.php" target="_blank"><span><?=$Lang['Header']['Menu']['ChangePassword']?></span></a></li>
				  <li><a href="javascript:logout()"><span><?=$i_general_logout?></span></a></li>
			  </ul>
		  </div>
          <div class="top_language">
          	<ul>
          	  	<li><a href="../lang.php?lang=en" class="<?=(($_SESSION['LastLang'] == 'en')?'top_language_current ':'')?>language_en"><?=$ip20_lang_eng?></a></li>
          		<li><a href="../lang.php?lang=b5" class="<?=(($_SESSION['LastLang'] == 'b5')?'top_language_current ':'')?>language_b5"><?=$ip20_lang_b5?></a></li>
          	</ul>
          </div>
        </div>
    </div>
	<div class="board" id="main_portal">
    	<div class="board_main">
	        <div class="board_main_content_left">
            	<div class="board_main_content">
                	<div class="main_tab tab_style1">
                        <ul><li class="current"><a><?=$Lang['Portal']['Tag']['Course']?></a></li></ul>                    
                    </div>
                  <div class="main_content">
                  <?=$user_eclass_layout?>
                  </div>
                </div>
            </div>
	        <div class="board_main_content_right">
            	<div class="board_main_content">
                	<div class="main_tab tab_style2">
                        <ul><li id="AnnouncementTab" class="<?=($AfterSignNotice?'':'current')?>"><a href="#"><?=$Lang['Portal']['Tag']['Announcement']?> (<?=$no_announcement?>)</a></li>
                        	<li id="eNoticeTab" class="<?=($AfterSignNotice?'current':'')?>"><a href="#"><?=$Lang['Portal']['Tag']['eNotice']?> (<?=$no_eNotice?>)</a></li></ul>
                    </div>
					<div class="main_content main_content_annoucement" style="display:<?=($AfterSignNotice?'none':'block')?>;">
						<?=$la->displayCustAnnouncement($UserID)?>
                    </div>
                    
	                <div class="main_content main_content_enotice" style="display:<?=($AfterSignNotice?'block':'none')?>;">
	                	<?=$ln->displayCusteNotice($UserID,($_SESSION['UserType']==USERTYPE_STAFF)?true:false)?>
	                </div>
                </div>
            </div>
        </div>
        <p class="spacer"></p>
    </div>
	<div class="footer"><a href="http://eclass.com.hk" title="eClass" target="_blank"></a><span><?=$Lang['Footer']['PowerBy']?></span></div>
</div>
</body>
<form name="form1" action="/home/portal.php" method="post">
	<input type="hidden" name="AfterSignNotice" id="AfterSignNotice" value="">	
</form>
</html>
<?php
intranet_closedb();
?>