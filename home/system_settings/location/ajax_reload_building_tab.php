<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_opendb();

# Get data
$BuildingID = $_REQUEST['BuildingID'];

$liblocation_ui = new liblocation_ui();
echo $liblocation_ui->Get_Building_Tab_Div($BuildingID);

intranet_closedb();

?>