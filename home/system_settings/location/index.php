<?php
// using :  

############# Change Log [Start] ################
#
# Date: 2018-08-22 Cameron
#       Fix goPrint, should open in new window tab
#
# Date:	2018-08-02 Carlos
#		Fix js issue, a hidden field is added that "$('form input')" wrongly gets the hidden field, changed "$('form input')" to $('form input[name=value]') to get the dynamic input element for jEdit. 
#
# Date: 2018-07-31 Cameron
#       add course category for location
#
# Date: 2018-07-06 Vito
#       Add print button
#
# Date: 2010-12-06 Carlos
#		add Export button
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$liblocation_ui = new liblocation_ui();
$CurrentPageArr['Location'] = 1;

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Campus";'."\n";
$js.= '</script>'."\n";

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['Location'].$js;

$title = $Lang['SysMgr']['Location']['LocationList']; 
$TAGS_OBJ[] = array($title,"");

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','campus');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);


$linterface->LAYOUT_START(); 

### Include JS and CSS ###
$include_JS_CSS = $liblocation_ui->Include_JS_CSS();


## Get Tab
if (!isset($BuildingID) || $BuildingID=="")
{
	$libBuilding = new Building();
	$biuldingInfoArr = $libBuilding->Get_All_Building();
	$BuildingID = $biuldingInfoArr[0]['BuildingID'];
}

$buildingTab = '';
$buildingTab .= '<div id="BuildingTabDiv" class="table_board">'."\n";
	$buildingTab .= $liblocation_ui->Get_Building_Tab_Div($BuildingID)."\n";
$buildingTab .= '</div>'."\n";


## Toolbar and Serach Box
$toolbarDiv = '';
/*
$toolbarDiv .= '<div class="content_top_tool">'."\n";
	$toolbarDiv .= '<div class="Conntent_tool">'."\n";
		# New Location
		$toolbarDiv .= $linterface->GET_LNK_NEW("#TB_inline?height=".$liblocation_ui->thickBoxHeight."&width=".$liblocation_ui->thickBoxWidth."&inlineId=FakeLayer", 
		//		$Lang['SysMgr']['Location']['Toolbar']['NewLocation'], "js_Show_Room_Add_Edit_Layer('','')", "title=\"".$Lang['SysMgr']['Location']['New']['Location']."\"");
		# Import
		$toolbarDiv .= $linterface->GET_LNK_IMPORT("#", $Lang['SysMgr']['Location']['Toolbar']['Import']);
		# Export
		$toolbarDiv .= $linterface->GET_LNK_EXPORT("#", $Lang['SysMgr']['Location']['Toolbar']['Export']);
	$toolbarDiv .= '</div>'."\n";
	# Search Box Div
	$toolbarDiv .= $linterface->Get_Search_Box_Div("SearchKeyword")."\n";
	$toolbarDiv .= '<br style="clear:both" />'."\n";
$toolbarDiv .= '</div>'."\n";
*/


## Content Table Div
$contentTableDiv = "";

$contentTableDiv .= '<div id="ContentTableDiv">';
	$contentTableDiv .= $liblocation_ui->Get_Content_Table($BuildingID, $SearchKeyword);
$contentTableDiv .= '</div>';


## Add/Edit Building Form Create & Reset Button ##
$btnAddBuilding = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Building();", "NewBuildingSubmitBtn"));
$btnCancelBuilding = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Building_Info();", "NewBuildingCancelBtn"));

## Add/Edit Floor Form Create & Reset Button ##
$btnAddFloor = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Floor();", "NewFloorSubmitBtn"));
$btnCancelFloor = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Floor_Info();", "NewFloorCancelBtn"));


/* 22 May 2009 - Hide the warning message
## Warning Message Box about duplicated Code
$duplicatedCodeWarningDiv = '';
$duplicatedCodeWarningDiv .= '<div id="DuplicatedCodeWarning">';
	$duplicatedCodeWarningDiv .= $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Warning'].'</font>', $Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Index']);
$duplicatedCodeWarningDiv .= '</div>';
*/


?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<div id="IndexDebugArea"></div>
<form id="form1" name="form1" action="" method="post">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="40">
		<?=$linterface->GET_LNK_PRINT("javascript:goPrint()",$button_print,"","","",0)?>
		<?=$linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0)?>
		<?=$linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0)?>
		</td>
	</tr>
	<tr> 
		<td class="main_content">
			<?= $include_JS_CSS ?>
			<?= $toolbarDiv ?>
			<?= $duplicatedCodeWarningDiv ?>
			<?= $buildingTab ?>
			<?= $contentTableDiv ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<script>
// record the current display building in the index page
var curBuildingID = '<?=$BuildingID?>';

var jsShowHideSpeed = 'fast';

function js_Show_Sub_Location(TargetLocationLevelID, TargetLocationID)
{
	var curr_class = $("#zoom_"+TargetLocationLevelID).attr("class");
	if(curr_class == 'zoom_in') {
		$("#zoom_"+TargetLocationLevelID).attr("class","zoom_out");
		$.post(
			"ajax_display_sub_location.php", 
			{
				targetLocationLevelID : TargetLocationLevelID,
				targetLocationID : TargetLocationID
			},
			function(ReturnData)
			{
				//$("#sub_location_div_"+TargetLocationLevelID).html();
				$("#sub_location_div_"+TargetLocationLevelID).html(ReturnData);
				$("#sub_location_div_"+TargetLocationLevelID).show();
				initThickBox();
				js_Init_JEdit();
				js_Init_DND_Table();
			}
		);
	}
	if(curr_class == 'zoom_out'){
		$("#zoom_"+TargetLocationLevelID).attr("class","zoom_in");
		$("#sub_location_div_"+TargetLocationLevelID).hide();
	}	
}


/************************* Add/Edit Building Form Functions *************************/
function js_Add_Building_Row() {
	if (!document.getElementById('GenAddBuildingRow')) {
		var TableBody = document.getElementById("BuildingInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddBuildingRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddBuildingRow";
		
		// Code
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="BuildingCode" id="BuildingCode" value="" class="textbox" maxlength="<?=$liblocation_ui->Code_Maxlength?>"/>';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		//Barcode
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="BuildingBarcode" id="BuildingBarcode" value="" class="textbox" maxlength="<?=$liblocation_ui->Code_Maxlength?>"/>';
			tempInput += '<div id="BarcodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="BuildingTitleEn" id="BuildingTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="TitleEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell3 = NewRow.insertCell(3);
		var tempInput = '<input type="text" name="BuildingTitleCh" id="BuildingTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="TitleChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell3.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell4 = NewRow.insertCell(4);
		var temp = '<?=$btnAddBuilding?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelBuilding?>';
		NewCell4.innerHTML = temp;
		
		$('input#BuildingCode').focus();
		
		// Code validation
		js_Add_KeyUp_Code_Checking("Building", "BuildingCode", "CodeWarningDiv_New");
		js_Add_KeyUp_Barcode_Checking("Building", "BuildingBarcode", "BarcodeWarningDiv_New");
		js_Add_KeyUp_Blank_Checking('BuildingTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
		js_Add_KeyUp_Blank_Checking('BuildingTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
	}
}
function js_Clear_Temp_Building_Info() {
	// Reset
	//$('#BuildingCode').val('');
	//$('#BuildingTitleEn').val('');
	//$('#BuildingTitleCh').val('');
	
	// Delete the Row
	var TableBody = document.getElementById("BuildingInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddBuildingRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}
function js_Insert_Building() {
	var Code = encodeURIComponent(Trim($('#BuildingCode').val()));
	var Barcode = encodeURIComponent(Trim($('#BuildingBarcode').val()));
	var TitleEn = encodeURIComponent(Trim($('#BuildingTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#BuildingTitleCh').val()));
	
	if (js_Is_Input_Blank('BuildingCode', 'CodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>'))
	{
		$('#BuildingCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('BuildingBarcode', 'BarcodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode']?>'))
	{
		$('#BuildingBarcode').focus();
		return false;
	}
	if (js_Is_Input_Blank('BuildingTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>'))
	{
		$('#BuildingTitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('BuildingTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>'))
	{
		$('#BuildingTitleCh').focus();
		return false;
	}
	
	/*
	if (!check_text(document.getElementById('BuildingCode'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterCode']?>'))
		return false;
	if (!check_text(document.getElementById('BuildingTitleEn'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleEn']?>'))
		return false;
	if (!check_text(document.getElementById('BuildingTitleCh'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleCh']?>'))
		return false;
	*/
	
	$.post(
		"ajax_validate_code.php", 
		{
			RecordType: "Building",
			InputValue: Trim($('#BuildingCode').val()),
			TargetID: ''
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				$.post(
					"ajax_validate_barcode.php",
					{
						RecordType: "Building",
						InputValue: Trim($('#BuildingBarcode').val()),
						TargetID: ''
					},
					function(ReturnData)
					{
						if(ReturnData == "")
						{
							// valid code
							$('#CodeWarningDiv_New').hide(jsShowHideSpeed);
							
							// Input data valid => disable submit button and then add building
							$('#NewBuildingSubmitBtn').attr("disabled", "true");
							$('#NewBuildingCancelBtn').attr("disabled", "true");
							
							$.post(
								"ajax_new_location.php", 
								{ 
									RecordType: "Building",
									Code: Code,
									Barcode: Barcode,
									TitleEn: TitleEn,
									TitleCh: TitleCh
								},
								function(ReturnData)
								{
									if (curBuildingID == '')
									{
										$.post(
											"ajax_get_new_building.php", 
											{ },
											function(newBuildingID)
											{
												curBuildingID = newBuildingID;
												
												js_Reload_Building_Tab(curBuildingID);
												js_Reload_Content_Table(curBuildingID);
												
												// Show system messages
												js_Show_System_Message("Building", "new", ReturnData);
											}
										);
									}
									else
									{
										js_Reload_Building_Add_Edit_Table();
										js_Reload_Building_Tab(curBuildingID);
										js_Reload_Content_Table(curBuildingID);
										
										// Show system messages
										js_Show_System_Message("Building", "new", ReturnData);
									}
									
									
									
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						else
						{
							// invalid code => show warning
							$('#BarcodeWarningDiv_New').html(ReturnData);
							$('#BarcodeWarningDiv_New').show(jsShowHideSpeed);
							
							$('#BuildingBarcode').focus();
							return ;
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html(ReturnData);
				$('#CodeWarningDiv_New').show(jsShowHideSpeed);
				
				$('#BuildingCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}
function js_Delete_Building(BuildingID)
{
	if (confirm('<?=$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteBuilding']?>'))
	{
		$.post(
			"ajax_delete_location.php", 
			{ 
				RecordType: "Building",
				BuildingID: BuildingID
			},
			function(ReturnData)
			{
				if(ReturnData=='1'){
				js_Reload_Building_Add_Edit_Table();
				js_Reload_Building_Tab(curBuildingID);
				js_Reload_Content_Table(curBuildingID);
				
				// Show system messages
				js_Show_System_Message("Building", "delete", ReturnData);
			
				//$('#debugArea').html(ReturnData);
				}
			}
		);
	}
	
	return false;
}
function js_Update_Building(DivID, DBFieldName, UpdateValue)
{
	$.post(
		"ajax_update_location.php", 
		{ 
			RecordType: "Building",
			DivID: encodeURIComponent(DivID),
			DBFieldName: encodeURIComponent(DBFieldName),
			UpdateValue: encodeURIComponent(UpdateValue)
		},
		function(ReturnData)
		{
			js_Reload_Building_Add_Edit_Table();
			js_Reload_Building_Tab(curBuildingID);
			
			// Show system messages
			js_Show_System_Message("Building", "update", ReturnData);
				
			//$('#debugArea').html(ReturnData);
		}
	);
}
// click tab => reload whole page info
function js_Switch_Building_Tab(targetBuildingID)
{
	curBuildingID = targetBuildingID;
	js_Reload_Building_Tab(curBuildingID);
	js_Reload_Content_Table(curBuildingID);
}
/************************* End of Add/Edit Building Form Functions *************************/



/************************* Add/Edit Floor Form Functions *************************/
function js_Add_Floor_Row() {
	if (!document.getElementById('GenAddFloorRow')) {
		var TableBody = document.getElementById("FloorInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddFloorRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddFloorRow";
		
		// Code
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="FloorCode" id="FloorCode" value="" class="textbox" maxlength="<?=$liblocation_ui->Code_Maxlength?>" />';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Barcode
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="FloorBarcode" id="FloorBarcode" value="" class="textbox" maxlength="<?=$liblocation_ui->Code_Maxlength?>" />';
			tempInput += '<div id="BarcodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="FloorTitleEn" id="FloorTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="TitleEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell3 = NewRow.insertCell(3);
		var tempInput = '<input type="text" name="FloorTitleCh" id="FloorTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="TitleChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell3.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell4 = NewRow.insertCell(4);
		var temp = '<?=$btnAddFloor?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelFloor?>';
		NewCell4.innerHTML = temp;
		
		$('#FloorCode').focus();
		// Code validation
		js_Add_KeyUp_Code_Checking("Floor", "FloorCode", "CodeWarningDiv_New");
		js_Add_KeyUp_Barcode_Checking("Floor", "FloorBarcode", "BarcodeWarningDiv_New");
		js_Add_KeyUp_Blank_Checking('FloorTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
		js_Add_KeyUp_Blank_Checking('FloorTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
	}
}
function js_Clear_Temp_Floor_Info() {
	// Reset
	//$('#FloorCode').val('');
	//$('#FloorTitleEn').val('');
	//$('#FloorTitleCh').val('');
	
	// Delete the Row
	var TableBody = document.getElementById("FloorInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddFloorRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}
function js_Insert_Floor() {
	var Code = encodeURIComponent(Trim($('#FloorCode').val()));
	var Barcode = encodeURIComponent(Trim($('#FloorBarcode').val()));
	var TitleEn = encodeURIComponent(Trim($('#FloorTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#FloorTitleCh').val()));
	
	if (js_Is_Input_Blank('FloorCode', 'CodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>'))
	{
		$('#FloorCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('FloorBarcode', 'BarcodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode']?>'))
	{
		$('#FloorCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('FloorTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>'))
	{
		$('#FloorTitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('FloorTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>'))
	{
		$('#FloorTitleEn').focus();
		return false;
	}
		
	/*
	if (!check_text(document.getElementById('FloorCode'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterCode']?>'))
		return false;
	if (!check_text(document.getElementById('FloorTitleEn'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleEn']?>'))
		return false;
	if (!check_text(document.getElementById('FloorTitleCh'), '<?=$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleCh']?>'))
		return false;
	*/
	$.post(
		"ajax_validate_code.php", 
		{
			RecordType: "Floor",
			InputValue: Trim($('#FloorCode').val()),
			TargetID: '',
			BuildingID: curBuildingID
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				$.post(
					"ajax_validate_barcode.php",
					{
						RecordType: "Floor",
						InputValue: Trim($('#FloorBarcode').val()),
						TargetID: '',
						BuildingID: curBuildingID
					},
					function(ReturnData)
					{
						if(ReturnData == "")
						{
							// valid barcode
							$('#BarcodeWarningDiv_New').hide(jsShowHideSpeed);
							
							// Input data valid => disable submit button and then add Floor
							$('#NewFloorSubmitBtn').attr("disabled", "true");
							$('#NewFloorCancelBtn').attr("disabled", "true");
							
							$.post(
								"ajax_new_location.php", 
								{ 
									RecordType: "Floor",
									BuildingID: curBuildingID,
									Code: Code,
									Barcode: Barcode,
									TitleEn: TitleEn,
									TitleCh: TitleCh
								},
								function(ReturnData)
								{
									js_Reload_Floor_Add_Edit_Table();
									js_Reload_Content_Table(curBuildingID);
									
									// Show system messages
									js_Show_System_Message("Floor", "new", ReturnData);
										
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						else
						{
							// invalid code => show warning
							$('#BarcodeWarningDiv_New').html(ReturnData);
							$('#BarcodeWarningDiv_New').show(jsShowHideSpeed);
							
							$('#FloorBarcode').focus();
							return ;
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html(ReturnData);
				$('#CodeWarningDiv_New').show(jsShowHideSpeed);
				
				$('#FloorCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}
function js_Delete_Floor(LocationLevelID)
{
	if (confirm('<?=$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteFloor']?>'))
	{
		$.post(
			"ajax_delete_location.php", 
			{ 
				RecordType: "Floor",
				LocationLevelID: LocationLevelID
			},
			function(ReturnData)
			{
				if(ReturnData=='1'){
				js_Reload_Floor_Add_Edit_Table();
				js_Reload_Content_Table(curBuildingID);
				
				// Show system messages
				js_Show_System_Message("Floor", "delete", ReturnData);
				
				//$('#debugArea').html(ReturnData);
				}
			}
		);
	}
	
	return false;
}
function js_Update_Floor(DivID, DBFieldName, UpdateValue)
{
	$.post(
		"ajax_update_location.php", 
		{ 
			RecordType: "Floor",
			DivID: encodeURIComponent(DivID),
			DBFieldName: encodeURIComponent(DBFieldName),
			UpdateValue: encodeURIComponent(UpdateValue)
		},
		function(ReturnData)
		{
			js_Reload_Floor_Add_Edit_Table();
			js_Reload_Content_Table(curBuildingID);
			
			// Show system messages
			js_Show_System_Message("Floor", "update", ReturnData);
				
			//$('#debugArea').html(ReturnData);
		}
	);
}
/************************* End of Add/Edit Floor Form Functions *************************/



/************************* Add/Edit Room Form Functions *************************/
function Insert_Edit_Room(LocationID, KeepAdding)
{
	var SelectedBuildingID = $("#BuildingSelected option:selected").val();
	var SelectedLocationLevelID = $("#FloorSelected option:selected").val();
	var Code = encodeURIComponent(Trim($('#RoomCode').val()));
	var Barcode = encodeURIComponent(Trim($('#RoomBarcode').val()));
	var TitleEn = encodeURIComponent(Trim($('#RoomTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#RoomTitleCh').val()));
	var Description = encodeURIComponent(Trim($('#RoomDescription').val()));
	var Capacity = encodeURIComponent(Trim($('#Capacity').val()));

<?php if ($sys_custom['project']['HKPF']): ?>
	$('#CategoryID option').attr('selected',true);
	var CategoryID = encodeURIComponent(Trim($('#CategoryID').val()));
<?php else:?>
	var CategoryID = '';
<?php endif;?>
	
	if (js_Is_Input_Blank('BuildingSelected', 'BuildingWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['NoSelected']['Building']?>'))
	{
		$("#BuildingSelected").focus();
		return false;
	}
	$('#FloorWarningDiv').html('').hide();
	//if (js_Is_Input_Blank('FloorSelected', 'FloorWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['NoSelected']['Floor']?>'))
	if($('#FloorSelected').val() == '')
	{
		$('#FloorWarningDiv').html('<?=$Lang['SysMgr']['Location']['Warning']['NoSelected']['Floor']?>').show();
		$("#FloorSelected").focus();
		return false;
	}
	if (js_Is_Input_Blank('RoomCode', 'CodeWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>'))
	{
		$("#RoomCode").focus();
		return false;
	}
	if (js_Is_Input_Blank('RoomBarcode', 'BarcodeWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode']?>'))
	{
		$("#RoomBarcode").focus();
		return false;
	}
	if (js_Is_Input_Blank('RoomTitleEn', 'TitleEnWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>'))
	{
		$("#RoomTitleEn").focus();
		return false;
	}
	if (js_Is_Input_Blank('RoomTitleCh', 'TitleChWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>'))
	{
		$("#RoomTitleCh").focus();
		return false;
	}
	if(!(Capacity>0||Capacity=='')){
		$('#CapacityWarningDiv').html('<?=$Lang['SysMgr']['Location']['Warning']['Incorrect']['Capacity']?>');
		$('#CapacityWarningDiv').show();
		$("#Capacity").focus();
		return false;
	}
	
	$.post(
		"ajax_validate_code.php", 
		{
			RecordType: "Room",
			InputValue: $('#RoomCode').val(),
			TargetID: LocationID,
			BuildingID: SelectedBuildingID,
			LocationLevelID: SelectedLocationLevelID
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				$.post(
					"ajax_validate_barcode.php",
					{
						RecordType: "Room",
						InputValue: $('#RoomBarcode').val(),
						TargetID: LocationID,
						BuildingID: SelectedBuildingID,
						LocationLevelID: SelectedLocationLevelID
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code
							$('#BarcodeWarningDiv').hide(jsShowHideSpeed);
							
							// Add or Edit
							var action = '';
							var actionScript = '';
							if (LocationID == '')
							{
								actionScript = 'ajax_new_location.php';
								action = "new";
							}
							else
							{
								actionScript = 'ajax_update_location.php';
								action = "update";
							}
							
							Block_Thickbox();
							
							$.post(
								actionScript, 
								{ 
									RecordType: "Room",
									LocationID: LocationID,
									LocationLevelID: SelectedLocationLevelID,
									Code: Code,
									Barcode: Barcode,
									TitleEn: TitleEn,
									TitleCh: TitleCh,
									Description: Description,
									Capacity: Capacity,
									CategoryID: CategoryID
								},
								function(ReturnData)
								{
									js_Show_Room_Add_Edit_Layer(SelectedLocationLevelID, '');
									//js_Reload_Content_Table(curBuildingID);
									
									js_Get_Num_Of_Sub_Location(SelectedLocationLevelID);
									js_Reload_Sub_Location(SelectedLocationLevelID);
									
									UnBlock_Thickbox();
									
									// Show system messages
									js_Show_System_Message("Room", action, ReturnData);
									
									if (KeepAdding != 1)
										js_Hide_ThickBox();
									
									//$('#IndexDebugArea').html(ReturnData);
								}
							);
						}
						else
						{
							$('#BarcodeWarningDiv').html(ReturnData);
							$('#BarcodeWarningDiv').show(jsShowHideSpeed);
							$('#RoomBarcode').focus();							
						}
					}
				);

			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv').html(ReturnData);
				$('#CodeWarningDiv').show(jsShowHideSpeed);
				$('#RoomCode').focus();
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Delete_Room(LocationLevelID, LocationID)
{
	if (confirm('<?=$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteRoom']?>'))
	{
		Block_Document();
		
		$.post(
			"ajax_delete_location.php", 
			{ 
				RecordType: "Room",
				LocationID: LocationID
			},
			function(ReturnData)
			{
				if(ReturnData=='1'){
				//js_Reload_Content_Table(curBuildingID);
				js_Get_Num_Of_Sub_Location(LocationLevelID);
				js_Reload_Sub_Location(LocationLevelID);
				
				UnBlock_Document();
				
				// Show system messages
				js_Show_System_Message("Room", "delete", ReturnData);
				
				//$('#IndexDebugArea').html(ReturnData);
				}
			}
		);
	}
	
	return false;
}

/************************* End of Add/Edit Room Form Functions *************************/



/************************* Show/Reload UI Functions *************************/
// Show Add/Edit Building Layer
function js_Show_Building_Add_Edit_Layer(WithAdd) {
	Hide_Return_Message();
	
	$.post(
		"ajax_get_location_add_edit_layer.php", 
		{ RecordType: "Building" },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			//$('#debugArea').html(ReturnData);
			
			// initialize the add box if the user click the add button
			if (WithAdd == 1)
				js_Add_Building_Row();
		}
	);
}
// Reload Add/Edit Building Table
function js_Reload_Building_Add_Edit_Table() {
	$.post(
		"ajax_reload_location_add_edit_table.php", 
		{ RecordType: "Building" },
		function(ReturnData)
		{
			$('#BuildingInfoSettingLayer').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			//$('#debugArea').html(ReturnData);
		}
	);
}


// Show Add/Edit Floor Layer
function js_Show_Floor_Add_Edit_Layer() {
	Hide_Return_Message();
	
	$.post(
		"ajax_get_location_add_edit_layer.php", 
		{ 
			RecordType: "Floor",
			BuildingID: curBuildingID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			//$('#debugArea').html(ReturnData);
		}
	);
}
// Reload Add/Edit Floor Table
function js_Reload_Floor_Add_Edit_Table() {
	$.post(
		"ajax_reload_location_add_edit_table.php", 
		{ 
			RecordType: "Floor",
			BuildingID: curBuildingID
		},
		function(ReturnData)
		{
			$('#FloorInfoSettingLayer').html(ReturnData);
			js_Init_JEdit();
			js_Init_DND_Table();
			//$('#debugArea').html(ReturnData);
		}
	);
}


// Show Add/Edit Room Layer
function js_Show_Room_Add_Edit_Layer(LocationLevelID, LocationID) {
	
	curLocationLevelID = LocationLevelID;
	curLocationID = LocationID;
	
	Hide_Return_Message();
	
	$.post(
		"ajax_get_location_add_edit_layer.php", 
		{ 
			RecordType: "Room",
			BuildingID: curBuildingID,
			LocationLevelID: LocationLevelID,
			LocationID: LocationID
		},
		function(ReturnData)
		{
			//$('#TB_ajaxContent').ready(function(){
				$('#TB_ajaxContent').html(ReturnData);
				js_Add_KeyUp_Code_Checking("Room", "RoomCode", "CodeWarningDiv", LocationID);
				js_Add_KeyUp_Barcode_Checking("Room", "RoomBarcode", "BarcodeWarningDiv", LocationID);
				
				if (LocationID != '')
					js_Code_Validation("Room", $('#RoomCode').val(), LocationID, "CodeWarningDiv")
				
				if (LocationLevelID != '')
					$('#RoomCode').focus();
				else
					$('#FloorSelected').focus();
					
				// add listener to check blank input
				js_Add_KeyUp_Blank_Checking('RoomTitleEn', 'TitleEnWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
				js_Add_KeyUp_Blank_Checking('RoomTitleCh', 'TitleChWarningDiv', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
					
				//$('#debugArea').html(ReturnData);
			//});
		}
	);
}

// Reload Building Tab in the index page
function js_Reload_Building_Tab(targetBuildingID)
{
	$('#BuildingTabDiv').load(
		"ajax_reload_building_tab.php", 
		{ BuildingID: targetBuildingID },
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Reload Content Table
function js_Reload_Content_Table(targetBuildingID)
{
	$('#ContentTableDiv').load(
		"ajax_reload_content_table.php",
		{ BuildingID: targetBuildingID },
		function(ReturnData)
		{
			//Show_Hide_Code_Warning();
			initThickBox();
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

// Hide ThickBox
function js_Hide_ThickBox()
{
	tb_remove();
}

// Load system message
function js_Show_System_Message(RecordType, Action, Status)
{
	var returnMessage = '';
	
	if (RecordType == "Building")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Building']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Building']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Building']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Building']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Building']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Building']?>';
				break;
		}
	}
	else if (RecordType == "Floor")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Floor']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Floor']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Floor']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Floor']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Floor']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Floor']?>';
				break;
		}
	}
	else if (RecordType == "Room")
	{
		switch(Action)
		{
			case "new":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Room']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Room']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Room']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Room']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Room']?>' : '<?=$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Room']?>';
				break;
		}
	}
	
	Get_Return_Message(returnMessage);
}

function js_Update_Floor_Selection()
{
	var SelectedBuildingID = $('#BuildingSelected').val();
	$('#FloorSelectionDiv').hide();
	
	$('#FloorSelectionDiv').load(
		"ajax_reload_selection.php", 
		{ 
			RecordType: 'Floor_Selection_For_Add_Edit_Room',
			BuildingID: SelectedBuildingID
		},
		function(ReturnData)
		{
			$('#FloorSelectionDiv').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

/* 22 May 2009 - Hide the warning message
function Show_Hide_Code_Warning()
{
	// Declared in liblocation_ui.php -> Get_Content_Table()
	var showWarning = $('#showWarningInput').val();
	
	if (showWarning == 1)
		$('#DuplicatedCodeWarning').show(jsShowHideSpeed);
	else
		$('#DuplicatedCodeWarning').hide(jsShowHideSpeed);
}
*/
/************************* End of Reload UI Functions *************************/





/************************* Initialization Functions *************************/
// jquery tablednd function
// form drag and drop
function js_Init_DND_Table() {
		
	var target_tBodiesIndex = 0;
	/*** Room Reordering in the index page ***/
	//var JQueryObj3 = $("#ContentTable");
	//var JQueryObj3 = $(".ContentTable");
	//var JQueryObj3 = $("#SubLocationTable");
	var JQueryObj = $(".common_table_list");
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			// Get the order string
			if(table.id == "BuildingInfoTable"){
				/*** Building Edit Form ***/
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					RecordOrder += rows[i].id+",";
				}
				//alert("RecordOrder = " + RecordOrder);
				
				$.post(
					"ajax_reorder_display_order.php", 
					{ 
						RecordType: "Building",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						// Reload the building info in the thickbox
						js_Reload_Building_Add_Edit_Table();
						js_Reload_Building_Tab(curBuildingID);
						
						// Show system messages
						//js_Show_System_Message("Building", "update", ReturnData);
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else if(table.id == "FloorInfoTable")
			{
				/*** Floor Edit Form ***/
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					RecordOrder += rows[i].id+",";
				}
				//alert("RecordOrder = " + RecordOrder);
				
				$.post(
					"ajax_reorder_display_order.php", 
					{ 
						RecordType: "Floor",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						// Reload the Floor info in the thickbox
						js_Reload_Floor_Add_Edit_Table();
						js_Reload_Content_Table(curBuildingID);
						
						// Show system messages
						//js_Show_System_Message("Floor", "update", ReturnData);
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				var rows = DroppedRow.parentNode.rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					RecordOrder += rows[i].id+",";
				}
				// Update database
				$.post(
					"ajax_reorder_display_order.php", 
					{ 
						RecordType: "Room",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						// Reload the index page
						//js_Reload_Content_Table(curBuildingID);	# disabled by Ronald (20091211)
						
						// Show system messages
						//js_Show_System_Message("Room", "update", ReturnData);
						
						//$('#IndexDebugArea').html(ReturnData);
						
						target_tBodiesIndex = 0;
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

function js_Init_JEdit()
{
	/************ Add/Edit Building Info ************/
	$('div.jEditCode').editable( 
		function(updateValue, settings) 
		{			
			var thisDivID = $(this).attr("id");
			var recordType = '';
			if (thisDivID.substring(0,8) == "Building")
				recordType = "Building";
			else if (thisDivID.substring(0,5) == "Floor")
				recordType = "Floor";
			else if (thisDivID.substring(0,4) == "Room")
				recordType = "Room";
			
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			var ElementObj = $(this);
			
			updateValue = $.trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate_code.php", 
					{
						RecordType: recordType,
						InputValue: updateValue,
						TargetID: targetID
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code
							$('#CodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
							
							// Update value
							if ($('#CodeWarningDiv_' + targetID).is(':visible'))
							{
								// not vaild
								$('#CodeWarningDiv_' + targetID).hide(jsShowHideSpeed);
								$(this)[0].reset();
							}
							else
							{
								// valid
								if (recordType == "Building")
									js_Update_Building(thisDivID, "Code", updateValue);
								else if (recordType == "Floor")
									js_Update_Floor(thisDivID, "Code", updateValue);
							}
						}
						else
						{
							// invalid code => show warning
							ElementObj[0].reset();
							$('#CodeWarningDiv_'+targetID).html('');
							$('#CodeWarningDiv_'+targetID).hide();
							//$('#CodeWarningDiv_'+targetID).html(ReturnData);
							//$('#CodeWarningDiv_'+targetID).show(jsShowHideSpeed);
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
				);
			}
			$(ElementObj[0]).html(updateValue);
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$liblocation_ui->Code_Maxlength?>",
			height: "20px"
		}
	); 
	// Code Validation
	$('div.jEditCode').keyup( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var recordType = '';
		
		if (divID.substring(0,8) == "Building")
			recordType = "Building";
		else if (divID.substring(0,5) == "Floor")
			recordType = "Floor";
		else if (divID.substring(0,4) == "Room")
			recordType = "Room";
			
		js_Code_Validation(recordType, inputValue, targetID, 'CodeWarningDiv_'+targetID);
	});
	$('div.jEditCode').click( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var recordType = '';
		
		if (divID.substring(0,8) == "Building")
			recordType = "Building";
		else if (divID.substring(0,5) == "Floor")
			recordType = "Floor";
		else if (divID.substring(0,4) == "Room")
			recordType = "Room";
			
		js_Code_Validation(recordType, inputValue, targetID, 'CodeWarningDiv_'+targetID);
	});
	
	// Barcode
	$('div.jEditBarcode').editable( 
		function(updateValue, settings) 
		{			
			var thisDivID = $(this).attr("id");
			var recordType = '';
			if (thisDivID.substring(0,8) == "Building")
				recordType = "Building";
			else if (thisDivID.substring(0,5) == "Floor")
				recordType = "Floor";
			else if (thisDivID.substring(0,4) == "Room")
				recordType = "Room";
			
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			var ElementObj = $(this);
			
			updateValue = $.trim(updateValue);
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// Empty or same Code => do nothing
				$('#BarcodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate_barcode.php", 
					{
						RecordType: recordType,
						InputValue: updateValue,
						TargetID: targetID
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code
							$('#BarcodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
							
							// Update value
							if ($('#BarcodeWarningDiv_' + targetID).is(':visible'))
							{
								// not vaild
								$('#BarcodeWarningDiv_' + targetID).hide(jsShowHideSpeed);
								$(this)[0].reset();
							}
							else
							{
								// valid
								if (recordType == "Building")
									js_Update_Building(thisDivID, "Barcode", updateValue);
								else if (recordType == "Floor")
									js_Update_Floor(thisDivID, "Barcode", updateValue);
							}
						}
						else
						{
							// invalid code => show warning
							//$('#BarcodeWarningDiv_'+targetID).html(ReturnData);
							//$('#BarcodeWarningDiv_'+targetID).show(jsShowHideSpeed);
							
							ElementObj[0].reset();
							$('#BarcodeWarningDiv_'+targetID).html('');
							$('#BarcodeWarningDiv_'+targetID).hide();
										
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
					
				);
			}
			$(ElementObj[0]).html(updateValue);
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$liblocation_ui->Code_Maxlength?>",
			height: "20px"
		}
	); 
	// Code Validation
	$('div.jEditBarcode').keyup( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var recordType = '';
		
		if (divID.substring(0,8) == "Building")
			recordType = "Building";
		else if (divID.substring(0,5) == "Floor")
			recordType = "Floor";
		else if (divID.substring(0,4) == "Room")
			recordType = "Room";
			
		js_Barcode_Validation(recordType, inputValue, targetID, 'BarcodeWarningDiv_'+targetID);
	});
	$('div.jEditBarcode').click( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var recordType = '';
		
		if (divID.substring(0,8) == "Building")
			recordType = "Building";
		else if (divID.substring(0,5) == "Floor")
			recordType = "Floor";
		else if (divID.substring(0,4) == "Room")
			recordType = "Room";
			
		js_Barcode_Validation(recordType, inputValue, targetID, 'BarcodeWarningDiv_'+targetID);
	});
	
	// TitleEn
	$('div.jEditTitleEn').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleEnWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				if (thisDivID.substring(0,8) == "Building")
					js_Update_Building(thisDivID, "NameEng", updateValue);
				else if (thisDivID.substring(0,5) == "Floor")
					js_Update_Floor(thisDivID, "NameEng", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleEn').keyup( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleEnWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	
	// TitleCh
	$('div.jEditTitleCh').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleChWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				if (thisDivID.substring(0,8) == "Building")
					js_Update_Building(thisDivID, "NameChi", updateValue);
				else if (thisDivID.substring(0,5) == "Floor")
					js_Update_Floor(thisDivID, "NameChi", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleCh').keyup( function() {
		var inputValue = $('form input[name=value]').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleChWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	/************ End of Add/Edit Building Info ************/
}
/************************* End of Initialization Functions *************************/



/************************* Code Validation *************************/
function js_Code_Validation(RecordType, InputValue, TargetID, WarningDivID)
{
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>');
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$('#' + WarningDivID).hide(jsShowHideSpeed);
		
		var BuildingID = '';
		var LocationLevelID = '';
		if (RecordType == 'Room')
		{
			BuildingID = $("#BuildingSelected option:selected").val();
			LocationLevelID = $("#FloorSelected option:selected").val();
		}
		else if (RecordType == 'Floor')
		{
			BuildingID = curBuildingID;
		}
		
		$.post(
			"ajax_validate_code.php", 
			{
				RecordType: RecordType,
				InputValue: InputValue,
				TargetID: TargetID,
				BuildingID: BuildingID,
				LocationLevelID: LocationLevelID
			},
			function(ReturnData)
			{
				if (ReturnData == "")
				{
					// valid code
					$('#' + WarningDivID).hide(jsShowHideSpeed);
				}
				else
				{
					// invalid code => show warning
					$('#' + WarningDivID).html(ReturnData);
					$('#' + WarningDivID).show(jsShowHideSpeed);
					return false;
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}
function js_Add_KeyUp_Code_Checking(RecordType, ListenerObjectID, WarningDivID, LocationID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null)
			targetID = '';
			
		if (!isNaN(LocationID))
			targetID = LocationID;
			
		js_Code_Validation(RecordType, inputValue, targetID, WarningDivID);
	});
}
function js_Barcode_Validation(RecordType, InputValue, TargetID, WarningDivID)
{
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode']?>');
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$('#' + WarningDivID).hide(jsShowHideSpeed);
		
		var BuildingID = '';
		var LocationLevelID = '';
		if (RecordType == 'Room')
		{
			BuildingID = $("#BuildingSelected option:selected").val();
			LocationLevelID = $("#FloorSelected option:selected").val();
		}
		else if (RecordType == 'Floor')
		{
			BuildingID = curBuildingID;
		}
		
		$.post(
			"ajax_validate_barcode.php", 
			{
				RecordType: RecordType,
				InputValue: InputValue,
				TargetID: TargetID,
				BuildingID: BuildingID,
				LocationLevelID: LocationLevelID
			},
			function(ReturnData)
			{
				if (ReturnData == "")
				{
					// valid code
					$('#' + WarningDivID).hide(jsShowHideSpeed);
				}
				else
				{
					// invalid code => show warning
					$('#' + WarningDivID).html(ReturnData);
					$('#' + WarningDivID).show(jsShowHideSpeed);
					
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}
function js_Add_KeyUp_Barcode_Checking(RecordType, ListenerObjectID, WarningDivID, LocationID)
{
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null)
			targetID = '';
			
		if (!isNaN(LocationID))
			targetID = LocationID;
			
		js_Barcode_Validation(RecordType, inputValue, targetID, WarningDivID);
	});
}

/************************* End of Code Validation *************************/

function js_Reload_Sub_Location(TargetLocationLevelID)
{
	var curr_class = $("#zoom_"+TargetLocationLevelID).attr("class");
	
	$.post(
		"ajax_reload_sub_location.php", 
		{
			targetLocationLevelID : TargetLocationLevelID
		},
		function(ReturnData)
		{
			$("#sub_location_div_"+TargetLocationLevelID).html();
			$("#sub_location_div_"+TargetLocationLevelID).html(ReturnData);
			initThickBox();
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
	$("#zoom_"+TargetLocationLevelID).attr("class","zoom_out");
	$("#sub_location_div_"+TargetLocationLevelID).show();
}

function js_Get_Num_Of_Sub_Location(TargetLocationLevelID)
{
	$.post(
		"ajax_get_num_of_sub_location.php", 
		{
			targetLocationLevelID : TargetLocationLevelID
		},
		function(ReturnData)
		{
			//$(".num_of_room_"+TargetLocationLevelID).html();
			//$(".num_of_room_"+TargetLocationLevelID).html(ReturnData);
			$("#NumOfSubLocation_"+TargetLocationLevelID).html();
			$("#NumOfSubLocation_"+TargetLocationLevelID).html(ReturnData);
			initThickBox();
			js_Init_JEdit();
			js_Init_DND_Table();
		}
	);
}

function goPrint()
{
	if(curBuildingID > 0){
		var formObj_print = document.form1;
		var action_print = formObj_print.action;
		var target_print = formObj_print.target;
		formObj_print.action="print.php?BuildingID="+curBuildingID;
		formObj_print.target="_blank";
		formObj_print.submit();
		formObj_print.action = action;
		formObj_print.target = target_print;
	}
}

function goExport()
{
	if(curBuildingID > 0){
		var formObj = document.form1;
		var action = formObj.action;
		var target = formObj.target;
		formObj.action="export.php?BuildingID="+curBuildingID;
		//formObj.target="_blank";
		formObj.submit();
		formObj.action = action;
		formObj.target = target;
	}
}


function goImport()
{
	if (curBuildingID == '' || isNaN(curBuildingID)) {
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Building']?>');
	}
	else {
		var formObj = document.form1;
		var action = formObj.action;
		var target = formObj.target;
		
		formObj.action="import_step1.php?BuildingID="+curBuildingID;
		formObj.submit();
		formObj.action = action;
		formObj.target = target;
	}	
}

js_Init_DND_Table();
/* 22 May 2009 - Hide the warning message
Show_Hide_Code_Warning();
*/
</script>