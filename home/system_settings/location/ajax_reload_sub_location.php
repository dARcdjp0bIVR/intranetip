<?php
// using : 
/**
 * Date: 2018-08-15 (Henry)
 *       fixed sql injection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$li = new libdb();
$linventory = new libinventory();
$linterface = new interface_html();

$targetLocationLevelID = IntegerSafe($targetLocationLevelID);

//$sql = "SELECT LocationID, Code, ".$linventory->getInventoryNameByLang().", Description FROM INVENTORY_LOCATION WHERE LocationID IN ($targetLocationID) AND RecordStatus = 1 ORDER BY DisplayOrder";
$sql = "SELECT LocationID, Code, Barcode, ".$linventory->getInventoryNameByLang().", Description, IF(Capacity IS NULL,'".	$Lang['General']['EmptySymbol']."',Capacity) AS Capacity FROM INVENTORY_LOCATION WHERE LocationLevelID IN ($targetLocationLevelID) AND RecordStatus = 1 ORDER BY DisplayOrder";
$result = $li->returnArray($sql,5);

//$SubLocatioinTable .= '<table border=0; class="common_table_list" id="SubLocationTable_$targetLocationLevelID">'."\n";

if(sizeof($result)>0){
	
	$SubLocatioinTable .= '<table border=0; class="common_table_list" id="SubLocationTable">'."\n";
	$SubLocatioinTable .= '<col align="left" style="width:20%" />'."\n";
	$SubLocatioinTable .= '<col align="left" style="width:20%" />'."\n";
	$SubLocatioinTable .= '<col align="left" style="width:20%" />'."\n";
	$SubLocatioinTable .= '<col align="left" />'."\n";
	$SubLocatioinTable .= '<col align="left" style="width:127px" />'."\n";
	$SubLocatioinTable .= "<tr class='nodrag nodrop'>";
	$SubLocatioinTable .= '<th class="sub_row_top">'.$Lang['SysMgr']['Location']['Code'].'</th>'."\n";
	$SubLocatioinTable .= '<th class="sub_row_top">'.$Lang['SysMgr']['Location']['Barcode'].'</th>'."\n";
	$SubLocatioinTable .= '<th class="sub_row_top">'.$Lang['SysMgr']['Location']['Title'].'</th>'."\n";
	$SubLocatioinTable .= '<th class="sub_row_top">'.$Lang['SysMgr']['Location']['Description'].'</th>'."\n";
	$SubLocatioinTable .= '<th class="sub_row_top">'.$Lang['SysMgr']['Location']['Capacity'].'</th>'."\n";
	$SubLocatioinTable .= '<th class="sub_row_top">&nbsp;</th>'."\n";
	$SubLocatioinTable .= '</tr>';
	
	for($i=0; $i<sizeof($result); $i++){
		list($SubLocationID, $SubLocationCode, $SubLocationBarcode, $SubLocationName, $SubLocationDescription,$SubLocationCapacity) = $result[$i];
		
		if($SubLocationName == ""){
			$SubLocationName = "&nbsp;";
		}
		if($SubLocationDescription == ""){
			$SubLocationDescription = "&nbsp;";
		}
		
		$roomObj = new Room($SubLocationID);
		$canDelete = $roomObj->Check_Can_Delete();
		
		$SubLocatioinTable .= '<tr class="sub_row" id="RoomRow_'.$SubLocationID.'">';
		$SubLocatioinTable .= '<td>'.$SubLocationCode.'</td>';
		$SubLocatioinTable .= '<td>'.$SubLocationBarcode.'</td>';
		$SubLocatioinTable .= '<td>'.$SubLocationName.'</td>';
		$SubLocatioinTable .= '<td>'.nl2br($SubLocationDescription).'</td>';
		$SubLocatioinTable .= '<td>'.$SubLocationCapacity.'</td>';
		$SubLocatioinTable .= '<td class="Dragable">'."\n";
		$SubLocatioinTable .= '<div class="table_row_tool">'."\n";
		# Move
		$SubLocatioinTable .= $linterface->GET_LNK_MOVE("#", $Lang['SysMgr']['Location']['Reorder']['Room']);
		# Edit Room Button
		$SubLocatioinTable .= $linterface->Get_Thickbox_Link( "450px", "450px", "edit_dim", 
		$Lang['SysMgr']['Location']['Edit']['Room'], "js_Show_Room_Add_Edit_Layer('$targetLocationLevelID', '$SubLocationID'); return false;");
		if ($canDelete)
		{
			# Delete Room Button
			$SubLocatioinTable .= $linterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Location']['Delete']['Room'], "js_Delete_Room('$targetLocationLevelID', '$SubLocationID'); return false;");
		}
		$SubLocatioinTable .= '</div>'."\n";
		$SubLocatioinTable .= '</td>'."\n";
		$SubLocatioinTable .= '</tr>';
		
	}
	$SubLocatioinTable .= '</table>';
}

echo $SubLocatioinTable;
intranet_closedb();
?>