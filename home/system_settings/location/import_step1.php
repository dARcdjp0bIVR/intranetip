<?php
// Modifying by:  

############# Change Log [Start] ################
#
#   Date:   2018-08-09 Cameron
#           add Sub-location Suited Course Category for HKPF
#
#	Date:	2016-06-024 Carlos - Added checking to verify the validaty of BuildingID.
#
# 	Date:	2015-10-16	Kenneth Yau
#		- Edited Import Column by libinterface.php->Get_Import_Page_Column_Display()
#		- Aligment of Style with v30 in Table
#
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lbuilding = new Building($BuildingID);
if(!(isset($lbuilding->BuildingID) && $lbuilding->BuildingID >0 && $lbuilding->BuildingID == $BuildingID))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
// $lc = new liblocation();

### Title / Menu
$CurrentPageArr['Location'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['Location'];
$title = $Lang['SysMgr']['Location']['LocationList']; 
$TAGS_OBJ[] = array($title,"");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
if ($sys_custom['project']['HKPF']) {
    $csvFileName = 'sample_campus_hkpf.csv';
}
else {
    $csvFileName = 'sample_campus.csv';
}
$csvFile = "<a href='". GET_CSV($csvFileName) ."' class='tablelink'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";


# data column
$DataColumnPropertyArr = array(3,3,3,3,3,3,3,3,3);
$columnTitleAry = $Lang['SysMgr']['Location']['ImportColumn'];
if ($sys_custom['project']['HKPF']) {
    $columnTitleAry[] = $Lang['SysMgr']['Location']['AdditionalImportColumn']['CourseCategory'];
}
$displayColumn = $linterface->Get_Import_Page_Column_Display($columnTitleAry, $DataColumnPropertyArr);



?>
<br />
<form name="form1" method="post" action="import_step2.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	<tr>
		<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td>
       		<br />
			<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td class="field_title" align="left"><?=$Lang['AccountMgmt']['ActionType']?></td>
					<td class="tabletext">
						<?=$linterface->Get_Radio_Button("action_type1", "action_type", "1", 1, "", $Lang['AccountMgmt']['ActionType.Add']).'&nbsp;'?>
						<?=$linterface->Get_Radio_Button("action_type2", "action_type", "2", 0, "", $Lang['AccountMgmt']['ActionType.Update'])?>
					</td>
				</tr>
				<tr>
					<td class="field_title" align="left"><?=$Lang['SysMgr']['Location']['Building']?></td>
					<td class="tabletext"><?=$lbuilding->Get_Name($intranet_session_language)?></td>
				</tr>
				<tr>
					<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				</tr>
				<tr>
					<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
					<td class="tabletext"><?=$displayColumn?></td>
				</tr>
				
			</table>
			</td>
		</tr>   
		<tr>
			<td colspan="2">  
				<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
					<tr>
						<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
					</tr>
					<tr>
				        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				    </tr>
			    </table>                                
			</td>
		</tr> 
		</table>
		</td>
	</tr>
	
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?BuildingID=$BuildingID'")?>
		</td>
	</tr>
</table>

<input type="hidden" name="BuildingID" value="<?=$BuildingID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
