<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_opendb();

$liblocation_ui = new liblocation_ui();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

if ($RecordType == "Floor_Selection_For_Add_Edit_Room")
{
	$BuildingID = $_REQUEST['BuildingID'];
	$returnString = $liblocation_ui->Get_Floor_Selection($BuildingID, "", "FloorSelected", '', 0, 1);
}
else if ($RecordType == "Reload_Floor_Selection")	// api in script.js
{
	$BuildingID = $_REQUEST['BuildingID'];
	$js_PATH_WRT_ROOT = stripslashes($_REQUEST['js_PATH_WRT_ROOT']);
	
	$FloorSelID = stripslashes($_REQUEST['FloorSelID']);
	$FloorSelFirstTitle = stripslashes($_REQUEST['FloorSelFirstTitle']);
	$RoomSelTrID = stripslashes($_REQUEST['RoomSelTrID']);
	$RoomSelDivID = stripslashes($_REQUEST['RoomSelDivID']);
	$RoomSelID = stripslashes($_REQUEST['RoomSelID']);
	$RoomSelFirstTitle = stripslashes($_REQUEST['RoomSelFirstTitle']);
	$RoomSelOnchange = stripslashes($_REQUEST['RoomSelOnchange']);
	$RoomSelIsMultiple = stripslashes($_REQUEST['RoomSelIsMultiple']);
	
	$NoFirst = ($FloorSelFirstTitle == '')? 1 : 0;
	
	$returnString = $liblocation_ui->Get_Floor_Selection(
							$BuildingID, 
							$SelectedLocationLevelID = "", 
							$FloorSelID, 
							"js_Change_Room_Selection('$js_PATH_WRT_ROOT', '$FloorSelID', '$RoomSelTrID', '$RoomSelDivID', '$RoomSelID', '$RoomSelFirstTitle', '$RoomSelOnchange', '$RoomSelIsMultiple')",
							$NoFirst,
							$ShowNoRoomFloor = 0,
							$FloorSelFirstTitle
							);
	
}
else if ($RecordType == "Reload_Room_Selection")	// api in script.js
{
	$LocationLevelID = $_REQUEST['LocationLevelID'];
	$RoomSelID = $_REQUEST['RoomSelID'];
	$RoomSelFirstTitle = stripslashes($_REQUEST['RoomSelFirstTitle']);
	$RoomSelOnchange = stripslashes($_REQUEST['RoomSelOnchange']);
	$RoomSelIsMultiple = stripslashes($_REQUEST['RoomSelIsMultiple']);
	
	$NoFirst = ($RoomSelFirstTitle == '')? 1 : 0;
	
	$returnString = $liblocation_ui->Get_Room_Selection(
							$LocationLevelID, 
							$SelectedLocationID = "", 
							$RoomSelID, 
							$RoomSelOnchange, 
							$NoFirst,
							$RoomSelFirstTitle,
							$RoomSelIsMultiple
							);
							
	if ($RoomSelIsMultiple)
	{
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$libinterface = new interface_html();
		$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$RoomSelID');", "SelectAllBtn");
	}
}

echo $returnString;


intranet_closedb();

?>