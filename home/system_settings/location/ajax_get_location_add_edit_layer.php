<?php
// using :
/**
 * 2018-08-10 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_opendb();

$liblocation_ui = new liblocation_ui();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

if ($RecordType == "Building")
{
	$returnDiv = $liblocation_ui->Get_Building_Add_Edit_Form_Div();
}
else if ($RecordType == "Floor")
{
	$BuildingID = IntegerSafe(stripslashes($_REQUEST['BuildingID']));
	$returnDiv = $liblocation_ui->Get_Floor_Add_Edit_Form_Div($BuildingID);
}
else if ($RecordType == "Room")
{
	$BuildingID = IntegerSafe(stripslashes($_REQUEST['BuildingID']));
	$LocationLevelID = IntegerSafe(stripslashes($_REQUEST['LocationLevelID']));
	$LocationID = IntegerSafe(stripslashes($_REQUEST['LocationID']));
	$returnDiv = $liblocation_ui->Get_Room_Add_Edit_Form_Div($LocationID, $LocationLevelID, $BuildingID);
}
else
{
	$returnDiv = "";
}

echo $returnDiv;


intranet_closedb();
?>