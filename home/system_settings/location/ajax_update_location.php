<?php
// using : 
/*
 *  2018-07-31 Cameron
 *      add locationCategory
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$DivID = stripslashes(urldecode($_REQUEST['DivID']));
$DBFieldName = stripslashes(urldecode($_REQUEST['DBFieldName']));
$UpdateValue = stripslashes(urldecode($_REQUEST['UpdateValue']));

$DivIDArr = explode('_', $DivID);
$LocationID = $DivIDArr[1];

$liblocation = NULL;
$DataArr = array();
$DataArr[$DBFieldName] = $UpdateValue;

# Initialize library
if ($RecordType == "Building")
{
	if ($LocationID != "")
		$liblocation = new Building($LocationID);
}
else if ($RecordType == "Floor")
{
	if ($LocationID != "")
		$liblocation = new Floor($LocationID);
}
else if ($RecordType == "Room")
{
	$LocationID = stripslashes($_REQUEST['LocationID']);
	$LocationLevelID = stripslashes($_REQUEST['LocationLevelID']);
	$Code = stripslashes(urldecode($_REQUEST['Code']));
	$Barcode = stripslashes(urldecode($_REQUEST['Barcode']));
	$TitleEn = stripslashes(urldecode($_REQUEST['TitleEn']));
	$TitleCh = stripslashes(urldecode($_REQUEST['TitleCh']));
	$Description = stripslashes(urldecode($_REQUEST['Description']));
	$Capacity = stripslashes(urldecode($_REQUEST['Capacity']));
	
	$DataArr = array();
	$DataArr['LocationLevelID'] = $LocationLevelID;
	$DataArr['Code'] = $Code;
	$DataArr['Barcode'] = $Barcode;
	$DataArr['NameEng'] = $TitleEn;
	$DataArr['NameChi'] = $TitleCh;
	$DataArr['Description'] = $Description;
	$DataArr['Capacity'] = $Capacity;
	
	if ($LocationID != "")
		$liblocation = new Room($LocationID);
	
}
else
{
	$success = false;
}

# Update Record
if ($liblocation != NULL) {
	$success = $liblocation->Update_Record($DataArr);
	
	if ($sys_custom['project']['HKPF'] && $success && $RecordType == "Room") {
	    $locationID = $LocationID;
	    $CategoryID = stripslashes(urldecode($_REQUEST['CategoryID']));
	    $categoryIDAry = explode(',', $CategoryID);
	    $result = array();
	    $liblocation->Start_Trans();
	    
	    $result[] = $liblocation->deleteLocationCategory($locationID);
	    
	    foreach((array) $categoryIDAry as $categoryID) {
	        if ($categoryID && $categoryID != 'null') {
	            $result[] = $liblocation->addLocationCategory($locationID, $categoryID);
	        }
	    }
	    
	    if (in_array(false, $result)) {
	        $liblocation->RollBack_Trans();
	    }
	    else {
	        $liblocation->Commit_Trans();
	    }
	}
	
}


echo $success;
intranet_closedb();

?>