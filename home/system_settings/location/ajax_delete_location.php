<?php
// using
/*
 *  2018-07-31 Cameron
 *      - also delete Location Category
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

# Initialize library
if ($RecordType == "Building")
{
	$BuildingID = $_REQUEST['BuildingID'];
	if ($BuildingID != "")
		$liblocation = new Building($BuildingID);
		$recordID = $BuildingID;	
}
else if ($RecordType == "Floor")
{
	$LocationLevelID = $_REQUEST['LocationLevelID'];
	if ($LocationLevelID != "")
		$liblocation = new Floor($LocationLevelID);	
		$recordID = $LocationLevelID;
}
else if ($RecordType == "Room")
{
	$LocationID = $_REQUEST['LocationID'];
	if ($LocationID != "")
		$liblocation = new Room($LocationID);
		$recordID = $LocationID;	
}
else
{
	$success = false;
}

# Insert Record
if ($liblocation != NULL){
	//$success = $liblocation->Delete_Record();
	$liblocation->Start_Trans();
	$deleteSuccess = $liblocation->Delete_Record();
	
	if ($sys_custom['project']['HKPF'] && $RecordType == "Room") {
	    $deleteLocationCategory = $liblocation->deleteLocationCategory($LocationID);
	    $deleteSuccess = $deleteSuccess & $deleteLocationCategory;
	}
	
	if($deleteSuccess){
		$addDeleteLogsuccess = $liblocation->Add_Campus_Delete_Log($recordID,$RecordType);
		if ($addDeleteLogsuccess == true){
			$success = '1';
			$liblocation-> Commit_Trans();
		}
		else{
			$success = '0';
			$liblocation-> RollBack_Trans();
		}
	}
	else{
		$success = '0';
		$liblocation-> RollBack_Trans();
	}
}
echo $success;
intranet_closedb();

?>
