<?php
// using : 

$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
//intranet_auth();
intranet_opendb();
$extraCardReaderObj = new liblocation_cardReader($ReaderID);

$linterface = new interface_html("popup.html");
$lebooking_ui	= new libebooking_ui();
//$linterface->hasAccessRight();

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Location']['CardReaderArr']['CardReader'];
$TAGS_OBJ[] = array($Lang['SysMgr']['Location']['CardReader']['ReaderInfo'], "");

$linterface->LAYOUT_START(); 

## Get parameters from form
if (isset($_GET['ReaderID'])) {
	$ReaderID = trim($_GET['ReaderID']);
}
else{
    $ReaderID = trim($_POST['ReaderID'][0]);
}

$LocationID = $_POST['LocationID'];
$Code = $_POST['ReaderCode'];
$ReportNameEng = $_POST['ReportNameEng'];
$ReportNameChi = $_POST['ReportNameChi'];
$Remark = $_POST['ReaderRemarks'];
$Status = $_POST['CardReaderStatus'];
$AccessRight = 'Internal';

$displayForm = $extraCardReaderObj->getDoorAccessEditPage($ReaderID, $LocationID, $AccessRight);

echo $displayForm;

?>
<script language="javascript">
function goBack() {
	window.location = 'index.php';
}

function CheckForm()
{
	var error_no = '';
	//Check if location is empty / used more than two times
	if($("#locationIDSelectionBox").val().Trim()=='')
	{
		$('div#locationIDWarningDiv').show();
		error_no++;
	}
	else{
		$.ajax({
		url:      "ajax_validate.php",
		type:     "POST",
		data:     $("#form1").serialize() + '&Action2=ValidateLocationID',
		async:		false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success: function(data)
				 {		
					 if(data==1)
					 {		
						 $('div#LocationIDInvalidWarningDiv').show();
						 $('div#locationIDWarningDiv').hide();
						 error_no++;
					 }
					 else
					 {
						 $('div#LocationIDInvalidWarningDiv').hide();
						 $('div#locationIDWarningDiv').hide();				
					 }
				 }
  		     });	
	}
	
	//Check if ReaderCode is empty / used more than two times
	if($("input#ReaderCode").val().Trim()=='')
	{
		$('div#readerIDWarningDiv').show();
		$("input#ReaderCode").focus();
		error_no++;
	}
	else{

		$.ajax({
		url:      "ajax_validate.php",
		type:     "POST",
		data:     $("#form1").serialize() + '&Action1=ValidateReaderCode',
		async:		false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success: function(data)
				 {		
					if(data==1)
					{	
						$('div#readerIDExistingWarningDiv').show();
						$('div#readerIDWarningDiv').hide();
						error_no++;
					}
					else
					{
						$('div#readerIDExistingWarningDiv').hide();	
						$('div#readerIDWarningDiv').hide();
							
					}
				}
  		    });
	}
	
	//Check if ReaderNameEng is empty / used more than two times
	if($("input#ReaderNameEng").val().Trim()=='')
	{
		$('div#readerNameEngWarningDiv').show();
		$("input#ReaderNameEng").focus();
		error_no++;
	}else{
		$('div#readerNameEngWarningDiv').hide();
	
	}
	
	//Check if ReaderNameChi is empty / used more than two times
	if($("input#ReaderNameChi").val().Trim()=='')
	{
		$('div#readerNameChiWarningDiv').show();
		$("input#ReaderNameChi").focus();
		error_no++;
	}else{
		$('div#readerNameChiWarningDiv').hide();
	
	}
	
	//Check error no.
	if(error_no>0){
		return false;
	}
	else{
		return true;
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>