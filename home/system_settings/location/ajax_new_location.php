<?php
// using : 
/*
 *  2018-07-31 Cameron
 *      add locationCategory
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$DataArr = array();
$DataArr['Code'] = stripslashes(urldecode($_REQUEST['Code']));
$DataArr['Barcode'] = stripslashes(urldecode($_REQUEST['Barcode']));
$DataArr['NameEng'] = stripslashes(urldecode($_REQUEST['TitleEn']));
$DataArr['NameChi'] = stripslashes(urldecode($_REQUEST['TitleCh']));



# Initialize library
if ($RecordType == "Building")
{
	$liblocation = new Building();
}
else if ($RecordType == "Floor")
{
	$DataArr['BuildingID'] = stripslashes(urldecode($_REQUEST['BuildingID']));
	$liblocation = new Floor("", $DataArr['BuildingID']);
}
else if ($RecordType == "Room")
{
	$DataArr['LocationLevelID'] = stripslashes(urldecode($_REQUEST['LocationLevelID']));
	$DataArr['Description'] = stripslashes(urldecode($_REQUEST['Description']));
	$DataArr['Capacity'] = stripslashes(urldecode($_REQUEST['Capacity']));
	$liblocation = new Room("", $DataArr['LocationLevelID']);
}
else
{
	$success = false;
}

# Insert Record
if ($liblocation != NULL) {
	$success = $liblocation->Insert_Record($DataArr);
	
	if ($sys_custom['project']['HKPF'] && $success && $RecordType == "Room") {
	    $locationID = $liblocation->RecordID;
	    $CategoryID = stripslashes(urldecode($_REQUEST['CategoryID']));
	    $categoryIDAry = explode(',', $CategoryID);
	    
	    $result = array();
	    $liblocation->Start_Trans();
	    foreach((array) $categoryIDAry as $categoryID) {
	        if ($categoryID && $categoryID != 'null') {
	            $result[] = $liblocation->addLocationCategory($locationID, $categoryID);
	        }
	    }
	    
	    if (in_array(false, $result)) {
	        $liblocation->RollBack_Trans();
	    }
	    else {
	        $liblocation->Commit_Trans();
	    }
	}
}


echo $success;
intranet_closedb();

?>