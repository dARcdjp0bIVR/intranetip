<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libperiods.php");
include_once($PATH_WRT_ROOT."includes/libperiods_ui.php");
intranet_opendb();

$lperiods = new libperiods();

$lperiods->Start_Trans();

$sql = "UPDATE 
			INTRANET_PERIOD 
		SET 
			PeriodName = '$PeriodName',
			DateModified = NOW()
		WHERE
			PeriodID = $PeriodID";
$result['UpdatePeriodName'] = $lperiods->db_db_query($sql);

if (in_array(false,$result)) {
	$lperiods->RollBack_Trans();
}else{
	//$PeriodID = $lperiods->db_insert_id();
	$lperiods->Commit_Trans();
	//echo $PeriodID;
}

intranet_closedb();
?>