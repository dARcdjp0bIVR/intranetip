<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libperiods.php");
include_once($PATH_WRT_ROOT."includes/libperiods_ui.php");
intranet_opendb();

$lperiods = new libperiods();

if ($lperiods->checkPeriodName($SchoolYearID,$PeriodName))
	echo '1'; // Period Name is good to use
else
	echo '0'; // Period name is not good to use

intranet_closedb();
?>