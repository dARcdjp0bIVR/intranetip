<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.ronald.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

$lui = new interface_html();
$Json = new JSON_obj();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['CycleDay']['CycleDayTitle'];
$title = $Lang['SysMgr']['CycleDay']['PeriodTitle'];
$TAGS_OBJ[] = array($title,"");
$lui->LAYOUT_START(); 

?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?=$lcycleperiods_ui->getDefinePeriodTable();?>

<?
$lui->LAYOUT_STOP();

intranet_closedb();
?>

<script language="javascript">
var data_firstday_by_type = new Array();
	data_firstday_by_type[0] = new Array();
	data_firstday_by_type[1] = new Array();
	data_firstday_by_type[2] = new Array();
	<?
	for ($i=0; $i<26; $i++)
	{
	     ?>
	     data_firstday_by_type[0][data_firstday_by_type[0].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_numeric[$i]?>");
	     data_firstday_by_type[1][data_firstday_by_type[1].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_alphabet[$i]?>");
	     data_firstday_by_type[2][data_firstday_by_type[2].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_roman[$i]?>");
	     <?
	}
	?>
	
function newPeriodForm(PeriodStart, PeriodEnd, PeriodType)//create a new period input form
{
	wordXmlHttp = GetXmlHttpObject();
    
	var url = 'ajax_period_add.php';
	if(PeriodStart != null && PeriodEnd != null && PeriodType != null){
		var postContent = 'PeriodStart='+PeriodStart;
			postContent = postContent+'&PeriodEnd='+PeriodEnd;
			postContent = postContent+'&PeriodType='+PeriodType;
	}else{
		var postContent = '';
	}
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}


function changePeriodType(val)		//control which <div> show in add period form
{
	if(val == 1){
		$("#normal_additional_info").show();
		$("#import_additional_info").hide();
	}else if(val == 2){
		$("#normal_additional_info").hide();
		$("#import_additional_info").show();
	}else{
		$("#normal_additional_info").hide();
		$("#import_additional_info").hide();
	}
}

function changeFirstDaySelect()
{
	type = document.getElementById('CycleType').value;
	max_num = document.getElementById('PeriodDays').value;
	obj = document.getElementById('FirstDay');
	
	var current = obj.options.length;
	
	for (var j=current;j>0;j--) obj.options[j-1] = null;
	for (var i=0;i<max_num;i++)
	{
		obj.options[obj.options.length] = new Option(data_firstday_by_type[type][i][1],data_firstday_by_type[type][i][0]);
	}
}

function checkInputFields()		//input period form's validation
{
	if($('input#period_start').val()=="")
	{
		alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartEmpty'];?>");
		return false;
	}
	else
	{
		if($('input#period_end').val()=="")
		{
			alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndEmpty']?>");
			return false;
		}
		else
		{
			if(check_date((document.getElementById('period_start')),"<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid']?>"))
			{
				if(check_date((document.getElementById('period_end')),"<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid']?>"))
				{
					var boolCheckPoint1 = 1;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	if(boolCheckPoint1 == 1)
	{
		if(compareDate(document.getElementById('period_end').value,document.getElementById('period_start').value) == 1)
		{
			return true;
		}
		else
		{
			alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['StartPeriodLargerThenEndPeriod'];?>");
			return false;
		}
	}
}

function updateNewPeriod()		// use AJAX to insert a new period
{
	// get all the input variable
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	var targetPeriodType = $('#targetPeriodType').val();
	var targetCycleType = $('#CycleType :selected').val();
	var targetPeriodDays = $('#PeriodDays :selected').val();
	var targetFirstDay = $('#FirstDay :selected').val();
	
	var element = document.getElementById('satCount');
	var targetSatCount = 0;
	if(element.checked == true){
		var targetSatCount = 1;
	}
	
	if(checkInputFields())
	{
		$.post(
			"ajax_period_add_update.php",
			{
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"PeriodType":targetPeriodType,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount
			},
			function(responseText){
				if(responseText != 0){
					Get_Return_Message(responseText);			
					reloadMainContent('#add_period');
					window.top.tb_remove();
				}else{
					newPeriodForm(targetPeriodStart,targetPeriodEnd,targetPeriodType);
					alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
					//reloadMainContent('#add_period');
					//window.top.tb_remove();
				}
			}
		);
	}
}

function updateEditPeriod(){
	wordXmlHttp = GetXmlHttpObject();
	var targetPeriodID = $('input#period_id').val();
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	var targetPeriodType = $('#targetPeriodType').val();
	var targetCycleType = $('#CycleType :selected').val();
	var targetPeriodDays = $('#PeriodDays :selected').val();
	var targetFirstDay = $('#FirstDay :selected').val();
	if(targetPeriodType == 1){
		var element = document.getElementById('satCount');
		var targetSatCount = 0;
		if(element.checked == true){
			var targetSatCount = 1;
		}
	}

	if(checkInputFields())
	{
		$.post(
			"ajax_period_edit_update.php",
			{
				"PeriodID":targetPeriodID,
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount
			},
			function(responseText){
				if(responseText != 0){
					Get_Return_Message(responseText);
					reloadMainContent('#edit_period');
					window.top.tb_remove();
				}else{
					editPeriodForm(targetPeriodID);
					alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
					//reloadMainContent('#add_period');
					//window.top.tb_remove();
				}
			}
		);
	}
}

function checkDelete()			//check any checkbox is checked for delete action
{
	var cnt=0;
	$("input[name=period_id[]]").each(function(id)
	{
		if(this.checked){
			cnt++;
		}else{
			//continuous
		}
	});
	if(cnt != 0){
		if(confirm("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDelete'];?>"))
			return true;
		else
			return false;
	}else{
		alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['SelectAtLeaseOnePeriod'];?>");
		return false;
	}
}

function editPeriodForm()
{
	var period_id = '';
	$("input[name=period_id[]]").each(function(id)
	{
		if(this.checked){
			period_id = this.value;
		}else{
			//continuous
		}
	});
	
	wordXmlHttp = GetXmlHttpObject();
	
	var url = 'ajax_period_edit.php';
	var postContent = 'PeriodID='+period_id;
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			//Init_JEdit_Input("div.jEditInput");
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function removePeriodForm()
{
	if(checkDelete()){
		wordXmlHttp = GetXmlHttpObject();
		
		var arrPeriodID = new Array(); 
		$("input[name=period_id[]]").each(function(id)
		{
			if(this.checked){
				message = $("input[name=period_id[]]").get(id);
				arrPeriodID.push(message.value);
			}else{
				//continuous
			}
		});
		
		var url = 'ajax_period_remove.php';
		var postContent = 'PeriodID='+arrPeriodID;
		
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
				reloadMainContent('#PeriodInfoLayer');
				window.top.tb_remove();
			}
		};
		wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
}

function reloadMainContent(layer)
{
	$.post(
		"ajax_period_reload.php",
		{},
		function(responseText){
			$('#PeriodInfoLayer').html(responseText);
			initThickBox();
			initCheckBox();
		}
	);
}

function Init_JEdit_Input(objDom) {
	$(objDom).editable( 
    function(value, settings) {
    	wordXmlHttp = GetXmlHttpObject();
	   
			if (wordXmlHttp == null)
			{
				alert (errAjax);
				return;
			} 
		    
			var ElementObj = $(this);
			var url = 'ajax_update_form.php';
			var postContent = "YearID="+$(this).attr("id");
			postContent += "&YearName="+encodeURIComponent(value);
			wordXmlHttp.onreadystatechange = function() {
				if (wordXmlHttp.readyState == 4) {
					ElementObj.html(value);
				}
			};
			wordXmlHttp.open("POST", url, true);
			wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			wordXmlHttp.send(postContent);
		}, {
    tooltip		: "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
    event 		: "click",
    onblur		: "submit",
    type		: "text",     
    style		: "inherit",
    height		: "20px"
  }); 
}

function initCheckBox()
{
	$("#checkAll").click(function()				
	{
		var checked_status = this.checked;
		$("input[name=period_id[]]").each(function()
		{
			this.checked = checked_status;
		});
	});	
}

function gotoPage(page){
	var url = page;
	self.location=url;
}

function checkEdit(periodID){		// Check any checkbox checked for edit
	var cnt=0;
	$("input[name=period_id[]]").each(function(id)
	{
		if(this.checked){
			cnt++;
		}else{
			//continuous
		}
	});
	
	if(cnt == 1){
		$("#edit_button").show();
	}else{
		$("#edit_button").hide();
	}
}

$(document).ready(function()
{
	initCheckBox();
});

</script>