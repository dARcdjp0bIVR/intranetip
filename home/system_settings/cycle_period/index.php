<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
intranet_opendb();

$lui = new interface_html();
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalander']['SettingTitle'],"index.php",1);
$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"preview.php",0);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalander']['ModuleTitle'];

$returnMsg = urldecode($msg);
$lui->LAYOUT_START($returnMsg); 
?>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?=$lcycleperiods_ui->getProductionCycleDaysCalanderView();?>
<?
$lui->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/javascript">
//var jsMonth = <?=$month?>;
//var jsYear = <?=$year?>;

// action: 1 means next month; -1 means previous month
function updateMonthYear(actionType)
{
	jsMonth += actionType;
	
	if (jsMonth > 12)
	{
		jsMonth = 1;
		jsYear += 1;
	}
	else if (jsMonth < 1)
	{
		jsMonth = 12;
		jsYear -= 1;
	}
}

function ChangeSchoolYear(type, schoolYearID)
{
	//$("#loadingSpan").fadeIn("fast");
	Block_Input();
	$.post(
		"ajax_calander_view.php",
		{
			"type":type,
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			//$('#Production_Calander').html(responseText);
			//$("#loadingSpan").fadeOut("fast");
			Unblock_Input();
			$('#main_body').html(responseText);
			//initThickBox();
			//initCheckBox();
		}
	);
}

function changeView(type, schoolYearID)
{
	//$("#loadingSpan").fadeIn("fast");
	Block_Input();
	$.post(
		"ajax_change_view.php",
		{
			"type":type,
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			//$("#loadingSpan").fadeOut("fast");
			Unblock_Input();
		}
	);
}

//function editPeriod()
//{
//	var url = "period.php";
//	self.location=url;
//}

function gotoPage(page)
{
	var url = page;
	self.location=url;
}

// block ui function
{
var isBlocking = false;
function Block_Input(){
	if (!isBlocking) {
		var blockMsg = "<?=$Lang['General']['Loading']?>";
		$('body').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
	}
}

function Unblock_Input() {
	$('body').unblock();
	isBlocking = false;
}
}

$(document).ready(function() 
{
	//$("table.bookable, table.non_bookable").hover(function() {
	//$("table.bookable").hover(function() {
	//	var currentTableID = $(this).attr('id');
	//	$(this.cells[1]).addClass('showDragHandle').click( function() {
	//		alert("currentTableID = " + currentTableID);
	//	});
	//}, function() {
	//	var currentTableID = $(this).attr('id');
	//	$(this.cells[1]).removeClass('showDragHandle').unbind('click');
	//});
	
	$("#prevCalBtn").click( function() {
		updateMonthYear(-1);
		var serverScript = "generate_calendar_aj.php";
		
		// Disable arrors and display loading message
		$("#prevCalBtn").attr("disabled", "disabled");
		$("#nextCalBtn").attr("disabled", "disabled");
		//$("#loadingSpan").fadeIn("fast");
		Block_Input();
		
		$("#CalendarDiv").load(
			serverScript, 
			{	
				//month: jsMonth, 
				//year: jsYear,
				serverScript: serverScript,
				calandarDivName: "CalendarDiv"
			},
			function() {
				$("#prevCalBtn").removeAttr("disabled");
				$("#nextCalBtn").removeAttr("disabled");
				//$("#loadingSpan").fadeOut("fast");
				UnBlock_Input();
			}
		);
	});
	
	$("#nextCalBtn").click( function() {
		updateMonthYear(1);
		var serverScript = "generate_calendar_aj.php";
		
		// Disable arrors and display loading message
		$("#prevCalBtn").attr("disabled", "disabled");
		$("#nextCalBtn").attr("disabled", "disabled");
		//$("#loadingSpan").fadeIn("fast");
		Block_Input();
		
		$("#CalendarDiv").load(
			serverScript, 
			{	
				//month: jsMonth, 
				//year: jsYear,
				serverScript: serverScript,
				calandarDivName: "CalendarDiv"
			},
			function() {
				$("#prevCalBtn").removeAttr("disabled");
				$("#nextCalBtn").removeAttr("disabled");
				//$("#loadingSpan").fadeOut("fast");
				Unblock_Input();
			}
		);
	});
});    
</script>  