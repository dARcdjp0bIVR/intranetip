<?php

// using:
/*
 *  Organization Info Setup for ip30
 * 
 *  2020-06-16 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"])) {
    No_Access_Right_Pop_Up();
}

$result = array();

## get original school info and update SchoolName
$school_data_file = "$intranet_root/file/school_data.txt";
if (file_exists($school_data_file)) {
    $school_data = explode("\n", get_file_content($school_data_file));
//    $school_name = $school_data[0];   # old school_name
    $school_org = $school_data[1];
    $school_phone = $school_data[2];
    $school_address = '';
    for ($i = 3, $iMax = count($school_data); $i < $iMax; $i++) {
        $school_address .= $school_data[$i] . "\n";
    }
}
else {
    $school_org = '';
    $school_phone = '';
    $school_address = '';
}
$school_data = "$schoolName\n$school_org\n$school_phone\n".trim($school_address);
$lf = new libfilesystem();
$result[] = $lf->file_write(htmlspecialchars(trim(stripslashes($school_data))), $school_data_file);

$re_path = "/file/organization_info";
$path = $intranet_root.$re_path;
$target = "";
$backgroundImageFile = $_FILES['backgroundImage'];
$loginLogoFile = $_FILES['loginLogo'];
//$returnMsgKey = 'ImageUploadFail';      // default fail
$returnMsgKey = 'RecordSaveSuccess';      // default fail

if ($backgroundImageFile['name'] || $loginLogoFile['name']) {
    if (!is_dir($path))
    {
        $lf->folder_new($path);
    }

    $ls = new libgeneralsettings();

    if ($backgroundImageFile['name']) {
        $validExtensionAry = array('.jpg','.png');
        $ext = strtolower($lf->file_ext($backgroundImageFile['name']));
        if (in_array($ext, $validExtensionAry)) {
            $target = $path . "/organization_background_image" . $ext;
            if (is_file($target)) {
                $lf->file_remove($target);      // delete it first
            }
            move_uploaded_file($backgroundImageFile["tmp_name"], $target);
            $result[] = $ls->Save_General_Setting('OrganizationInfo', array('BackgroundImage' => 'organization_background_image' . $ext));
        }
    }

    if ($loginLogoFile['name']) {
        $validExtensionAry = array('.jpg','.png','.gif');
        $ext = strtolower($lf->file_ext($loginLogoFile['name']));
        if (in_array($ext, $validExtensionAry)) {
//            $target = $path . "/organization_logo" . $ext;
//            if (is_file($target)) {
//                $lf->file_remove($target);      // delete it first
//            }
//            move_uploaded_file($loginLogoFile["tmp_name"], $target);
//            $result[] = $ls->Save_General_Setting('OrganizationInfo', array('LoginLogo' => 'organization_logo' . $ext));
            $imgfile = $lf->file_read($intranet_root."/file/schoolbadge.txt");
            $oldImageFile = "$intranet_root/file/$imgfile";

            if (is_uploaded_file($loginLogoFile["tmp_name"])) {
                if (is_file($oldImageFile)) {
                    $lf->file_remove($oldImageFile);
                }
                $lf->lfs_copy($loginLogoFile["tmp_name"], "$intranet_root/file/".$loginLogoFile['name']);
                $lf->file_write(htmlspecialchars(trim(stripslashes($loginLogoFile['name']))), $intranet_root."/file/schoolbadge.txt");
            }
        }
    }
}
$returnMsgKey = in_array(false,$result) ? 'RecordSaveUnSuccess' : 'RecordSaveSuccess';

intranet_closedb();

header("location: index_ip30.php?returnMsgKey=".$returnMsgKey);

?>
