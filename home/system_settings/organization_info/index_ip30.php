<?php

// using:
/*
 *  Organization Info Setup for IP30
 *
 *  2020-08-26 Cameron
 *      - append timestamp to school badge and background image to prevent cache [case #E193663]
 *
 *  2020-06-16 Cameron
 *      - create this file
 */

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"])) {
    No_Access_Right_Pop_Up();
}


$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['OrganizationInfo'] = 1;

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass School Info";'."\n";
$js.= '</script>'."\n";

### Title ###
$TAGS_OBJ[] = array($Lang['Header']['Menu']['OrganizationInfo']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['OrganizationInfo'].$js;

$currBackgroundImage = '';
$currLoginLogo = '';
$schoolName = GET_SCHOOL_NAME();

$ls = new libgeneralsettings();
$settingAry = $ls->Get_General_Setting('OrganizationInfo',array("'BackgroundImage'", "'LoginLogo'"));

if (count($settingAry)) {
    if ($settingAry['BackgroundImage']) {
        $backgroundImage = "/file/organization_info/" . $settingAry['BackgroundImage'];
        if (is_file($intranet_root.$backgroundImage)) {
            $currBackgroundImage = $backgroundImage;
        }
    }
//    if ($settingAry['LoginLogo']) {
//        $loginLogo = "/file/organization_info/" . $settingAry['LoginLogo'];
//        if (is_file($intranet_root.$loginLogo)) {
//            $currLoginLogo = $loginLogo;
//        }
//    }
}

$lf = new libfilesystem();
$loginLogo = $lf->file_read($intranet_root . "/file/schoolbadge.txt");
if (is_file($intranet_root."/file/".$loginLogo)) {
    $currLoginLogo = "/file/" . $loginLogo;
}


$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<script>
$(document).ready(function(){

	$('#btnSubmit').click(function(){
	    var isFormValid = true;
        hideError();
        if ($('#schoolName').val() == '') {
            $('#schoolNameWarningDiv').html('<?php echo $Lang['SysMgr']['OrganizationInfo']['PleaseInputSchoolName'];?>');
            $('#schoolNameWarningDiv').removeClass('error_msg_hide').addClass('error_msg_show').show('fast');
            isFormValid = false;
        }

        if ($('#backgroundImage').val() != '') {
            var extension = Get_File_Ext(document.getElementById('backgroundImage').value);
            var f = $('#backgroundImage')[0].files[0];

            if ((extension != "jpg" && extension != "png") || extension == false) {
                $('div#backgroundImageWarningDiv').html('<?php echo $Lang['SysMgr']['OrganizationInfo']['InvalidFile'];?>');
                $('div#backgroundImageWarningDiv').removeClass('error_msg_hide').addClass('error_msg_show').show('fast');
                isFormValid = false;
            }
        }

        if ($('#loginLogo').val() != '') {
            var extension = Get_File_Ext(document.getElementById('loginLogo').value);
            var f = $('#loginLogo')[0].files[0];

            if ((extension != "gif" && extension != "jpg" && extension != "png") || extension == false) {
                $('div#loginLogoWarningDiv').html('<?php echo $Lang['SysMgr']['OrganizationInfo']['InvalidFile'];?>');
                $('div#loginLogoWarningDiv').removeClass('error_msg_hide').addClass('error_msg_show').show('fast');
                isFormValid = false;
            }
        }

    	if (isFormValid) {
            hideError();
            $('#form1').submit();
        }
	});

	$('#btnReset').click(function(){
        hideError();
        $('#backgroundImage').val('');
        $('#loginLogo').val('');
	});

	$('#deleteBackgroundImage').click(function(){
		if(confirm("<?php echo $Lang['SysMgr']['OrganizationInfo']['ConfirmDeleteBackgroundImage'];?>?")){
			window.location = "delete_organization_photos.php?type=1";
		}
	});

    $('#deleteLoginLogo').click(function(){
        if(confirm("<?php echo $Lang['SysMgr']['OrganizationInfo']['ConfirmDeleteLogo'];?>?")){
            window.location = "delete_organization_photos.php?type=2";
        }
    });

});

function hideError()
{
    $('#schoolNameWarningDiv').removeClass('error_msg_show').addClass('error_msg_hide').hide();
    $('div.fileUploadWarning').removeClass('error_msg_show').addClass('error_msg_hide').hide();
}

</script>

<form name="form1" id="form1" enctype="multipart/form-data" method="post" action="organization_info_update_ip30.php">
	<table class="form_table_v30">


        <tr>
            <td valign="top" nowrap="nowrap" class="field_title"><span class='tabletextrequire'>*</span>
                <?php echo $i_SettingsSchoolName;?>
            </td>
            <td class="tabletextremark" valign="top">
                <input type="text" name="schoolName" id="schoolName" value="<?php echo $schoolName;?>" class="textboxtext">
                <div id="schoolNameWarningDiv" class="error_msg_hide"></div>
            </td>
        </tr>

        <tr>
            <td valign="top" nowrap="nowrap" class="field_title">
                <?php echo $Lang['SysMgr']['OrganizationInfo']['Logo'];?>
            </td>
            <td class="tabletextremark" valign="top">
                <input type="file" name="loginLogo" id="loginLogo" class="file"><br /><?php ?>
                <br><?php echo $Lang['SysMgr']['OrganizationInfo']['LogoInstruction2'];?>
                <div id="loginLogoWarningDiv" class="fileUploadWarning error_msg_hide"></div>

                <?php if ($currLoginLogo):?>
                    <div>&nbsp;</div><div><img src="<?php echo $currLoginLogo."?".time();?>"></div>
                    <div><a href="#" class="tablelink" id="deleteLoginLogo"><?php echo $Lang['SysMgr']['OrganizationInfo']['DeleteLogo'];?></a></div>
                <?php endif;?>
            </td>
        </tr>

		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?php echo $Lang['SysMgr']['OrganizationInfo']['BackgroundImage'];?>
			</td>
			<td class="tabletextremark" valign="top">
				<input type="file" name="backgroundImage" id="backgroundImage" class="file"><br /><?php ?>
				<br><?php echo $Lang['SysMgr']['OrganizationInfo']['BackgroundImageInstruction'];?>
				<div id="backgroundImageWarningDiv" class="fileUploadWarning error_msg_hide"></div>
				
<?php if ($currBackgroundImage):?>				
				<div>&nbsp;</div><div><img src="<?php echo $currBackgroundImage."?".time();?>"></div>
				<div><a href="#" class="tablelink" id="deleteBackgroundImage"><?php echo $Lang['SysMgr']['OrganizationInfo']['DeleteBackgroundImage'];?></a></div>
<?php endif;?>				
			</td>
		</tr>

    </table>

    <?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_save, "button", "", 'btnSubmit');?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "", 'btnReset');?>
<p class="spacer"></p>
</div>
<?php //echo csrfTokenHtml(generateCsrfToken());?>
</form>

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
