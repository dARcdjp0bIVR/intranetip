<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

$RoleName = trim(stripslashes(urldecode($_REQUEST['RoleName'])));
$RoleID = $_REQUEST['RoleID'];

intranet_opendb();

$RoleManage = new role_manage();

if ($RoleManage->Check_Role_Name($RoleName,$RoleID))
	echo '1'; // Role name is good to use
else
	echo '0'; // Role name is not good to use

intranet_closedb();
?>