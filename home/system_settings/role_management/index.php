<?php
// using:

############# Change Log [Start] ################
#   2019-12-23 Sam - for Mail-targeting, uncheck both Yes and No options if no config was found to avoid confusion (2019-0918-1436-31235)
#   2019-08-21 Henry - bug fix for role name checking
#   2018-07-06 Vito - add export function
#	2011-11-11 Carlos - modified js Toggle_Sub_Target_Row()
#	2011-10-20 Carlos - modified js Toggle_Teaching_Options()
#
#	Date:	2010-06-22	YatWoon
#			add online help button
#
/*
 *	26-01-2010 Ivan
 *	- For seraching user , Show ClassName and ClassNumber for Student only
 */
############# Change Log [End] ################

 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['Role'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['IdentityRole']);
//$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Role";'."\n";
$js.= '</script>'."\n";

$MODULE_OBJ['title'] = $Lang['SysMgr']['RoleManagement']['ModuleTitle'].$js;

# online help button
if (!$sys_custom['DHL']) {
//	$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','role_home');
//	$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);
}

$linterface->LAYOUT_START();


$RoleManageUI = new role_manage_ui();
$Json = new JSON_obj();
echo $RoleManageUI->Get_Identity_Manage();
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
// Ajax function
{
function Get_New_Role_Form() {
	NewRoleAjax = GetXmlHttpObject();

  if (NewRoleAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_new_role_form.php';
  var postContent = '';
	NewRoleAjax.onreadystatechange = function() {
		if (NewRoleAjax.readyState == 4) {
			ResponseText = Trim(NewRoleAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		}
	};
  NewRoleAjax.open("POST", url, true);
	NewRoleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	NewRoleAjax.send(postContent);
}

function Save_Role(EditAfterCreate) {
	Check_Role_Name();
	//Check_Identity_Selection();
	var RoleNameWarning = $('#RoleNameWarningLayer').html();
	//var IdentityWarning = $('#IdentityWarningLayer').html();
	//if (RoleNameWarning == "" && IdentityWarning == "") {
	if (RoleNameWarning == "") {
		var RoleType = Get_Selection_Value('RoleType','String');
		document.getElementById('SubmitRoleBtn').disabled = true;
		//document.getElementById('SubmitEditRoleBtn').disabled = true;
		document.getElementById('CancelRoleBtn').disabled = true;
		SaveRoleAjax = GetXmlHttpObject();

	  if (SaveRoleAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_create_role.php';
	  var PostString = 'RoleName='+document.getElementById("RoleName").value;
	  PostString += '&EditAfterCreate='+EditAfterCreate;
		SaveRoleAjax.onreadystatechange = function() {
			if (SaveRoleAjax.readyState == 4) {
				ResponseText = Trim(SaveRoleAjax.responseText);
				ReturnArray = ResponseText.split('~~~~~');
			  if (EditAfterCreate) {
			  	if (ReturnArray[0] == "false") {
			  		Get_Return_Message(ReturnArray[1]);
			  		//window.top.tb_remove();
			  	}
			  	else {
			  		//window.top.tb_remove();
			  		Get_Role_Detail(ReturnArray[0]);
			  		Get_Return_Message(ReturnArray[1]);
			  	}
			  }
			  else {
		  		Get_Return_Message(ReturnArray[1]);
		  		Get_Role_List_Table(RoleType);
		  		//window.top.tb_remove();
			  }
			}
		};
	  SaveRoleAjax.open("POST", url, true);
		SaveRoleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		SaveRoleAjax.send(PostString);
	}
}

function Check_Role_Name(RoleName,RoleID) {
	var RoleName;
	if (document.getElementById('RoleName'))
		RoleName = document.getElementById('RoleName').value;
	else
		RoleName = RoleName || "";

	var RoleID = RoleID || "";
	var ElementObj = $('#RoleNameWarningLayer');
	if (Trim(RoleName) != "") {
		CheckRoleNameAjax = GetXmlHttpObject();

	  if (CheckRoleNameAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_check_role_name.php';
	  var postContent = 'RoleName='+encodeURIComponent(RoleName);
	  postContent += '&RoleID='+RoleID;
		CheckRoleNameAjax.onreadystatechange = function() {
			if (CheckRoleNameAjax.readyState == 4) {
				ResponseText = Trim(CheckRoleNameAjax.responseText);
				if (ResponseText == "1") {
					ElementObj.html('');
					ElementObj.hide();
				}
				else {
					ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
					ElementObj.show('fast');
				}
			}
		};
	  CheckRoleNameAjax.open("POST", url, true);
		CheckRoleNameAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CheckRoleNameAjax.send(postContent);
	}
	else {
		CheckRoleNameAjax = "";

		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
		ElementObj.show('fast');
	}
}

function Get_Role_List_Table(RoleType) {
	RoleType = RoleType || '';

	GetRoleAjax = GetXmlHttpObject();

  if (GetRoleAjax == null)
  {
    alert (errAjax);
    return;
  }

  // if RoleType is not provided, whole page is refreshed, otherwise only the Layer of "RoleType" is refreshed
  var ReturnLayer = "";
  if (RoleType == "")
  	ReturnLayer = "IdentityManageLayer";
  else
  	ReturnLayer = RoleType+'RoleListLayer';

  var url = 'ajax_get_role_list.php';
  var postContent = 'RoleType='+RoleType;
	GetRoleAjax.onreadystatechange = function() {
		if (GetRoleAjax.readyState == 4) {
			ResponseText = Trim(GetRoleAjax.responseText);
		  document.getElementById(ReturnLayer).innerHTML = ResponseText;
		  Thick_Box_Init();
		}
	};
  GetRoleAjax.open("POST", url, true);
	GetRoleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetRoleAjax.send(postContent);
}

function Get_Role_Detail(RoleID,Show) {
	var Show = Show || "";

	RoleDetailAjax = GetXmlHttpObject();

  if (RoleDetailAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_role_detail.php';
  var postContent = 'RoleID='+RoleID;
  postContent += '&Show='+Show;
	RoleDetailAjax.onreadystatechange = function() {
		if (RoleDetailAjax.readyState == 4) {
			ResponseText = Trim(RoleDetailAjax.responseText);
		  document.getElementById('IdentityManageLayer').innerHTML = ResponseText;
		  Thick_Box_Init();
		  Init_JEdit_Input("div.jEditInput");
		}
	};
  RoleDetailAjax.open("POST", url, true);
	RoleDetailAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	RoleDetailAjax.send(postContent);
}

function Get_Role_Add_Member_Form(RoleID) {
	RoleAddMemberAjax = GetXmlHttpObject();

  if (RoleAddMemberAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_add_role_member_form.php';
  var postContent = 'RoleID='+RoleID;
	RoleAddMemberAjax.onreadystatechange = function() {
		if (RoleAddMemberAjax.readyState == 4) {
			ResponseText = Trim(RoleAddMemberAjax.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
		 	Init_JQuery_AutoComplete('UserSearch');
		}
		$('input#UserSearch').focus();
	};
  RoleAddMemberAjax.open("POST", url, true);
	RoleAddMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	RoleAddMemberAjax.send(postContent);
}

function Add_Role_Member(RoleID) {
	//Select_All_Options("AddUserID[]",true);
	var SelectedUser = Get_Selection_Value("AddUserID[]","QueryString",true);

	if (SelectedUser != "") {
		document.getElementById('AddMemberSubmitBtn').disabled = true;
		document.getElementById('AddMemberCancelBtn').disabled = true;
		Block_Thickbox();
		AddRoleMemberAjax = GetXmlHttpObject();

	  if (AddRoleMemberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_add_role_member.php';
	  var postContent = SelectedUser;
	  postContent += '&RoleID='+RoleID;
		AddRoleMemberAjax.onreadystatechange = function() {
			if (AddRoleMemberAjax.readyState == 4) {
				Update_Role_Member_List(RoleID);
				ResponseText = Trim(AddRoleMemberAjax.responseText);
			  Get_Return_Message(ResponseText);
			  UnBlock_Thickbox();
			  Scroll_To_Top();
			  window.top.tb_remove();
			}
		};
	  AddRoleMemberAjax.open("POST", url, true);
		AddRoleMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		AddRoleMemberAjax.send(postContent);
	}
	else {
		$('#AddRoleMemberWarningLayer').html('<?=$Lang['SysMgr']['RoleManagement']['AddRoleMemberWarning']?>');
		$('#AddRoleMemberWarningLayer').show('fast');
	}
}

function Update_Role_Member_List(RoleID) {
	RoleMemberListAjax = GetXmlHttpObject();

  if (RoleMemberListAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_get_role_member_list.php';
  var postContent = 'RoleID='+RoleID;
	RoleMemberListAjax.onreadystatechange = function() {
		if (RoleMemberListAjax.readyState == 4) {
			ResponseText = Trim(RoleMemberListAjax.responseText);
		  document.getElementById('MemberListSubLayer').innerHTML = ResponseText;
		  Thick_Box_Init();
		}
	};
  RoleMemberListAjax.open("POST", url, true);
	RoleMemberListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	RoleMemberListAjax.send(postContent);
}

//function Sort_Role_Memeber_List(Role, fieldcolumn){
//	RoleMemberListAjax = GetXmlHttpObject();
//	if (RoleMemberListAjax == null)
//	{
//		alert (errAjax);
//	    return;
//	}
//	
//	var targetOrder = document.getElementById('Ordering').value;
//	if(document.getElementById('targetfield').value != Role) {targetOrder = "";}
//	
//	var url = 'ajax_get_role_member_list.php';
//  	var postContent = 'RoleID='+Role+'&targetColumn='+fieldcolumn+'&targetOrder='+targetOrder;
//  	Block_Element('MemberListSubLayer');
//  	
// 	RoleMemberListAjax.onreadystatechange = function() {
//		if (RoleMemberListAjax.readyState == 4) {
//			ResponseText = Trim(RoleMemberListAjax.responseText);
//			document.getElementById('MemberListSubLayer').innerHTML = ResponseText;
//			if(targetOrder == 1){document.getElementById('Ordering').value = 0;} else {document.getElementById('Ordering').value = 1;}
//			document.getElementById('targetfield').value = Role;
//		  	UnBlock_Element('MemberListSubLayer');
//		}
//	};
//	
//  	RoleMemberListAjax.open("POST", url, true);
//	RoleMemberListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
//	RoleMemberListAjax.send(postContent);
//	
//}

function Remove_Role(RoleID) {
	if (confirm('<?=$Lang['SysMgr']['RoleManagement']['RoleDeleteWarning']?>')) {
		RemoveRoleAjax = GetXmlHttpObject();

	  Block_Document();
	  if (RemoveRoleAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_remove_role.php';
	  var postContent = 'RoleID='+RoleID;
		RemoveRoleAjax.onreadystatechange = function() {
			if (RemoveRoleAjax.readyState == 4) {
				ResponseText = Trim(RemoveRoleAjax.responseText);
				Get_Return_Message(ResponseText);
				Get_Role_List_Table();
				Thick_Box_Init();
				UnBlock_Document();
			}
		};
	  RemoveRoleAjax.open("POST", url, true);
		RemoveRoleAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		RemoveRoleAjax.send(postContent);
	}
}

function Delete_Role_Member(RoleID) {
	if (confirm('<?=$Lang['SysMgr']['RoleManagement']['RoleMemberDeleteWarning']?>')) {
		var UserID = Get_Check_Box_Value('UserID[]','QueryString');
		RemoveRoleMemberAjax = GetXmlHttpObject();

	  Block_Document();
	  if (RemoveRoleMemberAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_remove_role_member.php';
	  var postContent = 'RoleID='+RoleID;
	  postContent += '&'+UserID;
		RemoveRoleMemberAjax.onreadystatechange = function() {
			if (RemoveRoleMemberAjax.readyState == 4) {
				Update_Role_Member_List(RoleID);
				ResponseText = Trim(RemoveRoleMemberAjax.responseText);
			  Get_Return_Message(ResponseText);
			  Scroll_To_Top();
				UnBlock_Document();
			}
		};
	  RemoveRoleMemberAjax.open("POST", url, true);
		RemoveRoleMemberAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		RemoveRoleMemberAjax.send(postContent);
	}
}

function Save_Role_Right_Target(RoleID,RightOrTarget,NotSetBefore=false) {
	if (RightOrTarget == "Target") {
		/*var TeachingTarget = Get_Check_Box_Value('TeachingTarget[]','QueryString');
		var NonTeachingTarget = Get_Check_Box_Value('NonTeachingTarget[]','QueryString');
		var StudentTarget = Get_Check_Box_Value('StudentTarget[]','QueryString');
		var ParentTarget = Get_Check_Box_Value('ParentTarget[]','QueryString');*/
		var Target = Get_Check_Box_Value('Target[]','QueryString');
		if(NotSetBefore && Target.indexOf('All-Yes') < 0 && Target.indexOf('All-No') < 0) Target += 'Target[]=All-Yes&';
	}
	else
		var RoleRight = Get_Check_Box_Value('Right[]','QueryString');

	SaveRightTargetAjax = GetXmlHttpObject();

  Block_Document();
  if (SaveRightTargetAjax == null)
  {
    alert (errAjax);
    return;
  }

  var url = 'ajax_save_role_right_target.php';
  var postContent = 'RoleID='+RoleID;
  if (RightOrTarget == "Target") {
		/*postContent += '&'+TeachingTarget;
		postContent += '&'+NonTeachingTarget;
		postContent += '&'+StudentTarget;
		postContent += '&'+ParentTarget;*/
		postContent += '&'+Target;
	}
	else
		postContent += '&'+RoleRight;
  postContent += '&RightOrTarget='+RightOrTarget;

	SaveRightTargetAjax.onreadystatechange = function() {
		if (SaveRightTargetAjax.readyState == 4) {
			ResponseText = Trim(SaveRightTargetAjax.responseText);
			Get_Return_Message(ResponseText);
			Scroll_To_Top();
			if (RightOrTarget == "Target")
				Get_Role_Target_Detail(RoleID);
			else
				Get_Role_Right_Detail(RoleID);
		}
	};
  SaveRightTargetAjax.open("POST", url, true);
	SaveRightTargetAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	SaveRightTargetAjax.send(postContent);
}

function Get_Role_Target_Detail(RoleID) {
	GetRoleTargetAjax = GetXmlHttpObject();

  if (GetRoleTargetAjax == null)
  {
    alert (errAjax);
    return;
  }

	var url = 'ajax_get_role_target_detail.php';
  var postContent = 'RoleID='+RoleID;

	GetRoleTargetAjax.onreadystatechange = function() {
		if (GetRoleTargetAjax.readyState == 4) {
			ResponseText = Trim(GetRoleTargetAjax.responseText);
			//$('#TargetDetailSubLayer').html(ResponseText);
			document.getElementById('TargetDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
  GetRoleTargetAjax.open("POST", url, true);
	GetRoleTargetAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetRoleTargetAjax.send(postContent);
}

function Get_Role_Right_Detail(RoleID) {
	GetRoleRightAjax = GetXmlHttpObject();

  if (GetRoleRightAjax == null)
  {
    alert (errAjax);
    return;
  }

	var url = 'ajax_get_role_right_detail.php';
  var postContent = 'RoleID='+RoleID;

	GetRoleRightAjax.onreadystatechange = function() {
		if (GetRoleRightAjax.readyState == 4) {
			ResponseText = Trim(GetRoleRightAjax.responseText);
			document.getElementById('TopManagementDetailSubLayer').innerHTML = ResponseText;
			UnBlock_Document();
		}
	};
  GetRoleRightAjax.open("POST", url, true);
	GetRoleRightAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	GetRoleRightAjax.send(postContent);
}

var PreviousClass = "";
function Get_User_List(RoleID) {
	var RoleID = RoleID || document.getElementById('RoleID').value;
	var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	//Select_All_Options('AddUserID[]',true);
	var AddUserID = Get_Selection_Value('AddUserID[]','QueryString',true);

	if ((IdentityType != "Parent" && IdentityType != "Student") || (document.getElementById('YearClassSelect').style.display != "none" && document.getElementById('YearClassSelect').selectedIndex != 0)) {

		// hide the select class selection if switced to teacher/ staff
		if (IdentityType != "Parent" && IdentityType != "Student") {
			document.getElementById('YearClassSelect').selectedIndex = 0;
			$('#YearClassSelect').hide();
		}

		// get the form class ID
		var YearClassID = document.getElementById('YearClassSelect').options[document.getElementById('YearClassSelect').selectedIndex].value;

		var GetFinalList = false;
		if (PreviousClass == YearClassID)
			GetFinalList = true;

		// get student selected if the selection element exists
		var ParentStudentID = "";
		if (document.getElementById('ParentStudentID') && IdentityType == "Parent" && GetFinalList)
			ParentStudentID = document.getElementById('ParentStudentID').options[document.getElementById('ParentStudentID').selectedIndex].value;

		// setting global variable to prevent parent selection problem
		PreviousClass = YearClassID;

		StudListAjax = GetXmlHttpObject();

	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&YearClassID='+YearClassID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&ParentStudentID='+ParentStudentID;
	  postContent += '&RoleID='+RoleID;

		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
				if ((!document.getElementById('ParentStudentID') || !GetFinalList) && IdentityType == "Parent") {
			  	document.getElementById('ParentStudentLayer').innerHTML = ResponseText;
			  	Get_User_List(RoleID);
			  }
			  else {
			  	if (IdentityType != "Parent")
			  		document.getElementById('ParentStudentLayer').innerHTML = "";
			  	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			  }
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
	else {
		$('#YearClassSelect').show('fast');
		document.getElementById('ParentStudentLayer').innerHTML = "";

		StudListAjax = GetXmlHttpObject();
	  if (StudListAjax == null)
	  {
	    alert (errAjax);
	    return;
	  }

	  var url = 'ajax_get_user_list.php';
	  var postContent = AddUserID;
	  postContent += '&IdentityType='+IdentityType;
	  postContent += '&RoleID='+RoleID;

		StudListAjax.onreadystatechange = function() {
			if (StudListAjax.readyState == 4) {
				ResponseText = Trim(StudListAjax.responseText);
			 	document.getElementById('AvalUserLayer').innerHTML = ResponseText;
			}
		};
	  StudListAjax.open("POST", url, true);
		StudListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		StudListAjax.send(postContent);
	}
}
}

// pure js (no ajax) form validate function
{
function Check_Identity_Selection() {
	var ElementObj = $('#IdentityWarningLayer');
	if (Get_Selection_Value('RoleType','String') == "") {
		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['IdentitySelectWarning']?>');
		ElementObj.show('fast');
	}
	else {
		ElementObj.html('');
		ElementObj.hide();
	}
}
}

// jEditable function
{
function Init_JEdit_Input(objDom) {
	var WarningLayer = "#RoleNameWarningLayer";
	$(objDom).editable(
    function(value, settings) {
    	var ElementObj = $(this);
    	if ($(WarningLayer).html() == "" && ElementObj[0].revert != value) {
	    	wordXmlHttp = GetXmlHttpObject();

			  if (wordXmlHttp == null)
			  {
			    alert (errAjax);
			    return;
			  }

			  var url = 'ajax_rename_role.php';
				var postContent = "RoleID="+ElementObj.attr("id");
				postContent += "&RoleName="+encodeURIComponent(value);
				wordXmlHttp.onreadystatechange = function() {
					if (wordXmlHttp.readyState == 4) {
						Get_Return_Message(Trim(wordXmlHttp.responseText));
						ElementObj.html(value);
						$('span#RoleNameNavLayer').html(value);
					}
				};
			  wordXmlHttp.open("POST", url, true);
				wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				wordXmlHttp.send(postContent);
			}
			else {
				ElementObj[0].reset();
			}
		}, {
    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
    event : "click",
    onblur : "submit",
    type : "text",
    style  : "display: inline",
    height: "20px",
    maxlength: 100,
    onreset: function() {
    	$(WarningLayer).html('');
    	$(WarningLayer).hide();
    }
  });

  $(objDom).keyup(function() {
		var RoleName = $(this).find('input').val();
		var RoleID = $(this).attr('id');

		Check_Role_Name(RoleName,RoleID);
	});
}
}

// UI function
{
function Swap_Style(Obj) {
	var RowStyle;
	if (Obj.checked)
		RowStyle = 'selected_row';
	else
		RowStyle = 'sub_row';

	Obj.parentNode.parentNode.className = RowStyle;

	/*var CellBackground;
	if (Obj.checked)
		CellBackground = '#EFFDDB';
	else
		CellBackground = '#F4F4F4';

	Obj.parentNode.style.background = CellBackground;*/
}

function Switch_Layer(SwitchTo) {
	var MemberLayer = $('#MemberListSubLayer');
	var TargetLayer = $('#TargetDetailSubLayer');
	var TopManagementLayer = $('#TopManagementDetailSubLayer');
	if (SwitchTo == 'Target') {
		MemberLayer.hide();
		TargetLayer.show('fast');
		TopManagementLayer.hide();
	}
	else if (SwitchTo == 'Member') {
		MemberLayer.show('fast');
		TargetLayer.hide();
		TopManagementLayer.hide();
	}
	else {
		MemberLayer.hide();
		TargetLayer.hide();
		TopManagementLayer.show('fast');
	}
}

function Add_Form_Row() {
	if (!document.getElementById('GenAddRoleRow')) {
		var TableBody = document.getElementById("RoleListTable").tBodies[0];
		var RowIndex = document.getElementById("AddRoleRow").rowIndex-1;
	  var NewRow = TableBody.insertRow(RowIndex);

	  NewRow.id = "GenAddRoleRow";

		var NewCell0 = NewRow.insertCell(0);
		var temp = '<input name="RoleName" type="text" id="RoleName" value="" class="textbox" onkeyup="Check_Role_Name();" style="width:98%;"/>';
		temp += '<div id="RoleNameWarningLayer" style="display: none; color:red;"></div>';
		NewCell0.innerHTML = temp;
		$("#RoleName").focus();

		var NewCell1 = NewRow.insertCell(1);
		var temp = '<input id="SubmitRoleBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Done']?>" onclick="Save_Role(false);"/> ';
		temp += '<input id="CancelRoleBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Cancel']?>" onclick="Clear_Role_Form();"/>';
		NewCell1.colSpan = '5';
		NewCell1.innerHTML = temp;
	}
}

function Clear_Role_Form() {
	var TableBody = document.getElementById("RoleListTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddRoleRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function Show_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	LayerObj.style.backgroundPosition = "center right";
	LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "";
	LayerObj.style.backgroundPosition = "";
	LayerObj.style.backgroundRepeat = '';
}

function Toggle_Teaching_Options(Identity,ObjChecked) {
	var DomObj = $("input."+Identity+"TargetOption");
/*	switch(Identity) {
		case 'Teaching':
			DomObj = $("input.TeachingTargetOption");
			break;
		case 'NonTeaching':
			DomObj = $("input.NonTeachingTargetOption");
			break;
		case 'Student':
			DomObj = $("input.StudentTargetOption");
			break;
		case 'Parent':
			DomObj = $("input.ParentTargetOption");
			break;
	}*/
	if (ObjChecked) {
		DomObj.css('display','none');
	}
	else {
		DomObj.css('display','');
	}

	Toggle_Sub_Target_Row();
}

function Toggle_Sub_Target_Row() {
	if (document.getElementById('TeachingAll').checked 
		&& document.getElementById('NonTeachingAll').checked 
		&& document.getElementById('StudentAll').checked 
		&& document.getElementById('ParentAll').checked
<?php if($special_feature['alumni']){ ?>
		&& document.getElementById('AlumniAll').checked
<?php } ?>
	) {
		$('tbody#SubTargetRow').css('display','none');
	}
	else {
		$('tbody#SubTargetRow').css('display','');
	}
}
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Add_Selected_User(UserID,UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('AddUserID[]');

	UserSelected.options[UserSelected.length] = new Option(UserName,UserID);

	Update_Auto_Complete_Extra_Para();
}

function Remove_Selected_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Remove_All_User() {
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_Class_User() {
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	//Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();
}

function Add_All_User() {
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	// union selected and newly add user list
	if (ClassUser.length > UserSelected.length) {
		var cloneCell = document.getElementById('SelectedUserCell');

		// clone element from avaliable user list
		var cloneFrom = ClassUser.cloneNode(1);
		cloneFrom.setAttribute('id','AddUserID[]');
		cloneFrom.setAttribute('name','AddUserID[]');
		var j=0;
		for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			User = UserSelected.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      cloneFrom.add(elOptNew, cloneFrom.options[j]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      cloneFrom.add(elOptNew, j); // IE only
	    }
			UserSelected.options[i] = null;
			j++;
		}

		cloneCell.replaceChild(cloneFrom,UserSelected);
	}
	else {
		for (var i = (ClassUser.length -1); i >= 0 ; i--) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	// update auto complete parameter list
	Update_Auto_Complete_Extra_Para();

	// empty the avaliable user list
	document.getElementById('AvalUserLayer').innerHTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true"></select>';
	/*for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		User = ClassUser.options[i];
		UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
		ClassUser.options[i] = null;
	}

	Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();*/

}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}
}

// jquery autocomplete function
{
var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_search_user.php",
		{
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[3],li.selectValue+'('+li.extra[2]+')');
				Get_User_List();
			},
			formatItem: function(row) {
				// Hide Class and Class Number if both are empty
				if (row[1] == '' && row[2] == '')
					return row[0] + " (Identity: " + row[3] + ")";
				else
					return row[0] + " (Identity: " + row[3] + "[" + row[1] + "-" + row[2] + "])";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200,
			scrollLayer: 'EditLayer'
		}
	);

	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = Get_Selected_Value('AddUserID[]');
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function Get_Selected_Value(SelectID,ReturnType) {
	var ReturnType = ReturnType || "JQuery";
	var str = "";
	var isFirst = true;
	var Obj = document.getElementById(SelectID);

	if (ReturnType == "JQuery") {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.options[i].value;
			isFirst = false;
		}

		var resultArray = new Array();
		resultArray[SelectID] = str.split('&');
		return resultArray;
	}
	else {
		for (var i=0; i< Obj.length; i++) {
			if (!isFirst) {
				str += "&";
			}
			str += Obj.name + '=' +Obj.options[i].value;
			isFirst = false;
		}

		return str;
	}
}
}

// thick box function
{
//on page load call tb_init
function Thick_Box_Init(){
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

// misc
{
function Pop_Role_Detail(Mode) {
	if (Mode == "Print")
		newWindow("print.php?Mode="+Mode,10);
	
	else if (Mode == "Export"){
		var formObjForExport = document.getElementById('ExportForm');
		formObjForExport.action = "export.php?Mode="+Mode;
		formObjForExport.submit();
	}
	
	else {
		var formObj = document.getElementById('ExportForm');
		
		formObj.action = "print.php?Mode="+Mode;
		formObj.submit();
	}

}
}
</script>
