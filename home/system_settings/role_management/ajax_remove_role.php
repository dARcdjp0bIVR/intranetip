<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");

$RoleID = $_REQUEST['RoleID'];

intranet_opendb();

$RoleManage = new role_manage();
	
$RoleManage->Start_Trans();
	
$Result = $RoleManage->Remove_Role($RoleID);
	
if ($Result) {
	echo $Lang['SysMgr']['RoleManagement']['RoleRemoveSuccess'];
	$RoleManage->Commit_Trans();
}
else {
	echo $Lang['SysMgr']['RoleManagement']['RoleRemoveUnsuccess'];
	$RoleManage->RollBack_Trans();
}

intranet_closedb();
?>