<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

$lui = new interface_html();

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['IdentityRole'],"index.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['RoleManagement']['ModuleTitle'];
$lui->LAYOUT_START(); 

$RoleManageUI = new role_manage_ui();
$Json = new JSON_obj();

echo $RoleManageUI->Get_Module_Manage();
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<?
$lui->LAYOUT_STOP();

intranet_closedb();
?>
<script>

</script>