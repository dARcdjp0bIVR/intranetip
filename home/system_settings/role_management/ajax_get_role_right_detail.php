<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/role_manage_ui.php");

$RoleID = $_REQUEST['RoleID'];

intranet_opendb();

$RoleManageUI = new role_manage_ui();
$Role = new role($RoleID,true,false,false);
echo $RoleManageUI->Get_Top_Management_Form($Role);

intranet_closedb();
?>