<?php
#modifying by : 

######### Change Log [Start] ###########
#
#	Date:	2010-12-20	YatWoon
#			use date range instead of select option
#
#	Date:	2010-09-15 [Henry Chow]
#			export file with TAB separator (instead of using comma)
#
######### Change Log [End] ###########
SET_TIME_LIMIT(86400);
ini_set("memory_limit", "2048M"); 
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

/*
if ($range == "") $range = 1;
# Date conds
$date_field = "StartTime";
switch ($range)
{
        case 0: // TODAY
             $date_conds = "WHERE TO_DAYS(now()) = TO_DAYS($date_field)";
             break;
        case 1: // LAST WEEK
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=604800";
             break;
        case 2: // LAST 2 WEEKS
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=1209600";
             break;
        case 3: // LAST MONTH
             $date_conds = "WHERE UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP($date_field)<=2678400";
             break;
        case 4: // ALL
             $date_conds = "";
             break;
        default:
             $date_conds = "WHERE TO_DAYS(now()) = TO_DAYS($date_field)";
             break;
}
$sql  = "SELECT
               $username_field as UserName, a.StartTime, a.DateModified
               ,a.ClientHost
               ,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
         FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
         $date_conds ORDER BY a.StartTime DESC
         ";
*/
if($identity)
{
	$identity_sql = " and b.RecordType=$identity ";	
}
$username_field = getNameFieldWithLoginClassNumberByLang("b.");
$sql  = "SELECT
   $username_field, b.ClassName, a.StartTime, a.DateModified
   ,a.ClientHost
   ,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
where a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59' $identity_sql
order by a.StartTime
";
         
$lib = new libdb();
$result = $lib->returnArray($sql);

//$export_content="\"$i_UserName ($i_UserLogin)\",\"$i_UsageStartTime\",\"$i_UsageEndTime\",\"$i_UsageHost\",\"$i_UsageDuration\"\n";
$exportColumn = array("$i_UserName ($i_UserLogin)",$i_general_class,$i_UsageStartTime,$i_UsageEndTime,$i_UsageHost,$i_UsageDuration);
/*
$export_content="\"$i_UserName ($i_UserLogin)\"\t\"$i_general_class\"\t\"$i_UsageStartTime\"\t\"$i_UsageEndTime\"\t\"$i_UsageHost\"\t\"$i_UsageDuration\"\n";
for($i=0;$i<sizeof($result);$i++){
	
	list($userName,$this_class,$startTime,$dateModified,$clientHost,$duration)=$result[$i];
	//$export_content.="\"$userName\",\"$startTime\",\"$dateModified\",\"$clientHost\",\"$duration\"\n";
	$export_content.="\"$userName\"\t\"$this_class\"\t\"$startTime\"\t\"$dateModified\"\t\"$clientHost\"\t\"$duration\"\n";
}
*/       
intranet_closedb();      
$filename = "login-record.csv";

//$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

// Output the file to user browser
//output2browser($export_content, $filename);
?>