<?php 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$lexport = new libexporttext();
$libenroll = new libclubsenrol();

$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);
$order = htmlspecialchars($_POST["order"]);
$field = htmlspecialchars($_POST["field"]);

$ordercon = "";
if($order==0){
    $ordercon = "desc"; 
}

if($field==0){
    $fieldcon = "Order by b.UserLogin";    
}elseif($field==1){
    $fieldcon = "Order by CONCAT(	TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '',b.EnglishName,b.ChineseName), IF(b.TitleChinese!='', CONCAT('', b.TitleChinese), ''))),CONCAT(' ',IF(b.ClassNumber IS NULL OR b.ClassNumber = '','',CONCAT(' ,',b.ClassName,'-',b.ClassNumber)),''))";
}elseif($field==2){
    $fieldcon = "Order by a.StartTime";
}elseif($field==3){
    $fieldcon = "Order by a.DateModified";
}elseif($field==4){
    $fieldcon = "Order by a.ClientHost";
}else{
    $fieldcon = "Order by duration";
}



$headerAry = array();
$headerAry[0] = $i_UserLogin;
$headerAry[1] = $i_UserName;
$headerAry[2] = $i_UsageStartTime;
$headerAry[3] = $i_UsageEndTime;
$headerAry[4] = $i_UsageHost;
$headerAry[5] = $i_UsageDuration;

if($identity)
{
    $identity_sql = " and b.RecordType IN ($identity) ";
}
$sql  = "SELECT
            b.UserLogin,
            CONCAT(	TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '',b.EnglishName,b.ChineseName), IF(b.TitleChinese!='', CONCAT('', b.TitleChinese), ''))),CONCAT(' ',IF(b.ClassNumber IS NULL OR b.ClassNumber = '','',CONCAT(' ,',b.ClassName,'-',b.ClassNumber)),'')),
			a.StartTime,
			a.DateModified
			,a.ClientHost
			,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM
			INTRANET_LOGIN_SESSION as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		where
			a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59'
			 $identity_sql
			 $fieldcon $ordercon 		 
";
//debug_pr($sql);
$result = $libenroll->returnArray($sql);
$dataAry = array();
for($i = 0;$i<sizeof($result);$i++){
    $dataAry[$i][0] = $result[$i]['UserLogin'];
    $dataAry[$i][1] = $result[$i][1];
    $dataAry[$i][2] = $result[$i]['StartTime'];
    $dataAry[$i][3] = $result[$i]['DateModified'];
    $dataAry[$i][4] = $result[$i]['ClientHost'];
    $hr = "";
    $min = "";
    if ($result[$i]['duration'] > 3600)
    {
        $hr = intval($result[$i]['duration']/3600);
        $result[$i]['duration'] = $result[$i]['duration'] % 3600;
    }
    if ($result[$i]['duration'] > 60)
    {
        $min = intval($result[$i]['duration']/60);
        $result[$i]['duration'] = $result[$i]['duration'] % 60;
    }
    $s_data = "";
    global $i_Usage_Hour, $i_Usage_Min, $i_Usage_Sec;
    if ($hr != "") $s_data .= "$hr $i_Usage_Hour";
    if ($min != "") $s_data .= "$min $i_Usage_Min";
    $s_data .= $result[$i]['duration']. "$i_Usage_Sec";
    $dataAry[$i][5] = $s_data;
}
			 
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'login_records.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);
?>