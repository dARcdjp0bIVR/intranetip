<?php
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/lib.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_report.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libuserlogreport.php");
// include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

$mpdf = new mPDF();
// Set mPDF Settings
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->list_auto_mode = "mpdf";
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->use_kwt = true;
$mpdf->splitTableBorderWidth = 0.1; // split 1 table into 2 pages add border at the bottom

$mpdf->packTableDate = true;


// include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();
$ldb = new libdb();
$lu = new libuserlogreport();

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

// Get Current Time
$now = time();
$today = date('Y-m-d', $now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "account",
    "Action" => "pdf",
    "Date" => $today,
    "Time" => $nowTime
);
$report = $lu->insertLogReport($reportLogArr);

// Paging Option
$paging = array(
    "all" => true
);

// filter Options

$filterOpts = array();
$filterOpts['name'] = $filter_username;
$filterOpts['action'] = $filter_action;
if ($textFromDate == null && $textToDate == null) {
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
} else {
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}

// Sorting Options
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get All the Log Records
$logs = $lu->getAccountLog($filterOpts, $paging, $sortOpts);
unset($logs['PAGEMAX']);

// Handle Bulk data
ini_set("memory_limit","1024M");
if ($sys_custom['project']['HKPF']) {
    set_time_limit(21600);
    ini_set('max_input_time', 21600);
}
$htmlLimit = 50000;
$dataHTML = array();

// Table Content
// Table Header
$tableHead = "<thead>";
    $tableHead .= "<tr class='table_header'>";
        $tableHead .= "<th style=\"text-align:center;width:2.1mm;\">#</th>";
        $tableHead .= "<th style=\"text-align:center;width:16.8mm;\">" . $Lang['SystemSetting']['AccountLog']['Date'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:12.6mm;\">" . $Lang['SystemSetting']['AccountLog']['Time'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:12.6mm;\">" . $Lang['SystemSetting']['AccountLog']['IPAddress'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:23.1mm;\">" . $Lang['SystemSetting']['AccountLog']['LogBylogin'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:25.2mm;\">" . $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)' . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:23.1mm;\">" . $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)' . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:12.6mm;\">" . $Lang['SystemSetting']['AccountLog']['Action'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:23.1mm;\">" . $Lang['SystemSetting']['AccountLog']['Userlogin'] . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:25.2mm;\">" . $Lang['SystemSetting']['AccountLog']['User'] . ' (En)' . "</th>";
        $tableHead .= "<th style=\"text-align:center;width:23.1mm;\">" . $Lang['SystemSetting']['AccountLog']['User'] . ' (Ch)' . "</th>";
    $tableHead .= "</tr>";
$tableHead .= "</thead>";

            // Table Body Content
            $values = array(
                "", "LogDate", "LogTime", "IPAddress", "LogBy", "LogEnglishName",
                "LogChineseName", "Action", "UserLogin", "EnglishName", "ChineseName"
            );
            $tableBodies = array();
            
            $tableBody .= "<tbody>";
            $index = 1;
            if (sizeof($logs) == 0) {
                $tableBody .= "<tr>";
                $tableBody .= '<td align="center" colspan="' . sizeof($values) . '">' . $i_no_record_exists_msg . '</td>';
                $tableBody .= "</tr>";
            } else {
                // Print every record into a table row
                $index = 1;
                foreach($logs as $log){
                    $oddEven = $index % 2;
                    $tableBody .= "<tr class='row$oddEven'>";
                    foreach($values as $val){
                        if($val == 'Action'){
                            $tableBody .= "<td>" . decodeCRUD($log[$val]) . "</td>";
                        } else if($val != ''){
                            $tableBody .= "<td>" . $log[$val] . "</td>";
                        } else {
                            $tableBody .= "<td>" . $index . "</td>";
                        }
                    }
                    $tableBody .= "</tr>";
                    if(strlen($tableBody) > $htmlLimit){
                        $tableBodies[] = $tableBody;
                        $tableBody = "";
                    }
                    $index++;
                }
             }
            $tableBody .= "</tbody>";

$styleContent = <<< EOD
<style>
@page {
  size: A4;
  margin: 0;
  padding: 0;
  width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
    padding: 0;
    margin: 0;
    font-family: mingliu;
}
.login_records table{
    width: 100%;
}
.row1{
    background-color: #FFFFFF;
}
.row0{
    background-color: #F1F1F1;
}
.table_header th{
    background-color: #487db4/* #4A7ECC*/;
    color: #FFFFFF;
}
</style>
EOD;
// Write Style CSS
$mpdf->WriteHTML($styleContent, 1);

// PDF Header
$header = <<<EOT
<table width="100%">
    <tbody>
    <tr>
    <td align="center"
        style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
    </tbody>
</table>
EOT;

// PDF Footer
$footer = <<<EOF
<table width="100%">
    <tbody>
    <tr>
    <td align="center"
        style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
    <tr>
    <td align=right style="vertical-align:bottom;">This report is printed at $today $nowTime</td>
    </tr>
    </tbody>
</table>
EOF;

$resultTable = <<< EOE

    <table>
        $tableHead
EOE;

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
// Write Table Content HTML
$mpdf->WriteHTML($resultTable);

for($i=0;$i<sizeof($tableBodies);$i++){
    $mpdf->WriteHTML($tableBodies[$i]);
}
$mpdf->WriteHTML($tableBody.'</table>');

// Output File
$mpdf->Output("account_log.pdf", "I");

function secToTime($data)
{
    if ($data > 3600) {
        $hr = intval($data / 3600);
        $data = $data % 3600;
    }
    if ($data > 60) {
        $min = intval($data / 60);
        $data = $data % 60;
    }
    $s_data = "";
    global $i_Usage_Hour, $i_Usage_Min, $i_Usage_Sec;
    if ($hr != "")
        $s_data .= "$hr $i_Usage_Hour";
    if ($min != "")
        $s_data .= "$min $i_Usage_Min";
    $s_data .= "$data $i_Usage_Sec";
    return $s_data;
}

function decodeCRUD($target)
{
    $result = "";
    global $Lang;
    switch ($target) {
        case 'C':
            $result = $Lang['SystemSetting']['AccountLog']['Create'];
            break;
        case 'R':
            $result = $Lang['SystemSetting']['AccountLog']['Read'];
            break;
        case 'U':
            $result = $Lang['SystemSetting']['AccountLog']['Update'];
            break;
        case 'D':
            $result = $Lang['SystemSetting']['AccountLog']['Delete'];
            break;
        default:
            break;
    }
    return $result;
}
?>