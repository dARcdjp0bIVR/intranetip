<?php
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/lib.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_ui.php");

include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libuserlogreport.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once ($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$lu = new libuserlogreport();

$mpdf = new mPDF();

// Set mPDF Settings
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in = "UTF-8";
$mpdf->list_auto_mode = "mpdf";
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->use_kwt = true;
$mpdf->splitTableBorderWidth = 0.1; // split 1 table into 2 pages add border at the bottom

// include_once($PATH_WRT_ROOT."includes/libaccount.php");

// intranet_auth();
// intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

// intranet_auth();
// intranet_opendb();
$libenroll = new libclubsenrol();

// $start = $_POST['start'];
// $end = $_POST['end'];
$identity = htmlspecialchars($_POST["Identity"]);
$start = htmlspecialchars($_POST["start"]);
$end = htmlspecialchars($_POST["end"]);

// Get Current Time
$now = time();
$today = date('Y-m-d', $now);
$nowTime = date('H:i:s', $now);

// Report Log Record
$reportLogArr = array(
    "Rtype" => "login",
    "Action" => "pdf",
    "Date" => $today,
    "Time" => $nowTime
);
$report = $lu->insertLogReport($reportLogArr);

// Define Date Range
if ($start == "" || $end == "") {
    $start = $today;
    $end = $today;
}

// TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") {
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);

if ($field == "") {
    $field = 1;
}
switch ($field) {
    case 0:
        $field = 0;
        break;
    case 1:
        $field = 1;
        break;
    case 2:
        $field = 2;
        break;
    case 3:
        $field = 3;
        break;
    default:
        $field = 1;
        break;
}
$order = ($order == 1) ? 1 : 0;

$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_LoginRecords'] = 1;

$username_field = getNameFieldWithLoginClassNumberByLang("b.");
if ($identity) {
    $identity_sql = " and b.RecordType IN ($identity) ";
}

// Sorting setting
$fields = array("UserLogin", "UserName", "DateModified", "ClientHost", "duration");
$orders = array("DESC", "ASC");
$orderCond = "ORDER BY $fields[$field] $orders[$order]";

// SQL Query
$sql = "SELECT
            b.UserLogin,
            CONCAT(TRIM(CONCAT(IF(b.ChineseName IS NULL OR TRIM(b.ChineseName) = '', b.EnglishName, b.ChineseName), IF(b.TitleChinese != '', CONCAT('', b.TitleChinese), ''))), CONCAT(' ', IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' ,', b.ClassName, '-', b.ClassNumber)), '')) as UserName,
            a.StartTime,
            a.DateModified,
            a.ClientHost,
            UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM
			INTRANET_LOGIN_SESSION as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		WHERE
			a.StartTime >= '$start 00:00:00' AND a.DateModified <= '$end 23:59:59'
			$identity_sql
			$orderCond ";
// debug_pr($sql);

// TABLE INFO
$print = '';
$print .= $linterface->Get_Content_Tool_v30('print', "javascript:goToPrint();");
$htmlAry['printToolBar'] = $print;

$lib = new libdb();
$dataResult = $lib->returnArray($sql, 2);

// Table Header
$resultTable = "<thead>";
    $resultTable .= "<tr class='table_header'>";
    $resultTable .= "<th width=6.3mm class='field_title'>#</td>\n";
    $resultTable .= "<th width=27.3mm class='field_title'>" . $i_UserLogin . "</td>\n";
    $resultTable .= "<th width=39.9mm class='field_title'>" . $i_UserName . "</td>\n";
    $resultTable .= "<th width=39.9mm class='field_title'>" . $i_UsageStartTime . "</td>\n";
    $resultTable .= "<th width=39.9mm class='field_title'>" . $i_UsageEndTime . "</td>\n";
    $resultTable .= "<th width=21mm class='field_title'>" . $i_UsageHost . "</td>\n";
    $resultTable .= "<th width=35.7mm class='field_title'>" . $i_UsageDuration . "</td>\n";
    $resultTable .= "</tr>";
$resultTable .= "</thead>";

$datetype += 0;

// Table Body Content
$index = 1;
$colArr = array(
    "UserLogin",
    "UserName",
    "StartTime",
    "DateModified",
    "ClientHost",
    "duration"
);

ini_set("memory_limit","1024M");
if ($sys_custom['project']['HKPF']) {
    set_time_limit(21600);
    ini_set('max_input_time', 21600);
}
$htmlLimit = 50000;

$resultHTML = array();
$resultTable .= "<tbody>";
foreach ($dataResult as $data) {
    $oddeven = $index % 2;
    $resultTable .= "<tr class='row$oddeven'>";
    $resultTable .= "<td>$index</td>";
    foreach ($colArr as $col) {
        if ($col == 'duration') {
            $resultTable .= "<td>" . secToTime($data[$col]) . "</td>";
        } else {
            $resultTable .= "<td>" . $data[$col] . "</td>";
        }
    }
    $resultTable .= "</tr>";
    
    $index ++;
    if(strlen($resultTable) > $htmlLimit){
        $resultHTML[] = $resultTable;
        $resultTable = "";
    }
}
$resultTable .= "</tbody>";
/**
* End of Category selector
*/

$styleContent = <<< EOD
<style>
@page {
    size: A4;
    margin: 0;
    padding: 0;
    width: 100%;
}
html,body{
    height:297mm;
    width:210mm;
    padding: 0;
    margin: 0;
    font-family: mingliu;
}

.login_records table{
    width: 100%;
}

.row1{
    background-color: #FFFFFF;
}
.row0{
    background-color: #F1F1F1;
}

.table_header th{
    background-color: darkgrey;
    color: white;
}

.restricted{
    text-align: center;
    font-size: 5mm;
    font-weight: bold;
}
</style>
EOD;

// Write Style CSS
$mpdf->WriteHTML($styleContent, 1);

// PDF Header
$header = <<<EOT
<table width="100%">
<tbody>
    <tr>
        <td align="center" style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
</tbody>
</table>
EOT;

// PDF Footer
$footer = <<<EOF
<table width="100%">
<tbody>
    <tr>
        <td align="center" style="font-size: 5mm;font-weight: bold;">RESTRICTED</td>
    </tr>
    <tr>
        <td align=right style="vertical-align:bottom;">This report is printed at $today $nowTime</td>
    </tr>
</tbody>
</table>
EOF;
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);

$mpdf->WriteHTML('<body><div class="login_records" width="100%">
<table>');

for($i=0; $i<sizeof($resultHTML); $i++) {
    $mpdf->WriteHTML($resultHTML[$i]);
}

// Write Table Content HTML
$mpdf->WriteHTML($resultTable);

$mpdf->WriteHTML("</table></div></body>");

// Output File
$mpdf->Output("Login_record.pdf", "I");

function secToTime($data) {
    if ($data > 3600) {
        $hr = intval($data/3600);
        $data = $data % 3600;
    }
    if ($data > 60) {
        $min = intval($data/60);
        $data = $data % 60;
    }
    $s_data = "";
    
    global $i_Usage_Hour, $i_Usage_Min, $i_Usage_Sec;
    if ($hr != "") $s_data .= "$hr $i_Usage_Hour";
    if ($min != "") $s_data .= "$min $i_Usage_Min";
    $s_data .= "$data $i_Usage_Sec";
    
    return $s_data;
}
?>