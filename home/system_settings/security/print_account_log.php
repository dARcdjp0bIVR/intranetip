<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$avgSourceAry = $_POST['exportAry'];
// debug_pr($avgSourceAry);
$numRow = count($avgSourceAry);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $avgSourceAry[$i][0];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][1];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][2];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][3];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][4];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][5];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][6];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][7];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][8];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][9];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][10];
}
// debug_pr($dataAry);
    //$htmlAry['resultTable'] = $libenroll_report->getStudentWithoutEnrolmentReportResultTableHtml($dataAry);
$resultTable = "";
$resultTable .= "<table class='common_table_list_v30 view_table_list_v30'>";
$resultTable .= "<tr>";
$resultTable .= "<th style=\"text-align:center;width:1%;\">#</th>";
$resultTable .= "<th style=\"text-align:center;width:8%;\">Date</th>";
$resultTable .= "<th style=\"text-align:center;width:6%;\">Time</th>";
$resultTable .= "<th style=\"text-align:center;width:6%;\">IP Address</th>";
$resultTable .= "<th style=\"text-align:center; width:11%;\">Admin Userlogin</th>";
$resultTable .= "<th style=\"text-align:center; width:12%;\">Admin User (En)</th>";
$resultTable .= "<th style=\"text-align:center; width:11%;\">Admin User (Ch)</th>";
$resultTable .= "<th style=\"text-align:center;width:6%;\">Action</th>";
$resultTable .= "<th style=\"text-align:center;width:11%;\">Affected Userlogin</th>";
$resultTable .= "<th style=\"text-align:center;width:12%;\">Affected User (En)</th>";
$resultTable .= "<th style=\"text-align:center;width:11%;\">Affected User (Ch)</th>";

$resultTable .= "</tr>";
for ($j=0; $j<$numRow; $j++){
    $resultTable .= "<tr>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][0]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][1]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][2]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][3]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][4]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][5]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][6]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][7]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][8]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][9]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][10]. "</td>";
    $resultTable .= "</tr>";
}
$resultTable .= "</table>";
?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><? echo $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<? echo ($resultTable)?>
<?
intranet_closedb();
?>