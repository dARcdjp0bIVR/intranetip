<?php
// using: 

// ############ Change Log [Start] ################
//
// Date: 2018-08-31 [Cameron]
// Hide Password Policy and Account Logout Policy tab for HKPF
//
// Date: 2018-08-30 [Philips]
// Create this file
// ############ Change Log [End] ################
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libauth.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libaccount.php");

intranet_auth();
intranet_opendb();

if (! $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || !$sys_custom['project']['HKPF']) {
    header("Location: /");
    intranet_closedb();
    exit();
}

$ldb = new libdb();
$lu = new libuser();
$linterface = new interface_html();
//
//ini_set('display_errors',1);
//error_reporting(E_ALL ^ E_NOTICE);
//set_time_limit(21600);
//ini_set('max_input_time', 21600);

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_AccountLog'] = 1;

// ## Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Security']['Title'];

if (!$sys_custom['project']['HKPF']) {
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['PasswordPolicies'],
        "password_policy.php",
        $CurrentPageArr['SystemSecurity_Password']
    );
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['AccountLockoutPolicy'],
        "lockout_policy.php",
        $CurrentPageArr['SystemSecurity_Lockout']
    );
}

$TAGS_OBJ[] = array(
    $Lang['SystemSetting']['LoginRecords']['Title'],
    "login_records.php",
    $CurrentPageArr['SystemSecurity_LoginRecords']
);

if($sys_custom['project']['HKPF']){
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['AccountLog']['Title'],
        "account_log.php",
        $CurrentPageArr['SystemSecurity_AccountLog']
    );
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['ReportLog']['Title'],
        "report_log.php",
        $CurrentPageArr['SystemSecurity_ReportLog']
    );
}
// $TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);

$PAGE_NAVIGATION[] = array(
    $Lang['SystemSetting']['AccountLog']['FilterOption'],
    ""
);

// Get Current Date Time
$now = time();
$today = date('Y-m-d', $now);

// Date Range Selection Box Option
$ts_weekstart = mktime(0, 0, 0, date('m', $now), date('d', $now) - date('w', $now), date('Y', $now));
$ts_weekend = mktime(0, 0, - 1, date('m', $ts_weekstart), date('d', $ts_weekstart) + 7, date('Y', $ts_weekstart));
$ts_monthstart = mktime(0, 0, 0, date('m', $now), 1, date('Y', $now));
$ts_monthend = mktime(0, 0, - 1, date('m', $now) + 1, 1, date('Y', $now));

$weekstart = date('Y-m-d', $ts_weekstart);
$weekend = date('Y-m-d', $ts_weekend);
$monthstart = date('Y-m-d', $ts_monthstart);
$monthend = date('Y-m-d', $ts_monthend);
$yearstart = date('Y-m-d', getStartOfAcademicYear($now));
$yearend = date('Y-m-d', getEndOfAcademicYear($now));

// filter Options

$filterOpts = array();
$filterOpts['name'] = $filter_username;
$filterOpts['action'] = $filter_action;
if ($textFromDate == null && $textToDate == null) {
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
} else {
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}
// debug_pr($filter_username);
// debug_pr($filter_action);
// debug_pr($filter_logDate_start);

// Paging Options
if (! isset($pageNo)) {
    $pageNo = 1;
}
if (! isset($pageSize)) {
    $pageSize = 50;
}
$paging = array(
    "pageNo" => $pageNo,
    "pageSize" => $pageSize
);

// Sorting Options
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get All the Log Records
$logs = $lu->getAccountLog($filterOpts, $paging, $sortOpts);
$pageMax = $logs['PAGEMAX'];
$pageItem = $pageMax;
$pageMax = ceil($pageMax / $pageSize);
unset($logs['PAGEMAX']);

// filter Options


// debug_pr($logs);
// debug_pr($filterOpts);
// debug_pr($paging);

// Table Column Setting
$cols = array(
    array(
        'text' => '#',
        'style' => 'width: 1%;',
        'col' => ""
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Date'],
        'style' => 'width: 8%',
        'col' => 'LogDate'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Time'],
        'style' => 'width: 6%',
        'col' => 'LogTime'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['IPAddress'],
        'style' => 'width: 6%',
        'col' => 'IPAddress'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBylogin'],
        'style' => 'width: 11%',
        'col' => ""
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)',
        'style' => 'width: 12%',
        'col' => 'LogEnglishName'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)',
        'style' => 'width: 11%',
        'col' => 'LogChineseName'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Action'],
        'style' => 'width: 6%',
        'col' => 'Action'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Userlogin'],
        'style' => 'width: 11%',
        'col' => ""
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['User'] . ' (En)',
        'style' => 'width: 12%',
        'col' => 'EnglishName'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['User'] . ' (Ch)',
        'style' => 'width: 11%',
        'col' => 'ChineseName'
    )
);

// Data Fields Setting
$values = array(
    "", "LogDate", "LogTime", "IPAddress", "LogBy", "LogEnglishName", 
    "LogChineseName", "Action", "UserLogin", "EnglishName", "ChineseName"
);

$exportHiddenField = "";
$dataAry = "";
$x = "<table class='common_table_list_v30 view_table_list_v30'>";

$x .= "<thead>";
$x .= "<tr>";
for($i=0;$i<sizeof($cols);$i++) {
    if($cols[$i]['col']!=''){
        $x .= "<th style='$col[style]'><a href='javascript:sortTable(\"".$cols[$i]['col']."\")'>" . $cols[$i]['text'] . '</a></th>';
    } else {
        $x .= "<th style='$col[style]'>" . $cols[$i]['text'] . '</th>';
    }
}
$x .= "</tr>";
$x .= "</thead>";

$x .= "<tbody>";
// debug_pr($values);
// debug_pr($logs);
// debug_pr($dataAry);

// Show no record message
if (sizeof($logs) == 0) {
    $x .= "<tr>";
    $x .= '<td align="center" colspan="' . sizeof($cols) . '">' . $i_no_record_exists_msg . '</td>';
    $x .= "</tr>";
} else {
    // Print every record into a table row
    $index = 1;
    foreach($logs as $log){
        $x .= "<tr>";
        foreach($values as $val){
            if($val == 'Action'){
                $x .= "<td>" . decodeCRUD($log[$val]) . "</td>";
            } else if($val != ''){
                $x .= "<td>" . $log[$val] . "</td>";
            } else {
                $x .= "<td>" . $index . "</td>";
            }
        }
        $x .= "</tr>";
        $index++;
    }
    /*for ($no_dataAry = 0; $no_dataAry < $index - 1; $no_dataAry ++) {
        $x .= "<tr>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][Order] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][Date] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][Time] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][IP_Address] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][AdminUserlogin] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][LogNameEn] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][LogNameCh] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][Action] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][Userlogin] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][UsernameEn] . "</td>";
        $x .= "<td>" . $dataAry[SS][$no_dataAry][UsernameCh] . "</td>";
        $x .= "</tr>";
    }*/
}
// debug_pr(count($dataAry[SS][0]));
$x .= "</tbody>";
$x .= "</table>";

// End of Table Content

// Print Button
$subBtnAry[] = array(
    'javascript: goPrint();',
    $Lang['SystemSetting']['ReportLog']['Action']['html']
);
$subBtnAry[] = array(
    'javascript: goPDF(0);',
    $Lang['SystemSetting']['ReportLog']['Action']['pdf']
);
$btnAry[] = array(
    'print',
    'javascript: void(0);',
    '',
    $subBtnAry
);

$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

$linterface->LAYOUT_START();
?>
<style>
fieldset {
	background-color: lightgrey;
}

.filter_panel {
	width: 40%;
	height: auto;
}

log_content {
	margin-left: 5%;
	margin-right: 5%;
	width: 90%;
	padding-bottom: 20px;
	border: lightgrey 1px solid;
	border-radius: 25px;
	background-color: #ffffb3;
}

#logTable th, #logTable td {
	text-align: center;
}

#logTable {
	margin-top: 10px;
	margin-left: 5%;
	margin-right: 5%;
	width: 90%;
	border-collapse: collapse;
}

#logTable thead tr {
	height: 30px;
	background-color: #FFD0FF;
}

#logTable tbody tr {
	border: 1px black solid;
	border-left: 0;
	border-right: 0;
}

#logTable tbody tr:nth-child(odd) {
	background-color: #CCD0FF;
}

#logTable tbody tr:nth-child(even) {
	background-color: #AAD0FF;
}
</style>
<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption"> <a
		href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	<p class="spacer"></p>
	<span id="Form_Span" class="Form_Span" style="display: none">
		<div class="filter_panel">
 	<? //echo ($linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)); ?>
 	<form name="form1" id="form1" method="get" action="account_log.php">
				<table class="form_table_v30">
					<tr>
						<td class='field_title'><label for='filter_username'><?=$Lang['SystemSetting']['AccountLog']['Keyword']?>: </label>
						</td>
						<td><input type="text" name="filter_username"
							value='<?=$filter_username?>' /></td>
					</tr>
					<!--tr>
  			   <td class='field_title'>
  				    <?=$Lang['SystemSetting']['AccountLog']['showID']?>
  			   </td>
  			   <td>
  				    <label for='filter_userID'><input type="checkbox" name="filter_userID" <?= ($filter_userID) ? 'checked' : ''?> /></label>
  			   </td>
  		    </tr-->
					<tr>
						<td class='field_title'><label for='filter_action'><?=$Lang['SystemSetting']['AccountLog']['logType']?>: </label>
						</td>
						<td><select name="filter_action">
								<option value=''><?=$Lang['SystemSetting']['AccountLog']['All']?></option>
								<option value='C' <?=($filter_action=='C') ? 'selected' : ''?>><?=decodeCRUD('C')?></option>
								<!-- <option value='R' <?=($filter_action=='R') ? 'selected' : ''?> ><?=decodeCRUD('R')?></option>-->
								<option value='U' <?=($filter_action=='U') ? 'selected' : ''?>><?=decodeCRUD('U')?></option>
								<option value='D' <?=($filter_action=='D') ? 'selected' : ''?>><?=decodeCRUD('D')?></option>
						</select></td>
					</tr>
					<!--   		<tr> -->
					<!--   			<td class='field_title'> -->
					<!--   				<label for='filter_logDate'><?=$Lang['SystemSetting']['AccountLog']['DateRange']?>:</label> -->
					<!--   			</td> -->
					<!--   			<td> -->
					<!--   				<label for='filter_logDate_start'><?=$Lang['SystemSetting']['AccountLog']['From']?>:</label> -->
					<!--   				<input type='date' name='filter_logDate_start' value='<?=$filter_logDate_start?>' /> -->
					<!--   				<label for='filter_logDate_end'> <?=$Lang['SystemSetting']['AccountLog']['To']?>:</label> -->
					<!--   				<input type='date' name='filter_logDate_end' value='<?=$filter_logDate_end?>' /> -->
					<!--   			</td> -->
					<!--   		</tr> -->
					<tr>
						<td rowspan='2' class='field_title'><label for='filter_logDate'><?=$Lang['SystemSetting']['AccountLog']['DateRange']?>:</label>
						</td>
						<td><SELECT id="datetype" name="datetype"
							onChange="changeDateType(this.form)">;
								<OPTION value=0 <?=$selected[0]?>><?php echo $i_Profile_Today?></OPTION>
								<OPTION value=1 <?=$selected[1]?>><?php echo $i_Profile_ThisWeek?></OPTION>
								<OPTION value=2 <?=$selected[2]?>><?php echo $i_Profile_ThisMonth?></OPTION>
								<OPTION value=3 <?=$selected[3]?>><?php echo $i_Profile_ThisAcademicYear?></OPTION>
						</SELECT></td>
					</tr>
					<tr>
						<td onClick="changeRadioSelection('DATE')"
							onFocus="changeRadioSelection('DATE')"><label
							for='filter_logDate_start'><?=$Lang['SystemSetting']['AccountLog']['From']?>:</label>
					<?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='filterByCategory();' ")?>
					<?// echo $linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?>
					<label for='filter_logDate_end'> <?=$Lang['SystemSetting']['AccountLog']['To']?>:</label>
					<?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='filterByCategory();' ")?>
					<?=$linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?>
				</td>
					</tr>
				</table>
				<center>
  		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "filter_submit()")?>
  	</center>
				<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
				<input type="hidden" name="pageSize" id="pageSize" value="<?=$pageSize?>" />
				<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>" />
				<input type="hidden" name="sortOrder" id="sortOrder" value="<?=$sortOrder?>" />
			</form>
		</div>
	</span>
</div>

<!-- for export and printing function -->
<form id="form2" name="form2" method="post">
	<?php echo $exportHiddenField; ?>
</form>

<br />
<div id="contentToolDiv" class="content_top_tool" style="">
	<div class="Conntent_tool">
		<?php echo $linterface->Get_Content_Tool_v30("export", "javascript:goExport();");?>
		<?php echo $htmlAry['contentTool'];?>
	</div>
</div>
<div class="log_content">
 <?=$x?>
 <br>
	<div style='display: inline-block; width: 50%; padding: 0; margin: 0;'>
 <?=$Lang['SystemSetting']['AccountLog']['PageRecord']?> <?=($pageNo-1) * $pageSize + 1?> - <?=(($pageNo==$pageMax) ? $pageItem : $pageNo * $pageSize)?>
 | <?=$Lang['SystemSetting']['AccountLog']['PageItem']?>: <?=$pageItem?>
 | <?=$Lang['SystemSetting']['AccountLog']['ShowPage']?> 
 <select id="pageSize_setting" onchange="pageSize_change()">
			<option value="10" <?=(($pageSize==10)?'selected':'')?>>10</option>
			<option value="20" <?=(($pageSize==20)?'selected':'')?>>20</option>
			<option value="30" <?=(($pageSize==30)?'selected':'')?>>30</option>
			<option value="40" <?=(($pageSize==40)?'selected':'')?>>40</option>
			<option value="50" <?=(($pageSize==50)?'selected':'')?>>50</option>
		</select>
	</div>
	<div
		style='display: inline-block; text-align: right; width: 49%; padding: 0; margin: 0;'>
 <?=$Lang['SystemSetting']['AccountLog']['PageMax']?>: <?=$pageMax?>
 <?php if($pageNo > 1){?>
 	| <a href="javascript:prevPage()" class="tablelink"><?=$Lang['SystemSetting']['AccountLog']['PrevPage']?></a>
 <?php

}
echo "| " . $Lang['SystemSetting']['AccountLog']['CurrPage'] . " : " . $pageNo;
if ($pageNo < $pageMax) {
    ?>
 	| <a href="javascript:nextPage()" class="tablelink"><?=$Lang['SystemSetting']['AccountLog']['NextPage']?></a>
 <?php }?>
 </div>
</div>
<script language="javascript">
if($('input[name="textFromDate"]').val()==''){
}
if($('input[name="textToDate"]').val()==''){
}

function hideOptionLayer(){
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer(){
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function checkForm(){
	var canSubmit = true;	
		
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	
		canSubmit = false;
		$('div#div_DateEnd_err_msg').show();
	}
}

function nextPage(){
	var pageNo = $('#pageNo');
	pageNo.val(parseInt(pageNo.val())+1);
	$('#form1').submit();
}
function prevPage(){
	var pageNo = $('#pageNo');
	pageNo.val(parseInt(pageNo.val())-1);
	$('#form1').submit();
}
function changeDateType(obj){
	switch (obj.datetype.value){
 		case '0':
   			obj.textFromDate.value = '<?=$today?>';
         	obj.textToDate.value = '<?=$today?>';
        	break;
    	case '1':
         	obj.textFromDate.value = '<?=$weekstart?>';
         	obj.textToDate.value = '<?=$weekend?>';
           	break;
      	case '2':
           	obj.textFromDate.value = '<?=$monthstart?>';
          	obj.textToDate.value = '<?=$monthend?>';
           	break;
     	case '3':
         	obj.textFromDate.value = '<?=$yearstart?>';
          	obj.textToDate.value = '<?=$yearend?>';
           	break;
	}
         //obj.submit();
}

function filter_submit(){
	var canSubmit = true;	
	if (compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	
		canSubmit = false;
		$('div#div_DateEnd_err_msg').show();
	}
	var pageNo = $('#pageNo');
	var pageSize = $('#pageSize');
	pageNo.val(1);
	pageSize.val(50);
	$('#sortField').val('');
	$('#sortOrder').val('');
	$('#form1').submit();
}
function pageSize_change(){
	var pageSize = $('#pageSize_setting').val();
	$('#pageSize').val(pageSize);
	$('#form1').submit();
}

function goExport(){
	$('form#form1').attr('target', '_self').attr('action', 'account_log_export.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'account_log.php');
}

function goPrint(){
	//$('form#form2').attr('target', '_blank').attr('action', 'print_account_log.php').submit();
	$('form#form1').attr('target', '_blank').attr('action', 'account_log_print.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'account_log.php');
	
}
function goPDF(){
	//$('form#form2').attr('target', '_blank').attr('action', 'print_account_log.php').submit();
	$('form#form1').attr('target', '_blank').attr('action', 'account_log_pdf.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'account_log.php');
	
}
function sortTable(colVal){
	if($('#sortOrder').val() == '' || $('#sortOrder').val() == 1|| colVal != $('#sortField').val()){
		$('#sortOrder').val(0);
	} else {
		$('#sortOrder').val(1);		
	}
	$('#sortField').val(colVal);
	$('form#form1').submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

function decodeCRUD($target)
{
    $result = "";
    global $Lang;
    switch ($target) {
        case 'C':
            $result = $Lang['SystemSetting']['AccountLog']['Create'];
            break;
        case 'R':
            $result = $Lang['SystemSetting']['AccountLog']['Read'];
            break;
        case 'U':
            $result = $Lang['SystemSetting']['AccountLog']['Update'];
            break;
        case 'D':
            $result = $Lang['SystemSetting']['AccountLog']['Delete'];
            break;
        default:
            break;
    }
    return $result;
}
?>