<?php
// using: 

// ############ Change Log [Start] ################
//
// Date: 2018-08-31 [Cameron]
// Hide Password Policy and Account Logout Policy tab for HKPF
//
// Date: 2018-08-30 [Philips]
// Create this file
// ############ Change Log [End] ################
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libauth.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libaccount.php");
include_once ($PATH_WRT_ROOT . "includes/libuserlogreport.php");

intranet_auth();
intranet_opendb();

if (! $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || !$sys_custom['project']['HKPF']) {
    header("Location: /");
    intranet_closedb();
    exit();
}

$ldb = new libdb();
$lu = new libuserlogreport();
$linterface = new interface_html();

// setting top menu highlight
$CurrentPageArr['SystemSecurity'] = 1;
$CurrentPageArr['SystemSecurity_ReportLog'] = 1;

// ## Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['Security']['Title'];

if (!$sys_custom['project']['HKPF']) {
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['PasswordPolicies'],
        "password_policy.php",
        $CurrentPageArr['SystemSecurity_Password']
    );
    $TAGS_OBJ[] = array(
        $Lang['SysMgr']['Security']['AccountLockoutPolicy'],
        "lockout_policy.php",
        $CurrentPageArr['SystemSecurity_Lockout']
    );
}

$TAGS_OBJ[] = array(
    $Lang['SystemSetting']['LoginRecords']['Title'],
    "login_records.php",
    $CurrentPageArr['SystemSecurity_LoginRecords']
);

if($sys_custom['project']['HKPF']){
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['AccountLog']['Title'],
        "account_log.php",
        $CurrentPageArr['SystemSecurity_AccountLog']
    );
    $TAGS_OBJ[] = array(
        $Lang['SystemSetting']['ReportLog']['Title'],
        "report_log.php",
        $CurrentPageArr['SystemSecurity_ReportLog']
    );
}
// $TAGS_OBJ[] = array($Lang['SysMgr']['RoleManagement']['ModuleRole'],"module_role_index.php",0);

$PAGE_NAVIGATION[] = array(
    $Lang['SystemSetting']['AccountLog']['FilterOption'],
    ""
);

// Get Current Time
$now = time();
$today = date('Y-m-d', $now);

// Date Range Selection Box
$ts_weekstart = mktime(0, 0, 0, date('m', $now), date('d', $now) - date('w', $now), date('Y', $now));
$ts_weekend = mktime(0, 0, - 1, date('m', $ts_weekstart), date('d', $ts_weekstart) + 7, date('Y', $ts_weekstart));
$ts_monthstart = mktime(0, 0, 0, date('m', $now), 1, date('Y', $now));
$ts_monthend = mktime(0, 0, - 1, date('m', $now) + 1, 1, date('Y', $now));

$weekstart = date('Y-m-d', $ts_weekstart);
$weekend = date('Y-m-d', $ts_weekend);
$monthstart = date('Y-m-d', $ts_monthstart);
$monthend = date('Y-m-d', $ts_monthend);
$yearstart = date('Y-m-d', getStartOfAcademicYear($now));
$yearend = date('Y-m-d', getEndOfAcademicYear($now));

// filter Options
if (! isset($filter_Action)) {
    $filter_Action = - 1;
}
if (! isset($filter_Rtype)) {
    $filter_Rtype = - 1;
}

$filterOpts = array();
$filterOpts['UserName'] = $filter_username;
$filterOpts['Action'] = $filter_Action;
$filterOpts['Rtype'] = $filter_Rtype;
if ($textFromDate == null && $textToDate == null) {
    $filterOpts['DateStart'] = date("Y-m-d");
    $filterOpts['DateEnd'] = date("Y-m-d");
} else {
    $filterOpts['DateStart'] = $textFromDate;
    $filterOpts['DateEnd'] = $textToDate;
}

// Report type and action taken
$typeSelection = $lu->getReportTypeSelection($filter_Rtype, 'filter_Rtype');
$actionSelection = $lu->getReportActionSelection($filter_Action, 'filter_Action');

// Paging Options

if (! isset($pageNo)) {
    $pageNo = 1;
}
if (! isset($pageSize)) {
    $pageSize = 50;
}
$paging = array(
    "pageNo" => $pageNo,
    "pageSize" => $pageSize
);

// Ordering Option
$sortOpts = array();
$sortOpts['Field'] = $sortField;
$sortOpts['Order'] = $sortOrder;

// Get Recrd
$logs = $lu->getAllLogReport($filterOpts, $paging, $sortOpts);
$pageMax = $logs['PAGEMAX'];
$pageItem = $pageMax;
$pageMax = ceil($pageMax / $pageSize);
unset($logs['PAGEMAX']);

// Table Column Setting
$cols = array(
    array(
        'text' => '#',
        'style' => 'width: 1%;'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Date'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['Time'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['IPAddress'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Rtype'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['ReportLog']['Column']['Action'],
        'style' => 'width: 12%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBylogin'],
        'style' => 'width: 13%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (En)',
        'style' => 'width: 13%'
    ),
    array(
        'text' => $Lang['SystemSetting']['AccountLog']['LogBy'] . ' (Ch)',
        'style' => 'width: 13%'
    )
);
$values = array(
    "LogDate",
    "LogTime",
    "IPAddress",
    "Rtype",
    "Action",
    "UserLogin",
    "EnglishName",
    "ChineseName"
);

// Start Of Table Content
$resultTable = "<table class='common_table_list_v30 view_table_list_v30'>";

// Table Header Column
$resultTable .= "<thead>";
$resultTable .= "<tr>";
for($i=0; $i < sizeof($cols); $i++){
    if($i!=0){
        $resultTable .= "<th style='".$cols[$i]['style']."'><a href='javascript:sortTable(\"".$values[$i-1]."\")'>".$cols[$i]['text']."</a></th>";
    } else {
        $resultTable .= "<th style='".$cols[$i]['style']."'>".$cols[$i]['text']."</th>";
    }
}
$resultTable .= "</tr>";
$resultTable .= "</thead>";

// Table Body Content
$resultTable .= "<tbody>";
if (sizeof($logs) == 0) {
    // No Data
    $resultTable .= "<tr>";
    $resultTable .= '<td align="center" colspan="' . sizeof($cols) . '">' . $i_no_record_exists_msg . '</td>';
    $resultTable .= "</tr>";
} else {
    // Print every record into table row
    $index = 1;
    foreach ($logs as $log) {
        $resultTable .= "<tr>";
        $resultTable .= "<td>$index</td>";
        foreach ($values as $value) {
            if ($value == 'Rtype') {
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Rtype'][$log[$value]] . "</td>";
            } else if ($value == 'Action') {
                $resultTable .= "<td>" . $Lang['SystemSetting']['ReportLog']['Action'][$log[$value]] . "</td>";
            } else {
                $resultTable .= "<td>" . $log[$value] . "</td>";
            }
        }
        $resultTable .= "</tr>";
        $index ++;
    }
}

$resultTable .= "</tbody>";
$resultTable .= "</table>";
// END of Table Content

// Print Button
$subBtnAry[] = array(
    'javascript: goPrint();',
    $Lang['SystemSetting']['ReportLog']['Action']['html']
);
$subBtnAry[] = array(
    'javascript: goPDF(0);',
    $Lang['SystemSetting']['ReportLog']['Action']['pdf']
);
$btnAry[] = array(
    'print',
    'javascript: void(0);',
    '',
    $subBtnAry
);

$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

$linterface->LAYOUT_START();
?>
<style>
fieldset {
	background-color: lightgrey;
}

.filter_panel {
	width: 40%;
	height: auto;
}

log_content {
	margin-left: 5%;
	margin-right: 5%;
	width: 90%;
	padding-bottom: 20px;
	border: lightgrey 1px solid;
	border-radius: 25px;
	background-color: #ffffb3;
}
</style>
<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption"> <a
		href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span> <span id="spanHideOption" class="spanHideOption"
		style="display: none"> <a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	<p class="spacer"></p>
	<span id="Form_Span" class="Form_Span" style="display: none">
		<div class="filter_panel">
 	<? //echo ($linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)); ?>
 	<form name="form1" id="form1" method="get" action="report_log.php">
				<table class="form_table_v30">
					<tr>
						<td class='field_title'><label for='filter_username'><?=$Lang['SystemSetting']['AccountLog']['Keyword']?>: </label>
						</td>
						<td><input type="text" name="filter_username"
							value='<?=$filter_username?>' /></td>
					</tr>
					<!--tr>
  			   <td class='field_title'>
  				    <?=$Lang['SystemSetting']['AccountLog']['showID']?>
  			   </td>
  			   <td>
  				    <label for='filter_userID'><input type="checkbox" name="filter_userID" <?= ($filter_userID) ? 'checked' : ''?> /></label>
  			   </td>
  		    </tr-->
					<!--   		<tr> -->
					<!--   			<td class='field_title'> -->
					<!--   				<label for='filter_logDate'><?=$Lang['SystemSetting']['AccountLog']['DateRange']?>:</label> -->
					<!--   			</td> -->
					<!--   			<td> -->
					<!--   				<label for='filter_logDate_start'><?=$Lang['SystemSetting']['AccountLog']['From']?>:</label> -->
					<!--   				<input type='date' name='filter_logDate_start' value='<?=$filter_logDate_start?>' /> -->
					<!--   				<label for='filter_logDate_end'> <?=$Lang['SystemSetting']['AccountLog']['To']?>:</label> -->
					<!--   				<input type='date' name='filter_logDate_end' value='<?=$filter_logDate_end?>' /> -->
					<!--   			</td> -->
					<!--   		</tr> -->
					<tr>
						<td class='field_title'><label for='filter_Rtype'><?=$Lang['SystemSetting']['ReportLog']['Column']['Rtype']?>: </label>
						</td>
						<td>
  					<?=$typeSelection?>
  				</td>
					</tr>
					<tr>
						<td class='field_title'><label for='filter_Rtype'><?=$Lang['SystemSetting']['ReportLog']['Column']['Action']?>: </label>
						</td>
						<td>
  					<?=$actionSelection?>
  				</td>
					</tr>
					<tr>
						<td rowspan='2' class='field_title'><label for='filter_logDate'><?=$Lang['SystemSetting']['AccountLog']['DateRange']?>:</label>
						</td>
						<td><SELECT id="datetype" name="datetype"
							onChange="changeDateType(this.form)">;
								<OPTION value=0 <?=$selected[0]?>><?php echo $i_Profile_Today?></OPTION>
								<OPTION value=1 <?=$selected[1]?>><?php echo $i_Profile_ThisWeek?></OPTION>
								<OPTION value=2 <?=$selected[2]?>><?php echo $i_Profile_ThisMonth?></OPTION>
								<OPTION value=3 <?=$selected[3]?>><?php echo $i_Profile_ThisAcademicYear?></OPTION>
						</SELECT></td>
					</tr>
					<tr>
						<td onClick="changeRadioSelection('DATE')"
							onFocus="changeRadioSelection('DATE')"><label
							for='filter_logDate_start'><?=$Lang['SystemSetting']['AccountLog']['From']?>:</label>
					<?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='filterByCategory();' ")?>
					<?// echo $linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?>
					<label for='filter_logDate_end'> <?=$Lang['SystemSetting']['AccountLog']['To']?>:</label>
					<?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='filterByCategory();' ")?>
					<?=$linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?>
				</td>
					</tr>
				</table>
				<center>
  		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "filter_submit()")?>
  	</center>
				<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
				<input type="hidden" name="pageSize" id="pageSize" value="<?=$pageSize?>" />
				<input type="hidden" name="sortField" id="sortField" value="<?=$sortField?>" />
				<input type="hidden" name="sortOrder" id="sortOrder" value="<?=$sortOrder?>" />
			</form>
		</div>
	</span>
</div>

<!-- for export and printing function -->
<form id="form2" name="form2" method="post">
	<?php echo $exportHiddenField; ?>
</form>

<br />
<div id="contentToolDiv" class="content_top_tool" style="">
	<div class="Conntent_tool">
		<?php echo $linterface->Get_Content_Tool_v30("export", "javascript:goExport();");?>
		<?php echo $htmlAry['contentTool'];?>
	</div>
</div>
<div class="log_content">
 <?=$resultTable?>
 <br>
	<div style='display: inline-block; width: 50%; padding: 0; margin: 0;'>
 <?=$Lang['SystemSetting']['AccountLog']['PageRecord']?> <?=($pageNo-1) * $pageSize + 1?> - <?=(($pageNo==$pageMax) ? $pageItem : $pageNo * $pageSize)?>
 | <?=$Lang['SystemSetting']['AccountLog']['PageItem']?>: <?=$pageItem?>
 <?=$Lang['SystemSetting']['AccountLog']['ShowPage']?> 
 <select id="pageSize_setting" onchange="pageSize_change()">
			<option value="10" <?=(($pageSize==10)?'selected':'')?>>10</option>
			<option value="20" <?=(($pageSize==20)?'selected':'')?>>20</option>
			<option value="30" <?=(($pageSize==30)?'selected':'')?>>30</option>
			<option value="40" <?=(($pageSize==40)?'selected':'')?>>40</option>
			<option value="50" <?=(($pageSize==50)?'selected':'')?>>50</option>
		</select>
	</div>
	<div
		style='display: inline-block; text-align: right; width: 49%; padding: 0; margin: 0;'>
 <?=$Lang['SystemSetting']['AccountLog']['PageMax']?>: <?=$pageMax?>
 <?php if($pageNo > 1){?>
 	| <a href="javascript:prevPage()" class="tablelink"><?=$Lang['SystemSetting']['AccountLog']['PrevPage']?></a>
 <?php

}
echo "| " . $Lang['SystemSetting']['AccountLog']['CurrPage'] . " : " . $pageNo;
if ($pageNo < $pageMax) {
    ?>
 	| <a href="javascript:nextPage()" class="tablelink"><?=$Lang['SystemSetting']['AccountLog']['NextPage']?></a>
 <?php }?>
 </div>
</div>
<script language="javascript">
if($('input[name="textFromDate"]').val()==''){
}
if($('input[name="textToDate"]').val()==''){
}

function hideOptionLayer(){
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer(){
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function checkForm(){
	var canSubmit = true;	
		
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	
		canSubmit = false;
		$('div#div_DateEnd_err_msg').show();
	}
}

function nextPage(){
	var pageNo = $('#pageNo');
	pageNo.val(parseInt(pageNo.val())+1);
	$('#form1').submit();
}
function prevPage(){
	var pageNo = $('#pageNo');
	pageNo.val(parseInt(pageNo.val())-1);
	$('#form1').submit();
}
function changeDateType(obj){
	switch (obj.datetype.value){
 		case '0':
   			obj.textFromDate.value = '<?=$today?>';
         	obj.textToDate.value = '<?=$today?>';
        	break;
    	case '1':
         	obj.textFromDate.value = '<?=$weekstart?>';
         	obj.textToDate.value = '<?=$weekend?>';
           	break;
      	case '2':
           	obj.textFromDate.value = '<?=$monthstart?>';
          	obj.textToDate.value = '<?=$monthend?>';
           	break;
     	case '3':
         	obj.textFromDate.value = '<?=$yearstart?>';
          	obj.textToDate.value = '<?=$yearend?>';
           	break;
	}
         //obj.submit();
}

function filter_submit(){
	var canSubmit = true;	
	if (compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	
		canSubmit = false;
		$('div#div_DateEnd_err_msg').show();
	}
	var pageNo = $('#pageNo');
	var pageSize = $('#pageSize');
	pageNo.val(1);
	pageSize.val(50);
	$('#form1').submit();
}
function pageSize_change(){
	var pageSize = $('#pageSize_setting').val();
	$('#pageSize').val(pageSize);
	$('#form1').submit();
}

function goExport(){
	$('form#form2').attr('target', '_self').attr('action', 'report_log_export.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'report_log.php');
}

function goPrint(){
	//$('form#form2').attr('target', '_blank').attr('action', 'print_account_log.php').submit();
	$('form#form1').attr('target', '_blank').attr('action', 'report_log_print.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'report_log.php');
	
}
function goPDF(){
	//$('form#form2').attr('target', '_blank').attr('action', 'print_account_log.php').submit();
	$('form#form1').attr('target', '_blank').attr('action', 'report_log_pdf.php').submit();
	$('form#form1').attr('target', '_self').attr('action', 'report_log.php');
	
}
function sortTable(colVal){
	if($('#sortOrder').val() == '' || $('#sortOrder').val() == 1|| colVal != $('#sortField').val()){
		$('#sortOrder').val(0);
	} else {
		$('#sortOrder').val(1);		
	}
	$('#sortField').val(colVal);
	$('form#form1').submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>