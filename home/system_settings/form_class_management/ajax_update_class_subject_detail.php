<?php
// using : pUN
/*******************************
 * Date: 	2015-10-22 (Pun)
 * Details: added sync subject group to eClass 
 * Date: 	2013-01-23 (Rita)
 * Details: add update log, add multi selection 
 *******************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libupdatelog.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_opendb();

$fcm = new form_class_manage();
$sbj = new subject();
$YearClass = new year_class($YearClassSelect,false,false,true);
$libupdatelog = new libupdatelog();
$le = new libeclass();

$sql = "SELECT YearID FROM YEAR_CLASS WHERE YearClassID='$YearClassSelect'";
$YearID = $fcm->returnVector($sql);

# Subject of selected class(Form)
$result = $sbj->Get_Subject_By_Form($YearID[0]);


# Subject Group ID
$subjectAry = array();
for($i=0; $i<sizeof($result); $i++) {
	list($SubjectID, $CH_Name, $EN_Name) = $result[$i];
	$subjectAry[] = $SubjectID;	
	$subjectEngNameAry[] = $EN_Name;
}

# Student ID
$UserList = $YearClass->ClassStudentList;
for($i=0; $i<sizeof($UserList); $i++)
	$UserIDAry[] = $UserList[$i]['UserID'];

$success = array();	

if(sizeof($subjectAry)>0 && sizeof($UserList)>0) {
	//$YearTermID = GetCurrentSemesterID();
	$sql = "DELETE FROM SUBJECT_TERM_CLASS_USER WHERE UserID IN (".implode(',', $UserIDAry).") AND SubjectGroupID IN (SELECT SubjectGroupID FROM SUBJECT_TERM WHERE SubjectID IN (".implode(',',$subjectAry).") AND YearTermID='$YearTermID')";
	$success['removeStudentFromSubjectGroup'] = $sbj->db_db_query($sql);
}



$delim = "";
$subjectGroupIDListArr = array();
$subjectGroupSelectedValueList = array();

for($i=0; $i<sizeof($subjectAry); $i++) {
	$subjectID = $subjectAry[$i];
	$subjectGroupIDAry = array();
	for($j=0; $j<sizeof($UserIDAry); $j++) {
		$userid = $UserIDAry[$j];
		$subjectGroupIDAry = ${'SubjectGroup_'.$subjectID.'_'.$userid};	

		// Customization for multiple selection
		if($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'] == true && count($subjectGroupIDAry)>0){
			$uinqueSubjectGroupIDAry = array();
			$uinqueSubjectGroupIDAry = array_values(array_unique($subjectGroupIDAry));
			for($k=0; $k<count($uinqueSubjectGroupIDAry); $k++) {
				$subjectGroupID = $uinqueSubjectGroupIDAry[$k];
		
				if(trim($subjectGroupID)!='') {
					$values .= $delim." ('$subjectGroupID', '$userid', NOW(), '$UserID', NOW(), '$userid')";
					$delim = ",";
					
					# For Updata Log Record Info
					$subjectGroupIDListArr[] = $userid . '_' . $subjectID. '_' . $subjectGroupID;			
				}
			}
	
		// Single selection
		}else{
	
			$subjectGroupID = $subjectGroupIDAry[1];
			if($subjectGroupID!="") {
				$values .= $delim." ('$subjectGroupID', '$userid', NOW(), '$UserID', NOW(), '$userid')";
				$delim = ",";	
				
				# For Updata Log Record Info
				$subjectGroupIDListArr[] = $userid . '_' . $subjectID. '_' . $subjectGroupID;	
			}	
	
		}

	}	
}

# Insert Record to DB
$sql = "INSERT INTO SUBJECT_TERM_CLASS_USER (SubjectGroupID, UserID, DateInput, InputBy, DateModified, ModifiedBy) VALUES $values";
$success['insertStudentToSubjectGroup'] = $sbj->db_db_query($sql);


#############    Start of Inserting Update Log    #############
$section = 'Update_Class_SubjectEnrol_Details';
$tableName = 'SUBJECT_TERM_CLASS_USER';

# Start inserting delete log
$tmpAry = array();
$tmpAry['YearClassID'] = $YearClassSelect;
$tmpAry['YearTermID'] = $YearTermID;
$tmpAry['UserIDList'] = implode(',',$UserIDAry);
$tmpAry['SubjectIDList'] = implode(',',$subjectAry);
$tmpAry['SubjectEngNameList'] = implode(',',$subjectEngNameAry);
$tmpAry['SubjectUserIDSubjectIDGroupIDListArr'] = implode(',',$subjectGroupIDListArr); 
$success['log'] = $libupdatelog->INSERT_UPDATE_LOG('School Settings', $section, $libupdatelog->BUILD_UPDATE_DETAIL($tmpAry), $tableName, $YearClassSelect);
	
#############     End of Inserting Update Log     #############


######## Sync subject group to eClass START ########
if($addStd){
	#### Get releated user and course START ####
	$idSql = implode("','", $UserIDAry);
	$sql = "SELECT 
		course_id,
		user_id,
		intranet_user_id
	FROM
		{$eclass_db}.user_course
	WHERE
		intranet_user_id IN ('{$idSql}')";
	$rs = $le->returnResultSet($sql);
	
	$courseIdArr = array();
	$userIdArr = array();
	$userCourseArr = array();
	foreach($rs as $r){
		$courseIdArr[] = $r['course_id'];
		$userIdArr[] = $r['user_id'];
		$userCourseArr[ $r['intranet_user_id'] ][] = $r['course_id'];
	}
	#### Get releated user and course END ####
	
	#### Get subject group START ####
	$subjectGroupIDAry = array();
	for($i=0; $i<sizeof($subjectAry); $i++) {
		$subjectID = $subjectAry[$i];
		for($j=0; $j<sizeof($UserIDAry); $j++) {
			$userid = $UserIDAry[$j];
			$sgArr = array_unique(array_filter(${'SubjectGroup_'.$subjectID.'_'.$userid}));
			if(count($sgArr)){
				$subjectGroupIDAry[$userid] = array_merge((array)$subjectGroupIDAry[$userid], $sgArr);
			}
		}
	}
	#### Get subject group END ####
	
	#### Create subject group START ####
	foreach ($subjectGroupIDAry as $uid=>$sg){
		foreach((array)$userCourseArr[$uid] as $course_id){
			$le->createSubjectGroupForClassroom($course_id, $sg);
		}
	}
	#### Create subject group END ####
	
	#### Sync START ####
	foreach($courseIdArr as $course_id){
		$le->syncUserToSubjectGroup($course_id, $userIdArr);
	}
	#### Sync END ####
}
######## Sync subject group to eClass END ########


if(in_array(false, $success)){
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassEditUnsuccess'];
}else{
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassEditSuccess'];
}

intranet_closedb();
?>