<?
// Modifying by: henry chow

############# Change Log [Start] ################
#
#	Date:	2010-07-08	YatWoon
#			Fixed: missing pass academic year id to function $libclass->getClassID(), so always update current year's class teacher
#
#	Date:	2009-12-15 YatWoon
#			- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libimporttext.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_opendb();

$li = new libdb();
$fcm = new form_class_manage();
$Year = new Year();
$libclass = new libclass();
$libteaching = new libteaching();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


############################################################################
# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
############################################################################
$sql = "select * from TEMP_CLASS_TEACHER_IMPORT where UserID = $UserID order by RowNumber";
$class_result = $li->returnArray($sql);

$c = 1;
foreach($class_result as $k=>$d)
{
	list($TempID, $ImportUserID, $RowNumber, $UserLogin, $ClassNameEn, $DateInput) = $d;
	
	## Get StudentID
	$sql = "Select UserID FROM INTRANET_USER Where UserLogin = '".$UserLogin."'";
	$resultSet = $li->returnVector($sql);
	$TeacherID = $resultSet[0];
	
	## Get New Class List
	$YearClassNameArr = explode("###", $ClassNameEn);
	$YearClassNameArr = Array_Trim($YearClassNameArr);
	$numOfYearClassName = count($YearClassNameArr);
	$NewYearClassIDArr = array();
	for ($i=0; $i<$numOfYearClassName; $i++)
	{
		$thisYearClassName = $YearClassNameArr[$i];
		$thisYearClassID = $libclass->getClassID($thisYearClassName, $AcademicYearID);
		
		$NewYearClassIDArr[] = $thisYearClassID;
	}
	
	
	if($c==1) {	# remove teacher in classes (which is not exist in csv list), only 1st time
		## Get Current Teacher Class
		$TeachingClassArr = $libteaching->returnTeacherClass($TeacherID, $AcademicYearID);
		$numOfTeachingClass = count($TeachingClassArr);
		$TeachingYearClassIDArr = array();
		for ($i=0; $i<$numOfTeachingClass; $i++)
		{
			$TeachingYearClassIDArr[] = $TeachingClassArr[$i]['ClassID'];
		}
		
		# Remove the teacher from classes
		$RemovedClassIDArr = array_diff($TeachingYearClassIDArr, $NewYearClassIDArr);
		$numOfRemovedClassID = count($RemovedClassIDArr);
		
		for ($i=0; $i<$numOfRemovedClassID; $i++)
		{
			$thisYearClassID = $RemovedClassIDArr[$i];
			$fcm->Delete_Class_Teacher($thisYearClassID, $TeacherID);
		}
	}
	
	for ($i=0; $i<$numOfYearClassName; $i++)
	{
		$thisYearClassID = $NewYearClassIDArr[$i];
		$fcm->Add_Class_Teacher($thisYearClassID, $TeacherID);
	}
	$c++;
}


############################################################################
# delete data in Temp table
############################################################################
$sql = "delete from TEMP_CLASS_TEACHER_IMPORT where UserID = $UserID";
$li->db_db_query($sql) or die(mysql_error());



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportFormClass']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

?>


<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		
		<tr>
			<td class='tabletext' align='center'><?=count($class_result)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'"); ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
