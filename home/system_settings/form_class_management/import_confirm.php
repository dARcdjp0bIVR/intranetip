<?
// Modifying by: 

############# Change Log [Start] ################
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ay = new academic_year($AcademicYearID);
$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
$libFCM = new form_class_manage();
$lcg = new Class_Group();

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?AcademicYearID=$AcademicYearID&xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$file_format = array("Form Name","Form WEBSAMS Code","Class Name(En)","Class Name(Chi)","Class WEBSAMS Code");
if($sys_custom['Class']['ClassGroupSettings'])
	$file_format[] = "Class Group Code";
	
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import.php?AcademicYearID=$AcademicYearID&xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportFormClass']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_FORM_CLASS_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		FormName varchar(100),
		FormWebsamsCode varchar(100),
		ClassNameEn varchar(100),
		ClassNameChi varchar(100),
		ClassWebsamsCode varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());

if($sys_custom['Class']['ClassGroupSettings'])
{
	$sql = "ALTER TABLE TEMP_FORM_CLASS_IMPORT ADD COLUMN ClassGroupCode varchar(100) AFTER ClassWebsamsCode";
	$li->db_db_query($sql);
}

# delete the temp data in temp table 
$sql = "delete from TEMP_FORM_CLASS_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=1;


foreach($data as $k=>$d)
{
	$i++;
	$error_msg = array();
	
	### store csv data to temp data
	list($FormName, $FormWebsamsCode, $ClassNameEn, $ClassNameChi, $ClassWebsamsCode,$ClassGroupCode) = $d;
	$FormName = trim($FormName);
	$FormWebsamsCode = strtoupper(trim($FormWebsamsCode));
	$ClassNameEn = trim($ClassNameEn);
	$ClassNameChi = trim($ClassNameChi);
	$ClassWebsamsCode = trim($ClassWebsamsCode);
	$ClassGroupCode = trim($ClassGroupCode);
	
	$fields = "UserID, RowNumber, FormName, FormWebsamsCode, ClassNameEn, ClassNameChi, ClassWebsamsCode, DateInput";
	$values =  $_SESSION["UserID"] .", $i, '". $li->Get_Safe_Sql_Query($FormName) ."', '". $li->Get_Safe_Sql_Query($FormWebsamsCode) ."', '". $li->Get_Safe_Sql_Query($ClassNameEn) ."', '". $li->Get_Safe_Sql_Query($ClassNameChi) ."', '". $li->Get_Safe_Sql_Query($ClassWebsamsCode) ."', now()";
	if($sys_custom['Class']['ClassGroupSettings'])
	{
		$fields .= ", ClassGroupCode";
		$values .= ", '".$li->Get_Safe_Sql_Query($ClassGroupCode)."'";
	}
	
	$sql = "
			insert into TEMP_FORM_CLASS_IMPORT 
			($fields)
			values
			($values)
			";
			
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();
	
	### check data
	# 1. Check empty data
	if(empty($FormName) || empty($ClassNameEn) || empty($ClassNameChi)|| ($sys_custom['Class']['ClassGroupSettings'] && empty($ClassGroupCode)))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'] . " (***)";
	# 2. Check Form WEBSAMS Code is exists or not
	if($FormWebsamsCode && !array_key_exists($FormWebsamsCode, $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['FormWebsamsCodeInvalid'];
	# 3. Class Name Must Not Contain Single / Double Quote
	if(!($libFCM->Is_ClassName_Valid($ClassNameEn)))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'];
	if(!($libFCM->Is_ClassName_Valid($ClassNameChi)))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'];
	
	# 4. Check if Class Group Code exists
	if($sys_custom['Class']['ClassGroupSettings'] == true)
	{
		if(!empty($ClassGroupCode))
		{
			if(!$ClassGroupID = $lcg->Get_Class_Group_ID_By_Code($ClassGroupCode)) 
				$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassGroupCodeInvalid'];
			else
			{
				$lcg->RecordID = $ClassGroupID;
				$lcg->Get_Self_Info();
				
				#5. Check if Class Group Enabled
				if($lcg->RecordStatus != 1)
					$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassGroupDisabled'];
			}
		}
	}

	if(empty($error_msg))
	{
		$successCount++;
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". $ay->Get_Academic_Year_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if(!empty($error_result))
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['Form'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['Form'] ." ". $Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['NameOfClassEng'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['NameOfClassChi'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['Class'] ." ". $Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] ."</td>";
	if($sys_custom['Class']['ClassGroupSettings']) $x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['ClassGroupCode']."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_FORM_CLASS_IMPORT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k=>$d)
	{
		if($sys_custom['Class']['ClassGroupSettings'] == true)
			list($t_TempID, $t_UserID, $t_RowNumber, $t_FormName, $t_FormWebsamsCode, $t_ClassNameEn, $t_ClassNameChi, $t_ClassWebsamsCode, $t_ClassGroupCode) = $d;
		else
			list($t_TempID, $t_UserID, $t_RowNumber, $t_FormName, $t_FormWebsamsCode, $t_ClassNameEn, $t_ClassNameChi, $t_ClassWebsamsCode) = $d;
		$t_FormName = $t_FormName ? $t_FormName : "<font color='red'>***</font>";
		$t_ClassNameEn = $t_ClassNameEn ? $t_ClassNameEn : "<font color='red'>***</font>";
		$t_ClassNameChi = $t_ClassNameChi ? $t_ClassNameChi : "<font color='red'>***</font>";
		$t_FormWebsamsCode = $t_FormWebsamsCode ? $t_FormWebsamsCode : "--";
		$t_ClassWebsamsCode = $t_ClassWebsamsCode ? $t_ClassWebsamsCode : "--";
		
		
		# red in error fields
		if($t_FormWebsamsCode && !array_key_exists($t_FormWebsamsCode, $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']))
			$t_FormWebsamsCode = "<font color='red'>". $t_FormWebsamsCode."</font>";
		if(!($libFCM->Is_ClassName_Valid($t_ClassNameEn)))														
			$t_ClassNameEn = "<font color='red'>". $t_ClassNameEn."</font>";
		if(!($libFCM->Is_ClassName_Valid($t_ClassNameChi)))														
			$t_ClassNameChi = "<font color='red'>". $t_ClassNameChi."</font>";
			
		if($sys_custom['Class']['ClassGroupSettings'] == true)
		{
			if(!$t_ClassGroupCode)
				$t_ClassGroupCode = "<font color='red'>***</font>";
			else if(!$ClassGroupID = $lcg->Get_Class_Group_ID_By_Code($t_ClassGroupCode)) 
				$t_ClassGroupCode =  "<font color='red'>". $t_ClassGroupCode."</font>";
			else
			{
				$lcg->RecordID = $ClassGroupID;
				$lcg->Get_Self_Info();
				if($lcg->RecordStatus != 1)
					$t_ClassGroupCode =  "<font color='red'>". $t_ClassGroupCode."</font>";
			}
		}
		
		# build error remark
		$error_remark = "";
		
		foreach($error_result[$t_TempID] as $k1=>$d1)
		{
			$error_remark .= "<li>". $d1."</li>";
		}
		
		$error_remark = $error_remark ? "<ul>".$error_remark."</ul>" : "";
			
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_FormName ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_FormWebsamsCode ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_ClassNameEn ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_ClassNameChi ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_ClassWebsamsCode ."</td>";
		if($sys_custom['Class']['ClassGroupSettings'] == true) $x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $t_ClassGroupCode ."</td>";
		$x .= "<td valign=\"top\" class=\"tablebluerow$css_i\">". $error_remark ."</td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}
//$x .= "</table>";

if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?AcademicYearID=$AcademicYearID'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?AcademicYearID=$AcademicYearID'");
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
