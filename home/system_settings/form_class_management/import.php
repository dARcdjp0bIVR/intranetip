<?php
// Modifying by: 

############# Change Log [Start] ################
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$ay = new academic_year($AcademicYearID);

### Title / Menu
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportFormClass']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
//$csvFile = "form-class-csv-sample.csv";
if($sys_custom['Class']['ClassGroupSettings'])
	$csvFileName = 'form_class_sample2.csv';
else
	$csvFileName = 'form_class_sample.csv';
$csvFile = "<a href='". GET_CSV($csvFileName) ."' class='tablelink'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

/*
$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
				
$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/detention_import_illus_$intranet_session_language.png\"/><br/><div class=\"note\">";
$format_str .= "<li><span id=\"GM__date\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ul></div></li><br/>";
$format_str .= "</ol></div>";
*/
//$format_str = "description here~~~~~~~~~~";



?>
<br />
<form name="form1" method="post" action="import_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?> </td>
					<td class="tabletext" ><?=$ay->Get_Academic_Year_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="tabletext" align="left" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr>
				
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
