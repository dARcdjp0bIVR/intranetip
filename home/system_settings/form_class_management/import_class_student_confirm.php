<?
// Modifying by: 

############# Change Log [Start] ################
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libFCM = new form_class_manage();
$ay = new academic_year($AcademicYearID);
$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_class_student.php?AcademicYearID=".$AcademicYearID."&xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$file_format = array("Student Login ID", "Class Name (EN)", "Class Number");
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_class_student.php?AcademicYearID=".$AcademicYearID."&xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportClassStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		UserLogin varchar(100),
		ClassNameEn varchar(100),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());

# delete the temp data in temp table 
$sql = "delete from TEMP_CLASS_STUDENT_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());



$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=1;


foreach($data as $key => $data)
{
	$i++;
	$error_msg = array();
	
	### store csv data to temp data
	list($UserLogin, $ClassNameEn, $ClassNumber) = $data;
	$UserLogin = trim($UserLogin);
	$ClassNameEn = trim($ClassNameEn);
	$ClassNumber = trim($ClassNumber);
	
	
	### check data
	# 1. Check empty data
	if(empty($UserLogin) || empty($ClassNameEn))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		
	# 2. Check if user login exists
	$sql = " SELECT UserID FROM INTRANET_USER WHERE UserLogin = '".$UserLogin."' ";
	$tmpResultSet = $li->returnVector($sql);
	$tmpUserID = $tmpResultSet[0];
	if ($tmpUserID == '')
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'];
		
	# 3. Check if class name valid
	$isClassNameValid = $libFCM->Is_ClassName_Valid($ClassNameEn);
	if ($isClassNameValid == false)
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'];
		
	# 4. Check if class exist
	$isClassExist = $libFCM->Is_ClassName_Exist($ClassNameEn, $AcademicYearID);
	if ($isClassExist == false)
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNotExistWarning'];
		
	# 5. Check if the class number is numeric
	if ($ClassNumber!='' && !is_numeric($ClassNumber))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberIntegerOnlyWarning'];
		
	# 6. Check if the class number is in use
	if ($libFCM->Is_ClassNumber_Used_By_ClassName($ClassNumber, $ClassNameEn, $AcademicYearID))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedWarning'];
		
	# 7. Check if the class number is in use within the csv
	$sql = "Select 
					TempID 
			From 
					TEMP_CLASS_STUDENT_IMPORT 
			Where 
					UserID = '".$_SESSION['UserID']."' 
					And 
					ClassNameEn = '".$li->Get_Safe_Sql_Query($ClassNameEn)."'
					And
					ClassNumber = '".$li->Get_Safe_Sql_Query($ClassNumber)."'
					And 
					UserID = '".$_SESSION["UserID"]."'
			";
	$resultSet = $li->returnArray($sql);
	if (count($resultSet) > 0)
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedinCSVWarning'];
	
	# 8. Check if the Class Number is within 3 digits
	if (strlen($ClassNumber) > 9)
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['ClassNumberMustBeWithinDigits'];
	
	$sql = "
			insert into TEMP_CLASS_STUDENT_IMPORT 
			(UserID, RowNumber, UserLogin, ClassNameEn, ClassNumber, DateInput)
			values
			(". $_SESSION["UserID"] .", $i, '".$li->Get_Safe_Sql_Query($UserLogin)."', 
				'".$li->Get_Safe_Sql_Query($ClassNameEn)."', '".$li->Get_Safe_Sql_Query($ClassNumber)."', now())
			";
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();

	if(empty($error_msg))
	{
		$successCount++;
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". $ay->Get_Academic_Year_Name() ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['StudentLoginID'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['ClassTitleEN'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_CLASS_STUDENT_IMPORT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k=>$d)
	{
		list($t_TempID, $t_UserID, $t_RowNumber, $t_UserLogin, $t_ClassNameEn, $t_ClassNumber) = $d;
		
		$t_UserLogin = $t_UserLogin ? $t_UserLogin : "<font color='red'>***</font>";
		$t_ClassNameEn = $t_ClassNameEn ? $t_ClassNameEn : "<font color='red'>***</font>";
		$t_ClassNumber = $t_ClassNumber ? $t_ClassNumber : "--";
		
		$thisErrorArr = $error_result[$t_TempID];
		
		if (in_array($Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'], $thisErrorArr))
			$t_UserLogin = "<font color='red'>".$t_UserLogin."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'], $thisErrorArr))
			$t_ClassNameEn = "<font color='red'>".$t_ClassNameEn."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNotExistWarning'], $thisErrorArr))
			$t_ClassNameEn = "<font color='red'>".$t_ClassNameEn."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNumberIntegerOnlyWarning'], $thisErrorArr))
			$t_ClassNumber = "<font color='red'>".$t_ClassNumber."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedWarning'], $thisErrorArr))
			$t_ClassNumber = "<font color='red'>".$t_ClassNumber."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedinCSVWarning'], $thisErrorArr))
			$t_ClassNumber = "<font color='red'>".$t_ClassNumber."</font>";
			
		if (in_array($Lang['SysMgr']['FormClassMapping']['ClassNumberMustBeWithinDigits'], $thisErrorArr))
			$t_ClassNumber = "<font color='red'>".$t_ClassNumber."</font>";
			
		if (is_array($error_result[$t_TempID]))
			$errorDisplay = implode('<br />', $error_result[$t_TempID]);
		else
			$errorDisplay = $error_result[$t_TempID];
		
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr style='vertical-align:top'>";
		$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_UserLogin ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ClassNameEn ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ClassNumber ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $errorDisplay ."</td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_class_student.php?AcademicYearID=".$AcademicYearID."'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_class_student.php?AcademicYearID=".$AcademicYearID."'");
}
?>

<br />
<form name="form1" method="post" action="import_class_student_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
