<?php
// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$PrevYearClassID = $_REQUEST['PrevYearClassID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$SelectedStudentList = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']:array();
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$FormClassManageUI = new form_class_manage_ui();
echo $FormClassManageUI->Get_Class_Student_Selection($YearClassID,$PrevYearClassID,$SelectedStudentList,$AcademicYearID);

intranet_closedb();
?>