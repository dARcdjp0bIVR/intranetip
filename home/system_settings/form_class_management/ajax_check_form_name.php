<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearName = trim(stripslashes(urldecode($_REQUEST['YearName'])));
$YearID = $_REQUEST['YearID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$fcm = new form_class_manage();

if ($fcm->Check_Year_Name($YearID,$YearName)) 
	echo "1"; // Year name is good to use
else
	echo "0"; // Year name duplicated / blanked

intranet_closedb();
?>