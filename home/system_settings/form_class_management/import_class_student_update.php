<?
@SET_TIME_LIMIT(21600);
// Modifying by: 
############# Change Log [Start] ################
#
# Date: 2010-09-16 (Ivan)
#		- Run update in iFrame and modified the logic for update class student info by import
#
# Date:	2010-07-12 Ivan
#		- Added Class History Logic 
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libimporttext.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_opendb();

$li = new libdb();
$fcm = new form_class_manage();
$Year = new Year();
$libclass = new libclass();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportClassStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 


### Only imported Student's old and new classes are required to be updated 
$sql = "Select YearClassID, OldYearClassID From TEMP_CLASS_STUDENT_IMPORT Where UserID = '".$_SESSION['UserID']."'";
$YearClassArr = $li->returnArray($sql);
$NewYearClassIDArr = Get_Array_By_Key($YearClassArr, 'YearClassID');
$OldYearClassIDArr = Get_Array_By_Key($YearClassArr, 'OldYearClassID');
$InvolvedYearClassIDArr = array_values(array_unique(array_merge((array)$NewYearClassIDArr, (array)$OldYearClassIDArr)));
$numOfInvolvedClass = count($InvolvedYearClassIDArr);
for ($i=0; $i<$numOfInvolvedClass; $i++) {
	$_yearClassId = $InvolvedYearClassIDArr[$i];
	
	if ($_yearClassId == 0) {
		unset($InvolvedYearClassIDArr[$i]);
	}
}
$InvolvedYearClassIDArr = array_values($InvolvedYearClassIDArr);
$numOfInvolvedClass = count($InvolvedYearClassIDArr);


	
### iFrame for real-time update count
$thisSrc = "ajax_update.php?Action=Import_Class_Student";
$ImportIFrame = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";


### Thickbox loading message					
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfInvolvedClass, $Lang['SysMgr']['FormClassMapping']['ClassProcessed']);
$ProcessingMsg .= '</span>';


?>

<script language="javascript">
$(document).ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});
</script>

<br />
<form id="form1" name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td align="center"><?=$x?></td>
	</tr>
			
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			
			<tr>
				<td class='tabletext' align='center'>
					<span id="ImportStatusSpan"></span>
				</td>
			</tr>
	
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'"); ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<?=$ImportIFrame?>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
