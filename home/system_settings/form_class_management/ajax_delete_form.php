<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearID = $_REQUEST['YearID'];

$fcm = new form_class_manage();

if ($fcm->Delete_Form($YearID)) {
	echo $Lang['SysMgr']['FormClassMapping']['FormDeleteSuccess'];
}
else {
	echo $Lang['SysMgr']['FormClassMapping']['FormDeleteUnsuccess'];
}

intranet_closedb();
die;
?>