<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$SearchKeyword = stripslashes(urldecode($_REQUEST['SearchKeyword']));

$YearClass = new year_class($YearClassID,true,true,true);

$FormClassUI = new form_class_manage_ui();

echo $FormClassUI->Get_Class_Info($YearClass,$SearchKeyword);

intranet_closedb();
?>