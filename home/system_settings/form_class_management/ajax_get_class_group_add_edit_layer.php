<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

$RecordType = stripslashes(urldecode($_REQUEST['RecordType']));
$FormClassUI = new form_class_manage_ui();

if ($RecordType == 'Layer')
	echo $FormClassUI->Get_Class_Group_Info_Layer();
else if ($RecordType == 'Table')
	echo $FormClassUI->Get_Class_Group_Info_Layer_Table();
	
intranet_closedb();

?>