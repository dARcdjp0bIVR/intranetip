<?php
// using 
/*
 * 2015-12-31 (Carlos): $sys_custom['KIS_ClassGroupAttendanceType'] - added AttendanceType field for Class Group.
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Add")
{
	$DataArr['Code'] = stripslashes(urldecode($_REQUEST['Code']));
	$DataArr['TitleEn'] = stripslashes(urldecode($_REQUEST['TitleEn']));
	$DataArr['TitleCh'] = stripslashes(urldecode($_REQUEST['TitleCh']));
	if($sys_custom['KIS_ClassGroupAttendanceType']){
		$DataArr['AttendanceType'] = $_REQUEST['AttendanceType'];
	}
	
	$libClassGroup = new Class_Group();
	# return inserted TimetableID
	$successArr['Insert'] = $libClassGroup->Insert_Record($DataArr);
	
	$success = $successArr['Insert'];
}
else if ($Action == "Edit")
{
	$DivID = stripslashes(urldecode($_REQUEST['DivID']));
	$DBFieldName = stripslashes(urldecode($_REQUEST['DBFieldName']));
	$UpdateValue = stripslashes(urldecode($_REQUEST['UpdateValue']));
	
	$DivIDArr = explode('_', $DivID);
	$SubjectClassID = $DivIDArr[1];
	
	$DataArr = array();
	$DataArr[$DBFieldName] = $UpdateValue;
	
	$objClassGroup = new Class_Group($SubjectClassID);
	$success = $objClassGroup->Update_Record($DataArr);
}
else if ($Action == "Delete")
{
	$ClassGroupID = stripslashes($_REQUEST['ClassGroupID']);
	
	$objClassGroup = new Class_Group($ClassGroupID);
	$success = $objClassGroup->Delete_Record();
}

echo $success;


intranet_closedb();
?>