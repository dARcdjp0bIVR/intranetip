<?
// Modifying by: Siuwan

############# Change Log [Start] ################
#
# Date: 2010-09-16 (Ivan)
#		- Run validation in iFrame and modified the validation logic for update class student info by import
#
# Date:	2010-07-08 (Henry Chow)
#		- add GET_IMPORT_TXT_WITH_REFERENCE(), Apply new standard of import
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearID = $_REQUEST['YearID'];

$libFCM = new form_class_manage();
$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_Year = new Year($YearID);
$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_class_student_step1.php?AcademicYearID=".$AcademicYearID."&xmsg=import_failed");
	exit();
}

### move move to temp folder for ajax validation
$folder_prefix = $intranet_root."/file/import_temp/form_class_student/".$_SESSION['UserID'];
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFilePath = stripslashes($folder_prefix."/".date('Ymd_His').$ext);
$success['move'] = $lo->lfs_move($csvfile, $TargetFilePath);


$file_format = array("Student Login ID", "Current Class Name (EN)", "Current Class Number", "Target Class Name (EN)", "Target Class Number");
$flagAry = array(1, 0, 1, 1, 1, 1);
$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $file_format, $flagAry);
$col_name = array_shift($data);
$numOfData = count($data);



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_class_student_step1.php?AcademicYearID=".$AcademicYearID."&xmsg=".$returnMsg);
	exit();
}



# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Class'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['ImportClassStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
/* moved to addon schema
$sql = "CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		UserLogin varchar(100),
		ClassNameEn varchar(100),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());
*/

# delete the temp data in temp table 
$sql = "delete from TEMP_CLASS_STUDENT_IMPORT where UserID = ".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());
$sql = "delete from TEMP_CLASS_STUDENT_RESULT_IMPORT where UserID = ".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());


$InfoTable = '';
$InfoTable .= '<table class="form_table_v30" style="width:90%;">'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['SchoolYear'].'</td>'."\n";
		$InfoTable .= '<td>'.$obj_AcademicYear->Get_Academic_Year_Name().'</td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
		$InfoTable .= '<td>'.$obj_Year->Get_Year_Name().'</td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$InfoTable .= '<td><div id="SuccessCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$InfoTable .= '<td><div id="FailCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
	$InfoTable .= '</tr>'."\n";
$InfoTable .= '</table>'."\n";


### Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfData, $Lang['SysMgr']['Timetable']['RecordsValidated']);
$ProcessingMsg .= '</span>';


### Buttons
$ImportBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit");
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_class_student_step1.php?AcademicYearID=".$AcademicYearID."&YearID=".$YearID."'");


### Iframe
$thisSrc = "ajax_validate.php?Action=Import_Form_Class_Student&TargetFilePath=".$TargetFilePath."&AcademicYearID=".$AcademicYearID."&YearID=".$YearID;
$ImportLessonIFrame = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";

?>

<br />
<form name="form1" method="post" action="import_class_student_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<?=$InfoTable?>
		<br style="clear:both;" />
		<div id="ErrorTableDiv"></div>
	</td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<span id="ImportButtonDiv" style="display:none;"><?=$ImportBtn?></span>
				&nbsp;
				<span><?=$BackBtn?></span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearID" value="<?=$YearID?>">
<?=$ImportLessonIFrame?>
</form>
<br />

<script language="javascript">
	$('document').ready(function () {
		Block_Document('<?=$ProcessingMsg?>');
	});
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
