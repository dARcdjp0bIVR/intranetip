<?php
// using : 

########### Change Log [Start] ######## 
#
#	Date	:	2010-09-22 [Henry Chow]
#				user in array $special_feature['CanEditPreviousYearInClass'] can import record to previous year  
#
############ Change Log [End] #########

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$AcademicYearID = $_REQUEST['AcademicYearID'];
$ObjAcademicYear = new academic_year($AcademicYearID);


if($_SESSION['SSV_USER_ACCESS']['SchoolSettings-Class'] || (isset($special_feature['CanEditPreviousYearInClass']) && in_array($UserID, $special_feature['CanEditPreviousYearInClass']))) 
	echo "0";
else if ($ObjAcademicYear->Has_Academic_Year_Past()) 
	echo "1"; // Academic Year Passed
else
	echo "0"; // Academic Year is current year or is in the future

intranet_closedb();
?>