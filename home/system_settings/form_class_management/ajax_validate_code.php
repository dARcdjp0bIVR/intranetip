<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclassgroup.php");

intranet_opendb();

# Get data
$InputValue = stripslashes($_REQUEST['InputValue']);
$ExcludeID = stripslashes($_REQUEST['ExcludeID']);

# Initialize library
$libClassGroup = new Class_Group();
$isCodeExisted = $libClassGroup->Is_Code_Existed($InputValue, $ExcludeID);

	
if ($isCodeExisted == 1)
	echo $Lang['SysMgr']['FormClassMapping']['Warning']['DuplicatedCode']['ClassGroup'];
else
	echo "";


intranet_closedb();

?>