<?php
// using : 

############# Change Log [Start] ################
#	Date:	2017-04-13	Omas
#			fixed after array merge some key in $WebSamsCode being changed
#
#	Date:	2015-12-31	Carlos
#			$sys_custom['KIS_ClassGroupAttendanceType'] - modified js_Insert_Class_Group() and js_Add_Class_Group_Row(), added AttendanceType field for KIS.
#
#	Date:	2015-02-10	Omas
#			add checking when submit new class avoid create dupicate classname & ajax changed to sync mode
#
#	Date:	2013-08-13	Carlos
#			hide WEBSAMS code for KIS - modified js Add_Form_Row() 
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2010-06-18 (Henry Chow)
#		- Grouped import functions into layer, and added export & copy options
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

if ($sys_custom['Class']['ClassGroupSettings'] == true)
	include_once($PATH_WRT_ROOT."includes/libclassgroup.php");
	
if ($sys_custom['Form']['StageSettings'] == true)
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	$libFormStage = new Form_Stage();
}

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$AcademicYearID = $_REQUEST['AcademicYearID'];

$linterface = new interface_html();

$CurrentPageArr['Class'] = 1;

### Title ###
$title = $Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'];
$TAGS_OBJ[] = array($title,"");

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Class";'."\n";
$js.= '</script>'."\n";

$MODULE_OBJ['title'] = $Lang['SysMgr']['FormClassMapping']['ClassPageTitle'].$js;

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','class');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

$FormClassUI = new form_class_manage_ui();
$Json = new JSON_obj();

// prepare websams selection list for jeditable
$WebSamsCode['NA'] = "NA";
// Omas array merge will reassign integer key...  
// $WebSamsCode = array_merge($WebSamsCode,$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']);
$WebSamsCode =  $WebSamsCode + $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList'];
$WebSamsCode['selected'] = "";
$WebSamsCode = $Json->encode($WebSamsCode);

if ($sys_custom['Form']['StageSettings'] == true)
{
	$FormStageInfoArr = $libFormStage->Get_All_Form_Stage($active=1);
	$FormStageAssoArr = BuildMultiKeyAssoc($FormStageInfoArr, 'FormStageID', array(Get_Lang_Selection('TitleCh', 'TitleEn')), $SingleValue=1);
	$FormStage['0'] = "NA";
	$FormStage = $FormStage + $FormStageAssoArr;
	$FormStage['selected'] = "";
	$FormStage = $Json->encode($FormStage);
	
	$jsFormStageSelection = $libFormStage->Get_Form_Stage_Selection('FormStageID');
	$jsFormStageSelection = str_replace('"', '\"', $jsFormStageSelection);
	$jsFormStageSelection = str_replace("\n", "", $jsFormStageSelection);
}



## Add/Edit Building Form Create & Reset Button ##
$btnAddClassGroup = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Class_Group();", "NewClassGroupSubmitBtn"));
$btnCancelClassGroup = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Class_Group_Info();", "NewClassGroupCancelBtn"));

## Add/Edit Building Form Create & Reset Button ##
$btnAddFormStage = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Form_Stage();", "NewFormStageSubmitBtn"));
$btnCancelFormStage = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Form_Stage_Info();", "NewFormStageCancelBtn"));


echo $FormClassUI->Get_Form_Class_Management_List($AcademicYearID);
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<div id="IndexDebugDiv"></div>

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
var jsShowHideSpeed = 'fast';
var jsFormStageData = '<?=$FormStage?>';
var jsFormStageSelection = "<?=$jsFormStageSelection?>";

// setting up language variable for js usage in included js function
var Lang = Array();
Lang['ClassTitleDuplicateWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleDuplicationWarning']?>';
Lang['ClassTitleEmptyWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleWarning']?>';
Lang['ClassTitleQuoteWarning'] = '<?=$Lang['SysMgr']['FormClassMapping']['ClassTitleQuoteWarning']?>';
Lang['Enable'] = '<?=$Lang['SysMgr']['FormClassMapping']['InUse']?>';
Lang['Disable'] = '<?=$Lang['SysMgr']['FormClassMapping']['NotInUse']?>';

// Ajax function 
{
function Show_Form_List() {
	
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_form_list.php';
  var postContent = '';
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
		  // scrip to bind jeditable to layer
			Init_JEdit_Select("div.jEditSelect_WebSAMSCode");
			Init_JEdit_Select("div.jEditSelect_FormStage");
			Init_JEdit_Input("div.jEditInput");
			Init_DND_Table();
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Refresh_Form_List() {
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_refresh_form_list.php';
  var postContent = '';
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('FormListTableLayer').innerHTML = wordXmlHttp.responseText;
		  // scrip to bind jeditable to layer
			Init_JEdit_Select("div.jEditSelect_WebSAMSCode");
			Init_JEdit_Select("div.jEditSelect_FormStage");
			Init_JEdit_Input("div.jEditInput");
			Init_DND_Table();
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Delete_Form(YearID) {
	if (confirm('<?=$Lang['SysMgr']['FormClassMapping']['DeleteFormWarning']?>')) {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_delete_form.php';
	  var postContent = 'YearID='+YearID;
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				Get_Return_Message(Trim(wordXmlHttp.responseText));
				Refresh_Form_List();
				Get_Form_Class_List();
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
}

function Create_Form() {
	var YearName = document.getElementById('YearName').value;
	var WEBSAMSCode = document.getElementById('WEBSAMSCode').options[document.getElementById('WEBSAMSCode').selectedIndex].value;
	
	if (Trim(YearName) != "" && $('#NewFormWarningLayer').html() == "") {
		document.getElementById('FormListSubmitBtn').disabled = true;
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_new_form.php';
	  var postContent = 'YearName='+encodeURIComponent(YearName);
	  postContent += '&WEBSAMSCode='+encodeURIComponent(WEBSAMSCode);
	  	wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				Get_Return_Message(Trim(wordXmlHttp.responseText));
				Refresh_Form_List();
				Get_Form_Class_List();
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		$('#NewFormWarningLayer').html('<?=$Lang['SysMgr']['FormClassMapping']['FormNameWarning']?>');
		$('#NewFormWarningLayer').show('fast');
	}
}

function Get_Form_Class_List() {

	var AcademicYearID = document.getElementById('SchoolYear').options[document.getElementById('SchoolYear').selectedIndex].value;
	//var SearchKeyword = document.getElementById('SearchKeyword').value;
	
	FormClassListAjax = GetXmlHttpObject();
		   
  if (FormClassListAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_form_class_table.php';
  var postContent = 'AcademicYearID='+AcademicYearID;
  //postContent += '&SearchKeyword='+encodeURIComponent(SearchKeyword);
	FormClassListAjax.onreadystatechange = function() {
		if (FormClassListAjax.readyState == 4) {
			document.getElementById('DetailLayer').innerHTML = FormClassListAjax.responseText;
			Init_DND_Table();
			Thick_Box_Init();
		}
	};
  FormClassListAjax.open("POST", url, true);
	FormClassListAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	FormClassListAjax.send(postContent);
	
	
	// Hide import button if the selected year has past already
	$.post(
		"ajax_check_academic_year_past.php", 
		{ 
			AcademicYearID: AcademicYearID
		},
		function(ReturnData)
		{
			if (ReturnData == "0") {
				$('#import_button_div').show();
			}
			else
				$('#import_button_div').hide();
		}
	);
}

function Create_Class() {
	Select_All_Options('StudentSelected[]',true);
	Select_All_Options('SelectedClassTeacher[]',true);
	
	var ClassTitleEN = document.getElementById('ClassTitleEN').value;
	var ClassTitleB5 = document.getElementById('ClassTitleB5').value;
	
	var AcademicYearID = document.getElementById('SchoolYear').value;
	
	// checking functions
	Check_Name_Duplicate('EN',ClassTitleEN,'',AcademicYearID);
	Check_Name_Duplicate('B5',ClassTitleB5,'',AcademicYearID);

	Check_Class_Title_Empty(ClassTitleEN);
	Check_Name_Quote();
	
	var ClassTitleENWarning = document.getElementById("ClassTitleENWarningLayer").innerHTML;
	var ClassTitleB5Warning = document.getElementById("ClassTitleB5WarningLayer").innerHTML;
	
	if (ClassTitleENWarning == "" && ClassTitleB5Warning == "") {
		Block_Thickbox();
		document.getElementById("ClassTitleENWarningLayer").innerHTML = '';
		document.getElementById("ClassTitleENWarningRow").style.display = "none";
		
		document.getElementById('ClassSubmit').disabled = true; // disable submit button
		var PostString = Get_Form_Values(document.getElementById("ClassForm"));
		
		CreateClassAjax = GetXmlHttpObject();
		   
	  if (CreateClassAjax == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_create_class.php';
	  var PoseValue = PostString;
		CreateClassAjax.onreadystatechange = function() {
			if (CreateClassAjax.readyState == 4) {
			 //alert(CreateClassAjax.responseText);
				Get_Return_Message(Trim(CreateClassAjax.responseText));
				
				Get_Form_Class_List();
				UnBlock_Thickbox();
				window.top.tb_remove();
				
				//$('#IndexDebugDiv').html(CreateClassAjax.responseText);
			}
		};
	  CreateClassAjax.open("POST", url, true);
		CreateClassAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		CreateClassAjax.send(PoseValue);
	}
}

function Check_Form_Name(YearName,YearID,ReturnLayer) {
	CheckFormNameAjax = GetXmlHttpObject();
	   
  if (CheckFormNameAjax == null)
  {
    alert (errAjax);
    return;
  } 
    
  var ElementObj = $(ReturnLayer);
  var url = 'ajax_check_form_name.php';
	var postContent = "YearID="+YearID;
	postContent += "&YearName="+YearName;
	CheckFormNameAjax.onreadystatechange = function() {
		if (CheckFormNameAjax.readyState == 4) {
			var ResponseText = CheckFormNameAjax.responseText;
			if (ResponseText == "1") {
				ElementObj.html('');
				ElementObj.hide();
			}
			else {
				ElementObj.html('<?=$Lang['SysMgr']['FormClassMapping']['FormNameWarning']?>');
				ElementObj.show('fast');
			}
		}
	};
  CheckFormNameAjax.open("POST", url, true);
	CheckFormNameAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	CheckFormNameAjax.send(postContent);
}
}
// end of ajax


// dimed on 20090423, as featuer considered useless
/*
function Show_Name_Of_Class() {
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_get_name_of_class_form.php';
  var postContent = '';
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
		  document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Add_Name_Of_Class_Row() {
	if (!document.getElementById('GenAddNameOfClassRow')) {
		var TableBody = document.getElementById("NameOfClassTable").tBodies[0];
		var RowIndex = document.getElementById("AddNameOfClassRow").rowIndex;
	  var NewRow = TableBody.insertRow(RowIndex);
	  
	  NewRow.id = "GenAddNameOfClassRow";
	
	  var NewCell0 = NewRow.insertCell(0);
		NewCell0.innerHTML = '&nbsp;';
		
		var NewCell1 = NewRow.insertCell(1);
		var temp = '<input name="NewNameOfClassEng" type="text" id="NewNameOfClassEng" value="" class="textbox"/>';
		NewCell1.innerHTML = temp;
	
		var NewCell2 = NewRow.insertCell(2);
		var temp = '<input name="NewNameOfClassChi" type="text" id="NewNameOfClassChi" value="" class="textbox"/>';
		NewCell2.innerHTML = temp;
		
		var NewCell3 = NewRow.insertCell(3);
		var temp = '<input id="NewNameOfClassSubmitBtn" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="<?=$Lang['Btn']['Submit']?>" onclick="Create_New_Name_Of_Class();"/>';
		NewCell3.innerHTML = temp;
	}
}

function Create_New_Name_Of_Class() {
	var EngName = document.getElementById('NewNameOfClassEng').value;
	var ChiName = document.getElementById('NewNameOfClassChi').value;
	
	if (Trim(EngName) != "" || Trim(ChiName) != "") {
		document.getElementById('NewNameOfClassSubmitBtn').disabled = true;
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_new_name_of_class.php';
	  var postContent = 'NameOfClassEng='+encodeURIComponent(EngName);
	  postContent += '&NameOfClassChi='+encodeURIComponent(ChiName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				Show_Name_Of_Class();
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
}

function Delete_Name_Of_Class(ClassID) {
	wordXmlHttp = GetXmlHttpObject();
   
  if (wordXmlHttp == null)
  {
    alert (errAjax);
    return;
  } 
    
  var url = 'ajax_delete_name_of_class.php';
  var postContent = 'ClassID='+ClassID;
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			Show_Name_Of_Class();
		}
	};
  wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function Rename_Field(DistingishID,ElementName) {
	var RenameLayer = document.getElementById('Rename'+ElementName+'Div'+DistingishID);
	var OrgName = document.getElementById('Default'+ElementName+DistingishID).value;
	var OrgNameLayer = document.getElementById(ElementName+'Div'+DistingishID);
	var NewNameElement = document.getElementById('New'+ElementName+DistingishID);
	
	OrgNameLayer.style.visibility = 'hidden';
	OrgNameLayer.style.display = 'none';
	
	NewNameElement.value = OrgName;
	RenameLayer.style.visibility = 'visible';
	RenameLayer.style.display = 'inline';
	NewNameElement.focus();
	NewNameElement.select();
}

function Go_Rename_Field(DistingishID,ElementName) {
	//if (window.event) keycode = window.event.keyCode;
	//else if (e) keycode = e.which;
	
	var NewName = document.getElementById('New'+ElementName+DistingishID).value;
	if (Trim(NewName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_rename_name_of_class.php';
		var postContent = "ClassID="+DistingishID;
		postContent += "&"+ElementName+"="+encodeURIComponent(NewName);
		wordXmlHttp.onreadystatechange = function() {
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		wordXmlHttp.send(postContent);
		
		document.getElementById(ElementName+'Div'+DistingishID).innerHTML = NewName;
		document.getElementById('Default'+ElementName+DistingishID).value = NewName;
	}
	
	Cancel_Rename_Folder(DistingishID,ElementName);
}

function Cancel_Rename_Folder(DistingishID,ElementName) {
	var RenameLayer = document.getElementById('Rename'+ElementName+'Div'+DistingishID);
	var OrgName = document.getElementById('Default'+ElementName+DistingishID).value;
	var OrgNameLayer = document.getElementById(ElementName+'Div'+DistingishID);
	var NewNameElement = document.getElementById('New'+ElementName+DistingishID);
	
	OrgNameLayer.style.visibility = 'visible';
	OrgNameLayer.style.display = 'block';
	
	RenameLayer.style.visibility = 'hidden';
	RenameLayer.style.display = 'none';
}*/


function js_Update_Form_Info(jsYearID)
{
	var jsLinkID = 'ActiveStatus_' + jsYearID;
	var TargetRecordStatus;
	if ($('a#' + jsLinkID).attr('class') == 'active_item')
	{
		$('a#' + jsLinkID).attr('class', 'inactive_item');
		TargetRecordStatus = 0;
	}
	else
	{
		$('a#' + jsLinkID).attr('class', 'active_item');
		TargetRecordStatus = 1;
	}
	
	$.post(
		"ajax_update_form.php", 
		{ 
			YearID: jsYearID,
			RecordStatus: TargetRecordStatus
		},
		function(ReturnData)
		{
			Get_Return_Message(Trim(ReturnData));
			Get_Form_Class_List();
		}
	);
}



// jeditable functions
{
function Refresh_JEdit_Select(objDom) {
	$(objDom).editable("destroy",{});
	Init_JEdit_Select(objDom);
}

function Init_JEdit_Select(objDom){
	
	var jsData;
	var jsDBField;
	if (objDom == 'div.jEditSelect_WebSAMSCode')
	{
		jsData = '<?=$WebSamsCode?>';
		jsDBField = 'WEBSAMSCode';
	}
	if (objDom == 'div.jEditSelect_FormStage')
	{
		jsData = jsFormStageData;
		jsDBField = 'FormStageID';
	}
	
	$(objDom).each(function() {
		$(this).editable( 
	    function(value, settings) {
//	    	SelectEditAjaxObj = GetXmlHttpObject();
//		   
//			  if (SelectEditAjaxObj == null)
//			  {
//			    alert (errAjax);
//			    return;
//			  } 
			  
			  var ElementObj = $(this);
			  var jsID = $(this).attr("id");
			  var jsTmpArr = jsID.split('_');
			  var jsYearID = jsTmpArr[0];
			  
			  	var jsThisWebSAMSCode, jsThisRecordStatus;
				if (objDom == 'div.jEditSelect_WebSAMSCode')
				{
					jsThisWebSAMSCode = encodeURIComponent(value);
					jsThisFormStageID = '';
				}
				else if (objDom == 'div.jEditSelect_FormStage')
				{
					jsThisWebSAMSCode = '';
					jsThisFormStageID = encodeURIComponent(value);
				}
				
				$.post(
					"ajax_update_form.php", 
					{ 
						YearID: jsYearID,
						WEBSAMSCode: jsThisWebSAMSCode,
						FormStageID: jsThisFormStageID
					},
					function(ReturnData)
					{
						if (objDom == 'div.jEditSelect_WebSAMSCode') {
							ElementObj.html(value);
						}
						else if (objDom == 'div.jEditSelect_FormStage') {
							ElementObj.html($('div#' + jsID + ' form > select > option:selected').text());
						}	
							
						Refresh_JEdit_Select(objDom);
						Get_Return_Message(Trim(ReturnData));
						Get_Form_Class_List();
					}
				);
			  
			  
//			  var url = 'ajax_update_form.php';
//				var postContent = "YearID="+jsYearID;
//				postContent += "&WEBSAMSCode="+encodeURIComponent(value);
//				SelectEditAjaxObj.onreadystatechange = function() {
//					if (SelectEditAjaxObj.readyState == 4) {
//						ElementObj.html(value);
//						Refresh_JEdit_Select(objDom);
//						Get_Return_Message(Trim(SelectEditAjaxObj.responseText));
//						Get_Form_Class_List();
//					}
//				};
//			  	SelectEditAjaxObj.open("POST", url, true);
//				SelectEditAjaxObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//				SelectEditAjaxObj.send(postContent);

			}, {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "select",     
	    style  : "display: inline",
	    data   : jsData.substring(0,jsData.lastIndexOf(":")+1) + '"' + $(this).html() + '"}',
	    height: "20px"
	  }); 
	});
	
//	$(objDom).click( function() {
//		
//		var ElementObj = $(this);
//		var jsID = $(this).attr("id");
//		var jsTmpArr = jsID.split('_');
//		var jsYearID = jsTmpArr[0];
//		
//		if (jsCannotInactivate == 1)
//		{
//			var jsValue = $(this).find('form select').val();
//			$(this).html(Lang[jsValue]);	
//		}
//
//		
//	});
}

function Init_JEdit_Input(objDom) {
	$(objDom).editable( 
    function(value, settings) {
    	var WarningLayer = "#"+$(this).attr("id")+"WarningLayer";
    	var ElementObj = $(this);
    	
    	if (ElementObj[0].revert != value) {
	    	if ($(WarningLayer).html() == "") {
		    	eval("var wordXmlHttp"+$(this).attr("id")+" = GetXmlHttpObject();");
			   
				  if (wordXmlHttp == null)
				  {
				    alert (errAjax);
				    return;
				  } 
				     
				  var url = 'ajax_update_form.php';
					var postContent = "YearID="+$(this).attr("id");
					postContent += "&YearName="+encodeURIComponent(value);
					eval("wordXmlHttp"+$(this).attr("id")).onreadystatechange = function() {
						if (eval("wordXmlHttp"+ElementObj.attr("id")).readyState == 4) {
							ElementObj.html(value);
							Get_Return_Message(Trim(eval("wordXmlHttp"+ElementObj.attr("id")).responseText));
							Get_Form_Class_List();
						}
					};
				  eval("wordXmlHttp"+$(this).attr("id")).open("POST", url, true);
					eval("wordXmlHttp"+$(this).attr("id")).setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					eval("wordXmlHttp"+$(this).attr("id")).send(postContent);
				}
				else {
					
					ElementObj[0].reset();
					$(WarningLayer).html('');
					$(WarningLayer).hide();
				}
			}
			else {
				ElementObj.html(value);
			}
		}, {
    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
    event : "click",
    onblur : "submit",
    type : "text",     
    style  : "display: inline",
    height: "20px",
    maxlength: 50
  }); 
  
  $(objDom).keyup(function() {
		var YearName = $('form input').val();
		var YearID = $(this).attr('id');
		var WarningLayer = "#"+YearID+"WarningLayer";
		
		Check_Form_Name(YearName,YearID,WarningLayer)
	});
}
}

// jquery tablednd function
{
function Init_DND_Table() {
	var TableID = "FormListTable";
	var objDom = "table.ClassDragAndDrop"
	
	var JQueryObj1 = $("#"+TableID);
	var JQueryObj2 = $(objDom);
	
	// form drag and drop
	JQueryObj1.tableDnD({
	    onDrop: function(table, row) {
        var rows = table.tBodies[0].rows;
        var RecordOrder = "";
        for (var i=0; i<rows.length; i++) {
        	if (rows[i].id != "")
        		RecordOrder += rows[i].id+",";
        }
            
	      ReorderAjax = GetXmlHttpObject();
		   
			  if (ReorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			    
			  var ElementObj = $(this);
			  var url = "ajax_reorder_form_sequence.php";
				var postContent = "RecordOrder="+encodeURIComponent(RecordOrder);
				
				ReorderAjax.onreadystatechange = function() {
					if (ReorderAjax.readyState == 4) {
						Get_Form_Class_List();
					}
				};
			  ReorderAjax.open("POST", url, true);
				ReorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				ReorderAjax.send(postContent);
	    },
		onDragStart: function(table, row) {
			//$(#debugArea).html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	
	// class drag and drop
	JQueryObj2.tableDnD({
	    onDrop: function(table, row) {
        //var rows = table.tBodies[0].rows;
        var rows = row.parentNode.rows;
        var RecordOrder = "";
        for (var i=0; i<rows.length; i++) {
        	if (rows[i].id != "")
        		RecordOrder += rows[i].id+",";
        }
            
	      ReorderAjax = GetXmlHttpObject();
		   
			  if (ReorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			    
			  var ElementObj = $(this);
			  var url = 'ajax_reorder_class_sequence.php';
				var postContent = "RecordOrder="+encodeURIComponent(RecordOrder);
				
				ReorderAjax.onreadystatechange = function() {
					if (ReorderAjax.readyState == 4) {
					}
				};
			  ReorderAjax.open("POST", url, true);
				ReorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				ReorderAjax.send(postContent);
	    },
		onDragStart: function(table, row) {
			//$(#debugArea).html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable",
		onDragClass: "move_selected"
	});
	
	/*** Class Group ***/
	var JQueryObj3 = $("#ClassGroupInfoTable");
	JQueryObj3.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_reorder_display_order.php", 
				{ 
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Reload_Class_Group_Add_Edit_Table();
					
					
					//$('#debugArea').html(ReturnData);
				}
			);
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	
	/*** Form Stage ***/
	var JQueryObj4 = $("#FormStageInfoTable");
	JQueryObj4.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_update.php", 
				{ 
					Action: "Reorder_Form_Stage",
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					// Reload the building info in the thickbox
					js_Reload_Form_Stage_Settings_Table();
					
					
					//$('#debugArea').html(ReturnData);
				}
			);
		},
		onDragStart: function(table, row) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
}

// UI functions
{
function Add_Form_Row() {
	if (!document.getElementById('GenAddFormRow')) {
		var TableBody = document.getElementById("FormListTable").tBodies[0];
		var RowIndex = document.getElementById("AddFormRow").rowIndex-1;
	  var NewRow = TableBody.insertRow(RowIndex);
	  
	  NewRow.id = "GenAddFormRow";
	
		var jsCellCounter = 0;
		//var NewCell0 = NewRow.insertCell(0);
		eval("var NewCell" + jsCellCounter + " = NewRow.insertCell(" + jsCellCounter + ");");
		
		var temp = '<input name="YearName" type="text" id="YearName" value="" class="textbox" onkeyup="Check_Form_Name(this.value,\'\',\'#NewFormWarningLayer\');"/>';
		temp += '<div id="NewFormWarningLayer" style="display: none; color:red;"></div>';
		
		//NewCell0.innerHTML = temp;
		eval("NewCell" + jsCellCounter + ".innerHTML = temp;");
		jsCellCounter++;
	
		//var NewCell1 = NewRow.insertCell(1);
		eval("var NewCell" + jsCellCounter + " = NewRow.insertCell(" + jsCellCounter + ");");
<?
		$WEBSAMSCode .= '<select name="WEBSAMSCode" id="WEBSAMSCode">';
		$WEBSAMSCode .= '<option value="NA">NA</option>';
		foreach ($Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList'] as $key=>$var) {
			$WEBSAMSCode .= '<option value="'.$key.'">'.$var.'</option>';
		}
		$WEBSAMSCode .= '</select>';
?>
		var temp = '<?=$WEBSAMSCode?>';
		//NewCell1.innerHTML = temp;
		eval("NewCell" + jsCellCounter + ".innerHTML = temp;");
<?php if($isKIS) { ?>
		eval("NewCell" + jsCellCounter + ".style.display = 'none';");
<?php } ?>
		jsCellCounter++;
		
		
		<? if ($sys_custom['Form']['StageSettings'] == true) { ?>
			eval("var NewCell" + jsCellCounter + " = NewRow.insertCell(" + jsCellCounter + ");");
				var temp = jsFormStageSelection.replace('\\"', '"');
			eval("NewCell" + jsCellCounter + ".innerHTML = temp;");
			jsCellCounter++;
		<? } ?>
		
		
		//var NewCell2 = NewRow.insertCell(2);
		eval("var NewCell" + jsCellCounter + " = NewRow.insertCell(" + jsCellCounter + ");");
		var temp = '<input id="FormListSubmitBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Done']?>" onclick="Create_Form();"/> ';
		temp += '<input id="FormListSubmitBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Cancel']?>" onclick="Clear_Create_Form();"/>';
		//NewCell2.innerHTML = temp;
		eval("NewCell" + jsCellCounter + ".innerHTML = temp;");
		jsCellCounter++;
	}
}

function Clear_Create_Form() {
	var TableBody = document.getElementById("FormListTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddFormRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}
}

// import functions
{
function importFormClass()
{
	var AcademicYearID = $('#SchoolYear').val();
	//var AcademicYearID = document.getElementById('SchoolYear').options[document.getElementById('SchoolYear').selectedIndex].value;
	window.location="import_step1.php?AcademicYearID="+AcademicYearID;
}

function importClassStudent()
{
	var AcademicYearID = $('#SchoolYear').val();
	//var AcademicYearID = document.getElementById('SchoolYear').options[document.getElementById('SchoolYear').selectedIndex].value;
	window.location="import_class_student_step1.php?AcademicYearID="+AcademicYearID;
}

function importClassTeacher()
{
	var AcademicYearID = $('#SchoolYear').val();
	//var AcademicYearID = document.getElementById('SchoolYear').options[document.getElementById('SchoolYear').selectedIndex].value;
	window.location="import_class_teacher_step1.php?AcademicYearID="+AcademicYearID;
}

function exportClassTeacher()
{
	var AcademicYearID = $('#SchoolYear').val();
	window.location="export_class_teacher.php?AcademicYearID="+AcademicYearID;
}

function exportClassStudent()
{
	var AcademicYearID = $('#SchoolYear').val();
	window.location="export_class_student.php?AcademicYearID="+AcademicYearID;
}

function copyFromOtherYear() {
	var AcademicYearID = $('#SchoolYear').val();
	window.location="copyFromOtherYear.php?AcademicYearID="+AcademicYearID;
}

var imgArrowAry = new Array();
imgArrowAry[0] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_open.gif";
imgArrowAry[1] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_open_on.gif";
imgArrowAry[2] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_close.gif";
imgArrowAry[3] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_close_on.gif";

function swapImage(imgFlag, imgFile) {
	if(document.getElementById(imgFlag).value%2==0) {
		//alert(imgArrowAry[parseInt(document.getElementById(imgFlag).value)+1]);
		document.getElementById(imgFile).src = imgArrowAry[parseInt(document.getElementById(imgFlag).value)+1];
		document.getElementById(imgFlag).value = parseInt(document.getElementById(imgFlag).value)+1;
	} else {
		document.getElementById(imgFile).src = imgArrowAry[parseInt(document.getElementById(imgFlag).value)-1];
		document.getElementById(imgFlag).value = parseInt(document.getElementById(imgFlag).value)-1;
	}
}


function closeAllSpan() {
	MM_showHideLayers('import_option','','hide');		
	MM_showHideLayers('export_option','','hide');	
	document.getElementById('importArrow').src = imgArrowAry[0];
	document.getElementById('exportArrow').src = imgArrowAry[0];
	document.getElementById('importImgID').value = 0;
	document.getElementById('exportImgID').value = 0;
	
}

function checkArrowLayer(imgFlag,imgName,spanName) {
	if(document.getElementById(spanName).style.visibility=="hidden") {
		closeAllSpan();
		document.getElementById(spanName).style.visibility = "visible";
		document.getElementById(imgFlag).value = 2;
		swapImage(imgFlag, imgName);
	} else {
		closeAllSpan();
		document.getElementById(spanName).style.visibility = "hidden";
		document.getElementById(imgFlag).value = 0;
		swapImage(imgFlag, imgName);
	}
}

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
}


// Class Group function start
function js_Show_Class_Group_Settings_Layer()
{
	$.post(
		"ajax_get_class_group_add_edit_layer.php", 
		{ RecordType: 'Layer' },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			
			Init_DND_Table();
			js_Init_JEdit_Class_Group();
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Reload_Class_Group_Add_Edit_Table() {
	$.post(
		"ajax_get_class_group_add_edit_layer.php", 
		{ RecordType: 'Table' },
		function(ReturnData)
		{
			$('#ClassGroupInfoSettingLayer').html(ReturnData);
			js_Init_JEdit_Class_Group();
			Init_DND_Table();
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Add_Class_Group_Row() {
	if (!document.getElementById('GenAddClassGroupRow')) {
		var TableBody = document.getElementById("ClassGroupInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddClassGroupRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddClassGroupRow";
		
		var cell_index = 0;
		
		// Code
		var NewCell0 = NewRow.insertCell(cell_index);
		var tempInput = '<input type="text" name="ClassGroupCode" id="ClassGroupCode" value="" class="textbox" maxlength="<?=$FormClassUI->Code_Maxlength?>"/>';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		cell_index += 1;
		
		// Title (Eng)
		var NewCell1 = NewRow.insertCell(cell_index);
		var tempInput = '<input type="text" name="ClassGroupTitleEn" id="ClassGroupTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="TitleEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		cell_index += 1;
		
		// Title (Chi)
		var NewCell2 = NewRow.insertCell(cell_index);
		var tempInput = '<input type="text" name="ClassGroupTitleCh" id="ClassGroupTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="TitleChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		cell_index += 1;
		
	<?php if($sys_custom['KIS_ClassGroupAttendanceType']){ ?>	
		var NewCellAttendanceType = NewRow.insertCell(cell_index);
		var attendTypeInput = '<select name="ClassGroupAttendanceType" id="ClassGroupAttendanceType">';
		attendTypeInput += '<option value=""><?=$Lang['General']['NA']?></option>';
		<?php foreach($sys_custom['KIS_ClassGroupAttendanceType_Options'] as $key => $val){ ?>
			attendTypeInput += '<option value="<?=$key?>"><?=$val?></option>';
		<?php } ?>
		attendTypeInput += '</select>';
		NewCellAttendanceType.innerHTML = attendTypeInput;
		cell_index += 1;
	<?php } ?>	
	
		// Add and Reset Buttons
		var NewCell3 = NewRow.insertCell(cell_index);
		var temp = '<?=$btnAddClassGroup?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelClassGroup?>';
		NewCell3.innerHTML = temp;
		cell_index += 1;
		
		$('#ClassGroupCode').focus();
		
		// Code validation
		js_Add_KeyUp_Code_Checking("ClassGroupCode", "CodeWarningDiv_New", "");
		js_Add_KeyUp_Blank_Checking('ClassGroupTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
		js_Add_KeyUp_Blank_Checking('ClassGroupTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
	}
}

function js_Clear_Temp_Class_Group_Info() {
	// Delete the Row
	var TableBody = document.getElementById("ClassGroupInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddClassGroupRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Insert_Class_Group() {
	var Code = encodeURIComponent(Trim($('#ClassGroupCode').val()));
	var TitleEn = encodeURIComponent(Trim($('#ClassGroupTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#ClassGroupTitleCh').val()));
	
	if (js_Is_Input_Blank('ClassGroupCode', 'CodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>'))
	{
		$('#ClassGroupCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('ClassGroupTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>'))
	{
		$('#ClassGroupTitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('ClassGroupTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>'))
	{
		$('#ClassGroupTitleCh').focus();
		return false;
	}
	
	$.post(
		"ajax_validate_code.php", 
		{
			InputValue: Trim($('#ClassGroupCode').val())
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code
				$('#CodeWarningDiv_New').hide(jsShowHideSpeed);
				
				// Input data valid => disable submit button and then add building
				$('#NewClassGroupSubmitBtn').attr("disabled", "true");
				$('#NewClassGroupCancelBtn').attr("disabled", "true");
				
				$.post(
					"ajax_update_class_group.php", 
					{ 
						Action: 'Add',
						Code: Code,
						TitleEn: TitleEn,
						TitleCh: TitleCh <?=$sys_custom['KIS_ClassGroupAttendanceType']? ',AttendanceType: $("#ClassGroupAttendanceType").val()' :'' ?>
					},
					function(ReturnData)
					{
						js_Reload_Class_Group_Add_Edit_Table();
							
						// Show system messages
						js_Show_System_Message("ClassGroup", "add", ReturnData);
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html(ReturnData);
				$('#CodeWarningDiv_New').show(jsShowHideSpeed);
				
				$('#BuildingCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Update_Class_Group(DivID, DBFieldName, UpdateValue)
{
	$.post(
		"ajax_update_class_group.php", 
		{ 
			Action: "Edit",
			DivID: encodeURIComponent(DivID),
			DBFieldName: encodeURIComponent(DBFieldName),
			UpdateValue: encodeURIComponent(UpdateValue)
		},
		function(ReturnData)
		{
			js_Reload_Class_Group_Add_Edit_Table();
			
			// Show system messages
			js_Show_System_Message("ClassGroup", "update", ReturnData);
				
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Delete_Class_Group(ClassGroupID)
{
	if (confirm('<?=$Lang['SysMgr']['FormClassMapping']['jsWarning']['ConfirmDeleteClassGroup']?>'))
	{
		$.post(
			"ajax_update_class_group.php", 
			{ 
				Action: "Delete",
				ClassGroupID: ClassGroupID
			},
			function(ReturnData)
			{
				js_Reload_Class_Group_Add_Edit_Table();
			
				// Show system messages
				js_Show_System_Message("ClassGroup", "delete", ReturnData);
			
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	return false;
}

function js_Show_System_Message(RecordType, Action, Status)
{
	var returnMessage = '';
	
	if (RecordType == "ClassGroup")
	{
		switch(Action)
		{
			case "add":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Success']['ClassGroup']?>' : '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Failed']['ClassGroup']?>';
				break;
			case "update":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Success']['ClassGroup']?>' : '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Failed']['ClassGroup']?>';
				break;
			case "delete":
				returnMessage = (Status)? '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Success']['ClassGroup']?>' : '<?=$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Failed']['ClassGroup']?>';
				break;
		}
	}
	Get_Return_Message(returnMessage);
}

function js_Init_JEdit_Class_Group()
{
	// Code
	$('div.jEditCode').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate_code.php", 
					{
						InputValue: updateValue,
						ExcludeID: targetID
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code
							$('#CodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
							
							// Update value
							if ($('#CodeWarningDiv_' + targetID).is(':visible'))
							{
								// not vaild
								$('#CodeWarningDiv_' + targetID).hide(jsShowHideSpeed);
								$(this)[0].reset();
							}
							else
							{
								// valid
								js_Update_Class_Group(thisDivID, "Code", updateValue);
							}
						}
						else
						{
							// invalid code => show warning
							$('#CodeWarningDiv_'+targetID).html(ReturnData);
							$('#CodeWarningDiv_'+targetID).show(jsShowHideSpeed);
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$FormClassUI->Code_Maxlength?>",
			height: "20px"
		}
	); 
	// Code Validation
	$('div.jEditCode').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
			
		js_Code_Validation(inputValue, targetID, 'CodeWarningDiv_'+targetID);
	});
	$('div.jEditCode').click( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		js_Code_Validation(inputValue, targetID, 'CodeWarningDiv_'+targetID);
	});
	
	// TitleEn
	$('div.jEditTitleEn').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleEnWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Class_Group(thisDivID, "TitleEn", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleEn').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleEnWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	
	// TitleCh
	$('div.jEditTitleCh').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleChWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Class_Group(thisDivID, "TitleCh", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleCh').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleChWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	/************ End of Add/Edit Building Info ************/
}

function js_Code_Validation(InputValue, TargetID, WarningDivID, ObjectType)
{
	ObjectType = ObjectType || 'ClassGroup';
	
	if (Trim(InputValue) == '')
	{
		$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>');
		$('#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$('#' + WarningDivID).hide(jsShowHideSpeed);
		
		if (ObjectType == 'ClassGroup')
		{
			$.post(
				"ajax_validate_code.php", 
				{
					InputValue: InputValue,
					ExcludeID: TargetID
				},
				function(ReturnData)
				{
					if (ReturnData == "")
					{
						// valid code
						$('#' + WarningDivID).hide(jsShowHideSpeed);
					}
					else
					{
						// invalid code => show warning
						$('#' + WarningDivID).html(ReturnData);
						$('#' + WarningDivID).show(jsShowHideSpeed);
						
						//$('#debugArea').html('aaa ' + ReturnData);
					}
				}
			);
		}
		else if (ObjectType == 'FormStage')
		{
			$.post(
				"ajax_validate.php", 
				{
					Action: 'Form_Stage_Code',
					InputValue: InputValue,
					ExcludeID: TargetID
				},
				function(ReturnData)
				{
					if (ReturnData == "")
					{
						// valid code
						$('#' + WarningDivID).hide(jsShowHideSpeed);
					}
					else
					{
						// invalid code => show warning
						$('#' + WarningDivID).html(ReturnData);
						$('#' + WarningDivID).show(jsShowHideSpeed);
						
						//$('#debugArea').html('aaa ' + ReturnData);
					}
				}
			);
		}
		
	}
}
function js_Add_KeyUp_Code_Checking(ListenerObjectID, WarningDivID, ObjectID, ObjectType)
{
	ObjectType = ObjectType || 'ClassGroup';
	
	// Code Validation
	$('#' + ListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var targetID = idArr[1];
		
		if (targetID == null)
			targetID = '';
			
		if (!isNaN(ObjectID))
			targetID = ObjectID;
			
		js_Code_Validation(inputValue, targetID, WarningDivID, ObjectType);
	});
}

function js_Change_Active_Status(jsLinkID, jsType)
{
	var TargetRecordStatus;
	if ($('a#' + jsLinkID).attr('class') == 'active_item')
	{
		$('a#' + jsLinkID).attr('class', 'inactive_item');
		TargetRecordStatus = 0;
	}
	else
	{
		$('a#' + jsLinkID).attr('class', 'active_item');
		TargetRecordStatus = 1;
	}
	
	if (jsType == 'ClassGroup')
	{
		$.post(
			"ajax_update_class_group.php", 
			{ 
				Action: 'Edit',
				DivID: jsLinkID,
				DBFieldName: 'RecordStatus',
				UpdateValue: TargetRecordStatus
			},
			function(ReturnData)
			{
				js_Reload_Class_Group_Add_Edit_Table();
					
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	else if (jsType == 'FormStage')
	{
		$.post(
			"ajax_update.php", 
			{ 
				Action: 'Edit_Form_Stage',
				DivID: jsLinkID,
				DBFieldName: 'RecordStatus',
				UpdateValue: TargetRecordStatus
			},
			function(ReturnData)
			{
				js_Reload_Form_Stage_Settings_Table();
				js_Update_Form_Stage_JSON_Data();
				
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
}
// Class Group function end

// Form Stage function start
function js_Show_Form_Stage_Settings_Layer()
{
	$.post(
		"ajax_reload.php", 
		{ Action: 'Form_Stage_Settings_Layer' },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			
			Init_DND_Table();
			js_Init_JEdit_Form_Stage();
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Reload_Form_Stage_Settings_Table() {
	$.post(
		"ajax_reload.php", 
		{ Action: 'Form_Stage_Settings_Table' },
		function(ReturnData)
		{
			$('#FormStageInfoSettingLayer').html(ReturnData);
			Init_DND_Table();
			js_Init_JEdit_Form_Stage();
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Add_Form_Stage_Row() {
	if (!document.getElementById('GenAddFormStageRow')) {
		var TableBody = document.getElementById("FormStageInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddFormStageRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddFormStageRow";
		
		// Code
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="FormStageCode" id="FormStageCode" value="" class="textbox" maxlength="<?=$FormClassUI->Code_Maxlength?>"/>';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="FormStageTitleEn" id="FormStageTitleEn" value="" class="textbox"/>';
			tempInput += '<div id="TitleEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="FormStageTitleCh" id="FormStageTitleCh" value="" class="textbox"/>';
			tempInput += '<div id="TitleChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		
		// Add and Reset Buttons
		var NewCell3 = NewRow.insertCell(3);
		var temp = '<?=$btnAddFormStage?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancelFormStage?>';
		NewCell3.innerHTML = temp;
		
		$('#FormStageCode').focus();
		
		// Code validation
		js_Add_KeyUp_Code_Checking("FormStageCode", "CodeWarningDiv_New", "", 'FormStage');
		js_Add_KeyUp_Blank_Checking('FormStageTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
		js_Add_KeyUp_Blank_Checking('FormStageTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
	}
}

function js_Clear_Temp_Form_Stage_Info() {
	// Delete the Row
	var TableBody = document.getElementById("FormStageInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddFormStageRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Insert_Form_Stage() {
	var Code = encodeURIComponent(Trim($('#FormStageCode').val()));
	var TitleEn = encodeURIComponent(Trim($('#FormStageTitleEn').val()));
	var TitleCh = encodeURIComponent(Trim($('#FormStageTitleCh').val()));
	
	if (js_Is_Input_Blank('FormStageCode', 'CodeWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['Code']?>'))
	{
		$('#FormStageCode').focus();
		return false;
	}
	if (js_Is_Input_Blank('FormStageTitleEn', 'TitleEnWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>'))
	{
		$('#FormStageTitleEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('FormStageTitleCh', 'TitleChWarningDiv_New', '<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>'))
	{
		$('#FormStageTitleCh').focus();
		return false;
	}
	
	$.post(
		"ajax_validate.php", 
		{
			Action: 'Form_Stage_Code',
			InputValue: Trim($('#FormStageCode').val())
		},
		function(ReturnData)
		{
			if (ReturnData == "")
			{
				// valid code
				$('#CodeWarningDiv_New').hide(jsShowHideSpeed);
				
				// Input data valid => disable submit button and then add building
				$('#NewFormStageSubmitBtn').attr("disabled", "true");
				$('#NewFormStageCancelBtn').attr("disabled", "true");
				
				$.post(
					"ajax_update.php", 
					{ 
						Action: 'Add_Form_Stage',
						Code: Code,
						TitleEn: TitleEn,
						TitleCh: TitleCh
					},
					function(ReturnData)
					{
						js_Reload_Form_Stage_Settings_Table();
						
						// Reload Form Stage JSON data for Form mapping selection box
						js_Update_Form_Stage_JSON_Data();
							
						// Show system messages
						js_Show_System_Message("FormStage", "add", ReturnData);
											
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html(ReturnData);
				$('#CodeWarningDiv_New').show(jsShowHideSpeed);
				
				$('#FormStageCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Update_Form_Stage(DivID, DBFieldName, UpdateValue)
{
	$.post(
		"ajax_update.php", 
		{ 
			Action: "Edit_Form_Stage",
			DivID: encodeURIComponent(DivID),
			DBFieldName: encodeURIComponent(DBFieldName),
			UpdateValue: encodeURIComponent(UpdateValue)
		},
		function(ReturnData)
		{
			js_Reload_Form_Stage_Settings_Table();
			js_Update_Form_Stage_JSON_Data();
			
			// Show system messages
			js_Show_System_Message("FormStage", "update", ReturnData);
				
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Delete_Form_Stage(FormStageID)
{
	if (confirm('<?=$Lang['SysMgr']['FormClassMapping']['jsWarning']['ConfirmDeleteFormStage']?>'))
	{
		$.post(
			"ajax_update.php", 
			{ 
				Action: "Delete_Form_Stage",
				FormStageID: FormStageID
			},
			function(ReturnData)
			{
				js_Reload_Form_Stage_Settings_Table();
				js_Update_Form_Stage_JSON_Data();
			
				// Show system messages
				js_Show_System_Message("FormStage", "delete", ReturnData);
			
				//$('#debugArea').html(ReturnData);
			}
		);
	}
	
	return false;
}

function js_Update_Form_Stage_JSON_Data()
{
	// Refresh Form Stage JSON for jEditable
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Get_Form_Stage_JSON_Data"
		},
		function(ReturnData)
		{
			if (ReturnData != '') {
				jsFormStageData = ReturnData;
			}
		}
	);
	
	// Refresh Form Stage Selection for new Form
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Get_Form_Stage_Selection_JS"
		},
		function(ReturnData)
		{
			if (ReturnData != '') {
				jsFormStageSelection = ReturnData;
			}
		}
	);
}

function js_Init_JEdit_Form_Stage()
{
	// Code
	$('div.jEditCode').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var ElementObj = $(this);
			var thisDivID = $(this).attr("id");
			
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_' + targetID).hide();
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate.php", 
					{
						Action: 'Form_Stage_Code',
						InputValue: updateValue,
						ExcludeID: targetID
					},
					function(ReturnData)
					{
						if (ReturnData == "")
						{
							// valid code
							$('#CodeWarningDiv_'+targetID).hide(jsShowHideSpeed);
							
							// Update value
							if ($('#CodeWarningDiv_' + targetID).is(':visible'))
							{
								// not vaild
								$('#CodeWarningDiv_' + targetID).hide(jsShowHideSpeed);
								$(this)[0].reset();
							}
							else
							{
								// valid
								js_Update_Form_Stage(thisDivID, "Code", updateValue);
							}
						}
						else
						{
							// invalid code => show warning
							ElementObj[0].reset();
							$('#CodeWarningDiv_'+targetID).html(ReturnData);
							$('#CodeWarningDiv_'+targetID).show(jsShowHideSpeed);
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$FormClassUI->Code_Maxlength?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	); 
	// Code Validation
	$('div.jEditCode').keyup( function() {
		var inputValue = $('form > input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		
		js_Code_Validation(inputValue, targetID, 'CodeWarningDiv_'+targetID, 'FormStage');
	});
	$('div.jEditCode').click( function() {
		var inputValue = $('form > input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		js_Code_Validation(inputValue, targetID, 'CodeWarningDiv_'+targetID, 'FormStage');
	});
	
	// TitleEn
	$('div.jEditTitleEn').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleEnWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Form_Stage(thisDivID, "TitleEn", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleEn').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleEnWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	
	// TitleCh
	$('div.jEditTitleCh').editable( 
		function(updateValue, settings) 
		{
			//$(this).html("<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/indicator.gif'>");
			
			var thisDivID = $(this).attr("id");
			var idArr = thisDivID.split('_');
			var divID = idArr[0];
			var targetID = idArr[1];
			
			updateValue = Trim(updateValue);
			
			if (updateValue=='' || $(this)[0].revert == updateValue)
			{
				// not vaild
				$('#TitleChWarningDiv_' + targetID).hide(jsShowHideSpeed);
				$(this)[0].reset();
			}
			else
			{
				js_Update_Form_Stage(thisDivID, "TitleCh", updateValue);
			}
		}, 
		{
			tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",     
			style  : "display:inline",
			height: "20px"
		}
	); 
	// Code Validation - Check Blank Only
	$('div.jEditTitleCh').keyup( function() {
		var inputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var targetID = idArr[1];
		var WarningDivID = 'TitleChWarningDiv_' + targetID;
		
		if (Trim(inputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).hide(jsShowHideSpeed);
		}
	});
	/************ End of Add/Edit Building Info ************/
}

// Form Stage function end

Init_DND_Table();
Get_Form_Class_List();
</script>