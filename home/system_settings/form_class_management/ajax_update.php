<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/libeclass40.php');

intranet_opendb();
$libFCM = new form_class_manage();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Add_Form_Stage')
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	$libFormStage = new Form_Stage();
	
	$DataArr['Code'] = trim(urldecode(stripslashes($_REQUEST['Code'])));
	$DataArr['TitleEn'] = trim(urldecode(stripslashes($_REQUEST['TitleEn'])));
	$DataArr['TitleCh'] = trim(urldecode(stripslashes($_REQUEST['TitleCh'])));
	echo $libFormStage->Insert_Record($DataArr); 	// return inserted FormStageID
}
else if ($Action == 'Edit_Form_Stage')
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	
	$DivID = trim(urldecode(stripslashes($_REQUEST['DivID'])));
	$DBFieldName = trim(urldecode(stripslashes($_REQUEST['DBFieldName'])));
	$UpdateValue = trim(urldecode(stripslashes($_REQUEST['UpdateValue'])));
	
	$DivIDArr = explode('_', $DivID);
	$FormStageID = $DivIDArr[1];
	
	$DataArr = array();
	$DataArr[$DBFieldName] = $UpdateValue;
	
	$objFormStage = new Form_Stage($FormStageID);
	echo $objFormStage->Update_Record($DataArr);
}
else if ($Action == 'Reorder_Form_Stage')
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	$libFormStage = new Form_Stage();
	
	$DisplayOrderString = trim(urldecode(stripslashes($_REQUEST['DisplayOrderString'])));
	$displayOrderArr = $libFormStage->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "FormStageRow_");
	echo $libFormStage->Update_DisplayOrder($displayOrderArr);
}
else if ($Action == "Delete_Form_Stage")
{
	include_once($PATH_WRT_ROOT."includes/libformstage.php");
	
	$FormStageID = stripslashes($_REQUEST['FormStageID']);
	
	$objFormStage = new Form_Stage($FormStageID);
	echo $objFormStage->Delete_Record($SoftDelete=0);
}
else if ($Action == "Import_Class_Student")
{
	$libFCM->Start_Trans();
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	############################################################################
	# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
	############################################################################
	
	### Only imported Student old and new classes are required to be updated 
	$sql = "Select YearClassID, OldYearClassID From TEMP_CLASS_STUDENT_IMPORT Where UserID = '".$_SESSION['UserID']."'";
	$YearClassArr = $libFCM->returnArray($sql);
	$NewYearClassIDArr = Get_Array_By_Key($YearClassArr, 'YearClassID');
	$OldYearClassIDArr = Get_Array_By_Key($YearClassArr, 'OldYearClassID');
	$InvolvedYearClassIDArr = array_values(array_unique(array_merge((array)$NewYearClassIDArr, (array)$OldYearClassIDArr)));
	$InvolvedYearClassIDList = implode(',', (array)$InvolvedYearClassIDArr);
	$numOfInvolvedClass = count($InvolvedYearClassIDArr);
	for ($i=0; $i<$numOfInvolvedClass; $i++) {
		$_yearClassId = $InvolvedYearClassIDArr[$i];
		
		if ($_yearClassId == 0) {
			unset($InvolvedYearClassIDArr[$i]);
		}
	}
	$InvolvedYearClassIDArr = array_values($InvolvedYearClassIDArr);
	$numOfInvolvedClass = count($InvolvedYearClassIDArr);
	$InvolvedYearClassIDList = implode(',', (array)$InvolvedYearClassIDArr);
	
	$sql = "select * from TEMP_CLASS_STUDENT_RESULT_IMPORT where UserID = '".$_SESSION['UserID']."' And YearClassID In (".$InvolvedYearClassIDList.")";
	$StudentInfoArr = $libFCM->returnArray($sql);
	$numOfStudent = count($StudentInfoArr);
	$ClassStudentInfoArr = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$thisYearClassID = $StudentInfoArr[$i]['YearClassID'];
		(array)$ClassStudentInfoArr[$thisYearClassID][] = $StudentInfoArr[$i];
	}
	
	for ($i=0; $i<$numOfInvolvedClass; $i++) {
		$_yearClassId = $InvolvedYearClassIDArr[$i];
		
		if (!isset($ClassStudentInfoArr[$_yearClassId])) {
			$ClassStudentInfoArr[$_yearClassId] = array();
		}
	}
	
	$SuccessArr = array();
	$Counter = 0;
	foreach ($ClassStudentInfoArr as $thisYearClassID => $thisStudentInfoArr)
	{
		$thisStudentIDArr = Get_Array_By_Key($thisStudentInfoArr, 'StudentID');
		$thisClassNumberArr = Get_Array_By_Key($thisStudentInfoArr, 'ClassNumber');
		
		$thisClassObj = new year_class($thisYearClassID,$GetYearDetail=false,$GetClassTeacherList=true,$GetClassStudentList=false);
		$thisClassTeacherIDArr = Get_Array_By_Key($thisClassObj->ClassTeacherList, 'UserID');
		
		// update class student list
		$SuccessArr[$thisYearClassID]['EditClass'] = $libFCM->Edit_Class(	$thisYearClassID, 
																			$thisClassObj->ClassTitleEN, 
																			$thisClassObj->ClassTitleB5, 
																			$thisStudentIDArr, 
																			$thisClassTeacherIDArr, 
																			'ByTime', 
																			$thisClassObj->ClassWEBSAMSCode, 
																			'', 
																			'',
																			$thisClassObj->ClassGroupID
																		);
		
		// 2012-1217-1333-06156
		// Reset_Class_Numbers() should be called after assigned all students
		// Otherwise, if student change class from 1C to 1A, ClassName and ClassNumber of INTRANET_USER will be NULL
		// update class number of the student
		//$SuccessArr[$thisYearClassID]['UpdateClassNumber'] = $libFCM->Reset_Class_Numbers($thisYearClassID, $thisStudentIDArr, $thisClassNumberArr);
		
		$Counter++;
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.$Counter.'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	// update class number of the student
	foreach ($ClassStudentInfoArr as $thisYearClassID => $thisStudentInfoArr) {
		$thisStudentIDArr = Get_Array_By_Key($thisStudentInfoArr, 'StudentID');
		$thisClassNumberArr = Get_Array_By_Key($thisStudentInfoArr, 'ClassNumber');
		
		// update class number of the student
		$SuccessArr[$thisYearClassID]['UpdateClassNumber'] = $libFCM->Reset_Class_Numbers($thisYearClassID, $thisStudentIDArr, $thisClassNumberArr);
	}
	
	############################################################################
	# delete data in Temp table
	############################################################################
	$sql = "delete from TEMP_CLASS_STUDENT_IMPORT where UserID = $UserID";
	$libFCM->db_db_query($sql) or die(mysql_error());
	$sql = "delete from TEMP_CLASS_STUDENT_RESULT_IMPORT where UserID = $UserID";
	$libFCM->db_db_query($sql) or die(mysql_error());
	
	if (in_array(false, $SuccessArr))
	{
		$libFCM->RollBack_Trans();
		$ImportStatus = $Lang['SysMgr']['FormClassMapping']['ImportClassStudentFailed'];
	}
	else
	{
		$libFCM->Commit_Trans();
		$ImportStatus = $Lang['SysMgr']['FormClassMapping']['ImportClassStudentSuccess'];
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();

?>