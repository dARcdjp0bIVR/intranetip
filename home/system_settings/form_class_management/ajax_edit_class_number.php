<?php
@SET_TIME_LIMIT(21600);

// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearClassID = $_REQUEST['YearClassID'];
$UserIDArr = $_REQUEST['UserID'];
$ClassNumber = $_REQUEST['ClassNumber'];

$fcm = new form_class_manage();

$fcm->Start_Trans();

$Result = $fcm->Reset_Class_Numbers($YearClassID,$UserIDArr,$ClassNumber);

## Update Student class_number in eclass
/* moved to Reset_Class_Numbers
$YearClass = new year_class($YearClassID,false,false,false);		
$YearClassAcademicYearID = $YearClass->AcademicYearID;
$CurrentSchoolYearID = Get_Current_Academic_Year_ID();

if(!in_array(false,$Result) && $YearClassAcademicYearID==$CurrentSchoolYearID)
{  
  if(sizeof($UserIDArr) > 0)
  {
    $sql = "SELECT UserEmail, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN (".implode(',',$UserIDArr).") ";
    $row = $fcm->returnArray($sql);
    
    for($i=0; $i < sizeof($row); $i++)
    {
      list($email,$class_name,$class_number) = $row[$i];
      if(trim($class_name) !='')
      { 
        $lc = new libeclass();
        $lc->eClassUserUpdateClassNameIP($email,$class_name,$class_number);
      }      
    }
  }
  
}
*/
/*echo '<pre>';
var_dump($Result);
echo '</pre>';*/
if (!$Result) {
	echo $Lang['SysMgr']['FormClassMapping']['ClassUpdateUnsuccess'];
	$fcm->RollBack_Trans();
}
else {
	echo $Lang['SysMgr']['FormClassMapping']['ClassUpdateSuccess'];
	$fcm->Commit_Trans();
}
intranet_closedb();
?>