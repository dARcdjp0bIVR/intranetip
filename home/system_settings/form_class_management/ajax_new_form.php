<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$YearName = stripslashes(urldecode($_REQUEST['YearName']));
$WEBSAMSCode = stripslashes(urldecode($_REQUEST['WEBSAMSCode']));
$RecordStatus = trim(stripslashes(urldecode($_REQUEST['RecordStatus'])));
$RecordStatus = ($RecordStatus=='')? 1 : $RecordStatus;

$fcm = new form_class_manage();

if ($fcm->Create_Form($YearName,$WEBSAMSCode,$RecordStatus)) {
	echo $Lang['SysMgr']['FormClassMapping']['FormCreateSuccess'];
}
else {
	echo $Lang['SysMgr']['FormClassMapping']['FormCreateUnsuccess'];
}
intranet_closedb();
die;
?>