<?php

#using: Kenneth

############# Change Log [Start] ################
# 	Date:	2014-11-04	Omas
#		- Add import data column descripitions
#		- Js for show/hide remarks 
#
#	Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;
$lgroup = new libgroup();
$linterface 	= new interface_html();

### data column
$DataColumnTitleArr = array($Lang['Group']['NameEn'],$Lang['Group']['NameCh'],$Lang['Group']['GroupDescription'],$Lang['Group']['StorageQuota'],$Lang['Group']['CanUsePublicAnnouncementEvents'],$Lang['Group']['GroupCategory'],$Lang['Group']['DisplayInCommunity'],$Lang['Group']['AllowDeleteOthersAnnouncement']);
$DataColumnPropertyArr = array(1,1,3,1,1,1,3,3);
$DataColumnRemarksArr = array();
$DataColumnRemarksArr[4] = '<span class="tabletextremark">&nbsp;'.$Lang['General']['YesNo'].'</span>';
$DataColumnRemarksArr[5] = '<a id="remarkBtn_type" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'type\')">['.$Lang['Group']['Import']['CheckCategory'].']</a>';
$DataColumnRemarksArr[6] = '<span class="tabletextremark">&nbsp;'.$Lang['General']['YesNo'].'</span>';
$DataColumnRemarksArr[7] = '<span class="tabletextremark">&nbsp;'.$Lang['General']['YesNo'].'</span>';
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

### download csv
$csvFile = "<a class='tablelink' href='". GET_CSV("group_import.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### get Academic Year Selection (current and future year only) ###
$academic_year = new academic_year();
$YearList = $academic_year->Get_All_Year_List('',1);
$AcademicYearSelection = getSelectByArray($YearList," id='SchoolYear' name='SchoolYear' ",$SchoolYear,0,1);

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "index.php");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();   

##Get all category name
$GroupCategoryArr = $lgroup->returnAllCategory();
##Remarks Layer
$RemarksLayer .= '';
$thisRemarksType = 'type';
				$RemarksLayer .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<th>'.$Lang['Group']['GroupCategory'].'</th>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ($GroupCategoryArr as $CategoryName){
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= '<td>'.$CategoryName.'</td>'."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>'."\r\n";
				
?>
<script>
function CheckForm ()
{
	
	if(document.frm1.action == "import_update.php"&&!$("#csvfile").val())  //not select upload file yet
	{
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	
}

function SubmitForm(url)
{
	document.frm1.action = url
}


function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}

</script>

<form method="POST" name="frm1" id="frm1" action="import_update.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
   		<td>
       	<br />
		<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
				<!--<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle">
						<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
					</td>
					<td>
						<table><tr>
						<td valign="top">
							<select name="importTarget" id="importTarget" onChange="showResult(this.value);">
								<option value="form" <? if($importTarget=="form") { echo "selected";} ?>><?=$Lang['eSports']['Form']?></option>
								<option value="class" <? if($importTarget=="class") { echo "selected";} ?>><?=$Lang['eSports']['Class']?></option>
							</select>
						</td>
						<td>
							<?=$FormSelection?><?=$ClassSelection?>
							<br><?= $linterface->GET_ACTION_BTN($button_select_all, "button", "SelectAll();", "selectAllBtn01")?>
							<?= $linterface->GET_ACTION_BTN($button_export, "submit", "SubmitForm('export.php')","submit3","") ?>
						</td>
						</tr></table>
					</td>
				</tr>-->
		<tr>
			<td class="field_title" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?> </td>
			<td class="tabletext"><?=$AcademicYearSelection?></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><input class="file" type="file"  id = "csvfile" name="csvfile"></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
			<td class="tabletext"><?=$csvFile?></td>
		</tr>
		<tr>
			<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
			<td class="tabletext"><?=$DataColumn?></td>
		</tr>
		</table>
		</td>
	</tr>      
	<tr>
		<td colspan="2">        
		<table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
		<tr>
        	<td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
		</tr>		
		<tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
			</td>
		</tr>
        </table>                                
		</td>
	</tr> 
	</table>
	</td>
</tr>
</table>  
</form>
<?=$RemarksLayer?>
<?
	$linterface->LAYOUT_STOP();
?>