<?php
#using by :
/*Change log
 *  2019-08-08 Chris
 *  - updated SSO
 *  2018-09-14 Chris
 *  - added in GoogleSSO
 * */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$li 			= new libdb();
$iCal = new icalendar();
$CalID = $lgroup->CalID;
//SSO
if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false ) {
    include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $IsGoogleSSOEnable = true;
}
## Get Import Data
$ImportData = unserialize(stripslashes($postData));

foreach($ImportData as $key => $Record)
{
    list($user_id,$GroupID)=$Record;
    $lgroup = new libgroup($GroupID);
    $CalID = $lgroup->CalID;
    $RoleID = $lgroup->returnGroupDefaultRole($GroupID);
    
    if($lgroup->isGroupMember($user_id)<=0)
    {
        $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('$GroupID','$user_id','$RoleID',now(),now())";
        $li->db_db_query($sql);
        // # Google SSO Implementation
        if ($IsGoogleSSOEnable == true) {
            $results = $libGoogleSSO->enabledUserForGoogle($li, array($user_id));
            if (count($results['activeUsers'])>0 || count($results['suspendedUsers'])>0) {
                $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $user_id . '\';';
                $userlogin = current($li->returnVector($sql2));
                $libGoogleSSO->syncGroupMemberFromEClassToGoogle( $userlogin, $GroupID);
            }
        }
        ##############################################
        ## START - add CALENDAR_CALENDAR_VIEWER
        ##############################################
        $CalID = $lgroup->CalID;
        $cal_sql = "
			insert into CALENDAR_CALENDAR_VIEWER
			(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
			values
			($CalID, ".$user_id.", $GroupID, 'E', 'R', '2f75e9', '1')
		";
        $li->db_db_query($cal_sql);
        ##############################################
        ## END - add CALENDAR_CALENDAR_VIEWER
        ##############################################
    }
    
}


# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($li->Title, "index.php?GroupID=$GroupID&filter=$filter");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();

?>
<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><?=count($ImportData)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
?>
