<?php
#using:

/*************************************************
 *  modification log
 * 2019-08-09 Chris
 *  - Updated Google SSO sync
 *
 * 2018-09-17 Chris
 *  - Added in Google SSO sync
 *
 *	2011-10-04 Carlos
 *	- add calendar viewer for new members that other personal calendars have shared to this group
 *
 *	2010-12-20	YatWoon
 *	- use "INSERT IGNORE INTO" instead of "REPLACE INTO".  Fixed: using "REPLACE INTO" will lost all the admin right settings for the user
 *
 *	2010-05-18 YatWoon
 *	- due to the select student's page is changed to use "post" method
 *
 * 	20100416 Marcus:
 * 	- also insert EnrolGroupID into INTRANET_USERGROUP
 * ***********************************************/

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libgroup.php");
include_once("../../../../includes/form_class_manage.php");
include_once("../../../../includes/icalendar.php");
intranet_auth();
intranet_opendb();
$PATH_WRT_ROOT = "../../../../";
$li = new libdb();

$lgroup = new libgroup($GroupID);
$CalID = $lgroup->CalID;
// ### Google SSO //
if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false ) {
    include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $IsGoogleSSOEnable = true;
}
//$role = $_GET["role"];
$role = $_POST["role"];

$AcademicYearID = $lgroup->AcademicYearID;
$ay = new academic_year($AcademicYearID);
$sql = "";
if(isset($target))
{
    if(stristr($target,","))
        $target = explode(",",$target);
        else
            $target = array($target);
            
            # insert calendar viewer for those new group members to calendars that are shared to this group
            $insertOtherCalendarViewerUser = array();
            
            //$insert_sql = "REPLACE INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified, EnrolGroupID) VALUES ";
            $insert_sql = "INSERT IGNORE INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified, EnrolGroupID, ModifiedBy) VALUES ";
            $delimiter = $some_failed = '';
            $sql1 = 'SELECT `AcademicYearID` FROM `INTRANET_GROUP` WHERE `GroupID`=\''.$GroupID.'\';';
            $GroupAcademicYearID=current($li->returnVector($sql1));
            for ($i=0; $i<sizeof($target); $i++)
            {
                $targetType = substr($target[$i],0,1);
                $user = substr($target[$i],1);
                
                if($targetType=="U")
                {
                    if(!$ay->Check_User_Existence_In_Academic_Year($user))
                    {
                        $some_failed = true;
                        continue;
                    }
                    $sql .= $delimiter."($GroupID, ".$user.", $role, now(), now(), '$EnrolGroupID', '".$_SESSION['UserID']."')";
                    // ### Google SSO
                    
                    if ($IsGoogleSSOEnable == true ) {
                        $results = $libGoogleSSO->enabledUserForGoogle($li, array($user));
                        if (count($results['activeUsers'])>0 || count($results['suspendedUsers'])>0) {
                            $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $user . '\';';
                            $userlogin = current($li->returnVector($sql2));
                            $libGoogleSSO->syncGroupMemberFromEClassToGoogle( $userlogin, $GroupID);
                        }
                    }
                    ##############################################
                    ## START - add CALENDAR_CALENDAR_VIEWER
                    ##############################################
                    $cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				values
				($CalID, ".$user.", $GroupID, 'E', 'R', '2f75e9', '1')
			";
                    $li->db_db_query($cal_sql);
                    ##############################################
                    ## END - add CALENDAR_CALENDAR_VIEWER
                    ##############################################
                    $delimiter = ',';
                    
                    $insertOtherCalendarViewerUser[] = $user;
                }
                else
                {
                    $lg = new libgroup($user);
                    $GroupMember = $lg->returnGroupUser();
                    
                    foreach($GroupMember as $k=>$d)
                    {
                        if(!$ay->Check_User_Existence_In_Academic_Year($d["UserID"]))
                        {
                            $some_failed = true;
                            continue;
                        }
                        $sql .= $delimiter."($GroupID, ".$d["UserID"].", $role, now(), now(), '$EnrolGroupID', '".$_SESSION['UserID']."')";
                        // ### Google SSO
                        if ($IsGoogleSSOEnable == true) {
                            $results = $libGoogleSSO->enabledUserForGoogle($li, array($d['UserID']));
                            if (count($results['activeUsers'])>0||count($results['suspendedUsers'])>0) {
                                $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $d["UserID"] . '\';';
                                $userlogin = current($li->returnVector($sql2));
                                $libGoogleSSO->syncGroupMemberFromEClassToGoogle( $userlogin, $GroupID);
                            }
                        }
                        ##############################################
                        ## START - add CALENDAR_CALENDAR_VIEWER
                        ##############################################
                        $cal_sql = "
					insert into CALENDAR_CALENDAR_VIEWER
					(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
					values
					($CalID, ".$d["UserID"].", $GroupID, 'E', 'R', '2f75e9', '1')
				";
                        $li->db_db_query($cal_sql);
                        ##############################################
                        ## END - add CALENDAR_CALENDAR_VIEWER
                        ##############################################
                        $delimiter = ',';
                        
                        $insertOtherCalendarViewerUser[] = $d["UserID"];
                    }
                    
                }
                
            }
            
            if(count($insertOtherCalendarViewerUser)){
                $iCal = new icalendar();
                $iCal->addCalendarViewerToGroup($GroupID,$insertOtherCalendarViewerUser);
            }
            
            // 	debug($insert_sql.$sql); die;
            if(!empty($sql))
            {
                $success = $li->db_db_query($insert_sql.$sql) or die(mysql_error());
                $li->UpdateRole_UserGroup();
            }
            if($success && !$some_failed)
            {
                $msg = 1;
            }
            else
            {
                $msg = 2;
            }
            
            
}

/*
 if(sizeof($ChooseUserID) <> 0){
 $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID) VALUES ";
 for($i=0;$i<sizeof($ChooseUserID);$i++){
 $sql .= ($i==0) ? "" : ",";
 $sql .= "($GroupID, ".$ChooseUserID[$i].", $RoleID)";
 
 ##############################################
 ## START - add CALENDAR_CALENDAR_VIEWER
 ##############################################
 $cal_sql = "
 insert into CALENDAR_CALENDAR_VIEWER
 (CalID, UserID, GroupID, GroupType, Access, Color, Visible)
 values
 ($CalID, ".$ChooseUserID[$i].", $GroupID, 'E', 'R', 'FFFFFF', '1')
 ";
 $li->db_db_query($cal_sql);
 ##############################################
 ## END - add CALENDAR_CALENDAR_VIEWER
 ##############################################
 }
 $li->db_db_query($sql);
 $li->UpdateRole_UserGroup();
 
 
 
 }
 */
intranet_closedb();
if($Semester)
    $par = "&Semester=$Semester";
    header("Location: index.php?GroupID=$GroupID".$par."&xmsg=".$msg);
    ?>
