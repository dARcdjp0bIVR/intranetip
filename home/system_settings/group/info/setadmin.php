<?php
# Using By : 

############# Change Log [Start] ################
#
#	Date:	2015-04-21	Bill	[2015-0420-1038-16206] 
#			update DateModified and ModifiedBy to log any modification of admin flag
# 
#	Date:	2014-06-10	Bill
#			redirect to correct page for admin status filtering 
#	Date: 	2012-12-07  Rita add delete log
#	Date:	2010-11-02	YatWoon
#			can multiple set admin
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');

intranet_opendb();

$li = new libdb();
$liblog = new liblog();

if(is_array($UID)){
	$UID = implode(",",$UID);
}	
$user_list = str_replace(",","','",$UID);


###### Get Info for Delete Log #####
$sql = "SELECT 
				CalID,
				UserID,					 
				GroupID,
				GroupType,
				Access,
			    Color,
			    Visible,
			    GroupPath
		FROM
				CALENDAR_CALENDAR_VIEWER
		WHERE
				UserID IN ('$user_list')
		AND
				CalID in 
				(select CalID from INTRANET_GROUP where GroupID = '$GroupID')";
					
$infoAry =  $li->returnArray($sql);
######  End Of Get Info for Delete Log ###### 


if ($adminflag == "0" || $adminflag == "")
{
    $adminflag = "NULL";
    $rights_string = "NULL";
//     $user_list = implode("','",$UID);
	$NewAccess = "R";
	$logSection ='Update_Group_SetAsMember';

	######    Update Right   #######
	$sql = "update CALENDAR_CALENDAR_VIEWER set Access = '".$NewAccess."' where UserID IN ('$user_list') and CalID in (select CalID from INTRANET_GROUP where GroupID = '$GroupID')";
	$li->db_db_query($sql);
}
else
{
    $adminflag = "'$adminflag'";
    $logSection ='Update_Group_SetAsAdmin';
    
//     $user_list = "$UID";
    if ($alltools == 1)
    {
        $rights_string = 'NULL';
    }
    else
    {
        # Change the tool rights to integer
        $sum = 0;
        for ($i=0; $i<sizeof($grouprights); $i++)
        {
             $target = intval($grouprights[$i]);
             $sum += pow(2,$target);
        }
        $rights_string = $sum;
    }	
    
  
	if ($alltools == 1 || in_array("4",$grouprights)){
			
		$NewAccess = "W";
		
		##### Update Right
		$sql = "update CALENDAR_CALENDAR_VIEWER set Access = '".$NewAccess."' where UserID IN ('$user_list') and CalID in (select CalID from INTRANET_GROUP where GroupID = '$GroupID')";
		$li->db_db_query($sql);
	}
	else {
		
		$NewAccess = "R";
		
		##### Update Right
		$sql = "update CALENDAR_CALENDAR_VIEWER set Access = '".$NewAccess."' where UserID IN ('$user_list') and CalID in (select CalID from INTRANET_GROUP where GroupID = '$GroupID')";
		$li->db_db_query($sql);
	}
}


if ($sys_custom['Group']['AdminRightLog']) {
	############       Start of Edit Log       ###########
	$numOfInfoAry = count($infoAry);	
	for($i=0;$i<$numOfInfoAry;$i++){
		$tmpAry = array();		
		$section = $logSection;
		$tableName = 'CALENDAR_CALENDAR_VIEWER';
	
		$CalID = $infoAry[$i]['CalID'];
		$userRecordID = $infoAry[$i]['UserID'];
		$GroupID= $infoAry[$i]['GroupID'];
		$GroupType = $infoAry[$i]['GroupType'];
		$Color = $infoAry[$i]['Color'];
		$Visible = $infoAry[$i]['Visible'];
		$GroupPath = $infoAry[$i]['GroupPath'];
		$Access = $infoAry[$i]['Access'];
		
		$tmpAry['CalID'] = $CalID;
		$tmpAry['UserID'] = $userRecordID;
		$tmpAry['GroupID'] = $GroupID;
		$tmpAry['GroupType'] = $GroupType;
		$tmpAry['Color'] = $Color;
		$tmpAry['Visible'] = $Visible;
		$tmpAry['GroupPath'] = $GroupPath;
		
		## Changes
		$tmpAry['OrginalAccess'] = $Access;
		$tmpAry['NewAccess'] = $NewAccess;
			
		# Insert Delete Log
		$successArr['Log_Delete'] = $liblog->INSERT_LOG('SchoolSettings_Group', $section, $liblog->BUILD_DETAIL($tmpAry), $tableName, $userRecordID);	
	}
	
	###### Get Info for Delete Log #####
	$sql = "SELECT 
					UserGroupID,
					RecordType,
					AdminAccessRight,
					UserID,
					GroupID,
					EnrolGroupID
			FROM
					INTRANET_USERGROUP
			WHERE
					GroupID = $GroupID AND UserID IN ('$user_list')";
					
	$iuInfoAry =  $li->returnArray($sql);
	
	######  End Of Get Info for Delete Log ###### 
	$numOfIuInfoAry = count($iuInfoAry);	
	for($i=0;$i<$numOfIuInfoAry;$i++){
		$iuTmpAry = array();
							
		//if ($adminflag == "NULL" || $adminflag == "NULL"){
		if ($adminflag == "NULL"){
			$section = 'Update_Group_SetAsMember';
		}elseif($_POST['grouprights']){
			$section = 'Update_Group_SetAsAdmin';
		}else{
			$section = 'Update_Group_SetAsAdmin';
		}
		
		
		$tableName = 'INTRANET_USERGROUP';
	
		$userRecordID = $iuInfoAry[$i]['UserID'];
		$UserGroupID = $iuInfoAry[$i]['UserGroupID'];
		$originalRecordType = $iuInfoAry[$i]['RecordType'];
		$RecordType = $iuInfoAry[$i]['RecordType'];
		$AdminAccessRight = $iuInfoAry[$i]['AdminAccessRight'];
		$tempGroupID = $iuInfoAry[$i]['GroupID'];
		$tempEnrolGroupID = $iuInfoAry[$i]['EnrolGroupID'];
			
		$iuTmpAry['UserID'] = $userRecordID;
		$iuTmpAry['UserGroupID'] = $UserGroupID;
		$iuTmpAry['originalRecordType'] = $originalRecordType;
		$iuTmpAry['RecordType'] = $adminflag;
		$iuTmpAry['AdminAccessRight'] = $AdminAccessRight;
		$iuTmpAry['GroupID'] = $tempGroupID;
		$iuTmpAry['EnrolGroupID'] = $tempEnrolGroupID;
		
		# Insert Delete Log
		$successArr['Log_Delete'] = $liblog->INSERT_LOG('SchoolSettings_Group', $section, $liblog->BUILD_DETAIL($iuTmpAry), $tableName, $UserGroupID);	
	}		
	############       End of Edit Log      #############
}

// [2015-0420-1038-16206] update DateModified and ModifiedBy to log any modification of admin flag
$sql = "UPDATE INTRANET_USERGROUP SET RecordType = $adminflag, AdminAccessRight = $rights_string, DateModified = NOW(),	ModifiedBy = '".$_SESSION['UserID']."'
        WHERE GroupID = $GroupID AND UserID IN ('$user_list')";

$li->db_db_query($sql);

intranet_closedb();

if($Semester) $par = "&Semester=$Semester";
header("Location: index.php?GroupID=$GroupID&filter=$filter&msg=11&RolesType=".$RolesType.$par);
?>
