<?php
/*********************************
 * 	modification
 * 		20150421 Bill: [2015-0420-1038-16206]
 * 			to log modified by
 * 		20140610 Bill:
 * 			for admin status filtering, redirect to correct page
 * 		20100416 Marcus:
 * 			for enrolment, update user role of specific sem only   
 * *******************************/

include("../../../../includes/global.php");
include("../../../../includes/libdb.php");
intranet_opendb();


$li = new libdb();
if (is_array($UID) && sizeof($UID)!=0)
{
	if($EnrolGroupID)
		$EnrolGroupSql = " AND EnrolGroupID = $EnrolGroupID ";
	
	// [2015-0420-1038-16206] update ModifiedBy to log any modification of role
    $sql = "UPDATE INTRANET_USERGROUP SET RoleID = $RoleID, DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' WHERE GroupID = $GroupID $EnrolGroupSql AND UserID IN (".implode(",", $UID).")";
    //debug($sql); die;
	$li->db_db_query($sql);
}

intranet_closedb();
if($Semester)$par = "&Semester=$Semester";
if($SchoolYear)$par = "&SchoolYear=$SchoolYear";
header("Location: index.php?GroupID=$GroupID&filter=$filter&msg=2&RolesType=".$RolesType.$par);
?>