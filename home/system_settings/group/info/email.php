<?php

############# Change Log [Start] ################
# Date:	2010-04-16 Marcus
#		- add EnrolGroupID(if any) as condition to Sql 
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libgroup.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libaccount.php");
include($PATH_WRT_ROOT."includes/libinterface.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include("../../../templates/adminheader_intranet.php");
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface 	= new interface_html();
$li = new libgroup($GroupID);

if($EnrolGroupID) $EnrolGroupCond = " AND EnrolGroupID = '$EnrolGroupID' ";

$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'],"../index.php");
$PAGE_NAVIGATION[] = array($lg->Title,"index.php?GroupID=".$GroupID);
$PAGE_NAVIGATION[] = array($Lang['Group']['SendEmail'],"");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role");
$TAGS_OBJ = $li->getTopTabInfoAry('groupMember');
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['Group'];
$linterface->LAYOUT_START();   

?>

<script language="javascript">
function checkform(obj){
	if(!check_text(obj.subject, "<?php echo $i_alert_pleasefillin.$i_email_subject; ?>.")) return false;
	if(!check_text(obj.message, "<?php echo $i_alert_pleasefillin.$i_email_message; ?>.")) return false;
	return (confirm("<?php echo $i_email_sendemail; ?>?")) ? true : false;
}
</script>

<form name="form1" action=email_update.php method=post onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>

<tr>
	<td class="tabletext">
<table border=0 cellpadding=4 cellspacing=0>
<tr><td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?php echo $Lang['Group']['Recipient'] ?>:</td><td><select name=UserEmail[] size=5 multiple><?php echo $li->displayGroupUsersEmailOption($EnrolGroupCond); ?></select></td></tr>
<tr><td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?php echo $Lang['Group']['Subject'] ?>:</td><td><input class=text type=text name=subject size=35></td></tr>
<tr><td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?php echo $Lang['Group']['Comment'] ?>:</td><td><textarea name=message cols=55 rows=15 wrap=virtual></textarea></td></tr>
</table>
	</td>
</tr>

<input type=hidden name=GroupID value="<?php echo $li->GroupID; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<tr>
<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td>
			</tr>
		</table>                                


<!--<table border=0 cellpadding=0 cellspacing=0>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?> 
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>-->
</td>
</tr>
</table>
</form>

<?php 
intranet_closedb();
$linterface->LAYOUT_STOP();   

//include("../../../templates/adminfooter.php"); 
?>
