<?php
#using: yat

############# Change Log [Start] ################
#
#	Date:	2014-06-10	bill
#			add field for admin status filtering
#	Date:	2010-11-02	YatWoon
#			can multiple set admin
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
if (is_file($PATH_WRT_ROOT."includes/libqb.php"))
{
    include_once($PATH_WRT_ROOT."includes/libqb.php");
}
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$CurrentPageArr['Group']=1;
intranet_auth();
intranet_opendb();

//if (is_array($UID)) $UID = $UID[0];
$lu = new libuser();
$lg = new libgroup($GroupID);

$linterface = new interface_html();
$all_checked = "";
#### User Info
if(!is_array($UID))
	$UID[0] = $UID;

if ($lg->hasAllAdminRights($UID[0]))
{
    $all_checked = "CHECKED";
}
$availableRights = $lg->getSelectCurrentAdminRights($UID[0]);	# will according to the first user's access right as default
	
$user_info_ary = $lu->returnUser("","", $UID);
foreach($user_info_ary as $k=>$d)
{
	$user_name = $intranet_session_language=="en" ? $d['EnglishName'] : $d['ChineseName'];
	
	if($d['RecordType']==2)	# student
		$user_name_str .= $user_name . " (". $d['ClassName'] . "-". $d['ClassNumber'] .")<br>";
	else
		$user_name_str .= $user_name . " <br>";
}

$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'],"../index.php");
$PAGE_NAVIGATION[] = array($lg->TitleDisplay,"index.php?GroupID=".$GroupID);
$PAGE_NAVIGATION[] = array($i_GroupAssignAdminRight,"");


$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../",1);
$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role");
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=javascript>
function checkform(obj)
{
         if (countChecked(obj,'grouprights[]')==0)
         {
             alert(globalAlertMsg2);
             return false;
         }
         return true;
}
function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouprights[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouprights[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}
</SCRIPT>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>


<form name=form1 method=post action='setadmin.php' onSubmit="return checkform(this)">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?php echo $i_general_name; ?> (<?=$i_ClassName?>-<?=$i_ClassNumber?>)</td>
	<td><?=$user_name_str?></td>
</tr>
		<tr>
			<td class="field_title"><?php echo $i_GroupAssignAdminRight_AllRights; ?>:</td><td><input TYPE=checkbox NAME=alltools VALUE=1 <?=$all_checked?> onClick="allToolsChecked(this.form)"></td>
		</tr>
		<tr>
			<td class="field_title"><?=$i_GroupAssignAdminRight_AvailableAdminRights?>:</td><td><?=$availableRights?></td>
		</tr>
	</table>

	
	

<input type=hidden name=GroupID value="<?=$GroupID?>">
<input type=hidden name=EnrolGroupID value="<?=$EnrolGroupID?>">
<input type=hidden name=UID value="<?=implode(",",$UID)?>">
<input type=hidden name=adminflag value="A">
<input type=hidden name=RolesType value="<?=$RolesType?>">

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
<p class="spacer"></p>
</div>
		
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>