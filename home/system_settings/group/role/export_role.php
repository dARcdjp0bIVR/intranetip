<?php
# using:
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
// $libenroll = new libclubsenrol();
// $libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext(); 

// debug_pr($_POST['exportGroupRoleSQL']);
$type = "";
$roleInfoSourceFilter = $_POST['exporGroupRoleFilter'];
$roleInfoSourceKeyword = $_POST['exporGroupRoleKeyword'];
$roleInfoSourceOrder = $_POST['exporGroupRoleOrder'];
$roleInfoSourceField = $_POST['exportGroupRoleField'];
if ($roleInfoSourceFilter != "") $type_conds = "a.RecordType = '$roleInfoSourceFilter' AND";
if($roleInfoSourceOrder == '0'){
    switch($roleInfoSourceField){
        case '0':
            $type = "a.Title";
            break;
        case '1':
            $type = "a.Description";
            break;
        case '2':
            $type = "b.CategoryName";
            break;
        case '3':
            $type = "a.DateModified";
            break;
    }
    //debug_pr($roleInfoSourceFilter);
    $sql  = "SELECT
                        a.Title as Name,
                        a.Description,
                        b.CategoryName,
                        a.DateModified,
                        IF(a.RecordStatus='1','<img src={$image_path}/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=RoleID[] value=', a.RoleID ,'>'))
            FROM
                        INTRANET_ROLE as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b
                                      ON a.RecordType = b.GroupCategoryID
            WHERE
                        $type_conds (a.Title like '%$roleInfoSourceKeyword%' OR a.Description like '%$roleInfoSourceKeyword%') 
                        
            ORDER BY " . 
                        $type . " DESC
        ";
}
else{
    switch($roleInfoSourceField){
        case '0':
            $type = "a.Title";
            break;
        case '1':
            $type = "a.Description";
            break;
        case '2':
            $type = "b.CategoryName";
            break;
        case '3':
            $type = "a.DateModified";
            break;
    }
    $sql  = "SELECT
                        a.Title as Name,
                        a.Description,
                        b.CategoryName,
                        a.DateModified,
                        IF(a.RecordStatus='1','<img src={$image_path}/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=RoleID[] value=', a.RoleID ,'>'))
            FROM
                        INTRANET_ROLE as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b
                                      ON a.RecordType = b.GroupCategoryID
            WHERE
                        $type_conds (a.Title like '%$roleInfoSourceKeyword%' OR a.Description like '%$roleInfoSourceKeyword%')
                        
            ORDER BY " . 
                       $type . " ASC
        ";
}
//debug_pr($sql);
$rs = $ldb->returnResultSet($sql);
//debug_pr($rs);
$headerAry = array();
$headerAry[] = "Name";
$headerAry[] = "Description";
$headerAry[] = "Category";
$headerAry[] = "Last Modified";
$numRow = count($rs);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $rs[$i]['Name'];
    $dataAry[$i][$_col++] = $rs[$i]['Description'];
    $dataAry[$i][$_col++] = $rs[$i]['CategoryName'];
    $dataAry[$i][$_col++] = $rs[$i]['DateModified'];
//debug_pr($dataAry);
}
$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "Group_role.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>