<?php
$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/librole.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libaccount.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group']=1;

$lgroup = new libgroup();
$linterface = new interface_html();

$li = new librole();

### Title ###
$PAGE_NAVIGATION[] = array($i_admintitle_am_role, "index.php");
$PAGE_NAVIGATION[] = array($i_RoleDefault, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../");
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('groupRole');
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();   

?>

<form action=set_update.php method=post>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
				<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td class="tabletext"><?php echo $li->displaySetDefaultRole(); ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td>
			</tr>
		</table>                                
	</td>
</tr>
</table>   
<!--<p class=admin_head><?php echo $i_admintitle_sc.displayArrow().$i_adminmenu_sc_group_settings.displayArrow(); ?><a href=javascript:history.back()><?php echo $i_admintitle_am_role; ?></a><?php echo displayArrow();?><?php echo $i_RoleDefault; ?></p>-->
<!--<blockquote>
<?php echo $li->displaySetDefaultRole(); ?>
<input class=submit type=submit value="<?php echo $button_save; ?>"><input class=reset type=reset value="<?php echo $button_reset; ?>"><input class=button type=button value="<?php echo $button_cancel; ?>" onClick=history.back()>
</blockquote>-->
</form>

<?php
$linterface->LAYOUT_STOP();   

intranet_closedb();
//include("../../templates/adminfooter.php");
?>