<?php

############# Change Log [Start] ################
# Date:	2019-04-17 Pun [ip.2.5.10.5.1]
#		- Added SQL Injection protection
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();

$RecordType = IntegerSafe($RecordType);
$RoleID = IntegerSafe($RoleID);
$presetValue = IntegerSafe($presetValue);
$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));

if($presetValue==1)
{
	$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 0 WHERE RecordType = ".$RecordType;
	$li->db_db_query($sql);
}

$sql = "UPDATE INTRANET_ROLE SET Title = '$Title', Description = '$Description', RecordType = '$RecordType',RecordStatus = $presetValue, DateModified = now() WHERE RoleID = $RoleID";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$RecordType&xmsg=update");
?>
