<?php
$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "UPDATE INTRANET_USERGROUP SET RoleID = NULL WHERE RoleID IN (".implode(",", $RoleID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_ROLE WHERE RoleID IN (".implode(",", $RoleID).")";
$li->db_db_query($sql);

$li->UpdateRole_UserGroup();

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&xmsg=delete");
?>
