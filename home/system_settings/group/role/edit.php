<?php

#editing by 

/* Change Log
2019-04-17 Pun [ip.2.5.10.5.1]
    - Added XSS protection

2017-06-07 (Icarus): Modified the HTML code, added		<?= $linterface->RequiredSymbol() ?>
											added		<?=$linterface->Get_Radio_Button(		for yes and no buttons.
2017-06-06 (Icarus): Modified the HTML code, change the CSS of the form to reach the UI consistency to the sample page. 
						The original code has been commented.		

 */

############# Change Log [Start] ################
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$RoleID = IntegerSafe($RoleID);

$lgroup = new libgroup();
$li = new librole($RoleID[0]);
$lgc = new libgroupcategory($li->RecordType);

$CurrentPageArr['Group'] = 1;
$linterface 	= new interface_html();

$RoleID[0]==$li->returnDefaultRole()?$check_yes="CHECKED":$check_no="CHECKED";

#title
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$PAGE_NAVIGATION[] = array($i_admintitle_am_role, "index.php");
$PAGE_NAVIGATION[] = array($button_edit, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../");
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"../groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('groupRole');
$linterface->LAYOUT_START();   

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_RoleTitle; ?>.")) return false;
        if(obj.RecordType.value == '') { alert('<?=$i_alert_pleaseselect.$i_GroupRecordType?>'); return false; }
}
</script>
<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
<?=$linterface->GET_SYS_MSG($xmsg);?>
<table class="form_table_v30" style="padding:25px 0px;">
	<tr>
		<td class="field_title"><?= $linterface->RequiredSymbol() ?><?=$i_RoleTitle ?></td>
		<td><input class=text type=text name=Title size=30 maxlength=100 value="<?php echo $li->Title; ?>"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$i_RoleDescription ?></td>
		<td><textarea name=Description cols=30 rows=5><?php echo $li->Description; ?></textarea></td>
	</tr>
	<tr>
		<td class="field_title"><?= $linterface->RequiredSymbol() ?><?=$i_RoleRecordType ?></td>
		<td><?php echo (($li->RecordStatus==1) ? $lgc->CategoryName." <input type=hidden name=RecordType value='".$li->RecordType."'>" : $lgc->returnSelectCategory("name=RecordType",false,0,$li->RecordType) ); ?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['Group']['Role']['SetPresetRole']?></td>
		<td>
			<?=$linterface->Get_Radio_Button('presetValueYes', 'presetValue', $Value=1, $check_yes, $Class="", $Display="Yes", $Onclick="",$isDisabled=0);?>
			<?=$linterface->Get_Radio_Button('presetValueNo', 'presetValue', $Value=0, $check_no, $Class="", $Display="No", $Onclick="",$isDisabled=0);?>
		</td>
	</tr>
</table>
<br style="clear:both;" />
<span class="tabletextremark"><?=$i_general_required_field?></span>
<p class="spacer"></p>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<p class="spacer"></p>
</div>





<!--<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_admintitle_am_role, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_group_role_$intranet_session_language.gif", $msg) ?>
-->

<!--	THE ORIGINAL CODE
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
				<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_RoleTitle ?><span class='tabletextrequire'>*</span>:</td>
							<td class="tabletext"><input class=text type=text name=Title size=30 maxlength=100 value="<?php echo $li->Title; ?>"></td>
						</tr>
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_RoleDescription ?>:</td>
							<td class="tabletext"><textarea name=Description cols=30 rows=5><?php echo $li->Description; ?></textarea></td>
						</tr>
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_RoleRecordType ?><span class='tabletextrequire'>*</span>:</td>
							<td class="tabletext"><?php echo (($li->RecordStatus==1) ? $lgc->CategoryName." <input type=hidden name=RecordType value='".$li->RecordType."'>" : $lgc->returnSelectCategory("name=RecordType",false,0,$li->RecordType) ); ?></td>
						</tr>
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['Group']['Role']['SetPresetRole']?>:</td>
							<td class="tabletext">
								<input type="radio" id="presetValueYes" name="presetValue" value="1" <?=$check_yes?>><label for="presetValueYes"><?=$i_general_yes?></label>
								<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" />
								<input type="radio" id="presetValueNo" name="presetValue" value="0" <?=$check_no?>><label for="presetValueNo"><?=$i_general_no?></label>
							</td>
						</tr>						
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td>
			</tr>
		</table>                                
	</td>
</tr>
</table>   
-->


<input type=hidden name=RoleID value="<?php echo $li->RoleID; ?>">
</form>

<?php
$linterface->LAYOUT_STOP();

intranet_closedb();
//include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>