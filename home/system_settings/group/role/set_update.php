<?php
$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();

$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 0";
$li->db_db_query($sql);

for($i=0; $i<sizeof($i_GroupRole); $i++){
	$RoleID = ${"RoleID".$i};
	$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 1 WHERE RecordType = $i AND RoleID = $RoleID";
	$li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?filter=$filter&msg=2");
?>
