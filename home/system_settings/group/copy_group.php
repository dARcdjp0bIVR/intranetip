<?php
# using: 

/*	Change Log


2017-06-06 (Icarus): Modified the HTML code, change the CSS of the form to reach the UI consistency to the sample page. 
						The original code has been commented.					
*/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;

$linterface 	= new interface_html();
$lgroup = new libgroup();

### get Academic Year Selection ###
$academic_year = new academic_year();
$YearList = $academic_year->Get_All_Year_List();
//$FutureYearList = $academic_year->Get_All_Year_List('',1);
// 2012-0903-1628-12071
if (isset($sys_custom['Group']['CanEditPastYearGroupUserIDArr']) && in_array($_SESSION['UserID'], $sys_custom['Group']['CanEditPastYearGroupUserIDArr'])) {
	$FutureYearList = $academic_year->Get_All_Year_List();
}
else {
	$FutureYearList = $academic_year->Get_All_Year_List('',1);
}

$YearIDList = Get_Array_By_Key($YearList,"AcademicYearID");
$FutureYearIDList =   Get_Array_By_Key($FutureYearList,"AcademicYearID");

$CopyTo = $SchoolYear?$SchoolYear:$CopyTo;
$CopyToKey = (array_search($CopyTo,$YearIDList)-1)>=0?array_search($CopyTo,$YearIDList)-1:1;

$CopyFrom = $CopyFrom?$CopyFrom:$YearIDList[$CopyToKey];
$AcademicYearSelection = getSelectByArray($YearList," id='CopyFrom' name='CopyFrom' onchange='refreshGroupList();' ",$CopyFrom,0,1);
$CopyToSelection = getSelectByArray($FutureYearList," id='CopyTo' name='CopyTo' onchange='refreshGroupList();' ",$CopyTo,0,1);

### Get Btn
$BackBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_continue, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn = $NextBtn."&nbsp;".$BackBtn;

### Get table
if($SchoolYear)
	$CopyGroupTable = $lgroup->Get_Group_Mgmt_Copy_UI($CopyFrom, $CopyTo);
else
	 $CopyGroupTable = $lgroup->Get_Group_Mgmt_Copy_UI($CopyFrom, $CopyTo, $CopyGroup, $CopyMember);

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmt'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Group']['CopyGroup'], "");

//$TAGS_OBJ[] = array($Lang['Group']['CopyGroup'],"",1);
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($Lang['Group']['Options'], 1);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$linterface->LAYOUT_START();   
?>
<br />

<form name="frm1" method="POST" action="copy_confirm.php" onsubmit="return CheckForm();" >
<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
<div style="padding-top:25px;"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></div>
<br />
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyFrom']?></td>
		<td><?=$AcademicYearSelection?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyTo']?></td>
		<td><?=$CopyToSelection?></td>
	</tr>
</table>
<br style="clear:both;" />
<table class="form_table_v30">
	<tr>
		<td style="border-bottom-style:none;"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td>
	</tr>
	<tr>
		<td style="border-bottom-style:none;"><div id="GroupTableDiv"><?=$CopyGroupTable?></div></td>
	</tr>
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$Btn?>
	<p class="spacer"></p>
</div>

<!--  THE ORIGINAL CODE
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">

	<tr>
		<td clospan="2" align="center">
			<table width="90%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="100%">
					<tr>
						<td class='formfieldtitle' width="30%"><?=$Lang['Group']['CopyFrom']?></td>
						<td class='tabletext'><span id='CopyFromSelectSpan'><?=$AcademicYearSelection?></span></td>
					</tr>
					<tr>
						<td class='formfieldtitle' width="30%"><?=$Lang['Group']['CopyTo']?></td>
						<td class='tabletext'><span id='CopyToSelectSpan'> <?=$CopyToSelection?></span></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td></tr>
			</table>
			<table width="90%">
				<tr>
					<td><div id="GroupTableDiv"><?=$CopyGroupTable?></div></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
-->
</form>
<br><br>


<script>

function refreshGroupList()
{
	var CopyFrom = $("select#CopyFrom").val();
	var CopyTo = $("select#CopyTo").val();
	
	$.post(
		"ajax_task.php",
		{
			Task:"refreshGroupList",
			CopyFrom:CopyFrom,
			CopyTo:CopyTo
		},
		function(ReturnData)
		{
			$("div#GroupTableDiv").html(ReturnData);
		}
	);
}

function CheckAll(CheckBoxType)
{
	var checked = $("input#CheckAll"+CheckBoxType).attr("checked");
	$("input."+CheckBoxType).attr("checked",checked);
}

function UpdateMemberCheckBox(GroupID)
{
	if(GroupID)
	{
		if($("input#Group"+GroupID).attr("checked"))
			$("input#Member"+GroupID).attr("disabled","")
		else
			$("input#Member"+GroupID).attr("disabled","disabled")
	}
	else
	{
		if($("input#CheckAllGroup").attr("checked"))
			$("input.Member").attr("disabled","")
		else
			$("input.Member").attr("disabled","disabled")
	}
}

function CheckForm()
{
	var CopyFrom = $("select#CopyFrom").val();
	var CopyTo = $("select#CopyTo").val();
	if(CopyFrom == CopyTo)
	{
		alert("<?=$Lang['Group']['warnSelectDifferentYear']?>");
		return false;
	}
	
	if($("input.Group:checked").length == 0)
	{
		alert("<?=$Lang['Group']['warnSelectGroup']?>");
		return false;
	}
	
	return true;
}

</script>
<?
$linterface->LAYOUT_STOP();
?>
