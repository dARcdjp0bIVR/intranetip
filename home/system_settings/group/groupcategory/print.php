<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$avgSourceAry = $_POST['exportAwardAry'];
$numRow = count($avgSourceAry);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $avgSourceAry[$i][0];
    $dataAry[$i][$_col++] = $avgSourceAry[$i][1];
}
//debug_pr($dataAry);
    //$htmlAry['resultTable'] = $libenroll_report->getStudentWithoutEnrolmentReportResultTableHtml($dataAry);
$resultTable = "";
$resultTable .= "<table class='common_table_list_v30 view_table_list_v30'>";
$resultTable .= "<tr>";
$resultTable .= "<th style=\"text-align:center;\">Award Name(Eng)</th>";
$resultTable .= "<th style=\"text-align:center;\">Award Name(Chi)</th>";
$resultTable .= "</tr>";
for ($j=0; $j<$numRow; $j++){
    $resultTable .= "<tr>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][0]. "</td>";
    $resultTable .= "<td style=\"text-align:center;\">". $dataAry[$j][1]. "</td>";
    $resultTable .= "</tr>";
}
$resultTable .= "</table>";
?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<? echo ($resultTable)?>
<?
intranet_closedb();
?>