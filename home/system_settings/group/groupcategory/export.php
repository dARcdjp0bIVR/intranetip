<?php
# using:
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lexport = new libexporttext();

//debug_pr($_POST['exportGroupRoleKeyword']);

$groupInfoSourceKeyword = $_POST['exportGroupKeyword'];
$groupInfoSourceOrder = $_POST['exportGroupOrder'];
$groupInfoSourceField = $_POST['exportGroupField'];
$type = "";
//debug_pr($groupInfoSourceKeyword);
//debug_pr("groupInfoSourceOrder = " . $groupInfoSourceOrder);
//debug_pr("groupInfoSourceField = " . $groupInfoSourceField);

if($groupInfoSourceOrder == '0'){
    switch($groupInfoSourceField){
        case '0':
            $type = "CategoryName";
            break;
        case '1':
            $type = "a.DateModified";
            break;
    }
    $sql  = "   SELECT
                        a.CategoryName,
                        a.DateModified
                FROM
                        INTRANET_GROUP_CATEGORY as a
						LEFT JOIN INTRANET_GROUP as b ON a.GroupCategoryID = b.RecordType
				WHERE
						a.CategoryName LIKE" .  "'%" . $_POST['exportGroupKeyword']. "%'" ."
				GROUP BY
						a.CategoryName
                ORDER BY " . $type . " DESC
             ";
}
else{
    switch($groupInfoSourceField){
        case '0':
            $type = "CategoryName";
            break;
        case '1':
            $type = "a.DateModified";
            break;
    }
    $sql  = "   SELECT
                        a.CategoryName,
                        a.DateModified
                FROM
                        INTRANET_GROUP_CATEGORY as a
						LEFT JOIN INTRANET_GROUP as b ON a.GroupCategoryID = b.RecordType
				WHERE
						a.CategoryName LIKE" . "'%" . $_POST['exportGroupKeyword']. "%'" ."
				GROUP BY
						a.CategoryName
                ORDER BY " . $type . " ASC
                ";
}

//debug_pr($sql);
$rs = $ldb->returnResultSet($sql);
//debug_pr($sql);
//debug_pr($rs);
$headerAry = array();
$headerAry[] = "Category";
$headerAry[] = "Last Modified";
$numRow = count($rs);
$dataAry = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry[$i][$_col++] = $rs[$i]['CategoryName'];
    $dataAry[$i][$_col++] = $rs[$i]['DateModified'];
//debug_pr($dataAry);
}
$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "Group Category.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>