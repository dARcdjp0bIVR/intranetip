<?php

############# Change Log [Start] ################
# Date:	2019-04-17 Pun [ip.2.5.10.5.1]
#		- Added SQL Injection protection
#
# Date:	2015-10-19 Kenneth
#		- Edit system msg
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$li = new libdb();

$GroupCategoryID = IntegerSafe($GroupCategoryID);
$DefaultRole = intranet_htmlspecialchars(trim($DefaultRole));
$CategoryName = intranet_htmlspecialchars(trim($CategoryName));

$fieldname .= "CategoryName = '$CategoryName', ";
$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_GROUP_CATEGORY SET $fieldname WHERE GroupCategoryID = $GroupCategoryID";

$li->db_db_query($sql);

$li->UpdateRole_UserGroup();

/*
$lorg = new liborganization();
if ($display == 1)
{
    $lorg->setCategoryHidden($GroupCategoryID);
}
else if ($display == 3)
{
     $lorg->setCategoryDisplay($GroupCategoryID);
}
*/




if($DefaultRole)
{

	#update old default role to normal
	$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 0 WHERE RecordStatus = 1 AND RecordType = '$GroupCategoryID'";
	$li->db_db_query($sql);

	$sql = "SELECT RoleID FROM INTRANET_ROLE WHERE Title = '$DefaultRole' AND RecordType = '$GroupCategoryID'";
	$result = $li->returnVector($sql);
	
	if(count($result)>0)
	{
		# update existing role to default
		$sql = "UPDATE INTRANET_ROLE SET RecordStatus = 1 WHERE RoleID ='". $result[0] ."'";
		$li->db_db_query($sql);
	}
	else
	{
		#Add new role, set to default 
		$fieldname = "Title,Description,RecordType,RecordStatus,DateInput,DateModified";
		$fieldvalue = "'$DefaultRole','$DefaultRole','$GroupCategoryID',1,now(),now()";
		$sql = "INSERT INTO INTRANET_ROLE ($fieldname) VALUES ($fieldvalue)";
		$li->db_db_query($sql);
	}
}

intranet_closedb();
header("Location: index.php?filter=$RecordType&xmsg=update");
?>