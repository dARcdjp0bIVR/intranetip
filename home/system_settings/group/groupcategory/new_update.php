<?php
############# Change Log [Start] ################
# Date: 2015-12-14 Omas
#		- assign groupCatID (removed auto-increment for groupCatID)
#		- improved if fail return fail msg to new.php
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();

$CategoryName = intranet_htmlspecialchars(trim($CategoryName));
$DefaultRole = intranet_htmlspecialchars(trim($DefaultRole));

// get the next ID
$sql = "select (MAX(GroupCategoryID)+1) from INTRANET_GROUP_CATEGORY";
$tempResult = $li->returnVector($sql,1);
$newCatID = $tempResult[0];

$fieldname = "GroupCategoryID,CategoryName,DateInput,DateModified";
$fieldvalue = "'$newCatID','$CategoryName',now(),now()";

$sql = "INSERT INTO INTRANET_GROUP_CATEGORY ($fieldname) VALUES ($fieldvalue)";
$result = $li->db_db_query($sql);
//$catID = $li->db_insert_id();

if($result == true){
	$fieldname = "Title,Description,RecordType,RecordStatus,DateInput,DateModified";
	//$fieldvalue = "'$DefaultRole','$DefaultRole','$catID',1,now(),now()";
	$fieldvalue = "'$DefaultRole','$DefaultRole','$newCatID',1,now(),now()";
	$sql = "INSERT INTO INTRANET_ROLE ($fieldname) VALUES ($fieldvalue)";
	$li->db_db_query($sql);
	intranet_closedb();
	header("Location: index.php?xmsg=add");
}
else{
	intranet_closedb();
	header("Location: index.php?xmsg=add_failed");
}
?>