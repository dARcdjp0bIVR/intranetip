<?php
# using: 

/* Change Log

2017-06-07 (Icarus): Modified the HTML code, added		<?= $linterface->RequiredSymbol() ?>
2017-06-06 (Icarus): Modified the HTML code, change the CSS of the form to reach the UI consistency to the sample page. 
						The original code has been commented.		

 */

############# Change Log [Start] ################
#   2019-04-17 Pun [ip.2.5.10.5.1]
#   - Added XSS protection
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage	= "Group";
$CurrentPageArr['Group']=1;

$lgroup = new libgroup();
$linterface 	= new interface_html();


$GroupCategoryID = (is_array($GroupCategoryID)? $GroupCategoryID[0]:$GroupCategoryID);
$GroupCategoryID = IntegerSafe($GroupCategoryID);
$lg = new libgroupcategory($GroupCategoryID);

$displayOption = "<SELECT name=display>
                   <OPTION value=1>$i_OrganizationPage_NotDisplay</OPTION>
                   <OPTION value=2 SELECTED>$i_OrganizationPage_Unchange</OPTION>
                   <OPTION value=3>$i_OrganizationPage_DisplayAll</OPTION>
                   </SELECT>";

# retrieve default role
list($DefaultRoleID, $DefaultRole) = $lg->returnGroupCategoryDefaultRole($GroupCategoryID);
                   
### Title ###
$PAGE_NAVIGATION[] = array($i_GroupCategorySettings, "index.php");
$PAGE_NAVIGATION[] = array($button_edit, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../");
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('groupCategory');
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();   

?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.CategoryName, "<?php echo $i_alert_pleasefillin.$i_GroupCategoryName; ?>.")) return false;
     if(!check_text(obj.DefaultRole, "<?php echo $i_alert_pleasefillin.$i_GroupCategoryNewDefaultRole; ?>.")) return false;
}
</script>

<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
<?=$linterface->GET_SYS_MSG($xmsg);?>
<table class="form_table_v30" style="padding:25px 0px;">
	<tr>
		<td class="field_title"><?= $linterface->RequiredSymbol() ?><?=$i_GroupCategoryName?></td>
		<td><input class=text type=text name=CategoryName size=30 maxlength=100 value='<?=$lg->CategoryName?>' autofocus></td>
	</tr>
	<? if ($GroupCategoryID!=0) { ?>
	<tr>
		<td class="field_title"><?= $linterface->RequiredSymbol() ?><?=$i_GroupCategoryNewDefaultRole?></td>
		<td><input class=text type=text name=DefaultRole value="<?=$DefaultRole?>" size=30 maxlength=100 ></td>
	</tr>
	<? } ?>
</table>
<br style="clear:both;" />
<span class="tabletextremark"><?=$i_general_required_field?></span>
<p class="spacer"></p>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<p class="spacer"></p>
</div>



<!--	THE ORIGINAL CODE
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
				<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_GroupCategoryName ?> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext"><input class=text type=text name=CategoryName size=30 maxlength=100 value='<?=$lg->CategoryName?>'></td>
						</tr>
						<? if ($GroupCategoryID!=0) { ?>
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_GroupCategoryNewDefaultRole?> <span class='tabletextrequire'>*</span></td>
							<td class="tabletext"><input class=text type=text name=DefaultRole value="<?=$DefaultRole?>" size=30 maxlength=100 ></td>
						</tr>
						<? } ?>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td>
			</tr>
		</table>                                
	</td>
</tr>
</table>  
-->

   
<input type='hidden' name='GroupCategoryID' value='<?=$GroupCategoryID?>' >
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>