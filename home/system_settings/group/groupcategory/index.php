<?php

#using:

############# Change Log [Start] ################
#   Date:   2018-06-22 Vito
#           Add export function
#
#	Date:	2015-10-19 Kenneth Yau
#			add SearchBox Layout
#			encode search keyword's html characters by intranet_htmlspecialchars()
#			decode shows search keyword's html characters by intranet_undo_htmlspecialchars()
#			
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$CurrentPage	= "Group";
$CurrentPageArr['Group']=1;
$lgroup = new libgroup();
$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE SQL
$ldb = new libdb();
$keyword = stripslashes($keyword);
$keyword = intranet_htmlspecialchars(trim($keyword));
$keyword = $ldb->Get_Safe_Sql_Like_Query($keyword);

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        default: $field = 0; break;
}
$order = ($order == "") ? 1 : $order;
$sql  = "SELECT 
                        CONCAT('<a class=tablelink href=\"edit.php?GroupCategoryID[]=', GroupCategoryID, '\">', CategoryName, '</a>'),
                        a.DateModified,
                        IF(a.RecordType='1','<img src=$image_path/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=GroupCategoryID[] ', IF(b.RecordType IS NOT NULL,CONCAT('groupcatname=\'',CategoryName,'\''),''),' value=', GroupCategoryID ,'>'))
                FROM
                        INTRANET_GROUP_CATEGORY as a
						LEFT JOIN INTRANET_GROUP as b ON a.GroupCategoryID = b.RecordType
				WHERE
						a.CategoryName LIKE '%$keyword%'
				GROUP BY
						CategoryName
                ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CategoryName","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = 2;
// debug_pr($li->wrap_array);
### search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', stripslashes(stripslashes(intranet_undo_htmlspecialchars($keyword))));


// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=60% class=tableTitle>".$li->column(0, $i_GroupCategoryName)."</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column(1, $i_GroupCategoryLastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("GroupCategoryID[]")."</td>\n";

// TABLE FUNCTION BAR
/*
$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'GroupCategoryID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'GroupCategoryID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
*/
//$AddBtn 	= "<a href=\"javascript:checkNew('new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$AddBtn 	= "<a href=\"javascript:checkNew('new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" />" . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkHasGroup(); \" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'GroupCategoryID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$exportBth  = "<a href=\"javascript:js_Go_Export()\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_export . "</a>";
$defaultGroup = "<img src=$image_path/red_checkbox.gif vspace=3 hspace=4 border=0> ".$Lang['Group']['DefaultGroup'];

### Title ###
//$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($i_GroupCategorySettings, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"../");
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"../role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('groupCategory');
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','group');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

###	system message - show only when $xmsg is not null
$x = '';
if($xmsg != ''){
	$x =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG($xmsg).'</td></tr>';
}
$htmlAry['systemMsgTr'] = $x;

$linterface->LAYOUT_START();   

?>
<script>
function checkHasGroup()
{
	var objs = $("[name=GroupCategoryID[]]").get();
	var hasGroup = false;
	for(var x = 0 ; x< objs.length; x++)
	{
		
		//in sql, if groupcat contain groups, groupcatname will be selected
		if($(objs[x]).attr("groupcatname")!=""&&$(objs[x]).attr("groupcatname")!=null&&objs[x].checked==true)
		{
			hasGroup=true;
			//highlight row
			objs[x].parentNode.parentNode.style.backgroundColor="#ffdede";
			objs[x].checked=false;
		}
	}
	if (hasGroup)
	{
		alert('<?=$Lang['Group']['WarnHasGroup']?>')
	}
	else
		checkRemove(document.form1,'GroupCategoryID[]','remove.php');
}

function js_Go_Export(){
	$('form#form2').attr('target', '_self').attr('action', 'export.php').submit();
	//window.location("export.php");
}
function js_Go_Print(){
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}

</script>
<form name="form1" method="post">
<!--<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_GroupCategorySettings, '') ?>
<?= displayTag("head_group_category_$intranet_session_language.gif", $msg) ?>
-->
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<!--<tr>
	<td class="tabletext">
		<table width="97%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="100%" align="right" class="tabletext"><?=$searchTag?></td>
			</tr>
		</table>
	</td>
</tr>-->
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left" class="tabletext">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<?= $htmlAry['systemMsgTr'] ?>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="2" >
						<tr>
							<td>
								<?=$AddBtn?>
								<?php echo ($exportBth)?>
<!-- 								<a class="export" href="http://192.168.0.146:31002/home/system_settings/group/groupcategory/export.php" ><?php echo ($lang['Export'])?></a>
									<a class="print " href="javascript:js_Go_Print();" >列印</a> -->						
							</td>
						</tr>
					</table>
				</td>
				<td><?=$htmlAry['searchBox']?>
				</td>
			</tr>
			<tr class="table-action-bar">
				<td><?=$filterbar?></td>
				<td align="right" valign="bottom" colspan="2">
					<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
						<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td nowrap><?=$editBtn?></td>
								<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
								<td nowrap><?=$delBtn?></td>
							</tr>
							</table>
						</td>
						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<?= $li->display();?>
				</td>
			</tr>
			<tr>
				<td>
					<?=$defaultGroup?>
				</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<?php // debug_pr($li->display())?>
<!--
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
-->

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
 <?php 
//     debug_pr($li->order);
//     debug_pr($li->field);
?>
<form id="form2" name="form2" method="post">
<input type=hidden id=exportGroupKeywordHidden name=exportGroupKeyword value="<?php echo $keyword?>">
<input type=hidden id=exportGroupOrderHidden name=exportGroupOrder value="<?php echo $li->order?>">
<input type=hidden id=exportGroupFieldHidden name=exportGroupField value="<?php echo $li->field?>">
</form>

<?php
$linterface->LAYOUT_STOP();   

intranet_closedb();
//include_once("../../templates/adminfooter.php");
?>