<?php
// using

############# Change Log [Start] ################
#
#   2019-08-02  Bill
#   - update wording - from Type to Category
#
#   2019-04-17 Pun [ip.2.5.10.5.1]
#   - Added XSS protection
#
#	2010-12-21	YatWoon
#	- add option for group "allow delete others announcement"
#
#	2010-12-13	YatWoon
#	- IP25 UI standard
#	- Add "TitleChinese"
#
# - 2010-07-19 Marcus
#	remove ECA form category selection in client use eEnrolment
#
# - 2010-06-15 YatWoon
#	remove flag checking $special_announce_public_allowed, no need for this checking and assume this feature is as a general
#
# Date: 2009-01-04 Max (200912311437)
# - Hide the fields and row of "Allow using Group Tools selected below"
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libalbum.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$GroupID[0] = IntegerSafe($GroupID[0]);
$CurrentPageArr['Group'] = 1;

### Create Obj ###
$linterface = new interface_html();
$fcm = new form_class_manage();
$la = new libalbum();
$li = new libgroup($GroupID[0]);
$lgc = new libgroupcategory($li->RecordType);
$lorg = new liborganization();
$checked = ($li->AnnounceAllowed==1? "CHECKED":"");
$allToolsChecked = ($li->isAccessAllTools()? "CHECKED":"");
$availableTools = $li->getSelectCurrentAvailableFunctions();
$hideChked = ($lorg->isGroupHidden($GroupID[0])? "CHECKED":"");

$sql = "SELECT AcademicYearID FROM INTRANET_GROUP WHERE GroupID = '".$GroupID[0]."'";
$AcademicYearID = $li->returnVector($sql);

if(!($li->RecordStatus==1 || $li->RecordStatus==3))
{
	$SchoolYearList = $fcm->Get_Academic_Year_List('',1,'',1);
	$yearFilter .= '				<select name="SchoolYear" id="SchoolYear">';
	$yearFilter .= '					<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'].'</option>';
	for ($i=0; $i< sizeof($SchoolYearList); $i++) {
		$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
		
		unset($Selected);
		$Selected = ($AcademicYearID[0]== $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
		$yearFilter .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
	}
	$yearFilter .= '				</select>';
}

# GroupCatSelect
$exceptArr = array(0);
if($plugin['eEnrollment'])
	$exceptArr[] = 5; 
$GroupCatSelect = $lgc->returnSelectCategory("name=RecordType",true,0,$li->RecordType,$exceptArr);

# check link to class
if($li->RecordType == 3)
{ 
	$LinkedClass = $li->returnLinkedClass();
	$IsClassLinkedGroup = !empty($LinkedClass);
}

### Title ###
$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];

$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "index.php");
$PAGE_NAVIGATION[] = array($button_edit, "");

$title = $Lang['Group']['GroupMgmtCtr']; 
//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $li->getTopTabInfoAry('group');
$linterface->LAYOUT_START($xmsg); 


?>
<script language="javascript">
function reset_innerHtml()
{
	document.getElementById('div_SchoolYear_err_msg').innerHTML = "";
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_TitleChinese_err_msg').innerHTML = "";
 	document.getElementById('div_RecordType_err_msg').innerHTML = "";
 	document.getElementById('div_Quota_err_msg').innerHTML = "";
}

function click_reset()
{
	reset_innerHtml();
	document.form1.reset();
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	<? if(!($li->RecordStatus==1 || $li->RecordStatus==3)) {?>
	if(!check_text_30(obj.SchoolYear, "<?php echo $i_alert_pleaseselect.$i_EventAcademicYear; ?>.", "div_SchoolYear_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "SchoolYear";
	}
	<? } ?>
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['Group']['NameEn']; ?>.", "div_Title_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	}
	
	if(!check_text_30(obj.TitleChinese, "<?php echo $i_alert_pleasefillin.$Lang['Group']['NameCh']; ?>.", "div_TitleChinese_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "TitleChinese";
	}
	
	if(!check_positive_int_30(obj.Quota, "<?php echo $i_GroupQuotaIsInt; ?>.", 0,0, "div_Quota_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Quota";
	}
	
	if(!check_text_30(obj.RecordType, "<?php echo $i_alert_pleaseselect.$i_GroupRecordType; ?>.", "div_RecordType_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "RecordType";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}
function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}
</script>

<form name="form1" method="post" action="edit_update.php" onSubmit="return checkform(this);">

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<div class="form_content">
<table class="form_table_v30">

<? if(!($li->RecordStatus==1 || $li->RecordStatus==3)) {?>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?=$i_EventAcademicYear; ?></td>
	<td><?=$yearFilter?><br><span id='div_SchoolYear_err_msg'></span></td>
</tr>
<? } ?>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?=$Lang['Group']['NameEn']; ?></td>
	<td><input name="Title" type="text" id="Title" class="textboxtext" value="<?=$li->Title; ?>"/><br><span id='div_Title_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?=$Lang['Group']['NameCh']; ?></td>
	<td><input name="TitleChinese" type="text" id="TitleChinese" class="textboxtext" value="<?=$li->TitleChinese; ?>"/><br><span id='div_TitleChinese_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['GroupDescription']; ?></td>
	<td><?=$linterface->GET_TEXTAREA("Description", $li->Description);?></td>
</tr>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?=$Lang['Group']['StorageQuota']; ?></td>
	<td><input class=text type=text name=Quota size=5 maxlength=5 value="<?=$li->StorageQuota?>"><br><span id='div_Quota_err_msg'></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Group']['CanUsePublicAnnouncementEvents']; ?></td>
	<td><input TYPE=checkbox NAME=AnnounceAllowed VALUE=1 <?=$checked?>></td>
</tr>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?= $Lang['Group']['Category']; //$Lang['Group']['Type']; ?></td>
	<td><?=(($li->RecordType==0 || $IsClassLinkedGroup || ($plugin['eEnrollment']&&$li->RecordType==5)) ? $lgc->CategoryName." <input type=hidden name=RecordType value='".$li->RecordType."'>" : $GroupCatSelect ); ?><br><span id='div_RecordType_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['DisplayInCommunity']; ?></td>
	<td><input TYPE=checkbox NAME="DisplayInCommunity" VALUE=1 <?=$li->DisplayInCommunity==1?"CHECKED":""?>></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['AllowDeleteOthersAnnouncement']; ?></td>
	<td><input TYPE=checkbox NAME="AllowDeleteOthersAnnouncement" VALUE=1  <?=$li->AllowDeleteOthersAnnouncement==1?"CHECKED":""?>></td>
</tr>
</table>

<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?> 
<?= $linterface->GET_ACTION_BTN($button_reset, "button", "click_reset();") ?> 
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?> 
<p class="spacer"></p>
</div>
</div>


<input type=hidden name=GroupID value="<?php echo $li->GroupID; ?>">

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
