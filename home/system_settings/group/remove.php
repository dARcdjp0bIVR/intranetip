<?php
#using : 

############# Change Log [Start] ################
#   Date:   2018-09-13 Chris
#       - added in Gsuite SSO
#
#	Date:	2010-12-13	YatWoon
#			- update return system message display to IP25 standard
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
//$list = implode(",",$GroupID);
$iCal = new icalendar();
#$sql = "DELETE FROM INTRANET_GROUPTIMETABLE WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
#$sql = "DELETE FROM INTRANET_GROUPPOLLING WHERE GroupID IN (".implode(",", $GroupID).")";
#$li->db_db_query($sql);
$sql = "select CalID from INTRANET_GROUP WHERE GroupID IN (".implode(",", $GroupID).")";
$calID = $li->returnVector($sql);

#Added by Marcus 20090916, Also Delete Record in INTRANET_HOUSE if the group is house
/*$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE GroupID IN (".implode(",", $GroupID).") AND RecordType = 4";
$HouseGroupAry = $li->returnVector($sql);
if(!empty($HouseGroupAry))
{
	$HouseGroupStr = implode(",",$HouseGroupAry);
	$sql = "DELETE FROM INTRANET_HOUSE WHERE GroupID IN ($HouseGroupStr)";
	$li->db_db_query($sql);
}*/


# Get ClassGroupList
$sql = "SELECT GroupID FROM YEAR_CLASS WHERE GroupID IN (".implode(",", $GroupID).")";
$ClassGroupIDArr = $li->returnVector($sql);

if(count($ClassGroupIDArr)>0)
{
	$GroupID = array_diff($GroupID,$ClassGroupIDArr);
	$msg = "CannotDeleteClassGroup";
}
else
    $msg = "DeleteSuccess";

$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID IN (" . implode(",", $GroupID) . ")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_GROUP WHERE GroupID IN (" . implode(",", $GroupID) . ")";
$isDel = $li->db_db_query($sql);

$iCal->removeCalendar($calID);

if ($isDel == true) {
    // #Google SSO
    if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
        include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
        include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
        include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
        $libGoogleSSO = new libGoogleSSO();
        $libGoogleSSO->removeGroupFromEClassToGoogle($GroupID);
    }
}
# Set Class's GroupID to NULL
#$sql = "UPDATE INTRANET_CLASS SET GroupID = NULL WHERE GroupID IN ($list)";
#$li->db_db_query($sql);
$lclass = new libclass();
$lclass->fixClassGroupRelation();


intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&xmsg=$msg&SchoolYear=$SchoolYear");
?>