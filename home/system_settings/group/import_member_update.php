<?php
# using: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

@SET_TIME_LIMIT(216000);

intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;
$linterface 	= new interface_html();

//### download csv
//$csvFile = "<a class='tablelink' href='". GET_CSV("group_import.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";


$linterface 	= new interface_html();
$lgroup = new libgroup();
$limport = new libimporttext();
$lo = new libfilesystem();
$lu = new libuser();


### CSV Checking
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: group_member_import.php?xmsg=WrongFileFormat");
	exit();
}
$file_format = array("Class Name","Class Number","User Login","WebSAMSRegNo","Group Name");
$flag_arr = array(1,1,1,1,0,1);
$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,'','',$file_format,$flag_arr);

$CsvHeader = array_shift($data);
$SizeOfCsvHeader = sizeof($CsvHeader);
$CsvHeaderWrong = false;
for($i=0; $i<$SizeOfCsvHeader; $i++)
{
	if($CsvHeader[$i] != $file_format[$i])
	{
		$CsvHeaderWrong = true;
		break;
	}
}

if($CsvHeaderWrong || count($data)==0)
{
	$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
	intranet_closedb();
	header("location: group_member_import.php?xmsg=".$ReturnMsgKey);
	exit();
}

###### Prepare data for checking
### Get Current Academic Year
$CurrentAcademicYearID = Get_Current_Academic_Year_ID(); 

### Get Identity Groups
$IdentityGroupList = $lgroup->returnIdentityGroupsID();

### Get Group Users List, use to check duplicated user
$ExistingUser = $lgroup->returnGroupUser();

### Get ECA Groups
if($plugin['eEnrollment'] == true)
{
	$ECAGroups = $lgroup->returnGroupsByCategory(5, $SchoolYear);
	$ECAGroupsNameArr = Get_Array_By_Key($ECAGroups,"TitleEn");
}
	
$GroupList = $lgroup->returnAllGroupIDAndName();
$GroupNameList = Array();
foreach((array)$GroupList as $GroupData)
{
	list($GroupID,$GroupName,$AcademicYearID)= $GroupData;
	$GroupName = intranet_undo_htmlspecialchars($GroupName);
	
	$GroupNameList[$GroupID] = $GroupName;
	$GroupIDList[$AcademicYearID][$GroupName] = $GroupID;
	$GroupAcademicYear[$GroupName][] = $AcademicYearID; 
	if(in_array($GroupID,(array)$IdentityGroupList))
		$IdentityGroupNameArr[$GroupID] = $GroupName;
}

$ErrLog = Array(); // To record all err of each record

foreach ($data as $key => $rec)
{
	# reset css
	$css=array();
	$warnEmpty = array();
	$user_id='';
			
	list($ClassName,$ClassNumber,$UserLogin,$WebSamsRegNo,$GroupName)=$rec;

		if(empty($ClassName)&& empty($ClassNumber)&& empty($WebSamsRegNo)&& empty($UserLogin))
		{
			$ErrLog[$key][] = $Lang['Group']['EmptyUserInfo'];	
			$css["ClassName"] = "red"; 
			$css["ClassNumber"] = "red"; 
			$css["UserLogin"] = "red"; 
			$css["WebSamsRegNo"] = "red"; 
			$warnEmpty["UserInfo"] = true;		
		}
		else if(!empty($ClassName) && !empty($ClassNumber))
		{	
			// Fixed: problem when current year =/= selected school year
			$user_id = $lu->returnUserID($ClassName, $ClassNumber, $RecordType=2, $RecordStatus='-1', $SchoolYear);
		}
		else if(!empty($UserLogin))
		{
			$user_id = $lu->getUserIDByUserLogin($UserLogin);
		}
		else if(!empty($WebSamsRegNo)) 
		{
			$user_id = $lu->getUserIDByWebSamsRegNo($WebSamsRegNo);
		}

		if(empty($user_id))
		{
			$ErrLog[$key][] = $Lang['Group']['UserNotExist'];	
			$css["ClassName"] = "red"; 
			$css["ClassNumber"] = "red"; 
			$css["UserLogin"] = "red";
			$css["WebSamsRegNo"] = "red";  
		}
		
	#Check User
//	if(!empty($ClassName) && !empty($ClassNumber))
//		$user_id = $lu->returnUserID($ClassName,$ClassNumber);
//		
//	if(empty($user_id))
//	{
//		if(empty($ClassName)&& empty($ClassNumber)&& empty($UserLogin))
//		{
//			$ErrLog[$key][] = $Lang['Group']['EmptyUserInfo'];	
//			$css["ClassName"] = "red"; 
//			$css["ClassNumber"] = "red"; 
//			$css["UserLogin"] = "red"; 
//			$warnEmpty["UserInfo"] = true;		
//		}
//		else
//		{
//			$user_id = $lu->getUserIDByUserLogin($UserLogin);
//			if(empty($user_id))
//			{
//				$ErrLog[$key][] = $Lang['Group']['UserNotExist'];	
//				$css["ClassName"] = "red"; 
//				$css["ClassNumber"] = "red"; 
//				$css["UserLogin"] = "red"; 
//			}
//		}		
//	}
	
	if(!empty($user_id))
	{
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$ay = new academic_year($SchoolYear);
		if(!$ay->Check_User_Existence_In_Academic_Year($user_id))
		{
			$ErrLog[$key][] = $Lang['Group']['UserNotInSelectedSchoolYear'];	
			$css["ClassName"] = !empty($ClassName)?"red":""; 
			$css["ClassNumber"] = !empty($ClassNumber)?"red":"";
			$css["UserLogin"] = !empty($UserLogin)?"red":"";
			$css["WebSamsRegNo"] = !empty($WebSamsRegNo)?"red":"";  
		}
	}
	#Check Group
	
	if(trim($GroupName)=='')
	{
		$ErrLog[$key][] = $Lang['Group']['EmptyGroupName'];
		$css["GroupName"] = "red"; 
		$warnEmpty["GroupName"] = true;
	}
	else if(!in_array($GroupName,(array)$GroupNameList))
	{
		$ErrLog[$key][] = $Lang['Group']['GroupNotExist'];
		$css["GroupName"] = "red"; 
	}
	else if(in_array($GroupName,(array)$IdentityGroupNameArr))
	{
		$ErrLog[$key][] = $Lang['Group']['IsIdentityGroup'];
		$css["GroupName"] = "red"; 
	}
	else if(in_array($GroupName,(array)$ECAGroupsNameArr))
	{
		$ErrLog[$key][] = $Lang['Group']['IsECAGroup'];
		$css["GroupName"] = "red"; 
	}
	else if(!in_array($SchoolYear,(array)$GroupAcademicYear[$GroupName]))
	{
		$ErrLog[$key][] = $Lang['Group']['GroupNotInCurrentYear'];
		$css["GroupName"] = "red"; 
	}
	
	#Build Table Content
	if($ErrLog[$key])
	{
		$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
	
		$Confirmtable .= "	<tr $rowcss>";
		$Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
		$Confirmtable .= "		<td class='tabletext ".$css["ClassName"]."'>".($warnEmpty["UserInfo"]?"***":$ClassName)."</td>";
		$Confirmtable .= "		<td class='tabletext ".$css["ClassNumber"]."'>".($warnEmpty["UserInfo"]?"***":$ClassNumber)."</td>";
		$Confirmtable .= "		<td class='tabletext ".$css["UserLogin"]."'>".($warnEmpty["UserInfo"]?"***":$UserLogin)."</td>";
		$Confirmtable .= "		<td class='tabletext ".$css["WebSamsRegNo"]."'>".($warnEmpty["WebSamsRegNo"]?"***":$WebSamsRegNo)."</td>";
		$Confirmtable .= "		<td class='tabletext ".$css["GroupName"]."'>".($warnEmpty["GroupName"]?"***":$GroupName)."</td>";
		
		
		$ErrMsg = count($ErrLog[$key])>1?implode("<br>",$ErrLog[$key]):$ErrLog[$key][0];
		$Confirmtable .= "		<td class='red'>".$Lang['General']['Error'].":<br>$ErrMsg</td>"; 
		//}
		//else
		//{
		//	$Confirmtable .= "		<td>".$Lang['General']['NoError']."</td>";
		//}
		$Confirmtable .= "	</tr>";
		$errctr++;
	}			
		
	$postDataDetail[$key] = array($user_id,$GroupIDList[$SchoolYear][$GroupName]); 
}
### Gen Btn ###
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn .= empty($ErrLog)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;

### Post Import Value to next page ###
$postData = "<input type='hidden' value='".serialize($postDataDetail)."' name='postData'>";
$postData .= "<input type='hidden' value='".$SchoolYear."' name='SchoolYear'>";  

### Error Msg
//$xmsg = (!empty($ErrLog))?$Lang['General']['warnNoRecordInserted']:"";

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($li->Title, "index.php?GroupID=$GroupID&filter=$filter");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();   
?>
<br>
<form name="frm1" method="POST" action="import_member.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($ErrLog)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($ErrLog)?"red":""?>'><?=count($ErrLog)?></td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($ErrLog)){?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink'><?=$i_ClassName?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_ClassNumber?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_UserLogin?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_WebSAMS_Registration_No?></td>						
				<td class='tablebluetop tabletopnolink'><?=$i_GroupName?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
<?=$postData?>
</form>
<br><br>
<?
$linterface->LAYOUT_STOP();
?>
