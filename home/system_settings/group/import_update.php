<?php
# using: 
/************************************************
 * 	modification log
 *	2014-11-04	Omas
 *	- Improved: will warn if GroupName is duplicated (can continue to import)  	
 *
 *	2012-02-23 YatWoon
 *	- Fixed: missing to cater if the data include ' symblo
 *
 *	2010-12-13 YatWoon
 *	- add "Group Name (Chinese)"
 *
 * 		20100126 Marcus:
 * 			- add integer checking to Storage Quota  
 * **********************************************/


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Group";
$CurrentPageArr['Group'] = 1;
$linterface 	= new interface_html();

### download csv
$csvFile = "<a class='tablelink' href='". GET_CSV("group_import.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";


$linterface 	= new interface_html();
$lgroup = new libgroup();
$limport = new libimporttext();
$lo = new libfilesystem();

### CSV Checking
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: group_import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$csv_header = array_shift($data);

$file_format = array("Group Name (English)","Group Name (Chinese)","Description","Storage Quota","Use Public Announcements or Events","Type","Display in eCommunity","Allow Group Admin Delete or Edit");
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($csv_header[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	intranet_closedb();
	header("location:  group_import.php?xmsg=import_header_failed");
	exit();
}

if(empty($data))
{
	intranet_closedb();
	header("location: group_import.php?xmsg=import_failed");
	exit();
	
}

###### Prepare data for checking
### Check Group Category
$GroupCatList = $lgroup->returnAllCategoryName();

$ErrLog = Array(); // To record all err of each record

##Prepare data for checking Name duplication
$GroupNameAry = $lgroup->returnGroupName($SchoolYear);
$NumGroup = count($GroupNameAry);


foreach ($data as $key => $rec)
{
	list($GroupName,$GroupNameCh,$Description,$StorageQuota,$UsePublicAnnonuceOrEvent,$GroupCat,$DisplayineCommun,$AllowAdminEditOrDelete)=$rec;
	
	$css=Array();
	if($GroupName == '')
	{
		$ErrLog[$key][] = $Lang['Group']['warnEmptyGroupNameEn'];
		$css["GroupName"] = "red"; 
	}
	
	if($GroupNameCh == '')
	{
		$ErrLog[$key][] = $Lang['Group']['warnEmptyGroupNameCh'];
		$css["GroupNameCh"] = "red"; 
	}
	##Checking Name duplication
	if (!empty($GroupNameCh) && !empty($GroupName)){
		for ($i=0; $i<$NumGroup; $i++){
			if ($GroupNameAry[$i]['TitleChinese'] == $GroupNameCh){
				if (!in_array($Lang['Group']['warnChiNameDuplicate'], (array)$RemindLog[$key])) {
					$RemindLog[$key][] = $Lang['Group']['warnChiNameDuplicate'];
					$css["GroupNameCh"] = "red";
				}
			}
			if ($GroupNameAry[$i]['Title'] == $GroupName){
				if (!in_array($Lang['Group']['warnEngNameDuplicate'], (array)$RemindLog[$key])) {
					$RemindLog[$key][] = $Lang['Group']['warnEngNameDuplicate'];
					$css["GroupName"] = "red";
				}
			}
		}
	}
	if($StorageQuota == '' || $StorageQuota < 0 ||!is_numeric($StorageQuota)||strstr($StorageQuota,"."))
	{
		$ErrLog[$key][] = $Lang['Group']['warnPositiveNum'];
		$css["StorageQuota"] = "red";
	}
		
	if($GroupCat == '' || !in_array($GroupCat,$GroupCatList))
	{
		$ErrLog[$key][] = $Lang['Group']['warnGroupCatNotExist'];
		$css["GroupCat"] = "red";
	}
	
	$UsePublicAnnonuceOrEventToUpper = strtoupper($UsePublicAnnonuceOrEvent);
	if ($UsePublicAnnonuceOrEventToUpper=='Y' || $UsePublicAnnonuceOrEventToUpper=='N') {
		// valid
	}
	elseif($UsePublicAnnonuceOrEventToUpper=='')
	{
		$ErrLog[$key][] = $Lang['Group']['UsePublicAnnonuceOrEventIsEmpty'];
		$css["UsePublicAnnonuceOrEvent"] = "red";
	}
	elseif($UsePublicAnnonuceOrEventToUpper !='Y' && $UsePublicAnnonuceOrEventToUpper !='N'  ){
		$ErrLog[$key][] = $Lang['Group']['UsePublicAnnonuceOrEventIsInvalid'];
		$css["UsePublicAnnonuceOrEvent"] = "red";
	}
	
	$DisplayineCommunToUpper  = strtoupper($DisplayineCommun);
	if($DisplayineCommunToUpper=='Y'||$DisplayineCommunToUpper=='N')
	{
		//valid
	}
	elseif($DisplayineCommunToUpper==''){
		$ErrLog[$key][] = $Lang['Group']['DisplayineCommunIsNull'];
		$css["DisplayineCommun"] = "red";
		
	}elseif($DisplayineCommunToUpper !='Y' && $DisplayineCommunToUpper !='N' ){
		$ErrLog[$key][] = $Lang['Group']['DisplayineCommunIsInValid'];
		$css["DisplayineCommun"] = "red";
	}
	
	
	$AllowAdminEditOrDeleteToUpper  = strtoupper($AllowAdminEditOrDelete);
	
	if($AllowAdminEditOrDeleteToUpper=='Y'||$AllowAdminEditOrDeleteToUpper=='N')
	{
		//valid
	}
	elseif($AllowAdminEditOrDeleteToUpper=='')
	{
		$ErrLog[$key][] = $Lang['Group']['AllowAdminEditOrDeleteIsNull'];
		$css["AllowAdminEditOrDelete"] = "red";
		
	}elseif($AllowAdminEditOrDeleteToUpper !='Y' && $AllowAdminEditOrDeleteToUpper !='N' ){
		$ErrLog[$key][] = $Lang['Group']['AllowAdminEditOrDeleteIsInValid'];
		$css["AllowAdminEditOrDelete"] = "red";
	}
	
		
	#Build Table Content
	$rowcss = " class='".($key%2==0? "tablebluerow2":"tablebluerow1")."' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["GroupName"]."'>".($GroupName?$GroupName:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["GroupNameCh"]."'>".($GroupNameCh?$GroupNameCh:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext'>".($Description?$Description:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["StorageQuota"]."'>".($StorageQuota?$StorageQuota:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["UsePublicAnnonuceOrEvent"]."'>".($UsePublicAnnonuceOrEvent?$UsePublicAnnonuceOrEvent:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["GroupCat"]."'>".($GroupCat?$GroupCat:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["DisplayineCommun"]."'>".($DisplayineCommun?$DisplayineCommun:"***")."</td>";
	$Confirmtable .= "		<td class='tabletext ".$css["AllowAdminEditOrDelete"]."'>".($AllowAdminEditOrDelete?$AllowAdminEditOrDelete:"***")."</td>";
	
	if($ErrLog[$key])
	{
		$ErrMsg = count($ErrLog[$key])>1?implode("<br>",$ErrLog[$key]):$ErrLog[$key][0];
		$Confirmtable .= "		<td class='red'>".$Lang['General']['Error'].":<br>$ErrMsg</td>"; 
	}
	else if($RemindLog[$key]){
		$RemindMsg = count($RemindLog[$key])>1?implode("<br>",$RemindLog[$key]):$RemindLog[$key][0];
		$Confirmtable .= "		<td>".$RemindMsg."</td>";
	}
	else
	{
		$Confirmtable .= "		<td>".$Lang['General']['NoError']."</td>";
	}
	$Confirmtable .= "	</tr>";			
}

### Gen Btn ###
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn .= empty($ErrLog)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;


### Post Import Value to next page ###
$postData = "<input type='hidden' value='". intranet_htmlspecialchars(serialize($data))."' name='postData'>";
$postData .= "<input type='hidden' value='".$SchoolYear."' name='SchoolYear'>";  

### Error Msg
$xmsg = (!empty($ErrLog))?$Lang['General']['warnNoRecordInserted']:"";

### Title ###
$PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "../index.php");
$PAGE_NAVIGATION[] = array($li->Title, "index.php?GroupID=$GroupID&filter=$filter");
$PAGE_NAVIGATION[] = array($button_import, "");

//$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
//$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
//$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
$TAGS_OBJ = $lgroup->getTopTabInfoAry('group');

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
$linterface->LAYOUT_START();   
?>
<form name="frm1" method="POST" action="import.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($ErrLog)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($ErrLog)?"red":""?>'><?=count($ErrLog)?></td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($ErrLog) || !empty($RemindLog) ){?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['NameEn']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['NameCh']?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_GroupDescription?></td>
				
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['StorageQuota']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['CanUsePublicAnnouncementEvents']?></td>
			
				<td class='tablebluetop tabletopnolink'><?=$i_GroupRecordType?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['DisplayInCommunity']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['Group']['AllowDeleteOthersAnnouncement']?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
<?=$postData?>
</form>
<br><br>
<?
$linterface->LAYOUT_STOP();
?>
