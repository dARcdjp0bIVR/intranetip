<?php
//Modifying by:

############ Change Log Start ###############
#
#	Date:	2019-07-17	Bill
#			add auth checking
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();

$GroupNameLang = Get_Lang_Selection("ig.TitleChinese", "ig.Title");
		
# Export default format
$sql = "	SELECT 
				$GroupNameLang, 
				c.Title, 
				b.UserLogin,
				b.EnglishName,
				b.ChineseName, 
				b.ClassName, 
				b.ClassNumber, 
				a.Performance, 
				b.UserEmail
			FROM
				INTRANET_GROUP ig 
				INNER JOIN INTRANET_USERGROUP as a ON a.GroupID = ig.GroupID
				INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID
				LEFT JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
			WHERE 
				ig.RecordType = '".$filter."'
				AND	ig.AcademicYearID = '".$AcademicYearID."'
				AND b.RecordStatus IN (1,0)
			ORDER BY
				$GroupNameLang ASC ";

$li = new libdb();
$members = $li->returnArray($sql,7);

$exportColumn = array("Group","Role","UserLogin","EnglishName","ChineseName","ClassName","ClassNumber","Performance","UserEmail");

// Output the file to user browser
$filename = "eclass-groupUser.csv";

$export_content = $lexport->GET_EXPORT_TXT($members, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>