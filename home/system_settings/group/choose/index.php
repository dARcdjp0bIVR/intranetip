<?php
# modifying by : henry chow

############ Change Log [start] #################
#
# - 2010-05-18
# 	set the form method with "post", fixed: if the url data is too long, error occurs
#
# - 2010-04-16 Marcus
# 	if group is ECA group, exclude users of current sems member only.
#
# - 2009-12-22 [YatWoon]
#	if Gropu category is "Parent", then display class selection filter for user select parent from class
############ Change Log [end] #################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
					
intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['Group']['UserList'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

//$permitted[] = 2;
$permitted = array(1,2,3,4); //accept all type of users

$luser = new libuser();

$li = new libgrouping();
$lo = new libgroup($GroupID);
$lclass = new libclass();

$teaching=0;
if($CatID < 0){
     unset($ChooseGroupID);
     //$ChooseGroupID[0] = 0-$CatID;
     switch($CatID){
	     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
	     case -3 : $ChooseGroupID[0]=3; $teaching=0;break;
	     
	     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
	     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
	     default:$ChooseGroupID[0]=1; $teaching=1;
	 }
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$row = $lo->returnRoleType(1);

$RoleOptions .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$RoleOptions .= "</select>\n";


$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=\"CatID\" onChange=\"checkOptionNone(this.form.elements['ChooseGroupID[]']);this.form.submit()\">\n" : "<select name=\"CatID\" onChange=\"this.form.submit()\"s>\n";
$x1 .= "<option value=\"0\"></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";


$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
//if (in_array(3,$permitted))
$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
    
$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value=$GroupCatID";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if(isset($ChooseGroupID)) {
	if($EnrolGroupID)
		$userCond = " EnrolGroupID = '$EnrolGroupID' ";
		
	$existing_user = $lo->returnGroupUser('',$userCond);
	
	foreach((array)$existing_user as $data)
		$except_users[] = $data['UserID'];  
	
	$AcadmicYearID = $lo->AcademicYearID;
	if($CatID<0)
	{
	    //if($CatID==-3 && $classname)
	    if($CatID==-3)
	    {

	    	$sortByClass = 1;
		    # retrieve student list first
		    $StudentList = $lclass->getClassStudentList($classname);
		    $StudentIDStr = implode(",",$StudentList);
		    $row = $luser->getParent($StudentIDStr, $sortByClass);
		    //debug_pr($row);
// 		    debug_pr(sizeof($row));
	    }
	    else
	    {
			$row = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching);
		}
	}
	else
	{
   		$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
		}
   	
     $x3  = "<select name=ChooseUserID[] size=15 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     {
     	if(!in_array($row[$i][0],(array)$except_users)) {
     		if($CatID==-3) {
     			list($parentID, $parentName, $studentName, $clsName, $clsNo) = $row[$i];
	     		$x3 .= "<option value=".$parentID.">".$clsName.($clsNo!=""?"-".$clsNo:"")." ".$studentName." ".$Lang['General']['s']."(".$parentName.")</option>\n";
     		}
	     	else 
	     		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     	}
     }
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["target"];
     x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     checkOption(obj);
     //par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1&&obj.options[i]!=null){
			parObj.value += (parObj.value==""?"":",")+ x + obj.options[i].value;
          //par.checkOptionAdd(parObj, obj.options[i].text, x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
	 par.document.form1.elements["role"].value=document.form1.elements["RoleID"].value;
	par.document.form1.action="add_user.php";
	par.document.form1.method="post";
	par.document.form1.submit();
	window.close();
	 
     //par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function add_role(){
     Obj = window.document.form1;
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          Obj.elements["role"].value = role_name;

          Obj.action = "../info/add_role.php";
          Obj.submit();
     }
}
</script>

<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan=2>
						<?=$RoleOptions?> <?= $linterface->GET_SMALL_BTN($Lang['Group']['NewRole'] , "button","add_role()") . "<br>"; ?>
					</td>
				</tr>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$Lang['General']['SchoolYear']?></span>
					</td>
					<td ><?=getCurrentAcademicYear();?></td>
				</tr>

				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_category?></span>
					</td>
					<td ><?=$x1?></td>
				</tr>
				<? if($CatID!=0 && $CatID > 0) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_group?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td><?=$x2?></td>
								<td>
									<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				
				<? 
				# 2009-12-22 yatwoon 
				# if Group category is "Parent", then display class filter (requested by UCCKE)
				if($CatID==-3) 
				{ 
					$ClassSelection = $lclass->getSelectClass("name='classname' onChange='this.form.submit();'", $classname, "", $i_general_all_classes);
					?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_UserParentLink_SelectClass?></span>
					</td>
					<td>
						<?=$ClassSelection?>
					</td>
				</tr>
				<? } ?>
				
				<? if(isset($ChooseGroupID)) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
						<span class="tabletext"><?=$i_frontpage_campusmail_select_user?></span>
					</td>
					<td>
						<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
								<td><?=$x3?></td>
								<td>
									<?= $linterface->GET_BTN($button_add, "submit","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
									<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name=role value="">
<input type=hidden name=fieldname value="<?=$fieldname?>">
<input type=hidden name=GroupID value="<?=$GroupID?>">
<input type=hidden name=EnrolGroupID value="<?=$EnrolGroupID?>">

</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>