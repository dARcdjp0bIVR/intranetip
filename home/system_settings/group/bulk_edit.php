<?php
// using: Tommy

############# Change Log [Start] ################
#
#   Date: 2020-04-06 Tommy
#   - file created
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libalbum.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$GroupID = IntegerSafe($GroupID);
if(is_array($GroupID)){
    $group = Implode(", ", $GroupID);
}
$CurrentPageArr['Group'] = 1;

### Create Obj ###
$linterface = new interface_html();
$fcm = new form_class_manage();
$la = new libalbum();
$li = new libgroup();
$lgc = new libgroupcategory();
$lorg = new liborganization();

$groupInfo = $li->returnGroup('', $group);

$chi_name = "";
$eng_name = "";

for($i = 0; $i < sizeof($groupInfo); $i++){
    if($i == 0){
        $chi_name = $groupInfo[$i]["TitleChinese"];
        $eng_name = $groupInfo[$i]["Title"];
    }else{
        $chi_name .= " ," . $groupInfo[$i]["TitleChinese"];
        $eng_name .= " ," . $groupInfo[$i]["Title"];
    }
}

$sql = "SELECT AcademicYearID FROM INTRANET_GROUP WHERE GroupID IN (".$group.")";
$AcademicYearID = $li->returnVector($sql);

if(!($li->RecordStatus==1 || $li->RecordStatus==3))
{
    $SchoolYearList = $fcm->Get_Academic_Year_List('',1,'',1);
    for ($i=0; $i< sizeof($SchoolYearList); $i++) {
        $SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);

        if($AcademicYearID[0] == $SchoolYearList[$i]['AcademicYearID']){
            $yearFilter .= $SchoolYearName;
        }
    }
}

# GroupCatSelect
$exceptArr = array(0);
if($plugin['eEnrollment'])
    $exceptArr[] = 5;
    $GroupCatSelect = $lgc->returnSelectCategory("name=RecordType",true,0,$li->RecordType,$exceptArr);
    
    # check link to class
    if($li->RecordType == 3)
    {
        $LinkedClass = $li->returnLinkedClass();
        $IsClassLinkedGroup = !empty($LinkedClass);
    }
    
    ### Title ###
    $MODULE_OBJ['title'] = $Lang['Group']['GroupMgmt'];
    
    $PAGE_NAVIGATION[] = array($Lang['Group']['GroupMgmtCtr'], "index.php");
    $PAGE_NAVIGATION[] = array($button_edit, "");
    
    $title = $Lang['Group']['GroupMgmtCtr'];
    //$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'],"",1);
    //$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'],"groupcategory/");
    //$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'],"role/");
    $TAGS_OBJ = $li->getTopTabInfoAry('group');
    $linterface->LAYOUT_START($xmsg);
    
    
    ?>
<script language="javascript">
function reset_innerHtml()
{
 	document.getElementById('div_Quota_err_msg').innerHTML = "";
}

function click_reset()
{
	reset_innerHtml();
	document.form1.reset();
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(!check_positive_int_30(obj.Quota, "<?php echo $i_GroupQuotaIsInt; ?>.", 0,0, "div_Quota_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Quota";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}
function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}
</script>

<form name="form1" method="post" action="bulk_edit_update.php" onSubmit="return checkform(this);">

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<div class="form_content">
<table class="form_table_v30">

<? if(!($li->RecordStatus==1 || $li->RecordStatus==3)) {?>
<tr>
	<td class="field_title"> <?=$i_EventAcademicYear; ?></td>
	<td><?=$yearFilter?><br><span id='div_SchoolYear_err_msg'></span></td>
</tr>
<? } ?>
<tr>
	<td class="field_title"> <?=$Lang['Group']['NameEn']; ?></td>
	<td><label name="Title" id="Title" class="textboxtext"/><?=$chi_name?></label></td>
</tr>
<tr>
	<td class="field_title"> <?=$Lang['Group']['NameCh']; ?></td>
	<td><label name="TitleChinese" id="TitleChinese" class="textboxtext"/><?=$eng_name?></label></td>
</tr>
<tr>
	<td class="field_title"><span class='tabletextrequire'>*</span> <?=$Lang['Group']['StorageQuota']; ?></td>
	<td><input class=text type=text name=Quota size=5 maxlength=5 value="<?=$li->StorageQuota?>"><br><span id='div_Quota_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['CanUsePublicAnnouncementEvents']; ?></td>
	<td><input TYPE=checkbox NAME=AnnounceAllowed VALUE=1 <?=$checked?>></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['DisplayInCommunity']; ?></td>
	<td><input TYPE=checkbox NAME="DisplayInCommunity" VALUE=1 <?=$li->DisplayInCommunity==1?"CHECKED":""?>></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['Group']['AllowDeleteOthersAnnouncement']; ?></td>
	<td><input TYPE=checkbox NAME="AllowDeleteOthersAnnouncement" VALUE=1  <?=$li->AllowDeleteOthersAnnouncement==1?"CHECKED":""?>></td>
</tr>
</table>

<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?> 
<?= $linterface->GET_ACTION_BTN($button_reset, "button", "click_reset();") ?> 
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?> 
<p class="spacer"></p>
</div>
</div>

<?php for($i = 0; $i < sizeof($GroupID); $i++){?>
<input type=hidden id=GroupID[] name=GroupID[] value="<?=$GroupID[$i]?>">
<?php }?>

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
