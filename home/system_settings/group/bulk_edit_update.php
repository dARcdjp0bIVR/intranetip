<?php
// Using By : Tommy
############## Change Log [Start] ##############
#
#   Date: 2020-04-06 Tommy
#   - file created
#
############## Change Log [End] ################
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libgroup.php");
include_once ($PATH_WRT_ROOT . "includes/liborganization.php");

intranet_auth();
intranet_opendb();

// Check access right
if (! ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$GroupID = IntegerSafe($GroupID);
if(is_array($GroupID)){
    $group = Implode(", ", $GroupID);
}
$AnnounceAllowed = ($AnnounceAllowed == 1 ? 1 : 0);
$DisplayInCommunity = IntegerSafe($DisplayInCommunity);
$AllowDeleteOthersAnnouncement = IntegerSafe($AllowDeleteOthersAnnouncement);

$li = new libdb();

if (! is_numeric($Quota) || $Quota < 0)
    $Quota = 5;

$fieldname .= "StorageQuota = '$Quota',";
$fieldname .= "AnnounceAllowed = '$AnnounceAllowed',";
$fieldname .= "DisplayInCommunity = '$DisplayInCommunity',";
$fieldname .= "AllowDeleteOthersAnnouncement = '$AllowDeleteOthersAnnouncement',";

$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_GROUP SET $fieldname WHERE GroupID IN (". $group .")";
$li->db_db_query($sql) or die(mysql_error());

intranet_closedb();
header("Location: index.php?xmsg=UpdateSuccess&SchoolYear=$SchoolYear");
exit;