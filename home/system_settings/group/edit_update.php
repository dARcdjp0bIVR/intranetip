<?php
// # Using By :
// ############ Change Log [Start] ################
//
// 2019-04-17 Pun [ip.2.5.10.5.1]
// - Added SQL Injection protection
//
// 2018-09-13 Chris
// - added in Gsuite SSO
//
// 2010-12-21 YatWoon
// - add option for group "allow delete others announcement"
//
// Date: 2010-12-13 YatWoon
// - add "TitleChinese"
//
// Date: 2010-01-05 Max (200912311437)
// - disable the change of function access right
// Date: 2009-12-15 YatWoon
// - Add Access right checking
// ############ Change Log [End] ################
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libgroup.php");
include_once ($PATH_WRT_ROOT . "includes/liborganization.php");

intranet_auth();
intranet_opendb();

// Check access right
if (! ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$GroupID = IntegerSafe($GroupID);
$SchoolYear = IntegerSafe($SchoolYear);
$Title = intranet_htmlspecialchars(trim($Title));
$TitleChinese = intranet_htmlspecialchars(trim($TitleChinese));
$Description = intranet_htmlspecialchars(trim($Description));
$AnnounceAllowed = ($AnnounceAllowed == 1 ? 1 : 0);
$DisplayInCommunity = IntegerSafe($DisplayInCommunity);
$AllowDeleteOthersAnnouncement = IntegerSafe($AllowDeleteOthersAnnouncement);

$li = new libdb();
$lo = new libgroup($GroupID);
$oldName = $lo->Title;

if ($lo->RecordType != $RecordType) {
    $sql = "UPDATE INTRANET_USERGROUP SET RoleID = NULL WHERE GroupID = $GroupID";
    $li->db_db_query($sql);
}

if (! is_numeric($Quota) || $Quota < 0)
    $Quota = 5;

$RecordType += 0;
$fieldname = "Title = '$Title', ";
$fieldname .= "TitleChinese = '$TitleChinese', ";
$fieldname .= "Description = '$Description', ";
$fieldname .= "RecordType = '$RecordType', ";
$fieldname .= "StorageQuota = '$Quota',";
$fieldname .= "AnnounceAllowed = '$AnnounceAllowed',";
if (! ($lo->RecordStatus == 1 || $lo->RecordStatus == 3)) {
    $fieldname .= "AcademicYearID = $SchoolYear,";
}
$fieldname .= "DisplayInCommunity = '$DisplayInCommunity',";
$fieldname .= "AllowDeleteOthersAnnouncement = '$AllowDeleteOthersAnnouncement',";

$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_GROUP SET $fieldname WHERE GroupID = $GroupID";
$li->db_db_query($sql) or die(mysql_error());

$li->UpdateRole_UserGroup();

// Update Calendar
$sql = "UPDATE CALENDAR_CALENDAR SET Name='$Title' WHERE CalID in (select CalID from INTRANET_GROUP where GroupID = $GroupID)";
$li->db_db_query($sql);

// Map to INTRANET_CLASS
$sql = "UPDATE INTRANET_CLASS SET GroupID = NULL WHERE ClassName = '$oldName'";
$li->db_db_query($sql) or die(mysql_error());
$sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
$li->db_db_query($sql) or die(mysql_error());

// INTRANET_ORPAGE_GROUP
$lorg = new liborganization();
if ($hide == 1) {
    $lorg->setGroupHidden($GroupID);
} else {
    $lorg->setGroupDisplay($GroupID);
}
// GSuite Integration

if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
    include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
    include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $GroupIDArr = array(
        $GroupID
    );
    $libGoogleSSO->syncGroupFromEClassToGoogle($li, $GroupIDArr);
}

intranet_closedb();
header("Location: index.php?filter=$RecordType&xmsg=UpdateSuccess&SchoolYear=$SchoolYear");
exit;