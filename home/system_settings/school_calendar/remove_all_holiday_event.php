<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();

$StartDate = getStartDateOfAcademicYear($TargetYearID);
$EndDate = getEndDateOfAcademicYear($TargetYearID);

$result = $lcycleperiods->DeleteAllIntranetEventByDateRange($StartDate,$EndDate);

if($result){
	header("location: index.php?AcademicYearID=$TargetYearID&msg=".$Lang['General']['ReturnMessage']['DeleteSuccess']);
}else{
	header("location: index.php?AcademicYearID=$TargetYearID&msg=".$Lang['General']['ReturnMessage']['DeleteUnsuccess']);
}

intranet_closedb();
?>