<?php
## using by : 
/*
 * Modification Log:
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();

### Get Page Variable
$AcademicYearID = $_REQUEST['AcademicYearID'];
$TimezoneID = $_REQUEST['TimezoneID'];
$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];


### Initialize Library
$linterface = new interface_html();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();


### Check Access Right
$lcycleperiods->Check_School_Settings_Access_Right();


### Set UI Elements
$CurrentPageArr['SchoolCalendar'] = 1;
$TAGS_OBJ = $lcycleperiods_ui->Get_School_Settings_Top_Tab('Timezone');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];
$linterface->LAYOUT_START();

echo $lcycleperiods_ui->initJavaScript();
echo $lcycleperiods_ui->Get_Timezone_Special_Timetable_Settings_UI($AcademicYearID, $TimezoneID, $SpecialTimetableSettingsID); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/javascript">
$(document).ready( function() {
	$('input#BgColor').colorPicker();
	js_Changed_RepeatType($('select#RepeatTypeSel').val(), true);
});

function js_Go_Back_Time_Zone() {
	window.location = 'time_zone.php?AcademicYearID=<?=$AcademicYearID?>';
}

function js_Changed_RepeatType(jsRepeatType, jsIsInitial) {
	jsIsInitial = jsIsInitial || false;
	var jsSpecialTimetableSettingsID = '';
	if (jsIsInitial) {
		jsSpecialTimetableSettingsID = '<?=$SpecialTimetableSettingsID?>';
	} 
	
	var jsAction = '';
	if (jsRepeatType == 'cycle') {
		jsAction = 'Special_Timetable_RepeatDay_CycleDay_Checkboxes_Table';
	}
	else if (jsRepeatType == 'weekday'){
		jsAction = 'Special_Timetable_RepeatDay_Weekday_Checkboxes_Table';
	}
	
	$('div#RepeatDayDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: jsAction,
			TimezoneID: '<?=$TimezoneID?>',
			SpecialTimetableSettingsID: jsSpecialTimetableSettingsID
		},
		function(ReturnData)
		{
			js_Reload_Timetable_Selection(jsIsInitial);
			js_Reload_Selected_Dates_Table(jsIsInitial);
		}
	);
}

function js_Clicked_RepeatDay_Checkbox() {
	js_Reload_Timetable_Selection();
	js_Reload_Selected_Dates_Table();
}

function js_Reload_Timetable_Selection(jsIsInitial) {
	jsIsInitial = jsIsInitial || false;
	var jsSpecialTimetableSettingsID = '';
	if (jsIsInitial) {
		jsSpecialTimetableSettingsID = '<?=$SpecialTimetableSettingsID?>';
	} 
	
	$('div#TimetableSelDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Timetable_Selection',
			TimezoneID: '<?=$TimezoneID?>',
			SpecialTimetableSettingsID: jsSpecialTimetableSettingsID,
			RepeatType: $('select#RepeatTypeSel').val(),
			RepeatDayList: Get_Checkbox_Value_By_Class('DayChk')
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Reload_Selected_Dates_Table(jsIsInitial) {
	jsIsInitial = jsIsInitial || 0;
	
	$('div#SelectedDatesDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Special_Timetable_Selected_Dates_Table',
			TimezoneID: '<?=$TimezoneID?>',
			SpecialTimetableSettingsID: '<?=$SpecialTimetableSettingsID?>',
			RepeatType: $('select#RepeatTypeSel').val(),
			RepeatDayList: Get_Checkbox_Value_By_Class('DayChk'),
			IsInitial: jsIsInitial
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Clicked_Select_All_Date_Checkbox(jsChecked) {
	$('input.DateChk:enabled').each( function() {
		$(this).attr('checked', jsChecked);
		js_Clicked_Selected_Date_Checkbox($(this).attr('id'));
	});
}

function js_Clicked_Selected_Date_Checkbox(jsChkID) {
	// unset the checkmaster if uncheck the checkbox
	var jsChkObj = document.getElementById(jsChkID);
	unset_checkall(jsChkObj, jsChkObj.form);
	
	// grey the selected date if unchecked
	var jsSelectedDate = jsChkObj.value;
	var jsTrID = 'SelectDateTr_' + jsSelectedDate;
	if (jsChkObj.checked) {
		$('tr#' + jsTrID).removeClass('tabletextremark');
	}
	else {
		$('tr#' + jsTrID).addClass('tabletextremark');
	}
}

function js_Save_Settings() {
	$('div.WarningDiv').hide();
	
	var jsCanSubmit = true;
	
	if (parseInt($('input.DateChk:enabled:checked').length) == 0) {
		$('div#SelectDateWarningDiv').show();
		Scroll_To_Element('DateNavigationDiv');
		jsCanSubmit = false;
	}
	
	if ($('select#TimetableID').val() == '') {
		$('div#TimetableWarningDiv').show();
		Scroll_To_Element('TimetableID');
		jsCanSubmit = false;
	}
	
	
	if (jsCanSubmit) {
		$('form#form1').attr('action', 'special_timetable_settings_update.php').submit();	
	}
}

function js_Delete_Settings() {
	if (confirm('<?=$Lang['SysMgr']['CycleDay']['jsWarningArr']['DeleteSpecialTimetableSettings']?>')) {
		$('form#form1').attr('action', 'special_timetable_settings_delete.php').submit();
	}
}
</script>  