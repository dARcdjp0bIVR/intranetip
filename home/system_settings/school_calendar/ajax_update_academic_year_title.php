<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

$lcycleperiods->Start_Trans();

$result = $lcycleperiods->updateAcademicYearTitle($RecordType, $targetAcademicYearID, $UpdateValue);

if (in_array(false,$result)) {
	$lcycleperiods->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}else {
	$lcycleperiods->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
}

intranet_closedb();
?>