<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

$linterface = new interface_html();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$lfcm = new form_class_manage();
$libcalevent = new libcalevent2007();

$CurrentPageArr['SchoolCalendar'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"preview.php",1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

$returnMsg = urldecode($msg);
$linterface->LAYOUT_START($returnMsg); 
?>
<?=$lcycleperiods_ui->getCycleDaysCalendarPreview();?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/javascript">
var data_firstday_by_type = new Array();
	data_firstday_by_type[0] = new Array();
	data_firstday_by_type[1] = new Array();
	data_firstday_by_type[2] = new Array();
	<?
	for ($i=0; $i<26; $i++)
	{
	     ?>
	     data_firstday_by_type[0][data_firstday_by_type[0].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_numeric[$i]?>");
	     data_firstday_by_type[1][data_firstday_by_type[1].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_alphabet[$i]?>");
	     data_firstday_by_type[2][data_firstday_by_type[2].length] = Array(<?=$i?>,"<?=$lcycleperiods->array_roman[$i]?>");
	     <?
	}
	?>
//var jsMonth = <?=$month?>;
//var jsYear = <?=$year?>;

// action: 1 means next month; -1 means previous month
function updateMonthYear(actionType)
{
	jsMonth += actionType;
	
	if (jsMonth > 12)
	{
		jsMonth = 1;
		jsYear += 1;
	}
	else if (jsMonth < 1)
	{
		jsMonth = 12;
		jsYear -= 1;
	}
}

function ChangeSchoolYear(schoolYearID)
{
	Block_Document()
	$.post(
		"ajax_change_preview.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
			initThickBox();
		}
	);
}

function gotoPage(page)
{
	var url = page;
	self.location = url;
}

// block ui function
{
var isBlocking = false;
function Block_Input(){
	if (!isBlocking) {
		var blockMsg = "<?=$Lang['General']['Loading']?>";
		$('body').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
	}
}

function Unblock_Input() {
	$('body').unblock();
	isBlocking = false;
}
}

function reloadMainContent(schoolYearID,layer)
{
	Block_Document();
	$.post(
		"ajax_change_preview.php",
		{
			"targetSchoolYearID":schoolYearID
		},
		function(responseText){
			$('#main_body').html(responseText);
			UnBlock_Document();
			initThickBox();
		}
	);
}

function showAdditionalInfo(val)		//control which <div> show in add period form
{
	if(val == 1){
		$("#normal_additional_info").show();
		$("#targetPeriodType0").attr("checked",false);
	}else if(val == 0){
		$("#normal_additional_info").hide();
		$("#targetPeriodType1").attr("checked",false);
	}else if(val == -1){
		$("#normal_additional_info").show();
		$("#targetPeriodType0").attr("checked",false);
		$("#targetPeriodType1").attr("checked",true);
	}else if(val == -2){
		$("#normal_additional_info").hide();
		$("#targetPeriodType0").attr("checked",true);
		$("#targetPeriodType1").attr("checked",false);
	}
}

function newDefaultPeriodForm2(startDate)
{
	wordXmlHttp = GetXmlHttpObject();
    
	var yearID = $('#academic_year').val();
	var postContent = 'AcademicYearID='+yearID;
	var postContent = 'StartDate='+startDate;
	var url = 'ajax_default_period_add.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);	
}

function newDefaultPeriodForm()//create a new period input form
{
	wordXmlHttp = GetXmlHttpObject();
    
	var yearID = $('#academic_year').val();
	var postContent = 'AcademicYearID='+yearID;
	var url = 'ajax_default_period_add.php';
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function changeFirstDaySelect()
{
	type = document.getElementById('CycleType').value;
	max_num = document.getElementById('PeriodDays').value;
	obj = document.getElementById('FirstDay');
	
	var current = obj.options.length;
	
	for (var j=current;j>0;j--) obj.options[j-1] = null;
	for (var i=0;i<max_num;i++)
	{
		obj.options[obj.options.length] = new Option(data_firstday_by_type[type][i][1],data_firstday_by_type[type][i][0]);
	}
}

function checkInputFields()		//input period form's validation
{
	if($('input#period_start').val()=="")
	{
		alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartEmpty'];?>");
		return false;
	}
	else
	{
		if($('input#period_end').val()=="")
		{
			alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndEmpty']?>");
			return false;
		}
		else
		{
			if(check_date((document.getElementById('period_start')),"<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid']?>"))
			{
				if(check_date((document.getElementById('period_end')),"<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid']?>"))
				{
					var boolCheckPoint1 = 1;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	
	if(boolCheckPoint1 == 1)
	{
		if(compareDate2(document.getElementById('period_end').value,document.getElementById('period_start').value) == 1)
		{
			return true;
		}
		else
		{
			alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['StartPeriodLargerThenEndPeriod'];?>");
			return false;
		}
	}
}

function updateNewPeriod()		// use AJAX to insert a new period
{
	// get all the input variable
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	//var targetPeriodType = $('#targetPeriodType').val();
	//var targetPeriodType = 1;
	
	var element = document.getElementById('targetPeriodType1');
	var targetPeriodType = 0;
	if(element.checked == true){
		var targetPeriodType = 1;
	}
	
	var targetCycleType = $('#CycleType :selected').val();
	var targetPeriodDays = $('#PeriodDays :selected').val();
	var targetFirstDay = $('#FirstDay :selected').val();
	
	var element = document.getElementById('satCount');
	var targetSatCount = 0;
	if(element.checked == true){
		var targetSatCount = 1;
	}
	var bgColorCode = $("#bgColorCode").val();
	
	if(checkInputFields())
	{
		$.post(
			"ajax_period_add_update.php",
			{
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"PeriodType":targetPeriodType,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount,
				"SchoolYearID":$('#academic_year').val(),
				"ColorCode":bgColorCode
			},
			function(responseText){
				if(responseText != 0 && responseText != -1){
					Get_Return_Message(responseText);
					reloadMainContent($('#academic_year').val(),'#Production_Calendar');
					window.top.tb_remove();
				}else{
					newDefaultPeriodForm(targetPeriodStart,targetPeriodEnd,targetPeriodType);
					if(responseText == 0)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
						
					if(responseText == -1)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear'];?>");
				}
			}
		);
	}
}

function editPeriodRange(PeriodID)
{
	wordXmlHttp = GetXmlHttpObject();
	
	var url = 'ajax_period_edit.php';
	var postContent = 'PeriodID='+PeriodID;
	
	wordXmlHttp.onreadystatechange = function() {
		if (wordXmlHttp.readyState == 4) {
			ResponseText = Trim(wordXmlHttp.responseText);
			document.getElementById('TB_ajaxContent').innerHTML = wordXmlHttp.responseText;
			//Init_JEdit_Input("div.jEditInput");
			
			$('#bgColorCode').colorPicker();
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_start').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
			
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
			$('#period_end').datepick({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				changeFirstDay: false,
				firstDay: 0
			});
		}
	};
	wordXmlHttp.open("POST", url, true);
	wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	wordXmlHttp.send(postContent);
}

function updateEditPeriod(SchoolYearID){
	wordXmlHttp = GetXmlHttpObject();
	var targetSchoolYearID = SchoolYearID;
	var targetPeriodID = $('input#period_id').val();
	var targetPeriodStart = $('input#period_start').val();
	var targetPeriodEnd = $('input#period_end').val();
	var targetPeriodType = $('#targetPeriodType').val();
	var targetCycleType = $('#CycleType :selected').val();
	var targetPeriodDays = $('#PeriodDays :selected').val();
	var targetFirstDay = $('#FirstDay :selected').val();
	if(targetPeriodType == 1){
		var element = document.getElementById('satCount');
		var targetSatCount = 0;
		if(element.checked == true){
			var targetSatCount = 1;
		}
	}
	var bgColorCode = $("#bgColorCode").val();

	if(checkInputFields())
	{
		$.post(
			"ajax_period_edit_update.php",
			{
				"SchoolYearID":targetSchoolYearID,
				"PeriodID":targetPeriodID,
				"PeriodStart":targetPeriodStart,
				"PeriodEnd":targetPeriodEnd,
				"CycleType":targetCycleType,
				"PeriodDays":targetPeriodDays,
				"FirstDay":targetFirstDay,
				"SatCount":targetSatCount,
				"ColorCode":bgColorCode
			},
			function(responseText){
				if(responseText != 0 && responseText != -1){
					Get_Return_Message(responseText);
					reloadMainContent(targetSchoolYearID,'#edit_period');
					window.top.tb_remove();
				}else{
					editPeriodRange(targetPeriodID);
					if(responseText == 0)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'];?>");
					if(responseText == -1)
						alert("<?=$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear']?>");
				}
			}
		);
	}
}

function deletePeriod(periodID)
{
	Block_Thickbox();
	$.post(
			"ajax_period_remove.php",
			{
				"PeriodID":periodID
			},
			function(responseText){
				Get_Return_Message(responseText);
				reloadMainContent($('#academic_year').val(),'#Production_Calendar');
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		);
}

$(document).ready(function() 
{
	//$("table.bookable, table.non_bookable").hover(function() {
	//$("table.bookable").hover(function() {
	//	var currentTableID = $(this).attr('id');
	//	$(this.cells[1]).addClass('showDragHandle').click( function() {
	//		alert("currentTableID = " + currentTableID);
	//	});
	//}, function() {
	//	var currentTableID = $(this).attr('id');
	//	$(this.cells[1]).removeClass('showDragHandle').unbind('click');
	//});
	
	$("#prevCalBtn").click( function() {
		updateMonthYear(-1);
		var serverScript = "generate_calendar_aj.php";
		
		// Disable arrors and display loading message
		$("#prevCalBtn").attr("disabled", "disabled");
		$("#nextCalBtn").attr("disabled", "disabled");
		Block_Thickbox();
		
		$("#CalendarDiv").load(
			serverScript, 
			{	
				month: jsMonth, 
				year: jsYear,
				serverScript: serverScript,
				calandarDivName: "CalendarDiv"
			},
			function() {
				$("#prevCalBtn").removeAttr("disabled");
				$("#nextCalBtn").removeAttr("disabled");
				UnBlock_Thickbox();
			}
		);
	});
	
	$("#nextCalBtn").click( function() {
		updateMonthYear(1);
		var serverScript = "generate_calendar_aj.php";
		
		// Disable arrors and display loading message
		$("#prevCalBtn").attr("disabled", "disabled");
		$("#nextCalBtn").attr("disabled", "disabled");
		Block_Thickbox();
		
		$("#CalendarDiv").load(
			serverScript, 
			{	
				month: jsMonth, 
				year: jsYear,
				serverScript: serverScript,
				calandarDivName: "CalendarDiv"
			},
			function() {
				$("#prevCalBtn").removeAttr("disabled");
				$("#nextCalBtn").removeAttr("disabled");
				UnBlock_Thickbox();
			}
		);
	});
});
</script>  