<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lc = new libcycleperiods();

$li->Start_Trans();

//$sql = "DELETE FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID IN ($PeriodID)";
//$result = $li->db_db_query($sql);

$result = $lc->deletePeriod($PeriodID);

if (in_array(false,$result)) {
	$li->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}
else {
	$li->Commit_Trans();
	echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
}

### re-generate the Preview Calendar ###
$lc->generatePreview();

intranet_closedb();
?>