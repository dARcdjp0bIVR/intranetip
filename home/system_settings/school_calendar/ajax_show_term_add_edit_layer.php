<?php
// using by :
################# Change Log [Start] #####
#
#	Date	:	2018-08-08	Henry
# 				fixed sql injection
#
#	Date	:	2016-01-04	Omas
# 				add pram $RecordType for current and future term editing
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$AcademicYearID = IntegerSafe($AcademicYearID);
$YearTermID = IntegerSafe($YearTermID);

echo $lcycleperiods_ui->showTermAddEditLayer($AcademicYearID,$YearTermID,$TermTitleCh,$TermTitleEn,$TermStartDate,$TermEndDate,$RecordType);

intranet_closedb();
?>