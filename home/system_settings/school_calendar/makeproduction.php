<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

$lc = new libcycleperiods();

# Clear Production table
$lc->clearProduction();

### Old version -> run makeProduction() to copy from preview to production
# Copy to production
//$lc->makeProduction();

### New Version -> directly copy the setting to production
# Get Periods
$periods = $lc->returnPeriods_NEW();

# Handle each period
for ($i=0; $i<sizeof($periods); $i++)
{
     list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted) = $periods[$i];
     if ($PeriodDays < 1) continue;

         if ($CycleType == 1)
         {
             $txtArray = $lc->array_alphabet;
         }
         else if ($CycleType == 2)
         {
              $txtArray = $lc->array_roman;
         }
         else
         {
             $txtArray = $lc->array_numeric;
         }

         # Get Non-Cycle Days
         $temp = $lc->getNonCycleDays($PeriodStart,$PeriodEnd);
         for ($j=0; $j<sizeof($temp); $j++)
         {
              $ts = strtotime($temp[$j]);
              $non_cycle_days[$ts] = "1";
         }
         $ts_start = strtotime($PeriodStart);
         $ts_end = strtotime($PeriodEnd);
         $ts_current = $ts_start;
         $current_weekday = date("w",$ts_current);
         $current_cycleday = $FirstDay;
         $values = "";
         $delim = "";
         while ($ts_current <= $ts_end)
         {
                #$recordDate = date("Y-m-d",$ts_current);

                # Check is non-cycle day
                if ($non_cycle_days[$ts_current]==1) # Skip
                {
                    #echo "$recordDate -> Non cycle<br>\n";
                    $current_weekday = ($current_weekday+1)%7;
                    $ts_current += 86400;  # 1 Day
                    continue;
                }
                # Check is weekend
                if ($current_weekday==0 || ($current_weekday==6 && $SaturdayCounted!=1))
                {
                    #echo "$recordDate -> Weekend<br>\n";
                    $ts_current += 86400;  # 1 Day
                    $current_weekday = ($current_weekday+1)%7;
                    continue;
                }
                # Put in Database
                $recordDate = date("Y-m-d",$ts_current);
                $txtCycle = $txtArray[$current_cycleday];
//echo $i_CycleNew_Prefix_Chi."<br />";                
                $values .= "$delim ('$recordDate','$i_CycleNew_Prefix_Eng$txtCycle','$i_CycleNew_Prefix_Chi$txtCycle','$txtCycle')";
                $delim = ",";

                # Next iteration
                $current_weekday = ($current_weekday+1)%7;
                $current_cycleday = ($current_cycleday+1)%$PeriodDays;
                $ts_current += 86400;  # 1 Day
         }
         $sql = "INSERT INTO INTRANET_CYCLE_DAYS (RecordDate, TextEng, TextChi, TextShort)
                        VALUES $values";
         $lc->db_db_query($sql);
         
         $arrTargetPeriod[] .= $PeriodID;
         
         ### update the publish log ###
         $sql = "INSERT INTO INTRANET_CYCLE_DAYS_PRODUCTION_LOG (LogID,DateInput) VALUES (1,NOW()) ON DUPLICATE KEY UPDATE DateInput = NOW()";
         $lc->db_db_query($sql);
}
for ($i=0; $i<sizeof($periods); $i++)
{
	list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted) = $periods[$i];
	
	$sql = "UPDATE INTRANET_CYCLE_GENERATION_PERIOD SET RecordStatus = 1 WHERE PeriodID = $PeriodID";
//    echo $sql."<BR/>\r\n";
    $lc->db_db_query($sql);
}

intranet_closedb();
$path = urlencode($Lang['SysMgr']['CycleDay']['GenerateProduction']['ReturnSuccess']);
$path = "index.php?msg=".$path;

header("Location: $path");
?>