<?php
//using : 
############# Change Log [Start] ################
# Date:	2015-09-15 Ivan [U84654] [ip.2.5.6.10.1]
#			enable timezone settings for KIS if purchased eBooking module
#
# Date:	2011-07-06	YatWoon
#		- add "isSkipSAT" and "isSkipSUN"
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################
set_time_limit(86400);
ini_set("memory_limit", "512M");
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$linterface = new interface_html();

$lcycleperiods->Start_Trans();

$result = $lcycleperiods->importDataToINTRANET_EVENT();

if (in_array(false,(array)$result)) {
	$lcycleperiods->RollBack_Trans();
	$ImportResult = false;
} else {
	$lcycleperiods->Commit_Trans();
	$ImportResult = true;
}

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);

//$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
$showTimezone = true;
if ($_SESSION["platform"]=="KIS" && !$plugin['eBooking'] || $sys_custom['LivingHomeopathy'] || $sys_custom['HideTimeZone']){
	// kis without eBooking => hide timezone settings
	$showTimezone = false;
}
if ($showTimezone) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
}

$showAcademicYear = true;
if ($sys_custom['LivingHomeopathy'] || $sys_custom['HideAcademicYearTerm']) {
	$showAcademicYear = false;
}
if ($showAcademicYear) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
}
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

$linterface->LAYOUT_START($returnMsg); 
?>

<?
echo $lcycleperiods_ui->showImportHolidaysEventsResult($AcademicYearID,$ImportResult);
?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>