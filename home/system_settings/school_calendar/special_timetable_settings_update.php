<?php
## using by : 
/*
 * Modification Log:
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

### Get Page Variable
$AcademicYearID = $_REQUEST['AcademicYearID'];
$TimezoneID = $_REQUEST['TimezoneID'];
$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];

$TimetableID = $_REQUEST['TimetableID'];
$BgColor = $_REQUEST['BgColor'];
$RepeatType = $_REQUEST['RepeatType'];
$DayArr = $_REQUEST['DayArr'];
$DateArr = $_REQUEST['DateArr'];


$lcycleperiods = new libcycleperiods();
$Success = $lcycleperiods->Save_Special_Timetable_Settings($SpecialTimetableSettingsID, $TimezoneID, $TimetableID, $BgColor, $RepeatType, $DayArr, $DateArr);
$ReturnMsgKey = ($Success)? 'SpecialTimetableSettingsSavedSuccess' : 'SpecialTimetableSettingsSavedFailed';

intranet_closedb();
header("Location: time_zone.php?AcademicYearID=$AcademicYearID&ReturnMsgKey=$ReturnMsgKey");
?>