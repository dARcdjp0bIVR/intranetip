<?php
// Editing by  
#####################################
#   Date:   2019-01-09 Anna
#           added $EventNatureEng,$DescriptionEng for general
#
#   Date:   2018-03-12 Anna
#           added SFOC cust
#
#	Date:	2013-10-04 	Carlos
#			add $ClassGroupID to createSchoolHolidayEvent()
#
#	Date:	2011-09-08	YatWoon
#			Fixed: add empty $result array checking
#
#	Date:	2011-07-06	YatWoon
#			add "isSkipSAT" and "isSkipSUN"
#
#####################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lcycleperiods = new libcycleperiods();

$lcycleperiods->Start_Trans();

$StartDate = substr(getStartDateOfAcademicYear($SchoolYearID),0,10);
$EndDate = substr(getEndDateOfAcademicYear($SchoolYearID),0,10);
if(($StartDate <= $EventDate))
{
	$StartDateCheck = 1;
}
if(($StartDate <= $EventEndDate))
{
	$EndDateCheck = 1;
}

if(($StartDateCheck == 1) && ($EndDateCheck == 1))
{
    if($sys_custom['eClassApp']['SFOC']){
//         $EventSportType = $_POST['EventSportType'];
        $EventAssociation = $_POST['EventAssociation'];
        $EventSfocTitle= $_POST['EventSfocTitle'];
        $EventEmail= $_POST['EventEmail'];
        $EventContact= $_POST['EventContact'];
        $EventWebsite= $_POST['EventWebsite'];
        $EventToBeDetermined = $_POST['EventToBeDetermined'];
        $hourStart = str_pad($_POST['EventStartTimeHour'], 2, 0, STR_PAD_LEFT);
        $minStart = str_pad($_POST['EventStartTimeMin'], 2, 0, STR_PAD_LEFT);
        $hourEnd = str_pad($_POST['EventEndTimeHour'], 2, 0, STR_PAD_LEFT);
        $minEnd = str_pad($_POST['EventEndTimeMin'], 2, 0, STR_PAD_LEFT);
        
        $EventStartTime=$hourStart.':'.$minStart.':00';
        $EventEndTime= $hourEnd.':'.$minEnd.':00';
        
    }

	$result = $lcycleperiods->createSchoolHolidayEvent($EventDate,$EventEndDate,$RecordType,$TargetGroupID,$Title,$EventLocationID,$EventVenue,$EventNature,$Description,$isSkipCycle,$RecordStatus, $isSkipSAT, $isSkipSUN, $ClassGroupID
	    ,$EventAssociation,$EventStartTime,$EventEndTime,$EventEmail,$EventContact,$EventWebsite,$EventToBeDetermined, $EventTitleEng, $EventAssociationEng, $EventVenueEng,$EventNatureEng,$DescriptionEng);
	
	if(empty($result))
	{
		echo 0;
		$lcycleperiods->RollBack_Trans();
	}
	else if (!in_array(false,$result)) {
		echo 1;
		$lcycleperiods->Commit_Trans();
	} else {
		echo 0;
		$lcycleperiods->RollBack_Trans();
	}
} else {
	echo -1;
	$lcycleperiods->RollBack_Trans();
}


intranet_closedb();
?>