<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
$data .='<tr class="tabletop">
			<td>'.$Lang['SysMgr']['Location']['Building'].'</td>
			<td>'.$Lang['SysMgr']['Location']['Floor'].'</td>
			<td>'.$Lang['SysMgr']['Location']['Room'].'</td>
			<td>'.$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueCode'].'</td></tr>';
			
$sql = "SELECT ".$linventory->getInventoryNameByLang("a.").", ".$linventory->getInventoryNameByLang("b.").", ".$linventory->getInventoryNameByLang("c.").", CONCAT(a.Code,'>',b.Code,'>',c.Code) FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) WHERE a.RecordStatus = '1' AND b.RecordStatus = '1' AND c.RecordStatus = '1'";
$result = $linventory->returnArray($sql);
if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($buliding_name, $floor_name, $room_name, $code) = $result[$i];
		$data .= "<tr class='tablerow1'>";
		$data .= "<td>$buliding_name</td>";
		$data .= "<td>$floor_name</td>";
		$data .= "<td>$room_name</td>";
		$data .= "<td>$code</td>";
		$data .= "</tr>";
	}
}
else
{
	$data .= "<tr><td colspan='4' align='center'>$i_no_record_exists_msg</td></tr>";
}
	
$data .= "</table>";

$output = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:hideLayer(\'SiteCode\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
						'.$data.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';

echo $output;

?>