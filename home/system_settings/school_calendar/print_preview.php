<?php
## Using By : 
##########################################################
## Modification Log
## 2020-10-29: Tommy
## - set style for print calendar
##
## 2013-10-07: Carlos
## - [KIS] Added $_GET['class_group_id'] parameter for filtering class group events 
##
## 2010-06-04: Max (201006040909)
## - Get the academic year id from previous page
##
## 2009-12-14: Max (200912141001)
## - Link to a page with printing version
##########################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = $_GET['academicYearId'];

$FDTPrintColor = $_GET['p_color'];
$FDTMonthEvent = $_GET['p_month_event'];
$FDTMaxCalendar = $_GET['max_cal'];
$printingParArr = array("IS_COLOR_PRINT"=>($FDTPrintColor==1?true:false), "IS_PRINT_NO_EVENT"=>($FDTMonthEvent==0?true:false), "NUM_CAL_PER_PAGE"=>$FDTMaxCalendar);

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
if($isKIS) {
	if($_SESSION['UserType'] != USERTYPE_STAFF) {
		include_once($intranet_root."/includes/libclassgroup.php");
		$lclassgroup = new Class_Group();
		$ClassGroupID = $lclassgroup->Get_Class_GroupID_By_UserID($AcademicYearID);
	}else if($_GET['class_group_id'] != ''){
		$ClassGroupID = $_GET['class_group_id'];
	}
}

$linterface = new interface_html("popup_no_layout.html");
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

#############################################
## System Checking 
## Step 1 : check finish academic year setting or not 
##				IsAcademicYearSet() : 1 = true, 0= false
## Step 2 : check current date is in term or not
##				IsCurrentDateInTerm() : 1 = true, 0 = false
#############################################
if(!$lc->IsAcademicYearSet())
{
	$empty_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'];
	$msg = urlencode($empty_msg);
	//header("Location: academic_year.php?msg=$msg");
}
if(!$lc->IsCurrentDateInTerm())
{
	$not_in_term_msg = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'];
	$msg = urlencode($not_in_term_msg);
	//header("Location: academic_year.php?msg=$msg");
}
## End checking ##

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);
$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];


if($AcademicYearID == "")
{
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$returnMsg = urldecode($msg);


$linterface->LAYOUT_START($returnMsg); 
?>

<?=$lcycleperiods_ui->calendarDivision($AcademicYearID, $printingParArr, $ClassGroupID);?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script language="javascript">
$(document).ready(function(){
	
	$("html").css('background-color', '#FFFFFF');
	$("body").css('background-color', '#FFFFFF');
	$("table#calendarTable").parents("td").css({'vertical-align':'top'});
	
	window.print();
});
</script>
<style type="text/css">

    @media print {

        #calendarTable > tbody {
    	   page-break-inside: auto;
        }

        #calendarTable > tbody > tr {
    	   page-break-inside:avoid;
    	   display: flex;
        }

        .pageBreak{
            page-break-after:always;
        }
    }

</style>