<?php
//using : 

#####################################
#
#   Date:   2019-10-14 Tommy
#           add access checking for KIS
#
#   Date:   2019-02-25 Vito
#           modifcation(css style) in checkSiteCode() & Load_Reference()
#
#   Date:   2019-02-04 Vito
#           modified checkSiteCode()
#
# 	Date: 	2016-03-11 Kenneth [ip2.5.7.4.1]
#			added referecne thickbox for groupID
#
# 	Date: 	2015-09-15 Ivan [U84654] [ip.2.5.6.10.1]
#			enable timezone settings for KIS if purchased eBooking module
#
#	Date:	2013-10-07	Carlos
#			[KIS] added "Class Group Code"
#
#	Date:	2011-07-06	YatWoon
#			add "isSkipSAT" and "isSkipSUN"
#
#####################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

if($_SESSION["platform"]=="KIS" && !$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] && !$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]){
    header("location: /kis/#/apps/calendar/?clearCoo=1");
    exit();
}

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);

//$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
$showTimezone = true;
if ($_SESSION["platform"]=="KIS" && !$plugin['eBooking'] || $sys_custom['LivingHomeopathy'] || $sys_custom['HideTimeZone']){
	// kis without eBooking => hide timezone settings
	$showTimezone = false;
}
if ($showTimezone) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
}

$showAcademicYear = true;
if ($sys_custom['LivingHomeopathy'] || $sys_custom['HideAcademicYearTerm']) {
	$showAcademicYear = false;
}
if ($showAcademicYear) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
}
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

if($xmsg != "")
{
	$returnMsg = $xmsg;
}

$linterface->LAYOUT_START($returnMsg); 
?>

<script language="javascript">
	function hideLayer(pos)
	{
		$("#"+pos).css("visibility","hidden");
	}

	function checkSports(){
		$.post(
			"ajax_get_sport_suggestion.php",
			{
			},
			function(responseText){
				var p = $("#sportLink");
				var position = p.position();
				
				var t = position.top + 15;
				var l = position.left + p.width()-75;
				
				$("#SportSuggest").html(responseText);
				$("#SportSuggest").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
				$("#SportSuggest").css({"overflow-y":"auto","height":"200px"});
			}
		);
	}

//	Old version of checkSiteCode function
// 	function checkSiteCode()
// 	{
// 		$.post(
// 			"ajax_retrieve_site_code.php",
// 			{	
// 			},
// 			function(responseText){
// 				var p = $("#locationID");
// 				var position = p.position();
				
// 				var t = position.top + 15;
// 				var l = position.left + p.width()-75;

// 				$("#SiteCode").html(responseText);
// 				$("#SiteCode").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
// 				$("#SiteCode").css({"overflow-y":"auto","height":"200px"});
// 			}
// 		);
// 	}

	function checkSiteCode(siteCode)
	{
		$('#remarkDiv_type').html('');
		var AcademicYearID = $('#AcademicYearID').val();
		
		$.post(
			"ajax_reload.php", 
			{ 
				Action: "load_Site_Code",
				flag: siteCode,
				AcademicYearID: AcademicYearID
			},
			function(ReturnData)
			{
// 				$('#remarkDiv_type').width("546px");
				$('#remarkDiv_type').html(ReturnData);
				var widthOfTable = $('#containerSite').width();
// 				alert(widthOfTable);
// 				var divWidth = widthOfTable + 17;
				var isIE = /*@cc_on!@*/false || !!document.documentMode;
				// Internet Explorer 6-11
				if(isIE){
					var divWidth = widthOfTable + 18;
				}
				else{
					var divWidth = widthOfTable + 17;
				}
				$('#remarkDiv_type').width(divWidth + "px");
				$('#remarkDiv_type').css({"padding-top":"0px"});
				$('#stickyRemarks').width(divWidth + "px");
// 				alert(divWidth);
// 				alert($('#remarkDiv_type').position());
// 				alert($('#stickyRemarks').width());
				
			}
		);
		
		displayReference(siteCode);
	}
	
	function checkGroupID()
	{
		$.post(
			"ajax_retrieve_group_id.php",
			{	
			},
			function(responseText){
				$("#GroupID").html(responseText);
				$("#GroupID").css("visibility","visible");
			}
		);
	}
	
	function checkClassGroupCode()
	{
		$.post(
			"ajax_retrieve_class_group_code.php",
			{	
			},
			function(responseText){
				$("#ClassGroupCode").html(responseText);
				$("#ClassGroupCode").css("visibility","visible");
			}
		);
	}
	function Load_Reference(ref) {
	
		$('#remarkDiv_type').html('');
		var AcademicYearID = $('#AcademicYearID').val();
		
		$.post(
			"ajax_reload.php", 
			{ 
				Action: "load_reference",
				flag: ref,
				AcademicYearID: AcademicYearID
			},
			function(ReturnData)
			{				
				$('#remarkDiv_type').html(ReturnData);
				var widthOfTable = $('#containerGroupID').width();
// 				alert(widthOfTable);
				var isIE = /*@cc_on!@*/false || !!document.documentMode;
				if(isIE){
					var divWidth = widthOfTable + 18;
				}
				else{
					var divWidth = widthOfTable + 17;
				}
// 				alert(divWidth);
				$('#remarkDiv_type').width(divWidth + "px");
				$('#remarkDiv_type').css({"padding-top":"0px"});
				$('#stickyRemarks').width(divWidth + "px");
				
			
			}
		);
		
		displayReference(ref);
	}
	function displayReference(ref) {
		var p = $("#"+ref);
		var position = p.position();
		
		var t = position.top + 15;
		var l = position.left + p.width()+5;
		$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px"});
	}
	function getRemarkDivIdByRemarkType(remarkType) {
		return 'remarkDiv_' + remarkType;
	}
	function hideRemarkLayer(remarkType) {
		var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
		MM_showHideLayers(remarkDivId, '', 'hide');
	}
</script>

<?=$lcycleperiods_ui->getImportHolidaysEventsUI($AcademicYearID);?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>