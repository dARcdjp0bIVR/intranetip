<?php
// using : 

/** Change Log
 * 	
 *  2019-02-04 Vito
 *  add $Action = 'load_Site_Code', return referenceThickBox
 * 
 * 	2016-03-11 Kenneth
 * 	add $Action = 'load_reference', return reference thickbox
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();
$linterface = new interface_html();
$lgroup = new libgroup();
$linventory = new libinventory();

$Action = trim($Action);

if($Action == 'ShowImportantNote')
{
	$layer_content  = "<table width='100%' border='0' cellspacing='3' cellpadding='3'>";
	$layer_content .= "<tr>";
	$layer_content .= "<td><font color='red'><u><b>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Title']."</b></u></font></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Content']."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</table>";
	
	echo $layer_content;
}
else if ($Action == 'Special_Timetable_RepeatDay_Weekday_Checkboxes_Table') {
	$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];
	
	echo $lcycleperiods_ui->Get_Weekday_Checkboxes_Table($SpecialTimetableSettingsID);
}
else if ($Action == 'Special_Timetable_RepeatDay_CycleDay_Checkboxes_Table') {
	$TimezoneID = $_REQUEST['TimezoneID'];
	$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];
	
	echo $lcycleperiods_ui->Get_Cycle_Day_Checkboxes_Table($TimezoneID, $SpecialTimetableSettingsID);
}
else if ($Action == 'Special_Timetable_Selected_Dates_Table') {
	$TimezoneID = $_REQUEST['TimezoneID'];
	$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];
	$IsInitial = $_REQUEST['IsInitial'];
	$RepeatType = $_REQUEST['RepeatType'];
	$RepeatDayList = $_REQUEST['RepeatDayList'];
	$RepeatDayArr = explode(',', $RepeatDayList);
	
	echo $lcycleperiods_ui->Get_Special_Timetable_Date_Selection_Table($TimezoneID, $RepeatType, $RepeatDayArr, $SpecialTimetableSettingsID, $IsInitial);
}
else if ($Action == 'Edit_Timezone_Option_Layer') {
	$TimezoneID = $_REQUEST['TimezoneID'];
	$Date = $_REQUEST['Date'];
	
	echo $lcycleperiods_ui->Get_Edit_Timezone_Option_Layer_Table($TimezoneID, $Date);
}
else if ($Action == 'Timetable_Selection') {
	$TimezoneID = $_REQUEST['TimezoneID'];
	$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];
	$RepeatType = $_REQUEST['RepeatType'];	
	$RepeatDayList = $_REQUEST['RepeatDayList'];
	
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	### Get TimeZone Info
	$TimezoneInfoArr = $lcycleperiods->returnPeriods_NEW($TimezoneID);
	$TimezoneStartDate = $TimezoneInfoArr[0]['PeriodStart'];
	$PeriodDays = $TimezoneInfoArr[0]['PeriodDays'];
	
	$TimezoneAcademicInfoArr = $lcycleperiods->Get_Academic_Year_And_Term_Info_By_TimeZone($TimezoneID);
	$AcademicYearID = $TimezoneAcademicInfoArr[0]['AcademicYearID'];
	$YearTermID = $TimezoneAcademicInfoArr[0]['YearTermID'];
	
	### Get value based on selected options in UI
	if ($RepeatDayList == '') {
		$MaxCycleDay = 0;
	}
	else {
		$RepeatDayArr = explode(',', $RepeatDayList);
		$MaxCycleDay = $RepeatDayArr[count($RepeatDayArr)-1];
		if ($RepeatType == 'weekday' && in_array(0, $RepeatDayArr)) {
			$MaxCycleDay = 7;
		}
		else if ($RepeatType == 'cycle') {
			$MinCycleDay = $MaxCycleDay;
			$MaxCycleDay = $PeriodDays;  
		}
	}
		
	
	### If initialize -> Get Special Settings Info
	if ($SpecialTimetableSettingsID) {
		$SpecialTimetableSettingsInfoArr = $lcycleperiods->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
		
		$TimetableID = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['TimetableID'];
		$RepeatType = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['RepeatType'];
		$RepeatDayInfoArr = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['RepeatDayInfo'];
		$RepeatDayArr =  Get_Array_By_Key($RepeatDayInfoArr, 'RepeatDay');
		$MaxCycleDay = $RepeatDayArr[count($RepeatDayArr)-1];
		if ($RepeatType == 'weekday' && in_array(0, $RepeatDayArr)) {
			$MaxCycleDay = 7;
		}
		else if ($RepeatType == 'cycle') {
			$MinCycleDay = $MaxCycleDay;
			$MaxCycleDay = $PeriodDays;  
		}
	}
	
	
	### Get the timetable selection
	$ApplicableTimetableInfoArr = getAvailableTimetableTemplateWithDaysChecking($AcademicYearID, $YearTermID, $TimezoneStartDate, $RepeatType, $MaxCycleDay, $MinCycleDay);
	$ApplicableTimetableIDArr = Get_Array_By_Key($ApplicableTimetableInfoArr, 'TimeTableID');
	unset($ApplicableTimetableInfoArr);
	
	echo $libtimetable_ui->Get_Timetable_Selection('TimetableID', $AcademicYearID, $YearTermID, $TimetableID, $OnChange='', $noFirst=0, $ParFirstTitle='', $ShowAllocatedOnly=0, $ExcludeTimetableID='', $ApplicableTimetableIDArr);
}
else if($Action == 'load_reference'){
	if($flag == 'groupIDType'){
		$groupInfo = $lgroup->returnAllGroupIDAndName('AND (RecordType=0 OR AcademicYearID='.$AcademicYearID.')',Get_Lang_Selection('TitleChinese','Title'));
		$titleArray = array($Lang['General']['Code'],$Lang['Group']['Group']);
		$countOfGroup = count($groupInfo);
		
		for($i=0;$i<$countOfGroup;$i++){
			$contentArray[] = array($groupInfo[$i]['GroupID'],Get_Lang_Selection($groupInfo[$i]['TitleChinese'],$groupInfo[$i]['Title']));
		}
		
		$output = $linterface->getReferenceThickBox($titleArray,$contentArray,getAYNameByAyId($AcademicYearID).' '.$i_admintitle_am_group, 'GroupID');
	}
	
	echo $output;
}

else if($Action == 'load_Site_Code'){
    if($flag == 'locationID'){
        $sql = "SELECT ".$linventory->getInventoryNameByLang("a.").", ".$linventory->getInventoryNameByLang("b.").", ".
                $linventory->getInventoryNameByLang("c.").", CONCAT(a.Code,'>',b.Code,'>',c.Code) 
                FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) 
                INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) 
                WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";

        $result = $linventory->returnArray($sql);
//         debug_pr($sql);
//         debug_pr($result);

//         $groupInfo = $lgroup->returnAllSiteInfo('AND (RecordType=0 OR AcademicYearID='.$AcademicYearID.')',Get_Lang_Selection('TitleChinese','Title'));
//         debug_pr($lgroup->returnAllSiteInfo('AND (RecordType=0 OR AcademicYearID='.$AcademicYearID.')',Get_Lang_Selection('TitleChinese','Title')));
        $titleArray = array($Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueCode'], $Lang['SysMgr']['Location']['Building'], $Lang['SysMgr']['Location']['Floor'], $Lang['SysMgr']['Location']['Room']);
        $countOfSite = count($result);
        for($i=0;$i<$countOfSite;$i++){
            list($buliding_name, $floor_name, $room_name, $code) = $result[$i];
            $contentArray[] = array($code, $buliding_name, $floor_name, $room_name);
        }
        $output = $linterface->getReferenceThickBox($titleArray,$contentArray,getAYNameByAyId($AcademicYearID), 'Site');
    }
    echo $output;
}

intranet_closedb();
?>