<?php
## using by : 
/*
 * Modification Log:
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

### Get Page Variable
$AcademicYearID = $_REQUEST['AcademicYearID'];
$SpecialTimetableSettingsID = $_REQUEST['SpecialTimetableSettingsID'];

$lcycleperiods = new libcycleperiods();
$Success = $lcycleperiods->Delete_Special_Timetable_Settings($SpecialTimetableSettingsID);
$ReturnMsgKey = ($Success)? 'SpecialTimetableSettingsDeleteSuccess' : 'SpecialTimetableSettingsDeleteFailed';

intranet_closedb();
header("Location: time_zone.php?AcademicYearID=$AcademicYearID&ReturnMsgKey=$ReturnMsgKey");
?>