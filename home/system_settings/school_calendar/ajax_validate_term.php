<?php
// using 
################# Change Log [Start] #####
#
#	Date	:	2016-01-04	Omas
# 				add pram $RecordType for current and future term editing
#				add return -5 for current term
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();
$lcycleperiods = new libcycleperiods();

# Get data
$EditMode = stripslashes($_REQUEST['EditMode']);
$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
$ChiTermName = stripslashes($_REQUEST['TermTitleCh']);
$EngTermName = stripslashes($_REQUEST['TermTitleEn']);

if($EditMode == 1)
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
else
	$YearTermID = "";

$TermStartDate = stripslashes($_REQUEST['TermStartDate']);
$TermEndDate = stripslashes($_REQUEST['TermEndDate']);

$lcycleperiods = new libcycleperiods();

//if($lcycleperiods->CheckTermIsConsistency($AcademicYearID,$TermStartDate,$TermEndDate))
//{
//	if(!$lcycleperiods->checkTermOverlap($YearTermID,$TermStartDate,$TermEndDate))
//	{
//		if(!$lcycleperiods->CheckDuplicateChiTermName($AcademicYearID, $YearTermID, $ChiTermName))
//		{
//			if(!$lcycleperiods->CheckDuplicateEngTermName($AcademicYearID, $YearTermID, $EngTermName))
//			{
//				echo 1;
//			}
//			else
//			{
//				echo -3;
//			}
//		}
//		else
//		{
//			echo -2;
//		}
//	}
//	else
//	{
//		echo -1;
//	}
//}
//else
//{
//	echo -4;
//}
if($RecordType=='current'){
$endDateTimestamp = strtotime($TermEndDate);
$today = strtotime(date('Y-m-d'));
}

# edit mode current term check end date is > today
if($RecordType==''|| $RecordType=='future' || $RecordType=='current' && $endDateTimestamp >= $today)
{
	if(!$lcycleperiods->checkTermOverlap($YearTermID,$TermStartDate,$TermEndDate))
	{
		if(!$lcycleperiods->CheckDuplicateChiTermName($AcademicYearID, $YearTermID, $ChiTermName))
		{
			if(!$lcycleperiods->CheckDuplicateEngTermName($AcademicYearID, $YearTermID, $EngTermName))
			{
				if($lcycleperiods->CheckTermIsConsistency($AcademicYearID,$TermStartDate,$TermEndDate))
				{
					echo 1;
				}
				else {
					echo -4;
				}
			}
			else
			{
				echo -3;
			}
		}
		else
		{
			echo -2;
		}
	}
	else
	{
		echo -1;
	}
}
else
{
	echo -5;
}
	
intranet_closedb();
?>