<?php 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();
########################################################################################################
$lc = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$action = (isset($action) && $action != '') ? $action : '';




$sql = "SELECT SportID,SportChiName,SportEngName,IconPath
                    FROM SFOC_SPORTS_TYPE";
$result= $lc->returnArray($sql);

if(!empty($result)){
    foreach ($result as $row){
        $data[] = array('value' => handle_chars($row[0]), 'title' => handle_chars($row[0]));
    }
}



function handle_chars($str){
    $x = '';
    if($str != ''){
        $x = str_replace("\\", "\\\\", $str);
    }
    return $x;
}

echo json_encode($data);
?>