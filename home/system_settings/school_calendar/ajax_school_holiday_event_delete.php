<?php	
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$lcycleperiods = new libcycleperiods();

$lcycleperiods->Start_Trans();

$result = $lcycleperiods->deleteSchoolHolidayEvent($EventID,$DeleteRelatedEvent);

//$lcycleperiods->generateProduction();

if (!in_array(false,$result)) {
	echo 1;
	$lcycleperiods->Commit_Trans();
}
else {
	echo 0;
	$lcycleperiods->RollBack_Trans();
}
intranet_closedb();
?>