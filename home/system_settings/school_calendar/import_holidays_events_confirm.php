<?php

//using : 

#####################################
#   Date:   2018-01-09 Anna
#           added NatureEng DescriptionEng for general
#
# 	Date: 	2015-09-15 Ivan [U84654] [ip.2.5.6.10.1]
#			enable timezone settings for KIS if purchased eBooking module
#
#	Date:	2013-10-07	Carlos
#			add "Class Group Code"
#
#	Date:	2011-07-06	YatWoon
#			add "isSkipSAT" and "isSkipSUN"
#
#####################################
set_time_limit(86400);
ini_set("memory_limit", "512M");
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$linterface = new interface_html();
$limport = new libimporttext();
$lo = new libfilesystem();
$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];

$CurrentPageArr['SchoolCalendar'] = 1;
### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php",1);

//$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
$showTimezone = true;
if ($_SESSION["platform"]=="KIS" && !$plugin['eBooking'] || $sys_custom['LivingHomeopathy'] || $sys_custom['HideTimeZone']){
	// kis without eBooking => hide timezone settings
	$showTimezone = false;
}
if ($showTimezone) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php",0);
}
$showAcademicYear = true;
if ($sys_custom['LivingHomeopathy'] || $sys_custom['HideAcademicYearTerm']) {
	$showAcademicYear = false;
}
if ($showAcademicYear) {
	$TAGS_OBJ[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php",0);
}
$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'];

?>
<?
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_holidays_events.php?xmsg=".urlencode($Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['IncorrectFileExtention']));
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data)) {
    $col_name = array_shift($data);
}

## check csv title row is correct or not ##
//$file_format = array("Start Date","End Date","Type","Title","Venue","Nature","Description","Skip School Day","Group ID", "Skip Saturday", "Skip Sunday");
if($sys_custom['eClassApp']['SFOC']){
//     $file_format = array("Start Date","End Date","Type","Sport Type", "Association", "AssociationEng","Title", "TitleEng","Venue", "VenueEng","Nature","NatureEng","Description","DescriptionEng","Skip School Day","Group ID", "Skip Saturday", "Skip Sunday", "Email", "ContactNo", "Website");
    $file_format = array("Start Date","End Date","Type", "Association", "AssociationEng","Title", "TitleEng","Venue", "VenueEng","Nature","NatureEng","Description","DescriptionEng","Skip School Day","Group ID", "Skip Saturday", "Skip Sunday", "Email", "ContactNo", "Website");
} else {
    $file_format = array("Start Date","End Date","Type","Title", "TitleEng","Venue", "VenueEng","Nature","NatureEng","Description","DescriptionEng","Skip School Day","Group ID", "Skip Saturday", "Skip Sunday");
}
if($isKIS){
	$file_format = array_merge($file_format, array("Class Group Code"));
}
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import_holidays_events.php?xmsg=".urlencode($Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['InvalidFileFormat']));
	exit();
}
$linterface->LAYOUT_START($returnMsg); 
echo $lcycleperiods_ui->getImportHolidaysEventsConfirmUI($AcademicYearID, $data);
?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>