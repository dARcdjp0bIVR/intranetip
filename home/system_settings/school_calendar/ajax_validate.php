<?php
// using : yat

##########################################################
#
# Date:	2011-09-20 YatWoon
#		- add Action "Check_skip_sat_sun"
#
##########################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();
 
$lcycleperiods = new libcycleperiods();

$Action = trim($Action);
if ($Action == 'Check_Affect_Special_Timetable_Settings') {
	$StartDate = $_POST['StartDate'];
	$EndDate = $_POST['EndDate'];
	
	// Get the involved Time Zones of the date range
	$AffectSpecialTimetable = $lcycleperiods->Has_Special_Timetable_Settings_With_Cycle_Day($StartDate, $EndDate);
	
	echo ($AffectSpecialTimetable)? '1' : '0';
}

## check skip sat/san
if ($Action == 'Check_skip_sat_sun') {
	$StartDate = $_POST['StartDate'];
	$EndDate = $_POST['EndDate'];
	
	$NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 ) + 1;
	$Enable = 1;
	
	if($NumOfDay==1)	$Enable=0;
	if($NumOfDay==2)
	{
		# check the first day is sat or not
		# sat > skip check isSkip
		if(date("w",strtotime($StartDate))==6)	$Enable=0;
	}	
	
	echo $Enable;
}

intranet_closedb();
?>