<?php	
// using
/**
 * Date : 2018-08-15 (Henry)
 * fixed sql injection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lcycleperiods_ui = new libcycleperiods_ui();

$EventID = IntegerSafe($EventID);

echo $lcycleperiods_ui->editHolidayEventForm($EventID,$ShowEndDate,$CallFrom); 

intranet_closedb();
?>