<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	$_SESSION['DHL_COMPANY_RETURN_MSG'] =$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$libdhl = new libdhl();

$libdhl->Start_Trans();
$success = $libdhl->deleteCompanyRecord($_POST['CompanyID']);

if($success){
	$libdhl->Commit_Trans();
}else{
	$libdhl->RollBack_Trans();
}

$_SESSION['DHL_COMPANY_RETURN_MSG'] = $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];

intranet_closedb();
header("Location:index.php");
exit;
?>