<?php
// Editing by
/*
 * 2019-08-02 (Bill): clear department records using department and selected users as basis   [2019-0729-1711-21226]
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

$add_update_key = isset($_POST['DepartmentID']) && $_POST['DepartmentID']>0 ? 'Update' : 'Add';

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	$_SESSION['DHL_DEPARTMENT_RETURN_MSG'] =$Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}

$libdhl = new libdhl();
$libdhl->Start_Trans();
$success_or_newRecordId = $libdhl->upsertDepartmentRecord($_POST);

$department_id = isset($_POST['DepartmentID']) && $_POST['DepartmentID']>0 ? $_POST['DepartmentID'] : $success_or_newRecordId;
if($department_id != '' && $department_id > 0)
{
	$userIdAry = $libdhl->cleanTargetSelectionData((array)$_POST['DepartmentUserID'],'U');
	$userIdAry = array_values(array_diff($userIdAry,array(0,'')));

	$libdhl->deleteDepartmentUserRecord($department_id, '');
	// [2019-0729-1711-21226]
    $libdhl->deleteDepartmentUserRecord('', $userIdAry);
	$libdhl->insertDepartmentUserRecord($department_id, $userIdAry);

	$PICIdAry = $libdhl->cleanTargetSelectionData((array)$_POST['DepartmentPICID'],'U');
	$PICIdAry= array_values(array_diff($PICIdAry,array(0,'')));
	$libdhl->deletePICRecord('Department',$department_id);
	$libdhl->insertPICRecord('Department',$department_id, $PICIdAry);
}

if($success_or_newRecordId){
	$libdhl->Commit_Trans();
}else{
	$libdhl->RollBack_Trans();
}

$_SESSION['DHL_DEPARTMENT_RETURN_MSG'] = $success_or_newRecordId? $Lang['General']['ReturnMessage'][$add_update_key.'Success'] : $Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];

intranet_closedb();

header("Location:index.php");
exit;
?>