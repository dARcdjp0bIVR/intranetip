<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$cookie_expire_time = $clearCoo == 1? time() - 999999 : 0;

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, $cookie_expire_time, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_dhl_division_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_dhl_division_page_number", $pageNo, $cookie_expire_time, "", "", 0);
	$ck_dhl_division_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_dhl_division_page_number!="")
{
	$pageNo = $ck_dhl_division_page_number;
}

if ($ck_dhl_division_page_order!=$order && $order!="")
{
	setcookie("ck_dhl_division_page_order", $order, $cookie_expire_time, "", "", 0);
	$ck_dhl_division_page_order = $order;
} else if (!isset($order) && $ck_dhl_division_page_order!="")
{
	$order = $ck_dhl_division_page_order;
}

if ($ck_dhl_division_page_field!=$field && $field!="")
{
	setcookie("ck_dhl_division_page_field", $field, $cookie_expire_time, "", "", 0);
	$ck_dhl_division_page_field = $field;
} else if (!isset($field) && $ck_dhl_division_page_field!="")
{
	$field = $ck_dhl_division_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();
$libdhl = new libdhl();

$li = new libdbtable2007($field, $order, $pageNo);
//,'picView'=>1
$filterMap = array('GetDBQuery'=>1,'libdbtable'=>$li);
if(isset($Keyword) && trim($Keyword)!=''){
	$filterMap['Keyword'] = $Keyword;
}
$db_query_info = $libdhl->getDivisionRecords($filterMap);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}

$tool_buttons = array();
$tool_buttons[] = array('edit','javascript:checkEdit(document.form1,\'DivisionID[]\',\'edit.php\');','','edit_btn');
$tool_buttons[] = array('delete','javascript:checkRemove(document.form1,\'DivisionID[]\',\'delete.php\');','','delete_btn');

$CurrentPageArr['OrganizationSettings'] = 1;
$CurrentPage = 'Division';

### Title ###
$PAGE_NAVIGATION[] = array($Lang['DHL']['Division'], "");
$TAGS_OBJ = $libdhl->getTagsObjArr($CurrentPage);
$MODULE_OBJ['title'] = $Lang['DHL']['Organization'];

if(isset($_SESSION['DHL_DIVISION_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['DHL_DIVISION_RETURN_MSG'];
	unset($_SESSION['DHL_DIVISION_RETURN_MSG']);
}
$linterface->LAYOUT_START($ReturnMsg); 
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
<br style="clear:both;">
<form id="form1" name="form1" method="POST">
<div class="table_board">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$linterface->GET_LNK_NEW('edit.php', $Lang['Btn']['New'], $___ParOnClick="", $___ParOthers="", $___ParClass="", $____useThickBox=1)?>
		</div>
		<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="<?=intranet_htmlspecialchars(stripslashes($Keyword))?>" onkeyup="GoSearch(event);"></div>
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		
	</div>
	<br style="clear:both">	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			
		</td>
	</tr>
</table>
</div>
<input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />
<input type="hidden" name="field" value="<?=$li->field?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<br />
<script type="text/javascript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>