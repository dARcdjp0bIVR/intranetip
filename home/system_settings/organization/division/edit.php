<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($sys_custom['DHL'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$libdhl = new libdhl();

$is_edit = isset($_REQUEST['DivisionID']) && $_REQUEST['DivisionID']!=''? 1:0;

if($is_edit){
 	$records = $libdhl->getDivisionRecords(array('DivisionID'=>$_REQUEST['DivisionID']));
 	$record = $records[0];
 	$DivisionID = $record['DivisionID'];
 	$divisionCompanyAry = $libdhl->getDivisionCompanyRecords(array('DivisionID'=>$record['DivisionID']));
 	$companyIdAry = Get_Array_By_Key($divisionCompanyAry,'CompanyID');
 	
 	$divisionPICAry = $libdhl->getPICRecords('Division',$record['DivisionID']);
 	
 	$PICIdAry= Get_Array_By_Key($divisionPICAry,'UserID');
 	
 	$hidden_fields = '<input type="hidden" name="DivisionID" id="DivisionID" value="'.$record['DivisionID'].'" />'."\n";
}else{
	$record = array();
	$companyIdAry = array();
	$PICIdAry = array();
}

$CurrentPageArr['OrganizationSettings'] = 1;
$CurrentPage = 'Division';

### Title ###
$PAGE_NAVIGATION[] = array($Lang['DHL']['Division'], "index.php");
$PAGE_NAVIGATION[] = array($is_edit? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], "");
$TAGS_OBJ = $libdhl->getTagsObjArr($CurrentPage);
$MODULE_OBJ['title'] = $Lang['DHL']['Division'];

$linterface->LAYOUT_START(); 
?>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
<br />
<form name="form1" id="form1" action="edit_update.php" method="post" onsubmit="return false;">
<?=$hidden_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<thead>
	<col class="field_title">
	<col class="field_c">
	</thead>
	<tbody>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['Chinese'].')'?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("DivisionNameChi", "DivisionNameChi", $record['DivisionNameChi'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("DivisionNameChi_Error",'', "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['DivisionName'].'('.$Lang['DHL']['English'].')'?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("DivisionNameEng", "DivisionNameEng", $record['DivisionNameEng'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("DivisionNameEng_Error",'', "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['Code']?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTBOX_NAME("DivisionCode", "DivisionCode", $record['DivisionCode'], '', array('maxlength'=>255))?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("DivisionCode_Error",'', "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['RelatedCompany']?> <span class="tabletextrequire">*</span>
			</td>
			<td width="70%">
				<?=$libdhl->getCompanySelection("CompanyID", "CompanyID[]", $companyIdAry, $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="8" ')?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("CompanyID_Error",'', "WarnMsg")?>
			</td>
		</tr>
		
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['DivisionPIC']?>
			</td>
			<td width="70%">
				<table class="inside_form_table" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<?=$libdhl->getPICSelection('Division',$record['DivisionID']!=''? $record['DivisionID']:0, "DivisionPICID[]", "DivisionPICID[]", $PICIdAry , $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="12" ',$valuePrefix='', $hasOptgroupLabel=false)?>
						</td>
						<td>
							<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=DivisionPICID[]&DHLSelectGroup=1&isAdmin=1&SelectStaffOnly=1&DivisionID=$DivisionID', 9)")?><br />
							<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:remove_selection_option('DivisionPICID[]', 0);");?>
						</td>
					</tr>
					<tr>
						<?=$linterface->Get_Thickbox_Warning_Msg_Div("PICUserID_Error",'', "WarnMsg")?>						
					</tr>					
				</table>
			</td>
		</tr>
		
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['DHL']['Description']?>
			</td>
			<td width="70%">
				<?=$linterface->GET_TEXTAREA("Description", $record['Description'],80,10)?>
			</td>
		</tr>
	</tbody>
</table>
<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php'",'cancel_btn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
function checkSubmitForm(formObj)
{
	var valid = true;
	$('.WarnMsg').hide();
	$('#submit_btn').attr('disabled',true);
	
	var text_objs = document.getElementsByClassName('textbox_name');
	for(var i=0;i<text_objs.length;i++)
	{
		var id = text_objs[i].id;
		var val = $.trim(text_objs[i].value);
		if(val == ''){
			$('#'+id+'_Error').html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
			valid = false;
		}
	}
	
	if($('#CompanyID option:selected').length == 0){
		valid = false;
		$('#CompanyID_Error').html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
	}
	
	if(valid){
		var form_data = $(formObj).serialize();
		form_data += '&task=CheckDivision';
		$.post(
			'../common_task.php',
			form_data,
			function(returnJson)
			{
				var error_ary = [];
				if(JSON && JSON.parse){
					error_ary = JSON.parse(returnJson);
				}else{
					eval('error_ary='+returnJson+';');
				}
				
				if(error_ary.length > 0){
					valid = false;
					for(var i=0;i<error_ary.length;i++){
						var error_id = error_ary[i][0];
						var error_msg = error_ary[i][1];
						if($('#'+error_id).length>0)
							$('#'+error_id).html(error_msg).show();
					}
					$('#submit_btn').attr('disabled',false);
				}else{
					Select_All_Options('DivisionPICID[]',true);
					formObj.submit();
				}
			}
		);
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}

$(document).ready(function(){
	$($('input[type=text]')[0]).focus();
});
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>