<?php
//using: 
############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = '../../../../../';

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$lsubjectPanel = new libsubjectpanel();

$subjectId = IntegerSafe($_POST['subjectId']);
$formIdAry = (array)IntegerSafe($_POST['formIdAry']);
$teacherIdAry = (array)IntegerSafe($_POST['teacherIdAry']);

if ($subjectId!='' && count($formIdAry)>0 && count($teacherIdAry)>0) {
	$success = $lsubjectPanel->addSubjectPanel($subjectId, $formIdAry, $teacherIdAry); 
	$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';
}
else {
	$returnMsgKey = 'UpdateUnsuccess';
}

header("Location: subject_panel_list.php?returnMsgKey=$returnMsgKey");

intranet_closedb();

?>