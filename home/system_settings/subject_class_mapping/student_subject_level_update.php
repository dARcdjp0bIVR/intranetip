<?php
// Using: 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           Create file
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$lpf_dbs = new libpf_dbs_transcript();

$classId = IntegerSafe($_POST['classId']);
$subjectLevelAry = $_POST['subjectLevel'];
if ($classId > 0 && count($subjectLevelAry) > 0) {
    $success = $lpf_dbs->insertUpdateSubjectLevel($subjectLevelAry, $classId); 
	$returnMsgKey = $success? 'UpdateSuccess' : 'UpdateUnsuccess';
} else {
	$returnMsgKey = 'UpdateUnsuccess';
}

intranet_closedb();

header("Location: student_subject_level.php?returnMsgKey=$returnMsgKey&classId=$classId");
?>