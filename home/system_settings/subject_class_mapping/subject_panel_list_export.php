<?php 
// using:

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libsubjectpanel.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$lexport = new libexporttext();
$SubjectId = standardizeFormPostValue($_POST['SubjectId']);
$keyword = $ldb->Get_Safe_Sql_Like_Query(standardizeFormPostValue($_POST['keyword']));
$order = standardizeFormPostValue($_POST['order']);
$field = standardizeFormPostValue($_POST['field']);

if($field == 0){
    $orderField = 'TeacherName';
}else{
    $orderField = 'SubjectName';
    }

if($order == 0){
    $orderStr = 'desc';
}else{
    $orderStr = 'asc';
    }

$sql ="SELECT 
				IU.EnglishName as TeacherName,
				ASS.EN_DES as SubjectName,
				GROUP_CONCAT(DISTINCT Y.YearName SEPARATOR ',
') AS YearName
			FROM 
				ASSESSMENT_ACCESS_RIGHT AAR
				INNER JOIN INTRANET_USER IU ON (AAR.UserID = IU.UserID)
				INNER JOIN ASSESSMENT_SUBJECT ASS ON (AAR.SubjectID = ASS.RecordID)
				INNER JOIN YEAR Y ON (AAR.YearID = Y.YearID)
			WHERE
				AAR.AccessType = '1' ";
if ($keyword != null ||	$keyword != ''){ 
$sql .="    And (
		     IU.EnglishName Like '%$keyword%'
			 Or ASS.EN_DES Like '%$keyword%'
			 Or Y.YearName Like '%$keyword%'
        ) ";
}
if ($SubjectId != null ||	$SubjectId != ''){
$sql .="	And ASS.RecordID = '$SubjectId'"; 
}
$sql .="		GROUP BY
				AAR.UserID,
				AAR.SubjectID
                Order by $orderField $orderStr
";

$result = $ldb->returnResultSet($sql);
//debug_pr($sql);
$headerAry = array();
$headerAry[] = "Teacher Name";
$headerAry[] = "Subject";
$headerAry[] = "Form";

$dataAry = array();
 for($i=0;$i<count($result);$i++){
     $dataAry[$i][0] = $result[$i]['TeacherName'];
     $dataAry[$i][1] = $result[$i]['SubjectName'];
     $dataAry[$i][2] = $result[$i]['YearName'];
 }
 
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
 
$fileName = 'subject_panel_list.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);

?>