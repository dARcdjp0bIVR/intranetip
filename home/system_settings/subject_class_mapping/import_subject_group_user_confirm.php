<?
// Modifying by: 

/******************************************************
 * Modification log:
 *	Date:	2020-05-07 Tommy
 *          - modified access checking, added $plugin["Disable_Subject"]
 * 22-11-2016 (Villa) [T108937]
 * 		- add params to ajaxValid to enable import STRN
 * 23-10-2015 (Pun)
 * 		- added hidden input "addStd" for sync subject group to eClass
 * 21-06-2011 Marcus:
 * 		- add param YearTermID to iFrame src, 
 * 		YearTermID must be passed after cater same class code in different term
 * ****************************************************/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];

$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_YearTerm = new academic_year_term($YearTermID);
$libSCM = new subject_class_mapping();
	
$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_subject_group_user.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=import_failed");
	exit();
}

### move move to temp folder for ajax validation
$folder_prefix = $intranet_root."/file/import_temp/subject_group_user/".$_SESSION['UserID'];
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFilePath = stripslashes($folder_prefix."/".date('Ymd_His').$ext);
$success['move'] = $lo->lfs_move($csvfile, $TargetFilePath);

$data = $limport->GET_IMPORT_TXT($TargetFilePath);
$col_name = array_shift($data);
$numOfData = count($data);
# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

if($import_method){
	$file_format = array("Subject Group Code", "STRN");
}else{
	$file_format = array("Subject Group Code", "User Login ID");
}
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_subject_group_user.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."&xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_USER_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		UserLogin varchar(100),
		IsTeacher tinyint(1),
		SubjectGroupCode varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());

# delete the temp data in temp table 
$sql = "delete from TEMP_SUBJECT_GROUP_USER_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());



$InfoTable = '';
$InfoTable .= '<table class="form_table_v30" style="width:90%;">'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['SchoolYear'].'</td>'."\n";
		$InfoTable .= '<td>'.$obj_AcademicYear->Get_Academic_Year_Name().'</td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
		$InfoTable .= '<td>'.$obj_YearTerm->Get_Year_Term_Name().'</td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$InfoTable .= '<td><div id="SuccessCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
	$InfoTable .= '</tr>'."\n";
	$InfoTable .= '<tr>'."\n";
		$InfoTable .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$InfoTable .= '<td><div id="FailCountDiv">'.$Lang['General']['EmptySymbol'].'</div></td>'."\n";
	$InfoTable .= '</tr>'."\n";
$InfoTable .= '</table>'."\n";

### Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfData, $Lang['SysMgr']['Timetable']['RecordsValidated']);
$ProcessingMsg .= '</span>';


$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_subject_group_user.php?AcademicYearID=".$AcademicYearID."&YearTermID=".$YearTermID."'");
$ImportBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit");

$thisSrc = "ajax_validate.php?Action=Import_Subject_Group_User&AcademicYearID=".$AcademicYearID."&TargetFilePath=".$TargetFilePath."&YearTermID=".$YearTermID."&ImportMethod=".$import_method;
$ImportLessonIFrame = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";

?>

<br />
<form name="form1" method="post" action="import_subject_group_user_update.php">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td align="center">
			<?=$InfoTable?>
			<br style="clear:both;" />
			<div id="ErrorTableDiv"></div>
		</td>
	</tr>
			
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<span id="ImportButtonDiv" style="display:none;"><?=$ImportBtn?></span>
					<span><?=$BackBtn?>&nbsp;</span>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<?=$ImportLessonIFrame?>

	<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
	<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">
	<input type="hidden" name="addStd" value="<?=$addStd?>">

</form>
<br />

<script language="javascript">
	$('document').ready(function () {
		Block_Document('<?=$ProcessingMsg?>');
	});
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>