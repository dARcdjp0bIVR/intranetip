<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$SubjectID = stripslashes($_REQUEST['SubjectID']);
$LearningCategoryID = stripslashes($_REQUEST['LearningCategoryID']);
$isComponent = stripslashes($_REQUEST['isComponent']);
$ParentSubjectID = stripslashes($_REQUEST['ParentSubjectID']);

$SubjecClassUI = new subject_class_mapping_ui();
$returnDiv = $SubjecClassUI->Get_Subject_Add_Edit_Layer($SubjectID, $LearningCategoryID, $isComponent, $ParentSubjectID);

echo $returnDiv;

intranet_closedb();
?>