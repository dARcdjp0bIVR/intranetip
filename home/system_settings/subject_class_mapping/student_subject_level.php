<?php
// Using: 

############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
#   Date:   2018-08-29 (Bill)   [2017-1207-0956-53277]
#           Create file
# 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/dbs_transcript/libpf_dbs_transcript.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lfcm_ui = new form_class_manage_ui();
$lscm_ui = new subject_class_mapping_ui();

// $lscm = new subject_class_mapping();
$lpf_dbs = new libpf_dbs_transcript();

# Form & Class
$academicYearId = Get_Current_Academic_Year_ID();
$classId = $_POST['classId']? $_POST['classId'] : $_GET['classId'];
$classId = IntegerSafe(standardizeFormPostValue($classId));
if(!$classId)
{
    $year_class = new year_class();
    $class_list = $year_class->Get_All_Class_List($academicYearId);
    $classId = $class_list[0]['YearClassID'];
}
$year_class = new year_class($classId, true, false, true);
$yearId = $year_class->YearID;

# Curriculum & Subject Level
$curriculum = '--';
$subjectLevelAry = array();
$subjectLevelAry[''][''] = 'N.A.';
$allCurriculumAry = $lpf_dbs->getCurriculumSetting('','','','',$ipf_cfg["DSBTranscript"]["GENERAL"]["Status"]["Active"]);
foreach($allCurriculumAry as $thisCurriculumInfo)
{
    if(trim($thisCurriculumInfo['SubjectLevel']) != '')
    {
        $thisSubjectLevelAry = explode('<br />', nl2br(trim($thisCurriculumInfo['SubjectLevel'])));
        $thisSubjectLevelAry = array_values(array_unique(array_filter(Array_Trim($thisSubjectLevelAry))));
        foreach($thisSubjectLevelAry as $thisSubjectLevel) {
            $subjectLevelAry[$thisCurriculumInfo['Code']][trim($thisSubjectLevel)] = trim($thisSubjectLevel);
        }
    }
}
$curriculumMappingAry = BuildMultiKeyAssoc($allCurriculumAry, 'CurriculumID', 'Code', 1, 0);
if(isset($curriculumMappingAry[$year_class->CurriculumID])) {
    $curriculum = $curriculumMappingAry[$year_class->CurriculumID];
}

# Class Students
$classStudentAry = $year_class->ClassStudentList;
$numOfStudent = count($classStudentAry);

# Form Subjects
$subjects = new subject();
$formSubjectAry = $subjects->Get_Subject_By_Form($yearId);
$numOfSubject = count($formSubjectAry);

# Student Subject Level
$studentIDAry = Get_Array_By_Key((array)$classStudentAry, 'UserID');
$studentSubjectLevelAry = $lpf_dbs->getSubjectLevel('', '', $studentIDAry);
$studentSubjectLevelAry = BuildMultiKeyAssoc($studentSubjectLevelAry, array('StudentID', 'SubjectID'), 'SubjectLevel', 1, 0);

# Class Selection
$htmlAry["classSelection"] = $lfcm_ui->Get_Class_Selection($academicYearId, '', 'classId', $classId, 'document.form1.submit()', 1);

# View Content Table
## Table Header
$displayContent = "";
$displayContent .= "<table class='common_table_list display_table' width='100%' cellpadding='4' cellspacing='0'>";
$displayContent .= "<tr>";
$displayContent .= "<thead>";
if(empty($formSubjectAry))
{
    $displayContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['General']['ClassNumber']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['General']['StudentName']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['SysMgr']['FormClassMapping']['Curriculum']."</th>";
}
else
{
    // Table Column Style
    $colCount = $numOfSubject? $numOfSubject : 1;
    $colWidth = 75 / $colCount;
    $colWidth = (int)$colWidth."%";
    
    $displayContent .= "<th class='tablegreentop tabletopnolink' nowrap>".$Lang['General']['ClassNumber']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink tablescrollcell' nowrap>".$Lang['General']['StudentName']."</th>";
    $displayContent .= "<th class='tablegreentop tabletopnolink' nowrap>".$Lang['SysMgr']['FormClassMapping']['Curriculum']."</th>";
    foreach($formSubjectAry as $thisFormSubject)
    {
        $displayContent .= "<th class='tablegreentop tabletopnolink' nowrap>";
            $displayContent .= Get_Lang_Selection($thisFormSubject["CH_DES"], $thisFormSubject["EN_DES"]);
        $displayContent .= "</th>";
    }
}
$displayContent .= "</thead>";
$displayContent .= "</tr>";

## Table Content
$displayContent .= "<tbody>";
if(empty($classStudentAry))
{
    $colCount = $numOfSubject? $numOfSubject : 1;
    
    // No Record
    $displayContent .= "<tr>";
        $displayContent .= "<td colspan='".($colCount + 3)."' align='center'>";
            $displayContent .= $no_record_msg;
        $displayContent .= "</td>";
    $displayContent .= "</tr>";
}
else
{
    foreach($classStudentAry as $thisStudentInfo)
    {
        $thisStudentCurriculum = $curriculum;
        $studentCurriculumSetting = $lpf_dbs->getStudentCurriculumSettings($thisStudentInfo['UserID']);
        if(!empty($studentCurriculumSetting) && $studentCurriculumSetting[0]['CurriculumID'] && isset($curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']])) {
            $thisStudentCurriculum = $curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']];
        }
        
        $displayContent .= "<tr>";
            $displayContent .= "<th class='tabletext'>".$thisStudentInfo['ClassNumber']."</th>";
            $displayContent .= "<th class='tabletext tablescrollcell2'>".$thisStudentInfo['StudentName']."</th>";
            $displayContent .= "<th class='tabletext'>".$thisStudentCurriculum."</th>";

        foreach($formSubjectAry as $thisSubjectInfo)
        {
            $selected_value = $studentSubjectLevelAry[$thisStudentInfo["UserID"]][$thisSubjectInfo["RecordID"]];
            $selected_value = $selected_value? $selected_value : 'N.A.';
            $displayContent .= "<td class='tabletext'>".$selected_value."</td>";
        }
        $displayContent .= "</tr>";
    }
}
$displayContent .= "</tbody>";
$displayContent .= "</table>";
//$displayContent .= "</div>";
$htmlAry["displayTable"] = $displayContent;

# Edit Content Table
## Table Header
$tableContent = "";
$tableContent .= "<table class='common_table_list edit_table' width='100%' cellpadding='4' cellspacing='0' style='display: none'>";
$tableContent .= "<thead>";
$tableContent .= "<tr>";
if(empty($formSubjectAry))
{
    $tableContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['General']['ClassNumber']."</th>";
    $tableContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['General']['StudentName']."</th>";
    $tableContent .= "<th class='tablegreentop tabletopnolink'>".$Lang['SysMgr']['FormClassMapping']['Curriculum']."</th>";
}
else
{
    // Table Column Style
    $colCount = $numOfSubject? $numOfSubject : 1;
    $colWidth = 75 / $colCount;
    $colWidth = (int)$colWidth."%";

    $tableContent .= "<th class='tablegreentop tabletopnolink' nowrap>".$Lang['General']['ClassNumber']."</th>";
    $tableContent .= "<th class='tablegreentop tabletopnolink tablescrollcell' nowrap>".$Lang['General']['StudentName']."</th>";
    $tableContent .= "<th class='tablegreentop tabletopnolink' nowrap>".$Lang['SysMgr']['FormClassMapping']['Curriculum']."</th>";
    foreach($formSubjectAry as $thisFormSubject)
    {
        $select_tag = " id='subjectLevel_".$thisFormSubject["RecordID"]."' onChange='applyAllOption(\"".$thisFormSubject["RecordID"]."\", this.value)' ";
        $subjectLevelSelection = getSelectByAssoArray($subjectLevelAry, $select_tag, '', 0, 0);

        $tableContent .= "<th class='tablegreentop tabletopnolink' nowrap>";
            $tableContent .= Get_Lang_Selection($thisFormSubject["CH_DES"], $thisFormSubject["EN_DES"]).'<br/>'.$subjectLevelSelection;
        $tableContent .= "</th>";
    }
}
$tableContent .= "</tr>";
$tableContent .= "</thead>";

## Table Content
$tableContent .= "<tbody>";
if(empty($classStudentAry))
{
    $colCount = $numOfSubject ? $numOfSubject : 1;

    // No Record
    $tableContent .= "<tr>";
        $tableContent .= "<td colspan='".($colCount + 3)."' align='center'>";
            $tableContent .= $no_record_msg;
        $tableContent .= "</td>";
    $tableContent .= "</tr>";
}
else
{
    $tr_count = 0;
    foreach($classStudentAry as $thisStudentInfo)
    {
        $thisStudentCurriculum = $curriculum;
        $studentCurriculumSetting = $lpf_dbs->getStudentCurriculumSettings($thisStudentInfo['UserID']);
        if(!empty($studentCurriculumSetting) && $studentCurriculumSetting[0]['CurriculumID'] && isset($curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']])) {
            $thisStudentCurriculum = $curriculumMappingAry[$studentCurriculumSetting[0]['CurriculumID']];
        }

        $tableContent .= "<tr>";
            $tableContent .= "<th class='tabletext'>".$thisStudentInfo['ClassNumber']."</th>";
            $tableContent .= "<th class='tabletext tablescrollcell2'>".$thisStudentInfo['StudentName']."</th>";
            $tableContent .= "<th class='tabletext'>".$thisStudentCurriculum."</th>";

            $td_count = 0;
            foreach($formSubjectAry as $thisSubjectInfo)
            {
                $select_tag = " id='subjectLevel[$tr_count][$td_count]' name='subjectLevel[".$thisStudentInfo["UserID"]."][".$thisSubjectInfo["RecordID"]."]' ";
                $selected_value = $studentSubjectLevelAry[$thisStudentInfo["UserID"]][$thisSubjectInfo["RecordID"]];

                $subjectLevelSelection = getSelectByAssoArray($subjectLevelAry, $select_tag, $selected_value, 0, 1);
                $tableContent .= "<td class='tabletext'>".$subjectLevelSelection."</td>";
                $td_count++;
            }
        $tableContent .= "</tr>";
        $tr_count++;
    }
}
$tableContent .= "</tbody>";
$tableContent .= "</table>";
//$tableContent .= "</div>";
$htmlAry["dataTable"] = $tableContent;

## Toolbar and Serach Box
$toolbarDiv = '';
$toolbarDiv .= '<div class="content_top_tool">'."\n";
    $toolbarDiv .= '<div class="Conntent_tool">'."\n";
        $toolbarDiv .= '<div style="float:left">'."\n";
        $toolbarDiv .= $linterface->GET_LNK_IMPORT("javascript:goImport()", $Lang['Btn']['Import']);
        $toolbarDiv .= $linterface->GET_LNK_EXPORT("javascript:goExport()", $Lang['Btn']['Export']);
        $toolbarDiv .= '</div>'."\n";
    $toolbarDiv .= '</div>'."\n";
$toolbarDiv .= '<br style="clear:both" />'."\n";
$toolbarDiv .= '</div>'."\n";

# Button
$htmlAry["EditButton"] = "";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()", "submitBtn");
$htmlAry["EditButton"] .= "&nbsp;&nbsp;";
$htmlAry["EditButton"] .= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()", "backBtn");
$htmlAry["ViewButton"] = "";
$htmlAry["ViewButton"] .= $linterface->GET_ACTION_BTN($button_edit, "button", "goEdit()", "editBtn");

# Page
$CurrentPageArr['Subjects'] = 1;

# Title
$TAGS_OBJ = $lscm_ui->Get_SchoolSettings_Subject_Tab_Array('studentSubjectLevel');
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

# Display
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
?>

<style type="text/css">
div.subject_level_table, div.subject_level_table_2 {
    position: relative;
    max-width: 96vw;
    max-height: 60vh;
    overflow: scroll;
}

table.display_table, table.edit_table {
    position: relative;
    border-collapse: collapse;
}

table.display_table th, table.display_table td, table.edit_table th, table.edit_table td {
    padding: 0.25em;
}

table.display_table thead th, table.edit_table thead th, table.display_table th.tablescrollcell2, table.edit_table th.tablescrollcell2 {
    position: -webkit-sticky; /* for Safari */
    position: sticky;
    top: 0;
    z-index: 1;
}

table.display_table th.tablescrollcell, table.edit_table th.tablescrollcell {
    left: 0;
    z-index: 10;
}

table.display_table th.tablescrollcell2, table.edit_table th.tablescrollcell2 {
    left: 0;
    z-index: 9;
}
</style>

<script type="text/javascript">
function goBack() {
	$('.subject_level_table_2').hide();
	$('.edit_table').hide();
	$('.edit_button').hide();

	$('.subject_level_table').show();
	$('.display_table').show();
	$('.display_button').show();
}

function goEdit() {
	$('.subject_level_table').hide();
	$('.display_table').hide();
	$('.display_button').hide();

	$('.subject_level_table_2').show();
	$('.edit_table').show();
	$('.edit_button').show();
}

function goSubmit() {
	$('form#form1').attr('action', 'student_subject_level_update.php').submit();
}

function goImport() {
    $('form#form1').attr('action', 'student_subject_level_import.php').submit();
}

function goExport(){
	window.location.href = 'student_subject_level_export.php?classId=' + $('#classId').val();
}

function applyAllOption(subjectID, val) {
	$('select[name^="subjectLevel["][name$="[' + subjectID + ']"]').each(function(){
		$(this).val(val);
	});
}
</script>

<form name="form1" id="form1" method="POST" action="student_subject_level.php">
	<div class="table_board">
        <?= $toolbarDiv ?>
		<br />

		<div class="table_filter">
			<?=$htmlAry["classSelection"]?>
		</div>
		<p class="spacer"></p>
		<br />

		<div class="subject_level_table">
    		<?=$htmlAry['displayTable']?>
		</div>
		<div class="subject_level_table_2" style="display: none">
    		<?=$htmlAry['dataTable']?>
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<br/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="display_button">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["ViewButton"]?></td></tr>
	  		</table>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="edit_button" style="display: none">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["EditButton"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>