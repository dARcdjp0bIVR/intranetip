<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$isComponent = stripslashes($_REQUEST['isComponent']);

$DataArr = array();

if ($isComponent)
{
	$ParentSubjectID = stripslashes($_REQUEST['ParentSubjectID']);
	$ParentSubject_Obj = new subject($ParentSubjectID);
	$DataArr['CODEID'] = $ParentSubject_Obj->CODEID;
	
	$DataArr['CMP_CODEID'] = stripslashes(urldecode($_REQUEST['Code']));
	$DataArr['EN_DES'] = stripslashes(urldecode($_REQUEST['TitleEn']));
	$DataArr['CH_DES'] = stripslashes(urldecode($_REQUEST['TitleCh']));
	$DataArr['EN_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrEn']));
	$DataArr['CH_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrCh']));
	$DataArr['EN_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormEn']));
	$DataArr['CH_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormCh']));
	
	$libSubject = new Subject_Component();
}
else
{
	$DataArr['CODEID'] = stripslashes(urldecode($_REQUEST['Code']));
	$DataArr['EN_DES'] = stripslashes(urldecode($_REQUEST['TitleEn']));
	$DataArr['CH_DES'] = stripslashes(urldecode($_REQUEST['TitleCh']));
	$DataArr['EN_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrEn']));
	$DataArr['CH_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrCh']));
	$DataArr['EN_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormEn']));
	$DataArr['CH_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormCh']));
	$DataArr['LearningCategoryID'] = stripslashes($_REQUEST['LearningCategoryID']);
	$DataArr['CMP_CODEID'] = '';
	
	$libSubject = new subject();
}


$success = $libSubject->Insert_Record($DataArr);


echo $success;
intranet_closedb();

?>