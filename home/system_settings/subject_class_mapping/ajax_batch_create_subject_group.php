<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_opendb();

$YearTermID = $_REQUEST['YearTermID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$SubjectID = $_REQUEST['SubjectID'];
$SelectAll = array();

$fcm = new form_class_manage();
$FormList = $fcm->Get_Form_List();
for ($i=0; $i< sizeof($FormList); $i++) {
	if ($_REQUEST[$FormList[$i]['YearID'].'-SelectAll'] == "1") {
		$SelectAll[] = $FormList[$i]['YearID'];
	}
		
	$SelectClass[$FormList[$i]['YearID']] = $_REQUEST[$FormList[$i]['YearID']];
}
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';

echo '<pre>';
var_dump($SelectAll);
echo '</pre>';

echo '<pre>';
var_dump($SelectClass);
echo '</pre>';*/

$scm = new subject_class_mapping();

$scm->Start_Trans();

$Result = $scm->Batch_Create_Subject_Group($AcademicYearID,$YearTermID,$SubjectID,$FormList,$SelectAll,$SelectClass,$isCreateEclass);

if ($Result) {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassCreateSuccess'];
	$scm->Commit_Trans();
}
else {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassCreateUnsuccess'];
	$scm->RollBack_Trans();
}
intranet_closedb();
?>