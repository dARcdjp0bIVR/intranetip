<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$isComponent = $_REQUEST['isComponent'];

# Initialize library
$SubjectID = $_REQUEST['SubjectID'];

if ($isComponent)
{
	$Subject_Obj = new Subject_Component($SubjectID);
}
else
{
	$Subject_Obj = new subject($SubjectID);
}


# Delete Record
$success = $Subject_Obj->Delete_Record();


echo $success;
intranet_closedb();

?>