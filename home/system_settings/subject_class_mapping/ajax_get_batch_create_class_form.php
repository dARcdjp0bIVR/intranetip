<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTerm'];

$SubjectClassUI = new subject_class_mapping_ui();

echo $SubjectClassUI->Get_Batch_Create_Class($AcademicYearID,$YearTermID);

intranet_closedb();
?>