<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php"); 
intranet_opendb();

$YearID = $_REQUEST['YearID'];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectID = $_REQUEST['SubjectID'];
$ClassTitleEN = trim(stripslashes(urldecode($_REQUEST['ClassTitleEN'])));
$ClassTitleB5 = trim(stripslashes(urldecode($_REQUEST['ClassTitleB5'])));
$ClassCode = trim(stripslashes(urldecode($_REQUEST['ClassCode'])));
$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']: array();
$SelectedClassTeacher = (is_array($_REQUEST['SelectedClassTeacher']))? $_REQUEST['SelectedClassTeacher']: array();
$SubjectComponenetID = $_REQUEST['SubjectComponentID'];

$scm = new subject_class_mapping();

$scm->Start_Trans();

$Result = $scm->Create_Subject_Group($YearID,$YearTermID,$SubjectID,$ClassTitleEN,$ClassTitleB5,$ClassCode,$StudentSelected,$SelectedClassTeacher,'',($isCreateClass==1),'',($createNew==1),$eclass,$addStd,array(),$SubjectComponenetID);
 
if ($Result) {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassCreateSuccess'];
	$scm->Commit_Trans();
}
else {
	echo $Lang['SysMgr']['SubjectClassMapping']['ClassCreateUnsuccess'];
	$scm->RollBack_Trans();
}
intranet_closedb();
?>