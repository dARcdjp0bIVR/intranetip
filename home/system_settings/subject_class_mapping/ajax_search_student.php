<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$StudentSelected = $_REQUEST['StudentSelected'][0];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectID = $_REQUEST['SubjectID'];
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$YearID = $_REQUEST['YearID'];
$SearchValue = stripslashes(urldecode($_REQUEST['q']));

$scm = new subject_class_mapping();

$Result = $scm->Search_Student($SearchValue,$StudentSelected,$YearTermID,$SubjectID,$SubjectGroupID,$YearID);

for ($i=0; $i< sizeof($Result); $i++) {
	echo $Result[$i]['Name']." (".Get_Lang_Selection($Result[$i]['ClassTitleB5'],$Result[$i]['ClassTitleEN'])."-".$Result[$i]['ClassNumber'].")|".$Result[$i]['UserEmail']."|".$Result[$i]['UserID']."\n";
}

intranet_closedb();
die;
?>