<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$SelfID = stripslashes($_REQUEST['SelfID']);
$InputValue = stripslashes($_REQUEST['InputValue']);

$OjbLearningCategory = new Learning_Category($SelfID);
$isExistedTitle = $OjbLearningCategory->Is_Code_Existed($InputValue);


if ($isExistedTitle)
	echo $Lang['SysMgr']['SubjectClassMapping']['Duplicated_LC_CodeWarning'];
else
	echo "";


intranet_closedb();

?>