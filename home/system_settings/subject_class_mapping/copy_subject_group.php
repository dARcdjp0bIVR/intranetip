<?php
// Modifying by: 
############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT.'includes/libeclass40.php');
		

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$leclass = new libeclass();

### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Options'], 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopyResult'], 0);

if ($FromStep2)
{
	$AcademicYearID = $_REQUEST['ToAcademicYearID'];
	$YearTermID = $_REQUEST['ToYearTermID'];
}
else
{
	if ($AcademicYearID == '')
		$AcademicYearID = Get_Current_Academic_Year_ID();
	
	if ($YearTermID == '')
		$YearTermID = getCurrentSemesterID();
}

$obj_AcademicYear = new academic_year($AcademicYearID);
$obj_YearTerm = new academic_year_term($YearTermID);
$subject_class_mapping_ui = new subject_class_mapping_ui();

### Get data if it is back from Step 2
$SelectedAcademicYearID = '';
$SelectedYearTermID = '';
$SubjectGroupTable = '';
$FromStep2 = $_REQUEST['FromStep2'];
if ($FromStep2)
{
	### Get the previous selection
	$Global_Arr = unserialize(rawurldecode($Global_Arr));
	$Copy_Subject_Arr = unserialize(rawurldecode($Copy_Subject_Arr));
	$Teacher_Subject_Arr = unserialize(rawurldecode($Teacher_Subject_Arr));
	$Student_Subject_Arr = unserialize(rawurldecode($Student_Subject_Arr));
	$Copy_SubjectGroup_Arr = unserialize(rawurldecode($Copy_SubjectGroup_Arr));
	$Teacher_SubjectGroup_Arr = unserialize(rawurldecode($Teacher_SubjectGroup_Arr));
	$Student_SubjectGroup_Arr = unserialize(rawurldecode($Student_SubjectGroup_Arr));
	$Leader_Subject_Arr = unserialize(rawurldecode($Leader_Subject_Arr));
	$Leader_SubjectGroup_Arr = unserialize(rawurldecode($Leader_SubjectGroup_Arr));

	if (!is_array($Global_Arr))
		$Global_Arr = array();
	if (!is_array($Copy_Subject_Arr))
		$Copy_Subject_Arr = array();
	if (!is_array($Teacher_Subject_Arr))
		$Teacher_Subject_Arr = array();
	if (!is_array($Student_Subject_Arr))
		$Student_Subject_Arr = array();
	if (!is_array($Copy_SubjectGroup_Arr))
		$Copy_SubjectGroup_Arr = array();
	if (!is_array($Teacher_SubjectGroup_Arr))
		$Teacher_SubjectGroup_Arr = array();
	if (!is_array($Student_SubjectGroup_Arr))
		$Student_SubjectGroup_Arr = array();
	if (!is_array($Leader_Subject_Arr))
		$Leader_Subject_Arr = array();
	if (!is_array($Leader_SubjectGroup_Arr))
		$Leader_SubjectGroup_Arr = array();
	
	$SelectedAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$SelectedYearTermID = $_REQUEST['FromYearTermID'];
	$allowStudentChk = ($SelectedAcademicYearID == $AcademicYearID)? 1 : 0; 
	$CopyTimetable = $_REQUEST['CopyTimetable'];
	$ExpanedSubjectIDStr = $_REQUEST['ExpanedSubjectIDStr'];
	$ExpanedSubjectIDArr = explode(',', $ExpanedSubjectIDStr);
	
	$SubjectGroupTable = $subject_class_mapping_ui->Get_Copy_Subject_Group_Table($SelectedYearTermID, $allowStudentChk, $Global_Arr,
																					$Copy_Subject_Arr, $Teacher_Subject_Arr, $Student_Subject_Arr,
																					$Copy_SubjectGroup_Arr, $Teacher_SubjectGroup_Arr, $Student_SubjectGroup_Arr,$Leader_Subject_Arr,$Leader_SubjectGroup_Arr);
}
else
{
	# Get the term before the selected term
	$PastTermArr = $obj_YearTerm->Get_Semester_Before();
	$LastYearTermID = $PastTermArr[0]['YearTermID'];
	
	$ObjLastTerm = new academic_year_term($LastYearTermID, false);
	$SelectedAcademicYearID = $ObjLastTerm->AcademicYearID;
	$SelectedYearTermID = $LastYearTermID;
	
	$CopyTimetable = 1;
}

# Acadermic Year Selection
$fcm = new form_class_manage();
$FutureYearArr = $fcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $NoPastYear=1, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $SelectedAcademicYearID);
$FutureYearIDArr = Get_Array_By_Key($FutureYearArr, 'AcademicYearID');
$AcadermicYearSelection = getSelectAcademicYear('SelectedAcademicYearID', 'onchange="js_Reload_Term_Selection(this.value)"', 
								$noFirst=1, $noPastYear=0, $SelectedAcademicYearID, $displayAll=0, $pastYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=$FutureYearIDArr);
# Term Selection
$YearTermSelection = $subject_class_mapping_ui->Get_Term_Selection('SelectedYearTermID', $SelectedAcademicYearID, $SelectedYearTermID, 
								$OnChange='js_Reload_Subject_Group_Table()', $NoFirst=1, $NoPastTerm=0, $displayAll=0, $AllTitle='', $PastTermOnly=1, $YearTermID);

# Timetable Copy
if ($CopyTimetable)
{
	$CopyTimetableChk_Yes = 'checked';
	$CopyTimetableChk_No = '';
}
else
{
	$CopyTimetableChk_Yes = '';
	$CopyTimetableChk_No = 'checked';
}

# Create eClass Default
if ($Create_eClass)
{
	$Create_eClass_Chk_Yes = 'checked';
	$Create_eClass_Chk_No = '';
}
else
{
	$Create_eClass_Chk_Yes = '';
	$Create_eClass_Chk_No = 'checked';
}

if ($leclass->license != '' && $leclass->license > 0)
{
	$QuotaLeft = $leclass->ticket();
	$LicenseMsg = $subject_class_mapping_ui->Get_eClass_Licence_Quota_Message($leclass->license, $QuotaLeft);
	
	if ($QuotaLeft==0)
	{
		$thisStyle = 'style="color:red;"';
		$Create_eClass_Disabled = 'disabled';
	}
	else
	{
		$thisStyle = 'class="tabletextremark"';
		$Create_eClass_Disabled = '';
	}
	
	$eClassLicenseWarning = ' <span '.$thisStyle.'>('.$LicenseMsg.')</span>';
}


### Warning Box
$displayStyle = '';
if ($SelectedAcademicYearID == $AcademicYearID)
	$displayStyle = 'style="display:none;"';
else
	$displayStyle = 'style="display:block;"';

$warningText = '<font style="color:red">'.$Lang['SysMgr']['SubjectClassMapping']['WarningText'].'</font>';
$WarningBoxDiv = '';
$WarningBoxDiv .= '<div id="warning_box_div" '.$displayStyle.'">';
	$WarningBoxDiv .= $linterface->Get_Warning_Message_Box($warningText, $Lang['SysMgr']['SubjectClassMapping']['WarningArr']['CannotCopyStudentForDifferentAcademicYear']);
$WarningBoxDiv .= '</div>';




# For testing only
//$SubjectGroupTable = $subject_class_mapping_ui->Get_Copy_Subject_Group_Table(5);

?>
<br />
<form id="form1" name="form1" method="post" action="copy_subject_group_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?></td>
					<td class="tabletext" ><?=$obj_AcademicYear->Get_Academic_Year_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['Term']?></td>
					<td class="tabletext" ><?=$obj_YearTerm->Get_Year_Term_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyFrom']?></td>
					<td class="tabletext" >
						<div style="float:left">
							<?=$AcadermicYearSelection?>
						</div>
						<div style="float:left">&nbsp;&nbsp;</div>
						<div style="float:left" id="YearTermSelectionDiv">
							<?=$YearTermSelection?>
						</div>
						<br style="clear:both">
					<!--	<?= $WarningBoxDiv ?> -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!-- Subject Group Table -->
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td class="tabletext" ><?=$linterface->GET_NAVIGATION2($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'])?></td></tr>
				<tr>
					<td>
						<div id="SubjectGroupTableDiv">
							<?=$SubjectGroupTable?>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!-- Timetable Table -->
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td class="tabletext" ><?=$linterface->GET_NAVIGATION2($Lang['SysMgr']['Timetable']['ModuleTitle'])?></td></tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
						<?= $Lang['SysMgr']['SubjectClassMapping']['Copy'] ?>
					</td>
					<td class='tabletext'>
						<input type="radio" name="CopyTimetable" id="CopyTimetable_Yes" value="1" <?=$CopyTimetableChk_Yes?>>
							<label for="CopyTimetable_Yes"><?=$Lang['General']['Yes']?></label>
						</input>
						<input type="radio" name="CopyTimetable" id="CopyTimetable_No" value="0" <?=$CopyTimetableChk_No?>>
							<label for="CopyTimetable_No"><?=$Lang['General']['No']?></label>
						</input>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!-- eClass Table -->
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td class="tabletext" colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['SysMgr']['SubjectClassMapping']['eClass'])?></td></tr>
				<tr>
					<td valign="top" class="formfieldtitle tabletext" width="30%">
						<?= $Lang['SysMgr']['SubjectClassMapping']['CreateeClassIfSubjectGroupDoesNotHaveOne'] ?>
					</td>
					<td class='tabletext' valign='top'>
						<input type="radio" name="Create_eClass" id="Create_eClass_Yes" value="1" <?=$Create_eClass_Chk_Yes?>>
							<label for="Create_eClass_Yes"><?=$Lang['General']['Yes']?></label>
						</input>
						<input type="radio" name="Create_eClass" id="Create_eClass_No" value="0" <?=$Create_eClass_Chk_No?>>
							<label for="Create_eClass_No"><?=$Lang['General']['No']?></label>
						</input>
						&nbsp;&nbsp;
						<?= $eClassLicenseWarning ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button", "jsFormCheck();") ?>
			&nbsp; 
			<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='subject_group_mapping.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">
<input type="hidden" name="ExpanedSubjectIDStr" id="ExpanedSubjectIDStr" value="">


</form>
<br />


<script>
var jsAcademicYearID = <?=$AcademicYearID?>;
var jsYearTermID = <?=$YearTermID?>;
var ExpandedSubjectID = Array();	// for "back" from confirm page
var jsEClassQuotaLeft = "<?=$QuotaLeft?>";

/*
$(document).ready( function() {
	<? if ($FromStep2 == false) { ?>
			js_Reload_Subject_Group_Table();
	<? } else { 
		$numOfExpandSubject = count($ExpanedSubjectIDArr);
		for ($i=0; $i<$numOfExpandSubject; $i++)
		{
	?>
		js_Show_Subject_Group_Row(<?=$ExpanedSubjectIDArr[$i]?>);
	<? 
		} 
	}
	?>
});
*/

function js_Reload_Term_Selection(SelectedAcademicYearID)
{
	var selectionDivID = 'YearTermSelectionDiv';
	//$('div#' + selectionDivID).hide();
	
	$('div#' + selectionDivID).load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Term_Selection',
			AcademicYearID: SelectedAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: 'SelectedYearTermID',
			jsOnchange: 'js_Reload_Subject_Group_Table()'
		},
		function(ReturnData)
		{
			//$('#' + selectionDivID).show();
			js_Reload_Subject_Group_Table();
			
			if (SelectedAcademicYearID == jsAcademicYearID)
				$('div#warning_box_div').hide();
			else
				$('div#warning_box_div').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Reload_Subject_Group_Table()
{
	var selectionDivID = 'SubjectGroupTableDiv';
	$('div#' + selectionDivID).html('');
	
	var jsSelectedAcademicYearID = $('select#SelectedAcademicYearID').val();
	var jsSelectedYearTermID = $('select#SelectedYearTermID').val();
	
	var jsAllowStudentChk = 1;
	if (jsSelectedAcademicYearID != jsAcademicYearID)
		jsAllowStudentChk = 0;
		
	
	//$('#' + selectionDivID).hide();
	Block_Element(selectionDivID);
	
	$('div#' + selectionDivID).load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Subject_Group_Table',
			YearTermID: jsSelectedYearTermID,
			AllowStudentChk: jsAllowStudentChk
		},
		function(ReturnData)
		{
			//$('#' + selectionDivID).show();
			UnBlock_Element(selectionDivID);
			$('.CopyStudentChk').bind('click',function(){
				var suffix = /\d+$/.exec(this.id);
				var lclass = this.className.replace(/Student/i,'Leader')+'_'+suffix[0];
				$('.'+lclass).each(function(){
					var lid = this.id.replace(/Leader/i,'Student');
					$(this).attr('disabled',!$('#'+lid).attr('checked')||$('#'+lid).attr('disabled'))
				});
				var lid = this.id.replace(/Student/i,'Leader');
				$('#'+lid).each(function(){
					var lid = this.id.replace(/Leader/i,'Student');
					$(this).attr('disabled',!$('#'+lid).attr('checked')||$('#'+lid).attr('disabled'))
				});
			})
			$('#Student_Global').bind('click',function(){
				var lclass = this.className.replace(/Student/i,'Leader');
				$('.'+lclass).attr('disabled',!this.checked);
				lclass = lclass.replace(/_Global/i,'');
				$('.'+lclass).attr('disabled',!this.checked);
			});
	
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Change_Checkbox_Status(jsChecked, jsCheckBoxClass, jsSubjectID)
{
	jsSubjectID = jsSubjectID || '';
	
	var jsTargetClass = '';
	if (jsSubjectID == '')
	{
		// global settings
		jsTargetClass = jsCheckBoxClass;
	}
	else
	{
		// settings within subject
		jsTargetClass = jsCheckBoxClass + '_' + jsSubjectID;
		
		if (jsChecked == false)
		{
			// uncheck the global one also
			$('input.' + jsCheckBoxClass + '_Global').attr('checked', jsChecked);
		}
		else
		{
			// enable the global settings also
			$('input.' + jsCheckBoxClass + '_Global').attr('disabled', false);
		}
	}
	// apply the status to all
	$('input.' + jsTargetClass + ':enabled').attr('checked', jsChecked);
	
	
	// if "copy" is unchecked -> disable teacher and student checkboxes
	// if "copy" is checked -> enable teacher and student checkboxes
	if (jsCheckBoxClass == 'CopySGChk')
	{
		if (jsSubjectID == '')
		{
			// global settings
			jsTeacherChkClass = 'CopyTeacherChk';
			jsStudentChkClass = 'CopyStudentChk';
			jsLeaderChkClass = 'CopyLeaderChk';
			
			// disable the global selection also
			$('input#Teacher_Global').attr('disabled', !jsChecked);
			$('input#Student_Global').attr('disabled', !jsChecked);
			$('input#Leader_Global').attr('disabled', !jsChecked);
		}
		else
		{
			// settings within subject
			jsTeacherChkClass = 'CopyTeacherChk' + '_' + jsSubjectID;
			jsStudentChkClass = 'CopyStudentChk' + '_' + jsSubjectID;
			jsLeaderChkClass = 'CopyLeaderChk' + '_' + jsSubjectID;
			
			// disable the whole subject selection also
			$('input#Teacher_Subject_' + jsSubjectID).attr('disabled', !jsChecked);
			$('input#Student_Subject_' + jsSubjectID).attr('disabled', !jsChecked);
			$('input#Leader_Subject_' + jsSubjectID).each(function(){
				var lid = this.id.replace(/Leader/i,'Student');
				$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
			})
		}
		$('input.' + jsTeacherChkClass).attr('disabled', !jsChecked);
		$('input.' + jsStudentChkClass).attr('disabled', !jsChecked);
		$('input.' + jsLeaderChkClass).each(function(){
			var lid = this.id.replace(/Leader/i,'Student');
			$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
		});
		
		if (jsChecked == true)
		{
			$('input#Teacher_Global').attr('disabled', false);
			$('input#Student_Global').attr('disabled', false);
			$('input#Leader_Global').attr('disabled', !$('input#Student_Global').attr('checked'));
		}
	}
}

function js_Check_Uncheck_Subject_Group(jsChecked, jsTargetType, jsSubjectID, jsSubjectGroupID)
{
	if (jsChecked == false)
	{
		$('input#' + jsTargetType + '_Global').attr('checked', jsChecked);
		$('input#' + jsTargetType + '_Subject_' + jsSubjectID).attr('checked', jsChecked);
	}
	
	if (jsTargetType == 'Copy')
	{
		if (jsChecked == false)
		{
			$('input#Teacher_SubjectGroup_' + jsSubjectGroupID).attr('disabled', true);
			$('input#Student_SubjectGroup_' + jsSubjectGroupID).attr('disabled', true);
			$('input#Leader_SubjectGroup_' + jsSubjectGroupID).each(function(){
				var lid = this.id.replace(/Leader/i,'Student');
				$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
			});
			
			// If all "Copy" unchecked => disable the Subject checkbox of "Teacher" and "Student"
			var thisAllUnChecked = true;
			$('input.CopySGChk_' + jsSubjectID).each( function() {
				if ($(this).attr('checked') == true)
					thisAllUnChecked = false;
			});
			
			if (thisAllUnChecked == true)
			{
				$('input#Teacher_Subject_' + jsSubjectID).attr('disabled', true);
				$('input#Student_Subject_' + jsSubjectID).attr('disabled', true);
				$('input#Leader_Subject_' + jsSubjectID).each(function(){
					var lid = this.id.replace(/Leader/i,'Student');
					$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
				});
			}
		}
		else
		{
			$('input#Teacher_SubjectGroup_' + jsSubjectGroupID).attr('disabled', false);
			$('input#Student_SubjectGroup_' + jsSubjectGroupID).attr('disabled', false);
			$('input#Leader_SubjectGroup_' + jsSubjectGroupID).each(function(){
				var lid = this.id.replace(/Leader/i,'Student');
				$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
			});
			$('input#Teacher_Subject_' + jsSubjectID).attr('disabled', false);
			$('input#Student_Subject_' + jsSubjectID).attr('disabled', false);
			$('input#Leader_Subject_' + jsSubjectID).each(function(){
				var lid = this.id.replace(/Leader/i,'Student');
				$(this).attr('disabled', !jsChecked||!$('#'+lid).attr('checked'));
			});
		}
	}
}

function js_Show_Subject_Group_Row(jsSubjectID)
{
	var ExpandedArrLength = ExpandedSubjectID.length;
	var curr_class = $("a#zoom_" + jsSubjectID).attr("class");
	if(curr_class == 'zoom_in') {
		$('tr.SubjectSubRow_' + jsSubjectID).show();
		$("a#zoom_" + jsSubjectID).attr("class", "zoom_out");
		
		// Add the SubjectID in the epanded array
		ExpandedSubjectID[ExpandedArrLength] = jsSubjectID;
	}
	if(curr_class == 'zoom_out'){
		$('tr.SubjectSubRow_' + jsSubjectID).hide();
		$("a#zoom_" + jsSubjectID).attr("class", "zoom_in");
		
		// remove the SubjectID from the expanded array
		for (var i=0; i<ExpandedArrLength; i++)
		{ 
			if (ExpandedSubjectID[i] == jsSubjectID)
			{
				ExpandedSubjectID.splice(i, 1); 
			}
		} 
	}
}


function jsFormCheck()
{
	var jsCheckedArr = $('input.CopySGChk:checked');
	var jsCheckedArr_WithoutEClass = $('input.CopySGChk_WithoutEClass:checked');
	
	var jsCopyeClass;
	if ($('input#Create_eClass_Yes').attr('checked') == true)
		jsCopyeClass = true;
	else
		jsCopyeClass = false;
	
	if (jsCheckedArr.length == 0)
	{
		alert("<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectAtLeastOneSubjectGroup']?>");
	}
	else if (jsCopyeClass == true && jsEClassQuotaLeft != '' && jsCheckedArr_WithoutEClass.length > parseInt(jsEClassQuotaLeft))
	{
		alert('<?=$Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage']?>');
	}
	else
	{
		var thisExpanedSubjectIDStr = '';
		var ExpandedArrLength = ExpandedSubjectID.length;
		for(var i=0; i < ExpandedArrLength; i++)
		{ 
			thisExpanedSubjectIDStr += ExpandedSubjectID[i];
			
			if (i < ExpandedArrLength - 1)
				thisExpanedSubjectIDStr += ',';
		} 
		
		document.getElementById('ExpanedSubjectIDStr').value = thisExpanedSubjectIDStr;
		document.getElementById('form1').submit();
	}
}

$(document).ready( function() {
	<? if ($FromStep2 == false) { ?>
			js_Reload_Subject_Group_Table();
	<? } else { 
		$numOfExpandSubject = count($ExpanedSubjectIDArr);
		for ($i=0; $i<$numOfExpandSubject; $i++)
		{
	?>
		js_Show_Subject_Group_Row(<?=$ExpanedSubjectIDArr[$i]?>);
	<? 
		} 
	}
	?>
});
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
