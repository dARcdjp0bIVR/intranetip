<?
// Modifying by: 
############# Change Log [Start] ################
#
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libimporttext.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$li = new libdb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


############################################################################
# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
############################################################################
$sql = "select * from TEMP_SUBJECT_COMPONENT_IMPORT where UserID = $UserID order by RowNumber";
$class_result = $li->returnArray($sql);

$libSubjectComponent = new Subject_Component();

foreach($class_result as $k=>$d)
{
	list($TempID, $ImportUserID, $RowNumber, $ParentSubjectWebSAMSCode, $ComponentWebSAMSCode, $EN_DES, $CH_DES, $EN_ABBR, $CH_ABBR, $EN_SNAME, $CH_SNAME) = $d;
	
	# Construct array to create subject
	$DataArr = array();
	$DataArr['CODEID'] = $ParentSubjectWebSAMSCode;
	$DataArr['CMP_CODEID'] = $ComponentWebSAMSCode;
	$DataArr['EN_DES'] = $EN_DES;
	$DataArr['CH_DES'] = $CH_DES;
	$DataArr['EN_ABBR'] = $EN_ABBR;
	$DataArr['CH_ABBR'] = $CH_ABBR;
	$DataArr['EN_SNAME'] = $EN_SNAME;
	$DataArr['CH_SNAME'] = $CH_SNAME;
	
	$successArr[$ComponentWebSAMSCode] = $libSubjectComponent->Insert_Record($DataArr);
}




############################################################################
# delete data in Temp table
############################################################################
$sql = "delete from TEMP_SUBJECT_COMPONENT_IMPORT where UserID = $UserID";
$li->db_db_query($sql) or die(mysql_error());



# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

?>


<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		
		<tr>
			<td class='tabletext' align='center'><?=count($class_result)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'"); ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
