<?php
// Modifying by: 
@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/****************************************************************
 * Modification log:
 * 	20200507 Tommy:
 *      - modified access checking, added $plugin["Disable_Subject"]
 *  20190805 Bill:  [2019-0802-1105-08235]
 *      - added hidden input field "ToAcademicYearID" > fixed cannot copy timetable
 * 	20110620 Marcus:
 * 		- direct copy old Class Code to new Subject Group
 * 		- use $_POST instead of $_REQUEST, as the value retrieved by $_REQUEST may from cookies and caused error.
 * **************************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
//include_once($PATH_WRT_ROOT."includes/libtimetable.php");
//include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### Get Data
$ToAcademicYearID = $_POST['ToAcademicYearID'];
$ToYearTermID = $_POST['ToYearTermID'];
$FromAcademicYearID = $_POST['FromAcademicYearID'];
$FromYearTermID = $_POST['FromYearTermID'];
$Copy_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Copy_SubjectGroup_Arr']));
$Teacher_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Teacher_SubjectGroup_Arr']));
$Student_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Student_SubjectGroup_Arr']));
$Leader_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Leader_SubjectGroup_Arr']));
$CopyTimetable = $_POST['CopyTimetable'];
$Create_eClass = $_POST['Create_eClass'];

$linterface = new interface_html();

### Thickbox loading message
$numOfSubjectGroup = count($Copy_SubjectGroup_Arr);
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfSubjectGroup, $Lang['General']['ImportArr']['RecordsProcessed']);


################################
######## Copy Result Display
################################
### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup']);

$ReturnMessage = $Lang['SysMgr']['SubjectClassMapping']['ReturnMessage'][$ReturnMessageKey];
$linterface->LAYOUT_START($ReturnMessage); 

# step information
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Options'], 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['CopyResult'], 1);
?>

<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$h_ProgressMsg?>');
	processCopySubjectGroup();
});

function processCopySubjectGroup() {
	$('input#task').val('Copy_Subject_Group');
	$('form#form1').attr('target', 'ImportIFrame').attr('action', 'ajax_update.php').submit();
}

function showQuickEditInstruction() {
	$('tr#goQuickEditInstructionTr').show();
}

function updateProcessCount(counter) {
	document.getElementById("BlockUI_Processed_Number_Span").innerHTML = counter;
}

function showCopySuccessMsgSpan() {
	$('span#copySuccessMsgSpan').show();
}
</script>

<br />
<form id="form1" name="form1" method="post">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td align="center"><?=$x?></td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class='tabletext' align='center'><span id="copySuccessMsgSpan" style="display:none;"><span id="successCountSpan"></span> <?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCopySuccessfully']?></span></td>
			</tr>
			<tr id="goQuickEditInstructionTr" style="display:none;">
				<td class='tabletext' align='center'><?=$Lang['SysMgr']['SubjectClassMapping']['GoQuickEditInstruction']?></td>
			</tr>
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<?=$linterface->GET_ACTION_BTN($Lang['SysMgr']['SubjectClassMapping']['BackToSubjectGroupPage'], "button", "window.location='subject_group_mapping.php?AcademicYearID=$ToAcademicYearID&YearTermID=$ToYearTermID'"); ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	<iframe id="ImportIFrame" name="ImportIFrame" style="width:100%;height:500px;border:0px;display:none;"></iframe>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" name="AcademicYearID" value="<?=$_POST['AcademicYearID']?>" />
    <input type="hidden" name="ToAcademicYearID" value="<?=$_POST['ToAcademicYearID']?>" />
	<input type="hidden" name="ToYearTermID" value="<?=$_POST['ToYearTermID']?>" />
	<input type="hidden" name="FromAcademicYearID" value="<?=$_POST['FromAcademicYearID']?>" />
	<input type="hidden" name="FromYearTermID" value="<?=$_POST['FromYearTermID']?>" />
	<input type="hidden" name="Copy_SubjectGroup_Arr" value="<?=$_POST['Copy_SubjectGroup_Arr']?>" />
	<input type="hidden" name="Teacher_SubjectGroup_Arr" value="<?=$_POST['Teacher_SubjectGroup_Arr']?>" />
	<input type="hidden" name="Student_SubjectGroup_Arr" value="<?=$_POST['Student_SubjectGroup_Arr']?>" />
	<input type="hidden" name="Leader_SubjectGroup_Arr" value="<?=$_POST['Leader_SubjectGroup_Arr']?>" />
	<input type="hidden" name="CopyTimetable" value="<?=$_POST['CopyTimetable']?>" />
	<input type="hidden" name="Create_eClass" value="<?=$_POST['Create_eClass']?>" />
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>