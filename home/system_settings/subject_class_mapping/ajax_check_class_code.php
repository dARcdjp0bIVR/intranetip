<?php
// using 

/**********************************************************
 *  Modification
 * 		20110620 Marcus:
 * 			- Add $YearTermID to Check_Class_Code
 * *******************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$ClassCode = trim(stripslashes(urldecode($_REQUEST['ClassCode'])));
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$scm = new subject_class_mapping();


if ($scm->Check_Class_Code($SubjectGroupID,$ClassCode,$YearTermID)) 
	echo "1"; // Class Code is good to use
else
	echo "0"; // Class Code duplicated

intranet_closedb();
?>