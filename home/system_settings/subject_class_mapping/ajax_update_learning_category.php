<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
$DivID = stripslashes(urldecode($_REQUEST['DivID']));
$DBFieldName = stripslashes(urldecode($_REQUEST['DBFieldName']));
$UpdateValue = stripslashes(urldecode($_REQUEST['UpdateValue']));

$DivIDArr = explode('_', $DivID);
$LearningCategoryID = $DivIDArr[1];

$DataArr = array();
$DataArr[$DBFieldName] = $UpdateValue;


$libLC = new Learning_Category($LearningCategoryID);
$success = $libLC->Update_Record($DataArr);


echo $success;
intranet_closedb();

?>