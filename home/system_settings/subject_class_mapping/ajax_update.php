<?php
// using 

/******************************************************
 * Modification log:
 *  20170207 Villa
 *  	- #E97684  - support import archieved user to subject Group
 *  23-10-2015 (Pun)
 *  	- added subject group to eClass for Import_Subject_Group_User
 * 	20110621 Marcus:
 * 		- add param YearTermID to constructor subject_class_mapping, 
 * 		YearTermID must be passed after cater same class code in different term
 * ****************************************************/
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
$task = stripslashes($_POST['task']);

$returnString = '';
if ($Action == "Import_Subject_Group_User")
{
	$li = new libdb();
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	############################################################################
	# retrieve Form data from temp table TEMP_FORM_CLASS_IMPORT
	############################################################################
	$sql = "select * from TEMP_SUBJECT_GROUP_USER_IMPORT where UserID = $UserID order by RowNumber";
	$class_result = $li->returnArray($sql);
	
	$AddStudentArr = array();
	$AddTeacherArr = array();
	$UserLogin_UserID_MappingArr = array();
	$Counter = 0;
	foreach($class_result as $k=>$d)
	{
		list($TempID, $ImportUserID, $RowNumber, $UserLogin, $IsTeacher, $SubjectGroupCode) = $d;
		
		## Get UserID
		if ($UserLogin_UserID_MappingArr[$UserLogin] == '')
		{
			$sql = "Select UserID FROM INTRANET_USER Where UserLogin = '".$UserLogin."' And RecordStatus = 1";
			$resultSet = $li->returnVector($sql);
			$thisUserID = $resultSet[0];
			if($thisUserID == ''){
				$sql = "Select UserID FROM INTRANET_ARCHIVE_USER Where UserLogin = '".$UserLogin."' ORDER BY DateArchive desc";
				$resultSet = $li->returnVector($sql);
				$thisUserID = $resultSet[0];
			}
			
			$UserLogin_UserID_MappingArr[$UserLogin] = $thisUserID;
		}
		else
		{
			$thisUserID = $UserLogin_UserID_MappingArr[$UserLogin];
		}
		
		if (!isset($obj_SubjectGroup[$SubjectGroupCode]))
			$obj_SubjectGroup[$SubjectGroupCode] = new subject_term_class('', $GetTeacherList=false, $GetStudentList=false, $SubjectGroupCode, $YearTermID);
		$thisSubjectGroupID = $obj_SubjectGroup[$SubjectGroupCode]->SubjectGroupID;
		
		if ($IsTeacher)
		{
			if (!isset($AddTeacherArr[$thisSubjectGroupID]))
				$AddTeacherArr[$thisSubjectGroupID] = array();
			
			$AddTeacherArr[$thisSubjectGroupID][] = $thisUserID;
		}
		else
		{
			if (!isset($AddStudentArr[$thisSubjectGroupID]))
				$AddStudentArr[$thisSubjectGroupID] = array();
			
			$AddStudentArr[$thisSubjectGroupID][] = $thisUserID;
		}
		
		$Counter++;
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.$Counter.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.$Counter.'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	
	$libSCM = new subject_class_mapping();
	$li->Start_Trans();
	foreach($AddStudentArr as $thisSubjectGroupID => $thisStudentIDArr)
	{
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_WholeSpan").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['AddingStudentsToSubjectGroup'].'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		$SuccessArr[$thisSubjectGroupID]['Student'] = $libSCM->Add_Student_To_Subject_Group($thisSubjectGroupID, $thisStudentIDArr);
	}
	foreach($AddTeacherArr as $thisSubjectGroupID => $thisTeacherIDArr)
	{
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_WholeSpan").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['AddingTeachersToSubjectGroup'].'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		$SuccessArr[$thisSubjectGroupID]['Teacher'] = $libSCM->Add_Teacher_To_Subject_Group($thisSubjectGroupID, $thisTeacherIDArr);
	}
	
	######## Add student to eClass subject group START ########
	if($addStd){
		$le = new libeclass();
		$updatedCourse_id = array();
		foreach($AddStudentArr as $thisSubjectGroupID => $thisStudentIDArr){
			$course_idArr = $le->getCourseIdByIntranetUserId($thisStudentIDArr);
			foreach($course_idArr as $course_id){
				if(!in_array($course_id, $updatedCourse_id)){
					$user_idArr = array_values($le->getUserIdByIntranetUserId($course_id, $thisStudentIDArr));
					$courseSubjectGroupID = $le->returnSubjectGroupID($course_id);
					if(in_array($thisSubjectGroupID, $courseSubjectGroupID)){
						$le->createSubjectGroupForClassroom($course_id, $thisSubjectGroupID);
						$le->syncUserToSubjectGroup($course_id, $user_idArr);
					}
				}
			}
			$updatedCourse_id = array_merge($updatedCourse_id, $course_idArr);
		}
	}
	######## Add student to eClass subject group END ########
	
	if (in_array(false, $SuccessArr))
	{
		$li->RollBack_Trans();
	}
	else
	{
		$li->Commit_Trans();
	}
	
	
	############################################################################
	# delete data in Temp table
	############################################################################
	$sql = "delete from TEMP_SUBJECT_GROUP_USER_IMPORT where UserID = $UserID";
	$li->db_db_query($sql) or die(mysql_error());
	
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
else if ($task == "Copy_Subject_Group") {
	$ToAcademicYearID = $_POST['ToAcademicYearID'];
	$ToYearTermID = $_POST['ToYearTermID'];
	$FromAcademicYearID = $_POST['FromAcademicYearID'];
	$FromYearTermID = $_POST['FromYearTermID'];
	$Copy_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Copy_SubjectGroup_Arr']));
	$Teacher_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Teacher_SubjectGroup_Arr']));
	$Student_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Student_SubjectGroup_Arr']));
	$Leader_SubjectGroup_Arr = unserialize(rawurldecode($_POST['Leader_SubjectGroup_Arr']));
	$CopyTimetable = $_POST['CopyTimetable'];
	$Create_eClass = $_POST['Create_eClass'];

	### Get all Subject info
	$libSubject = new subject();
	$SubjectArr = $libSubject->Get_Subject_List($returnAsso=1, $withLearningCateory=0);
	
	$obj_ToAcademicYear = new academic_year($ToAcademicYearID);
	$ToAcademicYearNameEn = $obj_ToAcademicYear->YearNameEN;
	$ToAcademicYearNameEn = str_replace(' ', '', $ToAcademicYearNameEn);
	
	$obj_ToYearTerm = new academic_year_term($ToYearTermID);
	$ToYearTermNameEn = $obj_ToYearTerm->YearTermNameEN;
	$ToYearTermNameEn = str_replace(' ', '', $ToYearTermNameEn);
	
	$libSCM = new subject_class_mapping();
	
	
	# Get Current Date Time (keep ClassCode uniquiness)
	$CurDateTime = date('m_d_H_i_s');
	
	# Count the number of subject group copied of the subject (for generating ClassCode use)
	$SubjectGroupCounter = array();
	$SuccessArr = array();
	$SuccessCount = 0;

	$libSCM->Start_Trans();
	foreach ($Copy_SubjectGroup_Arr as $thisSubjectGroupID => $thisIsCopySubjectGroup)
	{
		### Copy Subject Group (check copy student / teacher list or not)
		$thisCopyStudent = $Student_SubjectGroup_Arr[$thisSubjectGroupID];
		$thisCopyTeacher = $Teacher_SubjectGroup_Arr[$thisSubjectGroupID];
		$thisCopyLeader = $Leader_SubjectGroup_Arr[$thisSubjectGroupID];
		
		$Obj_ThisSubjectGroup = new subject_term_class($thisSubjectGroupID, true, true);
		$thisSubjectID = $Obj_ThisSubjectGroup->SubjectID;
		
		if (isset($SubjectGroupCounter[$thisSubjectID]) == false)
			$SubjectGroupCounter[$thisSubjectID] = 1;
		else
			$SubjectGroupCounter[$thisSubjectID]++;
			
		$thisSubjectGroupNumber = 'SG'.$SubjectGroupCounter[$thisSubjectID];
		$thisSubjectCode = $SubjectArr[$thisSubjectID]['CODEID'];
	//	$thisCode = $ToAcademicYearNameEn.'-'.$ToYearTermNameEn.'-'.$thisSubjectCode.'-'.$thisSubjectGroupNumber.'-'.$CurDateTime;
		$thisCode = $Obj_ThisSubjectGroup->ClassCode;
		
		
		$SuccessArr[$thisSubjectGroupID]['CopySubjectGroup'] = $Obj_ThisSubjectGroup->Copy_Subject_Group($ToYearTermID, $thisCode, $thisCopyStudent, $thisCopyTeacher, $Create_eClass,$thisCopyLeader);
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.updateProcessCount(\''.(++$SuccessCount).'\');'."\n";
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
	}
	
	
	if ($CopyTimetable) {
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Progress_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['CopyTimetable'].'";'."\n";
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		### Build SubjectGroupID Mapping
		$SubjectGroupIDMapping = array();
		foreach ($Copy_SubjectGroup_Arr as $FromSubjectGroupID => $thisIsCopySubjectGroup) {
			$SubjectGroupIDArr = $libSCM->Get_Copied_SubjectGroup($FromSubjectGroupID, $ToYearTermID);
			$ToSubjectGroupID = $SubjectGroupIDArr[0];
			
			$SubjectGroupIDMapping[$FromSubjectGroupID] = $ToSubjectGroupID;
		}
		
		$libTimetable = new Timetable();
		
		### Get Timetime of the From Term
		$FromTimetableArr = $libTimetable->Get_All_TimeTable($FromAcademicYearID, $FromYearTermID);
		$FromTimetableIDArr = Get_Array_By_Key($FromTimetableArr, 'TimetableID');
		$numOfFromTimetable = count($FromTimetableIDArr);
	
		### Find the corresponding Timetable in the ToYearTermID
		### if no copied timetable => create one
		for ($i=0; $i<$numOfFromTimetable; $i++) {
			$this_FromTimetableID = $FromTimetableIDArr[$i];
			$this_ObjFromTimetable = new Timetable($this_FromTimetableID);
			
			$this_CopiedTimetableArr = $this_ObjFromTimetable->Get_Copied_Timetable($ToYearTermID);
			$this_ToTimetableID = $this_CopiedTimetableArr[0];
			
			if ($this_ToTimetableID == '') {
				# not copied => create a new one
				$DataArr['AcademicYearID'] = $ToAcademicYearID;
				$DataArr['YearTermID'] = $ToYearTermID;
				$DataArr['TimetableName'] = $Lang['SysMgr']['SubjectClassMapping']['CopyFrom'].' '.$this_ObjFromTimetable->TimetableName;
				$DataArr['CopyFromTimetableID'] = $this_FromTimetableID;
				
				$this_ToTimetableID = $libTimetable->Insert_Record($DataArr);
				$successArr[$this_FromTimetableID]['Insert_Timetable'] = $this_ToTimetableID;
			}
			
			# Copy the timetable
			$successArr[$this_FromTimetableID]['Copy_Timetable'] = $libTimetable->Copy_Timetable($this_FromTimetableID, $this_ToTimetableID, $SubjectGroupIDMapping);
		}
	}
	
	$returnMsg = '';
	if (in_array(false, $SuccessArr)) {
		$libSCM->RollBack_Trans();
		$ReturnMessageKey = 'CopySubjectGroupFailed';
		$successCount = 0;
	}
	else {
		$libSCM->Commit_Trans();
		$ReturnMessageKey = 'CopySubjectGroupSuccess';
		$successCount = count($Copy_SubjectGroup_Arr);
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		if ($successCount > 0) {
			$thisJSUpdate .= 'window.parent.showQuickEditInstruction();'."\n";
		}
		$thisJSUpdate .= 'window.parent.document.getElementById("successCountSpan").innerHTML = "'.$successCount.'";'."\n";
		$thisJSUpdate .= 'window.parent.showCopySuccessMsgSpan();'."\n";
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
intranet_closedb();
?>