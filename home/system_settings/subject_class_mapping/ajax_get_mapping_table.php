<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$YearTermID = $_REQUEST['YearTermID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$HideGroupLayers = $_REQUEST['HideGroupLayers'];
$HideNonRelate = $_REQUEST['HideNonRelate'];
$FormID = (is_array($_REQUEST['FormID']))? $_REQUEST['FormID']:array();
$SortingOrder = $_REQUEST['SortingOrder'];

$SubjectClassUI = new subject_class_mapping_ui();

echo $SubjectClassUI->Get_Subject_Class_Mapping_Table($AcademicYearID,$YearTermID,$HideGroupLayers,$FormID,$HideNonRelate,$SortingOrder);

intranet_closedb();
?>