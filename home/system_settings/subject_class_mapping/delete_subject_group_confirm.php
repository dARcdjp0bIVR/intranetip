<?php
// using 

/*	Change Log:
 * 
 * 	Date: 2020-05-07 Tommy
 *      - modified access checking, added $plugin["Disable_Subject"]
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];
$SubjectID = $_REQUEST['SubjectID'];

$linterface = new interface_html();

# Get Return Message
$ReturnMessageKey = $_REQUEST['ReturnMessageKey'];
$ReturnMessage = $Lang['SysMgr']['SubjectClassMapping']['ReturnMessage'][$ReturnMessageKey];

### Title ###
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'],"index.php",0);
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'],"subject_group_mapping.php",1);
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('school_settings','subject');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$CurrentPageArr['Subjects'] = 1;
$linterface->LAYOUT_START($ReturnMessage);

$SubjecClassUI = new subject_class_mapping_ui();

echo $SubjecClassUI->Get_Delete_Subject_Group_Confirm_UI($AcademicYearID, $YearTermID, $SubjectID);
?>

<script>
function js_Go_Back(jsReturnMsgKey)
{
	var jsLocation = "subject_group_mapping.php?AcademicYearID=<?=$AcademicYearID?>&YearTermID=<?=$YearTermID?>";
	
	if (jsReturnMsgKey != '' && jsReturnMsgKey != null)
		jsLocation += "&ReturnMessageKey=" + jsReturnMsgKey;
	
	window.location = jsLocation;
}

function js_Delete_Subject_Group()
{
	if (confirm('<?=$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['DeleteSubjectGroup']?>'))
	{
		Block_Document();
			
		$.post(
			"ajax_delete_subject_group.php", 
			{ 
				YearTermID: '<?=$YearTermID?>',
				SubjectID: '<?=$SubjectID?>'
			},
			function(ReturnData)
			{
				//Get_Return_Message(DeleteClassAjax.responseText);
				
				var ReturnMessageKey;
				
				if (ReturnData == '1')
					ReturnMessageKey = 'ClassDeleteSuccess';
				else
					ReturnMessageKey = 'ClassDeleteUnsuccess';
					
				js_Go_Back(ReturnMessageKey);
			}
		);
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>