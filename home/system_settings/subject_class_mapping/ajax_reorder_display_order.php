<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$RecordType = stripslashes($_REQUEST['RecordType']);
$DisplayOrderString = stripslashes(urldecode($_REQUEST['DisplayOrderString']));

if ($RecordType == "LearningCategory")
{
	$lib = new Learning_Category();
	$displayOrderArr = $lib->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "LC_Row_");
}
else if ($RecordType == "Subject")
{
	$lib = new subject();
	$displayOrderArr = $lib->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "SubjectRow_");
}
else if ($RecordType == "SubjectComponent")
{
	$lib = new Subject_Component();
	$displayOrderArr = $lib->Get_Update_Display_Order_Arr($DisplayOrderString, ",", "SubjectRow_");
	
}
else
{
	$success = false;
}

# Insert Record
if ($lib != NULL)
{
	$success = $lib->Update_DisplayOrder($displayOrderArr);
}

echo $success;
intranet_closedb();

?>