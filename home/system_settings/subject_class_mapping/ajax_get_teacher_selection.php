<?php
// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$SubjectTermClass = new subject_term_class($SubjectGroupID,true,true);

$SubjectClassUI = new subject_class_mapping_ui();
$fcm = new form_class_manage();

$TeacherList = $fcm->Get_Teaching_Staff_List();

		
$x .= '<select name="TeacherList" id="TeacherList" onchange="Add_Teaching_Teacher();">
		<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
		
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$Hide = false;
	for ($j=0; $j< sizeof($SubjectTermClass->ClassTeacherList); $j++) {
		if ($TeacherList[$i]['UserID'] == $SubjectTermClass->ClassTeacherList[$j]['UserID']) {
			$Hide = true;
			break;
		}
	}
	if (!$Hide) 
		$x .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$x .= '</select>';

intranet_closedb();

echo $x;
?>