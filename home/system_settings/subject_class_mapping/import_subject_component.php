<?php
// Modifying by: 

############# Change Log [Start] ################
#	Date:	2020-05-07 Tommy
#           - modified access checking, added $plugin["Disable_Subject"]
#
# 	Date:	2015-10-16	Kenneth
#		- Style Alignment v30 in Table
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();


### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
$csvFile = "<a class='tablelink' href='". GET_CSV("import_subject_component_sample.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### data column
$DataColumnTitleArr = $Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponents'];
$DataColumnPropertyArr = array(1,1,1,1,1,1,1,1);
$RemarkArr = array('<a id="SubjectComponentID" class="tablelink" href="javascript:Load_Reference(\'SubjectComponentID\');">['.$Lang['General']['Remark'].']</a>','','','','','');
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr,$RemarkArr);
//$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr);


?>
<script language="javascript">

function Load_Reference(ref) {
	
	$('#remarkDiv_type').html('');
	//var AcademicYearID = $('#AcademicYearID').val();
//	console.log(AcademicYearID);
	$.post(
		"ajax_reload.php", 
		{ 
			RecordType: "load_reference",
			flag: ref
//			AcademicYearID: AcademicYearID

		},
		function(ReturnData)
		{
			$('#remarkDiv_type').html(ReturnData);
		}
	);
	
	displayReference(ref);
}

function displayReference(ref) {
	var p = $("#"+ref);
	var position = p.position();

	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}
</script>

<div id="remarkDiv_type" class="selectbox_layer" style="width:400px"></div>							

<br />
<form name="form1" method="post" action="import_subject_component_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
		
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr> 
   					<td>
       				<br />
						<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="field_title" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
								<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
							</tr>
							<tr>
								<td class="field_title" align="left"><?=$Lang['General']['CSVSample']?> </td>
								<td class="tabletext"><?=$csvFile?></td>
							</tr>
							<tr>
								<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
								<td class="tabletext"><?=$DataColumn?></td>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="tabletext" align="left" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr>
				<tr>
			        <td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
				</tr>	
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	
	
	</tr>
	<!--
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				
			</table>
		</td>
	</tr>
	-->
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>
			&nbsp; 
			<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
		</td>
	</tr>
</table>

</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
