<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$YearID = (is_array($_REQUEST['YearID']))? $_REQUEST['YearID']:array();
$StudentSelected = (is_array($_REQUEST['StudentSelected']))? $_REQUEST['StudentSelected']:array();
$AcademicYearID = $_REQUEST['AcademicYearID'];
/*echo '<pre>';
var_dump($_REQUEST);
echo '</pre>';
die;*/
$scm = new subject_class_mapping();

echo implode(',',$scm->Check_Selected_Student($StudentSelected,$YearID,$AcademicYearID));

intranet_closedb();
?>