<?php
// Modifying by: 
/**
 * Change Log:
 *	Date:	2020-05-07 Tommy
 *          - modified access checking, added $plugin["Disable_Subject"]
 * 
 * 2016-11-22 (Villa) [T108937]
 * 		- Update UI add import method
 * 
 * 2015-10-30 (Ivan) [E84744] [ip.2.5.7.1.1]
 * 		- removed the file remarks
 * 
 * 2015-10-23 (Pun)
 * 		- added checkbox to sync subject group to eClass
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

if ($AcademicYearID == '')
	$AcademicYearID = Get_Current_Academic_Year_ID();
$obj_AcademicYear = new academic_year($AcademicYearID);

if ($YearTermID == '')
	$YearTermID = getCurrentSemesterID();
$obj_YearTerm = new academic_year_term($YearTermID);

### Title / Menu
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
$csvFile = "<a class='tablelink' href='". GET_CSV("import_subject_group_user_sample.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$csvFile2 = "<a class='tablelink' href='". GET_CSV("import_subject_group_user_sample2.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";




?>

<br/>
<form name="form1" method="post" action="import_subject_group_user_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?></td>
					<td class="tabletext" ><?=$obj_AcademicYear->Get_Academic_Year_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['Term']?></td>
					<td class="tabletext" ><?=$obj_YearTerm->Get_Year_Term_Name()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="right"><?=$Lang['Import']['Method']['Title']?></td>
					<td class="tabletext">
						<input type="radio" id="import_method_1" name="import_method" value=0 checked ><label for="import_method_1"><?=$Lang['Import']['Method']['UserLogin'] ?></label>
						<input type="radio" id="import_method_2" name="import_method" value=1 ><label for="import_method_2"><?=$Lang['Import']['Method']['STRN'] ?></label>
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext">
					<div id="csv_import1"><?=$csvFile?></div>
					<div id="csv_import2" style="display:none"><?=$csvFile2?></div>
					</td>
				</tr>
<?php if($sys_custom['SyncSubjectGroupToClassroom']){ ?>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['newStd2eclass']?> </td>
					<td class="tabletext">
						<?php 
							$checked = ($sys_custom['SubjectGroup']['DefaultSyncStudentToClassroom'])?'checked="checked"':'';
						?>
						<input type='checkbox' value='1' name='addStd' id='addStd' <?=$checked?> >
					</td>
				</tr>
<?php } ?>
				<!--tr>
					<td class="tabletext" align="left" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr-->
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; <?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='subject_group_mapping.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>">

</form>
<br />

<script>
$( "#import_method_1" ).click(function() {
	 $("#csv_import1").show();
	 $("#csv_import2").hide();
	});
$( "#import_method_2" ).click(function() {
	 $("#csv_import1").hide();
	 $("#csv_import2").show();
	});

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
