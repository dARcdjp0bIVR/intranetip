<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

$sbj = new subject();
$result = array();
# remove existing relationship
$result[] = $sbj->Delete_Subject_Year_Relation();

# create new relationship
$result[] = $sbj->Create_Form_Subject($_POST);

if(in_array(false,$result))
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
else 
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];

intranet_closedb();
?>