<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

$YearTermID = $_REQUEST['YearTermID'];
$AcademicYearID = $_REQUEST['AcademicYearID'];
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$OrderByStudentName = $_REQUEST['OrderByStudentName'];
$SortingOrder = $_REQUEST['SortingOrder'];


$SubjectClassUI = new subject_class_mapping_ui();

echo $SubjectClassUI->Get_Subject_Class_Detail($AcademicYearID, $YearTermID, $SubjectGroupID, $OrderByStudentName, $SortingOrder);

intranet_closedb();
?>