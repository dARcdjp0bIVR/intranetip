<?
# using: ivan

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();
$lexport = new libexporttext();
$libSubject = new subject();

## Get all subjects info
$SubjectInfoArr = $libSubject->Get_Subject_List($returnAsso=1);

## Get all learning category info
$libLC = new Learning_Category();
$LearningCategoryInfoArr = $libLC->Get_All_Learning_Category($returnAsso=1);


## construct export array
$exportContentArr = array();
$subject_counter = 0;
foreach ($SubjectInfoArr as $LearningCategoryID => $thisSubjectList)
{
	$thisLC_Code = $LearningCategoryInfoArr[$LearningCategoryID]['Code'];
	$thisLC_Name = $LearningCategoryInfoArr[$LearningCategoryID]['Name'];
	
	foreach ($thisSubjectList as $SubjectID => $SubjectInfoArr)
	{
		$exportContentArr[$subject_counter][] = $thisLC_Code;
		$exportContentArr[$subject_counter][] = $thisLC_Name;
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['CODEID'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['EN_DES'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['CH_DES'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['EN_ABBR'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['CH_ABBR'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['EN_SNAME'];
		$exportContentArr[$subject_counter][] = $SubjectInfoArr['CH_SNAME'];
		
		$subject_counter++;
	}
}

$exportHeader = array("Learning Category Code", "Learning Category Name", "Subject WebSAMS Code", "Name (Eng)", "Name (Chi)", "Abbreviation (Eng)", "Abbreviation (Chi)", "Short Form (Eng)", "Short Form (Chi)");
$export_content = $lexport->GET_EXPORT_TXT($exportContentArr, $exportHeader);

$filename = "subject_list.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>