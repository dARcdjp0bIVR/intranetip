<?
# using: ivan

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();
$lexport = new libexporttext();
$libSCM = new subject_class_mapping();
$libFCM = new form_class_manage();

# Get Academic Year Name for csv file name
$AcademicYearID = $_REQUEST['AcademicYearID'];
$obj_AcademicYear = new academic_year($AcademicYearID);
$AcademicYearName = $obj_AcademicYear->Get_Academic_Year_Name();

# Get Term Name for csv file name
$YearTermID = $_REQUEST['YearTermID'];
$obj_YearTerm = new academic_year_term($YearTermID);
$YearTermName = $obj_YearTerm->Get_Year_Term_Name();


# Get all subject groups
$AllSubjectGroupInfoArr = $libSCM->Get_Subject_Group_List($YearTermID, $SubjectID='', $returnAssociate=1);
$SubjectGroupInfoArr = $AllSubjectGroupInfoArr['SubjectGroupList'];

$exportContentArr = array();

if (count($SubjectGroupInfoArr) > 0)
{
	# Loop each Subject Group to get teacher and student list
	foreach ($SubjectGroupInfoArr as $thisSubjectGroupID => $thisSubjectGroupInfoArr)
	{
		$thisSubjectGroupCode = $thisSubjectGroupInfoArr['ClassCode'];
		
		$obj_SubjectGroup = new subject_term_class($thisSubjectGroupID, true, true);
		$thisSubjectGroupTitle = $obj_SubjectGroup->Get_Class_Title();
		
		# Get Subject Info
		$obj_Subject = new subject($obj_SubjectGroup->SubjectID);
		$SubjectName = $obj_Subject->Get_Subject_Desc();
		
		# Get Teacher list
		$thisTeacherArr = $obj_SubjectGroup->ClassTeacherList;
		$numOfTeacher = count($thisTeacherArr);
		for ($i=0; $i<$numOfTeacher; $i++)
		{
			$thisEnglishName = $thisTeacherArr[$i]['EnglishName'];
			$thisChineseName = $thisTeacherArr[$i]['ChineseName'];
			$thisUserLogin = $thisTeacherArr[$i]['UserLogin'];
			$thisUserType = 'Teacher';
			$exportContentArr[] = array($SubjectName, $thisSubjectGroupTitle, $thisSubjectGroupCode, $thisEnglishName, $thisChineseName, $thisUserLogin, $thisUserType);
		}
		
		# Get Student list
		$thisStudentArr = $obj_SubjectGroup->ClassStudentList;
		$numOfStudent = count($thisStudentArr);
		
		$thisStudentClassAry = $libFCM->Get_Student_Class_Info_In_AcademicYear(Get_Array_By_Key($thisStudentArr, 'UserID'), $AcademicYearID);
		$thisStudentClassAssoAry = BuildMultiKeyAssoc($thisStudentClassAry, 'UserID');
		unset($thisStudentClassAry);
		
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisUserID = $thisStudentArr[$i]['UserID'];
			$thisEnglishName = $thisStudentArr[$i]['EnglishName'];
			$thisChineseName = $thisStudentArr[$i]['ChineseName'];
			$thisUserLogin = $thisStudentArr[$i]['UserLogin'];
			$thisUserType = 'Student';
			$thisClassName = $thisStudentClassAssoAry[$thisUserID]['ClassTitleEN'];
			$thisClassNumber = $thisStudentClassAssoAry[$thisUserID]['ClassNumber'];
			
			$exportContentArr[] = array($SubjectName, $thisSubjectGroupTitle, $thisSubjectGroupCode, $thisEnglishName, $thisChineseName, $thisUserLogin, $thisUserType, $thisClassName, $thisClassNumber);
		}
	}
}

$exportHeader = array("Subject Name", "Subject Group Name", "Subject Group Code", "User English Name", "User Chinese Name", "User Login ID", "User Type", "Class Name (EN)", "Class Number");
$export_content = $lexport->GET_EXPORT_TXT($exportContentArr, $exportHeader, "", "\r\n", "", 0, "11");

$filename = $AcademicYearName."_".$YearTermName."_subject_group_user_list";
$filename = iconv("UTF-8", "Big5", $filename);
$filename = $filename.".csv";

$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>