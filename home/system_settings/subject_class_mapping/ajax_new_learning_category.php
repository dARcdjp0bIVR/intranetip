<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$DataArr['Code'] = stripslashes(urldecode($_REQUEST['Code']));
$DataArr['NameEng'] = stripslashes(urldecode($_REQUEST['TitleEn']));
$DataArr['NameChi'] = stripslashes(urldecode($_REQUEST['TitleCh']));

$libLC = new Learning_Category();

# Insert Record
$success = $libLC->Insert_Record($DataArr);


echo $success;
intranet_closedb();

?>