<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_opendb();

# Get data
$isComponent = stripslashes($_REQUEST['isComponent']);
$SubjectID = stripslashes($_REQUEST['SubjectID']);
$DataArr = array();

if ($isComponent)
{
	$DataArr['CMP_CODEID'] = stripslashes(urldecode($_REQUEST['Code']));
	$DataArr['EN_DES'] = stripslashes(urldecode($_REQUEST['TitleEn']));
	$DataArr['CH_DES'] = stripslashes(urldecode($_REQUEST['TitleCh']));
	$DataArr['EN_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrEn']));
	$DataArr['CH_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrCh']));
	$DataArr['EN_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormEn']));
	$DataArr['CH_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormCh']));
	
	$Subject_Obj = new Subject_Component($SubjectID);
}
else
{
	$DataArr['CODEID'] = stripslashes(urldecode($_REQUEST['Code']));
	$DataArr['EN_DES'] = stripslashes(urldecode($_REQUEST['TitleEn']));
	$DataArr['CH_DES'] = stripslashes(urldecode($_REQUEST['TitleCh']));
	$DataArr['EN_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrEn']));
	$DataArr['CH_ABBR'] = stripslashes(urldecode($_REQUEST['AbbrCh']));
	$DataArr['EN_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormEn']));
	$DataArr['CH_SNAME'] = stripslashes(urldecode($_REQUEST['ShortFormCh']));
	$DataArr['LearningCategoryID'] = stripslashes($_REQUEST['LearningCategoryID']);
	$DataArr['CMP_CODEID'] = '';

	$Subject_Obj = new subject($SubjectID);
}


# Update Record
$success = $Subject_Obj->Update_Record($DataArr);


echo $success;
intranet_closedb();

?>