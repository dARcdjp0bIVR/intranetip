<?
// Modifying by: 

/* 
 * 2020-05-07 Tommy: modified access checking, added $plugin["Disable_Subject"]
 * 2010-04-15 Ivan: change the WebSAMS Code length checking to max 10 characters due to Taiwan case
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) || $plugin["Disable_Subject"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_subject_component.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$file_format = array("Parent Subject WebSAMS Code", "Subject Componenet WebSAMS Code", "Subject Componenet Name (Eng)", "Subject Componenet Name (Chi)", "Abbreviation (Eng)", "Abbreviation (Chi)", "Short Form (Eng)", "Short Form (Chi)");
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_subject_component.php?xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();
$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent']);
$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_COMPONENT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		ParentSubjectWebSAMSCode varchar(50),
		WebSAMSCode varchar(50),
		EN_DES varchar(255),
		CH_DES varchar(255),
		EN_ABBR varchar(100),
		CH_ABBR varchar(100),
		EN_SNAME varchar(20),
		CH_SNAME varchar(20),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql) or die(mysql_error());

# delete the temp data in temp table 
$sql = "delete from TEMP_SUBJECT_COMPONENT_IMPORT where UserID=".$_SESSION["UserID"];
$li->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=1;


$InsertArr = array();
foreach($data as $key => $data)
{
	$i++;
	$error_msg = array();
	
	### store csv data to temp data
	list($ParentSubjectWebSAMSCode, $WebSAMSCode, $EN_DES, $CH_DES, $EN_ABBR, $CH_ABBR, $EN_SNAME, $CH_SNAME) = $data;
	$ParentSubjectWebSAMSCode = trim($ParentSubjectWebSAMSCode);
	$WebSAMSCode = trim($WebSAMSCode);
	$EN_DES = trim($EN_DES);
	$CH_DES = trim($CH_DES);
	$EN_ABBR = trim($EN_ABBR);
	$CH_ABBR = trim($CH_ABBR);
	$EN_SNAME = trim($EN_SNAME);
	$CH_SNAME = trim($CH_SNAME);
	
	### check data
	# 1. Check empty data
	if(empty($ParentSubjectWebSAMSCode) || empty($WebSAMSCode) || empty($EN_DES) || empty($CH_DES) || empty($EN_ABBR) || empty($CH_ABBR) || empty($EN_SNAME) || empty($CH_SNAME))
		$error_msg[] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		
	# 2. Check if the Parent Subject exists
	$libSubject = new subject();
	if ($libSubject->Is_Record_Existed($ParentSubjectWebSAMSCode) == false)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'];
		
	# 3. Check if the component subject exists
	$thisParentSubjectIDArr = $libSubject->Get_Subject_By_SubjectCode($ParentSubjectWebSAMSCode);
	$thisParentSubjectID = $thisParentSubjectIDArr[0]['RecordID'];
	$libSubjectComponent = new Subject_Component();
	if ($libSubjectComponent->Is_Code_Existed($WebSAMSCode, '', $thisParentSubjectID) == true)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeInUseWarning'];
	
	# 4. Check if Subject Group Code Duplicated within the csv file
	$sql = "Select 
					TempID 
			From 
					TEMP_SUBJECT_COMPONENT_IMPORT 
			Where 
					ParentSubjectWebSAMSCode = '".$li->Get_Safe_Sql_Query($ParentSubjectWebSAMSCode)."' 
					And
					WebSAMSCode = '".$li->Get_Safe_Sql_Query($WebSAMSCode)."' 
					And 
					UserID = '".$_SESSION["UserID"]."'
			";
	$TempArr = $li->returnVector($sql);
	if (count($TempArr) > 0)
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['ComponentCodeDuplicatedWithinCSVWarning'];
		
	# 5. Check if the WebSAMS Code is within 3 characters long
	$libSCM_ui = new subject_class_mapping_ui();
	if (strlen($WebSAMSCode) > $libSCM_ui->maxLength['WebSAMSCode'])
		$error_msg[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThan10Char'];
	
	
	# insert temp record
	$sql = "
			insert into TEMP_SUBJECT_COMPONENT_IMPORT 
			(UserID, RowNumber, ParentSubjectWebSAMSCode, WebSAMSCode, EN_DES, CH_DES, EN_ABBR, CH_ABBR, EN_SNAME, CH_SNAME, DateInput)
			values
			(	". $_SESSION["UserID"] .", 
				$i, 
				'".$li->Get_Safe_Sql_Query($ParentSubjectWebSAMSCode)."',
				'".$li->Get_Safe_Sql_Query($WebSAMSCode)."',
				'".$li->Get_Safe_Sql_Query($EN_DES)."',
				'".$li->Get_Safe_Sql_Query($CH_DES)."',
				'".$li->Get_Safe_Sql_Query($EN_ABBR)."',
				'".$li->Get_Safe_Sql_Query($CH_ABBR)."',
				'".$li->Get_Safe_Sql_Query($EN_SNAME)."',
				'".$li->Get_Safe_Sql_Query($CH_SNAME)."',
				now())
			";
	$li->db_db_query($sql) or die(mysql_error());
	$TempID = $li->db_insert_id();
	
	
	if(empty($error_msg))
	{
		$successCount++;
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectCode'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeComponent'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['TitleEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['TitleCh'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['AbbreviationEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['AbbreviationCh'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ShortFormEn'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['SysMgr']['SubjectClassMapping']['ShortFormCh'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	
	$error_TempID_str = substr($error_TempID_str, 0,-1);
	$sql = "select * from TEMP_SUBJECT_COMPONENT_IMPORT where TempID in ($error_TempID_str)";
	$tempData = $li->returnArray($sql);
	
	$i=0;
	foreach($tempData as $k=>$d)
	{
		list($t_TempID, $t_UserID, $t_RowNumber, $t_ParentSubjectWebSAMSCode, $t_ComponentWebSAMSCode, $t_EN_DES, $t_CH_DES, $t_EN_ABBR, $t_CH_ABBR, $t_EN_SNAME, $t_CH_SNAME) = $d;
		
		$t_ParentSubjectWebSAMSCode = $t_ParentSubjectWebSAMSCode ? $t_ParentSubjectWebSAMSCode : "<font color='red'>***</font>";
		$t_ComponentWebSAMSCode = $t_ComponentWebSAMSCode ? $t_ComponentWebSAMSCode : "<font color='red'>***</font>";
		$t_EN_DES = $t_EN_DES ? $t_EN_DES : "<font color='red'>***</font>";
		$t_CH_DES = $t_CH_DES ? $t_CH_DES : "<font color='red'>***</font>";
		$t_EN_ABBR = $t_EN_ABBR ? $t_EN_ABBR : "<font color='red'>***</font>";
		$t_CH_ABBR = $t_CH_ABBR ? $t_CH_ABBR : "<font color='red'>***</font>";
		$t_EN_SNAME = $t_EN_SNAME ? $t_EN_SNAME : "<font color='red'>***</font>";
		$t_CH_SNAME = $t_CH_SNAME ? $t_CH_SNAME : "<font color='red'>***</font>";
		
		$thisErrorArr = $error_result[$t_TempID];
		
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'], $thisErrorArr))
			$t_ParentSubjectWebSAMSCode = "<font color='red'>".$t_ParentSubjectWebSAMSCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeInUseWarning'], $thisErrorArr))
			$t_ComponentWebSAMSCode = "<font color='red'>".$t_ComponentWebSAMSCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['ComponentCodeDuplicatedWithinCSVWarning'], $thisErrorArr))
			$t_ComponentWebSAMSCode = "<font color='red'>".$t_ComponentWebSAMSCode."</font>";
			
		if (in_array($Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThan10Char'], $thisErrorArr))
			$t_ComponentWebSAMSCode = "<font color='red'>".$t_ComponentWebSAMSCode."</font>";
		
		if (is_array($error_result[$t_TempID]))
			$errorDisplay = implode('<br />', $error_result[$t_TempID]);
		else
			$errorDisplay = $error_result[$t_TempID];
		
		$css_i = ($i % 2) ? "2" : "";
		$x .= "<tr style='vertical-align:top'>";
		$x .= "<td class=\"tablebluerow$css_i\" width=\"10\">". $t_RowNumber ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ParentSubjectWebSAMSCode ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_ComponentWebSAMSCode ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_EN_DES ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_CH_DES ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_EN_ABBR ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_CH_ABBR ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_EN_SNAME ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $t_CH_SNAME ."</td>";
		$x .= "<td class=\"tablebluerow$css_i\">". $errorDisplay ."</td>";
		$x .= "</tr>";
		
		$i++;
	}
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_subject_component.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_subject_component.php'");
}
?>

<br />
<form name="form1" method="post" action="import_subject_component_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
