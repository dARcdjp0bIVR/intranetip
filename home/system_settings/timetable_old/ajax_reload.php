<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "SubjectGroupCheckboxSelection")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui_old.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	
	$returnString = $libtimetable_ui->Get_Subject_Group_Checkbox_Selection_Div($SubjectID, $YearTermID);
}
else if ($RecordType == "SubjectGroupSelectionDiv")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_class_mapping_ui = new subject_class_mapping_ui();
	
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$TimeSessionID = stripslashes($_REQUEST['TimeSessionID']);
	$CycleDays = stripslashes($_REQUEST['CycleDays']);
	$Day = stripslashes($_REQUEST['Day']);
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	
	$returnString = $libsubject_class_mapping_ui->Get_Subject_Group_Selection($SubjectID, 
																		"SelectedSubjectGroupID", 
																		"",
																		"js_Validate_Subject_Group_Overlap($TimeSessionID, $CycleDays, $Day, $RoomAllocationID)",
																		$YearTermID
																		);
}
else if ($RecordType == "Reload_Cycle_Days_Selection")
{
	include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
	$libcycleperiods_ui = new libcycleperiods_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	
	$returnString = $libcycleperiods_ui->getCycleDaysSelection("SelectedCyclePeriodID","SelectedCyclePeriodID", $selected="", $onchange="", $AcademicYearID, $NoFirst);
}
else if ($RecordType == "Reload_Cycle_Period_Selection")
{
	include_once($PATH_WRT_ROOT."includes/libcycleperiods_ui.php");
	$libcycleperiods_ui = new libcycleperiods_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	
	$returnString = $libcycleperiods_ui->getCyclePeriodSelection("SelectedCyclePeriodID","SelectedCyclePeriodID", $selected="", $onchange="", $AcademicYearID, $NoFirst);
}
else if ($RecordType == "Reload_Semester_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	
	$returnString = $libsubject_ui->Get_Term_Selection("SelectedYearTermID", $AcademicYearID, $selected="", "js_Update_Subject_Selection()", $NoFirst);
}
else if ($RecordType == "Reload_Floor_Selection")
{
	include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
	$liblocation_ui = new liblocation_ui();
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	$FloorSelectionObjID = stripslashes($_REQUEST['FloorSelectionObjID']);
	$RoomSelectionDivID = stripslashes($_REQUEST['RoomSelectionDivID']);
	$RoomSelectionID = stripslashes($_REQUEST['RoomSelectionID']);
	$EmptySymbol = stripslashes($_REQUEST['EmptySymbol']);
	
	$returnString = $liblocation_ui->Get_Floor_Selection($BuildingID, "", $FloorSelectionObjID, 
							"js_Change_Room_Selection('$FloorSelectionObjID', '$RoomSelectionDivID', '$RoomSelectionID', '$EmptySymbol')", 1);
}
else if ($RecordType == "Reload_Room_Selection")
{
	include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
	$liblocation_ui = new liblocation_ui();
	
	$LocationLevelID = stripslashes($_REQUEST['LocationLevelID']);
	$RoomSelectionObjID = stripslashes($_REQUEST['RoomSelectionObjID']);
	
	$returnString = $liblocation_ui->Get_Room_Selection($LocationLevelID, "", $RoomSelectionObjID, "", 1);
}
else if ($RecordType == "Reload_Subject_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$ID = stripslashes($_REQUEST['ID']);
	
	$returnString = $libsubject_ui->Get_Subject_Selection($ID, $SelectedSubjectID='', "js_Show_Hide_Subject_Group_Selection_Div()", 
															0, $Lang['SysMgr']['SubjectClassMapping']['All']['Subject'], $YearTermID, "js_Show_Hide_Subject_Group_Selection_Div()");
}
else if ($RecordType == "Reload_Timetable")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui_old.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$PeriodID = stripslashes($_REQUEST['PeriodID']);
	$CycleDays = stripslashes($_REQUEST['CycleDays']);
	$CyclePeriodID = stripslashes($_REQUEST['CyclePeriodID']);
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$LocationID = stripslashes($_REQUEST['LocationID']);
	$isViewMode = stripslashes($_REQUEST['isViewMode']);
	
	if ($SubjectID == "")
	{
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		$libsubject_class_mapping = new subject_class_mapping();
		
		# All Subjects
		$SubjectGroupArr = $libsubject_class_mapping->Get_Subject_Group_List($YearTermID, '', 0);
		$numOfSubjectGroup = count($SubjectGroupArr);
		
		$SubjectGroupIDArr = array();
		for ($i=0; $i<$numOfSubjectGroup; $i++)
		{
			$thisSubjectGroupID = $SubjectGroupArr[$i]['SubjectGroupID'];
			$SubjectGroupIDArr[] = $thisSubjectGroupID;
		}
	}
	else
	{
		# Specific Subjects
		$SubjectGroupIDList = stripslashes($_REQUEST['SubjectGroupIDList']);
		$SubjectGroupIDArr = explode(",", $SubjectGroupIDList);
		$SubjectGroupIDArr = array_remove_empty($SubjectGroupIDArr);
	}
	
	$returnString = $libtimetable_ui->Get_Timetable_Table($PeriodID, $CycleDays, $CyclePeriodID, $SubjectGroupIDArr, $LocationID, $DisplayOptionArr=array(), $isViewMode);
}


else if ($RecordType == "Reload_Secondary_Filter_Table")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui_old.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$ViewMode = stripslashes($_REQUEST['ViewMode']);
	
	$returnString = $libtimetable_ui->Get_View_Secondary_Settings_Table($ViewMode);
}
else if ($RecordType == "Reload_Class_Selection")
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	$form_class_manage_ui = new form_class_manage_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = stripslashes($_REQUEST['ID']);
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearID = stripslashes($_REQUEST['YearID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
	$OnChange = stripslashes($_REQUEST['OnChange']);
	
	$returnString = $form_class_manage_ui->Get_Class_Selection($AcademicYearID, $YearID, $ID, $SelectedYearClassID='', $OnChange, $NoFirst, $IsMultiple);
	
	if ($IsMultiple)
		$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}
else if ($RecordType == "Reload_Subject_Group_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = stripslashes($_REQUEST['ID']);
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);

	$returnString = $libsubject_ui->Get_Subject_Group_Selection($SubjectID, $ID, '', '', $YearTermID, $IsMultiple);
	$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}
else if ($RecordType == "Reload_Target_Selection")
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	$form_class_manage_ui = new form_class_manage_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = stripslashes($_REQUEST['ID']);
	$TargetType = stripslashes($_REQUEST['TargetType']);
	$YearClassID = stripslashes($_REQUEST['YearClassID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);

	if ($TargetType == "Student")
	{
		$returnString = $form_class_manage_ui->Get_Student_Selection($ID, $YearClassID, '', '', $NoFirst, $IsMultiple);
	}
	else
	{
		$isTeachingStaff = ($TargetType == "TeachingStaff")? 1 : 0;
		$returnString = $form_class_manage_ui->Get_Staff_Selection($ID, $isTeachingStaff, '', '', $NoFirst, $IsMultiple);
	}
	
	$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}


echo $returnString;

intranet_closedb();

?>