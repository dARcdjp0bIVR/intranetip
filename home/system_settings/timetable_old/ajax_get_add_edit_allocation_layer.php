<?php
// using ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_opendb();

$libtimetable_ui = new libtimetable_ui();

# Get data
$TimeSessionID = stripslashes($_REQUEST['TimeSessionID']);
$CycleDays = stripslashes($_REQUEST['CycleDays']);
$Day = stripslashes($_REQUEST['Day']);
$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
$YearTermID = stripslashes($_REQUEST['YearTermID']);

$returnDiv = $libtimetable_ui->Get_Add_Edit_Room_Allocation_Layer($TimeSessionID, $CycleDays, $Day, $YearTermID, $RoomAllocationID);


echo $returnDiv;


intranet_closedb();
?>