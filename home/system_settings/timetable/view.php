<?php
// using 

############# Change Log [Start] ################
#
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
# Date: 2019-05-20 Cameron
#       - add everyday timetable tag
# Date:	2017-09-12 Paul
#		- Bring the two option filters to front
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();

$arrCookies = array();
$arrCookies[] = array("ck_schoolSettings_timetable_from_eService", "From_eService");			
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$From_eService = (isset($_GET['From_eService']))? $_GET['From_eService'] : $From_eService;
	updateGetCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

### Check access right
$ValidUser = false;
if ($From_eService) {
	// check if the user is a teaching staff
	if ($_SESSION['UserType'] == USERTYPE_STAFF) {
		$ValidUser = true;
	}
	
	$CurrentPageArr['eServiceTimetable'] = 1;
	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View']);
}
else {
	// check admin management access right
    if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) {
		$ValidUser = true;
	}
	
	$CurrentPageArr['Timetable'] = 1;
	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['Management'], "index.php?clearCoo=1", 0);
	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View'], "view.php?clearCoo=1", 1);
	if ($sys_custom['eBooking']['EverydayTimetable']) {
	    $TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['EverydayTimetable'], "everyday_timetable.php?clearCoo=1", 3);
	}
}

if($plugin["Disable_Timetable"])
    $ValidUser = false;

if(!$ValidUser) {
	No_Access_Right_Pop_Up();
}

# change page web title
$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
$js.= 'document.title="eClass Timetable";'."\n";
$js.= '</script>'."\n";

$linterface = new interface_html();
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'].$js;
$linterface->LAYOUT_START(); 

$libtimetable_ui = new libtimetable_ui();

$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();

$viewModeTab = $libtimetable_ui->Get_Timetable_View_Mode_Tab('Timetable');

## Show Hide Options btn
$ShowHideBtnDiv = '';
$ShowHideBtnDiv .= '<div id="ShowHideOptionBtnDiv" style="display:none" align="left">';
	$ShowHideBtnDiv .= '<span id="spanShowOption">';
		$ShowHideBtnDiv .= '<a href="javascript:js_Show_Hide_Option(1);" class="contenttool">';
			$ShowHideBtnDiv .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">';
			$ShowHideBtnDiv .= $Lang['Btn']['Show'];
		$ShowHideBtnDiv .= '</a>';
	$ShowHideBtnDiv .= '</span>';
	$ShowHideBtnDiv .= '<span id="spanHideOption" style="display:none">';
		$ShowHideBtnDiv .= '<a href="javascript:js_Show_Hide_Option(0);" class="contenttool">';
			$ShowHideBtnDiv .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">';
			$ShowHideBtnDiv .= $Lang['Btn']['Hide'];
		$ShowHideBtnDiv .= '</a>';
	$ShowHideBtnDiv .= '</span>';
$ShowHideBtnDiv .= '</div>';
$ShowHideBtnDiv .= '<br style="clear:both">';


## Options
$OptionsDiv = '';
$OptionsDiv .= '<div id="FilterDiv">'."\n";
	$OptionsDiv .= '<div id="MainFilterDiv">'."\n";
		$OptionsDiv .= $libtimetable_ui->Get_View_Settings_Table('', $From_eService);
	$OptionsDiv .= '</div>';
	$OptionsDiv .= '<div id="SecondaryFilterDiv">'."\n";
	$OptionsDiv .= '</div>';
$OptionsDiv .= '</div>';




# Display Option Button
$displayOption_jsOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
$displayOptionBtn = '<a href="#" class="tablelink" onclick="'.$displayOption_jsOnchange.'"> '.$Lang['SysMgr']['Timetable']['DisplayOption'].'</a>'."\n";

# Display Option Layer Table
$displayOptionTable .= $libtimetable_ui->Get_Display_Option_Div_Table();




## Filter Btn
$filter_jsOnchange = "js_Show_Hide_Filter_Div(); return false;";
$filterBtn = '<a href="#" class="tablelink" onclick="'.$filter_jsOnchange.'"> '.$Lang['SysMgr']['Timetable']['DataFiltering'].'</a>'."\n";

# Close Filter Table Button
$close_jsOnchange = "js_Show_Hide_Filter_Div(); return false;";
$closeFilterBtn = '<a href="#" class="tablelink" onclick="'.$close_jsOnchange.'"> '.$Lang['Btn']['Close'].'</a>'."\n";


# Filter Layer
$FilterBtnDiv .= '<div id="FilterButtonDiv" style="display:none;float:right;">'."\n";
	$FilterBtnDiv .= '<table id="FilterButtonTable">'."\n";
		$FilterBtnDiv .= '<tr>'."\n";
			# Filter button
			$FilterBtnDiv .= '<td>'."\n";
					$FilterBtnDiv .= $displayOptionBtn;
					$FilterBtnDiv .= ' | ';
					$FilterBtnDiv .= $filterBtn;
			$FilterBtnDiv .= '</td>'."\n";
		$FilterBtnDiv .= '</tr>'."\n";
	$FilterBtnDiv .= '</table>'."\n";
$FilterBtnDiv .= '</div>'."\n";
$FilterBtnDiv .= '<div style="clear:both">'."\n";

# Display Option Layer
$FilterBtnDiv .= '<div id="DisplayOptionTableDivTableDiv" style="float:right;position:relative;left:-200px;z-index:100;">'."\n";
	$FilterBtnDiv .= '<table id="DisplayOptionDivTable">'."\n";
		$FilterBtnDiv .= '<tr>'."\n";
			$FilterBtnDiv .= '<td>'."\n";
				$FilterBtnDiv .= '<div id="DisplayOptionDiv" class="selectbox_layer" style="width:200px;">'.$displayOptionTable.'</div>'."\n";
			$FilterBtnDiv .= '</td>'."\n";
		$FilterBtnDiv .= '</tr>'."\n";
	$FilterBtnDiv .= '</table>'."\n";
$FilterBtnDiv .= '</div>'."\n";

# Selection Layer
$FilterBtnDiv .= '<div id="FilterSelectionTableDivTableDiv" style="float:right;position:relative;left:-500px; z-index:100;">'."\n";
	$FilterBtnDiv .= '<table id="FilterSelectionDivTable">'."\n";
		$FilterBtnDiv .= '<tr>'."\n";
			$FilterBtnDiv .= '<td>'."\n";
				$FilterBtnDiv .= '<div id="FilterSelectionDiv" class="selectbox_layer" style="width:500px;">';
				
					# Filter Layer Table
					$FilterBtnDiv .= '<table id="FilterSelectionTable" class="form_table">'."\n";
						$FilterBtnDiv .= '<col class="field_title" style="vertical-align:top"/>'."\n";
						$FilterBtnDiv .= '<col class="field_c" style="vertical-align:top"/>'."\n";
						# Close Btn
						$FilterBtnDiv .= '<tr>'."\n";
							$FilterBtnDiv .= '<td colspan="3" style="text-align:right">'."\n";
								$FilterBtnDiv .= $closeFilterBtn;
							$FilterBtnDiv .= '</td>'."\n";
						$FilterBtnDiv .= '</tr>'."\n";
					$FilterBtnDiv .= '</table>'."\n";
					
					# Filter Selection Content
					// Copy View Page after 1st submission
					$FilterBtnDiv .= '<div id="ViewModeSelectionLayer"></div>';
				
				$FilterBtnDiv .= '</div>'."\n";
			$FilterBtnDiv .= '</td>'."\n";
		$FilterBtnDiv .= '</tr>'."\n";
	$FilterBtnDiv .= '</table>'."\n";
$FilterBtnDiv .= '</div>'."\n";


## Timetable 
$TimetableDiv = '';
$TimetableDiv .= '<div id="TimetableDiv" style="display:none">'."\n";
$TimetableDiv .= '</div>';

?>

<div id="IndexDebugArea"></div>

<?= $include_JS_CSS ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<?=$viewModeTab?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr><td align="right"><?= $FilterBtnDiv ?></td></tr>
				<tr> 
					<td id="OptionsTd" width="80%" align="center">
						<?= $ShowHideBtnDiv ?>
						<?= $OptionsDiv ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr> 
		<td class="main_content">
			<?= $TimetableDiv ?>
		</td>
	</tr>
	
</table>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>
var CurAcademicYearID = '';
var CurYearTermID = '';
var CurTimetableID = '';
var From_eService = '<?=$From_eService?>';

var jsShowHideSpeed = 'fast';

$(document).ready(function () {
	CurAcademicYearID = $('#SelectedAcademicYearID').val();
	CurYearTermID = $('#SelectedYearTermID').val();
	CurTimetableID = $('#SelectedTimetableID').val();
});


function js_Reload_Term_Selection(SelectedAcademicYearID)
{
	CurAcademicYearID = SelectedAcademicYearID;
	
	if (CurAcademicYearID != '')
	{
		$('#TermSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Term_Selection',
				AcademicYearID: SelectedAcademicYearID,
				NoFirst : 1,
				OnChange: 'js_Reload_Timetable_Selection(\'\', 0); MM_showHideLayers(\'SubjectGroupSelectionDiv\', \'\', \'hide\');'
			},
			function(ReturnData)
			{
				$('#TermSelectionDiv').show();
				js_Reload_Timetable_Selection('');
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

function js_Reload_Timetable_Selection(TimetableID)
{
	CurYearTermID = $('#SelectedYearTermID').val();
	CurTimetableID = TimetableID;
	
	$('#TimetableSelectionDiv').hide();
	
	$('#TimetableSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: "Reload_Timetable_Selection",
			TimetableID: CurTimetableID,
			AcademicYearID: CurAcademicYearID,
			YearTermID: CurYearTermID,
			ShowAllocatedOnly: 0,
			NoOnchange : 1,
			From_eService : From_eService
		},
		function(ReturnData)
		{
			$('#TimetableSelectionDiv').show();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
}

function js_Reload_Secondary_Selection()
{
	var SelectedViewMode = $('#SelectedViewMode').val();
	
	$('#SecondaryFilterDiv').hide();
	$('#SecondaryFilterDiv').html('');
	$('#SecondaryFilterDiv').show();
	
	if (SelectedViewMode != '')
		js_Reload_Secondary_Filter_Table(SelectedViewMode);
		
	$('#SecondaryFilterDiv').show();
}

function js_Reload_Secondary_Filter_Table(ViewMode)
{
	$('#SecondaryFilterDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Secondary_Filter_Table',
			YearTermID: CurYearTermID,
			ViewMode: ViewMode
		},
		function(ReturnData)
		{
			$('#SecondaryFilterDiv').show();
	
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}


/******* Class Mode  *******/
function js_Reload_Class_Selection(isMultiple)
{
	var selectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var selectedYearID = $('#SelectedYearID').val();
	
	$('#ClassSelectionTr').hide();
	$('#ClassSelectionDiv').hide();
	$("#SubmitBtnDiv").hide();
	
	if (selectedYearID != "")
	{
		var OnChange = '';
		if (isMultiple == 0)
		{
			// from Personal view mode
			OnChange = "js_Reload_Target_Selection('Student')";
		}
	
		$('#ClassSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Class_Selection',
				ID: 'SelectedYearClassID',
				AcademicYearID: selectedAcademicYearID,
				YearID: selectedYearID,
				OnChange: OnChange,
				NoFirst: 1,
				IsMultiple: isMultiple
			},
			function(ReturnData)
			{
				if (isMultiple == 1)
				{
					$("#SelectedYearClassID option").attr("selected","selected");
					$("#SubmitBtnDiv").show();
				}
				else
					js_Reload_Target_Selection('Student');
	
				$('#ClassSelectionTr').show();
				$('#ClassSelectionDiv').show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}
/******* End of Class Mode  *******/



/******* Subject Mode  *******/
function js_Reload_Subject_Group_Selection()
{
	var selectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var selectedYearTermID = $('#SelectedYearTermID').val();
	var selectedSubjectID = $('#SelectedSubjectID').val();
	
	$('#SubjectGroupSelectionTr').hide();
	$("#SubmitBtnDiv").hide();
	
	if (selectedSubjectID != '')
	{
		$('#SubjectGroupSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Subject_Group_Selection',
				ID: 'SelectedSubjectGroupID',
				AcademicYearID: selectedAcademicYearID,
				YearTermID: selectedYearTermID,
				SubjectID: selectedSubjectID,
				NoFirst: 1,
				IsMultiple: 1
			},
			function(ReturnData)
			{
				$("#SelectedSubjectGroupID option").attr("selected","selected");
				$('#SubjectGroupSelectionTr').show();
				
				$("#SubmitBtnDiv").show();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}
/******* End of Subject Mode  *******/



/******* Personal Mode  *******/
function js_Show_Hide_Form_Class_Selection()
{
	var selectedIdentity = $('#SelectedIdentity').val();
	$("#SubmitBtnDiv").hide();
	
	if (selectedIdentity == '')
	{
		$('#FormSelectionDiv').hide();
		$('#ClassSelectionDiv').hide();
		$('#PersonSelectionDiv').hide();
		$('#FormSelectionTr').hide();
		$('#ClassSelectionTr').hide();
		$('#PersonSelectionTr').hide();
		$('#PersonSelectionDiv').html('');
	}	
	else if (selectedIdentity == 'Student')
	{
		$('#FormSelectionTr').show();
		$('#ClassSelectionTr').hide();
		$('#PersonSelectionTr').hide();
	}
	else
	{
		$('#FormSelectionTr').hide();
		$('#ClassSelectionTr').hide();
		$('#PersonSelectionTr').show();
		js_Reload_Target_Selection(selectedIdentity);
	}
}

function js_Reload_Target_Selection(selectedIdentity)
{
	var selectedYearClassID = $('#SelectedYearClassID').val();
	$('#PersonSelectionTr').hide();
	$('#PersonSelectionDiv').hide();
	
	$('#PersonSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Target_Selection',
			ID: 'SelectedUserID',
			TargetType: selectedIdentity,
			YearClassID: selectedYearClassID,
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			$("#SelectedUserID option").attr("selected","selected");
			$('#PersonSelectionTr').show();
			$('#PersonSelectionDiv').show();
			
			$("#SubmitBtnDiv").show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}
/******* End of Personal Mode  *******/


/********* Report Generation *********/
function Generate_Timetable()
{
	var SelectedTimetableID = $('#SelectedTimetableID').val();
	var SelectedViewMode = $('#SelectedViewMode').val();
	
	if (SelectedTimetableID == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Timetable']?>');
		return false;
	}
		
	if (SelectedViewMode == '')
	{
		alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['ViewMode']?>');
		return false;
	}
		
	Block_Document();
	
	/* 20090707
	 * Past: eDis1.2 Design - Show/Hide selection layer at the top
	 * Now: Button at the top right to show/hide the selection layer
	 */
	// Add grey background to the options
	//$('#OptionsTd').addClass("report_show_option");
	
	// Show the "Show option btn"
	//js_Show_ShowHideOptionBtn();
	
	// Hide the options
	//js_Show_Hide_Option(0);
	
	// Reload the timetable
	js_Reload_Timetable();
}

function js_Show_Hide_Option(isShow)
{
	if (isShow)
	{
		// Show Options
		$('#FilterDiv').show();
		
		// Hide "Show Btn"
		$('#spanShowOption').hide();
		
		// Show "Hide Btn"
		$('#spanHideOption').show();
	}
	else
	{
		// Hide Options
		$('#FilterDiv').hide();
		
		// Show "Show Btn"
		$('#spanShowOption').show();
		
		// Hide "Hide Btn"
		$('#spanHideOption').hide();
	}
}

function js_Show_ShowHideOptionBtn()
{
	$('#ShowHideOptionBtnDiv').show();
}

function js_Reload_Timetable()
{
	var PrintDataInOneTimetable = $('#PrintDataInOneTimetable_1').attr('checked')? 1 : 0;
	var SelectedAcademicYearID = $('#SelectedAcademicYearID').val();
	var SelectedYearTermID = $('#SelectedYearTermID').val();
	var SelectedTimetableID = $('#SelectedTimetableID').val();
	var SelectedViewMode = $('#SelectedViewMode').val();
	var SelectedIdentity = $('#SelectedIdentity').val();

	var SelectedYearClassID;
	var SelectedSubjectGroupID;
	var SelectedUserID;
	var SelectedBuildingID;
	var SelectedLocationID;
	var OthersLocation;
	
	switch(SelectedViewMode)
	{
		case "Class":
			// Class Mode
			SelectedYearClassID = $('#SelectedYearClassID').val().join(',');
			
			var SelectedYearIDDefault = $('#SelectedYearID').val();
			var SelectedYearClassIDDefault = $('#SelectedYearClassID').val();
			break;
		case "SubjectGroup":
			// Subject Group Mode
			SelectedSubjectGroupID = $('#SelectedSubjectGroupID').val().join(',');
			
			var SelectedSubjectIDDefault = $('#SelectedSubjectID').val();
			var SelectedSubjectGroupIDDefault = $('#SelectedSubjectGroupID').val();
			break;
		case "Personal":
			// Personal Mode
			SelectedUserID = $('#SelectedUserID').val().join(',');
			
			var SelectedYearIDDefault = $('#SelectedYearID').val();
			var SelectedYearClassIDDefault = $('#SelectedYearClassID').val();
			var SelectedUserIDDefault = $('#SelectedUserID').val();
			break;
		case "Room":
			// Room Mode
			SelectedBuildingID = $('select#SelectedBuildingID').val();
			OthersLocation = encodeURIComponent(Trim($("input#OthersLocationTb").val()));
			if ($('select#SelectedLocationIDArr\\[\\]').val() != null)
				SelectedLocationID = $('select#SelectedLocationIDArr\\[\\]').val().join(',');
			
			var SelectedBuildingIDDefault = $('select#SelectedBuildingID').val();
			var SelectedLocationLevelIDDefault = $('select#SelectedLocationLevelID').val();
			var SelectedLocationIDDefault = $('select#SelectedLocationIDArr\\[\\]').val();
			break;
	}
  
	SelectedYearClassID = js_Check_Object_Defined(SelectedYearClassID);
	SelectedSubjectGroupID = js_Check_Object_Defined(SelectedSubjectGroupID);
	SelectedUserID = js_Check_Object_Defined(SelectedUserID);
	SelectedBuildingID = js_Check_Object_Defined(SelectedBuildingID);
	SelectedLocationID = js_Check_Object_Defined(SelectedLocationID);
	OthersLocation = js_Check_Object_Defined(OthersLocation);
	SelectedIdentity = js_Check_Object_Defined(SelectedIdentity);
	
  //Block_Document();
	$('#TimetableDiv').hide();
	
	$('#TimetableDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Timetable',
			TimetableID: SelectedTimetableID,
			ViewMode: SelectedViewMode,
			YearClassID: SelectedYearClassID,
			YearTermID: SelectedYearTermID,
			SubjectGroupID: SelectedSubjectGroupID,
			SelectedUserID: SelectedUserID,
			BuildingID: SelectedBuildingID,
			LocationID: SelectedLocationID,
			OthersLocation: OthersLocation,
			Identity: SelectedIdentity,
			IsViewMode: 1,
			PrintDataInOneTimetable: PrintDataInOneTimetable
		},
		function(ReturnData)
		{
			$('#TimetableDiv').show();
			
			if ($('#ViewModeSelectionLayer').html() == '')
			{
				/** For 1st Generation **/
				// Copy the default selection to the filter layer
				$('#ViewModeSelectionLayer').html($('#FilterDiv').html());
				$('#FilterDiv').html('');
				
				// Preset Filters (For firefox)
				if (PrintDataInOneTimetable == 1) {
					$('input#PrintDataInOneTimetable_1').attr('checked', 'checked');
					$('input#PrintDataInOneTimetable_0').attr('checked', '');
				}
				else {
					$('input#PrintDataInOneTimetable_1').attr('checked', '');
					$('input#PrintDataInOneTimetable_0').attr('checked', 'checked');
				}
				
				$('#SelectedAcademicYearID').val(SelectedAcademicYearID);
				$('select#SelectedYearTermID').val(SelectedYearTermID);
				$('select#SelectedTimetableID').val(SelectedTimetableID);
				$('select#SelectedViewMode').val(SelectedViewMode);
				$('select#SelectedIdentity').val(SelectedIdentity);
				
				switch(SelectedViewMode)
				{
					case "Class":
						// Class Mode
						$('select#SelectedYearID').val(SelectedYearIDDefault);
						$('select#SelectedYearClassID').val(SelectedYearClassIDDefault);
						break;
					case "SubjectGroup":
						// Subject Group Mode
						$('select#SelectedSubjectID').val(SelectedSubjectIDDefault);
						$('select#SelectedSubjectGroupID').val(SelectedSubjectGroupIDDefault);
						break;
					case "Personal":
						// Personal Mode
						$('select#SelectedYearID').val(SelectedYearIDDefault);
						$('select#SelectedYearClassID').val(SelectedYearClassIDDefault);
						$('select#SelectedUserID').val(SelectedUserIDDefault);
						break;
					case "Room":
						// Room Mode
						$('select#SelectedBuildingID').val(SelectedBuildingIDDefault);
						$('select#SelectedLocationLevelID').val(SelectedLocationLevelIDDefault);
						$('select#SelectedLocationIDArr\\[\\]').val(SelectedLocationIDDefault);
						break;
				}
				
				
				
				// delete the original selection
				$('#FilterDiv').html('');
				// Show Data Filtering Btn
				$('#FilterButtonDiv').show();
			}
			else
			{
				// Hide the Selection Layer
				js_Show_Hide_Filter_Div();
			}
			
			js_Update_Timetable_Display();
			UnBlock_Document();
			
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function print_timetable()
{
  newWindow('view_print.php',35);
}


/********* End of Report Generation *********/


</script>