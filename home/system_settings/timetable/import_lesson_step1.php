<?php
// Modifying by: 

############# Change Log [Start] ################
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$TimetableID = $_REQUEST['TimetableID'];
$linterface = new interface_html();
$libtimetable_ui = new libtimetable_ui();


### Title / Menu
$CurrentPageArr['Timetable'] = 1;
$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];
$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array($CurrenrTag=1);


$ReturnMsg = $_REQUEST['ReturnMsg'];
$ReturnMsgDisplay = $Lang['SysMgr']['Timetable']['ReturnMessage']['Import'][$ReturnMsg];
$linterface->LAYOUT_START($ReturnMsgDisplay); 

echo $libtimetable_ui->Get_Import_Lesson_Step1_UI($TimetableID);

?>

<script language="javascript">

function js_Go_Download_CSV_Sample() {
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "export_lesson.php";
	jsObjForm.submit();
}

function js_Go_Back() {
	window.location = 'index.php';
}

function js_Continue() {
	if (Trim(document.getElementById('csvfile').value) == "") {
		 alert('<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>');
		 document.getElementById('csvfile').focus();
		 return false;
	}
	
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "import_lesson_step2.php";
	jsObjForm.submit();
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
