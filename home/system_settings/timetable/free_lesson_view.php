<?php
// using 

############# Change Log [Start] ################
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

$arrCookies = array();
$arrCookies[] = array("ck_schoolSettings_timetable_from_eService", "From_eService");			
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$viewType = $_GET['viewType'];

$linterface = new interface_html();
$libtimetable_ui = new libtimetable_ui();

### Check access right
$ValidUser = false;
if ($From_eService) {
	// check if the user is a teaching staff
	if ($_SESSION['UserType'] == USERTYPE_STAFF) {
		$ValidUser = true;
	}
	
	$CurrentPageArr['eServiceTimetable'] = 1;
	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View']);
}
else {
	// check admin management access right
    if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) {
		$ValidUser = true;
	}
	
	$CurrentPageArr['Timetable'] = 1;
	
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['Management'], "index.php?clearCoo=1", 0);
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Timetable']['Tag']['View'], "view.php?clearCoo=1", 1);
	$TAGS_OBJ = $libtimetable_ui->Get_Tag_Array(2);
}

if($plugin["Disable_Timetable"])
    $ValidUser = false;

if(!$ValidUser) {
	No_Access_Right_Pop_Up();
}


$MODULE_OBJ['title'] = $Lang['SysMgr']['Timetable']['ModuleTitle'];
$linterface->LAYOUT_START(); 


$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();
$htmlAry['viewModeTab'] = $libtimetable_ui->Get_Timetable_View_Mode_Tab('FreeLesson'.$viewType);


$divName = 'filterOptionDiv';
$x = '';
$x .= '<div>'."\n";
	$x .= '<table style="width:100%;">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td class="report_show_option">'."\n";
				$x .= '<span id="spanShowOption_'.$divName.'" style="display:none">'."\n";
					$x .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName');");
				$x .= '</span>'."\n";
				$x .= '<span id="spanHideOption_'.$divName.'">'."\n";
					$x .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName');");
				$x .= '</span>'."\n";
				$x .= '<div id="'.$divName.'">'."\n";
					$x .= $libtimetable_ui->Get_Timetable_View_Option_Filtering_Table($viewType, $From_eService);
					$x .= '<div class="edit_bottom_v30">'."\n";
						$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "viewTimetable();");
					$x .= '</div>'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '<div>'."\n";
$htmlAry['optionDiv'] = $x;


$divName = 'buttonDiv';
$x = '';
$x .= '<div id="'.$divName.'" style="display:none;">'."\n";
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "printTimetable();");
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry[$divName] = $x;

?>
<?= $include_JS_CSS ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>

<form id="form1" name="form1" method="post">
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="main_content">
				<?=$htmlAry['viewModeTab']?>
				<br />
				
				<?=$htmlAry['optionDiv']?>
				<br />
				
				<div id="timetableDiv"></div>
				<br />
				
				<?=$htmlAry['buttonDiv']?>
				<br />
			</td>
		</tr>
	</table>
	<input type="hidden" id="viewType" name="viewType" value="<?=$viewType?>" />
	<input type="hidden" id="filterIdList" name="filterIdList" value="" />
	<input type="hidden" id="timeSlotIdList" name="timeSlotIdList" value="" />
	<input type="hidden" id="From_eService" name="From_eService" value="<?=$From_eService?>" />
</form>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>

<script>
var CurAcademicYearID = '';
var CurYearTermID = '';
var CurTimetableID = '';
var viewType = '<?=$viewType?>';
var From_eService = '<?=$From_eService?>';


$(document).ready(function () {
	CurAcademicYearID = $('#SelectedAcademicYearID').val();
	CurYearTermID = $('#SelectedYearTermID').val();
	CurTimetableID = $('#SelectedTimetableID').val();
	
	//$('input#selectAllBtn').click();
});

function changedAcademicYearSelection(academicYearId) {
	CurAcademicYearID = academicYearId;
	reloadTermSelection();
}

function changedTermSelection(yearTermId) {
	CurYearTermID = yearTermId;
	reloadTimetableSelection('');
}

function changedTimetableSelection(timetableId) {
	CurTimetableID = timetableId;
	reloadTimeSlotCheckboxes();
}

function reloadTermSelection() {
	if (CurAcademicYearID != '') {
		$('#TermSelectionDiv').load(
			"ajax_reload.php", 
			{ 
				RecordType: 'Reload_Term_Selection',
				AcademicYearID: CurAcademicYearID,
				NoFirst : 1,
				OnChange: 'reloadTimetableSelection(\'\');'
			},
			function(ReturnData) {
				$('#TermSelectionDiv').show();
				reloadTimetableSelection('');
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

function reloadTimetableSelection(timetableId) {
	CurAcademicYearID = $('#SelectedAcademicYearID').val();
	CurYearTermID = $('#SelectedYearTermID').val();
	CurTimetableID = timetableId;
	
	$('#TimetableSelectionDiv').hide().load(
		"ajax_reload.php", 
		{ 
			RecordType: "Reload_Timetable_Selection",
			TimetableID: CurTimetableID,
			AcademicYearID: CurAcademicYearID,
			YearTermID: CurYearTermID,
			ShowAllocatedOnly: 0,
			NoOnchange: 0,
			OnChange: 'changedTimetableSelection(this.value);',
			From_eService: From_eService
		},
		function(ReturnData) {
			$('#TimetableSelectionDiv').show();
			reloadTimeSlotCheckboxes();
		}
	);
}

function reloadTimeSlotCheckboxes() {
	if (CurTimetableID == '') {
		$('#TimeSlotCheckboxesDiv').html('<?=$Lang['General']['EmptySymbol']?>');
	}
	else {
		$('#TimeSlotCheckboxesDiv').hide().load(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_TimeSlot_Checkboxes",
				TimetableID: CurTimetableID
			},
			function(ReturnData) {
				$('#TimeSlotCheckboxesDiv').show();
			}
		);
	}
}

function viewTimetable() {
	CurTimetableID = $('select#SelectedTimetableID').val();
	
	var canSubmit = true;
	$('div.warningDiv').hide();
	if (CurTimetableID == '') {
		$('div#warningSelectTimetableDiv').show();
		$('select#SelectedTimetableID').focus();
		canSubmit = false;
	}
	
	if ($('input.timeSlotChk:checked').length == 0) {
		$('div#warningSelectTimeSlotDiv').show();
		$('input#timeSlotSelectAllChk').focus();
		canSubmit = false;
	}
	
	if (canSubmit) {
		var timeSlotIdAry = new Array();
		$('input.timeSlotChk:checked').each( function() {
			timeSlotIdAry[timeSlotIdAry.length] = $(this).val();
		});
		var timeSlotIdList = timeSlotIdAry.join(',');
		
		var filterElementList = getFilterIdList();
		
		$('div#buttonDiv').hide();
		Block_Element('filterOptionTable');
		
		$('div#timetableDiv').html('').load(
			"ajax_reload.php", 
			{ 
				RecordType: "Reload_Free_Lesson_" + viewType + "_Timetable",
				TimetableID: CurTimetableID,
				FilterElementList: filterElementList,
				TimeSlotIdList: timeSlotIdList
			},
			function(ReturnData) {
				$('div#buttonDiv').show();
				
				js_Hide_Option_Div('filterOptionDiv');
				UnBlock_Element('filterOptionTable');
			}
		);
	}
}

function getFilterIdList() {
	var filterId;
	if (viewType == 'Teacher') {
		filterId = 'teacherSel';
	}
	else if (viewType == 'Room') {
		filterId = 'roomSel';
	}
	var selectedIdAry = $('select#' + filterId).val();
	filterElementList = (selectedIdAry == null)? '' : selectedIdAry.join(',');
	
	return filterElementList;
}

function printTimetable() {
	$('input#filterIdList').val(getFilterIdList());
	
	var timeSlotIdAry = new Array();
	$('input.timeSlotChk:checked').each( function() {
		timeSlotIdAry[timeSlotIdAry.length] = $(this).val();
	});
	$('input#timeSlotIdList').val(timeSlotIdAry.join(','));
	
	$('form#form1').attr('target', '_blank').attr('action', 'free_lesson_print.php').submit();
}

/********* End of Report Generation *********/


</script>