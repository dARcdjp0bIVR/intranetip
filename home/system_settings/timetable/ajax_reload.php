<?php
// using 
/**
 * 2018-08-09 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "Reload_Timetable_Add_Edit_Layer")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	
	$returnString = $libtimetable_ui->Get_Timetable_Add_Edit_Layer($AcademicYearID, $YearTermID, $TimetableID);
}
else if ($RecordType == "Reload_TimeSlot_Add_Edit_Layer")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$LastTimeSlotID = stripslashes($_REQUEST['LastTimeSlotID']);
	$NextTimeSlotID = stripslashes($_REQUEST['NextTimeSlotID']);
	
	$returnString = $libtimetable_ui->Get_TimeSlot_Add_Edit_Layer($TimetableID, $TimeSlotID, $LastTimeSlotID, $NextTimeSlotID);
}
else if ($RecordType == "Reload_Room_Allocation_Add_Edit_Layer")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$Day = stripslashes($_REQUEST['Day']);
	$RoomAllocationID = stripslashes($_REQUEST['RoomAllocationID']);
	
	$returnString = $libtimetable_ui->Get_Room_Allocation_Add_Edit_Layer($TimeSlotID, $Day, $RoomAllocationID);
}
else if ($RecordType == "Reload_Term_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$ForCopyTimetable = stripslashes($_REQUEST['ForCopyTimetable']);
	$OnChange = stripslashes($_REQUEST['OnChange']);
	/** Can select Past Term, but view only **/
	//$NoPastTerm = stripslashes($_REQUEST['NoPastTerm']);
	
	if ($ForCopyTimetable)
		$SelectionID = 'SelectedYearTermID_ForCopyTimetable';
	else
		$SelectionID = 'SelectedYearTermID';
	
	if ($OnChange == '') {
		$term_jsOnchange = "js_Changed_Term_Selection(this.value, '$ForCopyTimetable');";
	}
	else {
		$term_jsOnchange = $OnChange;
	}
	
	$returnString = $libsubject_ui->Get_Term_Selection($SelectionID, $AcademicYearID, $YearTermID, $term_jsOnchange, $NoFirst);
}
else if ($RecordType == "Reload_Timetable_Selection")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$ShowAllocatedOnly = stripslashes($_REQUEST['ShowAllocatedOnly']);
	$ForCopyTimetable = stripslashes($_REQUEST['ForCopyTimetable']);
	$NoOnchange = stripslashes($_REQUEST['NoOnchange']);
	$OnChange = stripslashes($_REQUEST['OnChange']);
	$ExcludeTimetableID = stripslashes($_REQUEST['ExcludeTimetableID']);
	$From_eService = stripslashes($_REQUEST['From_eService']);
	
	
	$timetable_jsOnchange = '';
	if ($NoOnchange) {
		// do nth
	}
	else {
		if (!$ForCopyTimetable) {
			$timetable_jsOnchange = ($OnChange=='')? 'js_Changed_Timetable_Selection(this.value);' : $OnChange;
		}
	}
	
		
	if ($ForCopyTimetable)
		$SelectionID = 'SelectedTimetableID_ForCopyTimetable';
	else
		$SelectionID = 'SelectedTimetableID';
		
	if ($From_eService) {
		$ReleaseStatus = 1;
	}
	else {
		$ReleaseStatus = '';
	}
	
	$returnString = $libtimetable_ui->Get_Timetable_Selection($SelectionID, $AcademicYearID, $YearTermID, $TimetableID, $timetable_jsOnchange, $NoFirst, '', $ShowAllocatedOnly, $ExcludeTimetableID, $IncludeTimetableIDArr='', $ReleaseStatus);
}
else if ($RecordType == "Reload_Timetable")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		
	$libtimetable_ui = new libtimetable_ui();
	$scm = new subject_class_mapping();
	
	## Get Selection Info
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$IsViewMode = stripslashes($_REQUEST['IsViewMode']);
	$ViewMode = stripslashes($_REQUEST['ViewMode']);
	$isPrintMode = stripslashes($_REQUEST['IsPrint']);
	$NoTitle = stripslashes($_REQUEST['NoTitle']);
	$PrintDataInOneTimetable = ($_REQUEST['PrintDataInOneTimetable']==1)? true : false;
	
	
	# Class
	$YearClassID = stripslashes($_REQUEST['YearClassID']);
	$YearClassIDArr = explode(",", $YearClassID);
	# Subject Group
	$SubjectGroupID = stripslashes($_REQUEST['SubjectGroupID']);
	$SubjectGroupIDArr = explode(",", $SubjectGroupID);
	# Personal
	$Identity = stripslashes($_REQUEST['Identity']);
	$SelectedUserID = stripslashes($_REQUEST['SelectedUserID']);
	$UserIDArr = explode(",", $SelectedUserID);
	# Room
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	$OthersLocation = trim(stripslashes(urldecode($_REQUEST['OthersLocation'])));
	$LocationID = stripslashes($_REQUEST['LocationID']);
	$LocationIDArr = explode(",",$LocationID);
		
	
	## Get Timetable Term
	$objTimetable = new Timetable($TimetableID);
	$YearTermID = $objTimetable->YearTermID;
	
	## Build Filtering Array Here
	$SubjectGroupID_DisplayFilterArr = array();
	$LocationID_DisplayFilterArr = array();
	
	if ($IsViewMode && !$PrintDataInOneTimetable)
	{
		if ($ViewMode == "Class")
		{
			# Get all subject groups of the class and assign to $SubjectGroupID_DisplayFilterArr
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			
			$numOfYearClassID = count($YearClassIDArr);
			for($i=0; $i<$numOfYearClassID; $i++)
			{
				$thisYearClassID = $YearClassIDArr[$i];
				$yc = new year_class($thisYearClassID);
				$TitleDisplay = Get_Lang_Selection($yc->ClassTitleB5, $yc->ClassTitleEN);
				$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID,$ViewMode,$thisYearClassID);
				
				// no subject group in the class
				if(sizeof($this_SubjectGroupID_DisplayFilterArr) <=0)
					$this_SubjectGroupID_DisplayFilterArr = array(0);
					
				if ($NoTitle != 1)
					$returnString .= $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'].": ".$TitleDisplay."<br />\n";
				$returnString .= $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, $this_SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr);
				$returnString .= ($i==$numOfYearClassID-1)?"":"<p style='page-break-before: always;'></p>\n";
			}
		}
		else if ($ViewMode == "SubjectGroup")
		{
			# Loop through all Selected Subject Groups to show the individual subject group timetable
			$numOfSubjectGroupID = count($SubjectGroupIDArr);
			
			for($i=0; $i<$numOfSubjectGroupID; $i++)
			{
				$thisSubjectGroupID = $SubjectGroupIDArr[$i];
				unset($SubjectGroupID_DisplayFilterArr);
				$SubjectGroupID_DisplayFilterArr = array($thisSubjectGroupID);
				if(sizeof($SubjectGroupID_DisplayFilterArr) <= 0)
					$SubjectGroupID_DisplayFilterArr = array(0);
				$ObjSubjectGroup = new subject_term_class($thisSubjectGroupID);
				$TitleDisplay = Get_Lang_Selection($ObjSubjectGroup->ClassTitleB5, $ObjSubjectGroup->ClassTitleEN);
				
				if ($NoTitle != 1)
					$returnString .= $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'].": ".$TitleDisplay."<br />\n";
				$returnString .= $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr);
				$returnString .= ($i==$numOfSubjectGroupID-1)?"":"<p style='page-break-before: always'>\n";
			}
		}
		else if ($ViewMode == "Personal")
		{
			# Loop through all Selected Person to show the personal timetable
			# Get all subject groups which the person has joined and assign to $SubjectGroupID_DisplayFilterArr
			$numOfUserID = count($UserIDArr);
			
			// added by kenneth chung on 20090714, to deal with archive user
			$NameField = getNameFieldByLang('u.');
			$ArchiveNameField = getNameFieldByLang2('au.');
			$sql = 'select
							  *
							from
							  (
							    (select
							      u.UserID,
							      '.$NameField.' as StudentName,
							      \'0\' as DeletedUser 
							    from
							      INTRANET_USER as u
							    where
							      u.UserID in ('.$SelectedUserID.')
							    )
							    UNION
							    (select
							      au.UserID,
							      '.$ArchiveNameField.' as StudentName,
							      \'1\' as DeletedUser
							    from
							      INTRANET_ARCHIVE_USER as au
							    where
							      au.UserID in ('.$SelectedUserID.')
							    )
							  ) as a';
			$Temp = $scm->returnArray($sql);
			for ($i=0; $i< sizeof($Temp); $i++) {
				$StudentNames[$Temp[$i]['UserID']] = $Temp[$i];
			}
			
			for($i=0; $i<$numOfUserID; $i++)
			{
				$thisUserID = $UserIDArr[$i];
				//$liu = new libuser($thisUserID);
				// dim by kenneth chung on 20090714, deal with archive user
				//$TitleDisplay = Get_Lang_Selection($liu->ChineseName, $liu->EnglishName);
				$this_SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID,$ViewMode,'',$thisUserID,'',$Identity);
				if(sizeof($this_SubjectGroupID_DisplayFilterArr) <= 0)
					$this_SubjectGroupID_DisplayFilterArr = array(0);
				
				if ($NoTitle != 1)
					$returnString .= $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'].": ".$StudentNames[$thisUserID]['StudentName']."<br />\n";
					$returnString .= $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, $this_SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocationFilter='', $Day='', $TimeSlotID='', $isSmallFont=0, $MapTimeZoneDay=0, $IndicateCurDay=0, $targetUserID = $thisUserID);
				$returnString .= ($i==$numOfUserID)?"":"<p style='page-break-before: always'>\n";
			}
		}
		else if ($ViewMode == "Room")
		{
			# Loop through all Selected Locations to show the individual location timetable
			# Assign the selected locations into the array $LocationID_DisplayFilterArr
			if ($BuildingID != -1)
			{
				# Location in School
				include_once($PATH_WRT_ROOT."includes/liblocation.php");
				
				$numOfLocationID = count($LocationIDArr);
				for($i=0; $i < $numOfLocationID; $i++)
				{
					$thisLocationID = $LocationIDArr[$i];
					unset($LocationID_DisplayFilterArr);
					$LocationID_DisplayFilterArr = array($thisLocationID);
					$lr = new Room($thisLocationID);
					$TitleDisplay = Get_Lang_Selection($lr->NameChi, $lr->NameEng);
					
					/*
					$SubjectGroupID_DisplayFilterArr = $scm->Get_SubjectGroupID_Taken($YearTermID,$ViewMode,'','',$thisLocationID);
					if(sizeof($SubjectGroupID_DisplayFilterArr) <= 0)
						$SubjectGroupID_DisplayFilterArr = array(0);
					*/
						
					if ($NoTitle != 1)
						$returnString .= $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'].": ".$TitleDisplay."\n";
					$returnString .= $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, array(), $LocationID_DisplayFilterArr);
					$returnString .= ($i==$numOfLocationID-1)?"":"<p style='page-break-before: always'>\n";
				}
			}
			else
			{
				# Others Location
				if ($OthersLocation=='')
				{
					# All Others Location
					$OthersLocationArr = $objTimetable->Get_All_Others_Location();
					$numOfOthersLocation = count($OthersLocationArr);
				}
				else
				{
					# Specific Others Location
					$OthersLocationArr = array($OthersLocation);
					$numOfOthersLocation = 1;
				}
				
				for($i=0; $i < $numOfOthersLocation; $i++)
				{
					$thisOthersLocation = $OthersLocationArr[$i];
					$LocationID_DisplayFilterArr = array(0);
					
					if ($NoTitle != 1)
						$returnString .= $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'].": ".$thisOthersLocation."<br />\n";
					$returnString .= $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $thisOthersLocation);
					$returnString .= ($i==$numOfOthersLocation-1)?"":"<p style='page-break-before: always'>\n";
				}
			}
		}
	}
	else
	{
		$FilterArr = $objTimetable->Get_Filter_Array_By_Filter_Settings($ViewMode, $YearClassIDArr, $SubjectGroupIDArr, $Identity, $UserIDArr, $BuildingID, $LocationIDArr, $OthersLocation);
		$SubjectGroupID_DisplayFilterArr = $FilterArr['SubjectGroupIDArr'];
		$LocationID_DisplayFilterArr = $FilterArr['LocationIDArr'];
	}
	
	if ($IsViewMode && !$PrintDataInOneTimetable)
	{
		$returnString .= ($isPrintMode)?'':'<p class="dotline"></p><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center"><input id="PrintBtn" class="formbutton" type="button" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" value="Print" name="PrintBtn" onclick="print_timetable();"/></td></tr></table>';
	}
	else
	{
		$returnString = $libtimetable_ui->Get_Timetable_Table($TimetableID, $IsViewMode, $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr, $OthersLocation, '', '', 0);
		
		if ($IsViewMode && $PrintDataInOneTimetable) {
			$returnString .= ($isPrintMode)?'':'<p class="dotline"></p><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center"><input id="PrintBtn" class="formbutton" type="button" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" value="Print" name="PrintBtn" onclick="print_timetable();"/></td></tr></table>';
		}
	}
}
else if ($RecordType == "Reload_Room_Allocation_Detail_Table")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$TimeSlotID = stripslashes($_REQUEST['TimeSlotID']);
	$Day = stripslashes($_REQUEST['Day']);
	$IsViewMode = stripslashes($_REQUEST['IsViewMode']);
	$ViewMode = stripslashes($_REQUEST['ViewMode']);
	
	### Get Filter Values
	$YearClassID = stripslashes($_REQUEST['YearClassID']);
	$YearClassIDArr = explode(",", $YearClassID);
	
	$SubjectGroupID = stripslashes($_REQUEST['SubjectGroupID']);
	$SubjectGroupIDArr = explode(",", $SubjectGroupID);
	
	$Identity = stripslashes($_REQUEST['Identity']);
	$SelectedUserID = stripslashes($_REQUEST['SelectedUserID']);
	$UserIDArr = explode(",", $SelectedUserID);
	
	$BuildingID = stripslashes($_REQUEST['BuildingID']);
	$OthersLocation = trim(stripslashes(urldecode($_REQUEST['OthersLocation'])));
	$LocationID = stripslashes($_REQUEST['LocationID']);
	$LocationIDArr = explode(",", $LocationID);
	
	### Get TimetableID from the TimeSlot info
	$objTimeSlot = new TimeSlot($TimeSlotID);
	$TimetableID = $objTimeSlot->TimetableID;
	$objTimetable = new Timetable($TimetableID);
		
	$FilterArr = $objTimetable->Get_Filter_Array_By_Filter_Settings($ViewMode, $YearClassIDArr, $SubjectGroupIDArr, $Identity, $UserIDArr, $BuildingID, $LocationIDArr, $OthersLocation);
	$SubjectGroupID_DisplayFilterArr = $FilterArr['SubjectGroupIDArr'];
	$LocationID_DisplayFilterArr = $FilterArr['LocationIDArr'];
	
	$returnString = $libtimetable_ui->Get_Room_Allocation_Detail_Table($IsViewMode, $TimeSlotID, $Day, '', '', '', $SubjectGroupID_DisplayFilterArr, $LocationID_DisplayFilterArr);
}
else if ($RecordType == "Reload_Timetable_Edit_Link")
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	
	$returnString = $libinterface->Get_Thickbox_Link($libtimetable_ui->thickBoxHeight, $libtimetable_ui->thickBoxWidth, "edit", 
										$Lang['SysMgr']['Timetable']['Edit']['Timetable'], "js_Show_Timetable_Add_Edit_Layer('$TimetableID'); return false;", 'FakeLayer','');
}
else if ($RecordType == "Reload_Timetable_Delete_Link")
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	
	$returnString = $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Timetable']['Delete']['Timetable'], "js_Delete_Timetable('$TimetableID'); return false;", "delete");
}
else if ($RecordType == "Reload_Subject_Group_Selection_InAddEditLayer")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_class_mapping_ui = new subject_class_mapping_ui();
	
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	
	$returnString = $libsubject_class_mapping_ui->Get_Subject_Group_Selection($SubjectID, 
																		"SelectedSubjectGroupID_InAddEditLayer", 
																		"",
																		"js_Validate_Subject_Group_Overlap('$TimeSlotID', '$Day', '$RoomAllocationID')",
																		$YearTermID
																		);
}
else if ($RecordType == "Get_TimeSlot_CycleDay_Info")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
	
	$TimetableID = stripslashes($_REQUEST['TimetableID']);
	$ObjTimetable = new Timetable($TimetableID);
	
	$CycleDays = $ObjTimetable->CycleDays;
	$TimeSlotInfoArr = $ObjTimetable->Get_TimeSlot_List();
	$numOfTimeSlot = count($TimeSlotInfoArr);
	
	$TimeSlotIDList = '';
	if ($numOfTimeSlot > 0)
	{
		$TimeSlotIDArr = array();
		for ($i=0; $i<$numOfTimeSlot; $i++)
			$TimeSlotIDArr[] = $TimeSlotInfoArr[$i]['TimeSlotID'];
		$TimeSlotIDList = implode(',', $TimeSlotIDArr);
	}
	
	$returnString = $CycleDays.'|||---separator---|||'.$TimeSlotIDList;
}


############ View Mode ###############
else if ($RecordType == "Reload_Secondary_Filter_Table")
{
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$ViewMode = stripslashes($_REQUEST['ViewMode']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	$InLayer = stripslashes($_REQUEST['InLayer']);
	
	$returnString = $libtimetable_ui->Get_View_Secondary_Settings_Table($ViewMode, $InLayer, $YearTermID);
}
else if ($RecordType == "Reload_Class_Selection")
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	$form_class_manage_ui = new form_class_manage_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = cleanHtmlJavascript(stripslashes($_REQUEST['ID']));
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearID = stripslashes($_REQUEST['YearID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
	$OnChange = stripslashes($_REQUEST['OnChange']);
	
	$returnString = $form_class_manage_ui->Get_Class_Selection($AcademicYearID, $YearID, $ID, $SelectedYearClassID='', $OnChange, $NoFirst, $IsMultiple);
	
	if ($IsMultiple)
		$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}
else if ($RecordType == "Reload_Subject_Group_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libsubject_ui = new subject_class_mapping_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = cleanHtmlJavascript(stripslashes($_REQUEST['ID']));
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	$YearTermID = IntegerSafe(stripslashes($_REQUEST['YearTermID']));
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);

	$returnString = $libsubject_ui->Get_Subject_Group_Selection($SubjectID, $ID, '', '', $YearTermID, $IsMultiple);
	$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}
else if ($RecordType == "Reload_Target_Selection")
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	$form_class_manage_ui = new form_class_manage_ui();
	
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$libinterface = new interface_html();
	
	$ID = cleanHtmlJavascript(stripslashes($_REQUEST['ID']));
	$TargetType = stripslashes($_REQUEST['TargetType']);
	$YearClassID = stripslashes($_REQUEST['YearClassID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);

	if ($TargetType == "Student")
	{
		$returnString = $form_class_manage_ui->Get_Student_Selection($ID, $YearClassID, '', '', $NoFirst, $IsMultiple);
	}
	else
	{
		$isTeachingStaff = ($TargetType == "TeachingStaff")? 1 : 0;
		$returnString = $form_class_manage_ui->Get_Staff_Selection($ID, $isTeachingStaff, '', '', $NoFirst, $IsMultiple);
	}
	
	$returnString .= $libinterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('$ID');", "SelectAllBtn");
}
else if ($RecordType == 'Reload_Free_Lesson_Teacher_Timetable') {
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$timetableId = $_POST['TimetableID'];
	$timeSlotIdList = $_POST['TimeSlotIdList'];
	$timeSlotIdAry = explode(',', $timeSlotIdList);
	$teacherList = $_POST['FilterElementList'];
	$teacherIdAry = explode(',', $teacherList);
	
	$returnString = $libtimetable_ui->Get_Free_Lesson_Teacher_Timetable_Table($timetableId, $teacherIdAry, $timeSlotIdAry);
}
else if ($RecordType == 'Reload_Free_Lesson_Room_Timetable') {
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$timetableId = $_POST['TimetableID'];
	$timeSlotIdList = $_POST['TimeSlotIdList'];
	$timeSlotIdAry = explode(',', $timeSlotIdList);
	$roomList = $_POST['FilterElementList'];
	$roomIdAry = explode(',', $roomList);
	
	$returnString = $libtimetable_ui->Get_Free_Room_Timetable_Table($timetableId, $roomIdAry, $timeSlotIdAry);
}
else if ($RecordType == 'Reload_TimeSlot_Checkboxes') {
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
	$libtimetable_ui = new libtimetable_ui();
	
	$timetableId = $_POST['TimetableID'];
	$returnString = $libtimetable_ui->Get_TimeSlot_Checkboxes($timetableId);
}


echo $returnString;

intranet_closedb();

?>