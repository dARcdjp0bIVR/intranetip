<?
//Using: 
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 *  2020-05-07 [Tommy] modified access checking, added $plugin["Disable_Timetable"]
 *
 * 	2019-05-28 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) || $plugin["Disable_Timetable"]) {
    No_Access_Right_Pop_Up();
}

$libtimetable_ui = new libtimetable_ui();
$linterface = new interface_html();

$characterset = 'utf-8';
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash


switch($action) {
    
    case 'getEverydayTimeTableCalendar':
        $academicYearID = IntegerSafe($_POST['SelectedAcademicYearID']);
        $timetableID = IntegerSafe($_POST['SelectedTimetableID']);
        $mode = $_POST['mode'];
        $x = $libtimetable_ui->getEverydayTimeTableCalendar($academicYearID, $timetableID, $mode);
//        $x = $libtimetable_ui->displayCalandarEverydayPreview(44, 6, 2019, 0, '', 'edit');

        if ($mode = 'edit') {
            ob_start();
?>            
<script>
$(document).ready(function(){
	var isLoading;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
    $('.timetableLink').click(function(e){
        e.preventDefault();
        if ($('#SelectedAcademicYearID').val() != '' && $('#SelectedTimetableID').val() != '') {
            $thisDate = $(this).attr('data-date');
            var $link = $(this);
            isLoading = true;
            $link.find('td.timetableName').html(loadingImg);
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: 'ajax.php',
                data : {
                    'action': 'updateEverydayTimetable',
                    'date': $thisDate,
                    'SelectedAcademicYearID': $('#SelectedAcademicYearID').val(),
                    'SelectedTimetableID': $('#SelectedTimetableID').val()
            	},		  
            	success: function(ajaxReturn){
                    if (ajaxReturn != null && ajaxReturn.success){
                        $link.find('td.timetableName').html(ajaxReturn.html);
                        isLoading = false;
                    }
            	},
            	error: show_ajax_error
        	});
        }
    });

    $('.timetableLink').hover(function(e){
        e.preventDefault();
    	if ($('#SelectedTimetableID').val() == '') {
			$(this).css('cursor', 'default');
    	}
    	else {
    		$(this).css('cursor', 'pointer');
    	}
    });
});
</script>
<?php           
            $x .= ob_get_contents();
            ob_end_clean();
        }
        $json['success'] = true;
        break;

    case 'updateEverydayTimetable':
        include_once($intranet_root."/includes/libebooking.php");
        $result = array();
        $date = $_POST['date'];
        $academicYearID = IntegerSafe($_POST['SelectedAcademicYearID']);
        $timetableID = IntegerSafe($_POST['SelectedTimetableID']);
        if (preg_match('/^(\d\d\d\d)-(\d{2})-(\d{2})$/',$date)) {
            
            $libcycleperiods = new libcycleperiods();
            $defaultTimetableAry = $libcycleperiods->getDefaultTimetableInfo($date, $date);
            if (count($defaultTimetableAry)) {
                $oldTimetableID = $defaultTimetableAry[$date]['TimetableID'] ? $defaultTimetableAry[$date]['TimetableID'] : $defaultTimetableAry[$date]['DefaultTimetableID'];
                
                $sql = "UPDATE INTRANET_SCHOOL_EVERYDAY_TIMETABLE SET TimetableID='".$timetableID."', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE RecordDate='".$date."'";
                $result['updateTimetable'] = $libtimetable_ui->db_db_query($sql);
                $result['updateBookingTimeslot_'.$date] = $libtimetable_ui->updateBookingTimeslotByTimetable($date, $oldTimetableID, $timetableID);
            }
            else {
                $sql = "INSERT IGNORE INTO INTRANET_SCHOOL_EVERYDAY_TIMETABLE (RecordDate, TimetableID, DateInput, InputBy, DateModified, ModifiedBy) VALUES ";
                $sql .= "('$date','$timetableID',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')";
                $result['insertTimetable'] = $libtimetable_ui->db_db_query($sql);
            }
            
            if (!in_array(false,$result)) {
                $timetableAry = $libtimetable_ui->Get_All_TimeTable($academicYearID);
                $timetableAssoc = BuildMultiKeyAssoc($timetableAry,array('TimetableID'),array('TimetableName'),1);
                $x = $timetableAssoc[$timetableID];
                $json['success'] = true;
            }
        }
        break;
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}


$json['html'] = $x;
echo $ljson->encode($json);


intranet_closedb();
?>