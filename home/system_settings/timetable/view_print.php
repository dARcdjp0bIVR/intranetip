<?php
// using Kit
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$CurrentPageArr['Timetable'] = 1;

$targetFontSize = ($_GET['fs'])? $_GET['fs'] : 10;


include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
//$linterface->LAYOUT_START(); 

$libtimetable_ui = new libtimetable_ui();
$include_JS_CSS = $libtimetable_ui->Include_JS_CSS();
$isPrint = 1;

$TimetableDiv = '';
$TimetableDiv .= '<div id="TimetableDiv" style="width:740px;">'."\n";
$TimetableDiv .= '</div>';

?>

<?= $include_JS_CSS ?>
<table border="0" cellpadding="0" width="99%" cellspacing="0" class="print_hide">
	<tr>
		<td align="right">
			<input type="button" class="formsubbutton" onclick="javascript:changeSize(1);" name="IncreaseFontBtn" id="IncreaseFontBtn" value="<?=$Lang['SysMgr']['Timetable']['Button']['IncreaseFontSize']?>" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<input type="button" class="formsubbutton" onclick="javascript:changeSize(-1);" name="DecreaseFontBtn" id="DecreaseFontBtn" value="<?=$Lang['SysMgr']['Timetable']['Button']['DecreaseFontSize']?>" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<input type="button" class="formsubbutton" onclick="javascript:window.print();" name="PrintBtn" id="PrintBtn" value="Print" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<br style="clear:both;" />
			<div style="float:right;">&nbsp;</div>
			<div id="resizeFontLoadingDiv" style="display:none; float:right;"><?=$linterface->Get_Ajax_Loading_Image($noLang=0, '<span style="color:red;">'.$Lang['General']['Procesesing'].'</span>')?></div>
			<br style="clear:both;" />
		</td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content" valign="top">
			<?= $TimetableDiv ?>
		</td>
	</tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
//$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<style type="text/css">
table {
	font-size: <?=$targetFontSize?>px;
}
</style>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>
<script>
var currentFontSize = <?=$targetFontSize?>;
var minFontSize = 1;
var maxFontSize = 20;

$(document).ready(function () {
	js_Reload_Timetable();
	updateButtonStatus();
});

//window.print();
function js_Reload_Timetable()
{
	var PrintDataInOneTimetable = $('#PrintDataInOneTimetable_1', opener.document ).attr('checked')? 1 : 0;
	var SelectedAcademicYearID = $('select#SelectedAcademicYearID', opener.document ).val();
	var SelectedYearTermID = $('select#SelectedYearTermID', opener.document ).val();
	var SelectedTimetableID = $('select#SelectedTimetableID', opener.document ).val();
	var SelectedViewMode = $('select#SelectedViewMode', opener.document ).val();
	var SelectedIdentity = $('select#SelectedIdentity', opener.document ).val();
	
	var SelectedYearClassID;
	var SelectedSubjectGroupID;
	var SelectedUserID;
	var SelectedBuildingID;
	var SelectedLocationID;
	var OthersLocation;
	
	switch(SelectedViewMode)
	{
		case "Class":
			// Class Mode
			SelectedYearClassID = $('select#SelectedYearClassID', opener.document ).val().join(',');
			break;
		case "SubjectGroup":
			// Subject Group Mode
			SelectedSubjectGroupID = $('select#SelectedSubjectGroupID', opener.document ).val().join(',');
			break;
		case "Personal":
			// Personal Mode
			SelectedUserID = $('select#SelectedUserID', opener.document ).val().join(',');
			break;
		case "Room":
			// Room Mode
			SelectedBuildingID = $('select#SelectedBuildingID', opener.document ).val();
			OthersLocation = encodeURIComponent(Trim($("input#OthersLocationTb", opener.document ).val()));
			if ($('select#SelectedLocationIDArr\\[\\]', opener.document ).val() != null)
				SelectedLocationID = $('select#SelectedLocationIDArr\\[\\]', opener.document ).val().join(',');
			break;
	}
	
	SelectedYearClassID = js_Check_Object_Defined(SelectedYearClassID);
	SelectedSubjectGroupID = js_Check_Object_Defined(SelectedSubjectGroupID);
	SelectedUserID = js_Check_Object_Defined(SelectedUserID);
	SelectedBuildingID = js_Check_Object_Defined(SelectedBuildingID);
	SelectedLocationID = js_Check_Object_Defined(SelectedLocationID);
	OthersLocation = js_Check_Object_Defined(OthersLocation);
	SelectedIdentity = js_Check_Object_Defined(SelectedIdentity);
	

	$('#TimetableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Reload_Timetable',
			TimetableID: SelectedTimetableID,
			ViewMode: SelectedViewMode,
			YearClassID: SelectedYearClassID,
			YearTermID: SelectedYearTermID,
			SubjectGroupID: SelectedSubjectGroupID,
			SelectedUserID: SelectedUserID,
			BuildingID: SelectedBuildingID,
			LocationID: SelectedLocationID,
			OthersLocation: OthersLocation,
			Identity: SelectedIdentity,
			IsViewMode: 1,
			IsPrint: 1,
			PrintDataInOneTimetable: PrintDataInOneTimetable
		},
		function(ReturnData)
		{
			//$('#TimetableDiv').show();
			//$('#IndexDebugArea').html(ReturnData);
			
			js_Update_Timetable_Display(1);
		}
	);
}


function changeSize(changes) {
	$('div#resizeFontLoadingDiv').show();
	
	setTimeout("processChangeSize(" + changes + ")", 500);
}

function processChangeSize(changes) {
	var targetFontSize = currentFontSize + changes;
//	if (targetFontSize >= minFontSize && targetFontSize <= maxFontSize) {
//		$("div#TimetableDiv").find("*").each(function(){
//			$(this).css("font-size", targetFontSize + "px"); 
//		});
//		
//		currentFontSize = targetFontSize;
//	}
//	
//	updateButtonStatus();
//	$('div#resizeFontLoadingDiv').hide();

	reloadTimetable(targetFontSize);
}

function updateButtonStatus() {
	if (currentFontSize == maxFontSize) {
		$('input#IncreaseFontBtn').attr('disabled', true);
	}
	else if (currentFontSize == minFontSize) {
		$('input#DecreaseFontBtn').attr('disabled', true);
	}
	else {
		$('input#IncreaseFontBtn').attr('disabled', false);
		$('input#DecreaseFontBtn').attr('disabled', false);
	}
}

function reloadTimetable(targetFontSize) {
	window.location = "view_print.php?fs=" + targetFontSize;
}
</script>