<?php
// using 

############# Change Log [Start] ################
# Date: 2020-05-07 Tommy
#       - modified access checking, added $plugin["Disable_Timetable"]
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();

$From_eService = $_REQUEST['From_eService'];
$viewType = $_POST['viewType'];
$TimetableID = $_POST['SelectedTimetableID'];
$targetFontSize = ($_POST['fs'])? $_POST['fs'] : 10;
$filterIdList = $_POST['filterIdList'];
$timeSlotIdList = $_POST['timeSlotIdList'];
$TimetableID = $_POST['SelectedTimetableID'];


$linterface = new interface_html();
$libtimetable_ui = new libtimetable_ui();

### Check access right
$ValidUser = false;
if ($From_eService) {
	// check if the user is a teaching staff
	if ($_SESSION['UserType'] == USERTYPE_STAFF) {
		$ValidUser = true;
	}
}
else {
	// check admin management access right
    if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) {
		$ValidUser = true;
	}
}

if($plugin["Disable_Timetable"])
    $ValidUser = false;

if(!$ValidUser) {
	No_Access_Right_Pop_Up();
}

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");


?>
<style type="text/css">
table {
	font-size: <?=$targetFontSize?>px;
}
</style>
<script type="text/javascript">
var viewType = '<?=$viewType?>';
var currentFontSize = <?=$targetFontSize?>;
var minFontSize = 1;
var maxFontSize = 20;

$(document).ready( function () {
	viewTimetable();
	updateButtonStatus();
});

function viewTimetable() {
	$('div#timetableDiv').html('').load(
		"ajax_reload.php", 
		{ 
			RecordType: "Reload_Free_Lesson_" + viewType + "_Timetable",
			TimetableID: $('input#SelectedTimetableID').val(),
			FilterElementList: $('input#filterIdList').val(),
			TimeSlotIdList: $('input#timeSlotIdList').val()
		},
		function(ReturnData) {
			
		}
	);
}

function changeSize(changes) {
	var targetFontSize = parseInt(currentFontSize) + parseInt(changes);
	$('input#fs').val(targetFontSize);
	
	$('form#form1').attr('action', 'free_lesson_print.php').submit();
}

function updateButtonStatus() {
	if (currentFontSize == maxFontSize) {
		$('input#IncreaseFontBtn').attr('disabled', true);
	}
	else if (currentFontSize == minFontSize) {
		$('input#DecreaseFontBtn').attr('disabled', true);
	}
	else {
		$('input#IncreaseFontBtn').attr('disabled', false);
		$('input#DecreaseFontBtn').attr('disabled', false);
	}
}
</script>
<table border="0" cellpadding="0" width="99%" cellspacing="0" class="print_hide">
	<tr>
		<td align="right">
			<input type="button" class="formsubbutton" onclick="javascript:changeSize(1);" name="IncreaseFontBtn" id="IncreaseFontBtn" value="<?=$Lang['SysMgr']['Timetable']['Button']['IncreaseFontSize']?>" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<input type="button" class="formsubbutton" onclick="javascript:changeSize(-1);" name="DecreaseFontBtn" id="DecreaseFontBtn" value="<?=$Lang['SysMgr']['Timetable']['Button']['DecreaseFontSize']?>" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<input type="button" class="formsubbutton" onclick="javascript:window.print();" name="PrintBtn" id="PrintBtn" value="Print" onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/>
			<br style="clear:both;" />
			<div style="float:right;">&nbsp;</div>
			<div id="resizeFontLoadingDiv" style="display:none; float:right;"><?=$linterface->Get_Ajax_Loading_Image($noLang=0, '<span style="color:red;">'.$Lang['General']['Procesesing'].'</span>')?></div>
			<br style="clear:both;" />
		</td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr> 
		<td class="main_content" valign="top">
			<div id="timetableDiv"></div>
		</td>
	</tr>
</table>
<form id="form1" name="form1" method="post">
	<input type="hidden" id="viewType" name="viewType" value="<?=$viewType?>" />
	<input type="hidden" id="SelectedTimetableID" name="SelectedTimetableID" value="<?=$TimetableID?>" />
	<input type="hidden" id="filterIdList" name="filterIdList" value="<?=$filterIdList?>" />
	<input type="hidden" id="timeSlotIdList" name="timeSlotIdList" value="<?=$timeSlotIdList?>" />
	<input type="hidden" id="fs" name="fs" value="" />
</form>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>