<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

# IES Module License access right checking


$ModuleCode = $_GET["ModuleCode"];


if (empty($ModuleCode)) {

	//if $ModuleCode  is empty , get the Encrypted Code
	$ModuleCode_e = $_GET["ModuleCode_e"];
	if(trim($ModuleCode_e) == ''){
		echo "The Module Code is not valid!";die;
	}else{
		$ModuleCode = getDecryptedText($ModuleCode_e);
	}

}
if ($ModuleCode == "hidden") {
	$ModuleCode = "";
}
libIntranetModule::checkModuleLicenceAccess($ModuleCode);
intranet_auth();
intranet_opendb();
#################################################################


// additional javascript for module only
include_once("module_script.php");
$libIntranetModule = new libIntranetModule();
$linterface 	= new interface_html('popup5.html');
$objTB 		= new libdbtable();
$li 		= new libdbtable2007($field, $order, $pageNo);


## Check user is "ADMIN"
//if($libIntranetModule->IS_ADMIN_USER($_SESSION["UserID"])){
	
	$libIntranetModule->create_tmp_module_license_count($_SESSION['UserID']);
	
	$li->sql = $libIntranetModule->get_module_license_list($_SESSION['UserID'],$ModuleCode);

	$li->page_size = $numPerPage 				= (isset($numPerPage) && $numPerPage != "")? $numPerPage : 20;	
	$pageSizeChangeEnabled 	= true;
	$order 					= ($order == '' || $order != 0) ? 1 : 0;
	if($field=="") $field 	= 1;
	
	
//	$li->field_array = array("im.ModuleID", "Description", "DateInput","assigned_quota", "quota");
	$li->field_array = array("Description", "assigned_quota", "quota");
		
	$li->IsColOff = "module_manage_license";
	$li->no_col = 4; // specify the number of column to set colspan
//	$li->column_array = array(12,12,12,12,12,0,0,0);
	
	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['Product'])."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['UsedQuota'])."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['RemainingQuota'])."</td>\n";
	
//}

$linterface->LAYOUT_START();
?>

<script language="JavaScript" type="text/JavaScript">

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
</script>



<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv='content-type' content='text/html; charset=UTF-8' />

<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif"  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_prev_month_on.gif','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_next_month_on.gif')">

<?php
//if(!$libIntranetModule->IS_ADMIN_USER($_SESSION["UserID"])){
?>
<!--
	<div style="padding: 30px 0 0 50px;height:100%;">
	<BR /><BR /><BR /><BR />
	<?=$eLib['SystemMsg']['AccessDenied']?>
	<BR /><BR /><BR /><BR />
	<input type="button" name="close_btn" id="close_btn" value="<?=$eLib["html"]["Close"]?>" />
	</div>
-->
<?php
//die();
//}?>
<script>	
</script>
<form name="form1" id="form" method="POST" style="margin:0px;padding:0px;">
<div style='background:#FFF;width:100%;height:100%;'>
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="50" valign="top"> 
				<table width="101%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
	                <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" width="10" height="41"></td>
	                <td width="300" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" class="title"><?=$Lang['ModuleLicense']['LicenseManagement']?></td>
	                <td width="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_02.gif" ></td>
	                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_03.gif">&nbsp;</td>
	                <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_04.gif" width="22" height="41"></td>
              	</tr>
            	</table>
            </td>
        </tr>
        <tr> 
          <td align="center">
			<BR /><BR />
			<div style="width:95%">
			<?=$li->display();?>
			</div>
		</td>
		</tr>
		<!-- Henry Y 2010-12-20: show number of deleted users -->		
		<?	
		# Henry 2010-12-20
		$ModuleID = $libIntranetModule->getModuleID($ModuleCode);
		$nDeleted = $libIntranetModule->get_deleted_users_count($ModuleID);
		if($nDeleted > 0){
			echo '<tr><td>';
			echo '<div style="margin-left:25px; margin-top:10px">';
				echo str_replace('{NUMBER}', $nDeleted, $Lang['ModuleLicense']['RemarkDeletedUser']);
			echo '</div>';
			echo '</td></tr>';
		}	
		?>							
		</table>
	</td>
</tr>
<tr> 
    <td height="20" bgcolor="#999999"> 
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="right"><span class="footertext">Powered by</span> <a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/logo_eclass_footer.gif" border="0" align="absmiddle"></a></td>
          <td width="20"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="20" height="20"></td>
        </tr>
    	</table>
   </td>
  </tr>
</table>

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/board_scheme_03.gif" width="4" height="4">

</div>
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />

</form>
<?php
$linterface->LAYOUT_STOP();

## Drop the tmp table
$libIntranetModule->drop_tmp_module_license_count($_SESSION['UserID']);
	
intranet_closedb();
?>