﻿<?php
//Using by:		Max
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
   	
function new3Get_Catalogue_Menu($Lang="chi") {
global $image_path, $LAYOUT_SKIN, $eLib, $catListArr;
$getChineseModuleList = getChineseModuleList(); 
$getEnglishModuleList = getEnglishModuleList();

//if($Lang == "chi"){
	$display = ($Lang == "eng")?"style='display:block;visibility:visible;position:relative;'":"style='display:none';visibility:visible;position:relative;";
	$x .= "<div id=\"elib_cata_wrap_01\" ".$display." >
		<div id=\"elib_cata_top1\">
			<div id=\"elib_lang01\">".$eLib['html']['english']."</div>
			<div id=\"elib_lang02\"><a href=\"javascript:void(0);\" onClick=\"document.getElementById('elib_cata_wrap_02').style.display='block';document.getElementById('elib_cata_wrap_01').style.display='none'\"; document.getElementById('elib_cata_wrap_02').style.visibility=\"visible\" >".$eLib['html']['chinese']."</a></div>
		</div>									
		<div id=\"elib_cata_border\">
				$getEnglishModuleList
				<div id=\"elib_cata_listall\"><a href=\"elib_catalogue_list.php\">".$eLib["html"]["list_all"]."</a></div>
	
		</div>

			<div id=\"elib_cata_bottom\"></div>
	</div>";
	
	$display = ($Lang == "chi")?"style='display:block;visibility:visible;position:relative;'":"style='display:none;visibility:visible;position:relative;'";
	$x .= "<div id=\"elib_cata_wrap_02\" ".$display.">
				<div id=\"elib_cata_top2\">
					<div id=\"elib_lang02\"><a href=\"javascript:void(0);\" onClick=\"document.getElementById('elib_cata_wrap_01').style.display='block';document.getElementById('elib_cata_wrap_02').style.display='none'\"  document.getElementById('elib_cata_wrap_01').style.visibility=\"visible\">".$eLib['html']['english']."</a></div>
					<div id=\"elib_lang01\">".$eLib['html']['chinese']."</div>
				</div>

				<div id=\"elib_cata_border\">
						$getChineseModuleList
					
						<div id=\"elib_cata_listall\"><a href=\"elib_catalogue_list.php\">".$eLib["html"]["list_all"]."</a></div> 
    			
				</div>

				<div id=\"elib_cata_bottom\"></div>
	</div>";
	

return $x;
} // end function get catalogue Menu


//var_dump($catListArr["eng"][0]["name"]);
///////////////////////////////////////////////////////////////////////////
// preset catalogue list //// 
$catListArr["eng"][0]["name"] = "Readers";
$catListArr["eng"][0]["subname"][0] = "Adventure";
$catListArr["eng"][0]["subname"][1] = "Comedy";
$catListArr["eng"][0]["subname"][2] = "Future Thriller";
$catListArr["eng"][0]["subname"][3] = "Human Interest";
$catListArr["eng"][0]["subname"][4] = "Horror";
$catListArr["eng"][0]["subname"][5] = "Ghost Story";
$catListArr["eng"][0]["subname"][6] = "Murder Mystery";
$catListArr["eng"][0]["subname"][7] = "Romance";
$catListArr["eng"][0]["subname"][8] = "Science Fiction";
$catListArr["eng"][0]["subname"][9] = "Short Stories";
$catListArr["eng"][0]["subname"][10] = "Thriller";

$catListArr["chi"][0]["name"] = "文學";
$catListArr["chi"][0]["subname"][0] = "中國現代文學名著";
$catListArr["chi"][0]["subname"][1] = "中國古典詩詞";
$catListArr["chi"][0]["subname"][2] = "中華歷史文庫";

$catListArr["chi"][1]["name"] = "傳記";
$catListArr["chi"][1]["subname"][0] = "中外名人傳記";

$catListArr["chi"][2]["name"] = "生活";
$catListArr["chi"][2]["subname"][0] = "科普藝術教育";
$catListArr["chi"][2]["subname"][1] = "家庭生活百科";


$catListArr["eng"][0]["total_count"] = 0;
$catListArr["eng"][0]["count"][0] = 0;
$catListArr["eng"][0]["count"][1] = 0;
$catListArr["eng"][0]["count"][2] = 0;
$catListArr["eng"][0]["count"][3] = 0;
$catListArr["eng"][0]["count"][4] = 0;
$catListArr["eng"][0]["count"][5] = 0;
$catListArr["eng"][0]["count"][6] = 0;
$catListArr["eng"][0]["count"][7] = 0;
$catListArr["eng"][0]["count"][8] = 0;
$catListArr["eng"][0]["count"][9] = 0;
$catListArr["eng"][0]["count"][10] = 0;

$catListArr["chi"][0]["total_count"] = 0;
$catListArr["chi"][0]["count"][0] = 0;
$catListArr["chi"][0]["count"][1] = 0;
$catListArr["chi"][0]["count"][2] = 0;

$catListArr["chi"][1]["total_count"] = 0;
$catListArr["chi"][1]["count"][0] = 0;

$catListArr["chi"][2]["total_count"] = 0;
$catListArr["chi"][2]["count"][0] = 0;
$catListArr["chi"][2]["count"][1] = 0;

//$eLib_Catalogue = new3Get_Catalogue_Menu("chi");


if($intranet_session_language == "en"){
	$eLib_Catalogue = new3Get_Catalogue_Menu("eng");
	
}else if($intranet_session_language == "b5"){
	$eLib_Catalogue = new3Get_Catalogue_Menu("chi");
}


function getChineseModuleList(){
	global $catListArr;
	
	$x="";
	
	for($i=0;$i<count($catListArr["chi"]);$i++){
		
		$x .= "<div id=\"elib_cata_head\">" .
				"<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["chi"][$i]["name"]."\">".$catListArr["chi"][$i]["name"]."</a></p>" .
				"</div>";
		$x2="";
		for($j=0;$j<count($catListArr["chi"][$i]["subname"]);$j++){
			$x2 .= "<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["chi"][$i]["name"]."&SubCategory=".$catListArr["chi"][$i]["subname"][$j]."\">".$catListArr["chi"][$i]["subname"][$j]."</a></p>";
		
		}
		$x .= "<div id=\"elib_cata_body\">".$x2."</div>";			
	}
	
	return $x;
}


function getEnglishModuleList(){
	global $catListArr;
	
	$x= "";
	
	for($i=0;$i<count($catListArr["eng"]);$i++){
		
		$x .= "<div id=\"elib_cata_head\"><p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["eng"][$i]["name"]."\">".$catListArr["eng"][$i]["name"]."</a></p></div>";
		$x2="";
		for($j=0;$j<count($catListArr["eng"][$i]["subname"]);$j++){
			
			$x2 .= "<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["eng"][$i]["name"]."&SubCategory=".$catListArr["eng"][$i]["subname"][$j]."\">".$catListArr["eng"][$i]["subname"][$j]."</a></p>";
		}
			$x .= "<div id=\"elib_cata_body\">".$x2."</div>";
	}
	
	return $x;
}?>