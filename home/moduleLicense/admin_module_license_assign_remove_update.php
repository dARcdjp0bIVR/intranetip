<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

# IES Module License access right checking
//libIntranetModule::checkModuleLicenceAccess();

intranet_auth();
intranet_opendb();
#################################################################
$strModuleStudentID = isset($_REQUEST['strModuleStudentID'])? $_REQUEST['strModuleStudentID'] : "";

if($strModuleStudentID==""){
	intranet_closedb();
	echo $eLib['SystemMsg']['Err_NoStudentsSelected'];
	die();
}  

//$objInstall	= new elibrary_install();
$libIntranetModule = new libIntranetModule();

if($libIntranetModule->delete_module_student($strModuleStudentID)){
	echo 1;
}else{
	echo 0;
}




#################################################################
intranet_closedb();
?>