<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

# IES Module License access right checking
//libIntranetModule::checkModuleLicenceAccess();

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("module_script.php");
###########################################################
$ModuleID 		= isset($_REQUEST['ModuleID'])? trim($_REQUEST['ModuleID']) : "";
$ModuleLicenseID 	= isset($_REQUEST['ModuleLicenseID'])? trim($_REQUEST['ModuleLicenseID']) : "";

# IES Module License access right checking
libIntranetModule::checkModuleLicenceAccessByModuleId($ModuleID);

$linterface = new interface_html('popup5.html');
//$objInstall	= new elibrary_install();
$libIntranetModule = new libIntranetModule();
$objUser 	= new libuser();
$libclass = new libclass();

$invalid_quota_msg= "";

if(empty($ModuleID)){
	die();
}
#####################################################
## Get quota available
$available = $libIntranetModule->get_available_quota($ModuleID);

$hasInvalidQuota = $libIntranetModule->check_module_quota_is_original($ModuleID);

$isValid = $hasInvalidQuota[0];
$invalidQuota = $hasInvalidQuota[1];

if($isValid){	
	$invalid_quota_msg = "<BR />".$invalidQuota.$elib['SystemMsg']['Err_ExistInvalidQuota'];
}
#####################################################
## Generate student select box

## Get list of students already assigned to module
$aryStudentLicense = $libIntranetModule->get_student_license_list($ModuleID);

$str = "";

//THERE IS STUDENT RECORD EXIST IN THE MODULE ID
if(sizeof($aryStudentLicense) > 0 && is_array($aryStudentLicense)){
	foreach($aryStudentLicense  as $i=> $data){
		$str .= $str != "" ? "," : "";
		$str .= $data['UserID'];	
	}
	$condition = " AND UserID NOT IN (".$str.")";
}

## Get list of ALL students
$aryStudentList = $objUser->returnUsersType(2, $condition);
$selectBox = getSelectByArray($aryStudentList,'name="studentID" id="studentID" size="20" multiple',"");


$ClassSelection= $libclass->getSelectClassID( "name='YearClassID' id='YearClassID' onChange='gen_year_class_user_ui(this);'", $ClassName );

//$objInstall->gen_year_class_user_ui

#####################################################


$linterface->LAYOUT_START();
?>
<script>

var _available = "<?=$available?>";
var _remain = "<?=$available?>";
 
function recalculate_availability(){
	_remain = parseInt(_available) - parseInt($('#confirmList option').size()) ;
	$("#available_panel").text(_remain);	
}

function add_student(){
	 if ($("#StudentID option:selected").length){
		$('#StudentID option:selected').each(function(){
			if(_remain <= 0){
				alert("<?=$eLib['SystemMsg']['NoMoreAvailableQuota']?>");
				return false;
			}
			$(this).remove().appendTo('#confirmList');
			_remain--;
		});
		recalculate_availability();	
	 }	 
}
function remove_student(){
	if ($("#confirmList option:selected").length){		
		$('#confirmList option:selected').each(function(){			
			
			$(this).remove();
			if($("#hidden_user_list_"+$(this).val()).val() == $(this).text()){			
				$(this).appendTo('#StudentID');				
			}
							
		});
		recalculate_availability();
	}	
}

function add_all_student(){
	$('#StudentID option').each(function(){		
		if(_remain <= 0){
			alert("<?=$eLib['SystemMsg']['NoMoreAvailableQuota']?>");
			return false;
		}
		$(this).remove().appendTo('#confirmList');
		_remain--;		
	});
	recalculate_availability();
}

function remove_all_student(){
	$('#confirmList option').each(function(){
	
		if($("#hidden_user_list_"+$(this).val()).val() == $(this).text())	
			$(this).remove().appendTo('#StudentID');
	});
	recalculate_availability();
}

function confirm_submit(){
	if($('#confirmList option').length == 0){
		alert("<?=$eLib['SystemMsg']['Err_NoStudentsSelected']?>");
		return;
	}	
	if(confirm("<?=$eLib['admin']['Confirm_Assign_Student']?>")){
			var aryStudentID = [];
			var aryStudentName = [];
			$('#confirmList option').each(function(){				
				aryStudentID.push($(this).val());
				aryStudentName.push($(this).text());								
			});
			
			$.post("admin_module_license_assign_update.php", 
					{ModuleID: "<?=$ModuleID?>", ModuleLicenseID: "<?=$ModuleLicenseID?>", "aryStudentID[]" : aryStudentID, "aryStudentName[]":aryStudentName},
					function(data){						
						$("#display_content").html(data);
					});
	}
}

function gen_year_class_user_ui(){
	var aryStudentID = []; 	
	$('#confirmList option').each(function(){				
		aryStudentID.push($(this).val());									
	});
			
	$.post("admin_ajax_get_class_user.php", 
			{YearClassID: $('#YearClassID option:selected').val(), ModuleID: "<?=$ModuleID?>", "aryStudentID[]": aryStudentID},
			function(data){
				$("#student_list_panel").html(data);				
			});
}


</script>
<form name="form1" id="form1" method="POST" action="admin_module_license_assign_update.php">
<div id="display_content" style="padding:0; margin:0;height: 500px; width:730;background:white;">
	<div  style="height: 470px;width:100%">
		<BR />
		<BR />
		<table align="center">			
			<tr>
				<td>
					<div  style="background:#EEE;width:300px;height:360px;padding:5px 0 0 10px;">
						<div style="height:30px;">
						<?=$ClassSelection?>
						</div>
						<div id="student_list_panel">
							<select name="StudentID" id="StudentID" size="18"  multiple style="height:274px;width:280px"></select>
						</div>
					</div>
				</td>
				<td align="center">
					<input type="button" name="add_btn" id="add_btn" value=">" style="width:40px" onClick="add_student()"  />
					<BR />
					<input type="button" name="add_all_btn" id="add_all_btn" value=">>" style="width:40px" onClick="add_all_student()" />
					<BR />
					<BR />
					
					<input type="button" name="remove_btn" id="remove_btn" value="<" style="width:40px" onClick="remove_student()"/>
					<BR /> 
					<input type="button" name="remove_all_btn" id="remove_all_btn" value="<<" style="width:40px" onClick="remove_all_student()"/>
				</td>
				<td >
					<div style="background:#effee2;width:320px;padding:0 0 0 10px;
					<?php if ($invalid_quota_msg ==""){?>
						height:365px;
					<?php }else{ ?>
						height:365px;
					<?php }?>  
					">
						<div style="height:30px;">
						<?=$eLib['admin']['SelectedStudent']?>
						</div>
						<div>
							<select name="confirmList" id="confirmList" size="18"  multiple style="height:274px;width:280px"></select>
						</div>
						<?=$eLib['admin']['Quota_Left']?> (<span id="available_panel"><?=$available?></span>)
												
						<?=$invalid_quota_msg?>
					</div>
				</td>
			</tr>
			</table>
	</div>
	
	<div id="btn_panel" style="width:100%;height:40px;text-align:center;background:#EEE;">
		<table width="100%" height="100%"><tr><td align="center" >
		<input type="button" name="confirm_btn" id="confirm_btn" value="<?=$eLib["html"]["confirm"]?>" onClick="confirm_submit();" />
		<input type="button" name="close_btn" id="close_btn" value="<?=$eLib["html"]["Close"]?>" onClick="window.parent.tb_remove();" />
		</td></tr></table>
	</div>

</div>

</form>
</div>
<?php
###########################################################
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
