<?
// using 
############################################################################################
################################# eClass 3.0 schema update #################################
############################################################################################

$sql_eClass_update[] = array(
"2003-10-17",
"support unlimited sized notes in planner",
"ALTER TABLE event CHANGE notes notes mediumtext"
);


$sql_eClass_update[] = array(
"2003-10-18",
"for eClass management in IP frontend",
"ALTER TABLE course ADD creator_id int(8) DEFAULT NULL"
);


$sql_eClass_update[] = array(
"2003-11-18",
"keep room type (i.e. reading room)",
"ALTER TABLE course ADD RoomType varchar(8) default 0"
);


$sql_eClass_update[] = array(
"2004-01-30",
"support event title with length of 127 in planner (50 only for old)",
"ALTER TABLE event CHANGE title title varchar(255)"
);


$sql_eClass_update[] = array(
"2004-02-10",
"support ELP courseware linkage",
"ALTER TABLE course ADD ReferenceID varchar(32)"
);


$sql_eClass_update[] = array(
"2004-03-12",
"extend bookmark URL to 255 characters",
"ALTER TABLE bookmark CHANGE url url varchar(255)"
);


$sql_eClass_update[] = array(
"2004-04-29",
"support start & end date for course, ie. SSR",
"ALTER TABLE course ADD StartDate date, ADD EndDate date"
);


$sql_eClass_update[] = array(
"2004-05-19",
"support selected color for events",
"ALTER TABLE event ADD color varchar(8) "
);

$sql_eClass_update[] = array(
"2003-09-08",
"support attachment/link in planner",
"ALTER TABLE event ADD link text "
);


$sql_eClass_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table user_course change user_email user_email varchar(100) "
);


$sql_eClass_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table user_school change user_email user_email varchar(100) "
);


$sql_eClass_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table bookmark change user_email user_email varchar(100) "
);


$sql_eClass_update[] = array(
"2004-06-17",
"set 'forest' as default scheme",
"ALTER TABLE user_course CHANGE scheme_id scheme_id int(2) DEFAULT 4 "
);


$sql_eClass_update[] = array(
"2005-01-18",
"for special settings (i.e. show user name in SSR) ",
"ALTER TABLE course ADD Remark varchar(255) "
);


$sql_eClass_update[] = array(
"2005-02-07",
"change URL length to unlimited ",
"ALTER TABLE bookmark CHANGE url url mediumtext "
);


$sql_eClass_update[] = array(
"2005-05-12",
"increase Class Number length ",
"ALTER TABLE user_school CHANGE class_number class_number varchar(64) "
);


$sql_eClass_update[] = array(
"2005-05-12",
"increase Class Number length ",
"ALTER TABLE user_course CHANGE class_number class_number varchar(64) "
);


$sql_eClass_update[] = array(
"2005-09-05",
"config for parent's access ",
"ALTER TABLE course ADD is_parent BOOL DEFAULT 0 "
);



$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio tables ",
"CREATE TABLE IF NOT EXISTS ASSESSMENT_SUBJECT (
 RecordID int(11) NOT NULL auto_increment,
 CODEID varchar(20),
 EN_SNAME varchar(20),
 CH_SNAME varchar(20),
 EN_DES varchar(255),
 CH_DES varchar(255),
 EN_ABBR varchar(100),
 CH_ABBR varchar(100),
 DisplayOrder int,
 AllLevelSubject int,
 Level1Subject int,
 Level2Subject int,
 Level3Subject int,
 Level4Subject int,
 Level5Subject int,
 Level6Subject int,
 Level7Subject int,
 Level8Subject int,
 Level9Subject int,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID),
 UNIQUE CODEID (CODEID),
 UNIQUE EN_SNAME (EN_SNAME),
 INDEX DisplayOrder (DisplayOrder)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio tables ",
"CREATE TABLE IF NOT EXISTS ASSESSMENT_STUDENT_MAIN_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 WebSAMSRegNo varchar(30),
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 Score float,
 Grade char(10),
 OrderMeritClass int,
 OrderMeritClassTotal int,
 OrderMeritForm int,
 OrderMeritFormTotal int,
 OrderMeritStream int,
 OrderMeritStreamTotal int,
 OrderMeritSubjGroup int,
 OrderMeritSubjGroupTotal int,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID),
 INDEX Year (Year)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio tables ",
"CREATE TABLE IF NOT EXISTS ASSESSMENT_STUDENT_SUBJECT_RECORD (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 WebSAMSRegNo varchar(30),
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 SubjectCode varchar(20),
 SubjectComponentCode varchar(20) default NULL,
 Score float,
 Grade char(10),
 OrderMeritClass int,
 OrderMeritClassTotal int,
 OrderMeritForm int,
 OrderMeritFormTotal int,
 OrderMeritStream int,
 OrderMeritStreamTotal int,
 OrderMeritSubjGroup int,
 OrderMeritSubjGroupTotal int,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID(UserID),
 INDEX SubjectCode (SubjectCode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio tables ",
"CREATE TABLE IF NOT EXISTS PORTFOLIO_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 CourseUserID int(8),
 WebSAMSRegNo varchar(30),
 UserKey varchar(64),
 Nationality varchar(32),
 PlaceOfBirth varchar(32),
 AdmissionDate date,
 IsSuspend tinyint DEFAULT 0,
 InputDate datetime,
 ModifiedDate datetime,
 INDEX UserID (UserID),
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table: ref to PROFILE_STUDENT_MERIT of IP  ",
"CREATE TABLE IF NOT EXISTS MERIT_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 MeritDate datetime,
 NumberOfUnit int(11),
 Reason text,
 PersonInCharge varchar(64),
 RecordType char(2),
 RecordStatus char(2),
 Remark text,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table: ref to PROFILE_STUDENT_ACTIVITY of IP ",
"CREATE TABLE IF NOT EXISTS ACTIVITY_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 ActivityName text,
 Role varchar(100),
 Performance text,
 Remark text,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table: ref to PROFILE_STUDENT_ATTENDANCE of IP ",
"CREATE TABLE IF NOT EXISTS ATTENDANCE_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 AttendanceDate datetime,
 Periods int(11),
 DayType int(11),
 Reason text,
 RecordType char(2),
 RecordStatus char(2),
 Remark text,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


// RecordType: 1 for school input, 2 for student submission
// RecordStatus: 1 for pending, 2 for approved, 3 for reject
// AwardFile: file(s) (separated by ;)
$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table: ref to PROFILE_STUDENT_AWARD of IP ",
"CREATE TABLE IF NOT EXISTS AWARD_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 AwardDate date,
 AwardName text,
 AwardFile text,
 Details text,
 Remark text,
 RecordType char(2),
 RecordStatus char(2),
 ProcessDate datetime,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table ",
"CREATE TABLE IF NOT EXISTS CONDUCT_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 IsAnnual int default 0,
 ClassName varchar(50),
 ClassNumber varchar(16),
 ConductGradeChar varchar(50),
 CommentChi text,
 CommentEng text,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


//RegNo,EnName,ChName,Phone,EmPhone
$sql_eClass_update[] = array(
"2005-12-16",
"iPortfolio table ",
"CREATE TABLE IF NOT EXISTS GUARDIAN_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 EnName varchar(128),
 ChName varchar(128),
 Relation varchar(2),
 Phone varchar(32),
 EmPhone varchar(32),
 Address varchar(255),
 isMain tinyint(4) default 1,
 isSMS tinyint(4) default 0,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);



$sql_eClass_update[] = array(
"2006-01-03",
"enlarge course referenceID",
"ALTER TABLE course CHANGE ReferenceID ReferenceID varchar(32) "
);


$sql_eClass_update[] = array(
"2006-03-31",
"support multiple guardians",
"ALTER TABLE GUARDIAN_STUDENT ADD isMain tinyint(4) default 1 "
);


$sql_eClass_update[] = array(
"2006-08-28",
"Add a field componet code id to ASSESSMENT_SUBJECT ",
"ALTER TABLE ASSESSMENT_SUBJECT ADD CMP_CODEID varchar(20) "
);

$sql_eClass_update[] = array(
"2006-08-29",
"drop the unique constraint CODEID in ASSESSMENT_SUBJECT ",
"ALTER TABLE ASSESSMENT_SUBJECT DROP INDEX CODEID "
);

$sql_eClass_update[] = array(
"2006-08-29",
"drop the unique constraint EN_SNAME in ASSESSMENT_SUBJECT ",
"ALTER TABLE ASSESSMENT_SUBJECT DROP INDEX EN_SNAME "
);

$sql_eClass_update[] = array(
"2006-08-29",
"add a unique constraint SBJ_CMP_CODEID to ASSESSMENT_SUBJECT ",
"ALTER TABLE ASSESSMENT_SUBJECT ADD UNIQUE SBJ_CMP_CODEID (CODEID, CMP_CODEID) "
);

// Category: 1 for competition, 2 for activity, 3 for service, 4 for award, 5 for course, 6 for others
// RecordStatus: 1 for pending, 2 for approved, 3 for reject
// Attachment: file(s) (separated by ;)
$sql_eClass_update[] = array(
"2006-09-06",
"iPortfolio table: ole records",
"CREATE TABLE IF NOT EXISTS OLE_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 StartDate date DEFAULT NULL,
 EndDate date DEFAULT NULL,
 Title text,
 Details text,
 Category char(2),
 Role varchar(64),
 Hours float,
 Attachment text,
 Remark text,
 ApprovedBy int(11),
 RecordStatus char(2),
 ProcessDate datetime DEFAULT NULL,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2006-09-25",
"iPortfolio table: ref to PROFILE_STUDENT_SERVICE of IP ",
"CREATE TABLE IF NOT EXISTS SERVICE_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Year varchar(50),
 Semester varchar(50),
 ServiceDate date,
 ServiceName text,
 Role varchar(100),
 Performance text,
 Hours float,
 Remark text,
 ClassName varchar(50),
 ClassNumber varchar(16),
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2006-12-20",
"optimize planner performance ",
"ALTER TABLE user_course add index course_id (course_id) "
);

$sql_eClass_update[] = array(
"2006-12-20",
"optimize planner performance ",
"ALTER TABLE event add index course_id (course_id) "
);

$sql_eClass_update[] = array(
"2006-12-20",
"optimize planner performance ",
"ALTER TABLE event add index user_id (user_id) "
);

$sql_eClass_update[] = array(
"2006-12-20",
"optimize planner performance ",
"ALTER TABLE event add index user_email (user_email) "
);

$sql_eClass_update[] = array(
"2007-01-11",
"Add a field ele to OLE_STUDENT ",
"ALTER TABLE OLE_STUDENT ADD ELE varchar(100) "
);


$sql_eClass_update[] = array(
"2007-05-04",
"Add a field to indicate the SMS setting for guardian ",
"ALTER TABLE GUARDIAN_STUDENT ADD isSMS tinyint(4) default 0 AFTER isMain "
);

$sql_eClass_update[] = array(
"2007-05-21",
"Add a field Achievement for OLR ",
"ALTER TABLE OLE_STUDENT ADD Achievement varchar(100) AFTER Hours "
);


$sql_eClass_update[] = array(
"2007-06-15",
"optimize iPortfolio",
"alter table PORTFOLIO_STUDENT add index UserID (UserID)"
);


$sql_eClass_update[] = array(
"2007-06-27",
"iPortfolio table: ole ELE",
"CREATE TABLE IF NOT EXISTS OLE_ELE (
 RecordID int(11) NOT NULL auto_increment,
 ChiTitle varchar(255) NOT NULL,
 EngTitle varchar(255) NOT NULL,
 RecordStatus tinyint default 1,
 DefaultID varchar(50),
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2007-06-27",
"iPortfolio table: ole category",
"CREATE TABLE IF NOT EXISTS OLE_CATEGORY (
 RecordID int(11) NOT NULL auto_increment,
 ChiTitle varchar(255) NOT NULL,
 EngTitle varchar(255) NOT NULL,
 RecordStatus tinyint default 1,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2007-10-16",
"iPortfolio table ",
"CREATE TABLE IF NOT EXISTS AUDIT_REPORT_STUDENT (
 AuditStudentID int(8) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 ClassName varchar(50),
 ClassNumber varchar(16),
 AuditField varchar(20),
 Grade varchar(10),
 ReportID int(8),
 LastModifiedBy int(11),
 InputDate datetime,
 ModifiedDate datetime,
 Remarks text,
 PRIMARY KEY (AuditStudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2007-10-16",
"iPortfolio table ",
"CREATE TABLE IF NOT EXISTS AUDIT_REPORT_STUDENT_MORE (
 AuditMoreID int(8) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 ReportID int(8) NOT NULL,
 UserRemarks text,
 FollowUps text,
 LastModifiedBy int(11),
 InputDate datetime,
 ModifiedDate datetime,
 UserType varchar(4),
 PromotionStatus int(4),
 PRIMARY KEY (AuditMoreID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2007-10-16",
"iPortfolio table ",
"CREATE TABLE IF NOT EXISTS AUDIT_REPORT (
 ReportID int(8) NOT NULL auto_increment,
 ReportYear varchar(50) NOT NULL,
 StudentStartDate datetime,
 StudentEndDate datetime,
 TeacherStartDate datetime,
 TeacherEndDate datetime,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClass_update[] = array(
"2007-11-16",
"Add promotion status for Audit Report",
"ALTER TABLE AUDIT_REPORT_STUDENT_MORE ADD COLUMN PromotionStatus INT(4)"
);

$sql_eClass_update[] = array(
"2007-11-23",
"Add Remarks for Audit Report",
"ALTER TABLE AUDIT_REPORT_STUDENT_MORE ADD COLUMN Remarks text"
);

$sql_eClass_update[] = array(
"2007-12-12",
"Correct Name in OLE_ELE",
"UPDATE OLE_ELE SET ChiTitle = '�P�u�@�����g��' WHERE ChiTitle = '�H�λP�u�@�����g��'"
);

$sql_eClass_update[] = array(
"2008-01-08",
"Add for Kei Wai iPortfolio Customization",
"CREATE TABLE IF NOT EXISTS KEIWAI_PROFILE_PROFESSIONAL
(
	RecordID int(8) AUTO_INCREMENT,
	WebSAMSRegNo varchar(30),
	RecordDate date,
	Certificate varchar(200),
	Qualification varchar(200),
	Organization varchar(200),
	ModifiedDate datetime,
	primary key(RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-01-08",
"Add for Kei Wai iPortfolio Customization",
"CREATE TABLE IF NOT EXISTS KEIWAI_PROFILE_ACADEMIC_RESULT
(
	RecordID int(8) AUTO_INCREMENT,
	WebSAMSRegNo varchar(30),
	Period varchar(30),
	ClassLevel varchar(200),
	PositionC int(8),
	TotalStudent int(8),
	PositionL int(8),
	TotalStudentL int(8),
	ModifiedDate datetime,
	primary key(RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-01-08",
"Add for Kei Wai iPortfolio Customization",
"CREATE TABLE IF NOT EXISTS KEIWAI_PROFILE_EDUCATION
(
	RecordID int(8) AUTO_INCREMENT,
	WebSAMSRegNo varchar(30),
	RecordDate date,
	Institute  varchar(200),
	CompleteDate date,
	ModifiedDate datetime,
	primary key(RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-01-09",
"for Kei Wai iPortfolio Customization",
"ALTER TABLE PORTFOLIO_STUDENT ADD COLUMN HKID VARCHAR(15)"
);

$sql_eClass_update[] = array(
"2008-01-09",
"for Kei Wai iPortfolio Customization",
"ALTER TABLE PORTFOLIO_STUDENT ADD COLUMN Religion VARCHAR(50)"
);

$sql_eClass_update[] = array(
"2008-01-09",
"for Kei Wai iPortfolio Customization",
"ALTER TABLE ACTIVITY_STUDENT ADD COLUMN Organization VARCHAR(200) AFTER Performance"
);

$sql_eClass_update[] = array(
"2008-01-09",
"for Kei Wai iPortfolio Customization",
"ALTER TABLE AWARD_STUDENT ADD COLUMN Organization VARCHAR(200) AFTER Details"
);

$sql_eClass_update[] = array(
"2008-01-09",
"for Kei Wai iPortfolio Customization",
"ALTER TABLE SERVICE_STUDENT ADD COLUMN Organization VARCHAR(200) AFTER Performance"
);

$sql_eClass_update[] = array(
"2008-01-14",
"for additional guardian information",
"CREATE TABLE  IF NOT EXISTS GUARDIAN_STUDENT_EXT_1
(
  ExtendRecordID int(8) NOT NULL auto_increment,
  UserID int(8) NOT NULL default '0',
  RecordID int(8) NOT NULL default '0',
  IsLiveTogether tinyint(1) NOT NULL default '1',
  IsEmergencyContact tinyint(1) NOT NULL default '1',
  Occupation varchar(60) default NULL,
  AreaCode char(2) default NULL,
  Road varchar(50) default NULL,
  Address varchar(50) default NULL,
  InputDate datetime default NULL,
  ModifiedDate datetime default NULL,
  PRIMARY KEY  (ExtendRecordID),
  UNIQUE KEY RecordID (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-01-22",
"Add a field subject area to AWARD_STUDENT",
"ALTER TABLE AWARD_STUDENT ADD SubjectArea varchar(200) AFTER Organization"
);


$sql_eClass_update[] = array(
"2008-04-29",
"Correct Name in OLE_ELE",
"UPDATE OLE_ELE SET EngTitle = 'Moral and Civic Education' WHERE EngTitle = 'Moral and Civil Education'"
);


$sql_eClass_update[] = array(
"2008-05-02",
"Add a field Organization to OLE_STUDENT ",
"ALTER TABLE OLE_STUDENT ADD Organization varchar(200) "
);

$sql_eClass_update[] = array(
"2008-07-08",
"iPortfolio table: self account records",
"CREATE TABLE IF NOT EXISTS SELF_ACCOUNT_STUDENT (
 RecordID int(11) NOT NULL auto_increment,
 UserID int(11) NOT NULL,
 Title text,
 Details longblob,
 AttachmentPath text,
 InputDate datetime,
 ModifiedDate datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-07-25",
"For iPortfolio OLE",
"CREATE TABLE IF NOT EXISTS OLE_PROGRAM
(
	ProgramID int(11) NOT NULL auto_increment,
	ProgramType char,
	Title text,
	StartDate Date,
	EndDate Date,
	Category char(2),
	Organization varchar(200),
	Details text,
	SchoolRemarks text,
	ELE varchar(100),
	InputDate datetime,
	ModifiedDate datetime,
	primary key(ProgramID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-07-25",
"For iPortfolio OLE",
"ALTER TABLE OLE_STUDENT ADD COLUMN ProgramID int(11)"
);

$sql_eClass_update[] = array(
"2008-08-11",
"For iPortfolio OLE",
"ALTER TABLE OLE_STUDENT ADD COLUMN TeacherComment text"
);


$sql_eClass_update[] = array(
"2008-08-15",
"Add a field ele to OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD CreatorID int(11) NOT NULL "
);


$sql_eClass_update[] = array(
"2008-08-15",
"Add a field ele to OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD CreatorID int(11) NOT NULL "
);


$sql_eClass_update[] = array(
"2008-08-16",
"Add a field ele to OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD IntExt char(3) DEFAULT 'INT' "
);

$sql_eClass_update[] = array(
"2009-11-11",
"Add a chinese title to OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD `TitleChi` mediumtext default NULL after `Title`"
);

$sql_eClass_update[] = array(
"2009-11-11",
"Add a chinese details to OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD `DetailsChi` mediumtext default NULL after `Details`"
);

$sql_eClass_update[] = array(
"2009-11-20",
"OLE_PROGRAM start to store AcademicYearID",
"ALTER TABLE OLE_PROGRAM ADD `AcademicYearID` int(8) default NULL"
);

$sql_eClass_update[] = array(
"2009-11-20",
"OLE_PROGRAM start to store Year Term ID",
"ALTER TABLE OLE_PROGRAM ADD   `YearTermID` int(8) default NULL"
);

$sql_eClass_update[] = array(
"2009-11-20",
"Add Student can join option for OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD `CanJoin` tinyint(1) default NULL"
);

$sql_eClass_update[] = array(
"2009-11-20",
"Add Student start join date for OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD `CanJoinStartDate` datetime default NULL"
);

$sql_eClass_update[] = array(
"2009-11-20",
"Add Student end join date for OLE_PROGRAM ",
"ALTER TABLE OLE_PROGRAM ADD `CanJoinEndDate` datetime default NULL"
);

$sql_eClass_update[] = array(
"2009-11-26",
"Add AutoApprove (Y, N) for OLE_PROGRAM when teacher set a PROGRAM allow student to approve",
"ALTER TABLE OLE_PROGRAM ADD `AutoApprove` char(2) default NULL"
);

$sql_eClass_update[] = array(
"2009-12-28",
"Add SUB CATEGORY ID for OLE_PROGRAM",
"ALTER TABLE OLE_PROGRAM ADD `SubCategoryID` int(11) default NULL after `Category`;"
);


$sql_eClass_update[] = array(
"2008-08-28",
"Add a field ele to OLE_STUDENT ",
"ALTER TABLE OLE_STUDENT ADD IntExt char(3) DEFAULT 'INT' "
);

$sql_eClass_update[] = array(
"2008-08-29",
"Add fields to SELF_ACCOUNT_STUDENT to accomplish default self account",
"ALTER TABLE SELF_ACCOUNT_STUDENT ADD COLUMN DefaultSA varchar(255) AFTER AttachmentPath"
);

$sql_eClass_update[] = array(
"2008-09-30",
"eEnrolment - Map enrolment Group info to OLE records",
"ALTER TABLE OLE_STUDENT ADD COLUMN GroupID int(8);"
);

$sql_eClass_update[] = array(
"2008-10-09",
"Add EnrolType in OLE_STUDENT for records added from eEnrolment: C - Club, A - Activity",
"ALTER TABLE OLE_STUDENT add COLUMN EnrolType char(2);"
);

$sql_eClass_update[] = array(
"2008-10-10",
"Add Index to OLE_STUDENT to increase join speed",
"ALTER TABLE OLE_STUDENT ADD INDEX ProgramID (ProgramID), ADD INDEX UserID (UserID);"
);

$sql_eClass_update[] = array(
"2008-10-21",
"Change title field of OLE_STUDENT to varchar(255)",
"ALTER TABLE OLE_STUDENT MODIFY Title varchar(255);"
);

$sql_eClass_update[] = array(
"2008-10-22",
"Addon log error mysql query table",
"CREATE TABLE IF NOT EXISTS ECLASS_LOGGING_TABLE
(
    RecordID int(11) NOT NULL auto_increment,
    UserID int(11),
    CourseID int(11),
    SqlQuery TEXT,
    SourcePath TEXT,
    InputDate datetime,
    IPAddress varchar(255),
    ErrorMsg TEXT,
    primary key(RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-10-28",
"For iPortfolio OLE comments",
"CREATE TABLE IF NOT EXISTS OLE_COMMENT_STUDENT
(
    RecordID int(11) NOT NULL auto_increment,
    ELEID int(11),
    StudentID int(11),
    TeacherID int(11),
    CommentContent text,
    InputDate datetime,
    ModifiedDate datetime,
    primary key(RecordID),
    unique key StudentELE (ELEID, StudentID),
    index (TeacherID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2008-11-07",
"To support key length with more than 64",
"ALTER TABLE PORTFOLIO_STUDENT MODIFY UserKey varchar(255)"
);

$sql_eClass_update[] = array(
"2008-11-17",
"Add Index to PORTFOLIO_STUDENT to increase join speed",
"ALTER TABLE PORTFOLIO_STUDENT ADD INDEX CourseUserID (CourseUserID)"
);

$sql_eClass_update[] = array(
"2009-02-04",
"Add column to support user-defined OLE records for SLP report",
"ALTER TABLE OLE_STUDENT ADD SLPOrder int(8)"
);


$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support sharing resource",
"CREATE TABLE IF NOT EXISTS `sharing_resource` (
  `share_resource_id` int(8) NOT NULL auto_increment,
  `resource_id` int(8) default NULL,
  `resource_type` int(3) default NULL,
  `title` varchar(255) default NULL,
  `description` text,
  `course_id` int(8) default NULL,
  `hitcount` int(8) default 0,
  `starcount` int(8) default 0,
  `tags` text,
  `createdby` int(8) default null,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`share_resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support common rubric set pool",
"CREATE TABLE IF NOT EXISTS `common_rubric_set` (
  `common_rubric_set_id` int(8) NOT NULL auto_increment,
  `set_title` text,
  `description` text,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`common_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support common rubric",
"CREATE TABLE IF NOT EXISTS `common_rubric` (
  `common_rubric_id` int(8) NOT NULL auto_increment,
  `common_rubric_set_id` int(8) NOT NULL,
  `title` text,
  `showorder` int(3) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`common_rubric_id`),
  KEY `key_std_rubric_set_id` (`common_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support common rubric detail",
"CREATE TABLE IF NOT EXISTS `common_rubric_detail` (
  `common_rubric_detail_id` int(8) NOT NULL auto_increment,
  `common_rubric_id` int(8) NOT NULL,
  `common_rubric_set_id` int(8) NOT NULL,
  `score` int(3) default NULL,
  `description` text,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`common_rubric_detail_id`),
  KEY `key_common_rubric_id` (`common_rubric_id`),
  KEY `key_common_rubric_set_id` (`common_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support sharing resource log",
"CREATE TABLE IF NOT EXISTS `sharing_resource_log` (
  `share_resource_log_id` int(8) NOT NULL auto_increment,
  `share_resource_id` int(8) default NULL,
  `dest_course_id` int(8) default NULL,
  `dest_resource_id` int(8) default NULL,
  `createdby` int(8) default null,
  `inputdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`share_resource_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-08",
"Create table to support sharing resource favourite",
"CREATE TABLE IF NOT EXISTS `sharing_resource_favourite` (
  `favourite_id` int(8) NOT NULL auto_increment,
  `share_resource_id` int(8) default null,
  `user_id` int(8) default null,
  `inputdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`favourite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-06-22",
"Create table to support sharing resource weblink",
"CREATE TABLE IF NOT EXISTS `common_weblink` (
  `common_weblink_id` int(8) NOT NULL auto_increment,
  `notes_id` int(8) default NULL,
  `category` varchar(100) default NULL,
  `title` varchar(100) default NULL,
  `url` text,
  `type` int(2) default NULL,
  `keyword` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `num_comment` int(4) default NULL,
  `tot_rate` int(4) default NULL,
  `readflag` text,
  `inputdate` datetime default NULL,
  `modified` datetime default NULL,
  `user_name` varchar(255) default NULL,
  `user_email` varchar(100) default NULL,
  `user_id` int(8) default NULL,
  `status` char(2) default '1',
  PRIMARY KEY  (`common_weblink_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
"2009-07-06",
"Create chinesename in eclass40 in user_course table",
"alter table user_course
add column chinesename varchar(100) default null"
);

$sql_eClass_update[] = array(
"2009-07-13",
"For Settings Interface - remember font size preference" ,
"alter table user_course add column font_size int(2) default 1"
);

//some table may have "font_size" before, use "change"
$sql_eClass_update[] = array(
"2009-07-13",
"For Settings Interface - remember font size preference" ,
"alter table user_course change font_size font_size int(2) default 1"
);

$sql_eClass_update[] = array(
"2009-07-13",
"For Settings Interface - remember default language preference" ,
"alter table user_course add column language int(2) default 1"
);

//some table may have "language" before, use "change"
$sql_eClass_update[] = array(
"2009-07-13",
"For Settings Interface - remember default language preference" ,
"alter table user_course change language language int(2) default 1"
);


$sql_eClass_update[] = array(
"2009-07-13",
"For Settings Interface - remember personal background image" ,
"alter table user_course add column scheme_background varchar(255) default NULL"
);

$sql_eClass_update[] = array(
"2009-07-20",
"For saving chat settings" ,
"alter table course
add column chat_message_poll_interval int default NULL,
add column chat_status_check_interval int default NULL,
add column chat_max_idle_period int default NULL,
add column chat_enabled_modules varchar(255) default NULL"
);

$sql_eClass_update[] = array(
"2009-08-21",
"For link with UserID from INTRANET_USER" ,
"alter table user_course add column intranet_user_id int(8) default null"
);

$sql_eClass_update[] = array(
"2009-09-04",
"add calID to course" ,
"alter table course add column CalID int(8)"
);

$sql_eClass_update[] = array(
"2009-09-09",
"alter course for subject group mapping" ,
"alter table course add column SubjectGroupID int(8), add column IsFromSubjectGroup char(1) default '0'"
);

$sql_eClass_update[] = array(
"2009-09-22",
"add IDs to ASSESSMENT_STUDENT_MAIN_RECORD for iPortfolio " ,
"ALTER TABLE ASSESSMENT_STUDENT_MAIN_RECORD
	ADD COLUMN AcademicYearID int(11) AFTER Year,
	ADD COLUMN YearTermID int(11) AFTER Semester,
	ADD COLUMN YearClassID int(11) AFTER ClassName"
);

$sql_eClass_update[] = array(
"2009-09-22",
"add IDs to ASSESSMENT_STUDENT_SUBJECT_RECORD for iPortfolio" ,
"ALTER TABLE ASSESSMENT_STUDENT_SUBJECT_RECORD
	ADD COLUMN AcademicYearID int(11) AFTER Year,
	ADD COLUMN YearTermID int(11) AFTER Semester,
	ADD COLUMN YearClassID int(11) AFTER ClassName,
	ADD COLUMN SubjectID int(11) AFTER SubjectCode,
	ADD COLUMN SubjectComponentID int(11) AFTER SubjectComponentCode"
);

$sql_eClass_update[] = array(
"2010-01-10",
"Add a field to differentiate whether the user is the past student or not",
"ALTER TABLE user_course ADD COLUMN is_past CHAR(2) DEFAULT NULL"
);

$sql_eClass_update[] = array(
"2010-01-22",
"Add a field to store which form can join an OLE programme",
"ALTER TABLE OLE_PROGRAM ADD COLUMN JoinableYear varchar(255)"
);

$sql_eClass_update[] = array(
"2010-01-22",
"Add index to increase OLE loading performance",
"ALTER TABLE OLE_PROGRAM ADD INDEX CreatorID (CreatorID), ADD INDEX AcademicYearID (AcademicYearID), ADD INDEX YearTermID (YearTermID)"
);

$sql_eClass_update[] = array(
"2010-01-26",
"Add ComeFrom to OLE_PROGRAM to indicate this record come from where (ipf , enrollment , import, teacher , student ...)",
"ALTER TABLE OLE_PROGRAM ADD `ComeFrom` SMALLINT default NULL"
);

$sql_eClass_update[] = array(
"2010-01-26",
"Add ComeFrom to OLE_STUDENT to indicate this record come from where (ipf , enrollment , import, teacher , student ...)",
"ALTER TABLE OLE_STUDENT ADD `ComeFrom` SMALLINT default NULL"
);

$sql_eClass_update[] = array(
"2010-02-01",
"For Dynamic Report in iPortfolio",
"CREATE TABLE IF NOT EXISTS `student_report_template` (
  `template_id` int(11) NOT NULL auto_increment,
  `template_title` varchar(255) NOT NULL default '' COMMENT 'Duplicated for display in table.',
  `data_source_list` varchar(255) default NULL COMMENT 'Duplicated for display in table.',
  `template_data` text NOT NULL,
  `template_data_bak1` text COMMENT 'For backup of the template_data only.',
  `template_data_bak2` text COMMENT 'For backup of the template_data only.',
  `inputdate` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC"
);
################################################################################
############## END OF eClass Main DB Tables UPDATE #############################
################################################################################

###################################### eClass ##############################################



$sql_course_update[] = array(
"2003-09-03",
"support handin re-do",
"ALTER TABLE handin ADD status varchar(2) DEFAULT 'L' "
);


$sql_course_update[] = array(
"2003-10-08",
"support media files in bulletin",
"ALTER TABLE bulletin ADD attachment text "
);


$sql_course_update[] = array(
"2003-10-08",
"keep status of group",
"ALTER TABLE grouping ADD Status int(2) default 0"
);


$sql_course_update[] = array(
"2003-11-04",
"keep category of notes",
"ALTER TABLE notes ADD ReadingCategoryID int(8) default 0"
);


$sql_course_update[] = array(
"2003-11-25",
"keep provider of question",
"ALTER TABLE question ADD Provider varchar(128) default NULL"
);


$sql_course_update[] = array(
"2003-12-15",
"store total size of media files in bulletin",
"ALTER TABLE bulletin ADD attachment_size int(11) default NULL "
);


$sql_course_update[] = array(
"2003-12-30",
"store identities who can attach file in bulletin",
"ALTER TABLE forum ADD AttachRight varchar(8) "
);


$sql_course_update[] = array(
"2004-01-02",
"control when to release marked exam paper",
"ALTER TABLE quiz ADD marked_release int(1) DEFAULT 0 "
);


$sql_course_update[] = array(
"2004-03-12",
"extend webliknks URL to 255 characters",
"ALTER TABLE weblink CHANGE url url varchar(255) "
);


$sql_course_update[] = array(
"2004-03-22",
"support unlimit attachments",
"alter table assignment change attachment attachment text "
);


$sql_course_update[] = array(
"2004-04-06",
"support start time for assignment ",
"alter table assignment add starttime datetime "
);


$sql_course_update[] = array(
"2004-04-15",
"store time accumulated ",
"alter table quiz_result add time_spend time DEFAULT 0 "
);


$sql_course_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table usermaster change user_email user_email varchar(100) "
);


$sql_course_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table bulletin change user_email user_email varchar(100) "
);


$sql_course_update[] = array(
"2004-06-17",
"increase email field to 100 characters",
"alter table weblink change user_email user_email varchar(100) "
);


$sql_course_update[] = array(
"2004-07-07",
"store marked answer of online writing",
"alter table handin add answer_marked text "
);


$sql_course_update[] = array(
"2004-07-26",
"distinguish Grade/Mark sheet",
"ALTER TABLE marksheet ADD RecordType char(2) DEFAULT '0' "
);


$sql_course_update[] = array(
"2004-07-26",
"support grade in marksheet column",
"ALTER TABLE marksheet_result CHANGE mark mark varchar(32) "
);


$sql_course_update[] = array(
"2004-07-26",
"support grade in marksheet column",
"ALTER TABLE marksheet_column CHANGE score score varchar(32) "
);


$sql_course_update[] = array(
"2004-07-26",
"support difficulties in question bank",
"ALTER TABLE question ADD difficulty varchar(2) DEFAULT '0' "
);


$sql_course_update[] = array(
"2004-07-30",
"marking-scheme for an assignment using writing online method",
"ALTER TABLE assignment ADD marking_scheme varchar(2) "
);


$sql_course_update[] = array(
"2004-08-05",
"allow teacher to initiate a thread of message ",
"ALTER TABLE diary ADD toStudent int(8) default NULL "
);


$sql_course_update[] = array(
"2004-08-16",
"increase the length of grade field for handins",
"ALTER TABLE handin CHANGE grade grade varchar(16) "
);


$sql_course_update[] = array(
"2004-08-17",
"record the total no of word and symbols ",
"ALTER TABLE handin ADD totalword varchar(64) "
);


$sql_course_update[] = array(
"2004-08-23",
"store correction of online writing and the user_id for marking",
"alter table handin add answer_correction text,
add is_correction bool default 0,
add correctiondate datetime,
add marked_by int(8) "
);


$sql_course_update[] = array(
"2004-08-23",
"store correction of online writing",
"ALTER TABLE notes ADD description varchar(255), ADD starttime datetime, ADD endtime datetime "
);


$sql_course_update[] = array(
"2004-09-09",
"store type of answer used",
"ALTER TABLE assignment ADD sheettype varchar(2) "
);


$sql_course_update[] = array(
"2004-09-22",
"store end date of schedule",
"ALTER TABLE schedule ADD enddate datetime "
);

$sql_course_update[] = array(
"2004-11-11",
"support power concept",
"CREATE TABLE IF NOT EXISTS powerconcept (
	PowerConceptID int(8) NOT NULL auto_increment,
	Name varchar(128),
	DataContent longblob,
	InputDate datetime DEFAULT NULL,
	ModifiedDate datetime DEFAULT NULL,
	user_id int(8),
	group_id int(8),
	assignment_id int(8),
	PRIMARY KEY (PowerConceptID)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


/*
$sql_course_update[] = array(
"2004-11-11",
"change datacontent to binary type",
"ALTER TABLE powerconcept change DataContent DataContent blob "
);
*/

$sql_course_update[] = array(
"2004-12-06",
"increase capacity of powerconcept data",
"ALTER TABLE powerconcept
  CHANGE datacontent DataContent longblob"
);


$sql_course_update[] = array(
"2005-01-07",
"limit the number of attempt",
"ALTER TABLE quiz
  ADD attempt_limit int(4) DEFAULT NULL"
);


$sql_course_update[] = array(
"2005-01-10",
"support multiple files in assignment handin ",
"ALTER TABLE handin
  CHANGE filename filename TEXT,
  ADD filepath varchar(255) "
);


$sql_course_update[] = array(
"2005-02-07",
"change URL length to unlimited ",
"ALTER TABLE notes CHANGE url url text "
);


$sql_course_update[] = array(
"2005-02-07",
"change URL length to unlimited ",
"ALTER TABLE weblink CHANGE url url text "
);


$sql_course_update[] = array(
"2005-05-12",
"increase Class Number length ",
"ALTER TABLE usermaster CHANGE class_number class_number varchar(64) "
);


$sql_course_update[] = array(
"2005-07-26",
"support PowerBoard ",
"CREATE TABLE IF NOT EXISTS powerboard (
	PowerBoardID int(8) NOT NULL auto_increment,
	Name varchar(128),
	DataContent longblob,
	InputDate datetime DEFAULT NULL,
	ModifiedDate datetime DEFAULT NULL,
	user_id int(8),
	group_id int(8),
	assignment_id int(8),
	PRIMARY KEY (PowerBoardID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_course_update[] = array(
"2005-08-19",
"outline description size ",
"ALTER TABLE outline CHANGE description description longblob "
);


$sql_course_update[] = array(
"2005-08-22",
"glossary explanation size ",
"ALTER TABLE glossary CHANGE explanation explanation longblob "
);


$sql_course_update[] = array(
"2005-08-22",
"my_notes content size ",
"ALTER TABLE my_notes CHANGE my_notes my_notes longblob "
);


$sql_course_update[] = array(
"2005-08-22",
"bulletin message size ",
"ALTER TABLE bulletin CHANGE message message longblob "
);


$sql_course_update[] = array(
"2005-08-22",
"question question and explanation sizes ",
"ALTER TABLE question CHANGE question question longblob, CHANGE explanation explanation longblob "
);


$sql_course_update[] = array(
"2005-08-29",
"notes size ",
"ALTER TABLE notes CHANGE description description text "
);


$sql_course_update[] = array(
"2005-09-06",
"optimize handin table",
"ALTER TABLE handin ADD INDEX assignment_id (assignment_id) "
);


$sql_course_update[] = array(
"2005-09-14",
"increase size for essay marking",
"ALTER TABLE handin CHANGE answer answer longblob, CHANGE answer_correction answer_correction longblob, CHANGE answer_marked answer_marked longblob "
);


$sql_course_update[] = array(
"2005-10-07",
"add record type to notes",
"alter table notes add RecordType char(8) "
);


$sql_course_update[] = array(
"2005-10-07",
"add record type to notes_student",
"alter table notes_student add RecordType char(8) "
);


$sql_course_update[] = array(
"2005-11-29",
"improve handin table speed",
"ALTER TABLE handin ADD INDEX user_id (user_id) "
);


$sql_course_update[] = array(
"2006-01-02",
"selection tracking of question",
"ALTER TABLE question ADD last_selected_date_exercise datetime,
	ADD last_selected_date_exam datetime,
	ADD last_selected_date datetime "
);

$sql_course_update[] = array(
"2006-01-21",
"public result",
"CREATE TABLE IF NOT EXISTS handin_public (
	handin_public_id int(8) NOT NULL auto_increment,
	assignment_id int(8),
	stats_public bool DEFAULT 0,
	inputdate datetime DEFAULT NULL,
	modified datetime DEFAULT NULL,
	PRIMARY KEY (handin_public_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2006-01-21",
"public result record",
"CREATE TABLE IF NOT EXISTS handin_public_record (
	handin_public_id int(8),
	handin_id int(8),
	grade_public bool DEFAULT 0,
	handin_public bool DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_course_update[] = array(
"2006-04-21",
"optimize quiz_result table",
"ALTER TABLE quiz_result ADD INDEX quiz_id (quiz_id) "
);


$sql_course_update[] = array(
"2006-06-12",
"avoid duplicate file in same folder",
"ALTER TABLE eclass_file ADD UNIQUE FolderFile (ParentDirID, Title) "
);


$sql_course_update[] = array(
"2006-06-15",
"alter the user_group table so that group_id and user_id are the unique key",
"ALTER TABLE user_group ADD UNIQUE GroupUser (group_id, user_id) "
);

$sql_course_update[] = array(
"2006-06-16",
"add status index for speeding the searching in usermaster table",
"ALTER TABLE usermaster ADD INDEX status (status) "
);

$sql_course_update[] = array(
"2006-06-16",
"add memberType index for speeding the searching in usermaster table",
"ALTER TABLE usermaster ADD INDEX memberType (memberType) "
);

$sql_course_update[] = array(
"2006-06-16",
"add user_id index for speeding the searching in marksheet_result table",
"ALTER TABLE marksheet_result ADD INDEX user_id (user_id) "
);

$sql_course_update[] = array(
"2006-07-13",
"adding the field 'TopPost' to the table 'bulletin'",
"ALTER TABLE bulletin ADD TopPost int(4) NOT NULL DEFAULT 999"
);

$sql_course_update[] = array(
"2006-07-31",
"control answering_mode",
"ALTER TABLE quiz ADD answering_mode varchar(2) DEFAULT 'S'"
);

$sql_course_update[] = array(
"2006-07-31",
"mark status of question",
"ALTER TABLE quiz_result_question ADD flag tinyint DEFAULT 0"
);

$sql_course_update[] = array(
"2006-08-09",
"mark status of question",
"ALTER TABLE self_test_question ADD flag tinyint DEFAULT 0"
);

$sql_course_update[] = array(
"2006-08-11",
"answering mode of self test",
"ALTER TABLE self_test ADD answering_mode varchar(1) DEFAULT 'P'"
);

$sql_course_update[] = array(
"2006-08-11",
"answering mode of quiz",
"ALTER TABLE quiz_result ADD answering_mode varchar(1) DEFAULT 'P'"
);

$sql_course_update[] = array(
"2006-08-28",
"add teacher subject ID for assignment ",
"alter table assignment add teacher_subject_id int(4) "
);

$sql_course_update[] = array(
"2006-08-28",
"add IsDraft in bulletin ",
"alter table bulletin add IsDraft tinyint DEFAULT 0"
);

$sql_course_update[] = array(
"2006-10-13",
"add answer display mode for assignment ",
"alter table assignment add answer_display_mode int(4) default 0"
);


$sql_course_update[] = array(
"2006-12-14",
"add a unique constraint marksheet_column_id, user_id to marksheet_result ",
"ALTER TABLE marksheet_result ADD UNIQUE MarksheetColumnUser (marksheet_column_id, user_id) "
);


$sql_course_update[] = array(
"2006-12-21",
"improve forum speed ",
"ALTER TABLE bulletin ADD index followup_id (followup_id) "
);

$sql_course_update[] = array(
"2007-03-21",
"add powervoice for eBook",
"ALTER TABLE notes ADD COLUMN voice varchar(255)"
);

$sql_course_update[] = array(
"2007-03-26",
"add powervoice for glossary",
"ALTER TABLE glossary ADD COLUMN voice varchar(255)"
);

$sql_course_update[] = array(
"2007-04-04",
"add powerboard for glossary",
"ALTER TABLE glossary ADD COLUMN powerboard varchar(255)"
);

$sql_course_update[] = array(
"2007-05-23",
"Add a column 'has_right' to the table 'grouping' for admin right setting",
"ALTER table grouping ADD has_right tinyint default 0"
);

$sql_course_update[] = array(
"2007-07-25",
"optimize quiz_result table",
"ALTER TABLE quiz_result ADD INDEX user_id (user_id) "
);

$sql_course_update[] = array(
"2007-07-25",
"optimize quiz_result_question table",
"ALTER TABLE quiz_result_question ADD INDEX question_id (question_id) "
);

$sql_course_update[] = array(
"2007-07-25",
"optimize self_test table",
"ALTER TABLE self_test ADD INDEX user_id (user_id) "
);

$sql_course_update[] = array(
"2007-07-25",
"optimize self_test_question table",
"ALTER TABLE self_test_question ADD INDEX question_id (question_id) "
);

$sql_course_update[] = array(
"2007-08-30",
"support WordCount in bulletin",
"ALTER TABLE bulletin ADD WordCount Int(5) default 0 "
);

$sql_course_update[] = array(
"2008-01-31",
"for emeeting eclass_file add convert type table",
"ALTER TABLE eclass_file ADD COLUMN convert_type char(1) default 0 "
);

$sql_course_update[] = array(
"2008-04-16",
"prevent duplicated questions exist in a quiz",
"ALTER TABLE quiz_question ADD UNIQUE question_by_quiz (quiz_id, question_id)"
);

$sql_course_update[] = array(
"2008-04-16",
"prevent duplicated questions exist in a quiz",
"ALTER TABLE quiz_result_question ADD UNIQUE question_by_user (quiz_result_id, question_id)"
);

$sql_course_update[] = array(
"2008-05-09",
"allow user to edit bulletin topics after submitted",
"ALTER TABLE bulletin ADD modified datetime "
);

$sql_course_update[] = array(
"2008-05-09",
"allow user to edit bulletin topics after submitted",
"ALTER TABLE forum ADD ModifyRight varchar(8) "
);

$sql_course_update[] = array(
"2008-06-16",
"Set record time for PowerVOICE Assignment",
"ALTER TABLE assignment ADD limit_voice int(4) "
);


$sql_course_update[] = array(
"2008-08-29",
"Set Folder Size of iportfolio scheme",
"alter table web_portfolio add folder_size int(64) "
);

$sql_course_update[] = array(
"2008-12-03",
"Lengthen group name field",
"alter table grouping modify group_name varchar(64)"
);

$sql_course_update[] = array(
"2009-01-14",
"support attachment in annoucement",
"alter table announcement add attachment text"
);

$sql_course_update[] = array(
"2009-04-23",
"It is used to handle the individual assessment for selecting particular user (students)",
"alter table grouping_function add column user_id int(8) default null after group_id"
);

$sql_course_update[] = array(
"2009-04-23",
"It is used to handle the fileupload settings (multi-upload or single upload) and answersheet settings which will be used later",
"alter table task add column sheettype int(2) default null after lowestmark"
);

$sql_course_update[] = array(
"2009-04-29",
"It is used to handle the showing order of each standard rubric in each standard rubric set",
"alter table standard_rubric add column showorder int(3) default null after title"
);

$sql_course_update[] = array(
"2009-04-30",
"It is used to handle the rubric record which is created temporarily without having a real assessment or phase or task",
"alter table task_rubric add column recordkey varchar(255) default null after showorder"
);

$sql_course_update[] = array(
"2009-05-26",
"It is used to handle the marking weight of teacher in a task",
"alter table task add column teacher_weight float default -1 after sheettype"
);

$sql_course_update[] = array(
"2009-05-26",
"It is used to handle the marking weight of self in a task",
"alter table task add column self_weight float default -1 after teacher_weight"
);

$sql_course_update[] = array(
"2009-05-26",
"It is used to handle the marking weight of peer in a task",
"alter table task add column peer_weight float default -1 after self_weight"
);

$sql_course_update[] = array(
"2009-06-04",
"It is userd to handle the sequence number in quiz question",
"alter table quiz_question add sequence_no tinyint(4)"
);

$sql_course_update[] = array(
"2009-06-05",
"It is userd to handle the description of standard rubric set",
"alter table standard_rubric_set add column description text after set_title"
);

$sql_course_update[] = array(
"2009-06-06",
"It is userd to handle the rubric set type of standard rubric set",
"alter table standard_rubric_set add column set_type int(3) after description"
);

$sql_course_update[] = array(
"2009-06-11",
"It is userd to handle the default show peer name in peer marking settings",
"alter table peer_marking_settings modify column show_peer_name int(3) default 0"
);

$sql_course_update[] = array(
"2009-06-11",
"It is userd to handle the time limit of power voice int task",
"alter table task add column limit_voice int(4) default null after sheettype"
);

$sql_course_update[] = array(
"2009-06-19",
"It is userd to handle the marking scheme of online writing int task",
"alter table task add column marking_scheme char(2) default null after sheettype"
);

$sql_course_update[] = array(
"2009-06-09",
"Add Insert/modify by/time to forum table",
"alter table forum add CreateDate datetime default null"
);

$sql_course_update[] = array(
"2009-06-09",
"Add Insert/modify by/time to forum table",
"alter table forum add CreateBy int(8) default null"
);

$sql_course_update[] = array(
"2009-06-09",
"Add Insert/modify by/time to forum table",
"alter table forum add LastModifyDate datetime default null"
);

$sql_course_update[] = array(
"2009-06-09",
"Add Insert/modify by/time to forum table",
"alter table forum add LastModifyBy int(8) default null"
);

$sql_course_update[] = array(
"2009-06-09",
"Add Insert/modify by/time to bulletin table",
"alter table bulletin add LastModifyBy int(8) default null"
);

$sql_course_update[] = array(
"2009-06-09",
"CREATE NOTES_REMARK FOR ECLASS40",
	"CREATE TABLE IF NOT EXISTS `notes_remark` (
  `notes_remark_id` int(8) NOT NULL auto_increment,
  `notes_id` int(8) default NULL,
  `notes_remark_type` int(3) default NULL,
  `content` text,
  `sequence_no` int(3) default NULL,
  `user_id` int(8),
  `inputdate` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`notes_remark_id`),
  KEY `notes_id` (`notes_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"Add user_id to notes_remark table after sequence_no",
"alter table notes_remark add column user_id int(8) after sequence_no"
);


$sql_course_update[] = array(
"2009-06-09",
"It is used to handle assessment in eclass40",
"CREATE TABLE IF NOT EXISTS `assessment` (
  `assessment_id` int(8) NOT NULL auto_increment,
  `category` varchar(255) default NULL,
  `handinby` int(2) default NULL,
  `title` text,
  `instruction` text default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `attachment` text,
  `notification` int(2) default NULL,
  `status` int(2) default NULL,
  `phasetype` int(2) default NULL,
  `publisheddate` datetime default NULL,
  `adjustfinalresult` int(2) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`assessment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle phase in eclass40",
"CREATE TABLE IF NOT EXISTS `phase` (
  `phase_id` int(8) NOT NULL auto_increment,
  `assessment_id` int(8) NOT NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `title` text,
  `description` text,
  `weight` float default NULL,
  `attachment` text,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`phase_id`),
  KEY `KEY_AssessmentID` (`assessment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle task in eclass40",
"CREATE TABLE IF NOT EXISTS `task` (
  `task_id` int(8) NOT NULL auto_increment,
  `phase_id` int(8) NOT NULL,
  `assessment_id` int(8) default NULL,
  `title` text,
  `quiz_id` int(8) default NULL,
  `weight` float default NULL,
  `method` int(2) default NULL,
  `attachment` text,
  `answer` varchar(255) default NULL,
  `answersheet` text,
  `modelanswer` text,
  `sequence_no` int(8) default NULL,
  `status` int(2) default NULL,
  `markingmethodtype` int(3) default NULL,
  `fullmark` float default NULL,
  `passmark` float default NULL,
  `lowestmark` float default NULL,
  `sheettype` int(2) default NULL,
  `marking_scheme` char(2) default NULL,
  `limit_voice` int(4) default null,
  `answer_display_mode` varchar(255) default '1',
  `teacher_weight` float default -1,
  `self_weight` float default -1,
  `peer_weight` float default -1,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `isppractice` int(2) default NULL, 
  `answersheet_status` int(2) default 2, 
  PRIMARY KEY  (`task_id`),
  KEY `key_phase_id` (`phase_id`),
  KEY `key_assessment_id` (`assessment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle standard rubric set in eclass40",
"CREATE TABLE IF NOT EXISTS `standard_rubric_set` (
  `std_rubric_set_id` int(8) NOT NULL auto_increment,
  `set_title` text,
  `description` text,
  `set_type` int(3) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`std_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle standard rubric in eclass40",
"CREATE TABLE IF NOT EXISTS `standard_rubric` (
  `std_rubric_id` int(8) NOT NULL auto_increment,
  `std_rubric_set_id` int(8) NOT NULL,
  `title` text,
  `showorder` int(3) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`std_rubric_id`),
  KEY `key_std_rubric_set_id` (`std_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle standard rubric detail in eclass40",
"CREATE TABLE IF NOT EXISTS `standard_rubric_detail` (
  `std_rubric_detail_id` int(8) NOT NULL auto_increment,
  `std_rubric_id` int(8) NOT NULL,
  `std_rubric_set_id` int(8) NOT NULL,
  `score` float,
  `description` text,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`std_rubric_detail_id`),
  KEY `key_std_rubric_id` (`std_rubric_id`),
  KEY `key_std_rubric_set_id` (`std_rubric_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle task rubric in eclass40",
"CREATE TABLE IF NOT EXISTS `task_rubric` (
  `rubric_id` int(8) NOT NULL auto_increment,
  `task_id` int(8) NOT NULL,
  `phase_id` int(8) NOT NULL,
  `assessment_id` int(8) NOT NULL,
  `title` text,
  `weight` int(3),
  `showorder` int(3),
  `recordkey` varchar(255) default NUll,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`rubric_id`),
  KEY `key_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle task rubric detail in eclass40",
"CREATE TABLE If NOT EXISTS `task_rubric_detail` (
  `rubric_detail_id` int(8) NOT NULL auto_increment,
  `rubric_id` int(8) NOT NULL,
  `task_id` int(8) NOT NULL,
  `phase_id` int(8) NOT NULL,
  `assessment_id` int(8) NOT NULL,
  `score` float,
  `description` text,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`rubric_detail_id`),
  KEY `key_rubric_id` (`rubric_id`),
  KEY `key_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle task rubric scoresheet in eclass40",
"CREATE TABLE IF NOT EXISTS `task_rubric_score_sheet` (
  `scoresheet_id` int(8) NOT NULL auto_increment,
  `task_id` int(8) NOT NULL,
  `phase_id` int(8) default NULL,
  `assessment_id` int(8)default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `marker_user_id` int(8) default NULL,
  `marker_group_id` int(8) default NULL,
  `mark_type` int(3) default NULL,
  `comment` text,
  `totalmark` float default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`scoresheet_id`),
  KEY `key_task_rubric_score_sheet` (`task_id`, `user_id`, `marker_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle task rubric score in eclass40",
"CREATE TABLE IF NOT EXISTS `task_rubric_score` (
  `rubric_score_id` int(8) NOT NULL auto_increment,
  `scoresheet_id` int(8) NOT NULL,
  `task_id` int(8) default NULL,
  `phase_id` int(8)default NULL,
  `assessment_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `rubric_id` int(8) default NULL,
  `score` int(3) default NULL,
  `marker_user_id` int(8) default NULL,
  `marker_group_id` int(8) default NULL,
  `mark_type` int(3) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`rubric_score_id`),
  KEY `key_scoresheet_id` (`scoresheet_id`),
  KEY `key_task_rubric_user_id` (`task_id`,`user_id`,`rubric_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle peer marking settings in eclass40",
"CREATE TABLE IF NOT EXISTS `peer_marking_settings` (
  `peer_settings_id` int(8) NOT NULL auto_increment,
  `task_id` int(8) default NULL,
  `recordkey` varchar(255) default NULL,
  `target` int(3) default NULL,
  `target_num` int(3) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `mark_type` int(3) default NULL,
  `show_peer_name` int(3) default '0',
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`peer_settings_id`),
  KEY `key_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle peer marking grouping in eclass40",
"CREATE TABLE IF NOT EXISTS `peer_marking_grouping` (
  `peer_group_id` int(8) NOT NULL auto_increment,
  `peer_settings_id` int(8) default NULL,
  `task_id` int(8) default NULL,
  `group_iden` varchar(255) default NULL,
  `user_id` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`peer_group_id`),
  KEY `key_peer_settings_id` (`peer_settings_id`),
  KEY `key_task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle peer or self handin marking in eclass40",
"CREATE TABLE IF NOT EXISTS `peer_self_handin_marking` (
  `ps_marking_id` int(8) NOT NULL auto_increment,
  `handin_id` int(8) NOT NULL,
  `grade` varchar(16) default NULL,
  `comment` text,
  `mark_type` int(3) default NULL,
  `marker_id` int(8) default NULL,
  `marker_group_id` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ps_marking_id`),
  KEY `KEY1_peer_self_handin_marking` (`handin_id`,`marker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-09",
"It is used to handle peer marking status in eclass40",
"CREATE TABLE IF NOT EXISTS `peer_self_marking_status` (
  `ps_status_id` int(8) NOT NULL auto_increment,
  `task_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `status` int(3) default NULL,
  `mark_type` int(3) default NULL,
  `mark_num` int(3) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ps_status_id`),
  KEY `KEY1_peer_self_marking_status` (`task_id`,`user_id`),
  KEY `KEY2_peer_self_marking_status` (`task_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);



$sql_course_update[] = array(
"2009-06-10",
"Add NoReply option for topics",
"alter table bulletin add NoReply int (4) default 999"
);

$sql_course_update[] = array(
"2009-06-10",
"Add ForumImage option for topics",
"alter table forum add ForumImage text default NULL"
);



$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"create table chat_message (
message_id int not null primary key AUTO_INCREMENT,
receiver_id int not null,
sender_id int not null,
message text,
message_timestamp datetime not null,
read_flag boolean not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"create index chat_message_idx1 on chat_message (receiver_id, read_flag)"
);

$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"create index chat_message_idx2 on chat_message (receiver_id, sender_id, message_timestamp)"
);


$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"alter table usermaster
add column chat_status char(1) not null DEFAULT 'F' check (chat_status in ('O', 'F')),
add column chat_status_timestamp datetime not null,
add column chat_ping_timestamp datetime not null"
);


$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"create index usermaster_chat_idx1 on usermaster (chat_status_timestamp)"
);

$sql_course_update[] = array(
"2009-06-12",
"Create chat in eclass40",
"create index usermaster_chat_idx2 on usermaster (chat_status, chat_ping_timestamp)"
);


$sql_course_update[] = array(
"2009-06-12",
"Add marking type - to define which result to use as final grade" ,
"alter table quiz add column result_type int(4) default 1 after attempt_limit"
);


$sql_course_update[] = array(
"2009-06-19",
"add table for user star on post",
'create table bulletin_user_star (
  BulletinID int(8) NOT NULL,
  UserID int(8) NOT NULL,
  InputDate datetime,
  PRIMARY KEY (BulletinID,UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

$sql_course_update[] = array(
"2009-06-19",
"alter table bulletin add views field for total view stat",
'alter table bulletin add Views int(10) default 0');

$sql_course_update[] = array(
"2009-06-19",
"add field to identify temp post",
'alter table bulletin add IsTemp int(2) default 0');

$sql_course_update[] = array(
"2009-06-19",
"Add INDEX for bulletin",
'alter table bulletin add KEY ForumIDPostID (forum_id,post_id)');

$sql_course_update[] = array(
"2009-06-19",
"drop old index key for bulletin",
'alter table bulletin drop KEY forum_id');

$sql_course_update[] = array(
"2009-06-19",
"add table for user star on post",
'alter table bulletin drop KEY followup_id');



$sql_course_update[] = array(
"2009-06-22",
"It is used to handle the marking scheme of online wiring  in eclass40" ,
"alter table task add column marking_scheme char(2) default null after sheettype"
);

$sql_course_update[] = array(
"2009-06-22",
"It is used to record the final mark of assessment of user or group in eclass40" ,
"CREATE TABLE IF NOT EXISTS `assessment_final_mark` (
  `assessment_mark_id` int(8) NOT NULL auto_increment,
  `assessment_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `mark` varchar(16),
  `is_final` int(1) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`assessment_mark_id`),
  KEY `KEY1_assessment_final_mark` (`assessment_id`,`user_id`),
  KEY `KEY2_assessment_final_mark` (`assessment_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-22",
"It is used to record the final mark of phase of user or group in eclass40" ,
"CREATE TABLE IF NOT EXISTS `phase_final_mark` (
  `phase_mark_id` int(8) NOT NULL auto_increment,
  `phase_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `mark` varchar(16),
  `is_final` int(1) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`phase_mark_id`),
  KEY `KEY1_phase_final_mark` (`phase_id`,`user_id`),
  KEY `KEY2_phase_final_mark` (`phase_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-25",
"It is used to handle the final result adjustment settings of assessment in eclass40" ,
"alter table assessment add column adjustfinalresult int(2) default NULL after publisheddate"
);

$sql_course_update[] = array(
"2009-06-26",
"Support changing online/offline status for chat",
"ALTER TABLE usermaster
ADD COLUMN chat_status_force_offline BOOLEAN DEFAULT '0' NOT NULL AFTER chat_status_timestamp"
);

$sql_course_update[] = array(
"2009-06-26",
"Add recordkey to recognize quiz for removing unused quiz" ,
"alter table quiz add column recordkey varchar(255) DEFAULT NULL"
);

$sql_course_update[] = array(
"2009-06-26",
"It is used to record the final mark of task of user or group in eclass40" ,
"CREATE TABLE IF NOT EXISTS `task_final_mark` (
  `task_mark_id` int(8) NOT NULL auto_increment,
  `task_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `group_id` int(8) default NULL,
  `mark` varchar(16) default NULL,
  `is_final` int(1) default NULL,
  `createdby` int(8) default NULL,
  `inputdate` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`task_mark_id`),
  KEY `KEY1_task_final_mark` (`task_id`,`user_id`),
  KEY `KEY2_task_final_mark` (`task_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-06-26",
"It is used to handle task release type in eclass40" ,
"alter table task add column answer_display_mode varchar(255) default '1' after limit_voice"
);


$sql_course_update[] = array(
"2009-07-06",
"Create chinesename in eclass40 in usermaster table",
"alter table usermaster
add column chinesename varchar(100) default null"
);

$sql_course_update[] = array(
"2009-07-08",
"Create temp table for assessment report",
"CREATE TABLE IF NOT EXISTS temp_assessment_report (
	user_id int(8),
	class_number varchar(64),
	firstname varchar(100),
	lastname varchar(100),
	chinesename varchar(100),
	assessment_num int(4),
	completed_ontime int(4),
	completed_late int(4),
	notcompleted_inprogress int(4),
	notcompleted_notsubmitted int(4),
	createdby int(8),
	inputdate datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-07-09",
"Support multiple deletions of chat message by sender and receiver",
"alter table chat_message
add column deleted_by varchar(255) null after read_flag"
);

$sql_course_update[] = array(
"2009-07-13",
"For portal outline - remember sequence" ,
"alter table outline add column sequence int(2) default 0"
);


$sql_course_update[] = array(
"2009-07-13",
"Create table for forum statistic",
"CREATE TABLE IF NOT EXISTS `login_forum_usage` (
  `login_forum_usage_id` int(8) NOT NULL auto_increment,
  `login_session_id` int(8) default NULL,
  `forum_id` int(8) default NULL,
  `inputdate` datetime default NULL,
  `memberType` varchar(10) default NULL,
  PRIMARY KEY  (`login_forum_usage_id`),
  UNIQUE KEY (`login_session_id`,`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-07-13",
"Create table for eContent statistic",
"CREATE TABLE IF NOT EXISTS `login_notes_usage` (
  `login_notes_usage_id` int(8) NOT NULL auto_increment,
  `login_session_id` int(8) default NULL,
  `notes_id` int(8) default NULL,
  `inputdate` datetime default NULL,
  `memberType` varchar(10) default NULL,
  PRIMARY KEY  (`login_notes_usage_id`),
  UNIQUE KEY (`login_session_id`,`notes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-07-16",
"Add section_name in login_usage table",
"alter table login_usage
add column section_name varchar(255) default null"
);

$sql_course_update[] = array(
"2009-07-16",
"Add action in login_usage table",
"alter table login_usage
add column action varchar(255) default null"
);

$sql_course_update[] = array(
"2009-07-16",
"Add ordering in marksheet column",
"alter table marksheet_column add column ordering int(4) default null"
);


$sql_course_update[] = array(
"2009-08-04",
"Create Survey Table",
"CREATE TABLE IF NOT EXISTS survey (
  survey_id int(8) NOT NULL auto_increment,
  title varchar(255) default NULL,
  instruction text,
  html_opt char(2) default NULL,
  status char(2) default NULL,
  attachment varchar(255) default NULL,
  answer varchar(255) default NULL,
  deadline datetime default NULL,
  answersheet text,
  modified datetime default NULL,
  inputdate datetime default NULL,
  modelanswer mediumtext default NULL,
  PRIMARY KEY  (survey_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-08-04",
"Storing student survey results",
"CREATE TABLE IF NOT EXISTS surveyee (
  surveyee_id int(8) NOT NULL auto_increment,
  survey_id int(8) default NULL,
  user_id int(8) default NULL,
  filename varchar(255) default NULL,
  filename_mark varchar(255) default NULL,
  answer text,
  grade varchar(10) default NULL,
  comment text,
  inputdate datetime default NULL,
  PRIMARY KEY  (surveyee_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-08-04",
"For portal survey - missing column required to be compantible with assignment table" ,
"alter table survey add column modelanswer mediumtext default NULL"
);

$sql_course_update[] = array(
"2009-08-08",
"It is used to handle the model answer / reference files in a task",
"alter table task add column answer varchar(255) default null after attachment"
);

$sql_course_update[] = array(
"2009-08-10",
"It is used to update the property of a field score in task_rubric_detail",
"alter table task_rubric_detail modify column score float default null"
);

$sql_course_update[] = array(
"2009-08-10",
"It is used to update the property of a field score in standard_rubric_detail",
"alter table standard_rubric_detail modify column score float default null"
);

$sql_course_update[] = array(
"2009-08-12",
"It is used to identify the power practice assignment type in eClass40 after data migration from eClass31",
"alter table task add column isppractice int(2) default null"
);

$sql_course_update[] = array(
"2009-08-13",
"Notes content ",
" CREATE TABLE IF NOT EXISTS `notes_section` (
  `notes_section_id` int(8) NOT NULL auto_increment,
  `notes_id` int(8) default NULL,
  `notes_section_type` int(3) default NULL,
  `content` longblob,
  `sequence_no` int(3) default NULL,
  `url` text,
  `inputdate` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`notes_section_id`),
  KEY `notes_id` (`notes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_update[] = array(
"2009-08-13",
"Notes - Post comment ",
" CREATE TABLE `comment` (
  `comment_id` int(8) NOT NULL auto_increment,
  `notes_id` int(8) default NULL,
  `user_id` int(8) default NULL,
  `content` mediumtext,
  `inputdate` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`comment_id`),
  KEY `notes_id` (`notes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_course_update[] = array(
"2009-08-13",
"It is used to reference to a note in weblink",
"alter table weblink add column notes_id int(8) default NULL after weblink_id"
);

$sql_course_update[] = array(
"2009-08-13",
"It is used to set the type of content",
"alter table weblink add column type int(2) default NULL after url"
);

$sql_course_update[] = array(
"2009-08-13",
"It is used to reference to a note in glossary",
"alter table glossary add column notes_id int(8) default NULL after glossary_id"
);

$sql_course_update[] = array(
"2009-09-08",
"Update schedule venue varchar size",
"alter table schedule modify column venue varchar(128) default null"
);

$sql_course_update[] = array(
"2009-09-08",
"Update schedule nature varchar size",
"alter table schedule modify column nature varchar(128) default null"
);

$sql_course_update[] = array(
"2009-12-10",
"Add a field to handle the answersheet draft logic in task",
"alter table task add column answersheet_status int(2) default 2"
);

$sql_course_update[] = array(
"2010-01-10",
"Add a field to differentiate whether the user is the past student or not",
"ALTER TABLE usermaster ADD COLUMN is_past CHAR(2) DEFAULT NULL"
);

$sql_course_update[] = array(
"2010-01-25",
"add a field to allow storing peer marking sheet in online writing",
"alter table peer_self_handin_marking add column `answer_marked` longblob"
);
#########################################################################
############## END OF COURSE TABLE UPDATE ###############################
#########################################################################


#########################################################################
########################### FOR SPECIAL ROOMS ###########################
#########################################################################

$sql_course_sr1[] = array(
"2004-09-07",
"add a field to store jet points",
"ALTER TABLE reading_report ADD JetPoint int(8)"
);

$sql_course_sr1[] = array(
"2004-09-07",
"add a field to store data for FLK",
"CREATE TABLE IF NOT EXISTS reading_library_record (
  ReadingLibraryRecordID int(8) NOT NULL auto_increment,
  user_id int(8),
 BorrowNum int(2),
  TotalPoint int(4),
  DateSubmission date,
  Semester int(1),
  DateInput datetime,
  PRIMARY KEY (ReadingLibraryRecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr1[] = array(
"2004-09-07",
"add a field to store data for FLK",
"CREATE TABLE IF NOT EXISTS reading_readwrite_record (
  ReadingReadwriteRecordID int(8) NOT NULL auto_increment,
  user_id int(8),
  ReportChiNum int(2),
  ReportEngNum int(2),
  StarSilver int(2),
  StarGolden int(2),
  TotalPoint int(4),
  Semester int(1),
  DateInput datetime,
  PRIMARY KEY (ReadingReadwriteRecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr1[] = array(
"2005-01-18",
"add a field to store teacher name for FLK",
"ALTER TABLE reading_report
  ADD MarkBy varchar(255) "
);




# iPortfolio

$sql_course_sr4[] = array(
"2006-05-30",
"For iPortfolio Template Management",
"ALTER table eclass_file ADD IsSuspend tinyint default 0"
);

$sql_course_sr4[] = array(
"2006-06-16",
"add a table to track notes changes",
"CREATE TABLE IF NOT EXISTS notes_update_track (
  notes_update_id int(8) NOT NULL auto_increment,
  web_portfolio_id int(8),
  school_last_update datetime,
  inputdate datetime,
  PRIMARY KEY (notes_update_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr4[] = array(
"2006-06-20",
"add a table to track notes student changes",
"CREATE TABLE IF NOT EXISTS notes_student_update_track (
  notes_student_update_id int(8) NOT NULL auto_increment,
  web_portfolio_id int(8),
  user_id int(8),
  school_last_update datetime,
  student_last_update datetime,
  inputdate datetime,
  UNIQUE UserPortfolio (web_portfolio_id, user_id),
  PRIMARY KEY (notes_student_update_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr4[] = array(
"2006-07-18",
"add a table for My Friend List in Learning Portfolio",
"CREATE TABLE IF NOT EXISTS user_friend (
  UserFriendID int(8) NOT NULL auto_increment,
  UserID int(8),
  FriendID int(8),
  TimeInput datetime,
  TimeModified datetime,
  UNIQUE UserFriend (UserID, FriendID),
  PRIMARY KEY (UserFriendID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr4[] = array(
"2006-09-14",
"Add a column allow_review for peer review config in publishing learning portfolio",
"ALTER table user_config ADD allow_review tinyint default 0"
);

$sql_course_sr4[] = array(
"2007-01-30",
"add a table for msg_function_group for Learning Portfolio",
"CREATE TABLE IF NOT EXISTS mgt_grouping_function (
  mgt_grouping_function_id int(8) NOT NULL auto_increment,
  group_id int(8) NOT NULL,
  function_right varchar(255),
  UNIQUE (group_id),
  PRIMARY KEY (mgt_grouping_function_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_course_sr4[] = array(
"2007-06-04",
"Add the index web_portfolio_id to the table portfolio_tracking_last",
"ALTER table portfolio_tracking_last add index web_portfolio_id (web_portfolio_id)"
);

$sql_course_sr4[] = array(
"2007-06-04",
"Add the index student_id to the table portfolio_tracking_last",
"ALTER table portfolio_tracking_last add index student_id (student_id)"
);

$sql_course_sr4[] = array(
"2007-06-04",
"Add the index teacher_id to the table portfolio_tracking_last",
"ALTER table portfolio_tracking_last add index teacher_id (teacher_id)"
);


$sql_course_sr4[] = array(
"2008-08-29",
"Set Folder Size of iportfolio scheme",
"alter table web_portfolio add folder_size int(64) "
);

$sql_course_sr4[] = array(
"2009-12-10",
"Sorting SBS records and Weblog records in handin",
"ALTER TABLE handin ADD COLUMN type varchar(4)"
);

# Please add new schema change under CORRESPONDING section!
?>
