<?php
#using: yat

if (!(isset($ck_intranet_justlogin) && $ck_intranet_justlogin != 0))
{
	
      setcookie("ck_intranet_justlogin",1);
      $ck_intranet_justlogin = 1;
}
else
{
    $ck_intranet_justlogin = 0;
}

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");

include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libannounce2007a.php");

intranet_auth();
intranet_opendb();
$memory_init2 = memory_get_usage();



$top_menu_mode = TOP_MENU_MODE_eService;
# Standalone System redirection
if ($eclass_standalone_module == "CampusTV")
{
    header("Location: /home/plugin/campustv/standalone.php");
    exit();
}

# Alumni user redirection
if ($_SESSION['UserType'] == USERTYPE_ALUMNI)
{
    header("Location: $intranet_httppath/home/alumni/");
    exit();
}

include_once($PATH_WRT_ROOT."includes/libportal.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html("");

$lu = new libuser2007($UserID);
$lportal = new libportal();
$li = new libcalevent2007($ts,$v);
$la = new libannounce2007();
$lb = new libfilesystem();
$lcycleperiods = new libcycleperiods();

if ($extra_setting['show_process_time'])
{
	debug("memory used in creating library instances: <font color='yellow' size='3'>".number_format((memory_get_usage()-$memory_init2)/1024)."KB (".number_format((memory_get_usage()-$memory_init2)/1024/1024, 1)."MB)");
}

####################################################################################################
## User Information
####################################################################################################
$name = ($intranet_session_language=="en"? $lu->EnglishName: $lu->ChineseName);

$CurDate = date("Y.m.d (D)");
$CurCycleDate = $lcycleperiods->getCycleDayStringByDate(date("Y-m-d"));

####################################################################################################
## Message of Day [Marquee]
####################################################################################################
# Marquee
$motd = $lb->convertAllLinks($lb->file_read($intranet_root."/file/motd.txt"));
if($motd<>"")
{
	$header_onload_js = "populate();";
	$scroller = "&nbsp;&nbsp;".getMarqueeTicker_20($motd);
	$scrollerTable ="
						<tr>
								<td>
								<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
								<tr>
									<td height=\"5\"><img src=\"/images/2009a/10x10.gif\" width=\"10\" height=\"5\"></td>
								</tr>
								<tr>
									<td height=\"25\" align=\"center\" valign=\"top\">
									<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
									<tr>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_01.gif\" width=\"4\" height=\"4\"></td>
										<td height=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_03.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									<tr>
										<td width=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td bgcolor=\"#FFF299\" class=\"indexscrolltext\" valign=\"middle\" title=\"{$motd}\" >{$scroller}</td>
										<td width=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									<tr>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_07.gif\" width=\"4\" height=\"4\"></td>
										<td height=\"4\" bgcolor=\"#FFF299\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"4\" height=\"4\"></td>
										<td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/scrolltext/board_09.gif\" width=\"4\" height=\"4\"></td>
									</tr>
									</table>
									</td>
								</tr>
								</table>
								</td>
							</tr>
							";
}
	
####################################################################################################
## Build right menu [eClass List (default) / Homework List / Group List]
####################################################################################################
# $ListMenu
include_once("index_right_menu.php");

if ($ListType == 1 && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"])
{
	# Homework List
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	$lh = new libhomework2007();
	$ListContent = $lh->displayIndex($classID,$UserID);
}
else if ($ListType == 2)
{
	# Groups
	$ListContent = $lu->displayGroupPage();
}
else
{
	# eClass
	include_once($PATH_WRT_ROOT."includes/libeclass.php");
	include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
	$le = new libeclass2007();
	
	if ((true) || (!$lu->isParent()))
	{
		$ListContent  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$ListContent .= $le->displayUserEClass12($lu->UserEmail,1);
		$ListContent .= "</table>";
	}
	else
	{
		$ListContent  = "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$ListContent .= "	<tr>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
									<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
								</tr>
							";
		$ListContent .= "</table>";
	}
}


####################################################################################################
## Build shortcut icon array
####################################################################################################
$LogoArr = array();

########################
##### Campus mail / iMail
########################
# check any new mail for both mail (Campus mail / iMail)
if ($special_feature['imail'])
	$iNewCampusMail = $lportal->returnNumNewMessage_iMail($UserID);
else
	$iNewCampusMail = $lportal->returnNumNewMessage($UserID);

$access2campusmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
$access2webmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"];


########################
##### eNotice
########################
if($plugin['notice'])
{
	if(!$lu->isTeacherStaff())
	{
		$unsigned_notice_icon = "";
		if (!$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
		{
		     if ($lu->isParent())
		     {
		         $header_notices_unsigned = $lportal->getParentNoticeUnsignedCount();
		     }
		     else if ($lu->isStudent())
		     {
		          $header_notices_unsigned = $lportal->getStudentNoticeUnsignedCount();
		     }
		     
		     if ($header_notices_unsigned != 0 && $header_notices_unsigned != "")
		     {
				$LogoArr["enotice"] = "
											<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q2','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/notice/'\" > \n
											<tr> \n
												<td width=\"41\" height=\"32\"><a href=\"/home/notice/\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enotice_off.gif\" name=\"q2\" border=\"0\" id=\"q2\"></a></td> \n
												<td width=\"29\" align=\"center\"><a href=\"/home/notice/\" class=\"indexquicklink\">{$header_notices_unsigned}</a></td> \n
											</tr> \n
											<tr> \n
												<td align=\"center\" valign=\"middle\" colspan=\"2\" ><a href=\"/home/notice/\" class=\"indexquickbtn\" >{$ip20_enotice}</a></td> \n
											</tr> \n
											</table> \n
					    					";
		     }
	 	}
	}
}

########################
##### Circular
########################
if($special_feature['circular'])
{
	if($lu->isTeacherStaff())
	{
		$circular_count = $lportal->getUnsignedCircularCount($UserID);
		$circular_path 	= "/home/eService/circular/" . (($circular_count !=0) ? "" : "?past=1");
		if ($circular_count != 0)
		{
			$LogoArr["eCircular"] = "
		                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q2_circular','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_circular_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$circular_path}'\" > \n
									<tr> \n
										<td height=\"32\" width=\"41\"><a href=\"{$circular_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_circular_off.gif\" name=\"q2_circular\" width=\"41\" height=\"32\" border=\"0\" id=\"q2_circular\"></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$circular_path}\" class=\"indexquicklink\">{$circular_count}</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$circular_path}\" class=\"indexquickbtn\" >{$Lang['Header']['Menu']['eCircular']}</a></td> \n
									</tr> \n
									</table> \n
									";
		}
	}
}

########################
##### Group Survey
########################
$new_survey_icon = "";
$survey_count = sizeof($lportal->returnSurvey());
if ($survey_count != 0)
{
   $LogoArr["survey"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_esurvery_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"javascript:newWindow('/home/school/survey/list.php',8)\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"javascript:newWindow('/home/school/survey/list.php',8);\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_esurvery_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"javascript:newWindow('/home/school/survey/list.php',8);\" class=\"indexquicklink\">{$survey_count}</a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"javascript:newWindow('/home/school/survey/list.php',8);\" class=\"indexquickbtn\" >{$i_GroupSettingsSurvey}</a></td> \n
						</tr> \n
						</table> \n
    					";
}

########################
##### Poll
########################
if(!$lu->isParent())
{
	$poll_list = $lportal->returnUnPollingList($UserID);
	$poll_count = sizeof($poll_list);
	if ($poll_count != 0)
	{
		$LogoArr["polling"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q4','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_polling_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/eoffice/polling/index.php'\" > \n

								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"/home/eoffice/polling/index.php\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_polling_blur.gif\" name=\"q4\" width=\"41\" height=\"32\" border=\"0\" id=\"q4\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/eoffice/polling/index.php\" class=\"indexquicklink\">{$poll_count}</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"/home/eoffice/polling/index.php\" class=\"indexquickbtn\" >{$i_adminmenu_polling}</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}

########################
##### Discipline (v12)
########################
if($plugin['Disciplinev12'])
{
	/*
	# iDiscipline Approval alert icon
	if ($header_ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
	{
		$AP_Pending_Record_Number = $header_ldisciplinev12->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING);
		if($AP_Pending_Record_Number != 0)
		{
			$Disciplinev12_path = "/home/admin/disciplinev12/management/award_punishment/";
			
			$LogoArr["eDisciplinev12"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q_disciplinev12','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Disciplinev12_path}'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"{$Disciplinev12_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_discipline_off.gif\" name=\"q3_disciplinev12\" border=\"0\" id=\"q3_disciplinev12\" /></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"{$Disciplinev12_path}\" class=\"indexquicklink\">".$AP_Pending_Record_Number."</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Disciplinev12_path}\" class=\"indexquickbtn\" >{$i_Discipline_System_Approval}</a></td> \n
								</tr> \n
								</table> \n
		    					";
		}
	
	}
	*/
}

########################
##### Sports enrollment
########################
if ($plugin['Sports'])
{
	if($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"] && $lu->isStudent())
	{
		$LogoArr["eSports"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_sport','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sport_enroll_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/sports/'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"/home/sports/\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_sport_enroll_off.gif\" name=\"q3_sport\" border=\"0\" id=\"q3_sport\" /></a></td> \n
									<td width=\"29\" align=\"center\">&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"/home/sports/\" class=\"indexquickbtn\" >{$i_Sports_Day_Enrolment}</a></td> \n
								</tr> \n
								</table> \n
		    					";
	}
}

########################
##### eNews Approval
########################
if($lu->isTeacherStaff() and $_SESSION["SSV_PRIVILEGE"]["adminjob"]["isAnnouncementAdmin"])
{
	$WaitingAnnouncement_count = $lportal->getWaitingAnnouncement($UserID);
	$WaitingAnnouncement_path = "/home/redirect.php?mode=1&url=/home/admin/announcement/";
	if ($WaitingAnnouncement_count != 0)
	{
		$LogoArr["eNewsApproval"] = "
	                           <table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" OnMouseOver=\"this.className='handCursor';MM_swapImage('q_announcement_approve','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_announcement_approve_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$WaitingAnnouncement_path}'\" > \n
								<tr> \n
									<td height=\"32\" width=\"41\"><a href=\"{$WaitingAnnouncement_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_announcement_approve_off.gif\" name=\"q_announcement_approve\" width=\"41\" height=\"32\" border=\"0\" id=\"q_announcement_approve\"></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"{$WaitingAnnouncement_path}\" class=\"indexquicklink\">{$WaitingAnnouncement_count}</a>&nbsp;&nbsp;</td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\"><a href=\"{$WaitingAnnouncement_path}\" class=\"indexquickbtn\" >{$i_eNews_Approval2}</a></td> \n
								</tr> \n
								</table> \n
								";
	}
}
####################################################################################################
## Build shortcut icon array [End]
####################################################################################################
# Build Shourtcut Icons table
$LogoTable = "";
if (is_array($LogoArr) && count($LogoArr)>0)
{
	$CeilCnt = 0;
	$LogoTable .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	foreach ($LogoArr as $Key => $Value)
	{
		if ($CeilCnt%3 == 0)
		{
			$LogoTable .= "<tr>";
		}
		$LogoTable .= "<td align=\"center\" valign=\"top\" >{$Value}</td>";
		if ($CeilCnt%3 == 2)
		{
			$LogoTable .= "</tr>";
		}
		$CeilCnt++;
	}
	$RemainCell = count($LogoArr) %3;
	if ($RemainCell >0)
	{
		for ($i=0;$i<(3-$RemainCell);$i++)
		{
			$LogoTable .= "<td>&nbsp;</td>";
		}
		$LogoTable .= "</tr>";
	}
	$LogoTable .= "</table>";
}

###################################################################
# Events Menu
###################################################################
if ($EventType == "")
{
	$EventType = 0;
}
if ($EventType==0)
{
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_left.gif\" width=\"4\" height=\"22\" /></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_month}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_on_off.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,1)\" class=\"indexeventtitleoff\">{$ip20_event_today}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType(1);
} else {
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_left.gif\" width=\"4\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a  href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,0)\" class=\"indexeventtitleoff\">{$ip20_event_month}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_off_on.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_today}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType(0);
}
###################################################################
# Events Menu [End]
###################################################################


###################################################################
# My Group 
###################################################################
# Get all groups selection
$sql = "SELECT
			a.GroupID, a.Title
		FROM
			INTRANET_GROUP as a, INTRANET_USERGROUP as b
		WHERE
			a.GroupID = b.GroupID AND b.UserID = $UserID
		ORDER BY
			a.RecordType+0, a.Title
	 ";
$result = $la->returnArray($sql,2);
$select_group = getSelectByArray($result,"name='GroupID' onChange=\"jAJAX_GO_CROUP_ANNOUNCE(document.AnnounceForm,this.value)\" class=\"tabletext\" ",$GroupID,1,0, $i_All_MyGroup);
$GroupForm .= $select_group;

# Get my group list
$row = $GroupID != "" ? array($GroupID) : $lu->returnGroupIDs();
$myGroupList = $la->displayGroupAnnounce($row);
###################################################################
# My Group [End]
###################################################################

###################################################################
# Protal TV
###################################################################
$is_portal_tv = false;
if ($plugin['tv'])
{
	$PortalTVinfo = $lportal->returnTVinfo();
	if($PortalTVinfo['live_show'])
	{
		# retrieve live movie
		$movie_url = $lportal->returnLiveURL();
 		$channelID = 0;
		
		if (trim($movie_url)!="")
		{
		        $live_width = $PortalTVinfo['live_width'];
		        $live_height = $PortalTVinfo['live_height'];
		        $live_auto_start = "true";
		        $is_portal_tv = true;
		}
	}
}

if ($is_portal_tv)		# may need to change the if condition
{
	$TVContent ="
		<tr>
			<td valign=\"top\">
				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\">
				<tr>
					<td width=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_01.gif\" width=\"7\" height=\"20\"></td>
					<td background=\"/images/2009a/index/campusTV/tv_board_02.gif\" height=\"20\">
						<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td class=\"indextvtitle\">eTV</td>
							<td align=\"right\"><a href=\"javascript:void(0)\" onclick=\"displayTable('tvContent')\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/arrow_down_off.gif\" name=\"tv_arrow\" width=\"9\" height=\"6\" border=\"0\" id=\"tv_arrow\" onMouseOver=\"MM_swapImage('tv_arrow','','/images/2009a/index/arrow_down_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>
						</tr>
						</table>
					</td>
					<td width=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_03.gif\" width=\"7\" height=\"20\"></td>
				</tr>
				<tr>
					<td width=\"7\" background=\"/images/2009a/index/campusTV/tv_board_04.gif\"><img src=\"/images/2009a/10x10.gif\" width=\"7\" height=\"10\"></td>
					<td align=\"center\" background=\"/images/2009a/index/campusTV/tv_board_05.gif\">
						<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" height=\"100%\">
						<tr id=\"tvContent\">
							<td align=\"center\">
								<!-- TV content //-->
								<div id=\"div_tv_movie_area\" style='z-index:1'></div>
								<!-- TV content [End] //-->
							</td>
						</tr>
						</table>
					</td>
					<td width=\"7\" background=\"/images/2009a/index/campusTV/tv_board_06.gif\"><img src=\"/images/2009a/10x10.gif\" width=\"7\" height=\"10\"></td>
				</tr>
				<tr>
					<td width=\"7\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_07.gif\" width=\"7\" height=\"7\"></td>
					<td background=\"/images/2009a/index/campusTV/tv_board_08.gif\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_08.gif\" width=\"7\" height=\"7\"></td>
					<td width=\"7\" height=\"7\"><img src=\"/images/2009a/index/campusTV/tv_board_09.gif\" width=\"7\" height=\"7\"></td>
				</tr>
				</table>
			</td>
		</tr>
	";
}
###################################################################
# Protal TV [End]
###################################################################
?>
<script><!-- used to enlarge the layout //--></script>
<?
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_header.php");
?>

<script language="Javascript">

top.window.moveTo(0,0);
if (document.all)
{
	top.window.resizeTo(screen.availWidth,screen.availHeight);
}
else if (document.layers||document.getElementById)
{
	if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
	{
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
	}
}
</script>

<script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script>
<script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script>

<script language="javascript">
<!--
var callback_event =
{
	success: function (o)
	{
		jChangeContent( "EventContentDiv", o.responseText );
	}
}
	
var callback_announcement =
{
	success: function (o)
	{
		jChangeContent( "groupwhatnews", o.responseText );
	}
}

var callback_list =
{
	success: function (o)
	{
		jChangeContent( "ListContent", o.responseText );
	}
}

var callback_calender =
{
	success: function (o)
	{
		jChangeContent( "CalContent", o.responseText);
	}
}
	
function jAJAX_GO_EVENT( jFormObject,jParType)
{
	jChangeContent( "EventInboxDiv", "<span class='tabletext' >&nbsp;<?=$ip20_loading?> ...</span>");

	jFormObject.type.value=jParType;
	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_event.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_event);
}

function jAJAX_GO_CROUP_ANNOUNCE( jFormObject, ParGroupID )
{
	jChangeContent( "groupwhatnews", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	jFormObject.GroupID.value=ParGroupID;

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_announcement.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_announcement);
}
	
function jAJAX_GO_HOMEWORK( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_homework.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_ECLASS( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_eclass.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_COMMUNITY( jFormObject)
{
	jChangeContent( "ListContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_community.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_list);
}

function jAJAX_GO_CALENDER( jFormObject, ParV, ParTS )
{
	jChangeContent( "CalContentDiv", "<span class='tabletext' ><?=$ip20_loading?> ...</span>");

	jFormObject.v.value=ParV;
	jFormObject.ts.value=ParTS;

	YAHOO.util.Connect.setForm(jFormObject);
	path = "index20_aj_calender.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_calender);
}
	
function moreNews(type)
{
	newWindow('moreannouncement.php?type='+type,1);
}

function moreGroupNews(type, group)
{
	newWindow('moreannouncement.php?type='+type+'&group='+group,1);
}

function displayMovie(moviid)
{
	$('#div_tv_movie_area').load(
		'aj_tv_retrieve_movie.php',
		function (data){
		});	
}


//-->
</script>

<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="Javascript">

top.window.moveTo(0,0);
if (document.all)
{
	top.window.resizeTo(screen.availWidth,screen.availHeight);
}
else if (document.layers||document.getElementById)
{
	if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
	{
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
	}
}

function jMAX_TABLE()
{
	var tableHeight = parseInt(document.body.clientHeight)-100;
	document.write('<table id="outertable1" width="100%" height="'+tableHeight+'" border="0" cellpadding="0" cellspacing="0"  >');
}

function jMAX_TABLE2()
{
	var tableHeight = parseInt(document.body.clientHeight)-109;
	var tableWidth = parseInt(document.body.clientWidth);
	document.all["outertable1"].style.height = tableHeight;
	document.all["outertable1"].style.width = tableWidth;
}

jMAX_TABLE();

window.onresize = function()
{
	jMAX_TABLE2();
	<?php
	if($motd<>"")
	{
	?>
		marqueewidth = (parseInt(document.body.clientWidth)-500);
		document.getElementById("MovingTextDiv").style.width=marqueewidth;
	<?php
	}
	?>
}

</script>

<tr>
	<td width="210" height="100%" valign="top" style='background:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/leftbg.gif) right'>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0"  >
	<tr>
		<td height="16" align="right" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_top_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_top_corner.gif" width="24" height="16" /></td>
	</tr>
	<tr>
		<td height="100%" valign="top">
		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td height="100%" align="right" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_bg.gif">

			<? ### Calendar & Events ### ?>
			<table height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="right" height="240">
				<? ### Calendar ### ?>
				<table width="193" height="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_01.gif" width="4" height="10" /></td>
					<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_02.gif" width="183" height="10" /></td>
					<td valign="bottom"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_03.gif" width="6" height="10" /></td>
				</tr>
				<tr>
					<td width="4" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_04.gif">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_04.gif" width="4" height="209" />
					</td>
					<td valign="top" bgcolor="#FFFFFF">

					<span id="CalContent"  >
					<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="indexcalendartoday" align="left"><?=$CurDate?></td>
							<td align="right" class="indexcalendarstoday"><?=$CurCycleDate?></td>
						</tr>
						</table>
						<span id="CalContentDiv"  >
						<?php
							echo $li->displayCalendar();
						?>
						</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="bottom" height="100%" >
						<table width="90%" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td width="12"><p><?=$li->displayPrevMonth()?></p></td>
							<td align="center" class="indexcalendarmonth"><?=$li->displayMonthText()?></td>
							<td width="12"><p><?=$li->displayNextMonth()?></p></td>
						</tr>
						</table>
						</td>
					</tr>

					<tr>
						<td align="center" valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
						<tr>
							<td align="center" valign="middle">
							<a href="javascript:fe_view_event_by_type(1)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_academicevent_off.gif" alt="<?=$i_EventTypeString[1]?>" name="ca1" border="0" id="ca1" onMouseOver="MM_swapImage('ca1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_academicevent_on.gif',1)" onMouseOut="MM_swapImgRestore()" /></a>
							</td>
							<td align="center" valign="middle">
							<a href="javascript:fe_view_event_by_type(0)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_schoolevent_off.gif" alt="<?=$i_EventTypeString[0]?>" name="ca2" border="0" id="ca2" onMouseOver="MM_swapImage('ca2','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_schoolevent_on.gif',1)" onMouseOut="MM_swapImgRestore()" /></a>
							</td>
							<td align="center" valign="middle">
							<a href="javascript:fe_view_event_by_type(2)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_holiday_off.gif" alt="<?=$i_EventTypeString[2]?>" name="ca3" border="0" id="ca3" onMouseOver="MM_swapImage('ca3','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_holiday_on.gif',1)" onMouseOut="MM_swapImgRestore()" /></a>
							</td>
							<td align="center" valign="middle">
							<a href="javascript:fe_view_event_by_type(3)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_groupevent_off.gif" alt="<?=$i_EventTypeString[3]?>" name="ca4" border="0" id="ca4" onMouseOver="MM_swapImage('ca4','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_groupevent_on.gif',1)" onMouseOut="MM_swapImgRestore()" /></a>
							</td>
							<td align="center" valign="middle">
							<a href="javascript:showAllEventList()"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_allevent_off.gif" alt="<?=$i_status_all?><?=($intranet_session_language=="en") ? " ": "" ?><?=$i_Events?>" name="ca5" border="0" id="ca5" onMouseOver="MM_swapImage('ca5','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_allevent_on.gif',1)" onMouseOut="MM_swapImgRestore()" /></a>
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table>
					</span>

					<form name="CalForm" >
					<input type="hidden" name="v" value="<?=$v?>" />
					<input type="hidden" name="ts" value="<?=$ts?>" />
					</form>
					</td>
					<td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_06.gif" width="6" height="10" /></td>
				</tr>
			<tr>
				<td width="4" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_07.gif" width="4" height="7" /></td>
				<td valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_08.gif" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_08.gif" width="4" height="7" /></td>
				<td width="6" height="7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/ca_09.gif" width="6" height="7" /></td>
			</tr>
			</table>
			<? ### Calendar End ### ?>
			</td>
		</tr>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5" /></td>
		</tr>
		<tr>
			<td align="right" height="100%" valign="top">
			<span id="EventContentDiv" style="height:100%">
			<table width="193" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="22"><?=$EventMenu?></td>
			</tr>
			<tr>
				<td height="100%"  valign="top" >
				<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
				<tr>
					<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_01.gif" width="5" height="5"></td>
					<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_02.gif" width="5" height="5"></td>
					<td width="7" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_03.gif" width="7" height="5"></td>
				</tr>
				<tr>
					<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_04.gif" width="5" height="5"></td>
					<td valign="top" bgcolor="#FFFFFF" height="100%" >
					<div id="EventInboxDiv" style="width:100%; height:100%; z-index:1; overflow: auto;">
					<?=$EventContent?>
					</div>
					</td>
					<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_06.gif" width="7" height="5"></td>
				</tr>
				<tr>
					<td width="5" height="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_07.gif" width="5" height="8"></td>
					<td height="8" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_08.gif" width="7" height="8"></td>
					<td width="7" height="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/event/board_09.gif" width="7" height="8"></td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</span>

			<form name="EventForm" >
			<input type="hidden" name="type" value="<?=$EventType?>" />
			</form>
			</td>
		</tr>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>
		</table>

		</td>
		<td width="12" height="100%" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_edge_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/left_edge_corner.gif" width="12" height="11"></td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>

	<td valign="top" bgcolor="#FFFFFF" height="100%" >
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="27">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="9"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_left.gif" width="9" height="27" /></td>
			<td align="center" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_bg.gif" class="indexwelcome">
				<?=$ip20_welcome?> , <?php echo (($lu->NickName=="") ? intranet_wordwrap($name,20,"\n",1) : intranet_wordwrap($lu->NickName,20,"\n",1) ); ?>
			</td>
			<td width="9"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/middle_welcome_right.gif" width="9" height="27" /></td>
		</tr>
		</table>
		</td>
	</tr>

	<?=$scrollerTable?>

	<tr>
		<td valign="top" height="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5" /></td>
		</tr>
		<?=$TVContent?>
		<tr>
			<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>
		<tr>
			<td >
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="24">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_01.gif" width="8" height="24"></td>
					<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_02.gif">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="indexnewstitle"><?=$ip20_what_is_new?></td>
						<td align="right">&nbsp;
						</td>
					</tr>
					</table>
					</td>
					<td width="8"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_03.gif" width="8" height="24"></td>
				</tr>
				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td height="100%" >
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_04.gif" width="5" height="5"></td>
				<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_05.gif" width="6" height="5"></td>
				<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_06.gif" width="5" height="5"></td>
			</tr>

			<tr>
				<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif" width="5" height="5"></td>
				<td valign="top" bgcolor="#FFFFFF" height="100%"  >
				<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" align="center" valign="top" height="100%"  >
					<table width="98%" border="0" cellspacing="0" cellpadding="1" height="100%" >
					<tr>
						<td height="2" class="indexnewslisttitle" align='left'>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_left.gif" width="11" height="15" align="absmiddle" />
						 <?=$ip20_public?>
						 <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_right.gif" align="absmiddle" /></td>
					</tr>
					<tr>
						<td height="2" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="2"></td>
					</tr>
					<tr>
						<td height="100%" >
						<div id="whatnews" style="width:100%; height:100%; z-index:1; overflow: auto;">
						<?=$la->displaySchoolAnnouncement($UserID)?>
						</div>
						</td>
					</tr>
					</table>

					</td>
					<td width="5" height="100%"  ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" height="10"></td>
					<td width="1" height="100%"  bgcolor="#E1E1E1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="10"></td>
					<td width="5" height="100%"  ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" height="10"></td>
					<td width="50%" align="center" valign="top" height="100%"  >
	  				<form name="form2" method="get" action="index20.php" >
					<table width="98%" border="0" cellspacing="0" cellpadding="1" height="100%"  >
					<tr>
						<td height="2" class="indexnewslistgrouptitle" align="left">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_left.gif" width="11" height="15" align="absmiddle">
						<?=$ip20_my_group?>
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/icon_arrow_right.gif" width="11" height="15" align="absmiddle">
						<?=$GroupForm?>
						</td>
					</tr>
					<tr>
						<td height="2" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="2"></td>
					</tr>
					<tr>
						<td height="100%" >
						<div id="groupwhatnews" style="width:100%; height:100%; z-index:1; overflow: auto;">
						<?=$myGroupList?>
						</div>
						</td>
					</tr>
					</table>
					</form>
					</td>
				</tr>
				</table>
				</td>
				<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif" width="5" height="5"></td>
			</tr>
			<tr>
				<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_10.gif" width="5" height="6"></td>
				<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif" width="5" height="6"></td>
				<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_12.gif" width="5" height="6"></td>
			</tr>
			</table>
			<form name="AnnounceForm" >
			<input type="hidden" name="GroupID" value="<?=$GroupID?>" />
			</form>

			</td>
		</tr>
		<tr>
			<td height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
		</tr>
		</table>
		</td>
	</tr>
    </table>
    </td>

    <td width="235" valign="top" height="100%"  style='background:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/rightbg.gif) left'>
	<table width="235" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td height="16" align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_top_bg.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_top_corner.gif" width="24" height="16"></td>
    </tr>
	<tr>
		<td valign="top" >
		<table width="235" height="100%" border="0" cellpadding="0" cellspacing="0" valign="top">
		<tr>
			<td width="12" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_edge_bg.gif"><p><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_edge_corner.gif" width="12" height="11"></p> </td>
			<td valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/right_bg.gif">
			<table width="223" height="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="0">
				<?=$LogoTable?>
				</td>
			</tr>
			<tr>
				<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
			</tr>
			<tr>
				<td  width="220" height="100%" valign="top" >
				<span id="ListContent" >
				<table width="220" border="0" cellpadding="0" cellspacing="0" height="100%" >
				<tr>
					<td height="40" width="220" ><span id="ListMenuDiv"><?=$ListMenu?></span></td>
				</tr>
				<tr>
					<td valign="top" width="220" >
					<table width="220" height="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="5" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_01.gif" width="5" height="5"></td>
						<td height="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_02.gif" width="5" height="5"></td>
						<td width="6" height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_03.gif" width="6" height="5"></td>
					</tr>
					<tr>
						<td width="5" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_04.gif" width="5" height="5"></td>
						<td valign="top" bgcolor="#FFFFFF">
						<div id="ListContentDiv" style="width:209px; height:100%; z-index:1; overflow: auto;">
						<?=$ListContent?>
						</div>
						</td>
						<td width="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_06.gif" width="6" height="5"></td>
					</tr>
					<tr>
						<td width="5" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_07.gif" width="5" height="6" /></td>
						<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_08.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_08.gif" width="6" height="6" /></td>
						<td width="6" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/group_tab/board_09.gif" width="6" height="6" /></td>
					</tr>
					</table>
					</span>

					</td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<?php


$NoSpace = 1;
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/home_footer.php");



# popup window
if ($ck_intranet_justlogin)
{
    $popup_file = get_file_content($intranet_root."/file/popupdate.txt");
    $dates = explode("\n",$popup_file);

    if ($dates[0]!= "" && $dates[1] != "")
    {
        $start = strtotime($dates[0]);
        $end = strtotime($dates[1]);
        if ($start != -1 && $end != -1)
        {
            $end += 3600*24;
            $now = time();
            if ($now >= $start && $now < $end)
            {
                if ($HTTP_COOKIE_VARS['ck_intranet_notshowhelp_'.$lu->UserLogin]!=1)
                {
?>
                <script language="javascript" >
                win_size = "resizable,status,top=40,left=40,width=500,height=450";
                newWin = window.open ("/help/", '', win_size);
                </script>
<?php
                }
            }
        }
    }

    # Set up AeroDrive Login
    if (isset($plugin['aerodrive']) && $plugin['aerodrive'])
    {
        $is_student = $lu->isStudent();
        if ($is_student || $lu->isTeacherStaff())
        include_once("plugin/aerodrive/goaero.php");
    }

	?>
	<SCRIPT language=Javascript>
		var intWidth = screen.width;
		var intHeight = screen.height;
		win_size = "top="+(intHeight+30)+",left="+intWidth+",width=220,height=170";
		newWin = window.open ("http://<?=$eclass_httppath?>/src/b2.php", 'back', win_size);
    </SCRIPT>
	<?php
}

intranet_closedb();
?>

<script language="javascript">
<!--
<? if ($is_portal_tv) {?>
displayMovie();
<? } ?>
//-->
</script>



<?
























































































################################################################################################################################################
################################################################################################################################################
################################################################################################################################################
################################################################################################################################################
################################################################################################################################################
################################################################################################################################################
################################################################################################################################################
exit;

include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libcycle.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/librb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

$top_menu_mode = 0;

$benchmark['after include'] = time();

if($plugin['Discipline'])
{
	include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
	$header_ldiscipline = new libdiscipline();
}
if($plugin['Disciplinev12'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$header_ldisciplinev12 = new libdisciplinev12();
}


$lc = new libcycle();
$lr = new librb();
$lclubsenrol = new libclubsenrol();

//$header_lu = new libuser($UserID);
$header_lu = $lu;
$header_lwebmail = new libwebmail();
$access2webmail = $header_lwebmail->hasWebmailAccess($UserID);
$header_lsurvey = new libsurvey();
$header_lteaching = new libteaching();
$header_lnotice = new libnotice();
$header_lcircular = new libcircular();
if($plugin['Sports'])
	$header_lsports = new libsports();
if($plugin['ReportCard'])
	$header_libreportcard = new libreportcard();
$benchmark['after create objects'] = time();


$name = ($intranet_session_language=="en"? $lu->EnglishName: $lu->ChineseName);
$navigation = "";

$laccess = new libaccess($UserID);
$access2campusmail = $laccess->isAccessCampusmail();
$access2resource = $laccess->isAccessResource();

if ($access2resource)
{
    $rbBlock = "<br>";
    $rbBlock .= $lr->displayIndex();
}
$benchmark['after resource booking'] = time();

$body_tags .= " background=/images/index/bg.gif";
$benchmark['b4 header'] = time();

$setTdContentHeight100 = 1;


$benchmark['after header'] = time();

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/


/*
# Check new mails
$access2campusmail = $laccess->isAccessCampusmail();

$newmail_icon = "";
//if ($access2campusmail && (!session_is_registered("iNewCampusMail") || strpos($SCRIPT_NAME,"viewfolder.php")!==false  ) )
if ($access2campusmail)
{
    // Check any new message
    include_once("$intranet_root/includes/libcampusmail.php");
    $header_lc = new libcampusmail();
    if ($special_feature['imail'])
        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
    else
    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);

    session_register("iNewCampusMail");

    # Check preference of check email
    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
    $temp = $header_lc->returnArray($sql,1);
    $optionSkipCheckEmail = $temp[0][0];
    if ($optionSkipCheckEmail)
    {
        $skipCheck = true;
    }
    else
    {
        $skipCheck = false;
    }
    
	//if ($ck_intranet_justlogin && $access2webmail && $header_lwebmail->type==3)
	if (!$skipCheck && $access2webmail && $header_lwebmail->type==3)
	{
	    if ($header_lwebmail->openInbox($header_lu->UserLogin, $header_lu->UserPassword))
	    {
	        $exmail_count = $header_lwebmail->checkMailCount();
	        $header_lwebmail->close();
	        $iNewCampusMail += $exmail_count;
	    }
	}
	# added by Ronald on 20080807 #
    session_unregister("iNewCampusMail");
}

if (($_SESSION["SSV_PRIVILEGE"]["special_feature"]['imail'])  || ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]) || ($access2webmail) || ($access2campusmail))
{
	$mail_count = $iNewCampusMail;
	$mail_path = ($special_feature['imail'] ? "imail":"campusmail");
	
	$mail_href	= $special_feature['imail'] ? "/home/{$mail_path}/" : "javascript:newWindow('{$PATH_WRT_ROOT}home/{$mail_path}/',8)";
	$mail_txt	= $special_feature['imail'] ? $i_CampusMail_New_iMail : $i_adminmenu_sc_campusmail;
	$mail_onclick	= $special_feature['imail'] ? "onclick=\"self.location='/home/{$mail_path}/'\"" : "onclick=\"newWindow('{$PATH_WRT_ROOT}home/{$mail_path}/',8)\"";

	if ($mail_count > 0)
	{
	    $LogoArr["imail"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" {$mail_onclick}  > \n
							<tr> \n
								<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_off.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
								<td width=\"29\" align=\"center\"><a href=\"{$mail_href}\" class=\"indexquicklink\">{$mail_count}</a></td> \n

							</tr> \n
							<tr> \n
								<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"{$mail_href}\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
							</tr> \n
							</table> \n
	    					";
	}
	else
	{

	    $LogoArr["imail"] = "
							<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q1','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" {$mail_onclick}  > \n
							<tr> \n
								<td width=\"41\" height=\"32\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_imail_blur.gif\" name=\"q1\" border=\"0\" id=\"q1\"></a></td> \n
								<td width=\"29\" align=\"center\"><a href=\"{$mail_href}\" class=\"indexquicklink\"></a></td> \n

							</tr> \n
							<tr> \n
								<td align=\"center\" colspan=\"2\" valign=\"middle\"><a href=\"{$mail_href}\" class=\"indexquickbtn\" >{$mail_txt}</a></td> \n
							</tr> \n
							</table> \n
	    					";
	}
}
*/

/*
# eReportCard
if($plugin['ReportCard'])
{
	if($header_lu->isTeacherStaff())
	{
		$PageRight = "TEACHER";
		if($header_libreportcard->hasAccessRight($UserID))
		{
			if($header_libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD() and $header_libreportcard->GET_TEACHER_CONFIRMATION_COUNT() != 0)
			{
				$LogoArr["eReportCard"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_reportcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/admin/reportcard/admin.php'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"/home/admin/reportcard/admin.php\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q3_reportcard\" border=\"0\" id=\"q3_reportcard\" /></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquicklink\">". $header_libreportcard->GET_TEACHER_CONFIRMATION_COUNT() ."</a></td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquickbtn\" >{$eReportCard['MarksheetSubmission']}</a></td> \n
								</tr> \n
								</table> \n
		    					";
			}
		}
	}

	//if($header_lu->isStudent() or $header_lu->isParent())
	if($header_lu->isStudent())
	{
		$PageRight = $header_lu->isStudent() ? "STUDENT" : "PARENT";
		if($header_libreportcard->hasAccessRight($UserID))
		{
			if($header_libreportcard->GET_MARKSHEET_VERIFICATION() and $header_libreportcard->GET_STUDENT_VERIFICATION_COUNT() != 0)
			{
				$LogoArr["eReportCard"] = "
								<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_reportcard','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='/home/admin/reportcard/admin.php'\"  > \n
								<tr> \n
									<td width=\"41\" height=\"32\"><a href=\"/home/admin/reportcard/admin.php\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_reportcard_off.gif\" name=\"q3_reportcard\" border=\"0\" id=\"q3_reportcard\" /></a></td> \n
									<td width=\"29\" align=\"center\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquicklink\">". $header_libreportcard->GET_STUDENT_VERIFICATION_COUNT() ."</a></td> \n
								</tr> \n
								<tr> \n
									<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"/home/admin/reportcard/admin.php\" class=\"indexquickbtn\" >{$eReportCard['MarksheetSubmission']}</a></td> \n
								</tr> \n
								</table> \n
		    					";
			}
		}
	}
}
*/

/*
# added by Ronald On 27 Dec 2007 #
# Checking Single Item Warranty Expiry Date Warning #
if($plugin['Inventory'])
{
	$linventory  = new libinventory();
	if($header_lu->isTeacherStaff())
	{
		$inventory_access_level = $linventory->getAccessLevel();
		//echo $inventory_access_level;
		if(($inventory_access_level == 1) || ($inventory_access_level == 2))
		{
			$warning_day_period = $linventory->getWarrantyExpiryWarningDayPeriod();
			
			if($linventory->IS_ADMIN_USER($UserID))
				$sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
			else
				$sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = $UserID";
				
			$tmp_arr_group = $linventory->returnVector($sql);
			if(sizeof($tmp_arr_group)>0)
			{
				$target_admin_group = implode(",",$tmp_arr_group);
			}
			
			if($target_admin_group != "")
			{
				$cond = " AND c.AdminGroupID IN ($target_admin_group) ";
			}

			$curr_date = date("Y-m-d");
			$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $warning_day_period, date("Y")));

			$sql = "SELECT
							".$linventory->getInventoryItemNameByLang("a.").",
							".$linventory->getInventoryNameByLang("c.").",
							".$linventory->getInventoryNameByLang("d.").",
							b.SupplierName,
							b.SupplierContact,
							b.MaintainInfo,
							b.WarrantyExpiryDate
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '') INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID)
					WHERE
							b.WarrantyExpiryDate BETWEEN '$curr_date' AND '$date_range'
							$cond
					ORDER BY
							b.WarrantyExpiryDate";

			//echo $sql;

			$arr_expiry = $linventory->returnArray($sql,7);

			### Show Warranty Expiry Warning ###
			if(count($arr_expiry)>0)
			{
				?>
				<script language="javascript">
				<!--
					newWindow("admin/inventory/report/item_warranty_expiry_warning.php",4);
				//-->
				</script>
				<?
			}
		}
		
		### Check Write-off approve ###
		if($intranet_inventory_admin || $linventory->IS_GROUP_ADMIN())
		{	
			$sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
			$arr_result = $linventory->returnArray($sql,1);

			### Show Write-off Approval Alert ###
			//if(count($arr_result)>0)
			if($linventory->retrieveWriteOffApprovalNumber()>0)
			{
				$Inventory_path = "admin/inventory/management/write_off_approval/write_off_item.php";
				$LogoArr["eInventory"] = "
									<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_inventory','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_approval_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$Inventory_path}'\"  > \n
									<tr> \n
										<td width=\"41\" height=\"32\"><a href=\"{$Inventory_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_approval_off.gif\" name=\"q3_inventory\" border=\"0\" id=\"q3_inventory\" /></a></td> \n
										<td width=\"29\" align=\"center\"><a href=\"{$Inventory_path}\" class=\"indexquicklink\">".$linventory->retrieveWriteOffApprovalNumber()."</a>&nbsp;&nbsp;</td> \n
									</tr> \n
									<tr> \n
										<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$Inventory_path}\" class=\"indexquickbtn\" >{$i_InventorySystem_Approval}</a></td> \n
									</tr> \n
									</table> \n
			    					";
			}
		}
		### show variance handling notice ###
		if($linventory->retriveVarianceHandlingNotice() > 0)
		{
			$variance_notice_path = "admin/inventory/management/variance_handling_notice/index.php";
			$LogoArr["eInventory_Variance_Handling"] = "
														<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3_inventory','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$variance_notice_path}'\"  > \n
														<tr> \n
															<td width=\"41\" height=\"32\"><a href=\"{$variance_notice_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_inventory_off.gif\" name=\"q3_inventory\" border=\"0\" id=\"q3_inventory\" /></a></td> \n
															<td width=\"29\" align=\"center\"><a href=\"{$variance_notice_path}\" class=\"indexquicklink\">".$linventory->retriveVarianceHandlingNotice()."</a>&nbsp;&nbsp;</td> \n
														</tr> \n
														<tr> \n
															<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\"><a href=\"{$variance_notice_path}\" class=\"indexquickbtn\" >{$i_InventorySystem_Notice}</a></td> \n
														</tr> \n
														</table> \n
								    					";
		}
	}
}
*/

/*
### eEnrollment
if ($plugin['eEnrollment'] and $lclubsenrol->isUseEnrollment() and !$lu->isTeacherStaff())
{
	$ChildArr = array();
	if ($lu->isStudent()) 
	{
		$ChildArr[] = array($UserID);
	}
	else		// parent
	{
		$ChildArr = $lu->getChildrenList();
	}
	$temp_disabled = 0;

	# show icon if within club enrolment period
	if ($lclubsenrol->WITHIN_ENROLMENT_STAGE("club"))
	{
		$disabled = "";	
	}
	else
	{
		$disabled = "disabled";
	}
	
	if ( (($lclubsenrol->enrollPersontype == 1)&&($lu->IsStudent())) || (($lclubsenrol->enrollPersontype == 0)&&($lu->IsParent())) )
		$disabled = "disabled";
			
	$defaultMax = $lclubsenrol->defaultMax;
	
	for($c=0;$c<sizeof($ChildArr);$c++)
	{
		$enrolled_club_number = sizeof($lclubsenrol->STUDENT_ENROLLED_CLUB($ChildArr[$c][0]));
		$enrolled_applied_number = sizeof($lclubsenrol->STUDENT_APPLIED_CLUB($ChildArr[$c][0]));
		$total_club_involved = $enrolled_club_number + $enrolled_applied_number;
		if ($defaultMax==0)
		{
			// no limit => always show icon	
			$temp_disabled += 1;
		}
		else
		{
			$temp_disabled += ($defaultMax-$total_club_involved) ? 1 : 0;
		}
	}
	$disabled = $temp_disabled ? $disabled : "disabled";
		
	$enrol_path = $disabled=="disabled" ? "": "/home/enrollment/index.php";
	$enrol_name = $i_ClubsEnrollment;
	//--- End Club Enrolment Checking
	

	//--- Start Activity Enrolment Checking	
	$in = 0;
	# check activity if club is disabled, otherwise skip activity check so that the icon is linked to the club enrolment page
	if($enrol_path=="" and $disabled=="disabled")
	{
		$disabled = "disabled";
		for($c=0;$c<sizeof($ChildArr);$c++)
		{
			$ApplicantID = $ChildArr[$c][0];
			$temp_disabled = 0;
			$LibUser = new libuser($ApplicantID);
			
			# check if within enrolment activity period
			if ($lclubsenrol->WITHIN_ENROLMENT_STAGE("activity"))
				$disabled = "";			
							
			# check if student has enrolled in any activities
        	$EventArr = $lclubsenrol->GET_AVAILABLE_EVENTINFO_LIST();        	
			for ($i = 0; $i < sizeof($EventArr); $i++) 
			{
				$DataArr['EnrolEventID'] = $EventArr[$i][0];
				$DataArr['StudentID'] = $ApplicantID;
				$tempMark = false;
				if (
					(
						( ($EventArr[$i][15] == "S")&&($LibUser->isStudent()) )||
						( ($EventArr[$i][15] == "P")&&($LibUser->isParent()) )||
						( ($EventArr[$i][15] == "T")&&($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) )||
						( ($EventArr[$i][15] == "P")&&($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) )
					) &&
					(!(!$lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr) && (date("Y-m-d H:i", strtotime($EventArr[$i][14])) < date("Y-m-d H:i"))))
					)
				{
					
				if ($EventArr[$i][2] == $EventArr[$i][1]) {
					// quota display
					if ($EventArr[$i][1] == 0) {
						// normal for apply
						$temp_disabled++;
					}
				} else {
					$tempMark = true;
				}
				
				if ($lclubsenrol->IS_STUDNET_ENROLLED_EVENT($DataArr)) {
					// enrolled event
					$applied = true;
				} else if ($lclubsenrol->IS_STUDNET_APPLIED_EVENT($DataArr)) {
					// applied event
					$TempEnrollStatus = $eEnrollment['front']['applied'];
				} else if ($tempMark) {
					if (
						(!((date("Y-m-d H:i", strtotime($EventArr[$i][13])) <= date("Y-m-d H:i"))&&
						  (date("Y-m-d H:i", strtotime($EventArr[$i][14])) > date("Y-m-d H:i")))) &&
						  !($EventArr[$i][13] == $EventArr[$i][14])
						) 
					{
						//$EnrollStatus = "---";
					} else {
						// normal for apply
						$temp_disabled++;
					}
				}
				
				
			}
		}
			$disabled = $temp_disabled ? "" : $disabled;
			$enrol_path = $disabled ? "": "/home/enrollment/event_index.php";
			$enrol_name = $i_Form_Activity;
		}
	}
	//--- End Activity Enrolment Checking
	
	if(!$disabled and $enrol_path and $lclubsenrol->hasAccessRight($UserID))
	{
   		$LogoArr["eEnrolment"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"self.location='{$enrol_path}'\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$enrol_name}</a></td> \n
						</tr> \n
						</table> \n
    					";
	}
}

if (!$plugin['eEnrollment'] and $plugin['enrollment']and $lclubsenrol->isFillForm() and $lu->isStudent())
{
	$enrol_path = "javascript:newWindow('/home/ecaenrol/',8)";
	$LogoArr["eEnrolment"] = "
						<table width=\"70\" height=\"68\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/btn_bg.gif\" onMouseOver=\"this.className='handCursor';MM_swapImage('q3','','{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onclick=\"newWindow('/home/ecaenrol/',8)\"  > \n
						<tr> \n
							<td width=\"41\" height=\"32\"><a href=\"{$enrol_path}\" ><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/quick_btn/icon_enrollment_off.gif\" name=\"q3\" border=\"0\" id=\"q3\" /></a></td> \n
							<td width=\"29\" align=\"center\"><a href=\"{$enrol_path}\" class=\"indexquicklink\"></a>&nbsp;&nbsp;</td> \n
						</tr> \n
						<tr> \n
							<td align=\"center\" valign=\"middle\" colspan=\"2\" width=\"100%\" ><a href=\"{$enrol_path}\" class=\"indexquickbtn\" >{$i_ClubsEnrollment}</a></td> \n
						</tr> \n
						</table> \n
    					";
} 
*/

/*
# end of checking #
$LogoTable = "";
if (is_array($LogoArr) && count($LogoArr)>0)
{
	$CeilCnt = 0;
	$LogoTable .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	foreach ($LogoArr as $Key => $Value)
	{
		if ($CeilCnt%3 == 0)
		{
			$LogoTable .= "<tr>";
		}
		$LogoTable .= "<td align=\"center\" valign=\"top\" >{$Value}</td>";
		if ($CeilCnt%3 == 2)
		{
			$LogoTable .= "</tr>";
		}
		$CeilCnt++;
	}
	$RemainCell = count($LogoArr) %3;
	if ($RemainCell >0)
	{
		for ($i=0;$i<(3-$RemainCell);$i++)
		{
			$LogoTable .= "<td>&nbsp;</td>";
		}
		$LogoTable .= "</tr>";
	}
	$LogoTable .= "</table>";
}
*/
/***
if ($ListType == 1)
{
	//$ListContent = $lu->displayGroupPage();

	if (!$lh->disabled)
	{
		$lh->field = $field;
		$lh->order = $order;
		$lh->filter = $filter;
		$hwBlock = "<br>";
		$hwBlock .= $lh->displayIndex($classID,$UserID);

		$ListContent = $hwBlock;
	}
	$benchmark['after homework list'] = time();
}
else if ($ListType == 2)
{
	$ListContent = $lu->displayGroupPage();
}
else
{
	if ((true) || (!$lu->isParent()))
	{
		$ListContent  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$ListContent .= $le->displayUserEClass12($lu->UserEmail,1);
		$ListContent .= "</table>";
	}
	else
	{
		$ListContent  = "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$ListContent .= "	<tr>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
									<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
									<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
								</tr>
							";
		$ListContent .= "</table>";
	}

}
$benchmark['eclass'] = time();
***/

/*
if ($EventType == "")
{
	$EventType = 0;
}
if ($EventType==0)
{
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_left.gif\" width=\"4\" height=\"22\" /></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_month}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_on_off.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,1)\" class=\"indexeventtitleoff\">{$ip20_event_today}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType(1);
} else {
	$EventMenu = "
						<table width=\"100%\" height=\"22\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
							<td width=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_left.gif\" width=\"4\" height=\"22\"></td>
							<td align=\"center\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_off_bg.gif\"><a  href=\"javascript:void(0)\" onclick=\"jAJAX_GO_EVENT(document.EventForm,0)\" class=\"indexeventtitleoff\">{$ip20_event_month}</a> </td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_middle_off_on.gif\" width=\"8\" height=\"22\"></td>
							<td align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_bg.gif\" class=\"indexeventtitleon\">{$ip20_event_today}</td>
							<td width=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/tab_on_right.gif\" width=\"8\" height=\"22\"></td>
						</tr>
						</table>
						";
	$EventContent =  $li->displayEventType(0);
}
*/



	



?>
<style type="text/css">
<!--
#calendarpanel
{
	position:absolute;
	z-index:1;
	visibility: visible;
	overflow: hidden;
	width: 193;
	top: 241px;
}
#event
{
	z-index:2;
	overflow: auto;
}

.handCursor {  cursor: hand}
-->
</style>

<script language="Javascript" src='/templates/tooltip.js'></script>

<style type="text/css">
	#ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="javascript">
	isMenu = true;
	isToolTip = true;
</script>
<div id="ToolTip"></div>
<div id="ToolMenu" style="position:absolute; left: 195px; top: 200px; visibility:hidden"></div>

