<?php
// using :
/**
 * Change Log:
 */

// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/json.php");
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$jsonDecoder = new JSON_obj();
// ####### Init END ########

// ####### Access Right START ########
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "settings.customReport";
$CurrentPageName = $Lang['SDAS']['Promotion']['ClassPromotionAssessmentConfig'];
$TAGS_OBJ[] = array(
    $CurrentPageName,
    ""
);
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
// ####### Page Setting END ########

$configInfoArr = $jsonDecoder->decode($libSDAS->GetPromotionConfig());
$CEMLAssoc = $libpf_exam->getSubjectIDWebSAMSMapping();

// ###################### YearForm Start #################################
$sql = "SELECT YearID, YearName, WebSAMSCode FROM YEAR ORDER BY WebSAMSCode, YearName";
$formArr = $libSDAS->returnResultSet($sql);
$formNameAssoc = BuildMultiKeyAssoc($formArr, 'YearID', array(
    'YearName'
), 1);
if (! empty($formArr)) {
    $juniorFormCheckBoxArr = array();
    $seniorFormCheckBoxArr = array();
    $juniorDisplayName = array();
    $seniorDisplayName = array();
    foreach ($formArr as $_formInfo) {
        $_yearId = $_formInfo['YearID'];
        $_yearName = $_formInfo['YearName'];
        $_thisIsJuniorForm = false;
        $_thisIsSeniorForm = false;
        
        // display saved junior form
        if (in_array($_yearId, $configInfoArr['junior']['classLevel'])) {
            $juniorDisplayNameArr[] = $_yearName;
            $_thisIsJuniorForm = true;
        }
        
        // display saved senior form
        if (in_array($_yearId, $configInfoArr['senior']['classLevel'])) {
            $seniorDisplayNameArr[] = $_yearName;
            $_thisIsSeniorForm = true;
        }
        
        // junior form check box group
        $juniorFormCheckBoxArr[] = array(
            'juniorFormCheckBoxGroup_' . $_yearId,
            'juniorFormCheckBoxs[]',
            $_yearId,
            $_thisIsJuniorForm,
            '',
            $_yearName
        );
        
        // senior form check box group
        $seniorFormCheckBoxArr[] = array(
            'seniorFormCheckBoxGroup_' . $_yearId,
            'seniorFormCheckBoxs[]',
            $_yearId,
            $_thisIsSeniorForm,
            '',
            $_yearName
        );
    }
}
// ###################### YearForm End #################################

// ##################### Subject Related Code Start ######################
$sql = "
		SELECT
			ass.RecordID as SubjectID,
            lc.LearningCategoryID,
            if(ass.CH_DES != '' AND ass.CH_DES IS NOT NULL, ass.CH_DES, ass.EN_DES) as subjectChi,
            ass.EN_DES as subjectEn,
            if(lc.NameChi != '' AND lc.NameChi IS NOT NULL, lc.NameChi, lc.NameEng) as categoryChi,
            lc.NameEng as categoryEn
		FROM
			ASSESSMENT_SUBJECT ass
		INNER JOIN
			LEARNING_CATEGORY lc ON lc.LearningCategoryID = ass.LearningCategoryID
		WHERE
			CMP_CODEID IS NULL OR CMP_CODEID = ''
        ORDER BY
            lc.DisplayOrder, ass.DisplayOrder
		";
$subjectArr = $libSDAS->returnResultSet($sql); // subject name and subject category

$subjectCheckBoxDataArr = array();
$coreSubjectNameArr = array();
foreach ($subjectArr as $_subjectInfo) {
    $_subjectId = $_subjectInfo['SubjectID'];
    $_subjectName = Get_Lang_Selection($_subjectInfo['subjectChi'], $_subjectInfo['subjectEn']);
    $_thisSubjectChecked = false;
    
    // For display previous saved core subject
    if (in_array($_subjectId, $configInfoArr['senior']['coreSubject'])) {
        $coreSubjectNameArr[] = $_subjectName;
        $_thisSubjectChecked = true;
    }
    
    // For core subject checkbox group
    $subjectCheckBoxDataArr[] = array(
        'coreSubjectCheckBoxGroup_' . $_subjectId,
        'coreSubjectCheckBoxs[]',
        $_subjectId,
        $_thisSubjectChecked,
        '',
        $_subjectName
    );
}

$subjectNameAssoc = BuildMultiKeyAssoc($subjectArr, array(
    'SubjectID'
), array(
    Get_Lang_Selection('subjectChi', 'subjectEn')
), 1);
$subjectWithCategoryAssoc = BuildMultiKeyAssoc($subjectArr, array(
    Get_Lang_Selection('categoryChi', 'categoryEn'),
    'SubjectID'
), array(
    Get_Lang_Selection('subjectChi', 'subjectEn')
), 1);
$selectInfoAssoc = array_merge(array(
    'Weighting Average' => array(
        '-999' => $Lang['SDAS']['Promotion']['WeightedAverage']
    )
), $subjectWithCategoryAssoc);

// ##################### Subject Related Code End ######################

$ruleType = array(
    'Repeat',
    'TryPromote'
);
$juniorRuleHTMLs = array();
$juniorViewHTMLs = array();
foreach ($ruleType as $_type) {
    for ($i = 1; $i <= 2; $i ++) {
        // for view saved rules
        $__subjectId = $configInfoArr['junior'][$_type][$i]['Subject'];
        $__scoreRequire = $configInfoArr['junior'][$_type][$i]['ScoreRequire'];
        if ($__subjectId == - 999) {
            $__subjectName = $Lang['SDAS']['Promotion']['WeightedAverage'];
        } else {
            $__subjectName = $subjectNameAssoc[$__subjectId];
        }
        
        $juniorViewHTMLs[$_type][$i] = $__subjectName . ' ≤ ' . $__scoreRequire;
        
        // for edit
        $_selectionName = 'rule' . $i . 'Subject_' . $_type;
        $_inputName = 'rule' . $i . 'SubjectScore_' . $_type;
        
        $_html = getSelectByAssoArray($selectInfoAssoc, "id='$_selectionName' name='$_selectionName'", $__subjectId, $all = 0, $noFirst = 1);
        $_html .= " ≤ ";
        $_html .= "<input type='number' class='required' id='$_inputName' name='$_inputName' min='0' max='999' size='3' value='$__scoreRequire'>";
        $juniorRuleHTMLs[$_type][$i] = $_html;
    }
}

// ##Form Btn Start
$htmlAry['editBtn'] = $linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "", 'editBtn', $ParOtherAttribute = "", $OtherClass = "actionBtn View", $ParTitle = '');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "", 'submitBtn', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn Edit");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "", 'cancelBtn', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn Edit");
// ##Form Btn End
intranet_closedb();

// ####### UI START ########
$linterface->LAYOUT_START();
?>
<script>
$( document ).ready(function(){
	// functions 
	var triggerViewMode = function(){
		$('.View').show();	
		$('.Edit').hide();
	}
	var triggerEditMode = function(){
		$('.View').hide();
		$('.Edit').show();
	}
	var submitForm = function(){
		valid = validateForm();
		if(valid){
			$('#form1').submit();
		}
	}
	var validateForm = function(){
		var valid = true; 
		$('.required').each( function() {
			if($(this).val() == ''){
				$(this).focus();
				alert('<?php echo $Lang['SDAS']['Promotion']['WarningEmpty']?>');
				valid = false;
				return false;
			}
		});
		return valid;
	}

	// on ready
	triggerViewMode();
	
	// button event listeners
	$('#editBtn').on('click', function(){
		triggerEditMode();	
	});
	$('#cancelBtn').on('click', function(){
		triggerViewMode();	
	});
	$('#submitBtn').on('click', function(){
		submitForm();	
	});

	// junior senior checkbox event
	$('.juniorFormCheckBoxGroup').click(function(){
		var thisCheckBox = $(this);
		var thisCheckBoxIsChecked = thisCheckBox.prop('checked');
		var thisCheckBoxId = thisCheckBox.val();
		if(thisCheckBoxIsChecked){
			$('#seniorFormCheckBoxGroup_'+thisCheckBoxId).prop('checked', false);
		}else{
			$('#seniorFormCheckBoxGroup_'+thisCheckBoxId).prop('checked', true);
		}
	});
	$('.seniorFormCheckBoxGroup').click(function(){
		var thisCheckBox = $(this);
		var thisCheckBoxIsChecked = thisCheckBox.prop('checked');
		var thisCheckBoxId = thisCheckBox.val();
		if(thisCheckBoxIsChecked){
			$('#juniorFormCheckBoxGroup_'+thisCheckBoxId).prop('checked', false);
		}else{
			$('#juniorFormCheckBoxGroup_'+thisCheckBoxId).prop('checked', true);
		}
	});
	$('#juniorFormCheckBoxGroup_global').click(function(){
		if($('#seniorFormCheckBoxGroup_global').prop('checked') ){
			if( $(this).prop('checked')){
				$('#seniorFormCheckBoxGroup_global').click();
			}
		}
	});
	$('#seniorFormCheckBoxGroup_global').click(function(){
		if($('#juniorFormCheckBoxGroup_global').prop('checked') ){
			if( $(this).prop('checked')){
				$('#juniorFormCheckBoxGroup_global').click();
			}
		}
	});
});
</script>
<form id="form1" method="POST"
	action="?t=reports.promotion.config_update">
	<div class="table_row_tool row_content_tool">
 		<?php echo $htmlAry['editBtn']?>
	</div>
	<p class="spacer"></p>
	<div>
		<?php echo $linterface->Get_Form_Sub_Title_v30($Lang['SDAS']['Promotion']['JuniorAndSeniorFormConfigration'])?>
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion']['JuniorForm'] ?></td>
				<td>
					<div class='View'><?php echo implode(', ', $juniorDisplayNameArr)?></div>
					<div class='Edit'><?php echo $linterface->Get_Checkbox_Table('juniorFormCheckBoxGroup', $juniorFormCheckBoxArr, $itemPerRow=5, false)?></div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion']['SeniorForm'] ?></td>
				<td>
					<div class='View'><?php echo implode(', ', $seniorDisplayNameArr)?></div>
					<div class='Edit'><?php echo $linterface->Get_Checkbox_Table('seniorFormCheckBoxGroup', $seniorFormCheckBoxArr, $itemPerRow=5, false)?></div>
				</td>
			</tr>
		</table>
	</div>
	<div>
		<?php echo $linterface->Get_Form_Sub_Title_v30($Lang['SDAS']['Promotion']['JuniorFormPromotionCriteria'])?>
		<table class="form_table_v30">
    		<?php foreach ($ruleType as $_type):?>
        		<?php for($i=1; $i<=2; $i++):?>
        		<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion'][$_type].$Lang['SDAS']['Promotion']['Rule'].$i ?></td>
				<td>
					<div class='View'>
            				<?php echo $juniorViewHTMLs[$_type][$i]?>
            			</div>
					<div class='Edit'>
            				<?php echo $juniorRuleHTMLs[$_type][$i]?>
            			</div>
				</td>
			</tr>
        		<?php endfor;?>
    		<?php endforeach;?>
		</table>
	</div>
	<div>
		<?php echo $linterface->Get_Form_Sub_Title_v30($Lang['SDAS']['Promotion']['SeniorFormPromotionCriteria'])?>
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion']['CoreSubject'] ?></td>
				<td>
					<div class='View'><?php echo implode(', ', $coreSubjectNameArr)?></div>
					<div class='Edit'><?php echo $linterface->Get_Checkbox_Table('coreSubjectCheckboxGroup',$subjectCheckBoxDataArr,5,false);?></div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion']['ElectiveSubject'] ?></td>
				<td><?php echo $Lang['SDAS']['Promotion']['SubjectsThatIsNotCoreAreElectives'] ?></td>
			</tr>
			<tr>
				<td class="field_title">
    				<?php echo $Lang['SDAS']['Promotion']['WeightedAverageCEML'].' '.$Lang['SDAS']['Promotion']['Weighting'] ?></td>
				</td>
				<td>
					<div class="View">
						<table>
							<tr>
								<td><?php echo $Lang['SDAS']['Promotion']['Chinese'] ?></td>
								<td><?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['080']] ?>%</td>
							</tr>
							<tr>
								<td><?php echo $Lang['SDAS']['Promotion']['English'] ?></td>
								<td><?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['165']] ?>%</td>
							</tr>
							<tr>
								<td><?php echo $Lang['SDAS']['Promotion']['Maths'] ?></td>
								<td><?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['22S']] ?>%</td>
							</tr>
							<tr>
								<td><?php echo $Lang['SDAS']['Promotion']['LS'] ?></td>
								<td><?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['265']] ?>%</td>
							</tr>
						</table>
					</div>
					<div class="Edit">
						<?php echo $Lang['SDAS']['Promotion']['Chinese'] ?>	&nbsp;<input
							type='number' class='required' id='Chinese_Weighting'
							name='Chinese_Weighting' min='0' max='100' size='3'
							value='<?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['080']] ?>'>
						%</br>
						<?php echo $Lang['SDAS']['Promotion']['English'] ?>	&nbsp;<input
							type='number' class='required' id='English_Weighting'
							name='English_Weighting' min='0' max='100' size='3'
							value='<?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['165']] ?>'>
						%</br>
						<?php echo $Lang['SDAS']['Promotion']['Maths'] ?>	&nbsp;<input
							type='number' class='required' id='Math_Weighting'
							name='Math_Weighting' min='0' max='100' size='3'
							value='<?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['22S']] ?>'>
						%</br>
						<?php echo $Lang['SDAS']['Promotion']['LS'] ?>		&nbsp;<input
							type='number' class='required' id='LS_Weighting'
							name='LS_Weighting' min='0' max='100' size='3'
							value='<?php echo $configInfoArr['senior']['CEMLWeighting'][$CEMLAssoc['265']] ?>'>
						%
					</div>
				</td>
			</tr>
    		<?php foreach ($ruleType as $_type):?>
    		<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion'][$_type].$Lang['SDAS']['Promotion']['Rule'].'1' ?></td>
				<td>
					<div class='View'>
        				<?php echo $configInfoArr['senior'][$_type]['electives']['numOfElectives']?>
        				<?php echo $Lang['SDAS']['Promotion']['ElectiveSubject']?> ≤
        				<?php echo $configInfoArr['senior'][$_type]['electives']['scoreRequirement']?>
    				</div>
					<div class='Edit'>
						<input type='number' class='required'
							id='NumOfElective_<?php echo $_type?>'
							name='NumOfElective_<?php echo $_type?>' min='1' max='9' size='1'
							value='<?php echo $configInfoArr['senior'][$_type]['electives']['numOfElectives']?>'>
        				<?php echo $Lang['SDAS']['Promotion']['ElectiveSubject']?>
        				≤
        				<input type='number' class='required'
							id='ElectiveScore_<?php echo $_type?>'
							name='ElectiveScore_<?php echo $_type?>' min='0' max='999'
							size='3'
							value='<?php echo $configInfoArr['senior'][$_type]['electives']['scoreRequirement']?>'>
					</div>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?php echo $Lang['SDAS']['Promotion'][$_type].$Lang['SDAS']['Promotion']['Rule'].'2' ?></td>
				<td>
					<div class='View'>
        				<?php echo $Lang['SDAS']['Promotion']['WeightedAverageCEML']?>
        				≤ 
        				<?php echo $configInfoArr['senior'][$_type]['CEMLScore']?>
        			</div>
					<div class='Edit'>
        				<?php echo $Lang['SDAS']['Promotion']['WeightedAverageCEML']?>
        				≤ 
        				<input type='number' class='required'
							id='CEML_<?php echo $_type?>' name='CEML_<?php echo $_type?>'
							min='0' max='999' size='3'
							value='<?php echo $configInfoArr['senior'][$_type]['CEMLScore']?>'>
					</div>
				</td>
			</tr>
    		<?php endforeach;?>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?php echo $htmlAry['submitBtn'].'&nbsp;'.$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
