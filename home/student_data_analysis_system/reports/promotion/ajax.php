<?php
// ini_set('display_errors',1);
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/promotionAssessment/promotionAssessment.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$jsonDecoder = new JSON_obj();

// handling POST
$academicYearId = $_POST['FromAcademicYearID'];
$yearId = $_POST['YearID'];
$yearType = $_POST['YearType'];
$termAssessment = 'NULL';
$termId = '';
if(preg_match('/(T\d{1}A\d{1})/' , $_POST['YearTermID'])){
	// regx TxAx Term Assessment
	$termAssessment = $_POST['YearTermID'];
}else{
	$termId = $_POST['YearTermID'];
}

# Get Report Config
$configInfoArr = $jsonDecoder->decode($libSDAS->GetPromotionConfig());
if($yearType == 'J'){
	$yearMode = 'junior';
}else if($yearType == 'S'){
	$yearMode = 'senior';
}else{
	$yearMode = '';
}
$andOr = $_POST['andOr'][$yearMode];

# use UI provided rule to overwrite saved config
$currentConfig = $configInfoArr[$yearMode];
if($yearMode == 'junior'){
    // repeat
    $currentConfig['Repeat'][1]['Subject'] = $_POST['rule1Subject_Repeat'];
    $currentConfig['Repeat'][1]['ScoreRequire'] = $_POST['rule1SubjectScore_Repeat'];
    $currentConfig['Repeat'][2]['Subject'] = $_POST['rule2Subject_Repeat'];
    $currentConfig['Repeat'][2]['ScoreRequire'] = $_POST['rule2SubjectScore_Repeat'];
    // try promote
    $currentConfig['TryPromote'][1]['Subject'] = $_POST['rule1Subject_TryPromote'];
    $currentConfig['TryPromote'][1]['ScoreRequire'] = $_POST['rule1SubjectScore_TryPromote'];
    $currentConfig['TryPromote'][2]['Subject'] = $_POST['rule2Subject_TryPromote'];
    $currentConfig['TryPromote'][2]['ScoreRequire'] = $_POST['rule2SubjectScore_TryPromote'];
}else{
    // repeat
    $currentConfig['Repeat']['CEMLScore'] = $_POST['CEML_Repeat'];
    $currentConfig['Repeat']['electives']['numOfElectives'] = $_POST['NumOfElective_Repeat'];
    $currentConfig['Repeat']['electives']['scoreRequirement'] = $_POST['ElectiveScore_Repeat'];
    // try promote
    $currentConfig['TryPromote']['CEMLScore'] = $_POST['CEML_TryPromote'];
    $currentConfig['TryPromote']['electives']['numOfElectives'] = $_POST['NumOfElective_TryPromote'];
    $currentConfig['TryPromote']['electives']['scoreRequirement'] = $_POST['ElectiveScore_TryPromote'];
}
$promotionAssessment = new promotionAssessment($currentConfig, $yearMode, $andOr);

$forStatSubjectIds = array();
if($yearMode == 'junior'){
    $forStatSubjectIds[] = $_POST['rule1Subject_Repeat'];
    $forStatSubjectIds[] = $_POST['rule2Subject_Repeat'];
    $forStatSubjectIds[] = $_POST['rule1Subject_TryPromote'];
    $forStatSubjectIds[] = $_POST['rule2Subject_TryPromote'];
    $forStatSubjectIds = array_unique($forStatSubjectIds);
    sort($forStatSubjectIds);
}else{
    $forStatSubjectIds[] = 'CEML';
}

# get Students 
$libclass = new libclass();
$classArr = $libpf_exam->getClassListByAcademicYearIDAndYearID($academicYearId, $yearId);
$studentArr = $libclass->getStudentByClassId(Get_Array_By_Key($classArr, 'YearClassID'));
$studentIdArr = Get_Array_By_Key($studentArr, 'UserID');
$studentInfoAssoc = BuildMultiKeyAssoc($libpf_exam->getStudentsInfoByID($studentIdArr, $academicYearId),'UserID' );

# get Student result
$resultArr = $libpf_exam->getSchoolExamData($academicYearId, true, $termId, $termAssessment, $studentIdArr);
$studentResultAssoc =  BuildMultiKeyAssoc($resultArr, array('StudentID', 'SubjectID'));
$studentsResultScoreAssoc = BuildMultiKeyAssoc($resultArr, array('StudentID', 'SubjectID'), array('Score'), 1);
$overallResultAssoc = BuildMultiKeyAssoc($libpf_exam->getSchoolExamOverallData($academicYearId, $termId, $termAssessment, $studentIdArr),'StudentID' );
foreach($overallResultAssoc as $_studentId => $_studentOverallResult){
    // put overall score for junior form into Result Assoc    
    $studentsResultScoreAssoc[$_studentId][-999] = $_studentOverallResult['Score'];
}

// calculate CEML score for senior form into Result Assoc
if($yearMode == 'senior'){
    foreach($studentsResultScoreAssoc as $_studentId => $_studentSubjectScores){
        $studentsResultScoreAssoc[$_studentId]['CEML'] = $promotionAssessment->calculateCEMLScore($_studentSubjectScores, $currentConfig['CEMLWeighting']);
    }
}

$forStatsSubjectScoreAssoc = array(); 
$studentScoreDisplayAssoc = array();
foreach($studentsResultScoreAssoc as $_studentId => $_studentResultAssoc){
    foreach($_studentResultAssoc as $_subjectId => $_subjectScore){
        if($_subjectScore > -1){
            $studentScoreDisplayAssoc[$_studentId][$_subjectId] = $_subjectScore;
        }else{
            if($studentResultAssoc[$_studentId][$_subjectId]['Grade'] != -1 
                && $studentResultAssoc[$_studentId][$_subjectId]['Grade'] != ''){
                $studentScoreDisplayAssoc[$_studentId][$_subjectId] = $studentResultAssoc[$_studentId][$_subjectId]['Grade'];
            }else{
                $studentScoreDisplayAssoc[$_studentId][$_subjectId] = '';
            }
        }
        
        // for selected subjects stats
        if(in_array($_subjectId, $forStatSubjectIds) && $_subjectScore > -1){
            $forStatsSubjectScoreAssoc[$_subjectId][] = $_subjectScore;
        }
    }
}

// calculate subjects stats
$subjectStatAssoc = array();
$subjectStatTypeArr = array(
    'average',
    'min',
    'percentile5',
    'percentile10',
    'percentile60'
);
if(count($forStatSubjectIds) > 0){
    foreach($forStatsSubjectScoreAssoc as $_subjectId => $_subjectScoreArr){
        if(count($_subjectScoreArr) > 0){
            $subjectStatAssoc[$_subjectId]['average'] = number_format(array_sum($_subjectScoreArr)/count($_subjectScoreArr), 2);
            $subjectStatAssoc[$_subjectId]['min'] = min($_subjectScoreArr);
            $subjectStatAssoc[$_subjectId]['percentile5'] = $libpf_exam->calculatePercentile(5, $_subjectScoreArr);
            $subjectStatAssoc[$_subjectId]['percentile10'] = $libpf_exam->calculatePercentile(10, $_subjectScoreArr);
            $subjectStatAssoc[$_subjectId]['percentile60'] = $libpf_exam->calculatePercentile(60, $_subjectScoreArr);
        }
    }
    $subjectStatColumnWidth = 80 / count($forStatSubjectIds).'%';
}

# get Subjects
$displaySubjectArr = array_unique(Get_Array_By_Key($resultArr, 'SubjectID' ));
$displaySubjectNameAssoc = BuildMultiKeyAssoc($libpf_exam->getSubjectCodeAndID($displaySubjectArr), 'SubjectID', array(Get_Lang_Selection('CH_SNAME','EN_SNAME')), 1 );
$allSubjectAssoc = BuildMultiKeyAssoc($libpf_exam->getSubjectCodeAndID(), 'SubjectID', array(Get_Lang_Selection('CH_SNAME','EN_SNAME')), 1 );
$allSubjectAssoc[-999] = $Lang['SDAS']['Promotion']['WeightedAverage'];
$allSubjectAssoc['CEML'] = $Lang['SDAS']['Promotion']['WeightedAverageCEML'];

# check each student meet requirement
if(!empty($studentsResultScoreAssoc)){
    $reportDataAssoc = array();
    $reportDataAssoc['All']['Repeat'] = array();
    $reportDataAssoc['All']['TryPromote'] = array();
    $reportDataAssoc['All']['Pass'] = array();
    foreach($studentsResultScoreAssoc as $_userId => $_subjectResultArr){
        $meetRepeat = $promotionAssessment->checkRequirement($_subjectResultArr, 'Repeat');
        $meetTryPromote = $promotionAssessment->checkRequirement($_subjectResultArr, 'TryPromote');
        
        $_thisStudentClass = $studentInfoAssoc[$_userId]['ClassName'];
        
        if ($meetRepeat){
            $reportDataAssoc['All']['Repeat'][] = $_userId;
            $reportDataAssoc[$_thisStudentClass]['Repeat'][] = $_userId;
        } elseif ($meetTryPromote){
            $reportDataAssoc['All']['TryPromote'][] = $_userId;
            $reportDataAssoc[$_thisStudentClass]['TryPromote'][] = $_userId;
        } else {
            $reportDataAssoc['All']['Pass'][] = $_userId;
            $reportDataAssoc[$_thisStudentClass]['Pass'][] = $_userId;
        }
    }
    
    $resultTypeArr = array('Repeat', 'TryPromote', 'Pass');
    $columnWidth = 80/ (count($displaySubjectNameAssoc)+1).'%';
    
    // prepare stat
    $resultStatAssoc = array();
    foreach($reportDataAssoc as $_classType => $_studentArr){
        foreach($resultTypeArr as $_resultType){
            $resultStatAssoc[$_classType][$_resultType] = count($_studentArr[$_resultType]);
        }
    }
    foreach($reportDataAssoc as $_classType => $_studentArr){
        $resultStatAssoc[$_classType]['Total'] = array_sum($resultStatAssoc[$_classType]);
    }
}

?>
<?php if(!empty($studentsResultScoreAssoc)):?>
	<?php echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['Promotion']['Stats']) ?>
	<div class="chart_tables">
        <table class="common_table_list_v30 view_table_list_v30">
        	<thead>
        		<tr>
        			<th></th>
        			<?php foreach($resultStatAssoc as $_class => $_classStatAssoc):?>
        				<th><?php echo ($_class=='All' ? $Lang['SDAS']['WholeForm'] : $_class )?></th>
        			<?php endforeach;?>
        		</tr>
        	</thead>
        	<tbody>
        		<?php foreach($resultTypeArr as $_resultType): ?>
            		<tr>
    					<td width="20%"><?php echo $Lang['SDAS']['Promotion']['Result'][$_resultType] ?></td>
    					<?php foreach($resultStatAssoc as $_class => $_classStatAssoc):?>
    						<td><?php echo $_classStatAssoc[$_resultType] ?></td>
    					<?php endforeach;?>		
        			</tr>
        		<?php endforeach;?>
        		<tr>
        			<td><?php echo $Lang['SDAS']['DSEstat']['Totalnumber']?></td>
        			<?php foreach($resultStatAssoc as $_class => $_classStatAssoc):?>
        				<td><?php echo $_classStatAssoc['Total'] ?></td>
        			<?php endforeach;?>
        		</tr>
        	</tbody>
        </table>
	</div>
	
	<?php echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['Promotion']['SubjectPerformance']) ?>
	<div class="chart_tables">
        <table class="common_table_list_v30 view_table_list_v30">
        	<thead>
        		<tr>
        			<th width="20%"></th>
        			<?php foreach($forStatSubjectIds as $_subjectId):?>
        				<th width="<?php echo $subjectStatColumnWidth?>"><?php echo $allSubjectAssoc[$_subjectId]?></th>
        			<?php endforeach;?>
        		</tr>
        	</thead>
        	<tbody>
        		<?php foreach($subjectStatTypeArr as $_subjectStatType): ?>
            		<tr>
    					<td><?php echo $Lang['SDAS']['Promotion']['SubjectStatsType'][$_subjectStatType] ?></td>
    					<?php foreach($forStatSubjectIds as $_subjectId):?>
    						<td><?php echo Get_String_Display($subjectStatAssoc[$_subjectId][$_subjectStatType]) ?></td>
    					<?php endforeach;?>		
        			</tr>
        		<?php endforeach;?>
        	</tbody>
        </table>
	</div>
		
    <?php foreach($resultTypeArr as $_resultType): ?>
    	<?php echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['Promotion']['Result'][$_resultType]) ?>
    	<div class="chart_tables">
            <table class="common_table_list_v30 view_table_list_v30 dataTable">
            	<thead>
            		<tr>
                		<th width="5%"><?php echo $Lang['SDAS']['Class']?></th>
                		<th width="5%"><?php echo $Lang['SDAS']['ClassNumber']?></th>
                		<th width="10%"><?php echo $Lang['SDAS']['StudentName']?></th>
                		<?php foreach($displaySubjectNameAssoc as $_subjectId => $_subjectName): ?>
                			<th width="<?php echo $columnWidth?>"><?php echo $_subjectName?></th>
                		<?php endforeach;?>
                		<?php if($yearMode == 'junior'):?>
                			<th width="<?php echo $columnWidth?>"><?php echo $Lang['SDAS']['Promotion']['WeightedAverage']?></th>
                		<?php elseif($yearMode == 'senior'):?>
                			<th width="<?php echo $columnWidth?>"><?php echo $Lang['SDAS']['Promotion']['WeightedAverageCEML']?></th>
                		<?php endif;?>
            		</tr>
            	</thead>
            	<tbody>
            		<?php foreach($reportDataAssoc['All'][$_resultType] as $_studentId):?>
                		<tr>
                			<td><?php echo $studentInfoAssoc[$_studentId]['ClassName']?></td>
                			<td><?php echo $studentInfoAssoc[$_studentId]['ClassNumber']?></td>
                			<td><?php echo $studentInfoAssoc[$_studentId]['Name']?></td>
                			<?php foreach($displaySubjectNameAssoc as $_subjectId => $_subjectName): ?>
                				<td><?php echo Get_String_Display($studentScoreDisplayAssoc[$_studentId][$_subjectId]) ?></td>
                			<?php endforeach;?>
                			<?php if($yearMode == 'junior'):?>
                				<td><?php echo Get_String_Display($studentScoreDisplayAssoc[$_studentId]['-999']) ?></td>
                			<?php elseif($yearMode == 'senior'):?>
                				<td><?php echo Get_String_Display($studentScoreDisplayAssoc[$_studentId]['CEML']) ?></td>
                			<?php endif;?>
                		</tr>
            		<?php endforeach;?>	
            	</tbody>
            </table>
        </div>
    <?php endforeach;?>
<?php else:?>
	<div style="text-align:center;"><?php echo $Lang['SDAS']['NoRecord']['A']?></div>
<?php endif;?>