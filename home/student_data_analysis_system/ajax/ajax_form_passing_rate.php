<?php
/*
 * Change Log:
 * Date:	2017-10-31	Omas
 * -	fix #U128910, revise $data using BuildMultiKeyAssoc
 * Date:	2017-07-06	Villa
 * -	Improve percentage to decimal 2
 * -	Support Whole Year
 * Date:	2017-05-31	Villa
 * -	Add thead to fix css
 * Date:	2017-05-25	Villa
 * -	Open the File
 */
$libSDAS = new libSDAS();

##Access Right Start
if(!$plugin['SDAS_module']['customReport']['kcmc_form_passing_rate']){
	echo 'Access Denied';
	return false;
}
##Access Right END

#Form Filter Start
$ClassLevelID_sql = implode(',',(array)$Form);
#Form Filter END

#YearTerm Filter Start
#YearTerm Mapping;
$sql = 'Select * FROM ACADEMIC_YEAR_TERM ORDER BY TermStart';
$rs = $libSDAS->returnResultSet($sql);
$i = 1;
foreach ((array)$rs as $_rs){
	$rs2[$_rs['AcademicYearID']][] = $_rs;
}
foreach ((array)$rs2 as $acdemicYearID=>$_rs2){
	foreach((array)$_rs2 as $key=>$__rs2){
		$semSort = $key+1;
		$YearTermMapping[$semSort][$acdemicYearID] = $__rs2;
	}
}
#Get the YearTermID filter by semsort and AcademicYear ID
$YearTerm = explode('_',$YearTerm);
$YearTerm_Semester = $YearTerm[0];
if($YearTerm_Semester){
	//Do nothing
	$IsAnnual = false;
}else{
	$IsAnnual = true;
	$YearTerm_Semester = '1';
	//set YearTerm => First Term As Reference
}
foreach ((array)$YearTermMapping[$YearTerm_Semester] as $acdemicYearID=>$_rs){
	if(in_array($acdemicYearID, (array)$AcademicYear)){
		$YearTermID[] = $_rs['YearTermID'];
	}
}
$YearTermID_sql = implode(',',(array)$YearTermID);
if($IsAnnual){
	$YearTerm_Cond = ' AND assm.IsAnnual = "1"';
}else{
	$YearTerm_Cond = ' AND assm.YearTermID in ('.$YearTermID_sql.')';
}

$YearTerm_Assessment = $YearTerm[1];

if($YearTerm_Assessment){
	$YearTerm_Assessment_Cond = ' AND TermAssessment = "'.$YearTerm_Assessment.'"';
}else{
	$YearTerm_Assessment_Cond = ' AND TermAssessment = "0"';
}
#YearTerm Filter END


##Data Getting Start
$sql = '
		Select
			assm.*, CONCAT(SubjectID,"_",SubjectComponentID) as SubjectID_SubjectComponentID, if(yc.ClassTitleEN IS NULL,"WholeForm",yc.ClassTitleEN) as ClassTitleEN, yc.ClassTitleB5
		From
			'.$eclass_db.'.ASSESSMENT_SUBJECT_SD_MEAN assm
		LEFT JOIN
			YEAR_CLASS yc
				ON yc.YearClassID = assm.YearClassID
		WHERE
			assm.ClassLevelID in ('.$ClassLevelID_sql.')
			'.$YearTerm_Cond.'
			'.$YearTerm_Assessment_Cond.'
			AND SubjectID != 0
		ORDER BY
			SubjectID, SubjectComponentID, YearTermID
		';
$raw_Data = $libSDAS->returnResultSet($sql);

if(empty($raw_Data)){
	$HTML = '<table style="width:100%;" class="common_table_list_v30 view_table_list_v30" >';
	$HTML .= '<tr>';
	$HTML .= '<td style="text-align: center;">'.$Lang['SDAS']['NoRecord']['A'] .'</td>';
	$HTML .= '</tr>';
	$HTML .= '</table>';
	echo $HTML;
	return false;
}

if($GroupBy=='subject'){
	$index_array = array('SubjectID_SubjectComponentID','AcademicYearID','ClassLevelID','ClassTitleEN');
}elseif($GroupBy=='class'){
	$index_array = array('ClassLevelID','SubjectID_SubjectComponentID','ClassTitleEN','AcademicYearID');
}
// foreach ((array)$raw_Data as $_raw_Data){
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['PassTotal'] = $_raw_Data['PassTotal'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['AttemptTotal'] = $_raw_Data['AttemptTotal'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['StudentTotal'] = $_raw_Data['StudentTotal'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['HighestMark'] = $_raw_Data['HighestMark'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['LowestMark'] = $_raw_Data['LowestMark'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['SD'] = $_raw_Data['SD'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['MEAN'] = $_raw_Data['MEAN'];
// 	$data[$_raw_Data[$index_array[0]]] [$_raw_Data[$index_array[1]]] [$_raw_Data[$index_array[2]]] [$_raw_Data[$index_array[3]]] ['YearClassID'] =  $_raw_Data['YearClassID'];
// }
$data = BuildMultiKeyAssoc($raw_Data, array($index_array[0],$index_array[1],$index_array[2], $index_array[3]));
##Data Getting End
##Table Header Data Getting Start
#Form
$sql = 'SELECT
			YearName, YearID
		FROM
			YEAR
		WHERE
			YearID in ('.$ClassLevelID_sql.')
		ORDER BY
			Sequence';
$rs =  $libSDAS->returnResultSet($sql);
foreach((array)$rs as $_rs){
	$FormHeader[$_rs['YearID']] = $_rs['YearName'];
}
#Class
$sql = 'SELECT
			yc.YearClassID, yc.YearID,  yc.ClassTitleEN, yc.ClassTitleB5
		FROM
			YEAR_CLASS yc
		INNER JOIN
			ACADEMIC_YEAR_TERM	ayt
				ON yc.AcademicYearID = ayt.AcademicYearID and ayt.YearTermID in ('.$YearTermID_sql.')
		WHERE
			YearID in ('.$ClassLevelID_sql.')';
$rs =  $libSDAS->returnResultSet($sql);
foreach((array)$rs as $_rs){
	$ClassHeader[$_rs['YearID']][$_rs['ClassTitleEN']]['ClassTitleEN'] = $_rs['ClassTitleEN'];
	$ClassHeader[$_rs['YearID']][$_rs['ClassTitleEN']]['ClassTitleB5'] = $_rs['ClassTitleB5'];
	// 	$ClassHeader[$_rs['YearID']][$_rs['ClassTitleEN']]['ClassIDArr'][] = $_rs['YearClassID'];
}
#AcademicYear
$sql = '
		SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM
			ACADEMIC_YEAR ay
		INNER JOIN
			ACADEMIC_YEAR_TERM ayt
				ON ay.AcademicYearID = ayt.AcademicYearID
		WHERE
			ayt.YearTermID in ('.$YearTermID_sql.')
		GROUP BY
			ay.AcademicYearID
		ORDER BY
			ayt.TermStart';
$rs =  $libSDAS->returnResultSet($sql);
foreach((array)$rs as $_rs){
	$AcademicYearHeader[$_rs['AcademicYearID']]['YearNameEN'] = $_rs['YearNameEN'];
	$AcademicYearHeader[$_rs['AcademicYearID']]['YearNameB5'] = $_rs['YearNameB5'];
}

#Subject
$sql = '
SELECT
	a.CH_DES as CMP_CH, a.EN_DES as CMP_EN, a.RecordID as CMP_SubjectID, a.CMP_CODEID as CMP_CODEID, b.CH_DES as MAIN_CH, b.EN_DES as MAIN_EN, b.RecordID as MAIN_SubjectID
FROM
	ASSESSMENT_SUBJECT a
INNER JOIN
	ASSESSMENT_SUBJECT b
		ON a.CODEID = b.CODEID AND b.CMP_CODEID = ""
';
$rs = $libSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	if($_rs['CMP_CODEID']){
		$CMPID = $_rs['CMP_SubjectID'];
		$SubjectCMPNameEN = $_rs['MAIN_EN'].'-'.$_rs['CMP_EN'];
		$SubjectCMPNameB5 = $_rs['MAIN_CH'].'-'.$_rs['CMP_CH'];
	}else{
		$CMPID = '0';
		$SubjectCMPNameEN = $_rs['MAIN_EN'];
		$SubjectCMPNameB5 = $_rs['MAIN_CH'];
	}
	$SubjectCMPID = $_rs['MAIN_SubjectID'].'_'.$CMPID;
	
	$SubjectMapping[$SubjectCMPID] = Get_Lang_Selection($SubjectCMPNameB5, $SubjectCMPNameEN);
	
}
$SubjectMapping['0_0'] = $Lang['SDAS']['Form_Passing_Rate']['OverallResult'];
## subject sorting #U128910 
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$sortedSubjectArr = $libpf_exam->getSubjectCodeAndID();

#Subject Teacher MAPPING Start duplicated
if($IsAnnual){
	foreach ((array)$YearTermMapping as $_YearTermMapping){
		foreach ((array)$_YearTermMapping as $acdemicYearID=>$_rs){
			if(in_array($acdemicYearID, (array)$AcademicYear)){
				$YearTermID_new[] = $_rs['YearTermID'];
			}
		}
	}
	$YearTermID_sql_new = implode(',',(array)$YearTermID_new);
}else{
	$YearTermID_sql_new = $YearTermID_sql;
}
$sql = '
		SELECT
			Distinct st.SubjectID, ycu.YearClassID, stct.UserID, iu.EnglishName, iu.ChineseName
		FROM
			SUBJECT_TERM st
		INNER JOIN
			ACADEMIC_YEAR_TERM ayt
				ON st.YearTermID = ayt.YearTermID
		INNER JOIN
			SUBJECT_TERM_CLASS_TEACHER stct
				ON st.SubjectGroupID = stct.SubjectGroupID
		INNER JOIN
			SUBJECT_TERM_CLASS_USER stcu
				ON st.SubjectGroupID = stcu.SubjectGroupID
		INNER JOIN
			YEAR_CLASS_USER	ycu
				ON ycu.UserID = stcu.UserID
		INNER JOIN
			YEAR_CLASS	yc
				ON ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = ayt.AcademicYearID
		INNER JOIN
			INTRANET_USER iu
				ON iu.UserID = stct.UserID
		INNER JOIN
			SUBJECT_TERM_CLASS_YEAR_RELATION stcyr
				ON stcyr.SubjectGroupID = st.SubjectGroupID
		WHERE
			st.YearTermID in ('.$YearTermID_sql_new.')
		AND
			stcyr.YearID in ('.$ClassLevelID_sql.')
';
$rs = $libSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	$SubjectTeacherMapping[$_rs['SubjectID']][$_rs['YearClassID']][] = Get_Lang_Selection($_rs['ChineseName'], $_rs['EnglishName']);
}
#Subject Teacher END
##Table Header Data Getting END
##Print Table Start
$HTML = '<style>
		table.view_table_list_v30 th{white-space: nowrap;}
		table.view_table_list_v30 td{white-space: nowrap;}
		</style>';
$HTML .= '<div class="chart_tables">';
if($GroupBy=='subject'){
    
    $exportArr = array();
    $exportsubjectArr = array();
    $rowNoForExport =0;
    
	// subject sorting #U128910
	//foreach ($data as $SubjectIndex => $SubjectIndexArr){ //Subject
	foreach( $sortedSubjectArr as $_subjectInfo){
		$SubjectIndex = $_subjectInfo['SubjectID'].'_0';
		if(isset($data[$SubjectIndex])){
			$SubjectIndexArr = $data[$SubjectIndex];
		}else{
			// next subject
			continue;
		}
		$HTML .= '<div style="font-size: 16pt;">'.'<b>'.$SubjectMapping[$SubjectIndex].':'.'</b>'.'</div>';
		$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][] = $SubjectMapping[$SubjectIndex];  
		$rowNoForExport++;
		$HTML .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%;">';
		foreach ($FormHeader as $FormHeader_YearID => $_FormHeader){ //By From
			$HTML .= '<thead>';
			$HTML .= '<tr>';
			$HTML .= '<th>'.$Lang['SDAS']['Form_Passing_Rate']['AcademicYear_Form'] .'</th>';
			$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][] = $Lang['SDAS']['Form_Passing_Rate']['AcademicYear_Form'];
			//Class Name Header Printing
			foreach ((array)$ClassHeader[$FormHeader_YearID] as $_ClassHeader){
				$HTML .= '<th style="width:20%;">'.Get_Lang_Selection($_ClassHeader['ClassTitleB5'],$_ClassHeader['ClassTitleEN']).'</th>';
				$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][] = Get_Lang_Selection($_ClassHeader['ClassTitleB5'],$_ClassHeader['ClassTitleEN']);
			}
			//Whole Form
			$HTML .= '<th>'.$Lang['SDAS']['Form_Passing_Rate']['WholeForm'].'</th>';
			$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][] = Get_Lang_Selection($_ClassHeader['ClassTitleB5'],$_ClassHeader['ClassTitleEN']);
			$rowNoForExport++;
			$HTML .= '</tr>';
			$HTML .= '</thead>';
			foreach ((array)$AcademicYearHeader as $AcademicYearHeaderID=> $_AcademicYearHeader){ //AcademicYear
				$HTML .= '<tr>';
				$HTML .= '<td>'.Get_Lang_Selection($_AcademicYearHeader['YearNameB5'], $_AcademicYearHeader['YearNameEN']).'</td>';
				$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][] = Get_Lang_Selection($_AcademicYearHeader['YearNameB5'], $_AcademicYearHeader['YearNameEN']);
				//By Class
				foreach ((array)$ClassHeader[$FormHeader_YearID] as $_ClassHeader){
					if($SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID][$_ClassHeader['ClassTitleEN']]['AttemptTotal']){
						$PassPercentage_Display = $SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID][$_ClassHeader['ClassTitleEN']]['PassTotal']*100/$SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID][$_ClassHeader['ClassTitleEN']]['AttemptTotal'];
						$PassPercentage_Display = round($PassPercentage_Display,2);
						$PassTotal_Display = $SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID][$_ClassHeader['ClassTitleEN']]['PassTotal'];
					}else{
						$PassPercentage_Display ='';
						$PassTotal_Display = '';
					}
					//SubjectTeacher
					$SubjectTeacher = '';
					$tempSubject = explode('_',$SubjectIndex);
					$tempSubject = $tempSubject[0];
					$Slash = '';
					foreach((array)$SubjectTeacherMapping[$tempSubject][$SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID][$_ClassHeader['ClassTitleEN']]['YearClassID']] as $TeacherID){
						$SubjectTeacher .= $Slash.$TeacherID;
						$Slash = '/';
					}
					
					$HTML .= '<td>';
					$HTML .= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')';
					$HTML .= '<br>';
					$HTML .= Get_String_Display($SubjectTeacher);
					$HTML .= '</td>';
					$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][]= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')'."\n".Get_String_Display($SubjectTeacher);
				}
				//Whole Form
				if($SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID]['WholeForm']['AttemptTotal']){
					$PassPercentage_Display = $SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID]['WholeForm']['PassTotal']*100/$SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID]['WholeForm']['AttemptTotal'];
					$PassPercentage_Display = round($PassPercentage_Display,2);
					$PassTotal_Display = $SubjectIndexArr[$AcademicYearHeaderID][$FormHeader_YearID]['WholeForm']['PassTotal'];
				}else{
					$PassPercentage_Display ='';
					$PassTotal_Display = '';
				}
				$HTML .= '<td>'.Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')'.'</td>';
				$HTML .= '</tr>';
				$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][]= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')';
				$rowNoForExport++;
			}
			$rowNoForExport++;
			$exportsubjectArr[$SubjectMapping[$SubjectIndex]][$rowNoForExport][]='';
			$rowNoForExport++;
		}
		$HTML .= '</table>';
		$HTML .= '<br>';
		$HTML .= '<span>'.$Lang['SDAS']['Form_Passing_Rate']['Remark']['1'].'</span>';
		$HTML .= '<br>';
		$HTML .= '<br>';
		$HTML .= '<br>';
	}
// 	debug_pr($exportsubjectArr);
	foreach($exportsubjectArr as $data){
	    foreach ($data as $d){
	        $exportArr[] = $d;
	    }
	} 
// 	debug_pr($exportArr);
	
}elseif($GroupBy=='class'){
    
    $exportArr = array();
    $exportClassArr = array();
    $rowNoForExport =0;
    
	foreach ($data as $FormIndex => $FormIndexArr){ //Form
		$isPrint = true;
		$HTML .= '<div style="font-size: 16pt;">'.'<b>'.$FormHeader[$FormIndex].':'.'</b>'.'</div>';
		$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][] = $FormHeader[$FormIndex];  
		$rowNoForExport++;
		$HTML .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%;">';
		$HTML .= '<thead>';
		$HTML .= '<tr>';
		if($isPrint){
			$HTML .= "<th>".$Lang['SDAS']['Form_Passing_Rate']['Subject_Class'] ."</th>";
			$isPrint = false;
		}else{
			$HTML .= '<td>'.'&nbsp;'.'</td>';
			$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= $Lang['SDAS']['Form_Passing_Rate']['Subject_Class'];
		}
		foreach ($ClassHeader[$FormIndex] as $_ClassHeader){ //Print class Header
		    $colspan = sizeof($AcademicYearHeader); //debug_pr($colspan);
			$HTML .= '<th colspan="'.$colspan.'">'.Get_Lang_Selection($_ClassHeader['ClassTitleB5'],$_ClassHeader['ClassTitleEN']).'</th>';
			$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= Get_Lang_Selection($_ClassHeader['ClassTitleB5'],$_ClassHeader['ClassTitleEN']);
			for($i=1; $i<$colspan; $i++){
			    $exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= "";
			}
		}
		//Whole Form
		$colspan = sizeof($AcademicYearHeader);
		$HTML .= '<th colspan="'.$colspan.'">'.$Lang['SDAS']['Form_Passing_Rate']['WholeForm'].'</th>';
		$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= $Lang['SDAS']['Form_Passing_Rate']['WholeForm'];
		$rowNoForExport++;
		$HTML .= '</tr>';
		$HTML .= '</thead>';
		
		$HTML .= '<tr>';
		$HTML .= '<td>'.'&nbsp;'.'</td>';
		foreach ($ClassHeader[$FormIndex] as $_ClassHeader){ //Print Year By Class
			foreach ($AcademicYearHeader as $_AcademicYearHeader){
				$HTML .= '<td>'.Get_Lang_Selection($_AcademicYearHeader['YearNameB5'],$_AcademicYearHeader['YearNameEN']).'</td>';
				$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= Get_Lang_Selection($_AcademicYearHeader['YearNameB5'],$_AcademicYearHeader['YearNameEN']);
			}
		}
		//Whole Form
		foreach ($AcademicYearHeader as $_AcademicYearHeader){
			$HTML .= '<td>'.Get_Lang_Selection($_AcademicYearHeader['YearNameB5'],$_AcademicYearHeader['YearNameEN']).'</td>';
			$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= Get_Lang_Selection($_AcademicYearHeader['YearNameB5'],$_AcademicYearHeader['YearNameEN']);
		}
		$rowNoForExport++;
		$HTML .= '</tr>';
		// subject sorting #U128910 
		//foreach ($FormIndexArr as $SubjectIndex => $SubjectIndexArr){ //Subject
		foreach( $sortedSubjectArr as $_subjectInfo){
			$SubjectIndex = $_subjectInfo['SubjectID'].'_0';
			if(isset($FormIndexArr[$SubjectIndex])){
				$SubjectIndexArr = $FormIndexArr[$SubjectIndex];
			}else{
				// next subject
				continue;
			}
			$HTML .= '<tr>';
			$HTML .= '<td>'.$SubjectMapping[$SubjectIndex].'</td>';
			$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= $SubjectMapping[$SubjectIndex];
			foreach ($ClassHeader[$FormIndex] as $_ClassHeader){ //Class
				
				foreach ($AcademicYearHeader as $_AcademicYearHeaderID => $_AcademicYearHeader){ //Academic Year
					
					if($SubjectIndexArr[$_ClassHeader['ClassTitleEN']][$_AcademicYearHeaderID]['AttemptTotal']){
						$PassPercentage_Display = $SubjectIndexArr[$_ClassHeader['ClassTitleEN']][$_AcademicYearHeaderID]['PassTotal']*100/$SubjectIndexArr[$_ClassHeader['ClassTitleEN']][$_AcademicYearHeaderID]['AttemptTotal'];
						$PassPercentage_Display = round($PassPercentage_Display, 2);
						$PassTotal_Display = $SubjectIndexArr[$_ClassHeader['ClassTitleEN']][$_AcademicYearHeaderID]['PassTotal'];
					}else{
						$PassPercentage_Display ='';
						$PassTotal_Display = '';
					}
					//SubjectTeacher
					$SubjectTeacher = '';
					$tempSubject = explode('_',$SubjectIndex);
					$tempSubject = $tempSubject[0];
					$Slash = '';
					foreach((array)$SubjectTeacherMapping[$tempSubject][$SubjectIndexArr[$_ClassHeader['ClassTitleEN']][$_AcademicYearHeaderID]['YearClassID']] as $TeacherID){
						$SubjectTeacher .= $Slash.$TeacherID;
						$Slash = '/';
					}
					$HTML .= '<td>';
					$HTML .= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')';
					$HTML .= '<br>';
					$HTML .= Get_String_Display($SubjectTeacher);
					$HTML .= '</td>';
					$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')'."\n".Get_String_Display($SubjectTeacher);
				}
			}
			//Whole Form
			foreach ($AcademicYearHeader as $_AcademicYearHeaderID => $_AcademicYearHeader){ //Academic Year
				if($SubjectIndexArr['WholeForm'][$_AcademicYearHeaderID]['AttemptTotal']){
					$PassPercentage_Display = $SubjectIndexArr['WholeForm'][$_AcademicYearHeaderID]['PassTotal']*100/$SubjectIndexArr['WholeForm'][$_AcademicYearHeaderID]['AttemptTotal'];
					$PassPercentage_Display = round($PassPercentage_Display, 2);
					$PassTotal_Display = $SubjectIndexArr['WholeForm'][$_AcademicYearHeaderID]['PassTotal'];
				}else{
					$PassPercentage_Display ='';
					$PassTotal_Display = '';
				}
				$HTML .= '<td>'.Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')'.'</td>';
				$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= Get_String_Display($PassPercentage_Display).'%'.'('.Get_String_Display($PassTotal_Display).')';
			}
			$HTML .= '</tr>';
			$rowNoForExport++;
			$exportClassArr[$FormHeader[$FormIndex]][$rowNoForExport][]= '';
			$rowNoForExport++;
		}
		$HTML .= '</table>';
		$HTML .= '<br>';
		$HTML .= '<span>'.$Lang['SDAS']['Form_Passing_Rate']['Remark']['1'].'</span>';
		$HTML .= '<br>';
		$HTML .= '<br>';
		$HTML .= '<br>';
		
	}
// 	debug_pr($exportClassArr);
	foreach($exportClassArr as $data){
	    foreach ($data as $d){
	        $exportArr[] = $d;
	    }
	} 
// 	debug_pr($exportArr);
}
$HTML .= '</div>';

if($_POST['isExport']){
//     debug_pr($data);
    include_once($PATH_WRT_ROOT."includes/libexporttext.php");
    $lexport = new libexporttext();
//     $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($data, $exportColumn=array(), "\t", "\r\n", "\t", 0, "11",1);
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportArr, $exportColumn=array(), "\t", "\r\n", "\t", 0, "11",1);
    $lexport->EXPORT_FILE('export.csv', $exportContent);
}else{
//     echo debug_pr($data);
    echo $HTML;
}
##Print Table End
// debug_pr($data);
?>