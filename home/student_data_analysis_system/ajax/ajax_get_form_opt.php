<?php
/*
 *  Change Log:
 *  Page using this ajax: DSE prediction
 *  
 *  Date:	2016-12-08 Villa Add Class Teacher Filtering
 *  Date:	2016-09-19 Villa Open the file - generate seclection box object for selected Academic Year  #K103120
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libSDAS = new libSDAS();
$libpf_exam = new libpf_exam();
$formNameList = explode(",",$formNameList);
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$x .= "(" ;
for($i=0 ;$i<count($formNameList); $i++){
	
	$x .= '"'.$formNameList[$i].'"';
	if($i != count($formNameList)-1){
		$x .=",";
	}
}
$x .= ")";
$aForm = $libpf_exam->getFormListByAcademicYearID($ay_id,$x);
### Start Class Teacher Filtering ###
if($is_ClassTeacher){
	$i=0;
	if(count($aForm)>0){
		foreach($accessRight['classTeacher'] as $_classInfo){
			foreach($aForm as $aForm_key=>$_aForm){
				if($_classInfo['AcademicYearID']==$ay_id){

					if($_aForm['YearID']==$_classInfo['YearID']){
						$temp[$i]['Yearname'] = $_aForm['Yearname'];
						$i++;
					}
				}
			}
		}
	}
	unset($aForm);
	$aForm = $temp;

}
### End Class Teacher filtering ###

if(count($aForm)>0){
	$i=0;
	foreach ($aForm as $_aForm){
		$bForm[$i][0] = $_aForm['Yearname'];
		$bForm[$i][1] = $_aForm['Yearname'];
		$i++;
	}
}
if($form_id==''){

	$html = getSelectByArray($bForm,"name='SelectForm' id='SelectForm' onChange='get_class_opt()'");
}
else{
	$html = getSelectByArray($bForm,"name='SelectForm' id='SelectForm' onChange='get_class_opt()'",$form_id);
}
echo $html;

?>