<?php 
/*Editing By:
 * Change Log:
 * Date 2020-04-29 Philips [2019-0115-1004-54206]
 * 		Merge Subject by flag $sys_custom['SDAS']['FormStudentPerformance']['MergeSubjectCodeArray']
 * 
 * Date 2019-06-21 Philips
 *      Added DisplayForm to choose Short Form or Full Name of Subject
 *      
 * Date 2019-04-03 Anna #J155830
 *      When select monitoring group, get student list rank compared with whole form
 * 
 * Date 2018-12-04 Anna #E143598
 *      - Fixed header name incorrect
 * Date	2017-05-24 Villa
 * -	recalulated $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] => ratio to 100%
 * Date	2017-05-08 Villa #B116599 
 * -	add $sys_custom['SDAS']['form_studenet_perfromance']['OrderFormNotFollowDB']: Calculating using Self-Sorting System in stead of Data in DB
 * -	Change the Remarks
 * Date 2017-04-07 Villa #J114860 
 * -	Add NoBr For Student Name
 * Date 2017-03-22 Villa
 * -	Modified $scoreAsso_New - handle same score for many student 
 * Date 2017-02-22 Villa 
 * -	ReWirte the Performance Data Getting - support compate in different year
 * Date 2017-02-09 Villa 
 * -	Restructure the data to support subject component
 * Date 2017-02-06 Villa 
 * -	Change the data getting logic to reduce sql query
 * Date 2017-02-06 Villa 
 * -	Change Position in Form to the Change of Position in Form
 * Date 2016-12-23 Villa
 * -	Allow to select rank and position subject 
 * Date 2016-12-09 Villa 
 * -	support class selection
 * Date 2016-11-xx 
 * -	Villa Open the file
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");;
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
intranet_auth();
intranet_opendb();
$fcm = new form_class_manage();
$lpf = new libportfolio();
$json = new JSON_obj();
$sbj = new subject();
$libSDAS = new libSDAS();
##### Infor Start #####
// $subject_arr = $sbj->Get_Subject_By_Form($yearID);
$sql = "
		SELECT 
			*
		FROM 
			ASSESSMENT_SUBJECT 
		WHERE
			RecordStatus = '1' 
		";
$temp = $libSDAS->returnResultSet($sql);
//Main Subject Mapping
foreach ((array)$temp as $_temp){
	$temp2[$_temp['CODEID']][] = $_temp['RecordID'];
	if(trim($_temp['CMP_CODEID'])==''){
	    $temp2[$_temp['CODEID']]['RecordID'] = $_temp['RecordID'];
	    $temp2[$_temp['CODEID']]['EN_DES'] = $displayForm ? $_temp['EN_DES'] : $_temp['EN_SNAME'];
	    $temp2[$_temp['CODEID']]['CH_DES'] = $displayForm ? $_temp['CH_DES'] : $_temp['CH_SNAME'];
	}

}
foreach ((array)$temp as $key => $_temp){
	$main_Subject_RecordID = $temp2[$_temp['CODEID']]['RecordID'];
	if(!trim($_temp['CMP_CODEID'])==''){
		$CMP_RecordID = $_temp['RecordID'];
		$CMP_EN_DES =  '-'. ($displayForm ? $_temp['EN_DES'] : $_temp['EN_SNAME']);
		$CMP_CH_DES =  '-'. ($displayForm ? $_temp['CH_DES'] : $_temp['CH_SNAME']);
	}else{
		$CMP_RecordID = '0';
		$CMP_EN_DES = '';
		$CMP_CH_DES = '';
	}
	$subject_arr[$main_Subject_RecordID.'_'.$CMP_RecordID]['RecordID'] = $main_Subject_RecordID.'_'.$CMP_RecordID;
	$subject_arr[$main_Subject_RecordID.'_'.$CMP_RecordID]['EN_DES'] = $temp2[$_temp['CODEID']]['EN_DES'].$CMP_EN_DES;
	$subject_arr[$main_Subject_RecordID.'_'.$CMP_RecordID]['CH_DES'] = $temp2[$_temp['CODEID']]['CH_DES'].$CMP_CH_DES;
	
	# 2020-04-29 (Philips) [2019-0115-1004-54206] - Merge Subject by flag
	if($sys_custom['SDAS']['FormStudentPerformance']['MergeSubjectCodeArray'][$_temp['CODEID']]){
		$mergeSubject = $sbj->Get_Subject_By_SubjectCode($sys_custom['SDAS']['FormStudentPerformance']['MergeSubjectCodeArray'][$_temp['CODEID']]);
		$subject_arr[$main_Subject_RecordID.'_'.$CMP_RecordID]['MergeID'] = $mergeSubject[0]['RecordID'];
	}
}
unset($temp);

$NumOfsubject_arr = count($subject_arr);
$subject_arr[$NumOfsubject_arr]['RecordID'] = '0';
$subject_arr[$NumOfsubject_arr]['CH_DES'] = 'AVG';
$subject_arr[$NumOfsubject_arr]['EN_DES'] = 'AVG';

$Subjects = array_merge((array)$Subject1,(array)$Subject2);
if(!empty($Subjects)){
	foreach ($subject_arr as $subject_arr_key => $subject_arr_value){
		if(!$subject_arr_value['MergeID']){
			if(!in_array($subject_arr_value['RecordID'],$Subjects)){
				unset($subject_arr[$subject_arr_key]);
			}
		} else {
			# 2020-04-29 (Philips) [2019-0115-1004-54206] - Merge Subject by flag
			if(!in_array($subject_arr_value['RecordID'],$Subjects) && !in_array($subject_arr_value['MergeID'].'_0',$Subjects)){
				unset($subject_arr[$subject_arr_key]);
			}
		}
	}
}

foreach ($subject_arr as $key => $_subject_arr){
	$temp = explode('_',$_subject_arr['RecordID']);
// 	if($temp[1]=='0'){
		$subject_arr_temp[] = $temp[0];
// 	}else{
// 		$subject_arr_temp[] = $temp[1];
// 	}
}
$subject_arr_sql = implode("','", $subject_arr_temp);
$sql = "SELECT 
			Title,PercentOrder,Percent
		FROM 
			ASSESSMENT_PERCENTILE_SETTING";
$percentArr = $libSDAS->returnResultSet($sql);
// $percentArr = build_assoc_array($percentArr, 'PercentOrder');
$numOfPercentArr = count($percentArr);
if($ClassSelection!=''){
	$cond_Class = " AND  yc.YearClassID = '$ClassSelection'";
}else{
	$cond_Class = "";
}
##### Infor END #####

##### Data Start #####
if($target_type=='monitoring_group' || count($monitoringGroup)){
	$rs = $libSDAS->getMonitoringGroupMember('2', $monitoringGroup);
	$student_Filter = $rs;
	
// 	$UserID_arr = Get_Array_By_Key($rs, 'UserID');
	$YearID_arr = Get_Array_By_Key($rs, 'YearID');
	$YearID_arr = array_unique($YearID_arr);
	#####J155830#####
	$sql = "SELECT
	       ycu.UserID, ycu.ClassNumber, yc.YearClassID, yc.AcademicYearID, yc.ClassTitleEN, yc.ClassTitleB5, iu.EnglishName, iu.ChineseName, yc.YearID
           FROM
        	YEAR_CLASS_USER ycu
        	INNER JOIN
        	YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
        	INNER JOIN
        	INTRANET_USER iu on iu.UserID = ycu.UserID
        	WHERE
        	yc.AcademicYearID = '{$academicYearID}'
        	AND
        	yc.YearID IN ('".implode("','",$YearID_arr)."')
        	ORDER BY
        	yc.ClassTitleEN, ycu.ClassNumber
	";
	$rs = $libSDAS->returnResultSet($sql);

	$UserID_arr = Get_Array_By_Key($rs, 'UserID');	
	$UserID_sql = implode("','", $UserID_arr);
}
else{
    $YearID_arr[0] = $yearID;
	# student info
	$sql = "SELECT 
				ycu.UserID, ycu.ClassNumber, yc.YearClassID, yc.AcademicYearID, yc.ClassTitleEN, yc.ClassTitleB5, iu.EnglishName, iu.ChineseName, yc.YearID 
			FROM 
				YEAR_CLASS_USER ycu 
			INNER JOIN 
				YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID 
			INNER JOIN 
				INTRANET_USER iu on iu.UserID = ycu.UserID
			WHERE
				yc.AcademicYearID = '{$academicYearID}'
			AND
				yc.YearID = '{$yearID}'
				
			$cond_Class
			ORDER BY
				yc.ClassTitleEN, ycu.ClassNumber
				";
	$student_Filter = $libSDAS->returnResultSet($sql);
	// $UserID_arr_By_select = Get_Array_By_Key($rs, 'UserID');
	# ALL student info
	$sql = "SELECT
				ycu.UserID, ycu.ClassNumber, yc.YearClassID, yc.AcademicYearID, yc.ClassTitleEN, yc.ClassTitleB5, iu.EnglishName, iu.ChineseName
			FROM
				YEAR_CLASS_USER ycu
			INNER JOIN
				YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
			INNER JOIN
				INTRANET_USER iu on iu.UserID = ycu.UserID
			WHERE
				yc.AcademicYearID = '{$academicYearID}'
			AND
				yc.YearID = '{$yearID}'
			ORDER BY
			yc.ClassTitleEN, ycu.ClassNumber
			";
	$rs = $libSDAS->returnResultSet($sql);
	$UserID_arr = Get_Array_By_Key($rs, 'UserID');
	$UserID_sql = implode("','", $UserID_arr);
}

#Performance 1 Start Performance[UserID] [CMPID] CMPID -> Main_Sup 

$TermID = explode('_',$YearTermID);
if(empty($TermID[0])){
	$cond_Term = "Semester = ''"; //overall result
}else{
	$cond_Term = "Semester = '$TermID[0]'";
}
if(empty($TermID[1])){
	$cond_AsseTerm = "TermAssessment IS NULL";
}else{
	$cond_AsseTerm = "TermAssessment = '$TermID[1]'";
}
$sql =
	"SELECT
		UserID, Score
	FROM
		{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
	WHERE
		UserID IN ('$UserID_sql')
	AND
		AcademicYearID =  '{$academicYearID}'
	AND
		Score>= '0'
	AND
		$cond_Term
	AND
		$cond_AsseTerm
	";
$tempMain = $libSDAS -> returnResultSet($sql);
$sql =
	"SELECT
		UserID, SubjectID, SubjectComponentID, Score, OrderMeritForm, OrderMeritFormTotal
	FROM
		{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	WHERE
		UserID IN ('$UserID_sql')
	AND
		AcademicYearID =  '{$academicYearID}'
	AND
		SubjectID IN ('$subject_arr_sql')
	AND
		Score>= '0'
	AND
		$cond_Term
	AND
		$cond_AsseTerm
	";
$tempSubject = $libSDAS -> returnResultSet($sql);

#Score Mapping Start
if(!empty($tempMain)){
	foreach ((array)$tempMain as $_tempMain){
		$scoreAsso[0][] = $_tempMain['Score'];
	}
}
if(!empty($tempSubject)){
	foreach ($tempSubject as $_tempSubject){
		if(!$_tempSubject['SubjectComponentID']){
			$CMPCode = $_tempSubject['SubjectID'].'_'.'0';
		}else{
			$CMPCode = $_tempSubject['SubjectID'].'_'.$_tempSubject['SubjectComponentID'];
		}
		$scoreAsso[$CMPCode][] = $_tempSubject['Score'];
	}
}
if(!empty($scoreAsso)){
	foreach ($scoreAsso as $key=>$_scoreAsso){
		$i=1;
		arsort($_scoreAsso);
		foreach($_scoreAsso as $score){
			if($score!=$previous_score){ #Y114977
				$scoreAsso_New[$key][$score] = $i;
				$scoreAsso_New[$key]['TotalInForm'] = $i;
			}
			$previous_score = $score;
			$i++;
		}
		unset($previous_score);
	}
}

if(!empty($tempMain)){
	foreach ((array)$tempMain as $_tempMain){
		$Performance[$_tempMain['UserID'] ] [0]  = $_tempMain;
		$Performance[$_tempMain['UserID'] ] [0] ['OrderMeritForm']  = $scoreAsso_New[0] [$Performance[$_tempMain['UserID'] ] [0] ['Score']];
		$Performance[$_tempMain['UserID'] ] [0] ['OrderMeritFormTotal'] = $scoreAsso_New[0]['TotalInForm'];
	}
}
if(!empty($tempSubject)){
	foreach ((array)$tempSubject as $_tempSubject){
		if(!$_tempSubject['SubjectComponentID']){
			$CMPCode = $_tempSubject['SubjectID'].'_'.'0';
		}else{
			$CMPCode = $_tempSubject['SubjectID'].'_'.$_tempSubject['SubjectComponentID'];
		}
		$Performance[$_tempSubject['UserID'] ] [$CMPCode]  = $_tempSubject;
		#B116599 
		if($sys_custom['SDAS']['form_studenet_perfromance']['OrderFormNotFollowDB']){
			$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $scoreAsso_New[$CMPCode] [$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['Score']];
			$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $scoreAsso_New[$CMPCode]['TotalInForm'];
		}else{
			$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $_tempSubject['OrderMeritForm'];
			$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $_tempSubject['OrderMeritFormTotal'];
		}
// 		$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $scoreAsso_New[$CMPCode] [$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['Score']];
// 		$Performance[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $scoreAsso_New[$CMPCode]['TotalInForm'];
		
// 		$numberOfStudent[$CMPCode][] = '';
	}
}
#Performance 1 END

#Performance 2 Start
$TermID_VS = explode('_',$YearTermID_VS);
if(empty($TermID_VS[0])){
	$cond_Term = "Semester = ''"; //overall result
}else{
	$cond_Term = "Semester = '$TermID_VS[0]'";
}
if(empty($TermID_VS[1])){
	$cond_AsseTerm = "TermAssessment IS NULL";
}else{
	$cond_AsseTerm = "TermAssessment = '$TermID_VS[1]'";
}
$sql =
		"SELECT
			UserID, Score, OrderMeritForm, OrderMeritFormTotal
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		WHERE
			UserID IN ('$UserID_sql')
		AND
			AcademicYearID =  '{$academicYearID_VS}'
		AND
			Score>= '0'
		AND
			$cond_Term
		AND
			$cond_AsseTerm
";
$tempMain = $libSDAS -> returnResultSet($sql);
$sql =
		"SELECT
			UserID, SubjectID, SubjectComponentID, Score, OrderMeritForm, OrderMeritFormTotal
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
		WHERE
			UserID IN ('$UserID_sql')
		AND
			AcademicYearID =  '{$academicYearID_VS}'
		AND
			SubjectID IN ('$subject_arr_sql')
		AND
			Score>= '0'
		AND
			$cond_Term
		AND
			$cond_AsseTerm
";
$tempSubject = $libSDAS -> returnResultSet($sql);

#Score Mapping Start
unset($scoreAsso);
unset($scoreAsso_New);
if(!empty($tempMain)){
	foreach ((array)$tempMain as $_tempMain){
		$scoreAsso[0][] = $_tempMain['Score'];
	}
}
if(!empty($tempSubject)){
	foreach ((array)$tempSubject as $_tempSubject){
		if(!$_tempSubject['SubjectComponentID']){
			$CMPCode = $_tempSubject['SubjectID'].'_'.'0';
		}else{
			$CMPCode = $_tempSubject['SubjectID'].'_'.$_tempSubject['SubjectComponentID'];
		}
		$scoreAsso[$CMPCode][] = $_tempSubject['Score'];
	}
}
if(!empty($scoreAsso)){
	foreach ($scoreAsso as $key=>$_scoreAsso){
		$i=1;
		arsort($_scoreAsso);
		foreach($_scoreAsso as $score){
			if($previous_score!=$score){ #Y114977
				$scoreAsso_New[$key][$score] = $i;
				$scoreAsso_New[$key]['TotalInForm'] = $i;
			}
			$previous_score = $score;
			$i++;
		}
		unset($previous_score);
	}
}
if(!empty($tempMain)){
	foreach ((array)$tempMain as $_tempMain){
		$Performance_VS[$_tempMain['UserID'] ] [0]  = $_tempMain;
		$Performance_VS[$_tempMain['UserID'] ] [0] ['OrderMeritForm']  = $scoreAsso_New[0] [$Performance_VS[$_tempMain['UserID'] ] [0] ['Score']];
		$Performance_VS[$_tempMain['UserID'] ] [0] ['OrderMeritFormTotal'] = $scoreAsso_New[0]['TotalInForm'];
	}
}
if(!empty($tempSubject)){
	foreach ((array)$tempSubject as $_tempSubject){
		if(!$_tempSubject['SubjectComponentID']){
			$CMPCode = $_tempSubject['SubjectID'].'_'.'0';
// 			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] [] = $_tempSubject;
		}else{
			$CMPCode = $_tempSubject['SubjectID'].'_'.$_tempSubject['SubjectComponentID'];
// 			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] [] = $_tempSubject;
		}

		$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode]  = $_tempSubject;
		#B116599 
		if($sys_custom['SDAS']['form_studenet_perfromance']['OrderFormNotFollowDB']){
			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $scoreAsso_New[$CMPCode] [$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['Score']];
			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $scoreAsso_New[$CMPCode]['TotalInForm'];
		}else{
			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $_tempSubject['OrderMeritForm'];
			$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $_tempSubject['OrderMeritFormTotal'];
		}
// 		$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritForm'] = $scoreAsso_New[$CMPCode] [$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['Score']];
// 		$Performance_VS[$_tempSubject['UserID'] ] [$CMPCode] ['OrderMeritFormTotal'] = $scoreAsso_New[$CMPCode]['TotalInForm'];
	}
}

#Performance 2 END
foreach ($student_Filter as $_rs){

	$student_name = empty($_rs['ChineseName'])? $_rs['EnglishName'] : Get_Lang_Selection($_rs['ChineseName'], $_rs['EnglishName']);
	$data[$_rs['UserID']]['StudentName'] = $student_name;
	$data[$_rs['UserID']] ['ClassName'] = Get_Lang_Selection($_rs['ClassTitleB5'], $_rs['ClassTitleEN']);
	$data[$_rs['UserID']] ['ClassNumber'] = $_rs['ClassNumber'];
	
// 	if($_SESSION['UserID'] == '1'){
// 	    debug_pr($_rs);
// 	}
	foreach($YearID_arr as $YearID){ #####J155830#####
	    if($_rs['YearID'] == $YearID){ #####J155830#####
    	    foreach ($subject_arr as $subject){
    	    	
    	    	# 2020-04-29 (Philips) [2019-0115-1004-54206] - Merge Subject by flag
    	    	if($subject_arr[$subject['RecordID']]['MergeID']){
    	    		$thisMergeID = $subject_arr[$subject['RecordID']]['MergeID'];
    	    		if(!$Performance_VS[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm']){
    	    			$Performance_VS[$_rs['UserID']] [$subject['RecordID']] = $Performance_VS[$_rs['UserID']] [$thisMergeID.'_0'];
    	    		}
    	    	}
    	    	
    	        ##### Ranking Related #####
    	        if($Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm'] && $Performance_VS[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm']){
    	            if($Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritFormTotal']){
    	                $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] = round($Performance_VS[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm']*100/$Performance_VS[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritFormTotal']-$Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm']*100/$Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritFormTotal']);
    	            }else{
    	                $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] = $Performance_VS[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm']-$Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm'];
    	            }
    	        }else{
    	            $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] = $Lang['General']['EmptySymbol'];
    	        }
    	        if(empty($Performance[$_rs['UserID']] [$subject['RecordID']] ['OrderMeritForm'])){
    	            $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] = $Lang['General']['EmptySymbol'];
    	            $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Level'] = $Lang['General']['EmptySymbol'];
    	        }else{
    	            if($data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] < 0){
    	                $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] = -1* $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] ;
    	                //bg color
    	                if($data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] >= '30'){
    	                    $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['bgColorRanking'] = '#D2F6F8';
    	                }
    	                $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] =
    	                '('.$data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'].')';
    	                $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['ColorRanking'] = 'Red';
    	                
    	            }else{
    	                // Do nothing
    	                if($data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Ranking'] >= '20'){
    	                    $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['bgColorRanking'] = '#FEEE80';
    	                }
    	            }
    	            
    	            ##### Level Related #####
    	            $previous = 0;
    	            for($i=0;$i<$numOfPercentArr;$i++){
    	                $current = $percentArr[$i]['Percent'] + $previous;
    	                $lowThan = $current*$Performance[$_rs['UserID']] [$subject['RecordID']]['OrderMeritFormTotal']/100;
    	                if($Performance[$_rs['UserID']] [$subject['RecordID']]['OrderMeritForm'] <=$lowThan && !isset($data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Level'])){
    	                    $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['Level']=  $percentArr[$i]['Title'];
    	                    if($i==0){	//First Rank
    	                        $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['ColorLevel'] = "Yellow ";
    	                    }elseif($i==$numOfPercentArr-1){	//Last Rank
    	                        $data[$_rs['UserID']] ['Result'] [$subject['RecordID']] ['ColorLevel'] = "Orange";
    	                    }
    	                }
    	                $previous = $current;
    	                
    	            }
    	            ##### Level Related #####
    	        }        
    	        ##### Ranking Related #####	        
    	    }
	    }
	}

}
##### Data END #####
##### Header Start #####
foreach($subject_arr as $subject){
//     debug_pr($subject);die();
	if($Subject1){
		if($subject['RecordID']=='0'){
			if(in_array($subject['RecordID'], $Subject1)){
				$header['Percent'][] =	$ec_iPortfolio['overall_result'];
			}
		}else{
			if(in_array($subject['RecordID'], $Subject1)){
				$header['Percent'][] = Get_Lang_Selection($subject['CH_DES'], $subject['EN_DES']);
			}
		}
	}
}
foreach($subject_arr as $subject){
	if($Subject2){
		if($subject['RecordID']=='0'){
			if(in_array($subject['RecordID'], $Subject2)){
				$header['Position'][] =	$ec_iPortfolio['overall_result'];
			}
		}else{
			if(in_array($subject['RecordID'], $Subject2)){
				$header['Position'][] = Get_Lang_Selection($subject['CH_DES'], $subject['EN_DES']);
			}
		}
	}
}
$header['Cat'][]  = $Lang['Header']['Menu']['Class'];
$header['Cat'][]  = $Lang['General']['ClassNumber'];
$header['Cat'][]  = $Lang['General']['Name'];
if($Subject1){
	$header['Cat'][] = "Percent";
}
$header['Cat'][] = "";
if($Subject2){
	$header['Cat'][] = "Position";
}
##### Header END #####

##### Table Start #####
$x = "";
$x .= "<div class='chart_tables'>";
$x .= "<table class='common_table_list_v30 view_table_list_v30 resultTable' id='chart_tables_1'>";
	##### Tabler Header Start #####
	$x .= "<thead>";
		$x .= "<tr>";
			foreach ($header['Cat'] as $_headerCat){
				$numOfHeader = count($header[$_headerCat]);
				switch ($_headerCat){
					default:
						$displayCell = $_headerCat;
						$rowspan ='rowspan="2"';
						break;
					case "Percent":
						$displayCell = $Lang['SDAS']['menu']['percentile'];
						$rowspan ='';
						break;
					case "Position":
					    $displayCell = $Lang['SDAS']['PositionInFormDiff'];
						$rowspan ='';
						break;
				}
				$x .= "<th style=' white-space: nowrap;' colspan='".$numOfHeader."' $rowspan >";
				$x .= $displayCell ;
				$x .= "</th>";
			}
		$x .= "</tr>";
		$x .= "<tr>";
			foreach ($header['Cat'] as $_headerCat){
				if(!empty($header[$_headerCat])){
					foreach ($header[$_headerCat] as $_header){
						$x .= "<th style=' white-space: nowrap;'>";
						$x .= $_header;
						$x .= "</th>";
					}
				}
			}
		$x .= "</tr>";
	$x .= "</thead>";
	##### Tabler Header END #####
	##### Tabler Body Start #####
	$x .= "<tbody>";
		if(!empty($data)){ 
			foreach ($data as $_data){ //each User
				if(!empty($_data['Result'])){
					$x .= "<tr>";
						$x .= "<td>";
						$x .= $_data['ClassName'];
						$x .= "</td>";
						$x .= "<td>";
						$x .= $_data['ClassNumber'];
						$x .= "</td>";
						$x .= "<td>";
						$x .= "<nobr>".$_data['StudentName']."</nobr>";
						$x .= "</td>";
						foreach ($_data['Result'] as $_data_key=>$__data){
							if($Subject1){
								if(in_array($_data_key, $Subject1)){
									$x .= "<td style=' background-color: {$__data ['ColorLevel']} '>";
									$x .= $__data['Level'];
									$x .= "</td>";
								}
							}
							
						}
						$x .= "<td></td>";
						foreach ($_data['Result'] as $_data_key=>$__data){
							if($Subject2){
								if(in_array($_data_key, $Subject2)){
									$x .= "<td style='color: {$__data['ColorRanking']}; background-color:{$__data['bgColorRanking']};'>";
									$x .= $__data['Ranking'];
									$x .= "</td>";
								}
							}
						}
						
					$x .= "</tr>";
				}
			}
		}
	$x .= "</tbody>";
	##### Tabler Body END #####
$x .= "</table>";
$x .= "</div>";
// $x .= "<div style='float: left; text-align: left;'>";
	$x .= "<table>";
	$x .= '<tr>';
	$x .= '<td style="vertical-align:top;">';
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['PercentileLabel'].':<br>';
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['Yellow'].'<br>';
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['Orange'].'<br>';
	$x .= '</td>';
	$x .= '<td>';
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['RankingLabel'].':<br>';
	$x .= $Lang['SDAS']['Form_student_Performance']['Remarks'].'<br>' ;
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['LightYellow'].'<br>';
	$x .= $Lang['SDAS']['FormStudentPerformance']['Remark']['LightBlue'].'<br>';
	$x .= '</td>';
	$x .= '</tr>';
	$x .= "</table>";
// $x .= "</div>";
##### Table END #####
?>
<script src="/templates/jquery/jquery.floatheader.min.js"></script>
<script>
$( document ).ready(function() {
	$('#chart_tables_1').floatHeader();
});
</script>
<?php echo $x;?>
