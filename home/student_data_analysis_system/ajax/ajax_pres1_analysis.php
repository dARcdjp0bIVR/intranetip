<?php
//Using:
/**
 * Change Log
 * 
 */

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

$libpf_exam = new libpf_exam();

$lpf = new libportfolio();
$json = new JSON_obj();
// $objSDAS = new libSDAS();
$Semester = $YearTermID;
if (strstr($YearTermID, "_"))
{
    $tmpArr = explode("_", $YearTermID);
    $YearTermID =  $tmpArr[0];
    $TermAssessment = trim($tmpArr[1]);
}
########## Access Right START ##########
$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
$canAccess = true;
if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) &&
    in_array('form_student_performance',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
        $accessRight['admin'] = 1;
}
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$academicYearID = Get_Current_Academic_Year_ID();
if(!$accessRight['admin']){
    if( count($accessRight['subjectPanel']) ){
        #### Subject Panel access right START ####
        
        $canAccess = true; // Subject Panel can always access this page
        $filterSubjectArr = array_keys($accessRight['subjectPanel']);
        
        #### Subject Panel access right END ####
    }else{
        #### Class teacher access right START ####
        
        if($academicYearID != $currentAcademicYearID){
            $canAccess = false;
        }
        $yearClassIdArr = array();
        foreach($accessRight['classTeacher'] as $yearClass){
            $yearClassIdArr[] = $yearClass['YearClassID'];
        }
        foreach((array)$YearClassID as $id){
            if(!in_array($id, $yearClassIdArr)){
                $canAccess = false;
                break;
            }
        }
        
        #### Class teacher access right END ####
    }
    
    if(!$canAccess){
        echo 'Access Denied.';
        die;
    }
}
########## Access Right END ##########

$li = new libdb();
$linterface = new interface_html();


#### Get Year START ####
$academicYearIDArr = $lpf->GetAcademicYears($FromAcademicYearID, $ToAcademicYearID);
$academicYearArr_sql = implode(',',$academicYearIDArr);
$sql = "SELECT YearNameEN, YearNameB5, AcademicYearID,Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID IN (".$academicYearArr_sql.")";
$rs = $lpf->returnResultSet($sql);
$AcademicYears= array(); //reset $academicYearArr
foreach ((array)$rs as $key=>$value){
    $AcademicYears[$key] ['YearNameEN'] = Get_Lang_Selection($value['YearNameB5'], $value['YearNameEN']);
    $AcademicYears[$key] ['AcademicYearID'] = $value['AcademicYearID'];
    $AcademicYears[$key] ['Sequence'] = $value['Sequence'];
}
#### Get Year  END ####

$ResultArray = array();

// Subject related
$SubjectMapping = $libpf_exam->getSubjectIDWebSAMSMapping();

$ChineseSubjectID = $SubjectMapping["080"];
$EnglishSubjectID = $SubjectMapping["165"];
$MathsSubjectID = $SubjectMapping["22S"];
$SubjectIDAry = array($ChineseSubjectID, $EnglishSubjectID, $MathsSubjectID);
$SubjectIDMappingAry = array($ChineseSubjectID => "Chi", $EnglishSubjectID => "Eng", $MathsSubjectID => "Math");

// Subject Title
$SubjectCodeAry = $libpf_exam->getSubjectCodeAndID($SubjectIDAry);

for($i=0; $i<count($SubjectCodeAry); $i++)
{
    $currentSubject = $SubjectCodeAry[$i];
    $currentSubjectCode =  $SubjectIDMappingAry[$currentSubject["SubjectID"]];
    $ResultArray["Subject"][$currentSubjectCode] = $currentSubject["main_name"];
}
$ResultArray["Subject"]["Average"] = $Lang['SDAS']['PreS1Analysis']['SubjectAverage'];
$highChartDataAssoAry['Subject'] = $json->encode($ResultArray['Subject']);

// Academic Year
$returnYears = array();

$AcademicYears= sortByColumn((array)$AcademicYears, "Sequence");
for($i=0; $i<count($AcademicYears); $i++){
    $returnYears[$i] = array();
    $returnYears[$i]["Name"] = $AcademicYears[$i]["YearNameEN"];
    $returnYears[$i]["Sequence"] = $AcademicYears[$i]["Sequence"];
    
    $ResultArray['highChart']['xAxis'][] = $AcademicYears[$i]["YearNameEN"];
}


$ResultArray["Year"] = sortByColumn((array)$returnYears, "Sequence");

$ResultArray["Stat"] = array();

// loop Academic Years
for($ycout=0; $ycout<count($AcademicYears); $ycout++)
{
    // get Year Info
    $CurrentAcademicYearID = $AcademicYears[$ycout]["AcademicYearID"];
  
    $CurrentAcademicYearName = $AcademicYears[$ycout]["YearNameEN"];
    
    // get Exam Data
    $ExamScore = $libpf_exam->getExamData($CurrentAcademicYearID , EXAMID_HKAT, $SubjectIDAry);
  
    $ExamScoreResult = BuildMultiKeyAssoc((array)$ExamScore, array("SubjectID", "RecordID"), array("Score"), 1);
    
    // array for Stat Data
  
    $statArray = array();

    // loop Exam Data
    foreach($ExamScoreResult as $CurrentSubjectID => $ScoreResult)
    {
      
        $CurrentSubjectCode = $SubjectIDMappingAry[$CurrentSubjectID];
        if(is_array($ScoreResult) && count($ScoreResult) > 0)
        {
            
            $statArray["Year"] = $CurrentAcademicYearName;
            
            // store Student Score in array
            foreach((array)$ScoreResult as $studentid => $Score)
            {
                $ResultArray["Score"][$CurrentSubjectCode][] = array('Year'=>$CurrentAcademicYearName, 'School'=>$SchoolCode, 'Subject'=>$CurrentSubjectCode, 'Score'=>$Score);
            }
            
            // get Stat Data
            $statArray[$CurrentSubjectCode] = $libpf_exam->getStatisData($ScoreResult);
        }
    }

    // store Stat Data
    if(!empty($statArray)){
        $ResultArray["Stat"][] = $statArray;
    }
}
if(!empty($ResultArray['Stat'])){
    
      
    $tmpArr = array();
    $tmp2Arr = array();
    $count = 0 ;
    foreach((array)$SubjectIDMappingAry as $_id => $_code){
        $countTmp2 = 0;
        foreach ($ResultArray['highChart']['xAxis'] as $_year){ //year loop
            $addDate = false;
            
            foreach((array)$ResultArray["Stat"] as $__key => $__yearResultArr ){
            
                if($_year == $__yearResultArr['Year']){
                    $tmpArr[$count]['name'] = $_code;
                    $tmpArr[$count]['data'][] = (float)$__yearResultArr[$_code]['Mean'];
                    
                    $tmp2Arr[$_code][$countTmp2][] = (float)$__yearResultArr[$_code]['Min'];
                    $tmp2Arr[$_code][$countTmp2][] = (float)$__yearResultArr[$_code]['Q1'];
                    $tmp2Arr[$_code][$countTmp2][] = (float)$__yearResultArr[$_code]['Median'];
                    $tmp2Arr[$_code][$countTmp2][] = (float)$__yearResultArr[$_code]['Q3'];
                    $tmp2Arr[$_code][$countTmp2][] = (float)$__yearResultArr[$_code]['Max'];
                    $addDate = true;
                }
            }
            if(!$addDate){
                $tmpArr[$count]['name'] = $_code;
                $tmpArr[$count]['data'][] = (float)0;
                
                $tmp2Arr[$_code][$countTmp2][] = (float)0;
                $tmp2Arr[$_code][$countTmp2][] = (float)0;
                $tmp2Arr[$_code][$countTmp2][] = (float)0;
                $tmp2Arr[$_code][$countTmp2][] = (float)0;
                $tmp2Arr[$_code][$countTmp2][] = (float)0;
            }
            $countTmp2++;
        }
        $count ++;
    }
    
    
    $ResultArray['highChart']['averageChart'] = $tmpArr;
    $ResultArray['highChart']['subjectBoxplot'] = $tmp2Arr;
    
    $highChartDataAssoAry['highChart'] = $json->encode($ResultArray['highChart']);
    // $highChartDataAssoAry['highChart']['seriesJson'] = $json->encode($highChartDataAssoAry['meanChart']['series']);
    
    unset($tmpArr);
    unset($tmp2Arr);
    
    // Build data for Stat Table
    $ResultArray["StatTable"] = array();
    $statTypeArr = array("Min", "Q1", "Median", "Q3", "Max");
    foreach((array)$SubjectIDMappingAry as $SubjectID => $SubjectCode){
        $ResultArray["StatTable"][$SubjectCode] = array();
        
        // loop Stat Data
        $StatData = $ResultArray["Stat"];
        for($j=0; $j<count((array)$StatData); $j++)
        {
            $currentYear = $StatData[$j]["Year"];
            $currentStat = $StatData[$j][$SubjectCode];
            if(is_array($currentStat) && count($currentStat)>0)
            {
                $statCount = 0;
                //					foreach((array)$currentStat as $statType => $statval){
                for($stcount=0; $stcount<count($statTypeArr); $stcount++)
                {
                    $statType = $statTypeArr[$stcount];
                    $statval = $currentStat[$statType];
                    
                    // skip if not target Stat Type
                    if($statval!=0 && empty($statval))
                        continue;
                        
                        $ResultArray["StatTable"][$SubjectCode][$statCount]["Type"] = $statType;
                        $ResultArray["StatTable"][$SubjectCode][$statCount][$currentYear] = my_round($statval);
                        $statCount++;
                }
            }
        }
    }
    
    $highChartDataAssoAry['StatTable'] = $json->encode( $ResultArray["StatTable"]);
    $highChartDataAssoAry['Stat'] = $json->encode($ResultArray["Stat"]);
    
    
    
    ######## UI START ########
    ##### Table 2 Data Start #####
    
    $table2 = "";
    $table2 .="<table class = 'table_generic'>";
    ### Table2 Head Start ###
        $table2 .="<thead>";
            $table2 .= "<tr>";
                $table2 .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
                foreach($AcademicYears as $YearStatAry){
                    $table2 .= "<th>".$YearStatAry['YearNameEN']."</th>";         
                }
            $table2 .= "</tr>";
        $table2 .= "</thead>";
    //     ### Table2 Head END ###
    //     ### Table Body Start ###
        $table2 .= "<tbody>";
        ### Average Start###
        $i=0;
        
        foreach ($ResultArray['highChart']['averageChart'] as $key=>$AverageAry){  
            $table2 .= "<tr><td>".$AverageAry['name']."</td>";     
            foreach ($AverageAry['data'] as $data){
                if($data != '0'){
                    $table2 .= "<td>".$data."</td>";  
                }  else{
                    $table2 .= "<td></td>";  
                }
                
            }
            $table2 .= "</tr>";
        }
       
    //     ### Tatal Pass END###
        $table2 .= "</tbody>";
    // ### Table Body END ###
    $table2 .= "</table>";
    ##### Table 2 Data End #####
//     debug_pr($ResultArray["Stat"]);
    foreach((array)$SubjectIDMappingAry as $SubjectID => $SubjectCode){
        $subjectTable[$SubjectCode] = '';
        $subjectTable[$SubjectCode] .= "<div id='".$SubjectCode."Table'>";
        $subjectTable[$SubjectCode] .="<table class = 'table_generic' id='".$SubjectCode."Table'>";
        ### Table2 Head Start ###
            $subjectTable[$SubjectCode] .="<thead>";
                $subjectTable[$SubjectCode] .= "<tr>";
                    $subjectTable[$SubjectCode] .= "<th style='width: 20%; white-space: nowrap;'>"." "."</th>";
                    foreach($AcademicYears as $YearStatAry){
                        $subjectTable[$SubjectCode] .= "<th>".$YearStatAry['YearNameEN']."</th>";
                    }
                $subjectTable[$SubjectCode] .= "</tr>";
            $subjectTable[$SubjectCode] .= "</thead>";
            $subjectTable[$SubjectCode] .= "<tbody>";
                $subjectTable[$SubjectCode] .= "<tr>";
                foreach ($statTypeArr as $ScoreType){
                    $subjectTable[$SubjectCode] .= "<td>".$ScoreType."</td>";    
               
                    foreach ($AcademicYears as $YearStatAry){         // loop selected academic year          
                        //                     foreach ($ResultArray["Stat"] as $key=>$ScoreAry){   ".$ScoreAry[$SubjectCode][$ScoreType]."
                        foreach ($ResultArray["Stat"] as $key=>$ScoreAry){                   
                           if($ScoreAry['Year'] == $YearStatAry['YearNameEN']){
                               $ScoreYearAry[$ScoreAry['Year']] = $ScoreAry[$SubjectCode][$ScoreType]; // store year score in array
                           }
                        }
                        if($ScoreYearAry[$YearStatAry['YearNameEN']] == ''){
                            $displayScore = '';
                        }else{
                            $displayScore = $ScoreYearAry[$YearStatAry['YearNameEN']];
                        }
                        $subjectTable[$SubjectCode] .= "<td>".$displayScore."</td>";
                    }
                    $subjectTable[$SubjectCode] .= "</tr>";
                }
                
                //     ### Tatal Pass END###
                $subjectTable[$SubjectCode] .= "</tbody>";
        // ### Table Body END ###
        $subjectTable[$SubjectCode] .= "</table></div>";
        $subjectTable[$SubjectCode] .= "<br><br>";
    }
    echo $linterface->Include_Thickbox_JS_CSS();
    ?>
    <style>
    
    /*tables*/
    /*.table_generic {margin:0 auto; font-size:1.1em; table-layout:fixed; width:100%;}*/ /*2016-7-14*/
    .table_generic {margin:0 auto; font-size:1.1em; border-collapse: collapse;}
    .table_generic th {border-bottom:#cdcdcd 2px solid; padding:10px 20px;}
    .table_generic td {border-bottom:#cdcdcd 1px solid; padding:10px; text-align:center;} /*2016-7-14*/
    .table_generic td:first-child {font-weight:bold; border-right:#cdcdcd 1px solid; text-align:left;}
    .table_generic tr:last-child td {border-bottom:#cdcdcd 2px solid;}
    .table_generic td.right {border-right:#cdcdcd 1px solid;}
    .table_generic tr.highlight, .table_generic td.highlight {background-color: #EEFAFF;}
    /*table stripe 2017-6-22*/
    .table_generic.stripe tr:nth-child(even) td{background: #fff; background: -moz-linear-gradient(top,  #f6f8fa 0%, #ffffff 30%, #ffffff 70%, #f6f8fa 100%); background: -webkit-linear-gradient(top,  #f6f8fa 0%,#ffffff 30%,#ffffff 70%,#f6f8fa 100%);
    background: linear-gradient(to bottom,  #f6f8fa 0%,#ffffff 30%,#ffffff 70%,#f6f8fa 100%);}
    /*table dse*/
    .table_generic.dse td:last-child {font-weight:bold; border-left:#cdcdcd 1px solid}
    
    </style>
    <!-- -------- Generate bar chart START -------- -->
    <script src="<?=$PATH_WRT_ROOT?>/templates/d3/d3_v3_5_16.js"></script>
    <style>
    .axis path,
    .axis line {
      fill: none;
      stroke: #000;
      shape-rendering: crispEdges;
    }
    
    .bar {
      fill: steelblue;
    }
    
    .x.axis path {
      display: none;
    }
    
    </style>
    
    <link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
    <div class="Average" id="Average">
    </div>
    
    <div class="AverageTable" id="AverageTable">
    <?=$table2?>
    </div>
    
    <div id="graphDiv">
    	<div class="Chi" id="Chi">
        </div>
        <div class="ChiTable" id="ChiTable"><?=$subjectTable['Chi']?></div>
        
    	<div class="Math" id="Math">
        </div>
        <div class="MathTable" id="MathTable"><?=$subjectTable['Math']?></div>
        
        <div class="Eng" id="Eng">
        </div>
        <div class="EngTable" id="MathTable"><?=$subjectTable['Eng']?></div>
        
    	
    </div>
    
    
    <script>
    
    highChart = <?=$highChartDataAssoAry['highChart']?>;
    subjectName = <?=$highChartDataAssoAry['Subject'] ?>;
    
    $('#Average').highcharts({
        chart: {
            type: 'line'
        },
        xAxis: {
            categories: highChart.xAxis,
            title: {
                 text: '<?=$Lang['SDAS']['toCEES']['SchoolYear']?>'
             },
        },
        yAxis: {
        	min:0,
        	max:100,
            title: {
                text: '<?=$Lang['SDAS']['PreS1Analysis']['SubjectAverage']?>'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: highChart.averageChart,    
        credits: {
         	enabled: false,
    	}
    });
    d3.select('#graphDiv').append('div').attr('id', 'AverageTable');
    
    var subjList = ["Chi", "Eng", "Math"];
    
    $.each( subjList, function( key, subject ) {	
    	 d3.select('#graphDiv').append('br');
    	 d3.select('#graphDiv').append('br');
    	 d3.select('#graphDiv').append('div').attr('id', subject);
    
    // 	 Highcharts.chart('#'+subject, {
    	 $('#'+subject).highcharts({
    	        chart: {
    	            type: 'boxplot'
    	        },
    	        title: {
    	            text: subjectName[subject]
    	        },
    
    	        legend: {
    	            enabled: false
    	        },
    	        xAxis: {
    	            categories: highChart.xAxis,
    	            title: {
    	                text: '<?=$Lang['SDAS']['toCEES']['SchoolYear']?>'
    	            },
    	        },
    	        yAxis: {
    	            title: {
    	                text: '<?=$Lang['SDAS']['Score']?>'
    	            },
    	            min:0,
            	max:100,
    	        },
    
    	        series: [{
    	            name: subjectName[subject],
    	            data: highChart.subjectBoxplot[subject],
    	            tooltip: {
    //	                headerFormat: '<em>'+'<?=$Lang['SDAS']['toCEES']['SchoolYear']?>'+' {point.key}</em><br/>'
    	            }
    	        }]

    	    });
    
    	 d3.select('#SubjectTable').append('div').attr('id', subject+'Table');
    });	 
    
    </script>
    <?php
    intranet_closedb();
}else{
    ?>
        <table border="1" bordercolor="#999999" cellpadding="5" cellspacing="0"
        width="100%">
        <tr>
        <td align="center"><i><?=$i_no_record_exists_msg?></i></td>
    	</tr>
    </table>
    <?php  
    intranet_closedb();
}
?>