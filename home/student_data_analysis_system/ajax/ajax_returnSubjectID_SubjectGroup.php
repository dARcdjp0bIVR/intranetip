<?php 
/*Change Log
 * Date: 2017-05-23 
 * -	Open the File
 */
$json = new JSON_obj();
$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
foreach ((array)$accessRight['classTeacher'] as $_accessRight){
	$ClassTeacher_YearClassID[] = $_accessRight['YearClassID'];
}
foreach ((array)$accessRight['subjectPanel'] as $SubjectID => $Subject_YearID){
	foreach ((array)$Subject_YearID as $_Subject_YearID=>$value){
		$SubjectPanel[$_Subject_YearID][] = $SubjectID;
	}
}

$sql = 'SELECT 
			DISTINCT stcyr.YearID, st.SubjectID 
		FROM SUBJECT_TERM_CLASS stc 
			INNER JOIN SUBJECT_TERM st
				ON	st.SubjectGroupID = stc.SubjectGroupID
			INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION  stcyr
				ON stc.SubjectGroupID = stcyr.SubjectGroupID
		WHERE
			stc.SubjectGroupID = "'.$SubjectGroupID.'"
';
$rs = $objSDAS->returnResultSet($sql);
$ReturnArr['NeedFilter'] = true;
foreach ((array)$rs as $_rs){
	if(in_array($_rs['SubjectID'],(array)$SubjectPanel[$_rs['YearID']])){
		$ReturnArr['SubjectIDArr'][] = (int)$_rs['SubjectID'];
	}
}
$ReturnArr = $json->encode($ReturnArr);
echo $ReturnArr;
return $ReturnArr;
?>