<?php
/*
 * Page using this ajax: Academic Progress
 * 
 * Change Log:
 * Date: 2019-04-24 Anna added str_replace to avoid Cross Site Scripting (Reflected)
 * Date: 2016-12-07 Villa Get_Specific_Class_Selection()
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');


intranet_auth();
intranet_opendb();
$fcm_ui = new form_class_manage_ui();
$lpf = new libPortfolio();
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$AcademicYearID = $_POST['AcademicYearID'];
$YearID = $_POST['YearID'];
$SelectedYearClassID = $_POST['SelectedYearClassID'];
$SelectionID = cleanHtmlJavascript($_POST['SelectionID']);
$OnChange = cleanHtmlJavascript($_POST['OnChange']);
$IsMultiple = stripslashes($_POST['IsMultiple']);
$NoFirst = stripslashes($_POST['NoFirst']);
$IsAll = $_POST['IsAll'];
$special = 0;
if($_POST['ClassID']){
	$ClassIDArr = explode(',',$_POST['ClassID']);
	$special = '1';
}

if($isClassTeacher){
	unset($ClassIDArr);
	foreach($accessRight['classTeacher'] as $Class){
		if($AcademicYearID==$Class['AcademicYearID']){
			$ClassIDArr[] = $Class['YearClassID'];
		}
		$special = '1';
	}
}
if($special){
	echo $fcm_ui->Get_Specific_Class_Selection($AcademicYearID, $YearID, $SelectionID, $ClassIDArr ,$SelectedYearClassID, $OnChange, $NoFirst, $IsMultiple, $IsAll,0);
	
}else{
	echo $fcm_ui->Get_Class_Selection($AcademicYearID, $YearID, $SelectionID, $SelectedYearClassID, $OnChange, $NoFirst, $IsMultiple, $IsAll,0,$Lang['SDAS']['WholeForm']);
}
intranet_closedb();

?>