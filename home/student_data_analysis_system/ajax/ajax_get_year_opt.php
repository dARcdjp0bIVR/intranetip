<?php
/*
 * Change Log:
 * Date 2016-12-06 (Villa)	Copy from profolio
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();

# get Class Level selection
$year_arr = $li_pf->getActivatedForm($ay_id, false);
$html_classlevel_selection = (sizeof($year_arr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($year_arr, "name='yearID'", "", 0, 1, "", 2);

echo $html_classlevel_selection;

//$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
//echo $ayterm_selection_html;

?>