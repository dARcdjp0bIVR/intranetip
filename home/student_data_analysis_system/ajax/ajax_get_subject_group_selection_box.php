<?php
//Using: 
/**
 * Change Log:
 * 2018-06-25 Pun [ip.2.5.9.7.1]
 *  - File Created
 */

$PATH_WRT_ROOT = dirname(__FILE__)."/../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

######## Init START ########
$lpf = new libPortfolio();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$subjectIdFilterArr = '';
if(!$accessRight['admin'] && count($accessRight['subjectPanel'])){
    $subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
}
######## Access Right END ########

#### Get YearTermID START ####
$sql = "SELECT
    YearTermID
FROM
    ACADEMIC_YEAR_TERM
WHERE
    AcademicYearID = '$AcademicYearID'
ORDER BY 
    TermStart
";
$rs = $lpf->returnVector($sql);
$YearTermID = $rs[0];
#### Get YearTermID END ####

### Subject Group Selection START ###
if($accessRight['admin'] ){
    $Subject_Group_Selection = $libSCM_ui->Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID($YearTermID);
}else{
    if(count($accessRight['subjectPanel'])){
        foreach ($accessRight['subjectPanel'] as $subjectPanel =>$_subjectPanel){
            $subjectIDKey[] = $subjectPanel;
        }
    }
    $Subject_Group_Selection = $libSCM_ui->Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID($YearTermID, $accessRight['subjectGroup'],$subjectIDKey);
}
### Subject Group Selection END ###

echo $Subject_Group_Selection;
intranet_closedb();
?>