<?php
//Using: 
/**
 * Change Log:
 * 2017-04-12 Villa
 * -	Show no subject if no result
 * 2017-02-07 Villa
 * -	Support Subject Component
 * 2016-12-23 Villa
 * -	add para to set checkbox name
 * 2016-12-14 Villa
 * -	Open the file
 *
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");

if($intranet_session_language == 'b5'){
	include_once($PATH_WRT_ROOT."lang/lang.b5.utf8.php");
}
intranet_auth();
intranet_opendb();
$subject_selection_html = '';
$lib = new interface_html();
if($CheckBoxName==''){
	$CheckBoxName = 'Subjects';
}
#### Get yearID for class teacher START ####
if(($yearClass || $yearClassId) && !$y_id){
	$objYear = new Year();
	$classArr = $objYear->Get_All_Classes();
	foreach((array)$classArr as $class){
		if($class['ClassTitleEN'] == $yearClass || $class['ClassTitleB5'] == $yearClass || $class['YearClassID'] == $yearClassId){
			$y_id = $class['YearID'];
			break;
		}
	}
	$subject_selection_html .= '<input type="hidden" name="yearID" value="'.$y_id.'" />';
}
#### Get yearID for class teacher END ####

$li_pf = new libpf_asr();
$lpf = new libportfolio();

#### Include Group Subject START ####
$includeCodeIdArr = array_keys((array)$sys_custom['iPortfolio_Group_Subject']);
#### Include Group Subject END ####

#### Remove Group Subject START ####
## MSSCH Cust START ##
$removeCodeIdArr = array();
foreach ((array)$sys_custom['iPortfolio_Group_Subject'] as $codeArr){
	$removeCodeIdArr = array_merge($removeCodeIdArr, $codeArr);
}
## MSSCH Cust END ##
#### Remove Group Subject END ####

$subj_arr = $li_pf->returnAllSubjectWithComponents($ay_id, $y_id, $includeCodeIdArr, $removeCodeIdArr, true);


// $subject_selection_html .= "<tr><td colspan=\"3\"><input type=\"checkbox\" id=\"SubjCheckMaster\" class=\"checkMaster\" /> <label for=\"SubjCheckMaster\" class=\"checkMaster\">{$button_check_all}</label></td></tr>";
if($subj_arr){
	foreach ((array)$subj_arr as $numOfArr =>$value){
		if(isset($sys_custom['SDAS']['SKHTST']['form_student_performance']['hideSubjectArr'])){
			if(in_array( $value['SubjectID'], $sys_custom['SDAS']['SKHTST']['form_student_performance']['hideSubjectArr'])){
				// skip
				continue;
			}
		}
		if($value['SubjectComponentID']){
			$_subj_arr[$numOfArr]['SubjectID'] = $value['SubjectID'].'_'.$value['SubjectComponentID'];
			$_subj_arr[$numOfArr]['SubjectName'] = $value['SubjectName'].'-'.$value['SubjectComponentName'];
			$_subj_arr[$numOfArr]['Type'] = '';
			$_subj_arr[$numOfArr]['Checked'] = true;
			if($sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['isActivate']){
				$_subj_arr[$numOfArr]['Checked'] = false;
			}
		}else{
			$_subj_arr[$numOfArr]['SubjectID'] = $value['SubjectID'].'_'.'0';
			$_subj_arr[$numOfArr]['SubjectName'] = $value['SubjectName'];
			$_subj_arr[$numOfArr]['Checked'] = true;
			if($sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['isActivate']){
				$_subj_arr[$numOfArr]['Checked'] = false;
				if($task == "sbjSelect2" && in_array($value['SubjectID'], $sys_custom['SDAS']['SKHTST']['form_student_performance']['defaultSbjSelect']['defaultSubj2'])){
					$_subj_arr[$numOfArr]['Checked'] = true;
				}else if($task == "sbjSelect1"){
					$_subj_arr[$numOfArr]['Checked'] = true;
				}
			}
		}
	}
	
	$defaultChecked = 1;
	$subject_arr[0][0] = $CheckBoxName.'_0';
	$subject_arr[0][1] = $CheckBoxName.'[]';
	$subject_arr[0][2] = '0';
	$subject_arr[0][3] = $defaultChecked;
	$subject_arr[0][4] = $CheckBoxName.'_arr';
	$subject_arr[0][5] = $ec_iPortfolio['overall_result'];
	
	$i =1;
	foreach ((array)$_subj_arr as $value){
		$subject_arr[$i][0] = $CheckBoxName.$value['SubjectID'];
		$subject_arr[$i][1] = $CheckBoxName.'[]';
		$subject_arr[$i][2] = $value['SubjectID'];
		$subject_arr[$i][3] = $value['Checked'];
		$subject_arr[$i][4] = $CheckBoxName.'_arr';
		$subject_arr[$i][5] = $value['SubjectName'];
		$i++;
	}
	$subject_selection_html = '';
	$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
	$subject_selection_html .= "<tr>";
	$subject_selection_html .= "<td>";
	$subject_selection_html .= $lib->Get_Checkbox_Table($CheckBoxName.'_arr',$subject_arr);
	$subject_selection_html .= "</tr>";
	$subject_selection_html .= "</td>";
	$subject_selection_html .= "</table>";
}else{
	$subject_selection_html = '';
	$subject_selection_html .= "<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" style=\"background: #EEEEEE\">";
	$subject_selection_html .= "<tr>";
	$subject_selection_html .= "<td>";
	$subject_selection_html .= $Lang['SDAS']['Error']['noSubject'];
	$subject_selection_html .= "</tr>";
	$subject_selection_html .= "</td>";
	$subject_selection_html .= "</table>";
}

echo $subject_selection_html;

intranet_closedb();
?>