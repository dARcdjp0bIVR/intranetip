<?php
/*
 * Change Log:
 * Date 2017-07-05 Villa sync the display term name/ optimize the sorting logic #W119800 
 * Date 2017-04-06 Villa add ComplexValue to Return YearTermID_TermAssessment #J114859 
 * Date 2017-03-04 Omas Open the file
 */

include_once($PATH_WRT_ROOT."includes/libpf-asr.php");
intranet_auth();
intranet_opendb();

$li_pf = new libpf_asr();

$sql = "
		SELECT 
			distinct assr.Semester, assr.YearTermID, assr.TermAssessment, ayt.YearTermNameEN, ayt.YearTermNameB5
		FROM
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
		INNER JOIN 
			ACADEMIC_YEAR_TERM ayt
				ON ayt.YearTermID = assr.YearTermID
		WHERE
			assr.AcademicYearID = {$AcademicYearID}
		AND 
			assr.Semester !=''
		ORDER BY
			ayt.TermStart, assr.TermAssessment
		";
			
$rs = $li_pf->returnResultSet($sql);
if($tag){
	//do nth
}else{
	$tag = " id='year_term' name='YearTermID'";
}
$selection_box = "<select $tag>";
$selection_box .= "<option value=''>". $ec_iPortfolio['overall_result'] ."</option>";

foreach ($rs as $_rs){
	if(!empty($_rs['TermAssessment'])){
		if($ComplexValue){
			$selection_box .= "<option value='".$_rs['YearTermID'].'_'.$_rs['TermAssessment']."'>".$_rs['TermAssessment']."</option>";
		}else{
			$selection_box .= "<option value='".$_rs['TermAssessment']."'>".$_rs['TermAssessment']."</option>";
		}
	}else{
		$selection_box .= "<option value='".$_rs['YearTermID']."'>".Get_Lang_Selection($_rs['YearTermNameB5'], $_rs['YearTermNameEN'])."</option>";
	}
}
$selection_box .= "</select>";
echo $selection_box;

?>