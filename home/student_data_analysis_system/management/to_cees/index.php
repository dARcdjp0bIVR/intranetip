<?php
// Modifing by 
################# Change Log [Start] #####
#   Date    : 2018-08-06 Anna
#           - check confirmAppealNoData only for DSE_APPEAL
#
#   Date    :   2018-07-04 Anna
#           - change alert messgae after syncToCEES
#	Date	:	2016-10-24 Omas
#			- remove hidden field for ../../../../../ 
#	Date	:	2016-10-04 Omas
#			- added JUPAS
#	Date	: 2016-07-12 Omas
#			added refresh btn
#	Date	: 2016-03-22 Omas
#			add support to year-end exam
#	Date	:	2016-02-19 Kenneth
#				Page Created
#
################## Change Log [End] ######


######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.CEES";
$CurrentPageName = $Lang['SDAS']['menu']['CEES'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## UI START ########
$linterface->LAYOUT_START();
// $AcademicYearSelection = 'villa';
$AcademicYearSelection = '<label>'.$Lang['SDAS']['toCEES']['SchoolYear'].'</label>'."\r\n";;
$AcademicYearSelection .= getSelectAcademicYear('AcademicYearID', $tag='onchange="loadAjax();"', $noFirst=0, $noPastYear=0);
$AcademicYearSelection .= '&nbsp;<a href="javascript:void(0);" id="refresh_btn" class="tablelink">'.$i_general_refresh.'</a>';
$AcademicYearSelection .= '<span id="ajax_loading"></span>';

# Table Open
$TableOpen =  '<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC" id="ajax_reload_table">';
# Table Close
$TableClose = "</table>";
?>
<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function(){
	loadAjax();
	$('#refresh_btn').click(function(){
		loadAjax();
	});
});
function loadAjax() {
	
	$('#refresh_btn').hide();
	$('#ajax_reload_table').html('');
	var url = '?t=management.to_cees.ajax_reload';
	var ay = $('#AcademicYearID').val();
	$('#ajax_loading').html(ajaxImage);
	$.ajax({
           type:"POST",
           url: url,
           data: { "ay_id" : ay,"action":"reload_table"},
           success:function(responseText)
           {
				$('#ajax_reload_table').html(responseText);
				//$("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['overall_result']?>");
           		$('#ajax_loading').html('');
           		$('#refresh_btn').show();
           }
    });
}
function downloadDataFile(num,academicYearId){
	var hiddenFormURL = $('#hiddenForm').attr('action');
	if(num != 4 ){
		// year end exam id = 4
	}else{
		var newURL = '?t=management.data_import.assessment.data_export_update';
		$('#hiddenForm').attr('action',newURL);	
		$('#report_type').val(0);
	}

	if(num == 6){
		// JUPAS
		var newURL = '?t=management.data_import.jupas.export_update';
		$('#hiddenForm').attr('action',newURL);	
	}

	if(num == 8){
		// EXITTO
		var newURL = '?t=management.data_import.exitto.export_update';
		$('#hiddenForm').attr('action',newURL);	
	}
	
// 	$('#PATH_WRT_ROOT').val('../../../../../');
	$('#exam').val(num);
	$('#academicYearID').val(academicYearId);
	$('#hiddenForm').submit();
	$('#hiddenForm').attr('action',hiddenFormURL);	
	
}

function syncToCEES(exam,academicYearId){
	if($('#confirmAppealNoData').length == 1 && $('#confirmAppealNoData:checked').length == 0 && exam==7){
		alert("<?php echo $Lang['SDAS']['toCEES']['PleaseConfirmAppealNoData']?>");
		console.log('herere22s');
		return false;
	} 

	var url = '?t=management.to_cees.sync';
	var confirmNoData = $('#confirmAppealNoData:checked').length == 1 ? 1:0;
	//$('div#debugOnly').html(ajaxImage); 
	
	$.ajax({
        type:"POST",
        url: url,
        /*async: false,*/
        data: { "academicYearID" : academicYearId ,"exam": exam, "confirmAppealNoData":confirmNoData},
        success:function(responseText)
        {
// 			$('div#debugOnly').html(responseText);
			
			if(responseText==1){
				//Successfully sync
				Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Sync'] ?>');
			}else if(responseText==0){
				//Failed
				Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Sync'] ?>');
			}else{
				alert(responseText);
				}
			loadAjax();
        }
        
 });
}
</script>
 <?= $AcademicYearSelection?>
 <?=$TableOpen.$TableClose?>

<form id="hiddenForm" method="POST" action="?t=management.data_import.exam.data_export_update">
<input type="hidden" name="exam" id="exam" value="">
<input type="hidden" name="academicYearID" id="academicYearID" value="">
<input type="hidden" name="report_type" id="report_type" value="">
</form>
<div id="debugOnly"></div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>