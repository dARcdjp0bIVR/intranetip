<?php
// Modifing by 
################# Change Log [Start] #####
#
################## Change Log [End] ######


######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########

######## Page Setting START ########
$CurrentPage = "management.Endorse";
// $CurrentPageName = $Lang['SDAS']['menu']['CEES'];
$CurrentPageName = $Lang['SDAS']['toCEES']['PrincipalConfirm'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## UI START ########
$linterface->LAYOUT_START();
// $a = '<a href="index.php?t=academic.dse_stats.dse_distribution" class="tablelink"> '.'check DSE'.'</a>';
$instructionBox = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['SDAS']['toCEES']['principalInstruction']);

$AcademicYearSelection = '<label>'.$Lang['SDAS']['toCEES']['SchoolYear'].'</label>'."\r\n";;
$AcademicYearSelection .= getSelectAcademicYear('AcademicYearID', $tag='onchange="loadAjax();"', $noFirst=0, $noPastYear=0);
$AcademicYearSelection .= '&nbsp;<a href="javascript:void(0);" id="refresh_btn" class="tablelink">'.$i_general_refresh.'</a>';
$AcademicYearSelection .= '<span id="ajax_loading"></span>';

?>
<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function(){
	loadAjax();
	$('#refresh_btn').click(function(){
		loadAjax();
	});
});
function loadAjax() {
	$('#refresh_btn').hide();
	$('#ajax_reload_div').html(ajaxImage);
	var url = '?t=management.to_cees.ajax_reload';
	var ay = $('#AcademicYearID').val();
	$('#ajax_loading').html(ajaxImage);
	$.ajax({
           type:"POST",
           url: url,
           data: { "ay_id" : ay,"action":"reload_endorse_table"},
           success:function(responseText)
           {
				$('#ajax_reload_div').html(responseText);
				//$("select[name=YearTermID] option[value='']").text("<?=$ec_iPortfolio['overall_result']?>");
           		$('#ajax_loading').html('');
           		$('#refresh_btn').show();
           }
    });
}
function downloadDataFile(num,academicYearId){
	var hiddenFormURL = $('#hiddenForm').attr('action');
	if(num != 4){
		// year end exam id = 4
	}else{
		var newURL = '?t=management.data_import.assessment.data_export_update';
		$('#hiddenForm').attr('action',newURL);	
		$('#report_type').val(0);
	}
	$('#PATH_WRT_ROOT').val('../../../../../');
	$('#exam').val(num);
	$('#academicYearID').val(academicYearId);
	$('#hiddenForm').submit();
	$('#hiddenForm').attr('action',hiddenFormURL);	
	
}

function endorse(exam,academicYearId){
	if(confirm('<?php echo $Lang['SDAS']['toCEES']['Confirm']?>')){
	var url = '?t=management.to_cees.endorse_update';
		//$('div#debugOnly').html(ajaxImage); 
		$.ajax({
	        type:"POST",
	        url: url,
	        /*async: false,*/
	        data: { "academicYearID" : academicYearId ,"exam": exam },
	        success:function(responseText)
	        {
// 				$('div#debugOnly').html(responseText);
				if(responseText>1){
					//Successfully sync
					Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Sync'] ?>');
				}else if(responseText==0){
					//Failed
					Get_Return_Message('<?= $Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Sync'] ?>');
				}
				loadAjax();
	        }
	 });
	}
}
</script>
<?php echo $instructionBox ?>
<?php //echo $AcademicYearSelection ?>
<?php //echo $TableOpen.$TableClose ?>
<a href="javascript:void(0);" id="refresh_btn" class="tablelink"><?php echo$i_general_refresh?></a>
<div id="ajax_reload_div"></div>
<form id="hiddenForm" method="POST" action="?t=management.data_import.exam.data_export_update">
<input type="hidden" name="exam" id="exam" value="">
<input type="hidden" name="academicYearID" id="academicYearID" value="">
<input type="hidden" name="PATH_WRT_ROOT" id="PATH_WRT_ROOT" value="">
<input type="hidden" name="report_type" id="report_type" value="">
</form>
<div id="debugOnly"></div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>