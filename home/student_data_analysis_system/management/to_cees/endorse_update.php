<?php
################# Change Log [Start] #####
/*
 * Date 2017-07-04 Villa
 * - add sync student info to cees
 */
################## Change Log [End] ######

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/cees/libcees.php");

intranet_opendb();
$jsonObj = new JSON_obj();
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lcees = new libcees();

if($_POST['academicYearID'] != '' && $_POST['exam'] != ''){
	$tempVerifyAry = $lcees->getVerifyStatusFromCentralServer($_POST['academicYearID'], $_POST['exam']);
	$sql = "SELECT AcademicYearID, ExamID, max(ModifiedDate) as lastUpdate from EXAM_DSE_STUDENT_SCORE where AcademicYearID = '{$_POST['academicYearID']}' GROUP BY ExamID, AcademicYearID";
	$lastUpdateAssoc = BuildMultiKeyAssoc($libpf_exam->returnResultSet($sql), array('AcademicYearID','ExamID'),array('lastUpdate'),1 );
	
	// check import & sync time 
	if(strtotime($tempVerifyAry['submitDate']) > strtotime($lastUpdateAssoc[$_POST['academicYearID']][$_POST['exam']])){
		$result = $lcees->sendDseEndorsementToCentral( $_POST['academicYearID'], $_POST['exam'] );
	}else{
		$result['rda']['success'] = 0;
	}
	
	if($result['rda']['success'] && $_POST['exam']=='2'){
		$AcademicYearID = $_POST['academicYearID'];
		//Get Student ID arr from selected academicYear
		$sql = 'Select Distinct StudentID from EXAM_DSE_STUDENT_SCORE WHERE AcademicYearID ='.$AcademicYearID;
		$StudentIDArr = Get_Array_By_Key($objSDAS->returnResultSet($sql), 'StudentID');
		$StudentIDArr_sql = implode(',',(array)$StudentIDArr);
		
		//Get Needed Student Info from student id
		$sql = 'Select WebSAMSRegNo, EnglishName, ChineseName  FROM INTRANET_USER WHERE UserID IN ('.$StudentIDArr_sql.')';
		$StudentInfoArr = $objSDAS->returnResultSet($sql);
		
		if(!empty($StudentInfoArr)){
			//sync data to cees
			$success = $lcees->syncStudentInfo($AcademicYearID,'DSEStudentInfo',$StudentInfoArr);
		}
	}
	echo $result['rda']['success'];
}

intranet_closedb();
?>