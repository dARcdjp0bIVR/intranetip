<?php
// Modifing by 
################# Change Log [Start] #####
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

if(!$plugin['SDAS_module']['advanceMode']){
	No_Access_Right_Pop_Up('','/');
	exit();
}

intranet_opendb();

$linterface = new libPopupInterface("sdas_popup_template.html");
$libpf_exam = new libpf_exam();
$exam_info_arr = BuildMultiKeyAssoc($libpf_exam->EXAM_INFO_ARR,'1',array('0'),1);

# exam selection
$exam_type_selection = getSelectByAssoArray($exam_info_arr,'id="exam" name="exam"');
$report_type_selection_html = $exam_type_selection;

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' ", Get_Current_Academic_Year_ID(), 0, 1, "", 2);

$CurrentPage = "management.importData";
$title = $Lang['SDAS']['ExportJUPASRecord'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="JavaScript">
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';

$(document).ready(function(){
	
  $("#form1").submit(function(){
//     $("#btn_export").attr("disabled","disabled");
  });

  
  $("#btn_close").click(function(){
    self.close();
  });
});
</script>

<FORM action="index.php?t=management.data_import.jupas.export_update" method="POST" name="form1" id="form1">
  <table width="500" border="0" cellspacing="0" cellpadding="5">
    <col width="100">
    <col width="400">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['year']?>:</td>
      <td class="tabletext">
        <table border="0" cellspacing="0" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap' id='ay_cell'><?=$ay_selection_html?>&nbsp;</td>
      		</tr>
      	</table>
      </td>
    </tr>
  </table>
  <br />
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
      
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td>&nbsp;</td>
      <td align="right">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_export?>" id="btn_export">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" id="btn_close">
      </td>
    </tr>
  </table>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
