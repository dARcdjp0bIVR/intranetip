<?php
// Modifing by

################# Change Log [Start] #####
#
#   Date    :   2019-08-05 Bill [2019-0805-1114-22207]
#           - improved for column re-ordering [Programme + Funding Category]
#
#	Date	:	2017-07-14 Omas
#			- improve to detect case-insensitive
#			- programme name will update everytime import
#			- allow Jupas code to be JSxxxx JAxxxx 
#
#	Date	:	2017-05-22 Omas
#			- allowed to use websams regno - #L117176 
#
#	Date	:	2017-02-22 Omas
#			- ucfirst & strtolower to JupasCode No offer checking
#
#	Date	:	2016-10-04 Omas
#			- created this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

intranet_opendb();

$filepath = $_FILES['userfile']['tmp_name'];
$filename = $_FILES['userfile']['name'];

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath)){
	header("Location: index.php?t=management.data_import.jupas.import");
	exit;
}
else{
	$AcademicYearID = $_POST['academicYearID'];
	############################################
	##	Step 0  - read file 				  ##
	############################################
	$libpf_exam = new libpf_exam(EXAMID_JUPAS);
	$luser = new libuser();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV" ||$ext == ".TXT" ){
		// tab seperated
		$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);
		
		// check import file header
		if(!$libpf_exam->checkCompulsoryField($header_row)){
			// wrong file format
			header("Location: index.php?t=management.data_import.jupas.import&xmsg=import_header_failed");
			exit;
		}
		
// 		$compulsoryColumnArr = array('Application No. / WebSAMS RegNo.', 'Offer', 'Funding Category','Band', 'Offer Round', 'Acceptance Status');
		
// 		$similarColumnArr = array('Programme', 'Institution'); //similar_text 
// 		$keyMappingArr = array();
// 		foreach((array)$similarColumnArr as $_columnVal){
// // 			$_columnVal = $similarColumnArr[0];
// 			$arr = array();
// 			foreach((array)$header_row as $_key => $_fileColumn){
// 				similar_text($_columnVal , $_fileColumn, $_similarity);
				
// 				$arr[$_key] = $_similarity;
// 			}
// // 			$programmeKey = array_keys($arr, max($arr));
// // 			$programmeKey = $programmeKey[0];
// 			$_thisKey = array_keys($arr, max($arr));
// 			$keyMappingArr[$_columnVal] = $_thisKey[0];
// 		}
// 		foreach((array)$compulsoryColumnArr as $pos => $columnVal){

// 			if($pos == 0){
// 				$_key = array_search( $columnVal, $header_row );
// 				if($_key==''){
// 					$_key = array_search( 'Application No.', $header_row );
// 				}
// 			}else{
// 				$_key = array_search( $columnVal, $header_row );
// 			}
// 			if($_key !== ''){
// 				$keyMappingArr[$columnVal] = $_key ;
// 			}else{
// 				$keyMappingArr[$columnVal] = -999;
// 			}
// 		}
		
// 		if(in_array(-999, $keyMappingArr)){
			
// 			// missing field
// 			header("Location: index.php?t=management.data_import.jupas.import&xmsg=import_failed");
// 			exit;
// 			// contiue			
// 		}
		$keyMappingArr = array(
			//"Programme" => 7,
            "Programme" => 6,
			"Institution" => 5,
			"Application No. / WebSAMS RegNo." => 0,
			"Offer" => 4,
			//"Funding Category" => 6,
            "Funding Category" => 7,
			"Band" => 8,
			"Offer Round" => 9,
			"Acceptance Status" => 10
		);
	}
	else{
		// wrong file format
		header("Location: index.php?t=management.data_import.jupas.import&xmsg=import_failed");
		exit;
	}

	$insertArr = array();
	$academicYearID = $_POST['academicYearID'];
	####################################################################
	##	1st Step - Insert jupas programme, get student identity first ##
	####################################################################
// 	$offerArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Offer']));
	$offerJupasCodeArr = array_unique(Get_Array_By_Key($data , $keyMappingArr['Offer']));
	$existJupasCodeArr = $libpf_exam->getAllJupasProgramme();
	$offerAssoArr = BuildMultiKeyAssoc($data, $keyMappingArr['Offer']);
	foreach((array)$offerAssoArr as $_jupasCode => $_jupasProgrammeInfoArr){
		//if(!in_array($_jupasCode, (array)$existJupasCodeArr) && preg_match("/(JS)\d+|(JA)\d+|(JS)\w+/",$_jupasCode) && ucfirst(strtolower($_jupasCode)) !='No offer'){
		if(preg_match("/(JS)\d+|(JA)\d+|(JS)\w+/",$_jupasCode) && ucfirst(strtolower($_jupasCode)) !='No offer'){
			// new program
			if(in_array($_jupasCode, (array)$existJupasCodeArr)){
				// if exist. remove it
				$sql = "DELETE FROM JUPAS_PROGRAMME WHERE JupasCode = '$_jupasCode'";
				$libpf_exam->db_db_query($sql);
			}
			$_programmeName = $_jupasProgrammeInfoArr[$keyMappingArr['Programme']];
			$_institution = $_jupasProgrammeInfoArr[$keyMappingArr['Institution']];
			//$_institution_db =  $JUPAS_CONFIG['Institution'][$_institution];
			$_institution_db =  $JUPAS_CONFIG['Institution'][strtoupper($_institution)];
			$_prgrammeType = $libpf_exam->getJupasProgTypeByName($_programmeName);
			$insertProgramArr[] = " ( '$_jupasCode', '".$libpf_exam->Get_Safe_Sql_Query($_programmeName)."', '$_institution_db', '$_prgrammeType' ) ";
		}
// 		else{
// 			// skip old progrm
// 			continue;
// 		}
	}
// 	$institutionArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Institution']));
// 	$fundArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Funding Category']));
// 	$bandArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Band']));
// 	$roundArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Offer Round']));
// 	$statusArr = array_unique(Get_Array_By_Key( $data , $keyMappingArr['Acceptance Status']));
	
	// get student mapping
	//$appNumArr = Get_Array_By_Key( $data , $keyMappingArr['Application No.']);
	$appNumArr = Get_Array_By_Key( $data , $keyMappingArr['Application No. / WebSAMS RegNo.']);
	
	if($_POST['format'] == 'jupasNo'){
		$currentStudent = $luser->getUserIDsByJupasNumArr($appNumArr, 0);
		$archiveStudent = $luser->getUserIDsByJupasNumArr($appNumArr, 1);
		$studentJupasAssoc = BuildMultiKeyAssoc( array_merge((array)$currentStudent, (array)$archiveStudent), 'jupasNo', array('UserID'), 1 );
	}else{
		$currentStudent = $luser->getUserIDsByWebSamsRegNoArr($appNumArr, 0,true);
		$archiveStudent = $luser->getUserIDsByWebSamsRegNoArr($appNumArr, 1,true);
		$studentJupasAssoc = BuildMultiKeyAssoc( array_merge((array)$currentStudent, (array)$archiveStudent), 'WebSamsRegNo', array('UserID'), 1 );
	}
	
	unset($currentStudent);
	unset($archiveStudent);

	############################################
	##	2nd Step - prepare Arr for last step  ##
	############################################
	// $JUPAS_CONFIG can found in libpf-exam-config.php
		
	$insertArr = array();
	
	foreach((array)$data as $_key => $_studentJupasInfoArr){
		
		$_jupasNo = $_studentJupasInfoArr[$keyMappingArr['Application No. / WebSAMS RegNo.']];
		if($_POST['format'] != 'jupasNo'){
			$_jupasNo = strtolower($_jupasNo);
		}
		$_offer = $_studentJupasInfoArr[$keyMappingArr['Offer']];
		
		if($_jupasNo != '' && $_offer != ''){
			$_institution = $_studentJupasInfoArr[$keyMappingArr['Institution']];
			$_funding = $_studentJupasInfoArr[$keyMappingArr['Funding Category']];
			$_band = $_studentJupasInfoArr[$keyMappingArr['Band']];
			$_round = $_studentJupasInfoArr[$keyMappingArr['Offer Round']];
			$_status = $_studentJupasInfoArr[$keyMappingArr['Acceptance Status']];
			
			$_studentId = ($studentJupasAssoc[$_jupasNo] =='') ? -999 : $studentJupasAssoc[$_jupasNo];
			$_offerJupasCode_db = strtoupper($_offer) == 'NO OFFER' ? $JUPAS_CONFIG['Offer']['No offer'] : $_offer;
			$_institution_db =  $JUPAS_CONFIG['Institution'][strtoupper($_institution)];
			$_funding_db = $JUPAS_CONFIG['Funding'][strtoupper($_funding)];
			$_round_db = $JUPAS_CONFIG['Round'][strtoupper($_round)];
			$_status_db = $JUPAS_CONFIG['Status'][strtoupper($_status)];
			
			$insertArr[] = "( '$AcademicYearID', '$_studentId', '$_jupasNo',
							'$_offerJupasCode_db', '$_institution_db', '$_band', 
							'$_funding_db', '$_round_db', '$_status_db',
							 now(),'".$_SESSION['UserID']."', now(),'".$_SESSION['UserID']."' )";

			if($_studentId == -999){
				$error_data[$_key]['user'] = $_jupasNo;
			}
			
			if($_offerJupasCode_db != $JUPAS_CONFIG['Offer']['No offer']){
				if(!preg_match("/(JS)\d+|(JA)\d+|(JS)\w+/",$_offerJupasCode_db)){
					$error_data[$_key]['JUPASinvalid'] = $_offerJupasCode_db;
				}
				if($_institution_db ==''){
					$error_data[$_key]['institution'] = $_institution;
				}
				if($_funding_db ==''){
					$error_data[$_key]['funding'] = $_funding;
				}
				if($_round_db ==''){
					$error_data[$_key]['round'] = $_round;
				}
				if($_status_db ==''){
					$error_data[$_key]['status'] = $_status;
				}
			}
		}else{
			$error_data[$_key]['JUPASempty'] = '';
		}
	}
	############################################
	##	Last Step - changes to db			  ##
	############################################
	if( count($insertProgramArr) > 0 ){
		$insert_prog = $libpf_exam->insertJupasProgrammeData($insertProgramArr);
	}
	if( count($insertArr) > 0){
		// clear same year data first 
		$libpf_exam->deleteJupasStudentData($AcademicYearID);
		$success_insert = $libpf_exam->insertJupasStudentData($insertArr);
	}	
}

$yearInfoArr = BuildMultiKeyAssoc( GetAllAcademicYearInfo(), 'AcademicYearID' );
$yearName = Get_Lang_Selection($yearInfoArr[$_POST['academicYearID']]['YearNameEN'], $yearInfoArr[$_POST['academicYearID']]['YearNameB5'] );
$navigationArr[] = array($yearName,'');
$navigationArr[] = array($Lang['SDAS']['JUPASRecord'] ,'');
$display_content = '';
$display_content .= $linterface->GET_NAVIGATION_IP25($navigationArr);
$display_content .= '<br>';
// Display Result
if(!empty($success_insert) || !empty($success_update) ){
// 	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_add).'</td><td class="tabletext">'.count($insertArr).'</td></tr>';
// 	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.count($updateArr).'</td></tr>';
	$display_content .= '<table border="0" cellpadding="5" cellspacing="0">';
	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_add).'</td><td class="tabletext">'.($success_insert==''?'0':$success_insert).'</td></tr>';
	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.count($success_update).'</td></tr>';
	if(count($error_data) > 0){
		$display_content .= '<tr><td class="tabletext red">'.$Lang['SDAS']['Exam']['Import']['InvalidRecords'].' :'.'</td><td class="tabletext red">'.count($error_data).'</td></tr>';
	}
	$display_content .= '</table>';
}
	
if (sizeof($error_data)!=0){
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	$error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";
	
// 		for ($i=0; $i<sizeof($error_data); $i++)
	$error_count = 0;
	foreach ($error_data as $i => $data_array){
		$t_typeArr = array();
		$t_dataArr = array();
		$t_row = $i;
		if(isset($error_data[$i]['user'])){
			$t_typeArr[] = 'user';
			$t_dataArr[] = $error_data[$i]['user'];
// 			if( $error_data[$i]['user'] == ''){
// 				$t_dataArr = $Lang['iPortfolio']['DynamicReport']['StatisticsArr']['Blank'];
// 			}
		}
		if(isset($error_data[$i]['institution'])){
			$t_typeArr[] = 'institution';
			$t_dataArr[] = $error_data[$i]['institution'];
		}
		if(isset($error_data[$i]['funding'])){
			$t_typeArr[] = 'funding';
			$t_dataArr[] = $error_data[$i]['funding'];
		}
		if(isset($error_data[$i]['round'])){
			$t_typeArr[] = 'round';
			$t_dataArr[] = $error_data[$i]['round'];
		}
		if(isset($error_data[$i]['status'])){
			$t_typeArr[] = 'status';
			$t_dataArr[] = $error_data[$i]['status'];
		}
		if(isset($error_data[$i]['JUPASempty'])){
			$t_typeArr[] = 'JUPASempty';
			$t_dataArr[] = $error_data[$i]['JUPASempty'];
		}
		if(isset($error_data[$i]['JUPASinvalid'])){
			$t_typeArr[] = 'JUPASinvalid';
			$t_dataArr[] = $error_data[$i]['JUPASinvalid'];
		}
			
		if ($t_row==-1){
			$t_row = "Column(s)";
		} else {
// 			if($format == 1 || $format ==''){
				$t_row++;     # set first row to 1
// 			}
		}

		$css_color = ($error_count%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
		$reason_string = $ec_guide['import_error_unknown'];
		$reason_stringArr = array();
		foreach((array) $t_typeArr as $t_type){
			switch ($t_type){
				case 'user':
					$reason_stringArr[] = $ec_guide['import_error_no_user'];
					break;
				default :
					$reason_stringArr[] = $Lang['SDAS']['Error'][$t_type];
					break;
			}
		}
		$reason_string = implode("<br>",$reason_stringArr);
		$error_table .= $reason_string;

		$error_contents = implode("<br>",$t_dataArr);
		$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
		$error_count ++;
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}	
// }

$linterface = new libPopupInterface("sdas_popup_template.html");
$CurrentPage = "management.importData";
$title = $Lang['SDAS']['ImportJUPASRecord'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;
$linterface->LAYOUT_START();
?>
<br>
<?= $display_content ?>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<p>
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='index.php?t=management.data_import.jupas.import'">
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
</p>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>