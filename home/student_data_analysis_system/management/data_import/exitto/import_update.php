<?php
// Modifing by anan
################# Change Log [Start] #####
#
#	Date	:	2018-11-26 Anna
#			- created this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

intranet_opendb();

$filepath = $_FILES['userfile']['tmp_name'];
$filename = $_FILES['userfile']['name'];

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath)){
	header("Location: index.php?t=management.data_import.exitto.import");
	exit;
}
else{
	$AcademicYearID = $_POST['academicYearID'];
	############################################
	##	Step 0  - read file 				  ##
	############################################
	$libpf_exam = new libpf_exam(EXAMID_EXITTO);
	$luser = new libuser();
	$lo = new libfilesystem();
	$limport = new libimporttext();
	
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV" ||$ext == ".TXT" ){
		// tab seperated
		$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);
		
		// check import file header
		if(!$libpf_exam->checkCompulsoryField($header_row)){
			// wrong file format
			header("Location: index.php?t=management.data_import.exitto.import&xmsg=import_header_failed");
			exit;
		}
		$keyMappingArr = array(
// 			"Institution" => 4,
			"Application No. / WebSAMS RegNo." => 0,
			"Final Choice" => 3,
		    "Remarks"=>4
		);
	}
	else{
		// wrong file format
		header("Location: index.php?t=management.data_import.exitto.import&xmsg=import_failed");
		exit;
	}

	$insertArr = array();
	$academicYearID = $_POST['academicYearID'];
	####################################################################
	##	1st Step -get student identity first ##
	####################################################################

	// get student mapping
	$appNumArr = Get_Array_By_Key( $data , $keyMappingArr['Application No. / WebSAMS RegNo.']);
	
	if($_POST['format'] == 'jupasNo'){
		$currentStudent = $luser->getUserIDsByJupasNumArr($appNumArr, 0);
		$archiveStudent = $luser->getUserIDsByJupasNumArr($appNumArr, 1);
		$studentJupasAssoc = BuildMultiKeyAssoc( array_merge((array)$currentStudent, (array)$archiveStudent), 'jupasNo', array('UserID'), 1 );
	}else{
		$currentStudent = $luser->getUserIDsByWebSamsRegNoArr($appNumArr, 0,true);
		$archiveStudent = $luser->getUserIDsByWebSamsRegNoArr($appNumArr, 1,true);
		$studentJupasAssoc = BuildMultiKeyAssoc( array_merge((array)$currentStudent, (array)$archiveStudent), 'WebSamsRegNo', array('UserID'), 1 );
	}
	
	unset($currentStudent);
	unset($archiveStudent);

	############################################
	##	2nd Step - prepare Arr for last step  ##
	############################################
	// $JUPAS_CONFIG can found in libpf-exam-config.php
		
	$insertArr = array();
	$i=0;
	foreach((array)$data as $_key => $_studentJupasInfoArr){
		
		$_jupasNo = $_studentJupasInfoArr[$keyMappingArr['Application No. / WebSAMS RegNo.']];
		if($_POST['format'] != 'jupasNo'){
			$_jupasNo = strtolower($_jupasNo);
		}
		
		$_studentId = ($studentJupasAssoc[$_jupasNo] =='') ? -999 : $studentJupasAssoc[$_jupasNo];
		
		$_finalChoice = $_studentJupasInfoArr[$keyMappingArr['Final Choice']];
		$_remark= $_studentJupasInfoArr[$keyMappingArr['Remarks']];
		if($_finalChoice == 'JUPAS'){
		    $_finalChoiceDb = '1';
		}else if($_finalChoice == 'EMPLOYMENT'){
		    $_finalChoiceDb = '3';
		}else{
		    $_finalChoiceDb = '2';
		}
	
		if($_finalChoiceDb!= ''){
		    
		    $DataAry[$i]['AcademicYearID'] = $AcademicYearID;
		    $DataAry[$i]['StudentID'] = $_studentId;
		    $DataAry[$i]['FinalChoice'] = $_finalChoiceDb;
		    $DataAry[$i]['Remark'] = $_remark;
		    $i++;
			if($_studentId == -999){
				$error_data[$_key]['user'] = $_jupasNo;
			}
			
	
		}else{
			$error_data[$_key]['JUPASempty'] = '';
		}
	}


	############################################
	##	Last Step - changes to db			  ##
	############################################

	if( count($DataAry) > 0){	  
	    $libpf_exam->Delete_ExitTo_Info($AcademicYearID);
	    for($j=0;$j<count($DataAry);$j++){
	        $success_insert = $libpf_exam->Insert_User_ExitTo_Info($DataAry[$j]);
	    }
		
	}	
}

$yearInfoArr = BuildMultiKeyAssoc( GetAllAcademicYearInfo(), 'AcademicYearID' );
$yearName = Get_Lang_Selection($yearInfoArr[$_POST['academicYearID']]['YearNameEN'], $yearInfoArr[$_POST['academicYearID']]['YearNameB5'] );
$navigationArr[] = array($yearName,'');
$navigationArr[] = array($Lang['SDAS']['JUPASRecord'] ,'');
$display_content = '';
$display_content .= $linterface->GET_NAVIGATION_IP25($navigationArr);
$display_content .= '<br>';


// Display Result
if(!empty($success_insert) || !empty($success_update) ){
    $display_content .= '<table border="0" cellpadding="5" cellspacing="0">';
	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_add).'</td><td class="tabletext">'.($success_insert==''?'0':$success_insert).'</td></tr>';
	$display_content .= '<tr><td class="tabletext">'.str_replace(array('.','。'),' :',$con_msg_update).'</td><td class="tabletext">'.count($success_update).'</td></tr>';
	if(count($error_data) > 0){
		$display_content .= '<tr><td class="tabletext red">'.$Lang['SDAS']['Exam']['Import']['InvalidRecords'].' :'.'</td><td class="tabletext red">'.count($error_data).'</td></tr>';
	}
	$display_content .= '</table>';
}
	
if (sizeof($error_data)!=0){
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	$error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";

	$error_count = 0;
	foreach ($error_data as $i => $data_array){
		$t_typeArr = array();
		$t_dataArr = array();
		$t_row = $i;
		if(isset($error_data[$i]['user'])){
			$t_typeArr[] = 'user';
			$t_dataArr[] = $error_data[$i]['user'];

		}
		
		if ($t_row==-1){
			$t_row = "Column(s)";
		} else {
// 			if($format == 1 || $format ==''){
				$t_row++;     # set first row to 1
// 			}
		}

		$css_color = ($error_count%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
		$reason_string = $ec_guide['import_error_unknown'];
		$reason_stringArr = array();
		foreach((array) $t_typeArr as $t_type){
			switch ($t_type){
				case 'user':
					$reason_stringArr[] = $ec_guide['import_error_no_user'];
					break;
				default :
					$reason_stringArr[] = $Lang['SDAS']['Error'][$t_type];
					break;
			}
		}
		$reason_string = implode("<br>",$reason_stringArr);
		$error_table .= $reason_string;

		$error_contents = implode("<br>",$t_dataArr);
		$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
		$error_count ++;
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}	
// }

$linterface = new libPopupInterface("sdas_popup_template.html");
$CurrentPage = "management.importData";
$title = $Lang['SDAS']['ExitToRecord'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;
$linterface->LAYOUT_START();
?>
<br>
<?= $display_content ?>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<p>
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$ec_guide['import_back']?>" onClick="self.location='index.php?t=management.data_import.exitto.import'">
<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
</p>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>