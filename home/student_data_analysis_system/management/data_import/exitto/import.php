<?php
// Modifing by ANNA
################# Change Log [Start] #####
#
#	Date	:	2018-11-23  Anna
#			- created this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

if(!$plugin['SDAS_module']['advanceMode']){
	No_Access_Right_Pop_Up('','/');
	exit();
}

intranet_opendb();
$linterface = new libPopupInterface("sdas_popup_template.html");
$libpf_exam = new libpf_exam();

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' ", Get_Current_Academic_Year_ID(), 0, 1, "", 2);

$mapFieldSelection = $linterface->Get_Radio_Button('format_1','format','jupasNo',1,"", $Lang['AccountMgmt']['HKJApplNo'], "");
$mapFieldSelection .= $linterface->Get_Radio_Button('format_2','format','websamsNo',0,"", $Lang['StudentRegistry']['WebSAMSRegNo'], "");

$CurrentPage = "management.importData";
$title = $Lang['SDAS']['ExitToRecord'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$message = '';
if($xmsg != ''){
	$message =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG($xmsg).'</td></tr>';
}
else if($smsg!= ''){
	$msg = $i_Homework_Error_NoSubject.' : '.$smsg;
	$message =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG('',$msg).'</td></tr>';
}
$linterface->LAYOUT_START();

?>

<script language="JavaScript">
$(document).ready(function(){
//   jChange_Template($('input[name=' + 'exam' + ']:checked').val());
  $("#form1").submit(function(){
    $("#btn_submit").attr("disabled","disabled");
    
	if($("input[name=userfile]").val() == "")
    {
      alert("<?=	$Lang['General']['warnSelectcsvFile']?>");
      $("#btn_submit").removeAttr("disabled");
      return false;
    }
    else
    {
      return true;
    }
  });
});

function jGEN_CSV_TEMPLATE()
{
    window.location.href = 'index.php?t=management.data_import.exitto.export_update&task=exitto_template';
}

</script>

<FORM name="form1" id="form1" enctype="multipart/form-data" action="index.php?t=management.data_import.exitto.import_update" method="POST">
  <table width="500" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="10" colspan="2"><?=$message?></td>
    </tr>
	<tr>
			<td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['year']?></td>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
		      		<tr>
		      			<td nowrap='nowrap'><?=$ay_selection_html?>&nbsp;</td>
		      		</tr>
		      	</table>
		      </td>
	</tr>
    <tr>
      <td align="right" class="tabletext"><?=$Lang['General']['File']?></td>
      <td class="tabletext"><input name="userfile" class="file" type="file" size="20" /></td>
    </tr>
    <tr>
			<td align="right" valign="top" class="tabletext"><?=''?></td>
			<td valign="top"><?=$mapFieldSelection?></td>
	</tr>
    <tr>
    	<td></td>
    	<td><a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE()"><?=$ec_iPortfolio['generate_csv_template']?></td>
    </tr>
    <!-- tr id="format">
    	<td align="right" class="tabletext"><?=$Lang['SDAS']['Exam']['ImportFileFormat']['Title']?></td>
    	<td id="formatSelect"><?php echo $Lang['SDAS']['Exam']['PleaseSelectType']?></td>
    </tr -->
  </table>
  
  <br>
  <span id='Remarks' class='tabletextremark'></span>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="right">
        <input id="btn_submit" class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" >
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
      </td>
    </tr>
  </table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
