<?php
// Modifing by 
################# Change Log [Start] #####
#	Date	:	2017-03-13 Omas
#				fix if classname no chinese name show empty problem
#
#	Date	:	2016-09-13 Omas
#				fix checkform() - infinity loop problem
#
#	Date	:	2016-04-21 Omas
#		 		add if not $plugin['SDAS_module']['advanceMode'] redirect
#
#	Date	:	2016-02-01	Omas
# 				Create this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

// if(!$plugin['SDAS_module']['advanceMode']){
// 	No_Access_Right_Pop_Up('','/');
// 	exit();
// }

intranet_opendb();

$libpf_exam = new libpf_exam();
$linterface = new libPopupInterface("sdas_popup_template.html");

$exam_infoAry = BuildMultiKeyAssoc($libpf_exam->EXAM_INFO_ARR,'1',array('0'),1);


if(!$plugin['SDAS_module']['advanceMode']){
    $exam_info_arr[] = $exam_infoAry[1];
}
else{
    $exam_info_arr = $exam_infoAry;
}


# exam selection
$exam = ($exam=='')? '': $exam;
$exam_type_selection = getSelectByAssoArray($exam_info_arr,'id="exam" name="exam" onChange="this.form.submit()" ',$exam);
$report_type_selection_html = $exam_type_selection;

# academic year selection
$lay = new academic_year();
if($exam!=''){
	$academicYearID = ($academicYearID=='')? '':$academicYearID;
	$academic_year_arr = $lay->Get_All_Year_List();
	$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' onChange='this.form.submit()'", $academicYearID, 0, 0, "", 2);
}
else{
	$ay_selection_html = $iPort["options_select"].$ec_iPortfolio['record_type'];
}

$endorseAlready = false;
if( $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" && ($exam == EXAMID_HKDSE || $exam == EXAMID_HKDSE_APPEAL)){
	include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
	$lcees = new libcees();
	$endorseAlready = $lcees->checkIfEndorsed($academicYearID, $exam);
}

# Get Record num
if($exam!=='' && !empty($academicYearID)){
	$record_num = $libpf_exam->getExamRecordNumV2($exam,$academicYearID);
}

$CurrentPage = "management.importData";
$title = $Lang['SDAS']['Exam']['EraseOtherExam'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="javascript">
// check if year and semester are both available
function checkform(){
	if ( $('#recordNum').html() ==0 || $('#recordNum').html() =='' ){
		alert("<?=$Lang['SDAS']['Exam']['NoRecordToErase']?>");
		return false;
	} 
	else if (!confirm("<?=$ec_warning['info_remove']?>")){
		return false;
	}

// 	$('form#form1').attr('action', 'index.php?t=management.data_import.exam.data_remove_update').submit();
	$('form#form1').attr('action', 'index.php?t=management.data_import.exam.data_remove_update');	
	return true;
}
</script>

<?php $disabled = ($semester_selection_html=="<i>".$no_record_msg."</i>") ? "DISABLED": ""; ?>

<FORM action="" method="POST" name="form1" id="form1" onSubmit="return checkform()">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
          <td height="10" colspan="2">&nbsp;</td>
        </tr>
		<tr>
	      <td align="right" class="tabletext"><?=$ec_iPortfolio['record_type']?>:</td>
	      <td class="tabletext"><?=$report_type_selection_html?></td>
	    </tr>
        <tr>
          <td align="right" class="tabletext"><?=$ec_iPortfolio['year']?>:</td><td class="tabletext"><?= $ay_selection_html ?></td>
        </tr>
		 <tr>
          <td align="right" class="tabletext"><?=$ec_iPortfolio['number_of_record']?>:</td><td class="tabletext"><div id="recordNum"><?= $record_num?></div></td>
        </tr>
        <?php if($endorseAlready){?>
        <tr>
          <td align="right" class="tabletext"></td><td class="tabletext red"><?php echo $Lang['SDAS']['Exam']['Import']['NotAllowRemoveAsEndorse']?></td>
        </tr>		
        <?php }?>
      </table>

	<br />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	<?php if(!$endorseAlready){?>
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_remove?>" name="btn_submit">
		<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
		</td>
	</tr>
	</table>
	<?php }?>
	<input type="hidden" name="year_change" >
	<input type="hidden" name="sem_change" >
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
