<?php 

// Modifing by

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
intranet_opendb();

switch($task){
	case 'getExamFormat':
		if($type == 'undefined' || $type == ''){
			echo $Lang['SDAS']['Exam']['PleaseSelectType'];
		}
		else if($type == EXAMID_HKAT){
			$formatSelection = $linterface->Get_Radio_Button('format_1','format',1,1,"", $Lang['SDAS']['Exam']['ImportFileFormat']['Default'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE('.EXAMID_HKAT.')"> ['.$Lang['SDAS']['Exam']['Template'].']  </a>';
			$formatSelection .= $linterface->Get_Radio_Button('format_2','format',2,0,"", $Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'], "javascript:jsFormatChangeTemplate();");
		}
		else if($type == EXAMID_HKDSE){
			$formatSelection = $linterface->Get_Radio_Button('format_1','format',1,1,"", $Lang['SDAS']['Exam']['ImportFileFormat']['HKEAA'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= $linterface->Get_Radio_Button('format_2','format',2,0,"", $Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE('.EXAMID_HKDSE.')"> ['.$Lang['SDAS']['Exam']['Template'].']</a>';
		}
		else if($type == EXAMID_MOCK){
			$formatSelection = $linterface->Get_Radio_Button('format_1','format',1,1,"", $Lang['SDAS']['Exam']['ImportFileFormat']['Default'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE('.EXAMID_MOCK.')"> ['.$Lang['SDAS']['Exam']['Template'].']</a>';
		}else if($type == EXAMID_HKDSE_APPEAL){
			$formatSelection = $linterface->Get_Radio_Button('format_1','format',1,1,"", $Lang['SDAS']['Exam']['ImportFileFormat']['HKEAA'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= $linterface->Get_Radio_Button('format_2','format',2,0,"", $Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'], "javascript:jsFormatChangeTemplate();");
			$formatSelection .= '<a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE('.EXAMID_HKDSE.')"> ['.$Lang['SDAS']['Exam']['Template'].']</a>';
		}
			echo $formatSelection;
		break;
}

?>