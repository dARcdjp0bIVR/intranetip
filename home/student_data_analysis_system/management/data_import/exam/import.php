<?php
// Modifing by 
################# Change Log [Start] #####
#   Date    :   2019-06-26 Anna
#               OPEN AT exam for all 
#
#	Date	:	2017-08-02	Omas
#				changed DSE appeal logic
#
#	Date	:	2016-06-27	Omas
#				change format & template display method 
#
#	Date	:	2016-04-28	Omas
#				add dse websams template
#
#	Date	:	2016-04-21 Omas
#		 		add if not $plugin['SDAS_module']['advanceMode'] redirect
#
#	Date	:	2016-04-12  Omas
#				added format for dse & hkat
#
#	Date	:	2016-02-26	Omas
# 				support import to archived student
#
#	Date	:	2016-02-01	Omas
# 				Create this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

// if(!$plugin['SDAS_module']['advanceMode']){
// 	No_Access_Right_Pop_Up('','/');
// 	exit();
// }

intranet_opendb();
$linterface = new libPopupInterface("sdas_popup_template.html");
$libpf_exam = new libpf_exam();

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' ", Get_Current_Academic_Year_ID(), 0, 1, "", 2);

$CurrentPage = "management.importData";
$title = $Lang['SDAS']['Exam']['ImportOtherExam'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$exam_info_arr = $libpf_exam->EXAM_INFO_ARR;

$exam_select = '';
//$exam_select .= $linterface->Get_Radio_Button('school_exam', 'school_exam', 'int', 0, "", 'school_exam', "onClick=goIPOimport()",0);
//$exam_select .= '&nbsp;';

foreach((array)$exam_info_arr as $_examInfo){
  
	$_examName = $_examInfo[0];
	$_examValue = $_examInfo[1];
	
	if($_examValue == '1' ){
	    $exam_select .= $linterface->Get_Radio_Button('exam_'.$_examValue, 'exam', $_examValue, 0, "", $_examName, "jChange_Template('$_examValue')",0);
	    $exam_select .= '&nbsp;';
	}
	
	if($_examValue != '1' && $plugin['SDAS_module']['advanceMode']){
	    $exam_select .= $linterface->Get_Radio_Button('exam_'.$_examValue, 'exam', $_examValue, 0, "", $_examName, "jChange_Template('$_examValue')",0);
	    $exam_select .= '&nbsp;';
	}
	
	
	${'exam_link_'.$_examValue} = '<a class="contenttool" href="generate_exam_template.php?Type='.$_examValue.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">下載'.$_examName.' template</a>';
}
//$dse_link = '<a class="contenttool" href="generate_exam_template.php?Type=1"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">下載DSE</a>';
//$hkat_link = '<a class="contenttool" href="generate_exam_template.php?Type=0"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">下載HKAT</a>';


$isArchiveSelection = $linterface->Get_Radio_Button('isArchive_no','isArchive',0,1,"",$Lang['General']['No']);
$isArchiveSelection .= $linterface->Get_Radio_Button('isArchive_yes','isArchive',1,0,"",$Lang['General']['Yes']);

// moved to ajax_task.php
// $formatSelection = $linterface->Get_Radio_Button('format_1','format',1,1,"", $Lang['SDAS']['Exam']['ImportFileFormat']['Default'], "javascript:jsFormatChangeTemplate();");
// $formatSelection .= $linterface->Get_Radio_Button('format_2','format',2,0,"", $Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'], "javascript:jsFormatChangeTemplate();");

// $HKATformatSelection = $linterface->Get_Radio_Button('ATformat_1','ATformat',0,1,"", 'Default Format');
// $HKATformatSelection .= $linterface->Get_Radio_Button('ATformat_2','ATformat',1,0,"", 'WebSAMS Format');
// $DSEformatSelection = $linterface->Get_Radio_Button('DSEformat_1','DSEformat',0,1,"", 'Default Format');
// $DSEformatSelection .= $linterface->Get_Radio_Button('DSEformat_2','DSEformat',1,0,"", 'WebSAMS Format');

$SubjectArr = $libpf_exam->getSubjectCodeAndID();
for($i=0, $i_max=sizeof($SubjectArr); $i<$i_max; $i++)
{
	list($s_code, $cmp_code, $s_name, $cmp_name) = $SubjectArr[$i];
	$subjectID = $SubjectArr[$i]['SubjectID'];
	$SubjectOption .= ($cmp_code=="") ? "<OPTION value='$subjectID'>$s_name</OPTION>\n" : "<OPTION value='".$subjectID."'>$s_name&nbsp;-&nbsp;$cmp_name</OPTION>\n";
}
$x = '<select name="source[]" size=15 multiple style="width:100%">';
$x .= $SubjectOption;
$x .= '</select>';

$message = '';
if($xmsg != ''){
	$message =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG($xmsg).'</td></tr>';
}
else if($smsg!= ''){
	$msg = $i_Homework_Error_NoSubject.' : '.$smsg;
	$message =  '<tr><td colspan="2" align="right" valign="bottom">'.$linterface->GET_SYS_MSG('',$msg).'</td></tr>';
}
$linterface->LAYOUT_START();

?>

<script language="JavaScript">
$(document).ready(function(){
//   jChange_Template($('input[name=' + 'exam' + ']:checked').val());
  $("#form1").submit(function(){
    $("#btn_submit").attr("disabled","disabled");
    
	if($('input[name=' + 'exam' + ']:checked').length != 1)
	{
		alert("<?=$Lang['SDAS']['Exam']['PleaseSelectType']?>");
		$("#btn_submit").removeAttr("disabled");
	    return false;
	}		
	else if($("input[name=userfile]").val() == "")
    {
      alert("<?=	$Lang['General']['warnSelectcsvFile']?>");
      $("#btn_submit").removeAttr("disabled");
      return false;
    }
    else
    {
      return true;
    }
  });
});

var ajaxReturned = true;
function jChange_Template(type){
	var url = 'index.php?t=management.data_import.exam.ajax_task';

	if(ajaxReturned){
		ajaxReturned = false;
		$('td#formatSelect').html('<?php echo $linterface->Get_Ajax_Loading_Image() ?>');

		setTimeout(function(){
			$.ajax({
		        type:"POST",
		        async: false,
		        url: url,
		        data:  {
					"task"		:	"getExamFormat",
					"type" 	:	type
					},
		        success:function(data)
		        {
		        	$('td#formatSelect').html(data);
		        	ajaxReturned = true;
		        }
	      })
		},50);
	}

	if(type == <?=EXAMID_HKDSE?> ){
		$('span#Remarks').html("<?php echo addslashes($Lang['SDAS']['Exam']['FormatRemarks']['HKEAA'])?>");
	}
	else{
		$('span#Remarks').html("<?php echo addslashes($Lang['SDAS']['Exam']['FormatRemarks']['Regno'])?>");
	}
// 	// HKAT template
// 	$("#HKAT_template").hide();
// 	$("#Mock_template").hide();
// 	$("#DSE_websams_template").hide();
// 	$("#format").hide();
	
//	if(type== <?=EXAMID_HKAT?>){
// 		$("#HKAT_template").show();
// 		$("#format").show();
// 		jsFormatChangeTemplate();
// 	}
//	else if(type == <?=EXAMID_MOCK?>){
// 		$("#Mock_template").show();
// 	}
//	else if(type == <?=EXAMID_HKDSE?>){
// 		$("#format").show();
// 		jsFormatChangeTemplate();
// 	}
}

function jsFormatChangeTemplate(){
	// 
	if($("#exam_1:checked").length  == 1){
    	if( $("#format_1:checked").length  == 1 ){
    		$("#HKAT_template").show();
    	}
    	else{
    		$("#HKAT_template").hide();
    	}
	}
	else if($("#exam_2:checked").length  == 1){
		if( $("#format_2:checked").length  == 1){
			$("#DSE_websams_template").show();
		}
		else{
			$("#DSE_websams_template").hide();
		}
	}
}

function jGEN_CSV_TEMPLATE(type)
{
	if(type == <?=EXAMID_MOCK?>){
		newWindow('index.php?t=management.data_import.assessment.generate_csv_template', 7);
	}
	else if(type == <?=EXAMID_HKAT?>){
//     newWindow('index.php?t=management.data_import.exam.data_export_update&task=HKAT_template', 7);
    	window.location.href = 'index.php?t=management.data_import.exam.data_export_update&task=HKAT_template';
    }
	else if(type == <?=EXAMID_HKDSE?>){
		window.location.href = 'index.php?t=management.data_import.exam.data_export_update&task=HKDSE_websams_template';
	}
}

</script>

<FORM name="form1" id="form1" enctype="multipart/form-data" action="index.php?t=management.data_import.exam.import_update" method="POST">
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="10" colspan="2"><?=$message?></td>
    </tr>
    <tr>
    	<td align="right" valign="top" class="tabletext">
    		<?=$ec_iPortfolio['record_type']?>
    	</td>
    	<td valign="top">
			<?=$exam_select?>
		</td>
    </tr>
	<tr>
			<td align="right" valign="top" class="tabletext"><?=$ec_iPortfolio['year']?></td>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
		      		<tr>
		      			<td nowrap='nowrap'><?=$ay_selection_html?>&nbsp;</td>
		      		</tr>
		      	</table>
		      </td>
	</tr>
    <tr>
      <td align="right" class="tabletext"><?=$Lang['General']['File']?></td>
      <td class="tabletext"><input name="userfile" class="file" type="file" size="20" /></td>
    </tr>
    
    <tr id="format">
    	<td align="right" class="tabletext"><?=$Lang['SDAS']['Exam']['ImportFileFormat']['Title']?></td>
    	<td id="formatSelect"><?php echo $Lang['SDAS']['Exam']['PleaseSelectType']?></td>
    </tr>
    <!-- 
    <tr>
      <td align="right" class="tabletext"><?=$Lang['SDAS']['Exam']['archivedStudent']?></td>
      <td class="tabletext"><?=$isArchiveSelection ; ?></td>
    </tr>
     -->
    <!-- 
    <tr>
    	<td></td>
    	<td><div id="exam_template"></div></td>
    </tr>
	<tr id="HKAT_template" style="display:none;">
    	<td></td>
    	<td><a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE(<?=EXAMID_HKAT?>)"><?=$ec_iPortfolio['generate_csv_template']?></td>
    </tr>
    <tr id="Mock_template" style="display:none;">
    	<td></td>
    	<td><a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE(<?=EXAMID_MOCK?>)"><?=$ec_iPortfolio['generate_csv_template']?></td>
    </tr>
    <tr id="DSE_websams_template" style="display:none;">
    	<td></td>
    	<td><a class="contenttool" href="javascript:jGEN_CSV_TEMPLATE(<?=EXAMID_HKDSE?>)"><?=$ec_iPortfolio['generate_csv_template']?></td>
    </tr>
     -->
  </table>
  
  <br>
  <span id='Remarks' class='tabletextremark'></span>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="right">
        <input id="btn_submit" class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_submit?>" >
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" onClick="self.close()">
      </td>
    </tr>
  </table>
	<input type="hidden" name="ClassName" value="<?=$ClassName?>" >
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
