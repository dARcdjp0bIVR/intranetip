<?php
// Modifing by 
################# Change Log [Start] #####
#	Date	:	2017-06-16 Villa
#				modified ExportALL - add DSE appeal		
#
#	Date	:	2016-09-13 Villa
#				update the button clicking event using 'on' method in stead of live method
#				fix export button always be disable problem
#
#	Date	:	2016-04-21 Omas
#		 		add if not $plugin['SDAS_module']['advanceMode'] redirect
#
#	Date	:	2016-04-12	Omas
#				if no class is retrieve, tick all can export student without class
#
#	Date	:	2016-02-01	Omas
# 				Create this page
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");

// if(!$plugin['SDAS_module']['advanceMode']){
// 	No_Access_Right_Pop_Up('','/');
// 	exit();
// }

intranet_opendb();

$linterface = new libPopupInterface("sdas_popup_template.html");
$libpf_exam = new libpf_exam();
$exam_infoAry = BuildMultiKeyAssoc($libpf_exam->EXAM_INFO_ARR,'1',array('0'),1);


if(!$plugin['SDAS_module']['advanceMode']){
    $exam_info_arr[] = $exam_infoAry[1];
}
else{
    $exam_info_arr = $exam_infoAry;
}

# exam selection
$exam_type_selection = getSelectByAssoArray($exam_info_arr,'id="exam" name="exam"');
$report_type_selection_html = $exam_type_selection;

# academic year selection
$lay = new academic_year();
$academic_year_arr = $lay->Get_All_Year_List();
$ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYear' ", Get_Current_Academic_Year_ID(), 0, 1, "", 2);

$CurrentPage = "management.importData";
$title = $Lang['SDAS']['Exam']['ExportOtherExam'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="JavaScript">
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
function jLOAD_CLASS()
{
  var academicYearID = $("select[name=academicYearID]").val();
  var examID = $("select[name=exam]").val();
  
  if(academicYearID == "" || examID == "")
  {
		if(examID==""){			
	    	$("#class_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['record_type']?>");
		}
		else{
	    	$("#class_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
		}
  }
  else
  {
  	$.ajax({
  		type: "POST",
  		url: "index.php?t=management.data_import.exam.ajax",
  		data: {
				"academicYearID":	academicYearID,
				"examID" 		:	examID
			},
  		beforeSend: function () {
  		  $("#class_cell").html(ajaxImage);
     	},
  		success: function (msg) {
          $("#class_cell").html(msg);
  		}
  	});
  }
}

function jSET_SUBMIT_BUTTON(){
  if(checkform())
  {
    //$("#btn_export").removeAttr("disabled");
    $("#btn_export").removeAttr("disabled");
  }
  else
  {
    $("#btn_export").attr("disabled","disabled");
  }
}

function checkform()
{
	if(typeof $("select[name=academicYearID]").val()=="undefined")
	{
		return false;
	}
	
	if($("input[name^=ClassName]").length==0 && $('#YCCheckMaster:checked').length == 0)
	{
   		return false;
    }

  return true;
}

$(document).ready(function(){
	$("#exportAll").hide();
	
  jSET_SUBMIT_BUTTON();
  jLOAD_CLASS();
  
  $("#exam").change(function(){
	jSET_SUBMIT_BUTTON();
	jLOAD_CLASS();

	if( $("#exam").val() == 2 || $("#exam").val() == 7){
		$("#exportAll").show();
	}
	else{
		$("#exportAll").hide();
	}
  });

  $( document ).on( "click", "select[name=academicYearID]", function() {
	  	jLOAD_CLASS(); // jQuery 1.7+
	});
//   $("select[name=academicYearID]").live("click", function(){
//     jLOAD_CLASS();
//   });

    $( document ).on( "click", "input[id=YCCheckMaster]", function() {
    	var checked = $(this).attr("checked");
        $("input[name^=ClassName]").attr("checked", checked);
        jSET_SUBMIT_BUTTON();
	});
	
//   $("input[id=YCCheckMaster]").live("click", function(){
//     var checked = $(this).attr("checked");
//     $("input[name^=ClassName]").attr("checked", checked);
//     jSET_SUBMIT_BUTTON();
//   });
  
    $( document ).on( "click", "input[name^=ClassName]", function() {
    	jSET_SUBMIT_BUTTON();
	});
//   $("input[name^=ClassName]").live("click", function(){
//     jSET_SUBMIT_BUTTON();
//   });
  
  $("#form1").submit(function(){
    $("#btn_export").attr("disabled","disabled");
  
    if(checkform())
    {
      var checked = ($("input[name^=ClassName]:checked").length > 0);
      var selected_type = $('#exam').val();

      if(!checked && $('#YCCheckMaster:checked').length ==1){
          // for previous student .. all don't have class before
		checked = true;
      }

      if(selected_type == ''){
    	  alert('here');
    	  $("#btn_export").attr("disabled", "");
    	  return false;
      }
      
      if(!checked)
      {
        alert(globalAlertMsg14);
  		  //$("#btn_export").removeAttr("disabled");
  		  $("#btn_export").attr("disabled", "");
        return false;
      }
      else {
      	$("#btn_export").attr("disabled", "");
      }
    }
    else
    {
      alert("<?=$ec_warning['eclass_data_no_record']?>");
  		//$("#btn_export").removeAttr("disabled");
  		$("#btn_export").attr("disabled", "");
  		return false;
    }
  });

  $("#exportAll").click(function(){
	  window.location = 'index.php?t=management.data_import.exam.data_export_update&academicYearID='+ $("#academicYear").val() +'&task=exportDSE_format2'+'&exam='+ $("#exam").val();
  });
  
  $("#btn_close").click(function(){
    self.close();
  });
});
</script>

<FORM action="index.php?t=management.data_import.exam.data_export_update" method="POST" name="form1" id="form1">
  <table width="500" border="0" cellspacing="0" cellpadding="5">
    <col width="100">
    <col width="400">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['record_type']?>:</td>
      <td class="tabletext"><?=$report_type_selection_html?></td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['year']?>:</td>
      <td class="tabletext">
        <table border="0" cellspacing="0" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap' id='ay_cell'><?=$ay_selection_html?>&nbsp;</td>
      		</tr>
      	</table>
      </td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['class']?>:</td>
      <td class="tabletext" id='class_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
    </tr>
    <!--  
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['number_of_record']?>:</td>
      <td class="tabletext" id='recnum_cell'>0</td>
    </tr>
    -->		
  </table>
  <br />
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
      
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td>&nbsp;</td>
      <td align="right">
      <input id="exportAll" class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$Lang['SDAS']['DSE']['exportAll']?>">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_export?>" id="btn_export">
        <!--<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">-->
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" id="btn_close">
      </td>
    </tr>
  </table>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
