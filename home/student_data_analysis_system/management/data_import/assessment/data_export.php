<?php
// Modifing by 
################# Change Log [Start] #####
#	Date	:	2016-12-02 Villa
#				Fix check all remove check function
#				Change the Semester from whole Semester only to semester with assessment semester				
#
#	Date	:	2016-09-13 Villa
#				update the button clicking event using 'on' method in stead of live method
#				fix export button always be disable problem
#
#	Date	:	2016-04-13  Omas
#				async change to default : true
#	Date	:	2016-02-01	Omas
# 				Copy from iPortfolio
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-asr.php");

intranet_opendb();

$linterface = new libPopupInterface("sdas_popup_template.html");
$lpf = new libpf_asr();

# report type selection
$report_type_selection_html = "<SELECT name='report_type' id='report_type'>";
$report_type_selection_html .= "<OPTION value=0 CHECKED>".$ec_iPortfolio['subject_result']."</OPTION>";
$report_type_selection_html .= "<OPTION value=1>".$ec_iPortfolio['student_overall_result']."</OPTION>";
$report_type_selection_html .= "</SELECT>";

$CurrentPage = "management.importData";
$title = $ec_iPortfolio['export_assessment_record'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $title;

$linterface->LAYOUT_START();

?>

<script language="JavaScript">

function jLOAD_AYEAR()
{
  var report_type = $("select[name=report_type]").val();


	$.ajax({
		type: "POST",
		url: "/home/portfolio/ajax/ajax_get_assess_ay_option.php",
		data: "report_type="+report_type,
		//async: false,
		beforeSend: function () {
		  $("#ay_cell").html('<img src="/images/<?php echo $LAYOUT_SKIN;?>/indicator.gif" />');
    },
		success: function (msg) {
      $("#ay_cell").html(msg);
		}
	});
}

function jLOAD_SEMESTER()
{
  var ay_id = $("select[name=academicYearID]").val();
  
  if(ay_id == "")
  {
    $("#ayterm_cell").html('&nbsp;');
    return;
  }
  
	$.ajax({
		type: "GET",
// 		url: "/home/portfolio/ajax/ajax_get_yearterm_opt.php",
		url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id,
		//async: false,
		beforeSend: function () {
		  $("#ayterm_cell").html('<img src="/images/<?php echo $LAYOUT_SKIN;?>/indicator.gif" />');
    },
		success: function (msg) {
      $("#ayterm_cell").html(msg);
		}
	});
}

function jLOAD_CLASS()
{
  var ay_id = $("select[name=academicYearID]").val();
  var report_type = $("select[name=report_type]").val();
  
  if(ay_id == "")
  {
    $("#class_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
  }
  else
  {
  	$.ajax({
  		type: "GET",
  		url: "/home/portfolio/ajax/ajax_get_assess_class_multiopt.php",
  		data: "ay_id="+ay_id+"&report_type="+report_type+"&returnClassWithStudentOnly=0",
  		//async: false,
  		beforeSend: function () {
  		  $("#class_cell").html('<img src="/images/<?php echo $LAYOUT_SKIN;?>/indicator.gif" />');
      },
  		success: function (msg) {
        $("#class_cell").html(msg);
  		}
  	});
  }
}

function jLOAD_RECORD_NUM()
{
//   var ay_id = $("select[name=academicYearID]").val();
//   var report_type = $("select[name=report_type]").val();
  
//   if($("input[name^=ClassName]:checked").length == 0)
//   {
//     $("#recnum_cell").html('0');
//   }
//   else
//   {
//   	$.ajax({
//   		type: "POST",
//   		url: "/home/portfolio/ajax/ajax_get_assess_recnum.php",
//   		data: $("#form1").serialize(),
//   		async: false,
//   		beforeSend: function () {
//   		  $("#recnum_cell").html('<img src="/images/<?php echo $LAYOUT_SKIN;?>/indicator.gif" />');
//       },
//   		success: function (msg) {
//         $("#recnum_cell").html(msg);
//   		}
//   	});
//   }
}

function jSET_SUBMIT_BUTTON(){
  if(checkform())
  {
    $("#btn_export").removeAttr("disabled");
    //alert("checkform");
    //$("#btn_export").attr("disabled","");
  }
  else
  {
// 	alert("disable");
    $("#btn_export").attr("disabled","disabled");
  }
}

function checkform()
{
	if(typeof $("select[name=academicYearID]").val()=="undefined")
	{
		return false;
	}
	
	if($("input[name^=ClassNames]").length==0)
	{
    return false;
  }
  
  return true;
}

$(document).ready(function(){
  jLOAD_AYEAR();
  jSET_SUBMIT_BUTTON();
  
  $("#report_type").change(function(){
    jLOAD_AYEAR();
    jLOAD_SEMESTER();
    jLOAD_CLASS();
    jLOAD_RECORD_NUM();
  });
  
  $( document ).on( "click", "select[name=academicYearID]", function() {
	jLOAD_SEMESTER();
    jLOAD_CLASS();
    jLOAD_RECORD_NUM();  // jQuery 1.7+
	});
//   $("select[name=academicYearID]").live("click", function(){
//     jLOAD_SEMESTER();
//     jLOAD_CLASS();
//     jLOAD_RECORD_NUM();
//   });
  $( document ).on( "click", "select[name=YearTermID]", function() {
	  jLOAD_RECORD_NUM();  // jQuery 1.7+
	});
//   $("select[name=YearTermID]").live("click", function(){
//     jLOAD_RECORD_NUM();
//   });
  $( document ).on( "click", "input[id=YCCheckMaster]", function() {
		var checked = $(this).attr("checked");
		if(checked =='checked'){
			$(this).parent().parent().parent().find("input[name^=ClassNames]").attr("checked", checked);
		}else{
			$(this).parent().parent().parent().find("input[name^=ClassNames]").removeAttr("checked");
		}
// 		$(this).parent().parent().parent().find("input[name^=ClassNames]").attr("checked", checked);
		jLOAD_RECORD_NUM();
		jSET_SUBMIT_BUTTON();  // jQuery 1.7+
	});
//   $("input[id=YCCheckMaster]").live("click", function(){
//     var checked = $(this).attr("checked");
//     $(this).parent().parent().parent().find("input[name^=ClassNames]").attr("checked", checked);
//     jLOAD_RECORD_NUM();
//     jSET_SUBMIT_BUTTON();
//   });

 	 $( document ).on( "click", "input[name^=ClassName]", function() {
 		jLOAD_RECORD_NUM();
	     jSET_SUBMIT_BUTTON();
	});
//   $("input[name^=ClassName]").live("click", function(){
//     jLOAD_RECORD_NUM();
//     jSET_SUBMIT_BUTTON();
//   });






  
  $("#form1").submit(function(){
    $("#btn_export").attr("disabled","disabled");
  
    if(checkform())
    {
      var checked = ($("input[name^=ClassNames]:checked").length > 0);
      
      if(!checked)
      {
        alert(globalAlertMsg14);
  		  //$("#btn_export").removeAttr("disabled");
  		  $("#btn_export").attr("disabled", "");
        return false;
      }
      else {
      	$("#btn_export").removeAttr("disabled");
      }
    }
    else
    {
      alert("<?=$ec_warning['eclass_data_no_record']?>");
  		//$("#btn_export").removeAttr("disabled");
  		$("#btn_export").attr("disabled", "");
  		return false;
    }
  });
  
  
  $("#btn_close").click(function(){
    self.close();
  });
});
</script>

<FORM action="index.php?t=management.data_import.assessment.data_export_update" method="POST" name="form1" id="form1">
  <table width="500" border="0" cellspacing="0" cellpadding="5">
    <col width="100">
    <col width="400">
    <tr>
      <td height="10" colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['report_type']?>:</td>
      <td class="tabletext"><?=$report_type_selection_html?></td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['year']?>:</td>
      <td class="tabletext">
        <table border="0" cellspacing="0" cellpadding="0">
      		<tr>
      			<td nowrap='nowrap' id='ay_cell'><?=$ay_selection_html?>&nbsp;</td>
      			<td nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></td>
      		</tr>
      	</table>
      </td>
    </tr>
    <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['class']?>:</td>
      <td class="tabletext" id='class_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
    </tr>
<!-- <tr>
      <td align="right" class="tabletext"><?=$ec_iPortfolio['number_of_record']?>:</td>
      <td class="tabletext" id='recnum_cell'>0</td>
    </tr> -->    		
  </table>
  <br />
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
    </tr>
  </table>
      
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td>&nbsp;</td>
      <td align="right">
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="submit" value="<?=$button_export?>" id="btn_export">
        <!--<input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="reset" value="<?=$button_reset?>">-->
        <input class="formbutton" onMouseOver="this.className='formbuttonon';" onMouseOut="this.className='formbutton';" type="button" value="<?=$button_cancel?>" id="btn_close">
      </td>
    </tr>
  </table>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
