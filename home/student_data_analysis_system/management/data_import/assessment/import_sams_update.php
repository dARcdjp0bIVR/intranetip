<?php
// Modifing by  
################# Change Log [Start] #####
#   Date    :   2019-06-27 Anna  [#161604]
#               Added checking header row is in order to prevent client delete OMC, directly import OMF
#
#	DAte	:	2017-07-13	Omas
#				fix subject code not found warning cannot show problem
#
#	Date	:	2017-03-02	Omas
#				accept Score - , *  store as -1  ( - : absent not count for score, * : dropped)
#
#	Date	:	2016-12-15	Omas
#				retrieve current student before archive student - X110117
#
#	Date	:	2016-12-06	Omas
#				added term checking with the file - search Term Validation
#
#	Date	:	2016-06-27	Omas
#				classname & subject short form mapping are now case insensitive
#				import archived and not-archived student once together
#				classname & subjectCode will get back original from db rather than using csv value as ASSESSMENT_STUDENT_SUBJECT_RECORD value
#
#	Date	:	2016-06-14	Omas
#				archived student websamsNo checking now is case insensitive
#
#	Date	:	2016-03-04	Omas
#				Add support to archived student [#L93508] - only for SDAS 
#
#	Date	:	2016-02-26	Omas
# 				Fix jump to iPo problem
#
#	Date	:	2016-02-01	Omas
# 				Copy from iPortfolio
#
################## Change Log [End] ######

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");
intranet_opendb();

define ("ERROR_TYPE_NO_MATCH_REGNO", 1);
define ("ERROR_TYPE_EMPTY_REGNO", 2);
define ("ERROR_TYPE_INCORRECT_REGNO", 3);
define ("ERROR_TYPE_INCORRECT_CLASSNAME", 4);
define ("ERROR_TYPE_NO_SUBJECT", 5);
define ("SBJ_CMP_SEPARATOR","###");

$IsMultipleAssessment = false;


function Standardize_Import_Data($Score, $forGrade=false) 
{
	$Score = trim($Score);
	
	if (!$forGrade) {
		$Score = str_replace('*', '', $Score);
	}
	
	return $Score;
}

function ParseAssementCode($text)
{
	return trim(str_replace("T4", "", str_replace("T3", "", str_replace("T2", "", str_replace("T1", "", $text)))));
}


if(!empty($academicYearID))
{
  $ay = new academic_year($academicYearID);
  $targetYear = $ay->YearNameEN;
}


if(!empty($YearTermID))
{
  $ayt = new academic_year_term($YearTermID);
  $targetSemester = $ayt->YearTermNameEN;
}
else
{
  $YearTermID = "NULL";
}

# Get Classes Info of this academic year
$libfcm = new form_class_manage();
$ClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year($academicYearID);
$numOfClass = count($ClassInfoArr);

// $ClassInfoAssoArr[$ClassNameEn / $ClassNameCh] = $YearClassID
$ClassInfoAssoArr = array();
for ($i=0; $i<$numOfClass; $i++)
{
	$thisYearClassID = $ClassInfoArr[$i]['YearClassID'];
	$thisClassNameEn = $ClassInfoArr[$i]['ClassTitleEN'];
	$thisClassNameCh = $ClassInfoArr[$i]['ClassTitleB5'];

// change to case insensitive
// 	$ClassInfoAssoArr[$thisClassNameEn] = $thisYearClassID;
// 	$ClassInfoAssoArr[$thisClassNameCh] = $thisYearClassID;
	$ClassInfoAssoArr[strtolower($thisClassNameEn)] = $thisYearClassID;
	$ClassInfoAssoArr[strtolower($thisClassNameCh)] = $thisYearClassID;
	$ClasReverseAssoArr[$thisYearClassID] = $thisClassNameEn;
}
unset($ClassInfoArr);

$OtherFailureDue2SubjectCode = 0;

if ($g_encoding_unicode) {
	$import_coding = ($g_chinese == "") ? "b5" : "gb";
}

# Param: $targetYear, $targetSemester, $IsAnnual
# uploaded file information
if(!$skipChecking){
	$filepath = $userfile;
	$filename = $userfile_name;
	$notPOSTfile = !is_uploaded_file($filepath);
}else{
	$folderPrefix = $intranet_root."/file/import_temp/sdas/assessment_result";
	$filepath = $folderPrefix.'/'.$tmpFileName;
	$filename = $tmpFileName;
	$notPOSTfile = false;
}

$del_log = array();

// if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
if($filepath=="none" || $filepath == "" || $notPOSTfile)
{
	header("Location: index.php?t=management.data_import.assessment.import_sams");
}
else
{
	$lpf = new libpf_sturec();
	$libscm = new subject_class_mapping();
	
	$CurrentPage = "management.importData";
	$title = $ec_iPortfolio['SAMS_import_assessment'];
	$TAGS_OBJ[] = array($title,"");
	$MODULE_OBJ["title"] = $title;
	
	$linterface = new libPopupInterface("sdas_popup_template.html");
	$linterface->LAYOUT_START();
	
	$li = new libdb();
	$lo = new libfilesystem();
	$limport = new libimporttext();

	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV")
	{
		$data = $lo->file_read_csv($filepath);// hdebug_r($data);
		//$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);                   # drop the title bar
	}
	# Check Title Row
	# Check first 9 rows

	################################################################
	# Fill code here
	# Fixed 1st-9th: *School ID, *School Year, *School Level, *School Session, *Class Level,
	# *Class        *Class Number        *Student Name        *Reg. No.
	#
	################################################################

	# Set and validate for CSV header
	$DefaultHeader = array(
													"School ID",
													"School Year",
													"School Level",
													"School Session",
													"Class Level",
													"Class",
													"Class Number",
													"Student Name",
													"Reg. No."
												);
	$limport->SET_CORE_HEADER($DefaultHeader);
	$numOfColumn = count($header_row);
	for ($i=0; $i<$numOfColumn; $i++) {
		$header_row[$i] = Standardize_Import_Data($header_row[$i]);
	}
	$correct_header = $limport->VALIDATE_HEADER(array($header_row), false);
	
	############### Term Validation (Start) ##############
	$existTermAssessment = array();
	$existTerm = array();
	for($i = 9; $i<count($header_row); $i++){
		$target = $header_row[$i];
		$temp = explode("_", $target);
		$taIndication = $temp[0];
		
		if($taIndication == 'CS'){
			//skip;
			continue;
		}
		
		if($taIndication != 'An'){
			preg_match_all('/([\d]+)/', $taIndication, $match);
			$termNoFromFile = $match[0][0];
		}else{
			$termNoFromFile = 'An';
		}
		
		if(!in_array($taIndication,$existTermAssessment)){
			$existTermAssessment[] = $taIndication;
		}
		if(!in_array($termNoFromFile,$existTerm)){
			$existTerm[] = $termNoFromFile;
		}
	}
	
	$isOnlyOneTerm = (count($existTermAssessment) == 1 && count($termNoFromFile) == 1);  
	
	if($isOnlyOneTerm && $correct_header && !$skipChecking){
		$sql = "SELECT * FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$academicYearID' order by TermStart";
		$termInfoArr = $li->returnResultSet($sql);
		foreach((array)$termInfoArr as $_key => $_arr){
			$termInfoArr[$_key]['SequenceNumber'] = ($_key+1);
		}
		$yearTermSequenceNoAssoc = BuildMultiKeyAssoc($termInfoArr, 'YearTermID', array('SequenceNumber'),1);
		$yearTermAssocByNo = BuildMultiKeyAssoc($termInfoArr, 'SequenceNumber', array(Get_Lang_Selection('YearTermNameB5','YearTermNameEN')),1);
		$yearTermAssocByID = BuildMultiKeyAssoc($termInfoArr, 'YearTermID', array(Get_Lang_Selection('YearTermNameB5','YearTermNameEN')),1);
		
		$targetTermNumber = $yearTermSequenceNoAssoc[$YearTermID];
		
		// $YearTermID == 'NULL' is overall result
		if(($YearTermID != 'NULL' && $targetTermNumber != $termNoFromFile)
		|| ($YearTermID == 'NULL' && $termNoFromFile != '0')){
			// may be wrong term
			$targetToWrongTerm = 1;
		}
		
		if( $YearTermID == 'NULL' && $targetTermNumber == '' && $termNoFromFile == 'An'){
			unset($targetToWrongTerm);
		}
	}
	$isContinueToProcess = (!$targetToWrongTerm && $isOnlyOneTerm && $correct_header);
	############### Term Validation (End) ##############
	
	# Pos of first subject score
	$pos_first_subject = count($DefaultHeader);

	if($isContinueToProcess ){
		
		# Get SubjectCode ID Mapping
		//$SubjectAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr($MapByShortName=1);
		$SubjectAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr($MapByShortName=1, $isCaseSensitive=0);
		$SubjectReverseAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr($MapByShortName=1, $isCaseSensitive=1, $reverseResult=1);
		$CmpReverseAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr($MapByShortName=1, $isCaseSensitive=1, $reverseResult=1, $onlyReturnCmp=1);
	
		# Get pos of overall score and end of file
		$pos_overall_score = 0;
		$pos_end = 0;
		$i = $pos_first_subject;
		
		// unset array
		unset($array_subject_code);
		unset($array_subject_code_language);
		unset($processedSubjectCodeRecordFlag);
		
		while ($i < sizeof($header_row))
		{
      		$target = $header_row[$i];
			$temp = explode("_", $target);
			$assessmentText = ParseAssementCode($temp[0]);
			//debug($temp[0], $temp[1], sizeof($temp), $assessmentText);
			if ($assessmentText!=$temp[0] && $assessmentText!="")
			{
				$assessmentText = $temp[0];  # store original format - T1A1
				$AssessmentArr[$assessmentText] = true;
				$AssessmentCount = true;
			} else
			{
				$AssessmentCount = false;
			}
			
			
			switch(sizeof($temp))
			{
		        case 2:     # Overall result
		          if ($pos_overall_score==0)
		          {
		          	# Yuen how to handle?
		            //$pos_overall_score = $i;
		          }
		          
		          list($set_term, $set_type) = $temp;
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
						$AssessmentHeader[$i+3] = $assessmentText;
						$AssessmentHeader[$i+4] = $assessmentText;
						$AssessmentHeader[$i+5] = $assessmentText;
					}
		          $AssessmentHeaderType[$i] = "OVERALL";
		          $AssessmentHeaderType[$i+1] = "OVERALL";
		          $AssessmentHeaderType[$i+2] = "OVERALL";
		          $AssessmentHeaderType[$i+3] = "OVERALL";
		          $AssessmentHeaderType[$i+4] = "OVERALL";
		          $AssessmentHeaderType[$i+5] = "OVERALL";
		          $i += 6;
		          break;
		        case 3:     # End of line
		          if ($pos_end==0)
		          {
		            $pos_end = $i;
		          }
		          $i++;
		          break 2;
		        case 4:     # Main Subject
		          list($set_term, $set_subjcode, $set_moi, $set_data_type) = $temp;
		          $array_subject_code[] = $set_subjcode;
		          
		          // Add teaching language of subject to array
		          $array_subject_code_language[$set_subjcode][] = $set_moi;
		          
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
					}
		          $AssessmentHeaderType[$i] = "MAIN_SUBJECT";
		          $AssessmentHeaderType[$i+1] = "MAIN_SUBJECT";
		          $AssessmentHeaderType[$i+2] = "MAIN_SUBJECT";
		          $i += 3;
		          break;
		        case 5:     # Component Subject
		          list($set_term, $set_subjcode, $set_sbj_cmp_code, $set_moi, $set_data_type) = $temp;
		          $array_subject_code[] = $set_subjcode.SBJ_CMP_SEPARATOR.$set_sbj_cmp_code;
		          
		          // Add teaching language of component subject to array
		          $array_subject_code_language[$set_subjcode.SBJ_CMP_SEPARATOR.$set_sbj_cmp_code][] = $set_moi;
		          
		          if ($AssessmentCount)
					{
						$AssessmentHeader[$i] = $assessmentText;
						$AssessmentHeader[$i+1] = $assessmentText;
						$AssessmentHeader[$i+2] = $assessmentText;
					}
		          $AssessmentHeaderType[$i] = "COMPONENT_SUBJECT";
		          $AssessmentHeaderType[$i+1] = "COMPONENT_SUBJECT";
		          $AssessmentHeaderType[$i+2] = "COMPONENT_SUBJECT";
		          $i += 3;
		          break;
		        default:    # Wrong file format
		        	$headerFormatError = true;
		        	echo $Lang['SDAS']['Import']['HeaderFormatError']." : ".$target."<br>";
		        	$i++;
		          break;
			}
			
			# till the end
			$pos_overall_score = $i-1;
		}
		if (sizeof($AssessmentArr)>=1 || $sys_custom['iPf']['ChingChungHauPoWoon']['Report']['StudentLearningProfile'])
		{
			$IsMultipleAssessment = true;
		}
		if($headerFormatError){
			echo '<p>
			<input class="formbutton" onMouseOut="this.className=\'formbutton\';" onMouseOver="this.className=\'formbuttonon\';" type="button" value="'.$button_back.'" onClick="self.location=\'index.php?t=management.data_import.assessment.import_sams\'">
			<input class="formbutton" onMouseOut="this.className=\'formbutton\';" onMouseOver="this.className=\'formbuttonon\';" type="button" value="'.$button_close.'" onClick="self.close()">
			</p>';
			$linterface->LAYOUT_STOP();
			exit;
		}

		$count_new = 0;
		$count_updated = 0;
//		hdebug_pr($SubjectAssoArr);

		// import archive and not archive once together
// 		if($isArchive==1){
		$webSamsArr = Get_Array_By_Key($data,'8');
		$sql = "SELECT
					UserID,
					LOWER(WebSAMSRegNo) as WebSAMSRegNo
				FROM
					{$intranet_db}.INTRANET_ARCHIVE_USER
				WHERE
					WebSAMSRegNo IN ('".implode("','",$webSamsArr)."')  AND WebSAMSRegNo != ''
					 AND RecordType = 2";
		$userWebSAMS = BuildMultiKeyAssoc($li->returnResultSet($sql), 'WebSAMSRegNo', array('UserID'), 1 );
// 		}
		
		# Process Data
		unset($error_data);
		for ($i=0; $i<sizeof($data);$i++)
		{
		  	// Eric Yip (20100524): skip empty line
     		if(count($data[$i]) == 0) continue;
		
			$target_year = trim($data[$i][1]);
			$target_class = Big5ToUnicode(trim($data[$i][5]));
			$target_classnum = trim($data[$i][6]);
			$target_regno = trim($data[$i][8]);
					
			#############################################################################
			# Fill here
			# Find studentID from DB
			# Get StudentID by regNo
			if ($target_regno == "")
			{
				$error_data[] = array($i, ERROR_TYPE_EMPTY_REGNO);
				continue;
			}
			else if (substr($target_regno, 0, 1)!="#")
			{
				$error_data[] = array($i, ERROR_TYPE_INCORRECT_REGNO, array($target_regno));
				continue;
			}
		
			// import archive and not archive once together
// 			if($isArchive==1){
// 				$target_id = $userWebSAMS[$target_regno];
//			move down - get data from INTRANET_USER first - X110117 
//			$target_id = $userWebSAMS[strtolower($target_regno)];
// 			}
// 			else{
// 			if(!isset($target_id)){
				$sql = "SELECT
		                  UserID
		                FROM
		                  {$intranet_db}.INTRANET_USER
		                WHERE
		                  (TRIM(WebSAMSRegNo) = '$target_regno' OR CONCAT('#', TRIM(WebSAMSRegNo)) = '$target_regno') AND
		                  RecordType = 2
	            ";				
				$target_id = current($li->returnVector($sql));
// 			}
// 			}
			if ($target_id == ""){
				$target_id = $userWebSAMS[strtolower($target_regno)];
			}
		
			if ($target_id == "")
			{
// 				debug_pr($target_regno);
// 				debug_pr($target_id);
		    	$error_data[] = array($i, ERROR_TYPE_NO_MATCH_REGNO);
		    	continue;
			}
			  
			# Map ClassID
			// case insensitive checking
			//$thisYearClassID = $ClassInfoAssoArr[$target_class];
			$thisYearClassID = $ClassInfoAssoArr[strtolower($target_class)];
			if ($thisYearClassID == "")	{
			    $error_data[] = array($i, ERROR_TYPE_INCORRECT_CLASSNAME);
			    continue;
			}
			else{
				// get back real ClassnameEn
				$target_class = $ClasReverseAssoArr[$thisYearClassID];
			}
		
			#############################################################################
		
			unset($subject_data);
			unset($overall_data);
		
			$j = $pos_first_subject;
			$curr_subj_pos = 0;
			while ($j <= $pos_overall_score)
			{
				if ($AssessmentHeaderType[$j] == "MAIN_SUBJECT" || $AssessmentHeaderType[$j] == "COMPONENT_SUBJECT")
				{
					$skipUpdateRecordFlag = false;     // a flag to indicate whether skip to update/insert the record , default should insert record (false)
	
			    	$t_score = Standardize_Import_Data($data[$i][$j]); 
			    	$t_grade = Standardize_Import_Data($data[$i][$j+1], $forGrade=true);
			    	$t_f_rank = Standardize_Import_Data($data[$i][$j+2]);
		  			
			        if (($t_score == "N.T." && $t_grade == "N.T." && $t_f_rank == "N.T.") || ($t_score == "N.A." && $t_grade == "N.A." && $t_f_rank == "N.A."))
			        {
						$skipUpdateRecordFlag = true;
			        }
			        $t_score = ( $t_score=="*" || $t_score=="-" || $t_score=="" || $t_score=="/" || strtoupper($t_score)=="N.T." || strtoupper($t_score)=="N.A.") ? "-1" : $t_score;
			        $t_score = $t_score * 1;	// change 67.00 to 67
			          
			        // Eric Yip (20100910): No rank input => null value in DB
			        if (strtoupper($t_f_rank)=="N.T." || strtoupper($t_f_rank)=="N.A.") {
			        	$t_rank_classlevel = "NULL";
			        	$t_rank_cl_total = "NULL";
			        }
			        else {
			        	list($t_rank_classlevel, $t_rank_cl_total) = explode("#",$t_f_rank);
				        $t_rank_classlevel = empty($t_rank_classlevel) ? "NULL" : $t_rank_classlevel;
				        $t_rank_cl_total = empty($t_rank_cl_total) ? "NULL" : $t_rank_cl_total;
			        }
			        
			          
			        #############################################################################
			        # Fill here
			        # Update subject data to DB
			        $target_subjcode = Big5ToUnicode($array_subject_code[$curr_subj_pos]);
					$subject_code_type = $target_subjcode;
			
			        # Check is CMP
			        $conds = '';
					if (strpos($target_subjcode,SBJ_CMP_SEPARATOR))
					{
				    	# It is component score
				        $temp = explode(SBJ_CMP_SEPARATOR, $target_subjcode);
				        list($target_subjcode, $target_sbj_cmp_code) = $temp;

				        //$conds = " AND SubjectComponentCode = '$target_sbj_cmp_code' ";
// 				        $string_sql_sbj_cmp_code = "'$target_sbj_cmp_code'";
				            
				        # check whether the subject code exist
				        $valid_subject_code = $lpf->checkSubjectCode($target_sbj_cmp_code);
				            
				        # Get Parent Subject ID
				        // case insensitive checking and get back original code
				        $target_subject_id = $SubjectAssoArr[strtolower($target_subjcode)];
				        $target_subjcode = $SubjectReverseAssoArr[$target_subject_id];
	
				        $target_subject_component_id = 'NULL';
			            $valid_subject_code = ($target_subject_id=='')? false : $valid_subject_code;
			            	
			            # Get Component Subject ID
				        //$t_temp_subject_key = $target_subjcode.SBJ_CMP_SEPARATOR.$target_sbj_cmp_code;
			            $t_temp_subject_key = strtolower($target_subjcode.SBJ_CMP_SEPARATOR.$target_sbj_cmp_code);
				        $target_subject_component_id = $SubjectAssoArr[$t_temp_subject_key];
				        // case insensitive checking and get back original code
				        $target_sbj_cmp_code = $CmpReverseAssoArr[$target_subject_component_id];
				        $string_sql_sbj_cmp_code = "'$target_sbj_cmp_code'";
				        
				        $conds .= " AND SubjectComponentID = '$target_subject_component_id' ";
			            $valid_subject_code = ($target_subject_component_id=='')? false : $valid_subject_code;
					}
			        else
					{
			            # Normal
			            $conds .= " AND (SubjectComponentCode IS NULL OR SubjectComponentCode='')";
			            $string_sql_sbj_cmp_code = "NULL";

			            # check whether the subject code exist
			            $valid_subject_code = $lpf->checkSubjectCode($target_subjcode);
			                
			            // case insensitive checking and get back original code 
			            $target_subject_id = $SubjectAssoArr[strtolower($target_subjcode)];
			            $target_subjcode = $SubjectReverseAssoArr[$target_subject_id];

			            $target_subject_component_id = 'NULL';
		            	//debug($valid_subject_code);
		            	$valid_subject_code = ($target_subject_id=='')? false : $valid_subject_code;
		            	//debug($valid_subject_code, $target_subject_id);
					}	
			          
			        if($valid_subject_code>0)
			        {
			        	$IsAnnual = ($targetSemester=="") ? 1 : 0;
			        	
			            $conds .= ($IsAnnual==0) ? " AND Semester = '$targetSemester'" : " AND IsAnnual = '$IsAnnual'";
			        	
			        	if ($IsMultipleAssessment && $AssessmentHeader[$j]!="")
			        	{
			        		$conds .= " AND TermAssessment='".$AssessmentHeader[$j]."' ";
			        	} else
			        	{
			        		$conds .= " AND TermAssessment IS NULL ";
			        	}
	
						$delSql = "delete 
									from 
									{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
									where 
									UserID = {$target_id} AND
			                        Year = '$targetYear' AND
									SubjectCode = '{$target_subjcode}'
									$conds
								";
							
							$hashed_value = md5($delSql);
							
							# Multiple teaching language checking
							# $hashed_value: unique value of each records, subjects and subject components
							// 1. All values are N.A. or N.T.
							// 2. Not yet deleted DB record => ensure first all "N.A." or "N.T" case
							// 3. Subject more than 1 teaching language
							 if($skipUpdateRecordFlag && !isset($processedSubjectCodeRecordFlag[$hashed_value]) && count($array_subject_code_language[$array_subject_code[$curr_subj_pos]]) > 1) {
							 	// Allow other languages of same subject to insert record
							 	$processedSubjectCodeRecordFlag[$hashed_value] = false;
							 }
							
							if (!$del_log[$hashed_value])
							{
								$del_log[md5($delSql)] = true;
	
								//delete the exist record 
								$li->db_db_query($delSql);
							} 
							elseif($processedSubjectCodeRecordFlag[$hashed_value] === false && count($array_subject_code_language[$array_subject_code[$curr_subj_pos]]) > 1 && !$skipUpdateRecordFlag){
								// do nothing: skip duplicate records checking until subject is processed
								// 1. Allow other languages of the same subject to insert record
								// 2. Subject more than 1 teaching languages
								// 3. All values are not N.A. or N.T.
							} 
							else {
								$skipUpdateRecordFlag = true;
							}
							
						if($skipUpdateRecordFlag == true){
							//do nothing
						}else{
							
							// Subject -> processed : Resume duplicate records checking 
							 $processedSubjectCodeRecordFlag[$hashed_value] = true;
							
							//insert record
							$TermAssessment_value = ($IsMultipleAssessment && $AssessmentHeader[$j]!="") ? "'".$AssessmentHeader[$j]."'" : "NULL";
			              $sql =  "
			                        INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
			                          (UserID, WebSAMSRegNo, ClassName, YearClassID,
			    											ClassNumber, Year, AcademicYearID, Semester, YearTermID, TermAssessment,
			    											IsAnnual, SubjectCode, SubjectID, Score, Grade,
			                          OrderMeritForm, OrderMeritFormTotal, 
			    											SubjectComponentCode, SubjectComponentID, InputDate, ModifiedDate)
			                        VALUES
			                          ('$target_id', '$target_regno', '$target_class', '$thisYearClassID',
			                          '$target_classnum', '$targetYear', $academicYearID,'$targetSemester', $YearTermID, $TermAssessment_value,
			                          '$IsAnnual', '$target_subjcode', '$target_subject_id', '$t_score', '$t_grade',
			                          '$t_rank_classlevel', '$t_rank_cl_total',
			                          $string_sql_sbj_cmp_code, $target_subject_component_id, now(), now())
			                      ";
								// if (strstr($target_subjcode, "CHIN"))
								
								
	//debug($sql);
							if ($li->db_db_query($sql))
							{
						    	$count_updated ++; // always count as update
							} else
							{
								//debug($sql);
							}		

						    
					    $cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID] = true;
					    						 
							}
						} else
						{
							if (!$detectedBefore[$subject_code_type])
							{
								// Display error message for subject and subject component
								$error_type = strpos($subject_code_type,SBJ_CMP_SEPARATOR)? ERROR_TYPE_NO_SUBJECTCOMP : ERROR_TYPE_NO_SUBJECT;
								$error_code = strpos($subject_code_type,SBJ_CMP_SEPARATOR)? str_replace(SBJ_CMP_SEPARATOR,'_',$subject_code_type):$subject_code_type;
								
								$error_data[] = array(-1, $error_type, $error_code);
								$detectedBefore[$target_subjcode] = true;
 							} else {
								$OtherFailureDue2SubjectCode ++;
 							}
						}
			          #############################################################################
			          $curr_subj_pos++;
			          $j += 3;
				} elseif ($AssessmentHeaderType[$j] == "OVERALL")
				{
					# overall result
					//debug($j);
					# Final score
					  $t_overall_score = Standardize_Import_Data($data[$i][$j]); //hdebug_r($t_overall_score); // it is Overall Average Score
					  $t_overall_score = (empty($t_overall_score) || strtolower($t_overall_score)==$NA_str || strtolower($t_overall_score)=='n.t.')? "-1" : $t_overall_score; 
					  $t_overall_score = $t_overall_score * 1;	// change 67.00 to 67
		
					  $t_overall_grade = Standardize_Import_Data($data[$i][$j+1], $forGrade=true);
					  
					  for($k = 2;$k<6; $k++){
					      $classlevelHeaderAry = array();
					      $classlevelHeaderAry = explode('_',$header_row[$j+$k]);
					      
					      if($classlevelHeaderAry[1]=='OMC'){
					          $temp_rank_class = Standardize_Import_Data($data[$i][$j+$k]);
					      }
					      if($classlevelHeaderAry[1]=='OMF'){
					          $temp_rank_classlevel =  Standardize_Import_Data($data[$i][$j+$k]);
					      }
					      
					      if($classlevelHeaderAry[1]=='OMS'){
					          $temp_rank_stream = Standardize_Import_Data($data[$i][$j+$k]);
					      }
					      
					      if($classlevelHeaderAry[1]=='OMSG'){
					          $temp_rank_subjgrp = Standardize_Import_Data($data[$i][$j+$k]);
					      }
					  }
					  
// 					  $temp_rank_class = Standardize_Import_Data($data[$i][$j+2]);
// 					  $temp_rank_classlevel = Standardize_Import_Data($data[$i][$j+3]);
// 					  $temp_rank_stream = Standardize_Import_Data($data[$i][$j+4]);
// 					  $temp_rank_subjgrp = Standardize_Import_Data($data[$i][$j+5]);
				
					  list($t_overall_rank_class, $t_overall_rank_class_total) = explode("#", $temp_rank_class);
					  list($t_overall_rank_classlevel, $t_overall_rank_classlevel_total) = explode("#", $temp_rank_classlevel);
					  list($t_overall_rank_stream, $t_overall_rank_stream_total) = explode("#", $temp_rank_stream);
					  list($t_overall_rank_subjgrp, $t_overall_rank_subjgrp_total) = explode("#", $temp_rank_subjgrp);
					  
				    // Eric Yip (20100910): No rank input => null value in DB
				    $NA_str = 'n.a.';
				    
				    $t_overall_rank_class = empty($t_overall_rank_class) ? "NULL" : $t_overall_rank_class;
			        $t_overall_rank_class = strtolower($t_overall_rank_class)==$NA_str ? "NULL" : $t_overall_rank_class;
			        
			        $t_overall_rank_class_total = empty($t_overall_rank_class_total) ? "NULL" : $t_overall_rank_class_total;
			        $t_overall_rank_class_total = strtolower($t_overall_rank_class_total)==$NA_str ? "NULL" : $t_overall_rank_class_total;
			        
			        $t_overall_rank_classlevel = empty($t_overall_rank_classlevel) ? "NULL" : $t_overall_rank_classlevel;
			        $t_overall_rank_classlevel = strtolower($t_overall_rank_classlevel)==$NA_str ? "NULL" : $t_overall_rank_classlevel;
			        
			        $t_overall_rank_classlevel_total = empty($t_overall_rank_classlevel_total) ? "NULL" : $t_overall_rank_classlevel_total;
			        $t_overall_rank_classlevel_total = strtolower($t_overall_rank_classlevel_total)==$NA_str ? "NULL" : $t_overall_rank_classlevel_total;
			        
			        $t_overall_rank_stream = empty($t_overall_rank_stream) ? "NULL" : $t_overall_rank_stream;
			        $t_overall_rank_stream = strtolower($t_overall_rank_stream)==$NA_str ? "NULL" : $t_overall_rank_stream;
			        
			        $t_overall_rank_stream_total = empty($t_overall_rank_stream_total) ? "NULL" : $t_overall_rank_stream_total;
			        $t_overall_rank_stream_total = strtolower($t_overall_rank_stream_total)==$NA_str ? "NULL" : $t_overall_rank_stream_total;
			        
			        $t_overall_rank_subjgrp = empty($t_overall_rank_subjgrp) ? "NULL" : $t_overall_rank_subjgrp;
			        $t_overall_rank_subjgrp = strtolower($t_overall_rank_subjgrp)==$NA_str ? "NULL" : $t_overall_rank_subjgrp;
		        
			        $t_overall_rank_subjgrp_total = empty($t_overall_rank_subjgrp_total) ? "NULL" : $t_overall_rank_subjgrp_total;
			        $t_overall_rank_subjgrp_total = strtolower($t_overall_rank_subjgrp_total)==$NA_str ? "NULL" : $t_overall_rank_subjgrp_total;
				
					  #############################################################################
					  # Fill here
					  # Update overall data to DB
					  $conds = ($IsAnnual==0) ? " AND Semester = '$targetSemester'" : " AND IsAnnual = '$IsAnnual'";
					  
					  
			        	if ($IsMultipleAssessment && $AssessmentHeader[$j]!="")
			        	{
			        		$conds .= " AND TermAssessment='".$AssessmentHeader[$j]."' ";
			        	} else
			        	{
			        		$conds .= " AND TermAssessment IS NULL ";
			        	}
			        	
		        $sql =  "
		                  SELECT
		                    RecordID
		                  FROM
		                    {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                  WHERE
		                    UserID = '$target_id' AND
		                    Year = '$targetYear'
		                    $conds
		                ";
					  $temp = $li->returnVector($sql); 
					  if (sizeof($temp)==0)
					  {
							$TermAssessment_value = ($IsMultipleAssessment && $AssessmentHeader[$j]!="") ? "'".$AssessmentHeader[$j]."'" : "NULL";
		        		  $sql =  "INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                    (UserID, WebSAMSRegNo, AcademicYearID, Year, Semester, YearTermID, TermAssessment,
		                    IsAnnual, ClassName, YearClassID, ClassNumber, Score,
		                    Grade, OrderMeritClass, OrderMeritClassTotal, OrderMeritForm,
		                    OrderMeritFormTotal, OrderMeritStream, OrderMeritStreamTotal,
		                    OrderMeritSubjGroup, OrderMeritSubjGroupTotal, InputDate, ModifiedDate)
		                  VALUES
		                    ('$target_id', '$target_regno' , $academicYearID, '$targetYear', '$targetSemester', $YearTermID, $TermAssessment_value,
		                    '$IsAnnual', '$target_class', '$thisYearClassID', '$target_classnum', '$t_overall_score',
		                    '$t_overall_grade', '$t_overall_rank_class', '$t_overall_rank_class_total', '$t_overall_rank_classlevel', '$t_overall_rank_classlevel_total', '$t_overall_rank_stream', '$t_overall_rank_stream_total', '$t_overall_rank_subjgrp', '$t_overall_rank_subjgrp_total', now(), now())
		                  ";
						  $li->db_db_query($sql);
 
					    $count_new ++;
					    $cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID] = true;
					  }
					  else
					  {
						  $t_record_id = $temp[0];
		          $sql =  "UPDATE
		                    {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
		                  SET
		                    Score = '$t_overall_score',
		                    Grade = '$t_overall_grade',
		                    OrderMeritClass = '$t_overall_rank_class',
		                    OrderMeritClassTotal ='$t_overall_rank_class_total',
		                    OrderMeritForm = '$t_overall_rank_classlevel',
		                    OrderMeritFormTotal = '$t_overall_rank_classlevel_total',
		                    OrderMeritStream = '$t_overall_rank_stream',
		                    OrderMeritStreamTotal = '$t_overall_rank_stream_total',
		                    OrderMeritSubjGroup = '$t_overall_rank_subjgrp',
		                    OrderMeritSubjGroupTotal = '$t_overall_rank_subjgrp_total',
		                    ModifiedDate = now()
		                  WHERE
		                    RecordID = '$t_record_id'";
		//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
					      $li->db_db_query($sql);
		//	hdebug_r('Update');
		//	hdebug_r($sql);		      
					      $count_updated ++;
					  }
					  $j += 6;
					 // debug($sql);
				} else
				{
					$j ++ ;
				}
			  }
		
			  
		
			  #############################################################################
		
		
		}
		# Process Error Row
	}
	
	# calculate SD and Mean for each form 
	//$cal_sd_mean_arr[$thisYearClassID][$academicYearID][$YearTermID]
	if (is_array($cal_sd_mean_arr) && sizeof($cal_sd_mean_arr)>0)
	{
		foreach ($cal_sd_mean_arr AS $CurYearClassID => $AcademicObj)
		{
			# get year_id
			# ...
			$lib_year = new year_class($CurYearClassID);
			$CurYearID = $lib_year->YearID;
			if (is_array($AcademicObj) && sizeof($AcademicObj)>0)
			{
				foreach ($AcademicObj AS $CurAcademicYearID => $YearTermObj)
				{
					if (is_array($YearTermObj) && sizeof($YearTermObj)>0)
					{
						foreach ($YearTermObj AS $CurYearTermID => $value)
						{
							$objSDAS->ComputeSDMean($CurYearID, $CurAcademicYearID, $CurYearTermID);	
							//debug($CurYearID, $CurAcademicYearID, $CurYearTermID);
							//debug("inside");
						}
					}
				}
			}
			
		}
	}
}

$yearInfoArr = BuildMultiKeyAssoc( GetAllAcademicYearInfo(), 'AcademicYearID' );
$yearName = Get_Lang_Selection($yearInfoArr[$_POST['academicYearID']]['YearNameEN'], $yearInfoArr[$_POST['academicYearID']]['YearNameB5'] );
$navigationArr[] = array($yearName,'');
$navigationArr[] = array($Lang['SDAS']['DSEstat']['Int'] ,'');
$display_content = '';
$display_content .= $linterface->GET_NAVIGATION_IP25($navigationArr);
$display_content .= '<br>';
if($isContinueToProcess){
// if($correct_header){
	# Display import stats
	$display_content .= "<table border='0' cellpadding='5' cellspacing='0'>";
//	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_add_no']." : </td><td class='tabletext'><b>".$count_new."</b></td></tr>\n";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_update_no']." : </td><td class='tabletext'><b>".$count_updated."</b></td></tr>\n";
	$display_content .= "<tr><td class='tabletext'>".$ec_guide['import_fail_no']." : </td><td class='tabletext'><b>".(sizeof($error_data)+$OtherFailureDue2SubjectCode)."</b></td></tr>\n";
	$display_content .= "</table>\n";
	
	if (sizeof($error_data)!=0)
	{
		$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1 class='table_b'>\n";
	    $error_table .= "<tr class=tableTitle bgcolor='#CFE6FE'><td nowrap class='tabletext'>".$ec_guide['import_error_data']."</td><td width='45%' class='tabletext'>".$ec_guide['import_error_reason']."</td><td width='55%' class='tabletext'>".$ec_guide['import_error_detail']."</td></tr>\n";
	
		for ($i=0; $i<sizeof($error_data); $i++)
		{
			list ($t_row, $t_type, $t_data) = $error_data[$i];
			if ($t_row==-1)
			{
				$t_row = "Column(s)";
			} else
			{
				$t_row++;     # set first row to 1
			}
			$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
			$error_table .= "<tr bgcolor='$css_color'><td class='tabletext'>$t_row</td><td class='tabletext'>";
			$reason_string = $ec_guide['import_error_unknown'];
			switch ($t_type)
			{
				case ERROR_TYPE_NO_MATCH_REGNO:
						$reason_string = $ec_guide['import_error_no_user'];
						break;
				case ERROR_TYPE_EMPTY_REGNO:
						$reason_string = $ec_iPortfolio['activation_result_no_regno'];
						break;
				case ERROR_TYPE_INCORRECT_REGNO:
						$reason_string = $ec_guide['import_error_incorrect_regno'];
						break;
				case ERROR_TYPE_INCORRECT_CLASSNAME:
						$reason_string = $ec_guide['import_error_incorrect_class'];
						break;
				case ERROR_TYPE_NO_SUBJECT:
						$reason_string = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsNotFound'];
						break;
				case ERROR_TYPE_NO_SUBJECTCOMP:
						$reason_string = $Lang['SysMgr']['Timetable']['Warning']['Import']['SubjectIsCompNotFound'];
						break;
				default:
						$reason_string = $ec_guide['import_error_unknown'];
						break;
			}
			$error_table .= $reason_string;
	
			$error_contents = (is_array($t_data)) ? implode(",",$t_data) : $t_data."&nbsp;";
			$error_table .= "</td><td class='tabletext'>".$error_contents."</td></tr>\n";
		}
		$error_table .= "</table>\n";
		$display_content .= $error_table;
	}
}
else
{
	//!$isContinueToProcess
	
	if(!$correct_header){
		# Header incorrect
		$display_content .= "<table border='0' cellpadding='5' cellspacing='0' width='90%'>";
		$display_content .= "<tr><td class='tabletext'>".$i_SAMS_import_error_wrong_format.":</td></tr>";
		$display_content .= "</table>\n";
		
		$error_table = "<table border='0' cellpadding='5' cellspacing='0' width='90%'><tr>";
		$error_table .= "<td class='tabletext' width='40%'>".implode("<br />", $DefaultHeader)."<br />...</td>";
		$error_table .= "<td class='tabletext' align='center' width='10%'>v.s.</td>";
		
		$error_table .= "<td class='tabletext' width='40%'>";
		for($i=0; $i<$pos_first_subject; $i++)
		{
			$error_table .= $header_row[$i]."<br />";
		}
		$error_table .= "...</td>";
		
		$error_table .= "</tr></table>\n";
	
		$display_content .= "<br />".$error_table;
	}
	
	if(!$isOnlyOneTerm){
		$display_content .= "<br />".$Lang['SDAS']['Exam']['Import']['MoreThanOneTerm'].":";
		$display_content .= "<br />".implode(', ', $existTermAssessment);
	}
	
	if($targetToWrongTerm){
		// send the file to temp folder
		include_once($intranet_root."/includes/libfilesystem.php");
		$lfs = new libfilesystem();
		### move to temp folder first for others validation
		$folderPrefix = $intranet_root."/file/import_temp/sdas/assessment_result";
		if (!file_exists($folderPrefix)) {
			$lfs->folder_new($folderPrefix);
		}
		$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
		$targetFilePath = stripslashes($folderPrefix."/".$targetFileName);
		$success = $lfs->lfs_move($userfile, $targetFilePath);
		
		if($success){
			$displaySuggestionTermName = ($termNoFromFile==0)? $ec_iPortfolio['overall_result'] : $yearTermAssocByNo[$termNoFromFile] ;
			$displayChoseTerm = ($YearTermID == 'NULL')? $ec_iPortfolio['overall_result'] : $yearTermAssocByID[$YearTermID];
			if($display_content != ''){
				$display_content .= '<br>';
			}
// 			$display_content .= "<span style='font-size:14px;'>Your file provide <b>T$termNoFromFile</b>. But you chose <b>$displayChoseTerm</b>, should it be <b>$displaySuggestionTermName</b>?</span>";
			$displayWarning = $Lang['SDAS']['Exam']['Import']['MisMatchTerm'];
			if($termNoFromFile == 'An'){
				$displayWarning = str_replace('T<!--termNoFromFile-->', $termNoFromFile, $displayWarning);
			}else{
				$displayWarning = str_replace('<!--termNoFromFile-->', $termNoFromFile, $displayWarning);
			}
			$displayWarning = str_replace('<!--displayChoseTerm-->', $displayChoseTerm, $displayWarning);
			$displayWarning = str_replace('<!--displaySuggestionTermName-->', $displaySuggestionTermName, $displayWarning);
			$display_content .= "<span style='font-size:14px;'>".$displayWarning."</span>";
			$display_content .= '<br><br>';
			$display_content .= $linterface->GET_HIDDEN_INPUT('academicYearID', 'academicYearID', $academicYearID);
			$display_content .= $linterface->GET_HIDDEN_INPUT('YearTermID', 'YearTermID', $YearTermID);
			$display_content .= $linterface->GET_HIDDEN_INPUT('tmpFileName', 'tmpFileName', $targetFileName);
			$display_content .= $linterface->GET_HIDDEN_INPUT('skipChecking', 'skipChecking', '1');
			$display_content .= '<span style="display:block; text-align:center;">';
			$display_content .= $linterface->Get_Action_Btn($Lang['SDAS']['Exam']['Import']['ConfirmToContinue'], "button", "goSubmit('$targetFileName')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="formbutton", $ParExtraClass="");
			$display_content .= '</span>';
		}else{
			$display_content .= $Lang['SDAS']['Exam']['Import']['Error'];
		}
	}
}

?>
<script>
function goSubmit(filename){
	document.form1.submit();
}
</script>
<FORM id="form1" name="form1" method="POST">
	<br>
	<?= $display_content ?>
	<br>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	</table>
	
	<p align="right">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_back?>" onClick="self.location='index.php?t=management.data_import.assessment.import_sams'">
	<input class="formbutton" onMouseOut="this.className='formbutton';" onMouseOver="this.className='formbuttonon';" type="button" value="<?=$button_close?>" onClick="self.close()">
	</p>
</FORM>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>