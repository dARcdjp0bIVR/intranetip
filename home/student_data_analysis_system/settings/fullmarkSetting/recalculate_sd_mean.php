<?php 
## Using By : 
##############################################
##	Modification Log:
##	2017-04-20 (Villa)
##	- Using ComputeSDMean Function in SDAS(fixed) in stead of iPort
##	2017-02-28 (Villa)
##	- copy from iPo
##
##############################################
// debug_pr($AcademicYearID);
// debug_pr($YearID);
// die;
// $PATH_WRT_ROOT = "../../../../";
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
// include_once($PATH_WRT_ROOT."includes/libportfolio.php");
intranet_opendb();
set_time_limit(300);
@ini_set('memory_limit', -1);
// echo 'START: ' . date('Y-m-d H:i:s') . '<br />';
// flush();

// $objLibportfolio = new libportfolio();
// $thisSuccess = true;

#### backup START ####
// $sql = "CREATE TABLE $eclass_db.ASSESSMENT_SUBJECT_SD_MEAN_".date('YmdHi')." SELECT * FROM $eclass_db.ASSESSMENT_SUBJECT_SD_MEAN";
// $success = $objLibportfolio->db_db_query($sql);
#### backup END ####

// if($success){
// 	echo "backup success - ASSESSMENT_SUBJECT_SD_MEAN_".date('YmdHi')."";
if($YearID && $AcademicYearID){
	#### Get all Year START ####
	$sql = "SELECT YearID FROM YEAR WHERE YearID='$YearID'";
	$yearArr = $objSDAS->returnVector($sql);
	#### Get all Year END ####

	#### Get all AcademicYear START ####
	$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR ";// . "WHERE AcademicYearID IN ('14')";
	$academicYearArr = $objSDAS->returnVector($sql);
	#### Get all AcademicYear END ####

	#### Get all YearTerm START ####
	$sql = "SELECT AcademicYearID, YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$AcademicYearID'";// . "WHERE AcademicYearID IN ('14')";
	$rs = $objSDAS->returnResultSet($sql);
	#### Get all YearTerm END ####

	#### Calculate SD START ####
	foreach($yearArr as $YearID){
		foreach($rs as $r){
			$objSDAS->ComputeSDMean($YearID, $r['AcademicYearID'], $r['YearTermID']);
// 			echo "YearID: {$YearID}, AcademicYearID: {$r['AcademicYearID']}, YearTermID: {$r['YearTermID']}<br />";
// 			flush();
		}
		foreach($rs as $r){
			$objSDAS->ComputeSDMean($YearID, $r['AcademicYearID'], 0);
// 			echo "YearID: {$YearID}, AcademicYearID: {$r['AcademicYearID']}, YearTermID: ''<br />";
// 			flush();
		}
	}
	#### Calculate SD END ####
// }
// echo 'END: ' . date('Y-m-d H:i:s') . '<br />';
}
header('Location: /home/student_data_analysis_system/?t=settings.fullmarkSetting.subject_full_mark ');
intranet_closedb();
?>