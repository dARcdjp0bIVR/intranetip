<?php
## Using By : 
##############################################
##	Modification Log:
##	2016-07-06 (Omas)
##	- copy from iPo
##
##############################################

// $PATH_WRT_ROOT = "../../../../../";
// include_once($PATH_WRT_ROOT."includes/global.php");
// include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

// Get View Mode
$ViewMode = stripslashes($_REQUEST['ViewMode']);
$ExportFormNameArr = $_REQUEST['ExportFormNameArr'];
$ExportContentArr = $_REQUEST['ExportContentArr'];

$lpf = new libpf_slp();
$lexport = new libexporttext();

$ShowMarkColumn = ($ViewMode=='' || $ViewMode=='Mark')? true : false;
$ShowGradeColumn = ($ViewMode=='' || $ViewMode=='Grade')? true : false;

### Column Title (2 dimensions array, 1st row is english, 2nd row is chinese)
$ColumnTitleArr = array();
$ColumnPropertyArr = array();

$ColumnPropertyArr[] = 1;
$ColumnTitleArr['En'][] = $Lang['iPortfolio']['Subject_En'];
$ColumnTitleArr['Ch'][] = $Lang['iPortfolio']['Subject_Ch'];

$numOfForm = count($ExportFormNameArr);
for ($i=0; $i<$numOfForm; $i++) {
	if ($ShowMarkColumn) {
		$ColumnPropertyArr[] = 1;
		$ColumnTitleArr['En'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['FullMark_En'];
		$ColumnTitleArr['Ch'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['FullMark_Ch'];
		$ColumnTitleArr['En'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['PassMark_En'];
		$ColumnTitleArr['Ch'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['PassMark_Ch'];
	}
	
	if ($ShowGradeColumn) {
		$ColumnPropertyArr[] = 1;
		$ColumnTitleArr['En'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['Grade_En'];
		$ColumnTitleArr['Ch'][] = $ExportFormNameArr[$i].'_'.$Lang['iPortfolio']['Grade_Ch'];
	}
}
$ExportColumnTitleArr = $lexport->GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $ColumnPropertyArr);


### Export Content
$ExportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportContentArr, $ExportColumnTitleArr);


# Output the file to user browser
$filename = 'subject_full_mark.csv';
$lexport->EXPORT_FILE($filename, $ExportContent);
?>