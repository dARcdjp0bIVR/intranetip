<?php
//Using: Pun
/**
 * Change Log:
 * 2016-02-17 (Pun)
 *  - Modified $('#form1').submit(), add checking for integer input
 */
 

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
######## Init END ########


######## Access Right START ########
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "settings.percentileSetting";
$CurrentPageName = $Lang['SDAS']['menu']['percentile'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## Get Percentile START ########
$allPercentileSetting = $lpf->getAllPercentileSetting();
######## Get Percentile END ########


######## UI START ########
$Msg = $Lang['General']['ReturnMessage'][$Msg];
$linterface->LAYOUT_START($Msg);
?>

<form id="form1" action="index.php" method="POST">
	<input type="hidden" name="t" value="settings.percentileSetting.savePercentile" />
	<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['MappingCriteria'] ?></td>
		<td>
			<table id="MainPercentileTable" class="common_table_list_v30">
				<colgroup>
					<col style="width:30%;">
					<col style="width:30%;">
					<col style="width:30%;">
					<col style="width:10%;">
				</colgroup>
				<thead>
					<tr style="vertical-align:top;">
						<th>&nbsp;</th>
						<th>
							<?=$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'] ?>
							(<?=$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'] ?>)
						</th>
						<th>
							<?=$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'] ?>
						</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($allPercentileSetting as $index=>$setting){ ?>
					<tr>
						<td>
							<span>
								<?=($index==0)? $Lang['iPortfolio']['Percentile']['FirstPercentGroup'] : $Lang['iPortfolio']['Percentile']['NextPercentGroup'] ?>
							</span>
						</td>
						<td>
							<input name="Percent[]" value="<?=$setting['Percent'] ?>" />%
						</td>
						<td>
							<input name="Title[]" value="<?=$setting['Title'] ?>" />
						</td>
						
						<td>
							<?php if($index==0){ ?>
								<span>&nbsp;</span>
							<?php }else{ ?>
								<span class="table_row_tool row_content_tool">
									<a href="#" class="delete_dim deleteRowBtn"></a>
								</span>
							<?php } ?>
						</td>
					</tr>
					<?php } ?>
					
					<tr>
						<td colspan="99">
							<span class="table_row_tool row_content_tool">
								<a href="#" class="add addRowBtn"></a>
							</span>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</table>
	
	<br style="clear:both;">
	<div class="edit_bottom_v30">
		<input type="submit" class="formbutton_v30 print_hide " value="<?=$Lang['Btn']['Save'] ?>">	
	</div>
</form>
	
<!-- ---- New Row Template START ---- -->
<table id="templateGroup" style="display: none;">
	<tr>
		<td>
			<span><?=$Lang['iPortfolio']['Percentile']['NextPercentGroup'] ?></span>
		</td>
		<td>
			<input name="Percent[]" value="" />%
		</td>
		<td>
			<input name="Title[]" value="" />
		</td>
		
		<td>
			<span class="table_row_tool row_content_tool">
				<a href="#" class="delete_dim deleteRowBtn"></a>
			</span>
		</td>
	</tr>
</table>
<!-- ---- New Row Template END ---- -->

<script>
function removeRow(ev){
	$(ev.target).closest('tr').remove();
	
	var $currentTable = $('#MainPercentileTable');
}
$('.deleteRowBtn').click(removeRow);

$('.addRowBtn').click(function(){
	var $currentTr = $(this).closest('tr');
	var $newTr = $('#templateGroup tr').clone();

	$newTr.find('.deleteRowBtn').click(removeRow);
	
	$currentTr.before( $newTr );
});

$('#form1').submit(function(){

	// Input all Percent checking START //
	var isAllHasInput = true;
	$(this).find('[name="Percent[]"]').each(function(){
		if($.trim($(this).val()) == ''){
			isAllHasInput = false;
			$(this).focus();
			return false; // exit foreach
		}
	});
	if(!isAllHasInput){
		alert('<?=$Lang['iPortfolio']['Percentile']['alert']['PleaseInputPercentile'] ?>');
		return false;
	}
	// Input all Percent checking END //

	// Percent isInt checking START //
	var isInt = true;
	$(this).find('[name="Percent[]"]').each(function(){
		if( !is_int($(this).val()) ){
			isInt = false;
			$(this).focus();
			return false; // exit foreach
		}
	});
	if(!isInt){
		alert(NonIntegerWarning);
		return false;
	}
	// Percent isInt checking END //

	// Input all Title checking START //
	var isAllHasInput = true;
	$(this).find('[name="Title[]"]').each(function(){
		if($.trim($(this).val()) == ''){
			isAllHasInput = false;
			$(this).focus();
			return false; // exit foreach
		}
	});
	if(!isAllHasInput){
		alert('<?=$Lang['iPortfolio']['Percentile']['alert']['PleaseInputTitle'] ?>');
		return false;
	}
	// Input all Title checking END //
	
	// Total percent checking START //
	var totalPercent = 0;
	$(this).find('[name="Percent[]"]').each(function(){
		totalPercent += parseInt($(this).val());
	});
	if(totalPercent != 100){
		alert('<?=$Lang['iPortfolio']['Percentile']['alert']['PercentileSumNotEqual100'] ?>');
		return false;
	}
	// Total percent checking END //

	return true;
});
</script>
	
	
	
	
	
	
	
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>