<?php

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//Lang Lib
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//Interface Lib
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lpf = new libportfolio2007();

if (!$lpf->IS_ADMIN_USER($UserID) && !$lpf->IS_TEACHER() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah")
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$sql = "select SettingValue from GENERAL_SETTING where SettingName = 'MonthlyReportPIC'";
$TeacherList =  $lpf->returnVector($sql);

############# Get Data START #############
$selectedIdArr = array();
$selectedGroup = array();
$nameField = getNameFieldByLang2("IU.");
$sql = "SELECT
	IU.UserID,
	{$nameField} As Name
FROM
	GENERAL_SETTING  GS
INNER JOIN
	INTRANET_USER IU
ON
	 IU.UserID IN ($TeacherList[0])
WHERE 
	GS.SettingName = 'MonthlyReportPIC'
ORDER BY
	IU.EnglishName
";

$rs = $lpf->returnResultSet($sql);
foreach($rs as $r){
	$selectedGroup[] = array(
		'value' => $r['UserID'], 
		'name' => $r['Name']
	);
	$selectedIdArr[] = $r['UserID'];
}
$selectedIdList = implode("','", (array)$selectedIdArr);

$deselectedGroup = array();
$_UserType = USERTYPE_STAFF;
$sql = "SELECT
	IU.UserID,
	{$nameField} As Name
FROM
	INTRANET_USER IU
WHERE
	RecordType = '{$_UserType}'
AND
	RecordStatus = '1'
AND
	UserID NOT IN ('{$selectedIdList}')
ORDER BY
	EnglishName
";
$rs = $lpf->returnResultSet($sql);
foreach($rs as $r){
	$deselectedGroup[] = array(
		'value' => $r['UserID'], 
		'name' => $r['Name']
	);
}

############# Get Data END #############

$settings = array(
	'deselectedHeader' => $Lang['SysMgr']['RoleManagement']['Users'],
	'deselectedSelectionName' => 'deselectedCategory',
	'deselectedList' => $deselectedGroup,
	'selectedHeader' => $Lang['SysMgr']['RoleManagement']['SelectedUser'],
	'selectedSelectionName' => 'selectedCategory',
	'selectedList' => $selectedGroup
);
$html = $linterface->generateUserSelection($settings);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
</head>

<body>

<form id="form1" action="?t=settings.assessmentStatReport.ajax.accessRightConfigAddMember" method="POST" target="_parent">
<input type="hidden" name="AccessType" value="monthlyReportPIC" />
<input type="hidden" name="redirect" value="?t=settings.assessmentStatReport.accessRightConfigMonthlyReport" />

<div class="edit_pop_board" style="height: 360px;">
	<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height: 290px;width:95%;margin: 0 auto;" align="center">

		<div style="margin-top: 10px;">
<?=$html?>
		</div>

	</div>
	<div class="edit_bottom">
		<span> </span>
		<p class="spacer"></p>
		<input name="" id="AddMemberSubmitBtn" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_submit?>" type="submit">
		<input name="" id="AddMemberCancelBtn" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" type="button">
		<p class="spacer"></p>
	</div>
</div>
</form>




<script type="text/javascript">
	$('#AddAll').click(function(){
		$('#selectedCategory').append( $('#deselectedCategory').find('option') );
	});
	$('#Add').click(function(){
		$('#selectedCategory').append( $('#deselectedCategory').find('option:selected') );
	});
	
	$('#Remove').click(function(){
		$('#selectedCategory > option:selected').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});
	$('#RemoveAll').click(function(){
		$('#selectedCategory > option').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#deselectedCategory').append($(this));
		});
	});

	$('#form1').submit(function(){
		$('#selectedCategory > option').attr('selected', 'selected');
		return true;
	});
</script>

</body>
</html>
<?php
intranet_closedb();
?>
