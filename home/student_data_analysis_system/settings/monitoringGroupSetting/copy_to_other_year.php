<?php
/*
 * Using:	
 *
 * Change Log:
 * Date:	2017-06-05 Villa
 * 			- Open the file
 */


######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['GroupList'],'?t=settings.monitoringGroupSetting.list');
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['CopyGroup'],'');
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationArr);
#Step Obj
$STEPS_OBJ[] = array($Lang['Group']['Options'], 1);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
$htmlAry['stepobj'] = $linterface->GET_STEPS($STEPS_OBJ);

###AcademicYear Selection Start###
$sql = 'SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM
			ACADEMIC_YEAR ay
		INNER JOIN
			ACADEMIC_YEAR_TERM ayt
				ON	ay.AcademicYearID = ayt.AcademicYearID
		GROUP By
			ay.AcademicYearID
		ORDER BY
			ayt.TermStart
				DESC';
$rs = $objSDAS->returnResultSet($sql);
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$htmlAry['FromAcademicYear']= '<select id="FromAcademicYearID" name="FromAcademicYearID" onChange="onLoadGroupList()">';
foreach((array)$rs as $_rs){
	if($CurrentAcademicYearID==$_rs['AcademicYearID']){
		$selected = 'Selected';
	}else{
		$selected = '';
	}
	$htmlAry['FromAcademicYear'].= '<option value="'.$_rs['AcademicYearID'].'" '.$selected.'>'.Get_Lang_Selection($_rs['YearNameB5'], $_rs['YearNameEN']).'</option>';
}
$htmlAry['FromAcademicYear'].= '</select>';

$htmlAry['ToAcademicYear']= '<select id="ToAcademicYearID" name="ToAcademicYearID" onChange="onLoadGroupList()">';
foreach((array)$rs as $_rs){
	$htmlAry['ToAcademicYear'].= '<option value="'.$_rs['AcademicYearID'].'" '.$selected.'>'.Get_Lang_Selection($_rs['YearNameB5'], $_rs['YearNameEN']).'</option>';
}
$htmlAry['ToAcademicYear'].= '</select>';
###AcademicYear Selection END###

### Get Btn
$BackBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","back"," class='formbutton_v30 print_hide '");
$NextBtn = $linterface->GET_ACTION_BTN($button_continue, "submit", "","submit2"," class='formbutton_v30 print_hide '");
$Btn = $NextBtn."&nbsp;".$BackBtn;

$linterface->LAYOUT_START($Msg);
?>
<form name="frm1" method="POST" action="?t=settings.monitoringGroupSetting.copy_to_other_year_confirm" onsubmit="return CheckForm();" >
<?=$htmlAry['navigation']?>
<div style="padding-top:25px;"><?=$htmlAry['stepobj']?></div>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyFrom']?></td>
		<td><?=$htmlAry['FromAcademicYear']?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['Group']['CopyTo']?></td>
		<td><?=$htmlAry['ToAcademicYear']?></td>
	</tr>
</table>
<br style="clear:both;" />
<table class="form_table_v30">
	<tr>
		<td style="border-bottom-style:none;"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td>
	</tr>
	<tr>
		<td style="border-bottom-style:none;"><div id="GroupTableDiv"></div></td>
	</tr>
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$Btn?>
	<p class="spacer"></p>
</div>
</form>
<script>
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";
$(document).ready(function(){
	onLoadGroupList();
});
function UpdateMemberCheckBox(GroupID)
{
	if(GroupID)
	{
		if($("input#CopyGroup_GroupID_"+GroupID).attr("checked")){
			$("input#CopyMember_GroupID_"+GroupID).removeAttr("disabled");
		}
		else{
			$("input#CopyMember_GroupID_"+GroupID).attr("disabled","disabled");
// 			$(".CopyGroup_master").removeAttr("checked")
		}
	}
	else
	{
		if($(".CopyGroup_master").attr("checked")){
			$("input.CopyMember_GroupID_").removeAttr("disabled");
		}
		else{
			$("input.CopyMember_GroupID_").attr("disabled","disabled");
		}
	}
}
function onLoadGroupList(){
	$('#GroupTableDiv').html(loadingImage);
	$.ajax({
		url: '?t=settings.monitoringGroupSetting.ajax_onload_groupcopy_table',
		type: 'POST',
		data: {FromAcademicYearID: $('#FromAcademicYearID').val(), ToAcademicYearID: $('#ToAcademicYearID').val()},
		success: function(ReturnHTML){
			$('#GroupTableDiv').html(ReturnHTML);
		}
	});
} 
function CheckAll(Class){
	$('.'+Class).each(function(){
		if( $('.'+Class+'_master').attr("checked") ){
			$(this).attr("checked","checked");
		}else{
			$(this).removeAttr("checked");
		}
	});
	UpdateMemberCheckBox();
}
function CheckForm()
{
	var CopyFrom = $("#FromAcademicYearID").val();
	var CopyTo = $("#ToAcademicYearID").val();
	if(CopyFrom == CopyTo)
	{
		alert("<?=$Lang['Group']['warnSelectDifferentYear']?>");
		return false;
	}
	if($(".CopyGroup").length == 0)
	{
		alert("<?=$Lang['Group']['warnSelectGroup']?>");
		return false;
	}
	return true;
}
</script>
<?php $linterface->LAYOUT_STOP($Msg);?>