<?php 
/*
 * Using:
 * Change Log:
 * Date:	2017-06-09	Villa
 * -		Open the file
 */
	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	#### Init START ####
	intranet_auth();
	intranet_opendb();
	
	$libdb = new libdb();
	
	$field = $_GET['field'];
	switch ($field){
		default:
			$RecordTypeCond = ' AND 1 ';
			break;
		case 'T':
			$RecordTypeCond = ' AND RecordType != "2" ';
			break;
		case 'S': 
			$RecordTypeCond = ' AND RecordType = "2" ';
			break;
			
	}
	$str = $libdb->Get_Safe_Sql_Like_Query($_GET['q']);
	$sql = 'SELECT 
				UserID, EnglishName, ChineseName 
			FROM 
				INTRANET_USER 
			WHERE 
				(EnglishName LIKE "%'.$str.'%" OR ChineseName LIKE "%'.$str.'%")'.$RecordTypeCond;
	$rs = $libdb->returnResultSet($sql);
	$x = '';
	foreach ((array)$rs as $_rs){
		$x .= Get_Lang_Selection($_rs['ChineseName'], $_rs['EnglishName']).'|'.'|'.$_rs['UserID']."\n";
	}
	echo $x;
	intranet_closedb();
	exit();
?>