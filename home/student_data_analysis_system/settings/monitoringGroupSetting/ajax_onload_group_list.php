<?php 
/*
 * Using: 
 * 
 * Change Log:
 * Date:	2017-06-23	Villa
 * 			-Revised the data getting part
 * Date:	2017-06-05	Villa
 * 			-Open the file
 * 
 */

###Header Congfig START
$HeaderCongfig = array('GroupNameCH','GroupNameEN','PIC','Member');
$NumberOfHeader = sizeof($HeaderCongfig)+2;
###Header Congfig END
##Data Get Start
$data = $objSDAS->getMonitoringGroup('',$AcademicYearID);

$rs = $objSDAS->getMonitoringGroupMember('','');
foreach ((array)$rs as $_rs){
	$data_member[$_rs['GroupID']][$_rs['RecordType']][] = Get_Lang_Selection($_rs['ChineseName'], $_rs['EnglishName']); 
}
##Data Get END

$table_tool ='<div nowrap="nowrap" class="common_table_tool">
				<a href="#" class="tool_delete" onclick="Delete()">'.$Lang['Btn']['Delete'].'</a>
			</div>';

$HTML = $table_tool;
$HTML .= '<table id="dataTable" class="common_table_list_v30" style="width:100%; vertical-align: top;">';
	$HTML .= '<thead>';
		$HTML .= '<tr class="class="tabletop">';
			$HTML .= '<th>'.'#'.'</th>';
			foreach ($HeaderCongfig as $_HeaderCongfig){
				$HTML .= '<th>'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead'][$_HeaderCongfig].'</th>';
			}
			$HTML .= '<th>'.'<input type="checkbox" onclick="CheckAll(\'delete_checkbox\')" class="delete_checkbox_master">'.'</th>';
		$HTML .= '</tr>';
	$HTML .= '</thead>';
	$HTML .= '<tbody>';
	if(!empty($data)){
		$i = 1;
		foreach ((array)$data as $_data){
			$HTML .= '<tr>';
				#index
				$HTML .= '<td style="width:1px;">'.$i.'</td>';
				#Academic Year
// 				$HTML .= '<td>'.Get_Lang_Selection($_data['YearNameB5'], $_data['YearNameEN']).'</td>';
				#Chinese Group Name
				$HTML .= '<td>'.'<a href="javascript: add_New('.$_data['GroupID'].');">'.$_data['GroupNameCH'].'</a>'.'</td>';
				#English Group Name
				$HTML .= '<td>'.'<a href="javascript: add_New('.$_data['GroupID'].');">'.$_data['GroupNameEN'].'</a>'.'</td>';
				#PIC
				$HTML .= '<td>';
				if(!empty($data_member[$_data['GroupID']][1])){
					foreach ((array)$data_member[$_data['GroupID']][1] as $_group_pic){
	// 					$HTML .= '<a href="javascript: addNewMember('.$_data['GroupID'].',1)">'.$_group_pic.'</a>';
						$HTML .= $_group_pic;
						$HTML .= '<br>';
					}
				}else{
					$HTML .= '-';
				}
				$HTML .= '</td>';
				#Number Of member
				$HTML .= '<td>'.'<a href="?t=settings.monitoringGroupSetting.member_list&GroupID='.$_data['GroupID'].'&RecordType=2">'.sizeof($data_member[$_data['GroupID']][2]).'</a>'.'</td>';
				#checkbox
				$HTML .= '<td style="width:1px;">'.'<input type="checkbox" class="delete_checkbox" value="'.$_data['GroupID'].'">'.'</td>';
			$HTML .= '</tr>';
			$i++;
		}
	}else{
		//no data
		$HTML .= '<tr>';
			$HTML .= '<td style="text-align: center;" colspan="'.$NumberOfHeader.'">'.$Lang['SDAS']['MonitoringGroupSetting']['NoResult'] .'</td>';
		$HTML .= '</tr>';
	}
	$HTML .= '</tbody>';
$HTML .= '</table>';
echo $HTML;
?>