<?php 
/*
 * Change Log:
 * Date:	2017-07-12 Villa
 * -		Modified to only success updated group will be shown  
 * Date:	2017-06-08 Villa
 * -		Open the file
 */
######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
$Json = new JSON_obj();
######## Page Setting END ########
$data = stripslashes($data);
$Data = $Json->decode($data);
$FromAcademicYearID = $Data['FromAcademicYearID'];
$UpdateSuccess = $Data['UpdateSuccess']; 
$Update = $Data['Update'];
###Update Process START
###GroupInfo START
$sql = 'SELECT
			GroupID, GroupNameEN, GroupNameCH
		FROM
			MONITORING_GROUP
		WHERE
			AcademicYearID = '.$FromAcademicYearID.'
		AND
			IsDeleted = "0"
		ORDER BY
			GroupID desc';
$rs =  $objSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	$rs_From[$_rs['GroupID']] = $_rs;
}

###Result Table START
$TableHTML = '<table style="width: 100%;" class="common_table_list_v30">';
$TableHTML .= '<tr>';
#GroupNameEN
$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'].'</th>';
#GroupNameCH
$TableHTML .= '<th style="width: 35%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'].'</th>';
#Copy
$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Copy'].'</th>';
#CopyMember
$TableHTML .= '<th style="text-align: center; width: 15%;">'.$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['CopyMember'].'</th>';
$TableHTML .= '</tr>';

foreach ($rs_From as $_rs_From){
// foreach ($UpdateSuccess['Group'] as $GroupID=>$val){
	$TableHTML .= '<tr>';
	#GroupNameEN
	$TableHTML .= '<td>'.$_rs_From['GroupNameEN'].'</td>';
	#GroupNameCH
	$TableHTML .= '<td>'.$_rs_From['GroupNameCH'].'</td>';
	#Copy
	$TableHTML .= '<td>';
	switch ($UpdateSuccess['Group'][$_rs_From['GroupID']]){
		case '1':
			$CopyGroupSuccess = $Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccess'];
			break;
		case '0':
			$CopyGroupSuccess = '<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopyFail'].'</span>';
			break;
		case '-1':
			$CopyGroupSuccess = '<span class="tabletextremark">'.Get_String_Display('').'</span>';
			break;
	}
	$TableHTML .=  $CopyGroupSuccess;
// 	$TableHTML .=  $UpdateSuccess['Group'][$GroupID]? str_replace('<--Count-->', '', $Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccess']):$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopyFail'];
	
	$TableHTML .= '</td>';
	
	#CopyMember
	$TableHTML .= '<td>';
	switch ($UpdateSuccess['Member'][$_rs_From['GroupID']]){
		case '1':
			$CopyMemberSuccess= str_replace('<--Count-->', $Update[$_rs_From['GroupID']], $Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccessWRecord']);
			break;
		case '0':
			$CopyMemberSuccess= '<span class="tabletextremark">'.$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopyFail'].'</span>';
			break;
		case '-1':
			$CopyMemberSuccess= '<span class="tabletextremark">'.Get_String_Display('').'</span>';
			break;
	}
	$TableHTML .= $CopyMemberSuccess;
	$TableHTML .= '</td>';
	
	$TableHTML .= '</tr>';
}
$TableHTML .= '</table>';
$TableHTML .= '';
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:window.location='?t=settings.monitoringGroupSetting.list'","back"," class='formbutton_v30'");
###Result Table END

#Navigation
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['GroupList'],'?t=settings.monitoringGroupSetting.list');
$navigationArr[] = array($Lang['SDAS']['MonitoringGroupSetting']['CopyGroup'],'');
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationArr);
#Step Obj
$STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 1);
$htmlAry['stepobj'] = $linterface->GET_STEPS($STEPS_OBJ);
$linterface->LAYOUT_START();
?>
<br />
<?=$htmlAry['navigation']?>
<div style="padding-top:25px;"><?= $htmlAry['stepobj']?></div>
<br />
<br style="clear:both;" />
<table class="form_table_v30">
	<tr>
		<td style="border-bottom-style:none;"><?=$linterface->GET_NAVIGATION2($Lang['Group']['Group'])?></td>
	</tr>
	<tr>
		<td style="border-bottom-style:none;"><?=$TableHTML?></td>
	</tr>
</table>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$CancelBtn?>
	<p class="spacer"></p>
</div>
<?php
$linterface->LAYOUT_STOP();
?>
?>