<?php 
/*
 * Change Log:
 * Date		2017-07-13	Villa
 * 			Update the Success Status: -1=>Not select to update, false(0) => Update Fail, true(1) => Update Success
 * Date		2017-06-08	Villa
 * -		Open the file
 */
######## Page Setting START ########
$CurrentPage = "settings.monitoringGroup";
$CurrentPageName = $Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $objSDAS->GET_MODULE_OBJ_ARR();
$Json = new JSON_obj();
######## Page Setting END ########
###Update Process START
###GroupInfo START
$sql = 'SELECT
			GroupID, GroupNameEN, GroupNameCH
		FROM
			MONITORING_GROUP
		WHERE
			AcademicYearID = '.$FromAcademicYearID.'
		AND
			IsDeleted = "0"
		ORDER BY
			GroupID desc';
$rs =  $objSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	$rs_From[$_rs['GroupID']] = $_rs;
	$UpdateSuccess['Group'][$_rs['GroupID']] = '-1';
	$UpdateSuccess['Member'][$_rs['GroupID']] = '-1';
}
$CopyGroup_GroupID = explode(',',$CopyGroup_GroupID_Str);
foreach ((array)$CopyGroup_GroupID as $_CopyGroup_GroupID){
	$sql = 'Insert into 
				MONITORING_GROUP  
					(AcademicYearID, GroupNameEN, GroupNameCH, IsDeleted, DateInput, InputBy, DateModified, ModifiedBy)
			VALUES
					("'.$ToAcademicYearID.'","'.$rs_From[$_CopyGroup_GroupID]['GroupNameEN'].'","'.$rs_From[$_CopyGroup_GroupID]['GroupNameCH'].'","0",now(),"'.$_SESSION['UserID'].'",now(),"'.$_SESSION['UserID'].'")';
	if($objSDAS->db_db_query($sql)){
		$GropuMapping[$_CopyGroup_GroupID] = $objSDAS->db_insert_id();
		$UpdateSuccess['Group'][$_CopyGroup_GroupID] = true;
	}else{
		$UpdateSuccess['Group'][$_CopyGroup_GroupID] = false;
	}
}
###GroupInfo END
###GroupMember Start
$CopyMember_GroupID = explode(',', $CopyMember_GroupID_Str);
$sql = 'SELECT
			GroupID, UserID, RecordType
		FROM
			MONITORING_GROUP_MEMBER
		WHERE
			GroupID in ('.$CopyMember_GroupID_Str.')
		AND
			IsDeleted = "0"
		ORDER BY
			GroupID desc';
$rs =  $objSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	$rs_Member_From[$_rs['GroupID']][] = $_rs;
}

foreach ((array)$CopyMember_GroupID as $_CopyMember_GroupID){
	$insertValue = '';
	$comma = '';
	$ToGroup = $GropuMapping[$_CopyMember_GroupID];
	$Update[$_CopyMember_GroupID] = 0;
	if($ToGroup){
		foreach ((array)$rs_Member_From[$_CopyMember_GroupID] as $_Member){
			$insertValue .= $comma.'("'.$ToGroup.'","'.$_Member['UserID'].'","'.$_Member['RecordType'].'","0",now(),"'.$_SESSION['UserID'].'",now(),"'.$_SESSION['UserID'].'")';
			$comma = ',';
			$Update[$_CopyMember_GroupID]++;
		}
		$sql = 'Insert into
					MONITORING_GROUP_MEMBER
				(GroupID, UserID, RecordType, IsDeleted, DateInput, InputBy, DateModified, ModifiedBy)
				VALUES
					'.$insertValue;
		if($objSDAS->db_db_query($sql)){
			$UpdateSuccess['Member'][$_CopyMember_GroupID] = true;
		}else{
			$UpdateSuccess['Member'][$_CopyMember_GroupID] = false;
		}
	}else{
		$UpdateSuccess['Member'][$_CopyMember_GroupID] = false;
	}
}
###GroupMember END
###Update Process END
$Data['UpdateSuccess'] = $UpdateSuccess;
$Data['Update'] = $Update;
$Data['FromAcademicYearID'] = $FromAcademicYearID;
$Json_Data= $Json->encode($Data);
echo $Json_Data;
###Result Table END
?>
