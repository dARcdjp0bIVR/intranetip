<?php 
/*
 * Using: Villa
 * Change Log:
 * Date:	2017-06-05 Villa
 * 			- Open the file
 */

######## Data Getting START########
if($GroupID){
	$sql = 'Select
			mg.GroupID,
			mg.AcademicYearID,
			mg.GroupNameEN,
			mg.GroupNameCH,
			mg.DateModified
		FROM
			MONITORING_GROUP mg
		WHERE
			mg.GroupID = "'.$GroupID.'"';
	$Data = $objSDAS->returnResultSet($sql);
	$Data = $Data[0];
}
######## Data Getting END########

###AcademicYear Selection Start###
$sql = 'SELECT
			ay.AcademicYearID, ay.YearNameEN, ay.YearNameB5
		FROM
			ACADEMIC_YEAR ay
		INNER JOIN
			ACADEMIC_YEAR_TERM ayt
				ON	ay.AcademicYearID = ayt.AcademicYearID
		GROUP By
			ay.AcademicYearID
		ORDER BY
			ayt.TermStart
				DESC';
$rs = $objSDAS->returnResultSet($sql);
$htmlAry['AcademicYear']= '<select id="AcademicYearID" name="AcademicYearID">';
foreach((array)$rs as $_rs){
	if($_rs['AcademicYearID'] == $Data['AcademicYearID']){
		$checked = 'selected';
	}else{
		$checked = '';
	}
	$htmlAry['AcademicYear'].= '<option value="'.$_rs['AcademicYearID'].'" '.$checked.'>'.Get_Lang_Selection($_rs['YearNameB5'], $_rs['YearNameEN']).'</option>';
}
$htmlAry['AcademicYear'].= '</select>';
###AcademicYear Selection END###

// Save Button
$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "ajax_SubmitForm();");
$CancelBtn  = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", " window.top.tb_remove();");
?>
<script type="text/javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";
var RoleType = 'T'; //setting for AutoComplete @ajax_add_member_thickbox
$( document ).ready(function() {
	OnloadPICSelect();
});

function ajax_SubmitForm(){
	if(checkform()){
		$(this).attr('disabled','disabled');	
		$.ajax({
			async: false,
			url: '?t=settings.monitoringGroupSetting.ajax_add_group_update',
			type: 'POST',
			data: $('#form1').serialize(),
			success: function(GroupID){
				Add_Role_Member_1(GroupID);
			}
		});
	}
}
function OnloadPICSelect(){
	$('div#PIC_Div').html(loadingImage);
	$.ajax({
		url: '?t=settings.monitoringGroupSetting.ajax_add_member_thickbox',
		type: 'POST',
		data: {GroupID : '<?=$GroupID?>', RecordType : '1'},
		success: function(ReturnHTML){
			$('div#PIC_Div').html(ReturnHTML);
		}
	});
}
function Add_Role_Member_1(GroupID){
	var GroupID = GroupID;
	var RecordType = '1';
	var UserID = '';
	var comma = '';
	var UserSelected = document.getElementById('AddUserID[]');
	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		UserID += comma + UserSelected.options[i].value;
		comma = ',';
	}
	$('#AddMemberSubmitBtn').attr('disabled','disabled');
// delete All Member in Group 
	$.ajax({
		url: "?t=settings.monitoringGroupSetting.ajax_delete_function",
		type: 'POST',
		async: false,
		data: {
			GroupID: GroupID, 
			RecordType: RecordType,
			Action: 'Delete_GROUP_RecordType_MONITORING_GROUP_MEMBER'
			},
		success: function(){
		}
	});
//Add UserToTheDb
	$.ajax({
		url: '?t=settings.monitoringGroupSetting.ajax_add_member_update',
		type: 'POST',
		async: false,
		data: {UserIDStr: UserID, GroupID: GroupID, RecordType: RecordType},
		success: function(ReturnMsg){
			window.top.tb_remove();
			Get_Return_Message(ReturnMsg);
			RecordListTable();
		}
	});
}
function checkform(){
	var check = true;
	if($('#GroupNameCH').val()==''){
		alert('<?=$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameCH']?>');
		check = false;
	}
	if($('#GroupNameEN').val()==''){
		alert('<?=$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameEN']?>');
		check = false;
	}
	if(check){
		return true;
	}else{
		return false;
	}
}
</script>
<form id='form1'>
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'] ?></td>
			<td><input name="GroupNameCH" id="GroupNameCH" value='<?=$Data['GroupNameCH']?>'></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'] ?></td>
			<td><input name="GroupNameEN" id="GroupNameEN" value='<?=$Data['GroupNameEN']?>'></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['AcademicYear'] ?>:</td>
			<td><?=$htmlAry['AcademicYear']?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['PIC'] ?>:</td>
			<td><div id='PIC_Div'></div></td>
	</table>
	<div class="edit_bottom_v30">
		<?=$SaveBtn?>
		<?=$CancelBtn?>
	</div>
	<input type='hidden' id='GroupID' name='GroupID' value='<?=$Data['GroupID']?>'>
</form>

