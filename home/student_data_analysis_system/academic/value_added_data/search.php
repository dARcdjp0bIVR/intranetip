<?php
//Using: 
/**
 * Change Log:
 * 2019-01-04 Pun [128759]
 *  - Fixed can select whole year for term selection
 * 2017-08-25 Pun
 *  - File Create
 */

######## Init START ########
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/valueAddedData/libValueAddedData.php");
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
######## Init END ########

// debug_pr($accessRight);
######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');


$ayterm_selection_html = '';
if(isset($sys_custom['iPf']['valueAddedDataReport'])){
	$accessRight['admin'] = 1;
}
if(!$accessRight['admin'] ){
	if(count($accessRight['classTeacher'])){
		$isClassTeacher = 1;
	}
}
// }
######## Access Right END ########


######## Subject Panel filter subject START ########
/*$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if(
	!$accessRight['admin'] && 
	count($accessRight['subjectPanel']) && 
	in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
){
	include_once($PATH_WRT_ROOT."includes/json.php");
	$json = new JSON_obj();
	foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
		foreach($_yearIdArr as $_yearID=>$_){
			$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
		}
	}
	$subjectPanelAccessRightList = $json->encode($subjectPanelAccessRightArr); // For PHP 2D Array to JS Object
	
	$isSubjectPanelView = true;
}

if(!$accessRight['admin'] && !$isClassTeacher && !$isSubjectPanelView){
	echo 'No access right';
	return false;
}
*/
######## Subject Panel filter subject END ########

######## Page Setting START ########
$CurrentPage = "value_added_data";
$TAGS_OBJ[] = array($Lang['SDAS']['ValueAddedData']['Tab']['ClassValueAdded'],"", 1);
$TAGS_OBJ[] = array($Lang['SDAS']['ValueAddedData']['Tab']['TeacherValueAdded'],"?t=academic.value_added_data.search2");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);
######## UI Releated END ########

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();

?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>


<form id="form1" name="form1">
	<input type="hidden" name="reportType" value="<?=libValueAddedData::VALUE_ADDED_TYPE_SUBJECT_GROUP ?>" />

<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">

<!-------- Form START --------> 

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
			<?=$html_year_selection?> 
		</td>
	</tr>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_student_Performance']['Compare']?> <?=$Lang['General']['Term']?></span></td>
		<td valign="top">
		 	<span id='ayterm_cell'><?=$ayterm_selection_html?></span> VS <span id='ayterm_cell_VS'><?=$ayterm_selection_html?></span>
		</td>
	</tr>
	<!--tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
		<td valign="top" id='y_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
	</tr-->

<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
	<div id="PrintButton">
	<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="resultDiv"></div></div>
</form>





<script language="javascript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};



$(function(){
	
	$('#option').hide();
////////For Class Teacher END ////////
	$('#academicYearID').change();
});

function get_yearterm_opt(){
	var ay_id = $('#academicYearID').val();
	if(ay_id == ''){
		$("#ayterm_cell").html("<select></select>");
		$("#ayterm_cell_VS").html("<select></select>");
		return;
	}

	var tag = "id=YearTermID name=YearTermID";
	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag,
					"hideOverall":true
				},	
		success: function (msg) {
			$("#ayterm_cell").html(msg);
		}
	});

	var tag = "id=YearTermID_VS name=YearTermID_VS";
	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag,
					"hideOverall":true
				},
		success: function (msg) {
			$("#ayterm_cell_VS").html(msg);
		}
	});
}



$('#academicYearID').change(function(){
	var filterSubjectArr = '';
	$("#ayterm_cell").html('').append(
		$(loadingImage).clone()
	);
	$("#ayterm_cell_VS").html('').append(
		$(loadingImage).clone()
	);

	$("#y_cell").html('').append(
		$(loadingImage)
	);
	
	get_yearterm_opt();
});



$('#viewForm').click(get_report);
function get_report()
{
	var form = $('#form1');

	if($('#academicYearID').val() == '' || $('#academicYearID_VS').val() == ''){
		alert("<?=$iPort["options_select"]?> <?=$Lang['SDAS']['Term']?>");
		return false;
	}
	
	if($("[name='yearID']").val()==''){
		alert("<?=$Lang['General']['JS_warning']['SelectAForm']?>");
		return false;
	}
	
  	$.ajax({
  		type: "POST",
  		url: "/home/student_data_analysis_system/ajax/ajax_value_added_data.php",
  		data: $('#form1').serialize(),
  		beforeSend: function () {
      		$("#resultDiv").parent().children().remove().end().append(
				$("<div></div>").attr("id", "resultDiv").append(
					$(loadingImage)
				)
            ); 
      	},
  		success: function (msg) {
  			$("#resultDiv").html(msg);
//	  			$("#PrintButton").show();
  			$( "#PrintBtn" ).show();
  		}
  	});
}


</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
