<?php
// using : 

/**
 * Change Log:
 * 2018-07-26 Bill
 * 	-	New File
 */

######## Init [START] ########
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();

$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libSDAS = new libSDAS();
######## Init [END] ########

######## Access Right [START] ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
######## Access Right [END] ########

######## Current Page Setting [START] ########
$CurrentPage = "report_comparsion";
$CurrentPageName = $Lang['SDAS']['menu']['ReportComparsion'];

$TAGS_OBJ[] = array($CurrentPageName, "");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Current Page Setting [END] ########

######## UI Related [START] ########

### Form Selection Box [START] ###
if($accessRight['admin'] || $accessRight['subjectPanel']) {
    $YearIDArr = '';
    $ClassIDArr = '';
} else {
    if(count($accessRight['classTeacher']) > 0){
        $YearIDArr = Get_Array_By_Key($accessRight['classTeacher'], 'YearID');
        $ClassIDArr = Get_Array_By_Key($accessRight['classTeacher'], 'YearClassID');
        $ClassIDArr = implode(',', $ClassIDArr);
    }
}
if($accessRight['admin'] || $accessRight['subjectPanel']) {
    $FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, ' js_Changed_Form_Selection(this.value); ', $noFirst=1, $isAll=0, $isMultiple=0);
} else {
    if(count($accessRight['classTeacher']) > 0){
        $FormSelection = $libFCM_ui->Get_Specific_Form_Selection('YearID', $YearIDArr, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
    }
}

### Year Selection Box [START] ###
$YearArr = $li_pf->returnAssessmentYear();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$presetYearIDArr = array($currentAcademicYearID, Get_Previous_Academic_Year_ID());
$YearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, " name='AcademicYearID[]' id='AcademicYearID' multiple='true' size='10' ", $presetYearIDArr, 0, 1, "", 2);

### Subject Selection Box [START] ###
// $classSelectionHTML = '<select id="YearClassID" name="YearClassID[]" onchange="js_Reload_Student_Selection()">';
// foreach($accessRight['classTeacher'] as $yearClass){
//     $name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
//     $classSelectionHTML .= "<option value=\"{$yearClass['YearClassID']}\">{$name}</option>";
// }
// $classSelectionHTML .= '</select>';

### Term Selection Box [START] ###
$TermArr = array();
$TermArr[] = array('1', $Lang['SDAS']['ReportComparison']['Term1']);
$TermArr[] = array('2', $Lang['SDAS']['ReportComparison']['Term2']);
$TermSelection = getSelectByArray($TermArr, " name='targetTermNumber' id='targetTermNumber' ", '1', 0, 1, "", 2);

### Subject Selection Box [START] ###
$subjectIdFilterArr = '';
if(!$accessRight['admin'] && (count($accessRight['subjectPanel']) || count($accessRight['subjectTeacher']))){
    $subjectIdFilterArr = array_merge((array)array_keys((array)$accessRight['subjectPanel']), $accessRight['subjectTeacher']);
}
if(count($accessRight['classTeacher'])){
    $subjectIdFilterArr = '';
}
if(!count($accessRight['subjectPanel']) && count($accessRight['MonitoringGroupPIC'])){
    $subjectIdFilterArr = '';
}
$SubjectSelection = $libSCM_ui->Get_Subject_Selection('SubjectID[]', $SubjectID, $OnChange='', 1, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=1, $IncludeSubjectIDArr=$subjectIdFilterArr);

### Subject Group Selection Box [START] ###
if($accessRight['admin']) {
    $SubjectGroupSelection = $libSCM_ui->Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID('', '', '', 'SubjectGroupID[]', '', $IsMultiple=1);
} else {
    if(count($accessRight['subjectPanel'])){
        foreach ($accessRight['subjectPanel'] as $subjectPanel => $_subjectPanel) {
            $subjectIDKey[] = $subjectPanel;
        }
    }
    $SubjectGroupSelection = $libSCM_ui->Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID('', $accessRight['subjectGroup'], $subjectIDKey, 'SubjectGroupID[]', '', $IsMultiple=1);
}

### Type Radio Btn [START] ###
$selected = '1';
if(count($accessRight['classTeacher']) || count($accessRight['subjectPanel']) || $accessRight['admin']) {
    $TypeRadioBtn = $linterface->Get_Radio_Button('type_Class', 'type', 'Class', $selected, '', $iPort['Class'], 'js_Changed_Type(this.value)');
    $TypeRadioBtn .= "&nbsp";
    $selected = '';
}
if(count($accessRight['subjectPanel']) || $accessRight['admin']) {
    $TypeRadioBtn .= $linterface->Get_Radio_Button('type_SubjectClass', 'type', 'SubjectClass', $selected, '', $iPort['SubjectGroup'], 'js_Changed_Type(this.value)');
    $selected = '';
}

### Action Button [START] ###
$btnAry[] = array('print', 'javascript:Print()', '', array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated [END] ########

######## UI [START] ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script>
function Print() {
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$(print).printArea(options);
};

function exportCSV() {
	// for csv display multiple headers
	$("tr.display_table_header").hide();
	$("tr.export_table_header").show();
	
	$("#DBTableDiv table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'report_comparison'
	});

	$("tr.display_table_header").show();
	$("tr.export_table_header").hide();
}

// $('body').on('click', '#ExportBtn', function () {
// 	$("#DBTableDiv table").tableExport({
// 		type: 'csv',
// 		headings: true,
// 		fileName: 'learn_and_teach_teacher_report'
// 	});
// });
</script>

<script language="JavaScript">
var jsCurMode = '';
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurAcademicYearID = '<?=$currentAcademicYearID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	
	js_Changed_Form_Selection(jsCurYearID);
	$('#assessmentAll').click();
	$('#compareAll').click();

	<?php if(count($accessRight['classTeacher']) || count($accessRight['subjectPanel']) || $accessRight['admin']) { ?>
		js_Changed_Type('Class');
	<?php } else { ?>
		js_Changed_Type('SubjectClass');
	<?php } ?>
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
});

function js_Changed_Type(jsType)
{
	jsCurMode = jsType;
	if(jsType == 'Class') {
		js_Select_All($('select[name="SubjectID[]"]')[0]);
		js_Unselect_All($('select[name="SubjectGroupID[]"]')[0]);
		
		$('#formRow').show();
		$('#classRow').show();
		$('#studentRow').show();
		$('#subjectRow').show();
		$('#subjectGroupRow').hide();
		js_Disable_Subject();
	}
	if(jsType == 'SubjectClass') {
		js_Unselect_All($('select[name="SubjectID[]"]')[0]);
		js_Select_All($('select[name="SubjectGroupID[]"]')[0]);
		
		$('#formRow').hide();
		$('#classRow').hide();
		$('#studentRow').hide();
		$('#subjectRow').hide();
		$('#subjectGroupRow').show();
	}
}

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
    <?php if($accessRight['admin']) { ?>
    	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
    		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
    		{
    			Action: 'Class_Selection',
    			AcademicYearID: jsCurAcademicYearID,
    			YearID: jsCurYearID,
    			SelectionID: 'YearClassID',
    			OnChange: "js_Reload_Student_Selection(); js_Disable_Subject();",
    			IsMultiple: 1,
    			NoFirst: 1,
    			IsAll: 1
    		},
    		function(ReturnData)
    		{
    			js_Select_All($('select#YearClassID')[0]);
    			jsCurYearClassID = $('select#YearClassID').val();
    			
    			js_Reload_Student_Selection();
    			js_Disable_Subject();
    		}
    	);
    <?php } else { ?>
    	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
    		"/home/student_data_analysis_system/ajax/ajax_get_class_selection_box.php", 
    		{
    			Action: 'Class_Selection',
    			AcademicYearID: jsCurAcademicYearID,
    			YearID: jsCurYearID,
    			SelectedYearClassID: jsCurYearClassID,
    			SelectionID: 'YearClassID',
    			OnChange: 'js_Reload_Student_Selection(); js_Disable_Subject();',
    			IsMultiple: 1,
    			NoFirst: 1,
    			IsAll: 1,
    			ClassID: '<?=$ClassIDArr?>',
    		},
    		function(ReturnData)
    		{
    			js_Select_All($('select#YearClassID')[0]);
    			jsCurYearClassID = $('select#YearClassID').val();
    			
    			js_Reload_Student_Selection();
    			js_Disable_Subject();
    		}
    	);
	<?php } ?>
}

function js_Reload_Student_Selection()
{
	jsCurYearClassID = $('select#YearClassID').val();
	$('div#StudentSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Student_Selection',
			AcademicYearID: jsCurAcademicYearID,
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'StudentID[]',
			IsMultiple: 1,
			NoFirst: 1,
			IsAll: 1
		},
		function(ReturnData)
		{
			js_Select_All($('select[name="StudentID[]"]')[0]);
		}
	);
}

function js_Reload_DBTable()
{
	$("#PrintBtn").hide();
	$("#ExportBtn").hide();

	if(jsCurMode == 'Class')
	{
    	if($('#YearClassID').length == 0 || $('#YearClassID').val() == '' || $('#YearClassID').val() == null) {
    		alert('<?=$ec_warning['select_class']?>');
    		return;
    	}
    	if($('select[name="StudentID[]"]').length == 0 || $('select[name="StudentID[]"]').val() == '' || $('select[name="StudentID[]"]').val() == null) {
    		alert('<?=$ec_warning['select_student']?>');
    		return;
    	}
    	if($('select[name="SubjectID[]"]').length == 0 || $('select[name="SubjectID[]"]').val() == '' || $('select[name="SubjectID[]"]').val() == null) {
    		alert('<?=$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubject']?>');
    		return;
    	}
	}
	else
	{
    	if($('select[name="SubjectGroupID[]"]').length == 0 || $('select[name="SubjectGroupID[]"]').val() == '' || $('select[name="SubjectGroupID[]"]').val() == null) {
    		alert('<?=$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubjectGroup']?>');
    		return;
    	}
	}
	if($('select[name="AcademicYearID[]"]').length == 0 || $('select[name="AcademicYearID[]"]').val() == '' || $('select[name="AcademicYearID[]"]').val() == null || $('select[name="AcademicYearID[]"]').val().length < 2) {
		alert('<?=$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelect2Years']?>');
		return;
	}
	if($('input[name="targetAssessment[]"]:checked').length == 0) {
		alert('<?=$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectAssessment']?>');
		return;
	}
	if($('input[name="compareColumn[]"]:checked').length == 0) {
		alert('<?=$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectCompareTarget']?>');
		return;
	}

	if($('input#type').length > 0) {
		$('input#type').val(jsCurMode);
	}
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
// .load(
// 		"/home/student_data_analysis_system/ajax/ajax_report_comparsion.php",
// 		$("#form1").serialize(),
// 		function(ReturnData)
// 		{
// 			$("#PrintBtn").show();
// 			$("#ExportBtn").show();
// 		}
// 	)
	$.ajax({
	    'url': '/home/student_data_analysis_system/ajax/ajax_report_comparsion.php',
	    'type': 'POST',
	    'data': $("#form1").serialize(),
	    'success': function(result){
	    	$('div#DBTableDiv').html(result);
			$("#PrintBtn").show();
			$("#ExportBtn").show();
	    }
	});
}

function js_Disable_Subject()
{
    <?php if(!$accessRight['admin'] && count($accessRight['subjectPanel']) > 0) { ?>
    	$.ajax({
    		async: false,
    		url: "index.php?t=ajax.ajax_returnSubjectID_arr",
    		type: "POST",
    		data: {"YearID":$('select#YearID').val(), "YearClassID": $('select#YearClassID').val() },	
    		success: function(ReturnData){
    			HideAndDisableSubject(ReturnData);
    		}
    	});
	<?php } ?>
}

function HideAndDisableSubject(jSonData)
{
	var data = JSON.parse(jSonData);
	var option = document.createElement("option");
	
	if(data['NeedFilter']==true) {
		$('select[name="SubjectID[]"] option').each(function() {
			$(this).attr("disabled", true);
			$(this).hide();
			$(this).parent('optgroup').hide();
		});
		$('select[name="SubjectID[]"] option').each(function() {
			if($.inArray(parseInt($(this).val()),data['SubjectIDArr']) < 0) {
				// do nothing
			} else {
				$(this).removeAttr("disabled");
				$(this).show();
				$(this).parent('optgroup').show();
			}
		});
	} else {
		$('select[name="SubjectID[]"] option').each(function() {
			$(this).removeAttr("disabled");
			$(this).show();
			$(this).parent('optgroup').show();
		});
	}
}

function js_Select_All(obj)
{
	if(obj && obj.length > 0) {
    	for (i=0; i<obj.length; i++) {
    		obj.options[i].selected = true;
    	}
	}
}

function js_Unselect_All(obj)
{
	if(obj && obj.length > 0) {
    	for (i=0; i<obj.length; i++) {
    		obj.options[i].selected = false;
    	}
	}
}
</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" action="/home/portfolio/profile/management/ajax_get_mark_analysis.php" method="POST" target="_blank">
	<input type="hidden" name="Action" value="REPORT_COMPARISON" />
	<input type="hidden" id="isExport" name="isExport" value="0" />
	<?= $html_tab_menu ?>
	
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
		<?php if($accessRight['admin'] || count($accessRight['subjectPanel']) || (count($accessRight['classTeacher']) && count($accessRight['subjectPanel']) || 
		              (count($accessRight['classTeacher']) && count($accessRight['MonitoringGroupPIC'])) || (count($accessRight['MonitoringGroupPIC']) && count($accessRight['subjectPanel'])))){ ?>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Mode']?></span></td>
				<td valign="top"><?=$TypeRadioBtn?></td>
			</tr>
		<?php } else { ?>
			<input type="hidden" id="type" name="type" value="" />
			<input type="hidden" id="singleTypeOnly" name="singleTypeOnly" value="1" />
		<?php } ?>
		<tr id='formRow'>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
			<td valign="top"><?=$FormSelection?></td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
			<td valign="top"><div id="ClassSelectionDiv"></div></td>
		</tr>
		<tr id='studentRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort['usertype_s']?></span></td>
			<td valign="top"><div id="StudentSelectionDiv"></div></td>
		</tr>
		<tr id='subjectRow'>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['subject']?></span></td>
			<td valign="top"><?=$SubjectSelection?></td>
		</tr>
		<tr id='subjectGroupRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort['SubjectGroup']?></span></td>
			<td valign="top"><?=$SubjectGroupSelection?></td>
		</tr>
		<tr id='yearRow'>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
			<td valign="top"><?=$YearSelection?></td>
		</tr>
		<tr id='termRow'>
			<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['term']?></span></td>
			<td valign="top"><?=$TermSelection?></td>
		</tr>
		<tr id='assessmentRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['ReportComparison']['Assessment']?></span></td>
			<td valign="top">
				<input type="checkbox" id="assessmentAll"/>
				<label for="assessmentAll"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
				
				<input type="checkbox" id="assessmentCA" name="targetAssessment[]" value="A1" class="displayField" />
				<label for="assessmentCA"><?php echo $Lang['SDAS']['ReportComparison']['AssessmentAry']['CA'] ?></label>&nbsp;
				
				<input type="checkbox" id="assessmentExam" name="targetAssessment[]" value="A2" class="displayField" />
				<label for="assessmentExam"><?php echo $Lang['SDAS']['ReportComparison']['AssessmentAry']['Exam'] ?></label>&nbsp;
				
				<input type="checkbox" id="assessmentYear" name="targetAssessment[]" value="NULL" class="displayField" />
				<label for="assessmentYear"><?php echo $Lang['SDAS']['ReportComparison']['AssessmentAry']['YearGrade'] ?></label>&nbsp;
			</td>
		</tr>
		<tr id='compareRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['ReportComparison']['Compare']?></span></td>
			<td valign="top">
				<input type="checkbox" id="compareAll" />
				<label for="compareAll"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
				
				<input type="checkbox" id="compareScore" name="compareColumn[]" value="score" class="displayField" />
				<label for="compareScore"><?php echo $ec_iPortfolio['score']?></label>&nbsp;
				
				<input type="checkbox" id="compareStandardScore" name="compareColumn[]" value="sd" class="displayField" />
				<label for="compareStandardScore"><?php echo $ec_iPortfolio['stand_score']?></label>&nbsp;
				
				<input type="checkbox" id="comparePercentile" name="compareColumn[]" value="percentile" class="displayField" />
				<label for="comparePercentile"><?php echo $Lang['SDAS']['ReportComparison']['Percentile']?></label>&nbsp;
			</td>
		</tr>
	</table>
	
	<span class="tabletextremark"></span>
	<p class="spacer"></p>
	
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
</form>

<script>
$('#assessmentAll').change(function() {
	if($(this).attr('checked')) {
		$(this).parent().find('input[name="targetAssessment[]"]').attr('checked','checked');
	} else {
		$(this).parent().find('input[name="targetAssessment[]"]').removeAttr('checked');
	}
});
$('input[name="targetAssessment[]"]').click(function() {
	if(!$(this).attr("checked")) {
		$(this).parent().find("#assessmentAll").removeAttr("checked");
	}
});

$('#compareAll').change(function() {
	if($(this).attr('checked')) {
		$(this).parent().find('input[name="compareColumn[]"]').attr('checked','checked');
	} else {
		$(this).parent().find('input[name="compareColumn[]"]').removeAttr('checked');
	}
});
$('input[name="compareColumn[]"]').click(function() {
	if(!$(this).attr("checked")) {
		$(this).parent().find("#compareAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>