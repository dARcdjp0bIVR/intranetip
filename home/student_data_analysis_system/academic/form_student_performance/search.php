<?php
//Using: anna
/**
 * Change Log:
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-05-10 Villa #T116811 
 * -	Fix Subject Panel Access Right Overwrite ClassTeacher (Subject Panel Cannot Access This Report Now)
 * 2017-02-22 Villa	
 * -	Change the Compare Term can be in Different Academic Year
 * 2017-02-07 Villa 
 * -	Update the UI wording
 * 2017-02-06 Villa 
 * -	add tr for yearterm comparsion
 * 2016_12-23 Villa 
 * -	add Position In Form Subject and Percentile Rank Subject
 * 2016-12-14 Villa 
 * -	update the UI
 * 2016-12-12 Villa	
 * -	modified get_overall_performace - add form submi checking
 * 2016-12-09 
 * -	Villa Change access right
 * 2016-11-25 villa 
 * -	Open the file - copy from other page
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
######## Init END ########

// debug_pr($accessRight);
######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');


$ayterm_selection_html = '';
if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
		in_array('form_student_performance',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}
if(!$accessRight['admin'] ){
	if(count($accessRight['classTeacher'])){
		$isClassTeacher = 1;
	}
}
// }
######## Access Right END ########


######## Subject Panel filter subject START ########
$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if(
	!$accessRight['admin'] && 
	count($accessRight['subjectPanel']) && 
	in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
){
	include_once($PATH_WRT_ROOT."includes/json.php");
	$json = new JSON_obj();
	foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
		foreach($_yearIdArr as $_yearID=>$_){
			$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
		}
	}
	$subjectPanelAccessRightList = $json->encode($subjectPanelAccessRightArr); // For PHP 2D Array to JS Object
	
	$isSubjectPanelView = true;
}

if(!$accessRight['admin'] && !$isClassTeacher && !$isSubjectPanelView){
	if($accessRight['MonitoringGroupPIC']){
		header('location: ?t=academic.form_student_performance.search_monitoringGroup');
	}else{
		echo 'No access right';
		return false;
	}
}

if($accessRight['MonitoringGroupPIC']){
	$monitoringGroup = '<input type="radio" name="target_type" id="target_type_class" value="class" checked>'.'<label for="target_type_class">'.$Lang['SDAS']['Class'].'</label>';
	$monitoringGroup .= '<input type="radio" name="target_type" id="target_type_monitoring_group" value="monitoring_group">'.'<label for="target_type_monitoring_group">'.$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] .'</label>';
}
######## Subject Panel filter subject END ########

######## Page Setting START ########
$CurrentPage = "form_student_performance";
// $CurrentPageName = $Lang['SDAS']['ClassPerformance']['IndividualYear'];
$TAGS_OBJ[] = array($Lang['SDAS']['menu']['FormStudentPerformance'],"");
// $TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"index.php?t=academic.subject_class_distribution.search2");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 0, "", 2);
$html_year_selection_VS = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID_VS' id='academicYearID_VS'", $currentAcademicYearID, 0, 0, "", 2);

//$html_classlevel_selection = getSelectByArray(array(), "name='YearID'", $YearID, 0, 0, "", 2);
######## UI Releated END ########

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
echo $linterface->Include_TableExport();
?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>


<form id="form1" name="form1" method="get" action="overall_performance_stat.php">


<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">

<!-------- Form START --------> 

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?>/ <?=$Lang['General']['Term']?></span></td>
		<td valign="top">
			<?=$html_year_selection?>&nbsp;	<span id='ayterm_cell'><?=$ayterm_selection_html?></span> 
		</td>
	</tr>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_student_Performance']['Compare']?> <?=$ec_iPortfolio['year']?>/ <?=$Lang['General']['Term']?></span></td>
		<td valign="top">
		 	<?=$html_year_selection_VS?>&nbsp; <span id='ayterm_cell_VS'><?=$ayterm_selection_html?></span>
		</td>
	</tr>
	<?php if($monitoringGroup){?>
	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Mode']?></span></td>
		<td><?=$monitoringGroup?></td>
	</tr>
	<?php }?>
	<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
		<td valign="top" id='y_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
	</tr>
	<tr id='YearClassRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
		<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>		
	</tr>
	<tr id='SubjectRow'>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['menu']['percentile']." ".$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>		
	</tr>
	<tr id='SubjectRow'>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['PositionInFormDiff'] ." ".$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell_2'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></td>		
	</tr>
	<tr id='OptionRow'>
		<td class="field_title"><span class="tabletext"><?php echo $Lang['SDAS']['SubjectNameDisplay'];?></span></td>
		<td valign="top" id='opt_cell'>
			<input type="radio" name="displayForm" id="displayForm_short" value="0" checked/><label for="displayForm_short"><?php echo $Lang['SDAS']['SubjectNameShortForm']; ?></label>
			<input type="radio" name="displayForm" id="displayForm_full" value="1" /><label for="displayForm_full"><?php echo $Lang['SDAS']['SubjectNameFullName'];?></label>
		</td>
	</tr>

<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
	<div id="PrintButton">
	<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="overallPerformancDiv"></div></div>
</form>





<script language="javascript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Form Student Performance'
	});

}

$(function(){
	
	$('#option').hide();
////////For Class Teacher END ////////
	$('#academicYearID').change();
	$('#academicYearID_VS').change();
	
	$('input[name="target_type"]').change(function(){
		if($(this).val() == 'monitoring_group'){
			window.location.href = '?t=academic.form_student_performance.search_monitoringGroup';
		}
	});
	
});

function get_yearterm_opt(){
	var ay_id = $('#academicYearID').val();
	var tag = "id=YearTermID name=YearTermID";
	if(ay_id == ''){
		$("#ayterm_cell").html("");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag
				},	
		success: function (msg) {
			$("#ayterm_cell").html(msg);
		}
	});
}
function get_yearterm2_opt(){
	var ay_id = $('#academicYearID_VS').val();
	var tag = "id=YearTermID_VS name=YearTermID_VS";
	if(ay_id == ''){
		$("#ayterm_cell_VS").html("");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data:	{
					"AcademicYearID":ay_id,
					"tag":tag
				},
		success: function (msg) {
			$("#ayterm_cell_VS").html(msg);
		}
	});
}
function get_year_opt(){
	var ay_id = $('#academicYearID').val();
	var isClassTeacher = '<?=$isClassTeacher?>';

	if(ay_id == ''){
		$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class'] ?>");
		$("#y_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_opt2.php",
		data: "ay_id="+ay_id,
		data: { "ay_id":ay_id,
				"isClassTeacher": isClassTeacher
		},
		success: function (msg) {
			$("#y_cell").html(msg);
			$("#y_cell").find('[name="yearID"]').change(get_yearclass_selection);
			get_yearclass_selection();
		}
	
	});


}
function get_yearclass_selection()
{

	var y_id = $("[name=yearID]").val();
	var ay_id = $("#academicYearID").val();
	var isClassTeacher = '<?=$isClassTeacher?>';
	var filterSubjectArr = '';
	if(typeof y_id == "undefined" || y_id == ""){
		$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
		return;
	}

	$("#yclass_cell").html('').append(
		$(loadingImage)
	);
	
	$.ajax({
		type: "POST",
		url: "/home/student_data_analysis_system/ajax/ajax_get_class_selection_box.php",
//			data: "y_id="+y_id+"&ay_id="+ay_id,
		data: {
			'AcademicYearID': ay_id,
			'YearID':y_id,
			'isClassTeacher':isClassTeacher,
			'SelectionID' : 'ClassSelection'
			},
		success: function (msg) {
			$("#yclass_cell").html(msg);
			get_subject_multiopt(filterSubjectArr);
			get_subject_multiopt2(filterSubjectArr);
		}

	});
}


function checkAll(){
	var checked = $(this).attr("checked");
	if(($(this).attr("checked"))=="checked" ){
		$(this).parent().parent().parent().find("input[name]").attr("checked", checked);
	
	}
	else{
		$(this).parent().parent().parent().find("input[name]").removeAttr("checked");
	}
	
};



$('#academicYearID').change(function(){
	var filterSubjectArr = '';
	$("#ayterm_cell").html('').append(
		$(loadingImage).clone()
	);
// 	$("#ayterm_cell_VS").html('').append(
// 			$(loadingImage).clone()
// 	);
	$("#y_cell").html('').append(
		$(loadingImage)
	);
	
	get_yearterm_opt();
// 	get_yearterm2_opt();
	get_year_opt();
// 	get_subject_multiopt(filterSubjectArr);
// 	get_subject_multiopt2(filterSubjectArr);
});

$('#academicYearID_VS').change(function(){
	$("#ayterm_cell_VS").html('').append(
			$(loadingImage).clone()
		);
	get_yearterm2_opt();
});



$("#reset_btn").click(function(){
	$("select[name=academicYearID]").val('');

	$("#ayterm_cell").html('');
	$("#y_cell").html('');



	get_yearclass_multiopt();
	get_subject_multiopt();
});


$('#viewForm').click(get_overall_performace);
function get_overall_performace()
{
	var form = $('#form1');

//		if($('#form1').find('.yearClassId').length > 0 && $('#form1').find('.yearClassId:checked').length==0)
//		{
//			return false;
//		}
//		else if($('#form1').find('#subj_cell [name]:checked').length==0)
//		{
		
//			return false;
//		}
	if($("[name='yearID']").val()==''){
		alert("<?=$ec_warning['please_select_class']?>");
		return false;
	}
	
  	$.ajax({
  		type: "POST",
  		url: "/home/student_data_analysis_system/ajax/ajax_form_student_performance.php",
  		data: $('#form1').serialize(),
  		beforeSend: function () {
  		  $("#overallPerformancDiv").parent().children().remove().end().append(
          $("<div></div>").attr("id", "overallPerformancDiv").append(
  		      $(loadingImage)
          )
        ); 
      },
  		success: function (msg) {
  			$("#overallPerformancDiv").html(msg);
//	  			$("#PrintButton").show();
  			$( "#PrintBtn" ).show();
  			$("#ExportBtn").show();	
  		}
  	});
}


////////For Class Teacher START ////////
$('#yclass_cell > select').change(function(){
var classID = $(this).val();
if(classID==''){
	$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
}else{
	$.ajax({
		type: "GET",
		url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
		data: {
			'ay_id': '<?=$currentAcademicYearID?>',
			'y_id': '',
			'yearClassId': $(this).val()
		},
		beforeSend: function () {
			$("#subj_cell").html('').append(
				$(loadingImage)
			);
		},
		success: function (msg) {
			$("#subj_cell").html(msg);
			$("#subj_cell").find('.checkMaster').change(checkAll);
			
			
		}
	});
}
});


function get_subject_multiopt(filterSubjectArr){
	filterSubjectArr = filterSubjectArr || [];

	var y_id = $("[name=yearID]").val();
	var ay_id = $("#academicYearID").val();

	if(typeof y_id == "undefined" || y_id == "")
	{
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		return;
	}
	$("#subj_cell").html('').append(
			$(loadingImage)
	);

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_subject_opt.php",
		data: {
			'ay_id': ay_id,//
			'y_id': y_id,
			'filterSubject[]': filterSubjectArr,
			'AllSubject': '1',
			'CheckBoxName': 'Subject1',
			'task' : 'sbjSelect1'
		},
		success: function (msg) {
			$("#subj_cell").html(msg);
		}
	});
}
function get_subject_multiopt2(filterSubjectArr){
	filterSubjectArr = filterSubjectArr || [];

	var y_id = $("[name=yearID]").val();
	var ay_id = $("#academicYearID").val();

	if(typeof y_id == "undefined" || y_id == "")
	{
		$("#subj_cell_2").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		return;
	}
	$("#subj_cell_2").html('').append(
			$(loadingImage)
	);

	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_subject_opt.php",
		data: {
			'ay_id': ay_id,//
			'y_id': y_id,
			'filterSubject[]': filterSubjectArr,
			'AllSubject': '1',
			'CheckBoxName': 'Subject2',
			'task' : 'sbjSelect2'
		},
		success: function (msg) {
			$("#subj_cell_2").html(msg);
		}
	});
}
	
$('input[name="chartType"]').change(function(){
	if($(this).val()=='column'){
		$('#option').hide();
	}else{
		$('#option').show();
	}
});
</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
