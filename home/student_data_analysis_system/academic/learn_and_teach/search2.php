<?php
//Using: 
/**
 * Change Log:
 * 2017-05-25 Pun
 *  - File Create
 */

######## Init START ########
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");
$objDB = new libdb();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$lnt = new learnAndTeach($objDB);
######## Init END ########

######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();

if(!$accessRight['admin'] && count($accessRight['subjectPanel']) == 0){
    echo 'Access Denied';
    exit();
}
######## Access Right END ########


######## Subject Panel filter subject START ########
/*$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if(
	!$accessRight['admin'] && 
	count($accessRight['subjectPanel']) && 
	in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
){
	include_once($PATH_WRT_ROOT."includes/json.php");
	$json = new JSON_obj();
	foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
		foreach($_yearIdArr as $_yearID=>$_){
			$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
		}
	}
	$subjectPanelAccessRightList = $json->encode($subjectPanelAccessRightArr); // For PHP 2D Array to JS Object
	
	$isSubjectPanelView = true;
}*/
######## Subject Panel filter subject END ########

######## Page Setting START ########
$CurrentPage = "learn_and_teach";
$TAGS_OBJ[] = array($Lang['SDAS']['LearnAndTeach']['TeacherReport'],"index.php?t=academic.learn_and_teach.search");
$TAGS_OBJ[] = array($Lang['SDAS']['LearnAndTeach']['SubjectReport'],"",1);
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
#### AcademicYear Selection START ####
if($accessRight['admin']){
    $rs = $lnt->getAllAcademicYear();
}else{
    $rs = $lnt->getAcademicYearByUserId();
}
$YearArr = array();
foreach($rs as $r){
    $YearArr[] = array(
        $r['AcademicYearID'],
        Get_Lang_Selection($r['YearNameB5'], $r['YearNameEN'])
    );
}
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($data=$YearArr, $tags="name='academicYearID' id='academicYearID'", $selected=$currentAcademicYearID, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
#### AcademicYear Selection END ####


#### Subject Selection START ####
$rs = $lnt->getAllLearningCategory();
$subjectArr = array();
if($accessRight['admin']){
    foreach($rs as $r){
        $subjectArr[] = array(
            $r['LearningCategoryID'],
            Get_Lang_Selection($r['NameChi'], $r['NameEng'])
        );
    }
}else{
    $allSubject = $lnt->getAllSubject();
    $subjectLearningCategory = BuildMultiKeyAssoc($allSubject, array('SubjectID') , array('LearningCategoryID'), $SingleValue=1);
        
    $allowSubjectIdArr = array_keys($accessRight['subjectPanel']);
    $allowLearningCategoryIdArr = array();
    foreach($allowSubjectIdArr as $subjectID){
        $allowLearningCategoryIdArr[] = $subjectLearningCategory[$subjectID];
    }
    foreach($rs as $r){
        if(!in_array($r['LearningCategoryID'], $allowLearningCategoryIdArr)){
            continue;
        }
        $subjectArr[] = array(
            $r['LearningCategoryID'],
            Get_Lang_Selection($r['NameChi'], $r['NameEng'])
        );
    }
}
$html_subject_selection = (sizeof($subjectArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($data=$subjectArr, $tags="name='learningCategoryID' id='learningCategoryID'", $selected=$currentAcademicYearID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
#### Subject Selection END ####
######## UI Releated END ########

######## UI START ########
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:void(0)','',array(),' id="csv_btn_Main" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>


<form id="form1" name="form1" method="get" action="">

<input type="hidden" name="reportType" value="2" />
<input type="hidden" name="showActualValue" value="0" />

<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">

<!-------- Form START --------> 

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
			<?=$html_year_selection?> 
		</td>
	</tr>
	<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['LearningCategory']?></span></td>
		<td valign="top">
			<div id="subjectSelection">
				<?=$html_subject_selection?>
			</div>
		</td>
	</tr>

<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>
	<div id="PrintButton">
	<?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="resultDiv"></div></div>
</form>





<script language="javascript">
var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

$('#csv_btn_Main').click(function(){
	$("#resultDiv table").tableExport({
		type: 'csv',
		fileName: 'learn_and_teach_subject_report'
	});
});

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};


$('#viewForm').click(get_learn_and_teach);
function get_learn_and_teach()
{
	if($('#academicYearID').length == 0 || $('#academicYearID').val() == ''){
		alert('<?=$Lang['SDAS']['Warning']['PleaseSelectAcademicYear']?>');
		return;
	}
	if($('#learningCategoryID').length == 0 || $('#learningCategoryID').val() == ''){
		alert('<?=$Lang['SDAS']['Warning']['PleaseSelectSubject']?>');
		return;
	}
	
  	$.ajax({
        type: "POST",
        url: "/home/student_data_analysis_system/ajax/ajax_learn_and_teach.php",
        data: $('#form1').serialize(),
        beforeSend: function () {
            $( "#PrintBtn" ).hide();
            $( "#csv_btn_Main" ).hide();
            $("#resultDiv").parent().children().remove().end().append(
                $("<div></div>").attr("id", "resultDiv").append(
            		$(loadingImage)
                )
            ); 
    	},
  		success: function (msg) {
  			$("#resultDiv").html(msg);
  			$( "#PrintBtn" ).show();
  			$( "#csv_btn_Main" ).show();
  		}
  	});
}

</script>




<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
