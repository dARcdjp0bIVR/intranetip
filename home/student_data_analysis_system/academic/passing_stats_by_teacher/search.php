<?php
// using :
/**
 * Change Log: 
 * 2020-05-18 Philips [DM3752]
 *  -	Replace From by Fr to avoid blocked ajax request
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-02-21 Villa
 *  -	Delete Console Log
 * 2016-12-29 Villa
 *  -	Fix Subject Panel teacherSelection when the page initializate
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-22 Cara
 *  -	set CurrentAcademicYearID as default value of FrAcademicYearSelection
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$user = new libuser($_SESSION['UserID']);

$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "passing_stats_by_teacher";
$CurrentPageName = $Lang['SDAS']['menu']['PassingRateStats'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## Print Button #######
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$YearArr = $li_pf->returnAssessmentYear();
$FrAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FrAcademicYearID' id='FrAcademicYearID' ", $CurrentAcademicYearID, 0, 0, "", 2);

$isSubjectPanel = false;
if($accessRight['admin']){
	$AllTeachers = $li_pf->GET_ALL_TEACHER(1);
	$TeacherSelection = $linterface->GET_SELECTION_BOX($AllTeachers, "name='TeacherID' id='TeacherID'", "", $ParSelected="");
}else if(count($accessRight['subjectPanel'])){
	$isSubjectPanel = true;
	$TeacherSelection = '';
	$subjectIdArr = array_keys($accessRight['subjectPanel']);
	$subjectIdList = "'".implode("','", $subjectIdArr)."'";
}

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};
function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Passing Rate Stats by teacher'
	});

}
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTermID = '<?=$FrYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}
});

function js_Reload_DBTable()
{
	$( "#PrintBtn" ).hide();
	$("#ExportBtn").hide();
	if ($('#FrAcademicYearID').val() == '')
	{
		alert("<?=$Lang['General']['JS_warning']['SelectSchoolYear']?>");
		return;
	}
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		$("#form1").serialize(),
		function(ReturnData)
		{		
			$("#PrintBtn").show();	
			$("#ExportBtn").show();
		}
	);
	
	
}

$(function(){

	<?php if($isSubjectPanel){ ?>
		$('#FrAcademicYearID').change(function(){
			if($(this).val() == ''){
				$('#teacherSelection').html('');
				return;
			}

			var teacherID = $('#teacherSelection').find('#TeacherID').val();
			$('#teacherSelection').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
				"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
				{
					'Action': 'Subject_Teacher_Selection',
					'AcademicYearID': $('#FrAcademicYearID').val(),
					'SelectionID': 'TeacherID',
					'SubjectIdArr[]': [<?=$subjectIdList ?>],
					'AddTeacherID[]': '<?=$_SESSION['UserID']?>',
					'TeacherID': teacherID
				},
				function(ReturnData)
				{
				}
			);
			
		});
	<?php } ?>
	$('#FrAcademicYearID').change();
	
	
}); // End $(function(){
</script>

<form id="form1" name="form1" method="POST" action="advancement.php">
	<input type="hidden" name="Action" value="TEACHER_STATS" />
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
				<td valign="top"><?=$FrAcademicYearSelection?></td>
			</tr>
<?php if($accessRight['admin'] || $isSubjectPanel){ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["teacher"]?></span></td>
				<td valign="top">
					<div id="teacherSelection">
						<?=$TeacherSelection?>
					</div>
				</td>
			</tr>
<?php }else{ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["teacher"]?></span></td>
				<td valign="top">
					<span><?=$user->UserName()?></span>
					<input type="hidden" name="TeacherID" value="<?=$_SESSION['UserID']?>" />
				</td>
			</tr>
<?php } ?>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
	<?php
	if($isSubjectPanel){
		foreach($subjectIdArr as $subjectID){
	?>
		<input type="hidden" name="filterSubjectIdArr[]" value="<?=$subjectID ?>" />
	<?php
		}
	}?>
</form>


<script>
$("#resultType_SelectAll").click(function(){
	if($(this).attr("checked")){
		$("#resultType_Form").attr("checked", "checked");
		$("#resultType_Class").attr("checked", "checked");
		$("#resultType_Group").attr("checked", "checked");
	}else{
		$("#resultType_Form").removeAttr("checked");
		$("#resultType_Class").removeAttr("checked");
		$("#resultType_Group").removeAttr("checked");
	}
});
$("#resultType_Form, #resultType_Class, #resultType_Group").click(function(){
	if(!$(this).attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
});
</script>



<form id="searchDetailForm" action="/home/portfolio/profile/management/ajax_get_mark_analysis.php" target="_blank" style="display:none;">
	<input id="searchDetail_FrAcademicYearID" name="FrAcademicYearID" />
	<input id="searchDetail_ToAcademicYearID" name="ToAcademicYearID" />
	<input id="searchDetail_FrSubjectID" name="FrSubjectID" />
	<input name="YearClassID" value="" />
	<input name="filterColumn[]" value="filterMark"/>
	<?php if($sys_custom['iPf']['Report']['AssessmentStatisticReport']['showMarksDifferent']){ ?>
		<input name="filterColumn[]" value="filterMarkDiff"/>
	<?php } ?>
	<input name="filterColumn[]" value="filterStandardScore"/>
	<input name="filterColumn[]" value="filterStandardScoreDiff"/>
	<input name="filterColumn[]" value="filterFormPosition"/>
	<input name="filterColumn[]" value="filterFormPositionDiff"/>
	<input id="SubjectGroupID" name="SubjectGroupID" value=""/>
	<input name="Action" value="STUDENT_PROGRESS"/>
	<input type="submit"/>
</form>
<script>
	/* Old Code * /
	function resetDetailForm(){
		$('#searchDetailForm input[name="addStudent\[\]"]').remove();
	}
	function submitDetailForm(AcademicYearID, SubjectID, StudentIdArr){
		resetDetailForm();
		$('#searchDetail_FrAcademicYearID, #searchDetail_ToAcademicYearID').val(AcademicYearID);
		$('#searchDetail_FrSubjectID').val(SubjectID);
		$.each(StudentIdArr, function(index,element){
			$('#searchDetailForm').append('<input name="addStudent[]" value="' + element + '"/>');
		});
		$('#searchDetailForm').submit();
	}/**/
	
	function submitDetailForm(AcademicYearID, SubjectID, SubjectGroupID){
		$('#searchDetail_FrAcademicYearID, #searchDetail_ToAcademicYearID').val(AcademicYearID);
		$('#searchDetail_FrSubjectID').val(SubjectID);
		$('#SubjectGroupID').val(SubjectGroupID);
		$('#searchDetailForm').submit();
	}
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>