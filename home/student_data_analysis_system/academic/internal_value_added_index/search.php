<?php
######## MODIFICATION LOG ########
# Date : 2020-05-15 Philips [DM3752]
#		- Replace From by Fr to avoid blocked ajax request
##################################
######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########

/*
 $FrYearTerm = "44_98A1";
 $FrTermAssessment = null;
 if (strstr($FrYearTerm, "_"))
 {
 $tmpArr = split("_", $FrYearTerm);
 $FrYearTerm = trim($tmpArr[0]);
 $FrTermAssessment = trim($tmpArr[1]);
 }
 debug($FrYearTerm );
 if ($FrTermAssessment!=null)
 debug($FrTermAssessment );
 */
 ######## Access Right START ########
 $accessRight = $lpf->getAssessmentStatReportAccessRight();
 $lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
 if($accessRight['admin']){
 	$subjectIdArr = '';
 }else{
 	$subjectIdArr = array_keys((array)$accessRight['subjectPanel']);
 }
 ######## Access Right END ########
 
 
 ######## Page Setting START ########
 $CurrentPage = "internal_value_added_index";
 $CurrentPageName = $Lang['iPortfolio']['InternalValueAdded'];
 $TAGS_OBJ[] = array($CurrentPageName,"");
 $MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
 ######## Page Setting END ########
 
 
 ######## UI Releated START ########
 $allSemesterArr = $li_pf->returnAllAssessmentSemester();
 $allSemesterArr = array_reverse($allSemesterArr);
 if(count($allSemesterArr) == 0){
 	$compareHTML = "<i>".$no_record_msg."</i>";
 }else{
 	$FrYearTermSelect = '<select name="FrYearTerm" id="FrYearTerm">';
 	$ToYearTermSelect = '<select name="ToYearTerm" id="ToYearTerm">';
 	foreach($allSemesterArr as $sem){
 		$FrYearTermSelect .= '<option value="'.$sem['YearTermID'].'">'.$sem['Year'].' '.$sem['Semester'].'</option>';
 		$ToYearTermSelect .= '<option value="'.$sem['YearTermID'].'">'.$sem['Year'].' '.$sem['Semester'].'</option>';
 	}
 	$FrYearTermSelect .= '</select>';
 	$ToYearTermSelect .= '</select>';
 	
 	$compareHTML = '<table border="0"><tr><td>'.$FrYearTermSelect.'</td><td> VS </td><td>'.$ToYearTermSelect.'</td></tr></table>';
 }
 
 $FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, '', $noFirst=1, $isAll=0, $isMultiple=0);
 
 $FrSubjectSelection = $libSCM_ui->Get_Subject_Selection('FrSubjectID', $FrSubjectID, $OnChange='', $noFirst=1, '', '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdArr);
 
 
 ### YearSelection Box START ###
 $YearArr = $li_pf->returnAssessmentYear();
 $FrAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FrAcademicYearID' id='FrAcademicYearID' onchange=\"js_Changed_AcademicYear_Selection(this.value, 'Fr')\"", $FrAcademicYearID, 0, 1, "", 2);
 $ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"js_Changed_AcademicYear_Selection(this.value, 'To')\"", $ToAcademicYearID, 0, 1, "", 2);
 ### YearSelection Box END ###
 
 
 ######## UI Releated END ########
 
 
 ######## UI START ########
 $linterface->LAYOUT_START();
 ?>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTerm = '<?=$FrYearTerm?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTerm = '<?=$ToYearTerm?>';
var jsClearCoo = '<?=$clearCoo?>';

//var FirstTitle = '<?=$Lang['General']['WholeYear']?>'

$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	//js_Reload_Term_Selection('Fr');
	
	js_Reload_Term_Selection('Fr');
	js_Reload_Term_Selection('To');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	//js_Reload_Term_Selection('To', jsRefreshDBTable);
});


/* show final result option
function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTerm = jsCur' + jsType + 'YearTerm;');
	eval('var jsSelectionID = "' + jsType + 'YearTerm";');
	var tag = "name="+jsType+"YearTerm id="+jsType+"YearTerm";
	
	 $('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/student_data_analysis_system/?t=ajax.ajax_get_year_term_id_selection", 
		{
			AcademicYearID: jsAcademicYearID,
			tag: tag,
			ComplexValue : true
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTerm = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
	
}*/



function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTerm;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTerm, jsType)
{
	eval('jsCur' + jsType + 'YearTerm = jsYearTerm;');
}


function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTerm = jsCur' + jsType + 'YearTerm;');
	eval('var jsSelectionID = "' + jsType + 'YearTerm";');

	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Assessment_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTerm: jsYearTerm,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 1,
			DisplayAll: 1,
			FirstTitle: ''
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTerm = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
	if (typeof(document.form1.FrYearTerm)=="undefined" || typeof(document.form1.ToYearTerm)=="undefined")
	{
		alert("<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term']?>");
		return;
	}
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		{
			Action: 'INTERNAL_VALUE_ADDED_STAT',
			FrAcademicYearID: $('select#FrAcademicYearID').val(),
			FrYearTerm: $('select#FrYearTerm').val(),
			ToAcademicYearID: $('select#ToAcademicYearID').val(),
			ToYearTerm: $('select#ToYearTerm').val(),
			YearID: $('select#YearID').val(),
			FrSubjectID: $('select#FrSubjectID').val()
		},
		function(ReturnData)
		{
			
		}
	);
}
</script>

<form id="form1" name="form1" method="POST">
	<?=$html_tab_menu ?>
					<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
						<tr>
							<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
							<td valign="top"><!-- <?=$ToYearTermSelect?> &nbsp; --> <?=$FormSelection?></td>
						</tr>
						<!-- 
						<tr>
							<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["compare"]?></span></td>
							<td valign="top"><?=$FrYearTermSelect?></td>
						</tr>
						-->
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td valign="top"><?=$FrSubjectSelection?></td>
						</tr>
						
						
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$iPort["compareScore"]?></span></td>
							<td valign="top">
								<table border="0" cellspacing="0" cellpadding="2" >
								<tr>
									<td valign="top">
										<div style="float:left;"><?=$ToAcademicYearSelection?></div>
										<div id="ToYearTermSelectionDiv" style="float:left;"></div>
									</td>
								</tr>
								<tr>							
									<td valign="top">
										V.S.
									</td>
								</tr>
								<tr>							
									<td valign="top">
										<div style="float:left;"><?=$FrAcademicYearSelection?></div>
										<div id="FrYearTermSelectionDiv" style="float:left;"></div>
									</td>
								</tr>
							</table>
					</td>
						</tr>
						
					</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="DBTableDiv"><?=$h_DBTable?></div>
	
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>