<?php 
//Using: Isaac
/*
 * Change Log:
 * 2019-01-14 Isaac
 * Added export table function
 *
 */
// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam ();
$libSDAS = new libSDAS ();
$lpf = new libPortfolio ();
$libFCM_ui = new form_class_manage_ui ();
$libSCM_ui = new subject_class_mapping_ui ();
// ####### Init END ########

// ####### Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight ();
$subjectIdFilterArr = '';
if (! $accessRight ['admin'] && count ( $accessRight ['subjectPanel'] )) {
	$subjectIdFilterArr = array_keys ( $accessRight ['subjectPanel'] );
}
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "dse_stats";
$CurrentPageName = $Lang ['SDAS'] ['menu'] ['DseAnalysis'];
$CurrentTag = 'dse_distribution';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR ();
// ####### Page Setting END ########

// ####### UI Releated START ########
 $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
// year info

$aYearAssocArr = BuildMultiKeyAssoc ( GetAllAcademicYearInfo (), 'AcademicYearID', array (
		Get_Lang_Selection (  'YearNameB5', 'YearNameEN') 
), 1 );
$yearArr = $libpf_exam->getDseYearWithRecords ();

$yearSelectArr = array ();
foreach ( ( array ) $yearArr as $yearID ) {
	$yearSelectArr [$yearID] = $aYearAssocArr [$yearID];
}
$FromAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );
$ToAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='ToAcademicYearID' id='ToAcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );
// $FromAcademicYearSelection = getSelectByArray($YearArr, "name='FromAcademicYearID[]' id='FromAcademicYearID' multiple", $classSelect, 0, 1, "", 2);
// $FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='', $noFirst=0, $noPastYear=0);
// $ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='', $noFirst=0, $noPastYear=0);
// $FromSubjectSelection = $libSCM_ui->Get_Subject_Selection ( 'FromSubjectID', $FromSubjectID, $OnChange = '', $noFirst = 1, '', '', $OnFocus = '', $FilterSubjectWithoutSG = 0, $IsMultiple = 0, $IncludeSubjectIDArr = $subjectIdFilterArr );

$targetFormSelection = getSelectByAssoArray ( $Lang ['SDAS'] ['DSEstat'] ['form'], "name='targetForm' id='targetForm'", '6', 0, 1 );

$class_arr = $libpf_exam->getClassListByYearName ( 6 );

$classCheckBox = '';
foreach ( ( array ) $class_arr as $_class_info ) {
	// $_value = $_class_info['YearClassID'];
	$_value = $_class_info ['ClassTitleEN'];
	$_displayLang = Get_Lang_Selection ( $_class_info ['ClassTitleB5'], $_class_info ['ClassTitleEN'] );
	$_engDisplay = $_class_info ['ClassTitleEN'];
	$classCheckBox .= '<input type="checkbox" class="ClassSel" id="class_' . $_engDisplay . '" name="Class[]" value="' . $_value . '" checked/><label for="class_' . $_engDisplay . '">' . $_displayLang . '</label>&nbsp;';
}

$displayBy = '';
$displayBy .= "<input type='checkbox' id='Display_Q' name='Display[]' value='Q' checked>";
$displayBy .= "<label for='Display_Q'><span></span>".$Lang['SDAS']['ColumnNumber']."</label>";
$displayBy .= '&nbsp;';
$displayBy .= "<input type='checkbox' id='Display_P' name='Display[]' value='P' checked>";
$displayBy .= "<label for='Display_P'><span></span>".$Lang['SDAS']['ColumnPercent']."</label>";

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// ####### UI Releated END ########

// ####### UI START ########
$linterface->LAYOUT_START ();
echo $linterface->Include_TableExport();
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script language="JavaScript" src="/templates/d3/d3_v3_5_16.min.js"></script>
<?php echo $linterface->Include_HighChart_JS()?>

<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function() {

	$( "#Btn_View" ).click(function() {
		$( "#PrintBtn" ).hide();
		$("#ExportBtn").hide();

		if( $('select#FromAcademicYearID').val() == '' || $('select#TomAcademicYearID').val() == ''  ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}

		if($('input[name="Display[]"]:checked').length == 0){
			$('input[name="Display[]"]').each(function(){
				$(this).attr('checked', true);
			});
		}	
		$("#DBTableDiv").html(ajaxImage);
		var url = "index.php?t=academic.dse_stats.ajax_task";
		$.ajax({
		    type:"POST",
		    url: url,
		    data: $("#form1").serialize(),
		    success:function(responseText)
		    {
		    	$("#DBTableDiv").html(responseText);
		    	$( "#PrintBtn" ).show();
		    	$("#ExportBtn").show();
		    }
		});  
	});

	
});

function exportCSV() {
	// for csv display multiple headers
	$("tr.display_table_header").hide();
	$("tr.export_table_header").show();
	
	$(".chart_tables table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'DSE Distribution'
	});

	$("tr.display_table_header").show();
	$("tr.export_table_header").hide();
}
</script>
<form id="form1" name="form1">
	<?=$html_tab_menu?>
					<table border="0" cellspacing="0" cellpadding="5"
		class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$Lang['General']['From'].' '.$FromAcademicYearSelection.' '.$Lang['General']['To'].' '.$ToAcademicYearSelection?></td>
		</tr>
		<!--<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$iPort['class']?></span></td>
			<td valign="top"><input type="checkbox" id="resultType_SelectAll" 
			onclick="(this.checked)?setChecked(1,this.form,'Class[]'):setChecked(0,this.form,'Class[]')" checked/>
				<label for="resultType_SelectAll"><?=$Lang['Btn']['SelectAll']?></label> &nbsp;&nbsp;&nbsp;
								<?=$classCheckBox?>
							</td>
		</tr> -->
		<tr>
			<td class="field_title"><?php echo $Lang['SDAS']['DisplayBy']?></td>
			<td><?php echo $displayBy?></td>
		</tr>
	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30">
		<input type="hidden" id="task" name="task" value="distribution" />  
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>"
			class='formbutton' onmouseover="this.className='formbuttonon'"
			onmouseout="this.className='formbutton'" />
	</div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	

	<div id="PrintArea">
		<div id="DBTableDiv">
		</div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP ();
intranet_closedb ();
?>