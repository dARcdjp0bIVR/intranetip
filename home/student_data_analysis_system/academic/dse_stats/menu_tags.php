<?php


$TAGS_OBJ[] = array($Lang['SDAS']['DseVsMock']['Title'], "index.php?t=academic.dse_stats.search", $CurrentTag == 'dse_vs_mock');
$TAGS_OBJ[] = array($Lang['SDAS']['DsePerformanceTrend']['Title'], "index.php?t=academic.dse_stats.trend", $CurrentTag == 'trend');
$TAGS_OBJ[] = array($Lang['SDAS']['DseDistribution']['Title'], "index.php?t=academic.dse_stats.dse_distribution", $CurrentTag == 'dse_distribution');
$TAGS_OBJ[] = array($Lang['SDAS']['DseDistributionSubjectGroup']['Title'], "index.php?t=academic.dse_stats.dse_distribution_subject_group", $CurrentTag == 'dse_distribution_subject_group');
$TAGS_OBJ[] = array($Lang['SDAS']['DseAttendance']['Title'], "index.php?t=academic.dse_stats.attendance", $CurrentTag == 'attendance');
$TAGS_OBJ[] = array($Lang['SDAS']['DSEBestFive']['Title'], "index.php?t=academic.dse_stats.best_five_score", $CurrentTag == 'best_five_score');
$TAGS_OBJ[] = array($Lang['SDAS']['DSEUniversity']['Title'], "index.php?t=academic.dse_stats.university", $CurrentTag == 'university');
$TAGS_OBJ[] = array($Lang['SDAS']['DseBestStudent']['Title'], "index.php?t=academic.dse_stats.best_student", $CurrentTag == 'best_student');
if($sys_custom['SDAS']['dse_vs_gpa']){
    $TAGS_OBJ[] = array($Lang['SDAS']['DseVsGpa']['Title'], "index.php?t=academic.dse_stats.dse_vs_gpa", $CurrentTag == 'dse_vs_gpa');
}