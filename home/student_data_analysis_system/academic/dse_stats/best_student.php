<?php 
/*
 * Change Log:
 * 2019-01-11 Isaac
 * Added export table function
 * 
 * Date:	2017-07-11 Villa
 * 			-add thichkbox js related
 */
// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam ();
$libSDAS = new libSDAS ();
$lpf = new libPortfolio ();
$libFCM_ui = new form_class_manage_ui ();
$libSCM_ui = new subject_class_mapping_ui ();
// ####### Init END ########

// ####### Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight ();
$subjectIdFilterArr = '';
if (! $accessRight ['admin'] && count ( $accessRight ['subjectPanel'] )) {
	$subjectIdFilterArr = array_keys ( $accessRight ['subjectPanel'] );
}
// ####### Access Right END ########

// ####### Page Setting START ########
$CurrentPage = "dse_stats";
$CurrentPageName = $Lang ['SDAS'] ['menu'] ['DseAnalysis'];
$CurrentTag = 'best_student';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR ();
// ####### Page Setting END ########

// ####### UI Releated START ########
 $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
// year info

$aYearAssocArr = BuildMultiKeyAssoc ( GetAllAcademicYearInfo (), 'AcademicYearID', array (
		Get_Lang_Selection (  'YearNameB5', 'YearNameEN') 
), 1 );
$yearArr = $libpf_exam->getDseYearWithRecords ();

$yearSelectArr = array ();
foreach ( ( array ) $yearArr as $yearID ) {
	$yearSelectArr [$yearID] = $aYearAssocArr [$yearID];
}
$FromAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", $CurrentAcademicYearID, 0, 0, "" );

// table 1 selection
$topStudentInput = $linterface->Get_Number_Selection('topStudentConfig', 1, 36, 36,'', 1, 0, '', 0);
$table1Setting = str_replace('<!--score-->', $topStudentInput, $Lang['SDAS']['DSETopStudent']['Top5']['Title'][0]);

// table 2 selection
$subjectInput = $linterface->Get_Number_Selection('subjectNumConfig', 1, 8, 4,'', 1, 0, '', 0);
$scoreInput = $linterface->Get_Number_Selection('scoreConfig', 1, 36, 24,'', 1, 0, '', 0);
$table2Setting = str_replace(array('<!--subject-->','<!--score-->'), array($subjectInput, $scoreInput),  $Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0]);

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// ####### UI Releated END ########

// ####### UI START ########
$linterface->LAYOUT_START ();
echo $linterface->Include_TableExport();
?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script language="JavaScript" src="/templates/d3/d3_v3_5_16.min.js"></script>
<?php echo $linterface->Include_HighChart_JS()?>
<?php echo $linterface->Include_Thickbox_JS_CSS()?>

<script>
var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready(function() {

	$( "#Btn_View" ).click(function() {
		$( "#PrintBtn" ).hide();
		$("#ExportBtn").hide();
		
		if( $('select#FromAcademicYearID').val() == '' || $('select#TomAcademicYearID').val() == ''  ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}
		
		$("#DBTableDiv").html(ajaxImage);
		var url = "index.php?t=academic.dse_stats.ajax_task";
		$.ajax({
		    type:"POST",
		    url: url,
		    data: $("#form1").serialize(),
		    success:function(responseText)
		    {
		    	$("#DBTableDiv").html(responseText);
		    	$("#PrintBtn").show();
		    	$("#ExportBtn").show();
		    	formJS.init();
		    }
		});  
	});
	formJS.init();
		
});

function exportCSV() {

	var tableTitle = [];
	$("#DBTableDiv span.sectiontitle_v30").each(function() {
		tableTitle.push($( this ).text());
	});
	
	$(".chart_tables table").each(function(index, element) {
			$( this ).tableExport({
    		type: 'csv',
    		headings: true,
    		fileName: 'Best Student '+tableTitle[index]
    	});
	});

}

var formJS = {
	vrs :{
		thickboxBtn : 'a.thickbox',
		studentRsBtn : 'a.studentRs'
	},
	ftn :{
		onLoadThickBox: function(){
			load_dyn_size_thickbox_ip('', '',inlineID='', defaultHeight=500, defaultWidth=600);
		},
		printStnRs: function(){
			var this_obj = $(this);
			var studentID = this_obj.attr('data-studentID');
			var academicYearID = this_obj.attr('data-academicYear');
			var title = this_obj.attr('data-title');
			var url = "index.php?t=academic.dse_stats.ajax_task";
			$.ajax({
				async: false,
			    type:"POST",
			    url: url,
			    data: {
				    'task': 'studentExamDetail',
				    'academicYearID' : academicYearID,
				    'studentID' : studentID
				},
			    success:function(responseText)
			    {
			    	$("#TB_ajaxContent").html(responseText);
// 			    	adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv','thickboxContainerDiv','thickboxContainerDiv');
			    }
			});  
			$("#TB_ajaxWindowTitle").html(title);
		}
	},
	bindListener: function(){
		$(formJS.vrs.thickboxBtn).on('click',formJS.ftn.onLoadThickBox);
		$(formJS.vrs.studentRsBtn).on('click',formJS.ftn.printStnRs);
	},
	destroyListener: function(){
		$(formJS.vrs.thickboxBtn).off('click',formJS.ftn.onLoadThickBox);
		$(formJS.vrs.studentRsBtn).off('click',formJS.ftn.printStnRs);
	},
	init : function(){
		formJS.destroyListener();
		formJS.bindListener();
	}	
};


</script>
<form id="form1" name="form1">
	<?=$html_tab_menu?>
					<table border="0" cellspacing="0" cellpadding="5"
		class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$FromAcademicYearSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?php echo $Lang['SDAS']['DSETopStudent']['Table'].' 1';?></span></td>
			<td valign="top">
			<?php echo $table1Setting?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletext"><?php echo $Lang['SDAS']['DSETopStudent']['Table'].' 2';?></span></td>
			<td valign="top">
			<?php echo $table2Setting?>
			</td>
		</tr>
	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30">
		<input type="hidden" id="task" name="task" value="bestStudent" />  
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>"
			class='formbutton' onmouseover="this.className='formbuttonon'"
			onmouseout="this.className='formbutton'" />
		
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>	
	</div>
	<br clear=“both”/>
	
	<div id="PrintArea">
		<div id="DBTableDiv"></div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP ();
intranet_closedb ();
?>