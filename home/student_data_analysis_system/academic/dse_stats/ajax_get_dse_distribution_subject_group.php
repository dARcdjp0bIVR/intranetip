<?php 
/* // using 
 * Change Log:
 * 2018-07-10 Anna
 *  - Fixed  #E142385   
 * 
 * 2017-12-12 Pun
 *  - File Create
 */
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
$libpf_exam = new libpf_exam();
$lpf = new libportfolio();
$ObjAcademicYear = new academic_year($AcademicYearID);
$libSCM = new subject_class_mapping();


#### Access Right START ####
$subjectIdFilterArr = '';
if($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']){
    $accessRight = $objSDAS->getAssessmentStatReportAccessRight();
    $subjectIdFilterArr = '';
    if (! $accessRight ['admin'] && count ( $accessRight ['subjectPanel'] )) {
        $subjectIdFilterArr = array_keys ( $accessRight ['subjectPanel'] );
    }
}
#### Access Right END ####


#### Get First Year Term START ####
$YearTermInfoArr = $ObjAcademicYear->Get_Term_List($returnAsso=0);
$YearTermID = $YearTermInfoArr[0]['YearTermID'];
#### Get First Year Term END ####


#### Get DSE Data START ####
$examDataArr = $libpf_exam->getExamData($AcademicYearID, EXAMID_HKDSE, $subjectIdFilterArr, '',$excludeCmpSubject=true, true);
//$numYear = count(array_unique(Get_Array_By_Key($examDataArr, 'AcademicYearID')));
$subjectIdArr = array_values(array_unique(Get_Array_By_Key($examDataArr, 'SubjectID')));
#### Get DSE Data END ####


#### Get Subject START ####
$subjectInfoArr = $libpf_exam->getSubjectCodeAndID($subjectIdArr, array(), true, false);
$subjectArr = BuildMultiKeyAssoc($subjectInfoArr, 'SubjectID', array(Get_Lang_Selection('ch_main_name', 'main_name')),1);
#### Get Subject END ####


#### Get YearID START ####
$objYear = new Year();
$yearList = $objYear->Get_All_Year_List();
$form6YearIdArr = array();
foreach($yearList as $year){
    if($year['WEBSAMSCode'] == 'S6'){
        $form6YearIdArr[] = $year['YearID'];
    }
}
#### Get YearID END ####


#### Get SubjectGroup Year Releateion START ####
$sql = "SELECT * FROM SUBJECT_TERM_CLASS_YEAR_RELATION";
$rs = $libSCM->returnResultSet($sql);

$subjectGroupYearMapping = array();
foreach($rs as $r){
    $subjectGroupYearMapping[$r['SubjectGroupID']][] = $r['YearID'];
}
#### Get SubjectGroup Year Releateion END ####


#### Get Subject Groups START ####
$rs = $libSCM->Get_Subject_Group_List($YearTermID);
$rs = $rs['SubjectGroupList'];

$subjectGroupIdArr = array();
$subjectGroupMapping = array();

foreach((array)$rs as $r){
    $subjectGroupID = $r['SubjectGroupID'];
    $yearIdArr = $subjectGroupYearMapping[$subjectGroupID];

    $isForm6 = false;
    foreach((array)$yearIdArr as $yearId){
        if(in_array($yearId, $form6YearIdArr)){
            $isForm6 = true;
            break;
        }
    }
    if(!$isForm6){
        continue;
    }
    
    $subjectGroupIdArr[] = $subjectGroupID;
    $subjectGroupMapping[$subjectGroupID] = $r;
}

$subjectIdGroupMapping = array();
foreach($subjectGroupMapping as $r){
    $subjectIdGroupMapping[$r['SubjectID']][] = $r;
}

$rs = $libSCM->Get_Subject_Group_Student_List($subjectGroupIdArr);
$subjectGroupStudentMapping = array();
foreach($rs as $r){
    $subjectGroupStudentIdMapping[$r['SubjectGroupID']][] = $r['UserID'];
}
#### Get Subject Groups END ####


#### Calculate stat START ####
$dseLevelArr = array_reverse($libpf_exam->getDseLevelSequenceArr());

$data = array();

## Count Student START ##
foreach($examDataArr as $examData){
    $SubjectID = $examData['SubjectID'];
    $StudentID = $examData['StudentID'];
    $Score = $examData['Score'];
    
    foreach((array)$subjectIdGroupMapping[$SubjectID] as $subjectGroup){
        $subjectGroupID = $subjectGroup['SubjectGroupID'];
        $subjectGroupStudentIds = $subjectGroupStudentIdMapping[$subjectGroupID];

        if(in_array($StudentID, (array)$subjectGroupStudentIds)){
            if(!isset($data[$SubjectID][$subjectGroupID])){
                foreach($dseLevelArr as $dseLevel){
                    $data[$SubjectID][$subjectGroupID][$dseLevel]['count'] = 0;
                }
                $data[$SubjectID][$subjectGroupID]['X']['count'] = 0;
            }
            
            switch($Score){
                case '5**':
                    $data[$SubjectID][$subjectGroupID]['5**']['count']++;
                case '5*':
                    $data[$SubjectID][$subjectGroupID]['5*']['count']++;
                case '5':
                    $data[$SubjectID][$subjectGroupID]['5']['count']++;
                case '4':
                    $data[$SubjectID][$subjectGroupID]['4']['count']++;
                case '3':
                    $data[$SubjectID][$subjectGroupID]['3']['count']++;
                case '2':
                    $data[$SubjectID][$subjectGroupID]['2']['count']++;
                case '1':
                    $data[$SubjectID][$subjectGroupID]['1']['count']++;
                    break;
                case 'U':
                    $data[$SubjectID][$subjectGroupID]['U']['count']++;
                    break;
                case 'X':
                    $data[$SubjectID][$subjectGroupID]['X']['count']++;
                    break;
            }
            
            
            break;
        }
    }
}
## Count Student END ##

## Count Percent START ##
foreach($data as $SubjectID=>$d1){
    foreach($d1 as $subjectGroupID=>$d2){
        $count1Above = $data[$SubjectID][$subjectGroupID]['1']['count'];
        $countU = $data[$SubjectID][$subjectGroupID]['U']['count'];
        
        $countAllSubjectGroupStudent = $count1Above + $countU;
        $data[$SubjectID][$subjectGroupID]['_Overall_']['count'] = $countAllSubjectGroupStudent;
        
        foreach($d2 as $Score=>$_count){
            $count = $_count['count'];
            $data[$SubjectID][$subjectGroupID][$Score]['percent'] = ($count*100/$countAllSubjectGroupStudent);
        }
    }
}
## Count Percent END ##

#### Calculate stat END ####

// debug_rt($examDataArr);
// debug_rt($subjectGroupArr);
// debug_rt($rs);
// debug_r($subjectGroupIdMapping);
// debug_r($subjectGroupStudentMapping);
// debug_r($data);


#### UI START ####
if(in_array('Q',$Display))
    $displayByQ = true;
if(in_array('P',$Display))
    $displayByP = true;
$displayColNum = count($Display);

?>



<div class="chart_tables">
<table id="resultTable" class="common_table_list_v30 view_table_list_v30">
    <thead>
        <tr>
            <th rowspan="2"><?=$Lang['Header']['Menu']['Subject']?></th>
            <th rowspan="2"><?=$Lang['SDAS']['SubjectGroup']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['5**']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['5*'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['5'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['4'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['3'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['2'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['1'].$Lang['SDAS']['DSEstat']['orAbove']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['SDAS']['DSEstat']['DSELevel']['U']?></th>
            <th colspan="<?=$displayColNum?>"><?=$Lang['General']['Total']?></th>
            <th colspan="1"><?=$Lang['SDAS']['DSEstat']['DSELevel']['X']?></th>
        </tr>
    	<tr>
    		<?php
            for($i = 0; $i < 10; $i++){
                if($i == 9){
                    echo "<th>{$Lang['SDAS']['ColumnNumberShort']}</th>";
                }else{
                    if($displayByQ)
                        echo "<th>{$Lang['SDAS']['ColumnNumberShort']}</th>";
                    if($displayByP)
                        echo "<th>{$Lang['SDAS']['ColumnPercentShort']}</th>";
                }
            }
            ?>
    	</tr>
    </thead>
    <tbody>
    	<?php
    	foreach((array)$subjectArr as $subjectID => $subjectName){
    	    $groupCount = count($data[$subjectID]);

    	    $index = 0;
    	    foreach((array)$subjectIdGroupMapping[$subjectID] as $subjectGroup){
    	        $subjectGroupID = $subjectGroup['SubjectGroupID'];
    	        $scoreInfo = $data[$subjectID][$subjectGroupID];
    	        if(!$scoreInfo){
    	            continue;
    	        }
    	        
    		    $subjectGroup = $subjectGroupMapping[$subjectGroupID];
    		    $subjectGroupName = Get_Lang_Selection($subjectGroup['ClassTitleB5'], $subjectGroup['ClassTitleEN']);
    		    
    		?>
            	<tr>
            		<?php if($index++ == 0){ ?>
            			<td rowspan="<?=$groupCount ?>"><?=$subjectName ?></td>
            		<?php } ?>
            		
            		<td style="font-weight:normal;"><?=$subjectGroupName ?></td>
            		
            		<?php 
            		foreach($dseLevelArr as $dseLevel){ 
            		    $resultInfo = $data[$subjectID][$subjectGroupID][$dseLevel];
            		    $countStudent = $resultInfo['count'];
            		    $countPercent = $resultInfo['percent'];
            		?>
            			<?php if($displayByQ){ ?>
            				<td><?=(int)$countStudent ?></td>
            			<?php } ?>
            			<?php if($displayByP){ ?>
                			<td><?=number_format($countPercent,1) ?></td>
            			<?php } ?>
            		<?php 
            		} 
            		?>
            		
        			<?php if($displayByQ){ ?>
        				<td><?=(int)$data[$subjectID][$subjectGroupID]['_Overall_']['count'] ?></td>
        			<?php } ?>
        			<?php if($displayByP){ ?>
            			<td>100</td>
        			<?php } ?>
        			
        			<td><?=(int)$data[$subjectID][$subjectGroupID]['X']['count'] ?></td>
            		
            	</tr>
        	<?php
        	}
    	}
    	?>
    </tbody>
</table>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

$(function() {$('#resultTable').floatHeader();});
</script>
