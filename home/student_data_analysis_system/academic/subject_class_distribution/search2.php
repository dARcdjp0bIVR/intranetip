<?php
// using : 
/**
 * Change Log:
 * 2020-10-23 (Philips) [2020-1020-1431-48206]
 * 	-   modified ajaxLoadSubject(), use empty Academic Year ID
 * 2020-09-15 (Philips) [DM#3909]
 *  -	Use YearTerm Instead of YearTermID
 * 2020-09-11 (Philips) [DM#3790]
 *  -	Modified ajaxLoadTerm(), copy from search.php get_yearterm_opt()
 * 2020-08-05 (Philips) [2020-0702-1146-07206]
 *  -	Modified ajaxLoadSubject(), fixed admin right overwrite by other access right
 * 2020-05-05 (Philips) [2019-0710-1725-23085]
 *  -   Added distribution chart
 * 2020-01-08 Philips [DM#3714]
 * loadTerm - overall_result, loadSubject - overall_score
 * 2020-01-08 Philips [DM#3713]
 * Prevent foreach error of non-array object
 * 2019-12-10 Philips [2019-1204-1054-57206]
 * Modified ajaxLoadSubject(), js_Reload_Class_Selection() to enable class teacher and admin to choose overall subject result
 * 2019-01-14 Isaac
 * Added export table function
 *  
 * 2017-12-12 Pun
 *  -   Added Overall option to subject selection
 * 2017-08-14 Villa
 * 	-	Modified the call ajax method using index?t=??? instead of calling direct link
 * 2016-12-14 Villa
 * Add UI selection - base on 100 
 * add yearterm
 * 2016-12-09 Villa
 *  - 	modified js_Reload_Class_Selection()
 * 2016-12-06 Villa
 * 	-	Update the UI
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 Anna
 * -    Default timeperiod as current year and deisplay mark
 * 2016-02-19 Pun
 * 	-	Hide year if it does not have any classes
 * 2016-02-18 Pun
 * 	-	Added "Additional Information" filter
 * 2016-02-17 Pun
 * 	-	Modified js_Reload_DBTable(), added checking for start year smaller then end year
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 * 	-	Added display "Age", "Primary School", "Percentile"
 */
include_once($PATH_WRT_ROOT . 'includes/libclass.php');
include_once($PATH_WRT_ROOT . 'includes/form_class_manage.php');
######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$lclass = new libclass();
######## Init END ########

######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
		in_array('subject_class_distribution',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}
$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);
$yearClassArray = array();
$subjectIDAry = array();
$yearIDAry = array();
$subjectClassAry = array();
$yearAry = array();
$classSelectionHTML = '<select name="YearClassID">';
foreach((array)$accessRight['classTeacher'] as $yearClass){
    $yearClassArray[$yearClass['YearClassID']] = array("ClassTitleEN" => $yearClass["ClassTitleEN"],"ClassTitleB5" => $yearClass["ClassTitleB5"]);
    $chk = true;
    foreach($yearAry as $yy){
        if($yy[0] == $yearClass['YearID'])
            $chk = false;
    }
    if($chk){
        $yearName = $lclass->getLevelName($yearClass['YearID']);
        $yearAry[] = array($yearClass['YearID'], $yearName);
    }
// 	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
// 	$classSelectionHTML .= "<option value=\"{$yearClass['YearClassID']}\">{$name}</option>";
}
foreach((array)$accessRight['subjectGroup'] as $subjectGroup){
    $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
    $subjectIDAry[] = $sGroup->SubjectID;
    $studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true, $showYearName = true);
    $classList = BuildMultiKeyAssoc($studentList, array("YearClassID"), array("ClassTitleB5","ClassTitleEN","YearName","YearID"));
    if(!$subjectClassAry[$sGroup->SubjectID]){
        $subjectClassAry[$sGroup->SubjectID] = array();
    }
    foreach($classList as $classID => $classDetail){
        if(!in_array($classID,$subjectClassAry[$sGroup->SubjectID])){
            $subjectClassAry[$sGroup->SubjectID][] = $classID;
        }
        $yearIDAry[$classDetail['YearID']] = $classDetail['YearName'];
        $yearClassArray[$classID] = array("ClassTitleEN" => $classDetail["ClassTitleEN"],"ClassTitleB5" => $classDetail["ClassTitleB5"]);
    }
}
foreach($yearClassArray as $yearClassID => $yearClass){
    $name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
    $classSelectionHTML .= "<option value=\"{$yearClassID}\">{$name}</option>";
    $classTeacherClassIdArr[] = $yearClassID;
}
$subjectIDAry = array_unique($subjectIDAry);
# added Subject Panel Into year selection
$subjectYearIDAry = array();
foreach((array)$accessRight['subjectPanel'] as $subjectID => $accessForm){
	$subjectYearIDAry = array_merge($subjectYearIDAry, array_keys($accessForm));
	foreach($accessForm as $yid => $yc){
		$year = new Year($yid);
		$yearIDAry[$yid] = $year->Get_Year_Name();
	}
}
foreach($yearIDAry as $yearID => $yearName){
    $chk = true;
    foreach($yearAry as $yy){
        if($yy[0] == $yearID)
            $chk = false;
    }
    if($chk == true){
        $yearAry[] = array($yearID, $yearName);
    }
}
$classSelectionHTML .= '</select>';

$isSubjectPanelView = false;
if(!$accessRight['admin'] 
  && in_array('class_marksheets', (array)$plugin['SDAS_module']['accessRight']['subjectPanel']) 
  && count($accessRight['subjectPanel'])){
			$isSubjectPanelView = true;
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "subject_class_distribution";
$CurrentPageName = $Lang['SDAS']['ClassPerformance']['CrossYear'];
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['IndividualYear'],'index.php?t=academic.subject_class_distribution.search');
$TAGS_OBJ[] = array($CurrentPageName,"javascript: void(0);", true);
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossTerm'],"index.php?t=academic.subject_class_distribution.search3");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
#### Get Year selection START ####
// $FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
$objYear = new Year();
// $li_pf = new libpf_asr();

# get Class Level selection
$allClassArr = $li_pf->getActivatedForm($currentAcademicYearID, false);
$allYearArr = BuildMultiKeyAssoc($allClassArr, array('YearID') , array('YearName'), $SingleValue=1);
if($accessRight['admin']){
    $FormSelection = getSelectByAssoArray($allYearArr,' id="YearID" name="YearID" onchange="js_Changed_Form_Selection(this.value);"', $selected=$YearID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
} else {
    $FormSelection = getSelectByArray($yearAry,' id="YearID" name="YearID" onchange="js_Changed_Form_Selection(this.value);"', $selected=$YearID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
}
#### Get Year selection END ####


$YearArr = $li_pf->returnAssessmentYear();
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FromAcademicYearID' id='FromAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 1, "", 2);

$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 1, "", 2);

if($accessRight['admin']){
// 	$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=1, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
	$FromSubjectSelection = $libSDAS->Get_Subject_Selection($SubjectIdArr='', $firstTitle=$ec_iPortfolio['overall_result']);
	$isSubjectPanel = 0;
}else if($isSubjectPanelView){
	// Subject Panel load the subject <select> by ajax.
	// Show all subject if he is the class teacher, otherwise show his subject.
	$FromSubjectSelection = '';
	$isSubjectPanel = 1;

	$subjectArr = array_keys($accessRight['subjectPanel']);
	$subjectList = "'".implode("','",$subjectArr)."'";
	$selfClassArr = Get_Array_By_Key($accessRight['classTeacher'], 'YearClassID');
	$selfClassList = "'".implode("','",$selfClassArr)."'";
} else {
    $FromSubjectSelection = $libSDAS->Get_Subject_Selection($subjectIDAry);
    $isSubjectPanel = 0;
}
//Print Button
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);


######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
echo $linterface->Include_TableExport();
?>
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {
	
	var tableTitle = ['Class Comparison', 'Pass Percentage']

	$(".chart_tables table").each(function(index, element) {
		$( this ).tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Class Subject Performance Distribution (Cross-Year)'+tableTitle[index]
	});
});
	
}
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	if($('input[name="chartType"]:checked').val() == 'column'){
		$('#option').hide();
	}
	
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	 $("#filterMark").attr("checked", "checked");
	
});

function js_Changed_Form_Selection(jsYearID)
{
	<?php if($isSubjectPanelView){ ?>
		$('div#subjectSelection').html('');
	<?php } ?>
	
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/student_data_analysis_system/ajax/ajax_get_class_selection_box.php", 
		{
// 			Action: 'Class_Selection',
			AcademicYearID: '<?=$currentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: 'ajaxLoadSubject(this.value)', // Only subject panel will load subject <select> by ajax
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
			var classIdAry = [<?php echo "'" . implode("','", (array)$classTeacherClassIdArr) . "'";?>];
			var subjectYearIdAry = [<? if($accessRight['subjectPanel']){
				echo "'" . implode("','", $subjectYearIDAry) . "'";
			}?>];
			<?php if(!$accessRight['admin']){?>
				if(subjectYearIdAry.indexOf($('select#YearID').val()) == -1){
					$('#YearClassID option').filter(function(index){
						return classIdAry.indexOf($(this).val()) == -1;
					}).remove();
				}
			<?php }?>
			//(<?=$isSubjectPanel ?>)? ajaxLoadSubject():'';
			$('#YearClassID').change();
			ajaxLoadTerm();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_DBTable()
{
	<?php if(!$accessRight['admin']){?>
	if($('select#subj_1 option').length < 1){
		alert("<?php echo $Lang['SDAS']['Error']['noSubject']?>");
		return;
	}
	<?php }?>
	$( "#PrintBtn" ).hide();
	$( "#ExportBtn" ).hide();
// 	if($('#YearClassID').val() == ''){
// 		return;
// 	}
	if($('#FromAcademicYearID').val() == '' || $('#ToAcademicYearID').val() == ''){
		alert('<?=$Lang['General']['WarningArr']['PleaseSelectPeriod']?>');
		return;
	}

	if($('#FromAcademicYearID')[0].selectedIndex < $('#ToAcademicYearID')[0].selectedIndex){
		alert('<?=$Lang['General']['WarningArr']['EndPeriodCannotEarlierThanStartPeriod']?>');
		return;
	}

	
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"index.php?t=ajax.ajax_subject_class_distribution_cross_year",
		$('#form1').serialize(),
		function(ReturnData)
		{
			$( "#PrintBtn" ).show();
			if($('#DBTableDiv').find('.chart_tables_2').length == 0 && $('#display_checkbox_table').attr('checked') ){
	  			$( "#ExportBtn" ).show();
  			} else {
  				$( "#ExportBtn" ).hide();
  			}
			
		}
	);
}

function js_Print()
{
	document.form1.submit();
}

function ajaxLoadSubject(yearClassID){
	if(yearClassID=='')yearClassID=null;
	var subjectList = [<?=$subjectList ?>];
	var selfClassList = [<?=$selfClassList ?>];
	var currentSelectedSubjectID = $('div#subjectSelection').find('#FromSubjectID').val();
	var classTeacherAry = [];
	<?php if(!empty($accessRight['classTeacher'])){ ?>
			classTeacherAry = [<?php echo implode(',', Get_Array_By_Key($accessRight['classTeacher'], 'YearClassID'));?>];
	<?php } ?>
	var includeSubjectIDArr = [];
	if(yearClassID != null){
	<?php foreach($subjectClassAry as $sid => $classList){?>
		subjectClassList = ['<?php echo implode("','", $classList); ?>'];
		if(subjectClassList.indexOf(yearClassID) != -1){
			subjectList.push("<?php echo $sid;?>");
		}
	<?php }?>
	if( selfClassList.indexOf(yearClassID) == -1 ){
		includeSubjectIDArr = subjectList;
	}
	} else {
		ycIDAry = [];
		$('#YearClassID').find('option').each(function(){
			ycIDAry.push($(this).val());
		});
		for(let x in ycIDAry){
			<?php foreach($subjectClassAry as $sid => $classList){?>
				subjectClassList = ['<?php echo implode("','", $classList); ?>'];
				if(subjectClassList.indexOf(ycIDAry[x]) != -1){
					subjectList.push("<?php echo $sid;?>");
				}
			<?php }?>
			if( selfClassList.indexOf(ycIDAry[x]) == -1 ){
				includeSubjectIDArr = subjectList;
			}
		}
	}
//	$('div#subjectSelection').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
// 		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
// 		{
// 			'Action': 'Subject_Selection',
// 			'YearTermID': '',
// 			'SubjectID': currentSelectedSubjectID,
// 			'SelectionID': 'FromSubjectID',
// 			'OnChange': '',
// 			'NoFirst': 1,
//			'FirstTitle': '<?=$Lang['iPortfolio']['OverallResult'] ?>',
// 			'IncludeSubjectIDArr[]': includeSubjectIDArr
			<?php if(!$accessRight['admin'] && !$isSubjectPanelView){?>
// 			,'yearClassId' : yearClassID
			<?php }?>
// 		},
// 		function(ReturnData)
// 		{
// 		}
// 	);
	$.ajax({
		type: "GET",
		url: "/home/student_data_analysis_system/ajax/ajax_get_subject_selection_box.php",
		data: {
			'ay_id': '',//'<?=$currentAcademicYearID?>',
			'y_id': (yearClassID) ? '' : $('#YearID').val(),
			'yearClassId': yearClassID,
			<?php if($accessRight['admin']){?>
			'filterSubject[]': [],
			<?php } else {?>
			'filterSubject[]': (classTeacherAry.indexOf(yearClassID) == -1 ) ? includeSubjectIDArr : [],
			<?php }
				if($accessRight['admin']){?>
			'firstItem': '<?=$ec_iPortfolio['overall_score']?>'
			<?php } else if(!empty($accessRight['classTeacher'])){?>
			'firstItem': (classTeacherAry.indexOf(yearClassID) != -1) ? '<?=$ec_iPortfolio['overall_score']?>' : ''
			<?php }?>
		},
		success: function (msg) {
			$("#subjectSelection").html(msg);
		}
	});

}
function ajaxLoadTerm(){
	/*
	var AcademicYearID = $('#FromAcademicYearID').val();

	$.ajax({
		type: 'POST',
		url: "/home/student_data_analysis_system/ajax/ajax_get_year_term_selection_box.php",
		data: {
			"AcademicYearID" : AcademicYearID,
			'firstItem': '<?=$ec_iPortfolio['overall_result']?>'
			},
		success: function (msg) {
			$("#Term_cell").html(msg);
		}
		
	});
	*/
	var ay_id = $('#FromAcademicYearID').val();
	if(ay_id == ''){
		$("#Term_cell").html("");
		return;
	}

	$.ajax({
		type: "GET",
		url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
		data: "ay_id="+ay_id,
		success: function (msg) {
			$("#Term_cell").html(msg);
		}
	});
}
</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="POST" action="stats_print.php" target="iport_stats">
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
		
<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
				<td valign="top"><?=$FormSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top"><div id="ClassSelectionDiv"></div></td>
			</tr>
<?php }else{ ?>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
				<td valign="top"><?=$FormSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top">
					<div id="ClassSelectionDiv">
						<?=$classSelectionHTML?>
					</div>
				</td>
			</tr>
<?php } ?>
			<tr id='PeriodRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["period"]?></span></td>
				<td valign="top"><?=$Lang['General']['From']?> <?=$FromAcademicYearSelection?> <?=$Lang['General']['To']?> <?=$ToAcademicYearSelection?></td>
			</tr>
			<tr id='TermRow'>
				<td class="field_title"><span class="tabletext"><?=$Lang['General']['Term']?></span></td>
				<td valign="top"><div id="Term_cell"></div></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
				<td valign="top">
					<div id="subjectSelection">
						<?=$FromSubjectSelection?>
					</div>
				</td>
			</tr>
			
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['chart_type']?></span></td>
				<td>
					<div><span class="tabletext"><input type="radio" id="Stacked_percentage_column" name="chartType" value="column" checked="<?php echo ($sys_custom['SDAS']['SKHTST']? '':'checked')?>"><label for="Stacked_percentage_column"><?= $Lang['SDAS']['StackedPercentageColumn']?></label></span></div>
					<div><span class="tabletext"><input type="radio" id="spline_diagram" name="chartType" value="spline" checked="<?php echo ($sys_custom['SDAS']['SKHTST']? 'checked':'')?>"><label for="spline_diagram"><?=$Lang['SDAS']['SplineDiagram']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="Student_distribution_chart" name="chartType" value="distribution_chart"><label for="Student_distribution_chart"><?=$Lang['SDAS']['StudentDistributionChart']?></label></span></div>
				</td>
			</tr>
			<tr id='displayRow'>
            	<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['DisplayItem']?></span></td>
            	<td>
            		<div><span class="tabletext">
            		<input type='checkbox' id='display_checkbox_chart' name="display_checkbox[chart]" value="1" checked><label for="display_checkbox_chart"><?=$Lang['SDAS']['class_subject_performance_summary']['DataChart'] ?></label>
            		<input type='checkbox' id='display_checkbox_table' name="display_checkbox[table]" value="1"><label for="display_checkbox_table"><?=$Lang['SDAS']['class_subject_performance_summary']['DataTable'] ?></label>
            		</span></div>
            	</td>
            </tr>
			<tr id='option'>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['XAsixOrdering']?></span></td>
				<td>
					<div><span class="tabletext">
					<input type='radio' id='option_checkbox_ascendong' name="option_checkbox" value="0" checked><label for="option_checkbox_ascendong"><?=$Lang['SDAS']['Ascending'] ?></label>
					<input type='radio' id='option_checkbox_descendong' name="option_checkbox" value="1"><label for="option_checkbox_descendong"><?=$Lang['SDAS']['Descending'] ?></label>
					</span></div>
				</td>
			</tr>
			<tr id='base'>
				<td class="field_title"><span class="tabletext"><?=	$ec_iPortfolio['full_mark']?></span></td>
				<td>
					<div><span class="tabletext"><input type="checkbox" id="base_checkbox" name="base_checkbox" value="1" checked="checked"><label for="base_checkbox"><?=$Lang['SDAS']['ConvertTo100']?></label></span></div>
				</td>
			</tr>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
		<?= $btn_Print ?>
	</div>
	
	<div id="PrintButton"><?php echo $htmlAry['contentTool']?></div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
	
	<input type="hidden" name="Action" id="Action" value="STUDENT_PROGRESS" />
</form>



<script>
$('.filterAll').change(function(){
	if($(this).attr('checked')){
		$(this).parent().find('input[name="filterColumn\[\]"]').attr('checked','checked');
	}else{
		$(this).parent().find('input[name="filterColumn\[\]"]').removeAttr('checked');
	}
});
$('input[name="filterColumn\[\]"]').click(function(){
	if(!$(this).attr("checked")){
		$(this).parent().find(".filterAll").removeAttr("checked");
	}
});
$('input[name="chartType"]').change(function(){
	if($(this).val()=='column'){
		$('#option').hide();
	}else{
		$('#option').show();
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>