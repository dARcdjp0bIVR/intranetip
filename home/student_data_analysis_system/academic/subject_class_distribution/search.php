<?php
// Using: Philips
/**
 * Change Log:
 * 2020-06-11 (Philips)
 *  -   modified class subject filter
 * 
 * 2020-05-05 (Philips) [2019-0710-1725-23085]
 *  -   Added distribution chart
 *  
 * 2020-01-08 (Philips) [DM#3713]
 * Prevent foreach error of non-array object
 *
 * 2019-09-10 (Bill)    [2019-0710-1725-23085]
 *  -   Added Highest Marks & Lowest Marks display options for Data Table
 *
 * 2019-06-19 Philips
 * -    Added Subject Group Access right for class selection
 *
 * 2019-01-11 Isaac
 * Added export table function
 * 
 * 2017-12-12 Pun
 * Added Overall option to subject selection
 * 
 * 2017-08-14 Villa
 * Modified the call ajax method using index?t=??? instead of calling direct link
 * 
 * 2017-01-09 Villa
 * Add UI selection - class/subject group
 * 
 * 2016-12-14 Villa
 * Add UI selection - base on 100 
 * 
 * 2016-12-07 Villa
 * Fix do not load the class while entering the page
 * 
 * 2016-12-06 Villa
 * modified get_overall_performace() - change js checking
 * update the UI
 * 
 * 2016-09-13 Villa
 * - Modified checkAll(), fixing check all (uncheck all) problem
 * 
 * 2016-09-08 Villa
 *  -	Add Print Function
 *  
 * 2016-09-01 (Omas)
 * - modified get_subject_multiopt() - removing yearClassId from ajax
 * 
 * 2016-08-30 Pun
 * - Added chart type selection 
 * 
 * 2016-06-23 Anna
 * - Change default value of class, subject 
 * 
 * 2016-06-03 Anna
 * - Change view of table and bar chart
 * 
 * 2016-03-30 Pun
 *  - Modified get_year_opt(), get_yearclass_multiopt(), fixed does not reset the <select> while selected empty
 *  
 * 2016-03-21 Pun [94169, 94175]
 *  - Modified get_subject_multiopt(), fixed hardcoded academic year ID
 *  
 * 2016-02-23 Pun
 *  - Change the chart from Flash to d3.js
 */

include_once($PATH_WRT_ROOT . 'includes/libclass.php');
######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$lclass = new libclass();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
// debug_pr($accessRight);
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

if(isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && 
		in_array('subject_class_distribution',$sys_custom['SDAS']['SKHTST']['FullAccessReport'])){
	$accessRight['admin'] = 1;
}

$ayterm_selection_html = '';
//$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
//$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classTeacherClassIdArr = array();
$yearClassArray = array();
$subjectIDAry = array();
$yearIDAry = array();
$yearAry = array();
$subjectClassAry = array();

$classSelectionHTML = '<select name="YearClassID[]">';
$classSelectionHTML .= "<option>".Get_Lang_Selection("-- 選擇  --","-- SELECT  --")."</option>";
foreach($accessRight['classTeacher'] as $yearClass){
	$yearClassArray[$yearClass['YearClassID']] = array("ClassTitleEN" => $yearClass["ClassTitleEN"],"ClassTitleB5" => $yearClass["ClassTitleB5"], 'YearID' => $yearClass['YearID']);
	$yearName = $lclass->getLevelName($yearClass['YearID']);
	$yearAry[$yearClass['YearID']] = $yearName;
}
foreach($accessRight['subjectGroup'] as $subjectGroup){
	$sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
	$subjectIDAry[] = $sGroup->SubjectID;
	$studentList = $sGroup->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=1, $showClassID = true, $showYearName = true);
	$classList = BuildMultiKeyAssoc($studentList, array("YearClassID"), array("ClassTitleB5","ClassTitleEN","YearName","YearID"));
	if(!$subjectClassAry[$sGroup->SubjectID]){
		$subjectClassAry[$sGroup->SubjectID] = array();
	}
	foreach($classList as $classID => $classDetail){
		if(!in_array($classID,$subjectClassAry[$sGroup->SubjectID])){
			$subjectClassAry[$sGroup->SubjectID][] = $classID;
		}
		$yearIDAry[$classDetail['YearID']] = $classDetail['YearName'];
		if(!is_array($classTeacherClassIdArr[$classDetail['YearID']])) $classTeacherClassIdArr[$classDetail['YearID']] = array();
		$classTeacherClassIdArr[$classDetail['YearID']][] = $classID;
		if(!$accessRight['subjectPanel']) $yearClassArray[$classID] = array("ClassTitleEN" => $classDetail["ClassTitleEN"],"ClassTitleB5" => $classDetail["ClassTitleB5"], "YearID" => $classDetail['YearID']);
	}
}
foreach($yearClassArray as $yearClassID => $yearClass){
	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$classSelectionHTML .= "<option value=\"{$yearClassID}\">{$name}</option>";
	if(!is_array($classTeacherClassIdArr[$yearClass['YearID']])) $classTeacherClassIdArr[$yearClass['YearID']] = array();
	$classTeacherClassIdArr[$yearClass['YearID']][] = $yearClassID;
}
$subjectIDAry = array_unique($subjectIDAry);
$yAry = array();
foreach($yearIDAry as $yid => $yName){
	$yAry[$yid] = $yName;
}
foreach($yearAry as $yid => $yName){
	$yAry[$yid] = $yName;
}
$yearAry = array();
foreach($yAry as $yearID => $yearName){
	$yearAry[] = array($yearID, $yearName);
}
$classSelectionHTML .= '</select>';
// $classTeacherClassIdList = implode("','", $classTeacherClassIdArr);
// $classTeacherClassIdList = "'{$classTeacherClassIdList}'";
######## Access Right END ########


######## Subject Panel filter subject START ########
$isSubjectPanelView = false;
$subjectPanelAccessRightList = "''";
if( 
	!$accessRight['admin'] && 
	count($accessRight['subjectPanel']) && 
	in_array('subject_performance_statistic', (array)$plugin['SDAS_module']['accessRight']['subjectPanel'])
){
	include_once($PATH_WRT_ROOT."includes/json.php");
	$json = new JSON_obj();
	foreach($accessRight['subjectPanel'] as $_subjectId => $_yearIdArr){
		foreach($_yearIdArr as $_yearID=>$_){
			$subjectPanelAccessRightArr[$_yearID][] = $_subjectId;
		}
	}
	$subjectPanelAccessRightList = $json->encode($subjectPanelAccessRightArr); // For PHP 2D Array to JS Object

	$isSubjectPanelView = true;
}
######## Subject Panel filter subject END ########


######## Page Setting START ########
$CurrentPage = "subject_class_distribution";
$CurrentPageName = $Lang['SDAS']['ClassPerformance']['IndividualYear'];
$TAGS_OBJ[] = array($CurrentPageName,'javascript: void(0);', true);
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"index.php?t=academic.subject_class_distribution.search2");
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossTerm'],"index.php?t=academic.subject_class_distribution.search3");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$YearArr = $li_pf->returnAssessmentYear();
$html_year_selection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='academicYearID' id='academicYearID'", $currentAcademicYearID, 0, 1, "", 2);
$html_classlevel_selection = (sizeof($yearAry)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($yearAry, "name='yearID'");
######## UI Releated END ########


$btnAry[] = array('print', 'javascript:Print()', '', array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_HighChart_JS();
echo $linterface->Include_TableExport();
?>

<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="get" action="">

<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
<!-------- Form START -------->

<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>

	<tr>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
		<td valign="top">
			<?=$html_year_selection?>&nbsp;
            <span id='ayterm_cell'><?=$ayterm_selection_html?></span>
		</td>
	</tr>

	<tr>
		<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Class']?>/<?=$Lang['SDAS']['SubjectGroup'] ?></span></td>
		<td valign="top">
			<input type="radio" id="SubjectGroup_class" name="SubjectGroup" value="class" checked ><label for="SubjectGroup_class"><?=$Lang['SDAS']['Class']?></label>
			<input type="radio" id="SubjectGroup_subjectGroup" name="SubjectGroup" value="subjectGroup"><label for="SubjectGroup_subjectGroup"><?=$Lang['SDAS']['SubjectGroup'] ?></label>
		</td>
	</tr>

	<tr id='YearRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
		<td valign="top" id='y_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?></td>
	</tr>

	<tr id='classRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
    	<td valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>	</td>
	</tr>

    <tr id='SubjectRow'>
		<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['SAMS_subject']?></span></td>
		<td valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?></td>
    </tr>

<?php } else{ ?>

    <tr>
        <td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
        <td valign="top">
            <input type="hidden" name="academicYearID" id="academicYearID" value="<?=$currentAcademicYearID?>" />
            <?=$currentAcademicYear?>&nbsp;&nbsp;
            <div nowrap='nowrap' id='ayterm_cell'><?=$ayterm_selection_html?></div>
        </td>
    </tr>

    <tr>
        <td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Class']?>/<?=$Lang['SDAS']['SubjectGroup'] ?></span></td>
        <td valign="top">
            <input type="radio" id="SubjectGroup_class" name="SubjectGroup" value="class" checked ><label for="SubjectGroup_class"><?=$Lang['SDAS']['Class']?></label>
            <input type="radio" id="SubjectGroup_subjectGroup" name="SubjectGroup" value="subjectGroup"><label for="SubjectGroup_subjectGroup"><?=$Lang['SDAS']['SubjectGroup'] ?></label>
        </td>
    </tr>

    <tr id='classRow'>
        <td class="field_title"><span class="tabletext"><?=$ec_iPortfolio_Report['form']?></span></td>
        <td valign="top" id='y_cell'><?php echo $html_classlevel_selection;?></td>
    </tr>

    <tr>
        <td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class'] ?></span></td>
        <td valign="top">
            <div valign="top" id='yclass_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?></div>
        </td>
    </tr>

    <tr>
        <td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['SAMS_subject']?></span></td>
        <td valign="top">
            <div valign="top" id='subj_cell'><?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?></div>
        </td>
    </tr>

    <input type="hidden" id="based_on_1" name="based_on" value="1" />

<?php } ?>

<tr id='classRow'>
	<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['chart_type']?></span></td>
	<td>
		<div><span class="tabletext"><input type="radio" id="Stacked_percentage_column" name="chartType" value="column" checked="checked"><label for="Stacked_percentage_column"><?=$Lang['SDAS']['StackedPercentageColumn']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="spline_diagram" name="chartType" value="spline"><label for="spline_diagram"><?=$Lang['SDAS']['SplineDiagram']?></label></span></div>
		<div><span class="tabletext"><input type="radio" id="Student_distribution_chart" name="chartType" value="distribution_chart"><label for="Student_distribution_chart"><?=$Lang['SDAS']['StudentDistributionChart']?></label></span></div>
	</td>
</tr>

<tr id='displayRow'>
	<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['DisplayItem']?></span></td>
	<td>
		<div><span class="tabletext">
		<input type='checkbox' id='display_checkbox_chart' name="display_checkbox[chart]" value="1" checked><label for="display_checkbox_chart"><?=$Lang['SDAS']['class_subject_performance_summary']['DataChart'] ?></label>
		<input type='checkbox' id='display_checkbox_table' name="display_checkbox[table]" value="1"><label for="display_checkbox_table"><?=$Lang['SDAS']['class_subject_performance_summary']['DataTable'] ?></label>
		</span></div>
	</td>
</tr>

<tr id='option'>
	<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['XAsixOrdering']?></span></td>
	<td>
		<div><span class="tabletext">
		<input type='radio' id='option_checkbox_ascendong' name="option_checkbox" value="0" checked><label for="option_checkbox_ascendong"><?=$Lang['SDAS']['Ascending'] ?></label>
		<input type='radio' id='option_checkbox_descendong' name="option_checkbox" value="1"><label for="option_checkbox_descendong"><?=$Lang['SDAS']['Descending'] ?></label>
		</span></div>
	</td>
</tr>

<tr id='table_option'>
    <td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['class_subject_performance_summary']['DataTableInfo']?></span></td>
    <td>
        <div><span class="tabletext">
        <input type='checkbox' id='display_checkbox_data_max' name="display_checkbox_data[max]" value="1" checked><label for="display_checkbox_data_max"><?=$iPort["HighestMark"] ?></label>
        <input type='checkbox' id='display_checkbox_data_min' name="display_checkbox_data[min]" value="1" checked><label for="display_checkbox_data_min"><?=$iPort["LowestMark"] ?></label>
        </span></div>
    </td>
</tr>

<tr id='base'>
	<td class="field_title"><span class="tabletext"><?=	$ec_iPortfolio['full_mark'] ?></span></td>
	<td>
		<div><span class="tabletext"><input type="checkbox" id="base_checkbox" name="base_checkbox" value="1" checked="checked"><label for="base_checkbox"><?=$Lang['SDAS']['ConvertTo100']?></label></span></div>
	</td>
</tr>

<!-------- Form END -------->
</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	    <!--input type="button" id="reset_btn" class="formbutton" value="<?=$button_reset?>" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"-->
	</div>

	<div id="PrintButton">
	    <?php echo $htmlAry['contentTool']?>
	</div>
	<div id="PrintArea" style="text-align:center"><div id="overallPerformancDiv"></div></div>
</form>

<script language="javascript">
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});

var data;
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {
	var tableTitle = ['Class Comparison', 'Pass Percentage']

 	$(".chart_tables table:visible").each(function(index, element) {
		$( this ).tableExport({
            type: 'csv',
            headings: true,
            fileName: 'Class Subject Performance Distribution '+tableTitle[index]
        });
    });
}

$(function(){
	$('#option').hide();
    $('#table_option').hide();
	$("#y_cell").find('[name="yearID"]').change(get_yearclass_multiopt);
	function get_yearterm_opt(){
		var ay_id = $('#academicYearID').val();
		if(ay_id == ''){
			$("#ayterm_cell").html("");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_yearterm_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#ayterm_cell").html(msg);
			}
		});
	}
	function get_year_opt(){
		var ay_id = $('#academicYearID').val();
		if(ay_id == ''){
			$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
			$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class'] ?>");
			$("#y_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/student_data_analysis_system/ajax/ajax_get_year_opt.php",
			data: "ay_id="+ay_id,
			success: function (msg) {
				$("#y_cell").html(msg);
				$("#y_cell").find('[name="yearID"]').change(get_yearclass_multiopt);
				get_yearclass_multiopt();
			}
		});
	}
	function get_yearclass_multiopt()
	{
		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
		//var classAry = [<?php if($classTeacherClassIdList != ''){echo $classTeacherClassIdList;}?>];
	
		$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
		if(typeof y_id == "undefined" || y_id == ""){
			$("#yclass_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['form']?>");
			return;
		}

		$("#yclass_cell").html('').append(
			$(loadingImage)
		);
		
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_class_multiopt2.php",
			data: "y_id="+y_id+"&ay_id="+ay_id,
			success: function (msg) {
				$("#yclass_cell").html(msg);
				classAry = {};
				<?php foreach($classTeacherClassIdArr as $yearID => $classID){
					echo "classAry[$yearID] = ['" . implode("','", $classID) . "'];";
				}?>
				<?php if(!$accessRight['admin'] && !$accessRight['subjectPanel']){?>
                    var filterClass = $('#yclass_cell').find('.yearClassId').filter(function(index){
                    	if(classAry[y_id] instanceof Array)
							return classAry[y_id].indexOf($(this).val()) == -1;
						else
							return false;
                    });
                    filterClass.next().remove();
                    filterClass.remove();
				<?php }?>
				$("#yclass_cell").find('.checkMaster').change(checkAll);
				$("#yclass_cell").find('input[type="checkbox"]').change(updateSubjectSelectDisplay);
				$("#yclass_cell").attr("checked", "checked");
				$("#YCCheckMaster.checkMaster").attr("checked", "checked");
				$(".yearClassId").attr("checked", "checked"); 
				$("#yclass_cell").find('.checkMaster').change();
			}
		});
	}

	function updateSubjectSelectDisplay(){
		if($('#yclass_cell').find('.yearClassId:checked').length == 0){
			$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
			return;
		}
		$("#subj_cell").html('').append(
			$(loadingImage)
		);

		var selfClassArr = [<?=$classTeacherClassIdList ?>];
		y_id = $("[name=yearID]").val();
		classAry = {};
		<?php foreach($classTeacherClassIdArr as $yearID => $classID){
			echo "classAry[$yearID] = ['" . implode("','", $classID) . "'];";
		}?>
		var isOnlySelfClassChecked = true;
		$('#yclass_cell').find('.yearClassId:checked').each(function(index, ele){
			if($.inArray($(ele).val(), classAry[y_id]) == -1){
				isOnlySelfClassChecked = false;
				return false; // Break $.each()
			}
		});
		
		var filterSubjectArr = [];
		<?php if($isSubjectPanelView){ ?>
			if(!isOnlySelfClassChecked){
				var subjectPanelAccessRightList = <?=$subjectPanelAccessRightList ?>;
				filterSubjectArr = subjectPanelAccessRightList[ $('[name="yearID"]').val() ];
				if( typeof(filterSubjectArr) == 'undefined'){
					filterSubjectArr = [0];
				}
			}
		<?php } ?>

		var subjectClassArr = [];
		<?php foreach($subjectClassAry as $subjectID => $classIDAry){
    		    echo "subjectClassArr['$subjectID'] = [];\n";
    		    foreach($classIDAry as $classID){
    		        echo "subjectClassArr['$subjectID'].push('$classID');\n";
    		    }
		}?>
		<?php if(!$accessRight['admin']){?>
			var classInUse = [];
			$('#yclass_cell').find('.yearClassId:checked').filter(function(index){
				var classId = $(this).val();
				if(classInUse.indexOf(classId) == -1)
					classInUse.push(classId);
			});
			subjectClassArr.forEach(function(classAry, subjectID){
				var check = true;
				classInUse.forEach(function(v, x){
					if(classAry.indexOf(v) == -1){
						check = false;
					}
				})
				if(check) filterSubjectArr.push(subjectID);
			});
		<?php }?>
		
		get_subject_multiopt(filterSubjectArr);
	}
	
	function get_subject_multiopt(filterSubjectArr)
	{
		filterSubjectArr = filterSubjectArr || [];

		var y_id = $("[name=yearID]").val();
		var ay_id = $("#academicYearID").val();
		var subjectClassArr = [];
		<?php foreach($subjectClassAry as $subjectID => $classIDAry){
    		    echo "subjectClassArr['$subjectID'] = [];\n";
    		    foreach($classIDAry as $classID){
    		        echo "subjectClassArr['$subjectID'].push('$classID');\n";
    		    }
		}?>
	
		if(typeof y_id == "undefined" || y_id == "")
		{
			$("#subj_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>");
			return;
		}

		$.ajax({
			type: "GET",
			url: "/home/student_data_analysis_system/ajax/ajax_get_subject_selection_box.php",
			data: {
				'ay_id': ay_id,//'<?=$currentAcademicYearID?>',
				'y_id': y_id,
				//'yearClassId': $(this).val(),
				'filterSubject[]': filterSubjectArr,
				'firstItem': ($('input[name="SubjectGroup"]:checked').val() == 'class')? '<?=$ec_iPortfolio['overall_result'] ?>' : ''
			},
			success: function (msg) {
				if($('#yclass_cell').find('.yearClassId:checked').length == 0){
					$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');

					return;
 				}

				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);
 				$("#subj_cell").attr("checked", "checked");
 				$("#SubjCheckMaster.checkMaster").attr("checked", "checked");
 				$(".subjectChk").attr("checked", "checked");
			}
		});
	}

	function checkAll(){
		var checked = $(this).attr("checked");
		if(($(this).attr("checked"))=="checked" ){
			$(this).parent().parent().parent().find("input[name]").attr("checked", checked);
		}
		else{
			$(this).parent().parent().parent().find("input[name]").removeAttr("checked");
		}
	};

	$('#academicYearID').change(function(){
		$("#ayterm_cell").html('').append(
			$(loadingImage).clone()
		);

		get_yearterm_opt();
		<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>
		    get_year_opt();
		<?php }else{?>
		    $('#yclass_cell > select').change();
		<?php }?>
	});

	$("#reset_btn").click(function(){
		$("select[name=academicYearID]").val('');
		$("#ayterm_cell").html('');
		$("#y_cell").html('');

		get_yearclass_multiopt();
		get_subject_multiopt();
	});

	$('#viewForm').click(get_overall_performace);
	function get_overall_performace()
	{
		var form = $('#form1');
		if( $('#academicYearID').val()==''){ //academic year
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
		if($('select[name="yearID"]').val()==''){ //form
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
		if($('#form1').find('.yearClassId').length > 0 && $('#form1').find('.yearClassId:checked').length==0)
		{
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
		if(typeof $('#subj_1').val()==='undefined'){ //subject
			alert("<?=$ec_warning['please_select_class']?>");
			return false;
		}
// 		else if($('#form1').find('#subj_cell [name]:checked').length==0)
// 		{
			
// 			return false;
// 		}
		
	  	$.ajax({
	  		type: "POST",
	  		url: "index.php?t=ajax.ajax_subject_class_distribution",
	  		data: $('#form1').serialize(),
	  		beforeSend: function () {
                $("#overallPerformancDiv").parent().children().remove().end().append(
                $("<div></div>").attr("id", "overallPerformancDiv").append(
                    $(loadingImage)
                )
	        ); 
	        },
            success: function (msg) {
	  			$("#overallPerformancDiv").html(msg);
// 	  			$("#PrintButton").show();
	  			$( "#PrintBtn" ).show();
	  			if($('#overallPerformancDiv').find('.chart_tables_2').length == 0 && $('#display_checkbox_table').attr('checked') ){
		  			$( "#ExportBtn" ).show();
	  			} else {
	  				$( "#ExportBtn" ).hide();
	  			}
	  		}
	  	});
	}

	////////For Class Teacher START ////////
	$('#yclass_cell > select').change(function(){
// 	$('#yclass_cell').change(function(){
	var classID = $(this).val();
	if(classID==''){
		$("#subj_cell").html('<?=$iPort["options_select"]?> <?=$ec_iPortfolio['class']?>');
	}else{
		$.ajax({
			type: "GET",
			url: "/home/portfolio/profile/management/ajax_get_subject_multiopt.php",
			data: {
				'ay_id': '<?=$currentAcademicYearID?>',
				'y_id': '',
				'yearClassId': $(this).val()
			},
			beforeSend: function () {
				$("#subj_cell").html('').append(
					$(loadingImage)
				);
			},
			success: function (msg) {
				$("#subj_cell").html(msg);
				$("#subj_cell").find('.checkMaster').change(checkAll);
			}
		});
	}
	});

	////////For Class Teacher END ////////
	$('#academicYearID').change();

	$('input[name="chartType"]').change(function(){
		if($(this).val()=='column'){
			$('#option').hide();
		}else{
			$('#option').show();
		}
	});
	$('input[name="SubjectGroup"]').change(function(){
		var option = $(this).val();
// 		if(option=='class'){
// 			$('#classRow').show();
// 		}else{
// 			$('#classRow').hide();
// 		}
		get_yearclass_multiopt();
	});
    $('input#display_checkbox_table').change(function(){
        if($(this).attr('checked')){
            $('#table_option').show();
        }else{
            $('#table_option').hide();
        }
    });
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>