<?php
/**
 * Change Log:
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-01-10 Villa
 *  -	Modified $FromSubjectSelection() support subject component
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 (Cara):
 * 	- Changed default value of resultType checkbox
 * 2015-02-17 (Pun):
 *  - Changed type filter lang
 * 
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$sbj = new subject();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
if($accessRight['admin']){
	$subjectIdArr = '';
}else{
	$subjectIdArr = array_keys((array)$accessRight['subjectPanel']);
}
######## Access Right END ########

######## Subject Selection START ########
if(!$accessRight['admin']){
    $subjectIDAry = array();
    foreach($accessRight['classTeacher'] as $class){
        $cYearID = $class['YearID'];
        $subjects = $sbj->Get_Subject_By_Form($cYearID);
        $sIDAry = Get_Array_By_key($subjects, 'RecordID');
        $subjectIDAry = array_merge($subjectIDAry, $sIDAry);
    }
    foreach($accessRight['subjectGroup'] as $subjectGroup){
        $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
        $subjectIDAry[] = $sGroup->SubjectID;
    }
    if($accessRight['subjectPanel']){
    	$subjectIDAry = array_merge($subjectIDAry, array_keys((array)$accessRight['subjectPanel']));
    }
}

######## Subject Selection END ##########

######## Page Setting START ########
$CurrentPage = "subject_statistics";
$CurrentPageName = $Lang['SDAS']['menu']['SubjectStats'];
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['IndividualYear'],"index.php?t=academic.subject_statistics.search",true);
$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"index.php?t=academic.subject_statistics.search2");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########

######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$YearArr = $li_pf->returnAssessmentYear();
$SingleAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='SingleAcademicYearID' id='SingleAcademicYearID' ", $CurrentAcademicYearID, 0, 0, "", 2);
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FromAcademicYearID' id='FromAcademicYearID' ", $CurrentAcademicYearID, 0, 0, "", 2);
$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' ", $CurrentAcademicYearID, 0, 0, "", 2);


// $FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=1, '', '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdArr);
$FromSubjectSelection = $libSDAS->Get_Subject_Selection($subjectIDAry);
//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Subject Statistics'
	});

}
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsCurSingleAcademicYearID = '<?=$FromAcademicYearID?>';
var jsClearCoo = '<?=$clearCoo?>';
$(document).ready( function() {
// 	if (jsCurYearID == '') {
// 		jsCurYearID = $('select#YearID').val();
// 	}
	
// 	if (jsCurFromAcademicYearID == '') {
// 		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
// 	}	
// 	if (jsCurToAcademicYearID == '') {
// 		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
// 	}
	if (jsCurSingleAcademicYearID == '') {
		jsCurSingleAcademicYearID = $('select#SingleAcademicYearID').val();
	}
	$('select#ToAcademicYearID').val('');
 	$('select#FromAcademicYearID').val('');
	//js_Reload_Class_Selection();
	//js_Reload_Term_Selection('From');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	$("#resultType_Class").attr("checked", "checked");
	
	//js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/ajax/ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
}

function js_Reload_DBTable()
{
		/*{
			Action: 'SUBJECT_STATS',
			FromAcademicYearID: $('select#FromAcademicYearID').val(),
			FromSubjectID: $('select#FromSubjectID').val(),
			ResultType: ResultTypes
			
		}*/
	$("#PrintBtn").hide(); 
	$("#ExportBtn").hide(); 
	if( $('#SingleAcademicYearID').val() == ""){
		
		alert("<?=$Lang['General']['JS_warning']['SelectSchoolYear']?>");
		return;
	}
		
// 	if($("#type_multiple").attr("checked")){
// 		if ($('#FromAcademicYearID').val() == '' || $('#ToAcademicYearID').val() == '' )
// 		{
//			alert("<?=$Lang['General']['JS_warning']['SelectSchoolYear']?>");
// 			return;
// 		}
// 	}
		
// 	if($("#type_single").attr("checked")){
// 		if ($('#SingleAcademicYearID').val() == '' )
// 		{
//			alert("<?=$Lang['General']['JS_warning']['SelectSchoolYear']?>");
// 			return;
// 		}
// 	}

	if($('[name="ResultType[]"]:checked').length == 0){
		alert('<?=$iPort["options_select"] . ' ' . $iPort["report_col"]["type"] ?>');
		return false;
	}

	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		$("#form1").serialize(),
		
		function(ReturnData)
		{
			$("#PrintBtn").show();
			$( "#ExportBtn" ).show();	
		}
	);
	
}
</script>
<form id="form1" name="form1" method="POST" action="advancement.php">
	<input type="hidden" name="Action" value="SUBJECT_STATS" />
	<?=$html_tab_menu ?>
					<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
						<tr>
							<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
							<td valign="top" id="year_single" ><?=$SingleAcademicYearSelection?></td>
						</tr>
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td valign="top"><?=$FromSubjectSelection?></td>
						</tr>
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["type"]?></span></td>
							<td valign="top">
								<input type="checkbox" id="resultType_SelectAll" />
								<label for="resultType_SelectAll"><?=$Lang['Btn']['SelectAll']?></label> &nbsp;&nbsp;&nbsp;
								
								<input type="checkbox" id="resultType_Form" name="ResultType[]" value="form" />
								<label for="resultType_Form"><?=$ec_iPortfolio['form']?></label> &nbsp;
								<input type="checkbox" id="resultType_Class" name="ResultType[]" value="class" />
								<label for="resultType_Class"><?=$ec_iPortfolio['class']?></label> &nbsp;
								<input type="checkbox" id="resultType_Group" name="ResultType[]" value="group" />
								<label for="resultType_Group"><?=$iPort["SubjectGroup"]?></label> &nbsp;
							</td>
						</tr>
					</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	

		<div id="PrintArea"><div id="DBTableDiv"><?=$h_DBTable?></div></div>

	
</form>

<script>
$("#resultType_SelectAll").click(function(){
	if($(this).attr("checked")){
		$("#resultType_Form").attr("checked", "checked");
		$("#resultType_Class").attr("checked", "checked");
		$("#resultType_Group").attr("checked", "checked");
	}else{
		$("#resultType_Form").removeAttr("checked");
		$("#resultType_Class").removeAttr("checked");
		$("#resultType_Group").removeAttr("checked");
	}
});
$("#resultType_Form, #resultType_Class, #resultType_Group").click(function(){
	if(!$(this).attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>