<?php
// using : Isaac
/*
################### Change Log [Start] ###########
## Date: 2019-01-16 Isaac
##       Added export table function 
##
## Date: 2017-04-18 Omas
##		add twgh jupas report
## Date: 2017-01-16 Omas
##
#################### Change Log [End] ############
*/

// ####### Init START ########
include_once ($PATH_WRT_ROOT . "includes/libpf-exam.php");
$libpf_exam = new libpf_exam ();
$libSDAS = new libSDAS ();
// ####### Init END ########


// ####### Page Setting START ########
$CurrentPage = "jupas_analysis";
$TAGS_OBJ[] = array($Lang['SDAS']['JupasOfferStat'], "index.php?t=academic.jupas.basic_stat");
$TAGS_OBJ[] = array($Lang['SDAS']['JupasVsDse'], "index.php?t=academic.jupas.jupas_vs_dse",1);
$TAGS_OBJ[] = array($Lang['SDAS']['JupasVsDseAvg'], "index.php?t=academic.jupas.jupas_vs_dse_avg");
if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
	$TAGS_OBJ[] = array($Lang['SDAS']['JupasVsDseAvg'].' (TWGH)', "index.php?t=academic.jupas.jupas_twgh");
}
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR ();
// ####### Page Setting END ########

// ####### UI Releated START ########
 $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
// year info

$aYearAssocArr = BuildMultiKeyAssoc ( GetAllAcademicYearInfo (), 
		'AcademicYearID', 
		array (	Get_Lang_Selection (  'YearNameB5', 'YearNameEN')),
		1 );
$yearArr = $libpf_exam->getJupasYearWithRecords ();

$yearSelectArr = array ();
foreach ( ( array ) $yearArr as $yearID ) {
	$yearSelectArr [$yearID] = $aYearAssocArr [$yearID];
}
$FromAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", $CurrentAcademicYearID, 0, 1, "" );
$ToAcademicYearSelection = getSelectByAssoArray ( $yearSelectArr, "name='ToAcademicYearID' id='ToAcademicYearID'", $CurrentAcademicYearID, 0, 1, "" );

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// ####### UI Releated END ########

// ####### UI START ########
$linterface->LAYOUT_START ();
echo $linterface->Include_TableExport();
?>

<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<?php echo $linterface->Include_HighChart_JS()?>

<script>

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'JUPAS vs DSE Results '
	});

}

var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';

$(document).ready( function() {
});

function jsLoadGraph()
{
		
	readyToSubmit = false;
	$('#DBTableDiv').html(ajaxImage);
	$("#PrintBtn").hide();
	$("#ExportBtn").hide();
	var url = "index.php?t=academic.jupas.ajax_task";
	$.ajax({
           type:"POST",
           url: url,
//            async: false,
           data: $("#form1").serialize(),
           success:function(responseText)
           {
        	   $('#DBTableDiv').html(responseText);
        	   $("#PrintBtn").show();
        	   $("#ExportBtn").show();	
           }
    });  
}	
</script>


<form id="form1" name="form1" method="POST" action="ajax_load_stat.php">
	<input type="hidden" name="task" value="jupas_vs_dse" />
	<?=$html_tab_menu?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$Lang['General']['From'].' '.$FromAcademicYearSelection?></td>
		</tr>

	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>

	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>"
			class='formbutton' onmouseover="this.className='formbuttonon'"
			onmouseout="this.className='formbutton'" onclick="jsLoadGraph();" />
	</div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	<br>
	<div id="PrintArea">
		<div id="DBTableDiv" class="chart_tables"></div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP ();
intranet_closedb ();
?>