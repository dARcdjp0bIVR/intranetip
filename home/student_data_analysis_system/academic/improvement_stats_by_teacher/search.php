<?php
// using :
/**
 * Change Log:
 * 2020-06-29 Philips [2019-0917-1535-36207]
 *  -   use YearTerm instead of YearTermID
 * 2020-05-15 Philips [DM3752]
 *  -	Replace From by Fr to avoid blocked ajax request
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2016-12-29 Villa
 *  -	Fix Subject Panel teacherSelection when the page initializate
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-07-14 Pun
 *  -   Fix highchart cannot show data in english view
 * 2016-06-23 Anna
 *  -   Change defalut of comparison
 * 2016-06-03 Cara
 *  -   Change bar chart with highcharts.
 * 2016-02-26 Carlos
 *  -   Added bar chart with d3. 
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$user = new libuser($_SESSION['UserID']);

$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "improvement_stats_by_teacher";
$CurrentPageName = $Lang['SDAS']['menu']['TeachingImprovementStats'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$YearArr = $li_pf->returnAssessmentYear();
$FrAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FrAcademicYearID' id='FrAcademicYearID' onchange=\"js_Reload_Term_Selection('Fr');js_Reload_Term_Selection('To');\"", $CurrentAcademicYearID, 0, 0, "", 2);

$isSubjectPanel = false;
if($accessRight['admin']){
	$AllTeachers = $li_pf->GET_ALL_TEACHER(1);
	$TeacherSelection = $linterface->GET_SELECTION_BOX($AllTeachers, "name='TeacherID' id='TeacherID'", "", $ParSelected="");
}else if(count($accessRight['subjectPanel'])){
	$isSubjectPanel = true;
	$TeacherSelection = '';
	$subjectIdArr = array_keys($accessRight['subjectPanel']);
	$subjectIdList = "'".implode("','", $subjectIdArr)."'";
}

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>
<style>

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.bar {
  fill: steelblue;
}

.x.axis path {
  display: block;
}

</style>

<!-- PrintButton Function -->
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
}

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Teaching Improvement Stats'
	});

}
</script>


<!-- <script src="/templates/d3/d3_v3_5_16.min.js"></script> -->
<!-- <script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script> -->
<script src="/templates/highchart/highcharts.js"></script>
<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTerm = '<?=$FrYearTerm?>';
var jsCurToAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurToYearTerm = '<?=$ToYearTerm?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	//js_Reload_Term_Selection('Fr');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	
	<?php if($isSubjectPanel){ ?>
	$('#FrAcademicYearID').change(function(){
		if($(this).val() == ''){
			$('#teacherSelection').html('');
			return;
		}

		var teacherID = $('#teacherSelection').find('#TeacherID').val();
		$('#teacherSelection').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
			{
				'Action': 'Subject_Teacher_Selection',
				'AcademicYearID': $('#FrAcademicYearID').val(),
				'SelectionID': 'TeacherID',
				'SubjectIdArr[]': [<?=$subjectIdList ?>],
				'AddTeacherID[]': '<?=$_SESSION['UserID']?>',
				'TeacherID': teacherID
			},
			function(ReturnData)
			{
			}
		);
		
	});	
	<?php } ?>
	$('#FrAcademicYearID').change();

	
//js_Reload_Class_Selection();
	//js_Reload_Term_Selection('To', jsRefreshDBTable);
});

function isInt(value) {
  return !isNaN(value) && 
         parseInt(Number(value)) == value && 
         !isNaN(parseInt(value, 10));
}

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: '',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTerm;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTerm, jsType)
{
	eval('jsCur' + jsType + 'YearTerm = jsYearTerm;');
}

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	jsCurToAcademicYearID = $('select#FrAcademicYearID').val();
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTerm = jsCur' + jsType + 'YearTerm;');
	eval('var jsSelectionID = "' + jsType + 'YearTerm";');

	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Term_Assessment_Selection',
			AcademicYearID: jsAcademicYearID,
			YearTerm: jsYearTerm,
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
			NoFirst: 1,
			DisplayAll: 1,
			FirstTitle: '',
			OnlyHasRecord: 1, //Show termAssignment that contains data
			showOverAll: 1 // Show the overall option of the selection box
		},
		function(ReturnData)
		{
			
			$('select#' + jsSelectionID + ' > option').each (function () {
				var _optionVal = $(this).val();	// 43, 43_T1A1, etc...

				if (isInt(_optionVal) && parseInt(_optionVal) > 0) {
					$('select#' + jsSelectionID).val(_optionVal);

					if (jsType=='Fr') {
						return false;	// break;
					}
				}
			});

			eval('jsCur' + jsType + 'YearTerm = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
	js_Reload_Class_Selection();
}

function js_Reload_DBTable()
{
	
	$("#PrintBtn").hide();
	$("#ExportBtn").hide();
	if ($('#FrAcademicYearID').val() == '')
	{
		alert("<?=$Lang['General']['JS_warning']['SelectSchoolYear']?>");
		return;
	}
	if (typeof(document.form1.FrYearTerm)=="undefined" || typeof(document.form1.ToYearTerm)=="undefined")
	{
		alert("<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term']?>");
		return;
	}
	if(document.form1.FrYearTerm.value == document.form1.ToYearTerm.value){
		alert("<?=$Lang['SDAS']['TeachingImprovementStats']['SelectDiffCompare']?>");
		return;
	}
	
	var Fr = $('#FrYearTerm').val().split('_');
	var FrYearTerm = Fr[0];
	var FrTermAssessment = (Fr.length == 2)?Fr[1]:'';
	var to = $('#ToYearTerm').val().split('_');
	var toYearTerm = to[0];
	var toTermAssessment = (to.length == 2)?to[1]:'';
	initSearchDetail(
		$('#FrAcademicYearID').val(),
		FrYearTerm,
		FrTermAssessment,
		toYearTerm,
		toTermAssessment
	);
	$('#ChartDiv').html('');
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php?prepareChartData=1", 
		/*{
			Action: 'TEACHER_STATS_IMPROVE',
			FrAcademicYearID: $('select#FrAcademicYearID').val(),
			FrTermAssessmentID: $('select#FrYearTerm').val(),
			ToTermAssessmentID: $('select#ToYearTerm').val(),
			TeacherID: $('#TeacherID').val()
			
		},*/
		
		$("#form1").serialize(),
		
		function(ReturnData){
			$("#PrintBtn").show();
			$("#ExportBtn").show();	
			$(function () {
				/**
				 * Grid-light theme for Highcharts JS
				 * @author Torstein Honsi
				 */

				// Load the fonts
				Highcharts.createElement('link', {
				   href: 'https://fonts.googleapis.com/css?family=Dosis:400,600',
				   rel: 'stylesheet',
				   type: 'text/css'
				}, null, document.getElementsByTagName('head')[0]);

				Highcharts.theme = {
				   colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
				      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
				   chart: {
				      backgroundColor: null,
				      style: {
				         fontFamily: "Dosis, sans-serif"
				      }
				   },
				   title: {
				      style: {
				         fontSize: '16px',
				         fontWeight: 'bold',
				         textTransform: 'uppercase'
				      }
				   },
				   tooltip: {
				      borderWidth: 0,
				      backgroundColor: 'rgba(219,219,216,0.8)',
				      shadow: false
				   },
				   legend: {
				      itemStyle: {
				         fontWeight: 'bold',
				         fontSize: '13px'
				      }
				   },
				   xAxis: {
				      gridLineWidth: 1,
				      labels: {
				         style: {
				            fontSize: '12px'
				         }
				      }
				   },
				   yAxis: {
				      minorTickInterval: 'auto',
				      title: {
				         style: {
				            textTransform: 'uppercase'
				         }
				      },
				      labels: {
				         style: {
				            fontSize: '12px'
				         }
				      }
				   },
				   plotOptions: {
				      candlestick: {
				         lineColor: '#404048'
				      }
				   },


				   // General
				   background2: '#F0F0EA'

				};

				// Apply the theme
				Highcharts.setOptions(Highcharts.theme);


			// chart randering
			if(settings.data.length > 0){
				var data = [];
				for (i=0; i< settings.data.length; i++){
					data[i] = [];
					data[i][0]=settings.data[i].x;
					data[i][1]=settings.data[i].<?=$iPort["report_col"]["improved_by_percentage"]?>;
				}
 			
			  $('#ChartDiv').highcharts({
				  chart: {
			            type: 'column'
			        },
			        title: {
			            text: settings.title
			        },
// 			        subtitle: {
// 			            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
// 			        },
			        xAxis: {
			            type: 'category',
			            labels: {
			                rotation: -45,
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: '<?=$Lang['General']['Percentage']?>'
			            }
			        },
			        legend: {
			            enabled: false
			        },
			        tooltip: {
			            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
			            footerFormat: '</table>',
			            shared: true,
			            useHTML: true
			        },
			        plotOptions: {
			            column: {
			                pointPadding: 0.2,
			                borderWidth: 0
			            }
			        },
			        series: [{
			            name: '<?=$Lang['General']['Percentage']?>',
			            data: data
			        }],
			        credits : {enabled: false,},
			    });
			}
		});
// 			if(settings && settings['data'].length>0)
// 			{
// 				settings['container']='#ChartDiv';
// 				js_Generate_Bar_Chart(settings);
// 			}
		});
}

function js_Generate_Bar_Chart(settings)
{
	// start setting parameters
	var container = typeof(settings['container'])=='undenfined'? 'body' : settings['container'];
	var chartWidth = typeof(settings['width'])=='undefined'? $(container).width() : settings['width'];
	var chartHeight = typeof(settings['height'])=='undefined'? 500 : settings['height'];
	var marginTop = typeof(settings['marginTop'])=='undefined'? 50 : settings['marginTop'];
	var marginRight = typeof(settings['marginRight'])=='undefined'? 50 : settings['marginRight'];
	var marginBottom = typeof(settings['marginBottom'])=='undefined'? 100 : settings['marginBottom'];
	var marginLeft = typeof(settings['marginLeft'])=='undefined'? 50 : settings['marginLeft']; 
	var chartTitle =  typeof(settings['title'])=='undefined'?'' : settings['title'];
	var xColumnName = typeof(settings['xColoumName'])=='undefined'? 'x' : settings['xColoumName'];
	var yColumnName = typeof(settings['yColumnName'])=='undefined'? 'y' : settings['yColumnName'];
	var legendSize = typeof(settings['legendSize'])=='undefined'? 18 : settings['legendSize'];
	var data  = typeof(settings['data'])=='undefined'? [] : settings['data'];
	var barColorAry = typeof(settings['barColors'])=='undefined'? [] : settings['barColors'];
	// end setting parameters
	
	$(container).html('');
	
	
	var wrap = function(text, width) {
	  text.each(function() {
	    var text = d3.select(this),
	        words = text.text().split(/\s+/).reverse(),
	        word,
	        line = [],
	        lineNumber = 0,
	        lineHeight = 1.1, // ems
	        y = text.attr("y"),
	        dy = parseFloat(text.attr("dy")),
	        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
	    while (word = words.pop()) {
	      line.push(word);
	      tspan.text(line.join(" "));
	      if (tspan.node().getComputedTextLength() > width) {
	        line.pop();
	        tspan.text(line.join(" "));
	        line = [word];
	        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
	      }
	    }
	  });
	};
	
	var margin = {top: marginTop, right: marginRight, bottom: marginBottom, left: marginLeft},
	    width = chartWidth - margin.left - margin.right,
	    height = chartHeight - margin.top - margin.bottom;
	
	var x0 = d3.scale.ordinal()
	    .rangeRoundBands([0, width], .1);
	
	var x1 = d3.scale.ordinal();
	
	var y = d3.scale.linear()
	    .range([height, 0]);
	
	var color = d3.scale.ordinal()
	    .range(barColorAry);
	
	var xAxis = d3.svg.axis()
	    .scale(x0)
	    .orient("bottom");
	
	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");
	    //.tickFormat(d3.format(".0s"));
	
	var svg = d3.select("#ChartDiv").append("svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	svg.append("text")
		.attr("class", "title")
		.attr("x", width/2)
		.text(chartTitle)
	    .style("text-anchor", "end");
	
	
	// Create the svg:defs element and the gradient definition.
    var svgDefs = svg.append('defs');
    for(var i=0;i<barColorAry.length;i++)
    {
    	if(typeof(barColorAry[i])=='string') continue;
	    var mainGradient = svgDefs.append('linearGradient')
	        .attr('id', barColorAry[i]['id']).attr('x1','0%').attr('x2','0%').attr('y1','0%').attr('y2','100%');
	    for(var j=0;j<barColorAry[i]['stops'].length;j++)
	    {
		    mainGradient.append('stop')
		        .attr('style', 'stop-color:' + barColorAry[i]['stops'][j]['color'])
		        .attr('offset', barColorAry[i]['stops'][j]['offset']);
	    }
    }
	// End of creating svg:defs and gradient definition
	

	  var columnNames = d3.keys(data[0]).filter(function(key) { return key !== xColumnName; });
	
	  data.forEach(function(d) {
	    d.values = columnNames.map(function(name) { return {name: name, value: +d[name]}; });
	  });
	  
	  x0.domain(data.map(function(d) { return d[xColumnName]; }));
	  x1.domain(columnNames).rangeRoundBands([0, x0.rangeBand()]);
	  y.domain([0, d3.max(data, function(d) { return Math.max(100, d3.max(d.values, function(d) { return d.value; })); })]);
	
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	      .selectAll(".tick text")
      	  .call(wrap, x0.rangeBand());
	
	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", -50)
	      .attr("x", -height / 2)
	      .attr("dy", "1em")
	      .style("text-anchor", "end")
	      .text(yColumnName);
	
	  var column = svg.selectAll(".column")
	      .data(data)
	    .enter().append("g")
	      .attr("class", "column")
	      .attr("transform", function(d) { return "translate(" + x0(d[xColumnName]) + ",0)"; });
	
	  column.selectAll("rect")
	      .data(function(d) { return d.values; })
	    .enter().append("rect")
	      .attr("width", x1.rangeBand())
	      .attr("x", function(d) { return x1(d.name); })
	      .attr("y", function(d) { return y(d.value); })
	      .attr("height", function(d) { return height - y(d.value); })
	      .style("fill", function(d) {
	      		if(typeof(color(d.name))=='string')
	      		{
	      			return color(d.name);
	      		}else{
	      			return 'url("#' + color(d.name)['id'] + '")';
	      		}
	      	});
	
	  var legend = svg.selectAll(".legend")
	  		.data(columnNames)
	      //.data(columnNames.slice().reverse())
	    	.enter().append("g")
	      .attr("class", "legend")
	      .attr("transform", function(d, i) { return "translate(0," + i * (legendSize + 2) + ")"; });
	
	  legend.append("rect")
	      .attr("x", width - legendSize)
	      .attr("width", legendSize)
	      .attr("height", legendSize)
	      .style("fill", function(d, i) {
	      		if(typeof(barColorAry[i])=='string')
	      		{
	      			return barColorAry[i];
	      		}else{
	      			return 'url("#' + barColorAry[i]['id'] + '")';
	      		}
	      	});
	
	  legend.append("text")
	      .attr("x", width - (legendSize + 6))
	      .attr("y", 9)
	      .attr("dy", ".35em")
	      .style("text-anchor", "end")
	      .text(function(d) { return d; });
	  
}

$(function(){

	
}); // End $(function(){
</script>

<form id="form1" name="form1" method="POST" action="advancement.php">
	<input type="hidden" name="Action" value="TEACHER_STATS_IMPROVE" />
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
				<td valign="top"><?=$FrAcademicYearSelection?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["compare"]?></span></td>
				<td valign="top"><table border="0"><tr><td><div id="FrYearTermSelectionDiv" style="float:left;">--</div></td><td> VS </td><td><div id="ToYearTermSelectionDiv" style="float:left;">--</div></td></tr></table></td>
			</tr>
			
<?php if($accessRight['admin'] || $isSubjectPanel){ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["teacher"]?></span></td>
				<td valign="top">
					<div id="teacherSelection">
						<?=$TeacherSelection?>
					</div>
				</td>
			</tr>
<?php }else{ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["teacher"]?></span></td>
				<td valign="top">
					<span><?=$user->UserName()?></span>
					<input type="hidden" name="TeacherID" id="TeacherID" value="<?=$_SESSION['UserID']?>" />
				</td>
			</tr>
<?php } ?>
			
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable()" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
		<br>
		<br>
		<div id="ChartDiv" class="chart"></div>
	</div>
	
	<?php
	if($isSubjectPanel){
		foreach($subjectIdArr as $subjectID){
	?>
		<input type="hidden" name="filterSubjectIdArr[]" value="<?=$subjectID ?>" />
	<?php
		}
	}?>
</form>



<form id="searchDetail" action="/home/portfolio/profile/management/ajax_get_improved_details.php" target="_blank" style="display:none">
	<input id="searchDetail_AcademicYearID" name="AcademicYearID" />
	<input id="searchDetail_FrYearTerm" name="FromYearTermID" />
	<input id="searchDetail_FrTermAssessment" name="FromTermAssessment" />
	<input id="searchDetail_ToYearTerm" name="ToYearTermID" />
	<input id="searchDetail_ToTermAssessment" name="ToTermAssessment" />
	<input id="searchDetail_SubjectID" name="SubjectID" />
	
	<input type="submit" />
</form>


<script>
function resetSearchDetail(){
	$('#searchDetail input[name="StudentID\[\]"]').remove();
}
function initSearchDetail(AcademicYearID, FrYearTerm, FrTermAssessment, ToYearTerm, ToTermAssessment){
	$('#searchDetail_AcademicYearID').val(AcademicYearID);
	$('#searchDetail_FrYearTerm').val(FrYearTerm);
	$('#searchDetail_FrTermAssessment').val(FrTermAssessment);
	$('#searchDetail_ToYearTerm').val(ToYearTerm);
	$('#searchDetail_ToTermAssessment').val(ToTermAssessment);
}

function submitSearchDetail(SubjectID, StudentIdArr){
	resetSearchDetail();
	$.each(StudentIdArr, function (index, element){
		$('#searchDetail').append('<input name="StudentID[]" value="' + element + '" />');
	});
	$('#searchDetail_SubjectID').val(SubjectID);
	$('#searchDetail').submit();
}
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>