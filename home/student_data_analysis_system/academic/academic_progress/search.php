<?php
// using : Isaac
/**
 * Change Log:
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2017-07-13 Villa
 *  -	Add default to -- please select -- after clicking Type btn
 * 2017-06-26 Villa
 * 	-	Add Monitoring Group Related
 *  -	No Longer Support Subject Teacher
 *  
 * 2017-05-31 Villa
 *  - Optimize the disable subject method/ checking
 *  
 * 2017-05-29 Villa
 *  - Optimize the disable subject method
 *  
 * 2017-05-26 Villa
 *  - Add ajax/ js in handling multi-access right problem for subject panel/ class teacher
 *  
 * 2017-04-06 Villa #J114859 
 * 	- Change TermSelection Box - Suppoer TermAssessment
 *  - Change Ajax Para for the New Assessment Term Selection Box 
 *  
 * 2016-12-12 Villa
 *  - support in subject panel
 *  
 * 2016-12-07 Villa 
 *  - modified js_Reload_DBTable() - pass target
 *  - update the UI 
 *  
 * 2016-12-06 Omas
 *  - Added subjectTeacher permission 
 * 2016-11-22 Villa
 *  - Add advanced filter
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-08-01
 * 	-	Add Ordering & Coloring Filter

 * 2016-03-11 Pun
 * 	-	Added float header
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$subjectIdFilterArr = '';
$subjectNoFirst = 0;
if(!$accessRight['admin'] 
  && (count($accessRight['subjectPanel']) || count($accessRight['subjectTeacher'] ) )
){
	//$subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
	$subjectIdFilterArr = array_merge( (array)array_keys((array)$accessRight['subjectPanel']), $accessRight['subjectTeacher']);
	if(count($accessRight['subjectTeacher'])){
		$subjectNoFirst = 1;
	}
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "academic_progress";
$CurrentPageName = $Lang['SDAS']['menu']['AcademicProgress'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
### YearArr ClassArr START ###
if($accessRight['admin'] || $accessRight['subjectPanel']){
	$YearIDArr = '';
	$ClassIDArr='';
}else{
	if(count($accessRight['classTeacher'])>0){
		$YearIDArr =  Get_Array_By_Key($accessRight['classTeacher'],'YearID');
		$ClassIDArr = Get_Array_By_Key($accessRight['classTeacher'],'YearClassID');
		$ClassIDArr = implode(',',$ClassIDArr);
	}
}
### YearArr ClassArr END ###

### FormSelection Box START ###
if($accessRight['admin'] || $accessRight['subjectPanel']){
	$FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
}else{
	if(count($accessRight['classTeacher'])>0){
		$FormSelection = $libFCM_ui->Get_Specific_Form_Selection('YearID', $YearIDArr, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
	}
}
### FormSelection Box END ###

### YearSelection Box START ###
$YearArr = $li_pf->returnAssessmentYear();
$FromAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FromAcademicYearID' id='FromAcademicYearID' onchange=\"js_Changed_AcademicYear_Selection(this.value, 'From')\"", $FromAcademicYearID, 0, 1, "", 2);
$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"js_Changed_AcademicYear_Selection(this.value, 'To')\"", $ToAcademicYearID, 0, 1, "", 2);
### YearSelection Box END ###


// $FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'From\')"', $noFirst=1, $noPastYear=0, $FromAcademicYearID);
// $ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $tag='onchange="js_Changed_AcademicYear_Selection(this.value, \'To\')"', $noFirst=1, $noPastYear=0, $ToAcademicYearID);

### SubjectSelection Box START ###
if( count($accessRight['classTeacher'] ) ){
	$subjectIdFilterArr='';
}
if(!count($accessRight['subjectPanel'])  && count($accessRight['MonitoringGroupPIC']) ){
	$subjectIdFilterArr='';
}
$FromSubjectSelectionALL = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $subjectNoFirst, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr='');
$ToSubjectSelectionALL = $libSCM_ui->Get_Subject_Selection('ToSubjectID', $ToSubjectID, $OnChange='', $subjectNoFirst, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr='');


$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $subjectNoFirst, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdFilterArr);
$ToSubjectSelection = $libSCM_ui->Get_Subject_Selection('ToSubjectID', $ToSubjectID, $OnChange='', $subjectNoFirst, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdFilterArr);
### SubjectSelection Box END ###
//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### TypeRadioBtn START ###
$selected = '1';
if(count($accessRight['classTeacher']) || count($accessRight['subjectPanel']) || $accessRight['admin']){
	$TypeRadioBtn = $linterface->Get_Radio_Button('TypeBtn_Class','TypeBtn','Class',$selected,'',$iPort['Class'],'js_Changed_Type(this.value)');
	$selected = '';
	$TypeRadioBtn .= "&nbsp";
}

if(count($accessRight['subjectPanel']) || $accessRight['admin']){
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_SubjectClass','TypeBtn','SubjectClass',$selected,'',$iPort['SubjectGroup'],'js_Changed_Type(this.value)');
	$selected = '';
}
##Monitoring Group 
if(count($accessRight['MonitoringGroupPIC']) || $accessRight['admin']){
	$TypeRadioBtn .= "&nbsp";
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_MonitoringGroup','TypeBtn','MonitoringGroup',$selected,'',$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] ,'js_Changed_Type(this.value)');
}
$MonintoringGroup = $libSDAS->getMonitoringGroup('',$CurrentAcademicYearID);
$MonintoringGroup_arr = BuildMultiKeyAssoc($MonintoringGroup, 'GroupID');
$MonintoringGroupSelection = '<select id="MonitoringGroupPIC" name="MonitoringGroupPIC">';
foreach ((array)$accessRight['MonitoringGroupPIC'] as $_GroupID){
	if($MonintoringGroup_arr[$_GroupID]){
		$MonintoringGroupSelection .= '<option value="'.$_GroupID.'">'.Get_Lang_Selection($MonintoringGroup_arr[$_GroupID]['GroupNameCH'], $MonintoringGroup_arr[$_GroupID]['GroupNameEN']).'</option>';
	}
}
$MonintoringGroupSelection .= '</select>';
### TypeRadioBtn END ###

### Subject Group Selection START ###
if($accessRight['admin'] ){
	$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox();
}else{
// 	if( count($accessRight['subjectGroup'])){
// 		$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox($accessRight['subjectGroup']);
// 	}
	if(count($accessRight['subjectPanel'])){
		foreach ($accessRight['subjectPanel'] as $subjectPanel =>$_subjectPanel){
			$subjectIDKey[] = $subjectPanel;		
		}
	}
	$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox($accessRight['subjectGroup'],$subjectIDKey);
}
### Subject Group Selection END ###

### Set Target if no display the target bar START ###
$display = 'none';
$display2 = 'none';
if(!($accessRight['admin'] || count($accessRight['subjectPanel']) ) ){
	
	if(  !( count($accessRight['classTeacher']) && count($accessRight['subjectPanel']) ) ){
		if(count($accessRight['classTeacher'])>0 || count($accessRight['subjectPanel'])){
			$target = 'Class';
		}elseif(count($accessRight['subjectPanel'])>0){
			$target = 'SubjectClass';
			$display = '';
		}elseif(count($accessRight['MonitoringGroupPIC'])>0){
			$target = 'MonitoringGroup';
			$display2 = '';
		}
	}
}
### Set Target if no display the target bar END ###

######## UI Releated END ########

######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>
<?php echo $linterface->Include_HighChart_JS()?>
<script>
Highcharts.setOptions({
	lang: {
		downloadSVG: "",
		downloadPDF: "",
		printChart: ""
	}
});
</script>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	$("#PrintArea table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Academic Progress'
	});

}
</script>

<script language="javascript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFromAcademicYearID = '<?=$FromAcademicYearID?>';
var jsCurFromYearTermID = '<?=$FromYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '0';
var sort_by = 0;
var sort_order = 0;


$(document).ready( function() {
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}
	if (jsCurFromAcademicYearID == '') {
		jsCurFromAcademicYearID = $('select#FromAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	js_Reload_Class_Selection();
	js_Reload_Term_Selection('From');
	js_Reload_Term_Selection('To');
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}

	$('#subjectGroup_box').change(function(){
		DisableSubject_SubjectGroup();
	});

});

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	var ClassID = '<?=$ClassIDArr?>';
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/student_data_analysis_system/ajax/ajax_get_class_selection_box.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: jsCurYearID,
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: 'DisableSubject();',
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 1,
			ClassID: ClassID,
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
			DisableSubject();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

// function js_Changed_Term_Selection(jsYearTermID, jsType)
// {
// 	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
// }

function js_Changed_Type(jsType){
	if(jsType=='Class'){
		$('#formRow').show();
		$('#classRow').show();
		$('#subjectRow').hide();
		$('#MonitoringGroup').hide();
		DisableSubject();
	}
	if(jsType=='SubjectClass'){
		$('#formRow').hide();
		$('#classRow').hide();
		$('#subjectRow').show();
		$('#MonitoringGroup').hide();
		DisableSubject_SubjectGroup();
	}
	if(jsType=='MonitoringGroup'){
		$('#formRow').hide();
		$('#classRow').hide();
		$('#subjectRow').hide();
		$('#MonitoringGroup').show();
		DisplayAllSubject();
	}
}

// function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
// {
// 	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
// 	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
// 	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	
//	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
// 		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
// 		{
// 			Action: 'Term_Selection',
// 			AcademicYearID: jsAcademicYearID,
// 			YearTermID: jsYearTermID,
// 			SelectionID: jsSelectionID,
// 			OnChange: 'js_Changed_Term_Selection(this.value, \''+ jsType +'\');',
// 			NoFirst: 0,
// 			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
// 		},
// 		function(ReturnData)
// 		{
// 			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
// 			if (jsRefreshDBTable == 1)
// 				js_Reload_DBTable();
// 		}
// 	);
// }

function js_Reload_Term_Selection(jsType, jsRefreshDBTable)
{
	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	eval('var jsSelectionID = "' + jsType + 'YearTermID";');
	var tag = "name="+jsType+"YearTermID id="+jsType+"YearTermID";
	
	 $('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/student_data_analysis_system/?t=ajax.ajax_get_year_term_id_selection", 
		{
			AcademicYearID: jsAcademicYearID,
			tag: tag,
			ComplexValue : true
		},
		function(ReturnData)
		{
			eval('jsCur' + jsType + 'YearTermID = $("select#' + jsSelectionID + '").val();');
			
			if (jsRefreshDBTable == 1)
				js_Reload_DBTable();
		}
	);
	
}

function js_Reload_DBTable()
{
	checkform();
	$("#PrintBtn").hide();
	$("#ExportBtn").hide();	
	var gender = $('input:checkbox:checked.gender').map(function () {
		  return this.value;
		}).get();
	var SEN = $('input:checkbox:checked.SENfilter').map(function () {
		  return this.value;
		}).get();	
	var x_name = $("#FromAcademicYearID").find('option:selected').text() + " " + $("#FromYearTermID").find('option:selected').text() + " " + $("#FromSubjectID").find('option:selected').text();
	var y_name = $("#ToAcademicYearID").find('option:selected').text() + " " + $("#ToYearTermID").find('option:selected').text() + " " + $("#ToSubjectID").find('option:selected').text();

	var target
	if(typeof $('[name="TypeBtn"]:checked').val()==='undefined'){ //if no show the target bar
		target = '<?=$target?>';
	}else{ //get target bar
		target = $('[name="TypeBtn"]:checked').val();
	}

	var low_score_val = $('#low_score').val();
	if( low_score_val<0 || !low_score_val || (low_score_val%1)!==0){
		$('#low_score_warning').html('<?=$Lang['SDAS']['AcademicProgress']['PleaseEnterInt'] ?>');
		return false;
	}
	var high_score_val = $('#high_score').val();
	if( high_score_val<0 || !high_score_val || (high_score_val%1)!==0){
		$('#high_score_warning').html('<?=$Lang['SDAS']['AcademicProgress']['PleaseEnterInt'] ?>');
		return false;
	}

	if($('#FromYearTermID').val()=='-1'||$('select#FromSubjectID').val()=='-1'){
		alert('<?=$Lang['SDAS']['AcademicProgress']['PleaseSelectSubject']?>');
		return false;
	}
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_reload.php", 
		{
			
			Action: 'Assessment_Statistics_Report_SDAS',
			YearID: jsCurYearID,
			YearClassID: $('select#YearClassID').val(),
			FromAcademicYearID: jsCurFromAcademicYearID,
// 			FromYearTermID: jsCurFromYearTermID,
			FromYearTermID: $('#FromYearTermID').val(),
			FromSubjectID: $('select#FromSubjectID').val(),
			ToAcademicYearID: jsCurToAcademicYearID,
// 			ToYearTermID: jsCurToYearTermID,
			ToYearTermID:  $('#ToYearTermID').val(),
			ToSubjectID: $('select#ToSubjectID').val(),
			field: $('#field').val(),
			order: $('#order').val(),
			pageNo: $('#pageNo').val(),
			numPerPage: $('#numPerPage').val(),
			page_size_change: $('#page_size_change').val(),
			sort_by: sort_by,
			sort_order: sort_order,
			low_score: '-'+ $('#low_score').val(),
			high_score: $('#high_score').val(),
			x_name: x_name,
			y_name: y_name,
			updateCookie: 1,
			Advanced_Search: $('[name="Advanced_Search"]:checked').val(),
			gender: $('[name="gender"]:checked').val(),
			non_chinese: $('[name="non_chinese"]:checked').val(),
			come_from_china: $('[name="come_from_china"]:checked').val(),
			cross_boundary: $('[name="cross_boundary"]:checked').val(),
			SEN : SEN,
			Target : target,
			SubjectGroup : $('#subjectGroup_box').val(),
			MonitoringGroup : $('#MonitoringGroupPIC').val()
			 
		
		},
		function(ReturnData)
		{
			$('#hide_show').text("<?= $Lang['SDAS']['AcademicProgress']['HideShow'] ?>");
			form1_gopage = window.gopage;
			$('select[name="num_per_page"]').attr('onchange', '').change(function(){
				$('#pageNo').val(1);
				$('#page_size_change').val(1);
				$('#numPerPage').val($(this).val());
				js_Reload_DBTable();
			});
			$('.page_no select').attr('onchange', '').change(function(){
				$('#pageNo').val($(this).val());
				js_Reload_DBTable();
			});
			$("#PrintBtn").show();
			$("#ExportBtn").show();		
		}
	);
	
}

window.sortPage = function (a, b, obj){
	$('#order').val(a);
	$('#field').val(b);
	$('#pageNo').val(1);
	js_Reload_DBTable();
}
window.gopage = function (page, obj){
	$('#pageNo').val(page);
	js_Reload_DBTable();
}

$(function(){
	$('#FromSubjectID').change(function(){
		$('#ToSubjectID').val($(this).val());
	});

	$('#sort_by_0').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 0;
	    }
	});
	$('#sort_by_1').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 1;
	    }
	});
	$('#sort_by_2').click(function(){
	    if($(this).is(':checked')){
	        sort_by = 2;
	    }
	});
	$('#sort_order_0').click(function(){
	    if($(this).is(':checked')){
	        sort_order = 0;
	    }
	});
	$('#sort_order_1').click(function(){
	    if($(this).is(':checked')){
	    	sort_order = 1;
	    }
	});
	<?php if(count($accessRight['subjectPanel'])>0){?>
		DisableSubject_SubjectGroup();
	<?php }?>
	<?php if(count($accessRight['subjectPanel'])>0){?>
		DisableSubject();
	<?php }?>
});
function checkform(){
}
function AdvancedSearch(Btn){
	if(Btn.value=='1'){
		$('.advanceFilter').show();
	}else{
		$('.advanceFilter').hide();
	}
}

function DisableSubject(){
	<?php if(!$accessRight['admin']){?>
	$.ajax({
		async: false,
		url: "index.php?t=ajax.ajax_returnSubjectID_arr",
		type: "POST",
		data: {"YearID":$('select#YearID').val(), "YearClassID": $('select#YearClassID').val() },	
		success: function(ReturnData){
			HideAndDisableSubject(ReturnData);
		}
	});
	<?php }?>
}

function DisableSubject_SubjectGroup(){
	<?php if(!$accessRight['admin']){?>
	$.ajax({
		async: false,
		url: "index.php?t=ajax.ajax_returnSubjectID_SubjectGroup",
		type: "POST",
		data: {"SubjectGroupID":$('select#subjectGroup_box').val() },	
		success: function(ReturnData){
			HideAndDisableSubject(ReturnData);
		}
	});
	<?php }?>
}
function DisplayAllSubject(){
	$('#FromSub').html('<?=$FromSubjectSelectionALL?>');
	$('#ToSub').html('<?=$ToSubjectSelectionALL?>');
}

function HideAndDisableSubject(jSonData){
	var data = JSON.parse(jSonData);
	var option = document.createElement("option");
	
	if(data['NeedFilter']==true){
		$('#FromSubjectID option').each(function(){
				$(this).attr("disabled", true);
				$(this).hide();
				$(this).parent('optgroup').hide();
		});
		
		$('#ToSubjectID option').each(function(){
				$(this).attr("disabled", true);
				$(this).hide();
				$(this).parent('optgroup').hide();
		});
		$('#FromSubjectID option').each(function(){
			if($.inArray(parseInt($(this).val()),data['SubjectIDArr']) < 0 ){
				//do nothing
			}else{
				$(this).removeAttr("disabled");
				$(this).show();
				$(this).parent('optgroup').show();
			}
		});
		
		$('#ToSubjectID option').each(function(){
			if( $.inArray(parseInt($(this).val()),data['SubjectIDArr']) < 0 ){
				//do nothing
			}else{
				$(this).removeAttr("disabled");
				$(this).show();
				$(this).parent('optgroup').show();
			}
		});

		if($('#FromSubjectID option[value="-1"]').val()=='-1'){
			$('#FromSubjectID option[value="-1"]').show();
			$('#FromSubjectID option[value="-1"]').removeAttr("disabled");
			$('#FromSubjectID option[value="-1"]').attr('selected',true);
		}else{
			$('#FromSubjectID').prepend("<option value='-1' selected><?=$Lang['SDAS']['AcademicProgress']['PleaseSelect']?></option>");	
			$('#FromSubjectID option[value="-1"]').attr('selected',true);
		}
		if($('#ToSubjectID option[value="-1"]').val()=='-1'){
			$('#ToSubjectID option[value="-1"]').show();
			$('#ToSubjectID option[value="-1"]').removeAttr("disabled");
			$('#ToSubjectID option[value="-1"]').attr('selected',true);
		}else{
			$('#ToSubjectID').prepend("<option value='-1' selected><?=$Lang['SDAS']['AcademicProgress']['PleaseSelect']?></option>");	
			$('#ToSubjectID option[value="-1"]').attr('selected',true);
		}
	}else{
		$('#FromSubjectID option').each(function(){
			$(this).removeAttr("disabled");
			$(this).show();
			$(this).parent('optgroup').show();
		});
		$('#ToSubjectID option').each(function(){
			$(this).removeAttr("disabled");
			$(this).show();
			$(this).parent('optgroup').show();
		});
	}
	
}
</script>
<!-- <script src="/templates/highchart/highcharts.js"></script> -->
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="POST">
	<input type="hidden" id="order" name="order" />
	<input type="hidden" id="field" name="field" />
	<input type="hidden" id="pageNo" name="pageNo" />
	<input type="hidden" id="numPerPage" name="numPerPage" />
	<input type="hidden" id="page_size_change" name="page_size_change" />

	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tbody>
			<tr id="level1">
				<td>
					<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
						<?php if($accessRight['admin'] ||
								count($accessRight['subjectPanel']) || 
										(	count($accessRight['classTeacher'])  && count($accessRight['subjectPanel']) 
											|| (count($accessRight['classTeacher'])  && count($accessRight['MonitoringGroupPIC'])) 
											|| (count($accessRight['MonitoringGroupPIC'])  && count($accessRight['subjectPanel'])) 
										)
								){?>
						<tr>
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['Mode'] ?></span></td>
							<td valign="top"><?=$TypeRadioBtn?></td>
						</tr>					
						<?php }?>
						<?php if($accessRight['admin']||
								count($accessRight['subjectPanel']) || 
								count($accessRight['classTeacher']) 
								){?>
						<tr id='formRow'>
							<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
							<td valign="top"><?=$FormSelection?></td>
						</tr>
						<?php }?>
						<?php if($accessRight['admin'] ||
								count($accessRight['subjectPanel'])||
								count($accessRight['classTeacher'])
								){?>
						<tr id='classRow'>
							<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
							<td valign="top"><div id="ClassSelectionDiv"></div></td>
						</tr>
						<?php }?>
						<?php if($accessRight['admin'] ||
								count($accessRight['subjectPanel'])
								){?>
						<tr id='subjectRow' style='display: <?=$display?>'>
							<td class="field_title"><span class="tabletext"><?=$iPort['SubjectGroup'] ?></span></td>
							<td><?=$Subject_Group_Selection?></td>
						</tr>
						<?php }?>
						<!-- MonitoringGroup -->
						<?php if(count($accessRight['MonitoringGroupPIC'])||$accessRight['admin']){?>
						<tr id='MonitoringGroup' style='display: <?=$display2?>'>
							<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] ?></span></td>
							<td><?=$MonintoringGroupSelection?></td>
						<?php }?>
						<!--tr>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
							<td class="dotline" height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" height="1" width="10"></td>
						</tr-->
					</table>
				</td>
			</tr>
			<tr id="level2">
				<td>
					<table border="0" cellspacing="0" cellpadding="6" class="form_table_v30">
						<tr>
							<td colspan="4" style="text-align:center;"><em class="form_sep_title"> - <?=$iPort["compareScore"]?> -</em></td>
							<!--td colspan="2"><em class="form_sep_title"> - <?=$iPort["compareScore"]?> -</em></td-->
						</tr>
						
						<tr>
							<td class="field_title" style="width:200px"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td valign="top">
								<div style="float:left;"><?=$FromAcademicYearSelection?></div>
								<div id="FromYearTermSelectionDiv" style="float:left;"></div>
							</td>
							<td class="field_title" style="width:200px"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
							<td valign="top">
								<div style="float:left;"><?=$ToAcademicYearSelection?></div>
								<div id="ToYearTermSelectionDiv" style="float:left;"></div>
							</td>
						</tr>
						
						<tr>
							<td class="field_title" style="width:200px"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td valign="top">
								<div id="FromSub"><?=$FromSubjectSelection?></div>
							</td >
							<td class="field_title" style="width:200px"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
							<td valign="top">
								<div id="ToSub"><?=$ToSubjectSelection?></div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id="level3">
				<td>
					<table border="0" cellspacing="0" cellpadding="6" class="form_table_v30">
						<tr>
							<td colspan="4" style="text-align:center;"><em class="form_sep_title"> - <?=$Lang['SDAS']['AcademicProgress']['Selection']?> -</em></td>
						</tr>
						
						<tr>
							<td class="field_title" style="width:200px"><?=$Lang['SDAS']['AcademicProgress']['Sort']?></td>
							<td valign="top" >
								<div><span class="tabletext"><input type="radio" id="sort_by_0" name="sort_by" value="0" <?=($sort_by==0?"checked='checked'":"")?>/><label for="sort_by_0"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Class']?></label></span></div>
								<div><span class="tabletext"><input type="radio" id="sort_by_1" name="sort_by" value="1" <?=($sort_by==1?"checked='checked'":"")?>/><label for="sort_by_1"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['ScoreDiff']?></label></span></div>
								<div><span class="tabletext"><input type="radio" id="sort_by_2" name="sort_by" value="2" <?=($sort_by==2?"checked='checked'":"")?>/><label for="sort_by_2"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['ZScoreDiff']?></label></span></div>
							</td>
						</tr>
						<tr>
							<td class="field_title" style="width:200px"><?=$Lang['SDAS']['AcademicProgress']['Order']?></td>
							<td valign="top" >
								<div><span class="tabletext"><input type="radio" id="sort_order_0" name="sort_order" value="0" <?=($sort_order==0?"checked='checked'":"")?>/><label for="sort_order_0"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Asc']?></label></span></div>
								<div><span class="tabletext"><input type="radio" id="sort_order_1" name="sort_order" value="1" <?=($sort_order==1?"checked='checked'":"")?>/><label for="sort_order_1"><?=$Lang['SDAS']['AcademicProgress']['Ordering']['Dec']?></label></span></div>
							</td>
					    </tr>
					    <tr>
							<td class="field_title" style="width:200px"><?= $Lang['SDAS']['AcademicProgress']['Color'] ?></td>
							<td>
							
<!-- 							<div><select name ='low_sore' id ='low_score'> -->
							<div>
							<font style='width:15px;'>- </font><input type='number' name ='low_score' id ='low_score' min="0" max="999" value="5">
							<?php
// 							for($i = -5; $i >= -100; $i=$i-5){
// 								if($i == -10){
// 									echo'<option value='.$i.' selected>'.$i.'</option>';
// 								}else{
// 									echo'<option value='.$i.'>'.$i.'</option>';
// 								}
// 							}
							?>
<!-- 							</select> -->
							<label><?=$Lang['SDAS']['AcademicProgress']['highlight']['red'] ?></label>
							<span id='low_score_warning' style="color:red;"></span>
							</div>
							
<!-- 							<div><select name ='high_sore' id ='high_score'> -->
							<div>
							<font style='width:15px;'>+</font><input type='number' name ='high_score' id ='high_score' min="0" max="999" value="5">
							<?php
// 								for($i = 5; $i <= 100; $i=$i+5){
// 									if($i == 10){
// 										echo'<option value='.$i.' selected>+'.$i.'</option>';
// 									}else{
// 										echo'<option value='.$i.'>+'.$i.'</option>';
// 									}
// 								}
							?>
<!-- 							</select> -->
							<label> <?=$Lang['SDAS']['AcademicProgress']['highlight']['blue'] ?></label>
							<span id='high_score_warning' style="color:red;"></span>
							</div>
							
							</td>
							
					    </tr>
					    	<?php if($sys_custom['SDAS']['AdvanceFilter']){?>
							<tr id="filterRow">
								<td class="field_title" style="border-bottom: 1px solid rgb(238, 238, 238);" nowrap="nowrap" valign="top">	<?=	$Lang['iPortfolio']['Filter'] ?>
								</td>
								<td valign="top">
								<?=$libSDAS->GET_FILTER_TABLE()?>
								</td>
							</tr>
							<?php }?>
						
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30">
		<input type="button"value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>	
	</div>
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>