<?php 
/*
 * Change Log:
 * 
 * Date:	2017-07-06	Villa
 * -	Change the TermSelectionBox Display
 * -	Change TermSelectionBox -> Support Whole Year
 * Date:	2017-05-25	Villa
 * -	Open the File
 */
$libSDAS = new libSDAS();
$interface = new interface_html();
######## Init END ########
##Access Right Start
if(!$plugin['SDAS_module']['customReport']['kcmc_form_passing_rate']){
	echo 'Access Denied';
	return false;
}
##Access Right END

#Get All TermAssessment
$sql = 'Select 
			AcademicYearID, YearTermNameEN, YearTermNameB5
		FROM 
			ACADEMIC_YEAR_TERM
	
		ORDER BY 
			TermStart
		';
$rs =  $libSDAS->returnResultSet($sql);
foreach ((array)$rs as $_rs){
	$rs2[$_rs['AcademicYearID']][] = Get_Lang_Selection($_rs['YearTermNameB5'], $_rs['YearTermNameEN']);
}
foreach ((array)$rs2 as $_rs2){
	foreach ((array)$_rs2 as $order=>$__rs2){
		$semSort[$order+1] = $__rs2;
	}
}
$numOfTerm = count($semSort);

$sql = 'Select Distinct assm.TermAssessment
	FROM 
		'.$eclass_db.'.ASSESSMENT_SUBJECT_SD_MEAN assm 
';
$rs =  $libSDAS->returnResultSet($sql);
$YearTerm_Selection = '<select name="YearTerm">';
$YearTerm_Selection .= '<option value="">'.$Lang['SDAS']['Form_Passing_Rate']['WholeYear'].'</option>';
for ($i=1;$i<$numOfTerm+1;$i++){
	$YearTerm_Selection .= '<option value="'.$i.'">'.$semSort[$i].'</option>';
	foreach ((array)$rs as $_rs){
		$string = 'T'.$i;
		if(strpos($_rs['TermAssessment'], $string)=== 0 && $_rs['TermAssessment']!='0'){
			$YearTerm_Selection .= '<option value="'.$i.'_'.$_rs['TermAssessment'].'">'.$semSort[$i].'_'.$_rs['TermAssessment'].'</option>' ;
		}
	}
}
$YearTerm_Selection .= '</select>';
#Form Start
$sql = '
	Select 
		Distinct assm.ClassLevelID, y.YearName
	FROM
		'.$eclass_db.'.ASSESSMENT_SUBJECT_SD_MEAN assm 
	INNER JOIN	
		YEAR y
			ON y.YearID = assm.ClassLevelID
';
$rs =  $libSDAS->returnResultSet($sql);
foreach ((array)$rs as $key=>$_rs){
	$FormData[$key][0] = 'Form'.$_rs['ClassLevelID'];
	$FormData[$key][1] = 'Form[]';
	$FormData[$key][2] = $_rs['ClassLevelID'];
	$FormData[$key][3] = true;
	$FormData[$key][5] = $_rs['YearName'];
}
$Form_Checkbox = $interface->Get_Checkbox_Table('Form',$FormData);

#AcademicYear Start
$sql = '
	Select 
		Distinct assm.AcademicYearID, ay.YearNameEN, ay.YearNameB5
	From 
		'.$eclass_db.'.ASSESSMENT_SUBJECT_SD_MEAN assm 
	INNER JOIN
		ACADEMIC_YEAR ay 
			ON assm.AcademicYearID = ay.AcademicYearID
	Order BY
		ay.Sequence desc 
';
$rs = $libSDAS->returnResultSet($sql);

foreach ((array)$rs as $key=>$_rs){
	$AcademicYearData[$key][0] = 'AcademicYear'.$_rs['AcademicYearID'];
	$AcademicYearData[$key][1] = 'AcademicYear[]';
	$AcademicYearData[$key][2] = $_rs['AcademicYearID'];
	$AcademicYearData[$key][3] = true;
	$AcademicYearData[$key][5] = Get_Lang_Selection($_rs['YearNameB5'], $_rs['YearNameEN']);
}
$AcademicYear_Checkbox = $interface->Get_Checkbox_Table('AcademicYear',$AcademicYearData);

#Group By Start
$GroupBy_Selection = '';
$GroupBy_Selection .= '<input type="radio" name="GroupBy" id="GroupBy_subject" value="subject" checked>'.'<label for="GroupBy_subject">'.$Lang['SDAS']['Form_Passing_Rate']['Subject'];
$GroupBy_Selection .= '<input type="radio" name="GroupBy" id="GroupBy_class" value="class">'.'<label for="GroupBy_class">'.$Lang['SDAS']['Form_Passing_Rate']['Class'];


//Functional Button
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
// $btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

######## Page Setting START ########
$CurrentPage = "form_passing_rate";
$CurrentPageName = $Lang['SDAS']['menu']['FormPassingRate'];
$TAGS_OBJ[] = array($CurrentPageName,"");
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########
intranet_opendb();
$linterface->LAYOUT_START();
// echo $linterface->Include_TableExport();
?>

<form id="form1" name="form1" method="get" action="">
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="width: 100%; max-width: 1024px;">
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_Passing_Rate']['AcademicYear']?></span></td>
				<td valign="top"><?=$AcademicYear_Checkbox?></td>
			</tr>		
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_Passing_Rate']['YearTerm']['YearTerm']?></span></td>
				<td valign="top"><?=$YearTerm_Selection?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_Passing_Rate']['Form']?></span></td>
				<td valign="top"><?=$Form_Checkbox?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletext"><?=$Lang['SDAS']['Form_Passing_Rate']['GroupBy'] ?></span></td>
				<td valign="top"><?=$GroupBy_Selection?></td>
			</tr>	
	</table>
	<input type="hidden" id="isExport" name="isExport" value="0" />
	<div class="edit_bottom_v30" style="max-width: 1024px;">
	    <input type="button" id="viewForm" value='<?=$button_view?>' class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'">
	</div>
</form>
<?=$htmlAry['contentTool']?>
<br>
<div id='Table_Div'></div>
<script language="javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";
function Print(){
	var options = { mode : "popup", popClose : false};
	$( "#Table_Div" ).printArea(options);
};

// function exportCSV(){
//  	$('#isExport').val(1);
//  	document.getElementById("form1").method = 'POST';
//  	 document.getElementById("form1").action = '?t=ajax.ajax_form_passing_rate';
// 	$('#form1').submit();
// 	$('#isExport').val(0);
// // 	document.getElementById("form1").action = '';
// }

// 	$("#PrintArea table").tableExport({
// 		type: 'csv',
// 		headings: true,
// 		fileName: 'Teaching Improvement Stats'
// 	});


function checkForm(){ 
	var check = true;
	if($('input[name="Form[]"]:checked').val()>0){
		//Do nth
	}else{
		alert('<?=$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectForm']?>');
		check = false;
	}
	if($('input[name="AcademicYear[]"]:checked').val()>0){
		//Do nth
	}else{
		alert('<?=$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectAcademicYear']?>');
		check = false;
	}
	if($('input[name="GroupBy"]:checked')){
		//Do nth
	}else{
// 		alert('abc');
		check = false;
	}
	return check;
}

$('#viewForm').click(function(){
	if(checkForm()){
		$('#PrintBtn').hide();
		$("#ExportBtn").hide();
		$("#Table_Div").html('').append(
			$(loadingImage)
		);
		$.ajax({
			type: "POST",
			url: "?t=ajax.ajax_form_passing_rate",
			data: $('#form1').serialize(),
			success: function(ReturnHTML){
				$('#Table_Div').html(ReturnHTML);
				$('#PrintBtn').show();
				$("#ExportBtn").show();	
			}
		});
	}
});
</script>
<?php 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>