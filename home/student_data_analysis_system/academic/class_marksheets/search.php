<?php
// using :
/**
 * Change Log:
 * 2020-05-15 Philips [DM3752]
 *  -	Replace From by Fr to avoid blocked ajax request
 * 2019-01-15 Isaac
 *  -   Added export table function
 * 2018-03-16 Omas
 *  -   Fix Sujbect teacher + class teacher cannot show subject select problem 
 * 2017-06-27 Villa
 *  -	Add monitoringGroup Related
 *  -	Modified the access right of class teacher + subject panel/ subject teacher 
 * 2017-05-22 Villa #T116811 
 *  -	Fix if the user having Class Teacher and Subject Teacher access right at the same thing
 * 2016-12-21 Villa
 *  -	Set Subject teacher only can select own subject
 * 2016-12-14 Villa
 *  -	Set TypeBtn value if not generate TypeBtn
 * 2016-12-09 Omas
 * 	- 	Add SubjectGroup selection
 * 2016-09-08 Villa
 *  -	Add Print Function
 * 2016-06-23 Anna
 * -    Default timeperiod as current year and deisplay mark
 * 2016-02-19 Pun
 * 	-	Hide year if it does not have any classes
 * 2016-02-18 Pun
 * 	-	Added "Additional Information" filter
 * 2016-02-17 Pun
 * 	-	Modified js_Reload_DBTable(), added checking for start year smaller then end year
 * 2016-01-26 Pun
 * 	-	Added support for subject panel
 * 	-	Added display "Age", "Primary School", "Percentile"
 */

######## Init START ########
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$li_pf = new libpf_asr();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();
$lpf->updateTabAccessRight($accessRight); // Update $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"] for tab
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$currentAcademicYear = getAcademicYearByAcademicYearID('','');

$ayterm_selection_html = '';
$academic_yearterm_arr = $li_pf->returnAssessmentSemester("", "", $currentAcademicYearID);
$ayterm_selection_html = getSelectByArray($academic_yearterm_arr, "name='YearTermID'", $yt_id, 0, 1, "", 2);

$classSelectionHTML = '<select id="YearClassID" name="YearClassID">';
foreach($accessRight['classTeacher'] as $yearClass){
	$name = ($intranet_session_language=="EN") ? $yearClass["ClassTitleEN"] : $yearClass["ClassTitleB5"];
	$classSelectionHTML .= "<option value=\"{$yearClass['YearClassID']}\">{$name}</option>";
}
$classSelectionHTML .= '</select>';

$isSubjectPanelView = false;
if(
	!$accessRight['admin'] && 
	in_array('class_marksheets', (array)$plugin['SDAS_module']['accessRight']['subjectPanel']) &&
	count($accessRight['subjectPanel'])
){
	$isSubjectPanelView = true;
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "class_subject_performance_summary";
if(!($accessRight['admin']||$accessRight['classTeacher'])){
	$TAGS_OBJ[] = array($Lang['SDAS']['menu']['ClassPerformance'],"javascript: void(0);");
	$TypeBtn = 'SubjectClass';
	if(!$accessRight['subjectPanel']){
		$onlySubject = true;
	}
}else{
	$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['IndividualYear'], 'index.php?t=academic.class_subject_performance_summary.search');
	$TAGS_OBJ[] = array($Lang['SDAS']['ClassPerformance']['CrossYear'],"javascript: void(0);", true);
}
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();


#### Get Year selection START ####
// $FormSelection = $libFCM_ui->Get_Form_Selection('YearID', $YearID, 'js_Changed_Form_Selection(this.value);', $noFirst=1, $isAll=0, $isMultiple=0);
$objYear = new Year();
$allClassArr = $objYear->Get_All_Classes();
$allYearArr = BuildMultiKeyAssoc($allClassArr, array('YearID') , array('YearName'), $SingleValue=1);
$FormSelection = getSelectByAssoArray($allYearArr,' id="YearID" name="YearID" onchange="js_Changed_Form_Selection(this.value);"', $selected=$YearID, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
#### Get Year selection END ####


$YearArr = $li_pf->returnAssessmentYear();
$FrAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='FrAcademicYearID' id='FrAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);

$ToAcademicYearSelection = (sizeof($YearArr)==0) ? "<i>".$no_record_msg."</i>" : getSelectByArray($YearArr, "name='ToAcademicYearID' id='ToAcademicYearID' onchange=\"\" ", $currentAcademicYearID, 0, 0, "", 2);

// if($accessRight['admin'] || !$isSubjectPanelView){
if($accessRight['admin'] || $accessRight['classTeacher']){
	$FrSubjectSelection = $libSCM_ui->Get_Subject_Selection('FrSubjectID', $FrSubjectID, $OnChange='', $noFirst=1, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0);
	$isSubjectPanel = 0;
}
// Subject Panel load the subject <select> by ajax.
// Show all subject if he is the class teacher, otherwise show his subject.
$subjectList = "";
$selfClassList = "";
if($isSubjectPanelView){
	$FrSubjectSelection = '';
	$isSubjectPanel = 1;
	$subjectArr = array_keys($accessRight['subjectPanel']);
	$subjectList = "'".implode("','",$subjectArr)."'";
	$selfClassArr = Get_Array_By_Key($accessRight['classTeacher'], 'YearClassID');
	$selfClassList = "'".implode("','",$selfClassArr)."'";
	foreach ($accessRight['subjectPanel'] as $subjectPanel =>$_subjectPanel){
		$subjectIDKey[] = $subjectPanel;
	}
}elseif($accessRight['subjectTeacher']&& !($accessRight['admin'] )){ //subject teacher
	$isSubjectPanel = 0;
	$subjectIdFilterArr = $accessRight['subjectTeacher'];
// 	$subjectArr = array_keys($accessRight['subjectTeacher']);
// 	$subjectList_St= "'".implode("','",$subjectIdFilterArr)."'";
	$subjectArr = $subjectIdFilterArr;
	$subjectList = "'".implode("','",$subjectArr)."'";
	if(!empty($accessRight['classTeacher'])){
    	$selfClassArr = Get_Array_By_Key($accessRight['classTeacher'], 'YearClassID');
    	$selfClassList = "'".implode("','",$selfClassArr)."'";
	}
	$FrSubjectSelection = $libSCM_ui->Get_Subject_Selection('FrSubjectID', $FrSubjectID, $OnChange='', $noFirst=1, $Lang['iPortfolio']['OverallResult'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr=$subjectIdFilterArr);
	// 		$FrSubjectSelection =$libSCM_ui->Get_Subject_Selection($ID_Name);
}elseif($accessRight['MonitoringGroupPIC'] && !($accessRight['admin'] || $accessRight['classTeacher'])){
	$isSubjectPanel = 0;
	header('Location: index.php?t=academic.class_marksheets.search2');
	exit();
}
//Print Button
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:exportCSV()', '', array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### TypeRadioBtn START ###
$selected = '1';
if(count($accessRight['classTeacher'])|| $accessRight['admin'] || count($accessRight['subjectPanel'])){
	$TypeRadioBtn = $linterface->Get_Radio_Button('TypeBtn_Class','TypeBtn','Class',$selected,'',$iPort['Class'],'js_Changed_Type(this.value)');
	$selected = '0';
}
if(count($accessRight['subjectTeacher'])||$accessRight['admin']){
	$TypeRadioBtn .= "&nbsp";
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_SubjectClass','TypeBtn','SubjectClass',$selected,'',$iPort['SubjectGroup'],'js_Changed_Type(this.value)');
	$selected = '0';
}
if(count($accessRight['MonitoringGroupPIC'])|| $accessRight['admin']){
	$TypeRadioBtn .= "&nbsp";
	$TypeRadioBtn .= $linterface->Get_Radio_Button('TypeBtn_MonitoringGroup','TypeBtn','MonitoringGroup',$selected,'',$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] ,'js_Changed_Type(this.value)');
}
### TypeRadioBtn END ###

### Subject Group Selection START ###
if($accessRight['admin'] ){
	$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox();
}else{
	$Subject_Group_Selection = $libSCM_ui->Get_Current_Year_Subject_Group_SelectionBox($accessRight['subjectGroup'],$subjectIDKey);
}
### Subject Group Selection END ###

######## UI Releated END ########


######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>

<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

function exportCSV() {

	
	$(".chart_tables table").tableExport({
		type: 'csv',
		headings: true,
		fileName: 'Class Marksheets'
	});

}
</script>

<script language="JavaScript">
var jsCurYearID = '<?=$YearID?>';
var jsCurYearClassID = '<?=$YearClassID?>';
var jsCurFrAcademicYearID = '<?=$FrAcademicYearID?>';
var jsCurFrYearTermID = '<?=$FrYearTermID?>';
var jsCurToAcademicYearID = '<?=$ToAcademicYearID?>';
var jsCurToYearTermID = '<?=$ToYearTermID?>';
var jsClearCoo = '<?=$clearCoo?>';

$(document).ready( function() {
	$('#TypeBtn_Class').click();
	if('<?=$onlySubject?>'){
		$('#classRow').hide();
	}
	if (jsCurYearID == '') {
		jsCurYearID = $('select#YearID').val();
	}	
	if (jsCurFrAcademicYearID == '') {
		jsCurFrAcademicYearID = $('select#FrAcademicYearID').val();
	}	
	if (jsCurToAcademicYearID == '') {
		jsCurToAcademicYearID = $('select#ToAcademicYearID').val();
	}	
	
	js_Reload_Class_Selection();
	
	var jsRefreshDBTable = 0;
	if (jsClearCoo == '') {
		jsRefreshDBTable = 1;
	}
	 $("#filterMark").attr("checked", "checked");
	
});

function js_Changed_Type(jsType){
	if(jsType=='Class'){
		$('#formRow').show();
		$('#classRow').show();
		$('#subjectRow').hide();
		var classID = $('#YearClassID').val();
		if(classID!=''){
			ajaxLoadSubject(classID);
		}
	}
	if(jsType=='SubjectClass'){
		$('#formRow').hide();
		$('#classRow').hide();
		$('#subjectRow').show();
		ajaxLoadSubject_BySubjectGroup();
	}
	if(jsType == 'MonitoringGroup'){
		window.location.href = 'index.php?t=academic.class_marksheets.search2';
	}
}

function js_Changed_Form_Selection(jsYearID)
{
	<?php if($isSubjectPanel){ ?>
		$('div#subjectSelection').html('');
	<?php } ?>
	
	jsCurYearID = jsYearID;
	js_Reload_Class_Selection();
}

function js_Reload_Class_Selection()
{
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			Action: 'Class_Selection',
			AcademicYearID: '<?=$CurrentAcademicYearID?>',
			YearID: $('select#YearID').val(),
			SelectedYearClassID: jsCurYearClassID,
			SelectionID: 'YearClassID',
			OnChange: (<?=$isSubjectPanel ?>)? 'ajaxLoadSubject(this.value)' : '', // Only subject panel will load subject <select> by ajax
			IsMultiple: 0,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			jsCurYearClassID = $('select#YearClassID').val();
		}
	);
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearTermID;
}

function js_Changed_AcademicYear_Selection(jsAcademicYearID, jsType)
{
	eval('jsCur' + jsType + 'AcademicYearID = jsAcademicYearID;');
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsYearTermID, jsType)
{
	eval('jsCur' + jsType + 'YearTermID = jsYearTermID;');
}

function js_Reload_DBTable()
{
	$( "#PrintBtn" ).hide();
	$( "#ExportBtn" ).hide();
	if($('[name="TypeBtn"]:checked').val() == 'Class' && $('#YearClassID').val() == ''){
		alert('<?=$Lang['General']['JS_warning']['SelectClasses']?>');
		return;
	}
	if($('#FrAcademicYearID').val() == '' || $('#ToAcademicYearID').val() == ''){
		alert('<?=$Lang['General']['WarningArr']['PleaseSelectPeriod']?>');
		return;
	}

	if($('#FrAcademicYearID')[0].selectedIndex < $('#ToAcademicYearID')[0].selectedIndex){
		alert('<?=$Lang['General']['WarningArr']['EndPeriodCannotEarlierThanStartPeriod']?>');
		return;
	}

	if($('.displayField:checked').length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectColumn']?>');
		return;
	}

// 	Mode: $('[name="TypeBtn"]:checked').val(),
// 	SubjectGroupID : $('select#subjectGroup_box').val(),
	
	$('div#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/ajax_get_mark_analysis.php", 
		$('#form1').serialize(),
		function(ReturnData)
		{
			$( "#PrintBtn" ).show();
			$( "#ExportBtn" ).show();			
		}
	);
}

function js_Print()
{
	document.form1.submit();
}

function ajaxLoadSubject(yearClassID){

	var subjectList = [<?=$subjectList ?>];
	var selfClassList = [<?=$selfClassList ?>];
	var currentSelectedSubjectID = $('div#subjectSelection').find('#FrSubjectID').val();
	
	var includeSubjectIDArr = '';
	if( $.inArray(yearClassID, selfClassList) == -1 ){
		includeSubjectIDArr = subjectList;
	}
	$('div#subjectSelection').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			'Action': 'Subject_Selection',
			'YearTermID': '',
			'SubjectID': currentSelectedSubjectID,
			'SelectionID': 'FrSubjectID',
			'OnChange': '',
			'NoFirst': 1,
			'FirstTitle': '<?=$Lang['iPortfolio']['OverallResult'] ?>',
			'IncludeSubjectIDArr[]': includeSubjectIDArr
		},
		function(ReturnData)
		{
		}
	);
}

function ajaxLoadSubject_BySubjectGroup(){
//	if('<?=$isSubjectPanelView?>'){
		//var subjectList = [<?=$subjectList ?>];
		//var selfClassList = [<?=$selfClassList ?>];
// 	}else{
	var subjectList = [<?=$subjectList ?>];
// 	}
	var currentSelectedSubjectID = $('div#subjectSelection').find('#FrSubjectID').val();
	
	var includeSubjectIDArr = subjectList;
// 	if( $.inArray(yearClassID, selfClassList) == -1 ){
// 		includeSubjectIDArr = subjectList;
// 	}
	$('div#subjectSelection').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"/home/portfolio/profile/management/../../ajax/ajax_reload.php", 
		{
			'Action': 'Subject_Selection',
			'YearTermID': '',
			'SubjectID': currentSelectedSubjectID,
			'SelectionID': 'FrSubjectID',
			'OnChange': '',
			'NoFirst': 1,
			'FirstTitle': '<?=$Lang['iPortfolio']['OverallResult'] ?>',
			'IncludeSubjectIDArr[]': includeSubjectIDArr
		},
		function(ReturnData)
		{
		}
	);
}

</script>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<form id="form1" name="form1" method="POST" action="stats_print.php" target="iport_stats">
	<?=$html_tab_menu ?>
		<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
					<?php if($accessRight['admin'] ||
					count($accessRight['subjectPanel']) || 
					(	count($accessRight['classTeacher'])  && count($accessRight['subjectTeacher']) 
						|| (count($accessRight['classTeacher'])  && count($accessRight['MonitoringGroupPIC'])) 
						|| (count($accessRight['MonitoringGroupPIC'])  && count($accessRight['subjectTeacher'])) 
					)
					){?>
						<tr>
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['Mode'] ?></span></td>
							<td valign="top"><?=$TypeRadioBtn?></td>
						</tr>					
						<?php }else{?>
						<tr style="display: none;">
							<td valign="top"><input type='hidden' name='TypeBtn' value='<?=$TypeBtn ?>'></td>
						</tr>	
							
						<?php }?>
<?php if($accessRight['admin'] || $isSubjectPanelView){ ?>
			<tr id='formRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['form']?></span></td>
				<td valign="top"><?=$FormSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top"><div id="ClassSelectionDiv"></div></td>
			</tr>
<?php }else{ ?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['class']?></span></td>
				<td valign="top">
					<?=$classSelectionHTML?>
				</td>
			</tr>
<?php } ?>
<?php if($accessRight['admin'] ||
								count($accessRight['subjectPanel']) ||
								count($accessRight['subjectGroup'])
								){?>
						<tr id='subjectRow' style='display: <?=$display?>'>
							<td class="field_title"><span class="tabletext"><?=$iPort['SubjectGroup'] ?></span></td>
							<td><?=$Subject_Group_Selection?></td>
						</tr>
<?php }?>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["period"]?></span></td>
				<td valign="top"><?=$Lang['General']['Fr']?> <?=$FrAcademicYearSelection?> <?=$Lang['General']['To']?> <?=$ToAcademicYearSelection?></td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
				<td valign="top">
					<div id="subjectSelection">
						<?=$FrSubjectSelection?>
					</div>
				</td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$ec_iPortfolio['display']?></span></td>
				<td valign="top">
					<input type="checkbox" class="filterAll" id="filterAllA"/>
					<label for="filterAllA"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					<input type="checkbox" id="filterMark" name="filterColumn[]" value="filterMark" class="displayField" />
					<label for="filterMark"><?=$Lang['iPortfolio']["Score"]?></label>&nbsp;
					
					<?php if($sys_custom['iPf']['Report']['AssessmentStatisticReport']['showMarksDifferent']){ ?>
					<input type="checkbox" id="filterMarkDiff" name="filterColumn[]" value="filterMarkDiff" class="displayField" />
					<label for="filterMarkDiff"><?=$Lang['iPortfolio']["MarkDifference"]?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterStandardScore" name="filterColumn[]" value="filterStandardScore" class="displayField" />
					<label for="filterStandardScore"><?=$Lang['iPortfolio']["StandardScore"]?></label>&nbsp;
					
					<input type="checkbox" id="filterStandardScoreDiff" name="filterColumn[]" value="filterStandardScoreDiff" class="displayField" />
					<label for="filterStandardScoreDiff"><?=$Lang['iPortfolio']["StandardScoreDifference"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPosition" name="filterColumn[]" value="filterFormPosition" class="displayField" />
					<label for="filterFormPosition"><?=$Lang['iPortfolio']["FormPosition"]?></label>&nbsp;
					
					<input type="checkbox" id="filterFormPositionDiff" name="filterColumn[]" value="filterFormPositionDiff" class="displayField" />
					<label for="filterFormPositionDiff"><?=$Lang['iPortfolio']["FormPositionDifference"]?></label>&nbsp;
					
				</td>
			</tr>
			<tr id='classRow'>
				<td class="field_title"><span class="tabletext"><?=$iPort["additionalInformation"]?></span></td>
				<td valign="top">
					<input type="checkbox" class="filterAll" id="filterAllB"/>
					<label for="filterAllB"><?=$Lang['Btn']['SelectAll']?></label>&nbsp;&nbsp;&nbsp;
					
					
					<input type="checkbox" id="filterAge" name="filterColumn[]" value="filterAge" />
					<label for="filterAge"><?=$ec_iPortfolio['age']?></label>&nbsp;
					
					<?php if($sys_custom['StudentAccountAdditionalFields']){ ?>
						<input type="checkbox" id="filterPrimarySchool" name="filterColumn[]" value="filterPrimarySchool" />
						<label for="filterPrimarySchool"><?=$Lang['AccountMgmt']['PrimarySchool']?></label>&nbsp;
					<?php } ?>
					
					<input type="checkbox" id="filterPercentile" name="filterColumn[]" value="filterPercentile"/>
					<label for="filterPercentile"><?=$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating']?></label>&nbsp;
				</td>
			</tr>
		</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton_v30' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="js_Reload_DBTable();" />
		<?= $btn_Print ?>
	</div>
	
	<div id="PrintButton"><?php echo $htmlAry['contentTool']?></div>
	
	<div id="PrintArea">
		<div id="DBTableDiv"><?=$h_DBTable?></div>
	</div>
	
	
	<input type="hidden" name="Action" id="Action" value="STUDENT_PROGRESS" />
</form>



<script>
$('.filterAll').change(function(){
	if($(this).attr('checked')){
		$(this).parent().find('input[name="filterColumn\[\]"]').attr('checked','checked');
	}else{
		$(this).parent().find('input[name="filterColumn\[\]"]').removeAttr('checked');
	}
});
$('input[name="filterColumn\[\]"]').click(function(){
	if(!$(this).attr("checked")){
		$(this).parent().find(".filterAll").removeAttr("checked");
	}
});
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>