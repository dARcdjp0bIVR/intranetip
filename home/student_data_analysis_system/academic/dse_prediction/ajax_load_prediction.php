<?php
/*//using: 
 ###################
 ## 2020-01-17 Philips [2020-0114-1414-49206]
 ##  Rename custom_termID into custom_term to avoid removing term name by IntegerSafe
 ##
 ## 2019-02-18 Anna
 ##  added check $_studentPredictionArr, if U grade not count #155506
 ##
 ##	2018-01-16 Omas
 ##	 modified $sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] - hide best 5 score
 ##
 ##	2017-11-03 Anna
 ##	 added SelectedYearID to make prediction according to weight 
 ##
 ## 2017-09-12 Omas
 ##  imp to exclude subject if have set form subject- X120006
 ##
 ## 2017-09-06 Pun
 ##	 fix custom weight delete row and add row will calculate wrong result
 ##
 ##	2017-09-01 Omas
 ##	 add $sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] allow panel to view own subject
 ##
 ## 2017-07-13 Omas
 ##	 fix mock exam will count all student instead of form student when counting rank
 ##
 ## 2017-06-21 Pun
 ##	added save predict record to DB
 ##
 ## 2017-06-20 Pun
 ##	added float header
 ##
 ## 2017-06-14 Omas
 ##	added custom Weighting, ref. year
 ##
 ##	2017-03-10 Omas #Z114381 
 ## if 22N 23N 24N (combined science) hard-code to get 21N's HKEAA subjectCode (A165)
 ##
 ##	2017-03-09 Villa #K114331 
 ##	fix displaying htmlCode/ Wrong File Name
 ##
 ##	2017-03-03 Omas
 ##	fixed H114022
 ##
 ## 2017-02-16 Villa
 ## Generate Export Data for Export Function
 ##
 ## 2017-01-19 Omas
 ## fix best 5 score wrong calculation problem-#M111966
 ###################
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
// include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();
$libpf_exam = new libpf_exam();

$action = $_POST['action'];
$selectForm = $_POST['SelectForm'];
$class = $_POST['Class'];
$report_type = $_POST['report_type'];
$fromAcademicYearID = $_POST['FromAcademicYearID'];
$toAcademicYearID = $_POST['ToAcademicYearID'];
// $fromDseAcademicYearID = $_POST['FromDseAcademicYearID'];
// $toDseAcademicYearID = $_POST['ToDseAcademicYearID'];
$YearTermID = $_POST['YearTermID'];
$targetForm = $_POST['targetForm'];
$display = $_POST['display'];
$filer_type = $_POST['filer_type'];
$conditions = $_POST['conditions'];
$configNumDseData = 3;

$predictMethod = $_POST['predict_method'];
$custom_types = $_POST['custom_type'];
$custom_forms = $_POST['custom_form'];
$custom_termIDs = $_POST['custom_term'];
$custom_weightings = $_POST['weighting'];
$SelectedAcademicYearIDAry = $_POST['SelectedYearID'];
$SelectedAcademicYearIDList = implode(',',$SelectedAcademicYearIDAry);

if($action == 'DSE_prediction'){

    $dseLvArr = array('7'=>'5**','6'=>'5*','5'=>'5','4'=>'4','3'=>'3','2'=>'2','1'=>'1');

    #### Get last saved START ####
    $schema = $libpf_exam->getLastDsePredictSchema();
    if(count($schema)){
        $lastUpdateDetailHTML = Get_Last_Modified_Remark($date=$schema['InputDate'], $byUserName='', $byUserID=$schema['InputBy'], $DisplayText='');
    }else{
        $lastUpdateDetailHTML = '';
    }
    $hasData = (count($schema))?'true':'false';
    #### Get last saved END ####
    
//	if( isset($_POST['FromDseAcademicYearID']) && isset($_POST['ToDseAcademicYearID'])){
		$lpf = new libportfolio();
//		$dseYearArr = $lpf->GetAcademicYears($_POST['FromDseAcademicYearID'], $_POST['ToDseAcademicYearID']);
//		$dseYearArr = array(19,20,21,22,42,49);
		$dseYearArr = $SelectedAcademicYearIDAry;
		$objYear = new form_class_manage();
		$academicYearListInfo = $objYear->Get_Academic_Year_List($dseYearArr, 0);
	
//	}else{
// 		$dseDataYearArr = $libpf_exam->getDseYearWithRecords();
// 		$objYear = new form_class_manage();
// 		$academicYearListInfo = $objYear->Get_Academic_Year_List($dseDataYearArr, 0);
		$SelectedAcademicYearInfo = $objYear->Get_Academic_Year_List($fromAcademicYearID, 0);
// 		unset($objYear);
// 		$dseYearArr = array_splice(Get_Array_By_Key($academicYearListInfo, 'AcademicYearID'), 0, $configNumDseData );
		
// 	}
	$academicYearListInfo = BuildMultiKeyAssoc($academicYearListInfo,'AcademicYearID');
	
	// Calculating DSE percentile
	$yearDsePercentileArr = array();
	$dsePercentileArr = array();
	$dseCount = 0;
	$numDseData = count($dseYearArr);
	$dseSubjectIDArr = array();
	foreach((array)$dseYearArr as $_dseYear){
		
		$dseCount++;
		$previouseDse = $libpf_exam->getExamData( $_dseYear, EXAMID_HKDSE, $subjectID='', '',false, true);
		$dseResultAssoc = BuildMultiKeyAssoc($previouseDse, array('SubjectID','StudentID'), array('Score'), 1, 0);

		foreach((array)$dseResultAssoc as $_subjectID => $_studentResult){
		
			if(!in_array($_subjectID, $dseSubjectIDArr)){
				$dseSubjectIDArr[] = $_subjectID;
			}
			// convert lv to score
			foreach((array)$_studentResult as $__id => $__lv){
				switch($__lv){
					case '5**':
						$__score = '7';
						break;
					case '5*':
						$__score = '6';
						break;
					case 'U':
					case 'X':
						$__score = '0';
						break;
					default:
						$__score = $__lv;
				}
				$_studentResult[$__id] = $__score; 
			}
			arsort($_studentResult);
			$totalNum = count($_studentResult);
			$dseLvCount = array_count_values($_studentResult);
			
			for($i =0 ; $i <=7; $i++){
				if(!isset($dseLvCount[$i])){
					$dseLvCount[$i] = 0;
				}
			}
			krsort($dseLvCount);
			
			
			// calculate each year each lv each subject %
			$count = 0;
			foreach((array)$dseLvCount as $__dseLvScore => $__numStudentGot){
				if(is_numeric($__dseLvScore)){
					$count += $__numStudentGot;
					$yearDsePercentileArr[$_subjectID][$__dseLvScore][$_dseYear] = number_format($count/$totalNum, 4);
				}
			}
		}
		
		
	}

	foreach((array) $yearDsePercentileArr as $_subjectID => $_lvYearPercentArr){
		
		foreach((array)$_lvYearPercentArr as $__dseLvScore => $__yearPercentArr){
			$__numYear = count($__yearPercentArr);

			if($sys_custom['SDAS']['DSE']['prediction']['refWeighting']){
				$sum=0;
				$weightSum=0;
		
				foreach($yearweight as $YearID => $weight){
					if($__yearPercentArr[$YearID] != ''){
						
						$Yearweight = $weight == ''? 0 :$weight;
				
						$weightScore = $__yearPercentArr[$YearID] * $Yearweight;

						$sum = $sum+ $weightScore;
						$weightSum = $weightSum + $Yearweight;						
					}
				}
			
				if($weightSum == 0){
					$dsePercentileArr[$_subjectID][$__dseLvScore]['result'] = array_sum($__yearPercentArr)/$__numYear;
				}else{
					$dsePercentileArr[$_subjectID][$__dseLvScore]['result'] = $sum/$weightSum;
				}
			}else{
				$dsePercentileArr[$_subjectID][$__dseLvScore]['result'] = array_sum($__yearPercentArr)/$__numYear;
			}

		}
	}

	$userIDArr = $libpf_exam->getStudentIDByClassNameAndYear( $fromAcademicYearID ,$Class);

	
	// get whole year student result for calculating the ranking 
	if($report_type == 'mock'){
		$formYearStudentArr = Get_Array_By_Key($libpf_exam->getStudentByAcademicYearIDAndForm($fromAcademicYearID, $SelectForm), 'UserID');
		$schoolExamResultArr = $libpf_exam->getExamData( $fromAcademicYearID, EXAMID_MOCK, $subjectID='', $formYearStudentArr, true);
		$studentResultAssoc = BuildMultiKeyAssoc($schoolExamResultArr, array('SubjectID','StudentID'), array('Score'),1,0);
	}
	else if($report_type == 'int'){
// 		$schoolExamResultArr = $libpf_exam->getStudentSchoolExamAllSubjectResult($targetForm, $userIDArr, $YearTermID);
// #H114022 
// 		$schoolExamResultArr = $libpf_exam->getStudentSchoolExamAllSubjectResult($targetForm, array(), $YearTermID);
		$formYearStudentArr = Get_Array_By_Key($libpf_exam->getStudentByAcademicYearIDAndForm($fromAcademicYearID, $SelectForm), 'UserID');
		$schoolExamResultArr = $libpf_exam->getStudentSchoolExamAllSubjectResult($targetForm, $formYearStudentArr, $YearTermID);
// 		$studentResultAssoc = BuildMultiKeyAssoc($schoolExamResultArr, array('SubjectID','StudentID'), array());
		$studentResultAssoc = BuildMultiKeyAssoc($schoolExamResultArr, array('SubjectID','StudentID'), array('Score'),1,0);
	}
	else if($report_type == 'custom'){
		$formYearStudentArr = Get_Array_By_Key($libpf_exam->getStudentByAcademicYearIDAndForm($fromAcademicYearID, $SelectForm), 'UserID');

		$custom_types = $_POST['custom_type'];
		$custom_forms = $_POST['custom_form'];
		$custom_termIDs = $_POST['custom_term'];
		$weightArr = array();
		foreach((array)$custom_weightings as $_key => $val){
			$weightArr[$_key] = $val/100; 
		}
		
		$subjectIdTemp = array();
		$studentResultAssocTemp = array();
		foreach((array)$custom_types as $_key => $_type){
			
			if($_type == 'int'){
				$_form = $custom_forms[$_key];
				$_term = $custom_termIDs[$_key];
				
				$schoolExamResultArr[$_key] = $libpf_exam->getStudentSchoolExamAllSubjectResult($_form, $formYearStudentArr, $_term);
				$subjectIdTemp = array_merge($subjectIdTemp, Get_Array_By_Key($schoolExamResultArr[$_key], 'SubjectID'));
				$studentResultAssocTemp[$_key] = BuildMultiKeyAssoc($schoolExamResultArr[$_key], array('SubjectID','StudentID'), array('Score'),1,0);
			}else if($_type == 'mock'){
 				$schoolExamResultArr[$_key] = $libpf_exam->getExamData( $fromAcademicYearID, EXAMID_MOCK, $subjectID='', $formYearStudentArr, true);
 				$subjectIdTemp = array_merge($subjectIdTemp, Get_Array_By_Key($schoolExamResultArr[$_key], 'SubjectID'));
				$studentResultAssocTemp[$_key] = BuildMultiKeyAssoc($schoolExamResultArr[$_key], array('SubjectID','StudentID'), array('Score'),1,0);
			}else{
				die();
			}
		}
		
		$subjectIdTemp = array_unique($subjectIdTemp);
		$newStudentResultAssoc = array();
		foreach((array)$formYearStudentArr as $_studentId){
			foreach((array)$subjectIdTemp as $__subjectId){
				$___sum = 0;
				$___weightingSum = 0;
				foreach((array)$custom_types as $___key => $___type){
					if(isset($studentResultAssocTemp[$___key][$__subjectId][$_studentId])){
						$___sum += $studentResultAssocTemp[$___key][$__subjectId][$_studentId] * $weightArr[$___key];
						$___weightingSum += $weightArr[$___key];
					}
				}

				if($___weightingSum > 0){
					$___calculatedScore = number_format($___sum / $___weightingSum, 2 );
					$studentResultAssoc[$__subjectId][$_studentId] = $___calculatedScore;
				}else{
					// do not count this student
					//$___calculatedScore = number_format($___sum);
				}
				
			}
		}
	}else{
		die();
	}
	
	//$studentInfoArr = $libpf_exam->getStudentsInfoByID($userIDArr);
	$studentInfoArr=  $libpf_exam->getStudentsYearInfoByID($userIDArr, $fromAcademicYearID);
	$studentAssoc = BuildMultiKeyAssoc($studentInfoArr, 'UserID', array());
	
// 	$subjectIDArr = array_unique(Get_Array_By_Key($schoolExamResultArr, 'SubjectID'));
// 	sort($subjectIDArr);
	
	// subjectPanel Access right
	if($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] && $isPanel == 1){
		$accessRight = $objSDAS->getAssessmentStatReportAccessRight();
		if(!empty($accessRight['subjectPanel'])){
			$sbjPanelSbjArr = array_keys($accessRight['subjectPanel']);
			$dseSubjectIDArr = $sbjPanelSbjArr;
		}
	}
	// exclude subject if have set form subject # X120006 
	$sql = "SELECT distinct syr.SubjectID 
			from YEAR as y 
			INNER JOIN SUBJECT_YEAR_RELATION as syr on y.YearID = syr.YearID 
			where y.YearName = '{$_POST['SelectForm']}'";
	$seniorFormSubjectIdArr = $libpf_exam->returnVector($sql);
	if(!empty($seniorFormSubjectIdArr) && !empty($dseSubjectIDArr)){
		$dseSubjectIDArr = array_intersect($seniorFormSubjectIdArr, $dseSubjectIDArr);
	}
	$subjectInfoArr = $libpf_exam->getSubjectCodeAndID($dseSubjectIDArr, array(), true, false);
	$subjectAssoc_Detail = BuildMultiKeyAssoc($subjectInfoArr, 'SubjectID');
	$subjectAssoc = BuildMultiKeyAssoc($subjectInfoArr, 'SubjectID', array(Get_Lang_Selection('CH_SNAME', 'EN_SNAME')),1);
	
	// calculate percentile of each student
	$studentPredictArr = array();
	$studentRankArr = array();
	$studentPercentileArr = array();
// 	if($report_type == 'mock'){
	foreach((array)$studentResultAssoc as $_subjectID => $_studentResult){
		arsort($_studentResult);
		$numSatInExam = count($_studentResult);
		// calculating ranking
		$previousScore = 0;
		$rankNow = 0;
		$count = 0;
		foreach((array)$_studentResult as $__studentID => $__score ){
			if($__score > 0){
				$count ++;
				if( $__score != $previousScore){
					$rankNow = $count;
				}
				$__studentPercentile = number_format($rankNow/$numSatInExam,4);
				$__subjectPercentile = $dsePercentileArr[$_subjectID];
				$studentRankArr[$_subjectID][$__studentID] = $rankNow;
				$debugScoreArr[$_subjectID][$__studentID] = $__score;
				$studentPercentileArr[$_subjectID][$__studentID] = $__studentPercentile;
				//if(in_array($__studentID,$userIDArr)){ // For save purpose
					$studentPredictArr[$__studentID][$_subjectID] = $libpf_exam->getPredictionGrade($__studentPercentile, $__subjectPercentile);
				//}
				// for compare with next student
				$previousScore = $__score;
			}
			
		}
	}
// 	if($_SESSION['UserID'] == 1 ){
	//	debug_pr($__score);
	// &&  $__studentID == 1990 && $_subjectID == 25
// 	debug_pr(count($debugScoreArr[25]));
// 	debug_pr($studentRankArr[25]);
// 	debug_pr($debugScoreArr[25]);
// 	debug_pr($studentResultAssoc[25][1990]);
// 	}
// 	}
// 	else if($report_type == 'int'){
// 		foreach((array)$studentResultAssoc as $_subjectID => $_studentResultArr){
// 			foreach((array)$_studentResultArr as $__studentID => $__resultInfoArr ){
// 				$numSatInExam = $__resultInfoArr['OrderMeritFormTotal'];
// 				$rankNow = $__resultInfoArr['OrderMeritForm'];
// 				if($rankNow > 0 && $numSatInExam > 0){
// 					$__studentPercentile = number_format($rankNow/$numSatInExam,4);
// 					$__subjectPercentile = $dsePercentileArr[$_subjectID];
// 					$studentRankArr[$_subjectID][$__studentID] = $rankNow;
// 					$studentPercentileArr[$_subjectID][$__studentID] = $__studentPercentile;
// 					if(in_array($__studentID,$userIDArr)){
// 						$studentPredictArr[$__studentID][$_subjectID] = $libpf_exam->getPredictionGrade($__studentPercentile, $__subjectPercentile);
// 					}
// 				}
// 			}
// 		}
// 	}

	

	######## Save data START ########
	if($isSave){
	
	    $allClassInfo = $libpf_exam->getClassListByAcademicYearIDAndForm($fromAcademicYearID,$selectForm);
	    $yearId = 0;
	    if(count($allClassInfo)){
	        $yearId = $allClassInfo[0]['YearID'];
	    }
	
	    if($report_type == 'mock'){
	        $examType = EXAM_REPORT_TYPE_MOCK;
	        $examDetails = '';
	    }else if($report_type == 'int'){
	        $examType = EXAM_REPORT_TYPE_SCHOOL_EXAM;
	        $examDetails = serialize(array(
	            'form' => $targetForm,
	            'YearTermID' => $YearTermID
	        ));
	    }else{
	        $examType = EXAM_REPORT_TYPE_CUSTOM;
	
	        $customArr = array();
	        foreach($custom_types as $index => $type){
	            if($type == 'mock'){
	                $type = EXAM_REPORT_TYPE_MOCK;
	            }else if($type == 'int'){
	                $type = EXAM_REPORT_TYPE_SCHOOL_EXAM;
	            }
	
	            $customArr[] = array(
	                'type' => $type,
	                'form' => $custom_forms[$index],
	                'YearTermID' => $custom_termIDs[$index],
	                'weight' => $custom_weightings[$index]
	            );
	        }
	        $examDetails = serialize($customArr);
	    }
	
	
	    $saveData = array(
	        'YearID' => $yearId,
/* 	        'FromDseAcademicYearID' => $fromDseAcademicYearID,
	        'ToDseAcademicYearID' => $toDseAcademicYearID, */
	        'ExamType' => $examType,
	        'ExamDetails' => $examDetails,
	        'PredictMethod' => $predictMethod,
	    	'SelectedDseAcademicYearIDs' => $SelectedAcademicYearIDList
	    );
	
	
	    $schemaId = $libpf_exam->saveDsePredictSchema($fromAcademicYearID, $saveData);
	    $saveSuccess = false;
	    if($schemaId){
	        $saveData = array();
	        foreach((array)$subjectAssoc as $_subjectID => $__subjectSName){
	            foreach((array)$studentResultAssoc[$_subjectID] as $__studentID => $__score ){
	                $predictLevel = $studentPredictArr[$__studentID][$_subjectID];
	                $predictLevel = $libpf_exam->getDSELevel($predictLevel);
	                if(is_null($predictLevel)){
	                    continue;
	                }
	
	                $saveData[$_subjectID][$__studentID] = array(
	                    'Score' => $__score,
	                    'PredictGrade' => $predictLevel
	                );
	            }
	        }
	
	        //debug_r($saveData);
	        $saveSuccess = $libpf_exam->saveDsePredictResult($fromAcademicYearID, $schemaId, $saveData);
	    }
	}
	######## Save data END ########
	
	
	
	
	if($filer_type != 'all'){
		$conditionBoolean = ($filer_type == 'yes')? true : false;
		if($filterType == 'bestfive'){
			$bestFiveTotal = $_POST['bestFiveTotal'];
// 			foreach((array)$studentPredictArr as $_studentID => $_subjectResultArr){
			foreach((array)$studentAssoc as $_studentID => $studentInfo){
				$_subjectResultArr = $studentPredictArr[$_studentID];
				if(!empty($_subjectResultArr)){
					$_subjectScoreArr = $libpf_exam->convertDseLevelToScore($_subjectResultArr);
					arsort($_subjectScoreArr);
					array_splice($_subjectScoreArr, 5);
					$thisTotalScore = array_sum($_subjectScoreArr);
					if( $thisTotalScore < $bestFiveTotal){
						$skipStudents[] = $_studentID;
					}
				}
				else{
					$skipStudents[] = $_studentID;
				}
			}
		}
		else if($filterType == 'subject'){
			// filter condition
			$filterSubjectID = $_POST['filterSubjectID'];
			$subjectLevel = $_POST['subjectLevel'];
// 			foreach((array)$studentPredictArr as $_studentID => $_subjectResultArr){
			foreach((array)$studentAssoc as $_studentID => $studentInfo){
				$_subjectResultArr = $studentPredictArr[$_studentID];
				$thisStudentSubjectGrade =  $_subjectResultArr[$filterSubjectID];
				if( ($libpf_exam->getDSEScore($thisStudentSubjectGrade) < $subjectLevel) ){
					$skipStudents[] = $_studentID;
				}
			}
		}
		else{
			$coreSubjectIDAssocArr = BuildMultiKeyAssoc($libpf_exam->getDseCompulsorySubjectID(), 'CODEID',array('SubjectID'),1);
			// filtering of 3322
			// 		$conditions = '4,4,2,2,2';
			$conditionArr = explode(',', $conditions);
			$eConditionArr = explode(',', $elective_conditions);
			// 		$condsNum = count($conditionArr);
			// 		if($condsNum > 4){
			// 			$compulConditionArr = array_splice($conditionArr,0,4);
			// 			$electiveConditionArr =  $conditionArr;
			// 		}
			// 		else{
			$compulConditionArr = $conditionArr;
			$electiveConditionArr = $eConditionArr;
			// 		}
			// prepare compulsory condition checking
			$compulConditionCountArr = array_count_values($compulConditionArr);
			$compulSubjectIDArr = Get_Array_By_Key($libpf_exam->getDseCompulsorySubjectID(), 'SubjectID');
			// initialize electives condition checking
			$electiveConditionCountArr = array_count_values($electiveConditionArr);
				
				
			$skipStudents = array();
			foreach((array)$studentPredictArr as $_studentID => $_subjectResultArr){
				// initialize condition checking Arr
				foreach((array)$compulConditionArr as $_num => $_lvRequired){
					switch($_num){
						case '0':
							// Chi
							$_subjectID = $coreSubjectIDAssocArr['080'];
							break;
						case '1':
							// Eng
							$_subjectID = $coreSubjectIDAssocArr['165'];
							break;
						case '2':
							// Math
							$_subjectID = $coreSubjectIDAssocArr['22S'];
							break;
						case '3':
							// LS
							$_subjectID = $coreSubjectIDAssocArr['265'];
							break;
					}
					$compulCondition[$_subjectID] = $_lvRequired;
				}
				foreach((array)$electiveConditionCountArr as $_lv => $_number){
					$electiveCondition[$_lv] = array();
					for($i=0; $i<$_number; $i++){
						$electiveCondition[$_lv][]  = '';
					}
				}
		
				$_compulsoryScoreArr = array();
				$_electiveScoreArr = array();
				foreach((array)$_subjectResultArr as $__subjectID => $__grade){
					// check is cmp subject , if yes skip it
					if(!$libpf_exam->checkIsCmptSubject($__subjectID)){
						// check is complusory subject
						if(in_array($__subjectID, (array)$compulSubjectIDArr)){
							// check 3322 here
							$_compulsoryScoreArr[$__subjectID] = $libpf_exam->getDSEScore($__grade);
						}
						else{
							// elective
							$_electiveScoreArr[$__subjectID] = $libpf_exam->getDSEScore($__grade);
						}
					}
					else{
						continue;
					}
				}
				arsort($_compulsoryScoreArr);
				arsort($_electiveScoreArr);
				// 20170619 Omas revised foreach loop avoid no score pass the checking
				$meetCoreRequirement = true;
				foreach((array)$compulCondition as $_subjectID => $_requirement){
					$_dseScore = $_compulsoryScoreArr[$_subjectID];
					if(isset($_dseScore) && $_dseScore >= $_requirement){
						continue;
					}
					else{
						$meetCoreRequirement = false;
						break;
					}
				}
				// prepare checking for elective subject requirement
				if(!empty($electiveConditionCountArr)){
					foreach((array)$_electiveScoreArr as $_dseScore){
						foreach((array)$electiveConditionCountArr as $_lv => $_number){
							if(in_array('', (array)$electiveCondition[$_lv] )){
								$firstNotFilledKey = array_search ( '', $electiveCondition[$_lv]);
								$thisLvScore = $libpf_exam->getDSEScore($_lv);
								if($_dseScore >= $thisLvScore){
									$electiveCondition[$_lv][$firstNotFilledKey] = $_dseScore;
									break;
								}
							}
							else{
								continue;
							}
						}
					}
				}
		
				// for debug purpose
				// 		if($_studentID == 5174){
				// 		debug_pr($compulCondition);
				// 		debug_pr($electiveCondition);
				// 		}
				$meetRequirement = true;
				// 				foreach((array)$compulConditionCountArr as $_lv => $_number){
				// 					if(in_array('', $compulCondition[$_lv] )){
				// 						$meetRequirement = false;
				// 					}
				// 				}
				foreach((array)$electiveConditionCountArr as $_lv => $_number){
					if(in_array('', $electiveCondition[$_lv] )){
						$meetRequirement = false;
					}
				}
				if($meetCoreRequirement && $meetRequirement){
					// do nothing
				}
				else{
					$skipStudents[] = $_studentID;
				}
			}
		}
	}
	#WebSAMS Subject Code Mapping
	$WEBSAMS_SubjectCodeMapping = $libpf_exam->getDseWebSAMSSubjectMapping();

	// Individual Student	
	$html = '';
	$html .= '<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">';
	$html .= '<thead>';
	$html .= '<tr>';
	$html .= '<th width="50">'.	$Lang['General']['Class'].'</th>';
	$html .= '<th width="50">'.	$Lang['General']['ClassNo_ShortForm'].'</th>';
	$html .= '<th width="60">'. $Lang['AccountMgmt']['StudentName'].'</th>';
	foreach((array)$subjectAssoc as $_subjectID => $_subjectSName){
		$html .= '<th>'.$_subjectSName.'</th>';
	}
	if( !($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] && $isPanel == 1)){
		// subject panel do not display this column
		$html .= '<th width="40">'.$Lang['SDAS']['DSEprediction']['BestFiveTotal'].'</th>';
	}
	$html .= '</tr>';
	$html .= '</thead>';
	$html .= '<tbody>';
	
	#Export Header
	$ExportHeader[] = 'EXAMCODE';
	$ExportHeader[] = 'examyear';
	$ExportHeader[] = 'classcode';
	$ExportHeader[] = 'classno';
	$ExportHeader[] = 'regno';
	$ExportHeader[] = 'ENNAME';
	$ExportHeader[] = 'sex';
	$ExportHeader[] = 'Subject Code';
	$ExportHeader[] = 'Subject Name';
	$ExportHeader[] = 'Subject Grade';
	if(in_array('score', (array)$display)){
		$ExportHeader[] = 'Subject Score';
	}
	if(in_array('percentile', (array)$display)){
		$ExportHeader[] = 'Subject Percentile';
	}
	$row = 0;
	
	foreach((array)$studentAssoc as $_studentID => $studentInfo){
		if( $filer_type != 'all' && in_array($_studentID, (array)$skipStudents) == $conditionBoolean){
			continue;
		}
		$html .= '<tr>';
		$html .= '<td>'.$studentInfo['ClassName'].'</td>';
		$html .= '<td>'.$studentInfo['ClassNumber'].'</td>';
		$html .= '<td>'.$studentInfo['Name'].'</td>';
		$_studentPredictionArr = array(); 
		foreach((array)$subjectAssoc as $__subjectID => $__subjectSName){
			$__subjectPercentile = $dsePercentileArr[$__subjectID];
			$__studentPercentile = $studentPercentileArr[$__subjectID][$_studentID];
// 			if($report_type == 'mock'){
			$__studentScore = $studentResultAssoc[$__subjectID][$_studentID];
// 			}
// 			else if($report_type == 'int'){
// 				$__studentScore = $studentResultAssoc[$__subjectID][$_studentID]['Score'];
// 			}
			if($__studentPercentile != '' && is_array($__subjectPercentile)){
				$__dsePrediction = $libpf_exam->getPredictionGrade($__studentPercentile, $__subjectPercentile);
				$__cellDisplay = $__dsePrediction;
				if(in_array('score', (array)$display)){
					$__cellDisplay .= '<br>';
					$__cellDisplay .= '('.$__studentScore.')';
				}
				if(in_array('percentile', (array)$display)){
					$__cellDisplay .= '<br>';
					$__cellDisplay .= '('.number_format($__studentPercentile*100, 1).')';
				}
// 				$__cellDisplay = $__dsePrediction.' '.'('.$__studentScore.')';
				// #M111966
				if($__dsePrediction != 'U'){
				    $_studentPredictionArr[$__subjectID] = (string)$__dsePrediction; 
				}
				if(!isset($__summaryArr[$__subjectID][$__dsePrediction])){
					$__summaryArr[$__subjectID][$__dsePrediction] = 0;
				}
				$__summaryArr[$__subjectID][$__dsePrediction] += 1;  
			}
			else{
				$__cellDisplay = Get_String_Display('');
				$__dsePrediction = Get_String_Display('');
				$__studentScore = Get_String_Display('');
			}
			$html .= '<td>'.$__cellDisplay.'</td>';
			
			###Export Data
			if($__cellDisplay!='--'){
				$exportArr[$row][] = 'DSE';
				$exportArr[$row][] = $SelectedAcademicYearInfo[0]['YearNameEN'];
				$exportArr[$row][] = $studentInfo['ClassName'];
				$exportArr[$row][] = $studentInfo['ClassNumber'];
				$exportArr[$row][] = $studentInfo['WebSAMSRegNo'];
				$exportArr[$row][] = $studentInfo['EnglishName'];
				$exportArr[$row][] = $studentInfo['Gender'];
				$__thisSubjectWebsams = $subjectAssoc_Detail[$__subjectID]['CODEID'];
				# Z114381
				if(in_array($__thisSubjectWebsams, array('22N','23N','24N')) ) {
					$__thisSubjectWebsams = '21N';
				}
				$exportArr[$row][] = $WEBSAMS_SubjectCodeMapping[$__thisSubjectWebsams];
				$exportArr[$row][] = $subjectAssoc_Detail[$__subjectID]['main_name'];
				$exportArr[$row][] = $__dsePrediction;
				if(in_array('score', (array)$display)){
					$exportArr[$row][] = $__studentScore;
				}
				if(in_array('percentile', (array)$display)){
					$exportArr[$row][] = $__studentPercentile? number_format($__studentPercentile*100, 1):Get_String_Display('');
				}
				$row++;
			}
		}
		// calculating best 5
// 		debug_pr($_studentPredictionArr);
		arsort($_studentPredictionArr);
		$_studentBest5Arr = array_splice($_studentPredictionArr, 0, 5);
		$_studentBest5Score = $libpf_exam->processDseScore($_studentBest5Arr);
		if( !($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject'] && $isPanel == 1)){
			// subject panel do not display this column
			$html .= '<td>'.$_studentBest5Score.'</td>';
		}
		$html .= '</tr>';
	}
	$html .= '</tbody>';
	$html .= '</table>';
	
	// summary table
	$summary_table .= '<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">';
	$summary_table .= '<thead>';
	$summary_table .= '<tr>';
	$summary_table .= '<th></th>';
	foreach((array)$subjectAssoc as $_subjectID => $_subjectSName){
		$summary_table .= '<th>'.$_subjectSName.'</th>';
	}
	$summary_table .= '<th>'.$Lang['General']['Total'].'</th>';
	$summary_table .= '</tr>';
	$summary_table .= '</thead>';
	$summary_table .= '<tbody>';
	$subjectSumArr = array();
	foreach((array)$dseLvArr as $_key => $_lv){
		$summary_table .= '<tr>';
		$summary_table .= '<td width="67">Level '.$_lv.'</td>';
		$lvTotal = 0;
		foreach((array)$subjectAssoc as $__subjectID => $__subjectSName){
			$__lvNum = $__summaryArr[$__subjectID][$_lv];
			if(isset($__summaryArr[$__subjectID][$_lv])){
				$__lvNum = $__lvNum;
			}
			else{
				$__lvNum = 0;
			}
			$summary_table .= '<td>'.$__lvNum.'</td>';
			$lvTotal += $__lvNum;
			if(!isset($subjectSumArr[$__subjectID])){
				$subjectSumArr[$__subjectID] = 0;
			}
			$subjectSumArr[$__subjectID] += $__lvNum;
		}
		$summary_table .= '<td>'.$lvTotal.'</td>';
		$summary_table .= '</tr>';
	}
	$summary_table .= '<tr>';
		$summary_table .= '<td>'.$Lang['General']['Total'].'</td>';
	foreach((array)$subjectAssoc as $__subjectID => $__subjectSName){
		$summary_table .= '<td>'.$subjectSumArr[$__subjectID].'</td>';
	}
		$summary_table .= '<td>'.Get_String_Display("").'</td>';
	$summary_table .= '</tr>';
	$summary_table .= '</tbody>';
	$summary_table .= '</table>';
	
	$percentile_table = '';
	$percentile_table .= '<table class="common_table_list_v30 view_table_list_v30" style="text-align:center;">';
	$percentile_table .= '<thead>';
	$percentile_table .= '<tr>';
	$percentile_table .= '<th></th>';
	foreach((array)$subjectAssoc as $_subjectID => $_subjectSName){
		$percentile_table .= '<th>'.$_subjectSName.'</th>';
	}
	$percentile_table .= '</tr>';
	$percentile_table .= '</thead>';
	$percentile_table .= '<tbody>';
	foreach((array)$dseLvArr as $_key => $_lv){
		$percentile_table .= '<tr>';
		$percentile_table .= '<td width="67">Level '.$_lv.'</td>';
		$reached100percentArr[$__subjectID] = false;
		foreach((array)$subjectAssoc as $__subjectID => $__subjectSName){
			$__percentile = $dsePercentileArr[$__subjectID][$_key]['result'];
			$__percentile = ($__percentile != '' && !$reached100percent[$__subjectID])? number_format($__percentile*100, 1) : '---';
			// hide more than 1 100.0
			if($__percentile == 100){
				$reached100percent[$__subjectID] = true;
			}
			$percentile_table .= '<td>'.$__percentile.'</td>';
		}
		$percentile_table .= '</tr>';
	}
	$percentile_table .= '</tbody>';
	$percentile_table .= '</table>';
	
	 // remarks
	$remarks_result = $Lang['SDAS']['DSEprediction']['FirstLine'].'<br>';
	if($report_type == 'mock'){
		$examLang = $Lang['SDAS']['DSEprediction']['mock'];
	}
	else if($report_type == 'int'){
		$examLang = $Lang['SDAS']['DSEprediction']['assessment'];
	}
	if(count($display) == 2){
		$remarks_result .= str_replace('<!--type-->', str_replace ('<!--exam-->', $examLang, $Lang['SDAS']['DSEprediction']['scoreRemark']), $Lang['SDAS']['DSEprediction']['SecondLine'] ).'<br>';
		$remarks_result .= str_replace ('<!--exam-->', $examLang, $Lang['SDAS']['DSEprediction']['ThirdLine']).'<br>';
	}
	else if(count($display) == 1){
		if(in_array('score', (array)$display)){
			$remarks_result .= str_replace('<!--type-->', str_replace ('<!--exam-->', $examLang, $Lang['SDAS']['DSEprediction']['scoreRemark']), $Lang['SDAS']['DSEprediction']['SecondLine'] ).'<br>';
		}
		else if(in_array('percentile', (array)$display)){
			$remarks_result .= str_replace('<!--type-->', str_replace ('<!--exam-->', $examLang, $Lang['SDAS']['DSEprediction']['percentileRemark']), $Lang['SDAS']['DSEprediction']['SecondLine'] ).'<br>';
		}
	}
	else{
		$remarks_result = '';
	}
	
// 	if( $_POST['FromDseAcademicYearID'] != $_POST['ToDseAcademicYearID']){
// 		$yearLang = $academicYearListInfo[$_POST['FromDseAcademicYearID']]['YearNameEN'].$Lang['SDAS']['DSEprediction']['to'].$academicYearListInfo[$_POST['ToDseAcademicYearID']]['YearNameEN'];
// 	}else{
// 		$yearLang = $academicYearListInfo[$_POST['FromDseAcademicYearID']]['YearNameEN'];
// 	}
	$YearNameArr=array();
	for($i=0;$i<sizeof($dseYearArr);$i++){
		$YearNameArr[] = $academicYearListInfo[$dseYearArr[$i]]['YearNameEN'];
	}
	$yearLang = implode(',',$YearNameArr);
	
	$remarks_mapping = str_replace('<!--Year-->', $yearLang, $Lang['SDAS']['DSEprediction']['remarks2']);
	
	
    if($isSave){
        if($saveSuccess){
            echo '<div style="text-align: center;">'.$Lang['SDAS']['DSEprediction']['saveEvaluationReportSuccess'].'</div>';
        }else{
            echo '<div style="text-align: center;">'.$Lang['SDAS']['DSEprediction']['saveEvaluationReportUnsuccess'].'</div>';
        }
    }else if(!$isExport){
		#Print the Table
		echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEprediction']['SummaryTable']);
		echo $summary_table;
		echo '<br>';
		echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEprediction']['IndividualStudent']);
		echo $html;
		echo '<span class="tabletextremark">'.$remarks_result .'</span>';
		echo '<br>';
		echo $linterface->GET_NAVIGATION2_IP25($Lang['SDAS']['DSEprediction']['PercentileTable']);
		echo $percentile_table;
		echo '<span class="tabletextremark">'.$remarks_mapping.'</span>';
        echo <<<HTML
        <script>$(function() {
            $('#resultDiv table').floatHeader();
            $('#lastSaved').html('{$lastUpdateDetailHTML}');
            hasData = {$hasData};
        });</script>
HTML;
	}else{
		#For Export Only
		$filename = "dse_prediction.csv";
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $ExportHeader, ",", "\r\n", ",", 0, "11",1);
		$lexport->EXPORT_FILE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="Big5");
// 		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $ExportHeader, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
// 		$lexport->EXPORT_FILE($filename, $export_content);
	}
	//$dseYearArr,$academicYearListInfo
}
?>