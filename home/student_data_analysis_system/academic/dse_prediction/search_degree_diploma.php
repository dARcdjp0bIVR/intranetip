<?php
################# Change Log [Start] ###########
# 2017-12-15 Pun
#	File Create
#
################## Change Log [End] ############

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
$libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
if(!$accessRight['admin']){
	No_Access_Right_Pop_Up('','/');
	exit();
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "dse_prediction";
$CurrentTag = 'DegreeDiploma';
include dirname(__FILE__).'/menu_tags.php';
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
// year info
$aYearAssocArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo() , 'AcademicYearID',array(Get_Lang_Selection('YearNameEN','YearNameB5')),1);
$yearArr = $libpf_exam->getDsePredictResultAcademicYearId();

$yearSelectArr = array();
foreach((array)$aYearAssocArr as $yearID => $yearName){
	if(in_array($yearID, (array)$yearArr)){
		$yearSelectArr[$yearID] = $yearName;
	}
}
$AcademicYearSelection = getSelectByAssoArray($yearSelectArr, "name='AcademicYearID' id='AcademicYearID'", Get_Current_Academic_Year_ID(), 0, 0, "");

//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:void(0)','',array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI Releated END ########



######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_TableExport();
?>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>

<!-- PrintButton Function -->
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};

var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var readyToSubmit = false;

$(document).ready( function() {
	readyToSubmit = true;

	$('#ExportBtn').click(function(){
		var $cloneTable = $("#resultDiv table").clone();
		$("#resultDiv table").find('.exportContent').each(function(){
			var data = $(this).html().trim();
			var $parent = $(this).parent();
			$(this).remove();
			$parent.append(data);
		});

		$("#resultDiv table").find('.exportHide').remove();
		
		$("#resultDiv table").tableExport({
			type: 'csv',
			fileName: 'prediction_evaluation_report'
		});

		$("#resultDiv table").html($cloneTable.html());
	});
});

 
function jsLoadReport()
{
	if(readyToSubmit){
		$( "#PrintBtn" ).hide();
		 $('#ExportBtn').hide();
		if( $('select#AcademicYearID').val() == '' ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}

		if($('#displayDegree:checked').length == 0 && $('#displayDiploma:checked').length == 0){
			$('#displayDegree, #displayDiploma').click();
		}
		
		readyToSubmit = false;
		$('#resultDiv').html(ajaxImage);
		var url = 'index.php?t=academic.dse_prediction.ajax_load_prediction_degree_diploma';
		
		return $.ajax({
			 type:"POST",
			 url: url,
			 data: $("#form1").serialize(),
			 success:function(responseText)
			 {
				 $('#resultDiv').html(responseText);
				 $('#PrintBtn').show();
				 $('#ExportBtn').show();
			 }
		}).done(function(){
			readyToSubmit = true;
		}); 
	}
}	
</script>

<form id="form1" name="form1" method="POST" action="ajax_load_stat.php">

	<?=$html_tab_menu ?>
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top"><?=$AcademicYearSelection?></td>
		</tr>
		<tr style="display:none;">
			<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['DSEprediction']['Display']?></span></td>
			<td valign="top">
				<input type="checkbox" id="displayDegree" name="display[]" value="degree" checked />
				<label for="displayDegree"><?=$Lang['SDAS']['PredictDegreeDiploma']['Degree'] ?></label>
				&nbsp;
				<input type="checkbox" id="displayDiploma" name="display[]" value="diploma" checked />
				<label for="displayDiploma"><?=$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] ?></label>
			</td>
		</tr>
	</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="jsLoadReport();" />
	</div>
	<div><?php echo $linterface->Get_Thickbox_Link('420', '620',"",'1A'."&nbsp;ABC", "jsOnloadDetailThickBox(1)","FakeLayer",$score)?></div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<br clear="both"/>
	
	
</form>
	<div id="PrintArea">
		<div id="resultDiv" class="chart_tables"></div>
	</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>