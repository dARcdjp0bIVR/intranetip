<?php 
/**
 * Change Log:
 * 2020-11-02 Philips [2020-1027-1009-54054]
 *  - Provide Year optgroup for year term selection
 * 2020-01-17 Philips [2020-0114-1414-49206]
 *  - Rename custom_termID into custom_term to avoid removing term name by IntegerSafe
 * 
 * 2017-09-06 Pun
 *  - Fix missing count for weighting, custom_form and custom_termID
 * 2017-09-01 Omas
 *  - add support to term assessment
 */
// $libFCM_ui = new academic_year(Get_Current_Academic_Year_ID());
// $academic_yearterm_arr = $libFCM_ui->Get_Term_List(false,$requestLang);

// $custom_term_selection_html = getSelectByArray($academic_yearterm_arr, "name='custom_termID[]' class='int_sel' style='color:graytext'", "", 0, 0, $ec_iPortfolio['overall_result'], 2);

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();

$termAssessmentArr = $libpf_exam->getTermAndTermAssessment();
$termAssessmentAssoc = array();
$_tmpYearName = '';
$custom_term_selection_html = "<select name='YearTermID' id='YearTermID'>";
$custom_term_selection_html .= "<option value='' >{$ec_iPortfolio['overall_result']}</option>";
foreach((array)$termAssessmentArr as $_termAssessmentInfo){
	$yearName = $_termAssessmentInfo[Get_Lang_Selection('YearNameB5', 'YearNameEN')];
	if($_tmpYearName != $yearName){
		if($_tmpYearName != ''){
			$custom_term_selection_html .= "</optgroup>";
		}
		$custom_term_selection_html .= "<optgroup label='$yearName'>";
		$_termId = $_termAssessmentInfo['YearTermID'];
		$_tmpYearName = $yearName;
	}
	$_termAssessment = $_termAssessmentInfo['TermAssessment'];
	$_key = ($_termAssessment == '')? $_termId : $_termId.'_'.$_termAssessment ;
	$_val = ($_termAssessment == '')? Get_Lang_Selection($_termAssessmentInfo['YearTermNameB5'], $_termAssessmentInfo['YearTermNameEN']) : $_termAssessment ;
	$custom_term_selection_html .= "<option value='$_key'>$_val</option>";
}
$custom_term_selection_html .= "</select>";
// foreach((array)$termAssessmentArr as $_termAssessmentInfo){
//     $_termId = $_termAssessmentInfo['YearTermID'];
//     $_termAssessment = $_termAssessmentInfo['TermAssessment'];
    
//     $_key = ($_termAssessment == '')? $_termId : $_termId.'_'.$_termAssessment ;
//     $_val = ($_termAssessment == '')? Get_Lang_Selection($_termAssessmentInfo['YearTermNameB5'], $_termAssessmentInfo['YearTermNameEN']) : $_termAssessment ;
//     $termAssessmentAssoc[$_key] = $_val;
// }
// $custom_term_selection_html = getSelectByAssoArray($termAssessmentAssoc,"name='custom_term[<!--customTableJS_count-->]' class='int_sel' style='color:graytext'","",0,0,$ec_iPortfolio['overall_result']);
$custom_targetFormSelection = getSelectByAssoArray($Lang['SDAS']['DSEstat']['form'],"name='custom_form[<!--customTableJS_count-->]' class='int_sel' style='color:graytext'",'6',0,1);

$customRowHtml = '';
$customRowHtml .= '<tr id="row<!--customTableJS_count-->">';
$customRowHtml .= '<td width="200">';
$customRowHtml .= '<input type="radio" class="rowTypeRadio" id="row<!--customTableJS_count-->_type_mock" name="custom_type[<!--customTableJS_count-->]" value="mock" checked="checked"/>';
$customRowHtml .= '<label for="row<!--customTableJS_count-->_type_mock">'.$Lang['SDAS']['DSEstat']['Mock'].'</label>&nbsp;<br>';
$customRowHtml .= '<input type="radio" class="rowTypeRadio int_radio" id="row<!--customTableJS_count-->_int_result" name="custom_type[<!--customTableJS_count-->]" value="int"/>';
$customRowHtml .= '<label for="row<!--customTableJS_count-->_int_result">'.$Lang['SDAS']['DSEstat']['Int'].'</label>';
$customRowHtml .= '&nbsp;<div class="intOptions" style="display:inline;">';
$customRowHtml .= $custom_targetFormSelection;
$customRowHtml .= $custom_term_selection_html;
$customRowHtml .= '</div>';
// $customRowHtml .= '<div id="row<!--customTableJS_count-->_formSel" style="display:inline;">'.$custom_targetFormSelection.'</div>';
// $customRowHtml .= '<div id="row<!--customTableJS_count-->_termSel" style="display:inline;">'.$custom_term_selection_html.'</div>';
$customRowHtml .= '</td>';
$customRowHtml .= '<td style="vertical-align:middle;">'.$Lang['SDAS']['Promotion']['Weighting'].':&nbsp;<input name="weighting[<!--customTableJS_count-->]" type="number" min="0" max="100"/>%</td>';
if($_POST['number'] > 0){
	$customRowHtml .= '<td style="vertical-align:middle;">'.($linterface->GET_LNK_DELETE('javascript:void(0);','','','delete_dim customTableDelete')).'</td>';
}
$customRowHtml .= '</tr>';

echo str_replace("<!--customTableJS_count-->",$_POST['number'],$customRowHtml);
?>