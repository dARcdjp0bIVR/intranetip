<?php
/*
 ###################
 ## 2017-12-08 Pun [ip.2.5.9.1.1]
 ## New File
 ###################
 */

include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
$libpf_exam = new libpf_exam();
$json = new JSON_obj();



#### Get current data START ####
$sql = "SELECT 
    * 
FROM 
    EXAM_DSE_PREDICT_SCORE 
WHERE 
    RecordID='{$RecordID}'
AND
    AcademicYearID='{$AcademicYearID}'
AND
    SubjectID='{$SubjectID}'
AND
    StudentID='{$StudentID}'
";
$rs = $libpf_exam->returnResultSet($sql);

if(count($rs) != 1){
    echo 'No record.';
    exit;
}

$rs = current($rs);
#### Get current data END ####

#### Update current data START ####
if($rs['PredictGradeOriginal'] == $DseGrade){
    $newOriginal = 'NULL';
}else if($rs['PredictGradeOriginal'] == '' && $DseGrade != $rs['PredictGrade']){
    $newOriginal = "'{$rs['PredictGrade']}'";
}else{
    $newOriginal = 'PredictGradeOriginal';
}

$sql = "UPDATE 
    EXAM_DSE_PREDICT_SCORE 
SET 
    PredictGrade = '{$DseGrade}', 
    PredictGradeOriginal = {$newOriginal},
    ModifiedDate = NOW(),
    ModifiedBy = '{$_SESSION['UserID']}'
WHERE 
    RecordID='{$RecordID}'";
$result = $libpf_exam->db_db_query($sql);
#### Update current data END ####

#### Get new data START ####
$sql = "SELECT * FROM EXAM_DSE_PREDICT_SCORE WHERE RecordID='{$RecordID}'";
$rs = $libpf_exam->returnResultSet($sql);
$predictScore = current($rs);
#### Get new data END ####

#### Calculate DSE Predict different START ####
if($ActualDseResult){
    $inputScore = $libpf_exam->getDSEScore($DseGrade);
    $actualScore = $libpf_exam->getDSEScore($ActualDseResult);
    $diff = $actualScore - $inputScore;
}else{
    $diff = null;
}
#### Calculate DSE Predict different START ####

#### Return result START ####
$result = array(
    'newGrade' => $predictScore['PredictGrade'],
    'originalGrade' => $predictScore['PredictGradeOriginal'],
    'diffScore' => $diff,
);

echo $json->encode($result);

