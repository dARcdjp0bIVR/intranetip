<?php

$predictionTagName = $Lang['SDAS']['menu']['DsePrediction'];
if($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']
		&& !$accessRight['admin'] == 1
		&& in_array('dse_prediction', $plugin['SDAS_module']['accessRight']['subjectPanel'])
		&& !empty($accessRight['subjectPanel'])
		&& empty($accessRight['classTeacher'])){
	$predictionTagName = $Lang['SDAS']['menu']['DsePrediction']."(".$ec_iPortfolio['subjectPanel_accessRight'].")";
}

$TAGS_OBJ[] = array($predictionTagName,"?t=academic.dse_prediction.search", $CurrentTag == 'DSE');
if($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']
	&& !$accessRight['admin'] == 1
	&& in_array('dse_prediction', $plugin['SDAS_module']['accessRight']['subjectPanel']) 
	&& !empty($accessRight['subjectPanel'])
	&& !empty($accessRight['classTeacher'])){
	$TAGS_OBJ[] = array($Lang['SDAS']['menu']['DsePrediction']."(".$ec_iPortfolio['subjectPanel_accessRight'].")","?t=academic.dse_prediction.search_panel", $CurrentTag == 'DSE_PANEL');
}
$TAGS_OBJ[] = array($Lang['SDAS']['menu']['DsePredictionEvaluation'], "?t=academic.dse_prediction.search2", $CurrentTag == 'Evaluation');
if($accessRight['admin'] && $sys_custom['SDAS']['DSE']['prediction']['degreeDiplomaPrediction']){
    $TAGS_OBJ[] = array($Lang['SDAS']['menu']['DsePredictionDegreeDiploma'], "?t=academic.dse_prediction.search_degree_diploma", $CurrentTag == 'DegreeDiploma');
}