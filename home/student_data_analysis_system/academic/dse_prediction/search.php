<?php
#using by : 
################# Change Log [Start] ###########
#	2020-11-02 Philips [2020-1027-1009-54054]
#	Provide Year optgroup for year term selection
#
#   2019-02-20 Anna
#   fixed table and view button width 
#
#   2019-01-03 Anna
#   fixed IE not support assign function [W154682]
#
# 2018-01-08 Omas
#	hide Confidence Interval (95%)
# 2017-11-03 Anna
#	add $sys_custom['SDAS']['DSE']['prediction']['refWeighting'] 
# 2017-09-01 Omas
#	allow panel to access if set in_array('dse_prediction', $plugin['SDAS_module']['accessRight']['subjectPanel'])
# 2017-06-21 Pun
#	Added evaluation report page
#	Added save predict record button
# 2017-06-20 Pun
#	Added Confidence Interval method, float header
# 2017-06-14 Omas
#	Improved academic year select only show year with mock / school exam
#	added custom Weighting, ref. year 
# 2016-12-09 Villa
#	Fix problem with $sys_custom['SDAS']['allTeacherAccessDSE']
# 2016-12-08 Villa
#	Add class teacher filtering
# 2016-09-19 Villa #K103120
#	Change the input button: Form and Class 
# 2016-09-08 Villa
#  -	Add Print Function
# 2016-06-23 Cara
#	Changed the default value of .ClassSel CheckBox
#
################## Change Log [End] ############

######## Init START ########
include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
include_once($PATH_WRT_ROOT."includes/json.php");
$libpf_exam = new libpf_exam();
$libSDAS = new libSDAS();
$lpf = new libPortfolio();
// $libFCM_ui = new form_class_manage_ui();
$libSCM_ui = new subject_class_mapping_ui();
$json = new JSON_obj();
######## Init END ########


######## Access Right START ########
$accessRight = $lpf->getAssessmentStatReportAccessRight();
$accessRight['realAdmin'] = $accessRight['admin']? 1 : 0;
$subjectIdFilterArr = '';
if(!$accessRight['admin'] && count($accessRight['subjectPanel'])){
	$subjectIdFilterArr = array_keys($accessRight['subjectPanel']);
}
if(!$sys_custom['SDAS']['allTeacherAccessDSE']){
	if(!$accessRight['admin'] && count($accessRight['classTeacher'])){ //not admin and class teacher
		$IsClassTeacher = 1;
		//do the filtering in the ajax 
	// 	$Year_Arr = Get_Array_By_Key($accessRight['classTeacher'],'YearID');
	// 	$Year_Arr = implode(',',$Year_Arr);
	// 	$ClassID_arr = Get_Array_By_Key($accessRight['classTeacher'],'YearClassID');
	// 	$ClassID_arr = implode(',',$ClassID_arr);
	}
}
if(in_array('dse_prediction', (array)$plugin['SDAS_module']['accessRight']['subjectPanel']) && !empty($accessRight['subjectPanel']) && $accessRight['admin'] != 1 ){
    if($sys_custom['SDAS']['DSE']['SubjectPanelOwnSubject']){
    	if(empty($accessRight['classTeacher'])){
    		header("Location: index.php?t=academic.dse_prediction.search_panel");
    		die();
    	}else{
	    	$addTo_TAGS_OBJ =  array($Lang['SDAS']['menu']['DsePrediction']."(".$ec_iPortfolio['subjectPanel_accessRight'].")","?t=academic.dse_prediction.search_panel");
    	}
    }else{
		$accessRight['admin'] = 1;
		$IsClassTeacher = 0;
    }
}
######## Access Right END ########


######## Page Setting START ########
$CurrentPage = "dse_prediction";
$CurrentPageName = $Lang['SDAS']['menu']['DsePrediction'];
$TAGS_OBJ[] = array($CurrentPageName,"",1);
if(isset($addTo_TAGS_OBJ)){
	$TAGS_OBJ[] = $addTo_TAGS_OBJ;
}
$TAGS_OBJ[] = array($Lang['SDAS']['menu']['DsePredictionEvaluation'],"?t=academic.dse_prediction.search2");
if($accessRight['admin'] && $sys_custom['SDAS']['DSE']['prediction']['degreeDiplomaPrediction']){
    $TAGS_OBJ[] = array($Lang['SDAS']['menu']['DsePredictionDegreeDiploma'], "?t=academic.dse_prediction.search_degree_diploma");
}
$MODULE_OBJ = $libSDAS->GET_MODULE_OBJ_ARR();
######## Page Setting END ########


######## UI Releated START ########
// year info
$aYearAssocArr = BuildMultiKeyAssoc(GetAllAcademicYearInfo() , 'AcademicYearID',array(Get_Lang_Selection('YearNameEN','YearNameB5')),1);
$tempYearArr_Mock = $libpf_exam->getExamYearWithRecords(EXAMID_MOCK);
$tempYearArr_SchExam = array_unique(Get_Array_By_Key($libpf_exam->getSchoolExamYearTermWithRecords(), 'AcademicYearID' ));
$yearArr = array_values(array_unique(array_merge((array)$tempYearArr_Mock,(array)$tempYearArr_SchExam)));
$dseYearArr = $libpf_exam->getDseYearWithRecords();

$yearSelectArr = array();
foreach((array)$aYearAssocArr as $yearID => $yearName){
	if(in_array($yearID, (array)$yearArr)){
		$yearSelectArr[$yearID] = $yearName;
	}
	if(in_array($yearID, (array)$dseYearArr)){
		$dseSelectArr[$yearID] = $yearName;
	}
}
$AcademicYearSelection = getSelectByAssoArray($yearSelectArr, "name='FromAcademicYearID' id='FromAcademicYearID'", Get_Current_Academic_Year_ID(), 0, 0, "");

// $FromDseAcademicYearSelection = getSelectByAssoArray($dseSelectArr, "name='FromDseAcademicYearID' id='FromAcademicYearID'", Get_Current_Academic_Year_ID(), 0, 1, "");
// $ToDseAcademicYearSelection = getSelectByAssoArray($dseSelectArr, "name='ToDseAcademicYearID' id='FromAcademicYearID'", Get_Current_Academic_Year_ID(), 0, 1, "");
$dseYear_json = $json->encode($dseSelectArr);

$targetFormSelection = getSelectByAssoArray($Lang['SDAS']['DSEstat']['form'],"name='targetForm' id='targetForm'",'6',0,1);
$custom_targetFormSelection = getSelectByAssoArray($Lang['SDAS']['DSEstat']['form'],"name='custom_form'",'6',0,1);

$class_arr = $libpf_exam->getClassListByYearName(array(4,5,6));
if(!$accessRight['admin']){
	$classTeacherAccess = Get_Array_By_Key($accessRight['classTeacher'], 'ClassTitleEN');
}

$classCheckBox = '';
foreach((array) $class_arr as $_class_info){
// 	$_value = $_class_info['YearClassID'];
	$_value = $_class_info['ClassTitleEN'];
	$formName = $_class_info['formName'];
	
	if(!$accessRight['admin'] && !in_array($_value,$classTeacherAccess) && !$sys_custom['SDAS']['allTeacherAccessDSE']){
		continue;
	}
	$formNameList[] = $formName;
	$_displayLang = Get_Lang_Selection($_class_info['ClassTitleB5'],$_class_info['ClassTitleEN']);
	$_engDisplay = $_class_info['ClassTitleEN'];
	$classCheckBox .= '<input type="checkbox" class="ClassSel" id="class_'.$_engDisplay.'" name="Class[]" value="'.$_value.'" formName="'.$formName.'"/><label for="class_'.$_engDisplay.'">'.$_displayLang.'</label>&nbsp;';
}

// $libFCM_ui = new academic_year(Get_Current_Academic_Year_ID());
// $academic_yearterm_arr = $libFCM_ui->Get_Term_List(false,$requestLang);
// $term_selection_html = getSelectByArray($academic_yearterm_arr, "id='YearTermID' name='YearTermID' disabled", "", 0, 0, $ec_iPortfolio['overall_result'], 2);
// $custom_term_selection_html = getSelectByArray($academic_yearterm_arr, "name='custom_termID[]'", "", 0, 0, $ec_iPortfolio['overall_result'], 2);
$termAssessmentArr = $libpf_exam->getTermAndTermAssessment();
$termAssessmentAssoc = array();
$_tmpYearName = '';
$term_selection_html = "<select name='YearTermID' id='YearTermID'>";
$term_selection_html .= "<option value='' >{$ec_iPortfolio['overall_result']}</option>";
foreach((array)$termAssessmentArr as $_termAssessmentInfo){
	$yearName = $_termAssessmentInfo[Get_Lang_Selection('YearNameB5', 'YearNameEN')];
	if($_tmpYearName != $yearName){
		if($_tmpYearName != ''){
			$term_selection_html .= "</optgroup>";
		}
		$term_selection_html .= "<optgroup label='$yearName'>";
		$_termId = $_termAssessmentInfo['YearTermID'];
	    $_tmpYearName = $yearName;
	}
	$_termAssessment = $_termAssessmentInfo['TermAssessment'];
    $_key = ($_termAssessment == '')? $_termId : $_termId.'_'.$_termAssessment ;
    $_val = ($_termAssessment == '')? Get_Lang_Selection($_termAssessmentInfo['YearTermNameB5'], $_termAssessmentInfo['YearTermNameEN']) : $_termAssessment ;
    $term_selection_html .= "<option value='$_key'>$_val</option>";
}
$term_selection_html .= "</select>";
// foreach((array)$termAssessmentArr as $_termAssessmentInfo){
//     $_termId = $_termAssessmentInfo['YearTermID'];
//     $_termAssessment = $_termAssessmentInfo['TermAssessment'];
    
//     $_key = ($_termAssessment == '')? $_termId : $_termId.'_'.$_termAssessment ;
//     $_val = ($_termAssessment == '')? Get_Lang_Selection($_termAssessmentInfo['YearTermNameB5'], $_termAssessmentInfo['YearTermNameEN']) : $_termAssessment ;
//     $termAssessmentAssoc[$_key] = $_val;
// }
// $term_selection_html = getSelectByAssoArray($termAssessmentAssoc,"name='YearTermID' id='YearTermID'","",0,0,$ec_iPortfolio['overall_result']);

$formNameList = implode(",",array_unique((array)$formNameList));//get a string to js
$methodSelection = '';
$methodSelection .= '<input type="radio" id="predict_method_1" name="predict_method" value="'.EXAM_REPORT_PREDICT_METHOD_PERCENTILE.'" checked /><label for="predict_method_1">'.$Lang['SDAS']['DSEprediction']['Method']['Percentile'].'</label>&nbsp;';
// $methodSelection .= '<input type="radio" id="predict_method_2" name="predict_method" value="'.EXAM_REPORT_PREDICT_METHOD_CONFIDENCE_INTERVAL.'" /><label for="predict_method_2">'.$Lang['SDAS']['DSEprediction']['Method']['ConfidenceInterval'].'</label>';

$formNameList = implode(",",array_unique((array)$formNameList));//get a string to js
$displayCheckBox = '';
$displayCheckBox .= '<input type="checkbox" id="display_score" name="display[]" value="score" /><label for="display_score">'.$Lang['Gamma']['Score'].'</label>&nbsp;';
$displayCheckBox .= '<input type="checkbox" id="display_percentile" name="display[]" value="percentile" /><label id="display_percentile_label" for="display_percentile">'.$Lang['SDAS']['StudentPerformanceTracking']['Percentile'].'</label>';

$filterSubjectSelection = $libSCM_ui->Get_Subject_Selection ( 'filterSubjectID', $filterSubjectID, $OnChange = '', $noFirst = 1, '', '', $OnFocus = '', $FilterSubjectWithoutSG = 0, $IsMultiple = 0, $IncludeSubjectIDArr = $subjectIdFilterArr );
$dseLevelArr = $libpf_exam->getDseLevelArr(true);
$dseLevelSelection = getSelectByAssoArray($dseLevelArr,"name='subjectLevel' id='subjectLevel' class='filterOptions'",'3',0,1);


#### Get last saved START ####
$schema = $libpf_exam->getLastDsePredictSchema();
$hasData = (count($schema))?'true':'false';
#### Get last saved END ####


//printbutton
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$btnAry[] = array('export', 'javascript:goExport()','',array(),' id="ExportBtn" style="display:none;"');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
######## UI Releated END ########


if( !$accessRight['realAdmin'] && in_array('dse_prediction', $plugin['SDAS_module']['accessRight']['subjectPanel']) && !empty($accessRight['subjectPanel'])){
	unset($accessRight['admin']);
}
######## UI START ########
$linterface->LAYOUT_START();
echo $linterface->Include_Thickbox_JS_CSS();
?>
<script language="JavaScript" src="/templates/jquery/jquery.floatheader.min.js"></script>
<script src="/templates/vuejs/vue.min.js"></script>

<!-- PrintButton Function -->
<script>
function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
};
</script>

<script language="JavaScript">
var customTableJS = {
	vrs : {
		rowHTML : '',
		rowCount :0,
		initialized : false,
		objSelector : "#customTable"
	},
	ftn : {
		getRowHTML: function(){
			$.ajax({
	            type:"POST",
// 	            async:false,
	            data : { number : customTableJS.vrs.rowCount },
	            url: '?t=academic.dse_prediction.ajax_custom_table_row',
	            success:function(responseText)
	            {
// 	            	customTableJS.vrs.rowHTML = responseText;

	            	$("table#customTable tr#addRow").before( responseText );
	    			customTableJS.ftn.unbindListener();
	    			customTableJS.ftn.bindListener();
	    			$("div#customTableLoading").hide();
	            }
	     	});
		},
		addRow : function(){
			//var html = customTableJS.ftn.getRowHTML();
			$("div#customTableLoading").show();
			customTableJS.ftn.getRowHTML();
			customTableJS.vrs.rowCount++;
			
		},
		removeRow : function(obj){
			obj.closest('tr').remove();
		},
		bindListener : function(){
			$("a.customTableAdd").each(function() {
				$(this).on("click", function(e) {
					customTableJS.ftn.addRow();
				});
			});
			
			$("a.customTableDelete").each(function() {
				$(this).on("click", function(e) {
					customTableJS.ftn.removeRow($(this));
				});
			});

			$("input.rowTypeRadio").each(function() {
				$(this).on("change", function(e) {
					if($(this).val() == "mock"){
						$(this).parent().children("div.intOptions").children().each(function(){
							$(this).attr("style", "color:graytext");
						});
					}else{
						$(this).parent().children("div.intOptions").children().each(function(){
							$(this).removeAttr("style");
						});
					}
				});
			});

			$("select.int_sel").each(function() {
				$(this).on("click", function(e) {
					if($(this).parent().parent().children(".int_radio:checked") != 1){
						$(this).parent().parent().children(".int_radio").click();
					}
				});
			});
		},
		unbindListener : function(){
			$("a.customTableAdd").each(function() {
				$(this).off("click");
			});
			
			$("a.customTableDelete").each(function() {
				$(this).off("click");
			});

			$("input.rowTypeRadio").each(function() {
				$(this).off("change");
			});
		}
	},
	init : function(){
		customTableJS.vrs.initialized = true;
		customTableJS.ftn.bindListener();
		$("a.customTableAdd").click();
	}
};

var ajaxImage = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var readyToSubmit = false;
var hasData = <?=$hasData?>;

$(document).ready( function() {
	$('#FromAcademicYearID').change(function(){
    	get_class_opt();
    	get_form_opt();
    });

	$( "#int_result" ).click(function() {
	  	if($(this).attr("checked")){
			$('#targetForm').removeAttr('disabled');
			$("select[name=YearTermID]").removeAttr('disabled');
			$(customTableJS.vrs.objSelector).hide();
		}
	});
	$( "#type_mock" ).click(function() {
	  	if($(this).attr("checked")){
			$('#targetForm').attr('disabled','disabled');
			$("select[name=YearTermID]").attr('disabled','disabled');
			$(customTableJS.vrs.objSelector).hide();
		}
	});
	$( "#type_custom" ).click(function() {
	  	if($(this).attr("checked")){
			$('#targetForm').attr('disabled','disabled');
			$("select[name=YearTermID]").attr('disabled','disabled');
			$(customTableJS.vrs.objSelector).show();
			if(!customTableJS.vrs.initialized){
				customTableJS.init();
			}
		}
	});


	$( "#type_all" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterOption(true);
		}
	});

	$( "#type_yes" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterOption(false);
	  		changeFilterType();
		}
	});

	$( "#type_no" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterOption(false);
	  		changeFilterType();
		}
	});

	$( "#filterType_uni" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterUni(false);
	  		disableFilterBestFive(true);
	  		disableFilterSubject(true);
		}
	});
	$( "#filterType_bestfive" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterUni(true);
	  		disableFilterBestFive(false);
	  		disableFilterSubject(true);
		}
	});
	$( "#filterType_subject" ).click(function() {
	  	if($(this).attr("checked")){
	  		disableFilterUni(true);
	  		disableFilterBestFive(true);
	  		disableFilterSubject(false);
		}
	});
    

    get_form_opt();
    $("select#YearTermID").attr('disabled','disabled');
    $('select#targetForm').attr('disabled','disabled');
    readyToSubmit = true;
	disableFilterOption(true);

	$('[name="predict_method"]').change(function(){
		if($(this).val() == '<?=EXAM_REPORT_PREDICT_METHOD_PERCENTILE ?>'){
			$('#container2').show();
			$('#display_percentile, #display_percentile_label').show();
		}else{
			$('#container2').hide();
			$('#display_percentile, #display_percentile_label').hide();
		}
	});


    $('#saveReportBtn').click(function(){
	    if( hasData && !confirm('<?=$Lang['SDAS']['DSEprediction']['ConfirmOverwrite'] ?>') ){
	        return;
        }
	    
		$('#PrintBtn, #ExportBtn, #saveArea').hide();
	    $('#resultDiv').html(ajaxImage);
	    $('[name="isSave"]').val(1);
	    
		var url = '';
		if($('[name="predict_method"]:checked').val() == '<?=EXAM_REPORT_PREDICT_METHOD_PERCENTILE ?>'){
		//	$('#container2').show();
			url = "index.php?t=academic.dse_prediction.ajax_load_prediction";
		}else{
	//		$('#container2').hide();
			url = "index.php?t=academic.dse_prediction.ajax_load_prediction2";
			
		}
		$.ajax({
	           type:"POST",
	           url: url,
	           data: $("#form1").serialize(),
	           success:function(responseText)
	           {
		           $('#resultDiv').html(responseText);
		           //$('#saveDiv').html(responseText);
	           }
	    });
	    $('[name="isSave"]').val(0);
	});
});

function get_class_opt(){
	var form_id = $("#SelectForm").val();
	var ay_id = $("#FromAcademicYearID").val();
	var is_ClassTeacher = '<?=$IsClassTeacher?>';
	var ClassArr = '<?=$ClassID_arr?>';
	  if(form_id == "")
	  {
	    $("#Class").html("<?= $Lang['General']['JS_warning']['SelectAForm']?>");
	  }
	  else
	  {
 		$("#Class").html(ajaxImage);
	  	$.ajax({
	  		type: "GET",
	  		url: "index.php?t=ajax.ajax_get_class_opt",
	  		data: "ay_id="+ay_id+"&form_id="+form_id+"&isClassTeacher="+is_ClassTeacher,
	  		success: function (msg) {
	        $("#Class").html(msg);
	  		}
	  	});
	  }
}

function get_form_opt()
{
	var ay_id = $("#FromAcademicYearID").val();
	var form_id = $("#SelectForm").val();
	var is_ClassTeacher = '<?=$IsClassTeacher?>';
	var Year_Arr = '<?=$Year_Arr?>';
	var formNameList = <?php echo '["'.$formNameList.'"]'?>

  if(ay_id == "")
  {
   // $("#yform_cell").html("<?=$iPort["options_select"]?> <?=$ec_iPortfolio['year']?>");
  }
  else
  {
	  $("#yform_cell").html(ajaxImage);
  	$.ajax({
  		type: "GET",
  		url: "index.php?t=ajax.ajax_get_form_opt",
  		data: "ay_id="+ay_id+"&form_id="+form_id+"&formNameList="+formNameList+"&is_ClassTeacher="+is_ClassTeacher,
  		success: function (msg) {
        $("#yform_cell").html(msg);
  		}
  	});
  }
}
 
function jsLoadReport()
{
	if(readyToSubmit){
		$('#PrintBtn, #ExportBtn, #saveArea').hide();
		if( $('select#FromAcademicYearID').val() == '' ){
			alert('<?=$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear']?>');
			return false;
		}
		if($('input[name="Class\[\]"]:checked').length == 0){
			alert('<?=$Lang['General']['JS_warning']['SelectClasses']?>');
			return false;
		}

		if($('#RefYearSelect').val() == null){
			alert('<?php echo $Lang['SDAS']['DSEprediction']['SelectRefYearWarning']?>');
			return false;
		}
		
		if($('#type_custom').attr("checked")){
			var gradeweight = $( "input[name^='weighting']" );		
			if(gradeweight.length >0){
				for(var i=0;i<gradeweight.length;i++){
					var inputvalue =gradeweight[i].value;
					if(inputvalue==""){
						alert('<?=$Lang['General']['JS_warning']['InputGradeWeight']?>');
						return false;
					}
				}
			}
		}
		
		 <?php if($sys_custom['SDAS']['DSE']['prediction']['refWeighting']){?>
				var DSEyearweight = $( "input[name^='yearweight']" );		
				if(DSEyearweight.length >0){
					for(var i=0;i<DSEyearweight.length;i++){
						var yearinputvalue =DSEyearweight[i].value;
						if(yearinputvalue==""){
							alert('<?=$Lang['General']['JS_warning']['InputYearWeight']?>');
							return false;
						}
					}
				}
		<?php }?>
		readyToSubmit = false;
		$('#resultDiv').html(ajaxImage);
		var url = '';
		if($('[name="predict_method"]:checked').val() == '<?=EXAM_REPORT_PREDICT_METHOD_PERCENTILE ?>'){
			url = "index.php?t=academic.dse_prediction.ajax_load_prediction";
		}else{
			url = "index.php?t=academic.dse_prediction.ajax_load_prediction2";
		}
		
		$.ajax({
	           type:"POST",
	           url: url,
	           data: $("#form1").serialize(),
	           success:function(responseText)
	           {
		           $('#resultDiv').html(responseText);
		           $('#PrintBtn, #ExportBtn, #saveArea').show();
	           }
	    });  
	    readyToSubmit = true; 
	}
}	

function selectAll(){
	if($("#resultType_SelectAll").attr("checked")){
		$(".ClassSel").attr("checked", "checked");
	}else{
		$(".ClassSel").removeAttr("checked");
	}

	
}

function unChecked(){
	if(!$("this").attr("checked")){
		$("#resultType_SelectAll").removeAttr("checked");
	}
	
}

function disableFilterOption(isDisabled){
	if(isDisabled){
		$('.filterOptions').attr('disabled','disabled');
		$('#filterOptionsTable').attr('class', 'tabletextremark');
		$('#filterSubjectID').attr('disabled','disabled');
	}
	else{
		$('.filterOptions').removeAttr('disabled');
		$('#filterOptionsTable').attr('class', '');
		$('#filterSubjectID').removeAttr('disabled');
	}
}
function changeFilterType(){
	if( $( "#filterType_uni" ).attr("checked") ){
		disableFilterUni(false);
  		disableFilterBestFive(true);
  		disableFilterSubject(true);
	}
	else if( $( "#filterType_bestfive" ).attr("checked") ){
		disableFilterUni(true);
  		disableFilterBestFive(false);
  		disableFilterSubject(true);
	}
	else if( $( "#filterType_subject" ).attr("checked") ){
		disableFilterUni(true);
  		disableFilterBestFive(true);
  		disableFilterSubject(false);
	}
}
function disableFilterUni(isDisabled){
	if(isDisabled){
		$('#filterUniTable').hide();
		$('#conditions').attr('disabled','disabled');
		$('#elective_conditions').attr('disabled','disabled');
	}
	else{
		$('#filterUniTable').show();
		$('#conditions').removeAttr('disabled');
		$('#elective_conditions').removeAttr('disabled');
	}
}
function disableFilterBestFive(isDisabled){
	if(isDisabled){
		$('#filterBestfiveTable').hide();
		$('#bestFiveTotal').attr('disabled','disabled');
	}
	else{
		$('#filterBestfiveTable').show();
		$('#bestFiveTotal').removeAttr('disabled');
	}
}
function disableFilterSubject(isDisabled){
	if(isDisabled){
		$('#filterSubjectTable').hide();
		$('#subjectLevel').attr('disabled','disabled');
		$('#filterSubjectID').attr('disabled','disabled');
	}
	else{
		$('#filterSubjectTable').show();
		$('#subjectLevel').removeAttr('disabled');
		$('#filterSubjectID').removeAttr('disabled');
	}
}
function goExport(){
	if($('[name="predict_method"]:checked').val() == '<?=EXAM_REPORT_PREDICT_METHOD_PERCENTILE ?>'){
    	document.form1.action = "index.php?t=academic.dse_prediction.ajax_load_prediction&isExport=1";
	}else{
    	document.form1.action = "index.php?t=academic.dse_prediction.ajax_load_prediction2&isExport=1";
	}
	document.form1.target = "_blank";
	document.form1.method = "post";
	document.form1.submit();
}
</script>

<form id="form1" name="form1" method="POST" action="ajax_load_stat.php">
	<input type="hidden" name="action" value="DSE_prediction" />
	<input type="hidden" name="isSave" value="0" />
	<?=$html_tab_menu ?>
					<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="table-layout:fixed; width:1100px;">
						<tr>
							<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
							<td valign="top"><?=$AcademicYearSelection?></td>
						</tr>
						<tr>
							<td class="field_title"><?=	$Lang['General']['Form']?></td>
							<td id=yform_cell></td>
						</tr>
						<tr id='classRow'>
						<td class="field_title"><span class="tabletext" ><?=$iPort['class']?></span></td> 
						<td id = "Class"><?= $Lang['General']['JS_warning']['SelectAForm']?></td>
						</tr>
						<tr>
							<td class="field_title"><?php echo $Lang['SDAS']['DSEprediction']['refYear']?></td>
							<td>
							<?php
		                     // echo $Lang['General']['From'].' '.$FromDseAcademicYearSelection.' '.$Lang['General']['To'].' '.$ToDseAcademicYearSelection?>
							<div id="selectYearDiv" class="selectYearDiv">
							    <div id="container1">
							        <select v-model="selectedYears" name="SelectedYearID[]" id="RefYearSelect" multiple>
							            <option v-for="(yearname, yearid) in years" :value="yearid">{{yearname}}</option>
							        </select>
							    </div>
							    <?php if($sys_custom['SDAS']['DSE']['prediction']['refWeighting']){?>
								    <div id="container2">
								        <div v-for="(yearid, key) in selectedYears">
								         	{{ yearid | getYearName(years)}} <?=$Lang['SDAS']['Promotion']['Weighting'].': '?><input type="number" :name="'yearweight[' + yearid + ']'"  :id="yearid" min="0" max="100">%
								         
								         </div>
								    </div>
							    <?php }?>
							</div>
							
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletext"><?=$iPort["report_col"]["type"]?></span></td>
							<td valign="top">
								<table width="100%">
									<colgroup>
										<col width="1"/>
										<col width="99%"/>
									</colgroup>
									<tr>
										<td><input type="radio" id="type_mock" name="report_type" value="mock" checked="checked"/></td>
										<td><label for="type_mock"><?=$Lang['SDAS']['DSEstat']['Mock']?></label></td>
									</tr>
									<tr>
										<td><input type="radio" id="int_result" name="report_type" value="int"/></td>
										<td>
											<label for="int_result"><?=$Lang['SDAS']['DSEstat']['Int']?></label>
											<div id="formSel" style="display:inline;"><?echo $targetFormSelection?></div>
											<div id="termSel" style="display:inline;"><?echo $term_selection_html?></div>
										</td>
									</tr>
									<tr>
										<td><input type="radio" id="type_custom" name="report_type" value="custom"/></td>
										<td><label for="type_custom"><?=$Lang['SDAS']['DSEprediction']['customWeighting']?></label></td>
									</tr>
									<tr>
										<td></td>
										<td>
											<table id="customTable" style="display: none;" width="100%">
												<tr id="addRow">
													<td>
													<?php echo $linterface->GET_ACTION_LNK('javascript:void(0);','','','add_dim customTableAdd')?>
													<div id="customTableLoading" style="display:none;"><?php echo $linterface->Get_Ajax_Loading_Image()?></div>
													</td>
													<td></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="display:none;">
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['DSEprediction']['PredictMethod']?></span></td>
							<td valign="top">
							<?=$methodSelection?>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['DSEprediction']['Filter']?></span></td>
							<td valign="top">
								<table width="100%">
									<tr>
										<td colspan="3">
											<input type="radio" id="type_all" name="filer_type" value="all" checked=""/>
											<label for="type_all"><?=	$Lang['General']['NotApplicable'] ?></label>
											<input type="radio" id="type_yes" name="filer_type" value="yes" />
											<label for="type_yes"><?=$Lang['SDAS']['DSEprediction']['Meet']?></label>
											<input type="radio" id="type_no" name="filer_type" value="no" />
											<label for="type_no"><?=$Lang['SDAS']['DSEprediction']['NotMeet']?></label>
										</td>
									</tr>
									<tr id="filterOptionsTable" >
										<td width="33%">
											<table width="100%">
												<tr>
													<td colspan="2" style="text-align: center;">
													<input type="radio" id="filterType_uni" name="filterType" value="uni" class="filterOptions"  checked="checked"/>
													<label for="filterType_uni"><?php echo $Lang['SDAS']['DSEprediction']['FilterType']['UniversityRequire']?></label>
													</td>
												</tr>
												<tr class="filterUniTable">
													<td><?php echo $Lang['SDAS']['DSEprediction']['compulsory']?></td>
													<td><input id="conditions" name="conditions" class="filterOptions" type="text" maxlength="9" value="3,3,2,2"></td>
												</tr>
												<tr class="filterUniTable">
													<td><?php echo $Lang['SDAS']['DSEprediction']['elective']?></td>
													<td><input id="elective_conditions" name="elective_conditions" class="filterOptions" type="text" maxlength="7" value="2"></td>
												</tr>
											</table>
										</td>
										<td width="33%">
											<table width="100%">
												<tr>
													<td style="text-align: center;">
													<input type="radio" id="filterType_bestfive" name="filterType" value="bestfive" class="filterOptions"/>
													<label for="filterType_bestfive"><?php echo $Lang['SDAS']['DSEprediction']['FilterType']['BestFive']?></label>
													</td>
												</tr>
												<tr class="filterBestfiveTable">
													<td style="text-align: center;"><input id="bestFiveTotal" name="bestFiveTotal" class="filterOptions" type="text" maxlength="2" value="20" size="1">
													<?php echo ' '.$Lang['SDAS']['DSEprediction']['DseLevelOrAbove']?>
													</td>
												</tr>
											</table>
										</td>
										<td width="33%">
											<table width="100%">
												<tr>
													<td style="text-align: center;">
													<input type="radio" id="filterType_subject" name="filterType" value="subject" class="filterOptions"/>
													<label for="filterType_subject"><?php echo $Lang['SDAS']['DSEprediction']['FilterType']['Subject']?></label>
													</td>
												</tr>
												<tr class="filterSubjectTable">
													<td><?php echo $filterSubjectSelection?></td>
												</tr>
												<tr class="filterSubjectTable">
													<td style="text-align: center;"><?php echo $dseLevelSelection.' '.$Lang['SDAS']['DSEprediction']['DseLevelOrAbove'] ?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="field_title"><span class="tabletext"><?=	$Lang['SDAS']['DSEprediction']['Display']?></span></td>
							<td valign="top">
							<?=$displayCheckBox?>
							</td>
						</tr>
					</table>
	
	<span class="tabletextremark"></span>
			<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="table-layout:fixed; width:1100px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="jsLoadReport();" />
	</div>
	<div><?php echo $linterface->Get_Thickbox_Link('420', '620',"",'1A'."&nbsp;ABC", "jsOnloadDetailThickBox(1)","FakeLayer",$score)?></div>
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	
	<br clear="both"/>
	
	
</form>
	<div id="PrintArea">
		<div id="resultDiv" class="chart_tables"></div>
	</div>
	
	<?php if($accessRight['admin']){ ?>
    	<div id="saveArea" style="display:none;margin-top:10px;">
        	<p class="spacer"></p>
        	<div class="edit_bottom_v30">
        		<input type="button" id="saveReportBtn" value="<?=$Lang['SDAS']['DSEprediction']['saveEvaluationReport'] ?>" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" />
            	<div id="lastSaved"></div>
            </div>
            <div id="saveDiv"></div>
        </div>
    <?php } ?>
	
	<script>

//	var dseYear = <?=$dseYear_json?>;	
//	var dseSelectYearID = <?=$dseSelectYearIDAry_json?>;
// 	var i=0;
// 	var year = [];
//     for (var key in dseYear) {         
//  		var resetKey = dseSelectYearID[i];
//  		var val = dseYear[resetKey];
//     	year[resetKey] = val;
//     	i++;
//     }
//     console.log(year);
// var dseYear = <?=$dseYear_jsontest?>;

// 	dseYearIDAry = [];	
//  	dseYearNameAry = []; 
//  	var test = [];
// 	var i=0;
//     for (var key in dseYear) { 
//     	if(Number.isInteger(parseInt(key))){
        	
//     	dseYearInfo = dseYear[key];
//     	dseYearID = dseYearInfo.AcademicYearID;
//     	dseYearIDAry.push(dseYearID);
//     	dseYearName  = dseYearInfo.AcademicYearName;
//     	dseYearNameAry.push(dseYearName);

//     	test[dseYearID] = dseYearName;
//     	i++;
//     	}
//     }

//     var __DATAtest__ = {'yearid':dseYearIDAry,'yearname':dseYearNameAry};

	
	var __DATA__ = {
	    'years':<?=$dseYear_json?> 
	};

// added for IE not support assign function
	if (typeof Object.assign != 'function') {
	  // Must be writable: true, enumerable: false, configurable: true
	  Object.defineProperty(Object, "assign", {
	    value: function assign(target, varArgs) { // .length of function is 2
	      'use strict';
	      if (target == null) { // TypeError if undefined or null
	        throw new TypeError('Cannot convert undefined or null to object');
	      }

	      var to = Object(target);

	      for (var index = 1; index < arguments.length; index++) {
	        var nextSource = arguments[index];

	        if (nextSource != null) { // Skip over if undefined or null
	          for (var nextKey in nextSource) {
	            // Avoid bugs when hasOwnProperty is shadowed
	            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
	              to[nextKey] = nextSource[nextKey];
	            }
	          }
	        }
	      }
	      return to;
	    }
	  });
	}

window.year = new Vue({
	    el: '#selectYearDiv',
	    data: function () {
	        return Object.assign(
	            {},
	            {selectedYears: []},
	            __DATA__
	        )
	    },
	    filters: {
	        getYearName: function (value, year) {
	       //     console.log(year);
	            return year[value];
	        }
	    }
	});



</script>
	
	
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>