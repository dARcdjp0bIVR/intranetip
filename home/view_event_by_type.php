<?php
// Editing by 

/*
 * 2018-08-08 (Henry): fixed sql injection 
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/lib.php");

intranet_auth();
intranet_opendb();

$li = new libcalevent($ts,$v);


$i_EventType = $i_EventTypeString[$tp];

$order = ($order == 1? 1 : 0);

if ($order == 0)
{
    $another = 1;
}
else
{
    $another = 0;
}
        
$academic_year_start_month = $academic_year_start_month ? $academic_year_start_month-1 : 8;
$sql = "select 
	distinct if(month(EventDate)>". $academic_year_start_month.", year(EventDate), year(EventDate)-1) as academicYear
from 
	INTRANET_EVENT
where         
	RecordStatus = '1' AND RecordType = $tp
order by academicYear desc
";
$result = $li->returnVector($sql,1);

/*
## academic year
$thisAcademicYear = GET_ACADEMIC_YEAR2();
list($CurrentStartYear, $CurrentEndYear, $s, $l) = $thisAcademicYear;

if($StartYear=="")
{
	if(!in_array($CurrentStartYear, $result))
	{
		$StartYear = $result[0];
        $EndYear = $StartYear + 1;
	}
}
else
{
	$EndYear 	= $StartYear + 1;
}
//check current academic in ther result or not
if(!in_array($CurrentStartYear, $result) && $CurrentStartYear != "")
{
	array_unshift($result, $CurrentStartYear);
}
*/

/*	
//select academic year display format
$name = $result;
for($i=0;$i<sizeof($result);$i++)
{
	if($s)
		$name[$i] = substr($result[$i], strlen($result[$i])-$l, $l) . $s . substr($result[$i]+1, strlen($result[$i]+1)-$l, $l);
	else
        $name[$i] = substr($result[$i], strlen($result[$i])-$l, $l);
}        
$select_academicYear = getSelectByValueDiffName($result,$name,"name=\"StartYear\" onChange=\"document.form1.submit()\"",$StartYear, 0, 1);
*/

$AcademicYearID = IntegerSafe($AcademicYearID);
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$select_academicYear = getSelectAcademicYear("AcademicYearID", "onChange=\"document.form1.submit()\"","","",$AcademicYearID);

$MODULE_OBJ['title'] = $i_Events;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function changeOrder()
{
	with(document.form1)
        {
        	order.value = <?=$another?>;
                submit();
        }
}
//-->
</script>

<table width='100%' border='0' cellspacing='0' cellpadding='10'>
<form name="form1" method="get">
<tr>
	<td><?=$li->displayEventByType($tp,$order,$AcademicYearID);?></td>
</tr>

<input type="hidden" name="MiddlePos" value="<?=$MiddlePos?>">
<input type="hidden" name="s" value="<?=$s?>">
<input type="hidden" name="l" value="<?=$l?>">
<input type="hidden" name="tp" value="<?=$tp?>">
<input type="hidden" name="order" value="<?=$order?>">
</form>
</table>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>