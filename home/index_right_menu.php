<?
// Using: 

################ Change Log [Start] #####################
#
#	Date:	2018-02-21 (Bill)   [2017-0901-1525-31265]
#           - allow Student & Parent to access eHomework tab ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_view_access"])
#
################ Change Log [End] #####################

# check if there's any itextbook
include_once($PATH_WRT_ROOT."includes/libportal.php");
$lportal = new libportal();
$has_itextbook = $lportal->hasiTextbookClass() && count($_SESSION['iTextbook'])>0;
//$has_itextbook = false;

if($sys_custom['portal']['hkugac']){
	return include_once("index_right_menu_hkugac.php");
	exit;
}

// [2017-0901-1525-31265]
$rightMenuHomeworkAccess = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_view_access"];
if ($rightMenuHomeworkAccess && $ListType == 1 && !($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']))		# set Homework List as default
{
	$ListMenu = "   <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_on_01.gif\" width=\"14\" height=\"18\"></td>
							<td height=\"18\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_on_02.gif\" class=\"indextabon\">{$i_Homework_list}</td>
							<td width=\"18\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_on_03.gif\" width=\"18\" height=\"18\"></td>
						</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_on_04.gif\" width=\"14\" height=\"22\"></td>
							<td height=\"22\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_on_05.gif\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							<tr>
								<td width=\"12\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_01.gif\" width=\"12\" height=\"22\"></td>
								<td width=\"90\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_COMMUNITY(document.ListForm)\" class=\"indextaboff\">{$i_Community_Communities}</a></td>
								<td width=\"12\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_01.gif\" width=\"12\" height=\"22\"></td>
								<td width=\"80\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_ECLASS(document.ListForm)\" class=\"indextaboff\">{$i_adminmenu_eclass}</a></td>";
    
	if($has_itextbook) {
		$ListMenu .= "<td width=\"12\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\" width=\"12\" height=\"22\"></td>";
	}
	else {
		$ListMenu .= "<td width=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_03.gif\" width=\"10\" height=\"22\"></td>";
	}
	
	if($has_itextbook)
	{
		$ListMenu .= "<td width=\"80\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_TEXTBOOK(document.ListForm)\" class=\"indextaboff\">{$Lang['itextbook']['itextbook']}</a></td>
					  <td width=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_03.gif\" width=\"10\" height=\"22\"></td>";
	}
	$ListMenu .= "          </tr>
						    </table>
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table> ";
}
else if ($ListType == 2)	# set Groups List as default
{
	$eclass_width = ($intranet_session_language=='en')?100:65; 
	$textbook_width = ($intranet_session_language=='en')?100:60;
	$hw_width = ($intranet_session_language=='en')?500:70;
	
	$ListMenu = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_on_01.gif\" width=\"14\" height=\"18\"></td>
							<td height=\"18\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_on_02.gif\" class=\"indextabon\">{$i_Community_Communities}</td>
							<td width=\"18\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_on_03.gif\" width=\"18\" height=\"18\"></td>
						</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_on_04.gif\" width=\"14\" height=\"22\"></td>
							<td height=\"22\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_on_05.gif\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							<tr>
								<td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_01.gif\" width=\"5\" height=\"22\"></td>
								<td width=\"".$eclass_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_ECLASS(document.ListForm)\" class=\"indextaboff\">{$i_adminmenu_eclass}</a></td> ";
	
	if($rightMenuHomeworkAccess && !($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']))
	{
		if($has_itextbook)
		{
            $ListMenu .= "<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
		    $ListMenu .= "<td width=\"".$textbook_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_TEXTBOOK(document.ListForm)\" class=\"indextaboff\">{$Lang['itextbook']['itextbook']}</a></td>";
			$ListMenu .= "<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
		}
		else
		{
			$ListMenu .= "<td width=\"12\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" width=\"12\" height=\"22\"></td>";
		}
		
		$ListMenu .= "   <td width=\"".$hw_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_HOMEWORK(document.ListForm)\" class=\"indextaboff\">{$i_Homework_list}</a></td>
		                 <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_03.gif\" width=\"5\" height=\"22\"></td> ";
	}
	else
	{
		$ListMenu .= "   <td width=\"12\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_03.gif\"></td>";
	}
	$ListMenu .= "</tr>
							</table>
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table> ";
}
else if ($ListType == 3)	# set eClass List as default
{
	$textbook_width = ($intranet_session_language=='en')?65:65;
	$hw_width = ($intranet_session_language=='en')?145:70;
	
	$ListMenu = "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_on_01.gif\" width=\"14\" height=\"18\"></td>
							<td height=\"18\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_on_02.gif\" class=\"indextabon\">{$i_adminmenu_eclass}</td>
							<td width=\"18\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_on_03.gif\" width=\"18\" height=\"18\"></td>
							
						</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_on_04.gif\" width=\"14\" height=\"22\"></td>
							<td height=\"22\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_on_05.gif\">
							<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							<tr> ";
    if($has_itextbook) {
        $ListMenu .= "  <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_01.gif\" width=\"5\" height=\"22\"></td>
					    <td width=\"".$textbook_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_TEXTBOOK(document.ListForm)\" class=\"indextaboff\">{$Lang['itextbook']['itextbook']}</a></td> ";							
    }
	
	if($rightMenuHomeworkAccess && !($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']))
	{
		if($has_itextbook) {
			$ListMenu .= "<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
		}
		else {
			$ListMenu .= "<td width=\"12\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" ></td>";
		}
		
		$ListMenu .= "    <td width=\"".$hw_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_HOMEWORK(document.ListForm)\" class=\"indextaboff\">{$i_Homework_list}</a></td>
		                  <td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
	}
	else
	{
		$ListMenu .= "    <td width=\"12\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_01.gif\" ></td>";
	}
	
	if (!$plugin['DisableeCommunity'])
	{
		$ListMenu .= "    <td width=\"50\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_COMMUNITY(document.ListForm)\" class=\"indextaboff\">{$i_Community_Communities}</a></td>";
	}
	$ListMenu .= "        <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_03.gif\" width=\"5\" height=\"22\"></td>";
	$ListMenu .= "     </tr>
					   </table>
					   </td>
					</tr>
					</table>
					</td>
				</tr>
				</table> ";
}
else if ($ListType == 4)	# set iTextbook List as default
{
	$hw_width = ($intranet_session_language=='en')?170:70; 
	$ListMenu = "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_on_01.gif\" width=\"14\" height=\"18\"></td>
							<td height=\"18\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_on_02.gif\" class=\"indextabon\">{$Lang['itextbook']['itextbook']}</td>
							<td width=\"18\" height=\"18\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_on_03.gif\" width=\"18\" height=\"18\"></td>
							
						</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"14\" height=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_on_04.gif\" width=\"14\" height=\"22\"></td>
							<td height=\"22\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/lakeblue_tab_on_05.gif\">
							<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							<tr> ";
    
	if($rightMenuHomeworkAccess && !($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']))
	{
		$ListMenu .= "      <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" ></td>";
		$ListMenu .= "      <td width=\"".$hw_width."\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_HOMEWORK(document.ListForm)\" class=\"indextaboff\">{$i_Homework_list}</a></td>
							<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
	}
	else
	{
		$ListMenu .= "      <td width=\"12\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_01.gif\" ></td>";
	}
    $ListMenu .= "          <td width=\"40\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_COMMUNITY(document.ListForm)\" class=\"indextaboff\">{$i_Community_Communities}</a></td>";
	$ListMenu .= "          <td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/orange_tab_off_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/green_tab_off_01.gif\" width=\"5\" height=\"22\"></td>";
    $ListMenu .= "";
	
	$ListMenu .= "          <td width=\"50\" align=\"center\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_02.gif\"><a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_ECLASS(document.ListForm)\" class=\"indextaboff\">{$i_adminmenu_eclass}</a></td>
						    <td width=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/blue_tab_off_03.gif\" width=\"5\" height=\"22\"></td>";
	$ListMenu.= "           </tr>
							</table>
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table> ";
}
?>