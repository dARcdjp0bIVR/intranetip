<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$StudentID = (array)$_REQUEST['StudentID'];
$Format = $_REQUEST['Format'];

$invalid_access = false;
$lu = new libuser($_SESSION['UserID']);

if($StudentID[0] != $_SESSION['UserID'])
{
	if($lu->isParent()){
		$childrenIdAry = $lu->getChildren();
		$StudentIDAry = $StudentID;
		for($i=0;$i<count($StudentIDAry);$i++){
			if(!in_array($StudentIDAry[$i],$childrenIdAry)){
				$invalid_access = true;
			}
		}
	}else{
		$invalid_access = true;
	}
}
if($invalid_access){
	No_Access_Right_Pop_Up();
}

$POS_UI = new libpos_ui();

if($Format=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $POS_UI->Get_Health_Personal_Report($StartDate,$EndDate,$StudentID,$Format);
if($Format=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>