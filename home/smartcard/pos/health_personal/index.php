<?php
// kenneth chung

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
$libsmartcard = new libsmartcard();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ReportHealthPersonal'] = 1;	// Left Menu

$linterface = new interface_html();

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['HealthPersonalReport'], '', 0);

$MODULE_OBJ = $libsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $libpos_ui->Get_Health_Personal_Report_Layout(true);

?>
<script language="javascript">
/*
function Get_Student_List(ClassID) {
	if (ClassID != "") {
		var PostVar = {"ClassID":ClassID};
		
		$('#StudentLayer').load("ajax_get_student_selection.php",PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = "/";
				}
				else {
					$('#StudentRow').css('display','');
					$('#SubmitCell').css('display','');
				}
			});
	}
	else {
		$('#SubmitCell').css('display','none');
		$('#StudentRow').css('display','none');
	}
}
*/

function Get_Health_Personal_Report() {
	var StudentIDs = document.getElementsByName('StudentID[]');
	var PostStudentId = new Array(StudentIDs.length);
	
	if (StudentIDs.length > 0 && document.getElementById('DPWL-StartDate').innerHTML == "" && document.getElementById('DPWL-EndDate').innerHTML == "") {
		$('#StudentWarningLayer').html('');
		
		for (var i=0; i< StudentIDs.length; i++) {
			PostStudentId[i] = StudentIDs[i].value;
		}
		
		var PostVar = {
			"StudentID[]":PostStudentId,
			"StartDate":$('#StartDate').val(),
			"EndDate":$('#EndDate').val(),
			"Format":"WEB"
		};
		
		Block_Element("ReportLayer");
		$('#ReportLayer').load("ajax_get_personal_health.php",PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = "/";
				}
				
				UnBlock_Element("ReportLayer");
			});
	}
	else {
		if (StudentIDs.length == 0) {
			$('#StudentWarningLayer').html('<?=$Lang['ePOS']['SelectAtLeast1StudentWarning']?>');
		}
	}
}

function PrintPage(obj) {
	old_url = obj.action;
	old_method = obj.method;
	obj.action="ajax_get_personal_health.php?Format=PRINT";
	obj.method = "POST";
	obj.target = "_blank";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
	obj.target = "";
}
				   
function ExportPage(obj) {
	old_url = obj.action;
	old_method = obj.method;
	obj.action="ajax_get_personal_health.php?Format=EXPORT";
	obj.method = "POST";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
