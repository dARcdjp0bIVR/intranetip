<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $hasRight = false;
}
else if ($lu->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     $linkedChildren = $lfamily->returnChildrens($UserID);
     if (!in_array($studentid,$linkedChildren))
     {
          header("Location: $intranet_httppath/");
          exit();
     }
     if (!in_array($target,$linkedChildren))
     {
          header("Location: $intranet_httppath/");
          exit();
     }
     if ($studentid == "")
     {
         $hasRight = false;
     }
}
else # Student
{
     $hasRight = false;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}

if (!is_numeric($amount))
{
     header("Location: payment_transfer.php?studentid=$studentid&error=1");
     exit();
}

$lpayment = new libpayment();
$studentid = IntegerSafe($studentid);
$temp = $lpayment->checkBalance($studentid);
list($balance, $lastupdated) = $temp;

if ($balance < $amount)
{
     header("Location: payment_transfer.php?studentid=$studentid&error=2");
     exit();
}

$success = $lpayment->account_transfer($studentid,$target,$amount);
if ($success)
{
    header("Location: paymentinfo.php?studentid=$target&transferred=1");
}
else
{
    header("Location: payment_transfer.php?studentid=$studentid&error=3");
}

intranet_closedb();
?>
