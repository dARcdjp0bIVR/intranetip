<?php
// Editing by 
/*
 * 2015-03-04 (Carlos): [ip2.5.6.3.1] Display PAYMENT_OVERALL_TRANSACTION_LOG.TransactionDetails if not null(summary of purchased items), instead of PAYMENT_OVERALL_TRANSACTION_LOG.Details
 * 2013-11-04 (Carlos): Transaction Type display [Library Payment] or [Purchase] depends on Details
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if(!$plugin['payment']){
    header("Location: $intranet_httppath/");	
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$lu 		= new libuser($UserID);
$CurrentPage	= "PagePaymentBalance";
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();

$hasRight = true;
if ($lu->RecordType == 3)  # Parent
{
	$lfamily = new libfamily();
        
	if ($studentid == "")
     	{
		$studentid = $lfamily->returnFirstChild($UserID);
		$linkedChildren = $lfamily->returnChildrens($UserID);
	}
	else
	{
		$linkedChildren = $lfamily->returnChildrens($UserID);
		if (!in_array($studentid,$linkedChildren))
		{
			header("Location: $intranet_httppath/");
			exit();
		}
	
        }
        
	if ($studentid == "")
	{
		$hasRight = false;
	}
        else
        {
		$selection = $lfamily->getSelectChildren($UserID,"name=studentid onChange=\"this.form.submit();\"",$studentid);
        }
}
else # Student / Teacher
{
       $studentid = $UserID;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}
$escaped_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
if ($hasRight)
{
        if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
        $pageSizeChangeEnabled = true;
        
        $order = ($order == 1) ? 1 : 0;
        if ($field == "") $field = 0;
        
        /*
        $sql  = "SELECT
        		1,
                       DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
                       IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
                       CASE a.TransactionType
                            WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                            WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                            WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
                            WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                            WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                            WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                            WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                            ELSE '$i_Payment_TransactionType_Other' END,
                       IF(a.TransactionType IN (1,5,6),CONCAT('$',FORMAT(a.Amount,1)),'&nbsp;'),
                       IF(a.TransactionType IN (2,3,4,7),CONCAT('$',FORMAT(a.Amount,1)),'&nbsp;'),
                       a.Details,CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
                       a.RefCode
                 FROM
                     PAYMENT_OVERALL_TRANSACTION_LOG as a
                       LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
                 WHERE
                      a.StudentID = $studentid              AND (
                           a.TransactionTime LIKE '%$keyword%' OR
                           a.Details LIKE '%$keyword%' OR
                           CONCAT('$',a.Amount) LIKE '%$keyword%'
                      ) 
                        ";
		*/
		//IF(a.TransactionType=3 AND SUBSTRING(a.RefCode,1,4)='VITA',
		
		$library_payment_words = explode(" / ",$Lang['ePayment']['LibraryPayment']);
		$library_payment = Get_Lang_Selection($library_payment_words[0],$library_payment_words[1]);
		
		$sql  = "SELECT
				1,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
               CASE a.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN IF(a.Details='".$Lang['ePayment']['LibraryPayment']."','$library_payment','$i_Payment_TransactionType_Purchase')
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEN '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END,
               IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),CONCAT('$',FORMAT(a.Amount,1)),'&nbsp;'),
               IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),CONCAT('$',FORMAT(a.Amount,1)),'&nbsp;'),
               IF(a.TransactionDetails IS NOT NULL,a.TransactionDetails,IF(a.Details = 'Transaction Void Refund', '".$Lang['ePOS']['TransactionDetails']['Refund']."',a.Details)) as Details,CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
               CONCAT(a.RefCode,
                    IF(a.TransactionType=3 AND SUBSTRING(a.RefCode,1,4)='VITA',
                      CONCAT(' <a href=\"#ToolMenu\" onClick=\"javascript:retrievePurchaseDetails(event,',a.LogID,')\"><img border=0 src=\"$image_path/icon_alt.gif\"></a>')
                      , IF(a.TransactionType=3 AND SUBSTRING(a.RefCode,1,5)='SALES',CONCAT(' <a href=\"#ToolMenu\" onClick=\"javascript:retrievePurchaseDetails(event,',a.LogID,')\"><img border=0 src=\"$image_path/icon_alt.gif\"></a>'),'') )
                      )
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a
               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID

         WHERE
              a.StudentID = '".$lpayment->Get_Safe_Sql_Query($studentid)."'   AND (
                   a.TransactionTime LIKE '%$escaped_keyword%' OR
                   a.Details LIKE '%$escaped_keyword%' OR
                   CONCAT('$',a.Amount) LIKE '%$escaped_keyword%'
              )
                ";

        # TABLE INFO
        $li = new libdbtable2007($field, $order, $pageNo);
        $li->field_array = array("a.TransactionTime");
        $li->sql = $sql;
        $li->no_col = sizeof($li->field_array)+8;
        $li->IsColOff = "SmartCardPaymentBalance";
        $li->title = "";
		$li->fieldorder2 = " , a.LogID " . (($li->order==0) ? " DESC" : " ASC");
		
        // TABLE COLUMN
        $pos = 0;
        $li->column_list .= "<td width=1 class='tablebluetop tabletopnolink'>#</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletopnolink'>".$i_Payment_Field_TransactionFileTime."</td>\n";
        $li->column_list .= "<td width=10% class='tablebluetop tabletopnolink'>".$i_Payment_Field_TransactionType."</td>\n";
        $li->column_list .= "<td width=8% class='tablebluetop tabletopnolink'>".$i_Payment_TransactionType_Credit."</td>\n";
        $li->column_list .= "<td width=8% class='tablebluetop tabletopnolink'>".$i_Payment_TransactionType_Debit."</td>\n";
        $li->column_list .= "<td width=16% class='tablebluetop tabletopnolink'>".$i_Payment_Field_TransactionDetail."</td>\n";
        $li->column_list .= "<td width=8% class='tablebluetop tabletopnolink'>".$i_Payment_Field_Balance."</td>\n";
        $li->column_list .= "<td width=10% class='tablebluetop tabletopnolink'>".$i_Payment_Field_RefCode."</td>\n";

        ### Title ###
        $MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
        $TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_accountbalance.gif' align='absmiddle' />";
        $TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_Payment_AccountBalance ."</span>";
        $TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
        $TAGS_OBJ[] = array($TitleTitle, "", 0);
        
        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }
        
        ### Button
        $searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
        $searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".escape_double_quotes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
        $searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
		$searchTag 	.= "</tr></table>";
        
        
        $linterface->LAYOUT_START();             
?>
<script language=JavaScript src=/templates/tooltip.js></script>
<script language=JavaScript src=/templates/ajax_yahoo.js></script>
<script language=JavaScript src=/templates/ajax_connection.js></script>
<style type="text/css">
#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;  width: 450px;}
</style>

<SCRIPT LANGUAGE=Javascript>
isMenu = true;
// By Kenneth
function jChangeContent( objID, new_content ) {
        var obj = document.getElementById(objID);
        if (obj!=null)
        {
                obj.innerHTML = new_content;
        }
//        showMenu(objID, new_content);

}

var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu", o.responseText );
        }
}
function retrievePurchaseDetails(e,trans_id)
{
        obj = document.form1;
       
        showMenu("ToolMenu","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$ip20_loading?></td></tr></table>",e.x,e.y);
		        
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getPurchasingDetails.php?TransactionID=" + trans_id;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
</SCRIPT>

<br />
<form name="form1" method="get">
<div id="ToolMenu"></div>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="5">
                <? if ($lu->RecordType == 3) { ?>
                <tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Profile_SelectStudent?> </span></td>
			<td valign="top" class="tabletext"><?=$selection?></td>
		</tr>
                <tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="30%"><?=$i_Payment_Field_Balance?> </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } else {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="10%"><?=$i_Payment_Field_Balance?> : </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } ?>
                <tr> 
			<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		</table>
        </td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                	<td><?=$searchTag?></td>
		</tr>
                
                <tr>
			<td colspan="2"><?=$li->display()?></td>
        	</tr>
                </table>
        </td>
</tr>
</table>        
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>    


<? 
}	//end of hasRight
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
