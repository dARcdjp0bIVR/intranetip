<?
$PATH_WRT_ROOT = "../../../"; 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('ePayment');
if(!$plugin['payment'] || !$Settings['AllowStudentChooseReceipt'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
$lps = new libuserpersonalsettings();

$SettingList['ePayment_NoNeedReceipt'] = $_REQUEST['NeedReceipt'] ? 0 : 1;

$lps->Save_Setting($UserID,$SettingList);

intranet_closedb();

header('location: receipt_settings.php?msg=SettingApplySuccess');
?>