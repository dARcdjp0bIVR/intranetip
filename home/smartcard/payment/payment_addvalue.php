<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if(!$plugin['payment']){
    header("Location: $intranet_httppath/");	
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}


$lu = new libuser($UserID);
$CurrentPage	= "PagePaymentAddValue";
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$lpayment 	= new libpayment();

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $hasRight = false;
}
else if ($lu->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     if ($studentid == "")
     {
         $studentid = $lfamily->returnFirstChild($UserID);
     }
     else
     {
         $linkedChildren = $lfamily->returnChildrens($UserID);
         if (!in_array($studentid,$linkedChildren))
         {
              header("Location: $intranet_httppath/");
              exit();
         }
     }
     if ($studentid == "")
     {
         $hasRight = false;
     }
     else
     {
         $selection = $lfamily->getSelectChildren($UserID,"name=studentid onChange=\"this.form.submit();\"",$studentid);
     }
}
else # Student
{
       $studentid = $UserID;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}

if ($hasRight)
{

	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
		
	if($lu->RecordType == 3)
		$date_cond = " AND (DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>= '$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate') ";
		
	if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
	$pageSizeChangeEnabled = true;

        $order = ($order == 1) ? 1 : 0;
        if ($field == "") $field = 0;
        $conds = "";
        
        $sql  = "SELECT
                       1,
                       IF(a.DateInput IS NULL,DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i')),
                       DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
                       FORMAT(a.Amount,2),
                       CASE a.RecordType
                            WHEN 1 THEN 
                                CASE a.PPSType
                                    WHEN 0 THEN '".$Lang['ePayment']['IEPS']."' 
							        WHEN 1 THEN '".$Lang['ePayment']['CounterBill']."' 
                                END
                            WHEN 2 THEN '$i_Payment_Credit_TypeCashDeposit'
                            WHEN 3 THEN '$i_Payment_Credit_TypeAddvalueMachine'
                            ELSE '$i_Payment_Credit_TypeUnknown' END,
                       a.RefCode
                 FROM
                     PAYMENT_CREDIT_TRANSACTION as a
                 WHERE
                      a.StudentID = '$studentid'
                      $date_cond
                        ";
        
        # TABLE INFO
        $li = new libdbtable2007($field, $order, $pageNo);
        $li->field_array = array("a.DateInput","a.TransactionTime","a.Amount","a.RecordType","a.RefCode");
        $li->sql = $sql;
        $li->no_col = sizeof($li->field_array)+1;
        $li->IsColOff = "SmartCardPaymentAddValue";
        $li->title = "";
        
        // TABLE COLUMN
        $pos = 0;
        $li->column_list .= "<td width=1 class='tablebluetop tabletoplink'>#</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PostTime)."</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Credit_Method)."</td>\n";
        $li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
        
        
	### Balance
        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }
        
        
### Title ###
        $MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
        $TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_addvalue.gif' align='absmiddle' />";
        $TitleTitle1 = "<span class='contenttitle'>". $TitleImage1.$i_Payment_ValueAddedRecord ."</span>";
        $TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
        $TAGS_OBJ[] = array($TitleTitle, "", 0);    
        
        $linterface->LAYOUT_START();     
?>

<SCRIPT LANGUAGE=Javascript>
function checkForm(formObj)
{
	if(formObj==null)return false;
	
        fromV = formObj.FromDate;
	toV= formObj.ToDate;
	
        if(!checkDate(fromV))
	{
		return false;
	}
	else if(!checkDate(toV))
        {
		return false;
	}
	
        return true;
}

function checkDate(obj)
{
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}

function submitForm(obj)
{
	if(checkForm(obj))
		obj.submit();	
}
</SCRIPT>

<br />
<form name="form1" method="get" onSubmit="return checkForm(this);">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="5">
                <? if($lu->RecordType==3) { ?>
                <tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Profile_SelectStudent?> </span></td>
			<td valign="top" class="tabletext"><?=$selection?></td>
		</tr>
                <tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="30%"><?=$i_Payment_Field_Balance?> </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } else { ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="10%"><?=$i_Payment_Field_Balance?> : </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } ?>
                <tr> 
			<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>

		<tr>
			<td align="left" class="tabletext" colspan="2"><br />
                              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
                                <? if($lu->RecordType==3) { ?>
                                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Profile_SelectSemester?> </span></td>
					<td valign="top" class="tabletext"><?=$i_Profile_From?> <input type="text" name="FromDate" value="<?=$FromDate?>" class="textboxnum">&nbsp;<?=$linterface->GET_CALENDAR("form1","FromDate");?>&nbsp;<?=$i_Profile_To?> <input type="text" name="ToDate" value="<?=$ToDate?>" class="textboxnum"> <?=$linterface->GET_CALENDAR("form1","ToDate");?></td>
				</tr>
                                
                                <tr>
                			<td align="left" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;</td>
                		</tr>
                                <tr> 
                			<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                		</tr>
                                <tr>
                                	<td colspan="2">        
                                                <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
                                                <tr>
                                			<td align="center">
                                				<?= $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                			</td>
                                		</tr>
                                                </table>                                
                                	</td>
                                </tr>
                                <? } ?>
				</table>
			</td>
		</tr>
                <tr>
			<td align="left" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;</td>
		</tr>

		
		</table>
        </td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
			<td colspan="2"><?=$li->display()?></td>
        	</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
}  //end hasRight

intranet_closedb();
$linterface->LAYOUT_STOP();
?>
