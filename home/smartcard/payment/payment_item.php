<?php
// Editing by 
/**
 * Log 
 *  2014-07-09 (Carlos): change word $i_Payment_Field_PaidTime to $Lang['ePayment']['PaymentStatus']
 *  2011-08-28 (Yuen) : improved the default order to the payment time
 *  
 */ 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if(!$plugin['payment']){
    header("Location: $intranet_httppath/");	
    exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lu = new libuser($UserID);
$CurrentPage	= "PagePaymentRecord";
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$lpayment 	= new libpayment();

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $hasRight = false;
}
else if ($lu->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     if ($studentid == "")
     {
         $studentid = $lfamily->returnFirstChild($UserID);
         $linkedChildren = $lfamily->returnChildrens($UserID);
     }
     else
     {
         $linkedChildren = $lfamily->returnChildrens($UserID);
         if (!in_array($studentid,$linkedChildren))
         {
              header("Location: $intranet_httppath/");
              exit();
         }
     }
     if ($studentid == "")
     {
         $hasRight = false;
     }
     else
     {
         $selection = $lfamily->getSelectChildren($UserID,"name=studentid",$studentid);
     }
}
else # Student
{
       $studentid = $UserID;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}

if ($hasRight)
{
	# date range

	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	
	if($lu->RecordType == 3)
		$date_cond = " AND (b.EndDate >='$FromDate' AND b.EndDate<='$ToDate') ";
		
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 0 : 1;
if ($field == "") $field = 5;
$conds = "";

if (isset($RecordStatus) && $RecordStatus != '')
{
    $conds .= " AND a.RecordStatus = '$RecordStatus'";
}
if (isset($CatID) && $CatID != '')
{
    $conds .= " AND b.CatID = '$CatID'";
}
/*
$sql  = "SELECT
		1,
               CONCAT('<a class=tablebluelink href=javascript:openReceipt(',a.PaymentID,')>',b.Name,'</a>')
               ,c.Name,
               FORMAT(a.Amount,2),b.EndDate,
               IF(a.RecordStatus=1,DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i'),'$i_Payment_PaymentStatus_Unpaid')
         FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
              LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID
         WHERE
              a.StudentID = $studentid
              $conds
              AND (
                   b.Name LIKE '%$keyword%' OR
                   c.Name LIKE '%$keyword%' OR
                   a.Amount = '$keyword' OR
                   b.EndDate = '$keyword'
              ) 
              $date_cond
                ";

# TABLE INFO
        $li = new libdbtable2007($field, $order, $pageNo);
        $li->field_array = array("b.Name","c.Name","a.Amount","b.EndDate","a.PaidTime");
        $li->sql = $sql;
        $li->no_col = sizeof($li->field_array)+1;
        $li->IsColOff = "SmartCardPaymentRecord";
        $li->title = "";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tablebluetop tabletopnolink'>#</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentCategory)."</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentDeadline)."</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaidTime)."</td>\n";
*/
$sql  = "SELECT 1,
               CONCAT('<a class=tablebluelink href=javascript:openReceipt(',a.PaymentID,')>',b.Name,'</a>')
               ,c.Name,
               IF(a.Amount IS NULL,'&nbsp;',CONCAT('$',FORMAT(a.Amount,1))),
               IF(a.SubsidyAmount IS NULL OR a.SubsidyAmount<=0,'&nbsp;',CONCAT('<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td class=tabletext>$',FORMAT(a.SubsidyAmount,1),'&nbsp;</td><td align=right><img src=\"".$image_path."/icon_alt.gif\" align=absmiddle border=0 onMouseOver=\'javascript:showSubsidySource(this,\"',d.UnitName,'\")\' onMouseOut=\'javascript:hideSubsidySource()\'></td></tr></table>')),
               b.EndDate,
               IF(a.RecordStatus=1,DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i'),'$i_Payment_PaymentStatus_Unpaid')
         FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
              LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID  LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d  ON (a.SubsidyUnitID = d.UnitID)
         WHERE
              a.StudentID = '$studentid' 
              $conds
              AND (
                   b.Name LIKE '%$keyword%' OR
                   c.Name LIKE '%$keyword%' OR
                   a.Amount = '$keyword' OR
                   b.EndDate = '$keyword'
              ) 
              $date_cond
                ";
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.Name","c.Name","a.Amount","a.SubsidyAmount","b.EndDate","a.PaidTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
//$li->column_array = array(0,0,0,0,0,0);
//$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = "SmartCardPaymentRecord";


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tablebluetop tabletoplink'>#</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width=20% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentCategory)."</td>\n";
$li->column_list .= "<td width=12% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width=12% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Subsidy_Amount)."</td>\n";
$li->column_list .= "<td width=18% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaymentDeadline)."</td>\n";
$li->column_list .= "<td width=18% class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_PaidTime)."</td>\n";

### Title ###
        $MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
        $TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_paymentrecord.gif' align='absmiddle' />";
        $TitleTitle1 = "<span class='contenttitle'>". $TitleImage1.$i_Payment_PaymentRecord ."</span>";
        $TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
        $TAGS_OBJ[] = array($TitleTitle, "", 0);
        
### Button
        $searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
        $searchTag 	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
        $searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
	$searchTag 	.= "</tr></table>";

### select
        $items_cat = $lpayment->returnPaymentCats();
        $select_cat = getSelectByArray($items_cat,"name=CatID",$CatID);
        
        $select_status = "<SELECT name=RecordStatus>\n";
        $select_status .= "<OPTION value='' ".($RecordStatus==''?"SELECTED":"").">$AllRecords</OPTION>\n";
        $select_status .= "<OPTION value='0' ".($RecordStatus=='0'?"SELECTED":"").">$i_Payment_PaymentStatus_Unpaid</OPTION>\n";
        $select_status .= "<OPTION value='1' ".($RecordStatus=='1'?"SELECTED":"").">$i_Payment_PaymentStatus_Paid</OPTION>\n";
        $select_status .= "</SELECT>\n";        
        
### Balance        
        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }
                
        $linterface->LAYOUT_START();           
?>
<SCRIPT LANGUAGE="Javascript">
function openReceipt(payment_id)
{
         newWindow('payment_item_receipt.php?PaymentID='+payment_id,6);
}

function checkForm(formObj){
		if(formObj==null)return false;
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					return false;
			}
			else if(!checkDate(toV)){
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}
	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
function showSubsidySource(obj,subsidy_name){
			var pos_left = getPostion(obj,"offsetLeft");
			var pos_top  = getPostion(obj,"offsetTop");
			
			offsetX = (obj==null)?0:obj.width;
			//offsetY = (obj==null)?0:obj.height;

			offsetY =0;
			objDiv = document.getElementById('tooltip3');
			
            str ='<table border=0 cellspacing=0 cellpadding=1 ><tr><td class=attendancestudentphoto><table border=0 cellspacing=0 cellpadding=3><tr><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td></td></tr></table></td></tr><tr><td class="tablebluerow1 tabletext" valign=top>'+subsidy_name+'</td></tr></table></td></tr></table>';
			
			if(objDiv!=null){
				objDiv.innerHTML = str;
				objDiv.style.visibility='visible';
				objDiv.style.top = pos_top+offsetY+'px';
				objDiv.style.left = pos_left+offsetX+'px';
				setDivVisible(true, "tooltip3", "lyrShim");
			}

}

function hideSubsidySource(){
		obj = document.getElementById('tooltip3');
		if(obj!=null)
			obj.style.visibility='hidden';
		setDivVisible(false, "tooltip3", "lyrShim");
}	
</SCRIPT>

<br />
<form name="form1" method="get" onSubmit="return checkForm(this);">
    <div id="tooltip3" style="position:absolute; top: 0px; left: 0px; z-index:4; visibility:hidden;"></div>
     	<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="5">
                <? if($lu->RecordType==3) { ?>
                <tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Profile_SelectStudent?> </span></td>
			<td valign="top" class="tabletext"><?=$selection?></td>
		</tr>
                <tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="30%"><?=$i_Payment_Field_Balance?> </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } else { ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="paymenttitle" width="10%"><?=$i_Payment_Field_Balance?> : </td>
			<td valign="top" class="tabletext"><?=$balance_str?></td>
		</tr>
                <? } ?>
                <tr> 
			<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		
		<tr>
			<td align="left" class="tabletext" colspan="2"><br />
                              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Payment_Field_PaymentCategory?> </span></td>
					<td valign="top" class="tabletext"><?=$select_cat?></td>
				</tr>
                                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['ePayment']['PaymentStatus']?> </span></td>
					<td valign="top" class="tabletext"><?=$select_status?></td>
				</tr>
                                <? if($lu->RecordType==3) { ?>
                                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_Profile_SelectSemester?> </span></td>
					<td valign="top" class="tabletext"><?=$i_Profile_From?> <input type="text" name="FromDate" value="<?=$FromDate?>" class="textboxnum">&nbsp;<?=$linterface->GET_CALENDAR("form1","FromDate");?>&nbsp;<?=$i_Profile_To?> <input type="text" name="ToDate" value="<?=$ToDate?>" class="textboxnum"> <?=$linterface->GET_CALENDAR("form1","ToDate");?></td>
				</tr>
                                <? } ?>
				</table>
			</td>
		</tr>
                <tr>
			<td align="left" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;</td>
		</tr>
                <tr> 
			<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
                <tr>
                	<td colspan="2">        
                                <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
                                <tr>
                			<td align="center">
                				<?= $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                			</td>
                		</tr>
                                </table>                                
                	</td>
                </tr>
		
		</table>
        </td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                	<td><?=$searchTag?></td>
		</tr>
                
                <tr>
			<td colspan="2"><?=$li->display()?></td>
        	</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<? 
}	//end of hasRight
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
