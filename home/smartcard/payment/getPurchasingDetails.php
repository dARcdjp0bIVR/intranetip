<?php
// Editing by 
/*
Creator : Kenneth Wong (20070720)
AJAX layer calling for purchasing transaction details
*/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

# Kenneth: Skipped checking first (implement later)

$TransactionID = IntegerSafe($TransactionID);

$lpayment = new libpayment();
$lu = new libuser($UserID);

$sql = "SELECT StudentID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE LogID='$TransactionID'";
$studentIdAry = $lpayment->returnVector($sql);

$hasRight = true;
if ($lu->isParent())  # Parent
{
	$childrenIdAry = $lu->getChildren();
	if(!in_array($studentIdAry[0],$childrenIdAry)){
		$hasRight = false;
	}
}else{
	$hasRight = ($studentIdAry[0] == $_SESSION['UserID']);
}
if(!$hasRight){
	No_Access_Right_Pop_Up();
}

$layer_data = "<table width=350 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
$layer_data .= "<tr><td><table width=97% border=1 bordercolorlight=#FBFDEA bordercolordark=#B3B692>";

$sql = "SELECT 
					pi.ItemName, 
					ppdr.ItemQty, 
					ppdr.ItemSubTotal
        FROM 
        	PAYMENT_PURCHASE_DETAIL_RECORD ppdr 
			INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as p ON p.LogID=ppdr.TransactionLogID 
        	INNER JOIN 
        	POS_ITEM pi
        	on 
        		ppdr.TransactionLogID = '$TransactionID' 
        		and 
        		ppdr.ItemID = pi.ItemID 
        ORDER BY 
        	RecordID";
$data = $lpayment->returnArray($sql,3);

global $i_EventCloseImage;
$layer_data .= "<tr><td colspan=3 align=right><a href=javascript:hideLayer(\"ToolMenu\")>[X]</a></td></tr>";
if (sizeof($data)==0)
{
    $layer_data .= "<tr><td colspan=3 align=center>$i_no_record_searched_msg</td></tr>";
}
else
{
    $layer_data .= "<tr><td>Item</td><td>Quantity</td><td>Sub-total</td></tr>";
    for ($i=0; $i<sizeof($data); $i++)
    {
         list($t_name, $t_qty, $t_subtotal) = $data[$i];
         $t_subtotal = '$'.number_format($t_subtotal,1);
         $layer_data .= "<tr><td>$t_name</td><td>$t_qty</td><td>$t_subtotal</td></tr>";
     }
}
$layer_data .= "<tr><td colspan=3 align=right><a href=javascript:hideLayer(\"ToolMenu\")>[X]</a></td></tr>";
$layer_data .= "</table></td></tr></table>";
$layer_data = nl2br($layer_data);

//$response = iconv("Big5","UTF-8//IGNORE",$layer_data);
//echo $response;
echo $layer_data;
intranet_closedb();

?>