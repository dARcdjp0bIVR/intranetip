<?php
// page used by: kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

if(!$plugin['payment'] || $special_option['disable_children_transfer']){
    header("Location: $intranet_httppath/");	
    exit();
}

$CurrentPage	= "PagePaymentTransfer";
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $hasRight = false;
}
else if ($lu->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     if ($studentid == "")
     {
     	$studentid = $lfamily->returnFirstChild($UserID);
     }
     else
     {
         $linkedChildren = $lfamily->returnChildrens($UserID);
         if (!in_array($studentid,$linkedChildren))
         {
              header("Location: $intranet_httppath/");
              exit();
         }
     }
     if ($studentid == "")
     {
         $hasRight = false;
     }
     else
     {
     }
}
else # Student
{
     $hasRight = false;
}

if (!$hasRight)
{
     header("Location: $intranet_httppath/");
     exit();
}

if ($hasRight)
{

        $lpayment = new libpayment();
        
        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }

        # grab other children
        #$linkedChildren
        $children = $lfamily->returnChildrensList($UserID);
        $targets = array();
        for ($i=0; $i<sizeof($children); $i++)
        {
             list($sid , $s_name) = $children[$i];
             if ($sid == $studentid) continue;
        
             $temp = $lpayment->checkBalance($sid);
             list($s_bal, $s_lastupdated) = $temp;
             $s_bal = number_format($s_bal,2);
             $display_str = "$s_name ($"."$s_bal)";
             $array = array($sid,$display_str);
             $targets[] = $array;
        }
	
        $selection_from = $lfamily->getSelectChildren($UserID,"name='studentid' onChange=\"document.form1.submit();\"",$studentid);
        $select_children = getSelectByArray($targets,"name=target","",1,1);
        
        ### Title ###
        $MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
        $TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_transfer.gif' align='absmiddle' />";
        $TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_Payment_Transfer ."</span>";
        $TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
        $TAGS_OBJ[] = array($TitleTitle, "", 0);
        
        $linterface->LAYOUT_START();           
?>
<script language="javascript">
<!--
function checkform(obj)
{
     if(!check_text(obj.amount, "<?php echo $i_alert_pleasefillin.$i_Payment_TransferAmount; ?>.")) return false;
     if(isNaN(obj.amount.value))
     {
     	alert("<?php echo $i_alert_pleasefillin.$i_Payment_TransferAmount; ?>");
        return false;
     }
     if(obj.amount.value <= 0)
     {
     	alert("<?php echo $i_Payment_Warning_InvalidAmount; ?>");
        return false;
     }
     
}    
//-->
</script>

<br />   
<form name="form1" method="get" action="payment_transfer_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tabletext">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Payment_TransactionType_TransferFrom?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$selection_from?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Payment_Field_Balance?> </span></td>
					<td><?=$balance_str?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Payment_TransactionType_TransferTo?> <span class='tabletextrequire'>*</span></span></td>
					<td><?=$select_children?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Payment_TransferAmount?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="amount" type="text" class="textboxnum" maxlength="255" value="<?=$amount?>"/></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />
</form>


<?
} //end hasRight
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.studentid");
$linterface->LAYOUT_STOP();
?>


