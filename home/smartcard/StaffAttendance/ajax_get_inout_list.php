<?php
//editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3UI = new libstaffattend3_ui();

$TargetDate = $_REQUEST['TargetDate'];
$SelectType = $_REQUEST['SelectType'];
$SelectStaff = $_REQUEST['SelectStaff'];
$Keyword = trim(stripslashes(urldecode($_REQUEST['Keyword'])));
$Task = strtoupper($_REQUEST['Task']);

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_DailyLog_List($TargetDate, $SelectType, $SelectStaff, $Keyword, "SmartCard","","",$Task);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>