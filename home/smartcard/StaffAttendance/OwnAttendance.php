<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();

$toDay=date('Y-m-d');
$page_load_time = time();
if(!$module_version['StaffAttendance'] == 2.0)
{
	header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$luser = new libuser($UserID);

if (!$luser->needAttendance()) {
	header("Location: $intranet_httppath/");
        exit();
}

$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$lsa = new libstaffattend2();
$CurrentPage	= "PageStaffOwnAttdenancePage";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_personalattend.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>". $i_StaffAttendance_SelfAttendance ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$linterface->LAYOUT_START();

if ($SelectYear == "" || $SelectMonth == "")
{
    $ts = time();
    $SelectYear = date('Y',$ts);
    $SelectMonth = date('m',$ts);
}
else {
	$SelectYear 	= date('Y', mktime(0, 0, 0, $SelectMonth, 1, $SelectYear));
	$SelectMonth 	= date('m', mktime(0, 0, 0, $SelectMonth, 1, $SelectYear));
}

$prevYear 	= date('Y', mktime(0, 0, 0, $SelectMonth-1, 1, $SelectYear));
$prevMonth 	= date('m', mktime(0, 0, 0, $SelectMonth-1, 1, $SelectYear));
$nextYear 	= date('Y', mktime(0, 0, 0, $SelectMonth+1, 1, $SelectYear));
$nextMonth 	= date('m', mktime(0, 0, 0, $SelectMonth+1, 1, $SelectYear));

# calendar 
$calendar_control ="<br>";
$calendar_control.='<table border="0" cellspacing="0" cellpadding="0">';
$calendar_control.='<tr>
        							<td width="20">&nbsp;</td>
        							<td width="16">'.make_staff_attend_button("<img src=\"".$image_path."/2009a/icon_prev_off.gif\" name=\"prevp21\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp21\">","?SelectYear=".$prevYear."&SelectMonth=".$prevMonth,""," class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp21','','".$image_path."/2007a/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ").'</td>
        							<td align="center" valign=center class="tabletext">
        								'.$i_StaffAttendance_Month_Selector_SelectMonth.'
        								&nbsp;&nbsp;'.$lsa->getYearMonthSelector($SelectYear, $SelectMonth).'&nbsp;<input name="submit3" type="submit" class="formsubbutton" onMouseOver="this.className=\'formsubbuttonon\'" onMouseOut="this.className=\'formsubbutton\'" value="Submit">&nbsp;&nbsp;
        								<font color="#FFFFFF">&nbsp; </font></td>
        							<td width="16">'.make_staff_attend_button("<img src=\"".$image_path."/2009a/icon_next_off.gif\" name=\"nextp21\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp21\">","?SelectYear=".$nextYear."&SelectMonth=".$nextMonth,""," class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp21','','".$image_path."/2007a/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ").'</td>
        						</tr>
    								</table>	
        					 ';

# print
/*$toolbar = make_staff_attend_button("<img src='".$staff_image_path."icon_print.gif' hspace=1 border=0 align=absMiddle>".$i_PrinterFriendlyPage,"staff_print.php?targetUserID=".$targetUserID."&SelectYear=".$SelectYear."&SelectMonth=".$SelectMonth,"_blank","class=iconLink");
$toolbar .= make_staff_attend_button("<img src='".$staff_image_path."icon_export.gif' hspace=1 border=0 align=absMiddle>".$button_export,"staff_export.php?targetUserID=".$targetUserID."&SelectYear=".$SelectYear."&SelectMonth=".$SelectMonth,"_blank","class=iconLink");*/


$sqlReason = "SELECT 
								a.StaffID,
								a.RecordDate,
								a.RecordType,
								b.ReasonTypeName, 
								a.Reason 
							FROM 
								CARD_STAFF_ATTENDANCE2_PROFILE AS a
								LEFT OUTER JOIN
								CARD_STAFF_ATTENDANCE2_REASON_TYPE AS b 
								ON 
								a.ReasonType=b.TypeID
							WHERE 
								StaffID=".$luser->UserID." 
								and 
								DATE_FORMAT(RecordDate,'%Y%m')='$SelectYear$SelectMonth' 
							";
$resultReason = $lsa->returnArray($sqlReason,5);

for ($i=0; $i<sizeof($resultReason); $i++)
{
     list($staffID,$recordDate,$recordType,$reasonTypeName,$Reason) = $resultReason[$i];
     $reasonTypeName = $reasonTypeName==""?"--":$reasonTypeName;
     # reason for Late
     if($recordType==2)
     		$result[$staffID][$recordDate]['In']['Reason'] = '['.$reasonTypeName.'] '.$Reason;
     # reason for early leave
     else if($recordType==3)
     		$result[$staffID][$recordDate]['Out']['Reason'] = '['.$reasonTypeName.'] '.$Reason;
     # reason for absent
     /*
     else if($recordType==1){
     		//$result[$staffID][$recordDate]['Out']['Reason'] = "--";
     		//$result[$staffID][$recordDate]['In']['Reason'] = "--";
     		$result[$staffID][$recordDate]['Out']['Reason'] = "--";
     		$result[$staffID][$recordDate]['In']['Reason'] = $reasonTypeName;

     }
     */
     else {
     		$result[$staffID][$recordDate]['Out']['Reason'] = "--";
     		$result[$staffID][$recordDate]['In']['Reason'] = '['.$reasonTypeName.'] '.$Reason;

     }

	
}
//echo "<p>$sqlReason</p>";

//retrieve OT Records, added on 20080307
$sqlOT = "Select 
						StaffID, 
						RecordDate, 
						TIME_FORMAT(StartTime,'%H:%i') as StartTime, 
						TIME_FORMAT(EndTime,'%H:%i') as EndTime 
					FROM 
						CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID=".$luser->UserID."
						and 
						DATE_FORMAT(RecordDate,'%Y%m')='$SelectYear$SelectMonth'";
$OTRecords = $lsa->returnArray($sqlOT,4);

for ($i=0; $i<sizeof($OTRecords); $i++) {
	list($staffID,$recordDate,$StartTime,$EndTime) = $OTRecords[$i];
	
	$result[$staffID][$recordDate]['In']['StartTime'] = $StartTime;
	$result[$staffID][$recordDate]['In']['InSchoolStatus'] = $i_StaffAttendance_DataManagmenet_OTRecord;
	$result[$staffID][$recordDate]['Out']['EndTime'] = $EndTime;
	$result[$staffID][$recordDate]['Out']['OutSchoolStatus'] = $i_StaffAttendance_DataManagmenet_OTRecord;
     
}						
//code added end

$table_name =$lsa->createTable_Card_Staff_Attendance2_Daily_Log($SelectYear,$SelectMonth);

//$sqlDays = " SELECT StaffID,DayNumber,IF(InTime IS NULL,'--',TIME_FORMAT(InTime,'%H:%i')) AS InTime,IF(OutTime IS NULL,'--',TIME_FORMAT(OutTime,'%H:%i')) AS OutTime,Duty,IF(DutyStart IS NULL, '--',TIME_FORMAT(DutyStart,'%H:%i')) AS DutyStart,IF(DutyEnd IS NULL,'--',TIME_FORMAT(DutyEnd,'%H:%i')) AS DutyEnd,InSchoolStatus,IF(OutSchoolStatus IS NULL,'--',OutSchoolStatus),StaffPresent,";
//$sqlDays.=" IF(InWaived=1,'$i_StaffAttendance_Result_Waived','--') AS InWaived,IF(OutWaived=1,'$i_StaffAttendance_Result_Waived','--') AS OutWaived FROM $table_name WHERE StaffID=$targetUserID ORDER BY DayNumber";
$sqlDays = " SELECT 
							StaffID,
							DayNumber,
							IF(InTime IS NULL,'--',TIME_FORMAT(InTime,'%H:%i')) AS InTime,
							IF(OutTime IS NULL,'--',TIME_FORMAT(OutTime,'%H:%i')) AS OutTime,
							Duty,
							IF(DutyStart IS NULL, '--',TIME_FORMAT(DutyStart,'%H:%i')) AS DutyStart,
							IF(DutyEnd IS NULL,'--',TIME_FORMAT(DutyEnd,'%H:%i')) AS DutyEnd,
							InSchoolStatus,
							IF(OutSchoolStatus IS NULL AND InSchoolStatus='".CARD_STATUS_ABSENT."','--',OutSchoolStatus),
							StaffPresent,
							IF(InWaived=1,'$i_StaffAttendance_Result_Waived','--') AS InWaived,
							IF(OutWaived=1,'$i_StaffAttendance_Result_Waived','--') AS OutWaived 
						FROM 
							".$table_name." 
						WHERE 
							StaffID=".$luser->UserID." 
						ORDER BY 
							DayNumber";

//echo "<p>$sqlDays</p>";
$resultDays = $lsa->returnArray($sqlDays,12);
$table_header .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">';
$table_header .= '<tr class="tabletop">
										<td rowspan="2" align="center" class="tabletopnolink">'.$i_StaffAttendance_IntermediateRecord_Date.'</td>
										<td rowspan="2" align="center" class="tabletopnolink">'.$i_StaffAttendance_Report_Staff_Duty.'</td>
										<td colspan="4" align="center" class="attendanceintop">'.$i_StaffAttendance_Status_in.'</td>
										<td colspan="4" align="center" class="attendanceouttop">'.$i_StaffAttendance_Status_out.'</td>
									</tr>';
$table_header .= '<tr class="tabletop">
  									<td align="center" class="attendanceintop">'.$i_StaffAttendance_Report_Staff_Time.'</td>
										<td align="center" class="attendanceintop">'.$i_StaffAttendance_Status.'</td>
										<td align="center" class="attendanceintop">'.$i_StaffAttendance_Leave_Reason.'</td>
										<td align="center" class="attendanceintop">'.$i_StaffAttendance_Report_Staff_Waived.'</td>
										<td align="center" class="attendanceouttop">'.$i_StaffAttendance_Report_Staff_Time.'</td>
										<td align="center" class="attendanceouttop">'.$i_StaffAttendance_Status.'</td>
										<td align="center" class="attendanceouttop">'.$i_StaffAttendance_Leave_Reason.'</td>
										<td align="center" class="attendanceouttop">'.$i_StaffAttendance_Report_Staff_Waived.'</td>
									</tr>
								';

$table_footer = '<tr class="tablebluebottom">
									<td class="tablebottom" colspan=10> &nbsp;
									</td>
								</tr>
								</table>';
$display = "$table_header";
if(sizeof($resultDays)==0){
	$display.="<tr><td class=\" tabletext\" align=center colspan=10>$i_no_record_exists_msg</td></tr>";
}
for ($i=0; $i<sizeof($resultDays); $i++)
{
   list($userID,$dayNumber, $inTime, $outTime, $duty, $dutyStart, $dutyEnd,$inSchoolStatus,$outSchoolStatus,$staffPresent,$inWaived,$outWaived) = $resultDays[$i];
   
   $rDate = date("Y-m-d",mktime(0,0,0,$SelectMonth,$dayNumber,$SelectYear))."";
   $css = ($i%2?"2":"");
   $display .= '<tr class="tableline"><td align="center" nowrap class="tableline tabletext">'.date("Y-m-d",mktime(0,0,0,$SelectMonth,$dayNumber,$SelectYear)).'</td>';
     // $dutyTime = $duty==1?($dutyStart."-".$dutyEnd):"$i_StaffAttendance_Report_Staff_Off";
   if($duty!=1){
     $dutyTime = $i_StaffAttendance_Report_Staff_Off;
     $inStatus = $result[$userID][$rDate]['In']['InSchoolStatus'];
     $outStatus = $result[$userID][$rDate]['Out']['OutSchoolStatus'];
     $inTime = $result[$userID][$rDate]['In']['StartTime'];
     $outTime = $result[$userID][$rDate]['Out']['EndTime'];
	 }else{
	   $dutyTime = $dutyStart."-".$dutyEnd;
	 }
     

   	$display .='<td align="center" nowrap class="tableline tabletext">'.$dutyTime.'</td>';
  	$inReason = $result[$userID][$rDate]['In']['Reason'];
   	$outReason= $result[$userID][$rDate]['Out']['Reason'];
  	//$inStatus	=	$inSchoolStatus==0?$i_StaffAttendance_Result_Normal:($inSchoolStatus==1?$i_StaffAttendance_Result_Absent:$i_StaffAttendance_Result_Late);
  	if ($duty == 1) {
	  	switch($inSchoolStatus){
	    	case CARD_STATUS_ABSENT		: $inStatus = $i_StaffAttendance_Status_Absent;break;
	    	case CARD_STATUS_LATE		: $inStatus = $i_StaffAttendance_Status_Late;break;
	    	case CARD_STATUS_OUTGOING	: $inStatus = $i_StaffAttendance_Status_Outgoing; break;
	    	case CARD_STATUS_HOLIDAY	: $inStatus =$i_StaffAttendance_Status_Holiday; break;
	    	case CARD_STATUS_NORMAL		: $inStatus =$i_StaffAttendance_Status_Normal;break;
	    	default : //$inStatus = $i_StaffAttendance_Status_Normal; 
	    		break;
	  	
	    }

   	//$outStatus=	$outSchoolStatus==3?$i_StaffAttendance_Status_EarlyLeave:$i_StaffAttendance_Status_Normal;
	   	switch($outSchoolStatus){
		   	case "--" : $outStatus = "--"; break;
		   	case CARD_STATUS_EARLYLEAVE : $outStatus = $i_StaffAttendance_Status_EarlyLeave; break;
	    	case CARD_STATUS_OUTGOING	: $outStatus = $i_StaffAttendance_Status_Outgoing;break;
	    	case CARD_STATUS_NORMAL		: $outStatus =$i_StaffAttendance_Status_Normal; break;
	    	default: //$outStatus = $i_StaffAttendance_Status_Normal;
	    		break;
			}
		}
	   		
		if($inReason=="")$inReason="--";
		if($outReason=="")$outReason="--";
		if($inStatus=="")$inStatus="--";
		if($outStatus=="")$outStatus="--";
		/*
		if($staffPresent==0||$staffPresent==""){
			$inTime="--";
			$outTime="--";
			$outStatus="--";
			$inStatus="--";
		}*/
 		
		$display .='<td align="center" class="attendancein tabletext">'.$inTime.'</td>
								<td align="center" class="attendancein Warningtitletext">'.$inStatus.'</td>
								<td align="center" class="attendancein tabletext">'.$inReason.'</td>
								<td align="center" class="attendancein tabletext">'.$inWaived.'</td>
								
								<td align="center" class="attendanceout tabletext">'.$outTime.'</td>
								<td align="center" class="attendanceout tabletext">'.$outStatus.'</td>
								<td align="center" class="attendanceout tabletext">'.$outReason.'</td>
								<td align="center" class="attendanceout tabletext">'.$outWaived.'</td>
								</tr>';
								
 //}
 
}
$display .= $table_footer;
?>

<style type="text/css">
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>
<br />
<form name=form1 method=get>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="Left">    
		<?=$calendar_control?>                           
	</td>
</tr>
<tr>
	<td align="center">    
		<?=$display?>                           
	</td>
</tr>
</table>
<br />
<input name="PageLoadTime" type="hidden" value="<?=$page_load_time?>">   
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

