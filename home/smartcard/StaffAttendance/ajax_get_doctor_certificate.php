<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DoctorCertificate'] 
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

switch($task)
{
	case "Get_Doctor_Certificate_Management_DBTable":
		$TargetYear = $_REQUEST['TargetYear'];
		$TargetMonth = $_REQUEST['TargetMonth'];
		$RecordType = $_REQUEST['RecordType'];
		$StatusType = $_REQUEST['StatusType'];
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		
		$arrCookies = array();
		$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_size", "numPerPage");
		$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_number", "pageNo");
		$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_order", "order");
		$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_field", "field");	
		$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $StaffAttend3UI->Get_Doctor_Certificate_Management_DBTable($TargetYear,$TargetMonth,$RecordType,$StatusType,$Keyword,$field,$order,$pageNo,$numPerPage,true);
	break;
}

intranet_closedb();
?>