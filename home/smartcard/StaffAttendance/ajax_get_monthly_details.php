<?php
//editing by
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3UI = new libstaffattend3_ui();

$Year = intranet_htmlspecialchars($_REQUEST['SelectYear']);
$Month = intranet_htmlspecialchars($_REQUEST['SelectMonth']);
$TargetUserID = IntegerSafe($_SESSION['UserID']);
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));

echo $StaffAttend3UI->Get_Self_Report_Attendance_Monthly_Details($TargetUserID, $Year, $Month, $Keyword);

intranet_closedb();
?>