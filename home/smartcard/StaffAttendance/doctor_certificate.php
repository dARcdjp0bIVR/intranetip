<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DoctorCertificate'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$arrCookies = array();
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_size", "numPerPage");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_number", "pageNo");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_order", "order");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_field", "field");	
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_keyword", "Keyword");

if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageStaffDoctorCertificate";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['DoctorCertificate'],"doctor_certificate.php",1);
$linterface->LAYOUT_START();

$Keyword = trim(stripslashes($Keyword));
$StatusType = isset($StatusType)?$StatusType:array(CARD_STATUS_OUTGOING,CARD_STATUS_HOLIDAY,CARD_STATUS_ABSENT);

echo $StaffAttend3UI->Get_Doctor_Certificate_Management_Index($TargetYear,$TargetMonth,$RecordType,$StatusType,$Keyword,$field,$order,$pageNo,$numOfPage,true);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" type="JavaScript">
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Doctor_Certificate_Management_DBTable();
	}
	else
		return false;
}

function Get_Doctor_Certificate_Management_DBTable()
{
	var formData = $('#form1').serialize();
	formData += "&task=Get_Doctor_Certificate_Management_DBTable";
	
	Block_Element("form1");
	$.ajax({  
		type: "POST",  
		url: "ajax_get_doctor_certificate.php",
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("#DBTableLayer").html(ReturnData);
			UnBlock_Element("form1");
			Scroll_To_Top();
		} 
	});
}
</script>