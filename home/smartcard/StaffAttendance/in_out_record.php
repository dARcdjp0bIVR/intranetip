<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageStaffOwnAttendance";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'], "attendance_record.php", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'], "in_out_record.php", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['EntryLogReport'], "entry_log.php", 0);
$linterface->LAYOUT_START();

echo $StaffAttend3UI->Get_Report_DailyLog_Index("","",$_SESSION['UserID'],"","SmartCard");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_DailyLog(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_DailyLog_List();
	}
	else
		return false;
}
}

// ajax function
{
function Get_Report_DailyLog_List()
{
	var PostVar = {
			"TargetDate": $('#SelectYear').val()+'-'+$('#SelectMonth').val()+'-01',
			"SelectType": $('#SelectType').val(),
			"SelectStaff": $('#SelectStaff').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	Block_Element("ReportDailyLogTableLayer");
	$.post('ajax_get_inout_list.php',PostVar,
					function(data){
						if (data == "die")
							window.top.location = '/';
						else {
							$('div#ReportDailyLogTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportDailyLogTableLayer");
						}
					});
}

}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>