<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$sys_custom['StaffAttendance']['DailyAttendanceRecord'] || 
	!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageStaffDailyAttendanceRecord";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['DailyAttendanceRecord'],"daily_attendance_record.php",1);
$linterface->LAYOUT_START();

echo $StaffAttend3UI->Get_Daily_Attendance_Record_Index(true);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
