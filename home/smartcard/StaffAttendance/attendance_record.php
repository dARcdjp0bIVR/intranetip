<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$lsmartcard = new libsmartcard();
$CurrentPage = "PageStaffOwnAttendance";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'], "attendance_record.php", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'], "in_out_record.php", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['EntryLogReport'], "entry_log.php", 0);
$linterface->LAYOUT_START();

echo $StaffAttend3UI->Get_Self_Report_Attendance_Monthly_Summary_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_Monthly_Details(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Monthly_Details_List();
	}
	else
		return false;
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}
}

// ajax function
{
function Get_Report_Attendance_Monthly_Summary()
{
	var StartYear = $('#StartYear').val();
	var EndYear = $('#EndYear').val();
	var PostVar = {
		"StartYear":StartYear,
		"EndYear":EndYear
	}
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_summary.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Summary_List()
{
	var StartYear = $('#StartYear').val();
	var EndYear = $('#EndYear').val();
	var PostVar = {
		"StartYear":StartYear,
		"EndYear":EndYear
	}
	
	if(StartYear > EndYear)
	{
		$WarningLayer = $('#DateWarningLayer');
		$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning('DateWarningLayer', 3000);
		return;
	}
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_summary_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details(Year, Month)
{
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var Keyword = '';
	if(document.getElementById('Keyword'))
		Keyword = encodeURIComponent($('Input#Keyword').val());
	var PostVar = {
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"Keyword": Keyword
	};
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_details.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details_List(Year, Month)
{
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var Keyword = '';
	if(document.getElementById('Keyword'))
		Keyword = encodeURIComponent($('Input#Keyword').val());
	var PostVar = {
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"Keyword": Keyword
	};
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_details_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>