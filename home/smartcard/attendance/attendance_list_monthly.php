<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

if(!$plugin['attendancestudent'])
{
	header("Location: $intranet_httppath/");
        exit();
}

$CurrentPage	= "PageAttendance";
$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();

$lu = new libuser($UserID);
$hasRight = true;
if ($lu->isTeacherStaff()) # is teacher
{
	$lteaching = new libteaching();
	$class = $lteaching->returnTeacherClass($UserID);
	$lclass = new libclass();
	
	if (sizeof($class) == 1) {
		$classname = $class[0][1];
	}
	else if (sizeof($class) > 1) {
		if ($classname == "") 
			$classname = $class[0][1];
		
		for ($i=0; $i< sizeof($class); $i++) {
			$ClassList[] = array($class[$i][1],$class[$i][1]);
		}
		$SelectClass = $lteaching->getSelect($ClassList,' name="classname" id="classname" onchange="document.getElementById(\'form1\').submit();" ',$classname,1);
	}
	else if (sizeof($class) == 0) {
		$toolbar = $i_Profile_NotClassTeacher;
		$hasRight = false;
	}
	
	$lu_student = new libuser($StudentID);
	if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
	{
		$StudentID = "";
	}
	if ($StudentID == "")
	{
		$namelist = $lclass->returnStudentListByClass($classname);
		$StudentID = $namelist[0][0];
		if ($StudentID == "") $StudentID = $UserID;
	}

	$selection = $lclass->getStudentSelectByClass($classname,"name=StudentID",$StudentID);
}
else if ($lu->isParent())  # Parent
{
     $lfamily = new libfamily();
     if ($StudentID == "")
     {
         $StudentID = $lfamily->returnFirstChild($UserID);
     }
     $children_list = $lfamily->returnChildrens($UserID);
     if (!in_array($StudentID,$children_list))
     {
        //header("Location: $intranet_httppath/");
        //exit();
        $hasRight = false;
     }
     
     $selection = $lfamily->getSelectChildren($UserID,"name=StudentID ",$StudentID);
}
else # Student
{
	$StudentID = $UserID;
}

if ($StudentID=="" and $hasRight)
{
    header("Location: $intranet_httppath/");
    exit();
}

// Date setting
if (!isset($year) || $year == "")
{
     $year = date('Y');
}
if (!isset($month) || $month == "")
{
     $month = date('m');
}

// Retrieve records
$lcard = new libcardstudentattend2();
$slot_cols_count = ($lcard->attendance_mode==0 || $lcard->attendance_mode == 1)?3:4;

$results = $lcard->retrieveStudentMonthlyRecord($StudentID, $year, $month, 1);

if($field=="") $field = 0;
if($order=="") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tablebluetop tabletoplink' width='33%'>". $i_SmartCard_DateRecorded ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink' width='33%'>". $i_StudentAttendance_ToSchoolTime ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink' width='33%'>". $i_StudentAttendance_LeaveSchoolTime ."</td>\n";
 
$li->IsColOff = "SmartCardAttandencdListMonthly";
$li->title = "";

### month selection
$month_select = "<select name='month'>";
for($m = 1; $m<=12; $m++)
$month_select .= "<option value='".($m<10?"0":"") ."$m' ". ($m==$month?"selected":"") .">". $i_general_MonthShortForm[$m-1] ."</option>";
$month_select .= "</select>";
        
### year selection
$m = date("n");
if($m>=9){
	$year_list = array(date('Y'),date('Y')+1);
}else{
	$year_list = array(date('Y')-1,date('Y'));
}
$year_select = "<select name='year'>";
//for($y = date(Y); $y<=date(Y)+1; $y++)
for($i=0;$i<sizeof($year_list);$i++){
	$y = $year_list[$i];
	$year_select .= "<option value='$y' ". ($y==$year?"selected":"") .">". $y ."</option>";
}
$year_select .= "</select>";

$my_select = $month_select . " " . $year_select;
       
if(!$hasRight)
{
        if ($lu->isTeacherStaff())
	{
		if ($classname=="")
			$prompt_msg = $i_Profile_NotClassTeacher;
		else if (sizeof($students)==0)
		{
			$prompt_msg = $i_StudentAttendance_Message_NoStudentInClass;
                }
		else 
                	$prompt_msg = $i_StudentAttendance_Message_ThisClassNoNeedToTake;
	}
	else     # Parent
	{
		$prompt_msg = $i_Profile_NoFamilyRelation;
	}
}       
       
### Title ###
$TAGS_OBJ[] = array($StatusRecord,"attendance_list.php", 0);
$TAGS_OBJ[] = array($MonthlyRecord,"attendance_list_monthly.php", 1);
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();        

?>

<br />
<form name="form1" id="form1" method="get">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<? if($hasRight) {?>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
    	<? if ($SelectClass != "") {?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_general_class?> </span></td>
					<td valign="top" class="iMailrecipientgroup"><?=$SelectClass?></td>
				</tr>
			<? }?>
                                <? if($lu->RecordType !=2) {?>
        <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_UserStudentName?> </span></td>
					<td valign="top" class="iMailrecipientgroup"><?=$selection?></td>
				</tr>
                                <? } ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_ReportMonth?> </span></td>
					<td valign="top" class="iMailrecipientgroup"><?=$my_select?></td>
				</tr>
				</table>
			</td>
		</tr>
                <tr>
			<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
		</tr>
                <tr> 
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
                <tr>
                	<td colspan="2">        
                                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                                <tr>
                			<td align="center">
                				<?= $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                			</td>
                		</tr>
                                </table>                                
                	</td>
                </tr>
		
		</table>
        </td>
</tr>
<? } ?>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
			<td colspan="2"><?=$li->display($prompt_msg)?></td>
        	</tr>
                </table>
        </td>
</tr>
</table>        

<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />

</form>    

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>