<?php
// Editing by 
/****************************************** Changes *************************************************
 * 2019-10-31 (Ray)   : Added date range select
 * 2018-09-06 (Carlos): Only Allow teacher to access.
 * 2012-12-28 (Carlos): Added continuous absent day option
 * 2012-03-02 (YatWoon): Fixed - failed to display the class name [Case#2012-0302-0931-41132]
 * 2011-12-20 (Carlos): Modified to use class id for class selection
 ****************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$MODULE_OBJ['title'] = $i_StudentAttendance_Report_ClassMonth;
$linterface = new interface_html("popup.html");
$lstudentattendance_ui = new libstudentattendance_ui();
$lclass = new libclass();
$GeneralSetting = new libgeneralsettings();
$SettingList = array("'ProfileAttendCount'","'AttendanceMode'","'ClassTeacherTakeOwnClassOnly'");
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$attendance_mode = $Settings['AttendanceMode'];
$take_own_class = $Settings['ClassTeacherTakeOwnClassOnly'];

$AcademicYear = $_REQUEST['AcademicYear'];
$ClassID = IntegerSafe($ClassID);
# Get Selected Classes 
$classes=array();
if($take_own_class==1)
{
	if(trim($ClassID)=="")# select all
	{
		$lteaching = new libteaching();
		$class = $lteaching->returnTeacherClass($UserID);
		for ($i=0; $i< sizeof($class); $i++) {
			$classes[$i]['ClassID']=$class[$i][0];//class ID
			$classes[$i]['ClassName']=$class[$i][1]; // class name
		}
	}else{ # select only one class
		$classes[0]['ClassID']=$ClassID;
		$ClassName = $lclass->getClassName($ClassID);
		$classes[0]['ClassName']=($ClassName == "")? $ClassID:$ClassName;
	}
}else # Teachers can view all classes
{
	if(trim($ClassID)=="")# select all
	{
		$classes = $lclass->getClassList();
	}
	else{ # select only one class
		$classes[0]['ClassID']=$ClassID;
		$ClassName = $lclass->getClassName($ClassID);
		$classes[0]['ClassName']=($ClassName == "")? $ClassID:$ClassName;
	}
}


#Get Date Range
$startDate = trim($_REQUEST['StartDate']);
$endDate = trim($_REQUEST['EndDate']);

# Get Year & Month
if ($Year == "")
{
	$Year = date('Y');
}
if ($Month == "")
{
	$Month = date('m');
}
if (strlen($Month)==1)
{
	$Month = "0".$Month;
}

# Get Optional Data Columns
$Columns=array();
if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	$Columns['ChineseName'] = $ColumnChineseName;
	$Columns['EnglishName'] = $ColumnEnglishName;
	$Columns['Gender'] = $ColumnGender;
	$Columns['Data'] = $ColumnData;
	$Columns['DailyStat'] = $ColumnDailyStat;
	$Columns['Schooldays'] = $ColumnSchoolDays;
	$Columns['MonthlyStat'] = $ColumnMonthlyStat;
	$Columns['Remark'] = $ColumnRemark;
	$Columns['ReasonStat'] = $ColumnReasonStat;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = $ColumnSession;
	}
}else # Default All Columns are shown
{
	$Columns['ChineseName'] = "1";
	$Columns['EnglishName'] = "1";
	$Columns['Gender'] = "1";
	$Columns['Data'] = "1";
	$Columns['DailyStat'] = "1";
	$Columns['Schooldays'] = "1";
	$Columns['MonthlyStat'] = "1";
	$Columns['Remark'] = "1";
	$Columns['ReasonStat'] = "1";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = "1";
	}
}

$ContinuousAbsentDay = round($_REQUEST['AbsentDay']);

# Get Report Content
if($UseDateRange == 0) {
	$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report($classes, $Year, $Month, $Columns, $_REQUEST['HideNoData'], false, $AcademicYear, $StudentStatus, 0, $ContinuousAbsentDay);
} else {
	$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report_By_Date_Range($classes,$Year, $Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,$startDate,$endDate,'',$ContinuousAbsentDay);
}
if ($format == 1)     # Excel
{
	# Get template
	$template_content = get_file_content("$file_path/home/eAdmin/StudentMgmt/attendance/report/template.html");
	$output = str_replace("__MAIN_CONTENT__",$content,$template_content);
	$output_filename = "class_attend_".$classes[sizeof($classes)-1]['ClassName'].".xls";
	output2browser($output,$output_filename);
	flush();
}else {           # Web
$linterface->LAYOUT_START();
?>
<style type="text/css">
#contentTable1, #contentTable2 {
	border:1px solid black;
}

#contentTable1 td, #contentTable2 td {
	border:1px solid black;
}

#contentTable3 td, #contentTable4 td {
	border:0px solid black;
}
</style>
<?php
echo "<br />";
echo $content;
$linterface->LAYOUT_STOP();
}
intranet_closedb();
?>