<?php
// Editing by 
/*
 * 2018-09-06 (Carlos) : Only Allow teacher to access.
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardattend = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$lcardattend->retrieveSettings();

# process date range
if($_REQUEST['startStr']=="" || $_REQUEST['endStr']==""){
	die("Error in parameters");
}else{
  $StartDate = $_REQUEST['startStr'];
  $EndDate = $_REQUEST['endStr'];
  $TargetType = $_REQUEST['targetType'];
  $OrderBy  = $_REQUEST['orderby'];
  $Format = $_REQUEST['format'];
  $AM = $_REQUEST['AM'];
  $PM = $_REQUEST['PM'];
  $LunchOut = $_REQUEST['LunchOut'];
  $LeaveSchool = $_REQUEST['slot_out'];
  $Details = $_REQUEST['details'];
  $ClassName = $_REQUEST['class_name'];

	$StudentAttendUI->Get_No_Tapping_Card_Report($StartDate,$EndDate,$TargetType,$OrderBy,$Format,$AM,$LunchOut,$PM,$LeaveSchool,$Details,$ClassName);
}

intranet_closedb();
?>
