<?php
// using: 
/*
 * 2013-02-26 (Carlos): return empty string if no both $ClassID and $GroupID
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();

if(!$plugin['attendancestudent'] && !$plugin['attendancelesson'])
{
        header("Location: $intranet_httppath/");
        intranet_closedb();
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}


$ClassName = trim(urldecode(stripslashes($_REQUEST['ClassName'])));
$GroupID = $_REQUEST['groupIDString'];

$StudAttend = new libcardstudentattend2();

### Get The current system time ###
$TargetDate = $_REQUEST['ToDate'];

$current_time = time();
$today = date('Y-m-d',$current_time);
$ts_now = $current_time - strtotime($today);

$ClassID = $StudAttend->getClassID($ClassName);
$TimeSetting = false;
if ($GroupID != "") {
	$TimeSetting = $StudAttend->Get_Group_Attend_Time_Setting($TargetDate,"",false,$GroupID,true);
}
else if($ClassID != ""){
	$TimeSetting = $StudAttend->Get_Class_Attend_Time_Setting($TargetDate,"",$ClassID);
}
//debug_r($TimeSetting);

if ($TimeSetting != "NonSchoolDay" && $TimeSetting !== false) {
	$ts_morningTime = $StudAttend->timeToSec($TimeSetting['MorningTime']);
	$ts_lunchStart = $StudAttend->timeToSec($TimeSetting['LunchStart']);
	$ts_lunchEnd = $StudAttend->timeToSec($TimeSetting['LunchEnd']);
	$ts_leaveSchool = $StudAttend->timeToSec($TimeSetting['LeaveSchoolTime']);
	
	if ($ts_now > $ts_lunchEnd)
		$attendance_time = "PM";
	else
		$attendance_time = "AM" ;	
		
	echo $attendance_time;
}
else {
	echo "";
}

intranet_closedb();
?>