<?php
// editing by 
##################################### Change Log #####################################################
#
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$StudAttendUI = new libstudentattendance_ui();
$lsmartcard = new libsmartcard();

$limport = new libimporttext();
$lf = new libfilesystem();

$format_array = array("Class Name","Class No.","Date","Time Slot","Reason","Waive","Submitted Prove Document","Remark");
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
	if($limport->CHECK_FILE_EXT($filename))
	{
	  # read file into array
	  # return 0 if fail, return csv array if success
	  //$data = $lf->file_read_csv($filepath);
	  $data = $limport->GET_IMPORT_TXT($filepath);
	  if(sizeof($data)>0)
	  {
	  	$toprow = array_shift($data);                   # drop the title bar
	  }else
	  {
	  	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	    intranet_closedb();
	    exit();
	  }
	}
	for ($i=0; $i<sizeof($format_array); $i++)
	{
	 if ($toprow[$i] != $format_array[$i])
	 {
	     header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	     intranet_closedb();
	     exit();
	 }
	}

	$CurrentPage = "PagePresetAbsence";
	$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/new.php", 1);
	$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_student.php", 0);
	$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_date.php", 0);
	
	$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START(urldecode($Msg));
	$lteaching = new libteaching($_SESSION['UserID']);
	$TeachingClassListOnly = array();
	$TeachingClassList = $lteaching->returnTeacherClass($_SESSION['UserID']);
	if(count($TeachingClassList) > 0) {
		$TeachingClassListOnly = Get_Array_By_Key($TeachingClassList, 'ClassName');
	}
  echo $StudAttendUI->Get_Preset_Absence_Import_Confirm_Page($data);
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>