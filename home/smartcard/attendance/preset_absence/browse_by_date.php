<?php
// Editing by 
##################################### Change Log #####################################################
# 2019-10-15 Ray: Added session From/To
# 2019-08-06 Ray: Added date range
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_daily_absence_browse_date_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_daily_absence_browse_date_page_number!="")
{
	$pageNo = $ck_attend_daily_absence_browse_date_page_number;
}

if ($ck_attend_daily_absence_browse_date_page_order!=$order && $order!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_order", $order, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_order = $order;
} else if (!isset($order) && $ck_attend_daily_absence_browse_date_page_order!="")
{
	$order = $ck_attend_daily_absence_browse_date_page_order;
}

if ($ck_attend_daily_absence_browse_date_page_field!=$field && $field!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_field", $field, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_field = $field;
} else if (!isset($field) && $ck_attend_daily_absence_browse_date_page_field!="")
{
	$field = $ck_attend_daily_absence_browse_date_page_field;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$lsmartcard = new libsmartcard();
$CurrentPage = "PagePresetAbsence";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$filter = $filter==""?2:$filter;

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$StartDate = $StartDate==""?$academic_year_startdate:$StartDate;
$EndDate = $EndDate==""?$academic_year_enddate:$EndDate;
$date_cond = " ((DATE_FORMAT(a.RecordDate,'%Y-%m-%d')) BETWEEN '$StartDate' AND '$EndDate')";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$has_apply_leave = $plugin['eClassApp']? true : false;

if($StartDate!="" && $EndDate!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$namefield = getNameFieldByLang("b.");
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " a.ApprovedAt,$approved_by_namefield as ApprovedBy, ";
	}

	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

	$conds = '';
	$lteaching = new libteaching($_SESSION['UserID']);
	$TeachingClassList = $lteaching->returnTeacherClass($_SESSION['UserID']);
	if(count($TeachingClassList) > 0) {
		$temp = Get_Array_By_Key($TeachingClassList, 'ClassName');
		$conds = " AND b.ClassName IN ('".implode("','", $temp)."')";
	}

	$sql=" SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',$namefield,'</a>'),
				b.ClassName,b.ClassNumber,
			    a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 $session_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified,
				$more_fields 
				 CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				$conds
				";
	$li = new libdbtable2007($field, $order, $pageNo);
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.SessionFrom","a.SessionTo","a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified");
		$column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	} else {
		$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.Reason", "Waive", "DocumentStatus", "a.Remark","a.DateModified");
		$column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	if($has_apply_leave){
		$field_array[] = "a.ApprovedAt";
		$field_array[] = "ApprovedBy";
		$column_array[] = 0;
		$column_array[] = 0;
		$wrap_array[] = 0;
		$wrap_array[] = 0;
	}
	$li->field_array = $field_array;
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	
	$li->column_array = $column_array;
	$li->wrap_array = $wrap_array;
	$li->IsColOff = 2;

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	$li->column_list .= "<td width='17%'>".$li->column($pos++, $i_StudentAttendance_Field_Date)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Attendance_DayType)."</td>\n";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionFrom'])."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionTo'])."</td>\n";
	}
	$li->column_list .= "<td width='17%'>".$li->column($pos++, $i_Attendance_Reason)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['Waived'])."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['ProveDocument'])."</td>\n";
	$li->column_list .= "<td width='17%'>".$li->column($pos++, $i_Attendance_Remark)."</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</td>\n";
	if($has_apply_leave){
		$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['StudentAttendance']['ApprovalTime'])."</td>\n";
		$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['StudentAttendance']['ApprovalUser'])."</td>\n";
	}
	$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

}

$sql="SELECT DISTINCT DATE_FORMAT(RecordDate,'%Y%m%d') FROM CARD_STUDENT_PRESET_LEAVE";
$dates = $li->returnVector($sql);

$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/new.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_student.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_date.php", 1);

$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language='javascript'>
<!--
function changeClass(){
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj==null) return;
	objTargetDate = document.form1.target_date;
	if(!check_date(objTargetDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
			objTargetDate.focus();
			return;
	}
	obj.submit();
}
function openPrintPage()
{
        newWindow("browse_by_date_print.php?class=<?=$class?>&StartDate=<?=$StartDate?>&EndDate=<?=$EndDate?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>", 12);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
-->
</script>
<br />
<form name="form1" method="GET">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_StudentAttendance_Field_Date ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Error')?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "submit", "submitForm()","submit2") ?>
		</td>
	</tr>
</table>
<br />
<?php if($StartDate!="" && $EndDate!="") {
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')","","","","",0);
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage('browse_by_date_export.php')","","","","",0);
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
	
	$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_edit
						</a>
					</td>";
	
	$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
	
?>
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<?php } ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>