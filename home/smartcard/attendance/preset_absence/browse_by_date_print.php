<?php
// Editing by 
##################################### Change Log #####################################################
# 2019-10-15 Ray: Added session From/To
# 2019-08-06 Ray: Added date range
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$filter = $filter==""?2:$filter;

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$StartDate = $StartDate==""?$academic_year_startdate:$StartDate;
$EndDate = $EndDate==""?$academic_year_enddate:$EndDate;
$date_cond = " ((DATE_FORMAT(a.RecordDate,'%Y-%m-%d')) BETWEEN '$StartDate' AND '$EndDate')";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$has_apply_leave = $plugin['eClassApp']? true : false;

$namefield = getNameFieldByLang("b.");

if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo", "a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified");
} else {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified");
}
if($has_apply_leave){
	$field_array[] = "a.ApprovedAt";
	$field_array[] = "ApprovedBy";
}
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($StartDate!="" && $EndDate!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$lb = new libdb();
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " ,a.ApprovedAt,$approved_by_namefield as ApprovedBy ";
	}

	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

	$conds = '';
	$lteaching = new libteaching($_SESSION['UserID']);
	$TeachingClassList = $lteaching->returnTeacherClass($_SESSION['UserID']);
	if(count($TeachingClassList) > 0) {
		$temp = Get_Array_By_Key($TeachingClassList, 'ClassName');
		$conds = " AND b.ClassName IN ('".implode("','", $temp)."')";
	}

	$sql=" SELECT $namefield, b.ClassName,b.ClassNumber,
	             a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 $session_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
 				 IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified 
				 $more_fields
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				$conds
				 $order_str ";
	$temp = $lb->returnArray($sql);
	
	$display= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
	$display.="<tr>";
	$display.= "<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_UserStudentName."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassName."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassNumber."</td>\n";
	$display.= "<td width=\"17%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_StudentAttendance_Field_Date."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_DayType."</td>\n";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionFrom']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionTo']."</td>\n";
	}
	$display.= "<td width=\"17%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Reason."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['Waived']."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ProveDocument']."</td>\n";
	$display.= "<td width=\"17%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Remark."</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['LastUpdatedTime']."</td>\n";
	if($has_apply_leave){
		$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalTime']."</td>\n";
		$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalUser']."</td>\n";
	}
	for($i=0;$i<sizeof($temp);$i++){
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			list($username, $classname, $classnum, $daytype, $session_from, $sesion_to, $reason, $Waive, $Handin_Prove, $remark, $record_date, $last_updated_time) = $temp[$i];
		} else {
			list($username, $classname, $classnum, $daytype, $reason, $Waive, $Handin_Prove, $remark, $record_date, $last_updated_time) = $temp[$i];
		}
		$css = $i%2==0?"tableContent":"tableContent2";
		$row = "<tr>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$username</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classname&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classnum&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$daytype</td>";
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_from</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$sesion_to</td>";
		}
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$reason&nbsp;</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Waive&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Handin_Prove&nbsp;</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$record_date&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$last_updated_time&nbsp;</td>";
		if($has_apply_leave){
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedAt']."&nbsp;</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedBy']."&nbsp;</td>";
		}
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0){
		$row = "<tr><td class=\"eSportprinttext\" height=\"40\" colspan=\"".count($field_array)."\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
}

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><b><?=$i_SmartCard_DailyOperation_Preset_Absence?> (<?=$StartDate?> - <?=$EndDate?>)</b></td>
	</tr>
</table>
<?=$display?>
<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>