<?php
/*
 * 2017-12-04 (Carlos): Changed $HTTP_SERVER_VARS['HTTP_REFERER'] to $_SERVER['HTTP_REFERER']
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$return_url = $_SERVER['HTTP_REFERER'];

if($RecordID=="" || sizeof($RecordID)<=0)
	header("Location: $return_url");

if(!is_array($RecordID))
	$RecordID = array($RecordID);

$idlist=implode(",",IntegerSafe($RecordID));

$sql="DELETE FROM CARD_STUDENT_PRESET_LEAVE WHERE RecordID IN($idlist)";

$lc = new libcardstudentattend2();
$Result = $lc->db_db_query($sql);	

intranet_closedb();

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;

if ($Result) 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceDeleteSuccess'];
else 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceDeleteFail'];
	
for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"Msg=") != false) {
			$ary[$i]="Msg=".urlencode($Msg);
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"Msg=".urlencode($Msg));
}
$return_url = $filename."?".implode("&",$ary);	
//echo $return_url;
header("Location: $return_url");
?>
