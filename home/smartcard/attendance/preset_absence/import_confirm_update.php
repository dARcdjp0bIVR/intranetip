<?php
// editing by 
##################################### Change Log #####################################################
#
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Only allow teacher to access
$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$StudAttendUI = new libstudentattendance_ui();
$lsmartcard = new libsmartcard();

$CurrentPage = "PagePresetAbsence";
$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/new.php", 1);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_student.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/smartcard/attendance/preset_absence/browse_by_date.php", 0);

$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$limport = new libimporttext();
$lf = new libfilesystem();

echo $StudAttendUI->Get_Preset_Absence_Import_Finish_Page();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>