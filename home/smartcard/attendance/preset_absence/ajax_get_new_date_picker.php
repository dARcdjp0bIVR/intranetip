<?
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

if ($_SESSION["UserID"] == "") {
	echo 'die';
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$DateCount = cleanCrossSiteScriptingCode($_REQUEST['DateCount']);
$Defined = 1;
echo $linterface->GET_DATE_PICKER("target_date[]", date('Y-m-d'),"","yy-mm-dd","","","","","target_date".$DateCount)."<br />";

intranet_closedb();
?>