<?php
##################################### Change Log #####################################################
#
# 2019-08-09 Ray:   Add session from/to
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$luser = new libuser($_SESSION['UserID']);
if(!$luser->isTeacherStaff()){
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

if ($return_url=="") $return_url="new.php";

if(sizeof($studentID)<=0 || sizeof($target_date)<=0 || ($dateTypeAM!=1 && $dateTypePM!=1) )
	header("Location: $return_url");

$values_session = "";
$fields_session = "";
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	if($SessionFrom == '') {
		$SessionFrom_value = "NULL";
	} else {
		$SessionFrom_value = "'$SessionFrom'";
	}

	if($SessionTo == '') {
		$SessionTo_value = "NULL";
	} else {
		$SessionTo_value = "'$SessionTo'";
	}
	$values_session .= ", $SessionFrom_value,$SessionTo_value";
	$fields_session = ",SessionFrom,SessionTo";
}

$studentID = array_unique($studentID);
$target_date = array_unique($target_date);

$values="";
$delim="";
$li = new libcardstudentattend2();

$late_records_ids = array();
for($i=0;$i<sizeof($studentID);$i++){
	$sid =$studentID[$i];
	for($j=0;$j<sizeof($target_date);$j++){
		$td = $target_date[$j];
		if($dateTypeAM==1){
			$DayPeriod = PROFILE_DAY_TYPE_AM;
			$sql = "SELECT count(*) as count FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID='$sid' AND RecordDate='$td' AND DayPeriod='2'";
			$rs = $li->returnArray($sql);
			if($rs[0]['count'] == 0) {
			$values.= $delim."($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			$delim=",";
		}
		}
		if($dateTypePM==1){
			$DayPeriod = PROFILE_DAY_TYPE_PM;
			$sql = "SELECT count(*) as count FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID='$sid' AND RecordDate='$td' AND DayPeriod='3'";
			$rs = $li->returnArray($sql);
			if($rs[0]['count'] == 0) {
			$values.= $delim."($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			$delim=",";
			}
		}
		if($dateTypeAM==1 && $dateTypePM==1) {
			$DayPeriod = PROFILE_DAY_TYPE_WD;
		}

		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$ret = $li->InsertSubmittedAbsentLateRecord($sid, $td, $DayPeriod, '', '', CARD_LEAVE_NORMAL, $SessionFrom, $SessionTo);
			if($ret) {
				$late_records_ids[] = $li->db_insert_id();
			}
		}
	}
}


if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	if(count($late_records_ids) > 0) {
		$site = (checkHttpsWebProtocol()?"https":"http")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
		$cmd = "/usr/bin/nohup wget --no-check-certificate -q '$site/schedule_task/send_student_absent_late_record.php?RecordIDs=".OsCommandSafe(implode(',', $late_records_ids))."' > /dev/null &";
		shell_exec($cmd);
	}
}

$sql=" INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified $fields_session) VALUES $values";


$Result = $li->db_db_query($sql);


intranet_closedb();

/*
$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;
for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"msg=")!==false){
			$ary[$i]="msg=1";
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"msg=1");
}
$return_url = $filename."?".implode("&",$ary);
*/

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;

if ($Result)
	$Msg = $Lang['StudentAttendance']['PresetAbsenceCreateSuccess'];
else
	$Msg = $Lang['StudentAttendance']['PresetAbsenceCreateFail'];

for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"Msg=") != false) {
		$ary[$i]="Msg=".urlencode($Msg);
		$found = true;
		break;
	}
}
if(!$found){
	array_push($ary,"Msg=".urlencode($Msg));
}
$return_url = $filename."?".implode("&",$ary);
header("Location: $return_url");
?>
