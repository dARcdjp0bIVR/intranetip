package com.elements {
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import mx.containers.Panel;

	public class SeatPanel extends Panel {
		
		public var mainFloor:Object;
		public var mainApp:Object;
		private var _seat:AbstractSeat;
		
		public function set seat(s:AbstractSeat):void{
			_seat = s;
		}
		
		public function get seat():AbstractSeat{
			return _seat;
		}
		
		public function SeatPanel():void {
			super();
			_seat = new Seat();
			_seat.row = -1;
			_seat.colume = -1;
			_seat.xPos = this.x;
			_seat.yPos = this.y;
			_seat.isOccupied = false;
			
			//_seat.mainApp = mainApp;
			//_seat.mainFloor = mainFloor;
			
			addChild(_seat);
		}

		public function resize(h:Number):void{
			this.height = h + 22;
		}

		private function handleDown(e:Event):void{
			this.startDrag();
		}

		private function handleUp(e:Event):void{
			this.stopDrag();
			// ******************************************************
			// update the x,y info of child Seat and Person classes
			// ******************************************************
			_seat.xPos = this.x;
			_seat.yPos = this.y;
			if(_seat.isOccupied == true){
				var theOccupier:SeatOccupier = _seat.occupier;
				theOccupier.xPos = _seat.xPos;
				theOccupier.yPos = _seat.yPos;
				mainFloor.setPosition(theOccupier);			
			}
		}

		override protected function createChildren():void{
			super.createChildren();
			super.titleBar.addEventListener(MouseEvent.MOUSE_DOWN, handleDown);
			super.titleBar.addEventListener(MouseEvent.MOUSE_UP, handleUp);
		}
	}
}
