package com.elements {
	
	/**
	 * ...
	 * @author adam
	 */
	import flash.display.Sprite
	import flash.events.*;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.net.URLRequest;
	 
	import com.hexagonstar.util.debug.Debug; //Alcon Debugger
	import mx.core.UIComponent; 
	
	public class ImageContainer extends UIComponent{//Sprite	{
		
		protected var _myWidth:Number;
		protected var _myHeight:Number;
		protected var _imagePath:String;
		protected var _imageLoader:Loader;
		protected var _urlRequest:URLRequest;
		
		public function get imagePath():String{
			return _imagePath;
		}
		
		public function ImageContainer(x, y, w, h, path) {
			_myWidth = w;
			_myHeight = h;
			
			graphics.beginFill(0xCCCCCC,1.0);
			graphics.drawRect(x, y, w, h);
			graphics.endFill();
			//cacheAsBitmap = true;
			
			_imageLoader = new Loader();
			addChild(_imageLoader);
			
			_imagePath = path;
			if (_imagePath != "") {
				pasteImage();
			}
		}
		
		public function getUploadedImage(isSuccess, result):void {
			if (isSuccess == true) {	
				_imagePath = result;
				pasteImage();
			}else {
				Debug.trace("image container's error message: "+result); //error message
			}
		}
		
		protected function pasteImage():void {
			Debug.trace("image container get image path: " + _imagePath);
			_imageLoader.unload();
			
			_urlRequest = new URLRequest(_imagePath);
            _imageLoader.load(_urlRequest);

			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadedImage);
	
		}		
		
		protected function onLoadedImage(event:Event):void {
			Debug.trace("image loaded and width: " + _imageLoader.width + " / height: " + _imageLoader.height);
			//Debug.trace("container width: "+_myWidth+" / height: "+_myHeight);
			//image resize and other modification 
			if (_imageLoader.width > _imageLoader.height) {
				_imageLoader.height = _imageLoader.height * (_myWidth /_imageLoader.width);
				_imageLoader.width = _myWidth;
			}else {
				_imageLoader.width = _imageLoader.width * (_myHeight / _imageLoader.height);
				_imageLoader.height = _myHeight;
			}
			//Debug.trace("image resized - width: " + _imageLoader.width + " / height: " + _imageLoader.height);
			_imageLoader.x = (_myWidth - _imageLoader.width) / 2;
			_imageLoader.y = (_myHeight - _imageLoader.height) / 2;
		}
				
	}
	
}