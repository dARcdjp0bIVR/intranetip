package {
    import mx.controls.Label;
    import mx.controls.listClasses.*;

    public class AttendanceLabel extends Label {

		private const RED:uint = 0xFF0000;
		private const GREEN:uint = 0x00CC00;
		private const BLUE:uint = 0x0066FF;
		private const PINK:uint = 0xFF6532;
		
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            /* Set the font color based on the item price. */
            setStyle("color", (data.status == '出席')?GREEN:(data.status == '外出')?BLUE:(data.status == '遲到')?PINK:(data.status == '缺席')?RED:GREEN);
        }
    }
}

