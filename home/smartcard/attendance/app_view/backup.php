<?php 

$PATH_WRT_ROOT = "../../../../";
$PATH_WRT_ROOT_NEW = "../../../..";


switch (strtolower($parLang)) {
		case 'en':
			$intranet_hardcode_lang = 'en';
			break;
		default:
			$intranet_hardcode_lang = 'b5';
	}



include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}


if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date==""||!isset($timeslots)||$timeslots=="")header('Location:attendance_list.php');

$date = date("Y/m/d",strtotime($date));	
$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

if(!isset($status_switch) || $status_switch=="")
{
	$status_switch = "all";
}

//intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lu = new libuser();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();

$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$conds = "";
$TABLES = "";
$class_name = "";
$student_list = "";
$reason = "";
$present_count = 0;
$absent_count = 0;
$late_count = 0;
$outing_count = 0;
$status_array = "";
$student_array = array();
if($timeslots=="am")
{
	$ExpectedField = $lcardattend->Get_AM_Expected_Field("b.","c.","f.");
	$StatusField = "AMStatus";
	$DayType = PROFILE_DAY_TYPE_AM;
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);

	
}
else{
	
	$ExpectedField = $lcardattend->Get_PM_Expected_Field("b.","c.","f.");
	$StatusField = "PMStatus";
	$DayType = PROFILE_DAY_TYPE_PM;
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);

	$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k
										ON
											k.StudentID = a.UserID
											AND k.RecordDate = '".$ToDate."'
											AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
											AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
	
}


if($charactor=="class"){
    $sql="SELECT ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." FROM YEAR_CLASS WHERE YearClassID='".$id."'"; 
    $result = $libdb->returnVector($sql);
    $class_name = $result[0];
    
    $TABLES="INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID";
    $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND ycu.YearClassID='".$id."'";
    
	
}

if($charactor=="group"){
    $sql="SELECT ".Get_Lang_Selection('TitleChinese', 'Title')." FROM INTRANET_GROUP WHERE groupid='".$id."'";
    $result = $libdb->returnVector($sql);
    $class_name = $result[0];
	
    $TABLES="INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID";
    $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1) AND ug.GroupID='".$id."'";
    	
}

if($charactor=="subjectgroup"){
    $sql="SELECT ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID='".$id."'";
	$result = $libdb->returnVector($sql);
	$class_name = $result[0];
	
	$TABLES="INNER JOIN SUBJECT_TERM_CLASS_USER as su ON su.UserID=a.UserID";
	$conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND su.SubjectGroupID='".$id."'";
	
}

$sql = "    SELECT
			    b.RecordID,
             	a.UserID,		
				".getNameFieldByLang("a.")." as name,				
				IF(b.".$StatusField." IS NOT NULL,b.".$StatusField.", ".$ExpectedField."),
			    if (".$ExpectedField." = '".CARD_STATUS_ABSENT."',
					".$AbsentExpectedReasonField.",
					IF(".$ExpectedField." = '".CARD_STATUS_LATE."',
						".$LateExpectedReasonField.",
						IF(".$ExpectedField." = '".CARD_STATUS_OUTING."',
							".$OutingExpectedReasonField.",
							j.Reason))) as Reason,
				IF(d.Remark IS NULL,IF(b.".$StatusField."='".CARD_STATUS_ABSENT."' OR (b.".$StatusField." IS NULL AND ".$ExpectedField."='".CARD_STATUS_ABSENT."'),f.Remark,d.Remark),d.Remark)

			FROM
				INTRANET_USER AS a
				".$TABLES."
				LEFT OUTER JOIN $card_log_table_name AS b
				ON (a.UserID=b.UserID AND b.DayNumber = '".$txt_day."')
				LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$ToDate."' AND c.DayType = '".$DayType."')								
				LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='".$ToDate."' AND d.DayType='$DayType')
				LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate='".$ToDate."' AND f.DayPeriod='".$DayType."')
				LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
				ON j.StudentID = a.UserID 
					AND j.RecordDate = '".$ToDate."' 
					AND j.DayType = '".$DayType."' 
					AND 
					(
						j.RecordType = b.".$StatusField." 
						OR  
						(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
						AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
						)
					)
				".$AMJoinTable." 		
		    WHERE
				".$conds;
$result = $libdb->returnArray($sql);
$all_count = count($result);

for($i=0;$i<sizeof($result);$i++){
	
	if($result[$i][3]==0) $present_count++;
	if($result[$i][3]==1) $absent_count++;	
	if($result[$i][3]==2) $late_count++;	
	if($result[$i][3]==3) $outing_count++;
}

if($status_switch == "all"){
	for($j=0;$j<sizeof($result);$j++){
		list($RecordID,$user_id,$Student_name,$status,$reason,$remark) = $result[$j];
		$status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
		$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
		
		$student_array[] = $user_id;
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
	    //debug_pr($RecordID);
	    
		if($status==0){
		    $student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
		    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'>
		    		                <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
					                <p>".$Student_name."</p>
					              </a>
					          </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";		
		}
	    
	    if ($status==1){		
			$student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
			                        <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>		
				                    <p>".$Student_name."</p>		
				                    <p class='absent' id='ab_".$user_id."'></p>		
				                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Absent']."</h2>		
			                      </a>		
			                  </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";		
		}
			
		if($status==2){		
			$student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>		
			                        <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>		
				                    <p>".$Student_name."</p>		
				                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Late']."</h2>		
				                  </a>		
				              </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";					
		}		
		if($status==3){
			$student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#329700;' id='a_".$user_id."'>
	    		                    <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                    <p>".$Student_name."</p>
				                    <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Outing']."</h2>
				                  </a>
				              </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";		
		}
		
		
	}

}

if($status_switch == "present"){
	for($j=0;$j<sizeof($result);$j++){
		list($RecordID,$user_id,$Student_name,$status) = $result[$j];
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
		 
		if ($status==0){
			$student_array[] = $user_id;
				
			$status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
			$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";

		    $student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
		    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'>
		    		                <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
					                <p>".$Student_name."</p>
					              </a>
					          </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";	
		}

	}
}


if($status_switch == "absent"){
	for($j=0;$j<sizeof($result);$j++){
		list($RecordID,$user_id,$Student_name,$status) = $result[$j];
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
	  
		if ($status==1){
		    $student_array[] = $user_id;
							
		    $status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
		    $recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
		    
		    $student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
			                        <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                    <p>".$Student_name."</p>
				                    <p class='absent' id='ab_".$user_id."'></p>
				                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Absent']."</h2>
			                      </a>
			                  </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";				    
		}

	}
}

if($status_switch == "late"){
	for($j=0;$j<sizeof($result);$j++){
		list($RecordID,$user_id,$Student_name,$status) = $result[$j];
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
		 
		if($status==2){		
		    $student_array[] = $user_id;
							
    		$status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
    		$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
    		
			$student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>		
			                        <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>		
				                    <p>".$Student_name."</p>		
				                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Late']."</h2>		
				                  </a>		
				              </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";				
		}	

	}
}

if($status_switch == "outing"){
	for($j=0;$j<sizeof($result);$j++){
		list($RecordID,$user_id,$Student_name,$status) = $result[$j];
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
		 
		if($status==3){
	    	$student_array[] = $user_id;
							
    		$status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
    		$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
    		
			$student_list .= "<li><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#329700;' id='a_".$user_id."'>
	    		                    <img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                    <p>".$Student_name."</p>
				                    <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Outing']."</h2>
				                  </a>
				              </li>";

			$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					          <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";						
		}

	}
}

$student_array_string = implode(";",$student_array);
//  debug_pr($student_array);
 // die();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>

<script>
    var array_string = '<?= $student_array_string?>' ;
    var student_array = array_string.split(';');
    
	jQuery(document).ready( function() {
		$("#status_select").hide(); 
		$("#mode").val("single");
		document.getElementById("<?=$status_switch?>").selected=true;
		$('select').selectmenu('refresh', true);
		
		$("a").on("taphold",function(){
             var id = $(this).attr("id").substr(2);
             var status = $('#status_'+id).val();
             if(status==0){       
            	 var remark = $('#rmk_'+id).val()
            	 $('#remark2').val(remark);      
                 $('#cancle_btn2').attr('href','javascript:cancle2('+id+')');
                 $('#confirm_btn2').attr('href','javascript:reason_confirm2('+id+')');       
                 $('#link2').trigger("click");
             }else{
               	 var reason = $('#rea_'+id).val()
            	 $('#reason1').val(reason);                     
            	 var remark = $('#rmk_'+id).val()            	 
            	 $('#remark1').val(remark);      
                 $('#cancle_btn1').attr('href','javascript:cancle1('+id+')');
            	 $('#confirm_btn1').attr('href','javascript:reason_confirm1('+id+')');                    	 
                 $('#link1').trigger("click");

             }

                        
		});        

// 	    $( "#popup1" ).popup({
// 	        afteropen: function() {
//             	  $("#reason1").focus();
// 	        }
// 	    }); 
		 
// 		$( "#popup2" ).popup({
// 	        afteropen: function(event, ui) {
// 	        }
// 		});   


	});

	function focus1(){
        $('#remark2').focus();                      	         	 
		}
	function show_status_select(){

		 $("#mode").val("multiple");
		 $("#status_select").show();
	     $("#mutiple_select").hide();

	}

	function show_mutiple_select(){

		 $("#mode").val("single");
		 $("input[type='checkbox']").prop("checked",false).checkboxradio("refresh");
	     $("img.selected").remove();
		 $("#status_select").hide();
	     $("#mutiple_select").show();

	}
		
	function refresh(){
		   //alert("refresh");
		   document.form1.action="take_attendance.php";
		   document.form1.submit();
	}

    function choose(userID){
          if($("#mode").val()=="multiple"){
		        if(!$("#c_"+userID)[0].checked){
		    	   $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		    	   $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
		        }
		        else{
			       $("#c_"+userID).prop("checked",false).checkboxradio("refresh");	
			       $("#s_"+userID).remove();
			       
			    }
          } 
          else{
              
              if($("#status_"+userID).val()==0){
                  
             	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<h2 style='color:#d92b2b;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['Absent']?></h2> <p class='absent' id='ab_"+userID+"'></p>");
             	             	 
                 $("#status_"+userID).val(1);
              }
              
              else if($("#status_"+userID).val()==1){
                  
              	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});
              	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Late']?>").css({"color":"#0094bf"});              
              	 $("#ab_"+userID).remove();           	 
                  $("#status_"+userID).val(2);
              }

              else if($("#status_"+userID).val()==2){
                  
               	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"});
              	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Outing']?>").css({"color":"#309100"});              
	             	 
                   $("#status_"+userID).val(3);
              }

              else if($("#status_"+userID).val()==3){
                  
                  $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});

			      $("#h2_"+userID).remove();
		              
                  $("#status_"+userID).val(0);
               }              

          }
    }

    function choose_all(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if(!$("#c_"+userID)[0].checked){
            	 $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
	             $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
             }
         }
    }

    function turn_present(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});
			     $("#h2_"+userID).remove();
              	 $("#ab_"+userID).remove();           	 		             
                 $("#status_"+userID).val(0);
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();
	                                 
             }
         }

    }

    function turn_absent(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
                 if($("#status_"+userID).val()==0){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p><h2 style='color:#d92b2b;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['Absent']?></h2> ");                 
                 }
                 else if($("#status_"+userID).val()==2||$("#status_"+userID).val()==3){

                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p>");                 
                 	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Absent']?>").css({"color":"#d92b2b"});              
                 	                   	
                 }

              	          	 		             
                 $("#status_"+userID).val(1);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();
	                                 
             }
         }
    
    
    }

    function turn_late(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
                 if($("#status_"+userID).val()==0){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"}).append("<h2 style='color:#0094bf;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['Late']?></h2> ");                 
                 }
                 else{
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});                 
                 	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Late']?>").css({"color":"#0094bf"});       
                  	 $("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 }

                 $("#status_"+userID).val(2);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();
	                                 
             }
         }    
    }

    function turn_outing(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
                 if($("#status_"+userID).val()==0){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['Outing']?></h2> ");                 
                 }
                 else{
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"});                 
                 	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Outing']?>").css({"color":"#309100"});       
                  	 $("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 }

                 $("#status_"+userID).val(3);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();
	                                 
             }
         }               
    }

    function submit(){
    	 if (confirm("<?=$Lang['StudentAttendance']['TeacherApp']['AreYouSureToSubmit'] ?>"))
    	   {
    	        document.form1.action = "handle_attendance.php";
    	        document.form1.submit(); 
    	    }
    	  else{}
       
    }
    
    function back(){
		 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&timeslots=<?=$timeslots?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>";			 			 
    }
    
    function cancle1(userID){
    	$("#popup1").popup("close");	
    }  
     
    function cancle2(userID){
    	$("#popup2").popup("close");	
    }

    function reason_confirm1(userID){
    	$("#popup1").popup("close");
    	var reason = $('#reason1').val()
    	$('#rea_'+userID).val(reason);
    	var remark = $('#remark1').val()
    	$('#rmk_'+userID).val(remark);
    }
    
    function reason_confirm2(userID){
    	$("#popup2").popup("close");
    	var remark = $('#remark2').val()
    	$('#rmk_'+userID).val(remark);
    }

</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<style type="text/css">
    #body{
        
         background:#ffffff;
         text-shadow:none;
    }
    #show_class{
    
         text-shadow:none;
    }
    a:link { text-decoration: none; color: black;}
 　　a:active {text-decoration: none; color: black !important;} 
 　　a:hover {text-decoration: none;color: black !important;}  
    a:visited {text-decoration: none;color: black !important;} 
   
   	.header a:link { text-decoration: none; color: white;}
 　　.header a:active {text-decoration: none; color: white !important;} 
 　　.header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
		
    .student_list {
    }
	.student_list .ui-content{
       padding:0;

    }
	.student_list .ui-listview li {
		float: left;
        width: 80px;/*用百分比可以定個數，用px是按寬度改變*/
		height: 104px;
        margin: 10px;
        margin-top:0 ;
	    margin-left:10px ;		
	}
	.student_list .ui-listview li > .ui-btn {
		-webkit-box-sizing: border-box; /* include padding and border in height so we can set it to 100% */
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		box-sizing: border-box;
		height: 100%;
		text-shadow:none;/*no shadow*/
		width:80px;
		padding-right:0;
 		border:2px; 
 		border-style: solid;  
        border-color:#75b70d;
        
	}
	.student_list .ui-listview li.ui-li-has-thumb .ui-li-thumb {
		height: auto; /* To keep aspect ratio. */
		max-width: 100%;
		max-height: none;
	}
	/* Make all list items and anchors inherit the border-radius from the UL. */
	.student_list .ui-listview li,
	.student_list .ui-listview li .ui-btn,
	.student_list .ui-listview .ui-li-thumb {
		-webkit-border-radius: inherit;
		border-radius: inherit;
		padding:0 !important;
		
	}
	/* Hide the icon */
	.student_list .ui-listview .ui-btn-icon-right:after {
		display: none;
	}
	/* Make text wrap. */
	.student_list .ui-listview p {
		white-space: normal;
		overflow: visible;
		position: absolute;
		left: 0;
		right: 0;
	}
	/* Text position */
	.student_list .ui-listview p {
		font-size: 0.8em;
		margin: 0;
        text-align:center;
		min-height: 20%;
		bottom:0;
		width:80px !important;
	}
	/* Semi transparent background and different position if there is a thumb. The button has overflow hidden so we don't need to set border-radius. */
	.ui-listview .ui-li-has-thumb p {
		background: rgba(255,255,255,.7);
	}

	.ui-listview .ui-li-has-thumb p {
		min-height: 20%;
	}
	.student_list .ui-listview .absent {
		width: auto;
		min-height: 100%;
		top: 0;
		left: 0;
		bottom: 0;
		background: rgba(255,255,255,.7);
		-webkit-border-top-right-radius: inherit;
		border-top-right-radius: inherit;
		-webkit-border-bottom-left-radius: inherit;
		border-bottom-left-radius: inherit;
		-webkit-border-bottom-right-radius: 0;
		border-bottom-right-radius: 0;
		
	}

	/* Animate focus and hover style, and resizing. */
	.student_list .ui-listview li,
	.student_list .ui-listview .ui-btn {
		-webkit-transition: all 500ms ease;
		-moz-transition: all 500ms ease;
		-o-transition: all 500ms ease;
		-ms-transition: all 500ms ease;
		transition: all 500ms ease;
	}
    .student_list .ui-listview .selected {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		right: 0;
    	bottom: 0;
    	opacity:0.8;
    	padding-right:2px;
        padding-bottom:2px;    	
        width:30%;
        z-index:1;   
    }
    
    .student_list .ui-listview h2 {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		top: 0;
		right: 0; 
	    font-size: 1em;
 	    margin-top:0.3em; 
 	    margin-right:0.3em; 
		text-shadow:0 0 2px #FFFFFF;
		z-index:1;
         }
</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

		
</head>
	<body>
	
		<div data-role="page" id="body"  class="student_list">			

			<form name="form1" method="post" id="form1">
			
			    <div data-role="header" data-position="fixed" style="border:0;height:80px">
			        

			        <div id="show_class" style ="height:60px;">	                
			
			           <table width="100%" style ="background-color:#01AE0E;height:60px;font-size:1em;font-weight:normal;">
			
			             <tr>
			
			                <td style="padding-left:0em;">
			                   <table> 
			                     <tr><td rowspan="2">
			                    <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
			                     </td><td><?=$class_name?>
			                     </td></tr>
			                     <tr><td><?=$date?></td></tr>
			                   </table>
			                </td>
			
			                <td  align= "right" style="padding-right:0.5em;">
								
								    <select name="status_switch" id="status_switch" data-mini="true" onchange="refresh()">
								
								        <option value="all" id="all"><?=$Lang['Btn']['All']?> (<?=$all_count?>)</option>
								 						
								        <option value="present" id="present"><?=$Lang['StudentAttendance']['Present']?> (<?=$present_count?>)</option>
								        
										<option value="absent" id="absent"><?=$Lang['StudentAttendance']['Absent']?> (<?=$absent_count?>)</option>
								
								        <option value="late" id="late"><?=$Lang['StudentAttendance']['Late']?> (<?=$late_count?>)</option>							
								
								        <option value="outing" id="outing"><?=$Lang['StudentAttendance']['Outing']?> (<?=$outing_count?>)</option>
								
								    </select>								
								
			                </td>	              
			
			              </tr>
			
			            </table>
			
			          </div>
			
			          <div id="mutiple_select" align="right" style="background-color:white;height: 30px;font-size:1em;">     
			            <table style="">
			               <tr>
			                 <td>
			                    <a href="javascript:show_status_select();" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change.png" style="width:1.4em; height:1.4em;" /></a>
			                 </td>
			                 <td style="padding-right: 1em">
			                    <a href="javascript:show_status_select();" ><?=$Lang['AccountMgmt']['MultiSelect']?></a>
			                 </td>
			               </tr>
			               
			            </table>
			          </div>
			          <div id="status_select" style="background-color:white;height: 30px;font-size:1em;" >
			            <table style="width:100%">
			               <tr>
			                 <td style="padding-left: 0.3em;">
			                    <a href="javascript:choose_all();" ><?=$Lang['Btn']['SelectAll']?></a>
			                 </td>
			                 <td>
				                 <table>
					                 <tr>
						                 <td >
						                    <a href="javascript:show_mutiple_select()" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change_select.png" style="width:1.4em; height:1.4em;" /></a>
						                 </td>
						                 <td >
						                    <a href="javascript:show_mutiple_select()" style="color: grey;"><?=$Lang['StudentAttendance']['TurnTo']?></a>
						                 </td>
					                 </tr>
				                 </table>
			                 </td>
			                 <td >
			                    <a href="javascript:turn_present();" ><?=$Lang['StudentAttendance']['Present']?></a>
			                 </td>
			                 <td >
			                    <a href="javascript:turn_late();" ><?=$Lang['StudentAttendance']['Late']?></a>
			                 </td>
			                 <td >
			                    <a href="javascript:turn_absent();" ><?=$Lang['StudentAttendance']['Absent']?></a>
			                 </td>
			                 <td style="padding-right:0.1em">
			                    <a href="javascript:turn_outing();" ><?=$Lang['StudentAttendance']['Outing']?></a>
			                 </td>
			               </tr>
			               
			            </table>			
			          </div>		
			          
			       </div>
			
	            <div data-role="content">
	            
	            
	                 <ul data-role="listview" data-inset="true">
	                 
	                    <?=$student_list?>           
	            	            
			         </ul>
	            </div>

	            <div data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">
	               <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#3a960e;display:block;margin-left:10px;margin-right:10px;
	                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$Lang['StudentAttendance']['Confirm']?></a>

	            </div>	 
	            	            
	            <a id='link1' href='#popup1'  data-rel='popup' data-position-to='window' class='ui-btn ui-corner-all' data-transition='pop' style='display: none;'></a>
			    <div data-role='popup' id='popup1' data-theme='a' class='ui-corner-all'>
					 <div style='padding:10px 20px;'>
		                  <h4><?=$Lang['StudentAttendance']['Reason']?></h4>
						  <input type='text' value = '' id='reason1' data-theme='a' >
						  <h4><?=$Lang['StudentAttendance']['Remark']?></h4>
						  <input type='text' value = '' id='remark1' data-theme='a'>
						  <div  align='center'>
							   <a href='' data-role='button' data-inline='true' id='cancle_btn1'><?=$Lang['Btn']['Cancel']?></a>	   							   
							   <a href='' data-role='button' data-inline='true' id='confirm_btn1'><?=$Lang['Btn']['Confirm']?></a>
						  </div>
		             </div>
		        </div>
	            <a id='link2' href='#popup2' data-rel='popup' data-position-to='window' class='ui-btn ui-corner-all' data-transition='pop' style='display: none;'></a>
			    <div data-role='popup' id='popup2' data-theme='a' class='ui-corner-all'>
					 <div style='padding:10px 20px;'>
						  <h4><?=$Lang['StudentAttendance']['Remark']?></h4>
						  <input type='text' value = '' id='remark2' data-theme='a'>
						  <div  align='center'>
							   <a href='' data-role='button' data-inline='true' id='cancle_btn2'><?=$Lang['Btn']['Cancel']?></a>	   							   
							   <a href='' data-role='button' data-inline='true' id='confirm_btn2'><?=$Lang['Btn']['Confirm']?></a>
						  </div>
		             </div>
		        </div>
		        
  	    	    <input type="hidden" value = "<?=$id?>" name="id">
			    <input type="hidden" value = "<?=$charactor?>" name="charactor">
			    <input type="hidden" value = "<?=$date?>" name="date">
			    <input type="hidden" value = "<?=$timeslots?>" name="timeslots">
			    <input type="hidden" value = "<?=$student_array_string?>" name="student_list">
			    <input type="hidden" name="mode" id="mode"> 
			    <input type="hidden" value=<?=$token?> name="token">
                <input type="hidden" value=<?=$uid?> name="uid">
                <input type="hidden" value=<?=$ul?> name="ul">
                <input type="hidden" value=<?=$parLang?> name="parLang">                      
                 <?=$reason_submit?>      
			     <?=$status_array?> 
			     <?=$recordID_array?>

			</form>


		</div>

	</body>
</html>

<?php

intranet_closedb();

?>