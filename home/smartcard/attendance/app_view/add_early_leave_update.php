<?php
/*
 *  2019-05-29 Cameron
 *      - synchronize eAttendance record status to eSchoolbus [case #Y161736]
 */
$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_POST['parLang'])? $_POST['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

$uid = IntegerSafe($_POST['uid']);
$studentID = IntegerSafe($_POST['studentID']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record) + 0;

$libdb = new libdb();
$lu = new libuser($UserID);
$lcardattend = new libcardstudentattend2();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
$card_student_daily_log .= $txt_year."_";
$card_student_daily_log .= $txt_month;

$leave_hour = substr($early_leave_time, 0, 2);

if($timeslots == "am") {

    $DayType = PROFILE_DAY_TYPE_AM;
    $leave_status = 1;

}
else if($timeslots == "pm"){
    $DayType = PROFILE_DAY_TYPE_PM;
    $leave_status = 2;
}else{
    $DayType = PROFILE_DAY_TYPE_AM;
    $leave_status = 1;
}

if($leavePlace == "")	# Check Leave School Station is Empty or not
{
    $sql_leave_station = "LeaveSchoolStation = NULL";
}
else
{
    $sql_leave_station = "LeaveSchoolStation = '". $leavePlace ."'";
}

$sql = "
		UPDATE 
			$card_student_daily_log
		SET
			LeaveSchoolTime = '$leaveTime',
			$sql_leave_station,				
			LeaveStatus = $leave_status,
			DateModified = NOW()
		WHERE
			UserID = '$studentID' 
			AND DayNumber = $txt_day
		";
$Result['SetLeaveStatus'] = $libdb->db_db_query($sql);

$waived_insert = $waived==1?1:0;
$document_status = $proveDocument==1?1:0;


$Result['SetProfile'] = $lcardattend->Set_Profile($studentID,$ToDate,$DayType,PROFILE_TYPE_EARLY,$reason,"|**NULL**|","|**NULL**|","|**NULL**|","",false,$waived_insert,true);

$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='$document_status' WHERE RecordDate='$ToDate' AND StudentID='$studentID' AND DayType='$DayType' AND RecordType='".PROFILE_TYPE_EARLY."'";
$Result['Document'] = $lcardattend->db_db_query($sql);
$Result['TeacherRemark'] = $lcardattend->updateTeacherRemark($studentID,$ToDate,$DayType,$teacherRemark);

if ($plugin['eSchoolBus']) {
    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($studentID);
    if ($isTakingBusStudent) {
        $teacherRemark = trim($teacherRemark);
        if($has_magic_quotes){
            $teacherRemark = stripslashes($teacherRemark);
        }
        $dayTypeStr = 'PM';
        
        $sql = "SELECT RecordID FROM $card_student_daily_log WHERE UserID = '$studentID' AND DayNumber = '$txt_day'";
        $dailyLogAry = $lcardattend->returnResultSet($sql);
        if (count($dailyLogAry)) {
            $my_record_id = $dailyLogAry[0]['RecordID'];
        }
        else {
            $my_record_id = 0;
        }
        // check if apply leave record already exist
        $isAppliedLeave = $leSchoolBus->isTimeSlotAppliedLeave($studentID, $ToDate, $ToDate, $dayTypeStr, $dayTypeStr);
        if ($isAppliedLeave) {
            // update reason and teacher's remark only
            $Result['UpdateReasonAndRemark_'.$my_record_id] = $leSchoolBus->updateReasonAndRemark($studentID, $ToDate, $my_record_id, $reason, $teacherRemark);
        }
        else {    // add record
            $Result['SyncAbsentRecordToeSchoolBus_'.$my_record_id] = $leSchoolBus->syncAbsentRecordFromAttendance($studentID, $ToDate, $reason, $teacherRemark, $dayTypeStr, $my_record_id, $attendanceType=3);
        }
    }
}

intranet_closedb();
if($Result['SetLeaveStatus']){
    $msg=1;
}else{
    $msg=0;
}

header('Location:attendance_list.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&is_early_leave='.$is_early_leave.'&parLang='.$parLang.'');

?>