<?php
// Editing by
/*
 * 2019-10-03 by Cameron: - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
 * should apply updateSchoolBusTimeSlotAttendanceRecordID() and updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
 * 2019-05-29 by Cameron: synchronize eAttendance record status to eSchoolBus [case #Y161736]
 * 2018-07-23 by Carlos: Fixed office remark not applied issue. 
 * 2018-03-12 by Carlos: Only update [Submitted Prove Document] when setting TeacherCanManageProveDocumentStatus is on.
 * 2018-02-09 by Carlos: Modified "INSERT INTO CARD_STUDENT_DAILY_LOG_YYYY_MM" query with "ON DUPLICATE KEY UPDATE" statement to avoid situation that no RecordID submitted from UI but actually it has generated the records.
 * 2017-11-16 by Isaac: [appview enhancement] changed 'store the settings for show student name or not' which spilt into 3 type of info to store in DB
 * 2017-05-31 by Carlos: [ip.2.5.8.7.1] Changed $my_record_status from |**NULL**| to NULL to make the record as confirmed.
 * 										Check and skip outdated record if attendance record last modified time is later than page load time.
 * 2017-02-27 by Carlos: [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
 */

$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_POST['parLang'])? $_POST['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

$uid = IntegerSafe($_POST['uid']);
$id = IntegerSafe($_POST['id']);
$RoomAllocationID = IntegerSafe($_POST['RoomAllocationID']);
$AttendOverviewID = IntegerSafe($_POST['AttendOverviewID']);
$student_list = IntegerSafe($student_list,";");

$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$isShowStudentName = standardizeFormPostValue($_POST['isShowStudentName']);
$isShowStudentInfoName = standardizeFormPostValue($_POST['isShowStudentInfoName']);
$isShowStudentInfoClassAndNo = standardizeFormPostValue($_POST['isShowStudentInfoClassAndNo']);

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date==""||!isset($timeslots)||$timeslots==""||!isset($mode)||$mode=="")header('Location:attendance_list.php');

$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$student_array = explode(";",$student_list);

$libdb = new libdb();
$lu = new libuser($UserID);
$isStudentHelper = $lu->isStudent();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();
$LessonAttendUI = new libstudentattendance_ui();
$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';
$use_magic_quotes = get_magic_quotes_gpc();

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

#store the settings for what student info to be shown
$settingsAssoAry = array();
$settingsAssoAry['eAttendance_ShowStudentName'] = $isShowStudentName;
$settingsAssoAry['eAttendance_ShowStudentInfoName'] = $isShowStudentInfoName;
$settingsAssoAry['eAttendance_ShowStudentInfoClassAndNo'] = $isShowStudentInfoClassAndNo;
foreach ((array)$settingsAssoAry as $_key => $_val) {
    $sql="select * from APP_USER_SETTING where UserID='".$UserID."' and SettingName='".$_key."'";
    $getShowStudentNameArray = $libdb->returnArray($sql);
    if(sizeof($getShowStudentNameArray)==0){
        $insertSettingSQL="INSERT INTO APP_USER_SETTING (UserID,SettingName,SettingValue,DateModified,ModifiedBy) VALUES ('".$UserID."','".$_key."','".$_val."',NOW(),".$UserID.") ";
        $libdb->db_db_query($insertSettingSQL);
    }else{
        $updateSettingSQL="UPDATE APP_USER_SETTING set SettingValue ='".$_val."', DateModified = NOW(), ModifiedBy='".$UserID."' where UserID='".$UserID."' and SettingName = '".$_key."'";
        $libdb->db_db_query($updateSettingSQL);
    }
    
}

if($charactor == "lesson"){
    if($LessonAttendUI->Is_Lesson_Plan_Outdated($id, $ToDate, $RoomAllocationID, $PageLoadTime)){
        $msg = 99;  // data outdated
        header('Location:take_attendance.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&id='.$id.'&taken='.$taken.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'&RoomAllocationID='.$RoomAllocationID.'&AttendOverviewID='.$AttendOverviewID.'&Start_time='.$Start_time.'&End_time='.$End_time.'&LessonTitle='.$LessonTitle.'&lessonType='.$lessonType);
        exit();
    }
    
    for($i=0;$i<sizeof($student_array);$i++){
        $student_status = $_POST['status_'.$student_array[$i].''];
        //change back to 0:present, 1:outing, 2:late, 3:absent to fit the sql
        if($_POST['status_'.$student_array[$i].'']==0)$student_status=0;
        if($_POST['status_'.$student_array[$i].'']==1)$student_status=3;
        if($_POST['status_'.$student_array[$i].'']==2)$student_status=2;
        if($_POST['status_'.$student_array[$i].'']==3)$student_status=1;
        $student_remark = trim(htmlspecialchars_decode(stripslashes(${"rmk_".$student_array[$i]}), ENT_QUOTES));
        
        $StudentLessonArray[$i][0] = $student_array[$i];
        $StudentLessonArray[$i][1] = $student_status;
        $StudentLessonArray[$i][2] = $student_remark;
        $StudentLessonArray[$i][3] = 0;
    }
    
    if($StudentLessonArray[0][0] != ""){
        $AttendOverviewID = $lcardattend->Save_Lesson_Plan($id, $ToDate, $RoomAllocationID, $Start_time, $End_time,"","","","");
        if($lcardattend->Save_Lesson_Attendance_Data($AttendOverviewID, $StudentLessonArray)){
            $lcardattend->Clone_Class_Lesson_Attendance_Overview_And_Students($ToDate, $AttendOverviewID);
            $msg = 1;
        }
    }
}else{
    /*
     //CHECK RECORD UPDATED AFTER LOAD THE SUBMIT PAGE, CHECK DATA CONSISTENCE
     if($lcardattend->isDataOutdated($student_array,$ToDate,$PageLoadTime)){
     $msg = 99;  // data outdated
     header('Location:take_attendance.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&id='.$id.'&taken='.$taken.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'');
     exit();
     }
     */
    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
    $my_record_date = $ToDate;
    
    if ($timeslots == "am"){
        $DayType = PROFILE_DAY_TYPE_AM;
        $StatusField = "AMStatus";
        $ConfirmField = "IsConfirmed";
        $ConfirmByField = "ConfirmedUserID";
        
        $DateModifiedField = "DateModified";
        $ModifyByField = "ModifyBy";
        $dayTypeStr = 'AM';
    }
    else {
        $DayType = PROFILE_DAY_TYPE_PM;
        $StatusField = "PMStatus";
        $ConfirmField = "PMIsConfirmed";
        $ConfirmByField = "PMConfirmedUserID";
        
        $DateModifiedField = "PMDateModified";
        $ModifyByField = "PMModifyBy";
        $dayTypeStr = 'PM';
    }
    
    if(sizeof($student_array)>0){
        
        $student_list1 = "'".implode("','",$student_array)."'";
        
        ## Get Remark list
        $sql = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='$my_record_date' AND StudentID IN ( $student_list1) AND DayType='$DayType'";
        $r = $libdb->returnArray($sql,2);
        for($i=0;$i<sizeof($r);$i++){
            $rid = $r[$i][0];
            $student_id = $r[$i][1];
            $remarkResult[$student_id]['RecordID']= $rid;
        }
    }
    
    $studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$ToDate,'StudentID'=>$student_array,'StudentIDToRecord'=>1));
    
    $appliedLeaveStudentAry = $lcardattend->checkStudentApplyLeave($student_array,$my_record_date,$DayType);
    $appliedLeaveStudentIDAry = array_keys($appliedLeaveStudentAry);
    
//    $insertRemarkSQL="INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark) VALUES ";
//    $removeRemarkSQL="DELETE FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID IN( ";
//    $insertRemark = false;
//    $removeRemark = false;
    for($i=0; $i<sizeof($student_array); $i++){
        $my_user_id = $student_array[$i];
        $my_day = $txt_day;
        $my_drop_down_status = $_POST['status_'.$student_array[$i].''];
        $my_record_id = IntegerSafe($_POST['recordID_'.$student_array[$i].'']);
        
        // check and skip outdated record if attendance record last modified time is later than page load time
        if(isset($studentIdToDailyLog[$my_user_id])){
            if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
            {
                $am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
                if($am_modified_ts >= $PageLoadTime) continue;
            }
            if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
            {
                $pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
                if($pm_modified_ts >= $PageLoadTime) continue;
            }
        }
        
        $remark_record_id = $remarkResult[$my_user_id]['RecordID'];
        $my_reason = trim($_POST["rmk_".$my_user_id]); 
        //$my_reason = trim(htmlspecialchars_decode(stripslashes(${"rmk_".$my_user_id}), ENT_QUOTES));
		 // Retrieve Waived only if absent/ late

	    $my_record_status = ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE || $my_drop_down_status==CARD_STATUS_OUTING)? (${"waivedStatus_".$my_user_id} == '1'?1:0) :'|**NULL**|';
	    if(!$lcardattend->TeacherCanManageWaiveStatus){
	     	$my_record_status = 'NULL';
	    }
	    
        $upadteeDisLateRecord = $lcardattend->OnlySynceDisLateRecordWhenConfirm != '1';
        
        $my_late_session = trim(htmlspecialchars_decode(stripslashes(${"late_".$my_user_id}), ENT_QUOTES));
        $my_request_leave_session = trim(htmlspecialchars_decode(stripslashes(${"request_".$my_user_id}), ENT_QUOTES));
        $my_play_truant_session = trim(htmlspecialchars_decode(stripslashes(${"playTruant_".$my_user_id}), ENT_QUOTES));
        $my_offical_leave_session = trim(htmlspecialchars_decode(stripslashes(${"offical_".$my_user_id}), ENT_QUOTES));
        $my_absent_session = trim(htmlspecialchars_decode(stripslashes(${"absent_".$my_user_id}), ENT_QUOTES));
        
        if($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
            
            $sql = "select
				            	LateSession,
								RequestLeaveSession,
								PlayTruantSession,
								OfficalLeaveSession,
								AbsentSession
							from
								CARD_STUDENT_STATUS_SESSION_COUNT
							where
								RecordDate = '".$my_record_date."'
								AND
								StudentID = '".$my_user_id."'
								AND
								DayType = '".$DayType."' ";
            $OrgRecord = $lcardattend->returnArray($sql);
            
            if ($OrgRecord[0]['LateSession'] != $my_late_session
                || $OrgRecord[0]['RequestLeaveSession'] != $my_request_leave_session
                || $OrgRecord[0]['PlayTruantSession'] != $my_play_truant_session
                || $OrgRecord[0]['OfficalLeaveSession'] != $my_offical_leave_session
                || $OrgRecord[0]['AbsentSession'] != $my_absent_session) {
                    # Update Daily table
                    $sql = "UPDATE ".$card_log_table_name." SET
	  							".$DateModifiedField."=now(),
									".$ModifyByField."='".$_SESSION['UserID']."'
							WHERE
								DayNumber = '".$my_day."'
	    					and
								UserID='".$my_user_id."'";
                    $Result['UpdateLastModify:'.$my_user_id] = $lcardattend->db_db_query($sql);
                }
                
                if (sizeof($OrgRecord) > 0) {
                    $sql = "update CARD_STUDENT_STATUS_SESSION_COUNT set
									LateSession = '".$my_late_session."',
									RequestLeaveSession = '".$my_request_leave_session."',
									PlayTruantSession = '".$my_play_truant_session."',
									OfficalLeaveSession = '".$my_offical_leave_session."',
									AbsentSession = '".$my_absent_session."',
									DateModify = NOW(),
									ModifyBy = '".$_SESSION['UserID']."'
								where
									RecordDate = '".$my_record_date."'
									AND
									StudentID = '".$my_user_id."'
									AND
									DayType = '".$DayType."'";
                }
                else {
                    // insert/update the number of sessions to db
                    $sql = "insert into CARD_STUDENT_STATUS_SESSION_COUNT (
									RecordDate,
									StudentID,
									DayType,
						            LateSession,
									RequestLeaveSession,
									PlayTruantSession,
									OfficalLeaveSession,
									AbsentSession,
									DateInput,
									InputBy,
									DateModify,
									ModifyBy
									)
								values (
									'".$my_record_date."',
									'".$my_user_id."',
									'".$DayType."',
									'".$my_late_session."',
									'".$my_request_leave_session."',
									'".$my_play_truant_session."',
									'".$my_offical_leave_session."',
									'".$my_absent_session."',
									NOW(),
								  '".$_SESSION['UserID']."',
								  NOW(),
								  '".$_SESSION['UserID']."'
								 )
							";
                }
                $Result['UpdateSessions:'.$my_user_id] = $lcardattend->db_db_query($sql);
        }
        
		
		
//        if($my_reason!=""){
//            # insert
//            if($remark_record_id==""){
//                $insertRemark = true;
//                $insertRemarkSQL .="('$my_user_id','$my_record_date','$DayType','".$libdb->Get_Safe_Sql_Query($my_reason)."'),";
//            }
//            # update
//            else{
//                $updateRemarkSQL ="UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='".$libdb->Get_Safe_Sql_Query($my_reason)."' WHERE RecordID='$remark_record_id'";
//                $libdb->db_db_query($updateRemarkSQL);
//            }
//        }else if( ( ($my_drop_down_status!=CARD_STATUS_ABSENT && $my_drop_down_status!=CARD_STATUS_LATE)
//            || !$isStudentHelper) && $remark_record_id!=""){
//                # remove Remarks if :
//                # 1. not absent/late
//                # 2. remark is null , and not student helper
//                $removeRemark=true;
//                $removeRemarkSQL .=$remark_record_id.",";
//        }
        
        //$sql = "SELECT * FROM $card_log_table_name WHERE UserID='$my_user_id' AND DayNumber='$my_day'";
        //$daily_log_record = $libdb->returnArray($sql);
        //$my_record_id = count($daily_log_record)>0? $daily_log_record[0]['RecordID'] : '';
        
        if($my_record_id==""){
            $sql = "INSERT INTO $card_log_table_name
				(UserID,DayNumber,".$StatusField.",".$ConfirmField.",".$ConfirmByField.",DateInput,InputBy,".$DateModifiedField.",".$ModifyByField.")
						VALUES
						('$my_user_id','$my_day','$my_drop_down_status','1','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."') 
					ON DUPLICATE KEY UPDATE ".$StatusField."='$my_drop_down_status',".$ConfirmField."='1',".$ConfirmByField."='".$_SESSION['UserID']."',".$DateModifiedField."='".$_SESSION['UserID']."',".$ModifyByField."='".$_SESSION['UserID']."',RecordID=LAST_INSERT_ID(RecordID) ";
            $insert_success = $libdb->db_db_query($sql);
            $logRecordID = $libdb->db_insert_id();
            
            //$_POST['SQL_'.$my_user_id] = $sql." - ".($insert_success?'1':'0');
            # Check Bad actions
            if ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)           # empty -> present
            {
                $lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
                # Forgot to bring card
                $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,"");
            }
            $RealInTime = "";
            
            $log_row .= '['.$my_record_date.' '.$DayType.' '.$my_user_id.' NULL '.$my_drop_down_status.']';
            
            if ($plugin['eSchoolBus']) {
                $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
                if ($isTakingBusStudent) {
                    if ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_OUTING) {

                        if (in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
                            $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                            $conditionAry = array();
                            $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                            $conditionAry['StudentID'] = $my_user_id;
                            $conditionAry['StartDate'] = $my_record_date;
                            $Result[$i.'_updateSchoolBusAttendanceRecordID'] = $leSchoolBus->updateSchoolBusAttendanceRecordID($logRecordID, $conditionAry);

                            unset($conditionAry);
                            $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                            $conditionAry['t.LeaveDate'] = $my_record_date;
                            $Result[$i.'_updateSchoolBusTimeSlotAttendanceRecordID'] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($my_user_id, $logRecordID, $conditionAry);
                        }
                        else {
                            $teacherRemark = $my_reason;
                            $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $my_record_date, $my_reason, $teacherRemark, $dayTypeStr, $logRecordID);
                        }
                    }
                    else if (($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE) && (in_array($my_user_id,(array)$appliedLeaveStudentIDAry))){
                        $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                        $conditionAry = array();
                        $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                        $conditionAry['StudentID'] = $my_user_id;
                        $conditionAry['StartDate'] = $my_record_date;
                        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                        $Result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                        unset($conditionAry);
                        $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                        $conditionAry['t.LeaveDate'] = $my_record_date;
                        $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                        $Result['ChangeFromAbsentToOthers_Timeslot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
                    }
                }
            }
            
        }
        else {
            
            # Grab original status
            $sql = "SELECT ".$StatusField.", InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
            $temp = $libdb->returnArray($sql,5);
            list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
            
            ## get old time
            if ($lcardattend->attendance_mode==1 || $DayType == PROFILE_DAY_TYPE_AM){ # PM only
                $RealInTime = $old_inTime;
            }else{
                $RealInTime = $old_lunchBackTime;
            }
            
            if ($old_status != $my_drop_down_status)
            {
                if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                {
                    $lcardattend->removeBadActionNoCardEntrance($my_user_id,$my_record_date);
                    if($bad_action_time_field!=""){ # has time but absent
                        # Has card record but not present in classroom
                        $lcardattend->addBadActionFakedCard($my_user_id,$my_record_date,$RealInTime,$DayType);
                    }
                }
                
                # Absent -> Late / Present
                if (($old_status==CARD_STATUS_ABSENT || trim($old_status) == "") && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                {
                    $lcardattend->removeBadActionFakedCard($my_user_id,$my_record_date,$DayType);
                    # forgot to bring card
                    if(trim($old_inTime)=='' && trim($old_lunchBackTime)==''){ // add no card entrance log if and only if no both AM in time and PM in time
                        $lcardattend->addBadActionNoCardEntrance($my_user_id,$my_record_date,$RealInTime);
                    }
                }
                
                if ($plugin['eSchoolBus']) {
                    
                    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
                    if ($isTakingBusStudent) {
                        # Late / Present -> Absent / Outgoing
                        if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING)) {
                            $teacherRemark = $my_reason;
                            if($use_magic_quotes){
                                $teacherRemark = stripslashes($teacherRemark);
                            }
                            
                            $attendanceApplyLeaveID = $appliedLeaveStudentAry[$my_user_id];
                            $isSchoolBusApplyLeaveRecordExist = $leSchoolBus->isSchoolBusApplyLeaveRecordExist($attendanceApplyLeaveID, $my_user_id, $my_record_date, $dayTypeStr);
                            
                            if (in_array($my_user_id,(array)$appliedLeaveStudentIDAry) && $isSchoolBusApplyLeaveRecordExist) {
                                $conditionAry = array();
                                $conditionAry['AttendanceRecordID'] = $my_record_id;
                                $conditionAry['StudentID'] = $my_user_id;
                                $conditionAry['StartDate'] = $my_record_date;
                                $Result[$i.'_updateSchoolBusAttendanceRecordID'] = $leSchoolBus->updateSchoolBusAttendanceRecordID($my_record_id, $conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceRecordID'] = $my_record_id;
                                $conditionAry['t.LeaveDate'] = $my_record_date;
                                $Result[$i.'_updateSchoolBusTimeSlotAttendanceRecordID'] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($my_user_id, $my_record_id, $conditionAry);
                            }
                            else {
                                $Result[$i.'_SyncAbsentRecordToeSchoolBus'] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $my_record_date, $my_reason, $teacherRemark, $dayTypeStr, $my_record_id);
                            }
                        }

                        $deleteApplyLeave = false;
                        # Absent / Outgoing -> Late / Present
                        if (($old_status==CARD_STATUS_ABSENT || $old_status==CARD_STATUS_OUTING) && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE)) {
                            $conditionAry = array();
                            $conditionAry['AttendanceRecordID'] = $my_record_id;
                            $conditionAry['StudentID'] = $my_user_id;
                            $conditionAry['StartDate'] = $my_record_date;
                            $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                            unset($conditionAry);
                            $conditionAry['t.AttendanceRecordID'] = $my_record_id;
                            $conditionAry['t.LeaveDate'] = $my_record_date;
                            $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_TimeSlot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);

                            $deleteApplyLeave = true;       // Use AttendanceApplyLeaveID to filter rather than AttendanceRecordID in case AttendanceRecordID is empty
                            $attendanceApplyLeaveID= $appliedLeaveStudentAry[$my_user_id];
                        }

                        # Present -> Late && have applied leave
                        if ($old_status==CARD_STATUS_PRESENT && $my_drop_down_status==CARD_STATUS_LATE && in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
                            $deleteApplyLeave = true;
                            $attendanceApplyLeaveID= $appliedLeaveStudentAry[$my_user_id];
                        }
                        
                        # Late -> Present && have applied leave
                        if ($old_status==CARD_STATUS_LATE  && $my_drop_down_status==CARD_STATUS_PRESENT && in_array($my_user_id,(array)$appliedLeaveStudentIDAry)) {
                            $deleteApplyLeave = true;
                            $attendanceApplyLeaveID = $appliedLeaveStudentAry[$my_user_id];
                        }
                        
                        if ($deleteApplyLeave) {
                            $conditionAry = array();
                            $conditionAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
                            $conditionAry['StudentID'] = $my_user_id;
                            $conditionAry['StartDate'] = $my_record_date;
                            $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                            unset($conditionAry);
                            $conditionAry['t.AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
                            $conditionAry['t.LeaveDate'] = $my_record_date;
                            $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_TimeSlot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
                        }
                    }
                }
                
                $LastModifyUpdate = ", ".$DateModifiedField."=now(),
				                            ".$ModifyByField."='".$_SESSION['UserID']."' ";
            }
            else {
                $LastModifyUpdate = "";
                if ($plugin['eSchoolBus']) {
                    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
                    if ($isTakingBusStudent) {
                        
                        if (($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING)) {
                            $teacherRemark = $my_reason;
                            if($use_magic_quotes){
                                $teacherRemark = stripslashes($teacherRemark);
                            }
                            
                            // update reason and teacher's remark only
                            $Result['UpdateReasonAndRemark_'.$my_record_id] = $leSchoolBus->updateReasonAndRemark($my_user_id, $my_record_date, $my_record_id, $my_reason, $teacherRemark);
                        }
                        else if (($my_drop_down_status == CARD_STATUS_PRESENT || $my_drop_down_status == CARD_STATUS_LATE) && (in_array($my_user_id,(array)$appliedLeaveStudentIDAry))){
                            $appliedLeaveRecordID = $appliedLeaveStudentAry[$my_user_id];
                            $conditionAry = array();
                            $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                            $conditionAry['StudentID'] = $my_user_id;
                            $conditionAry['StartDate'] = $my_record_date;
                            $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                            unset($conditionAry);
                            $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                            $conditionAry['t.LeaveDate'] = $my_record_date;
                            $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                            $Result['ChangeFromAbsentToOthers_TimeSlot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
                        }
                    }
                }
            }
            # Update Daily table
            $sql = "UPDATE $card_log_table_name SET
						     ".$StatusField."='".$my_drop_down_status."',
							 ".$ConfirmField." = '1',
							 ".$ConfirmByField."='".$_SESSION['UserID']."'
							 ".$LastModifyUpdate."
				       	 WHERE RecordID='".$my_record_id."'";
            $update_success =  $libdb->db_db_query($sql);
            //$_POST['SQL_'.$my_user_id] = $sql." - ".($update_success?'1':'0');
            
            $log_row .= '['.$my_record_date.' '.$DayType.' '.$my_user_id.' '.$old_status.' '.$my_drop_down_status.']';
        }
        
        
        # Try to remove profile records if applicable
        if ($my_drop_down_status == CARD_STATUS_ABSENT || $my_drop_down_status == CARD_STATUS_LATE || $my_drop_down_status == CARD_STATUS_OUTING) {
            // cust field
            $txtInputReason = trim(htmlspecialchars_decode(stripslashes(${"rea_$my_user_id"}), ENT_QUOTES));
            if(!$lcardattend->TeacherCanManageReason){
				$txtInputReason = null;
			}
			//debug_pr($txtInputReason);
            $lcardattend->Set_Profile($my_user_id,$my_record_date,$DayType,$my_drop_down_status,$txtInputReason,"|**NULL**|","|**NULL**|","|**NULL**|",$RealInTime,false,$my_record_status,false,$upadteeDisLateRecord);
     		if($my_drop_down_status == CARD_STATUS_ABSENT && $lcardattend->TeacherCanManageProveDocumentStatus){
				$HandIn_prove_sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='".$_POST['submittedFileStatus_'.$my_user_id]."' WHERE StudentID='$my_user_id' AND RecordDate='$my_record_date' AND RecordType='$my_drop_down_status'";
	    		$lcardattend->db_db_query($HandIn_prove_sql);
			}
        }
        else {
            $lcardattend->Clear_Profile($my_user_id,$my_record_date,$DayType,true);
        }
        
        
        if($lcardattend->TeacherCanManageTeacherRemark)
		{
			$remark_value = $my_reason;
			if(!$use_magic_quotes){
				$remark_value = addslashes($remark_value);
			}
			$lcardattend->updateTeacherRemark($my_user_id,$my_record_date,$DayType,$remark_value);
		}
		
		
		if($my_drop_down_status != CARD_STATUS_PRESENT){
			$office_remark = trim($_POST["office_rmk_".$my_user_id]);
			$office_remark_value = trim($office_remark);
			if(!$use_magic_quotes){
				$office_remark_value = addslashes($office_remark_value);
			}
			$RealProfileType = $my_drop_down_status;
			if($my_drop_down_status == CARD_STATUS_OUTING){
				$RealProfileType = CARD_STATUS_ABSENT;
			}
			$lcardattend->updateOfficeRemark($my_user_id,$my_record_date,$DayType,$RealProfileType,$office_remark_value);
		}
        
    }
    
//    if($insertRemark){
//        $insertRemarkSQL = substr($insertRemarkSQL,0,strlen($insertRemarkSQL)-1);
//        $libdb->db_db_query($insertRemarkSQL);
//    }
//    if($removeRemark){
//        $removeRemarkSQL =substr($removeRemarkSQL,0,strlen($removeRemarkSQL)-1).")";
//        $libdb->db_db_query($removeRemarkSQL);
//    }
    ### for class / GROUP confirm
    $card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
    $card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;
    $card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;
    
    if($charactor=="class"){
        $sql="SELECT RecordID FROM ".$card_student_daily_class_confirm." WHERE ClassID='".$id."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
        $result = $libdb->returnVector($sql);
        $confirmed_id = $result[0];
    }
    if($charactor=="group"){
        $sql="SELECT RecordID FROM ".$card_student_daily_group_confirm." WHERE GroupID='".$id."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
        $result = $libdb->returnVector($sql);
        $confirmed_id = $result[0];
    }
    if($charactor=="subjectgroup"){
        $sql="SELECT RecordID FROM ".$card_student_daily_subject_group_confirm." WHERE SubjectGroupID='".$id."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
        $result = $libdb->returnVector($sql);
        $confirmed_id = $result[0];
    }
    
    if( $confirmed_id <> "" ){
        # update if record exist
        if($charactor=="class")
            $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=$UserID, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
            elseif($charactor=="group")
            {
                $sql = "UPDATE $card_student_daily_group_confirm SET ConfirmedUserID=$UserID, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
            }
            elseif($charactor=="subjectgroup")
            {
                $sql = "UPDATE $card_student_daily_subject_group_confirm SET ConfirmedUserID=$UserID, DateModified=NOW() WHERE RecordID='$confirmed_id'";
            }
            
            $libdb->db_db_query($sql);
            $msg = 1;
    }
    else{
        switch($timeslots){
            case "am": $DayType2 = 2; break;
            case "pm": $DayType2 = 3; break;
            default : $DayType2 = 2; break;
        }
        
        if($charactor=="class"){
            
            $sql = "INSERT INTO $card_student_daily_class_confirm
			(
			ClassID,
			ConfirmedUserID,
			DayNumber,
			DayType,
			DateInput,
			DateModified
			) VALUES
			(
			'$id',
			'$UserID',
			'".$txt_day."',
			'$DayType2',
			NOW(),
			NOW()
			)
			";
            
        }
        elseif($charactor=="group"){
            
            $sql = "INSERT INTO $card_student_daily_group_confirm
			(
			GroupID,
			ConfirmedUserID,
			DayNumber,
			DayType,
			DateInput,
			DateModified
			) VALUES
			(
			'$id',
			'$UserID',
			'".$txt_day."',
			'$DayType2',
			NOW(),
			NOW()
			)
			";
            
        }
        elseif($charactor=="subjectgroup"){
            
            $sql = "INSERT INTO $card_student_daily_subject_group_confirm
			(
			SubjectGroupID,
			ConfirmedUserID,
			DayNumber,
			DayType,
			DateInput,
			DateModified
			) VALUES
			(
			'$id',
			'$UserID',
			'".$txt_day."',
			'$DayType2',
			NOW(),
			NOW()
			)
			";
        }
        
        $libdb->db_db_query($sql);
        $msg = 1;
        
    }
    
    if($charactor=="class"){
        $sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$my_record_date."' and DayType = '".$DayType."' and Type = 'Class'";
    }
    elseif($charactor=="group"){
        $sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$my_record_date."' and DayType = '".$DayType."' and Type = 'Group'";
    }
    elseif($charactor=="subjectgroup"){
        $sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$my_record_date."' and DayType = '".$DayType."' and Type = 'Subject_Group'";
    }
    $Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);
    
}

$lcardattend->log($log_row);

intranet_closedb();

header('Location:attendance_list.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&lessonType='.$lessonType.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'');

?>