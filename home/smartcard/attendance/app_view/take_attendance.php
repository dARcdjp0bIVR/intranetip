<?php
/**
 * editing by:
 * 
 * Change log:
 * 2020-09-29 Ray
 * Add preset reason & remark
 *
 * 2019-11-08 Ray
 * Add $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']
 *
 * 2019-11-05 Tiffany
 * change the get teacher remark function for platform change it
 *
 * 2019-10-30 Tiffany
 * Fix Lesson missing tag problem
 *
 * 2019-05-08 Tiffany
 * add teacher remark pool
 *
 * 2019-03-06 Tiffany
 * Don't change the status and hide the submit button when "cannot take past date record" and "cannot take future date record" enabled.
 *
 * 2019-01-04 Carlos 
 * Do not apply setting [iSmartCardDisableModifyStatusAfterConfirmed] for lesson attendance.
 * 
 * 2018-08-03 Carlos [ip.2.5.9.10.1]
 * Apply setting [iSmartCardDisableModifyStatusAfterConfirmed] to disable status change for confirmed attendance records.
 * 
 * 2018-03-12 Carlos
 * Check setting TeacherCanManageProveDocumentStatus to show/hide [Submitted Prove Document] option.
 * 
 * 2018-01-04 Isaac
 * fixed function show_status_select() and function show_mutiple_select(): fix names and removecheckboxradio("refresh");
 *  
 * 2017-11-13 Isaac
 * modified multiselect and show name.
 *
 */
$PATH_WRT_ROOT = "../../../../";
$PATH_WRT_ROOT_NEW = "../../../..";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT.'includes/libeclass_ws.php');
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$uid = IntegerSafe($_GET['uid']);
$id = IntegerSafe($_GET['id']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date==""||!isset($timeslots)||$timeslots=="")header('Location:attendance_list.php');

$show_date = date("Y/m/d",strtotime($date));	
$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$today = date('m/d/Y');
$today_record = strtotime($today);
$page_load_time = time()+1;

if(!isset($status_switch) || $status_switch=="")
{
	$status_switch = "all";
}

$libdb = new libdb();
$lu = new libuser();
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$attendance_mode = $Settings['AttendanceMode'];
$libws = new libeclass_ws();
$lcardattend = new libcardstudentattend2();
$json = new JSON_obj();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$conds = "";
$TABLES = "";
$class_name = "";
$student_list = "";
$reason = "";
$present_count = 0;
$absent_count = 0;
$late_count = 0;
$outing_count = 0;
$order = "";
$status_array = "";
$student_array = array();
$taphold_leavesession = "";
$reason_confirm1_leavesession = "";

$lword = new libwordtemplates();
$words_absence = $lword->getWordListAttendance(1);
$words_late = $lword->getWordListAttendance(2);
$words_outing = $lcardattend->Get_Outing_Reason();


$sql = "SELECT ReasonText FROM CARD_STUDENT_PRESET_TEACHER_REMARK as s";
$result = $libdb->returnVector($sql);

for ($i=0; $i<sizeof($result); $i++)
{
    if(trim($result[$i])!=""){
        $remark_pool_data .= "<li data-icon='false' id=remark_pool_$i><a href='javascript:input_remark($i);'>".trim($result[$i])."</a></li>";
    }
}
//// Get preset Teacher's Remark from txt file
//$fs = new libfilesystem();
//$file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
//if (file_exists($file_path))
//{
//    // Get data
//    $data = trim($fs->file_read($file_path));
//    if ($data != "")
//    {
//        $data = explode("\n", $data);
//        for ($i=0; $i<sizeof($data); $i++)
//        {
//            if(trim($data[$i])!=""){
//                $remark_pool_data .= "<li data-icon='false' id=remark_pool_$i><a href='javascript:input_remark($i);'>".trim($data[$i])."</a></li>";
//            }
//        }
//    }
//}

if(!isset($msg) || $msg=="")
{
    $msg=0;
}

#get the settings for show student info or not
$sql="select * from APP_USER_SETTING where UserID='".$UserID."' and SettingName='eAttendance_ShowStudentInfoClassAndNo'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentName = '0';
}else{
    $isShowStudentName = $getShowStudentNameArray[0]['SettingValue'];
}

#get the settings for show student name or not
$sql="select * from APP_USER_SETTING where UserID='".$UserID."' and SettingName='eAttendance_ShowStudentInfoName'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentInfoName = '0';
}else{
    $isShowStudentInfoName = $getShowStudentNameArray[0]['SettingValue'];
}

#get the settings for show student class and class Number or not
$sql="select * from APP_USER_SETTING where UserID='".$UserID."' and SettingName='eAttendance_ShowStudentInfoClassAndNo'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentInfoClassAndNo = '0';
}else{
    $isShowStudentInfoClassAndNo = $getShowStudentNameArray[0]['SettingValue'];
}

$disable_modify_status = $lcardattend->iSmartCardDisableModifyStatusAfterConfirmed && $charactor != "lesson";
$disable_past_date     = $lcardattend->DisableISmartCardPastDate && $charactor != "lesson";
$disable_future_date   = $lcardattend->CannotTakeFutureDateRecord && $charactor != "lesson";

if($disable_past_date&&($ts_record < $today_record)){
    $displaySubmitButton = 'display:none;';
}else if($disable_future_date&&($ts_record > $today_record)){
    $displaySubmitButton = 'display:none;';
}else{
    $displaySubmitButton = '';
}

$confirmed_by_admin_ary = array();

//disable submit
$disable=false;
if ($Settings['EnableEntryLeavePeriod']==1) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$ToDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}

if($Settings['EditDaysBeforeRecord']==1&&($ts_record<=$today_record)){

    $daycount=ceil(($today_record-$ts_record)/86400)+1; 
	if($daycount>$Settings['EditDaysBeforeRecordSelect']){	
		$disable=true;
	}
}

for($i=0;$i<sizeof($words_absence);$i++){
	$reason_absent .= addslashes(" <li data-icon='false' id=rea_pool_$i><a href='javascript:input_reason($i);'>".$words_absence[$i]."</a></li>"); 	
}

for($i=0;$i<sizeof($words_late);$i++){
	$reason_late .= addslashes(" <li data-icon='false' id=rea_pool_$i><a href='javascript:input_reason($i);'>".$words_late[$i]."</a></li>"); 
}

for($i=0;$i<sizeof($words_outing);$i++){
	$reason_outing .= addslashes(" <li data-icon='false' id=rea_pool_$i><a href='javascript:input_reason($i);'>".$words_outing[$i]."</a></li>"); 	
}

if($sys_custom['SmartCardAttendance_StudentAbsentSession']){
	$taphold_leavesession = "
				 var lateSession = $('#late_'+id).val();
		         var absentSession = $('#absent_'+id).val();
	    		 var requestLeaveSession = $('#request_'+id).val();
	    		 var playTruantSession = $('#playTruant_'+id).val();
	    		 var officalLeaveSession = $('#offical_'+id).val();
		       	 $('#late_session1').val(lateSession);   
	    		 $('#offical_leave_session1').val(officalLeaveSession);   
	    		 $('#request_leave_session1').val(requestLeaveSession); 
	    		 $('#play_truant_session1').val(playTruantSession); 
	    		 $('#absent_session1').val(absentSession); 
				 $('#late_session2').val(lateSession);   
	    		 $('#offical_leave_session2').val(officalLeaveSession);   
	    		 $('#request_leave_session2').val(requestLeaveSession); 
	    		 $('#play_truant_session2').val(playTruantSession); 
	    		 $('#absent_session2').val(absentSession); 
	    		 $('select').selectmenu('refresh', true);";
	$reason_confirm1_leavesession="  
				var lateSession = $('#late_session1').val()
		    	$('#late_'+userID).val(lateSession);
		    	var absentSession = $('#absent_session1').val()
		    	$('#absent_'+userID).val(absentSession);
		    	var officalLeaveSession = $('#offical_leave_session1').val()
		    	$('#offical_'+userID).val(officalLeaveSession);
		    	var requestLeaveSession = $('#request_leave_session1').val()
		    	$('#request_'+userID).val(requestLeaveSession);
		    	var playTruantSession  = $('#play_truant_session1').val()
		    	$('#playTruant_'+userID).val(playTruantSession );";
}

if($timeslots=="am")
{
	$ExpectedField = $lcardattend->Get_AM_Expected_Field("b.","c.","f.");
	$StatusField = "AMStatus";
	$DayType = PROFILE_DAY_TYPE_AM;
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);

	$ConfirmedByAdminField = "b.AMConfirmedByAdmin";
}
else{
	
	$ExpectedField = $lcardattend->Get_PM_Expected_Field("b.","c.","f.");
	$StatusField = "PMStatus";
	$DayType = PROFILE_DAY_TYPE_PM;
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);

	$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k
										ON
											k.StudentID = a.UserID
											AND k.RecordDate = '".$ToDate."'
											AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
											AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
	
	$ConfirmedByAdminField = "b.PMConfirmedByAdmin";
}

if($charactor=="class"){
    $sql="SELECT ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." FROM YEAR_CLASS WHERE YearClassID='".$id."'"; 
    $result = $libdb->returnVector($sql);
    $class_name = $result[0];
    
    $TABLES="INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID
    		 LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
    		 LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID)";
    $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND ycu.YearClassID='".$id."' ";
    $order = " order by ycu.ClassNumber asc ";
    $groupBy = "";
}

if($charactor=="group"){
    $sql="SELECT ".Get_Lang_Selection('TitleChinese', 'Title')." FROM INTRANET_GROUP WHERE groupid='".$id."'";
    $result = $libdb->returnVector($sql);
    $class_name = $result[0];
	
    $TABLES="INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID 
    		 LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (a.UserID = ycu.UserID)
    		 LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID )
    		 LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID)
    		";
    $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1) AND ug.GroupID='".$id."' ";
    $order = " order by y.Sequence, yc.Sequence, ycu.ClassNumber asc ";
    $groupBy = " group by a.UserID ";
}


if($charactor=="subjectgroup"){
    $sql="SELECT ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID='".$id."'";
	$result = $libdb->returnVector($sql);
	$class_name = $result[0];
	
	$TABLES="INNER JOIN SUBJECT_TERM_CLASS_USER as su ON su.UserID=a.UserID
			 LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (a.UserID = ycu.UserID)
    		 LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID )
    		 LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID)
    		";

	$conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND su.SubjectGroupID='".$id."' ";
	$order = " order by y.Sequence, yc.Sequence, ycu.ClassNumber asc ";	
	//$order = " order by a.ClassNumber ASC";		
	$groupBy = " group by a.UserID ";
}

if($charactor =="lesson"){
//    debug_pr($id);
//    debug_pr($AttendOverviewID);
//    debug_pr($ToDate);
//    debug_pr($RoomAllocationID);
//    debug_pr($Start_time);
//    debug_pr($End_time);
//    debug_pr($LessonTitle);
//die();
    $class_name = $LessonTitle;
    $post_info = "<input type='hidden' id='RoomAllocationID' name='RoomAllocationID' value='".$RoomAllocationID."'>
    		      <input type='hidden' id='Start_time' name='Start_time' value='".$Start_time."'>
    		      <input type='hidden' id='End_time' name='End_time' value='".$End_time."'>
    		      <input type='hidden' id='AttendOverviewID' name='AttendOverviewID' value='".$AttendOverviewID."'>
    		      <input type='hidden' id='LessonTitle' name='LessonTitle' value='".$LessonTitle."'>	
    		     ";
	if ($AttendOverviewID < 0) {	
	   $StudentList = $lcardattend->Get_Student_List($id, "SubjectGroup", $ToDate);	
	} else {	
	   $StudentList = $lcardattend->Get_Lesson_Attendance_Record($AttendOverviewID);	
	}
	$all_count = count($StudentList);
    for($j=0;$j<sizeof($StudentList);$j++){
        //0:present, 1:outing, 2:late, 3:absent
        if($StudentList[$j][5]==0)$status=0;
        if($StudentList[$j][5]==1)$status=3;
        if($StudentList[$j][5]==2)$status=2;
        if($StudentList[$j][5]==3)$status=1;
        if($status==0) $present_count++;
        if($status==1) $absent_count++;
        if($status==2) $late_count++;
        if($status==3) $outing_count++;
        $user_id = $StudentList[$j][0];
        $remark =  $StudentList[$j][6];
        $Student_name = Get_Lang_Selection($StudentList[$j][2],$StudentList[$j][1]);
        $studentClassName = Get_Lang_Selection($StudentList[$j][9],$StudentList[$j][4]);
        $studentClassNumber = $StudentList[$j][3];
        //	list($RecordID,$user_id,$Student_name,$status,$reason,$remark,$LateSession,$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession,$DefaultAbsentReason,$DefaultLateReason,$DefaultOutingReason) = $result[$j];
        $status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
        //	$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
        
        $student_array[] = $user_id;
        $photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
        $StudentTags = "<div class='tagArea'><p class='StudentName'>".$Student_name."</p>
                        <p class='ClassAndNum'><span class='openBracket'>(</span>".$studentClassName." - ".$studentClassNumber."<span class='closeBracket'>)</span></p></div>";
        
        if($status==0){
            $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
		    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'> 
		    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
                                    $StudentTags
                                    <h2 style='color:#75b70d;' id='h2_".$user_id."'></h2>
					              </a>
					          </li>";
        }
        
        if ($status==1){
            $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;image-orientation:90deg'>
                                    $StudentTags
									<p class='absent' id='ab_".$user_id."'></p>	
				                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Absent']."</h2>
			                      </a>
			                  </li>";
        }
        
        if($status==2){
            $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>
			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
                                    $StudentTags
				                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Late']."</h2>
				                  </a>
				              </li>";
        }
        if($status==3){
            $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#329700;' id='a_".$user_id."'>
	    		                    <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
                                    $StudentTags
				                    <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Outing']."</h2>
				                  </a>
				              </li>";
		}			         
		$reason_submit .="<input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";
	}
}else{
	
	$sql = "    SELECT
			    b.RecordID,
             	a.UserID,		
				".getNameFieldByLang("a.")." as name,				
				IF(b.".$StatusField." IS NOT NULL,b.".$StatusField.", ".$ExpectedField."),
			    if (".$ExpectedField." = '".CARD_STATUS_ABSENT."',
					".$AbsentExpectedReasonField.",
					IF(".$ExpectedField." = '".CARD_STATUS_LATE."',
						".$LateExpectedReasonField.",
						IF(".$ExpectedField." = '".CARD_STATUS_OUTING."',
							".$OutingExpectedReasonField.",
							j.Reason))) as Reason,
				IF(d.Remark IS NULL,IF(b.".$StatusField."='".CARD_STATUS_ABSENT."' OR (b.".$StatusField." IS NULL AND ".$ExpectedField."='".CARD_STATUS_ABSENT."'),'',d.Remark),d.Remark),
			    IF(j.OfficeRemark IS NULL AND TRIM(f.Remark)!='',f.Remark,j.OfficeRemark) as OfficeRemark,
				ssc.LateSession,
                ssc.RequestLeaveSession,
			    ssc.PlayTruantSession,
			    ssc.OfficalLeaveSession,
                ssc.AbsentSession,
				".$AbsentExpectedReasonField." as DefaultAbsentReason,
		    	".$LateExpectedReasonField." as DefaultLateReason, 
				".$OutingExpectedReasonField." as DefaultOutingReason,
				yc.".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN').",
				ycu.ClassNumber,
				b.InSchoolTime,			
                b.LunchBackTime,
                b.LunchOutTime,
                b.LeaveSchoolTime,
               	f.Waive as PresetWaive,
               	j.RecordStatus,	
                IF(j.RecordID IS NOT NULL,j.DocumentStatus,f.DocumentStatus) as DocumentStatus,
				$ConfirmedByAdminField as ConfirmedByAdmin,
				IF(f.RecordID IS NOT NULL,1,0),
                f.Reason as PresetReason,
                f.Remark as PresetRemark       		
			FROM
				INTRANET_USER AS a
				".$FilterInActiveStudent ." 
				".$TABLES."
				LEFT OUTER JOIN $card_log_table_name AS b
				ON (a.UserID=b.UserID AND b.DayNumber = '".$txt_day."')
				LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$ToDate."' AND c.DayType = '".$DayType."')								
				LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='".$ToDate."' AND d.DayType='$DayType')
				LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate='".$ToDate."' AND f.DayPeriod='".$DayType."')
				LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
				ON j.StudentID = a.UserID 
					AND j.RecordDate = '".$ToDate."' 
					AND j.DayType = '".$DayType."' 
					AND 
					(
						(j.RecordType = b.".$StatusField." AND j.RecordType<>'".PROFILE_TYPE_EARLY."')
						OR  
						(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
						AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
						)
					)
				".$AMJoinTable."
				LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as ssc 
						ON ssc.StudentID = a.UserID 
							AND ssc.RecordDate = '".$ToDate."' 
                               AND ssc.DayType = '".$DayType."' 		
		    WHERE yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' and
				".$conds.$groupBy.$order;
    $result = $libdb->returnArray($sql);
    $all_count = count($result);

    //$confirmed_by_admin_ary = array();
    
    for($j=0;$j<sizeof($result);$j++){
        if($result[$j][3]==0) $present_count++;
        if($result[$j][3]==1) $absent_count++;
        if($result[$j][3]==2) $late_count++;
        if($result[$j][3]==3) $outing_count++; 
        list($RecordID,$user_id,$Student_name,$status,$reason,$remark,$officeRemark,$LateSession,$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,
            $AbsentSession,$DefaultAbsentReason,$DefaultLateReason,$DefaultOutingReason,$studentClassName,$studentClassNumber,$inSchoolTime,$lunchBackTime,$lunchOutTime,$leaveSchoolTime,$preset_waive,$IsWaived,$DocumentStatus,$ConfirmedByAdmin, $preset_record,$preset_reason,$preset_remark) = $result[$j];

            if($disable_modify_status){
            	$confirmed_by_admin_ary[$user_id] = $ConfirmedByAdmin?1:0;
            }
            
            $status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
            $recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$RecordID."'>";
            if($attendance_mode == 2){
                if($timeslots=="am"){
                    $arrival_time.="<input type='hidden' id='arrival_time_".$user_id."' name='arrival_time_".$user_id."' value='".$inSchoolTime."'>";
                    $leave_time.="<input type='hidden' id='leave_time_".$user_id."' name='leave_time_".$user_id."' value='".$lunchOutTime."'>";
                }else{
                    $arrival_time.="<input type='hidden' id='arrival_time_".$user_id."' name='arrival_time_".$user_id."' value='".$lunchBackTime."'>";
                    $leave_time.="<input type='hidden' id='leave_time_".$user_id."' name='leave_time_".$user_id."' value='".$leaveSchoolTime."'>";
                }
            }else{
                $arrival_time.="<input type='hidden' id='arrival_time_".$user_id."' name='arrival_time_".$user_id."' value='".$inSchoolTime."'>";
                $leave_time.="<input type='hidden' id='leave_time_".$user_id."' name='leave_time_".$user_id."' value='".$leaveSchoolTime."'>";
            }
            $waivedStatus.="<input type='hidden' id='waivedStatus_".$user_id."' name='waivedStatus_".$user_id."' value='".(($IsWaived == 1 || ($status=="1" && $preset_waive == "1"))? '1':'')."'>";
            $submittedFileStatus.="<input type='hidden' id='submittedFileStatus_".$user_id."' name='submittedFileStatus_".$user_id."' value='".$DocumentStatus."'>";
            $student_array[] = $user_id;
            $photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
            //debug_pr($RecordID);
            
            //default the $RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession
            if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
            {
                if ($LateSession==""&&$AbsentSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
                    
                    if ($status == CARD_STATUS_ABSENT) {
                        $LateSession = $PlayTruantSession = $OfficalLeaveSession = $AbsentSession = 0;
                        $RequestLeaveSession = 1;
                        if(isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){
                            $PlayTruantSession = $Settings['DefaultNumOfSessionForAbsenteesism'];
                        }
                        if(isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){
                            $RequestLeaveSession = $Settings['DefaultNumOfSessionForLeave'];
                        }
                        if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){
                            $AbsentSession = $Settings['DefaultNumOfSessionForAbsent'];
                        }
                    }
                    else if ($status==CARD_STATUS_LATE) {
                        $LateSession = 1;
                        $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = $AbsentSession = 0;
                        if(isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] != -1){
                            $LateSession = $Settings['DefaultNumOfSessionForLate'];
                        }
                    }
                    else {
                        $LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = $AbsentSession = 0;
                    }
                    
                }
            }

            if($disable_past_date&&($ts_record < $today_record)){
                $status_js = 'show_disable_past_date_alert();';
            }else if($disable_future_date&&($ts_record > $today_record)){
                $status_js = 'show_disable_future_date_alert();';
            }else if($disable_modify_status && $ConfirmedByAdmin){
                $status_js = 'show_confirmed_by_admin_alert();';
            }else{
                $status_js = 'choose('.$user_id.');';
            }


            //$status_js = $disable_modify_status && $ConfirmedByAdmin ? 'show_confirmed_by_admin_alert();' : 'choose('.$user_id.');';
            
            $StudentTags = "<div class='tagArea'><p class='StudentName'>".$Student_name."</p>
                        <p class='ClassAndNum'><span class='openBracket'>(</span>".$studentClassName." - ".$studentClassNumber."<span class='closeBracket'>)</span></p></div>";
            
            if($status==0){
                $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
		    		              <a href='javascript:$status_js' id='a_".$user_id."'>
		    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
					                $StudentTags";
                if($PlayTruantSession>0){
                    $student_list .= "<h2 style='color:#75b70d;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['PlayTruantShow']."</h2>";
                }else{
                    $student_list .= "<h2 style='color:#75b70d;' id='h2_".$user_id."'></h2>";
                }
                $student_list .= "</a></li>";
		}
	    
	    if ($status==1){		
			$student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:$status_js' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;image-orientation:90deg'>
				                    $StudentTags
				                    <p class='absent' id='ab_".$user_id."'></p>
				                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Absent']."</h2>
			                      </a>
			                  </li>";
            }
            
            if($status==2){
                $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:$status_js' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>
			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                    $StudentTags
				                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Late']."</h2>
				                  </a>
				              </li>";
            }
            if($status==3){
                $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
					              <a href='javascript:$status_js' style='border:2px;border-style: solid; border-color:#329700;' id='a_".$user_id."'>
	    		                    <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
                                    $StudentTags
				                    <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['Outing']."</h2>
				                  </a>
				              </li>";
		}		
		$reason_submit .="<input type='hidden' value='".$reason."' id='rea_".$user_id."' name='rea_".$user_id."'>
					      <input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>
					      <input type='hidden' value='".$officeRemark."' id='office_rmk_".$user_id."' name='office_rmk_".$user_id."'>		
					      <input type='hidden' value='".$DefaultAbsentReason."' id='defaultAbsentRea_".$user_id."' name='defaultAbsentRea_".$user_id."'>					              		
					      <input type='hidden' value='".$DefaultLateReason."' id='defaultLateRea_".$user_id."' name='defaultLateRea_".$user_id."'>
			              <input type='hidden' value='".$DefaultOutingReason."' id='defaultOutingRea_".$user_id."' name='defaultOutingRea_".$user_id."'>
		                  <input type='hidden' value='0' id='confirmed_".$user_id."' name='confirmed_".$user_id."'>
		                  <input type='hidden' value='".$preset_record."' id='preset_record_".$user_id."' name='preset_record_".$user_id."'>
		                  <input type='hidden' value='".$preset_reason."' id='preset_reason_".$user_id."' name='preset_reason_".$user_id."'>
		                  <input type='hidden' value='".$preset_remark."' id='preset_remark_".$user_id."' name='preset_remark_".$user_id."'>";
		                  
		if($sys_custom['SmartCardAttendance_StudentAbsentSession']){
			$reason_submit .= "<input type='hidden' value='".$LateSession."' id='late_".$user_id."' name='late_".$user_id."'>
			              <input type='hidden' value='".$AbsentSession."' id='absent_".$user_id."' name='absent_".$user_id."'>
	                      <input type='hidden' value='".$RequestLeaveSession."' id='request_".$user_id."' name='request_".$user_id."'>
		                  <input type='hidden' value='".$PlayTruantSession."' id='playTruant_".$user_id."' name='playTruant_".$user_id."'>
                          <input type='hidden' value='".$OfficalLeaveSession."' id='offical_".$user_id."' name='offical_".$user_id."'>";
		}                  
	}

    

	//drop down menu for $RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession
	if($sys_custom['SmartCardAttendance_StudentAbsentSession']&&$charactor!="lesson")
	{	
	
		$selLateSession1 = "<select id=\"late_session1\"/>";
		$selOfficalLeaveSession1 = "<select id=\"offical_leave_session1\"/>";
		$selRequestLeaveSession1 = "<select id=\"request_leave_session1\"/>";
		$selPlayTruantSession1 = "<select id=\"play_truant_session1\"/>";
		$selAbsentSession1 = "<select id=\"absent_session1\"/>";
		
		$selLateSession2 = "<select id=\"late_session2\"/>";	
		$selOfficalLeaveSession2 = "<select id=\"offical_leave_session2\"/>";
		$selRequestLeaveSession2 = "<select id=\"request_leave_session2\"/>";
		$selPlayTruantSession2 = "<select id=\"play_truant_session2\"/>";
		$selAbsentSession2 = "<select id=\"absent_session2\"/>";
		
		$k=0.0;
		while($k<=CARD_STUDENT_MAX_SESSION){
				
	        $selLateSession1 .= "<option value=\"$k\" >".$k."</option>";
			$selOfficalLeaveSession1 .= "<option value=\"$k\" >".$k."</option>";
			$selRequestLeaveSession1 .= "<option value=\"".$k."\">".$k."</option>";
			$selPlayTruantSession1 .= "<option value=\"".$k."\">".$k."</option>";
			$selAbsentSession1 .= "<option value=\"$k\">".$k."</option>";
			
			$selLateSession2 .= "<option value=\"$k\" >".$k."</option>";
			$selOfficalLeaveSession2 .= "<option value=\"$k\" >".$k."</option>";
			$selRequestLeaveSession2 .= "<option value=\"".$k."\">".$k."</option>";
			$selPlayTruantSession2 .= "<option value=\"".$k."\">".$k."</option>";
			$selAbsentSession2 .= "<option value=\"$k\">".$k."</option>";
			
			$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
			
		}
		$selLateSession1 .= "</select>\n";
		$selOfficalLeaveSession1 .= "</select>\n";
		$selRequestLeaveSession1 .= "</select>\n";
		$selPlayTruantSession1 .= "</select>\n";
		$selAbsentSession1 .= "</select>\n";
		
		$selLateSession2 .= "</select>\n";
		$selOfficalLeaveSession2 .= "</select>\n";
		$selRequestLeaveSession2 .= "</select>\n";
		$selPlayTruantSession2 .= "</select>\n";
		$selAbsentSession2 .= "</select>\n";
	}
	
	
}
    $student_array_string = implode(";",$student_array);
//  debug_pr($student_array);
//  die();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>

<script>
    var array_string = '<?= $student_array_string?>' ;
    var student_array = array_string.split(';');
	var disable = '<?=$disable?1:0;?>';
    var charactor = '<?=$charactor?>';
    var isShowStudentName = '<?=$isShowStudentName?>';
    var isShowStudentInfoName = '<?=$isShowStudentInfoName?>';
    var isShowStudentInfoClassAndNo = '<?=$isShowStudentInfoClassAndNo?>';
    var studentIdToConfirmedByAdmin = <?=$json->encode($confirmed_by_admin_ary)?>;
    var absent_session_toggle_ignore_confirm_check = <?=(($sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm'] == true) ? 'true' : 'false')?>;

    window.onload = function () {
    	
    	$("a").on("taphold",function(){

			 if(disable==1){			 	
			 	return true;
			 }

             var id = $(this).attr("id").substr(2);
             var status = $('#status_'+id).val();
             <?=$taphold_leavesession?>
             var arrivaltime = $('#arrival_time_'+id).val();
             if(arrivaltime!=""){
        	 	$('#arrival_time1').html(arrivaltime);            	 	
        	 }else{
        	 	$('#arrival_time1').html('- - -'); 
        	 }

            var leavetime = $('#leave_time_'+id).val();
            if(leavetime!=""){
                $('#leave_time1').html(leavetime);
            }else{
                $('#leave_time1').html('- - -');
            }

            var waivedStatus = $('#waivedStatus_'+id).val();
        	 if(status==0||status==3){
        	 	$("#waivedCheckbox").hide(); 
        	 }else{
        	 	$("#waivedCheckbox").show(); 
        	 	if(waivedStatus==1){
        	     	$("#waived").prop("checked",true).checkboxradio("refresh");        	 		
        	 	}else{
        	 		$("#waived").prop("checked",false).checkboxradio("refresh");        	 		       	 		
        	 	}
        	 }

        	 var submittedFileStatus = $('#submittedFileStatus_'+id).val();
        	 if(status==1){
        	 	$("#submittedFileCheckbox").show(); 
        	 	if(submittedFileStatus==1){
        	     	$("#HandIn_prove").prop("checked",true).checkboxradio("refresh");        	 		
        	 	}else{
        	 		$("#HandIn_prove").prop("checked",false).checkboxradio("refresh");  
        	 	}
        	 }else{
        	 	$("#submittedFileCheckbox").hide(); 
        	 }
        	
        	 if(status==0||charactor=='lesson'){
        	 	$("#reason_insert").hide(); 
        	 }else{
        	 	$("#reason_insert").show();         	 	
        	 	var reason = $('#rea_'+id).val()
        	    $('#reason1').val(reason);      
        	 } 	 
             
        	 var remark = $('#rmk_'+id).val()            	 
        	 $('#remark1').val(remark);     
        	 
        	 if(charactor=='lesson'){
        	 	$("#office_remark_div1").hide();
        	 }else{           	 	
            	 var office_remark = $('#office_rmk_'+id).val() 
            	 if(office_remark!=""){
            	    $('#office_remark1').html(office_remark);
            	 }else{
            	 	$('#office_remark1').html('- - -'); 
        	     }
        	 }


        	 if($('#preset_record_'+id).val() == 1) {
        	    var preset_reason = $('#preset_reason_'+id).val();
        	    var preset_remark = $('#preset_remark_'+id).val();
        	    if(preset_reason == '') {
                    preset_reason = '- - -';
                }
                 if(preset_remark == '') {
                     preset_remark = '- - -';
                 }
        	    $("#preset_reason1").html(preset_reason);
                $("#preset_remark1").html(preset_remark);
                $("#preset_reason_div").show();
                $("#preset_remark_div").show();
             } else {
                 $('#preset_reason1').html('- - -');
                 $('#preset_remark1').html('- - -');
                 $("#preset_reason_div").hide();
                 $("#preset_remark_div").hide();
             }

        	 $('#reason_pool_div').hide();
        	 if(status==1){
        	 	$('#reason_pool').html("<?=$reason_absent?>");            	 	
        	 }
        	 if(status==2){
        	 	$('#reason_pool').html("<?=$reason_late?>");            	 	
        	 }
        	 if(status==3){
        	 	$('#reason_pool').html("<?=$reason_outing?>");            	 	
        	 }
        	 $('ul').listview('refresh');
        	  
             $('#cancle_btn1').attr('href','javascript:cancle1('+id+')');
        	 $('#confirm_btn1').attr('href','javascript:reason_confirm1('+id+')');                    	 
             $('#link1').trigger("click");
                            
		});     
    	
    }
	jQuery(document).ready( function() {

        if(disable==1){			 	
		   $("#mutiple_select").hide(); 
		   $("#header").height('60px');
		   $("#footer").hide();
		}
		$("#status_select").hide();
		change_dropdown_icon();
		//defult not show tags
		$(".openBracket").hide(); 
		$(".closeBracket").hide();
        $(".ClassAndNum").hide();
        $(".StudentName").hide();

//         isShowStudentInfoName='1';
        
        if(isShowStudentName=='1'){
			$('#SelectAll').prop("checked", true).checkboxradio('refresh');
		} else{
			$('#SelectAll').prop("checked", false).checkboxradio('refresh');		
		}    

		if(isShowStudentInfoName=='1'){
			$('#showStudentName').prop("checked", true).checkboxradio('refresh');			
		} else{
			$('#showStudentName').prop("checked", false).checkboxradio('refresh');	
		}	
		
		if(isShowStudentInfoClassAndNo =='1'){
			$('#showClassAndNum').prop("checked", true).checkboxradio('refresh');
		} else{
			$('#showClassAndNum').prop("checked", false).checkboxradio('refresh');
		}

		CheckingCheckboxes();

		$("#mode").val("single");
		//document.getElementById("<?=$status_switch?>").selected=true;
		//$('select').selectmenu('refresh', true);
		
		var msg = <?=$msg?>;
		if(msg=="99") {
			 alert("<?=$i_StudentAttendance_Warning_Data_Outdated?>");
	    }
	    
   

// 	    $( "#popup1" ).popup({
// 	        afteropen: function() {
//             	  $("#reason1").focus();
// 	        }
// 	    }); 
		 
// 		$( "#popup2" ).popup({
// 	        afteropen: function(event, ui) {
// 	        }
// 		});   
	});

	function show_confirmed_by_admin_alert()
	{
		alert("<?=$Lang['StudentAttendance']['AttendanceRecordConfirmedByAdminAlertMsg']?>");
	}

    function show_disable_past_date_alert()
    {
        alert("<?=$Lang['StudentAttendance']['ISmartCardDisableTakePastRecord']?>");
    }

    function show_disable_future_date_alert()
    {
        alert("<?=$Lang['StudentAttendance']['WarningCannotTakeFutureDateRecord']?>");
    }

	function show_status_select(){
		$("#mode").val("single");
	     $("img.selected").remove();
		 $("#status_select").hide();
		 $("#mutiple_select").show(); 
		 $(".custom_collapsible").show();
	}

	function show_mutiple_select(){
		$("#mode").val("multiple");
		 $("#status_select").show();
	     $("#mutiple_select").hide();
	     $(".custom_collapsible").hide();
	}
		
	function refresh(){
		
		   document.form1.action="take_attendance.php";
		   document.form1.submit();
	}

    function choose(userID){
    	  var disable = '<?=$disable?1:0;?>';
		  if(disable==1){			 	
			 return true;
		  }
		  if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userId]==1){
		  	return true;
		  }

        var playTruantNum = $('#playTruant_'+userID).val();

        if($("#mode").val()=="multiple"){
		        if(!$("#c_"+userID)[0].checked){
		    	   $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		    	   $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
		        }
		        else{
			       $("#c_"+userID).prop("checked",false).checkboxradio("refresh");	
			       $("#s_"+userID).remove();
			       
			    }
          } 
          else{
        	  var taken = "<?=$taken?>";
              if($("#status_"+userID).val()==0){
                  
             	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p>");
                  $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Absent']?>").css({"color":"#d92b2b"});

                  $("#status_"+userID).val(1);

                 var absent_count = parseInt($("#absent_count").val())+1;
                 var present_count = parseInt($("#present_count").val())-1;
                 $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");
                 $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
                 $("#absent_count").val(absent_count);
                 $("#present_count").val(present_count);                 
                 $('select').selectmenu('refresh', true);
                 
                 var confirmed = $("#confirmed_"+userID).val();
 	             if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
 	             	
 	             	//dropdown 
		            var officalSession = document.getElementById('offical_'+userID);
                    if(officalSession){
		                $("#offical_"+userID).val(0);
		                $("#late_"+userID).val(0);
		 				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){?>
		 				    $("#request_"+userID).val(<?=($Settings['DefaultNumOfSessionForLeave'])?>);
		 				<?}else{?>
		 				    $("#request_"+userID).val(1);
		 				<?}?>
		 				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){?>
		 				    $("#playTruant_"+userID).val(<?=($Settings['DefaultNumOfSessionForAbsenteesism'])?>);
		 				<?}else{?>
		 			     	$("#playTruant_"+userID).val(0);
		 				<?}?>
		 				<?php if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){ ?>
		 			    	$("#absent_"+userID).val(<?=($Settings['DefaultNumOfSessionForAbsent'])?>);
						<?php } ?>
                    }
                    $("#rea_"+userID).val($("#defaultAbsentRea_"+userID).val());	 	             
 	             }
              }
              
              else if($("#status_"+userID).val()==1){
                  
              	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});
              	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Late']?>").css({"color":"#0094bf"});              
              	 $("#ab_"+userID).remove();           	 
                 $("#status_"+userID).val(2);

                 var late_count = parseInt($("#late_count").val())+1;
                 var absent_count = parseInt($("#absent_count").val())-1;
                 $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
                 $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");                 
                 $("#late_count").val(late_count);
                 $("#absent_count").val(absent_count);                  
                 $('select').selectmenu('refresh', true);

                 var confirmed = $("#confirmed_"+userID).val();
 	             if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
 	             	 //dropdown 
                     var officalSession = document.getElementById('offical_'+userID);
                     if(officalSession){
   	             	  $("#offical_"+userID).val(0);
   					  $("#request_"+userID).val(0);
   					  $("#playTruant_"+userID).val(0);
   					  $("#absent_"+userID).val(0);
   					  <?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] != -1){?>
   						  $("#late_"+userID).val(<?=($Settings['DefaultNumOfSessionForLate'])?>);
   					  <?}else{?>
   						  $("#late_"+userID).val(1);
   					  <?}?>
                     }
                     $("#rea_"+userID).val($("#defaultLateRea_"+userID).val());	 	             
 	             }
              }

              else if($("#status_"+userID).val()==2){
                  
               	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"});
              	 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Outing']?>").css({"color":"#309100"});              
	             	 
                 $("#status_"+userID).val(3);

                 var outing_count = parseInt($("#outing_count").val())+1;
                 var late_count = parseInt($("#late_count").val())-1;
                 $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");
                 $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
                 $("#outing_count").val(outing_count);
                 $("#late_count").val(late_count);                  
                 $('select').selectmenu('refresh', true);

                 var confirmed = $("#confirmed_"+userID).val();
 	             if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
                     $("#rea_"+userID).val($("#defaultOutingRea_"+userID).val());	 	
                     //dropdown 
                     var officalSession = document.getElementById('offical_'+userID);
                     if(officalSession){
   	            	  $("#offical_"+userID).val(0);
   					  $("#request_"+userID).val(0);
   					  $("#playTruant_"+userID).val(0);
   					  $("#absent_"+userID).val(0);
   					  $("#late_"+userID).val(0);
                     }             
 	             }
              }

              else if($("#status_"+userID).val()==3){
                  
                  $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});

                  if(playTruantNum>0){
                      $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['PlayTruantShow']?>").css({"color":"#75b70d"});
                  }else{
                      $("#h2_"+userID).text("");
                  }

                  $("#status_"+userID).val(0);

                  var present_count = parseInt($("#present_count").val())+1;
                  var outing_count = parseInt($("#outing_count").val())-1;
                  $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
                  $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");   
                  $("#present_count").val(present_count);
                  $("#outing_count").val(outing_count);                  
                  $('select').selectmenu('refresh', true);

                  var confirmed = $("#confirmed_"+userID).val();
  	              if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
  	              	  //dropdown 
                      var officalSession = document.getElementById('offical_'+userID);
                      if(officalSession){
    	            	  $("#offical_"+userID).val(0);
    					  $("#request_"+userID).val(0);
    					  $("#playTruant_"+userID).val(0);
    					  $("#absent_"+userID).val(0);
    					  $("#late_"+userID).val(0);
                      }
                      $("#rea_"+userID).val("");	 	             
  	              }
               }              
          }
    }

    function choose_all(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userID]==1){
			 	continue;
			 }
             if($("#li_"+userID).css("display")=="block"){
				 if(!$("#c_"+userID)[0].checked){
	            	 $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		             $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
	             }
             }
         }
    }

    function turn_present(){
         var present_count = 0;
         var absent_count = 0;
         var late_count = 0;
         var outing_count = 0;
         var taken = "<?=$taken?>";
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userID]==1){
			 	continue;
			 }
             var playTruantNum = $('#playTruant_'+userID).val();

             if($("#c_"+userID)[0].checked){

                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});
			     //$("#h2_"+userID).remove();
                 if(playTruantNum>0){
                     $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['PlayTruantShow']?>").css({"color":"#75b70d"});
                 }else{
                     $("#h2_"+userID).text("");
                 }
              	 $("#ab_"+userID).remove();           	 		             
                 $("#status_"+userID).val(0);
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();	  

                  var confirmed = $("#confirmed_"+userID).val();
 	              if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
 	                 //dropdown 
                     var officalSession = document.getElementById('offical_'+userID);
                     if(officalSession){
   	            	  $("#offical_"+userID).val(0);
   					  $("#request_"+userID).val(0);
   					  $("#playTruant_"+userID).val(0);
   					  $("#absent_"+userID).val(0);
   					  $("#late_"+userID).val(0);
                     }
                     $("#rea_"+userID).val("");	 	             
 	              }                           
             }

             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
             else if($('#status_'+userID).val()==2) late_count++;
             else if($('#status_'+userID).val()==3) outing_count++;
             
         }
         $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
         $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");                 
         $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
         $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");            
         $("#present_count").val(present_count);
         $("#absent_count").val(absent_count);
         $("#late_count").val(late_count);
         $("#outing_count").val(outing_count);
         $('select').selectmenu('refresh', true);
         
    }

    function turn_absent(){
    	 var present_count = 0;
         var absent_count = 0;
         var late_count = 0;
         var outing_count = 0;
         var taken = "<?=$taken?>";
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userID]==1){
			 	continue;
			 }
             if($("#c_"+userID)[0].checked){
                 
                 
                 //if($("#status_"+userID).val()==0){
                 //	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p><h2 style='color:#d92b2b;' id='h2_"+userID+"'><?//=$Lang['StudentAttendance']['Absent']?>//</h2> ");
                 //}
                 //else if($("#status_"+userID).val()==2||$("#status_"+userID).val()==3){

                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p>");
                 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Absent']?>").css({"color":"#d92b2b"});
                 	                   	
                 // }

              	          	 		             
                 $("#status_"+userID).val(1);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

		         var confirmed = $("#confirmed_"+userID).val();
	 	         if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
	 	         	//dropdown 
		            var officalSession = document.getElementById('offical_'+userID);
                    if(officalSession){
		                $("#offical_"+userID).val(0);
		                $("#late_"+userID).val(0);
		 				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){?>
		 				    $("#request_"+userID).val(<?=($Settings['DefaultNumOfSessionForLeave'])?>);
		 				<?}else{?>
		 				    $("#request_"+userID).val(1);
		 				<?}?>
		 				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){?>
		 				    $("#playTruant_"+userID).val(<?=($Settings['DefaultNumOfSessionForAbsenteesism'])?>);
		 				<?}else{?>
		 			     	$("#playTruant_"+userID).val(0);
		 				<?}?>
		 				<?php if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){ ?>
		 			    	$("#absent_"+userID).val(<?=($Settings['DefaultNumOfSessionForAbsent'])?>);
						<?php } ?>
                    }
	 	         	
	                $("#rea_"+userID).val($("#defaultAbsentRea_"+userID).val());	 	             
	 	         }     
             }

             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
             else if($('#status_'+userID).val()==2) late_count++;
             else if($('#status_'+userID).val()==3) outing_count++;
         }
         $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
         $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");                 
         $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
         $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");            
         $("#present_count").val(present_count);
         $("#absent_count").val(absent_count);
         $("#late_count").val(late_count);
         $("#outing_count").val(outing_count);
         $('select').selectmenu('refresh', true);
    
    }

    function turn_late(){
    	 var present_count = 0;
         var absent_count = 0;
         var late_count = 0;
         var outing_count = 0;
         var taken = "<?=$taken?>";
         
 		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userID]==1){
			 	continue;
			 }
             if($("#c_"+userID)[0].checked){
                 
                 
                 //if($("#status_"+userID).val()==0){
                 //	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"}).append("<h2 style='color:#0094bf;' id='h2_"+userID+"'><?//=$Lang['StudentAttendance']['Late']?>//</h2> ");
                 //}
                 //else{
                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});
                 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Late']?>").css({"color":"#0094bf"});
                 $("#ab_"+userID).remove();
                 // }

                 $("#status_"+userID).val(2);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

			     var confirmed = $("#confirmed_"+userID).val();
 	             if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
 	             	 //dropdown 
                     var officalSession = document.getElementById('offical_'+userID);
                     if(officalSession){
   	             	  $("#offical_"+userID).val(0);
   					  $("#request_"+userID).val(0);
   					  $("#playTruant_"+userID).val(0);
   					  $("#absent_"+userID).val(0);
   					  <?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] != -1){?>
   						  $("#late_"+userID).val(<?=($Settings['DefaultNumOfSessionForLate'])?>);
   					  <?}else{?>
   						  $("#late_"+userID).val(1);
   					  <?}?>
                     }
                     $("#rea_"+userID).val($("#defaultLateRea_"+userID).val());	 	             
 	             }
             }
             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
             else if($('#status_'+userID).val()==2) late_count++;
             else if($('#status_'+userID).val()==3) outing_count++;
         } 
         $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
         $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");                 
         $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
         $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");            
         $("#present_count").val(present_count);
         $("#absent_count").val(absent_count);
         $("#late_count").val(late_count);
         $("#outing_count").val(outing_count);
         $('select').selectmenu('refresh', true);   
    }

    function turn_outing(){
    	 var present_count = 0;
         var absent_count = 0;
         var late_count = 0;
         var outing_count = 0;
         var taken = "<?=$taken?>";
         
 		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 if(studentIdToConfirmedByAdmin[userID] && studentIdToConfirmedByAdmin[userID]==1){
			 	continue;
			 }
             if($("#c_"+userID)[0].checked){
                 
                 
                 //if($("#status_"+userID).val()==0){
                 //	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?//=$Lang['StudentAttendance']['Outing']?>//</h2> ");
                 //}
                 //else{
                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"});
                 $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['Outing']?>").css({"color":"#309100"});
                 $("#ab_"+userID).remove();
                 // }

                 $("#status_"+userID).val(3);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

			     var confirmed = $("#confirmed_"+userID).val();
 	             if((taken==0&&confirmed==0) || absent_session_toggle_ignore_confirm_check == true){
 	                 //dropdown 
                     var officalSession = document.getElementById('offical_'+userID);
                     if(officalSession){
   	            	  $("#offical_"+userID).val(0);
   					  $("#request_"+userID).val(0);
   					  $("#playTruant_"+userID).val(0);
   					  $("#absent_"+userID).val(0);
   					  $("#late_"+userID).val(0);
                     }
 	             	
                     $("#rea_"+userID).val($("#defaultOutingRea_"+userID).val());	 	             
 	             }
             }
             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
             else if($('#status_'+userID).val()==2) late_count++;
             else if($('#status_'+userID).val()==3) outing_count++;
         }
         $("#present").text("<?=$Lang['StudentAttendance']['Present']?> ("+present_count+")");
         $("#absent").text("<?=$Lang['StudentAttendance']['Absent']?> ("+absent_count+")");                 
         $("#late").text("<?=$Lang['StudentAttendance']['Late']?> ("+late_count+")");   
         $("#outing").text("<?=$Lang['StudentAttendance']['Outing']?> ("+outing_count+")");            
         $("#present_count").val(present_count);
         $("#absent_count").val(absent_count);
         $("#late_count").val(late_count);
         $("#outing_count").val(outing_count);
         $('select').selectmenu('refresh', true);                  
    }

    function submit(){
    	 if (confirm("<?=$Lang['StudentAttendance']['TeacherApp']['AreYouSureToSubmit'] ?>"))
    	   {
    	        document.form1.action = "handle_attendance.php";
    	        document.form1.submit();
    	   }
    	  else{}
    }
    
    function back(){
		 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&timeslots=<?=$timeslots?>&lessonType=<?=$lessonType?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>";
    }
    
    function cancle1(userID){
    	$("#popup1").popup("close");	
    }  
    
    function reason_confirm1(userID){
    	var status = $('#status_'+userID).val();
    	$("#popup1").popup("close");
    	
    	if($('#waived:checked').val()==1){
    	    $('#waivedStatus_'+userID).val(1);
    	}else{
    	    $('#waivedStatus_'+userID).val(0);    		
    	}
        
        if($('#HandIn_prove:checked').val()==1){
    	    $('#submittedFileStatus_'+userID).val(1);
    	}else{
    	    $('#submittedFileStatus_'+userID).val(0);
    	}



    	if(status!=0){
    		var reason = $('#reason1').val();
         	$('#rea_'+userID).val(reason);
    	}else{
            var playTruantSession  = $('#play_truant_session1').val()

            if(playTruantSession>0){
                $("#h2_"+userID).text("<?=$Lang['StudentAttendance']['PlayTruantShow']?>").css({"color":"#75b70d"});
            }else{
                $("#h2_"+userID).text("");
            }
        }
    	var remark = $('#remark1').val();
    	$('#rmk_'+userID).val(remark);
    	<?=$reason_confirm1_leavesession?>
    	$('#confirmed_'+userID).val(1);
    }

    function select_change(){
    	var selected = document.getElementById("status_switch_multiselect");
    	var selectedID = selected.selectedIndex;
    	if(selectedID==1){
    		turn_present();
        }
    	else if(selectedID==2){
    		turn_late();	          
    	}
    	else if(selectedID==3){
    		turn_absent();
    	}
    	else if(selectedID==4){
    		turn_outing();
    	}
//    	$('#turn_to').prop('selected', true);
//     	$('#status_switch_multiselect').selectmenu('refresh');
//     	$('#turn_to').selectmenu('enable');
// 		var elements = selected.options;
//     	for(var i = 0; i < elements.length; i++){
//     	      elements[i].selected = false;
//     	    }
  	$('#turn_to').attr('selected', true);
 	$('#turn_to').prop('selected', true);	    
// 		$('#turn_to').selectmenu('disable');
//    	$('#turn_to').selectmenu('refresh');
    }

    function select_filter(){
    	var selected=document.getElementById("status_switch");
    	var selectedID = selected.selectedIndex;
    	if(selectedID==0){
	   	    for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
			    $('#li_'+userID).show();
	        }   
        }
    	else if(selectedID==1){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==0){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==2){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==1){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==3){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==2){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}    	
    	else if(selectedID==4){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==3){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    }
    
    function reason_pool(){
	    $('#reason_pool_div').toggle();
    }   
    
     function input_reason(id){
        $('#reason1').val($('#rea_pool_'+id).text());
	    $('#reason_pool_div').toggle();
     }

     function remark_pool(){
         $('#remark_pool_div').toggle();
     }

    function input_remark(id){
        $('#remark1').val($('#remark_pool_'+id).text());
        $('#remark_pool_div').toggle();
    }

     function CheckingCheckboxes(){
		 tag_class_and_no();
		 tag_name();
		 checked_all_or_not();
     }
 	     
     function tag_name(){
       var NameisChecked = $('#showStudentName:checked').val()?true:false;
    	if(NameisChecked){
    		$('p.StudentName').show();
    		$("#isShowStudentInfoName").val("1");
        }  else {
        	$('p.StudentName').hide();
        	$("#isShowStudentInfoName").val("0");
        } 
    		
     }


     function othersAndClassAndNumChecked(){
      	var OtherOptionsChecked = false;
      	$("input[name='tagIds[]']").each(function() {
    	         if (this.checked && (this.id!= 'showClassAndNum') && (this.id!= 'SelectAll') && $('#showClassAndNum:checked').val()){
    	        		$('span.openBracket').show();
    	        		$('span.closeBracket').show();
    	         }
    	     });
     }

     function checked_all_or_not(){
       	var numberOfChecked = $("input[name='tagIds[]']:checkbox:checked").length;
       	var totalCheckboxes = $("input[name='tagIds[]']:checkbox").length;
//        	console.log(numberOfChecked + ', ' + totalCheckboxes);
       	if (!$('#SelectAll:checked').val() && numberOfChecked == totalCheckboxes -1){
       		$('#SelectAll').prop("checked", true).checkboxradio('refresh');
       		$("#isShowStudentName").val("1");	
       	} else{
       	$("input[name='tagIds[]']").each(function() {
     	         if (!this.checked){
     	        	$('#SelectAll').prop("checked", false).checkboxradio('refresh');	
     	         }
     	     });
	     		if(!$('#SelectAll:checked').val()){
	     			$("#isShowStudentName").val("0");
	     		}
       	}
      }

     
     function tag_class_and_no(){
    	var ClassAndNumisChecked = $('#showClassAndNum:checked').val()?true:false;
     	if(ClassAndNumisChecked){
         		$('p.ClassAndNum').show();
         		$('span.openBracket').hide();
        		$('span.closeBracket').hide();
     			othersAndClassAndNumChecked();
     			$("#isShowStudentInfoClassAndNo").val("1");
         } 	else {
         	$('p.ClassAndNum').hide();
         	$('span.openBracket').hide();
    		$('span.closeBracket').hide();
         	$("#isShowStudentInfoClassAndNo").val("0");
         } 
     }

     function full_tag(){
    	 var SelectAllisChecked = $('#SelectAll:checked').val()?true:false;
    	 if (SelectAllisChecked){
        	 clickAll(1);
        	 $("#isShowStudentName").val("1");
    	 }else{
        	 clickAll(0);
        	 $("#isShowStudentName").val("0");
    	 }
    	 CheckingCheckboxes();
     }

     function clickAll(val){
// 		$(".clickAll").click(function() {
 			  // if(document.getElementsByName("checkAll")[0].checked) {
 			
 			   if(val == 1){
 			    $("input[name='tagIds[]']").each(function() {
// 				$("input[name='studentIds[]']").prop("checked",true);
 			
 			        $(this).prop("checked", true).checkboxradio('refresh');
 			    });
 			   } else {
 			     $("input[name='tagIds[]']").each(function() {
 			         $(this).prop("checked", false).checkboxradio('refresh');
 			     });           
 			//	   $("input[name='studentIds[]']").prop("checked", false);
 			   }
// 			});
 		
 	}

 	function change_dropdown_icon(){
 		$( ".custom_collapsible" ).collapsible({
 			  collapsedIcon: "arrow-d",
 			  expandedIcon: "arrow-u" 
 			});

 	}
 	
</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<style type="text/css">
     ul.ui-listview.ui-listview-inset.ui-corner-all.ui-shadow {
    margin-top: 0px;
    }
       
     .tagArea{
        position: absolute;
        bottom:0;
     }

     .ui-controlgroup{ 
      margin: 0!important; 
     } 
	.ui-collapsible-content {
		padding: 0 !important;
	}
	
	label.ui-btn.ui-corner-all.ui-btn-inherit.ui-btn-icon-left {
        width: 120px;
        
    }
    

	
	.custom_collapsible{
	   width: 150px;
	    margin-right: 30px!important;
	   
	}
	
	
    #body{
        
         background:#ffffff;
         text-shadow:none;
         -webkit-touch-callout: none !important;
         padding-top: 80px; 
         padding-bottom: 72px;
    }
    #show_class{
    
         text-shadow:none;
    }
    a:link { text-decoration: none; color: black;}
    a:active {text-decoration: none; color: black !important;} 
    a:hover {text-decoration: none;color: black !important;}  
    a:visited {text-decoration: none;color: black !important;} 
   
   	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
		
    .student_list {
    }
	.student_list .ui-content{
       padding:0;

    }
	.student_list .ui-listview li {
		float: left;
        width: 80px;
		height: 104px;
        margin: auto;
        margin-top:10px ;
	    margin-left:10px ;		
	}
	.student_list .ui-listview li > .ui-btn {
		-webkit-box-sizing: border-box; /* include padding and border in height so we can set it to 100% */
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		box-sizing: border-box;
		height: 100%;
		text-shadow:none;/*no shadow*/
		width:80px;
		padding-right:0;
 		border:2px; 
 		border-style: solid;  
        border-color:#75b70d;
        
	}
	.student_list .ui-listview li.ui-li-has-thumb .ui-li-thumb {
		height: auto; /* To keep aspect ratio. */
		max-width: 100%;
		max-height: none;
	}
	/* Make all list items and anchors inherit the border-radius from the UL. */
	.student_list .ui-listview li,
	.student_list .ui-listview li .ui-btn,
	.student_list .ui-listview .ui-li-thumb {
		-webkit-border-radius: inherit;
		border-radius: inherit;
		padding:0 !important;
		
	}
	/* Hide the icon */
	.student_list .ui-listview .ui-btn-icon-right:after {
		display: none;
	}
	/* Make text wrap. */
	.student_list .ui-listview p {
		white-space: normal;
		overflow: visible;
		position: relative;
		left: 0;
		right: 0;
	}
	/* Text position */
	.student_list .ui-listview p {
		font-size: 0.8em;
		margin: 0;
        text-align:center;
		min-height: 20%;
		bottom:0;
		width:80px !important;
	}
	/* Semi transparent background and different position if there is a thumb. The button has overflow hidden so we don't need to set border-radius. */
	.student_list .ui-listview .ui-li-has-thumb p {
		background: rgba(255,255,255,.7);
	}

	.student_list .ui-listview .ui-li-has-thumb p {
		min-height: 20%;
	}
	.student_list .ui-listview .absent {
		width: auto;
        height: 100%;
		top: 0;
		left: 0;
		bottom: 0;
		background: rgba(255,255,255,.7);
		-webkit-border-top-right-radius: inherit;
		border-top-right-radius: inherit;
		-webkit-border-bottom-left-radius: inherit;
		border-bottom-left-radius: inherit;
		-webkit-border-bottom-right-radius: 0;
		border-bottom-right-radius: 0;
		
	}

	/* Animate focus and hover style, and resizing. */
	.student_list .ui-listview li,
	.student_list .ui-listview .ui-btn {
		-webkit-transition: all 500ms ease;
		-moz-transition: all 500ms ease;
		-o-transition: all 500ms ease;
		-ms-transition: all 500ms ease;
		transition: all 500ms ease;
	}
    .student_list .ui-listview .selected {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		right: 0;
    	bottom: 0;
    	opacity:0.8;
    	padding-right:2px;
        padding-bottom:2px;    	
        width:30%;
        z-index:1;   
    }
    
    .student_list .ui-listview h2 {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		top: 0;
		right: 0; 
	    font-size: 1em;
 	    margin-top:0.3em; 
 	    margin-right:0.3em; 
		text-shadow:0 0 2px #FFFFFF;
		z-index:1;
    }
</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
		
</head>
	<body>
	
		<div data-role="page" id="body">			

			<form name="form1" method="post" id="form1">
			
			    <div id="header" data-role="header" data-position="fixed" style="border:0;height:110px; background-color:#ffffff;">
			       
			        <div id="show_class" >	                
			
			           <table width="100%" style ="background-color:#429DEA;height:60px;font-size:1em;font-weight:normal;">
			
			             <tr>
			
			                <td style="padding-left:0em;">
			                   <table> 
			                     <tr><td rowspan="2">
			                    <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
			                     </td><td><?=$class_name?>
			                     </td></tr>
			                     <tr><td><?=$show_date?></td></tr>
			                   </table>
			                </td>
			
			                <td  align= "right" style="padding-right:0.5em;">
								
								    <select name="status_switch" id="status_switch" data-mini="true" onchange="select_filter()">
								
								        <option value="all" id="all"><?=$Lang['Btn']['All']?> (<?=$all_count?>)</option>
								 						
								        <option value="present" id="present"><?=$Lang['StudentAttendance']['Present']?> (<?=$present_count?>)</option>
								        
										<option value="absent" id="absent"><?=$Lang['StudentAttendance']['Absent']?> (<?=$absent_count?>)</option>
								
								        <option value="late" id="late"><?=$Lang['StudentAttendance']['Late']?> (<?=$late_count?>)</option>							
								
								        <option value="outing" id="outing"><?=$Lang['StudentAttendance']['Outing']?> (<?=$outing_count?>)</option>
								
								    </select>								
								
			                </td>	              
			
			              </tr>
			
			            </table>
			
			          </div>	
			           <div id ="header_menu" >			
			          <div id="mutiple_select" align="left" style=" float:left; background-color:white;height: 30px;font-size:1em;">     
			            <table style="margin-top: .8em">
			               <tr>
			                 <td>
			                    <a href="javascript:show_mutiple_select();" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change.png" style="width:1.4em; height:1.4em;" /> </a>
			                 </td>
			                 <td style="padding-right: .5em">
			                    <a href="javascript:show_mutiple_select();" ><?=$Lang['AccountMgmt']['MultiSelect']?></a>
			                 </td>
			               </tr>
			               
			            </table>
			          </div>		          			
			          <div id="status_select" style=" float:left; background-color:white;height: 30px;" >
			            <table >
			               <tr>
			                 <td style="padding-left: 0.3em; padding-right: 0.3em;">
			                    <a style="white-space: nowrap;" href="javascript:choose_all();" ><?=$Lang['Btn']['SelectAll']?></a>
			                 </td>
			                 <td>
				             	<a href="javascript:show_status_select()" >
				                	<table>
					               		<tr>
						           			<td>
						            			<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change_select.png" style="width:1.4em; height:1.4em;" />
						            		</td> 
						            		<td>	
						            			<?=$Lang['Btn']['Cancel']?> 
					          				</td>
					          			</tr>
				              		</table>
				              	</a>      
			                  </td>
			              <td>
			                  <select  name="status_switch_multiselect" id="status_switch_multiselect" data-mini="true" onchange="select_change()">
								
								       <option selected disabled value="" id="turn_to"><?=$Lang['StudentAttendance']['TurnTo']?></option>
								 						
								        <option value="present" id="present"><?=$Lang['StudentAttendance']['Present']?></option>
								        
										<option value="absent" id="absent"><?=$Lang['StudentAttendance']['Late']?></option>
								
								        <option value="late" id="late"><?=$Lang['StudentAttendance']['Absent']?></option>							
								
								        <option value="outing" id="outing"><?=$Lang['StudentAttendance']['Outing']?></option>
								
								    </select>		
							</td>
			               </tr>			               
			            </table>			
			          </div>		
			          <div id="ShowStudentInfo" align="left" style=" background-color:white;height: 30px;font-size:1em;" >     
			            <table> 
			               <tr>
			                 <td>
			                 <div data-role='collapsible' class='custom_collapsible'>
			                 	<h1 id='showTagDropdpwnLabel'><?=$Lang['eClassApp']['StudentAttendance']['ShowStudentTag']?></h1>		       
			                   <fieldset  data-role='controlgroup' name="tag_select" id="tag_select" data-mini="true">
			 			                 
			 			                <label for="SelectAll"><?=$Lang['Btn']['SelectAll'] ?></label>		
			                			<input type="checkbox" value="showAllInfo" id="SelectAll" name="tagIds[]" onchange="full_tag();">               			
			                			
			                			<label for="showStudentName"><?=$Lang['eClassApp']['StudentAttendance']['ShowNames']?></label>				 						
								        <input type="checkbox" value="showStudentName" id="showStudentName" name="tagIds[]" onchange="CheckingCheckboxes();">
								        
								        <label for="showClassAndNum"><?=$Lang['eClassApp']['StudentAttendance']['ShowClassAndNum']?></label>
										<input type="checkbox" value="showClassAndNum" id="showClassAndNum" name="tagIds[]" onchange="CheckingCheckboxes();">
								    </fieldset>	
								 </div>
			                 </td>
			               </tr>			               
			            </table>
			          </div>
			       </div>
			     </div>
	            <div data-role="content" class="student_list">
	                 <ul data-role="listview" data-inset="true">	                 
	                    <?=$student_list?>           	            	            
			         </ul>
	            </div>	            	           
	            <a id='link1' href='#popup1'  data-rel='popup' data-position-to='window' class='ui-btn ui-corner-all' data-transition='pop' style='display: none;'></a>
			    <div data-role='popup' id='popup1' data-theme='a' class='ui-corner-all'>
					 <div style='padding:10px 20px;'>
					 	  <?=$Lang['eClassApp']['TakeAttendance']['ArrivalTime']?>					      
					      <p id='arrival_time1'> - - - </p>
                          <?=$Lang['eClassApp']['TakeAttendance']['LeaveTime']?>
                          <p id='leave_time1'> - - - </p>
                          <?php
        	                 if($lcardattend->TeacherCanManageWaiveStatus){
					      ?>					      
					      <div id='waivedCheckbox'><?=$i_SmartCard_Frontend_Take_Attendance_Waived?><br><br><input type="checkbox" class="waiveBtn" name="waived" id="waived" value="1"><br></div>			      
					      <?php
                               }
					      ?>
					      <?php 
					      if($lcardattend->TeacherCanManageProveDocumentStatus){
					      ?>
					      <div id="submittedFileCheckbox"><?=$Lang['StudentAttendance']['ProveDocument']?><br><br><input type="checkbox" class="HandIn" name="HandIn_prove" id="HandIn_prove" value="1"><br></div>			      					      
					      <?php 
					      } 
					      ?>
					      <?php
					         if($lcardattend->TeacherCanManageReason){					      
					      ?>			      					      
		                  <div id='reason_insert'><?=$Lang['StudentAttendance']['Reason']?><a href="javascript:reason_pool();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_on.gif" style="width:30; height:30;" /></a>
						  <input type='text' value = '' id='reason1' data-theme='a' >
						  <div id="reason_pool_div" style="display:none" class="reason_pool">
						     <ul id="reason_pool" data-role='listview' data-inset='true'>
						     </ul>
					      </div></div>
					      <?php 
					           }
					          if($lcardattend->TeacherCanManageTeacherRemark){
					      ?>
						  <?=$Lang['StudentAttendance']['Remark']?><a href="javascript:remark_pool();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_pre-set_on.gif" style="width:30; height:30;" /></a>
						  <input type='text' value = '' id='remark1' data-theme='a'>
                          <div id="remark_pool_div" style="display:none" class="remark_pool">
                              <ul id="remark_pool" data-role='listview' data-inset='true'>
                                  <?=$remark_pool_data?>
                              </ul>
                          </div>


						  <?php }?>
						  <?=$Lang['StudentAttendance']['OfficeRemark']?>					      
					      <p id='office_remark1'> - - - </p>

                         <div id="preset_reason_div">
							 <?=$Lang['StudentAttendance']['PresetAbsenceReason']?>
                             <p id='preset_reason1'> - - - </p>
                         </div>

                         <div id="preset_remark_div">
							 <?=$Lang['StudentAttendance']['PresetAbsenceRemark']?>
                             <p id='preset_remark1'> - - - </p>
                         </div>

						    <?php if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
						    <?=$Lang['StudentAttendance']['Late']?>
						    <?=$selLateSession1?>
						    <?=$Lang['StudentAttendance']['RequestLeave']?>
						    <?=$selRequestLeaveSession1?>
						    <?=$Lang['StudentAttendance']['PlayTruant']?>
						    <?=$selPlayTruantSession1?>
						    <?=$Lang['StudentAttendance']['OfficalLeave']?>
						    <?=$selOfficalLeaveSession1?>						    
						    <?= $Lang['StudentAttendance']['Absent']?>
						    <?=$selAbsentSession1?>
						  <?php } ?>



						  <div  align='center'>
							   <a href='' data-role='button' data-inline='true' id='cancle_btn1'><?=$Lang['Btn']['Cancel']?></a>	   							   
							   <a href='' data-role='button' data-inline='true' id='confirm_btn1' style ="<?=$displaySubmitButton?>"><?=$Lang['Btn']['Confirm']?></a>
						  </div>
		             </div>
		        </div>
  	    	    <input type="hidden" value = "<?=$id?>" name="id">
			    <input type="hidden" value = "<?=$charactor?>" name="charactor">
			    <input type="hidden" value = "<?=$date?>" name="date">
			    <input type="hidden" value = "<?=$timeslots?>" name="timeslots">
			    <input type="hidden" value = "<?=$student_array_string?>" name="student_list">
			    <input type="hidden" value = "" name="mode" id="mode"> 
			    <input type="hidden" value=<?=$token?> name="token">
                <input type="hidden" value=<?=$uid?> name="uid">
                <input type="hidden" value=<?=$ul?> name="ul">
                <input type="hidden" value=<?=$parLang?> name="parLang">
                <input type="hidden" value=<?=$present_count?> name="present_count" id="present_count">
                <input type="hidden" value=<?=$absent_count?> name="absent_count" id="absent_count">
                <input type="hidden" value=<?=$late_count?> name="late_count" id="late_count">
                <input type="hidden" value=<?=$outing_count?> name="outing_count" id="outing_count">
                <input type="hidden" value=<?=$taken?> name="taken" id="taken">         
                <input type="hidden" value=<?=$isShowStudentInfoName?> name="isShowStudentInfoName" id="isShowStudentInfoName">
                <input type="hidden" value=<?=$isShowStudentInfoClassAndNo?> name="isShowStudentInfoClassAndNo" id="isShowStudentInfoClassAndNo">                                 
                <input type="hidden" value=<?=$isShowStudentName?> name="isShowStudentName" id="$isShowStudentName">
                <input type="hidden" value ="<?=$lessonType?>" name="lessonType">
                <input name="PageLoadTime" type="hidden" value="<?=$page_load_time?>">   
                 <?=$reason_submit?>      
			     <?=$status_array?> 
			     <?=$recordID_array?>
			     <?=$arrival_time?>
                 <?=$leave_time?>
                 <?=$post_info?>
                 <?=$waivedStatus?>
                 <?=$submittedFileStatus?>                 
                 <div id="footer" data-role="footer" data-position="fixed"  style ="background-color:white;height:70px;<?=$displaySubmitButton?>" align="center" >
	               <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
	                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$Lang['StudentAttendance']['Confirm']?></a>

	            </div>	 
			</form>

		</div>
		
	</body>
</html>

<?php

intranet_closedb();

?>