<?php 

$PATH_WRT_ROOT = "../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

$uid = IntegerSafe($_GET['uid']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['CurrentSchoolYearID'] ='';

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

if(!isset($date) || $date=="")
{
	$date=date('m/d/Y');
}

$selected_date = $date;

if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) {
	$date = date('m/d/Y');
	$subject_teacher_date = $date;
	$charactor = "subjectgroup";
}


$ts_record = strtotime($date);
if ($ts_record == -1)
{
	$ts_record = strtotime(date('m/d/Y'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$ToDate = date('Y-m-d',strtotime($date));
$today = date('m/d/Y');

if($is_early_leave==1)
{
    $is_early_leave=1;
}else{
    $is_early_leave=0;
}

$hide_tabs = "";
/*
if($sys_custom['eClassApp']['eAttendanceTakeEarlyLeave']){
    $hide_tabs="";
}else{
    $hide_tabs='style="visibility:hidden"';
}
*/

$libdb = new libdb();
$lcardattend = new libcardstudentattend2();
$linterface = new interface_html();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();
$attendance_mode = $lcardattend->attendance_mode;
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

$LessonAttendanceSettingList = array("'TeacherCanTakeAllLessons'");
$LessonAttendanceSettings = $GeneralSetting->Get_General_Setting('LessonAttendance',$LessonAttendanceSettingList);

if(method_exists($lcardattend,'getAttendanceGroupRecords')){
	$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
	$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$uid));
	$responsible_user_group_ids = Get_Array_By_Key($responsible_user_groups,'GroupID');

		// get all group admins with group id as key to admin users
		$groupIdToAdminUsers = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'GroupIDToUsers'=>1));
		// go through all groups to see any admin users added to the group
		for($i=0;$i<count($attendance_groups);$i++){
			if(!isset($groupIdToAdminUsers[$attendance_groups[$i]['GroupID']]) && !in_array($attendance_groups[$i]['GroupID'],$responsible_user_group_ids)){
				// if the group hasn't set any admin, staff can take it
				$responsible_user_group_ids[] = $attendance_groups[$i]['GroupID'];
			}
		}
		
	$responsible_user_group_id_size = count($responsible_user_group_ids);
}

$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;

if(!isset($charactor) || $charactor=="")
{
	if($sys_custom['PowerClass'] || get_client_region() == 'zh_TW') {
		if ($plugin['attendancelesson']) {
			$charactor = "class";
		}
	} else {
		if ($plugin['attendancestudent']) {
			$charactor = "class";
		} else if ($plugin['attendancelesson']) {
			$charactor = "lesson";
		}
	}
}
$charactor = intranet_htmlspecialchars($charactor);

if(!isset($timeslots) || $timeslots=="")
{
	if($attendance_mode==0){
		$timeslots = "am";
	}
	else if($attendance_mode==1){
		$timeslots = "pm";
	}else{
	
		if(date("a")=="am")
		{
			$timeslots = "am";
		}
		else
		{
			$timeslots = "pm";
		}
	}
}

if(!isset($msg) || $msg=="")
{
	$msg=0;
}


if($timeslots == "am") 	$DayType = PROFILE_DAY_TYPE_AM;
	
else 	$DayType = PROFILE_DAY_TYPE_PM;
	
if ($Settings['EnableEntryLeavePeriod']==1) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on u.UserID = selp.UserID 
        	AND 
        	'".$ToDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}
	
$show_main_table = "";
if($charactor=="class"){
	
	 $class_list = $lcardattend->getClassListToTakeAttendanceByDate($ToDate,$_SESSION['UserID']);
	
	 if(sizeof($class_list)==0){
		$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	 }
	 for($i=0;$i<sizeof($class_list);$i++){
	 	
	 	list($itr_class_id, $itr_class_name,$YearName,$itr_class_name_en) = $class_list[$i];
	 	//$show_main_table .= "<tr><td  width='25%'>".$itr_class_name."</td>";
	 	
	 	$sql1="SELECT COUNT(DISTINCT a.UserID) FROM YEAR_CLASS_USER as a INNER JOIN INTRANET_USER as u on a.UserID=u.UserID ".$FilterInActiveStudent ." where a.YearClassID='".$itr_class_id."' and u.RecordType = 2 AND u.RecordStatus IN (0,1,2)";

	 	$result1 = $libdb->returnVector($sql1);
	 	//$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";	
	 	 	
	    $sql2="SELECT COUNT(DISTINCT ClassID) FROM ".$card_student_daily_class_confirm." WHERE ClassID='".$itr_class_id."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
	    $result2 = $libdb->returnVector($sql2);
	  	
	    $show_main_table .= "<tr><td class='line' width='25%'><a href='javascript:handle_attendance(".$itr_class_id.",".$taken.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$itr_class_name."</a></td>";
	    $show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";	
	    if($result2[0]==0){
	    	$show_main_table .= "<td style='color:red;' width='25%'  class='line'>".$Lang['StudentAttendance']['NotTake']."</td>";
	    	$taken=0;
	    }
	    else{
	    	$show_main_table .= "<td style='color:#3a960e;' width='25%'  class='line'>".$Lang['StudentAttendance']['HasTaken']."</td>";     
	    	$taken=1;       	 
	    }
	              
	    $show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$itr_class_id.",".$taken.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
	 }	

}

if($charactor=="group"){
	
	$group_list = $lcardattend->getGroupListToTakeAttendanceByDate($ToDate);
	
	if(sizeof($group_list)==0){
		$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	for($i=0;$i<sizeof($group_list);$i++){
		 
		list($itr_group_id, $itr_group_name) = $group_list[$i];
			//$show_main_table .= "<tr><td width='25%'>".$itr_group_name."</td>";
   		if($responsible_user_group_id_size>0 && in_array($itr_group_id,$responsible_user_group_ids))
	    {
			$sql1="SELECT COUNT(DISTINCT ug.UserID) FROM INTRANET_USERGROUP as ug INNER JOIN INTRANET_USER as u on ug.UserID = u.UserID ".$FilterInActiveStudent ." where ug.GroupID='".$itr_group_id."' and u.RecordType = 2 AND u.RecordStatus IN (0,1)";
	
			$result1 = $libdb->returnVector($sql1);
			//$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";
		 	
			$sql2="SELECT COUNT(DISTINCT GroupID) FROM ".$card_student_daily_group_confirm." WHERE GroupID='".$itr_group_id."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
			$result2 = $libdb->returnVector($sql2);
			
			
			$show_main_table .= "<tr><td class='line' width='25%'><a href='javascript:handle_attendance(".$itr_group_id.",".$taken.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$itr_group_name."</a></td>";
			$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";
			if($result2[0]==0){
				$show_main_table .= "<td style='color:red;' width='25%'  class='line'>".$Lang['StudentAttendance']['NotTake']."</td>";
				$taken=0;
			}
			else{
				$show_main_table .= "<td style='color:#3a960e;' width='25%'  class='line'>".$Lang['StudentAttendance']['HasTaken']."</td>";
				$taken=1;
			}
	
			$show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$itr_group_id.",".$taken.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
        }
	}
}

if($charactor=="subjectgroup"){

    if($sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']){
    	
    	$SubjectGroupList = $lcardattend->Get_Subject_Group_Session($_SESSION['UserID'],$ToDate,true);
    	
		if(sizeof($SubjectGroupList)==0){
			$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
		}
		for($i=0;$i<sizeof($SubjectGroupList);$i++){
				
			list($SubjectGroupID,$ClassTitleEN,$ClassTitleB5) = $SubjectGroupList[$i];
			$SubjectGroupName = Get_Lang_Selection($ClassTitleB5,$ClassTitleEN);
			
			//$show_main_table .= "<tr><td  width='25%'>".$SubjectGroupName."</td>";
				
			$sql1="SELECT COUNT(DISTINCT su.UserID) FROM SUBJECT_TERM_CLASS_USER as su INNER JOIN INTRANET_USER as u on su.UserID = u.UserID ".$FilterInActiveStudent ." where su.SubjectGroupID='".$SubjectGroupID."' and u.RecordType = 2 AND u.RecordStatus IN (0,1,2)";
			$result1 = $libdb->returnVector($sql1);
			//$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";
			 
			$sql2="SELECT COUNT(DISTINCT SubjectGroupID) FROM ".$card_student_daily_subject_group_confirm." WHERE SubjectGroupID='".$SubjectGroupID."' AND DayType='".$DayType."' AND DayNumber='".$txt_day."'";
			$result2 = $libdb->returnVector($sql2);
			
			$show_main_table .= "<tr><td class='line' width='25%'><a href='javascript:handle_attendance(".$SubjectGroupID.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$SubjectGroupName."</a></td>";
			if($result2[0]==0){
				$show_main_table .= "<td style='color:red;' width='25%'  class='line'>".$Lang['StudentAttendance']['NotTake']."</td>";
			}
			else{
				$show_main_table .= "<td style='color:#3a960e;' width='25%'  class='line'>".$Lang['StudentAttendance']['HasTaken']."</td>";
			}
	
			$show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$SubjectGroupID.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
		}
			 
	 }else{
	 	
	   	 $scm_ui = new subject_class_mapping_ui();
		 $fcm = new form_class_manage();
		 $YearTermInfo = $fcm->Get_Academic_Year_And_Year_Term_By_Date($ToDate);
		 $YearTermID_ = $YearTermInfo[0]['YearTermID'];

		 if($lcardattend->ClassTeacherTakeOwnClassOnly || $sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance'])
		 {
			$scm = new subject_class_mapping();
			$TeachingOnly_ = 1;
			if ($YearTermID == '')
			{
				$YearTermArr = getCurrentAcademicYearAndYearTerm();
				$YearTermID = $YearTermArr['YearTermID'];
			}

			 if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) {
				 $SubjectName = '';
				 $SubjectID_array = array();
				 $SubjectGroupID_array = array();
			     $allow_timeslot_ids = array();
				 $current_TimeSlotID = '';
				 $current_day_type = '';
				 $current_timeslots = '';
				 $allow_take_attendance = false;
				 $timetableid = $lcardattend->getDateTimeTableID($ToDate);
				 $ltimetable = new TimeTable($timetableid);
				 $timeslot_list = $ltimetable->Get_TimeSlot_List();

				 foreach($timeslot_list as $temp) {
                    if(in_array($temp['DisplayOrder'], $sys_custom['StudentAttendance']['SubjectTeacherTakeAttendanceTimeslot'])) {
						$allow_timeslot_ids[] = array($temp['TimeSlotID'], $temp['StartTime'], $temp['EndTime'], $temp['SessionType']);
					}
				 }

				 $currenttime = time();
				 //$currenttime = strtotime("2020-08-07 08:10:01");
				 //$currenttime = strtotime("2020-08-07 13:45:01");
				 foreach($allow_timeslot_ids as $timeslot_temp) {
					 $starttime = strtotime($ToDate . " " . $timeslot_temp[1]);
					 $endtime = strtotime($ToDate . " " . $timeslot_temp[2]);
					 if ($currenttime >= $starttime && $currenttime <= $endtime) {
						 $current_TimeSlotID = $timeslot_temp[0];
						 $current_timeslots = $timeslot_temp[3];
						 $current_day_type = ($timeslot_temp[3] == 'am') ? PROFILE_DAY_TYPE_AM : PROFILE_DAY_TYPE_PM;
						 $allow_take_attendance = true;
					     break;
					 }
				 }

				 if($current_TimeSlotID != '') {
					 $lesson_oveview = $lcardattend->Get_Class_Lesson_Attendance_Overview2($ToDate);
					 foreach ($lesson_oveview as $temp) {
						 if ($temp['TimeSlotID'] == $current_TimeSlotID) {
							 $SubjectGroupID_array[] = $temp['SubjectGroupID'] . '_' . $temp['SubjectID'];
							 $SubjectID_array[] = $temp['SubjectID'];
							 $thisSubjectNameEN = intranet_htmlspecialchars($temp['SubjectNameEN']);
							 $thisSubjectNameB5 = intranet_htmlspecialchars($temp['SubjectNameB5']);
							 $SubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
						 }
					 }
				 }

				 $SubjectID_array = array_unique($SubjectID_array);
				 $SubjectID = $SubjectID_array;
				 $tmp_subject_group_list = $scm->Get_Subject_Group_List($YearTermID, $SubjectID_array, $returnAssociate=0, $TeachingOnly_);
				 $IncludeSubjectIDArr_ = array_values(array_unique(Get_Array_By_Key($tmp_subject_group_list, 'SubjectID')));

			 } else {
				 $tmp_subject_group_list = $scm->Get_Subject_Group_List($YearTermID, $SubjectID, $returnAssociate = 0, $TeachingOnly_);
				 $IncludeSubjectIDArr_ = array_values(array_unique(Get_Array_By_Key($tmp_subject_group_list, 'SubjectID')));
			 }
		 }else{
			$TeachingOnly_ = 0;
			$IncludeSubjectIDArr_ = '';
		 }
		 
		 $SubjectSelect = $scm_ui->Get_Subject_Selection("SubjectID", $SubjectID, 'javascript:refresh();', $noFirst_='0', $firstTitle_=$button_select, $YearTermID_='', $OnFocus_='', $FilterSubjectWithoutSG_=1, $IsMultiple_=0, $IncludeSubjectIDArr_);
	 		 	
	 	 if($SubjectID!=""){
			if ($YearTermID == '')
			{
				$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
				$YearTermID = $CurrentYearTermArr['YearTermID'];
			}

			$scm = new subject_class_mapping();
			$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID, $SubjectID, $returnAssociate=1,$TeachingOnly_);
			$SubjectGroupArr = $SubjectGroupInfoArr['SubjectGroupList'];
		 	$numOfSubjectGroup = count($SubjectGroupArr); 


			if($numOfSubjectGroup==0){
				$show_main_table .= "<tr><td colspan=4>" . $Lang['General']['NoRecordAtThisMoment'] . "</td></tr>";
			}

			if ($numOfSubjectGroup > 0)
			{
			    $numOfRecord = 0;
				foreach ($SubjectGroupArr as $thisSubjectGroupID => $thisSubjectGroupArr)
				{
				    $day_type = $DayType;
					if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) {
					    $temp = $thisSubjectGroupArr['SubjectGroupID'].'_'.$thisSubjectGroupArr['SubjectID'];
					    if(in_array($temp, $SubjectGroupID_array) == false) {
							continue;
						}
						$day_type = $current_day_type;
						$numOfRecord++;
                    }

					$SubjectGroupID = $thisSubjectGroupArr['SubjectGroupID'];
					$thisSubjectGroupNameEN = intranet_htmlspecialchars($thisSubjectGroupArr['ClassTitleEN']);
					$thisSubjectGroupNameB5 = intranet_htmlspecialchars($thisSubjectGroupArr['ClassTitleB5']);
					$SubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);

					//$show_main_table .= "<tr><td  width='25%'>".$SubjectGroupName."</td>";						
					$sql1="SELECT COUNT(DISTINCT su.UserID) FROM SUBJECT_TERM_CLASS_USER as su INNER JOIN INTRANET_USER as u on su.UserID = u.UserID ".$FilterInActiveStudent ." where su.SubjectGroupID='".$SubjectGroupID."' and u.RecordType = 2 AND u.RecordStatus IN (0,1,2)";
					$result1 = $libdb->returnVector($sql1);
					//$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";
					 
					$sql2="SELECT COUNT(DISTINCT SubjectGroupID) FROM ".$card_student_daily_subject_group_confirm." WHERE SubjectGroupID='".$SubjectGroupID."' AND DayType='".$day_type."' AND DayNumber='".$txt_day."'";

					$result2 = $libdb->returnVector($sql2);
					
					$show_main_table .= "<tr><td class='line' width='25%'><a href='javascript:handle_attendance(".$SubjectGroupID.");' data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$SubjectGroupName."</a></td>";
					$show_main_table .= "<td width='25%'  class='line'>".$result1[0]."</td>";
					if($result2[0]==0){
						$show_main_table .= "<td style='color:red;' width='25%'  class='line'>".$Lang['StudentAttendance']['NotTake']."</td>";
					}
					else{
						$show_main_table .= "<td style='color:#3a960e;' width='25%'  class='line'>".$Lang['StudentAttendance']['HasTaken']."</td>";
					}
			
					$show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href='javascript:handle_attendance(".$SubjectGroupID.");' data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
		
				}

				if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) {
				    if($numOfRecord == 0) {
						$show_main_table .= "<tr><td colspan=4>" . $Lang['General']['NoRecordAtThisMoment'] . "</td></tr>";
					}
				}
			}
		

	 	 }
	 	
	 }
}

if($charactor=="lesson"){

	$lesson_ordered_ary = $lcardattend->Get_Subject_Group_Session($UserID,$ToDate,$SubjectGroupListOnly="", ($LessonAttendanceSettings['TeacherCanTakeAllLessons']==1 && isset($lessonType)) ? $lessonType : 0);

	if(sizeof($lesson_ordered_ary)==0){
		$show_main_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	$LessonAttendUI = new libstudentattendance_ui();
	for ($i=0; $i<sizeof($lesson_ordered_ary); $i++) {
		$_roomAllocationId = $lesson_ordered_ary[$i]['RoomAllocationID'];
		$_lessonInfoAry = $lesson_ordered_ary[$i];
		$_lessonInfoAry['AttendOverviewID'] = $LessonAttendUI->Check_Record_Exist_Subject_Group_Attend_Overview($_lessonInfoAry['SubjectGroupID'],$ToDate,$_lessonInfoAry['RoomAllocationID']);
		$RecordType = "SubjectGroup";
		$StudentList = $lcardattend->Get_Student_List($_lessonInfoAry['SubjectGroupID'], $RecordType, $ToDate);
		$StudentListCount = count($StudentList);
		$LessonTitle = 	$_lessonInfoAry['TimeSlotName'].": ".Get_Lang_Selection($_lessonInfoAry['ClassTitleB5'],$_lessonInfoAry['ClassTitleEN']);

		$show_main_table .= "<tr><td class='line' width='25%'><a href=\"javascript:handle_attendance_lesson(".$_lessonInfoAry['SubjectGroupID'].",".$_lessonInfoAry['RoomAllocationID'].",'".$_lessonInfoAry['StartTime']."','".$_lessonInfoAry['EndTime']."','".$LessonTitle."',".$_lessonInfoAry['AttendOverviewID'].",".$taken.");\" data-transition='slide' style='color: #2286C5; text-decoration: none;'>".$LessonTitle."</a></td>";
		$show_main_table .= "<td width='25%'  class='line'>".$StudentListCount."</td>";
		if($_lessonInfoAry['AttendOverviewID'] == -1){
			$show_main_table .= "<td style='color:red;' width='25%'  class='line'>".$Lang['StudentAttendance']['NotTake']."</td>";
			$taken=0;
		}
		else{
			$show_main_table .= "<td style='color:#3a960e;' width='25%'  class='line'>".$Lang['StudentAttendance']['HasTaken']."</td>";
			$taken=1;
		}

		$show_main_table .="<td width='25%'  class='line' style='text-align:center'><a href=\"javascript:handle_attendance_lesson(".$_lessonInfoAry['SubjectGroupID'].",".$_lessonInfoAry['RoomAllocationID'].",'".$_lessonInfoAry['StartTime']."','".$_lessonInfoAry['EndTime']."','".$LessonTitle."',".$_lessonInfoAry['AttendOverviewID'].",".$taken.");\" data-role='button' data-inline='true' data-icon='carat-r'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
	}

}

//early leave table


$date = $selected_date;
$ts_record = strtotime($date);
if ($ts_record == -1)
{
	$ts_record = strtotime(date('m/d/Y'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$ToDate = date('Y-m-d',strtotime($date));
$today = date('m/d/Y');

$show_early_leave_table = "";
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$sql_early_leave  =
        "SELECT 
			a.UserID, 
	        ".getNameFieldByLang("a.")." as Name,
	        yc.".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')." as Class,
			ycu.ClassNumber,
			b.RecordID
		FROM
			$card_log_table_name as b 
			LEFT JOIN INTRANET_USER as a ON (b.UserID = a.UserID)
            INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=a.UserID
    		LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
    		INNER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID and yct.UserID = ".$UserID.")
    	    LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID)
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c ON c.StudentID = a.UserID 
         WHERE 
         		b.DayNumber = '$txt_day' 
         		AND c.RecordDate = '$ToDate' 
				AND c.DayType = '$DayType' 
				AND c.RecordType = '".PROFILE_TYPE_EARLY."' 
         		AND a.RecordType = 2 
         		AND a.RecordStatus IN (0,1,2)
         		AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
         ORDER BY 
         		Class, ClassNumber asc
		";

$early_leave_list = $libdb->returnArray($sql_early_leave);

if(sizeof($early_leave_list)==0){
    $show_early_leave_table .="<tr><td colspan=4>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
}
for($i=0;$i<sizeof($early_leave_list);$i++){

    list($early_leave_studentID, $early_leave_name,$early_leave_class_name,$early_leave_class_number, $early_leave_recordid) = $early_leave_list[$i];

    $show_early_leave_table .= "<tr><td class='line' width='25%'>".$early_leave_class_name."</td>";
    $show_early_leave_table .= "<td width='25%'  class='line'>".$early_leave_class_number."</td>";
    $show_early_leave_table .= "<td width='30%'  class='line'>".$early_leave_name."</td>";
	$show_early_leave_table .="<td width='10%'  class='line' style='text-align:center'><a href='javascript:edit_early_leave(".$early_leave_recordid.");' data-role='button' data-inline='true' data-icon='edit'  data-iconpos='notext' data-transition='slide'></a></td>";
	$show_early_leave_table .="<td width='10%'  class='line' style='text-align:center'><a href='javascript:delete_early_leave(".$early_leave_studentID.",".strtotime($ToDate).",".$DayType.");' data-role='button' data-inline='true' data-icon='delete'  data-iconpos='notext' data-transition='slide'></a></td><tr>";
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="Javascript" src='<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/lesson_attendance.js'></script>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>

<script>

	jQuery(document).ready( function() {
		 var msg = <?=$msg?>;
		 if(msg=="1") {
			 alert("<?=$Lang['StudentAttendance']['TeacherApp']['RecordHasUpdated']?>");
			 window.location.href="attendance_list.php?date=<?=urlencode($date)?>&charactor=<?=urlencode($charactor)?>&timeslots=<?=urlencode($timeslots)?>&lessonType=<?=urlencode($lessonType)?>&token=<?=urlencode($token)?>&uid=<?=urlencode($uid)?>&ul=<?=urlencode($ul)?>&is_early_leave=<?=urlencode($is_early_leave)?>&parLang=<?=urlencode($parLang)?>";
	     } else if(msg=="2") {
             alert("<?=$Lang['SysMgr']['SchoolNews']['UpdateUnsuccess']?>");
             window.location.href="attendance_list.php?date=<?=urlencode($date)?>&charactor=<?=urlencode($charactor)?>&timeslots=<?=urlencode($timeslots)?>&lessonType=<?=urlencode($lessonType)?>&token=<?=urlencode($token)?>&uid=<?=urlencode($uid)?>&ul=<?=urlencode($ul)?>&is_early_leave=<?=urlencode($is_early_leave)?>&parLang=<?=urlencode($parLang)?>";
         }

		//for charactor	
		 <?php if($plugin['attendancestudent']){?>
		 document.getElementById("<?=$charactor?>").selected=true;
		 $('select').selectmenu('refresh', true);
		 <?php } ?>
		 var charactor = '<?=$charactor?>';
		 if(charactor=='lesson'){
		 	timeslots_div
		 	$('#timeslots_div').hide();
		 }else{		 	
		 	if(!checkTargetDateChanged()){
		 		var today = '<?=$today?>';
		 		$('#date').val(today);
		 		refresh();	
		 	}
		 }
		//for timeslots
		 $( "input[id='<?=$timeslots?>']" ).prop( "checked", true ).checkboxradio( "refresh" );
		 
		 $("input[name='timeslots']").change(function(){
			 refresh();
		 });



        var is_early_leave = '<?=$is_early_leave?>';

        if(is_early_leave==1){
            $('#early_leave_tab').click();

        }else{
            $('#take_attendance_tab').click();
        }
		 
		 
	});
	
	//handle the datepicker onchange function
	function onSelectedDateOfJqueryMobileDatePicker() {
		   refresh();	
	}
	
	function refresh(){
	   charactor = '<?=$charactor?>';
	   if(charactor=='lesson'){
	        document.form1.action="attendance_list.php";
	        document.form1.submit();
	   }else{	   	
		   if(!checkTargetDateChanged()) location.reload();
	       else{
	    	   document.form1.action="attendance_list.php";
	    	   document.form1.submit();
	       }
	   }

    }

    function handle_attendance(ID,taken){
       var div = document.getElementById("div1");
       var input = document.createElement("input");
       input.type = "hidden";
       input.name = "id";
       input.value = ID;
       div.appendChild(input);
       
       var input1 = document.createElement("input");
       input1.type = "hidden";
       input1.name = "taken";
       input1.value = taken;
       div.appendChild(input1);

        <?php if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) { ?>
        $("#date").val('<?=$subject_teacher_date?>');
        $( "input[id='am']" ).prop( "checked", false );
        $( "input[id='pm']" ).prop( "checked", false );
        $( "input[id='<?=$current_timeslots?>']" ).prop( "checked", true );
        <?php } ?>

       document.form1.action = "take_attendance.php";
       document.form1.submit();
    }
    
    function handle_attendance_lesson(ID,RoomAllocationID,Start_time,End_time,LessonTitle,AttendOverviewID,taken){
       
       var div = document.getElementById("div1");
       var input = document.createElement("input");
       input.type = "hidden";
       input.name = "id";
       input.value = ID;
       div.appendChild(input);
       
       var input1 = document.createElement("input");
       input1.type = "hidden";
       input1.name = "AttendOverviewID";
       input1.value = AttendOverviewID;
       div.appendChild(input1);
       
       var input2 = document.createElement("input");
       input2.type = "hidden";
       input2.name = "taken";
       input2.value = taken;
       div.appendChild(input2);
       
       var input3 = document.createElement("input");
       input3.type = "hidden";
       input3.name = "RoomAllocationID";
       input3.value = RoomAllocationID;
       div.appendChild(input3);
       
       var input4 = document.createElement("input");
       input4.type = "hidden";
       input4.name = "Start_time";
       input4.value = Start_time;
       div.appendChild(input4);
       
       var input5 = document.createElement("input");
       input5.type = "hidden";
       input5.name = "End_time";
       input5.value = End_time;
       div.appendChild(input5);
       
       var input6 = document.createElement("input");
       input6.type = "hidden";
       input6.name = "LessonTitle";
       input6.value = LessonTitle;
       div.appendChild(input6);
       
       document.form1.action = "take_attendance.php";
       document.form1.submit();
    }
    

    function back(){
       alert("back");
    }

    function checkTargetDateChanged()
    {
    	var target_date = $('#date').val();
    	
    	var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    	return true;
    }

    function delete_early_leave(studentID, TargetDate, DayType){
        var alertConfirmRemove = "<?=$Lang['eClassApp']['WebModules']['DeleteConfirm2']?>";
        if(confirm(alertConfirmRemove)){
            $.post(
                'ajax_delete_early_leave.php',
                {
                    'studentID': studentID,
                    'TargetDate': TargetDate,
                    'DayType': DayType
                },
                function(ReturnData)
                {
                    refresh();
                }
            );
        }
    }

    function edit_early_leave(recordid) {
        document.form1.action = "edit_early_leave.php";
        document.form1.submit();
    }

    function add_early_leave(){
        document.form1.action = "add_early_leave.php";
        document.form1.submit();
    }

    function setTabs(is_early_leave){
        <?php if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) { ?>
        if(is_early_leave == 1) {
            $("#attendance_info").show();
            $("#subject_teacher_take_attendance_info").hide();
        } else {
          //  window.location.href="attendance_list.php?date=<?=urlencode($date)?>&charactor=<?=urlencode($charactor)?>&timeslots=<?=urlencode($timeslots)?>&lessonType=<?=urlencode($lessonType)?>&token=<?=urlencode($token)?>&uid=<?=urlencode($uid)?>&ul=<?=urlencode($ul)?>&is_early_leave=0&parLang=<?=urlencode($parLang)?>";
           // return;
            $("#attendance_info").hide();
            <?php if($allow_take_attendance == false) { ?>
            $("#take_attendance_data_table").hide();
			<?php } ?>
            $("#subject_teacher_take_attendance_info").show();
        }
        $("#take_attendance_info").hide();
        <?php } ?>
        $('#is_early_leave').val(is_early_leave);
    }

</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style type="text/css">
    #body{
            
         background:#ffffff;
    }
      
    .form_table_attendence{ width:100%; margin:0;}
 	.form_table_attendence tr td.line{vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;	border-bottom:2px solid #EFEFEF; padding-left:2px; padding-right:2px}
    .form_table_attendence tr td {vertical-align:center;	text-align:center; line-height:18px; padding-top:5px; padding-bottom:5px;padding-left:2px; padding-right:2px} 
		
	.form_table_attendence tr td.field_title{width:25%;  padding-left:2px; padding-right:2px;border-width: 5px;}
	.form_table_attendence tr td.field_title_short{	border-bottom:2px solid #FFFFFF ; width:14%; padding-left:2px; padding-right:2px}
	.form_table_attendence col.field_title_short{width:14%; }
	.form_table_attendence col.field_content_short{width:20%; }
	.form_table_attendence tr td a{ color: #9966CC;	}
	.form_table_attendence tr th a{ color: #FFFFFF;	}
	.form_table_attendence tr td a:hover{ color: #FF0000;	}
	.form_table_attendence col.field_c {width:10px;}
	.form_table_attendence tr td .textbox{	width:98%}
	
	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;}
<?php if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) { ?>
    #attendance_info { display: none; }
<?php } ?>
</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />	

</head>
	<body>
	
		<div data-role="page" id="body">			
   	        <div data-role="content">
                <div data-role="tabs" id="tabs">
                    <div data-role="navbar" <?=$hide_tabs?>>
                        <ul>
                            <li><a href="#TakeAttendance" data-ajax="false" onclick="javascript:setTabs(0)" id="take_attendance_tab"><?=$Lang['eClassApp']['StudentAttendance']['TakeAttendance']?></a></li>
                            <li><a href="#EarlyLeave" data-ajax="false" onclick="javascript:setTabs(1);" id="early_leave_tab"><?=$Lang['eClassApp']['StudentAttendance']['EarlyLeave']?></a></li>
                        </ul>
                    </div>
                    <?php if($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendance']) { ?>
                    <table class="form_table_attendence" width="100%" id="subject_teacher_take_attendance_info">
                        <?php if($allow_take_attendance) { ?>
                        <tr>
                            <td class="field_title">
                                <label class="label" for="date" ><?=$Lang['StudentAttendance']['RecordDate']?></label>
                            </td>
                            <td style="text-align:left;">
                                <?=$subject_teacher_date?>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">
                                <label class="label" for="charactor" ><?=$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupCategory']?></label>
                            </td>
                            <td style="text-align:left">
                               <?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']?>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title">
                                <label class="label" for="SubjectID" ><?=$Lang['Header']['Menu']['Subject']?></label>
                            </td>
                            <td style="text-align:left">
								<?=$SubjectName?>
                            </td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                            <td>
                                <?php
                                    $temp_timeslots = array();
                                    foreach($sys_custom['StudentAttendance']['SubjectTeacherTakeAttendanceTimeslot'] as $temp) {
										$temp_timeslots[] = $temp - 1;
									}
                                    echo str_replace("{TIMESLOTS}", implode(", ", $temp_timeslots), $Lang['StudentAttendance']['OnlyAllowTimeslotTakeAttendance']);
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <?php } ?>

                    <form id="form1" name="form1" method="get" >
                        <?php if(!isset($msg)||$msg=="") { ?>
                        <table class="form_table_attendence" width="100%" id="attendance_info">
                            <tr>
                                <td class="field_title">
                                    <label class="label" for="date" ><?=$Lang['StudentAttendance']['RecordDate']?></label>
                                </td>
                                <td>
                                    <input type="text" data-role="date" id="date" name="date" value="<?=escape_double_quotes($date)?>" onchange="javascript:refresh();">
                                </td>
                            </tr>
                            <tr id="timeslots_div">
                                <td class="field_title">
                                    <label class="label" for="timeslots" ><?=$Lang['StudentAttendance']['TimeSlot']?></label>
                                </td>
                                <td style="text-align:left;">
                                    <fieldset data-role="controlgroup" data-type="horizontal" width="100%" >
                                        <?php if($attendance_mode!=1){ ?>
                                            <input type="radio" name="timeslots" id="am" value="am" >
                                            <label for="am"><?=$Lang['StudentAttendance']['ApplyLeaveAry']['AM']?></label>
                                        <?php }if($attendance_mode!=0){?>
                                            <input type="radio" name="timeslots" id="pm" value="pm" >
                                            <label for="pm"><?=$Lang['StudentAttendance']['ApplyLeaveAry']['PM']?></label>
                                        <?php }?>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                        <?php } ?>
                        <div id="TakeAttendance">

                        <?php if(!isset($msg)||$msg=="") { ?>

                            <table class="form_table_attendence" width="100%" id="take_attendance_info">
                                <tr>
                                    <td class="field_title">
                                        <label class="label" for="charactor" ><?=$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupCategory']?></label>
                                    </td>
                                    <?php if($plugin['attendancestudent']&&$plugin['attendancelesson']){?>
                                        <td>
                                            <select name="charactor" id="charactor" onchange="refresh()">
                                                <option value="class" id="class" ><?=$Lang['StudentAttendance']['Class']?></option>
                                                <?php if(get_client_region() != 'zh_TW') { ?>
                                                <option value="group" id="group"><?=$Lang['Group']['GroupMgmt']?></option>
                                                <option value="subjectgroup" id="subjectgroup"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']?></option>
												<?php } ?>
                                                <option value="lesson" id="lesson"><?=$Lang['StudentAttendance']['Lesson']?></option>
                                            </select>
                                        </td>
                                    <?php }else if($plugin['attendancestudent']&&!$plugin['attendancelesson']){?>
                                        <td>
                                            <select name="charactor" id="charactor" onchange="refresh()">
                                                <option value="class" id="class" ><?=$Lang['StudentAttendance']['Class']?></option>
												<?php if(get_client_region() != 'zh_TW') { ?>
                                                <option value="group" id="group"><?=$Lang['Group']['GroupMgmt']?></option>
                                                <option value="subjectgroup" id="subjectgroup"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']?></option>
												<?php } ?>
                                            </select>
                                        </td>
                                    <?php }else if($plugin['attendancelesson']&&!$plugin['attendancestudent']){?>
                                        <td style="text-align:left">
                                            <p><?=$Lang['StudentAttendance']['Lesson']?></p>
                                            <input type='hidden' id='charactor' name='charactor' value='lesson'>
                                        </td>
                                    <?php }?>
                                </tr>
                                <?php if(!$sys_custom['StudentAttendance']['SubjectGroupAttendanceByTimeTable']&&$charactor=="subjectgroup"){ ?>
                                    <tr>
                                        <td class="field_title">
                                            <label class="label" for="SubjectID" ><?=$Lang['Header']['Menu']['Subject']?></label>
                                        </td>
                                        <td>
                                            <?=$SubjectSelect?>
                                        </td>
                                    </tr>
                                <?php }?>

                                <?php if($charactor == 'lesson') { ?>
                                    <?php if($LessonAttendanceSettings['TeacherCanTakeAllLessons']==1){ ?>
                                    <tr>
                                        <td class="field_title">
                                            <label class="label" for="SubjectID" ><?=$Lang['LessonAttendance']['LessonType']?></label>
                                        </td>
                                        <td>
											<?php
											$leasonTypeArr = array();
											$leasonTypeArr[] = array(0, $Lang['LessonAttendance']['TeachersOwnLessons']);
											$leasonTypeArr[] = array(1, $Lang['LessonAttendance']['AllLessons']);
											echo $linterface->GET_SELECTION_BOX($leasonTypeArr, "name=\"lessonType\" id=\"lessonType\" onChange=\"refresh()\"", '', $lessonType);
											?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                            <table class="form_table_attendence" width="100%" id="take_attendance_data_table">
                             <tr style="background-color:#E6E6E6;">
                                <td width="25%">
                                   <?php
                                   if($charactor=="class"){
                                      echo $Lang['StudentAttendance']['Class'];
                                   }
                                   elseif($charactor=="group"){
                                      echo $Lang['Group']['GroupMgmt'];
                                   }
                                   elseif($charactor=="subjectgroup"){
                                      echo $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'];
                                   }
                                   elseif($charactor=="lesson"){
                                      echo $Lang['StudentAttendance']['Session'];
                                   }
                                   ?>
                                </td>
                                <td width="25%">
                                   <?=$Lang['StudentRegistry']['Amount']?>
                                </td>
                                <td width="25%">
                                   <?=$Lang['General']['Status']?>
                                </td>
                                <td width="25%">
                                </td>
                             </tr>
                             <?=$show_main_table?>
                          </table>
                        <?php } ?>
                    </div>
                    <div id="EarlyLeave">
                            <?php if(!isset($msg)||$msg=="") { ?>

                                <table class="form_table_attendence" width="100%" >
                                    <tr>
                                        <td width='10%' style='text-align:right'><a href='javascript:add_early_leave();' data-role='button' data-inline='true' data-icon='plus'  data-iconpos='notext' data-transition='slide'></a></td>
                                    </tr>
                                </table>
                                <table class="form_table_attendence" width="100%" >
                                    <tr style="background-color:#E6E6E6;">
                                        <td width="25%">
                                            <?=$Lang['StudentAttendance']['Class']?>
                                        </td>
                                        <td width="25%">
                                            <?=$Lang['General']['ClassNumber']?>
                                        </td>
                                        <td width="30%">
                                            <?=$Lang['General']['StudentName']?>
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                    </tr>
                                    <?=$show_early_leave_table?>
                                </table>
                            <?php } ?>
                    </div>
                    <div id="div1">
                    </div>

                    <input type="hidden" value="<?=escape_double_quotes($token)?>" name="token">
                    <input type="hidden" value="<?=escape_double_quotes($uid)?>" name="uid">
                    <input type="hidden" value="<?=escape_double_quotes($ul)?>" name="ul">
                    <input type="hidden" value="<?=escape_double_quotes($parLang)?>" name="parLang">
                    <input type="hidden" value="<?=escape_double_quotes($is_early_leave)?>" name="is_early_leave" id="is_early_leave">
                    <input type="hidden" value="<?=escape_double_quotes($early_leave_recordid)?>" name="early_leave_recordid">
                    </form>
                </div>
			</div>
	    </div>
	   
	</body>
</html>

<?php

intranet_closedb();

?>