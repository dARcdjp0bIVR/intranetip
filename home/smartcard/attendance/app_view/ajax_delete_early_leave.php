<?php
/*
 *  2019-10-03 Cameron
 *      - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
 *      should apply updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
 *
 *  2019-05-29 Cameron
 *      - synchronize eAttendance record status to eSchoolbus [case #Y161736]
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

intranet_auth();
intranet_opendb();

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

$UserID = $_SESSION['UserID'];
$TargetDate = $_POST["TargetDate"];
$TargetDate = date('Y-m-d',$TargetDate);
$studentID = IntegerSafe($_POST['studentID']);
$DayType = $_POST["DayType"];

echo $TargetDate;
$TargetDate = date("Y-m-d",strtotime($TargetDate));
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$libeClassApp = new libeClassApp();

$libdb = new libdb();
$lu = new libuser($UserID);
$lcardattend = new libcardstudentattend2();
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType == PROFILE_DAY_TYPE_AM) {
    $DateModifiedField = "DateModified";
    $ModifyByField = "ModifyBy";
}
else {
    $DateModifiedField = "PMDateModified";
    $ModifyByField = "PMModifyBy";
}

# Remove Reason record
$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		       WHERE RecordDate = '$TargetDate'
		             AND StudentID = '$studentID'
		             AND DayType = '".$DayType."'
		             AND RecordType = '".PROFILE_TYPE_EARLY."'";
$lcardattend->db_db_query($sql);

# Remove Profile record
$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		       WHERE AttendanceDate = '$TargetDate'
		             AND UserID = '$studentID'
		             AND DayType = '".$DayType."'
		             AND RecordType = '".PROFILE_TYPE_EARLY."'";
$lcardattend->db_db_query($sql);
# Set to Daily Record Table
# Set LeaveStatus and InSchoolTime
$sql = "UPDATE $card_log_table_name
		      SET 
			      LeaveStatus = NULL,
			      ".$DateModifiedField." = NOW(),
			      ".$ModifyByField." = '".$UserID."' 
		      WHERE DayNumber = '$txt_day'
		            AND UserID = $studentID";
//echo "sql [".$sql."]<br>";
echo $sql;
$lcardattend->db_db_query($sql);

if ($DayType == PROFILE_DAY_TYPE_AM && $lcardattend->attendance_mode == 3 && $lcardattend->PMStatusNotFollowAMStatus != 1) {
    $lcardattend->Auto_Set_PM_Status(CARD_STATUS_PRESENT,$studentID,$TargetDate);
}

// Early Leave -> Present on Time
if ($plugin['eSchoolBus']) {
    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($studentID);
    if ($isTakingBusStudent) {
        $sql = "SELECT RecordID FROM $card_log_table_name WHERE UserID = '$studentID' AND DayNumber = '$txt_day'";
        $dailyLogAry = $lcardattend->returnResultSet($sql);
        if (count($dailyLogAry)) {
            $my_record_id = $dailyLogAry[0]['RecordID'];
            $conditionAry['AttendanceRecordID'] = $my_record_id;
            $conditionAry['StudentID'] = $studentID;
            $conditionAry['StartDate'] = $TargetDate;
            $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
            $Result['ChangeFromAbsentToOnTime_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

            unset($conditionAry);
            $conditionAry['t.AttendanceRecordID'] = $my_record_id;
            $conditionAry['t.LeaveDate'] = $TargetDate;
            $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
            $Result['ChangeFromAbsentToOthers_Timeslot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($studentID, $conditionAry);
        }
    }
}

intranet_closedb();

?>