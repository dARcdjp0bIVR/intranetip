<?php
// use by kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();

$toDay=date('Y-m-d');
$page_load_time = time();
if(!$plugin['attendancestudent'])
{
        header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

// get default attend status
// get default status from basic settings
$student_attend_default_status = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));
$student_attend_default_status = ($student_attend_default_status == "")? CARD_STATUS_ABSENT:$student_attend_default_status;

$luser = new libuser($UserID);
$isStudentHelper = false;
$isStudentHelper= $luser->isStudent();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();
/*
if ($lcardattend->attendance_mode==1)
{
    header("Location: index.php");
    exit();
}
*/

$linterface         = new interface_html();
$lsmartcard			= new libsmartcard();
$CurrentPage        = "PageTakeAttendance";

if ($luser->isStudent())
{
    $class_name = $luser->ClassName;
}

$class_list = $lcardattend->getClassListToTakeAttendanceByDate($toDay);
$group_list = $lcardattend->getGroupListToTakeAttendanceByDate($toDay);

/*
if($class_name=="")
{
        list($itr_class_id, $itr_class_name) = $class_list[0];
        $class_name = $itr_class_name;
}
*/


### Period setting
//if($period=="")        $period = "AM";
switch ($period){
        case "AM": $display_period = $i_DayTypeAM;  break;
        case "PM": $display_period = $i_DayTypePM;  break;
        //default : $display_period = $i_DayTypeAM; $period = "AM";
}

if($class_name)
{
        # Check this class need to take attendance
        if (!$lcardattend->isRequiredToTakeAttendanceByDate($class_name,$toDay))
        {
             header ("Location: /");
             intranet_closedb();
             exit();
        }
}

### build confirmation info table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".date("Y")."_".date("m");

# Group Type defaults to "Class"
$groupType = 1;

### Class select
$class_select = "<SELECT id=\"class_name\" name=\"class_name\" onChange=\"retrivePMTime(1);\">";
$empty_selected = ($class_name == '')? "SELECTED":"";
$class_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($class_list); $i++)
{
        list($itr_class_id, $itr_class_name) = $class_list[$i];
        $selected = ($itr_class_name==$class_name)?"SELECTED":"";
        $class_select .= "<OPTION value=\"".$itr_class_name."\" $selected>".$itr_class_name."</OPTION>\n";
}
$class_select .= "</SELECT>\n";

### Group select
$group_select = "<SELECT id=\"group_id\" name=\"group_id\" onChange=\"retrivePMTime(2);\">\n";
$empty_selected = ($group_id == '')? "SELECTED":"";
$group_select .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
for($i=0; $i<sizeOf($group_list); $i++)
{
        list($itr_group_id, $itr_group_name) = $group_list[$i];
        $selected = ($itr_group_id==$group_id)?"SELECTED":"";
        $group_select .= "<OPTION value=\"".$itr_group_id."\" $selected>".$itr_group_name."</OPTION>\n";
}
$group_select .= "</SELECT>\n";



### Time slot select
/*
$slot_select = "<SELECT name=period>\n";
if ($lcardattend->attendance_mode != 1)
{
    $slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
    //$slot_select .= "<input type=\"radio\" name=\"period\" value=\"AM\" ($period=='AM'?'SELECTED':'') >$i_DayTypeAM ";
}
if ($lcardattend->attendance_mode != 0)
{
    $slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
    //$slot_select .= "<input type=\"radio\" name=\"period\" value=\"PM\" ($period=='PM'?'SELECTED':'') >$i_DayTypePM ";
}
$slot_select .= "</SELECT>\n";
*/
### Modifing By Ronald On 20081015


if ($period != "" && $class_name!="")
{
	#  use pull-down menu when taking

	//$slot_select = "<SELECT name=period onChange=\"javascript: changeSlot(this.value, '$class_name');\" >\n";
	$slot_select = "<SELECT name=period >\n";
	if ($lcardattend->attendance_mode != 1)
	{
		$slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
		//$slot_select .= "<input type=\"radio\" name=\"period\" value=\"AM\" ($period=='AM'?'SELECTED':'') >$i_DayTypeAM ";
	}
	if ($lcardattend->attendance_mode != 0)
	{
		$slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
		//$slot_select .= "<input type=\"radio\" name=\"period\" value=\"PM\" ($period=='PM'?'SELECTED':'') >$i_DayTypePM ";
	}
	$slot_select .= "</SELECT>\n";

} else
{
	# by default, radio buttons are used

	//$slot_select = "<SELECT name=period onChange=\"javascript: changeSlot(this.value, '$class_name');\" >\n";
	$slot_select = "<SELECT name=period >\n";
	if ($lcardattend->attendance_mode != 1)
	{
		$slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
		//$slot_select .= "<input type=\"radio\" name=\"period\" value=\"AM\" ".($period=='AM' ? 'CHECKED' : ($period=='' ? 'CHECKED' : '') ) .">$i_DayTypeAM ";
	}
	if ($lcardattend->attendance_mode != 0)
	{
		$slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
		//$slot_select .= "<input type=\"radio\" name=\"period\" value=\"PM\" ".($period=='PM'?'CHECKED':'') .">$i_DayTypePM ";
	}
	$slot_select .= "</SELECT>\n";
}
####

$DayPeriod = $period == "AM" ? "2" : "3";

if($class_name && $period)
{
        ### Last update string
        $sql = "SELECT
              a.RecordID,
              IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
              a.DateModified
              FROM
              $card_student_daily_class_confirm as a
              LEFT OUTER JOIN INTRANET_CLASS as b ON (a.ClassID=b.ClassID)
              LEFT OUTER JOIN INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID)
              WHERE b.ClassName = '".$luser->Get_Safe_Sql_Query($class_name)."' AND DayNumber = ".date("d")." AND DayType = '$DayPeriod'";
        $result2 = $luser->returnArray($sql,3);
        list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result2[0];
        if($confirmed_id==""){
                $update_str = $i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record;
        }
        else{
                $update_str .= $i_LastModified .": ".$last_confirmed_date." (".$confirmed_user_name.")";
        }


        $ldb = new libdb();
        ### check if Preset Absence exists
        $sql="SELECT COUNT(*) FROM INTRANET_USER AS A INNER JOIN CARD_STUDENT_PRESET_LEAVE AS B ON(A.USERID = B.STUDENTID) WHERE A.RECORDTYPE=2 AND A.RECORDSTATUS IN(0,1,2) AND A.CLASSNAME='".$ldb->Get_Safe_Sql_Query($class_name)."' AND B.RECORDDATE=CURDATE() AND B.DayPeriod='$DayPeriod'";
        $temp = $ldb->returnVector($sql);
        $preset_count = $temp[0];

        ### build student attend record table
        $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".date("Y")."_".date("m");

        $conds = "";
        $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";

        if($class_name<>"" && $period<>"")
                $conds .= "AND a.ClassName = '".$ldb->Get_Safe_Sql_Query($class_name)."' ";
        $conds .= "ORDER BY a.ClassNumber ASC ";

        if($period=="AM")
        {
                if(!$sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){ # no need to show photo
                        $username_field = getNameFieldByLang("a.");

						$sql  = "SELECT        b.RecordID,
                                       a.UserID,
                                       ".getNameFieldByLang("a.")."as name,
                                       a.ClassNumber,
                                       IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
                                       IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                                       IF(b.AMStatus IS NOT NULL,
                                          b.AMStatus,
                                          CASE
                                              WHEN b.InSchoolTime IS NOT NULL THEN '0'
                                              WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '".$student_attend_default_status."'
                                              WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '3'
                                              END
                                              ),
										IF(d.Remark IS NULL, IF( (b.AMStatus IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL) OR b.AMStatus=1,f.Reason,d.Remark),d.Remark),
                                        IF(f.RecordID IS NOT NULL,1,0),
                                        f.Reason,
                                        f.Remark,
                                        IF(c.OutingID IS NOT NULL,1,0),
                                         c.RecordDate,
                                         c.OutTime,
                                         c.BackTime,
                                         c.Location,
                                         c.Objective,
                                         c.Detail, 
                                         g.Reason, 
                                        $username_field 
		                        FROM
		                            	INTRANET_USER AS a
		                            	LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = '".date("d")."')
		                            	LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
		                            	LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
		                            	LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod='$DayPeriod')
		                            	LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
		                            	ON g.StudentID = a.UserID AND g.RecordDate = CURDATE() 
		                            		 AND g.DayType = '$DayPeriod' 
		                        WHERE $conds
                				";
                }
                else{ # need to show photo
                        $sql = "SELECT a.UserID,a.WebSAMSRegNo,a.PhotoLink  FROM INTRANET_USER AS a WHERE a.ClassName='".$ldb->Get_Safe_Sql_Query($class_name)."' AND a.RecordType=2 AND a.RecordStatus IN(0,1)";

                        $temp = $ldb->returnArray($sql,3);

                        $link_ary=array();
                        for($i=0;$i<sizeof($temp);$i++){
                                list($sid,$regno,$p_link)=$temp[$i];
                                $photo_url="";
                                if($regno!=""){ # use official photo
                                        $tmp_regno = trim(str_replace("#", "", $regno));
                                        $photolink = "/file/official_photo/".$tmp_regno.".jpg";
                                        if(file_exists($intranet_root.$photolink))
                                                        $photo_url = $photolink;
                                }
                                if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
                                        $photo_url = $p_link;
                                }
                                if($photo_url!="" && file_exists($intranet_root.$photo_url))
                                        $link_ary[$sid]=$photo_url;
                                else $link_ary[$sid]="";

                        }

                        $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
                        if(sizeof($link_ary)>0){
                                foreach($link_ary AS $student_id => $link){
                                        $sql_insert_temp.="($student_id,'$link'),";
                                }
                                $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
                        }

                        $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
                                                                        StudentID INT(8),Link VARCHAR(255) )
                                                                ";
                        $ldb->db_db_query($create_temp_table);
                        $ldb->db_db_query($sql_insert_temp);
//echo "sql_insert_temp [".$sql_insert_temp."]<br>";
                        $username_field = getNameFieldByLang("a.");

                                $sql  = "SELECT        b.RecordID,
                                       a.UserID,
                                       IF((e.Link IS NULL OR e.Link=''),$username_field,
                                        CONCAT('<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img src=', e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name,
                                       a.ClassNumber,
                                       IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
                                       IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                                       IF(b.AMStatus IS NOT NULL,
                                          b.AMStatus,
                                          CASE
                                              WHEN b.InSchoolTime IS NOT NULL THEN '0'
                                              WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '".$student_attend_default_status."'
                                              WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '3'
                                              END
                                              ),
                                         IF(d.Remark IS NULL, IF( (b.AMStatus IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL) OR b.AMStatus=1,f.Reason,d.Remark),d.Remark),
                                         IF(f.RecordID IS NOT NULL,1,0),
                                         f.Reason,
                                         f.Remark,
                                         IF(c.OutingID IS NOT NULL,1,0),
                                         c.RecordDate,
                                         c.OutTime,
                                         c.BackTime,
                                         c.Location,
                                         c.Objective,
                                         c.Detail,
                                         g.Reason, 
                                         $username_field
                        FROM
                            INTRANET_USER AS a
                                LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = '".date("d")."')
                                LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
                                LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
                                LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)
                                LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod='$DayPeriod') 
                                LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
	                            	ON g.StudentID = a.UserID AND g.RecordDate = CURDATE() 
	                            		 AND g.DayType = '$DayPeriod' 
                        WHERE $conds
                ";
                }
        }
        else
        {
                        $pm_status_field =   "   CASE
                        			  			WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LeaveStatus IN('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND b.PMStatus IS NULL AND b.LeaveSchoolTime IS NOT NULL AND b.LeaveSchoolStation IS NOT NULL) THEN '".CARD_STATUS_ABSENT."'
                                      WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (b.LunchOutTime IS NULL OR b.LunchBackTime IS NOT NULL))
                                           THEN '".CARD_STATUS_PRESENT."'
                                      WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LunchOutTime IS NOT NULL) AND (b.LunchBackTime IS NULL)
                                           THEN '".CARD_STATUS_ABSENT."'
                                      WHEN c.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."' 
                                      WHEN b.AMStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."'
                                      ELSE '".$student_attend_default_status."'
                                  END
                                                        ";
                        $username_field = getNameFieldByLang("a.");

		        if(!$sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){ # no need to show photo
		        $sql  = "SELECT        b.RecordID,
		                               a.UserID,
		                               ".getNameFieldByLang("a.")."as name,
		                               a.ClassNumber,
		                               IF(b.LunchBackTime IS NULL,
		                                  '-',
		                                  b.LunchBackTime),
		                               IF(b.LunchBackStation IS NULL,
		                                  '-',
		                                  b.LunchBackStation),
		                               IF(b.PMStatus IS NOT NULL,
		                                  b.PMStatus,$pm_status_field ),
		                               IF(d.Remark IS NULL,IF(b.PMStatus='".CARD_STATUS_ABSENT."' OR (b.PMStatus IS NULL AND $pm_status_field='".CARD_STATUS_ABSENT."'),f.Reason,d.Remark),d.Remark),
		                               IF(f.RecordID IS NOT NULL,1,0),
		                               f.Reason,
		                               f.Remark,
		                               IF(c.OutingID IS NOT NULL,1,0),
                                         c.RecordDate,
                                         c.OutTime,
                                         c.BackTime,
                                         c.Location,
                                         c.Objective,
                                         c.Detail,
                                         g.Reason, 
		                               $username_field
		                               FROM
		                                   INTRANET_USER AS a
		                                      LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = ".date("d").")
		                                      LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
		                                      LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
		                                      LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=3) 
						                            	LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
						                            	ON g.StudentID = a.UserID AND g.RecordDate = CURDATE() 
						                            		 AND g.DayType = '$DayPeriod' 
		                               WHERE
		                                    $conds";
		        }
		        else{ # need to show photo
		
		                $sql = "SELECT a.UserID,a.WebSAMSRegNo,a.PhotoLink  FROM INTRANET_USER AS a WHERE a.ClassName='".$ldb->Get_Safe_Sql_Query($class_name)."' AND a.RecordType=2 AND a.RecordStatus IN(0,1)";
		
		                $temp = $ldb->returnArray($sql,3);
		
		                $link_ary=array();
		                for($i=0;$i<sizeof($temp);$i++){
		                        list($sid,$regno,$p_link)=$temp[$i];
		                        $photo_url="";
		                        if($regno!=""){ # use official photo
		                                $tmp_regno = trim(str_replace("#", "", $regno));
		                                $photolink = "/file/official_photo/".$tmp_regno.".jpg";
		                                if(file_exists($intranet_root.$photolink))
		                                                $photo_url = $photolink;
		                        }
		                        if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
		                                $photo_url = $p_link;
		                        }
		                        if($photo_url!="" && file_exists($intranet_root.$photo_url))
		                                $link_ary[$sid]=$photo_url;
		                        else $link_ary[$sid]="";
		
		                }
		
		                $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
		                if(sizeof($link_ary)>0){
		                        foreach($link_ary AS $student_id => $link){
		                                $sql_insert_temp.="($student_id,'$link'),";
		                        }
		                        $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
		                }
		
		                $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
		                                                                StudentID INT(8),Link VARCHAR(255) )
		                                                        ";
		                $ldb->db_db_query($create_temp_table);
		                $ldb->db_db_query($sql_insert_temp);
		                $username_field = getNameFieldByLang("a.");
		
		        $sql  = "SELECT        b.RecordID,
		                               a.UserID,
		                               IF((e.Link IS NULL OR e.Link=''),$username_field,
		                                   CONCAT('<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img src=', e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name,
		                               a.ClassNumber,
		                               IF(b.LunchBackTime IS NULL,
		                                  '-',
		                                  b.LunchBackTime),
		                               IF(b.LunchBackStation IS NULL,
		                                  '-',
		                                  b.LunchBackStation),
		                               IF(b.PMStatus IS NOT NULL,
		                                  b.PMStatus, $pm_status_field),
		                               IF(d.Remark IS NULL,IF(b.PMStatus='".CARD_STATUS_ABSENT."' OR (b.PMStatus IS NULL AND $pm_status_field='".CARD_STATUS_ABSENT."'),f.Reason,d.Remark),d.Remark),
		                               IF(f.RecordID IS NOT NULL,1,0),
		                               f.Reason,
		                               f.Remark,
		                               IF(c.OutingID IS NOT NULL,1,0),
                                         c.RecordDate,
                                         c.OutTime,
                                         c.BackTime,
                                         c.Location,
                                         c.Objective,
                                         c.Detail,
                                         g.Reason, 
		                               $username_field
		                               FROM
		                                   INTRANET_USER AS a
                                      LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = ".date("d").")
                                      LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
                                      LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
                                      LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)
                                      LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=3) 
                                      LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as g 
				                            	ON g.StudentID = a.UserID AND g.RecordDate = CURDATE() 
				                            		 AND g.DayType = '$DayPeriod' 
		                               WHERE
		                                    $conds";
		
		
		        }
        }
        //echo "sql [".$sql."]<br>";
        $result = $luser->returnArray($sql,20);

        if($field=="") $field = 0;
        if($order=="") $order = 1;
        $li = new libdbtable2007($field, $order, $pageNo);

        // TABLE COLUMN
        $pos = 0;
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>". $i_UserClassNumber ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='20%'>". $i_UserName ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='18%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Time ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='16%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Station ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='9%'>". $i_SmartCard_Frontend_Take_Attendance_PreStatus ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='2%'>&nbsp;</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='15%'>". $i_SmartCard_Frontend_Take_Attendance_Status ."</td>\n";
        $li->column_list .= "<td class='tablegreentop tabletoplink' width='22%'>". $i_Attendance_Reason ."</td>\n";
        if (!$isStudentHelper) {
        	$li->column_list .= "<td class='tablegreentop tabletoplink' width='22%'>". $i_Attendance_Remark ."</td>\n";
        }

        $li->IsColOff = "SmartCardTakeAttendance";
        $li->title = "";
}

### Button / Tag
$ViewPastRecord = '<a href="#" onclick="window.open(\'attendance_ref.php?ClassName='.urlencode($class_name).'\', \'attendRefWin\');" class="tabletool">';
$ViewPastRecord .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view_s.gif" width="12" height="12" border="0" align="absmiddle" />'.$i_StudentAttendance_ViewPastRecords.'</a>';
$DisplayPhotoBtn = "<a href='javascript:toggle_photo();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> <span id=\"DisplayAllPhotoTxt\">" . $DisplayAllPhoto . "<span></a>";
$DisplayPhotoBtn = $sys_custom['SmartCardAttendance_Teacher_ShowPhoto'] ? $DisplayPhotoBtn : "";
$setAtoP        = "<a href='javascript:AbsToPre();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $i_StudentAttendance_Action_SetAbsentToPresent . "</a>";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>". $i_StudentAttendance_Frontend_menu_TakeAttendance ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface->LAYOUT_START();

echo "<script language=\"Javascript\" src='/templates/tooltip.js'></script>";
echo "<style type=\"text/css\">\n";
echo "#ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}";
echo "</style>";
echo "<div id=\"ToolTip2\"></div>";
echo "<div id=\"ToolTip3\" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>";
?>

<script language="javascript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                //jChangeContent( "ToolMenu2", o.responseText );
                //writeToLayer('ToolMenu2',o.responseText);
                //showMenu2("img2_"+imgObj,"ToolMenu2");
                //alert(o.responseText);
                var obj = document.getElementById('period');
                if(o.responseText == 'AM'){
								<? if($lcardattend->attendance_mode !=1) {?>
	               	obj.options[0].selected = true;
	                obj.options[1].selected = false;
	                <? } ?>
	                
                }
                if(o.responseText == 'PM'){
	              <? if($lcardattend->attendance_mode !=0) {?>
	                obj.options[0].selected = false;
	                obj.options[1].selected = true;
	                <? } ?>
                }
        }
}
// start AJAX
function retrivePMTime(mode)
{
		obj = document.form1;
		if(mode == 1){
			var name = obj.class_name.value;
		}
		if(mode == 2){
			var GroupID = obj.group_id.value;
		}
				
		//var myElement = document.getElementById("ToolMenu2");
		//writeToLayer('ToolMenu2','<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>');
        //imgObj = i;
        //showMenu2("img2_"+i,"ToolMenu2");
        
        YAHOO.util.Connect.setForm(obj);
        if(mode == 1){
        	var path = "retrive_pm_time.php?ClassName=" + name;
    	}
        if(mode == 2){
        	var path = "retrive_pm_time.php?groupIDString=" + GroupID;
    	}
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
function showMenu2(objName,menuName)
{
                  hideMenu2('ToolMenu');
                  hideMenu2('ToolMenu2');
           objIMG = document.getElementById(objName);

                        offsetX = (objIMG==null)?0:objIMG.width;
                        offsetY =0;
            var pos_left = getPostion(objIMG,"offsetLeft");
                        var pos_top  = getPostion(objIMG,"offsetTop");
                        objDiv = document.getElementById(menuName);

                        if(objDiv!=null){
                                objDiv.style.visibility='visible';
                                objDiv.style.top = pos_top+offsetY;
                                objDiv.style.left = pos_left+offsetX;
                                setDivVisible(true, menuName, "lyrShim");
                        }
}
-->
</script>

<script language="JavaScript">
isToolTip = false;
function setToolTip(value)
{
        isToolTip=value;
}
function toggle_photo()
{

        with(document.form1)
        {
                <? for($i=0; $i<sizeof($result); $i++) {
                                        ?>
                if(document.getElementById("photo<?=$result[$i][1]?>a") != null)
                        if(photo<?=$result[$i][1]?>a.style.display=="inline")
                        {
                                photo<?=$result[$i][1]?>a.style.display = "none"
                                photo<?=$result[$i][1]?>b.style.display = "inline"
                                DisplayAllPhotoTxt.innerHTML = "<?=$HiddenAllPhoto?>";
                        }
                        else
                        {
                                photo<?=$result[$i][1]?>a.style.display = "inline"
                                photo<?=$result[$i][1]?>b.style.display = "none"
                                DisplayAllPhotoTxt.innerHTML = "<?=$DisplayAllPhoto?>";
                        }
                <? } ?>
        }
}
</script>

<style type="text/css">
        #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
    isMenu =false;
</script>

<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>
<div id="ToolMenu" style="position:absolute; width:200; height:100; visibility:hidden"></div>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	  <td align="center">
	  	<table width="96%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	    	<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['StudentAttendance']['TakeAttendanceFor']?></span></td>
					<td valign="top" class="tabletext">
						<input type="radio" name="AttendanceFor" value="School" checked="checked" onclick="Get_School_Layer(this.checked);">
						<?=$Lang['StudentAttendance']['School']?> 
						<input type="radio" name="AttendanceFor" value="Lesson" onclick="Get_Lesson_Layer(this.checked);">
						<?=$Lang['StudentAttendance']['Lesson']?>  
					</td>
				</tr>
				</table>
				</td>
			</tr>
	    <tr>
	    	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	    </tr>
	    <tr>
	      <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
	      <td colspan="2">
	    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	      <tr>
          <td align="center">
          </td>
        </tr>
	      </table>
	      </td>
	   	</tr>
	    <tr>
        <td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
        <?
        //DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
        if($msg == 99)
        {
                echo "<br><font color =\"red\">".$i_StudentAttendance_Warning_Data_Outdated."</font><br><br>";
        }
        ?>

        </td>
	    </tr>
	  	</table>
	  </td>
	</tr>
</table>
<div id="SchoolAttendanceLayer">
	<form name="form1" method="get">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	        <td align="center">
	                <table width="96%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                        <td align="left" class="tabletext">
	
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<?
						if($class_name != "")
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_ClassName?> </span></td>
								<td valign="top" class="tabletext"><?=intranet_htmlspecialchars($class_name)?><input type="hidden" name="class_name" value="<?=escape_double_quotes($class_name)?>"></td>
							</tr>
	<?
						}
						elseif($group_id != "")
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_GroupName?> </span></td>
								<td valign="top" class="tabletext"><?=intranet_htmlspecialchars($groupName)?><input type="hidden" name="group_id" value="<?=escape_double_quotes($group_id)?>"></td>
							</tr>
	<?
						}
						else
						{
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_StudentAttendance_By?></span></td>
								<td>
									<input type="radio" name="groupType" value="1"<?=($groupType==1)?" checked":""?> id="groupTypeClass" onClick="toggle(this.value);"><label for="groupTypeClass"><?=$i_SmartCard_ClassName?></label>&nbsp;
									<input type="radio" name="groupType" value="2"<?=($groupType==2)?" checked":""?> id="groupTypeGroup" onClick="toggle(this.value);"><label for="groupTypeGroup"><?=$i_SmartCard_GroupName?></label>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext">&nbsp;</span></td>
								<td><?=$class_select?><?=$group_select?></td>
							</tr>
	<?
						}
	?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_BookingTimeSlots?> </span></td>							
								<td valign="top" class="tabletext"><?=($class_name and $period) ? $slot_select ."<input type=\"hidden\" name=\"period\" value=\"".escape_double_quotes($period)."\">" : $slot_select?></td>
							</tr>
							</table>
	
	                        </td>
	                </tr>
	                <tr>
	                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	                </tr>
	                <tr>
	                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
	                        <td colspan="2">
	                                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	                                <tr>
	                                        <td align="center">
	                                        <? if($class_name && $period) {?>
	                                                <?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                                <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='take_attendance_lesson_demo.php'","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        <? } else {?>
	                                                <?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        <? } ?>
	                                        </td>
	                                </tr>
	                                </table>
	                        </td>
	                </tr>
	
	                <tr>
	                        <td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
	                        <?
	                        //DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
	                        if($msg == 99)
	                        {
	                                echo "<br><font color =\"red\">".$i_StudentAttendance_Warning_Data_Outdated."</font><br><br>";
	                        }
	                        ?>
	
	                        </td>
	                </tr>
	                <? if($class_name && $period) {?>
	                <tr>
	                        <td align="left" colspan="2">
	
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                        <tr>
	                                                <td><span class="tabletext"><?=$update_str?></span></td>
	                                                <td align="right" valign="bottom">
	                                                        <table border="0" cellspacing="0" cellpadding="0">
	                                                        <tr>
	                                                                <td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_01.gif" width="21" height="23" /></td>
	                                                                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_02.gif">
	                                                                        <table border="0" cellspacing="0" cellpadding="2">
	                                                                        <tr>
	                                                                        				<td nowrap><?=$ViewPastRecord?></td>
	                                                                                <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
	                                                                                <td nowrap><?=$DisplayPhotoBtn?></td>
	                                                                                <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
	                                                                                <td nowrap><?=$setAtoP?></td>
	                                                                        </tr>
	                                                                        </table>
	                                                                </td>
	                                                                <td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_03.gif" width="6" height="23" /></td>
	                                                        </tr>
	                                                        </table>
	                                                </td>
	                                        </tr>
	                                </table>
	                        </td>
	                </tr>
	
	                <tr>
	                        <td align="center">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                <tr>
	                                        <td colspan="2"><?=$li->display($display_period)?></td>
	                                </tr>
	                                </table>
	                        </td>
	                </tr>
	
	                <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
	                                <tr>
	                                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                                </tr>
	                                <tr>
	                                        <td align="center">
	                                                <?= $linterface->GET_ACTION_BTN($button_submit, "button", "AlertPost(document.form1,'take_update.php','$i_SmartCard_Confirm_Update_Attend?')","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "resetForm(document.form1);","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	                                        </td>
	                                </tr>
	                        </table>
	                </td>
	                </tr>
	                <? } ?>
	                </table>
	        </td>
	</tr>
	</table>
	<br />
	<input name="confirmed_user_id" type="hidden" value="<?=escape_double_quotes($UserID)?>">
	<input name="confirmed_id" type="hidden" value="<?=escape_double_quotes($confirmed_id)?>">
	<input name="PageLoadTime" type="hidden" value="<?=escape_double_quotes($page_load_time)?>">
	<? for ($i=0; $i<sizeof($editAllowed); $i++) { ?>
	<input type="hidden" name="editAllowed<?=$i?>" value="<?=escape_double_quotes($editAllowed[$i])?>">
	<? } ?>
	</form>
</div>

<!-- lesson attendance layer-->
<div id="LessonAttendanceLayer" style="display:none;">
	<form name="form2" method="get">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
	<tr>
	        <td align="center">
	                <table width="96%" border="0" cellspacing="0" cellpadding="0">
	                <tr>
	                        <td align="left" class="tabletext">
	
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$Lang['StudentAttendance']['Session']?> </span></td>
								<td valign="top" class="tabletext">
									<select name="SessionSelect">
										<option value="1">Chinese Language 08:30 - 09:30</option>
										<option value="2">Physics 10:30 - 11:30</option>
									</select>
								</td>
							</tr>
							</table>
	
	                        </td>
	                </tr>
	                <tr>
	                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
	                </tr>
	                <tr>
	                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
	                <tr>
                    <td colspan="2">
                      <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                      <tr>
                        <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_view, "button", "View_Session();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        </td>
                      </tr>
                      </table>
                    </td>
	                </tr>
	                </table>
	        </td>
	</tr>
	</table>
	<br />
	</form>
</div>

<SCRIPT LANGUAGE="Javascript">
function view_class()
{
<? if($class_name) {?>
	window.location="?class_name=" + document.getElementById('class_name').value + "&period="+document.getElementById('period')[document.getElementById('period').selectedIndex].value;
<? } else {?>
	var PeriodValue = document.getElementById('period')[document.getElementById('period').selectedIndex].value;
	if(document.form1.groupType[0].checked == true) {
		var ClassValue = document.getElementById('class_name')[document.getElementById('class_name').selectedIndex].value;
		window.location="?class_name=" + ClassValue + "&period="+PeriodValue;
	}
	else {
		var GroupValue = document.getElementById('group_id')[document.getElementById('group_id').selectedIndex].value;
		window.location="take_attendance_group.php?group_id=" + GroupValue + "&period="+PeriodValue;
	}
<?}?>
		
	
        /*with(document.form1)
        {
                <? if($class_name) {?>
                window.location="?class_name=" + class_name.value + "&period="+period.value;
                <? } else {?>
                        if(document.form1.groupType[0].checked == true)
                        {
							
							if (typeof(document.form1.period[0])!="undefined")
							{
								if(document.form1.period[0].checked == true) {
									window.location="?class_name=" + class_name.value + "&period="+period[0].value;
								}
								if(document.form1.period[1].checked == true) {
									window.location="?class_name=" + class_name.value + "&period="+period[1].value;
								}
							} else
							{
								window.location="?class_name=" + class_name.value + "&period="+period.value;
							}
                        }else if(document.form1.groupType[1].checked == true)
                        {
							if (typeof(document.form1.period[0])!="undefined")
							{
								if(document.form1.period[0].checked == true) {
									window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period[0].value;
								}
								if(document.form1.period[1].checked == true) {
									window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period[1].value;
								}
							} else
							{
								window.location="take_attendance_group.php?group_id=" + group_id.value + "&period="+period.value;
							}
                        }
                        <? } ?>

        }*/
}

var reasonSelection = Array();
<?=$reason_js_array?>
        var posx = 0;
        var posy = 0;
function calMousePos(e) {

        if (!e) var e = window.event;
        if (e.pageX || e.pageY)         {
                posx = e.pageX;
                posy = e.pageY;
        }
        else if (e.clientX || e.clientY)         {
                posx = e.clientX;
                posy = e.clientY;
        }
}

function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
        setEditAllowed();
}

function setMenu(value){
        isMenu=value;
}

function showSelection(i, allowed)
{
         if (allowed == 1)
         {
                 setMenu(true);
                 setToolTip(false);
                 writeToLayer('ToolMenu', reasonSelection[i]);
                 moveToolTip('ToolMenu',posy,posx );
                 showLayer('ToolMenu');
         }
}
/*
function showSelection2(pos)
{
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 jArrayTemp[0] = "abc";
                 jArrayTemp[1] = "def";
             }
             else if (obj2.selectedIndex == 2)
             {
                 jArrayTemp[0] = "123";
                 jArrayTemp[1] = "456";
             }

}
*/

<?
# Grab reasons
?>
function getReasons(pos)
{
         var jArrayTemp = new Array();
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 return jArrayWordsAbsence;
             }
             else if (obj2.selectedIndex == 2)
             {
                  return jArrayWordsLate;
             }
             else return jArrayTemp;
}

function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function clearReason(obj){
        if(obj!=null)
                obj.value="";
}

function setEditAllowed(){
	<? if(!$isStudentHelper){ ?>
        drop_down_list = document.getElementsByName('drop_down_status[]');
        if(drop_down_list==null)return;
        for(i=0;i<drop_down_list.length;i++){
                editAllowedObj = eval('document.form1.editAllowed'+i);
                statusObj = drop_down_list[i];
                if (eval('document.form1.reason'+i)) {
                        reasonObj = eval('document.form1.reason'+i);
                        reasoniconObj = eval('document.form1.posimg'+i);
                      }
                if(editAllowedObj==null || statusObj==null) return;

                if(statusObj.selectedIndex==1||statusObj.selectedIndex==2){
                        editAllowedObj.value=1;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.disabled=false;
                                //reasoniconObj.style.display="inline";
                        }
                       if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="inline";
                            }
                            reasonObj.style.background='#FFFFFF';

                }else{
                        editAllowedObj.value=0;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.value="";
                                reasonObj.disabled = true;
                                //reasoniconObj.style.display="none";
                        }
                        if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="none";
                            }
                            reasonObj.style.background='#DFDFDF';

                }
                //if(editAllowedObj.value==0)
                // All status change hide menu
                // hideMenu('ToolMenu');

                if(typeof(listContent) != 'undefined')
                {
                	obj2 = document.getElementById('listContent').style.display='none';
                	setDivVisible(false, 'listContent', 'lyrShim');
            	}
        }
	<? } ?>
}

function setEditAllowed_single(i){
	<? if(!$isStudentHelper){ ?>
        drop_down_list = document.getElementsByName('drop_down_status[]');
        if(drop_down_list==null)return;

                editAllowedObj = eval('document.form1.editAllowed'+i);
                statusObj = drop_down_list[i];
                if (eval('document.form1.reason'+i)) {
                        reasonObj = eval('document.form1.reason'+i);
                        reasoniconObj = eval('document.form1.posimg'+i);
                      }
                if(editAllowedObj==null || statusObj==null) return;

                if(statusObj.selectedIndex==1||statusObj.selectedIndex==2){
                        editAllowedObj.value=1;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.disabled=false;
                                //reasoniconObj.style.display="inline";
                        }
                       if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="inline";
                            }
                            reasonObj.style.background='#FFFFFF';

                }else{
                        editAllowedObj.value=0;
                        if(typeof(reasonObj) != 'undefined'){
                                reasonObj.value="";
                                reasonObj.disabled = true;
                                //reasoniconObj.style.display="none";
                        }
                        if(typeof(reasoniconObj) != 'undefined'){
                                         reasoniconObj.style.display="none";
                            }
                            reasonObj.style.background='#DFDFDF';
                }
                //if(editAllowedObj.value==0)
                // All status change hide menu
                // hideMenu('ToolMenu');
                if(typeof(listContent) != 'undefined')
                {
                	obj2 = document.getElementById('listContent').style.display='none';
                	setDivVisible(false, 'listContent', 'lyrShim');
            	}
	<?}?>

}

function resetForm(formObj){
        formObj.reset();
        setEditAllowed();

}

// for Preset Absence
function showPresetAbs(obj,reason){
                        var pos_left = getPostion(obj,"offsetLeft");
                        var pos_top  = getPostion(obj,"offsetTop");

                        offsetX = (obj==null)?0:obj.width;
                        offsetY =0;
                        objDiv = document.getElementById('ToolTip3');
                        if(objDiv!=null){
                                objDiv.innerHTML = reason;
                                objDiv.style.visibility='visible';
                                objDiv.style.top = pos_top+offsetY;
                                objDiv.style.left = pos_left+offsetX;
                                setDivVisible(true, "ToolTip3", "lyrShim");
                        }

}

// for preset absence
function hidePresetAbs(){
                obj = document.getElementById('ToolTip3');
                if(obj!=null)
                        obj.style.visibility='hidden';
                setDivVisible(false, "ToolTip3", "lyrShim");
}

function toggle(x)
{
	if(x==1) {
		document.getElementById('class_name').style.display="inline";
		document.getElementById('group_id').style.display="none";
	} else {
		document.getElementById('class_name').style.display="none";
		document.getElementById('group_id').style.display="inline";
	}
}

function changeSlot(val, class_name)
{
	/*if(val != ""){
		self.location = "take_attendance.php?class_name="+class_name+"&period="+val;
	}*/
}

// lesson attendance
{
function Get_School_Layer() {
	document.getElementById('LessonAttendanceLayer').style.display = "none";
	document.getElementById('SchoolAttendanceLayer').style.display = "";
}

function Get_Lesson_Layer() {
	document.getElementById('LessonAttendanceLayer').style.display = "";
	document.getElementById('SchoolAttendanceLayer').style.display = "none";
}

function View_Session() {
	var SessionSelected = document.getElementById('SessionSelect').options[document.getElementById('SessionSelect').selectedIndex].value;
	var FormObj = document.form2;
	if (SessionSelected == "1") 
		FormObj.action = "<?=$PATH_WRT_ROOT?>test/attendance_demo/index.php";
	else 
		FormObj.action = "<?=$PATH_WRT_ROOT?>test/attendance_demo/index1.php";
		
	FormObj.submit();
}
}
</SCRIPT>

<script>
setEditAllowed();
<? if ($class_name == "") { ?>
toggle(<?=IntegerSafe($groupType)?>);
<? } ?>
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>