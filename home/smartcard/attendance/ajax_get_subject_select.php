<?php
// Editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$scm_ui = new subject_class_mapping_ui();
$fcm = new form_class_manage();

if(!isset($TargetDate) || $TargetDate == ''){
	$TargetDate = date("Y-m-d");
}

$lcardattend = new libcardstudentattend2();
$YearTermInfo = $fcm->Get_Academic_Year_And_Year_Term_By_Date($TargetDate);
$YearTermID_ = $YearTermInfo[0]['YearTermID'];
if($lcardattend->ClassTeacherTakeOwnClassOnly)
{
	$scm = new subject_class_mapping();
	$TeachingOnly_ = 1;
	if ($YearTermID == '')
	{
		$YearTermArr = getCurrentAcademicYearAndYearTerm();
		$YearTermID = $YearTermArr['YearTermID'];
	}
	$tmp_subject_group_list = $scm->Get_Subject_Group_List($YearTermID, $SubjectID, $returnAssociate=0, $TeachingOnly_);
	$IncludeSubjectIDArr_ = array_values(array_unique( Get_Array_By_Key( $tmp_subject_group_list, 'SubjectID') ));
}else{
	$TeachingOnly_ = 0;
	$IncludeSubjectIDArr_ = '';
}

$SubjectSelect = $scm_ui->Get_Subject_Selection("SubjectID", $SubjectID, 'subjectChanged(this.options[this.selectedIndex].value);', $noFirst_='0', $firstTitle_=$button_select, $YearTermID_, $OnFocus_='', $FilterSubjectWithoutSG_=1, $IsMultiple_=0, $IncludeSubjectIDArr_);

echo $SubjectSelect;

intranet_closedb();
?>