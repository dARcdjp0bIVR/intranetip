<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_opendb();

if(!$plugin['attendancestudent'])
{
	header("Location: /");
	intranet_closedb();
    exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$li = new libdb();
$Reason = intranet_htmlspecialchars($Reason);

$lclass = new libclass();
$student_list = $lclass->getClassStudentList($ClassName);
//echo sizeof($student_list);
//echo $StudentID;
$StudentID = IntegerSafe($StudentID);

if($StudentID==-1)
{
	for($i=0;$i<sizeof($student_list);$i++)
        {
                $sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
                VALUES ('".$li->Get_Safe_Sql_Query($student_list[$i])."','".$li->Get_Safe_Sql_Query($RemindDate)."','".$li->Get_Safe_Sql_Query($UserID)."','".$li->Get_Safe_Sql_Query($Reason)."',now(),now())";
        	
                $li->db_db_query($sql);
        }
}
else
{
	$sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified)
        VALUES ('".$li->Get_Safe_Sql_Query($StudentID)."','".$li->Get_Safe_Sql_Query($RemindDate)."','".$li->Get_Safe_Sql_Query($UserID)."','".$li->Get_Safe_Sql_Query($Reason)."',now(),now())";
	
        $li->db_db_query($sql);
}

header ("Location: reminder.php?xmsg=add");
intranet_closedb();
?>
