<?php
// using by 
##################################### Change Log ###############################################
# 2019-11-08 by Ray: Add $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']
# 2019-09-24 by Carlos: Fixed PM only mode display wrong in time and in station fields for PM session.
# 2019-06-19 by Ray   : Modified teacher_remark_js read from database
# 2019-06-13 by Carlos: Added primary key to temporary table TEMP_STUDENT_PHOTO_LINK to improve query performance.
# 2019-03-27 by Carlos: [ip.2.5.10.4.1] Toggle display of mouse over tooltip image with showHideTooltipPhoto(mouseOverObj,isShow) which use jquery position api.
# 2018-08-03 by Carlos: [ip.2.5.9.10.1] Apply setting [iSmartCardDisableModifyStatusAfterConfirmed] to disable status change for confirmed attendance records.
# 2018-06-22 by Carlos: Display group name to follow UI language.
# 2018-03-12 by Carlos: Check setting TeacherCanManageProveDocumentStatus to show/hide [Submitted Prove Document] option.
# 2018-01-30 by Carlos: Fine tune group admin checking, if a group hasn't set any admin users, default staff can take it, student can only take is group admin groups.
# 2017-12-15 by Carlos: [ip.2.5.9.1.1] Fixed PM to follow AM Teacher's remark. 
# 2017-11-22 by Carlos: [ip.2.5.9.1.1] added checking to only allow group admin to take attendance. 
# 2017-05-26 by Carlos: [ip.2.5.8.7.1] $sys_custom['StudentAttendance']['RecordBodyTemperature'] display body temperature.
# 2017-05-17 by Carlos: [ip.2.5.8.7.1] $sys_custom['StudentAttendance']['NoCardPhoto'] added no bring card photo.
# 2017-04-12 by Carlos: [ip.2.5.8.4.1] Allow manage [Waived], [Reason] and [Teacher's remark] according to settings, added [Submitted Prove Document], added teacher's remark selection.
# 2016-10-17 by Carlos: [ip2.5.7.10.1] Improved to pre-display outing remark if have preset outing record.
# 2016-07-05 by Carlos: [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - allow edit in time.
# 2016-03-23 by Carlos: [ip2.5.7.4.1] Added change all status tool button.
# 2016-03-11 by Carlos: [ip2.5.7.4.1] $sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] - customized to make students that do not go out for lunch 
#										to set PM status to follow AM status because these students do not tap card in lunch time to trigger the Pm in status.
# 2015-11-06 by Carlos: [ip2.5.7.1.1] Fixed Outing and Early Leave duplicated records bug.
# 2015-10-22 by Carlos: Display Last Modified info under time slot selection.
# 2015-10-14 by Carlos: Cater official photo to be consistent with teacher app. 
# 2015-07-14 by Carlos: Disable all editable fields if disallow to edit n days before records. 
# 2015-03-31 by Carlos: Display preset absence remark to Office remark and do not allow edit, not to Teachers's remark. 
# 2015-02-13 by Carlos: Added [Office Remark].
# 2014-03-17 by Carlos: Added Absent Session for $sys_custom['SmartCardAttendance_StudentAbsentSession2']
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2013-02-07 by Carlos: If preset absent record's remark is set, display as teacher's remark
# 2012-06-07 by Carlos: Join CARD_STUDENT_ATTENDANCE_GROUP instead of CARD_STUDENT_CLASS_SPECIFIC_MODE
# 2012-03-16 by Carlos: PM display PMDateModified and PMModifyBy
# 2011-11-16 by Carlos: Get LeaveStatus and LeaveSchoolTime from sql
# 2010-04-23 by Carlos: Allow take attendance of past dates
# 2010-04-22 by Carlos: Allow input late session if student is late
# 2010-03-17 by Kenneth Chung: add condition of EnableEntryLeavePeriod setting
# 2010-03-08 by Kenneth Chung: Move Get Default PM Status Logic to libcardstudentattend2.php
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();

$lcardattend = new libcardstudentattend2();

if(method_exists($lcardattend,'getAttendanceGroupRecords')){
	$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
	$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$_SESSION['UserID']));
	$responsible_user_group_ids = Get_Array_By_Key($responsible_user_groups,'GroupID');
	
	if($_SESSION['UserType'] == USERTYPE_STAFF){
		// get all group admins with group id as key to admin users
		$groupIdToAdminUsers = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'GroupIDToUsers'=>1));
		// go through all groups to see any admin users added to the group
		for($i=0;$i<count($attendance_groups);$i++){
			if(!isset($groupIdToAdminUsers[$attendance_groups[$i]['GroupID']]) && !in_array($attendance_groups[$i]['GroupID'],$responsible_user_group_ids)){
				// if the group hasn't set any admin, staff can take it
				$responsible_user_group_ids[] = $attendance_groups[$i]['GroupID'];
			}
		}
	}
	
	$responsible_user_group_id_size = count($responsible_user_group_ids);
	$isGroupAdmin = $responsible_user_group_id_size>0;
}

$setting_noNeedTakeAttendace = 2;

if(!isset($ToDate) || $ToDate=="")
{
	$ToDate=date('Y-m-d');
}
$ts_record = strtotime($ToDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
//$toDay=date('Y-m-d');

$page_load_time = time()+1;
if(!$plugin['attendancestudent'])
{
        header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!($ladminjob->isSmartAttendenceAdmin() || $isGroupAdmin))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$ldb = new libdb();
$luser = new libuser($UserID);
$isStudentHelper = false;
$isStudentHelper= $luser->isStudent();

$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year,$txt_month);
/*
if ($lcardattend->attendance_mode==1)
{
    header("Location: index.php");
    exit();
}
*/

$linterface         = new interface_html();
$lsmartcard        = new libsmartcard();
$CurrentPage        = "PageTakeAttendance";
if($group_id == "")
{

    $return_url =         "take_attendance.php";
    header("Location: $return_url");
     intranet_closedb();
     exit();
}
/*
if ($luser->isStudent())
{
    $class_name = $luser->ClassName;
}
*/
//$class_list = $lcardattend->getGroupListToTakeAttendanceByDate($toDay);

/*
if($class_name=="")
{
        list($itr_class_id, $itr_class_name) = $class_list[0];
        $class_name = $itr_class_name;
}
*/

$EditDaysBeforeRecord = $lcardattend->EditDaysBeforeRecord == 1;
$EditDaysBeforeRecordSelect = $lcardattend->EditDaysBeforeRecordSelect;
$IsEditDaysBeforeDisabled = false;
if($EditDaysBeforeRecord)
{
	$ts_target_date = strtotime($ToDate);
	$ts_today = strtotime(date("Y-m-d"));
	$ts_edit_days_before = $ts_today - $EditDaysBeforeRecordSelect * 86400;
	$IsEditDaysBeforeDisabled = $ts_target_date <= $ts_edit_days_before; 
}

### Period setting
//if($period=="")        $period = "AM";
switch ($period){
  case "AM": 
  	$display_period = $i_DayTypeAM;
  	$DayType = PROFILE_DAY_TYPE_AM;
  	break;
  case "PM": 
  	$display_period = $i_DayTypePM;
  	$DayType = PROFILE_DAY_TYPE_PM;
  	break;
  default : 
  	if ($lc->attendance_mode == 1) {
  		$display_period = $i_DayTypePM; 
	  	$DayType = PROFILE_DAY_TYPE_PM;
	  	$period = "PM";
  	}
  	else {
    	$display_period = $i_DayTypeAM; 
	  	$DayType = PROFILE_DAY_TYPE_AM;
	  	$period = "AM";
	  }
  	break;
}
if($group_id)
{
        # Check this class need to take attendance
        if (!$lcardattend->groupIsRequiredToTakeAttendanceByDate($group_id,$ToDate))
        {
             header ("Location: /");
             intranet_closedb();
             exit();
        }
}

if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$DefaultNumOfSessionSettings = $lcardattend->getDefaultNumOfSessionSettings();
}


  // Get preset Teacher's Remark from txt file
/*$fs = new libfilesystem();
$words_remarks = array();
$file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
if (file_exists($file_path))
{
	// Get data
	$text_data = trim($fs->file_read($file_path));
	if ($text_data != "")
	{
		$text_data_ary = explode("\n", $text_data);
		for ($i=0; $i<sizeof($text_data_ary); $i++)
		{
			if(trim($text_data_ary[$i])!="")
					$words_remarks[] = trim($text_data_ary[$i]);
		}
	}
}*/

$teacher_remark_js = "";
$teacher_remark_words = $lcardattend->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
    foreach($teacher_remark_words as $key=>$word) {
        $teacher_remark_words_temp[$key]= htmlspecialchars($word);
    }
    $teacher_remark_js = $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "RemarksArrayWords", 1);
}

### build confirmation info table
//$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".date("Y")."_".date("m");
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;

### Class select
/*$class_select = "<SELECT name=group_id>\n";
for($i=0; $i<sizeOf($class_list); $i++)
{
        list($itr_class_id, $itr_group_id) = $class_list[$i];
        $selected = ($itr_group_id==$group_id)?"SELECTED":"";
        $class_select .= "<OPTION value=\"".$itr_class_id."\" $selected>".$itr_group_id."</OPTION>\n";
}
$class_select .= "</SELECT>\n";
*/
### Time slot select
$slot_select = "<SELECT name=period>\n";
if ($lcardattend->attendance_mode != 1)
{
    $slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
}
if ($lcardattend->attendance_mode != 0)
{
    $slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
}
$slot_select .= "</SELECT>\n";
//echo "group_id [".$group_id."]<br>";
if($group_id)
{
	// get academic year info for target date
	list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($ToDate);
	
  //GET THE GROUP NAME/TITLE
  $sql = "select Title,TitleChinese from INTRANET_GROUP where groupid = '".$group_id."'";
  $resultSet = $luser->returnArray($sql,1);
  $groupName = Get_Lang_Selection($resultSet[0][1],$resultSet[0][0]);

  ### Last update string
  $sql = "SELECT
        a.RecordID,
        IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
        a.DateModified
        FROM
        $card_student_daily_group_confirm as a LEFT OUTER JOIN
                    INTRANET_GROUP as b ON (a.groupid=b.groupid) LEFT OUTER JOIN
                    INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID)
        WHERE b.groupid = '$group_id' AND DayNumber = '".$txt_day."' AND DayType = '$DayType'";
//echo "sql [".$sql."]<br>";
  $result2 = $luser->returnArray($sql,3);
  list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result2[0];

  if($confirmed_id==""){
          $update_str = $i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record;
  }
  else{
          //$update_str .= $i_LastModified .": ".$last_confirmed_date." (".$confirmed_user_name.")";
          $update_str = $last_confirmed_date." (".$confirmed_user_name.")";
  }

	if($sys_custom['StudentAttendance']['NoCardPhoto']){
		$studentIdToNoCardPhotoRecords = $lcardattend->getNoCardPhotoRecords(array('RecordDate'=>$ToDate,'StudentIDToRecord'=>1));
	}

  ### build student attend record table
  $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

  $conds = "";
  $conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1)";

  if($group_id<>"" && $period<>"")
  {
    $conds .= "AND g.groupid = \"$group_id\" ";
    $conds .= "AND g.groupid = h.groupid ";
    $conds .= "AND h.userid = a.userid ";
    $conds .= " ";
  }
  $conds .= " and (m.Mode != ".$setting_noNeedTakeAttendace." or m.Mode is null) ";
  $conds .= " ORDER BY a.classname asc,a.ClassNumber ASC ";
  
  $username_field = getNameFieldByLang("a.");
	if($sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){
		$sql = "SELECT u.UserID,u.WebSAMSRegNo,u.PhotoLink,u.UserLogin  
						FROM 
							INTRANET_GROUP as g 
							INNER JOIN 
							INTRANET_USERGROUP as ug 
							on g.GroupID = ug.GroupID 
								AND g.GroupID = '".$group_id."' 
							INNER JOIN 
							INTRANET_USER as u 
							on ug.UserID = u.UserID 
								AND 
								u.RecordType = 2 
								AND 
								u.RecordStatus IN (0,1)
						";
		//debug_r($sql);
    $temp = $ldb->returnArray($sql,4);

    $link_ary=array();
    for($i=0;$i<sizeof($temp);$i++){
      list($sid,$regno,$p_link, $p_userlogin)=$temp[$i];
      $photo_url="";
      //$PhotoPath = $luser->GET_OFFICIAL_PHOTO_BY_USER_ID($sid);
      //if (file_exists($PhotoPath[0])) {
      //	$photo_url = $PhotoPath[1];
      //}
      $PhotoAry = $luser->GET_USER_PHOTO($p_userlogin, $sid);
      if(strpos($PhotoAry[1],'samplephoto.gif')===false){
      	$photo_url = $PhotoAry[2]; // url
      }
      /*
      if($regno!=""){ # use official photo
              $tmp_regno = trim(str_replace("#", "", $regno));
              $photolink = "/file/official_photo/".$tmp_regno.".jpg";
              if(file_exists($intranet_root.$photolink))
                              $photo_url = $photolink;
      }
      */
      if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
              $photo_url = $p_link;
      }
      if($photo_url!="" && file_exists($intranet_root.$photo_url))
              $link_ary[$sid]=$photo_url;
      else $link_ary[$sid]="";

    }

    $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
    if(sizeof($link_ary)>0){
      foreach($link_ary AS $student_id => $link){
              $sql_insert_temp.="($student_id,'$link'),";
      }
      $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
    }

    $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
                                                    StudentID INT(8),Link VARCHAR(255), PRIMARY KEY(StudentID))
                                            ";
    $ldb->db_db_query($create_temp_table);
    $ldb->db_db_query($sql_insert_temp);
		//echo "sql_insert_temp [".$sql_insert_temp."]<br>";
		
		//$NameField = "
		//					IF((e.Link IS NULL OR e.Link=''),$username_field,
		//					CONCAT('
		//						<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img width=100px src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img width=100px src=',e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name";
		$NameField = "
							IF((e.Link IS NULL OR e.Link=''),$username_field,
							CONCAT('
								<table style=\"display:inline\" id=photo', a.UserID ,'a cellpadding=0 cellspacing=0><tr><td style=\"border:none\"><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif onMouseOver=\"showHideTooltipPhoto(this,true);\" onMouseOut=\"showHideTooltipPhoto(this,false);\">
								<table style=\"display:none;position:absolute;\" border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img width=100px src=',e.Link,'></td></tr></table>
								&nbsp;</td><td class=\"tabletext\" style=\"border:none\">',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.UserID ,'b cellpadding=0 cellspacing=0><tr><td><img width=100px src=',e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name";						
		$JoinTable = "LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)";
	}
	else {
		$NameField = $username_field." as name";
		$JoinTable = "";
	}
	
	if ($lcardattend->EnableEntryLeavePeriod) {
		$FilterInActiveStudent = "
 	        INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.UserID = selp.UserID 
          	AND 
          	'$ToDate' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
          ";
	}
	if($period=="AM")
	{
		$ExpectedField = $lcardattend->Get_AM_Expected_Field("b.","c.","f.");
		$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
		$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
		$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
		$ConfirmedUserField = "ConfirmedUserID";
		$IsConfirmedField = "IsConfirmed";
		$StatusField = "AMStatus";
		$BackTimeInfoField = "IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
													IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),";
	
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
		
		// original remark field
		//$daily_remark_field = "IF(d.Remark IS NULL,IF(b.".$StatusField."='".CARD_STATUS_ABSENT."' OR (b.".$StatusField." IS NULL AND ".$ExpectedField."='".CARD_STATUS_ABSENT."'),'',d.Remark),d.Remark) ";
		$daily_remark_field = "d.Remark as TeacherRemark ";
		
		$ConfirmedByAdminField = "b.AMConfirmedByAdmin";
	}
	else
	{
		if($sys_custom['StudentAttendance']['PMStatusFollowLunchSetting']){
			if($lcardattend->attendance_mode == 2 && $lcardattend->PMStatusNotFollowAMStatus){ // whole day with lunch, PM status does not follow AM status
				$LUNCH_ALLOW_LIST_TABLE = "CARD_STUDENT_LUNCH_ALLOW_LIST";
				$join_lunch_setting_table = " LEFT JOIN $LUNCH_ALLOW_LIST_TABLE ON $LUNCH_ALLOW_LIST_TABLE.StudentID=a.UserID ";
			}
		}
		
		if ($lcardattend->attendance_mode==1) // PM only mode
		{
	    	$time_field = "InSchoolTime";
		  	$station_field = "InSchoolStation";
		}
		else
		{
	    	$time_field = "LunchBackTime";
	    	$station_field = "LunchBackStation";
		}
		
		$ExpectedField = $lcardattend->Get_PM_Expected_Field("b.","c.","f.");
		$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
		$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
		$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
		$ConfirmedUserField = "PMConfirmedUserID";
		$IsConfirmedField = "PMIsConfirmed";
		$StatusField = "PMStatus";
		$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k 
										ON 
											k.StudentID = a.UserID 
											AND k.RecordDate = '".$ToDate."' 
											AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
											AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
		$BackTimeInfoField = "IF(b.$time_field IS NULL,'-',b.$time_field),
													IF(b.$station_field IS NULL,'-',b.$station_field),";
	
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
		
		$daily_remark_field = "IF(d.RecordID IS NULL AND TRIM(d2.Remark)<>'',d2.Remark,d.Remark) as TeacherRemark ";
		$join_am_daily_remark_table = " LEFT JOIN CARD_STUDENT_DAILY_REMARK as d2 ON d2.StudentID=a.UserID AND d2.RecordDate='$ToDate' AND d2.DayType='".PROFILE_DAY_TYPE_AM."' ";
		
		$ConfirmedByAdminField = "b.PMConfirmedByAdmin";
	}
  
  $sql = "SELECT        
						b.RecordID,
						a.UserID,
						".$NameField.",
						a.ClassNumber,
						a.ClassName,
						".$BackTimeInfoField." 
						IF(b.".$StatusField." IS NOT NULL,b.".$StatusField.", ".$ExpectedField."),
						$daily_remark_field,
						IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,c.Detail)) as OfficeRemark,
						IF(f.RecordID IS NOT NULL,1,0),
						f.Reason,
						f.Remark,
						f.Waive as PresetWaive, 
						IF(c.OutingID IS NOT NULL,1,0),
						c.RecordDate,
						c.OutTime,
						c.BackTime,
						c.Location,
						c.Objective,
						c.Detail,
						".$username_field.",
						if (".$ExpectedField." = '".CARD_STATUS_ABSENT."',
							".$AbsentExpectedReasonField.",
							IF(".$ExpectedField." = '".CARD_STATUS_LATE."',
								".$LateExpectedReasonField.",
								IF(".$ExpectedField." = '".CARD_STATUS_OUTING."',
									".$OutingExpectedReasonField.",
									j.Reason))) as Reason,
						j.AbsentSession,
						IFNULL(b.".$DateModifiedField.",'--'),
						IFNULL(".getNameFieldByLang("u.").",'--') as ModifyName, 
						b.".$IsConfirmedField." as IsConfirmed,
						IFNULL(".getNameFieldByLang("u1.").",'--') as ConfirmUserName, 
						j.RecordStatus, 
						ssc.LateSession,
						ssc.RequestLeaveSession,
						ssc.PlayTruantSession,
						ssc.OfficalLeaveSession,
						".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"ssc.AbsentSession as Absent2Session,":"'' as Absent2Session,")." 
						".$AbsentExpectedReasonField." as DefaultLateReason,
						".$LateExpectedReasonField." as DefaultAbsentReason, 
						".$OutingExpectedReasonField." as DefaultOutingReason,
						b.LeaveStatus,
						b.LeaveSchoolTime,
						IF(j.RecordID IS NOT NULL,j.DocumentStatus,f.DocumentStatus) as DocumentStatus,
						$ConfirmedByAdminField as ConfirmedByAdmin 
					FROM
						INTRANET_GROUP as g,
						INTRANET_USERGROUP as h,
						INTRANET_USER AS a 
						".$FilterInActiveStudent." 
						LEFT OUTER JOIN ".$card_log_table_name." AS b 
						ON (a.UserID=b.UserID AND b.DayNumber = '".$txt_day."')
						LEFT JOIN INTRANET_USER as u on b.".$ModifyByField." = u.UserID 
						LEFT JOIN INTRANET_USER as u1 on b.".$ConfirmedUserField." = u1.UserID 
						LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$ToDate."' AND c.DayType = '".$DayType."')
						LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='".$ToDate."' AND d.DayType='$DayType') 
						$join_am_daily_remark_table 
						".$JoinTable." 
						LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate='".$ToDate."' AND f.DayPeriod='".$DayType."')
						LEFT OUTER JOIN YEAR_CLASS as yc 
						on yc.ClassTitleEN = a.ClassName 
							AND 
							yc.AcademicYearID = '".$school_year_id."' 
						LEFT OUTER JOIN CARD_STUDENT_ATTENDANCE_GROUP as m on m.GroupID = yc.YearClassID
						LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = a.UserID 
							AND j.RecordDate = '".$ToDate."' 
							AND j.DayType = '".$DayType."' 
							AND 
							(
							(j.RecordType = b.".$StatusField." AND j.RecordType<>'".PROFILE_TYPE_EARLY."') 
							OR  
							(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
							AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
							)
							)
						".$AMJoinTable." $join_lunch_setting_table 
						LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as ssc
						ON ssc.StudentID = a.UserID 
							AND ssc.RecordDate = '".$ToDate."' 
							AND ssc.DayType = '".$DayType."' 
					WHERE
						".$conds;
	//debug_r($sql);
  $result = $luser->returnArray($sql);
  
  	if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
		$student_id_ary = Get_Array_By_Key($result, 'UserID');
		$bodyTemperatureRecords = $lcardattend->getBodyTemperatureRecords(array('RecordDate'=>$ToDate,'StudentID'=>$student_id_ary,'StudentIDToRecord'=>1));
	}
  
  if($field=="") $field = 0;
  if($order=="") $order = 1;
  $li = new libdbtable2007($field, $order, $pageNo);

  $setAllAttend = "<br />
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
						<option value=\"".CARD_STATUS_PRESENT."\"".($lcardattend->DefaultAttendanceStatus == CARD_STATUS_PRESENT?" selected":"").">".$Lang['StudentAttendance']['Present']."</option>
						<option value=\"".CARD_STATUS_ABSENT."\"".($lcardattend->DefaultAttendanceStatus == CARD_STATUS_ABSENT?" selected":"").">".$Lang['StudentAttendance']['Absent']."</option>						
						<option value=\"".CARD_STATUS_LATE."\">".$Lang['StudentAttendance']['Late']."</option>
						<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>
					</select><br />
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();");

  // TABLE COLUMN
  $pos = 0;
	// $li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>&nbsp;</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='15%'>". $i_UserName ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='8%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Time ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='6%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Station ."</td>\n";
  if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>". $Lang['StudentAttendance']['BodyTemperature'] ."</td>\n";
  }
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>". $i_SmartCard_Frontend_Take_Attendance_PreStatus ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='1%'>&nbsp;</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>". $i_SmartCard_Frontend_Take_Attendance_Status.$setAllAttend."</td>\n";
  if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['Late']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['RequestLeave']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['PlayTruant']."</td>\n";
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['OfficalLeave']."</td>\n";
  }
  if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['Absent']."</td>\n";
  }
  if($lcardattend->TeacherCanManageWaiveStatus)
  {
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>
  											".$i_SmartCard_Frontend_Take_Attendance_Waived.
  											($IsEditDaysBeforeDisabled?"":"<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"$('.WaiveBtn').attr('checked',this.checked);\">")."</td>\n";
  }
  if($lcardattend->TeacherCanManageProveDocumentStatus){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='5%'>".$Lang['StudentAttendance']['ProveDocument']."<br /><input type=\"checkbox\" name=\"master_Handin_prove\" value=\"1\" onClick=\"(this.checked)?setAllHandin(document.form1,1):setAllHandin(document.form1,0);\"></td>";
  }
  if($lcardattend->TeacherCanManageReason){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $i_Attendance_Reason ."</td>\n";
  }
  if($lcardattend->TeacherCanManageTeacherRemark){
  	$li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $Lang['StudentAttendance']['iSmartCardRemark']."<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllRemark\" id=\"SetAllRemark\" maxlength=\"255\" value=\"\">".$linterface->GET_PRESET_LIST("RemarksArrayWords", "SetAllRemarkIcon", "SetAllRemark")."<br>".$linterface->Get_Apply_All_Icon("javascript:SetAllRemarks();")."</td>\n";
  }
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='10%'>". $Lang['StudentAttendance']['OfficeRemark'] ."</td>\n";
  $li->column_list .= "<td class='tablegreentop tabletoplink' width='12%'>".$Lang['General']['LastModified']." (".$Lang['General']['LastModifiedBy'].")</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='15%'>".$Lang['StudentAttendance']['LastConfirmPerson']."</td>\n";

	// $li->IsColOff = "SmartCardTakeAttendance";
  $li->IsColOff = "SmartCardTakeGroupAttendance";
  $li->title = "";
}

### Button / Tag
$ViewPastRecord = '<a href="#" onclick="window.open(\'attendance_ref.php?group_id='.$group_id.'&Year='.$txt_year.'&Month='.$txt_month.'\', \'attendRefWin\');" class="tabletool">';
$ViewPastRecord .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view_s.gif" width="12" height="12" border="0" align="absmiddle" />'.$i_StudentAttendance_ViewPastRecords.'</a>';
$DisplayPhotoBtn= "<a href='javascript:toggle_photo();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> <span id=\"DisplayAllPhotoTxt\">" . $DisplayAllPhoto . "<span></a>";
$DisplayPhotoBtn = $sys_custom['SmartCardAttendance_Teacher_ShowPhoto'] ? $DisplayPhotoBtn : "";
$setAtoP        = "<a href='javascript:AbsToPre();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $i_StudentAttendance_Action_SetAbsentToPresent . "</a>";
if($IsEditDaysBeforeDisabled){
	$setAtoP = '';
}

# check eDis LATE category
if($plugin['Disciplinev12']) {	# check whether eDis "Late" category is set to "Not In Use" , and with any GM Late record in Target Date

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	
	$CanPassGmLateChecking = $ldiscipline->Check_Can_Pass_Gm_Late_Checking($ToDate);
	
} else {
	$CanPassGmLateChecking = 1;	
}


### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>".$TitleImage1. $i_StudentAttendance_Frontend_menu_TakeAttendance ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);

$linterface->LAYOUT_START();


////display html ////
echo "<script language=\"Javascript\" src='/templates/tooltip.js'></script>";
echo "<style type=\"text/css\">\n";
echo "#ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}";
echo "</style>";
echo "<div id=\"ToolTip2\"></div>";
echo "<div id=\"ToolTip3\" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>";
?>
<script language="JavaScript">
isToolTip = false;
function setToolTip(value)
{
        isToolTip=value;
}

function toggle_photo()
{

        with(document.form1)
        {
                var DisplayAllPhotoTxt=document.getElementById("DisplayAllPhotoTxt");
                <? for($i=0; $i<sizeof($result); $i++) {
                                ?>
                if(document.getElementById("photo<?=$result[$i][1]?>a"))
                {
                	var photo<?=$result[$i][1]?>a=document.getElementById("photo<?=$result[$i][1]?>a");
                	var photo<?=$result[$i][1]?>b=document.getElementById("photo<?=$result[$i][1]?>b");
                        if(photo<?=$result[$i][1]?>a.style.display=="inline")
                        {
                                photo<?=$result[$i][1]?>a.style.display = "none";
                                photo<?=$result[$i][1]?>b.style.display = "inline";
                                DisplayAllPhotoTxt.innerHTML = "<?=$HiddenAllPhoto?>";
                        }
                        else
                        {
                                photo<?=$result[$i][1]?>a.style.display = "inline";
                                photo<?=$result[$i][1]?>b.style.display = "none";
                                DisplayAllPhotoTxt.innerHTML = "<?=$DisplayAllPhoto?>";
                        }
                }
                <? } ?>
        }
}

function showHideTooltipPhoto(mouseOverObj,isShow)
{
	var obj = $(mouseOverObj);
	var imageObj = obj.next();
	
	var pos = obj.position();
	var offsetX = 18;
	var offsetY = 18;
	imageObj.css({'left':pos.left + offsetX,'top':pos.top+offsetY});
	if(isShow){
		imageObj.show();
	}else{
		imageObj.hide();
	}
}

function Check_Form() {
	<? if(!$CanPassGmLateChecking) {?>
		alert('<?=$Lang['eAttendance']['eDisciplineCategoryNotInUseWarning']?>');
		return false;
	<?
	} else {?>
		AlertPost(document.form1, 'take_update.php', '<?=$i_SmartCard_Confirm_Update_Attend?>');
	<? }
	?>
}
</script>

<style type="text/css">
        #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
    isMenu =false;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<br />
<form name="form1" method="get">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
<tr>
        <td align="center">
                <table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                        <td align="left" class="tabletext">
                      <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                                        <td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_GroupName?> </span></td>
                                        <td valign="top" class="tabletext"><?=$groupName."<input type='hidden' name='group_id' value='".$group_id."'>"?></td>
                                </tr>
                                <tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%">
										<?=$i_StudentAttendance_Field_Date?>
									</td>
									<td class="tabletext" width="70%">
										<?=$ToDate?>
									</td>
								</tr>
                                <tr>
                                        <td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_BookingTimeSlots?> </span></td>
                                        <td valign="top" class="tabletext"><?=$group_id ? $period ."<input type='hidden' name='period' value='".$period."'>" : $slot_select?></td>
                                </tr>
                                <?php if($group_id) {?>
                                <tr>
                                	<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_LastModified?> </span></td>
                                	<td valign="top" class="tabletext"><?=$update_str?></td>
                                </tr>
                                <?php } ?>
                                </table>

                        </td>
                </tr>
                <tr>
                        <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
                        <td colspan="2">
                                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                                <tr>
                                        <td align="center">
                                        <? if($group_id) {?>
                                                <?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='take_attendance.php'","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                        <? } else {?>
                                                <?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                        <? } ?>
                                        </td>
                                </tr>
                                </table>
                        </td>
                </tr>

                <tr>
                        <td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
                        <?
                        //DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
                        if($msg == 99)
                        {
                                echo "<br><font color =\"red\">".$i_StudentAttendance_Warning_Data_Outdated."</font><br><br>";
                        }
                        ?>
                        </td>
                </tr>
                <? if($group_id) {?>
                <tr>
                        <td align="left" colspan="2">

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                                <td valign="bottom" align="left">
                                                	
																									<table border=0>
																										<tr>
																											<td class="attendance_late" width="15px">
																												&nbsp;
																											</td>
																											<td class="tabletext">
																												<?=$Lang['StudentAttendance']['Late']?>
																											</td>
																											<td class="attendance_norecord" width="15px">
																												&nbsp;
																											</td>
																											<td class="tabletext">
																												<?=$Lang['StudentAttendance']['Absent']?>
																											</td>
																											<td class="attendance_outing" width="15px">
																												&nbsp;
																											</td>
																											<td class="tabletext">
																												<?=$Lang['StudentAttendance']['Outgoing']?>
																											</td>
																											<td class="tablebottom" width="15px">
																												&nbsp;
																											</td>
																											<td class="tabletext">
																												<?=$Lang['StudentAttendance']['UnConfirmed']?>
																											</td>
																										</tr>
																									</table>
                                                </td>
                                                <td align="right" valign="bottom">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_01.gif" width="21" height="23" /></td>
                                                                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_02.gif">
                                                                        <table border="0" cellspacing="0" cellpadding="2">
                                                                        <tr>
                                                                        				<td nowrap><?=$ViewPastRecord?></td>
                                                                        				<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
                                                                                <td nowrap><?=$DisplayPhotoBtn?></td>
                                                                                <td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
                                                                                <td nowrap><?=$setAtoP?></td>
                                                                        </tr>
                                                                        </table>
                                                                </td>
                                                                <td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_03.gif" width="6" height="23" /></td>
                                                        </tr>
                                                        </table>
                                                </td>
                                        </tr>
                                </table>
                        </td>
                </tr>

                <tr>
                        <td align="center">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                        <td colspan="2"><?=$li->display($display_period)?></td>
                                </tr>
                                </table>
                        </td>
                </tr>
				<?php if(!$IsEditDaysBeforeDisabled){ ?>
                <tr>
                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
                        <td align="center">
                           <?= $linterface->GET_ACTION_BTN($button_submit, "button", "Check_Form(document.form1,'take_update.php','$i_SmartCard_Confirm_Update_Attend?')","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                           <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "resetForm(document.form1);","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        </td>
                </tr>
                <?php } // end of if($IsEditDaysBeforeDisabled) ?>
                <? } ?>
                </table>
        </td>
</tr>
</table>
<br />
<input name="confirmed_user_id" type="hidden" value="<?=$UserID?>">
<input name="confirmed_id" type="hidden" value="<?=$confirmed_id?>">
<input name="hidGroup_id" type="hidden" value="<?=$group_id?>">
<input name="PageLoadTime" type="hidden" value="<?=$page_load_time?>">
<input name="ToDate" type="hidden" value="<?=$ToDate?>">
<? for ($i=0; $i<sizeof($editAllowed); $i++) { ?>
<input type="hidden" name="editAllowed<?=$i?>" value="<?=$editAllowed[$i]?>">
<? } ?>
</form>

<SCRIPT LANGUAGE="Javascript">
function view_class()
{
        with(document.form1)
        {
                window.location="?group_id=" + group_id.value + "&period="+period.value;
        }
}

var reasonSelection = Array();
<?=$reason_js_array?>
        var posx = 0;
        var posy = 0;
function calMousePos(e) {

        if (!e) var e = window.event;
        if (e.pageX || e.pageY)         {
                posx = e.pageX;
                posy = e.pageY;
        }
        else if (e.clientX || e.clientY)         {
                posx = e.clientX;
                posy = e.clientY;
        }
}

function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                	if(obj.elements[i].style.display == 'none') continue;
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
        setEditAllowed();
}

function setMenu(value){
        isMenu=value;
}

function showSelection(i, allowed)
{
         if (allowed == 1)
         {
                 setMenu(true);
                 setToolTip(false);
                 writeToLayer('ToolMenu', reasonSelection[i]);
                 moveToolTip('ToolMenu',posy,posx );
                 showLayer('ToolMenu');
         }
}
function getReasons(pos)
{
         var jArrayTemp = new Array();
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 return jArrayWordsAbsence;
             }
             else if (obj2.selectedIndex == 2)
             {
                  return jArrayWordsLate;
             }
             else if (obj2.selectedIndex == 3)
             {
                  return jArrayWordsOuting;
             }
             else return jArrayTemp;
}
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function clearReason(i){
	if (document.getElementById('input_reason'+i)) {
		var CurStatus = document.getElementById('drop_down_status_'+i).value;
		var PrevStatus = document.getElementById('PrevStatus['+i+']').value;
		
		if (PrevStatus == 1) {
			document.getElementById('AbsentDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 2) {
			document.getElementById('LateDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 3) {
			document.getElementById('OutingDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		
		if (CurStatus == 1) { // absent
			document.getElementById('input_reason'+i).value = document.getElementById('AbsentDefaultReason['+i+']').value;
		}
		else if (CurStatus == 2) { // late
			document.getElementById('input_reason'+i).value = document.getElementById('LateDefaultReason['+i+']').value;
		}
		else if (CurStatus == 3) { // Outing
			document.getElementById('input_reason'+i).value = document.getElementById('OutingDefaultReason['+i+']').value;
		}
		
		document.getElementById('PrevStatus['+i+']').value = CurStatus;
	}
}
function setEditAllowed(i){
	var drop_down_list = document.getElementsByName('drop_down_status[]');
	if(drop_down_list==null)return;
	
	var i = i || "";
<? if(!$isStudentHelper){ ?>
	if (i == "") {
		var LoopStart = 0;
		var LoopTo = drop_down_list.length;
	}
	else {
		var LoopStart = i;
		var LoopTo = (i+1);
	}
	
  for(i=LoopStart;i<LoopTo;i++){
    editAllowedObj = eval('document.form1.editAllowed'+i);
    statusObj = drop_down_list[i];
    var statusVal = $(statusObj).val();
    var office_remark_obj = $('#OfficeRemark'+i);
	if(office_remark_obj.length>0){
		statusVal != '<?=CARD_STATUS_PRESENT?>'? office_remark_obj.show(): office_remark_obj.hide();
	}
    if(editAllowedObj==null || statusObj==null) return;

		if(statusObj.selectedIndex==1||statusObj.selectedIndex==2||statusObj.selectedIndex==3){
			editAllowedObj.value=1;
		}
		else {
			editAllowedObj.value=0;
		}
		if (document.getElementById('Waived'+i)) {
			if(editAllowedObj.value == 1 && statusObj.selectedIndex!=3){
				document.getElementById('Waived'+i).style.display = "";
			}
			else 
				document.getElementById('Waived'+i).style.display = "none";
		}
		/* commented by kenneth on 20100726
		if (document.getElementById('reason'+i)) {
    	reasonObj = document.getElementById('reason'+i);
      reasoniconObj = document.getElementById('posimg'+i);
      
      if(editAllowedObj.value == 1){
        reasonObj.style.display="";
				reasoniconObj.style.display="";
      }else{
        reasonObj.style.display="none";
				reasoniconObj.style.display="none";
      }
    }
    */
    if (document.getElementById('input_reason'+i))
		{
      inputReasonObj = document.getElementById('input_reason'+i)
      inputReasoniconObj = document.getElementById('posimg_'+i);
      if(editAllowedObj.value == 1){
        inputReasonObj.style.display="";
        if (inputReasoniconObj)
					inputReasoniconObj.style.display="";
      }else{
        inputReasonObj.style.display="none";
        if (inputReasoniconObj)
					inputReasoniconObj.style.display="none";
      }
    }
    if(document.getElementById('HandIn'+i)){
    	document.getElementById('HandIn'+i).style.display = statusVal == '<?=CARD_STATUS_ABSENT?>'?'':'none';
    }
    //if(editAllowedObj.value==0)
    // All status change hide menu
    // hideMenu('ToolMenu');

    if(document.getElementById('listContent'))
    {
    	obj2 = document.getElementById('listContent').style.display='none';
    	setDivVisible(false, 'listContent', 'lyrShim');
		}
    
    <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
    	if(statusObj.selectedIndex == 1 || statusObj.selectedIndex == 2)
    	{
    		absentSessionObj = document.getElementById('absent_session'+i);
    		if(absentSessionObj != null)
    		{
    			absentSessionObj.style.display = "inline";
    		}
    	}else
    	{
    		absentSessionObj = document.getElementById('absent_session'+i);
    		if(absentSessionObj != null)
    		{
    			absentSessionObj.style.display = "none";
    		}
    	}
    	
		if(!global_do_not_init_default_absent_session){ 	
    		ChangeDefaultSessionSelect(i);
		}
    <?}?>
  }
<? } ?>
}
/*
function setEditAllowed_single(i){
        drop_down_list = document.getElementsByName('drop_down_status[]');
        if(drop_down_list==null)return;

                editAllowedObj = eval('document.form1.editAllowed'+i);
                statusObj = drop_down_list[i];
                reasonObj = eval('document.form1.reason'+i);
                reasoniconObj = eval('document.form1.posimg'+i);
                if(editAllowedObj==null || statusObj==null) return;
				//if(statusObj.selectedIndex==1||statusObj.selectedIndex==2){
                if(statusObj.selectedIndex >= 0){
                        editAllowedObj.value=1;
                        if(reasonObj!=null){
                                reasonObj.disabled=false;
                                reasoniconObj.style.display="inline";
                                reasonObj.style.background='#FFFFFF';
                        }
                }else{
                        reasoniconObj.style.display="none";
                        editAllowedObj.value=0;
                        if(reasonObj!=null){
                                reasonObj.value="";
                                reasonObj.disabled = true;
                                reasonObj.style.background='#DFDFDF';
                        }
                }
                //if(editAllowedObj.value==0)
                //        hideMenu('ToolMenu');
                if(typeof(listContent) != 'undefined')
                {
                	document.getElementById('listContent').style.display='none';
                	setDivVisible(false, 'listContent', 'lyrShim');
            	}
				
			  <?if($sys_custom['SmartCardAttendance_StaffInputReason'] == true){?>
              	if(statusObj.selectedIndex==0)
              	{
              		if (eval('document.form1.input_reason'+i))
              		{
                        inputReasonObj = eval('document.form1.input_reason'+i);
                        inputReasoniconObj = document.getElementById('posimg_'+i);
                        packimage = document.getElementById('posimg_pack'+i);
                        if(typeof(inputReasonObj) != 'undefined')
                        {
                        	inputReasonObj.value="";
                            inputReasonObj.disabled = true;
                            inputReasonObj.style.background='#DFDFDF';
                        }
                        if(inputReasoniconObj != null)
                        {
                            inputReasoniconObj.style.display="none";
                        }
                        if(packimage != null)
                        {
                        	packimage.style.display = "inline";
                        }
                    }
              	}else
              	{
              		if (eval('document.form1.input_reason'+i))
              		{
                        inputReasonObj = eval('document.form1.input_reason'+i);
                        inputReasoniconObj = document.getElementById('posimg_'+i);
                        packimage = document.getElementById('posimg_pack'+i);
                        if(typeof(inputReasonObj) != 'undefined')
                        {
                            inputReasonObj.value="";
                            inputReasonObj.disabled = false;
                            inputReasonObj.style.background='#FFFFFF';
                        }
                        if(inputReasoniconObj != null)
                        {
                            inputReasoniconObj.style.display="inline";
                        }
                        if(packimage != null)
                        {
                        	packimage.style.display = "none";
                        }
                    }
              	}
              <?}?>
              
              <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
              	if(statusObj.selectedIndex == 1 || statusObj.selectedIndex == 2)
              	{
              		absentSessionObj = document.getElementById('absent_session'+i);
              		if(absentSessionObj != null)
              		{
              			absentSessionObj.style.display = "inline";
              		}
              	}else
              	{
              		absentSessionObj = document.getElementById('absent_session'+i);
              		if(absentSessionObj != null)
              		{
              			absentSessionObj.style.display = "none";
              		}
              	}
              <?}?>

}
*/
function resetForm(formObj){
        formObj.reset();
        setEditAllowed();

}

// for Preset Absence
function showPresetAbs(obj,reason){
                        var pos_left = getPostion(obj,"offsetLeft");
                        var pos_top  = getPostion(obj,"offsetTop");

                        offsetX = (obj==null)?0:obj.width;
                        offsetY =0;
                        objDiv = document.getElementById('ToolTip3');
                        if(objDiv!=null){
                                objDiv.innerHTML = reason;
                                objDiv.style.visibility='visible';
                                objDiv.style.top = pos_top+offsetY+'px';
                                objDiv.style.left = pos_left+offsetX+'px';
                                setDivVisible(true, "ToolTip3", "lyrShim");
                        }

}

// for preset absence
function hidePresetAbs(){
                obj = document.getElementById('ToolTip3');
                if(obj!=null)
                        obj.style.visibility='hidden';
                setDivVisible(false, "ToolTip3", "lyrShim");
}

// generate options from jArrData and bold selected option
function jSelectPresetReasonText(jArrData, jTarget, jMenu, jPos){
	var listStr = "";
	var fieldValue;
	var count = 0;

	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";

	listStr = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='19'><img src='"+jImagePath+"/can_board_01.gif' width='5' height='19'></td>";
	listStr += "<td height='19' valign='middle' background='"+jImagePath+"/can_board_02.gif'></td>";
	listStr += "<td width='19' height='19'><a href='javascript:void(0)' onClick=\"document.getElementById('" + jMenu + "').style.display='none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\"><img src='"+jImagePath+"/can_board_close_off.gif' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','"+jImagePath+"/can_board_close_on.gif',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td width='5' height='50' background='"+jImagePath+"/can_board_04.gif'><img src='"+jImagePath+"/can_board_04.gif' width='5' height='19'></td>";
	
	// 20090907 Ivan
	// Problem: Scroll bar disappeared if the user open the layer more than one times
	// Temp Solution: Always show the scroll bar - changed style from overflow:auto; to overflow: scroll;
	listStr += "<td bgcolor='#FFFFF7'><div style=\"overflow-X: scroll; overflow-Y: scroll; height: " + box_height + "px; width: 160px; \">";
	
	listStr += "<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			for (var j = 0; j < jArrData[i].length; j++)
			{
				fieldValue = document.getElementById(jTarget).value.replace(/&/g, "&amp;").replace(/'/g, "&#96;").replace(/`/g, "&#96;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
				tmpValue = jArrData[i][j].replace(/&#039;/g, "\\'");
				tmpShowValue = jArrData[i][j].replace(/</g, "&lt;").replace(/>/g, "&gt;");

				listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: ";
				if (fieldValue == jArrData[i][j])
				{
					listStr += "#FFF2C3";
				} else
				{
					listStr += "#FFFFFF";
				}
				listStr += "\">";

				if (fieldValue == jArrData[i][j])
				{
					listStr += "<b>";
				}
				listStr += "<a class='presetlist' href=\"javascript:document.getElementById('" + jTarget + "').value = '" + tmpValue + "' ;document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\" title=\"" + jArrData[i][j] + "\">" + tmpShowValue + "</a>";
				
				if (fieldValue == jArrData[i][j])
				{
					listStr += "</b>";
				}

				listStr += "</td></tr>";
				count ++;
			}
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td width='6' background='"+jImagePath+"/can_board_06.gif'><img src='"+jImagePath+"/can_board_06.gif' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td width='5' height='6'><img src='"+jImagePath+"/can_board_07.gif' width='5' height='6'></td>";
	listStr += "<td height='6' background='"+jImagePath+"/can_board_08.gif'><img src='"+jImagePath+"/can_board_08.gif' width='100%' height='6' border='0' /></td>";
	listStr += "<td width='6' height='6'><img src='"+jImagePath+"/can_board_09.gif' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";

	document.getElementById(jMenu).innerHTML = listStr;
	document.getElementById(jMenu).style.visibility = 'visible';

	var pos = jFindPos(document.getElementById("posimg_" + jPos));

	document.getElementById(jMenu).style.left = parseInt(pos[0]) + "px";
	document.getElementById(jMenu).style.top = parseInt(pos[1]) + "px";

	setDivVisible(true, jMenu, "lyrShim");

}

function ChangeDefaultSessionSelect(i) {
	var IsConfirmedAlready = document.getElementById('Confirmed['+i+']');
    var ignore_confirm_check = <?=(($sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm'] == true) ? 'true' : 'false')?>;
	if ((IsConfirmedAlready && IsConfirmedAlready.value == "") || ignore_confirm_check == true) {
		var drop_down_list = document.getElementsByName('drop_down_status[]');
		if(drop_down_list==null)return;
		var StatusObj = drop_down_list[i];
		var LateSessionObj = document.getElementById('late_session'+i);
		var OfficalLeaveSessionObj = document.getElementById('offical_leave_session'+i);
		var RequestLeaveSessionObj = document.getElementById('request_leave_session'+i);
		var PlayTruantSessionObj = document.getElementById('play_truant_session'+i);
		if (LateSessionObj) {
			if (StatusObj.selectedIndex == 1) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				//RequestLeaveSessionObj.selectedIndex = 2;
				//PlayTruantSessionObj.selectedIndex = 0;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave'] != -1){?>
					RequestLeaveSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForLeave']*2)?>;
				<?}else{?>
					RequestLeaveSessionObj.selectedIndex = 2;
				<?}?>
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism'] != -1){?>
					PlayTruantSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsenteesism']*2)?>;
				<?}else{?>
					PlayTruantSessionObj.selectedIndex = 0;
				<?}?>
			}
			else if (StatusObj.selectedIndex == 2) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				//LateSessionObj.selectedIndex = 2;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForLate'] != -1){?>
					LateSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForLate']*2)?>;
				<?}else{?>
					LateSessionObj.selectedIndex = 2;
				<?}?>
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
			else {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
		}
		<?php if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){ ?>
		var AbsentSessionObj = document.getElementById('absent2_session'+i);
		if(AbsentSessionObj){
			AbsentSessionObj.selectedIndex = 0;
			if(StatusObj.selectedIndex == 1) {
				<?php if(isset($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']) && $DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent'] != -1){ ?>
				AbsentSessionObj.selectedIndex = <?=($DefaultNumOfSessionSettings['DefaultNumOfSessionForAbsent']*2)?>;
				<?php } ?>
			}
		}
		<?php } ?>
	}
}

function SetAllAttend()
{
	var apply_all_value = $('#drop_down_status_all').val();
	var status_selections = document.getElementsByName('drop_down_status[]');
	for(var i=0;i<status_selections.length;i++){
		if(status_selections[i].style.display == 'none') continue;
		$(status_selections[i]).val(apply_all_value);
		clearReason(i);
		setEditAllowed(i);
		ChangeDefaultSessionSelect(i);
	}
}

function setAllHandin(formObj,val)
{
	if(formObj==null) return;
	for(i=0;i<formObj.elements.length;i++){
		obj = formObj.elements[i];
		if(typeof(obj)!="undefined"){
			if(obj.name.indexOf("HandIn_prove_")>-1){
				obj.checked = val==1?true:false;
			}
		}
	}
}

function SetAllRemarks() 
{
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyRemark = document.getElementById("SetAllRemark").value;
	for (var i=0; i< StatusSelection.length; i++) {
		document.getElementById('reason'+i).value = ApplyRemark;
	}
}

function getRemarks(pos)
{
	return RemarksArrayWords;
}

<?php if($sys_custom['StudentAttendance']['AllowEditTime']){ ?>
function onRevealEditTime(uid, time)
{
	var text_field = '<input type="text" id="InSchoolTime'+uid+'" name="InSchoolTime'+uid+'" value="'+time+'" size="10" onchange="onEditTimeChanged('+uid+')" />';
	
	var text_obj = $('#InSchoolTime'+uid);
	if(text_obj && text_obj.length > 0)
	{
		text_obj.val(time);
	}else{
		$('#cancelTimeBtn'+uid).before(text_field);
	}
	
	$('#InSchoolTime_Display'+uid).hide();
	$('#InSchoolTime_Edit'+uid).show();
}

function onCancelEditTime(uid)
{
	$('#InSchoolTime_Display'+uid).show();
	$('#InSchoolTime_Edit'+uid).hide();
	$('#InSchoolTime'+uid).remove();
}

function onEditTimeChanged(uid)
{
	var obj = $('#InSchoolTime'+uid);
	var val = $.trim(obj.val());
	if(val != ''){
		if(!val.match(/^\d\d:\d\d:\d\d$/g)){
			obj.val('');
			return;
		}
		var parts = val.split(':');
		var hr = Math.max(0, Math.min(24, parseInt(parts[0])));
		var min = Math.max(0, Math.min(60, parseInt(parts[1])));
		var sec = Math.max(0, Math.min(60, parseInt(parts[2])));
		
		var val_str = hr < 10? '0' + hr : '' + hr;
		val_str += ':';
		val_str += min < 10? '0' + min : '' + min;
		val_str += ':';
		val_str += sec < 10? '0' + sec : '' + sec;
		obj.val(val_str);
	}
}
<?php } ?>
</SCRIPT>
<?=$teacher_remark_js?>
<script>
var global_do_not_init_default_absent_session = <?php echo $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']?'true':'false'?>;
setEditAllowed();
global_do_not_init_default_absent_session = false;
</script>
<?php if($sys_custom['StudentAttendance']['NoCardPhoto']){ ?>
<link rel="stylesheet" href="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$('a.fancybox').fancybox();
});
</script>
<?php } ?>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>