<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_auth();
intranet_opendb();
$toDay=date('Y-m-d');
$page_load_time = time();
if(!$plugin['attendancestudent'])
{
	header("Location: $intranet_httppath/");
        exit();
}

$ladminjob = new libadminjob($UserID);
if (!$ladminjob->isSmartAttendenceAdmin())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$ldb = new libdb();
$luser = new libuser($UserID);
$isStudentHelper = false;
$isStudentHelper= $luser->isStudent();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
if ($lcardattend->attendance_mode==1)
{
    header("Location: index.php");
    exit();
}


$linterface 	= new interface_html();
$lsmartcard	= new libsmartcard();
$CurrentPage	= "PageTakeAttendance";

/* fai have question 
if ($luser->isStudent())
{
    $class_name = $luser->ClassName;
}
*/
$class_list = $lcardattend->getGroupListToTakeAttendanceByDate($toDay);

/*
if($class_name=="")
{
	list($itr_class_id, $itr_class_name) = $class_list[0];
        $class_name = $itr_class_name;
}
*/

### Period setting
//if($period=="")	$period = "AM";
switch ($period){
        case "AM": $display_period = $i_DayTypeAM;  break;
        case "PM": $display_period = $i_DayTypePM;  break;
        default : $display_period = $i_DayTypeAM; $period = "AM";
}
if($group_id)
{
	# Check this class need to take attendance
	if (!$lcardattend->isRequiredToTakeAttendanceByDate($group_id,$toDay))
	{
	     header ("Location: /");
	     intranet_closedb();
	     exit();
	}
}

### build confirmation info table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".date("Y")."_".date("m");

### Class select
$class_select = "<SELECT name=group_id>\n";
for($i=0; $i<sizeOf($class_list); $i++)
{
        list($itr_class_id, $itr_group_id) = $class_list[$i];
        $selected = ($itr_group_id==$group_id)?"SELECTED":"";
        $class_select .= "<OPTION value=\"".$itr_class_id."\" $selected>".$itr_group_id."</OPTION>\n";
}
$class_select .= "</SELECT>\n";

### Time slot select
$slot_select = "<SELECT name=period>\n";
if ($lcardattend->attendance_mode != 1)
{
    $slot_select .= "<OPTION value=\"AM\" ".($period=="AM"?"SELECTED":"").">$i_DayTypeAM</OPTION>\n";
}
if ($lcardattend->attendance_mode != 0)
{
    $slot_select .= "<OPTION value=\"PM\" ".($period=="PM"?"SELECTED":"").">$i_DayTypePM</OPTION>\n";
}
$slot_select .= "</SELECT>\n";

$DayPeriod = $period == "AM" ? "2" : "3";
//echo "group_id [".$group_id."]<br>";
if($group_id)
{
	//GET THE GROUP NAME/TITLE
	$sql = "select title from INTRANET_GROUP where groupid = ".$group_id; 
	$resultSet = $luser->returnArray($sql,1);
	$groupName = $resultSet[0][0];

	### Last update string
	$sql = "SELECT
              a.RecordID,
              IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
              a.DateModified
              FROM
              $card_student_daily_class_confirm as a LEFT OUTER JOIN 
			  INTRANET_GROUP as b ON (a.ClassID=b.groupid) LEFT OUTER JOIN 
			  INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID)
              WHERE b.groupid = \"$group_id\" AND DayNumber = ".date("d")." AND DayType = $DayPeriod";
//echo "sql [".$sql."]<br>"; //fai wait for testing
	$result2 = $luser->returnArray($sql,3);
	list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result2[0];

	if($confirmed_id==""){
		$update_str = $i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record;
	}
	else{
	        $update_str .= $i_LastModified .": ".$last_confirmed_date." (".$confirmed_user_name.")";
	}

//fai	
//	$ldb = new libdb();
	### check if Preset Absence exists
$sql = "select count(*) 
	from 
		INTRANET_USER a,
		CARD_STUDENT_PRESET_LEAVE b,
		INTRANET_GROUP c,
		INTRANET_USERGROUP d
	where 
		a.recordtype = 2 and
		a.recordstatus in (0,1,2) and
		c.groupid = '$group_id' and
		b.recorddate = curdate() and
		b.dayperiod = '$DayPeriod' and
		a.userid = b.studentid and
		c.groupid = d.groupid and
		a.userid = d.userid
		";	
//	echo "sql [".$sql."]<br>";
	$temp = $ldb->returnVector($sql);
	$preset_count = $temp[0];
	
	### build student attend record table
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".date("Y")."_".date("m");
	
	$conds = "";
	$conds .= " a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
	
	if($group_id<>"" && $period<>"")
	{
	        $conds .= "AND g.groupid = \"$group_id\" ";
			$conds .= "AND g.groupid = h.groupid ";
			$conds .= "AND h.userid = a.userid ";
			$conds .= " ";
	}
	$conds .= "ORDER BY a.classname asc,a.ClassNumber ASC ";
	if($period=="AM")
	{
	        if(!$sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){ # no need to show photo
	        	$username_field = getNameFieldByLang("a.");	                
	                        $sql  = "SELECT        b.RecordID,
	                               a.UserID,
	                               ".getNameFieldByLang("a.")."as name,
	                               a.ClassNumber,
								   a.ClassName,
	                               IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
	                               IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
	                               IF(b.AMStatus IS NOT NULL,
	                                  b.AMStatus,
	                                  CASE
	                                      WHEN b.InSchoolTime IS NOT NULL THEN '0'
	                                      WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '1'
	                                      WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '3'
	                                      END
	                                      ),
	                       			IF(d.Remark IS NULL, IF( (b.AMStatus IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL) OR b.AMStatus=1,f.Reason,d.Remark),d.Remark),
	                       			IF(f.RecordID IS NOT NULL,1,0),
	                       			f.Reason,
	                       			f.Remark,
									$username_field
	                FROM
						INTRANET_GROUP as g,
						INTRANET_USERGROUP as h,
	                    INTRANET_USER AS a
	                        LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = '".date("d")."')
	                        LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
	                        LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
	               			LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=$DayPeriod)
	                WHERE $conds
	        ";
//echo " ======> sql [".$sql."]<br>";
	        }
	        else{ # need to show photo
	                //$sql = "SELECT a.UserID,a.WebSAMSRegNo,a.PhotoLink  FROM INTRANET_USER AS a WHERE a.ClassName='$group_id' AND a.RecordType=2 AND a.RecordStatus IN(0,1)";
					$sql = "SELECT 
								a.userid, a.WebSAMSRegNo,a.PhotoLink 
							From 
								INTRANET_USER a, INTRANET_GROUP b , INTRANET_USERGROUP c
							where
								a.RecordType=2 AND 
								a.RecordStatus IN(0,1) AND
								b.groupid = '$group_id'  AND
								b.groupid = c.groupid AND
								c.userid = a.userid
							";
//echo "aaaa ==> [".$sql."]<br>";	        
	                $temp = $ldb->returnArray($sql,3);
	        
	                $link_ary=array();
	                for($i=0;$i<sizeof($temp);$i++){
	                        list($sid,$regno,$p_link)=$temp[$i];
	                        $photo_url="";
	                        if($regno!=""){ # use official photo
	                                $tmp_regno = trim(str_replace("#", "", $regno));
	                                $photolink = "/file/official_photo/".$tmp_regno.".jpg";
	                                if(file_exists($intranet_root.$photolink))
	                                                $photo_url = $photolink;
	                        }
	                        if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
	                                $photo_url = $p_link;
	                        }
	                        if($photo_url!="" && file_exists($intranet_root.$photo_url))
	                                $link_ary[$sid]=$photo_url;
	                        else $link_ary[$sid]="";
	        
	                }
	        
	                $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
	                if(sizeof($link_ary)>0){
	                        foreach($link_ary AS $student_id => $link){
	                                $sql_insert_temp.="($student_id,'$link'),";
	                        }
	                        $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
	                }
	        
	                $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
	                                                                StudentID INT(8),Link VARCHAR(255) )
	                                                        ";
	                $ldb->db_db_query($create_temp_table);
	                $ldb->db_db_query($sql_insert_temp);
	                $username_field = getNameFieldByLang("a.");
//sql 22
	          $sql  = "SELECT        b.RecordID,
	                               a.UserID,
	                               IF((e.Link IS NULL OR e.Link=''),$username_field,
	        			CONCAT('<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.ClassNumber ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.ClassNumber ,'b cellpadding=0 cellspacing=0><tr><td><img src=', e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name,
	                               a.ClassNumber,
								   a.ClassName,	
	                               IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
	                               IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
	                               IF(b.AMStatus IS NOT NULL,
	                                  b.AMStatus,
	                                  CASE
	                                      WHEN b.InSchoolTime IS NOT NULL THEN '0'
	                                      WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '1'
	                                      WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '3'
	                                      END
	                                      ),
	                                 IF(d.Remark IS NULL, IF( (b.AMStatus IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL) OR b.AMStatus=1,f.Reason,d.Remark),d.Remark),
	                                 IF(f.RecordID IS NOT NULL,1,0),
	                                 f.Reason,
	                                 f.Remark,
	                                 $username_field
	                FROM
						INTRANET_GROUP as g,
						INTRANET_USERGROUP as h,
	                    INTRANET_USER AS a
	                        LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = '".date("d")."')
	                        LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
	                        LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
	                        LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)
	               			LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=$DayPeriod)
	                WHERE $conds
		        ";
//			echo "sql sql [".$sql."]<br>";
	        }
	}
	else
	{
			$pm_status_field =   "   CASE
	                              WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (b.LunchOutTime IS NULL OR b.LunchBackTime IS NOT NULL))
	                                   THEN '".CARD_STATUS_PRESENT."'
	                              WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LunchOutTime IS NOT NULL) AND (b.LunchBackTime IS NULL)
	                                   THEN '".CARD_STATUS_ABSENT."'
	                              WHEN c.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
	                              ELSE '".CARD_STATUS_ABSENT."'
	                          END
							";
			$username_field = getNameFieldByLang("a.");

	if(!$sys_custom['SmartCardAttendance_Teacher_ShowPhoto']){ # no need to show photo
	$sql  = "SELECT        b.RecordID,
	                       a.UserID,
	                       ".getNameFieldByLang("a.")."as name,
	                       a.ClassNumber,
							a.ClassName,
	                       IF(b.LunchBackTime IS NULL,
	                          '-',
	                          b.LunchBackTime),
	                       IF(b.LunchBackStation IS NULL,
	                          '-',
	                          b.LunchBackStation),
	                       IF(b.PMStatus IS NOT NULL,
	                          b.PMStatus,$pm_status_field ),
	                       IF(d.Remark IS NULL,IF(b.PMStatus='".CARD_STATUS_ABSENT."' OR (b.PMStatus IS NULL AND $pm_status_field='".CARD_STATUS_ABSENT."'),f.Reason,d.Remark),d.Remark),
	                       IF(f.RecordID IS NOT NULL,1,0),
	                       f.Reason,
	                       f.Remark,
	                       $username_field
	                       FROM
							   INTRANET_GROUP as g,
							   INTRANET_USERGROUP as h,
	                           INTRANET_USER AS a
	                              LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = ".date("d").")
	                              LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
	                              LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
	                   			  LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=3)
	                       WHERE
	                            $conds";
	}
	else{ # need to show photo
	
	
		//$sql = "SELECT a.UserID,a.WebSAMSRegNo,a.PhotoLink  FROM INTRANET_USER AS a WHERE a.ClassName='$group_id' AND a.RecordType=2 AND a.RecordStatus IN(0,1)";
			$sql = "SELECT 
						a.userid, a.WebSAMSRegNo,a.PhotoLink 
					From 
						INTRANET_USER a, INTRANET_GROUP b , INTRANET_USERGROUP c
					where
						a.RecordType=2 AND 
						a.RecordStatus IN(0,1) AND
						b.groupid = '$group_id'  AND
						b.groupid = c.groupid AND
						c.userid = a.userid
				   ";
	        $temp = $ldb->returnArray($sql,3);
	
	        $link_ary=array();
	        for($i=0;$i<sizeof($temp);$i++){
	                list($sid,$regno,$p_link)=$temp[$i];
	                $photo_url="";
	                if($regno!=""){ # use official photo
	                        $tmp_regno = trim(str_replace("#", "", $regno));
	                        $photolink = "/file/official_photo/".$tmp_regno.".jpg";
	                        if(file_exists($intranet_root.$photolink))
	                                        $photo_url = $photolink;
	                }
	                if($photo_url=="" && $p_link!=""){ # use photo uploaded by student
	                        $photo_url = $p_link;
	                }
	                if($photo_url!="" && file_exists($intranet_root.$photo_url))
	                        $link_ary[$sid]=$photo_url;
	                else $link_ary[$sid]="";
	
	        }
	
	        $sql_insert_temp = "INSERT INTO TEMP_STUDENT_PHOTO_LINK (StudentID,Link) VALUES";
	        if(sizeof($link_ary)>0){
	                foreach($link_ary AS $student_id => $link){
	                        $sql_insert_temp.="($student_id,'$link'),";
	                }
	                $sql_insert_temp = substr($sql_insert_temp,0,strlen($sql_insert_temp)-1);
	        }
	
	        $create_temp_table = "CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_STUDENT_PHOTO_LINK(
	                                                        StudentID INT(8),Link VARCHAR(255) )
	                                                ";
	        $ldb->db_db_query($create_temp_table);
	        $ldb->db_db_query($sql_insert_temp);
	        $username_field = getNameFieldByLang("a.");
//sql 44	
	$sql  = "SELECT        b.RecordID,
	                       a.UserID,
	                       IF((e.Link IS NULL OR e.Link=''),$username_field,
	                           CONCAT('<div onMouseOut=\"setToolTip(false);setMenu(true);hideTip();\" onMouseOver=\"setToolTip(true);setMenu(false);showTip(\'ToolTip2\',makeTip(\'<table border=0 cellpadding=5 cellspacing=0 class=attendancestudentphoto><tr><td><img src=',e.Link,'></td></tr></table>\'))\"><table style=\"display:inline\" id=photo', a.ClassNumber ,'a cellpadding=0 cellspacing=0><tr><td><img src={$image_path}/{$LAYOUT_SKIN}/icon_photo.gif>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table></div><table style=\"display:none\" id=photo', a.ClassNumber ,'b cellpadding=0 cellspacing=0><tr><td><img src=', e.Link,'>&nbsp;</td><td class=tabletext>',$username_field,'</td></tr></table>')) as name,
	                       a.ClassNumber,
						   a.ClassName,
	                       IF(b.LunchBackTime IS NULL,
	                          '-',
	                          b.LunchBackTime),
	                       IF(b.LunchBackStation IS NULL,
	                          '-',
	                          b.LunchBackStation),
	                       IF(b.PMStatus IS NOT NULL,
	                          b.PMStatus, $pm_status_field),
	                       IF(d.Remark IS NULL,IF(b.PMStatus='".CARD_STATUS_ABSENT."' OR (b.PMStatus IS NULL AND $pm_status_field='".CARD_STATUS_ABSENT."'),f.Reason,d.Remark),d.Remark),
	                       IF(f.RecordID IS NOT NULL,1,0),
	                       f.Reason,
	                       f.Remark,
	                       $username_field
	                       FROM
							   INTRANET_GROUP as g,
							   INTRANET_USERGROUP as h,
	                           INTRANET_USER AS a
	                              LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID=b.UserID AND b.DayNumber = ".date("d").")
	                              LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = CURDATE())
	                              LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate=CURDATE())
	                              LEFT OUTER JOIN TEMP_STUDENT_PHOTO_LINK AS e ON (a.UserID=e.StudentID)
	       						 LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.RecordDate=CURDATE() AND f.DayPeriod=3)
	                       WHERE
	                            $conds";
	
	                            
	}
	}        
	
	$result = $luser->returnArray($sql,12);
	
	if($field=="") $field = 0;
	if($order=="") $order = 1;
	$li = new libdbtable2007($field, $order, $pageNo);
	
	// TABLE COLUMN
	$pos = 0;
//	$li->column_list .= "<td class='tablegreentop tabletoplink' width='3%'>&nbsp;</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='20%'>". $i_UserName ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='18%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Time ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='18%'>". $i_SmartCard_Frontend_Take_Attendance_In_School_Station ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='9%'>". $i_SmartCard_Frontend_Take_Attendance_PreStatus ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='2%'>&nbsp;</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='15%'>". $i_SmartCard_Frontend_Take_Attendance_Status ."</td>\n";
	$li->column_list .= "<td class='tablegreentop tabletoplink' width='20%'>". $i_Attendance_Remark ."</td>\n";
	             
//	$li->IsColOff = "SmartCardTakeAttendance";
	$li->IsColOff = "SmartCardTakeGroupAttendance";
	$li->title = "";
}

### Button / Tag
$DisplayPhotoBtn= "<a href='javascript:toggle_photo();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> <span id=\"DisplayAllPhotoTxt\">" . $DisplayAllPhoto . "<span></a>";
$DisplayPhotoBtn = $sys_custom['SmartCardAttendance_Teacher_ShowPhoto'] ? $DisplayPhotoBtn : "";
$setAtoP	= "<a href='javascript:AbsToPre();' class='tabletool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $i_StudentAttendance_Action_SetAbsentToPresent . "</a>";

### Title ###
$MODULE_OBJ = $lsmartcard->GET_MODULE_OBJ_ARR();
$TitleImage1 = "<img src='{$image_path}/{$LAYOUT_SKIN}/smartcard/icon_takeattendance.gif' align='absmiddle' />";
$TitleTitle1 = "<span class='contenttitle'>". $i_StudentAttendance_Frontend_menu_TakeAttendance ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleImage1.$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$linterface->LAYOUT_START();


////display html ////
echo "<script language=\"Javascript\" src='/templates/tooltip.js'></script>";
echo "<style type=\"text/css\">\n";
echo "#ToolTip2{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}";
echo "</style>";
echo "<div id=\"ToolTip2\"></div>";
echo "<div id=\"ToolTip3\" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>";
?>
<script language="JavaScript">
isToolTip = false;
function setToolTip(value)
{
	isToolTip=value;
}

function toggle_photo()
{

	with(document.form1)
        {
                <? for($i=0; $i<sizeof($result); $i++) {
/*
					Testing 
					echo "size of [".sizeof($result[$i])."]<br>";
					for($y = 0;$y < sizeof($result[$i]);$y++)
					{
						echo "===> [".$result[$i][$y]."]\n";
					}
*/
				?>
                if(document.getElementById("photo<?=$result[$i][3]?>a") != null)
			if(photo<?=$result[$i][3]?>a.style.display=="inline")	
                        {
                        	photo<?=$result[$i][3]?>a.style.display = "none"
                                photo<?=$result[$i][3]?>b.style.display = "inline"
                                DisplayAllPhotoTxt.innerHTML = "<?=$HiddenAllPhoto?>";
			}
                        else
                        {
                        	photo<?=$result[$i][3]?>a.style.display = "inline"
                                photo<?=$result[$i][3]?>b.style.display = "none"
                                DisplayAllPhotoTxt.innerHTML = "<?=$DisplayAllPhoto?>";
			}
		<? } ?>
        }
}
</script>

<style type="text/css">
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
    isMenu =false;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<br />
<form name="form1" method="get">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" onMouseMove="overhere();">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
              	<table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_SmartCard_GroupName?> </span></td>
					<td valign="top" class="tabletext"><?=$group_id ? $groupName ."<input type='hidden' name='group_id' value='".$group_id."'>" : $class_select?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletext"><?=$i_BookingTimeSlots?> </span></td>
					<td valign="top" class="tabletext"><?=$group_id ? $period ."<input type='hidden' name='period' value='".$period."'>" : $slot_select?></td>
				</tr>
				</table>

			</td>
		</tr>
                <tr>
			<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
		</tr>
                <tr> 
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
                <tr>
                	<td colspan="2">        
                                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                                <tr>
                			<td align="center">
                			<? if($group_id) {?>
                				<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='take_attendance.php'","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
							<? } else {?>
								<?= $linterface->GET_ACTION_BTN($button_view, "button", "view_class();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>							
							<? } ?>
                			</td>
                		</tr>
                                </table>                                
                	</td>
                </tr>
                
		<tr>
			<td align="center" class="tabletextremark" colspan="2">&nbsp;&nbsp;&nbsp;
			<?
			//DATA HAS BEEN UPDATED BY OTHER BEFORE SUBMIT THIS PAGE, DATA NOT CONSISTENCE
			if($msg == 99)
			{
				echo "<br><font color =\"red\">".$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record_Update."</font><br><br>";
			}
			?>
			</td>
		</tr>
                <? if($group_id) {?>
                <tr>
                	<td align="left" colspan="2">
                	
        			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                	<tr>
                                		<td><span class="tabletext"><?=$update_str?></span></td>
                                		<td align="right" valign="bottom">
                                                	<table border="0" cellspacing="0" cellpadding="0">
                                			<tr>
                                				<td width="21"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_01.gif" width="21" height="23" /></td>
                                				<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_02.gif">
                                                                	<table border="0" cellspacing="0" cellpadding="2">
                                					<tr>
                                						<td nowrap><?=$DisplayPhotoBtn?></td>
										<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="5" /></td>
                                						<td nowrap><?=$setAtoP?></td>
                                					</tr>
                                					</table>
								</td>
                                                                <td width="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/tablegreen_tool_03.gif" width="6" height="23" /></td>
							</tr>
                                                        </table>
						</td>
					</tr>
				</table>
                        </td>
		</tr>
          
                <tr>
                	<td align="center">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                			<td colspan="2"><?=$li->display($display_period)?></td>
                        	</tr>
                                </table>
                        </td>
                </tr>

                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "AlertPost(document.form1,'take_update.php','$i_SmartCard_Confirm_Update_Attend?')","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "resetForm(document.form1);","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
		<? } ?>      
                </table>                                
	</td>
</tr>
</table>
<br />        
<input name="confirmed_user_id" type="hidden" value="<?=$UserID?>">
<input name="confirmed_id" type="hidden" value="<?=$confirmed_id?>">
<input name="hidGroup_id" type="hidden" value="<?=$group_id?>">
<input name="PageLoadTime" type="hidden" value="<?=$page_load_time?>">
<? for ($i=0; $i<sizeof($editAllowed); $i++) { ?>
<input type="hidden" name="editAllowed<?=$i?>" value="<?=$editAllowed[$i]?>">
<? } ?>
</form>    

<SCRIPT LANGUAGE="Javascript">
function view_class()
{
	with(document.form1)
        {
        	window.location="?group_id=" + group_id.value + "&period="+period.value;
        }
}

var reasonSelection = Array();
<?=$reason_js_array?>
        var posx = 0;
        var posy = 0;
function calMousePos(e) {

        if (!e) var e = window.event;
        if (e.pageX || e.pageY)         {
                posx = e.pageX;
                posy = e.pageY;
        }
        else if (e.clientX || e.clientY)         {
                posx = e.clientX;
                posy = e.clientY;
        }
}

function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
        setEditAllowed();
}

function setMenu(value){
        isMenu=value;
}

function showSelection(i, allowed)
{
         if (allowed == 1)
         {
                 setMenu(true);
                 setToolTip(false);
                 writeToLayer('ToolMenu', reasonSelection[i]);
                 moveToolTip('ToolMenu',posy,posx );
                 showLayer('ToolMenu');
         }
}

function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function clearReason(obj){
	if(obj!=null)
		obj.value="";
}
function setEditAllowed(){
        drop_down_list = document.getElementsByName('drop_down_status[]');
        if(drop_down_list==null)return;
        for(i=0;i<drop_down_list.length;i++){
                editAllowedObj = eval('document.form1.editAllowed'+i);
                statusObj = drop_down_list[i];
                reasonObj = eval('document.form1.reason'+i);
                reasoniconObj = eval('document.form1.posimg'+i);
                if(editAllowedObj==null || statusObj==null) return;

                if(statusObj.selectedIndex==1||statusObj.selectedIndex==2){
                        editAllowedObj.value=1;
                        if(reasonObj!=null){
                                reasonObj.disabled=false;
                                reasoniconObj.style.display="inline";
                        }
                }else{
                	reasoniconObj.style.display="none";
                        editAllowedObj.value=0;
                        if(reasonObj!=null){
                                reasonObj.value="";
                                reasonObj.disabled = true;
                        }
                }
                if(editAllowedObj.value==0)
                        hideMenu('ToolMenu');
        }
}

function resetForm(formObj){
        formObj.reset();
        setEditAllowed();
        
}

// for Preset Absence
function showPresetAbs(obj,reason){
			var pos_left = getPostion(obj,"offsetLeft");
			var pos_top  = getPostion(obj,"offsetTop");
			
			offsetX = (obj==null)?0:obj.width;
			offsetY =0;
			objDiv = document.getElementById('tooltip3');
			if(objDiv!=null){
				objDiv.innerHTML = reason;
				objDiv.style.visibility='visible';
				objDiv.style.top = pos_top+offsetY;
				objDiv.style.left = pos_left+offsetX;
				setDivVisible(true, "tooltip3", "lyrShim");
			}

}

// for preset absence
function hidePresetAbs(){
		obj = document.getElementById('tooltip3');
		if(obj!=null)
			obj.style.visibility='hidden';
		setDivVisible(false, "tooltip3", "lyrShim");
}
</SCRIPT>

<script>
setEditAllowed();
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

