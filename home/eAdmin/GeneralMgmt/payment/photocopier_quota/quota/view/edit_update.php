<?
// Editing by 
/*
 * 2014-04-10 (Carlos): Added quota change log which is missing before
 * 2014-03-28 (Carlos): Added PAYMENT_PRINTING_QUOTA.ModifiedBy and PAYMENT_PRINTING_QUOTA_CHANGE_LOG.ModifiedBy for trace purpose
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

//$TempUserID = $_SESSION['UserID'];

$lidb = new libdb();

$Result = array();
$lidb->Start_Trans();
if(is_array($user_id))
{
	$targetUser = "'".implode("','",$user_id)."'";
	
	$sql = "SELECT UserID FROM PAYMENT_PRINTING_QUOTA WHERE UserID IN ($targetUser)";
	$record_exist = $lidb->returnVector($sql);
	$record_not_exist = array_diff($user_id, $record_exist);
		
	if(sizeof($record_exist) > 0)
	{
		foreach($record_exist AS $key => $tmp_user_id)
		{	
			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = '". ${"total_quota_$tmp_user_id"} ."', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID = '$tmp_user_id'";
			$updatePrintingQuotaLogCondition = "where UserID='$tmp_user_id' ";
			$Result['UpdatePaymentQuotaLog'] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($_REQUEST['total_quota_'.$tmp_user_id],2,$updatePrintingQuotaLogCondition,$lidb);	
			$Result['UpdateQuota:'.$tmp_user_id] = $lidb->db_db_query($sql);
		}
	}
	if(sizeof($record_not_exist) > 0)
	{
		foreach($record_not_exist AS $key => $tmp_user_id)
		{
			$sql = "INSERT INTO PAYMENT_PRINTING_QUOTA VALUES ('$tmp_user_id', '". ${"total_quota_$tmp_user_id"} ."', 0, '', '', NOW(), NOW(), '".$_SESSION['UserID']."')";
			$updatePrintingQuotaLogCondition = "where UserID='$tmp_user_id' ";
			$Result['UpdatePaymentQuotaLog'] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($_REQUEST['total_quota_'.$tmp_user_id],2,$updatePrintingQuotaLogCondition,$lidb);	
			$Result['InsertQuota:'.$tmp_user_id] = $lidb->db_db_query($sql);
		}
	}
}
else
{
	$Result[] = false;
}

if (in_array(false,$Result)) {
	$lidb->RollBack_Trans();
	$Msg = $Lang['ePayment']['PrintQuotaEditFail'];
}
else {
	$lidb->Commit_Trans();
	$Msg = $Lang['ePayment']['PrintQuotaEditSuccess'];
}

//$_SESSION['UserID'] = $TempUserID;

header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();

?>