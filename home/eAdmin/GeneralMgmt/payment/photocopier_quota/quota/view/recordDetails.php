<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
$linterface = new interface_html();

$lidb = new libdb();
intranet_opendb();
$sql = "select 
			PaperType,  NumberOfPaper
		from 
			PAYMENT_COPIER_TRANSACTION_DETAIL 
		where 
			transactionLogID = '$transactionID'
		order by 
			PaperType
		";
$temp = $lidb->returnArray($sql,2);

$templateContent = get_file_content("inc_transactionDetails.tmpl");

$templateContent = str_replace('<!--Type-->', $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type, $templateContent);
$templateContent = str_replace('<!--Qty-->', $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity, $templateContent);

/*$resultContent = "<table border=\"1\" width=\"300\" bordercolorlight=\"#FBFDEA\" bordercolordark=\"#B3B692\" cellpadding=\"2\" cellspacing=\"0\" bgcolor=\"#FBFDEA\">";
$resultContent .= "<tr class=admin_bg_menu><td colspan=\"3\" align=right><input type=button value=' X ' onClick=hideMenu('ToolMenu2')></td></tr>";
$resultContent .= "<tr class=tableTitle><td width=1>#</td><td width=50%>$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type</td><td width=50%>$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity</td></tr>";
*/

if (sizeof($temp)==0)
	$resultContent .= "<tr class=\"tableorangerow1\"><td colspan=3 align=center class=\"tabletext\">$i_SmartCard_Payment_PhotoCopier_Details_Transaction_No_Record</td></tr>";
else
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($paperType, $noOfPaper) = $temp[$i];
		$no = $i+1;
		$displayPaperType = "";
		switch($paperType)
		{
			case a4_bw:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_bw;
				break;
			case nona4_bw:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_bw;
				break;
			case a4_color:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_color;
				break;
			case nona4_color:
				$displayPaperType = $i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_color;
				break;
		}

		$resultContent .= "<tr class=\"tableorangerow1\"><td class=\"tabletext\">$displayPaperType</td><td class=\"tabletext\">$noOfPaper</td></tr>";
	}
}
//$resultContent .= "<tr><td colspan=\"3\" align=right>&nbsp;</td></tr>";

//$resultContent .= "</table>";

$displayImgPath = $image_path."/".$LAYOUT_SKIN;
$css_template_path = "/templates/".$LAYOUT_SKIN."/css";

$templateContent = str_replace('<!--result-->', $resultContent, $templateContent);
$templateContent = str_replace('<!--imagePath-->', $displayImgPath , $templateContent);
$templateContent = str_replace('<!--Template_path-->', $css_template_path , $templateContent);

$response = iconv("Big5","UTF-8",$templateContent);
echo $response;

intranet_closedb();
?>
