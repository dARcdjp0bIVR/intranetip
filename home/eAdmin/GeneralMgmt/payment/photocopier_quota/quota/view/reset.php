<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lidb = new libdb();

if(is_array($user_id))
{
	$user_list = "'".implode("','", $user_id)."'";
	
	$sql = "SELECT UserID FROM PAYMENT_PRINTING_QUOTA WHERE UserID IN ($user_list)";
	$record_exist = $lidb->returnVector($sql);
	
	if(sizeof($record_exist) > 0)
	{
		$namefield = getNameFieldWithClassNumberByLang("a.");
		
		foreach ($user_id AS $key => $tUserID)
		{
			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = (TotalQuota - UsedQuota), UsedQuota = 0 WHERE UserID = '$tUserID'";
			$result = $lidb->db_db_query($sql);
			if($result == 1)
				$x = 2;
			else
				$x = 12;
		}
	}
	else
	{
		$x = 12;
	}
}
else
{
	$x = 12;
}

header("Location: index.php?msg=".$x);
intranet_closedb();
?>