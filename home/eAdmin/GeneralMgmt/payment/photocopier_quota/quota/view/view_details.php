<?php
// Editing by 
/*
 * 2014-04-10 (Carlos): Changed $i_SmartCard_Payment_PhotoCopier_FinalQuota to $Lang['ePayment']['QuotaLeft']
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_QuotaManagement";

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_terminal_log_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_terminal_log_page_number!="")
{
	$pageNo = $ck_data_log_browsing_terminal_log_page_number;
}

if ($ck_data_log_browsing_terminal_log_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_terminal_log_page_order!="")
{
	$order = $ck_data_log_browsing_terminal_log_page_order;
}

if ($ck_data_log_browsing_terminal_log_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_terminal_log_page_field!="")
{
	$field = $ck_data_log_browsing_terminal_log_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
*/
$u_id = IntegerSafe($u_id);

$sql = "SELECT ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE UserID='$u_id'";
$nameRecord = $lpayment->returnVector($sql);
$username = $nameRecord[0];

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3) $RecordStatus = 1;



	$sql  =  "select a.DateModified, ";
//	$sql .=  "a.QuotaChange, ";
	$sql .=  "if (a.QuotaChange=0,'--',if(a.RecordType=3,concat(' ',a.QuotaChange, concat('<a class=\"tran_Details\" onMouseMove=\"overhere(\'a\');setToolTip(false);\" href=\'javascript:retrieveTransactionInfo(',a.recordid,')\'><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" border=\'0\' alt=\'$button_select\'></a>')),a.QuotaChange)), ";
	$sql .=  "a.QuotaAfter, ";
	$sql .=  "CASE a.RecordType ";
	$sql .=  "WHEN 1 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin."' ";
	$sql .=  "WHEN 2 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin."' ";
	$sql .=  "WHEN 3 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction."' ";
	$sql .=  "ELSE '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing."' END ";
	$sql .= "from ";
	$sql .= "PAYMENT_PRINTING_QUOTA_CHANGE_LOG a ";
	$sql .= "where ";
	$sql .= "a.UserID = '".$u_id."' ";
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateModified","a.QuotaChange","a.QuotaAfter","a.RecordType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletoplink'>#</td>\n";
$li->column_list .= "<td width='30%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_LastestTransactionTime)."</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_ChangeQuota)."</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $Lang['ePayment']['QuotaLeft']/* $i_SmartCard_Payment_PhotoCopier_FinalQuota */)."</td>\n";
$li->column_list .= "<td width='29%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_SmartCard_Payment_PhotoCopier_ChangeRecordType)."</td>\n";


$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
//$searchbar = "";

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);

//$TAGS_OBJ[] = array($i_Payment_Menu_Browse_TerminalLog, "", 0);
$TAGS_OBJ[] = array($button_view, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/view/index.php", 1);
$TAGS_OBJ[] = array($button_add, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/add/add.php", 0);
$TAGS_OBJ[] = array($button_reset, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/reset/reset.php", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_view,$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/view/index.php");
$PAGE_NAVIGATION[] = array($username,'');

?>
<br />

<!--AJAX JS SETUP -->
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
         #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
		 .tran_Details{text-align:right;}
</style>
<script language="JavaScript">
isToolTip = false;
function setToolTip(value)
{
	isToolTip=value;
}

isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
<div id="ToolMenu2"></div>
<script>
<!--AJAX JS END SETUP -->
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveTransactionInfo(transactionID)
{
        //FormObject.testing.value = 1;

        obj = document.form2;

        var myElement = document.getElementById("ToolMenu2");

        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>&nbsp;</td></tr></table>");
        YAHOO.util.Connect.setForm(obj);

        var path = "recordDetails.php?transactionID=" + transactionID;

        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);

}

function openPrintPage()
{
	newWindow('view_details_print.php?StudentID=<?=$u_id?>&keyword=<?=$keyword?>',10);
}
</SCRIPT>
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$searchbar?></td>
	</tr>
</table>
<?php echo $li->displayFormat2("96%", "blue"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="u_id" value="<?=$u_id?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
