<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_QuotaManagement";

$linterface = new interface_html();

$lidb = new libdb();

if(isset($user_id))
{
	$user_list = "'".implode("','", $user_id)."'";
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE a.UserID IN ($user_list) AND a.RecordStatus IN (0,1,2) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql,4);
	
	$table_content = "";
	
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $name, $total_quota, $used_quota) = $result[$i];
			$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
			$table_content .= "<input type=\"hidden\" name=\"user_id[]\" value=\"$uid\">";
			$table_content .= "<tr class=\"$css\">";
			$table_content .= "<td class=\"tabletext\">$name</td>";
			$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"total_quota_$uid\" value=\"". ($total_quota == "" ? 0 : $total_quota) ."\"></td>";
			$table_content .= "<td class=\"tabletext\">". ($used_quota == "" ? 0 : $used_quota) ."</td></tr>";
		}
	}
}

$TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Quota_Management, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit);
?>

<script language="JavaScript">
function checkQuota()
{
	var cnt = 0;
	<?
	for($i=0; $i<sizeof($result); $i++)
	{
		list($uid, $name, $total_quota, $used_quota) = $result[$i];
	?>
		var element = eval("document.form1.total_quota_"+<?=$uid?>);
		if (element.value < <?=$used_quota?>)
		{
			cnt = 1;
		}
	<?
	}
	?>
	if(cnt==1)
	{
		alert ("<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>");
		return false;
	}
	else
	{
		return true;
	}
	
}
</script>
<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkQuota()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr class="tabletop">
					<td class="tabletoplink"><?=$i_UserName?></td>
					<td class="tabletoplink"><?=$i_SmartCard_Payment_PhotoCopier_TotalQuota?></td>
					<td class="tabletoplink"><?=$i_SmartCard_Payment_PhotoCopier_UsedQuota?></td>
				</tr>
				<?=$table_content?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				 <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>