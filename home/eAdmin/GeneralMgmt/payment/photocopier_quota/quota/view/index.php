<?php
// Editing by 
/*
 * 2014-04-10 (Carlos): Changed $i_SmartCard_Payment_PhotoCopier_TotalQuota to $Lang['ePayment']['FinalQuota']
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_QuotaManagement";

$linterface = new interface_html();

$lidb = new libdb();
$lclass = new libclass();

### User Type Selection ###
$usertype = array(
	array(1,$i_identity_teachstaff),
	array(2,$i_identity_student),
);
$select_userType = getSelectByArray($usertype,"name=targetUserType onChange=\"generalFormSubmitCheck(this.form)\"",$targetUserType,1);

### Teacher Selection ###
$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT a.UserID, $namefield FROM INTRANET_USER AS a WHERE a.RecordType = 1 AND a.RecordStatus=1 ORDER BY a.EnglishName";
$temp = $lidb->returnArray($sql,2);
$select_teacher = getSelectByArray($temp,"name=targetTeacher",$targetTeacher,1);

### Class & Student Selection ###
$select_class = $lclass->getSelectClass("name=targetClass onChange=\"changeClass(this.form)\"",$targetClass);
if ($targetClass != "")
{
    $student_list = $lclass->returnStudentListByClass($targetClass);
    $select_students = getSelectByArray($student_list,"name=targetStudent",$targetStudent,1);
}


if ($keyword!="")
{
	$search_cond = "AND (a.EnglishName LIKE '%$keyword%' OR
               			 a.ChineseName LIKE '%$keyword%' OR
               			 a.ClassName LIKE '%$keyword%' OR
               			 a.ClassNumber LIKE '%$keyword%'
               			 ) ";
}
else
{
	$search_cond = " ";	
}

### Get Data From Database For All Users ###
if($targetUserType == "" && $flag == 3)
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $search_cond OR a.RecordType = 1 AND a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql, 4);
}

### Get Data From Database For Teachers ###
if($targetUserType == 1 && $flag == 3)
{
	if($targetTeacher == "")
	{
		$cond = " a.RecordType = 1 AND ";
	}
	else
	{
		$cond = " a.RecordType = 1 AND a.UserID = '$targetTeacher' AND ";
	}
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql, 4);
}

### Get Data From Database For Students ###
if($targetUserType == 2 && $flag == 3)
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	if(($targetClass != "") && ($targetStudent == ""))
	{
		$cond = " a.RecordType = 2 AND a.ClassName = '$targetClass' AND ";
		$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	}
	if(($targetClass != "") && ($targetStudent != ""))
	{
		$cond = " a.RecordType = 2 AND a.ClassName = '$targetClass' AND a.UserID = '$targetStudent' AND ";
		$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE $cond a.RecordStatus IN (0,1,2) $search_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	}
	$result = $lidb->returnArray($sql, 4);
}
	
$table_content = "";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($user_id, $user_name, $total_quota, $used_quota) = $result[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href =\"view_details.php?u_id=".$user_id."\">$user_name</a></td>";
		$displayTotalQuota = ($total_quota != "") ? $total_quota : 0;
		$displayUsedQuota = ($used_quota != "") ?$used_quota:0;

		$table_content .= "<td class=\"tabletext\">".$displayTotalQuota."</td>";
		$table_content .= "<td class=\"tabletext\">".$displayUsedQuota."</td>";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"user_id[]\" value=\"$user_id\"></td>";
		$table_content .= "</tr>";
		
	}
}

if(sizeof($result) <= 0)
{
	$table_content .= "<tr class=\"tablerow1 tabletext\">";
	$table_content .= "<td class=\"tabletext\" colspan=\"4\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

$table_tool = "";


$table_tool .= "<td nowrap=\"nowrap\">";
$table_tool .= "<a href=\"javascript:checkEdit(document.form1,'user_id[]','edit.php')\" class=\"tabletool\">";
$table_tool .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">";
$table_tool .= $button_edit;
$table_tool .= "</a>";
$table_tool .= "</td>";


$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

## $TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Quota_Management, "", 0);

$TAGS_OBJ[] = array($button_view, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/view/index.php", 1);
$TAGS_OBJ[] = array($button_add, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/add/add.php", 0);
$TAGS_OBJ[] = array($button_reset, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/reset/reset.php", 0);


$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$PAGE_NAVIGATION[] = array($button_view);

if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript">
function checkEdit(obj,element,page){
        if(countChecked(obj,element)>=1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
}
function generalFormSubmitCheck(obj)
{
		 if (obj.targetUserType.selectedIndex == 2){
				obj.flag.value = 2;		 
			}
         obj.submit();
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetStudent != undefined)
             obj.targetStudent.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function checkForm()
{
	if(document.form1.targetUserType.selectedIndex == 2)
	{
		if(document.form1.targetClass.selectedIndex == 0)
		{
			alert ("<?=$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction?>");
			document.form1.flag.value = 0;
			return false;
		}
	}		
	document.form1.flag.value = 3;
	return true
}
</script>
<br />
<form name="form1" action="" method="post" onsubmit="return checkForm();">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td colspan="2" align="right"><?=$SysMsg?></td></tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_identity?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_userType?>
	    			</td>
    			</tr>
<?
	if($targetUserType == 1) {
?>
				<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_teacher?>
	    			</td>
    			</tr>
<?
	}
	if($targetUserType == 2) {
?>
				<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_ClassName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_class?>
	    			</td>
    			</tr>
<?
		if($targetClass != "") {
?>
				<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserStudentName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_students?>
	    			</td>
    			</tr>
<?
		}
	}
?>
				<tr>
                	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
				    <td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
					</td>
				</tr>
    		</table>
    	</td>
	</tr>
</table>
<br />
<input type="hidden" name="flag" value="0">
<?
	if($flag == 3) {
?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
							<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tablebluetop">
		<td class="tabletoplink"><?=$i_UserName?></td>
		<td class="tabletoplink"><?=$Lang['ePayment']['FinalQuota']/*$i_SmartCard_Payment_PhotoCopier_TotalQuota*/?></td>
		<td class="tabletoplink"><?=$i_SmartCard_Payment_PhotoCopier_UsedQuota?></td>
		<td class="tabletoplink"><input type="checkbox" onClick="(this.checked)?setChecked(1,this.form,'user_id[]'):setChecked(0,this.form,'user_id[]')"></td>
	</tr>
	<?=$table_content?>
</table>
<?
	}
?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
if($targetUserType == 1)
	print $linterface->FOCUS_ON_LOAD("form1.targetTeacher");
else if($targetUserType == 2 && $targetClass == "")
	print $linterface->FOCUS_ON_LOAD("form1.targetClass");
else if ($targetClass != "")
	print $linterface->FOCUS_ON_LOAD("form1.targetStudent");
else
	print $linterface->FOCUS_ON_LOAD("form1.targetUserType");

intranet_closedb();
?>