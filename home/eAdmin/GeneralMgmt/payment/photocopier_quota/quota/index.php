<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_QuotaManagement";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Quota_Management, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_view, "view/");
?>
<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
	    			<td class="tabletext" align="left">
	    				<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	    			</td>
    			</tr>
    			<tr>
	    			<td class="tabletext" align="left">
	    				<a class="tablelink" href="add/add.php" ><?=$button_add?></a>
	    			</td>
    			</tr>
    			<tr>
	    			<td class="tabletext" align="left">
	    				<a class="tablelink" href="reset/reset.php" ><?=$button_reset?></a>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
?>