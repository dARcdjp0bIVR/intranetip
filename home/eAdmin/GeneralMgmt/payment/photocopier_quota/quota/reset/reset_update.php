<?
// Editing by 
/*
 * 2014-03-28 (Carlos): Added PAYMENT_PRINTING_QUOTA.ModifiedBy and PAYMENT_PRINTING_QUOTA_CHANGE_LOG.ModifiedBy for trace purpose
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
	
intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$lidb = new libdb();

$Result = array();
$lidb->Start_Trans();
if($add_type==0)
{
	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2)";
	$temp = $lidb->returnVector($sql);
	$user_list = "'".implode("','",$temp)."'";
	
	$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified, ModifiedBy) SELECT UserID, 0, 0, '', '', NOW(), NOW(),'".$_SESSION['UserID']."' FROM INTRANET_USER WHERE UserID IN ($user_list)";
	$Result['InsertPaymentQuota'] = $lidb->db_db_query($sql);
	
	$updatePrintingQuotaLogCondition = "where userID in (".$user_list.")";
	//$Result['UpdatePaymentQuotaLog'] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,2,$updatePrintingQuotaLogCondition,$lidb);	


	//$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = $total_quota, UsedQuota = 0, DateModified = NOW() WHERE UserID IN ($user_list)";
	if($reset_type == 0)
		$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = '$total_quota', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN ($user_list)";
	else if($reset_type == 1)
		$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = '$total_quota', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN ($user_list)";
	$Result['UpdatePaymentQuotaLog'] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,2,$updatePrintingQuotaLogCondition,$lidb,$reset_type);	
	$Result['UpdatePaymentQuota'] = $lidb->db_db_query($sql);
}

if($add_type==1)
{
	if(sizeof($LevelID)>0)
	{
		for($i=0; $i<sizeof($LevelID); $i++)
		{
			if ($intranet_version == "2.5") {
				$sql = "SELECT UserID 
								FROM 
									INTRANET_USER AS a 
									INNER JOIN 
									YEAR_CLASS as yc 
									on a.ClassName = yc.ClassTitleEN 
									INNER JOIN 
									YEAR as y 
									on yc.YearID = y.YearID 
								WHERE 
									a.RecordType = 2 
									AND a.RecordStatus IN (0,1,2) 
									AND y.YearID = '".$LevelID[$i]."'";
			}
			else {
				$sql = "SELECT UserID 
								FROM 
									INTRANET_USER AS a 
									INNER JOIN 
									INTRANET_CLASS as yc 
									on a.ClassName = yc.ClassName 
									INNER JOIN 
									INTRANET_CLASSLEVEL as y 
									on yc.ClassLevelID = y.ClassLevelID 
								WHERE 
									a.RecordType = 2 
									AND a.RecordStatus IN (0,1,2) 
									AND y.ClassLevelID = '".$LevelID[$i]."'";
			}
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$LevelID[$i]] = $user_list; ### Update List ###
		}
		
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($LevelID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified, ModifiedBy) SELECT UserID, 0, 0, '', '', Now(), Now(),'".$_SESSION['UserID']."' FROM INTRANET_USER WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
				$Result['InserQuota'.$i] = $lidb->db_db_query($sql);
			}
		}
	}
	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_".$LevelID[$i]};

			$updatePrintingQuotaLogCondition = "where userID in (".$user_arr[$LevelID[$i]].")";
			//$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb);	

			//$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = $totalQuota, UsedQuota = 0, DateModified = NOW() WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
			if($reset_type == 0)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = $totalQuota, DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
			else if($reset_type == 1)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = $totalQuota, DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
			$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb,$reset_type);	
			$Result['UpdateQuota'.$i] = $lidb->db_db_query($sql);
		}
	}
}

if($add_type==2)
{
	if(sizeof($ClassID)>0)
	{
		for($i=0; $i<sizeof($ClassID); $i++)
		{
			if ($intranet_version == "2.5") {
				$sql = "SELECT 
									a.UserID 
								FROM 
									INTRANET_USER AS a 
									INNER JOIN 
									YEAR_CLASS AS b 
									ON (a.ClassName = b.ClassTitleEN) 
								WHERE 
									b.YearClassID = '".$ClassID[$i]."' 
									AND 
									a.RecordType IN (1,2) 
									AND 
									a.RecordStatus IN (0,1,2)";
			}
			else {
				$sql = "SELECT 
									a.UserID 
								FROM 
									INTRANET_USER AS a 
									INNER JOIN 
									INTRANET_CLASS AS b 
									ON (a.ClassName = b.ClassName) 
								WHERE 
									b.ClassID = '".$ClassID[$i]."' 
									AND 
									a.RecordType IN (1,2) 
									AND 
									a.RecordStatus IN (0,1,2)";
			}
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$ClassID[$i]] = $user_list; ### Update List ###
		}
		
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($ClassID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified, ModifiedBy) SELECT UserID, 0, 0, '', '', Now(), Now(), '".$_SESSION['UserID']."' FROM INTRANET_USER WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
				$Result['InserQuota'.$i] = $lidb->db_db_query($sql);
			}
		}
	}
	
	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_$ClassID[$i]"};

			$updatePrintingQuotaLogCondition = "where userID in (".$user_arr[$ClassID[$i]].")";
			//$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb);	

			//$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = $totalQuota, UsedQuota = 0, DateModified = NOW() WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
			if($reset_type == 0)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = '$totalQuota', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
			else if($reset_type == 1)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = '$totalQuota', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
			$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb,$reset_type);	
			$Result['UpdateQuota'.$i] = $lidb->db_db_query($sql);
		}
	}
}

if($add_type==3)
{
	if(sizeof($TypeID) > 0)
	{
		for($i=0; $i<sizeof($TypeID); $i++)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = '".$TypeID[$i]."' AND RecordStatus IN (0,1,2)";
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$TypeID[$i]] = $user_list;
		}
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($TypeID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified, ModifiedBy) SELECT UserID, 0, 0, '', '', Now(), Now(), '".$_SESSION['UserID']."' FROM INTRANET_USER WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
				$Result['InserQuota'.$i] = $lidb->db_db_query($sql);
			}
		}
	}

	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_$TypeID[$i]"};

			$updatePrintingQuotaLogCondition = "where userID in (".$user_arr[$TypeID[$i]].")";
			//$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb);	

			//$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = $totalQuota, UsedQuota = 0, DateModified = NOW() WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
			if($reset_type == 0)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = '$totalQuota', DateModified = NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
			else if($reset_type == 1)
				$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET UsedQuota = '$totalQuota', DateModified = NOW(),ModifiedBy='".$_SESSION['UserID']."' WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
			$Result['UpdatePaymentQuotaLog'.$i] = $lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,2,$updatePrintingQuotaLogCondition,$lidb,$reset_type);	
			$Result['UpdateQuota'.$i] = $lidb->db_db_query($sql);
		}
	}
}

if (in_array(false,$Result)) {
	$lidb->RollBack_Trans();
	$Msg = $Lang['ePayment']['PrintQuotaResetFail'];
}
else {
	$lidb->Commit_Trans();
	$Msg = $Lang['ePayment']['PrintQuotaResetSuccess'];
}

header ("Location: reset.php?reset_type=".$reset_type."&targetType=".$targetType."&Msg=".urlencode($Msg));
intranet_closedb();
?>