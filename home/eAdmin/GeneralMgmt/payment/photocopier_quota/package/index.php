<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_PackageSettings";

$linterface = new interface_html();

$li = new libdb();

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('insert.php')","","","","",0);


$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'record_id[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'record_id[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";


$table_content = "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletoplink\" width=\"1\">#</td>";
$table_content .= "<td class=\"tabletoplink\">$i_Payment_Field_PaymentItem</td>";
$table_content .= "<td class=\"tabletoplink\">$i_Payment_Field_Quota</td>";
$table_content .= "<td class=\"tabletoplink\">$i_Payment_Field_Amount</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"1\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'record_id[]'):setChecked(0,this.form,'record_id[]')\"></td>";
$table_content .= "</tr>";


$sql = "SELECT
				RecordID, NumOfQuota, CONCAT('$', FORMAT(Price, 1)) AS Amount, PurchasingItemName, RecordType 
		FROM
				PAYMENT_PRINTING_PACKAGE
		ORDER BY
				NumOfQuota, Price
		";

$temp = $li->ReturnArray($sql,5);

if(sizeof($temp) > 0)
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		$j++;
		list($record_id, $num_of_quota, $price, $item_name, $record_type) = $temp[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">$j</td>\n";
		$table_content .= "<td class=\"tabletext\">$item_name</td>\n";
		$table_content .= "<td class=\"tabletext\">$num_of_quota</td>\n";
		$table_content .= "<td class=\"tabletext\">$price</td>\n";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"record_id[]\" value=\"$record_id\"></td>";
		$table_content .= "</tr>";
	}
}
else
{
	$table_content .= "<tr class=\"tablerow2 tabletext\">";
	$table_content .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td>";
	$table_content .= "</tr>\n";
}

$TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Package_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

// return message
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == "1") $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == "3") $SysMsg = $linterface->GET_SYS_MSG("delete");
?>

<script language="JavaScript">
function checkEdit(obj,element,page){
        if(countChecked(obj,element)>=1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
}
function checkRemove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg3)){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
</script>
<br />
<form name="form1" action="" method="post">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><?= $toolbar ?></td>
	</tr>
	<tr class="table-action-bar">
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
