<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$li = new libdb();

$Result = array();
$li->Start_Trans();
if(sizeof($record_id) > 0)
{
	for($i=0; $i<sizeof($record_id); $i++)
	{
		list($r_id) = $record_id[$i];
		$sql = "DELETE FROM PAYMENT_PRINTING_PACKAGE WHERE RecordID = '$r_id'";
		$Result[] = $li->db_db_query($sql);
	}
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PhotocopyPackageDeleteFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PhotocopyPackageDeleteSuccess'];
}

header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>