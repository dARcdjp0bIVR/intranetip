<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_PackageSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Package_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);

?>

<script language="Javascript">
function checkForm()
{
	var obj = document.form1;
	if(obj.item_name.value != '')
	{
		if(obj.num_of_quota.value != '')
		{
			if(obj.price.value != '')
			{
				return ture;
			}
			else
			{
				alert ('<?=$i_SmartCard_Payment_Input_Amount_Instruction?>');
				return false;
			}
		}
		else
		{
			alert ('<?=$i_SmartCard_Payment_Input_Quota_Instruction?>');
			return false;
		}
	}
	else
	{
		alert ('<?=$i_SmartCard_Payment_Input_PaymentItem_Instruction?>');
		return false;
	}
}
</script>
<br />
<form name="form1" action="insert_update.php" method="post" OnSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentItem?> <span class="tabletextrequire">*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="item_name">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_Quota?> <span class="tabletextrequire">*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="num_of_quota">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_Amount?> <span class="tabletextrequire">*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="price">
	    			</td>
    			</tr>
    			<tr>
    				<td colspan="2" valign="top" nowrap="nowrap" class="tabletextremark">
	    				<br /><?=$i_general_required_field2?>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.item_name");
intranet_closedb();
?>