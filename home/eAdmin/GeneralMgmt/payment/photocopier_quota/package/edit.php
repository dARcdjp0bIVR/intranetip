<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_PackageSettings";

$linterface = new interface_html();

$li = new libdb();

$table_content = "";

if(sizeof($record_id) > 0)
{
	$table_content = "";
	
	$edit_list = "'".implode("','",$record_id)."'";
	
	$sql = "SELECT
					RecordID, NumOfQuota, Price, PurchasingItemName, RecordType 
			FROM
					PAYMENT_PRINTING_PACKAGE
			WHERE
					RecordID IN ($edit_list)
			ORDER BY
					NumOfQuota, Price
			";
			
	$temp = $li->returnArray($sql,5);
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($r_id, $num_of_quota, $price, $name, $record_type) = $temp[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$j++;
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">$j</td><td class=\"tabletext\">$name</td>";
		$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"num_of_quota_$r_id\" value=\"$num_of_quota\"></td>";
		$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"price_$r_id\" value=\"$price\"></td></tr>";
		$table_content .= "<input type=\"hidden\" name=\"edit_list[]\" value=\"$r_id\">";
	}
}

$TAGS_OBJ[] = array($i_Payment_Menu_PhotoCopier_Package_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit);
?>

<script language="JavaScript">
function checkForm()
{
	var cnt_1 = 0;
	var cnt_2 = 0;
	var obj = document.form1;
	<?
	for($i=0; $i<sizeof($record_id); $i++)
	{
	?>
		temp_num_of_quota = eval("obj.num_of_quota_"+<?=$record_id[$i]?>+".value");
		temp_price = eval("obj.price_"+<?=$record_id[$i]?>+".value");
		if(temp_num_of_quota != "")
		{
			if(temp_price != "")
			{
				if(!isNaN(temp_num_of_quota))
				{
					if(temp_num_of_quota >= 0)
					{
						cnt_1=cnt_1+1;
					}
					else
					{
						alert("Invalid quota");
					}
				}
				else
				{
					alert("Invalid value");
				}
				if(!isNaN(temp_price))
				{
					if(temp_price >= 0)
					{
						cnt_2=cnt_2+1;
					}
					else
					{
						alert("Invalid Price");
					}
				}
				else
				{
					alert("Invalid Price");
				}
			}
			else
			{
				alert ("Price Empty");
			}
		}
		else
		{
			alert ("Quota Empty");
		}
	<?
	}
	?>
	if((cnt_1 == <?=sizeof($record_id);?>)&&(cnt_2 == <?=sizeof($record_id);?>))
	{
		return true;
	}
	else
	{
		return false;
	}
}
</script>
<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr class="tabletop">
    				<td class="tabletoplink">#</td>
    				<td class="tabletoplink"><?=$i_Payment_Field_PaymentItem?></td>
    				<td class="tabletoplink"><?=$i_Payment_Field_Quota?></td>
    				<td class="tabletoplink"><?=$i_Payment_Field_Amount?></td>
    			</tr>
    			<?=$table_content?>
    			<tr class="tablebottom"><td colspan="4">&nbsp;</td></tr>
    		</table>
    	</td>
    <tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.num_of_quota_".$record_id[0]);
intranet_closedb();
?>