<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$li = new libdb();

$sql = "INSERT INTO 
					PAYMENT_PRINTING_PACKAGE
		VALUES 
					('', $num_of_quota, $price, '$item_name', '', '', NOW(), NOW())
		";

if ($li->db_db_query($sql)) {
	$Msg = $Lang['ePayment']['PhotocopyPackageCreateSuccess'];
}
else {
	$Msg = $Lang['ePayment']['PhotocopyPackageCreateFail'];
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>