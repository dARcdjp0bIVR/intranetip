<?php
// Editing by 
/*
 * 2019-08-12 Carlos: Added HandlingFee.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
//$lpayment = new libpayment();

$userObj = new libuser($_SESSION['UserID']);
$is_broadlearning_account = ($userObj->UserLogin == 'broadlearning') ? true : false;


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_MerchantAccount";

$flashData = array();
if(isset($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA']);
}

$service_provider_map = $lpayment->getPaymentServiceProviderMapping();
$service_provider_list = $lpayment->getPaymentServiceProviderMapping(true,true);

$is_edit = false;
$current_quota = '';
if(isset($AccountID)){
	if(is_array($AccountID)) $AccountID = $AccountID[0];
	
	$records = $lpayment->getMerchantAccounts(array('AccountID'=>$AccountID));
	if(count($records)>0){
		$hidden_fields = '<input type="hidden" id="AccountID" name="AccountID" value="'.$AccountID.'" />';
		$is_edit = true;
		$record = $records[0];

		if($record['ServiceProvider'] == 'FPS') {
			$year = date('Y');
			$month = date('m');
			$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='$AccountID'";
			$rs_quota = $lpayment->returnArray($sql);
			if($rs_quota) {
				$current_quota = $rs_quota[0]['Quota'];
			}
		}
	}else{
		$record = array('ServiceProvider'=>'ALIPAY');
	}
}else{
	$record = array('ServiceProvider'=>'ALIPAY');
	if(count($flashData)>0){
		foreach($flashData as $key => $val){
			$record[$key] = $val;
		}
	}
}

if ($is_broadlearning_account) {
	$service_provider_selection = $linterface->GET_SELECTION_BOX($service_provider_list, ' id="ServiceProvider" name="ServiceProvider" ', '', $record['ServiceProvider'], false);
} else {
	$service_provider_selection = $record['ServiceProvider'];
	$service_provider_selection .= '<input type="hidden" id="ServiceProvider" name="ServiceProvider" value="'.$record['ServiceProvider'].'"/>';
}


$pages_arr = array(
	array($Lang['ePayment']['MerchantAccount'],'index.php'),
	array($AccountID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['MerchantAccount'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<br />
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form id="form1" name="form1" method="post" action="update.php" onsubmit="return false;">
<?=$hidden_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['ServiceProvider']?></td>
		<td>
			<?=$service_provider_selection?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['MerchantAccountName']?><span class="tabletextrequire">*</span></td>
		<td>
			<?=$linterface->GET_TEXTBOX_NAME("AccountName", "AccountName", $record['AccountName'], 'required', array('maxlength'=>255))?>&nbsp;<span id="Error_AccountName" class="red error"><?=$flashData['Error_AccountName']?></span>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['MerchantAccount']?><span class="tabletextrequire">*</span></td>
		<td>
            <?php if($is_broadlearning_account) { ?>
			<?=$linterface->GET_TEXTBOX_NAME("MerchantAccount", "MerchantAccount", $record['MerchantAccount'], 'required', array('maxlength'=>255))?>&nbsp;<span id="Error_MerchantAccount" class="red error"><?=$flashData['Error_MerchantAccount']?></span>
		    <?php } else { ?>
            <?=$record['MerchantAccount']?>
            <?php } ?>
        </td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['MerchantUID']?><span class="tabletextrequire">*</span></td>
		<td>
			<?php if($is_broadlearning_account) { ?>
			<?=$linterface->GET_TEXTBOX_NAME("MerchantUID", "MerchantUID", $record['MerchantUID'], 'required', array('maxlength'=>255))?>&nbsp;<span id="Error_MerchantUID" class="red error"><?=$flashData['Error_MerchantUID']?></span>
			<?php } else { ?>
				<?=$record['MerchantUID']?>
			<?php } ?>
        </td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['HandlingFee']?></td>
		<td>
			<?=$lpayment->getWebDisplayAmountFormat('').$linterface->GET_TEXTBOX_NUMBER("HandlingFee", "HandlingFee", $record['HandlingFee'], '', array('maxlength'=>10,'onchange'=>'handlingFeeChanges(this);'))?>&nbsp;<span id="Error_HandlingFee" class="red error"><?=$flashData['Error_HandlingFee']?></span>
		</td>
	</tr>

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    <tr class="fps_div">
        <td class="field_title"><?=$Lang['ePayment']['FPSMonthlyWaiveCount']?></td>
        <td>
			<?=$linterface->GET_TEXTBOX_NAME("FPSMonthlyWaiveCount", "FPSMonthlyWaiveCount", $current_quota, '',  array('maxlength'=>'10', 'style'=>"width: 80px;",'onchange'=>'handlingQuotaChanges(this)')) ?> <?=$i_Merit_Unit?>

            <?php if($is_edit) { ?>
				<?=$linterface->Get_Thickbox_Link(480, 750,  'new', $Lang['ePayment']['ManageQuota'], 'showModalForm('.$AccountID.')', $InlineID="FakeLayer", $Lang['ePayment']['ManageQuota']);?>
            <?php } ?>
        </td>
    </tr>

    <tr>
        <td class="field_title"><?=$Lang['ePayment']['AdministrativeFee']?></td>
        <td>
            <?php
            $fix_amount_checked = false;
            $percentage_amount_checked = false;
            if($record['ServiceProvider'] == 'FPS') {
				$fix_amount_checked = true;
			} else {
                if($record['AdministrativeFeePercentageAmount']!='' && $record['AdministrativeFeePercentageAmount']!='0') {
					$percentage_amount_checked = true;
				} else {
					$fix_amount_checked = true;
				}
			}

            ?>
            <span class="">
				<?=$linterface->Get_Radio_Button("AdministrativeFeeFixAmount", "AdministrativeFee", "0", $fix_amount_checked, '', '', 'checkAdministrativeFee(0)')?>
				$ <?=$linterface->GET_TEXTBOX_NAME("AdministrativeFeeFixAmountValue", "AdministrativeFeeFixAmount", $record['AdministrativeFeeFixAmount'], '',  array('maxlength'=>'5', 'style'=>"width: 50px;",'onchange'=>'handlingFeeChanges(this)')) ?>
            </span>
            <span class="non_fps_div">
				<?=$linterface->Get_Radio_Button("AdministrativeFeePercentageAmount", "AdministrativeFee", "1", $percentage_amount_checked, '', '', 'checkAdministrativeFee(1)')?>
				<?=$linterface->GET_TEXTBOX_NAME("AdministrativeFeePercentageAmountValue", "AdministrativeFeePercentageAmount", $record['AdministrativeFeePercentageAmount'], '',  array('maxlength'=>'4', 'style'=>"width: 50px;",'onchange'=>'handlingFeeChanges(this)')) ?> %
            </span>

        </td>
    </tr>
    <?php } ?>

	<tr>
		<td class="field_title"><?=$Lang['General']['Status']?></td>
		<td>
			<?=$linterface->Get_Radio_Button("RecordStatusActive", "RecordStatus", "1", in_array($record['RecordStatus'],array('1',1,'')), $___Class="", $i_general_active).'&nbsp;'?>
			<?=$linterface->Get_Radio_Button("RecordStatusInactive", "RecordStatus", "0", $record['RecordStatus']=='0', $___Class="", $i_general_inactive)?>
		</td>
	</tr>
</table>
<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php'",'cancel_btn')?>
</div>
</form>	
<br /><br />
<script type="text/javascript">
function QuotaYearChange(obj, id) {
    $.post(
        'ajax.php',
        {
            'task':'getQuotaDate',
            'AccountID': id,
            'Year': $(obj).val()
        },
        function(returnHtml){
            $("div#TB_ajaxContent2").html(returnHtml);
        }
    );
}

function showModalForm(id)
{
    tb_show('<?=$Lang['ePayment']['ManageQuota']?>',"#TB_inline?height=480&width=750&inlineId=FakeLayer");
    $.post(
        'ajax.php',
        {
            'task':'getModalForm',
            'AccountID': id,
        },
        function(returnHtml){
            $("div#TB_ajaxContent").html(returnHtml);
            QuotaYearChange($("#modalForm select[name='Year']"), id);
        }
    );
}

$(document).ready(function(){

    $("#ServiceProvider").change(function() {
        checkServiceProvider(this);
    });

    checkServiceProvider($("#ServiceProvider"));
});

function checkServiceProvider(obj) {
    var value = $(obj).val();
    if(value == "FPS") {
        $(".non_fps_div").hide();
        $(".fps_div").show();
    } else {
        $(".non_fps_div").show();
        $(".fps_div").hide();
    }
}
function checkAdministrativeFee(value) {
    if(value == 0) {
        $("#AdministrativeFeePercentageAmountValue").val("");
    } else {
        $("#AdministrativeFeeFixAmountValue").val("");
    }
}

function handlingFeeChanges(elem)
{
	var value = parseFloat($.trim(elem.value));
	if(isNaN(value) || value < 0.00){
		value = '';
	}
	elem.value = value;
}

function handlingQuotaChanges(elem)
{
    var value = parseInt($.trim(elem.value));
    if(isNaN(value) || value < 0){
        value = '';
    }
    elem.value = value;
}

function checkSubmitForm(formObj)
{
	$('#submit_btn').attr('disabled',true);
	$('.error').html('');
	var valid = true;
	var inputs = $('input.required');
	for(var i=0;i<inputs.length;i++){
		var obj = $(inputs[i]);
		var val = $.trim(obj.val());
		if(val == ''){
			$('#Error_'+obj.attr('id')).html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
			valid = false;
		}
	}
	
	handlingFeeChanges(document.form1['HandlingFee']);

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    handlingQuotaChanges(document.form1['FPSMonthlyWaiveCount']);
    handlingFeeChanges(document.form1['AdministrativeFeeFixAmountValue']);
    handlingFeeChanges(document.form1['AdministrativeFeePercentageAmountValue']);
    <?php } ?>

	if(valid){
		formObj.submit();
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>