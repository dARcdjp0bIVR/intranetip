<?php
// Editing by 
/*
 *  2020-03-03 (Ray): Added UnitCode
 * 2014-05-23 (Carlos): Modified allow input negative subsidy amount
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$select_status = "<SELECT name=Status>\n";
$select_status .= "<OPTION value=1 >$i_general_active</OPTION>";
$select_status .= "<OPTION value='' >$i_general_inactive</OPTION>";
$select_status .= "</SELECT>\n";

$linterface = new interface_html();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($button_new);

$linterface->LAYOUT_START();

?>
<script language='javascript'>
function checkform()
{
    $("#UnitCodeError").html('');
	obj = document.form1;
	if(obj==null) return false;
	
	objName = obj.UnitName;
	objAmount = obj.TotalAmount;
	if(!check_text(objName,'<?=$i_Payment_Subsidy_Warning_Enter_UnitName?>')) return false;
	if(!js_check_numeric(objAmount,'','<?=$i_Payment_Warning_InvalidAmount?>')) return false;

    $("form#form1 input[name='action']").val('check_unitcode');
    var data = $('form#form1').serialize();
    var check_result = false;
    $.ajax({
        url: "ajax_check_subsidy_unit.php",
        type: "POST",
        data: data,
        async: false,
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
        },
        success: function (data) {
            if(data == '1') {
                check_result = true;
            } else {
                $("#UnitCodeError").html(data);
            }
        }
    });

    return check_result;

}

function js_check_numeric(f,d,msg){
	var text_val = f.value.Trim();
	var numeric_val = parseFloat(f.value,10);
    if(text_val=='' || (text_val!='' && isNaN(text_val))){
            alert(msg);
            f.value=d;
            f.focus();
            return false;
    }else{
            return true;
    }
}
</script>

<form id="form1" name="form1" action="new_update.php" method=POST onSubmit='return checkform()'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$lpayment->getResponseMsg($msg)?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Subsidy_UnitName?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%"><input type='text' name='UnitName' size=50  value=''>
	    			</td>
    			</tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_Payment_Subsidy_UnitCode?></span>
                    </td>
                    <td class="tabletext" width="70%"><input type='text' name='UnitCode' size=50  value=''>
                        <div id="UnitCodeError" style="color: #ff0000;"></div>
                    </td>
                </tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Subsidy_Total_Amount?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type='text' class="textboxnum" name='TotalAmount' value=''>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_status?>  
	    			</td>
	    			<td class="tabletext" width="70%">
						<?=$select_status?>		
	    			</td>
    			</tr>
    		</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
    <input type="hidden" name="action" value="" />
</form>
<br />


<?

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
