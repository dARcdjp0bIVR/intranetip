<?php
// Editing by 
/*
 * 2014-03-24 Carlos - Allow negative subsidy balance
 * 2013-10-18 Carlos - Modified to sum subsidy amount from PAYMENT_PAYMENT_ITEM_SUBSIDY
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$lpayment = new libpayment();


if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEM_SUBSIDY AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = round($temp[0],2);
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
//$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_left_amount = round($unit_left_amount,2);
$unit_total_amount = round($unit_total_amount,2);
//$unit_total_amount = number_format($unit_total_amount,2);
//$unit_used_amount  = number_format($unit_used_amount,2);
//$unit_left_amount  = number_format($unit_left_amount,2);

$sql="
	SELECT 
		COUNT(*), ROUND(SUM(s.SubsidyAmount),2)
	FROM 
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON a.PaymentID=s.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = round($temp[0][1],2);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;

$paid_status = $paid_status ? $paid_status : -1;
switch($paid_status)
{
	case 1:
		$status_con = " and a.RecordStatus=1 ";
		break;
	case 2:
		$status_con = " and a.RecordStatus!=1 ";
		break;	
}

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";
$namefield = getNameFieldWithClassNumberByLang("c.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield2 = "f.ChineseName";
}else $archive_namefield2 = "f.EnglishName";
$namefield2 = getNameFieldByLang("e.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('*',$archive_namefield), $namefield),
		b.Name,ROUND(s.SubsidyAmount,2),
		if(s.SubsidyPICAdmin!='', s.SubsidyPICAdmin, IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield2,'</i>'), $namefield2)) as picadmin,
		s.DateModified,
		IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid') as PayStatus
	FROM 
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON a.PaymentID=s.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
		LEFT JOIN INTRANET_USER AS e ON (e.UserID = s.SubsidyPICUserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS f ON (f.UserID = s.SubsidyPICUserID) 
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'
              )	
		$status_con
	";            
# TABLE INFO
$field_array = array("$namefield","b.Name","s.SubsidyAmount","picadmin","s.DateModified","PayStatus");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql,5);



$x="\"$i_Payment_Subsidy_Unit\",\"$unit_name\"\n";
$x.="\"$i_Payment_Subsidy_Total_Amount\",\"$unit_total_amount\"\n";
$x.="\"$i_Payment_Subsidy_Used_Amount\",\"$unit_used_amount\"\n";
$x.="\"$i_Payment_Subsidy_Left_Amount\",\"$unit_left_amount\"\n";
$x.="\"$i_Payment_Subsidy_Search_Amount\",\"$search_amount ($no_of_students $i_Payment_Students)\"\n";



$utf_x=$i_Payment_Subsidy_Unit."\t";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t\r\n";

$utf_x=$i_Payment_Subsidy_Unit."\t".$unit_name."\r\n";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t".$unit_total_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Used_Amount."\t".$unit_used_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Left_Amount."\t".$unit_left_amount."\r\n";
$utf_x.=$i_Payment_Subsidy_Search_Amount."\t"."$search_amount ($no_of_students $i_Payment_Students)"."\r\n";


$x.= "\"$i_UserStudentName\",";
$x.= "\"$i_Payment_Field_PaymentItem\",";
$x.= "\"$i_Payment_Subsidy_Amount\",";
$x.= "\"$i_Payment_Subsidy_Unit_Admin\",";
$x.= "\"$i_Payment_Subsidy_Unit_UpdateTime\"\n";

$utf_x.= $i_UserStudentName."\t";
$utf_x.= $i_Payment_Field_PaymentItem."\t";
$utf_x.= $i_Payment_Subsidy_Amount."\t";
$utf_x.= $i_Payment_Subsidy_Unit_Admin."\t";
$utf_x.= $i_Payment_Subsidy_Unit_UpdateTime."\t";
$utf_x.= $i_general_status."\r\n";


if(sizeof($temp)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{
	for($i=0;$i<sizeof($temp);$i++){
		list($student_name,$item_name,$sub_amount,$last_modified,$last_modified_date, $status)=$temp[$i];
		$x.="\"$student_name\",\"$item_name\",\"$sub_amount\",\"$last_modified\",\"$last_modified_date\"\n";
// 		$utf_x.=$student_name."\t".$item_name."\t".$sub_amount."\t".$last_modified."\t".$last_modified_date."\r\n";
		$utf_x.=$student_name."\t".$item_name."\t".$sub_amount."\t".$last_modified."\t".$last_modified_date."\t".$status."\r\n";

	}	
	$x.="\n\"".$i_Payment_Note_StudentRemoved2."\"\n";
	$utf_x.="\r\n".$i_Payment_Note_StudentRemoved2."\r\n";
}

$content = $x;

$utf_content=$utf_x;


intranet_closedb();

$filename = "subsidy_unit_studentlist.csv";
$lexport->EXPORT_FILE($filename, $utf_content);

?>
