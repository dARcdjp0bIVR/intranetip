<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$li = new libdb();
$UnitName = intranet_htmlspecialchars($UnitName);
$TotalAmount = $TotalAmount==""?0:$TotalAmount;
$UnitCode = trim($UnitCode);
$unitcode_check = true;
$Result = false;
$fields = '';
$values = '';
if($UnitCode != '') {
	$sql = "SELECT UnitID FROM PAYMENT_SUBSIDY_UNIT WHERE UnitCode='.$li->Get_Safe_Sql_Query($UnitCode).'";
	$temp = $li->returnArray($sql);
	if (count($temp) != 0) {
		$unitcode_check = false;
	} else {
		$fields .= ',UnitCode';
		$values .= ",'".$li->Get_Safe_Sql_Query($UnitCode)."'";
	}
}

if($unitcode_check == true) {
	$sql = "INSERT INTO PAYMENT_SUBSIDY_UNIT (UnitName,TotalAmount,RecordStatus,DateInput,DateModified $fields) VALUES('$UnitName','$TotalAmount','$Status',NOW(),NOW() $values)";

	$Result = $li->db_db_query($sql);


	$RecordDetail = 'Source of Subsidy: ' . $UnitName . '<br/>Total: ' . $TotalAmount . '<br/>Status: ' . ($Status ? $i_general_active : $i_general_inactive);
	$sql = "INSERT INTO
				MODULE_RECORD_UPDATE_LOG
				(Module, Section, RecordDetail, UpdateTableName, UpdateRecordID, LogDate, LogBy)
		VALUES
				('ePayment', 'SourceOfSubsidySettingsAdd','$RecordDetail', 'PAYMENT_SUBSIDY_UNIT', '" . mysql_insert_id() . "', NOW(), $UserID)
		";
	$Result2 = $li->db_db_query($sql);

}

if ($Result) {
	$Msg = $Lang['ePayment']['CreateSubsidySourceSuccess'];
}
else {
	$Msg = $Lang['ePayment']['CreateSubsidySourceUnsuccess'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
