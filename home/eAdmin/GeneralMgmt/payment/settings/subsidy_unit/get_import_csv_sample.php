<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($intranet_root."/includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ImportSubsidySource']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lexport = new libexporttext();

$filename = 'subsidy_source_import_sample.csv';
$headers = array('Source of Subsidy','Total','Status');
$samples = array();
$samples[] = array('Scholarship','200000','1');
$samples[] = array('Full Fee Remission','100000','1');
$samples[] = array('Half Fee Remission','80000','0');

intranet_closedb();

$content = $lexport->GET_EXPORT_TXT($samples, $headers, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename,$content);
?>