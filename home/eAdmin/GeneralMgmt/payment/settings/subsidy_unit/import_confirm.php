<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ImportSubsidySource']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();
$lf = new libfilesystem();
$limport = new libimporttext();

$header_array = array('Source of Subsidy','Total','Status');

$filepath = $_FILES["upload_file"]["tmp_name"];
$filename = $_FILES["upload_file"]["name"];

if($filepath=="none" || $filepath == "")
{
	# import failed
    intranet_closedb();
    $_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
    header("Location:import.php");
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
        	$csv_header = array_shift($data);                   # drop the title bar
        	$format_wrong = false;
        	for($i=0; $i<sizeof($header_array); $i++)
			{
				if ($csv_header[$i]!=$header_array[$i])
				{
					$format_wrong = true;
					break;
				}
			}
			if($format_wrong){
				intranet_closedb();
			    $_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'];
	    		header("Location:import.php");
			    exit();
			}
        }else
        {
		    intranet_closedb();
		    $_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportUnsuccess_NoRecord'];
    		header("Location:import.php");
		    exit();
        }
    }
    
    $data_size = count($data);

	$cur_sessionid = md5(session_id());
	$cur_time = date("Y-m-d H:i:s");

	$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_SUBSIDY_UNIT_IMPORT_RECORD (
				RecordID int(11) NOT NULL auto_increment,
				SessionID varchar(300) NOT NULL,
				ImportTime datetime NOT NULL,
				UnitID int(11) DEFAULT NULL,
				UnitName varchar(255) NOT NULL,
				TotalAmount decimal(13,2) NOT NULL,
				RecordStatus tinyint NOT NULL,
				PRIMARY KEY(RecordID),
				INDEX IdxSessionID(SessionID),
				INDEX IdxImportTime(ImportTime) 
			)ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$lpayment->db_db_query($sql);

	$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
	$sql = "DELETE FROM PAYMENT_SUBSIDY_UNIT_IMPORT_RECORD WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
	$lpayment->db_db_query($sql);
	
}


$sql = "SELECT a.UnitName,".$lpayment->getExportAmountFormatDB("a.TotalAmount")." as TotalAmount,a.RecordStatus,".$lpayment->getExportAmountFormatDB("SUM(b.SubsidyAmount)")." as Consumed,a.UnitID 
		FROM PAYMENT_SUBSIDY_UNIT AS a 
		LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY AS b ON (a.UnitID = b.SubsidyUnitID) 
		GROUP BY a.UnitID";
$units = $lpayment->returnResultSet($sql);
$units_size = count($units);
$unitNameToAry = array();
for($i=0;$i<$units_size;$i++){
	$unitNameToAry[$units[$i]['UnitName']] = $units[$i];
}

$errorAry = array();
$valueAry = array(); // [row #], [type], [unit name], [total amount], [status], [errors]
$values = '';
$addedUnitNameAry = array();

$valid_status_ary = array('1','0');
$status_map = array('1'=>$i_general_active,'0'=>$i_general_inactive);

$delim = "";
for($i=0;$i<$data_size;$i++) {
	
	$row_num = $i+2;
	list($unit_name, $total_amount, $status) = $data[$i];
	$unit_name = trim($unit_name);
	$total_amount = trim($total_amount);
	$status = trim($status);
	
	$error_row = array();
	
	$unit_id = '';
	
	if(isset($unitNameToAry[$unit_name])){
		$unit_id = $unitNameToAry[$unit_name]['UnitID'];
	}
	
	if($unit_name == ''){
		$error_row['UnitName'] = $i_Payment_Subsidy_UnitName.$Lang['General']['_Invalid'];
	}else if(in_array($unit_name,$addedUnitNameAry)){
		$error_row['UnitName'] = $Lang['ePayment']['DuplicatedRecord'];
	}
	$addedUnitNameAry[] = $unit_name;
	
	if(!is_numeric($total_amount) || $total_amount < 0.0){
		$error_row['TotalAmount'] = $i_Payment_Subsidy_Total_Amount.$Lang['General']['_Invalid'];
	}
	
	if(!in_array($status, $valid_status_ary)){
		$error_row['Status'] = $i_general_status.$Lang['General']['_Invalid'];
	}
	
	if(count($error_row)>0){
		$errorAry[$row_num] = $error_row;
	}
	
	$values .= $delim."('$cur_sessionid','$cur_time',".($unit_id==""?"NULL":"'$unit_id'").",'".$lpayment->Get_Safe_Sql_Query($unit_name)."','".$lpayment->Get_Safe_Sql_Query($total_amount)."','".$lpayment->Get_Safe_Sql_Query($status)."')";
	$delim = ",";
	
	$valueAry[] = array($row_num,$unit_id,$unit_name,$total_amount,$status,$errorAry);
}

$data_size = count($valueAry);

$x = '<table class="common_table_list_v30" width="96%">';
$x .= '<thead>
			<tr>
				<th class="num_check">'.$Lang['General']['ImportArr']['Row'].'</th>';
		$x .= '<th style="width:15%">'.$Lang['General']['Type'].'</th>';
		$x .= '<th style="width:25%">'.$i_Payment_Subsidy_UnitName.'</th>';
		$x .= '<th style="width:20%">'.$i_Payment_Subsidy_Total_Amount.'</th>';
		$x .= '<th style="width:15%">'.$i_general_status.'</th>';
		$x .= '<th style="width:25%">'.$Lang['General']['Error'].'</th>';
	$x .= '</tr>
		</thead>';
$x .= '<tbody>';

for($i=0;$i<$data_size;$i++){
	$row_num = $valueAry[$i][0];
	
	$display_errors = isset($errorAry[$row_num]) && count($errorAry[$row_num])>0 ? '<span class="red">'. implode('<br />',$errorAry[$row_num]).'</span>' : '&nbsp;';
	$x .= '<tr>';
		$x .= '<td>'.$row_num.'</td>';
		$x .= '<td>'.($valueAry[$i][1]!=''?$Lang['Group']['Update']:$Lang['Btn']['Add']).'</td>';
		$error_css = isset($errorAry[$row_num]) && isset($errorAry[$row_num]['UnitName'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.$valueAry[$i][2].'</td>';
		$error_css = isset($errorAry[$row_num]) && isset($errorAry[$row_num]['TotalAmount'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.$lpayment->getWebDisplayAmountFormat($valueAry[$i][3]).'</td>';
		$error_css = isset($errorAry[$row_num]) && isset($errorAry[$row_num]['Status'])? ' class="red"' : '';
		$x .= '<td'.$error_css.'>'.($status_map[$valueAry[$i][4]]==''? $valueAry[$i][4]: $status_map[$valueAry[$i][4]]).'</td>';
		$x .= '<td>'.$display_errors.'</td>';
	$x .= '</tr>';
}
$x .= '</tbody>';
$x .= '</table>';

if(count($errorAry)==0 && $values != ''){
	$sql = "INSERT INTO PAYMENT_SUBSIDY_UNIT_IMPORT_RECORD (SessionID,ImportTime,UnitID,UnitName,TotalAmount,RecordStatus) VALUES ".$values;
	$lpayment->db_db_query($sql);
}

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['SourceOfSubsidyChangeLog'], "change_log.php", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

//debug_pr($units);

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($i_Payment_Subsidy_Setting,'index.php');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'');

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);
?>
<br />
<form name="form1" method="POST" action="import_confirm_update.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$x?>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=count($errorAry)>0?'':$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='import.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>