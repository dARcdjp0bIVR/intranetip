<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$Payment = new libpayment();
$PaymentUI = new libpayment_ui();
$linterface = new interface_html();

$CurrentPageArr['ePayment'] = true;
$CurrentPage['SystemProperty'] = true;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePayment']['SystemProperties'], '', 0);

# Get Return Message
$msg = $_REQUEST['msg'];
	
$MODULE_OBJ = $Payment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($msg));

echo $PaymentUI->Get_General_Settings_Form();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>