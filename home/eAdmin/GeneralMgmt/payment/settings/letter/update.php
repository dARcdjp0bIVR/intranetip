<?php

################## Change Log [Start] ##############
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($PaymentLetterHeader==strip_tags($PaymentLetterHeader))
	{
		$PaymentLetterHeader = nl2br($PaymentLetterHeader);
	}
	if ($PaymentLetterHeader1==strip_tags($PaymentLetterHeader1))
	{
		$PaymentLetterHeader1 = nl2br($PaymentLetterHeader1);
	}
	if ($PaymentLetterFooter==strip_tags($PaymentLetterFooter))
	{
		$PaymentLetterFooter = nl2br($PaymentLetterFooter);
	}
}
$PaymentLetterSetting['PaymentLetterHeader'] = trim(stripslashes(urldecode(HTMLtoDB($PaymentLetterHeader))));
$PaymentLetterSetting['PaymentLetterHeader1'] = trim(stripslashes(urldecode(HTMLtoDB($PaymentLetterHeader1))));
$PaymentLetterSetting['PaymentLetterFooter'] = trim(stripslashes(urldecode(HTMLtoDB($PaymentLetterFooter))));

$GeneralSetting = new libgeneralsettings();

$GeneralSetting->Save_General_Setting('ePayment',$PaymentLetterSetting);

intranet_closedb();
header("Location: index.php?msg=2");
?>