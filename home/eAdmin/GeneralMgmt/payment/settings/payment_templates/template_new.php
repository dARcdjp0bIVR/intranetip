<?php 
//editing:
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libAppTemplate = new libeClassApp_template();

# check Access Right
$libAppTemplate->canAccess($Module,$Section);

# Init libeClassApp_template
$libAppTemplate->setModule($Module);
$Section='all';
$libAppTemplate->setSection($Section);

# Value Ready for edit
if(!empty($TemplateID)){
	//debug_pr($TemplateID);
	if(is_array($TemplateID)){
		$TemplateIDStr = $TemplateID[0];
	}
	else{
		$TemplateIDStr = $TemplateID;
	}
	$TemplateAssoAry = $libAppTemplate->getMessageTemplate('',true);
	$TitleValue = $TemplateAssoAry[$TemplateIDStr]['Title'];
	$ContentValue = $TemplateAssoAry[$TemplateIDStr]['Content'];
	//debug_pr($TemplateAssoAry);
}
else{
	$TitleValue='';
	$ContentValue='';
}

# Page Setting & Left Menu
$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;	
$CurrentPage = "PushMessageTemplateSettings";
	
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['SMS']['MessageTemplates']);


# Start layout
$linterface->LAYOUT_START($xmsg);

# Navigation bar
$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageTemplates'], "javascript:history.back();");
$PAGE_NAVIGATION[] = (isset($TemplateID) && $TemplateID>0) ? array($Lang['Btn']['Edit']) : array($Lang['Btn']['New']);
$navigationBar = $linterface->GET_NAVIGATION4($PAGE_NAVIGATION);

# Form Data Field
$ContentTextArea = $linterface->GET_TEXTAREA("MessageContent", $ContentValue, 50,8);

# Message tag drop down menu
$MessageTagSelect = $libAppTemplate->getMessageTagSelectionBox();
if($MessageTagSelect == ''){
	$autoFillRow = '';
}
else{
	$autoFillRow= '<tr>
						<td valign="top" nowrap="nowrap" class="field_title">
							'.$i_SMS_Personalized_Msg_Tags.'
						</td>
						<td class="tabletext" width="70%">
							'.$MessageTagSelect.'
						</td>
					</tr>';
}

# PageEndButton
$PageEndButton .= $linterface->GET_ACTION_BTN($button_submit, "submit");
$PageEndButton .= '&nbsp;';
$PageEndButton .=  $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?Module=$Module&Section=$Section'");



?>
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<script>
$(document).ready( function() {
	AppMessageReminder.initInsertAtTextarea();
});

function checkform(obj){
	if(!check_text(obj.Title, "<?=$Lang['AppNotifyMessage']['Warning']['FillInTitle']?>")) return false;
	if(!check_text(obj.MessageContent, "<?=$Lang['AppNotifyMessage']['Warning']['FillInContent']?>")) return false;
}
</script>

<form name="form1" action="template_new_update.php" method="post" onsubmit="return checkform(this)">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				
				<?=$navigationBar?>

			</td>
		</tr>
	</table>

	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['AppNotifyMessage']['Title']?>&nbsp;<span class="tabletextrequire">*</span>
			</td>
			<td class="tabletext" width="70%">
				<input type="text" id="Title" name="Title" value="<?=intranet_htmlspecialchars($TitleValue)?>" size="50" />
			</td>
		</tr>
		<tr>
    		<td valign="top" nowrap="nowrap" class="field_title">
    			<?=$Lang['Gamma']['Message']?>&nbsp;<span class="tabletextrequire">*</span>
	   	 	</td>
			<td class="tabletext" width="70%">
				<?=$ContentTextArea?>
			</td>
		</tr>
			<?=$autoFillRow?>
		<tr>
			<td align="left" class="tabletextremark">
				<?=$Lang['General']['RequiredField']?>
			</td>
		</tr>
		
		<input type="hidden" id="TemplateID" name="TemplateID" value="<?=$TemplateIDStr?>" />
		<input type="hidden" name="Module" id="Module" value="<?=$Module?>" />
		<input type="hidden" name="Section" id="Section" value="<?=$Section?>" />
	
	</table>

	<div class="edit_bottom_v30">
		<?=$PageEndButton?>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>