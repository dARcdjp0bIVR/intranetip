<?php

################## Change Log [Start] ##############
#
#	Date:	2017-06-21	Anna
#			Create this file - for new/edit/remove a template
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$libAppTemplate = new libeClassApp_template();

# check Access Right
$libAppTemplate->canAccess($Module,$Section);

# Init libeClassApp_template
$libAppTemplate->setModule($Module);
$libAppTemplate->setSection($Section);
//debug_pr($Section);
//die();
$Title = standardizeFormPostValue($Title);
$Content = standardizeFormPostValue($MessageContent);

preg_match_all('(\(\$[A-z]+\))', $Content, $output_array);

for($i=0;$i<count($output_array[0]);$i++){
	$output_array[0][$i]=substr($output_array[0][$i],2,-1);
}
//debug_pr($output_array);
$allTagArr = $libAppTemplate->GetMessageTagAry($Module,'all');
//debug_pr($allTagArr);
$Section='';
for($i=0;$i<count($output_array[0]);$i++){
	for($j=0;$j<count($allTagArr);$j++){
		if($output_array[0][$i]==$allTagArr[$j][0]){
			if($allTagArr[$j][2]!=''){
				$Section = $allTagArr[$j][2];
				break;
			}
		}
	}
}
//debug_pr($Section);
$libAppTemplate->setSection($Section);

if(is_array($TemplateID) && $Delete == 1){
	$TemplateIDstr = implode("','",$TemplateID);
	$result = $libAppTemplate->insertUpdatMessageTemplate('DELETE', $UserID, '', '',$TemplateIDstr);

	if($result>0){
		$msg = "DeleteSuccess";
	}else{
		$msg = "DeleteFail";
	}
}
else{
	if($TemplateID==''){
		$result = $libAppTemplate->insertUpdatMessageTemplate('INSERT', $UserID, $Title, $Content);
	
		if($result>0){
			$msg = "AddSuccess";
		}else{
			$msg = "AddFail";
		}
	}
	else{
		$result = $libAppTemplate->insertUpdatMessageTemplate('UPDATE', $UserID, $Title, $Content, $TemplateID,$Section);
	
		if($result>0){
			$msg = "UpdateSuccess";
		}else{
			$msg = "UpdateFail";
		}
	}
}


intranet_closedb();

header("Location: index.php?Module=$Module&Section=&xmsg={$msg}");
exit();

?>