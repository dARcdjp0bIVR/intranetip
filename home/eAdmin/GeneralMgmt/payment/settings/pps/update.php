<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();

$SettingList['PPSChargeEnabled'] = ($_REQUEST['PPSChargeEnabled'])? $_REQUEST['PPSChargeEnabled']:0;
if ($SettingList['PPSChargeEnabled'] == 1) {
	$SettingList['PPSIEPSCharge'] = $_REQUEST['PPSIEPSCharge'];
	$SettingList['PPSCounterBillCharge'] = $_REQUEST['PPSCounterBillCharge'];
}
$GeneralSetting->Save_General_Setting('ePayment',$SettingList);

intranet_closedb();
header("Location: index.php?msg=2");
?>