<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_LoginAccount";

$linterface = new interface_html();

$TerminalUserID = (is_array($TerminalUserID)? $TerminalUserID[0]:$TerminalUserID);

$li = new libdb();
$sql = "SELECT Username, PaymentAllowed, PurchaseAllowed FROM PAYMENT_TERMINAL_USER WHERE TerminalUserID = '$TerminalUserID'";
$temp = $li->returnArray($sql,3);
list($Username, $payment, $purchase) = $temp[0];

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_TerminalAccount, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit)

?>
<script language="Javascript">
function checkform(obj)
{
     if(!check_text(obj.Username, "<?php echo $i_alert_pleasefillin.$i_Payment_Field_Username; ?>.")) return false;
     if (!obj.paymentAllowed.checked && !obj.purchaseAllowed.checked)
     {
          alert("<?=$i_Payment_alert_selectOneFunction?>");
          return false;
     }
     if (obj.needChangePwd.checked)
     {
         if(!check_text(obj.Password, "<?php echo $i_alert_pleasefillin.$i_UserPassword; ?>.")) return false;
         if (obj.Password.value != obj.RePassword.value)
         {
             alert("<?=$i_frontpage_myinfo_password_mismatch?>");
             return false;
         }
     }

}

function change(obj)
{
         obj.form.Password.disabled = !obj.checked;
         obj.form.RePassword.disabled = !obj.checked;
}
</script>
<form name="form1" action="edit_update.php" method="post" onsubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_Username?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="Username" maxlength="10" value="<?=$Username?>">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_ChangePassword?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="needChangePwd" onchange="change(this)">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserPassword?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="password" name="Password" disabled maxlength="10">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_frontpage_myinfo_password_retype?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="password" name="RePassword" maxlength="10" disabled>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_Function?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="paymentAllowed" value="1" <?=($payment==1?"CHECKED":"")?>> <?=$i_Payment_Field_PaymentAllowed?>
	    				<input type="checkbox" name="purchaseAllowed" value="1" <?=($purchase==1?"CHECKED":"")?>> <?=$i_Payment_Field_PurchaseAllowed?>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="TerminalUserID" value="<?=$TerminalUserID?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
