<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$Username = intranet_htmlspecialchars(trim($Username));
$Password = intranet_htmlspecialchars(trim($Password));
$payment = ($paymentAllowed == 1? "1":"0");
$purchase = ($purchaseAllowed == 1? "1": "0");


$sql = "INSERT INTO PAYMENT_TERMINAL_USER (Username,Password,PaymentAllowed,PurchaseAllowed,DateInput,DateModified)
        VALUES ('$Username','$Password','$payment','$purchase',now(),now())";
$li->db_db_query($sql);
header ("Location: index.php?msg=1");

intranet_closedb();
?>
