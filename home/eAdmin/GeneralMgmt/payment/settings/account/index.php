<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_setting_terminal_account_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_payment_setting_terminal_account_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_setting_terminal_account_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_payment_setting_terminal_account_page_number!="")
{
	$pageNo = $ck_payment_setting_terminal_account_page_number;
}

if ($ck_payment_setting_terminal_account_page_order!=$order && $order!="")
{
	setcookie("ck_payment_setting_terminal_account_page_order", $order, 0, "", "", 0);
	$ck_payment_setting_terminal_account_page_order = $order;
} else if (!isset($order) && $ck_payment_setting_terminal_account_page_order!="")
{
	$order = $ck_payment_setting_terminal_account_page_order;
}

if ($ck_payment_setting_terminal_account_page_field!=$field && $field!="")
{
	setcookie("ck_payment_setting_terminal_account_page_field", $field, 0, "", "", 0);
	$ck_payment_setting_terminal_account_page_field = $field;
} else if (!isset($field) && $ck_payment_setting_terminal_account_page_field!="")
{
	$field = $ck_payment_setting_terminal_account_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_LoginAccount";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

#$order = ($order == 1) ? 1 : 0;

$sql  = "SELECT
               Username, 
               IF(PaymentAllowed=1,'<img src=''{$image_path}/{$LAYOUT_SKIN}/icon_tick.gif''>','&nbsp;'), 
               IF(PurchaseAllowed=1,'<img src=''{$image_path}/{$LAYOUT_SKIN}/icon_tick.gif''>','&nbsp;'), 
               /*LastLogin, */
               DateInput,
               CONCAT('<input type=checkbox name=TerminalUserID[] value=', TerminalUserID ,'>')
         FROM
               PAYMENT_TERMINAL_USER
         WHERE
		       (Username LIKE '%$keyword%')
                ";
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Username","PaymentAllowed","PurchaseAllowed",/*"LastLogin",*/"DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$Col = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($Col++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($Col++, $i_Payment_Field_PaymentAllowed)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($Col++, $i_Payment_Field_PurchaseAllowed)."</td>\n";
//$li->column_list .= "<td width='20%'>".$li->column(3, $i_Payment_Field_LastLogin)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($Col++, $i_Payment_Field_AccountCreation)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("TerminalUserID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_TerminalAccount, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == "1") $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == "3") $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="2" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
