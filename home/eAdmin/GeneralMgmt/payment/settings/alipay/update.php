<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
if(!$lpayment->isAlipayDirectPayEnabled() && !$lpayment->isAlipayTopUpEnabled()) {
	intranet_closedb();
	header("Location: /");
	exit();
}

$GeneralSetting = new libgeneralsettings();

$AlipayHKHandlingFee = ($_REQUEST['AlipayHKHandlingFee'])? $_REQUEST['AlipayHKHandlingFee']:0;
if(is_numeric($AlipayHKHandlingFee)) {
	$AlipayHKHandlingFee = floor($AlipayHKHandlingFee*100)/100;
} else {
	$AlipayHKHandlingFee = 0;
}

if($AlipayHKHandlingFee < 0) {
	$AlipayHKHandlingFee = 0;
}
$SettingList['AlipayHKHandlingFee'] = $AlipayHKHandlingFee;
$GeneralSetting->Save_General_Setting('ePayment',$SettingList);

intranet_closedb();
header("Location: index.php?msg=2");
?>