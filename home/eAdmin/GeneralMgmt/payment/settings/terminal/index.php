<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_TerminalSettings";

$linterface = new interface_html();
$lpayment = new libpayment();
$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'TerminalIPList'";
$SettingList[] = "'ConnectionTimeout'";
$SettingList[] = "'PaymentNoAuth'";
$SettingList[] = "'PurchaseNoAuth'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_TerminalIP, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="post" action="update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletext" align="right" colspan="2">
						<?= $SysMsg ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_SmartCard_Terminal_IPList?>
					</td>
					<td class="tabletext" width="70%">
						<span class="tabletextremark"><?=$i_SmartCard_Terminal_IPInput?><br />
						<?=$i_SmartCard_Terminal_YourAddress?>:<?=getRemoteIpAddress()?></span><br />
						<?=$linterface->GET_TEXTAREA("TerminalIPList", $Settings['TerminalIPList'])?>
					</td>
				</tr>
				<tr>
					<td valign="top" class="formfieldtitle tabletext">
						<?=$i_Payment_Auth_PaymentRequired?>
					</td>
					<td class="tabletext" width="70%">
						<input type="checkbox" name="PaymentNoAuth" value="1" <?=($Settings['PaymentNoAuth']?"CHECKED":"")?>>
					</td>
				</tr>
				<tr>
					<td valign="top" class="formfieldtitle tabletext">
						<?=$i_Payment_Auth_PurchaseRequired?>
					</td>
					<td class="tabletext" width="70%">
						<input type="checkbox" name="PurchaseNoAuth" value="1" <?=($Settings['PurchaseNoAuth']?"CHECKED":"")?>>
					</td>
				</tr>
				<tr>
					<td valign="top" class="formfieldtitle tabletext">
						<?=$i_SmartCard_Terminal_ExpiryTime?>
					</td>
					<td class="tabletext" width="70%">
						<input class="textboxnum" type="text" name="ConnectionTimeout" value="<?=$Settings['ConnectionTimeout']?>">
					</td>
				</tr>
				<tr>
					<td class="tabletext" colspan="2"><br /><?=$i_SmartCard_Description_Terminal_IP_Settings?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.TerminalIPList");
intranet_closedb();
?>