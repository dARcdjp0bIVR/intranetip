<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "CatCode") {
	$CatCode = stripslashes($_REQUEST['Value']);
	$ExcludeItemID = stripslashes($_REQUEST['ItemID']);
	$returnString = 0;
	if($CatCode != "") {
		$returnString = $lpayment->checkPaymentCatCodeValid($CatCode, $ExcludeItemID);
		$returnString = ($returnString) ? '1' : '0';
	}
}


intranet_closedb();

echo $returnString;
?>