<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$is_edit = isset($IdentityID) && $IdentityID > 0;

if(trim($IdentityName) == '' || trim($Code) == ''){
	$flashData = $_POST;
	if(trim($IdentityName) == ''){
		$flashData['Error_IdentityName'] = $Lang['General']['JS_warning']['CannotBeBlank'];
	}
	if(trim($Code) == ''){
		$flashData['Error_Code'] = $Lang['General']['JS_warning']['CannotBeBlank'];
	}
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:edit.php");
	exit;
}

$params = array('IdentityName'=>$IdentityName);
if($is_edit){
	$params['ExcludeIdentityID'] = $IdentityID;
}
$existing_records = $lpayment->getSubsidyIdentity($params);

$params2 = array('Code'=>$Code);
if($is_edit){
	$params2['ExcludeIdentityID'] = $IdentityID;
}
$existing_records2 = $lpayment->getSubsidyIdentity($params2);

$flashData = array();
if(count($existing_records)>0 || count($existing_records2)>0){
	$flashData = $_POST;
	if(count($existing_records)>0){
		$flashData['Error_IdentityName'] = $Lang['ePayment']['SubsidyIdentityNameUsedByAnotherRecord'];
	}
	if(count($existing_records2)>0){
		$flashData['Error_Code'] = $Lang['ePayment']['SubsidyIdentityCodeUsedByAnotherRecord'];
	}
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:edit.php");
	exit;
}


$upsert_success = $lpayment->upsertSubsidyIdentity($_POST);

if($upsert_success){
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess'];
}else{
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:index.php");
?>