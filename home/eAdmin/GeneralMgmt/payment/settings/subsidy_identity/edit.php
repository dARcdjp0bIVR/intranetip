<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";

$flashData = array();
if(isset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA']);
}

if(isset($IdentityID)){
	if(is_array($IdentityID)) $IdentityID = $IdentityID[0];
	
	$records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$IdentityID));
	if(count($records)>0){
		$hidden_fields = '<input type="hidden" id="IdentityID" name="IdentityID" value="'.$IdentityID.'" />';
		$is_edit = true;
		$record = $records[0];
	}else{
		intranet_closedb();
		header ("Location: index.php");
		exit();
	}
}else{
	$record = array();
	if(count($flashData)>0){
		foreach($flashData as $key => $val){
			$record[$key] = $val;
		}
	}
}

$pages_arr = array(
	array($Lang['ePayment']['SubsidyIdentity'],'index.php'),
	array($IdentityID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form id="form1" name="form1" method="post" action="update.php" onsubmit="return false;">
<?=$hidden_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['SubsidyIdentityName']?><span class="tabletextrequire">*</span></td>
		<td>
			<?=$linterface->GET_TEXTBOX_NAME("IdentityName", "IdentityName", $record['IdentityName'], 'required', array('maxlength'=>255))?>&nbsp;<span id="Error_IdentityName" class="error" style="color:red;"><?=$flashData['Error_IdentityName']?></span>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['ePayment']['SubsidyIdentityCode']?><span class="tabletextrequire">*</span></td>
		<td>
			<?=$linterface->GET_TEXTBOX_NAME("Code", "Code", $record['Code'], 'required', array('maxlength'=>128))?>&nbsp;<span id="Error_Code" class="error" style="color:red;"><?=$flashData['Error_Code']?></span>
		</td>
	</tr>
</table>
<div style="float:left;margin-left:8px;">
<?=$linterface->MandatoryField()?>
</div>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php'",'cancel_btn')?>
</div>
</form>	
<br /><br />
<script type="text/javascript">
function checkSubmitForm(formObj)
{
	$('#submit_btn').attr('disabled',true);
	$('.error').html('');
	var valid = true;
	var inputs = $('input.required');
	for(var i=0;i<inputs.length;i++){
		var obj = $(inputs[i]);
		var val = $.trim(obj.val());
		if(val == ''){
			$('#Error_'+obj.attr('id')).html('<?=$Lang['General']['JS_warning']['CannotBeBlank']?>').show();
			valid = false;
		}
	}
	
	if(valid){
		formObj.submit();
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}

$(document).ready(function(){
	$('#IdentityName').focus();
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>