<?php
// Editing by
/*
 * 2019-06-24 (Ray): Created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
    intranet_closedb();
    header ("Location: /");
    exit();
}

$linterface = new interface_html();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$PAGE_NAVIGATION[] = array($button_import);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

$sql = "SELECT 
            i.Code,
			i.IdentityName
		FROM PAYMENT_SUBSIDY_IDENTITY as i 
		WHERE i.RecordStatus='1'";

$rs = $lpayment->returnArray($sql);

$subsidy_content = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="text-align: center;">
	<tbody>
		<tr class="tabletop">
			<th>'.$Lang['ePayment']['SubsidyIdentityCode'].'</th>
			<th>'.$Lang['ePayment']['SubsidyIdentityName'].'</th>
		</tr>';
foreach($rs as $temp) {
	$subsidy_content .= '<tr>
                <td class="tabletext tablerow">'.cleanHtmlJavascript($temp['Code']).'</td>
                <td class="tabletext tablerow">'.cleanHtmlJavascript($temp['IdentityName']).'</td>
            </tr>';
}
$subsidy_content .= '
	</tbody>
</table>';

$tempLayer = returnRemarkLayer($subsidy_content,"hideMenu('ToolMenu2');", 300);

?>
<script>
function hideMenu(menuName)
{
    objMenu = document.getElementById(menuName);
    if(objMenu!=null)
        objMenu.style.visibility='hidden';
}

function showMenu2(objName,menuName)
{
    objIMG = document.getElementById(objName);
    offsetX = (objIMG==null)?0:objIMG.width;
    offsetY =0;
    var pos_left = getPostion(objIMG,"offsetLeft");
    var pos_top  = getPostion(objIMG,"offsetTop");

    objDiv = document.getElementById(menuName);

    if(objDiv!=null){
        objDiv.style.visibility='visible';
        objDiv.style.top = pos_top+offsetY+"px";
        objDiv.style.left = pos_left+offsetX+"px";
    }
}

</script>
<br />
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"><?=$tempLayer?></div>
<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data">
    <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td><?= $linterface->GET_STEPS($STEPS_OBJ)?></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <table>
            <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_select_file?></td>
                    <td width="70%"class="tabletext">
                        <input type="file" class="file" name="userfile"><br />
                        <?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
                    </td>
                </tr>
                <tr>
                    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
                    <td width="70%" class="tabletext">
                        <input type="radio" name="format" id="format1" value="1" checked="checked">
                        <label for="format1"><?=$i_SubsidyIdentity_Import_Format_SILink?></label>
                        <br />
                        [<a class="tablelink" href="get_csv_sample.php?type=1" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a>]
                        <br />
                        <br />
                        <input type="radio" name="format" id="format2" value="2">
                        <label for="format2"><?=$i_SubsidyIdentity_Import_Format_SILink2?></label>
                        <br />
                        [<a class="tablelink" href="get_csv_sample.php?type=2" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a>]
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="tabletextremark" colspan="2"><?=$Lang['formClassMapping']['Reference']?></td>
                </tr>
                <tr>
                    <td class="tabletextremark" colspan="2"><?=$Lang['General']['RequiredField']?></td>
                </tr>
            </table>
            <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
                <tr>
                    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
            </table>
            <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
                <tr>
                    <td align="center" colspan="2">
                        <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location.href= 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                    </td>
                </tr>
            </table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
