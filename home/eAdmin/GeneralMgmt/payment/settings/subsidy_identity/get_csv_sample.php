<?php
// Editing by
// !!!!!! Please use BIG5 encoding. !!!!!!
/*
 * 2019-06-24 (Ray): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
    intranet_closedb();
    header ("Location: /");
    exit();
}

$lexport = new libexporttext();

$type = $_REQUEST['type'];
$file_format = $lpayment->Get_Subsidy_Identity_Format_Array($type);

if($type == '1')
{
    $filename = "student_subsidy_identity_settings_by_classname_import_sample.csv";
    $samples = array();
    $samples[] = array("CA","1A","6","Chan Tai Man","2018-09-01","2019-09-30");
    $samples[] = array("CA","1A","7");
}else {
    $filename = "student_subsidy_identity_settings_by_login_id_import_sample.csv";
    $samples = array();
	$samples[] = array("CA","s20190001","Lee Siu Man","2018-09-01","2019-09-30");
}

$content = $lexport->GET_EXPORT_TXT($samples, $file_format, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename,$content);

intranet_closedb();
?>