<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
    intranet_closedb();
    header ("Location: /");
    exit();
}


$li = new libdb();
$lexport = new libexporttext();

$exportColumn = $lpayment->Get_Subsidy_Identity_Format_Array($format);
$rows = array();
$timestamp = time();

$intranet_user_name = Get_Lang_Selection("u1.ChineseName","u1.EnglishName");
$intranet_archive_user_name = Get_Lang_Selection("u2.ChineseName","u2.EnglishName");

if($format == '1') {
    $sql = "SELECT 
            b.Code,
			IF(u1.UserID IS NOT NULL,u1.ClassName,u2.ClassName) as ClassName,
			IF(u1.UserID IS NOT NULL,u1.ClassNumber,u2.ClassNumber) as ClassNumber,
			IF(u1.UserID IS NOT NULL,$intranet_user_name,$intranet_archive_user_name) as StudentName,
			s.EffectiveStartDate,
			s.EffectiveEndDate
		FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT as s
		LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as b ON s.IdentityID=b.IdentityID
		LEFT JOIN INTRANET_USER as u1 ON s.StudentID=u1.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON s.StudentID=u2.UserID
		ORDER BY b.Code, ClassName, ClassNumber";


    $arr_result = $li->returnArray($sql,6);

    if(sizeof($arr_result)>0) {
        for($i=0; $i<sizeof($arr_result); $i++){
            list($subsidy_code, $class_name, $class_number, $student_name, $effective_startdate, $effective_enddate) = $arr_result[$i];
            $rows[] = array($subsidy_code, $class_name, $class_number, $student_name, $effective_startdate, $effective_enddate);
        }
    }

    $filename = "student_subsidy_identity_by_classname_classnumber_".$timestamp.".csv";
} else if($format == '2') {
    $sql = "SELECT 
            b.Code,
			IF(u1.UserID IS NOT NULL,u1.ClassName,u2.ClassName) as ClassName,
			IF(u1.UserID IS NOT NULL,u1.ClassNumber,u2.ClassNumber) as ClassNumber,
		    IF(u1.UserID IS NOT NULL,u1.UserLogin,u2.UserLogin) as UserLogin,
			IF(u1.UserID IS NOT NULL,$intranet_user_name,$intranet_archive_user_name) as StudentName,
			s.EffectiveStartDate,
			s.EffectiveEndDate
		FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT as s
		LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as b ON s.IdentityID=b.IdentityID
		LEFT JOIN INTRANET_USER as u1 ON s.StudentID=u1.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON s.StudentID=u2.UserID
		ORDER BY b.Code, ClassName, ClassNumber";

    $arr_result = $li->returnArray($sql,7);

    if(sizeof($arr_result)>0) {
        for($i=0; $i<sizeof($arr_result); $i++){
            list($subsidy_code, $class_name, $class_number, $user_login, $student_name, $effective_startdate, $effective_enddate) = $arr_result[$i];
            $rows[] = array($subsidy_code, $user_login, $student_name, $effective_startdate, $effective_enddate);
        }
    }

    $filename = "student_subsidy_identity_by_loginid_".$timestamp.".csv";
}

intranet_closedb();

$export_content = $utf_display.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>