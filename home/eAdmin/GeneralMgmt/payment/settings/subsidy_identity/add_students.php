<?php
// Editing by 
/*
 * 2020-04-03 Ray: allow effective end date empty
 * 2019-08-27 Ray: Added ajax change identity check
 * 2019-08-23 Carlos: Added effective period.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !isset($IdentityID) || $IdentityID=='') {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";

$flashData = array();
if(isset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA']);
}

$records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$IdentityID));
if(count($records)==0){
	$flashData['return_msg'] = '0|=|'.$Lang['General']['InvalidData'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header ("Location: index.php");
	exit();
}

$IdentityID = IntegerSafe($IdentityID);

$identity_name = $records[0]['IdentityName'];

$pages_arr = array(
	array($Lang['ePayment']['SubsidyIdentity'],'index.php'),
	array($identity_name, 'student_list.php?IdentityID='.$IdentityID),
	array($eDiscipline["AddStudents"],'')
);

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>

<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<br />
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form id="form1" name="form1" method="post" action="add_students_update.php" onsubmit="return false;">
<input type="hidden" name="IdentityID" value="<?=$IdentityID?>" />
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
	<tr>
		<td class="field_title"><?=$eDiscipline["AddStudents"]?><span class="tabletextrequire">*</span></td>
		<td>
			<table class="" border="0" width="100%" border="0" cellspacing="0" cellpadding="0">
	    		<tr>
	    			<td valign="top" width="20%" style="border:none;">
						<?=$linterface->GET_SELECTION_BOX(array(), ' name="StudentID[]" id="StudentID[]" multiple="multiple" size="20" style="width:100%;" ', '', "", false)?>
					</td>
					<td valign="bottom" align="left" width="80%" style="vertical-align:bottom;border:none;">
						<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "openStudentSelection();") ?><br />
	    				<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "removeStudent();") ?>
	    			</td>
	    		</tr>
	    	</table>
	    	<span id="StudentID_Error" class="red error"></span>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['SubsidyIdentity']['EffectivePeriod']?></td>
		<td>
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("EffectiveStartDate",$record['EffectiveStartDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EffectiveEndDate",$record['EffectiveEndDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?>
			<div class="tabletextremark">(<?=$Lang['ePayment']['SubsidyIdentityEffectivePeriodRemark']?>)</div>
		</td>
	</tr>
</table>
<div style="float:left;margin-left:8px; margin-bottom: 10px;">
<?=$linterface->MandatoryField()?>
</div>
    <div id="WarningLayer"></div>
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='student_list.php?IdentityID=".$IdentityID."'",'cancel_btn')?>
</div>
</form>	
<br /><br />
<script type="text/javascript">
function openStudentSelection() 
{
	newWindow('/home/eAdmin/GeneralMgmt/payment/settings/payment_item/choose/index.php?fieldname=StudentID[]&approvedUserOnly=1',9);
}

function removeStudent()
{
	checkOptionRemove(document.form1.elements["StudentID[]"]);
}

function validateDate(value)
{
	var dateformat = /^\d{4}-\d{2}-\d{2}$/;
	if(!value.match(dateformat))
	{
		return false;
	}
  
	var parts = value.split('-');
	var year_int = parseInt(parts[0]);
	var month_int = parseInt(parts[1]);
	var day_int = parseInt(parts[2]);
	
	var date_obj = new Date(value);
	var date_year_int = date_obj.getFullYear();
	var date_month_int = date_obj.getMonth() + 1;
	var date_day_int = date_obj.getDate();
	
	if(year_int != date_year_int || month_int != date_month_int || day_int != date_day_int){
		return false;
	}
	
	return true;
}

function checkSubmitForm(formObj)
{
	$('#submit_btn').attr('disabled',true);
	$('.error').html('');
	$('#DPWL-EffectiveStartDate').html('').hide();
	$('#DPWL-EffectiveEndDate').html('').hide();
	var valid = true;

	$('#StudentID_Error').html('').hide();
	var selected_count = $('select#StudentID\\[\\] option').length;
	if(selected_count == 0){
		valid = false;
		$('#StudentID_Error').html('<?=$Lang['General']['JS_warning']['SelectAtLeastOneStudent']?>').show();
	}
	
	var start_date_value = $.trim($('#EffectiveStartDate').val());
	var end_date_value = $.trim($('#EffectiveEndDate').val());
	
	if(start_date_value != '' || end_date_value != ''){
		if(start_date_value == ''){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['WarningArr']['InputDate']?>').show();
		}else if(!validateDate(start_date_value)){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
		}
		if(end_date_value == ''){
			//valid = false;
			//$('#DPWL-EffectiveEndDate').html('<?=$Lang['General']['WarningArr']['InputDate']?>').show();
		}else if(!validateDate(end_date_value)){
			valid = false;
			$('#DPWL-EffectiveEndDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
		}
		if(start_date_value != '' && end_date_value != '' && end_date_value < start_date_value){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		}
	}
	
	if(valid){
		//checkOptionAll(document.getElementById('StudentID[]'));
		//formObj.submit();

        Block_Element('form1','<?=$Lang['StudentAttendance']['Checking']?>');
        checkOptionAll(document.getElementById('StudentID[]'));
        $.post(
            'ajax_get_subsidy_identity_check_result.php',
            $(formObj).serialize(),
            function(data){
                UnBlock_Element('form1');
                if(data != ''){
                    $('#WarningLayer').html(data);
                    jConfirm('<?=$Lang['ePayment']['StudentWillRemoveExistingIdentity']?>', '<?=$Lang['Btn']['Confirm']?>', function(r) {
                        if(r){
                            // OK
                            formObj.submit();
                        }else{
                            // Cancel
                            $('#submit_btn').attr('disabled',false);
                        }
                    });
                }else{
                    $('#WarningLayer').html('');
                    formObj.submit();
                }
            }
        );
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}
$(document).ready(function(){
	$('#StudentID\\[\\]').focus();
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>