<?php
// Editing by 
/*
 * 2019-08-27 Ray: Added effective date change log
 * 2019-08-23 Carlos: Created for editing one student's effective period.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if(!isset($IdentityID) || empty($IdentityID) || !isset($StudentID) || empty($StudentID) || !isset($EffectiveStartDate) || !isset($EffectiveEndDate)){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php".($IdentityID!=''?"?IdentityID=".$IdentityID:""));
	exit;
}

$IdentityID = IntegerSafe($IdentityID);
$StudentID = IntegerSafe($StudentID);

$records = $lpayment->getSubsidyIdentityStudents(array('IdentityID'=>$IdentityID,'StudentID'=>$StudentID,'GetStudentUser'=>false));
if(count($records)==0){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php?IdentityID=".$IdentityID);
	exit;
}

$record = $records[0];

if(($EffectiveStartDate != '' && !intranet_validateDate($EffectiveStartDate)) || ($EffectiveEndDate != '' && !intranet_validateDate($EffectiveEndDate))){
	$EffectiveStartDate = '';
	$EffectiveEndDate = '';
}

$delete_success = $lpayment->deleteSubsidyIdentityStudents(array('IdentityID'=>$IdentityID,'StudentID'=>$StudentID));
$add_success = false;
if($delete_success){
	$data = array();
	$data['StudentID'] = $StudentID;
	$data['OldIdentityID'] = $record['IdentityID'];
	$data['OldEffectiveStartDate'] = $record['EffectiveStartDate'];
	$data['OldEffectiveEndDate'] = $record['EffectiveEndDate'];
	$data['NewIdentityID'] = $IdentityID;
	$data['NewEffectiveStartDate'] = $EffectiveStartDate;
	$data['NewEffectiveEndDate'] = $EffectiveEndDate;
	$lpayment->addSubsidyIdentityStudentChangeLogs(array($data));

	$add_success = $lpayment->addSubsidyIdentityStudents(array('IdentityID'=>array($IdentityID),'StudentID'=>array($StudentID),'EffectiveStartDate'=>array($EffectiveStartDate),'EffectiveEndDate'=>array($EffectiveEndDate)));
}

if($delete_success && $add_success){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:student_list.php?IdentityID=".$IdentityID);
?>