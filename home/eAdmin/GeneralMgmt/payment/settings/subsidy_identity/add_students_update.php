<?php
// Editing by 
/*
 * 2019-08-27 Ray: Add effective date change log
 * 2019-08-23 Carlos: Added effective period.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !isset($IdentityID) || $IdentityID=='') {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$IdentityID));
if(count($records)==0){
	$flashData['return_msg'] = '0|=|'.$Lang['General']['InvalidData'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header ("Location: index.php");
	exit();
}

if(!isset($StudentID) || count($StudentID)==0){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php?IdentityID=".$IdentityID);
	exit;
}

if(($EffectiveStartDate != '' && !intranet_validateDate($EffectiveStartDate)) || ($EffectiveEndDate != '' && !intranet_validateDate($EffectiveEndDate))){
	$EffectiveStartDate = '';
	$EffectiveEndDate = '';
}

$identity_id_ary = array_fill(0,count($StudentID), $IdentityID);
$effective_start_date_ary = array_fill(0,count($StudentID), $EffectiveStartDate);
$effective_end_date_ary = array_fill(0,count($StudentID), $EffectiveEndDate);

$change_log_data = array();
foreach($StudentID as $sid) {
	$students_record = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$sid,'GetStudentUser'=>false));
	if(count($students_record) > 0) {
		$students_record = $students_record[0];
		$data = array();
		$data['StudentID'] = $sid;
		$data['OldIdentityID'] = $students_record['IdentityID'];
		$data['OldEffectiveStartDate'] = $students_record['EffectiveStartDate'];
		$data['OldEffectiveEndDate'] = $students_record['EffectiveEndDate'];
		$data['NewIdentityID'] = $IdentityID;
		$data['NewEffectiveStartDate'] = $EffectiveStartDate;
		$data['NewEffectiveEndDate'] = $EffectiveEndDate;
		$change_log_data[] = $data;
	} else {
		$data = array();
		$data['StudentID'] = $sid;
		$data['OldIdentityID'] = '';
		$data['OldEffectiveStartDate'] = '';
		$data['OldEffectiveEndDate'] = '';
		$data['NewIdentityID'] = $IdentityID;
		$data['NewEffectiveStartDate'] = $EffectiveStartDate;
		$data['NewEffectiveEndDate'] = $EffectiveEndDate;
		$change_log_data[] = $data;
	}
}

$remove_success = $lpayment->deleteSubsidyIdentityStudents(array('StudentID'=>$StudentID));

$lpayment->addSubsidyIdentityStudentChangeLogs($change_log_data);

$insert_success = $lpayment->addSubsidyIdentityStudents(array('IdentityID'=>$identity_id_ary,'StudentID'=>$StudentID,'EffectiveStartDate'=>$effective_start_date_ary,'EffectiveEndDate'=>$effective_end_date_ary));


if($insert_success){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:student_list.php?IdentityID=".$IdentityID);
?>