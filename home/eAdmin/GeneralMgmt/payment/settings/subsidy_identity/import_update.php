<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
    intranet_closedb();
    header ("Location: /");
    exit();
}

$lpayment_ui = new libpayment_ui();
$linterface = new interface_html();



$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";
$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'], "", 0);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $lpayment_ui->Get_Subsidy_Identity_Import_Finish_Page();
$linterface->LAYOUT_STOP();
intranet_closedb();

