<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if(!isset($IdentityID) || empty($IdentityID) || !isset($StudentID) || count($StudentID)==0){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php".($IdentityID!=''?"?IdentityID=".$IdentityID:""));
	exit;
}
$records = $lpayment->getSubsidyIdentityStudents(array('IdentityID'=>$IdentityID,'StudentID'=>$StudentID,'GetStudentUser'=>false));
foreach($records as $record) {
	$data = array();
	$data['StudentID'] = $record['StudentID'];
	$data['OldIdentityID'] = $record['IdentityID'];
	$data['OldEffectiveStartDate'] = $record['EffectiveStartDate'];
	$data['OldEffectiveEndDate'] = $record['EffectiveEndDate'];
	$data['NewIdentityID'] = '';
	$data['NewEffectiveStartDate'] = '';
	$data['NewEffectiveEndDate'] = '';
	$lpayment->addSubsidyIdentityStudentChangeLogs(array($data));
}
$remove_success = $lpayment->deleteSubsidyIdentityStudents(array('IdentityID'=>$IdentityID,'StudentID'=>$StudentID));

$flashData['return_msg'] = $remove_success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:student_list.php?IdentityID=".$IdentityID);
?>