<?php
// editing by 
/*
 * Created on 2012-10-04
 * 2020-10-19 Ray    : Add PayPriority order
 * 2016-11-21 Carlos : Pay for selected items when $notall=1 and number of $ItemID > 0.
 * 2015-10-28 Carlos : Include staff users to pay.
 * 2015-10-13 Carlos : $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay.
 * 2014-08-20 Carlos : $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2013-10-28 Carlos : KIS - Hide [Current Balance] and [Balance After Pay]
 * 2013-04-15 Carlos : Allow all user status to pay
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$today = date("Y-m-d");

$name_field = getNameFieldByLang("u.");

if($notall == 1 && count($ItemID)>0){
	$pay_selected = 1;
	$item_cond = " AND i.ItemID IN ('".implode("','",$ItemID)."') ";
}else{
	$item_cond = "AND ((DATE_FORMAT(i.StartDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') OR (DATE_FORMAT(i.EndDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate')) ";
}

$sql = "SELECT
			i.ItemID,
			i.Name as ItemName,
			c.Name as CategoryName,
			i.Description,
			i.DefaultAmount,
			i.StartDate,
			i.EndDate,
			s.PaymentID,
			u.UserID,
			$name_field as StudentName,
			u.ClassName,
			u.ClassNumber,
			".$lpayment->getExportAmountFormatDB("s.Amount")." as Amount  
		FROM PAYMENT_PAYMENT_ITEM as i   
		INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON i.ItemID=s.ItemID 
		INNER JOIN INTRANET_USER as u ON u.UserID=s.StudentID 
		LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON (c.CatID = i.CatID) 
		WHERE 
			i.RecordStatus <> 1 AND s.RecordStatus=0 AND u.RecordStatus IN (0,1,2,3) $item_cond  
		ORDER BY i.PayPriority,i.DateInput,u.ClassName,u.ClassNumber+0 ";

$unpaid_records = $lpayment->returnArray($sql);
$x = '';

$UserIDToBalance = array();
if(count($unpaid_records)>0) {
	$involved_userid = Get_Array_By_Key($unpaid_records,'UserID');
	$involved_userid = array_unique($involved_userid);
	
	$sql = "SELECT 
				StudentID,
				".$lpayment->getExportAmountFormatDB("Balance")." as Balance 
			FROM PAYMENT_ACCOUNT 
			WHERE StudentID IN (".implode(",",$involved_userid).")";
	$user_balances = $lpayment->returnArray($sql);
	
	for($i=0;$i<count($user_balances);$i++){
		$UserIDToBalance[$user_balances[$i]['StudentID']] = $user_balances[$i]['Balance'];
	}
}else{
	$x .= '<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">';
		$x .= '<tr>
				<td align="center">';
			$x .= $Lang['ePayment']['NoUnpaidItems'];
		 $x .= '</td>
				</tr>
			</table>';
}

$curItemID = 0;
$payableCount = 0;
for($i=0;$i<count($unpaid_records);$i++) {
	list($item_id, $item_name, $category_name, $description, $default_amount, $start_date, $end_date, $payment_id, $user_id, $student_name, $class_name, $class_number, $amount) = $unpaid_records[$i];
	
	$formatted_amount = '$'.number_format($amount,LIBPAYMENT_AMOUNT_DECIMAIL,".",",");
	$balance = $UserIDToBalance[$user_id];
	$formatted_balance = '$'.number_format($balance,LIBPAYMENT_AMOUNT_DECIMAIL,".",",");
	$balance_afterpay = $lpayment->getExportAmountFormat($balance - $amount);
	$formatted_balance_afterpay = '$'.number_format($balance_afterpay,LIBPAYMENT_AMOUNT_DECIMAIL,".",",");
	$UserIDToBalance[$user_id] = $balance_afterpay;
	
	if($curItemID != $item_id) {
		// Start displaying a item
		$formatted_default_amount = '$'.number_format($default_amount,LIBPAYMENT_AMOUNT_DECIMAIL,".",",");
		
		$css_row = 0;
		$x .= '<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_Payment_Field_PaymentItem.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.htmlspecialchars($item_name).'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_Payment_Field_PaymentCategory.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.htmlspecialchars($category_name).'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_general_description.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.nl2br(htmlspecialchars($description)).'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_Payment_Field_Amount.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.$formatted_default_amount.'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_general_startdate.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.$start_date.'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
		$x .= '<td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;">'.$i_general_enddate.': </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;">'.$end_date.'</td>';
		$x .= '</tr>';
		$x .= '</table>';
		
		$x .= '<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr class="tablebluetop">
					<td class="tabletoplink" style="width:20%;">'.$i_UserName.'</td>
					<td class="tabletoplink" style="width:20%;">'.$i_UserClassName.'</td>
					<td class="tabletoplink" style="width:10%;">'.$i_UserClassNumber.'</td>
					<td class="tabletoplink" style="width:16%;">'.$i_Payment_Field_Amount.'</td>';			
		if(!$isKIS){
			$x .= '<td class="tabletoplink" style="width:16%;">'.$i_Payment_Field_CurrentBalance.'</td>
					<td class="tabletoplink" style="width:16%;">'.$i_Payment_Field_BalanceAfterPay.'</td>';
		}
		$x .= '</tr>';
	}
	
	$css = $css_row % 2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
	if($balance_afterpay >= 0 || $sys_custom['ePayment']['AllowNegativeBalance']){
		$css_red = '';
	}else{
		$css_red = 'style="color:red;"';
	}
	
	$x .= '<tr class="'.$css.'">';
		$x .= '<td class="tabletext" '.$css_red.'>'.$student_name;
		if($sys_custom['ePayment']['AllowNegativeBalance'] || $balance_afterpay >=0){
			$x .= '<input type="hidden" name="PaymentID[]" value="'.$payment_id.'" />';
			$x .= '<input type="hidden" name="ItemID[]" value="'.$item_id.'" />';
			$x .= '<input type="hidden" name="StudentID[]" value="'.$user_id.'" />';
			$payableCount++;
		}
		//else{
		//	$x .= '<input type="hidden" name="NegativePaymentID[]" value="'.$payment_id.'" />';
		//}
		$x .= '</td>';
		$x .= '<td class="tabletext" '.$css_red.'>'.$class_name.'</td>';
		$x .= '<td class="tabletext" '.$css_red.'>'.$class_number.'</td>';
		$x .= '<td class="tabletext" '.$css_red.'>'.$formatted_amount.'</td>';
	if(!$isKIS) {
		$x .= '<td class="tabletext" '.$css_red.'>'.$formatted_balance.'</td>';
		$x .= '<td class="tabletext" '.$css_red.'>'.$formatted_balance_afterpay.'</td>';
	}
	$x .= '</tr>';
	
	$css_row++;
	
	if($item_id != $unpaid_records[$i+1]['ItemID']){
		$x .= '</table><br /><br />';
	}
	
	$curItemID = $item_id;
}

$PAGE_NAVIGATION[] = array($i_Payment_Menu_Settings_PaymentItem,'index.php');
$PAGE_NAVIGATION[] = array($pay_selected? $Lang['ePayment']['PaymentItem'] : $Lang['ePayment']['PayAllItems'],'');

?>
<br />
<form name="form1" action="pay_all_items_update.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td><?=$x?></td>
	</tr>
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td class="tabletextremark">
						<?=$sys_custom['ePayment']['AllowNegativeBalance']?'' : $i_Payment_PaymentItem_Pay_Warning?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?php 
			if($payableCount>0){
				echo $linterface->GET_ACTION_BTN($i_Payment_action_proceed_payment, "submit", ""); 
			}
			?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location.href='index.php?FromDate=".$FromDate."&ToDate=".$ToDate."';") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="FromDate" value="<?=$FromDate?>" />
<input type="hidden" name="ToDate" value="<?=$ToDate?>" />
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>