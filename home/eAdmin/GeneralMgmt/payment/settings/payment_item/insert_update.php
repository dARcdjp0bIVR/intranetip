<?php
// Editing by 
/*
 * 2015-11-26 (Carlos): Modified to include left students.
 * 2015-10-28 (Carlos): Allow add staff users to payment item.
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lidb = new libdb();
$lclass = new libclass();
$lpayment = new libpayment();

### remove U from UXXXX 
for($i=0;$i<count($student);$i++){
	$student[$i] = str_replace('U','',$student[$i]);
}

#student - Array contains the students selected
$student_list = "'".implode("','", $student)."'";


### Step 1 - Double Check The RecordType = 2 AND RecordStatus IN (0,1,2) ### 
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2,3) AND UserID IN ($student_list)";

$temp_passed_student = $lclass->returnVector($sql);

if (sizeof($temp_passed_student) == sizeof($student))
{
	$final_list = "'".implode("','", $student)."'";
	
	### Step 2 - Double Check PAYMENT_PAYMENT_ITEMSTUDENT ###
	$sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND StudentID IN ($final_list)";
	
	$checking = $lclass->returnVector($sql);
	
	if (sizeof($checking) == 0)
	{		
		$lidb->Start_Trans();
		
		for ($i=0; $i<sizeof($student); $i++)
		{
			$sql = " 
					INSERT INTO 
								PAYMENT_PAYMENT_ITEMSTUDENT 
								(ItemID, StudentID, Amount, RecordType, RecordStatus, ProcessingTerminalUser, ProcessingAdminUser, ProcessingTerminalIP, DateInput, DateModified) 
					VALUES 
								('$ItemID', '".$student[$i]."', '$amount', 0, 0, NULL, '$PHP_AUTH_USER', NULL, NOW(), NOW())
					";
			
			$Result['InsertStudentPayment'.$student[$i]] = $lidb->db_db_query($sql);
		}
		
		if (in_array(false,$Result)) {
			$lidb->RollBack_Trans();
			$Msg = $Lang['ePayment']['PaymentStudentItemAddedUnsuccess'];
		}
		else {
			$lidb->Commit_Trans();
			$Msg = $Lang['ePayment']['PaymentStudentItemAddedSuccess'];
			
			if($sys_custom['ePayment']['PaymentItemChangeLog']){
				$logDisplayDetail = '';
				$logHiddenDetail = '';
				
				$name_field = getNameFieldWithClassNumberByLang("u.");
				$sql = "SELECT ppi.Name as ItemName, a.StudentID, a.Amount, c.Balance, a.PaymentID, $name_field as StudentName 
			            FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
							INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
							LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
			                LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
			            WHERE ppi.ItemID='$ItemID' AND a.ItemID='$ItemID' AND a.StudentID IN ($final_list) 
						GROUP BY a.PaymentID";
			    $oldRecords = $lpayment->returnResultSet($sql);
				
				for($i=0;$i<count($oldRecords);$i++){
					$logDisplayDetail .= 'Item: '.$oldRecords[$i]['ItemName'].' Student: '.$oldRecords[$i]['StudentName'].' Amount: $'.$oldRecords[$i]['Amount'] .'<br />';
					$delim = '';
					foreach($oldRecords[$i] as $key => $val){
						$logHiddenDetail .= $delim.$key.': '.$val;
						$delim = ', ';
					}
					$logHiddenDetail .= '<br />';
				}
				
				$logType = $lpayment->getPaymentItemChangeLogType("AddStudent");
				$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);		
			}
		}
	}
	else
	{
		$Msg = $Lang['ePayment']['PaymentStudentItemAddedUnsuccess'];
		//return false;
	}
}
else
{
	$Msg = $Lang['ePayment']['PaymentStudentItemAddedUnsuccess'];
	//return false;
}

intranet_closedb();
header("Location: list.php?ItemID=$ItemID&Msg=".urlencode($Msg));
exit;
?>