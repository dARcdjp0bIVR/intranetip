<?php
// Editing by 
/*
 * 2019-08-07 (Carlos): If using direct pay mode, change undo to refund.
 * 2019-06-21 (Carlos): Do not change balance if payment item was paid by eWallet.
 * 2018-06-15 (Carlos): $sys_custom['ePayment']['PaymentMethod'] clear PaymentMethod and ReceiptRemark.
 * 2017-11-28 (Carlos): Do not change balance for undo TNG payments.
 * 2017-09-18 (Carlos): $sys_custom['ePayment']['TNG'] - mark TNG transaction records as refunded.
 * 2015-10-28 (Carlos): Include staff users. 
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$PaymentID = IntegerSafe($PaymentID);
$ItemID = IntegerSafe($ItemID);

$list = "'".implode("','",$PaymentID)."'";

$lpayment = new libpayment();

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();

$itemName = $lpayment->returnPaymentItemName($ItemID);

$itemName = addslashes($itemName);

/*$sql = "LOCK TABLES
             PAYMENT_PAYMENT_ITEMSTUDENT AS a READ,
             PAYMENT_ACCOUNT AS c READ,
             PAYMENT_PAYMENT_ITEMSTUDENT WRITE,
             PAYMENT_ACCOUNT WRITE,
             PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);*/
$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID ";
	if($use_payment_system){
		$sql .= " ,t.RecordID as TNGRecordID ";
	}
             $sql .= " FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID ";
	if($use_payment_system){
		$sql .= " LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
	}
    $sql .= " WHERE a.RecordStatus = 1 AND a.PaymentID IN ($list)";
$result = $lpayment->returnArray($sql);

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$name_field = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID, $name_field as StudentName 
            FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
                LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
            WHERE a.RecordStatus = 1 AND a.PaymentID IN ($list)";
    $oldRecords = $lpayment->returnResultSet($sql);
}

# Proceed undo here
$Result=array();
$lpayment->Start_Trans();
$more_fields = '';
if($sys_custom['ePayment']['PaymentMethod']){
	$more_fields .= ' PaymentMethod=NULL,ReceiptRemark=NULL, ';
}
for ($i=0; $i<sizeof($result); $i++)
{
     list ($StudentID, $amount, $balance, $paymentID) = $result[$i];
     $balanceAfter = $balance + $amount;
     
     $change_to_record_status = $use_payment_system? 2 : 0; // 2=Refund, 0=Unpaid, 1=Paid
     # 1. Set Payment Record Status to UNPAID
     $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = '$change_to_record_status',PaidTime = NULL,$more_fields ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='".$_SESSION['UserID']."',DateModified=NOW()  WHERE PaymentID = '$paymentID'";
     $Result['Update'.$i.':PAYMENT_PAYMENT_ITEMSTUDENT'] = $lpayment->db_db_query($sql);
	 if($use_payment_system /* && $result[$i]['TNGRecordID'] != '' */){
	 	 # Do not modify balance for TNG transaction
	 	 $balanceAfter = $balance;
	 }else{
	     # 2. Update Account Balance
	     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balanceAfter',
	                    LastUpdateByAdmin = '".$_SESSION['UserID']."',
	                    LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = '$StudentID'";
	     $Result['Update'.$i.':PAYMENT_ACCOUNT'] = $lpayment->db_db_query($sql);
	 }
     # 3. Insert Transaction Record
	
	 $change_to_transaction_type = $use_payment_system? 7 : 6; // 7=Refund, 6=Cancel payment item
	 $details = $use_payment_system? $Lang['ePayment']['Refund']." $itemName" : "$i_Payment_action_cancel_payment $itemName";
	 $refcode_prefix = $use_payment_system? "RF" : "PAY";
     $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
             (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
             VALUES
             ('$StudentID', $change_to_transaction_type,'$amount','$paymentID','$balanceAfter',NOW(),'$details')";
     $Result['Insert'.$i.':PAYMENT_OVERALL_TRANSACTION_LOG'] = $lpayment->db_db_query($sql);
     $logID = $lpayment->db_insert_id();
     $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('$refcode_prefix',LogID) WHERE LogID = '$logID'";
     $Result['Update'.$i.':PAYMENT_OVERALL_TRANSACTION_LOG'] = $lpayment->db_db_query($sql);
	/*
	if($use_payment_system){
		// Mark charge status as Refunded
		$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='4',RefundStatus=NULL WHERE PaymentID='".$paymentID."'";
		$lpayment->db_db_query($sql);
	}
	*/
}

$pay_msg = 3;

/*$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);*/

if (in_array(false,$Result)) {
	$lpayment->RollBack_Trans();
	$Msg = $Lang['ePayment']['PayUndoUnsuccess'];
}
else {
	$lpayment->Commit_Trans();
	$Msg = $Lang['ePayment']['PayUndoSuccess'];
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$logDisplayDetail = stripslashes($itemName).'<br />';
		$logHiddenDetail = $ItemID.' | '.stripslashes($itemName).'<br />';
		for($i=0;$i<count($oldRecords);$i++){
			$logDisplayDetail .= 'User: '.$oldRecords[$i]['StudentName'].' Balance: $'.$oldRecords[$i]['Balance'].' Amount: $'.$oldRecords[$i]['Amount'] .'<br />';
			$delim = '';
			foreach($oldRecords[$i] as $key => $val){
				$logHiddenDetail .= $delim.$key.': '.$val;
				$delim = ', ';
			}
			$logHiddenDetail .= '<br />';
		}
		$logType = $lpayment->getPaymentItemChangeLogType("UndoPay");
		$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
	}
}

intranet_closedb();
header ("Location: list.php?ItemID=$ItemID&ClassName=".urlencode($ClassName)."&pageNo=$pageNo&order=$order&field=$field&pay=$pay_msg&Msg=".urlencode($Msg));
?>