<?php
// Editing by 
/*
 * 2015-04-30 (Carlos): Created for student_payment_item.php
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$row_index = $_REQUEST['row_index'];

$x = '<tr class="AddValueTableRow">
		<td>'.$linterface->GET_TEXTBOX_NUMBER('AddValueAmount_'.$row_index, 'AddValueAmount_'.$row_index, '', '', array('onchange'=>'recalculateAddValues();')).'<div id="WarnAddValueAmount_'.$row_index.'" style="color:#FF0000;display:none;">'.$i_Payment_DepositAmountWarning.'</div></td>
		<td>'.$linterface->GET_TEXTBOX('AddValueRef_'.$row_index, 'AddValueRef_'.$row_index, '').'</td>
		<td>'.$linterface->GET_DATE_PICKER("AddValueDate_".$row_index,date("Y-m-d")).'<div id="WarnAddValueDate_'.$row_index.'" style="color:#FF0000;display:none;">'.$i_invalid_date.'</div></td>
		<td>'.$linterface->Get_Time_Selection("AddValueTime_".$row_index, date("H:i:s"), '').'</td>
		<td><span class="table_row_tool"><a class="delete" href="javascript:void(0);" onclick="removeAddValueRow(this);" title="'.$Lang['Btn']['Delete'].'"></a></span></td>
	 </tr>';

echo $x;

intranet_closedb();
?>