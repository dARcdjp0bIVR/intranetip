<?php
# Editing by 

###########################################
#	Date:	2019-08-07  Carlos
#			If using direct pay mode, change undo to refund.
#
#	Date:	2015-11-26  Carlos
#			Include left students.
#
#	Date: 	2015-10-28  Carlos
#			Include staff users. 
#
#	Date:	2014-08-20  Carlos
#			$sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
#
#	Date:	2013-10-28	Carlos
#			KIS - hide [Current Balance] and [Balance After Pay]
#	
#	Date:	2012-08-02	YatWoon
#			fixed: not display $xx,xxx.xx amount format for Balance and Balance after
# 
###########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$is_direct_pay = $lpayment->isEWalletDirectPayEnabled();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($is_direct_pay?$Lang['ePayment']['Refund']:$i_Payment_action_undo);

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

# Get List
$list = "'".implode("','",$PaymentID)."'";
$namefield = getNameFieldByLang("b.");
$sql = "SELECT a.PaymentID, CONCAT($namefield,IF(b.RecordStatus='3','[".$Lang['Status']['Left']."]','')), b.ClassName, b.ClassNumber, a.Amount, c.Balance
               FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus in (0,1,2,3) 
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
               WHERE a.RecordStatus = 1 AND a.PaymentID IN ($list)
               ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
$result = $lpayment->returnArray($sql,6);
if (sizeof($result)==0)
{
    header("Location: list.php?ItemID=$ItemID");
    exit();
}
$allEnough = true;
$x = "";
for ($i=0; $i<sizeof($result); $i++)
{
     list ($PaymentID, $name, $class, $classnum, $amount, $balance) = $result[$i];
     $balanceAfter = $balance + $amount;

     $balance = $lpayment->getWebDisplayAmountFormat($balance);
     $balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
     
     $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
     $x .= "<tr class=\"$css\">\n";
     $x .= "<td class\"tabletext\">$name</td>\n";
     $x .= "<td class\"tabletext\">$class</td>\n";
     $x .= "<td class\"tabletext\">$classnum</td>\n";
     $x .= "<td class\"tabletext\">$amount</td>\n";
   if(!$isKIS && !$is_direct_pay){
     $x .= "<td class\"tabletext\">$balance</td>\n";
     $x .= "<td class\"tabletext\">$balanceAfter</td>\n";
   }
     $x .= "</tr>\n";
     $HiddenFields .= "<input type=\"hidden\" name=\"PaymentID[]\" value=\"$PaymentID\">\n";
}

$itemName = $lpayment->returnPaymentItemName($ItemID);

$linterface->LAYOUT_START();
?>

<form name="form1" action="student_undopay_confirm.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr class="tabletop">
					<td class="tabletoplink"><?= $i_UserName ?></td>
					<td class="tabletoplink"><?= $i_UserClassName ?></td>
					<td class="tabletoplink"><?= $i_UserClassNumber ?></td>
					<td class="tabletoplink"><?= $i_Payment_Field_Amount ?></td>
			<?php if(!$isKIS && !$is_direct_pay){ ?>
					<td class="tabletoplink"><?= $i_Payment_Field_CurrentBalance ?></td>
					<td class="tabletoplink"><?= $i_Payment_Field_BalanceAfterUndo ?></td>
			<?php } ?>
				</tr>
				<?=$x?>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
		<? if ($allEnough) { ?>
			<?= $linterface->GET_ACTION_BTN($is_direct_pay? $Lang['Btn']['Confirm'] : $i_Payment_action_proceed_undo, "submit", "") ?>
		<? }  ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()") ?>
		</td>
	</tr>
</table>
<?=$HiddenFields?>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
