<?php
// Editing by

######################################### Change Log ###############################################
# 2020-05-22 by Ray:    Change summary display
# 2020-01-06 by Bill:   Display '#' for left students   [2019-1220-1013-05235]
# 2019-08-07 by Carlos: If using direct pay mode, change undo to refund.
# 2019-06-17 by Carlos: Added Notice Payment Method column if using payment gateway.
# 2019-05-30 by Carlos: Added display column [Student Subsidy Identity].
# 2018-02-21 by Carlos: Added [Send payment notification] and [Send reminder notification] table tool buttons.
# 2016-07-08 by Carlos: Display Remarks(General remark and payment remark) in table column. 
# 2015-11-26 by Carlos: Added student filter option [Left student(s)].
# 2015-10-28 by Carlos: rename words about students to general term users.
# 2015-08-04 by Carlos: Search keyword would also count last modified user name.
# 2015-07-07 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added Payment Method filter
# 2014-09-17 by Carlos: $sys_custom['ePayment']['ThermalPaperReceipt'] - Added print thermal paper receipt function
# 2014-08-20 by Carlos: $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
# 2014-06-17 by Carlos: Added [Print Receipt] button
# 2014-05-23 by Carlos: Added remark to search condition
# 2014-03-24 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
# 2014-01-30 by Carlos: Fix summary query of subsidy amount and number of subsidy students
# 2013-11-25 by Carlos: Added payment item remark button to the dbtable
# 2013-10-18 by Carlos: Modified sql to display multiple soure of subsidy in csv format
# 2012-06-08 by Carlos: Display Delete button for RecordStatus [All] or [Unpaid], and go to confirm page to truely proceed deletion
# 2010-02-10 by Carlos: Split the Student name with class name class number into three column
#
####################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($ItemID == "")
{
    header("Location: index.php");
    exit();
}

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_item_setting_list_page_number != $pageNo && $pageNo != "")
{
	setcookie("ck_payment_item_setting_list_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_item_setting_list_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_payment_item_setting_list_page_number != "")
{
	$pageNo = $ck_payment_item_setting_list_page_number;
}

if ($ck_payment_item_setting_list_page_order != $order && $order != "")
{
	setcookie("ck_payment_item_setting_list_page_order", $order, 0, "", "", 0);
	$ck_payment_item_setting_list_page_order = $order;
}
else if (!isset($order) && $ck_payment_item_setting_list_page_order != "")
{
	$order = $ck_payment_item_setting_list_page_order;
}

if ($ck_payment_item_setting_list_page_field != $field && $field != "")
{
	setcookie("ck_payment_item_setting_list_page_field", $field, 0, "", "", 0);
	$ck_payment_item_setting_list_page_field = $field;
}
else if (!isset($field) && $ck_payment_item_setting_list_page_field != "")
{
	$field = $ck_payment_item_setting_list_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$ItemID = IntegerSafe($ItemID);

$linterface = new interface_html();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$use_tng = $sys_custom['ePayment']['TNG'];
//$use_alipay = $sys_custom['ePayment']['Alipay'];
//$use_topup = $lpayment->isEWalletTopUpEnabled();
$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
if($sys_custom['ttmss_ePayment']) {
	$use_payment_system = false;
}

$itemStatus = $lpayment->returnPaymentItemStatus($ItemID);

$student_status_cond="";
if($StudentStatus==1){          ## Removed Student
	$student_status_cond =" AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){    ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL AND b.RecordStatus IN (0,1,2) ";
}else if($StudentStatus==3){
	$student_status_cond = " AND b.UserID IS NOT NULL AND b.RecordStatus='3' ";
}else{                          ## All Student
	$student_status_cond ="";
}

$conds = "";
if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = '$RecordStatus'";
}
if ($ClassName != "")
{
    $conds .= " AND (b.Classname = '$ClassName' OR c.Classname='$ClassName')";
}

if($sys_custom['ePayment']['PaymentMethod'] && $PaymentMethod != '') {
	$conds .= " AND a.PaymentMethod='$PaymentMethod' ";
}

//$namefield = getNameFieldWithClassNumberByLang("b.");
$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else{
    $archive_namefield = "c.EnglishName";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
}else {
	$Inchargearchive_namefield = " IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}
$last_updated_field = "            IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',
											CASE 
												WHEN e.UserID IS NULL AND f.UserID IS NULL THEN a.ProcessingAdminUser 
												WHEN e.UserID IS NOT NULL THEN ".$Inchargenamefield." 
												ELSE ".$Inchargearchive_namefield." 
											END,
											IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='', a.ProcessingTerminalUser, '&nbsp;')
                                   )";

$sql = "SELECT
            IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName) as ClassName,
            IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber) as ClassNumber,
            IF((b.UserID IS NULL AND c.UserID IS NOT NULL),
                    CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), 
                    CONCAT(IF(b.UserID IS NOT NULL AND b.RecordStatus='3','<font color=red>#</font>',''), $namefield)
            ) as StudentName,	
        ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
		IF(a.Remark IS NOT NULL AND a.Remark <> '',a.Remark,'".$Lang['General']['EmptySymbol']."') as Remark,
        IF(a.SubsidyAmount IS NULL OR a.SubsidyAmount=0,".$lpayment->getWebDisplayAmountFormatDB('0').",".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").") AS SubsidyAmount,
        IFNULL(GROUP_CONCAT(d.UnitName SEPARATOR ', '),'&nbsp;') as UnitName,
		i.IdentityName, ";
if($sys_custom['ePayment']['PaymentMethod'])
{
    $sql .= "IF(a.RecordStatus = 0, '', ";
	$sql.=" CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
		/*
				WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' */
	}
	if($sys_custom['ttmss_ePayment']) {
		$sql .= "
		ELSE
            CASE
                 WHEN a.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
                 WHEN a.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
                 ELSE '".$Lang['ePayment']['NA']."' 
            END 
		END) as PaymentMethod, ";
	} else {
		$sql .= "   ELSE '" . $Lang['ePayment']['NA'] . "' 
			        END) as PaymentMethod,";
	}
	$sql .= "a.ReceiptRemark,";
}

$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query(trim($keyword));

$sql .= " IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid',IF(a.RecordStatus=2,'".$Lang['ePayment']['Refunded']."','<span style=\"color:red\">$i_Payment_PaymentStatus_Unpaid</span>')) as PayStatus,";
if($use_payment_system)
{
	$payment_method_sql = "";
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, false);
		foreach ($service_provider_list as $k => $temp) {
			$payment_method_sql .= "WHEN potl.Sp='" . strtolower($k) . "' THEN '" . $temp . "'";
		}
	}

	$sql .= "CASE
                WHEN a.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
                WHEN a.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
                $payment_method_sql
                ELSE ''
            END as NoticePaymentMethod, ";
}
		 $sql .= " ".$last_updated_field." as LastUpdateBy,
                    IF(a.PaidTime IS NULL,'&nbsp;',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s')) as PaidTime,
					CONCAT('<input type=checkbox name=PaymentID[] value=', a.PaymentID ,'><input type=\"checkbox\" name=\"PayStatus[]\" value=\"',a.RecordStatus,'\" style=\"display:none\" />'),
				    a.PaymentID 
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a 
             LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID 
			 LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as s ON s.PaymentID=a.PaymentID AND s.UserID=a.StudentID 
             LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (s.SubsidyUnitID=d.UnitID) 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s2 ON s2.StudentID=a.StudentID 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s2.IdentityID AND i.RecordStatus='1' 
             LEFT OUTER JOIN INTRANET_USER as e ON e.UserID = a.ProcessingAdminUser
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS f ON f.UserID = a.ProcessingAdminUser 
             LEFT JOIN PAYMENT_TNG_TRANSACTION as potl ON potl.RecordID=(SELECT RecordID FROM PAYMENT_TNG_TRANSACTION WHERE PaymentID=a.PaymentID ORDER BY InputDate DESC LIMIT 1)
             
         WHERE 
              a.ItemID = '$ItemID' AND 
              (b.EnglishName LIKE '%$safe_keyword%' OR
               b.ChineseName LIKE '%$safe_keyword%' OR
               b.ClassName LIKE '%$safe_keyword%' OR
               b.ClassNumber LIKE '%$safe_keyword%' OR
               c.EnglishName LIKE '%$safe_keyword%' OR
               c.ChineseName LIKE '%$safe_keyword%' OR
               c.ClassName LIKE '%$safe_keyword%' OR
               c.ClassNumber LIKE '%$safe_keyword%' OR
               d.UnitName LIKE '%$safe_keyword%' OR 
			   a.Remark LIKE '%$safe_keyword%' OR 
			   e.ChineseName LIKE '%$safe_keyword%' OR 
			   e.EnglishName LIKE '%$safe_keyword%' 
              )
              $conds $student_status_cond 
         GROUP BY a.PaymentID ";

$ldb = new libdb();

# Grab Class list
$class_sql = "SELECT DISTINCT b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			  WHERE
					a.ItemID = '$ItemID' AND
					b.ClassName IS NOT NULL
			  ORDER BY b.ClassName";
$classes = $ldb->returnVector($class_sql);
$select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()",$ClassName,1);

# Grab Summary
$summary_sql = "SELECT a.Amount, a.SubsidyAmount,GROUP_CONCAT(d.SubsidyUnitID) as SubsidyUnitID,a.RecordStatus 
                FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	            LEFT JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID 
				LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as d ON (a.PaymentID=d.PaymentID) 
				LEFT JOIN PAYMENT_SUBSIDY_UNIT as e ON e.UnitID=d.SubsidyUnitID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID = a.ProcessingAdminUser 
	            WHERE
	              a.ItemID = '$ItemID' AND
	              (b.EnglishName LIKE '%$safe_keyword%' OR
	               b.ChineseName LIKE '%$safe_keyword%' OR
	               b.ClassName LIKE '%$safe_keyword%' OR
	               b.ClassNumber LIKE '%$safe_keyword%' OR
	               c.EnglishName LIKE '%$safe_keyword%' OR
	               c.ChineseName LIKE '%$safe_keyword%' OR
	               c.ClassName LIKE '%$safe_keyword%' OR
	               c.ClassNumber LIKE '%$safe_keyword%' OR
	               e.UnitName LIKE '%$safe_keyword%' OR 
				   a.Remark LIKE '%$safe_keyword%' OR 
				   u2.ChineseName LIKE '%$safe_keyword%' OR 
				   u2.EnglishName LIKE '%$safe_keyword%' 
	            ) 
	            $conds $student_status_cond
	 			GROUP BY a.PaymentID ";

$count_paid = 0;
$count_unpaid = 0;
$count_refund = 0;
$sum_refund = 0;
$sum_paid = 0;
$sum_unpaid = 0;
$sum_subsidy_total = 0;
$count_subsidy_total = 0;
$temp = $ldb->returnArray($summary_sql,4);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($amount,$sub_amount,$sub_unit_id,$status) = $temp[$i];
     if ($status == 1)
     {
         $count_paid++;
         $sum_paid += $amount;
	 } else {
		 if($status == 2) {
			 $count_refund++;
			 $sum_refund += $amount;
		 } else {
			 $count_unpaid++;
			 $sum_unpaid += $amount;
		 }
     }
     if($sub_unit_id>0){
       	$count_subsidy_total++;
	 }
     if($sub_amount>0){
     	$sum_subsidy_total+=$sub_amount;
     }
}
$sum_total = $sum_paid + $sum_unpaid;
$count_total = $count_paid + $count_unpaid;
$sum_item_total = number_format($sum_paid+$sum_subsidy_total, 2);
$sum_paid = number_format($sum_paid,2);
$sum_unpaid = number_format($sum_unpaid,2);
$sum_total = number_format($sum_total,2);
$sum_subsidy_total = number_format($sum_subsidy_total,2);
$sum_refund = number_format($sum_refund,2);

$infobar = "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_Payment_ItemSummary)."</td></tr>\n";
/*
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemPaid</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$ $sum_paid ($count_paid ".$Lang['ePayment']['users'].")</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemUnpaid</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_unpaid ($count_unpaid ".$Lang['ePayment']['users'].")</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemTotal</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_total ($count_total ".$Lang['ePayment']['users'].")</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Subsidy_Total_Subsidy_Amount</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_subsidy_total ($count_subsidy_total ".$Lang['ePayment']['users'].")</td></tr>\n";
*/

$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Field_Chargeable_Amount</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$ $sum_total ($count_total $i_Payment_Students)</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemPaid</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_paid ($count_paid $i_Payment_Students)</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Subsidy_Total_Subsidy_Amount</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_subsidy_total ($count_subsidy_total $i_Payment_Students)</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemTotal</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ ".$sum_item_total."</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemUnpaid</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_unpaid ($count_unpaid $i_Payment_Students)</td></tr>\n";

if($lpayment->isEWalletDirectPayEnabled()) {
	$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_RefundAmount</td>\n";
	$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $sum_refund ($count_refund $i_Payment_Students)</td></tr>\n";
}

# TABLE INFO
//debug_r($sql);
$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("ClassName,ClassNumber","ClassNumber","StudentName","a.Amount","a.SubsidyAmount","UnitName","a.RecordStatus","LastUpdateBy","PaidTime");
$field_array = array("ClassName","ClassNumber","StudentName","a.Amount","a.Remark","a.SubsidyAmount","UnitName","i.IdentityName");
if($sys_custom['ePayment']['PaymentMethod']){
	$field_array = array_merge($field_array, array("PaymentMethod","a.ReceiptRemark"));
	//$li->field_array = array("ClassName","ClassNumber","StudentName","a.Amount","a.Remark","a.SubsidyAmount","UnitName","i.IdentityName","PaymentMethod","a.ReceiptRemark","a.RecordStatus","LastUpdateBy","PaidTime");
}
$field_array = array_merge($field_array,array("a.RecordStatus"));
//else{
	//$li->field_array = array("ClassName","ClassNumber","StudentName","a.Amount","a.Remark","a.SubsidyAmount","UnitName","i.IdentityName","a.RecordStatus","LastUpdateBy","PaidTime");
//}
if($use_payment_system){
	$field_array = array_merge($field_array,array("NoticePaymentMethod"));
}

$field_array = array_merge($field_array, array("LastUpdateBy","PaidTime"));
$li->field_array = $field_array;

if($field==0)
{
	$li->fieldorder2 = ", ClassNumber asc";
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array_fill(0,sizeof($li->field_array),0);
$li->wrap_array = array_fill(0,sizeof($li->field_array),0);
$li->IsColOff = 2;

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 1;
}

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserName)."</td>\n";
//$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['General']['Remark'])."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Payment_Subsidy_Amount)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Subsidy_Unit)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['ePayment']['SubsidyIdentity'])."</td>\n";
if($sys_custom['ePayment']['PaymentMethod']){
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['ePayment']['PaymentMethod'])."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['ePayment']['PaymentRemark'])."</td>\n";
}
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_general_status)."</td>\n";
if($use_payment_system){
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['eNotice']['PaymentNoticePaymentMethod'])."</td>\n";
}
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_general_last_modified_by)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("PaymentID[]")."</td>\n";

$student_status = "<SELECT name=StudentStatus onChange=this.form.submit()>\n";
$student_status .= "<OPTION value='-1' ".($StudentStatus=="-1"? "SELECTED":"").">$i_status_all</OPTION>\n";
$student_status .= "<OPTION value='1' ".(isset($StudentStatus) && $StudentStatus=="1"? "SELECTED":"").">$i_Payment_StudentStatus_Removed</OPTION>\n";
$student_status .= "<OPTION value='2' ".(isset($StudentStatus) && $StudentStatus=="2"? "SELECTED":"").">$i_Payment_StudentStatus_NonRemoved</OPTION>\n";
$student_status .= "<OPTION value='3' ".(isset($StudentStatus) && $StudentStatus=="3"? "SELECTED":"").">".$Lang['General']['LeftStudents']."</OPTION>\n";
$student_status .= "</SELECT>\n";

$select_status = "<select name='RecordStatus' onChange='this.form.submit()'>\n";
$select_status .= "<option value='' ".(!isset($RecordStatus) || $RecordStatus==""? "SELECTED":"").">$i_status_all</option>\n";
$select_status .= "<option value='0' ".(isset($RecordStatus) && $RecordStatus=="0"? "SELECTED":"").">$i_Payment_PaymentStatus_Unpaid</option>\n";
$select_status .= "<option value='1' ".(isset($RecordStatus) && $RecordStatus==1? "SELECTED":"").">$i_Payment_PaymentStatus_Paid</option>\n";
$select_status .= "</select>\n";

if($sys_custom['ePayment']['PaymentMethod']){
	$select_payment_method = $lpayment->getPaymentMethodSelection(array(), " id=\"PaymentMethod\" name=\"PaymentMethod\" onchange=\"document.form1.submit();\" ", $PaymentMethod, 0, 0, $Lang['General']['All']);
}

if($itemStatus!=1){
	$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('insert.php?ItemID=$ItemID')","","","","",0);
	$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php?ItemID=$ItemID')","","","","",0);
}
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:newWindowExport(document.form1,'export.php?ItemID=$ItemID')","","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);
if($sys_custom['ttmss_ePayment']) {
    // empty
} else {
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:printReceipt();", $Lang['ePayment']['PrintReceipt'], "", "", "", 0);
}
if($sys_custom['ePayment']['ThermalPaperReceipt']){
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:printThermalPaperReceipt();",$Lang['ePayment']['PrintThermalPaperReceipt'],"","","",0);
}

$filterbar .= $select_class."&nbsp";
$filterbar .= $select_status.($sys_custom['ePayment']['PaymentMethod']?"&nbsp;".$select_payment_method:'')."&nbsp;".$student_status;

$table_tool_pay .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkAlert(document.form1,'PaymentID[]','student_pay.php','$i_Payment_alert_forcepay')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pay.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_pay
					</a>
				</td>";
$table_tool_pay .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AlertPost(document.form1,'bulk_student_pay.php?ItemID=".$ItemID."','$i_Payment_alert_forcepayall');\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pay.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$Lang['ePayment']['PayAll']."
					</a>
				</td>";
$table_tool_pay .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool_undo .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkAlert(document.form1,'PaymentID[]','student_undopay.php','".($use_payment_system?$i_Payment_alert_refund:$i_Payment_alert_undopay)."')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_undo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".($use_payment_system? $Lang['ePayment']['Refund'] : $button_undo)."
					</a>
				</td>";
$table_tool_edit .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool_edit .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'PaymentID[]','student_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$table_tool_remove .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'PaymentID[]','student_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

if($plugin['eClassApp']){
	$table_tool_send_payment_notification = "<td nowrap=\"nowrap\">
												<a href=\"javascript:checkSendPaymentNotification();\" class=\"tabletool\" title=\"".$Lang['ePayment']['SendPaymentNotificationTooltipDescription']."\">
													<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
													".$Lang['ePayment']['SendPaymentNotification']."
												</a>
											</td>";
	
	$table_tool_send_reminder_notification = "<td nowrap=\"nowrap\">
												<a href=\"javascript:checkSendReminderNotification();\" class=\"tabletool\" title=\"".$Lang['ePayment']['SendReminderNotificationTooltipDescription']."\">
													<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
													".$Lang['ePayment']['SendReminder']."
												</a>
											</td>";
}

if (!isset($RecordStatus) || $RecordStatus == "")    # ALL
{
     $table_tool .= "$table_tool_pay $table_tool_undo $table_tool_send_payment_notification $table_tool_send_reminder_notification $table_tool_edit $table_tool_remove";
}
else if ($RecordStatus == 1)
{
     $table_tool .= "$table_tool_undo $table_tool_send_payment_notification";
}
else
{
     $table_tool .= "$table_tool_pay $table_tool_send_reminder_notification $table_tool_edit $table_tool_remove";
}

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

if ($paid == 1)
{
    $xmsg = "$i_Payment_con_PaidNoEdit";
}

$itemName = $lpayment->returnPaymentItemName($ItemID);

if ($pay == 1)
{
    $xmsg = "$i_Payment_con_PaymentProcessed";
}
else if ($pay == 2)
{
     $xmsg = "$i_Payment_con_PaymentProcessFailed";
}
else if ($pay == 3)
{
     $xmsg = "$i_Payment_con_PaymentUndo";
}

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$PAGE_NAVIGATION[] = array($itemName);

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
}
?>

<script language="Javascript">
function newWindowExport(obj,url)
{
     obj.target = "_blank";
     checkPost(obj,'export.php?ItemID=<?=$ItemID?>');

     obj.target = "";
     obj.action = "";
     obj.method = "GET";
}
// override the common one
function checkEdit(obj,element,page){
    if(countChecked(obj,element)>=1) {
            obj.action=page;
            obj.submit();
    } else {
            alert(globalAlertMsg2);
    }
}

function openPrintPage()
{
	general_values = "&keyword=<?=urlencode($keyword)?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	page_values = "ItemID=<?=urlencode($ItemID)?>&RecordStatus=<?=urlencode($RecordStatus)?>&StudentStatus=<?=urlencode($StudentStatus)?>&itemStatus=<?=urlencode($itemStatus)?>&ClassName=<?=urlencode($ClassName)?><?=$sys_custom['ePayment']['PaymentMethod']?"&PaymentMethod=".urlencode($PaymentMethod):""?>";
	url = "print.php?"+page_values+general_values;
    newWindow(url,8);
}

function showHideRemarkLayer(obj, payment_id)
{
	var btnObj = $(obj);
	var remarkDiv = $('div#remark'+payment_id);
	if(remarkDiv.length == 0){
		$.get(
			'ajax_load_payment_item_remark_layer.php?PaymentID='+payment_id,
			{},
			function(ReturnData){
				btnObj.after(ReturnData);
			}
		);
	}
}

function printReceipt()
{
	var objs = $('input[name="PaymentID\\[\\]"]:checked"');
	var payment_id_ary = [];
	
	for(var i=0;i<objs.length;i++){
		var paid = $(objs.get(i)).closest('tr').find('td:eq(<?=$sys_custom['ePayment']['PaymentMethod']?"8":"7"?>) > span').length == 0;
		if(paid){
			payment_id_ary.push(objs.get(i).value);
		}
	}
	if(payment_id_ary.length == 0){
		alert('<?=$Lang['ePayment']['PleaseSelectAtLeastOnePaidStudent']?>');
		return;
	}
	
	var url = '';
	var winType = '36';
	var win_name = 'intranet_popup'+winType;
	
	var tmp_form_html = '<form id="tmp_form" name="tmp_form" method="post" action="payment_item_receipt.php" target="'+win_name+'">';
	for(var i=0;i<payment_id_ary.length;i++){
		tmp_form_html += '<input type="hidden" name="PaymentID[]" value="'+payment_id_ary[i]+'" />';
	}
	tmp_form_html += '</form>';
	
	if($('#tmp_form').length == 0){
		$('form[name=form1]').after(tmp_form_html);
	}
	newWindow(url, winType);
	$('#tmp_form').submit();
	
	if($('#tmp_form').length > 0){
		$('#tmp_form').remove();
	}
}

<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
function printThermalPaperReceipt()
{
	var objs = $('input[name="PaymentID\\[\\]"]:checked"');
	var payment_id_ary = [];
	
	for(var i=0;i<objs.length;i++){
		var paid = $(objs.get(i)).closest('tr').find('td:eq(<?=$sys_custom['ePayment']['PaymentMethod']?"8":"7"?>) > span').length == 0;
		if(paid){
			payment_id_ary.push(objs.get(i).value);
		}
	}
	if(payment_id_ary.length == 0){
		alert('<?=$Lang['ePayment']['PleaseSelectAtLeastOnePaidStudent']?>');
		return;
	}
	
	var url = '';
	var winType = '0';
	var win_name = 'intranet_popup'+winType;
	
	var tmp_form_html = '<form id="tmp_form" name="tmp_form" method="post" action="payment_item_thermal_paper_receipt.php" target="'+win_name+'">';
	for(var i=0;i<payment_id_ary.length;i++){
		tmp_form_html += '<input type="hidden" name="PaymentID[]" value="'+payment_id_ary[i]+'" />';
	}
	tmp_form_html += '</form>';
	
	if($('#tmp_form').length == 0){
		$('form[name=form1]').after(tmp_form_html);
	}
	newWindow(url, winType);
	$('#tmp_form').submit();
	
	if($('#tmp_form').length > 0){
		$('#tmp_form').remove();
	}
}
<?php } ?>

<?php if($plugin['eClassApp']): ?>
function checkSendPaymentNotification()
{
	var objs = $('input[name="PaymentID[]"]:checked"');
	var paid_payment_id_ary = [];
	var not_paid_payment_id_ary = [];
	
	for(var i=0;i<objs.length;i++){
		var paid = $(objs.get(i)).closest('tr').find('input[name="PayStatus[]"]').val() == '1';
		if(paid){
			paid_payment_id_ary.push(objs.get(i).value);
		}else{
			not_paid_payment_id_ary.push(objs.get(i).value);
		}
	}
	if(paid_payment_id_ary.length == 0 || not_paid_payment_id_ary.length > 0){
		alert('<?php echo $Lang['ePayment']['SendPaymentNotificationSelectionWarning']?>');
		return;
	}
	
	var form_obj = $('#form1');
	form_obj.append('<input type="hidden" name="PushMessageType" id="PushMessageType" value="1" />');

	var old_action = document.form1.action;
	var old_method = document.form1.method;
	document.form1.action = 'prepare_send_push_message.php';
	document.form1.method = 'post';
	document.form1.submit();
	document.form1.action = old_action;
	document.form1.method = old_method;
	$('#PushMessageType').remove();
}

function checkSendReminderNotification()
{
	var objs = $('input[name="PaymentID[]"]:checked"');
	var paid_payment_id_ary = [];
	var not_paid_payment_id_ary = [];
	
	for(var i=0;i<objs.length;i++){
		var paid = $(objs.get(i)).closest('tr').find('input[name="PayStatus[]"]').val() == '1';
		if(paid){
			paid_payment_id_ary.push(objs.get(i).value);
		}else{
			not_paid_payment_id_ary.push(objs.get(i).value);
		}
	}
	if(not_paid_payment_id_ary.length == 0 || paid_payment_id_ary.length >0 ){
		alert('<?php echo $Lang['ePayment']['SendReminderNotificationSelectionWarning']?>');
		return;
	}
	
	var form_obj = $('#form1');
	form_obj.append('<input type="hidden" name="PushMessageType" id="PushMessageType" value="2" />');

	var old_action = document.form1.action;
	var old_method = document.form1.method;
	document.form1.PushMessageType.value = '2';
	document.form1.action = 'prepare_send_push_message.php';
	document.form1.method = 'post';
	document.form1.submit();
	document.form1.action = old_action;
	document.form1.method = old_method;
	$('#PushMessageType').remove();
}
<?php endif; ?>
</script>

<br />
<form name="form1" id="form1" method="get">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar?>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
		<?php if($itemStatus!=1){?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr><?=$table_tool?></tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		<?php } ?>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			<?=$i_Payment_Note_StudentRemoved?><br/>
            <?=$Lang['General']['LeftStudentField']?>
		</td>
	</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>