<?php
# using: 

##################################
#   2020-03-03 (Ray):   Added Subsidy
#	2016-06-22 (Carlos): $sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] - cust to allow input negative amount for payment item.
#	2015-11-26 (Carlos): [ip2.5.7.1.1] Allow import left students.
#	2015-10-28 (Carlos): [ip2.5.6.12.1] Allow to add staff users to payment item.
#	2015-08-20 (Carlos): [ip2.5.6.10.1] Changed to use UserID as record key.
#	2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
#	2014-07-08 (Carlos): Check empty string or negative amount as invalid, allow amount to be 0 
#	2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
#	2013-11-26 (Carlos): Added [Remark] import field
#	
#	Date:	2011-11-23	YatWoon
#			Improved: display student name and user login in the error display page
#			Fixed: empty amount should not cater as an error
#
##################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$lo = new libfilesystem();
$limport = new libimporttext();
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($button_import);

$itemName = $lpayment->returnPaymentItemName($ItemID);

$file_format1 = array("Class Name","Class Number","Amount","Remark");
if($sys_custom['ePayment']['PaymentMethod']){
	$file_format1[] = "Payment Method";
}
$file_format1[] = "Source Of Subsidy Code";
$file_format1[] = "Subsidy Amount";

$file_format2 = array("User Login","Amount","Remark");
if($sys_custom['ePayment']['PaymentMethod']){
	$file_format2[] = "Payment Method";
}
$file_format2[] = "Source Of Subsidy Code";
$file_format2[] = "Subsidy Amount";

if($format==1){
        $file_format = $file_format1;
		$flagAry = array(1, 1, 0, 1, 1);
}elseif($format==2){
        $file_format = $file_format2;
		$flagAry = array(1, 0, 1, 1);
}
if($sys_custom['ePayment']['PaymentMethod']){
	$flagAry[] = 1;
	
	$paymentMethodReverseMap = array();
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $k => $v){
		$paymentMethodReverseMap[$v] = $k;
	}
}
$flagAry[] = 1;
$flagAry[] = 1;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?ItemID=$ItemID&failed=2");
        exit();
}  else {
        $ext = strtoupper($lo->file_ext($filename));
        if ($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           //$col_name = array_shift($data);                   # drop the title bar
		  
		  #20130902 Siuwan Get data from csv with reference
			/*$data = $limport->GET_IMPORT_TXT($filepath);
           //debug_r($data); die;
		   	$col_name = array_shift($data); */
			$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, "", "", $file_format, $flagAry);
			$col_name = array_shift($data); 
           #Check file format
           $format_wrong = false;

           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: import.php?ItemID=$ItemID&failed=2");
               exit();
           }
        # Get Existing non-paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND RecordStatus = 0";
        $unpaid = $li->returnVector($sql);

        # Get Existing paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND RecordStatus = 1";
        $paid = $li->returnVector($sql);
        
        $duplicated_list = array();
                   
         # build student array
         if($format==1){
       	  	$t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber, ". getNameFieldByLang("") ."  FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN(0,1,2,3)";
         }else {
         	$t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber, ". getNameFieldByLang("") ."  FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN(0,1,2,3)";
         }
          $t_result = $li->returnArray($t_sql,4);
          for($i=0;$i<sizeof($t_result);$i++){
               list($u_id,$u_login,$u_class,$u_classnum, $user_name) = $t_result[$i];
               if($format==1){
                          $students[$u_class][$u_classnum]['uid']=$u_id;
                          $students[$u_class][$u_classnum]['login']=$u_login;
                          $students[$u_class][$u_classnum]['user_name']=$user_name;

               }else if($format==2){
                          $students[$u_login]['uid'] = $u_id;
                          $students[$u_login]['class'] = $u_class;
                          $students[$u_login]['classnumber']=$u_classnum;
                          $students[$u_login]['login']=$u_login;
                          $students[$u_login]['user_name']=$user_name;

               }
       }
               
        // checking of duplicated entries
        for($i=0;$i<sizeof($data);$i++){
	        if(trim(implode("",$data[$i]))=="") continue;
	        if($format==1){
		        list($class,$classnum,$amount)=$data[$i];
		        $class = trim($class);
		        $classnum = trim($classnum);
		        $duplicated_list[$class.":".$classnum]+=1+0;
		    }
		    else if($format ==2){
			    list($user_login,$amount) = $data[$i];
			    $duplicated_list[$user_login]+=1+0;
			}
	    }       
   
               
	$sql_data = array();
	$action_list = array();
           $error1 = false; // No matched Student
		   $error2 = false; // Invalid Amount
		   $error3 = false; // Duplicated entries
			$error4 = false;
			$error5 = false;
			$error6 = false;

           # Update TEMP_CASH_DEPOSIT
           $values = "";
           $delim = "";

			$subsidy_used_amount = array();

           for ($i=0; $i<sizeof($data); $i++)
           {

				$warning_msg = array();
                # check for empty line
                    $test = trim(implode("",$data[$i]));
                    if($test=="") continue;

                    if($format==1){
                        list($class, $classnum, $amount, $remark) = $data[$i];
                        if($sys_custom['ePayment']['PaymentMethod']){
                        	$payment_method = $data[$i][4];
							$subsidy_code = $data[$i][5];
                            $subsidy_amount = $data[$i][6];
                        } else {
							$subsidy_code = $data[$i][4];
							$subsidy_amount = $data[$i][5];
						}
						$str_subsidy_code = $subsidy_code;
						$subsidy_unitid = '';

                           $class = trim($class);
                        $classnum = trim($classnum);
                        if($students[$class][$classnum]['uid']==""){ # no matched student
                            $error1 = true;
                            //$warning1="<font color=red>*</font>";
                            //$warning1 = $i_Payment_Import_NoMatch_Entry;
                            $warning_msg[] = $i_Payment_Import_NoMatch_Entry;

                        }
                        
                        $target_user_id = $students[$class][$classnum]['uid'];
                        
                        if(trim($amount)=='' || (!is_numeric($amount) || (!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] && $amount<0) ) )
                        { # invalid amount
	                        $error2 = true;
	                        //$warning2="<font color=red>**</font>";
	                        $warning_msg[]=$i_Payment_Import_InvalidAmount;
	                    }
	                    if($duplicated_list[$class.":".$classnum]>1){
		                    $error3 = true;
		                    $warning_msg[]=$i_Payment_Import_DuplicatedStudent;
		                }

						if($subsidy_code != '') {
							if(trim($subsidy_amount)=='' || (!is_numeric($subsidy_amount) || $subsidy_amount<0) )
							{
								$error4 = true;
								$warning_msg[]=$i_Payment_Import_InvalidSubSidyAmount;
							} else {
								$amount = $amount - $subsidy_amount;
								if($amount < 0) {
									$error4 = true;
									$warning_msg[]=$i_Payment_Import_InvalidSubSidyAmount;
								}
							}

							if($error4 == false) {
								$sql = "SELECT UnitID,TotalAmount,UnitName FROM PAYMENT_SUBSIDY_UNIT WHERE UnitCode = '" . $li->Get_Safe_Sql_Query($subsidy_code) . "'";
								$temp = $li->returnArray($sql, 3);
								if (count($temp) == 0) {
									$error5 = true;
									$warning_msg[] = $i_Payment_Import_SubsidyNotFound;
								} else {
									$unit_id = $temp[0]['UnitID'];
									$subsidy_unitid = $unit_id;
									$subsidy_total_amount = $temp[0]['TotalAmount'];
									$str_subsidy_code = $temp[0]['UnitName'];
									$sql = "SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEM_SUBSIDY AS a WHERE a.SubsidyUnitID ='$unit_id' ";
									$temp = $li->returnVector($sql);
									$unit_used_amount = $temp[0];

									if (!isset($subsidy_used_amount[$unit_id])) {
										$subsidy_used_amount[$unit_id] = 0;
									}
									$subsidy_remain_amount = $subsidy_total_amount - $unit_used_amount - $subsidy_used_amount[$unit_id] - $subsidy_amount;
									if($subsidy_remain_amount < 0) {
										$error6 = true;
										$warning_msg[]=$i_Payment_Import_SourceOfSubsidyNotEnoughMoney;
									} else {
										$subsidy_used_amount[$unit_id] += $subsidy_amount;
									}
								}
							}
						} else {
							$subsidy_amount = 0.0;
						}

	                    //if($error2 || $error1){
		                    $str_amount = $error2?"$".$amount:"$".number_format($amount,2);
						    $str_subsidy_amount = $error4?"$".$subsidy_amount:"$".number_format($subsidy_amount,2);

                    		//$warning_msg = "<font color=red>".implode("<BR>",$warning_msg)."</font>&nbsp;";
                    		$warning_message = "<table border=0>";
                    		for($x=0;$x<sizeof($warning_msg);$x++){
	                    		$warning_message.="<tr class='tabletext'><td><font color=red>-</font></td><td><font color=red>".$warning_msg[$x]."</font></td></tr>";
	                    	}
	                    	if(sizeof($warning_msg)<=0)
	                    		$warning_message.="<tr><td colspan='2'>&nbsp;</td></tr>";	                    	
	                    	$warning_message.="</table>";
                        	//$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>$warning1</i></font>","<i>$class</i>","<i>$classnum</i>","","$str_amount $warning2");
                        	if($sys_custom['ePayment']['PaymentMethod']){
                        		$error_entries[] = array($target_user_id,$students[$class][$classnum]['user_name'],"$class","$classnum",$students[$class][$classnum]['login'],$str_amount,intranet_htmlspecialchars($remark),$payment_method,$str_subsidy_code, $str_subsidy_amount,$warning_message);
                        	}else{
                        		$error_entries[] = array($target_user_id,$students[$class][$classnum]['user_name'],"$class","$classnum",$students[$class][$classnum]['login'],$str_amount,intranet_htmlspecialchars($remark),$str_subsidy_code, $str_subsidy_amount,$warning_message);
                        	}
                        //}


                }else if($format==2){
                           list($user_login, $amount, $remark) = $data[$i];
                           if($sys_custom['ePayment']['PaymentMethod']){
                        		$payment_method = $data[$i][3];
							    $subsidy_code = $data[$i][4];
                                $subsidy_amount = $data[$i][5];
                        	} else {
							   $subsidy_code = $data[$i][3];
							   $subsidy_amount = $data[$i][4];
						   }

                              if(trim($amount)=='' || (!is_numeric($amount) || (!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] && $amount<0) ) )
                              { # invalid amount
                                       $error2 = true;
	                        			//$warning2="<font color=red>**</font>";
                            			$warning_msg[]=$i_Payment_Import_InvalidAmount;
	                              
	                          }

                          if($students[$user_login]['uid']==""){ # no matched student
                                  $error1 = true;
            						//$warning1="<font color=red>*</font>";
            						$warning_msg[]=$i_Payment_Import_NoMatch_Entry2;
                          }

                        else if($user_login!=""){
                                   $class = $students[$user_login]['class'];
                                   $classnum = $students[$user_login]['classnumber'];
                           }
                           $target_user_id = $students[$user_login]['uid'];
                       if($duplicated_list[$user_login]>1){
		                    $error3 = true;
		                    $warning_msg[]=$i_Payment_Import_DuplicatedStudent;
		                }

						if($subsidy_code != '') {
							if(trim($subsidy_amount)=='' || (!is_numeric($subsidy_amount) || $subsidy_amount<0) )
							{
								$error4 = true;
								$warning_msg[]=$i_Payment_Import_InvalidSubSidyAmount;
							} else {
								$amount = $amount - $subsidy_amount;
								if($amount < 0) {
									$error4 = true;
									$warning_msg[]=$i_Payment_Import_InvalidSubSidyAmount;
								}
							}

							if($error4 == false) {
								$sql = "SELECT UnitID,TotalAmount,UnitName FROM PAYMENT_SUBSIDY_UNIT WHERE UnitCode = '" . $li->Get_Safe_Sql_Query($subsidy_code) . "'";
								$temp = $li->returnArray($sql, 3);
								if (count($temp) == 0) {
									$error5 = true;
									$warning_msg[] = $i_Payment_Import_SubsidyNotFound;
								} else {
									$unit_id = $temp[0]['UnitID'];
									$subsidy_unitid = $unit_id;
									$subsidy_total_amount = $temp[0]['TotalAmount'];
									$str_subsidy_code = $temp[0]['UnitName'];
									$sql = "SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEM_SUBSIDY AS a WHERE a.SubsidyUnitID ='$unit_id' ";
									$temp = $li->returnVector($sql);
									$unit_used_amount = $temp[0];

									if (!isset($subsidy_used_amount[$unit_id])) {
										$subsidy_used_amount[$unit_id] = 0;
									}
									$subsidy_remain_amount = $subsidy_total_amount - $unit_used_amount - $subsidy_used_amount[$unit_id] - $subsidy_amount;
									if($subsidy_remain_amount < 0) {
										$error6 = true;
										$warning_msg[]=$i_Payment_Import_SourceOfSubsidyNotEnoughMoney;
									} else {
										$subsidy_used_amount[$unit_id] += $subsidy_amount;
									}
								}
							}
						} else {
							$subsidy_amount = 0.0;
						}

   	                    //if($error2 || $error1){
                   		     $str_amount = $error2?"$".$amount:"$".number_format($amount,2);
						     $str_subsidy_amount = $error4?"$".$subsidy_amount:"$".number_format($subsidy_amount,2);
                    					//$warning_msg = "<font color=red>".implode("<BR>",$warning_msg)."</font>&nbsp;";
                  			$warning_message = "<table border=0>";
                    		for($x=0;$x<sizeof($warning_msg);$x++){
	                    		$warning_message.="<tr class='tabletext'><td><font color=red>-</font></td><td><font color=red>".$warning_msg[$x]."</font></td></tr>";
	                    	}
	                    	if(sizeof($warning_msg)<=0)
	                    		$warning_message.="<tr><td colspan='2'>&nbsp;</td></tr>";
	                    	$warning_message.="</table>";

		                    		//$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>$warning1</i></font>","","","<i>$user_login</i>","$str_amount $warning2");
		                if($sys_custom['ePayment']['PaymentMethod']){
		                	$error_entries[] = array($target_user_id,$students[$user_login]['user_name'],$students[$user_login]['class'],$students[$user_login]['classnumber'],"$user_login",$str_amount,intranet_htmlspecialchars($remark),$payment_method,$str_subsidy_code, $str_subsidy_amount,$warning_message);
		                }else{   
                            $error_entries[] = array($target_user_id,$students[$user_login]['user_name'],$students[$user_login]['class'],$students[$user_login]['classnumber'],"$user_login",$str_amount,intranet_htmlspecialchars($remark),$str_subsidy_code, $str_subsidy_amount,$warning_message);
		                }
                        //}

                           

                }
                
                //if (!$error1 && !$error2 && !$error3 && $class != "" && $classnum != ""){
	            //    	$sql_data[$class.":".$classnum] = "('$class','$classnum','$amount','".$li->Get_Safe_Sql_Query($remark)."'".($sys_custom['ePayment']['PaymentMethod']?",'".$paymentMethodReverseMap[$payment_method]."'":"").")";
	            if (!$error1 && !$error2 && !$error3 && !$error4 && !$error5 && !$error6 && $target_user_id != ''){
	            		$sql_data[$target_user_id] = "('$target_user_id','$amount','".$li->Get_Safe_Sql_Query($remark)."'".($sys_custom['ePayment']['PaymentMethod']?",'".$paymentMethodReverseMap[$payment_method]."'":"").",'".$li->Get_Safe_Sql_Query($subsidy_unitid)."','".$li->Get_Safe_Sql_Query($subsidy_amount)."')";
	            	    	
	                	if($target_user_id!=""){
		                	if(in_array($target_user_id,$unpaid)){
	                			//$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update;
	                			$action_list[$target_user_id] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update;
		                	}else if(in_array($target_user_id,$paid)){
	                			//$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange;
	                			$action_list[$target_user_id] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange;
		                	}else{
	                			//$action_list[$class.":".$classnum] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_New;
	                			$action_list[$target_user_id] =$i_Payment_Menu_Settings_PaymentItem_Import_Action_New;
		                	}
	                	}
                          //$values .= "$delim ('$class','$classnum','$amount')";
                           //$delim = ",";
                }

           }
           if(!$error1 && !$error2 && !$error3 && !$error4 && !$error5 && !$error6){
           				/* Created in import.php
                                $sql = "CREATE TABLE IF NOT EXISTS TEMP_PAYMENT_ITEM_IMPORT (
                                 ClassName varchar(20),
                                 ClassNumber varchar(20),
                                 Amount float
                                )";
                                $li->db_db_query($sql);
                        */
                                $values = implode(",",$sql_data);
                              $sql = "INSERT INTO TEMP_PAYMENT_ITEM_IMPORT (UserID,Amount,Remark ".($sys_custom['ePayment']['PaymentMethod']?",PaymentMethod":"").",SubsidyUnitID,SubsidyAmount) VALUES $values";
                   $li->db_db_query($sql);
                }
        }
        else
        {
            header ("Location: import.php?ItemID=$ItemID&failed=1");
            exit();
        }
}

$error_exists = $error1 || $error2 || $error3 || $error4 || $error5 || $error6 ? true:false;

if(!$error_exists){
        $namefield = getNameFieldByLang("b.");

        $sql = "SELECT a.UserID,$namefield, b.ClassName, b.ClassNumber, b.UserLogin, a.Amount, a.Remark ";
        if($sys_custom['ePayment']['PaymentMethod']){
        	$sql .= ",CASE a.PaymentMethod 
						WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
						WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
						WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' 
						ELSE '".$Lang['ePayment']['NA']."' 
					END as PaymentMethod ";
        }
		$sql.=" , c.UnitName, a.SubsidyAmount FROM TEMP_PAYMENT_ITEM_IMPORT as a LEFT JOIN INTRANET_USER as b
             ON a.UserID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus IN (0,1,2)
             LEFT JOIN PAYMENT_SUBSIDY_UNIT as c ON a.SubsidyUnitID=c.UnitID
     ";
     $result = $li->returnArray($sql);
}
else{
         $result = $error_entries;

}

$display = "<tr class=\"tabletop\">
				<td class=\"tabletoplink\" width=\"10%\">$i_UserName</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_UserClassName</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_UserClassNumber</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_UserLogin</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_Payment_Field_Amount</td>
				<td class=\"tabletoplink\" width=\"10%\">".$Lang['General']['Remark']."</td>";
	if($sys_custom['ePayment']['PaymentMethod']){
		$display .= "<td class=\"tabletoplink\" width=\"10%\">".$Lang['ePayment']['PaymentMethod']."</td>";
	}
$display .= "<td class=\"tabletoplink\" width=\"10%\">".$i_Payment_Subsidy_UnitCode."</td>
                <td class=\"tabletoplink\" width=\"10%\">".$i_Payment_Subsidy_Amount."</td>";

if(!$error_exists)
	$display.="<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Menu_Settings_PaymentItem_Import_Action</td>";
else $display.="<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason</td>";
$display.="</tr>\n";
				
$display.="</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
	if($sys_custom['ePayment']['PaymentMethod']){
		list($user_id,$name,$class,$classnum,$login,$amount,$remark,$payment_method,$subsidy_name,$subsidy_amount,$reason) = $result[$i];
	}else{
     	list($user_id,$name,$class,$classnum,$login,$amount,$remark,$subsidy_name,$subsidy_amount,$reason) = $result[$i];
	}
     $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
     $str_amount = $error_exists? $amount:"$".number_format($amount, 2);
	 $str_subsidy_amount = $error_exists? $subsidy_amount:"$".number_format($subsidy_amount, 2);
     if(!$error_exists)
     	$reason = "<font color=green>".$action_list[$user_id]."</font>";
     
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"tabletext\">$name</td><td class=\"tabletext\">$class</td><td class=\"tabletext\">$classnum</td>";
     $display .= "<td class=\"tabletext\">$login</td><td class=\"tabletext\">$str_amount</td><td class=\"tabletext\">".intranet_htmlspecialchars($remark)."</td>";
     if($sys_custom['ePayment']['PaymentMethod']){
     	$display .= "<td class=\"tabletext\">$payment_method</td>";
     }
	 $display .= "<td class=\"tabletext\">$subsidy_name</td>";
	 $display .= "<td class=\"tabletext\">$str_subsidy_amount</td>";
     $display .= "<td class=\"tabletext\">$reason</td>";
     $display .= "</tr>\n";
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html();
$linterface->LAYOUT_START();

?>

<form name="form1" method="get" action="import_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<?php if($error_exists){?>
			    <tr>
					<td class="tabletext"><?= $i_Payment_Import_Error ?></td>
				</tr>
			<!--
				<?php if($error1) {?>
				<tr>
					<td class="tabletext">
						<span class='tabletextrequire'>*</span>
						<?= ($format==1?$i_Payment_Import_NoMatch_Entry:$i_Payment_Import_NoMatch_Entry2) ?>
					</td>
				</tr>
				<?php } ?>
				<?php if($error2) {?>
				<tr>
					<td class="tabletext">
						<span class='tabletextrequire'>**</span>
						<?= $i_Payment_Import_InvalidAmount ?>
					</td>
				</tr>
				<?php } ?>
				-->
				<?php } else { ?>
				 <tr>
					<td class="tabletext"><?= $i_Payment_Import_Confirm ?></td>
				</tr>
				<?php } ?>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<?=$display?>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
	    <?php if(!$error_exists){ ?>
			<?= $linterface->GET_ACTION_BTN($button_confirm, "submit", "") ?>
		<?php } ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import.php?ItemID=$ItemID&clear=1'") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="confirm" value="1">
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
