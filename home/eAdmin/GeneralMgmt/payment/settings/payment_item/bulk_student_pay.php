<?php
// Editing by 
/*
 * 2015-10-28 Carlos : Include staff users to pay.
 * 2013-04-15 Carlos : Allow all user status to pay.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
intranet_opendb();

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

@set_time_limit(0);
$ItemID = $_REQUEST['ItemID'];

$lpayment = new libpayment();

$sql = "SELECT a.PaymentID
        FROM 
        	PAYMENT_PAYMENT_ITEMSTUDENT as a 
        	INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus IN (0,1,2,3) 
		WHERE 
			a.ItemID = '$ItemID' 
        	AND a.RecordStatus = 0 ";
$result = $lpayment->returnArray($sql,1);

?>
<body>
<form name="BulkPayment" id="BulkPayment" method="POST" action="student_pay.php">
<input type="hidden" name="BulkPayment" value="1" >
<input type="hidden" name="ItemID" value="<?=$ItemID?>" >
<?
for ($i=0; $i<sizeof($result); $i++) {
?>
	<input type="hidden" name="PaymentID[]" value="<?=$result[$i][0]?>" >
<?
}
?>
</form>
<?
intranet_closedb();
?>
</body>
<script>
document.getElementById('BulkPayment').submit();
</script>
