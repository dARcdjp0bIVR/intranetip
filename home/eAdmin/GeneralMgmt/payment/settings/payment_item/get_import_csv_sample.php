<?php
// Editing by 
/*
 * 請使用萬國碼 UTF-8 編輯此文件
 * 2015-10-28 (Carlos): Modified sample data for staff user type.
 * 2014-03-24 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$lpayment = new libpayment();
$lexport = new libexporttext();

if($file_type == 1){
	$filename = "sample_unicode.csv";
	$exportColumn = array("Class Name","Class Number","Student Name [Ref]","Amount","Remark");
    $rows = array();
    $rows[0] = array("(班別)","(班號)","(學生姓名)[參考用途]","(需繳付金額)","(備註)");
	$rows[1] = array("1A","13","Student 1","1002","現金交付");
	$rows[2] = array("1B","2","Student 2","1001.5","以支票繳付");
	$rows[3] = array("1C","11","Student 3","4000","PPS");
	$rows[4] = array("2A","11","Student 4","100","");
	if($sys_custom['ePayment']['PaymentMethod']){
		$exportColumn[] = "Payment Method";
		$rows[0][] = "(付款方法)";
		$rows[1][] = "現金";
		$rows[2][] = "支票";
		$rows[3][] = "轉帳";
		$rows[4][] = "";
	}
	$exportColumn[] = "Source Of Subsidy Code";
	$exportColumn[] = "Subsidy Amount";
	$rows[0][] = "(資助來源代碼)";
	$rows[0][] = "(資助金額)";
} else if($file_type == 2){
	$filename = "sample2_unicode.csv";
	$exportColumn = array("User Login","User Name [Ref]","Amount","Remark");
	$rows = array();
	$rows[0] = array("(內聯網帳號)","(用戶姓名)[參考用途]","(需繳付金額)","(備註)");
	$rows[1] = array("s1000111","Student 1","1002","PPS");
	$rows[2] = array("s123456","Student 2","1001.5","現金");
	$rows[3] = array("t000011","Teacher 1","200","");
	if($sys_custom['ePayment']['PaymentMethod']){
		$exportColumn[] = "Payment Method";
		$rows[0][] = "(付款方法)";
		$rows[1][] = "";
		$rows[2][] = "現金";
		$rows[3][] = "轉帳";
	}
	$exportColumn[] = "Source Of Subsidy Code";
	$exportColumn[] = "Subsidy Amount";
	$rows[0][] = "(資助來源代碼)";
	$rows[0][] = "(資助金額)";
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>