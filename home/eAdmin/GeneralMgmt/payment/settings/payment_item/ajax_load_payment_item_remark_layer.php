<?php
// Editing by 
/*
 * 2013-11-25 (Carlos): Created for loading payment item remark
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT Remark FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID='$PaymentID'";
$record = $li->returnVector($sql);
$remark = $record[0];

$x = '<div id="remark'.$PaymentID.'" style="display: block; position: absolute; width: 320px; height: 150px; z-index: 1; visibility: visible; border: 0px solid rgb(165, 172, 178);">'."\n";
$x.= '<table class="layer_table" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td height="19">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td height="19" width="5"><img src="/images/'.$LAYOUT_SKIN.'/can_board_01.gif" height="19" width="5"></td>
								<td background="/images/'.$LAYOUT_SKIN.'/can_board_02.gif" height="19" valign="middle">&nbsp;</td>
								<td height="19" width="19">
									<a href="javascript:void(0);" onclick="$(this).closest(\'div#remark'.$PaymentID.'\').remove();">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close'.$PaymentID.'" id="pre_close'.$PaymentID.'" onmouseover="MM_swapImage(\'pre_close'.$PaymentID.'\',\'\',\'/images/'.$LAYOUT_SKIN.'/can_board_close_on.gif\',1)" onmouseout="MM_swapImgRestore()" border="0" height="19" width="19"></a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td background="/images/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_04.gif" height="19" width="5">
								</td>
								<td bgcolor="#FFFFF7">
									<div style="height:150px; overflow:auto;">
										<table class="layer_table_detail" bgcolor="#CCCCCC" border="0" cellpadding="0" cellspacing="0" width="98%">
											<tbody>
												<tr class="tabletop">
													<td align="left">'.$Lang['General']['Remark'].'</td>
												</tr>
												<tr class="row_approved">
													<td class="tabletext">'.intranet_htmlspecialchars($remark).'</td>
												</tr>
											</tbody>
										</table>
									</div>
									<br>
								</td>
								<td background="/images/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_06.gif" height="6" width="6">
								</td>
							</tr>
							<tr>
								<td height="6" width="5">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_07.gif" height="6" width="5">
								</td>
								<td background="/images/'.$LAYOUT_SKIN.'/can_board_08.gif" height="6">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_08.gif" height="6" width="5">
								</td>
								<td height="6" width="6">
									<img src="/images/'.$LAYOUT_SKIN.'/can_board_09.gif" height="6" width="6">
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	  </table>'."\n";
$x .= '</div>';

echo $x;

intranet_closedb();
?>