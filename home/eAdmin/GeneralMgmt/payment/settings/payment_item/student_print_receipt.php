<?php
#using: 
/*
 * 2015-02-18 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method] and [Receipt Remark] for KIS.
 * 2014-12-30 (Carlos): $sys_custom['PaymentReceiptNumber'] - Get the last print receipt number for printing.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$ReceiptTitle=$Lang['ePayment']['OfficialReceipt'];
$lpayment = new libpayment();

$linterface = new interface_html();

$page_breaker = "<p class=\"breakhere\">";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css">
	body th, body td {font-size: 12px;} 
</style>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?
if($sys_custom['ePayment']['ReceiptByCategoryDesc']) {
	$_REQUEST['Signature'] = 1;
	$_REQUEST['SignatureText'] = $i_Payment_Receipt_Person_In_Charge;
}

$StartingReceiptNumber = $_REQUEST['StartingReceiptNumber'];
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];

if ($sys_custom['PaymentReceiptNumber']) {
	// consolidate school year to recipt number
	$SchoolYear = explode(' - ',GET_ACADEMIC_YEAR());
	$ReceiptHead = substr($SchoolYear[0],-2).substr($SchoolYear[1],-2);
	
	if($StartingReceiptNumber == ""){
		$StartingReceiptNumber = $lpayment->Get_Next_Receipt_Number();
	}
}

$table_header  = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\" class=\"eSporttableborder\">";
$table_header .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Category</td>";
$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Item</td>";
$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Description</td>";
$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_RefCode</td>";
if($sys_custom['ePayment']['PaymentMethod']){
	$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentMethod']."</td>";
	$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['Remark']."</td>";
}
$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Date</td>";
$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Amount</td></tr>";

$display = "";

$lclass = new libclass();
if ($StudentID == "")
{
    if ($ClassName == "")
    {
        # All students
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
    else
    {
        # All students in $ClassName
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
}
else
{
	if (is_array($StudentID)) {
		$target_students = $StudentID;
	}
	else {
    $target_students = array($StudentID);
  }
}

$date_field = "a.PaidTime";
$conds = "";
if ($date_from != "")
{
    $conds .= " AND $date_field >= '$date_from'";
}
if ($date_to != "")
{
    $conds .= " AND $date_field <= '$date_to 23:59:59'";
}

# Get Latest Balance
$lpayment = new libpayment();
$list = "'".implode("','", $target_students)."'";
$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
$temp = $lpayment->returnArray($sql,2);
$balance_array = build_assoc_array($temp);
//$PaymentID = explode(',', $_REQUEST['PaymentID']);
$total_record = 0;

$conds .= " AND a.PaymentID IN (".$PaymentID.") ";

$colspan = 5;
if($sys_custom['ePayment']['PaymentMethod']){
	$colspan += 2;
}

for ($j=0; $j<sizeof($target_students); $j++)
{
	$target_id = $target_students[$j];
	$lu = new libuser($target_id);
	$studentname = $lu->UserNameClassNumber();
	$i_title = $studentname;
	$curr_balance = $balance_array[$target_id];
           
    $sql = "SELECT
						c.Name, 
						b.Name,
						b.Description, 
						a.PaidTime,
						a.Amount, 
						d.RefCode,
						d.RelatedTransactionID,
						c.Description ";
	if($sys_custom['ePayment']['PaymentMethod']){
		$sql .= ",a.PaymentMethod,a.ReceiptRemark ";
	}
	$sql .= " FROM 
				PAYMENT_PAYMENT_ITEMSTUDENT as a
			  inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID='$target_id')
	          LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        WHERE 
	        	a.StudentID = '$target_id' 
	        	AND a.RecordStatus = 1 AND d.TransactionType=2 
	        	$conds 	        			
	        ORDER BY c.DisplayOrder, b.DisplayOrder ".($sys_custom['ePayment']['ReceiptByCategoryDesc']?",c.Description ":"")." ";
	$student_itemlist = $lu->returnArray($sql);
	
	if($sys_custom['ePayment']['ReceiptByCategoryDesc']){
		// items are group by student and payment item category description
		$sql = "SELECT 
					DISTINCT c.Description 
				FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
			  	INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID='$target_id')
	          	LEFT JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          	LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        	WHERE 
	        		a.StudentID = '$target_id' 
	        		AND a.RecordStatus = 1 AND d.TransactionType=2 
	        		$conds ";
		$tmp_category_desc_list = $lu->returnVector($sql);
		$category_desc_list = array();
		for($k=0;$k<sizeof($tmp_category_desc_list);$k++)
		{
			$category_records = array();
			for($i=0;$i<sizeof($student_itemlist);$i++) {
				if($tmp_category_desc_list[$k] == $student_itemlist[$i]['Description']){
					$category_records[] = $student_itemlist[$i];
				}
			}
			if(sizeof($category_records)>0){
				$category_desc_list[] = $category_records;
			}
		}
	}else{
		// normal flow, items are group by student
		$category_desc_list = array();
		$category_desc_list[] = $student_itemlist;
	}
	
	
	for($n=0;$n<sizeof($category_desc_list);$n++)
	{
		$student_itemlist = $category_desc_list[$n];
		if(!empty($student_itemlist))
		{
			$RecordByRelatedTransactionID = array();
			for ($i=0; $i<sizeof($student_itemlist); $i++)
			{
				$RecordByRelatedTransactionID[$student_itemlist[$i]['RelatedTransactionID']]  = $student_itemlist[$i];
			}
			
			$total_record++;
			# start student
			$total_amount = 0;
			$list_table = "$table_header";
			
			foreach($RecordByRelatedTransactionID as $key => $recordAry)
			{
			     list($cat_name, $item_name, $item_desp,$item_paidtime, $item_amount, $ref_code) = $recordAry;
			     $total_amount += $item_amount;
			     $item_amount = '$'.number_format($item_amount,2);
			     $list_table .= "<tr><td class=\"eSporttdborder eSportprinttext\">$cat_name&nbsp;</td>";
			     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_name&nbsp;</td>";
			     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_desp&nbsp;</td>";
			     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$ref_code&nbsp;</td>";
			     if($sys_custom['ePayment']['PaymentMethod']){
			     	$payment_method = $recordAry[8];
			     	$receipt_remark = $recordAry[9];
			     	$list_table .= "<td class=\"eSporttdborder eSportprinttext\">".$sys_custom['ePayment']['PaymentMethodItems'][$payment_method]."&nbsp;</td>";
			     	$list_table .= "<td class=\"eSporttdborder eSportprinttext\">".nl2br($receipt_remark)."&nbsp;</td>";
			     }
			     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_paidtime&nbsp;</td>";
			     $list_table .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">$item_amount</td></tr>\n";
			}
	
			$total_amount = '$'.number_format($total_amount,2);
			$list_table .= "<tr>";
			$list_table .= "<td class=\"eSporttdborder eSportprinttext\" colspan=\"$colspan\" align=\"right\">$i_Payment_Receipt_Payment_Total</td>";
			$list_table .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">$total_amount</td></tr>\n";
			$list_table .= "</table>";
			$name_string = "$i_Payment_Receipt_ReceivedFrom $studentname $i_Payment_Receipt_PaidAmount$total_amount $i_Payment_Receipt_ForItems";
			
			// consolidate receipt number
			$ReceiptNumber = $ReceiptHead;
			$ReceiptNumber .= (trim($lu->ClassName) != "")? '-'.$lu->ClassName:'';
			$ReceiptNumber .= (trim($lu->ClassNumber) != "")? '-'.$lu->ClassNumber:'';
			$ReceiptNumber .= '-'.$StartingReceiptNumber;
			
			if ($j < (sizeof($target_students)-1) || ($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $n < (sizeof($category_desc_list)-1)))
				echo '<div style="page-break-after:always">';
		
			echo "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class='tabletext'>\n";
			
			if($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $student_itemlist[0]['Description'] != ''){
				echo '<br>
					<div align=center><font size=+2>'.$student_itemlist[0]['Description'].'</font></div><br>
					<div align=center><font size=+1>'.$ReceiptTitle.'</font></div><br>
					<br><br><br>';
			}else if (is_file("$intranet_root/templates/reportheader/paymentlist_header.php"))
			{
			    include("$intranet_root/templates/reportheader/paymentlist_header.php");
			}
			echo "</td></tr></table>\n";
			if ($sys_custom['PaymentReceiptNumber']) {
				echo "<table width=95% border=0 cellspacing=0 cellpadding=0 >
							<tr>
								<td align=right>
									".$Lang['Payment']['ReceiptNumber'].": ".$ReceiptNumber."
								</td>
							</tr>
						</table>";
			}
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class='tabletext'>$name_string</td></tr></table>\n";
			echo "<br>";
			echo $list_table;
			echo "</td></tr></table>\n";
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
			if (is_file("$intranet_root/templates/reportheader/paymentlist_footer.php"))
			{
			    include("$intranet_root/templates/reportheader/paymentlist_footer.php");
			}
			echo "</td></tr></table>\n";
			echo "</table>\n";
			
			if ($j < sizeof($target_students))
				echo "</div>";
			# End of 1 student
			
			if ($sys_custom['PaymentReceiptNumber']) {
				$StartingReceiptNumber++;
				switch (strlen($StartingReceiptNumber)) {
					case 0:
						$StartingReceiptNumber = '0001';
						break;
					case 1:
						$StartingReceiptNumber = '000'.$StartingReceiptNumber;
						break;
					case 2:
						$StartingReceiptNumber = '00'.$StartingReceiptNumber;
						break;
					case 3:
						$StartingReceiptNumber = '0'.$StartingReceiptNumber;
						break;
					default:
						break;
				}
			}
		}
	}
}

$StartingReceiptNumber += 0;
if ($sys_custom['PaymentReceiptNumber']) {
	$lpayment->Save_Receipt_Number($StartingReceiptNumber);
}

if($total_record==0)
{
	echo "<div align='center'>$i_no_record_exists_msg</div>";
}

intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
?>