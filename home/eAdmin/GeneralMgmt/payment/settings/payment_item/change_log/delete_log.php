<?php
// Editing by 
/*
 * 2014-08-20 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['PaymentItemChangeLog']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if($_SERVER['REQUEST_METHOD'] != 'POST' || count($LogID)==0) 
{
	header ("Location: index.php?Msg=DeleteUnsuccess");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$success = $lpayment->deletePaymentItemChangeLog($LogID);

intranet_closedb();
if($success){
	$Msg = "DeleteSuccess";
}else{
	$Msg = "DeleteUnsuccess";
}
header("Location: index.php?Msg=".$Msg);
?>