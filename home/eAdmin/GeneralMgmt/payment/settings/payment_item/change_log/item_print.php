<?php
// editing by 
########################################### Change Log ###################################################
#
##########################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lpayment = new libpayment();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 3;
$order = ($order == 1) ? 1 : 0;

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d") : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d") : $date_to;

$log_type_value = $i_status_all;
if(isset($Lang['ePayment']['PaymentItemChangeLogType'][$LogType])) {
	$log_type_value = $Lang['ePayment']['PaymentItemChangeLogType'][$LogType];
}

$log_types = $lpayment->getPaymentItemChangeLogTypeArray();
$log_type_ary = array();

$name_field = getNameFieldByLang("u.");

$log_type_field = "CASE a.LogType ";
for($i=0;$i<count($log_types);$i++){
	$log_type_field .= " WHEN '".$log_types[$i]."' THEN '".$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]."' ";
	$log_type_ary[] = array($log_types[$i],$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]);
}
$log_type_field .= " END ";


$log_type_cond = "";
if(isset($LogType) && $LogType != ''){
	$log_type_cond = " a.LogType='$LogType' AND ";
}

$sql = "SELECT 
			DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s') as DateInput,
			$log_type_field as LogType,
			a.LogDisplayDetail,
			$name_field as InputBy ";
//$sql.= ",CONCAT('<input type=\"checkbox\" name=\"LogID[]\" value=\"', a.LogID ,'\" />') ";
$sql.=" FROM PAYMENT_PAYMENT_ITEM_CHANGE_LOG as a 
		LEFT JOIN INTRANET_USER as u ON u.UserID=a.InputBy 
		WHERE $log_type_cond a.DateInput >= '$date_from 00:00:00' AND a.DateInput <= '$date_to 23:59:59' ";


$field_array = array("DateInput","LogType","a.LogDisplayDetail","InputBy");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";

$li = new libdb();

$temp = $li->returnArray($sql, sizeof($field_array));


$table="<table width=95% border=0 cellpadding=2 cellspacing=0 align='center' class='eSporttableborder'>";
$table.="<thead>";
$table.="<tr class='eSporttdborder eSportprinttabletitle'>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>#</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['DateInput']."</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['RecordType']."</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['RecordInfo']."</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['InputBy']."</td>";
$table.="</tr>";
$table.="</thead>";
$table.="<tbody>";

for($i=0;$i<sizeof($temp);$i++){
	list($date_input,$log_type,$log_display_detail,$input_by)=$temp[$i];
	$css = $i%2==0?"attendancepresent":"attendanceouting";
	$table.="<tr class='$css'>
	<Td class='eSporttdborder eSportprinttext'>".($i+1)."</td>
	<td class='eSporttdborder eSportprinttext'>$date_input</td>
	<td class='eSporttdborder eSportprinttext'>$log_type</td>
	<td class='eSporttdborder eSportprinttext'>$log_display_detail</td>
	<td class='eSporttdborder eSportprinttext'>$input_by</td>
	</tr>";
}
if(sizeof($temp)<=0){
	$table.="<tr><td colspan='11' align=center height=40 class='eSporttdborder eSportprinttext'>$i_no_record_exists_msg</td></tr>";
}
$table.="</tbody>";
$table.="</table>";


include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>

<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<!-- date range -->
<table border=0 width=95% align=center>
<tr><td class='<?=$css_title?>'><b><?=$Lang['ePayment']['PaymentItemChangeLog']?></b></td></tr>
</table>
<table border=0 width=95% align=center>
<tr><td class='<?=$css_text?>'><B><?=$Lang['ePayment']['RecordType']?></B>: <?=$log_type_value?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$Lang['General']['From']?></B>: <?=$date_from?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$Lang['General']['To']?></B>: <?=$date_to?></td></tr>
</table>
<BR>
<?php echo $table; ?>




<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
