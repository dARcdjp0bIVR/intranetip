<?php
// Editing by 
/*
 * 2017-08-08 (Carlos): Added select students by input search text similar to that of eNotice.
 * 2016-06-22 (Carlos): $sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] - cust to allow input negative amount for payment item.
 * 2015-10-28 (Carlos): Rename word [select students] to [select users].
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lidb = new libdb();
$lclass = new libclass();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);


$itemName = $lpayment->returnPaymentItemName($ItemID);


$sql = "
			SELECT 
					IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.UserID,IF((b.UserID IS NOT NULL AND c.UserID IS NULL),b.UserID,''))
			FROM 
					PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN 
					INTRANET_USER as b ON a.StudentID = b.UserID LEFT OUTER JOIN 
					INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
			WHERE 
					a.ItemID = '$ItemID'
		";

$result = $lidb->returnVector($sql);

if(sizeof($result)>0)
	$student_list = implode(",", $result);	# Student Who Are In The Payment List Already


$sql = "
			SELECT
					DefaultAmount
			FROM
					PAYMENT_PAYMENT_ITEM
			WHERE
					ItemID = '$ItemID'
		";

$temp = $lidb->returnVector($sql);
if(sizeof($temp)>0)
	list($default_amount) = $temp;
	
	
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lclass->returnArray($sql,2);
}

## select student
for ($i=0; $i<sizeof($array_students); $i++)
{
     list($student_id[], $student_name[]) = $array_students[$i];
}

$select_student = getSelectByValueDiffName($student_id,$student_name," id=\"student[]\" name=\"student[]\" size=\"15\" multiple=\"multiple\" style=\"width:99%;\"","",0,1);
echo $linterface->Include_AutoComplete_JS_CSS();
?>
<script language="Javascript">
var AutoCompleteObj_ClassNameClassNumber;

var eFormAppJS = {
	vars: {
		ajaxURL: "/home/eAdmin/StudentMgmt/notice/ajax_search_user_by_classname_classnumber.php",
		xhr: "",
		selectedUserContainer: "UserClassNameClassNumberSearchTb"
	},
	listener: {
		insertPICFromTextarea: function(e) {
			var textarea = $('div.pic_textarea textarea').eq(0);

			if (textarea.val() == "") {
				textarea.focus();
			} else {
				eFormAppJS.func.ajaxFromTextArea(textarea, "pic");
			}
		},
		insertFromTextarea: function(e) {
			var textarea = $('div.student_textarea textarea').eq(0);
			if (textarea.val() == "") {
				textarea.focus();
			} else {
				eFormAppJS.func.ajaxFromTextArea(textarea, "student");
			}
			e.preventDefault();
		}
	},
	func: {
		ajaxFromTextArea: function(textarea, contentType) {
			var targetSelectID = "student[]";
			var textareaMsgObj = '#textarea_studentmsg';
			switch (contentType) {
				case "pic":
					textareaMsgObj = '#textarea_msg';
					targetSelectID = "target_PIC";
					break;
			}
			if (textarea.val() == "") {
				textarea.focus();
				$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
			} else {
				var xhr = eFormAppJS.vars.xhr;
				if (xhr != "") {
					xhr.abort();
				}
				var textareaVal = textarea.val();
				var getTargetListVal = "";
				if (contentType == "pic") {
					$('select#' + targetSelectID + ' option').each(function(ndex, obj) {
						if (getTargetListVal != "") {
							getTargetListVal += ",";
						}
						getTargetListVal += $(this).val();
					});
				} else { 
					$('select[name="' + targetSelectID + '"] option').each(function(ndex, obj) {
						if (getTargetListVal != "") {
							getTargetListVal += ",";
						}
						getTargetListVal += $(this).val();
					});
				} 
				
				eFormAppJS.vars.xhr = $.ajax({
					url: eFormAppJS.vars.ajaxURL,
					type: 'get',
					data: { "q": textareaVal.split("\n").join("||"), "searchfrom":"textarea", "nt_userType" : contentType, "SelectedUserIDList" : getTargetListVal },
					error: function() {
						textarea.focus();
					},
					success: function(data) {
						var lineRec = data.split("\n");
						if (lineRec.length > 0) {
							var UserSelected = "";
							if (contentType == "pic") {
								UserSelected = $('#' + targetSelectID);
							} else {
								UserSelected = $('select[name="' + targetSelectID + '"]').eq(0);
							}
							// console.log(UserSelected);
							var afterTextarea = "";
							$.each(lineRec, function(index, data) {
								if (data != "") {
									var tmpData = data.split("||");
									if (typeof tmpData[0] != "undefined" && typeof tmpData[1] != "undefined" && tmpData[1] != "NOT_FOUND") {  
										// Add_Selected_User(tmpData[1], tmpData[0], eFormAppJS.vars.selectedUserContainer);
										// UserSelected.options[UserSelected.length] = new Option(tmpData[0], tmpData[1]);
										UserSelected.append($("<option></option>")
							                    .attr("value", tmpData[1])
							                    .text(tmpData[0])); 
										
									} else {
										if (afterTextarea != "") {
											afterTextarea += "\n";
										}
										afterTextarea += tmpData[2];									
									}
								}
							});
							if (afterTextarea != "") {
								textarea.val(afterTextarea);
								$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
							} else {
								textarea.val("");
								$(textareaMsgObj).html('');
							}
							checkOptionAll(document.getElementById('form1').elements[targetSelectID]);
						} else {
							textarea.focus();
							$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
						}
					}
				});
			}
		},
		init: function() {
			$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"});

			$('div.student_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertFromTextarea);
			//$('div.pic_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertPICFromTextarea);
			
			$('div.student_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertFromTextarea);
			//$('div.pic_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertPICFromTextarea);
		}
	}
};

function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber) {
	
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"/home/eAdmin/StudentMgmt/notice/ajax_search_user_by_classname_classnumber.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('student[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(){
	checkOptionAll(document.getElementById('form1').elements["student[]"]);
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('student[]', 'Array', true);
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
}

function restrictNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseFloat(val);
	
	if(isNaN(num) || val == ''){
		num = 0;
	}
	obj.value = num;
}

function removeStudent(){
		 checkOptionRemove(document.form1.elements["student[]"]);
		 Update_Auto_Complete_Extra_Para();
 		 submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'insert_update.php';
         checkOptionAll(obj.elements["student[]"]);
         //obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         //obj.submit();
}
function formSubmit(obj)
{
	if (obj.flag.value == 0)
	{
		obj.flag.value = 1;
		generalFormSubmitCheck(obj);
		obj.submit();
		return true;
	}
	else
	{
		return finishSelection();
	}
}
function checkForm()
{
	obj = document.form1;
	var cnt = 0;
	
	if(obj.elements["student[]"].length != 0)
	{
		cnt++;
	}
	else
	{
		alert("<?=$i_SmartCard_Payment_Student_Select_Instruction?>");
		return false;
	}
        
	if(obj.amount.value != '')
	{
		<?php if(!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount']){ ?>	
		if(check_numeric(obj.amount,'<?=$default_amount?>','<?=$i_Payment_Warning_InvalidAmount?>')){
		<?php } ?>
		cnt++;
		<?php if(!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount']){ ?>
		}
		<?php } ?>
	}
	else
	{
		alert("<?=$i_SmartCard_Payment_Input_Amount_Instruction?>");
		return false;
	}
		    
	if(cnt==2)
		return formSubmit(obj);
	else
	{
		alert('<?=$i_SmartCard_Payment_Student_Select_Instruction?>');
		return false;
	}
}

function Get_Student_Selection() {
	var FormObj = document.getElementById("form1");
	var OrgTarget = FormObj.target;
	
	FormObj = document.form1;
	FormObj.action = 'choose/index.php?fieldname=student[]';
	FormObj.target = '_blank';
	FormObj.submit();
	
	FormObj.action = '';
	FormObj.target = OrgTarget;
}

$(document).ready(function(){
	Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb');
	$('input#UserClassNameClassNumberSearchTb').focus();
	eFormAppJS.func.init();
});
</script>
<br>
<form name="form1" id="form1" action="" method="post" onsubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$Lang['General']['ChooseUser']?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    					<tr>
	    						<td class="tablerow2" valign="top" width="30%">
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td class="tabletext"><?=$i_general_from_class_group?></td>
									</tr>
									<tr>
										<td class="tabletext"><?=$linterface->GET_BTN($button_select, "button", "newWindow('choose/index.php?fieldname=student[]',1);")?></td>
									</tr>
									<tr>
										<td class="tabletext"><i><?=$Lang['General']['Or']?></i></td>
									</tr>
									<tr>
										<td class="tabletext">
											<?=$i_general_search_by_inputformat?>
											<br />
											<div style="float:left;">
												<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />
											</div>
										</td>
									</tr>
									</table>
								</td>
								<td class="tabletext" ><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
								<td class="tablerow2" width="30%">
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td><i><?=$Lang['General']['Or']?></i></td>
									</tr>
									<tr>
										<td class="tabletext">
											<?=$Lang['AppNotifyMessage']['msgSearchAndInsertInfo']?>
											<br />
											<div style="float:left;" class="student_textarea">
												<textarea style="height:100px;" id="UserClassNameClassNumberAreaSearchTb" name="UserClassNameClassNumberAreaSearchTb"></textarea><span id="textarea_studentmsg"></span><br><br>
												<?=$linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertStudent'], "button" , "")?>
											</div>
										</td>
									</tr>
									</table>
								</td>
								<td class="tabletext" ><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	    						<td valign="top" width="40%">
	    							<?=$select_student?>
	    							<div style="margin-top:8px;float:right;"><?=$linterface->GET_BTN($button_remove, "button", "removeStudent();") ?></div>
	    						</td>
	    					</tr>
	    				</table>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_Amount?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="amount" value="<?=$default_amount?>" onchange="restrictNumber(this);" />
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "this.form.flag.value=1") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='list.php?ItemID=$ItemID'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="flag" value="0">
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
<input type="hidden" name="student_list" value="<?=$student_list?>">
</form>

<?																																																																																													
$linterface->LAYOUT_STOP();
intranet_closedb();
?>