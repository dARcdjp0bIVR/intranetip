<?php
// Editing by 
/*
 * @Required paramaters: 
 * $ItemID : int
 * $StudentID : array of int
 * $PushMessageType : int
 * $PushMessageTitle : string
 * $MessageContent : string
 * 
 * @Called from: 
 * /home/eAdmin/GeneralMgmt/payment/settings/payment_item/prepare_send_push_message.php
 *
 * 2019-07-24 (Ray):    added payment start date, end date
 * 2018-02-21 (Carlos): modified to support two type of push message.
 * 2017-07-21 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$plugin['eClassApp'] || !isset($ItemID) || !isset($StudentID) || !isset($PushMessageType)) {
	intranet_closedb();
	echo $Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
	exit();
}

$lpayment = new libpayment();

$list = "'".implode("','",(array)$StudentID)."'";

if($PushMessageType == 1){
	// paid - get transaction log record detail
	$sql = "SELECT a.StudentID, t.Amount, t.BalanceAfter as Balance, a.PaymentID, a.RecordStatus, DATE_FORMAT(d.StartDate,'%Y-%m-%d') as StartDate, DATE_FORMAT(d.EndDate,'%Y-%m-%d') as EndDate 
	       FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
		   INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON 
		   t.LogID = (SELECT LogID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE TransactionType='2' AND RelatedTransactionID = a.PaymentID AND TIME_TO_SEC(TIMEDIFF(TransactionTime,a.PaidTime))<10 ORDER BY LogID DESC LIMIT 1)
	       LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
	       LEFT JOIN PAYMENT_PAYMENT_ITEM as d ON d.ItemID = a.ItemID
	       WHERE a.ItemID='$ItemID' AND a.RecordStatus='1' AND a.StudentID IN ($list)  ";
}else{
	// not pay yet - get payment item record
	$sql = "SELECT a.StudentID, a.Amount, c.Balance, a.PaymentID, a.RecordStatus, DATE_FORMAT(d.StartDate,'%Y-%m-%d') as StartDate, DATE_FORMAT(d.EndDate,'%Y-%m-%d') as EndDate  
	        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
	        INNER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
	        LEFT JOIN PAYMENT_PAYMENT_ITEM as d ON d.ItemID = a.ItemID
	        WHERE a.ItemID='$ItemID' AND a.RecordStatus = 0 AND a.StudentID IN ($list)";
}
$result = $lpayment->returnResultSet($sql);

$msg = $Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

if(count($_POST['StudentID'])>0 && count($result)>0){
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	
	$ldbsms 	= new libsmsv2();
	$luser = new libuser();
	$libeClassApp = new libeClassApp();
	
	$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
	$messageContent = standardizeFormPostValue($_POST['MessageContent']);
	$ItemID =  $_POST['ItemID'];
	$itemName = $lpayment->returnPaymentItemName($ItemID);
	$ClickedStudentIDAry = $_POST['StudentID'];

	$isPublic = "N";
	//debug_pr($luser->getParentStudentMappingInfo($studentIds));
	$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($ClickedStudentIDAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
	
	$individualMessageInfoAry = array();
	for ($i=0; $i<sizeof($result); $i++)
	{
 		//list ($studentId, $amount, $balance, $paymentID) = $result[$i];
 		$studentId = $result[$i]['StudentID'];
 		$amount = $result[$i]['Amount'];
 		$balance = $result[$i]['Balance'];
 		$paymentID = $result[$i]['PaymentID'];
 		$startdate = $result[$i]['StartDate'];
 		$enddate = $result[$i]['EndDate'];
		if(in_array($studentId,$ClickedStudentIDAry) && in_array($studentId,$studentWithParentUsingAppAry)){
			$thisStudent = new libuser($studentId);
			$appParentIdAry = $luser->getParentUsingParentApp($studentId);
		
			//	debug_pr($appParentIdAry);
			$_individualMessageInfoAry = array();
			foreach ($appParentIdAry as $parentId) {
				$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
			}
			$_individualMessageInfoAry['messageTitle'] = $messageTitle;
			$_individualMessageInfoAry['messageTitle'] = str_replace('($Payment_StartDate)',$startdate,$_individualMessageInfoAry['messageTitle']);
			$_individualMessageInfoAry['messageTitle'] = str_replace('($Payment_EndDate)',$enddate,$_individualMessageInfoAry['messageTitle']);

			$_individualMessageInfoAry['messageContent'] = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
			//$_individualMessageInfoAry['messageContent'] = $tempContent;
			
			$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Amount)',$amount,$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($payment_balance)',$balance,$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Item)',$itemName,$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($payment_outstanding_amount)', max($amount, $amount-$balance),$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($Inadequate_Amount)',max(0.00,$amount-$balance),$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $_individualMessageInfoAry['messageContent'], '');
			$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_StartDate)',$startdate,$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_EndDate)',$enddate,$_individualMessageInfoAry['messageContent']);
			$individualMessageInfoAry[] = $_individualMessageInfoAry;
		}
	}

	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='ePayment', $moduleRecordID=$ItemID);
	
	$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
}

intranet_closedb();

echo $msg;	
?>