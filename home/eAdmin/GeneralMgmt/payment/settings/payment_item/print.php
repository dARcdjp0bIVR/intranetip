<?php
// editing by

######################################### Change Log ###############################################
# 2020-10-19 by Ray: add print button
# 2019-12-30 by Bill: undo htmlspecialchars in remarks  [2019-1213-0924-18066]
# 2019-08-07 by Carlos: If using direct pay mode, change undo to refund.
# 2019-08-06 by Ray: Added subsidy detail
# 2019-06-17 by Carlos: Added Notice Payment Method column if using payment gateway.
# 2019-05-30 by Carlos: Added display column [Student Subsidy Identity].
# 2016-07-08 by Carlos: Display Remarks(General remark and payment remark) in table column. 
# 2015-11-26 by Carlos: Modified student status filter condition to include left students.
# 2015-10-28 by Carlos: Rename words about students to users.
# 2015-08-04 by Carlos: Search keyword would also count last modified user name.
# 2015-07-07 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added Payment Method filter
# 2014-05-23 by Carlos: Added remark to search condition
# 2014-03-24 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
# 2014-01-30 by Carlos: Fix summary query of subsidy amount and number of subsidy students
# 2013-11-25 by Carlos: Added remark field
# 2013-10-18 by Carlos: Modified table sql to display multiple soure of subsidy in csv format
# 2010-02-10 by Carlos: Split the Student name with class name class number into three column
####################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
//include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$linterface = new interface_html();

//$use_topup = $lpayment->isEWalletTopUpEnabled();
$use_payment_system = $lpayment->isEWalletDirectPayEnabled();

$ItemID = IntegerSafe($ItemID);

//$itemStatus = $lpayment->returnPaymentItemStatus($ItemID);
//$itemName = $lpayment->returnPaymentItemName($ItemID);
$sql = "SELECT a.Name,b.Name,a.DefaultAmount,a.StartDate,a.EndDate,a.RecordStatus FROM PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,6);
list($itemName,$catName,$defaultAmount,$startDate,$endDate,$itemStatus) = $temp[0];
$defaultAmount = $lpayment->getWebDisplayAmountFormat($defaultAmount);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
//if($field > 3) $field+=1; // offset one column after remark field
$order = ($order == 1) ? 1 : 0;

$student_status_cond = "";
if($StudentStatus==1){ ## Removed Student
	$student_status_cond = " AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){ ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL AND b.RecordStatus IN (0,1,2) ";
}else if($StudentStatus==3){
	$student_status_cond = " AND b.UserID IS NOT NULL AND b.RecordStatus='3' ";
}else{ ## All Student
	$student_status_cond = "";
}

$conds = "";
if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = '$RecordStatus' ";
}
if ($ClassName != "")
{
    $conds .= " AND (b.Classname = '$ClassName' OR c.Classname='$ClassName')";
}

if($sys_custom['ePayment']['PaymentMethod'] && $PaymentMethod != '')
{
	$conds .= " AND a.PaymentMethod='$PaymentMethod' ";
}

//$namefield = getNameFieldWithClassNumberByLang("b.");
$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else{
    $archive_namefield = "c.EnglishName";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
}else {
	$Inchargearchive_namefield = " IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}
$last_updated_field = "IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',
											CASE 
												WHEN e.UserID IS NULL AND f.UserID IS NULL THEN a.ProcessingAdminUser 
												WHEN e.UserID IS NOT NULL THEN ".$Inchargenamefield." 
												ELSE ".$Inchargearchive_namefield." 
											END,
											IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',
												a.ProcessingTerminalUser,
												'&nbsp;'
											)
										)";

$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query(trim($keyword));

$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName) as ClassName,
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber) as ClassNumber,
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),
                    CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), 
                    CONCAT(IF(b.UserID IS NOT NULL AND b.RecordStatus='3','<font color=red>#</font>',''), $namefield)
                ) as StudentName,	
                ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
				a.Remark,
                IF(a.SubsidyAmount IS NULL OR a.SubsidyAmount=0,".$lpayment->getWebDisplayAmountFormatDB("0").",".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").") AS SubsidyAmount,
                IFNULL(GROUP_CONCAT(CONCAT(d.UnitName, '($',s.SubsidyAmount,')') SEPARATOR ', '),'&nbsp;') as UnitName,
				i.IdentityName, ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql .= " CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
		/*
				WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' */
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,";
	$sql .= "a.ReceiptRemark,";
}
$sql .= " IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid',IF(a.RecordStatus=2,'".$Lang['ePayment']['Refunded']."','$i_Payment_PaymentStatus_Unpaid')),";
	if($use_payment_system){
		$payment_method_sql = "";
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, false);
			foreach ($service_provider_list as $k => $temp) {
				$payment_method_sql .= "WHEN potl.Sp='" . strtolower($k) . "' THEN '" . $temp . "'";
			}
		}
		$sql .= "CASE
				 WHEN a.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
				 WHEN a.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
				 $payment_method_sql
				 ELSE '' 
				END as NoticePaymentMethod, ";
	}
	$sql .= " ".$last_updated_field." as LastUpdateBy,
               IF(a.PaidTime IS NULL,'&nbsp;',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s'))
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID 
			 LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as s ON s.PaymentID=a.PaymentID AND s.UserID=a.StudentID 
			 LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (s.SubsidyUnitID=d.UnitID) 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s2 ON s2.StudentID=a.StudentID 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s2.IdentityID AND i.RecordStatus='1' 
             LEFT OUTER JOIN INTRANET_USER as e ON e.UserID = a.ProcessingAdminUser
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS f ON f.UserID = a.ProcessingAdminUser 
             LEFT JOIN PAYMENT_TNG_TRANSACTION as potl ON potl.RecordID=(SELECT RecordID FROM PAYMENT_TNG_TRANSACTION WHERE PaymentID=a.PaymentID ORDER BY InputDate DESC LIMIT 1)
             
         WHERE
              a.ItemID = '$ItemID' AND
              (b.EnglishName LIKE '%$safe_keyword%' OR
               b.ChineseName LIKE '%$safe_keyword%' OR
               b.ClassName LIKE '%$safe_keyword%' OR
               b.ClassNumber LIKE '%$safe_keyword%' OR
               c.EnglishName LIKE '%$safe_keyword%' OR
               c.ChineseName LIKE '%$safe_keyword%' OR
               c.ClassName LIKE '%$safe_keyword%' OR
               c.ClassNumber LIKE '%$safe_keyword%' OR
               d.UnitName LIKE '%$safe_keyword%' OR 
				a.Remark LIKE '%$safe_keyword%' OR 
				e.ChineseName LIKE '%$safe_keyword%' OR 
				e.EnglishName LIKE '%$safe_keyword%' 
              )
              $conds $student_status_cond 
         GROUP BY a.PaymentID";

$ldb = new libdb();
# Grab Class list
$class_sql = "SELECT DISTINCT b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			  WHERE
					a.ItemID = '$ItemID' AND
					b.ClassName IS NOT NULL
			  ORDER BY b.ClassName";
$classes = $ldb->returnVector($class_sql);
$select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()",$ClassName,1);

# Grab Summary
$summary_sql = "SELECT a.Amount, a.SubsidyAmount,GROUP_CONCAT(d.SubsidyUnitID) as SubsidyUnitID,a.RecordStatus FROM
	             PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	             LEFT JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID 
				 LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as d ON (a.PaymentID=d.PaymentID) 
				LEFT JOIN PAYMENT_SUBSIDY_UNIT as e ON e.UnitID=d.SubsidyUnitID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID = a.ProcessingAdminUser 
	         WHERE
	              a.ItemID = '$ItemID' AND
	               (b.EnglishName LIKE '%$safe_keyword%' OR
	               b.ChineseName LIKE '%$safe_keyword%' OR
	               b.ClassName LIKE '%$safe_keyword%' OR
	               b.ClassNumber LIKE '%$safe_keyword%' OR
	               c.EnglishName LIKE '%$safe_keyword%' OR
	               c.ChineseName LIKE '%$safe_keyword%' OR
	               c.ClassName LIKE '%$safe_keyword%' OR
	               c.ClassNumber LIKE '%$safe_keyword%' OR
	               e.UnitName LIKE '%$safe_keyword%' OR 
					a.Remark LIKE '%$safe_keyword%' OR 
					u2.ChineseName LIKE '%$safe_keyword%' OR 
					u2.EnglishName LIKE '%$safe_keyword%' 
	              ) 
	              $conds $student_status_cond
	 			GROUP BY a.PaymentID ";

$count_paid = 0;
$count_unpaid = 0;
$count_refund = 0;
$sum_refund = 0;
$sum_paid = 0;
$sum_unpaid = 0;
$sum_subsidy_total = 0;
$count_subsidy_total = 0;
$temp = $ldb->returnArray($summary_sql,4);
for ($i=0; $i<sizeof($temp); $i++)
{
	list($amount,$sub_amount,$sub_unit_id,$status) = $temp[$i];
	if ($status == 1)
	{
		$count_paid++;
		$sum_paid += $amount;
	} else {
		if($status == 2) {
			$count_refund++;
			$sum_refund += $amount;
		} else {
			$count_unpaid++;
			$sum_unpaid += $amount;
		}
	}
	if($sub_unit_id>0){
		$count_subsidy_total++;
	}
	if($sub_amount>0){
		$sum_subsidy_total+=$sub_amount;
	}
}
$sum_total = $sum_paid + $sum_unpaid;
$count_total = $count_paid + $count_unpaid;
$sum_item_total = number_format($sum_paid+$sum_subsidy_total, 2);
$sum_paid = number_format($sum_paid,2);
$sum_unpaid = number_format($sum_unpaid,2);
$sum_total = number_format($sum_total,2);
$sum_subsidy_total = number_format($sum_subsidy_total,2);
$sum_refund = number_format($sum_refund,2);


$subsidy_sql = "SELECT SUM(d.SubsidyAmount) as SubsidyAmount, d.SubsidyUnitID, e.UnitName, COUNT(*) as count FROM
	             PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	             LEFT JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID 
				 LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as d ON (a.PaymentID=d.PaymentID) 
				LEFT JOIN PAYMENT_SUBSIDY_UNIT as e ON e.UnitID=d.SubsidyUnitID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID = a.ProcessingAdminUser 
	         WHERE
	              a.ItemID = '$ItemID' AND
	               (b.EnglishName LIKE '%$safe_keyword%' OR
	               b.ChineseName LIKE '%$safe_keyword%' OR
	               b.ClassName LIKE '%$safe_keyword%' OR
	               b.ClassNumber LIKE '%$safe_keyword%' OR
	               c.EnglishName LIKE '%$safe_keyword%' OR
	               c.ChineseName LIKE '%$safe_keyword%' OR
	               c.ClassName LIKE '%$safe_keyword%' OR
	               c.ClassNumber LIKE '%$safe_keyword%' OR
	               e.UnitName LIKE '%$safe_keyword%' OR 
					a.Remark LIKE '%$safe_keyword%' OR 
					u2.ChineseName LIKE '%$safe_keyword%' OR 
					u2.EnglishName LIKE '%$safe_keyword%' 
	              ) 
	              $conds $student_status_cond
	 			AND d.SubsidyAmount IS NOT NULL
                GROUP BY SubsidyUnitID ORDER BY SubsidyUnitID ";
$temp = $ldb->returnArray($subsidy_sql,4);
$subsidy_detail = '';
for ($i=0; $i<sizeof($temp); $i++) {
	list($sub_amount,$sub_unit_id,$sub_unit_name, $user_count) = $temp[$i];
	$subsidy_detail .= $sub_unit_name.'($'.$sub_amount.')<br>';
}

$infobar="";
//$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
//$infobar .= "<b>$i_Payment_Field_PaymentItem:</b> $itemName<br>\n";
$infobar .= "<span class='$css_title'><b>$itemName</B></span><br><BR>\n";
$infobar .= "<b>$i_Payment_Field_PaymentCategory:</b> $catName<br>\n";
$infobar .= "<b>$i_Payment_PresetPaymentItem_PaymentPeriod:</b> $startDate $i_Profile_To $endDate<br>\n";

$infobar .= "<b>$i_Payment_Field_Chargeable_Amount:</b>$ $sum_total ($count_total $i_Payment_Students)</br>\n";
$infobar .= "<b>$i_Payment_ItemPaid:</b>$ $sum_paid ($count_paid $i_Payment_Students)</br>\n";
$infobar .= "<b>$i_Payment_Subsidy_Total_Subsidy_Amount:</b>$ $sum_subsidy_total ($count_subsidy_total $i_Payment_Students)</br>\n";
$infobar .= "<b>$i_Payment_ItemTotal:</b>$ ".$sum_item_total."</br>\n";
$infobar .= "<b>$i_Payment_ItemUnpaid:</b>$ $sum_unpaid ($count_unpaid $i_Payment_Students)</br>\n";

if($lpayment->isEWalletDirectPayEnabled()) {
	$infobar .= "<b>$i_Payment_RefundAmount:</b>$ $sum_refund ($count_refund $i_Payment_Students)</br>\n";
}

/*
$infobar .= "<b>$i_Payment_Field_PaymentDefaultAmount:</b> $defaultAmount<br>\n";
$infobar .= "<b>$i_Payment_ItemPaid:</b> $sum_paid ($count_paid ".$Lang['ePayment']['users'].")<br>\n";
$infobar .= "<b>$i_Payment_ItemUnpaid:</b> $sum_unpaid ($count_unpaid ".$Lang['ePayment']['users'].")<br>\n";
$infobar .= "<B>$i_Payment_Field_Total_Chargeable_Amount:</b> $sum_total ($count_total ".$Lang['ePayment']['users'].")<br>";
$infobar .= "<B>$i_Payment_Subsidy_Total_Subsidy_Amount:</b> $sum_subsidy_total ($count_subsidy_total ".$Lang['ePayment']['users'].")<br>";
*/

$infobar .="<table border=0 cellpadding=0 cellspacing=0>
    <tr valign=\"top\">
        <td><b>$i_Payment_Subsidy_Total_Subsidy_Amount_Detail:</b>&nbsp;</td>
        <td>$subsidy_detail</td>
    </tr>
</table>";

//$infobar .= "<B>$i_Payment_Subsidy_Total_Subsidy_Amount:</b> <span>$subsidy_detail</span><br>";

# TABLE INFO
/*
$li = new libdbtable($field, $order, $pageNo);
//$li->field_array = array("b.ClassName","b.ClassNumber","b.EnglishName","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->field_array = array("IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;
*/
$field_array = array("ClassName,ClassNumber","ClassNumber","StudentName","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName");
if($sys_custom['ePayment']['PaymentMethod']){
	$field_array = array_merge($field_array, array("PaymentMethod","a.ReceiptRemark"));
	//$field_array = array("ClassName,ClassNumber","ClassNumber","IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName","PaymentMethod","a.ReceiptRemark","a.RecordStatus","LastUpdateBy","a.PaidTime");
}
//else{
	//$field_array = array("ClassName,ClassNumber","ClassNumber","IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName","a.RecordStatus","LastUpdateBy","a.PaidTime");
//}
if($use_payment_system){
	$field_array = array_merge($field_array,array("a.NoticePaymentMethod"));
}
$field_array = array_merge($field_array, array("a.RecordStatus","LastUpdateBy","a.PaidTime"));

$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";

$li = new libdb();
$temp = $li->returnArray($sql, 12);

$css_table = "eSporttableborder";
$css_table_title = "eSporttdborder eSportprinttabletitle";
$x="<table width=95% border=0 cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x.="<thead>";
$x.="<Tr>";
$x.="<td width=1 class='$css_table_title'>#</td>\n";
$x.="<td width=5% class='$css_table_title'>".$i_UserClassName."</td>\n";
$x.="<td width=5% class='$css_table_title'>".$i_UserClassNumber."</td>\n";
$x.="<td width=10% class='$css_table_title'>".$i_UserName."</td>\n";
$x.="<td width=5% class='$css_table_title'>".$i_Payment_Field_Chargeable_Amount."</td>\n";
$x.="<td width=5% class='$css_table_title'>".$Lang['General']['Remark']."</td>\n";
$x.="<td width=5% class='$css_table_title'>".$i_Payment_Subsidy_Amount."</td>\n";
$x.="<td width=15% class='$css_table_title'>".$i_Payment_Subsidy_Unit."</td>\n";
$x.="<td width=5% class='$css_table_title'>".$Lang['ePayment']['SubsidyIdentity']."</td>\n";
if($sys_custom['ePayment']['PaymentMethod']){
	$x.="<td width=5% class='$css_table_title'>".$Lang['ePayment']['PaymentMethod']."</td>\n";
	$x.="<td width=5% class='$css_table_title'>".$Lang['ePayment']['PaymentRemark']."</td>\n";
}
$x.="<td width=5% class='$css_table_title'>".$i_general_status."</td>\n";
if($use_payment_system){
	$x.="<td width=5% class='$css_table_title'>".$Lang['eNotice']['PaymentNoticePaymentMethod']."</td>\n";
}
$x.="<td width=15% class='$css_table_title'>".$i_general_last_modified_by."</td>\n";
$x.= "<td width=15% class='$css_table_title'>".$i_Payment_Field_TransactionTime."</td>\n";
$x.="</tr>";
$x.="</thead>";
$x.="<tbody>";

for($i=0;$i<sizeof($temp);$i++){
	//$css = $i%2==0?"tableContent":"tableContent2";
	//$css =$i%2==0?$css_table_content:$css_table_content."2";
	$css = "eSporttdborder eSportprinttext";
	//if($sys_custom['ePayment']['PaymentMethod']){
	//	list($classname,$classnumber,$name,$amount,$remark,$sub_amount,$sub_unit,$sub_identity,$payment_method,$payment_remark,$paid_status,$admin,$paid_time)=$temp[$i];
	//}else{
	//	list($classname,$classnumber,$name,$amount,$remark,$sub_amount,$sub_unit,$sub_identity,$paid_status,$admin,$paid_time)=$temp[$i];
	//}

	$col = 0;
	$classname = $temp[$i][$col++];
	$classnumber = $temp[$i][$col++];
	$name = $temp[$i][$col++];
	$amount = $temp[$i][$col++];
	$remark = $temp[$i][$col++];
	$sub_amount = $temp[$i][$col++];
	$sub_unit = $temp[$i][$col++];
	$sub_identity = $temp[$i][$col++];
	if($sys_custom['ePayment']['PaymentMethod']){
		$payment_method = $temp[$i][$col++];
		$payment_remark = $temp[$i][$col++];
	}
	$paid_status = $temp[$i][$col++];
	if($use_payment_system){
		$notice_payment_method = $temp[$i][$col++];
	}
	$admin = $temp[$i][$col++];
	$paid_time = $temp[$i][$col++];

	$x.="<tr class='$css'>";
	$x.="<td class='$css'>".($i+1)."</td>";
	$x.="<td class='$css'>$classname&nbsp;</td>";
	$x.="<td class='$css'>$classnumber&nbsp;</td>";
	$x.="<td class='$css'>$name&nbsp;</td>";
	$x.="<td class='$css'>$amount</td>";
	//$x.="<td class='$css'>".Get_String_Display(intranet_htmlspecialchars($remark),0,'&nbsp;')."</td>";
    $x.="<td class='$css'>".intranet_undo_htmlspecialchars(Get_String_Display(intranet_htmlspecialchars($remark),0,'&nbsp;'))."</td>";
	$x.="<td class='$css'>$sub_amount</td>";
	$x.="<td class='$css'>$sub_unit</td>";
	$x.="<td class='$css'>$sub_identity</td>";
	if($sys_custom['ePayment']['PaymentMethod']){
		$x.="<td class='$css'>".$payment_method."</td>";
		$x.="<td class='$css'>".$payment_remark."</td>";
	}
	$x.="<td class='$css'>$paid_status</td>";
	if($use_payment_system){
		$x.="<td class='$css'>".$notice_payment_method."</td>";
	}
	$x.="<td class='$css'>$admin</td>";
	$x.="<td class='$css'>$paid_time</td>";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
	$colspan = 12;
	if($sys_custom['ePayment']['PaymentMethod']) $colspan+=2;
	$x.="<tr><td colspan=\"$colspan\" align=\"center\" class=\"eSporttdborder eSportprinttext\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
}
$x .= "</tbody>";
$x .= "</table>";
$x .= "<br>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>

<style type="text/css" media="print">
thead {display: table-header-group;}
</style>

    <table width="90%" align="center" class="print_hide" border="0">
        <tr>
            <td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit_print")?></td>
        </tr>
    </table>

<table width=95% border=0 cellpadding=0 cellspacing=0 align="center">
<tr class='<?=$css_text?>'><td><?= $infobar ?></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td class='<?=$css_text?>'><?= $i_Payment_Note_StudentRemoved2 ?></td></tr>
<tr><td class='<?=$css_text?>'><?= $i_Payment_Note_StudentLeft ?></td></tr>
</table>

<?php echo $x; ?>

<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>