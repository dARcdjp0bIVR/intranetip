<?
# using: 

##############################################################
#	Date:	2014-08-20	Carlos
#			2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
#
#	Date:	2013-11-25	Carlos
#			add remark for payment item
#
#	Date:	2013-10-18  Carlos
#			Modified to allow input multiple source of subsidy
#
#	Date:	2011-10-25	YatWoon
#			Fixed: failed to store PICAdminID due to using $PHP_AUTH_USER not sesstion UserID ($PHP_AUTH_USER is used in admin console)
#
##############################################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

if(sizeof($PaymentID)<=0){
	header("Location: list.php?ItemID".$ItemID);
}

$li = new libdb();
$list = "'".implode("','",$PaymentID)."'";
$sql="SELECT PaymentID,StudentID,Amount,SubsidyAmount FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ($list)";
$temp = $li->returnArray($sql,4);

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$name_field = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT ppi.Name as ItemName, a.PaymentID, $name_field as StudentName, a.StudentID, a.Amount, a.SubsidyAmount, c.Balance, a.Remark, a.RecordType, a.ProcessingAdminUser, a.DateModified 
	        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
	            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
	        WHERE a.PaymentID IN ($list) ";
	$oldRecords = $lpayment->returnResultSet($sql);	
}

/*$lock_sql="LOCK TABLES PAYMENT_PAYMENT_ITEMSTUDENT as WRITE";
$li->db_db_query($lock_sql);*/

$Result = array();
$li->Start_Trans();
for($i=0;$i<sizeof($temp);$i++){
	list($payment_id,$sid,$old_amount,$old_sub_amount) = $temp[$i];
	$old_sub_amount+=0;
	$old_amount+=0;
	$amount = trim($_REQUEST['new_amount_'.$sid]);
	$pay_amount = $_REQUEST['pay_amount_'.$sid];
	
	$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_SUBSIDY WHERE PaymentID='$payment_id' AND UserID='$sid'";
	$Result['DeleteSubsidy_PaymentID_'.$payment_id.'_StudentID_'.$sid] = $li->db_db_query($sql);
	
	$new_sub_amount = 0;
	$unitid_ary = $_REQUEST['unitid_'.$sid];
	$new_subsidy_amount_ary = $_REQUEST['new_subsidy_amount_'.$sid];
	$sql_insert_subsidy = "INSERT INTO PAYMENT_PAYMENT_ITEM_SUBSIDY (PaymentID,UserID,SubsidyUnitID,SubsidyAmount,SubsidyPICUserID,DateInput,DateModified,InputBy,ModifyBy) VALUES ";
	$sep = "";
	for($j=0;$j<sizeof($new_subsidy_amount_ary);$j++)
	{
		if($unitid_ary[$j] != '' && $new_subsidy_amount_ary[$j]>0) {
			$new_sub_amount += $new_subsidy_amount_ary[$j];
			
			$sql_insert_subsidy .= $sep."('$payment_id','$sid','".$unitid_ary[$j]."','". $new_subsidy_amount_ary[$j]."','".$_SESSION['UserID']."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$sep = ",";
		}
	}
	if($new_sub_amount > 0){
		$Result['InsertSubsidy_PaymentID_'.$payment_id.'_StudentID_'.$sid] = $li->db_db_query($sql_insert_subsidy);
	}
	
	$remark = trim($_REQUEST['Remark_'.$sid]);
	
	$sql ="UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET ";
	$sql.=" Amount='$pay_amount' ";
	$sql.=" ,RecordType=1 ";
	$sql.=" ,ProcessingTerminalUser=NULL ";
	$sql.=" ,ProcessingTerminalIP=NULL ";
	$sql.=" ,ProcessingAdminUser='".$_SESSION['UserID']."' ";
	$sql.=" ,DateModified=NOW() ";
	
	$sql.=" ,SubsidyAmount ='$new_sub_amount' ";
	$sql.=" ,Remark='".$remark."' ";
	/*
	if($unit_id!="")
		$sql.=" ,SubsidyUnitID='$unit_id' ";
	else $sql.=" ,SubsidyUnitID=NULL ";

	if($sub!=$old_sub_amount){
		$sql.=" ,SubsidyPICUserID='".$_SESSION['UserID']."' ";
		$sql.=" ,SubsidyAmount ='$sub' ";
	}
	*/
	
	$sql.=" WHERE PaymentID = '$payment_id' AND RecordStatus = 0";

	$Result['UpdateStudent-PaymentID:'.$payment_id.'-StudentID:'.$sid] = $li->db_db_query($sql);
}

// Update remarks of paid students
for($i=0;$i<count($PaidPaymentID);$i++){
	$paid_payment_id = $PaidPaymentID[$i];
	$paid_student_id = $PaidStudentIDs[$i];
	$remark = trim($_REQUEST['Remark_'.$paid_student_id]);
	
	$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Remark='".$remark."',ProcessingAdminUser='".$_SESSION['UserID']."',DateModified=NOW() WHERE PaymentID='$paid_payment_id' AND StudentID='$paid_student_id'";
	$Result['UpdatePaidStudent-PaymentID:'.$paid_payment_id.'-StudentID:'.$paid_student_id] = $li->db_db_query($sql);
}

/*$unlock_sql="UNLOCK TABLES ";
$li->db_db_query($unlock_sql);*/

/*
$amount = trim($amount);

if ($amount != "")
{
    $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount = '$amount', RecordType = 1, ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER', DateModified=NOW() WHERE PaymentID = $PaymentID AND RecordStatus = 0";
}
$li->db_db_query($sql);
*/

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PaymentStudentItemUpdatedUnsuccess'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PaymentStudentItemUpdatedSuccess'];
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$logDisplayDetail = '';
		$logHiddenDetail = '';
		$name_field = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT ppi.Name as ItemName, a.PaymentID, $name_field as StudentName, a.StudentID, a.Amount, a.SubsidyAmount, c.Balance, a.Remark, a.RecordType, a.ProcessingAdminUser, a.DateModified 
		        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
					INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
		            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
		        WHERE a.PaymentID IN ($list) ";
		$newRecords = $lpayment->returnResultSet($sql);	
		
		for($i=0;$i<count($oldRecords);$i++){
			$logDisplayDetail .= 'Item: '.$oldRecords[$i]['ItemName'].' Student: '.$oldRecords[$i]['StudentName'].'<br />';
			foreach($oldRecords[$i] as $key => $val){

				if($oldRecords[$i][$key] == "") {
					$oldRecords[$i][$key] = 0;
				}

				if($oldRecords[$i][$key] != $newRecords[$i][$key]){
					$logDisplayDetail .= $key.': from '.$oldRecords[$i][$key].' to '.$newRecords[$i][$key].'<br />';
				}
				$logHiddenDetail .= $key.': '.$oldRecords[$i][$key].' | '.$newRecords[$i][$key].'<br />';
			}
			$logDisplayDetail.= '<br />';
		}
		
		$logType = $lpayment->getPaymentItemChangeLogType("EditStudent");
		$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);		
	}
}
header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&Msg=".urlencode($Msg));
intranet_closedb();
?>