<?php
// editing by 
/*
 * 2019-11-26 (Carlos): Cater display refunded status.
 * 2015-11-26 (Carlos): Modified to include left students.
 * 2015-10-28 (Carlos): Include staff users.
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2012-06-08 (Carlos): Only display unpaid payment item students to proceed delete
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$itemName = $lpayment->returnPaymentItemName($ItemID);

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($itemName);

# Get List
$list = "'".implode("','",$PaymentID)."'";
$namefield = getNameFieldByLang("b.");
$sql = "SELECT a.PaymentID, $namefield, b.ClassName, b.ClassNumber, 
			".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
			IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid',IF(a.RecordStatus=2,'".$Lang['ePayment']['Refunded']."','<span style=''color:red''>$i_Payment_PaymentStatus_Unpaid</span>')) as PayStatus,
			a.RecordStatus   
        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
		LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus in (0,1,2,3) 
        WHERE a.PaymentID IN ($list) 
        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
$result = $lpayment->returnArray($sql,6);
//if (sizeof($result)==0)
//{
//    header("Location: list.php?ItemID=$ItemID");
//    exit();
//}
$removeCount = 0;
$unremoveCount = 0;
$x = "";
$x2 = "";
for ($i=0; $i<sizeof($result); $i++)
{
     list ($PaymentID, $name, $class, $classnum, $amount, $payStatus, $recordStatus) = $result[$i];
	 
	 if($recordStatus != 1){
	     $css = $removeCount%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
	     $x .= "<tr class=\"$css\">\n";
	     $x .= "<td class\"tabletext\">$name</td>\n";
	     $x .= "<td class\"tabletext\">$class</td>\n";
	     $x .= "<td class\"tabletext\">$classnum</td>\n";
	     $x .= "<td class\"tabletext\">$amount</td>\n";
	     $x .= "<td class\"tabletext\">$payStatus</td>\n";
	     $x .= "</tr>\n";
	     
	     $HiddenFields .= "<input type=\"hidden\" name=\"PaymentID[]\" value=\"$PaymentID\">\n";
     	 $removeCount++;
	 }else{
	     $css2 = $unremoveCount%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
	     $x2 .= "<tr class=\"$css2\">\n";
	     $x2 .= "<td class\"tabletext\">$name</td>\n";
	     $x2 .= "<td class\"tabletext\">$class</td>\n";
	     $x2 .= "<td class\"tabletext\">$classnum</td>\n";
	     $x2 .= "<td class\"tabletext\">$amount</td>\n";
	     $x2 .= "<td class\"tabletext\">$payStatus</td>\n";
	     $x2 .= "</tr>\n";
	     
	     $unremoveCount++;
	 }
}

$linterface->LAYOUT_START();
?>

<form name="form1" action="student_remove_confirm.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr><td colspan="5"><?=$Lang['ePayment']['PaymentItemStudentsToDelete']?>: </td></tr>
			    <tr class="tabletop">
					<td class="tabletoplink"><?= $i_UserName ?></td>
					<td class="tabletoplink"><?= $i_UserClassName ?></td>
					<td class="tabletoplink"><?= $i_UserClassNumber ?></td>
					<td class="tabletoplink"><?= $i_Payment_Field_Amount ?></td>
					<td class="tabletoplink"><?= $i_general_status ?></td>
				</tr>
				<?=$x?>
			</table>
		</td>
	</tr>
<?php 
if($unremoveCount > 0){
	echo '<tr>'."\n";
	echo '<td>'."\n";
	//echo 'The following students will not be removed from this payment item: '."\n";
	echo '<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td colspan="5">'.$Lang['ePayment']['PaymentItemStudentsNotToDelete'].': </td></tr>
		    <tr class="tabletop">
				<td class="tabletoplink">'.$i_UserName.'</td>
				<td class="tabletoplink">'.$i_UserClassName.'</td>
				<td class="tabletoplink">'.$i_UserClassNumber.'</td>
				<td class="tabletoplink">'.$i_Payment_Field_Amount.'</td>
				<td class="tabletoplink">'.$i_general_status.'</td>
			</tr>'."\n";
	echo $x2;
	echo '</table>'."\n";
	echo '</td>'."\n";
	echo '</tr>'."\n";
}
?>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
			<?php 
			if($removeCount>0) {
				echo $linterface->GET_ACTION_BTN($Lang['ePayment']['ProceedToDelete'], "submit", "");
			}
			?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()") ?>
		</td>
	</tr>
</table>
<?=$HiddenFields?>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>