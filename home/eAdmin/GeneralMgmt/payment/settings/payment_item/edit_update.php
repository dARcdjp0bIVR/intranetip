<?php
// Editing by 
/*
 * 2020-06-29 (Ray):    Added multi payment gateway
 * 2019-09-27 (Ray):    Added Item Code
 * 2018-07-17 (Carlos): $sys_custom['ePayment']['Alipay'] - apply MerchantAccountID for Alipay.
 * 2016-11-09 (Carlos): $sys_custom['ePayment']['SyncLogAfterChangedPaymentItemName'] - synchronize item name to overall transaction log after item name updated.
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$li = new libdb();

$use_merchant_account = $lpayment->useEWalletMerchantAccount();

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='$ItemID'";
	$records = $lpayment->returnResultSet($sql);
	$oldRecord = $records[0];
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$payment_merchant_account_ids = $lpayment->returnPaymentItemMultiMerchantAccountID($ItemID);
		$oldRecord['multi_payment_gateway'] = implode(',',$payment_merchant_account_ids);
	}
}

$li->Start_Trans();
$ItemName = intranet_htmlspecialchars($ItemName);
$Description = intranet_htmlspecialchars($Description);
$DisplayOrder = intranet_htmlspecialchars($DisplayOrder);
$PayPriority = intranet_htmlspecialchars($PayPriority);
$ItemCode = intranet_htmlspecialchars($ItemCode);

$item_code_valid = true;
if($sys_custom['ePayment_PaymentItem_ItemCode']) {
	if($ItemCode == "") {
		$item_code_valid = false;
	} else {
		$item_code_valid = $lpayment->checkPaymentItemItemCodeValid($ItemCode, $ItemID);
	}
}

if($item_code_valid == true) {
	$fields = "Name = '$ItemName'";
	$fields .= ",CatID = '$CatID'";
	$fields .= ",DisplayOrder = '$DisplayOrder'";
	$fields .= ",PayPriority = '$PayPriority'";
	$fields .= ",Description = '$Description'";
	$fields .= ",StartDate = '$StartDate'";
	$fields .= ",EndDate = '$EndDate'";
	$fields .= ",ProcessingTerminalUser=NULL";
	$fields .= ",ProcessingTerminalIP=NULL";
	$fields .= ",ProcessingAdminUser='" . $_SESSION['UserID'] . "'";
	$fields .= ",DateModified = now()";
	if($sys_custom['ePayment_PaymentItem_ItemCode']) {
		$fields .= ",ItemCode='$ItemCode'";
	}
	if ($ChangeAmount == 1) {
		$fields .= ",DefaultAmount='$Amount' ";
	}


	if ($use_merchant_account) {
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY WHERE ItemID='$ItemID'";
			$Result['DeletePaymentGateway'] = $li->db_db_query($sql);

			$last_MerchantAccountID_value = 'NULL';
			$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
			foreach($service_provider_list as $k=>$temp) {
				$MerchantAccountID_value = ${'MerchantAccountID_'.$k};
				if($MerchantAccountID_value == '') {
					continue;
				}
				$last_MerchantAccountID_value = "'$MerchantAccountID_value'";
				$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY
						(ItemID, MerchantAccountID, DateInput, InputBy)
						VALUES
						($ItemID, '$MerchantAccountID_value', now(), '".$_SESSION['UserID']."')
						";
				$Result['CreatePaymentGateway'.$k] = $li->db_db_query($sql);
			}
			$fields .= ",MerchantAccountID=$last_MerchantAccountID_value ";
		} else {
			$fields .= $MerchantAccountID == '' ? ",MerchantAccountID=NULL " : ",MerchantAccountID='$MerchantAccountID' ";
		}
	}
	
	$sql = "UPDATE PAYMENT_PAYMENT_ITEM SET $fields WHERE ItemID = '$ItemID'";
	$Result['UpdatePaymentItem'] = $li->db_db_query($sql);

# Update Default Amount (Unpaid ONLY)
	if ($ChangeAmount == 1) {
		$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount = '$Amount'
            WHERE ItemID = '$ItemID' AND RecordStatus = 0 AND RecordType = 0";
		$Result['UpdatePaymentStudentAmount'] = $li->db_db_query($sql);
	}

	if ($sys_custom['ePayment']['SyncLogAfterChangedPaymentItemName']) {
		$sql = "SELECT t.LogID 
			FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
			INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON t.RelatedTransactionID=s.PaymentID AND s.StudentID=t.StudentID 
			WHERE t.TransactionType=2 AND s.ItemID='$ItemID'";
		$logIdAry = $li->returnVector($sql);
		if (count($logIdAry) > 0) {
			$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET Details='$ItemName' WHERE LogID IN (" . implode(",", $logIdAry) . ")";
			$li->db_db_query($sql);
		}
	}
} else {
	$Result['ItemCodeDuplicate'] = false;
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PaymentItemEditUnsuccess'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PaymentItemEditSuccess'];
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='$ItemID'";
		$records = $lpayment->returnResultSet($sql);
		$newRecord = $records[0];
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$payment_merchant_account_ids = $lpayment->returnPaymentItemMultiMerchantAccountID($ItemID);
			$newRecord['multi_payment_gateway'] = implode(',',$payment_merchant_account_ids);
		}

		$logDisplayDetail = $oldRecord['Name'].'<br />';
		$logHiddenDetail = '';
		if(count($oldRecord)>0){
			foreach($oldRecord as $key => $val){
				if($oldRecord[$key] != $newRecord[$key]){
					$logDisplayDetail .= $key.': from '.$oldRecord[$key].' to '.$newRecord[$key].'<br />';
				}
				$logHiddenDetail .= $key.": ".$oldRecord[$key]." | ".$newRecord[$key].'<br />';
			}
		}
		
		$logType = $lpayment->getPaymentItemChangeLogType("Edit");
		$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);
	}
}

intranet_closedb();
header ("Location: index.php?Msg=".urlencode($Msg));
?>