<?php
// Editing by 
/*
 * 2015-10-28 Carlos : Include staff users to pay.
 * 2015-10-13 Carlos : $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay.
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log 
 * Created on 2012-10-04 
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(count($PaymentID)==0 || count($ItemID)==0 || count($StudentID)==0){
	intranet_closedb();
	header("Location: index.php?FromDate=".$FromDate."&ToDate=".$ToDate."&Msg=".urlencode($Lang['ePayment']['PayProcessUnsuccess']));
	exit;
}

$lpayment = new libpayment();

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ('".implode("','",(array)$ItemID)."')";
	$oldRecords = $lpayment->returnResultSet($sql);
}

$AllowNegativeBalance = $sys_custom['ePayment']['AllowNegativeBalance'];

$Result = array();
$lpayment->Start_Trans();

for($i=0;$i<count($PaymentID);$i++){
	if($ItemID[$i] == '' || $StudentID[$i]=='' || $PaymentID[$i]==''){
		$Result[] = false;
	}else{
		$Result[] = $lpayment->Paid_Payment_Item($ItemID[$i],$StudentID[$i],$PaymentID[$i],$AllowNegativeBalance);
	}
}

if (in_array(false,$Result)) {
	$lpayment->RollBack_Trans();
	$Msg = $Lang['ePayment']['PayProcessUnsuccess'];
}
else {
	$lpayment->Commit_Trans();
	$Msg = $Lang['ePayment']['PayProcessSuccess'];
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$logDisplayDetail = '';
		$logHiddenDetail = '';
		for($i=0;$i<count($oldRecords);$i++){
			$logDisplayDetail .= $oldRecords[$i]['Name'].'<br />';
			$logHiddenDetail .= $oldRecords[$i]['ItemID'].' | '.$oldRecords[$i]['Name'].' | $'.$oldRecords[$i]['DefaultAmount'].'<br />';
		}
		$logType = $lpayment->getPaymentItemChangeLogType("PayAll");
		$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);
	}
}

header ("Location: index.php?FromDate=".$FromDate."&ToDate=".$ToDate."&Msg=".urlencode($Msg));
intranet_closedb();
?>