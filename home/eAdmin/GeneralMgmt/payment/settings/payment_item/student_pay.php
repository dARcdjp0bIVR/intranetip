<?php
// Editing by 
/* 
 * 2018-02-21 (Carlos): Disabled send push message function here. Moved the function to prepare_send_push_message.php
 * 2017-09-26 (Carlos): Use flag $sys_custom['ePayment']['DefaultDoNotShowSendingPushMessage'] to control whether hide the [eClass App push message] function layer. 
 * 2017-07-21 (Carlos): For the push message, added push message type (1) Send notification after paid (2) Send reminder notification
 * 2017-06-22 (Anna): $plugin['eClassApp']  - send push message  
 * 2017-02-09 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - added payment time and default payment method.
 * 2015-11-26 (Carlos): Modified to include left students.
 * 2015-10-28 (Carlos): Allow and include staff users to pay. 
 * 2015-10-13 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay.
 * 2015-08-04 (Carlos): Added row number and total amount at table bottom.
 * 2015-03-06 (Carlos): $sys_custom['ePayment']['PaymentMethod'] -  Added [ReceiptRemark]
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-03-24 Carlos : $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
 * 2013-10-28 Carlos : KIS - hide [Current Balance] and [Balance After Pay]
 * 2013-04-15 Carlos : Allow all user status to pay
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !isset($ItemID)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$BulkPayment = $_REQUEST['BulkPayment'];
$ItemID = $_REQUEST['ItemID'];


$lpayment = new libpayment();
$libAppTemplate = new libeClassApp_template();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();


$itemName = $lpayment->returnPaymentItemName($ItemID);

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($i_Payment_action_batchpay);
$PAGE_NAVIGATION[] = array($itemName);

$can_send_push_message = $plugin['eClassApp'];
$can_send_push_message = false;

if($can_send_push_message){
	$Module = "ePayment";
	# check Access Right
	$libAppTemplate->canAccess($Module,$Section);
	
	# Init libeClassApp_template
	$libAppTemplate->setModule($Module);
	$libAppTemplate->setSection($Section);
	//$libAppTemplate->setExcludedTags(array("payment_outstanding_amount","Inadequate_Amount"));
	
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser();
	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
	
	$showSendingPushMessage = isset($sys_custom['ePayment']['DefaultDoNotShowSendingPushMessage'])? !$sys_custom['ePayment']['DefaultDoNotShowSendingPushMessage'] : true;
}
$sql_paid = '';
if($sys_custom['ttmss_ePayment']) {

} else {
	$sql_paid = 'a.RecordStatus = 0 AND';
}

# Get List
$list = "'".implode("','",$PaymentID)."'";
$namefield = getNameFieldByLang("b.");
$archive_namefield = Get_Lang_Selection("d.ChineseName","d.EnglishName");
$order_field = Get_Lang_Selection("b.ChineseName","b.EnglishName");
$sql = "SELECT 
			a.PaymentID,
			IF(b.UserID IS NOT NULL, $namefield, $archive_namefield),
			IF(b.UserID IS NOT NULL, b.ClassName, d.ClassName),
			IF(b.UserID IS NOT NULL, b.ClassNumber, d.ClassNumber),
			a.Amount,
			c.Balance,
			a.RecordStatus
			 ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql .= ",a.PaymentMethod, a.ReceiptRemark ";
}
if($sys_custom['ttmss_ePayment']) {
    $sql .=",a.NoticePaymentMethod ";
}
$sql.=",a.StudentID 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
			LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus in (0,1,2,3) 
			LEFT JOIN INTRANET_ARCHIVE_USER AS d ON a.StudentID = d.UserID 
            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
       WHERE $sql_paid a.PaymentID IN ($list)
       ORDER BY b.ClassName, b.ClassNumber+0, $order_field";

$result = $lpayment->returnArray($sql);
if (sizeof($result)==0)
{
    header("Location: list.php?ItemID=$ItemID");
    exit();
}

if($sys_custom['ePayment']['HartsPreschool']){
	$studentIdToPaymentMethod = $lpayment->getStudentPaymentMethodRecords(Get_Array_By_Key($result,'StudentID'),1);
}

$col_count = 4;
if(!$isKIS) $col_count += 2;
if($sys_custom['ePayment']['HartsPreschool']) $col_count += 1;
if($sys_custom['ePayment']['PaymentMethod']) $col_count += 2;
if($plugin['eClassApp']) $col_count += 1;
$col_width = sprintf("%.2f", 100 / $col_count);

$now = date("Y-m-d H:i:s");
$total = 0.0;
$allEnough = true;
$x = "";

for ($i=0; $i<sizeof($result); $i++)
{
     list ($PaymentID, $name, $class, $classnum, $amount, $balance) = $result[$i];
     $total += $amount;
     $balanceAfter = $balance - $amount;
     //if (abs($balanceAfter) < 0.001)    # Smaller than 0.1 cent
     //{
     //    $balanceAfter = 0;
     //}
     $hidden = '';
     if ($balanceAfter < 0 && !$sys_custom['ePayment']['AllowNegativeBalance'])
     {
     	 $allEnough = false;
         $balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
         $balanceAfter = "<font color=red>$balanceAfter</font>";
         //$allEnough = false;
         //$balanceAfter = number_format($balanceAfter, 1);
         //$balanceAfter = "<font color=red>".'$'."$balanceAfter</font>";
     }
     else
     {
     		$balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
        $hidden = "<input type=hidden name=PaymentID[] value='$PaymentID'>\n";
        //$balanceAfter = '$'.number_format($balanceAfter, 1);
     }  
	
     $css = $i%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
     $x .= "<tr class=\"$css\">\n";
     $x .= "<td class\"tabletext\">".($i+1)."</td>\n";
     $x .= "<td class\"tabletext\">$name</td>\n";
     $x .= "<td class\"tabletext\">$class</td>\n";
     $x .= "<td class\"tabletext\">$classnum</td>\n";
     $x .= "<td class\"tabletext\">".$lpayment->getWebDisplayAmountFormat($amount)."</td>\n";
  if(!$isKIS){
	  if($lpayment->isEWalletDirectPayEnabled() == false) {
		  $x .= "<td class\"tabletext\">" . $lpayment->getWebDisplayAmountFormat($balance) . "</td>\n";
		  $x .= "<td class\"tabletext\">$balanceAfter";
	  }
  }
     $x .= $hidden."\n";
     $x .= "</td>\n";
     if($sys_custom['ePayment']['HartsPreschool']){
     	$x .= "<td class\"tabletext\">".$linterface->GET_TEXTBOX("PaymentTime[".$PaymentID."]", "PaymentTime[".$PaymentID."]", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);'))."</td>\n";
     }
     if($can_send_push_message){
//      	include_once($PATH_WRT_ROOT."includes/libuser.php");
//      	$luser = new libuser();
//      	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
     	if(in_array($result[$i]['StudentID'], $studentWithParentUsingAppAry)){
     		$x .= '<td class="num_check push_message" align="center" valign="top"><input type="checkbox" name="StudentID[]" value="'.$result[$i]['StudentID'].'"></td>';
     	}else{
     		$x .= '<td class="num_check push_message" align="center" valign="top"></td>';
     	}
     }
     if($sys_custom['ePayment']['PaymentMethod']){
     	$payment_method = $result[$i]['PaymentMethod'];
     	if($sys_custom['ePayment']['HartsPreschool'] && $payment_method == '' && isset($studentIdToPaymentMethod[$result[$i]['StudentID']])){
     		$payment_method = $studentIdToPaymentMethod[$result[$i]['StudentID']]['PaymentMethod'];
     	}
		 if($sys_custom['ttmss_ePayment']) {
			 $noFirst = 1;
		 } else {
			 $noFirst = 0;
		 }
     	$x_payment_method = "<td class=\"tabletext\">".$lpayment->getPaymentMethodSelection(array(), " name=\"PaymentMethod[".$PaymentID."]\" ", $payment_method, 0, $noFirst, $Lang['ePayment']['NA'])."</td>";
		 if($sys_custom['ttmss_ePayment']) {
            if($result[$i]['NoticePaymentMethod'] != '') {
				$x_payment_method = "<td class=\"tabletext\">".$result[$i]['NoticePaymentMethod']."</td>";
			}
		 }

		$x .= $x_payment_method;
     	$x .= "<td class=\"tabletext\">".$linterface->GET_TEXTAREA("ReceiptRemark[".$PaymentID."]", $result[$i]['ReceiptRemark'] , 40, 3, "", "", '', '', '', '')."</td>";
     }
     $x .= "</tr>\n";
}

$x .= "<tr class=\"tablebluebottom\"><td class=\"tabletext\" align=\"right\" colspan=\"4\">".$Lang['General']['Total'].":</td><td class=\"tabletext\" colspan=\"".($col_count+1-4)."\">".$lpayment->getWebDisplayAmountFormat($total)."</td></tr>";

// $itemName = $lpayment->returnPaymentItemName($ItemID);

$linterface->LAYOUT_START();
?>

<script language="JavaScript" type="text/javascript">
<?php if($can_send_push_message){ ?>
$(document).ready(function(){
<?php if($showSendingPushMessage){ ?>
	clickedCheckbox(true);
<?php }else{ ?>
	clickedCheckbox(false);
<?php } ?>
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
	var StudentID = "";
	var checkbox = document.getElementsByName("StudentID[]");
	for(var i =0; i<checkbox.length;i++){
		if( checkbox[i].checked){
			StudentID += "," + checkbox[i].value;
		}
	}
	
});

function clickedCheckbox(elementChecked) {
	if (elementChecked) {
		$('div#send_push_message').show();
		$('.push_message').show();
	}
	else {
		$('div#send_push_message').hide();
		$('.push_message').hide();
	}
}

function onPushMessageTypeChanged()
{
	var type = $('input[name="PushMessageType"]:checked').val();
	type == '2'? $('#send_reminder_btn').show():$('#send_reminder_btn').hide();
}

function sendReminderPushMessage()
{
	var title = document.getElementById('PushMessageTitle');
	var content = document.getElementById('MessageContent');
	if (title.value == "") {
		alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>");
		return false;
	} else if (content.value == "") {
		alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>");
		return false;
	}
	if($('input[name="StudentID[]"]:checked').length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
		return false;
	}
	Block_Element('form1','<?=$Lang['General']['Sending...']?>');
	$.post(
		'send_push_message.php',
		$('#form1').serialize(),
		function(returnMsg){
			UnBlock_Element('form1');
			Get_Return_Message(returnMsg);
		}
	);
}
<?php }?>
function checkForm() {
<?php if($can_send_push_message){ ?>
	if(document.getElementById("send_push_message") && document.getElementById("send_push_message").checked){
		var title = document.getElementById('PushMessageTitle');
		var content = document.getElementById('MessageContent');
		if (title.value == "") {
			alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>");
			return false;
		} else if (content.value == "") {
			alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>");
			return false;
		} else {
			return true;
		}
	}
<?php } ?>

    <?php if($sys_custom['ttmss_ePayment'])  {?>
    var payment_method_checked = true;
    $('select[name^="PaymentMethod"]').each(function() {
        var val = $(this).val();
        if(val == "") {
            alert('<?=$i_Payment_Warning_Select_PaymentMethod?>');
            payment_method_checked =false;
            return false;
        }
    });
    if(payment_method_checked == false) {
        return false;
    }
    <?php } ?>
	return true;
}
</script>

<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<br />
<form id="form1" name="form1" action="student_pay_confirm.php" method="post" onsubmit="return checkForm();">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<?php if($can_send_push_message){ ?>
		<tr>
			<td>
				<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
					 	<td>	
							<?= $linterface->Get_Checkbox('send_push_message', 'send_push_message[]', 'send_push_message', $isChecked=$showSendingPushMessage, $Class='', $Lang['AppNotifyMessage']['PushMessage'], 'clickedCheckbox(this.checked);')?>
						</td>
					</tr>	
					<tr>
						<td>
							<div id="send_push_message">
								<table border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
										<td class="tabletext" width="70%">
											<?=$linterface->Get_Radio_Button("PushMessageType_PaidNotification", "PushMessageType", "1", $___isChecked=1, $___Class="", $Lang['ePayment']['SendNotificationAfterPaid'], "onPushMessageTypeChanged();",0)?>&nbsp;&nbsp;
											<?=$linterface->Get_Radio_Button("PushMessageType_Reminder", "PushMessageType", "2", $___isChecked=0, $___Class="", $Lang['ePayment']['SendReminder'], "onPushMessageTypeChanged();",0)?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?=$Lang['SMS']['MessageTemplates']?>
										</td>
										<td class="tabletext" width="70%">
											<?=$libAppTemplate->getMessageTemplateSelectionBox(true);?>
											<span id="AjaxStatus"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?=$Lang['AppNotifyMessage']['Title']?> <span class="tabletextrequire">*</span>
										</td>
										<td class="tabletext" width="70%">
											<?=$linterface->GET_TEXTAREA("PushMessageTitle", $messageTitle, $taCols=70, $taRows=2)?>
											
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?=$Lang['AppNotifyMessage']['Description']?> <span class="tabletextrequire">*</span>
										</td>
										<td class="tabletext" width="70%">
											<?=$linterface->GET_TEXTAREA("MessageContent", $messageContent)?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?=$Lang['eClassApp']['AutoFillItem']?>
										</td>
										<td class="tabletext" width="70%">
											<?=$libAppTemplate->getMessageTagSelectionBox();?>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td align="right"><?=$linterface->GET_SMALL_BTN($Lang['ePayment']['SendReminder'], "button", "sendReminderPushMessage();","send_reminder_btn"," style=\"display:none;\" ")?></td>
									</tr>										
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php } ?>
		<tr>
	    	<td>
	    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				    <tr class="tablebluetop">
				    	<td class="tabletoplink" width="1">#</td>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_UserName ?></td>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_UserClassName ?></td>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_UserClassNumber ?></td>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_Payment_Field_Amount ?></td>
				<?php if(!$isKIS){ ?>
                        <?php if($lpayment->isEWalletDirectPayEnabled() == false) { ?>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_Payment_Field_CurrentBalance ?></td>
						<td class="tabletoplink" width="<?=$col_width?>%"><?= $i_Payment_Field_BalanceAfterPay ?></td>
                        <?php } ?>
				<?php } ?>
				<?php if($sys_custom['ePayment']['HartsPreschool']){ ?>
						<td class="tabletoplink" width="<?=$col_width?>%">
							<?=$Lang['ePayment']['PaymentTime']?><br />
							<?=$linterface->GET_TEXTBOX("MasterPaymentTime", "MasterPaymentTime", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);'))?>
							<a href="javascript:void(0);" onclick="$('input[name*=PaymentTime]').val($('#MasterPaymentTime').val());"><img src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif" height="12" border="0" width="12"></a>
						</td>
				<?php } ?>
				
				<?php if($can_send_push_message){?>
					<td class="tabletoplink push_message"><?=$Lang['AppNotifyMessage']['PushMessage']?><input type="checkbox" onClick="(this.checked)?setChecked(1,this.form,'StudentID[]'):setChecked(0,this.form,'StudentID[]')"></input></td>
     	
				<?php }?>
				<?php if($sys_custom['ePayment']['PaymentMethod']){ ?>
						<td class="tabletoplink" width="<?=$col_width?>%">
							<?= $Lang['ePayment']['PaymentMethod'] ?><br />
                            <?php
                            if($sys_custom['ttmss_ePayment']) {
								$noFirst = 1;
							} else {
								$noFirst = 0;
							}
							?>
							<?=$lpayment->getPaymentMethodSelection(array(), " id=\"MasterPaymentMethod\" name=\"MasterPaymentMethod\" ", "", 0, $noFirst, $Lang['ePayment']['NA'])?>
							<a href="javascript:void(0);" onclick="$('select[name*=PaymentMethod]').val($('#MasterPaymentMethod').val());"><img src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif" height="12" border="0" width="12"></a>
						</td>
						<td class="tabletoplink" width="<?=$col_width?>%">
							<?=$Lang['General']['Remark']?>
						</td>
				<?php } ?>
					</tr>
					<?=$x?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td class="tabletextremark">
						<?=$sys_custom['ePayment']['AllowNegativeBalance']?'':$i_Payment_PaymentItem_Pay_Warning?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		
		<tr>
	    	<td>
			    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
			    	<tr>
	                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	                </tr>
			    </table>
		    </td>
		</tr>
		
		<tr>
		    <td align="center" colspan="2">
			<? //if ($allEnough) { 
				?>
				<?= $linterface->GET_ACTION_BTN($i_Payment_action_proceed_payment, "submit", "") ?>
			<? //}  
			?>
			<? if ($BulkPayment === "1") {?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'list.php?ItemID=".$ItemID."';") ?>
			<? } 
				 else {?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()") ?>
			<?	}?>
			</td>
		</tr>
		
	</table>
	<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
	<input type=hidden name=order value="<?php echo $order; ?>">
	<input type=hidden name=field value="<?php echo $field; ?>">
	<input type=hidden name=ClassName value="<?=$ClassName?>">
	<input type=hidden name=ItemID value="<?=$ItemID?>">
	<?php if($can_send_push_message){?>
		<input type="hidden" name="Module" id="Module" value="<?=$Module?>">
		<input type="hidden" name="Module" id="Section" value="">
	<?php }?>
</form>
<?php if($sys_custom['ePayment']['HartsPreschool']){ ?>
<script type="text/javascript" language="javascript">
function formatDatetimeString(str)
{
	var dt = new Date(str);
	var year = dt.getFullYear();
	var month = dt.getMonth()+1;
	var day = dt.getDate();
	var hour = dt.getHours();
	var min = dt.getMinutes();
	var sec = dt.getSeconds();
	var formatted = year + '-' + (month<10?'0'+month:month) + '-' + (day<10?'0'+day:day) + ' ' + (hour<10?'0'+hour:hour) + ':' + (min<10?'0'+min:min) + ':' + (sec<10?'0'+sec:sec);
	if(isNaN(year) || isNaN(month) || isNaN(day) || isNaN(hour) || isNaN(min) || isNaN(sec)){
		formatted = formatDatetimeString(new Date());
	}
	return formatted;
}
</script>
<?php } ?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>