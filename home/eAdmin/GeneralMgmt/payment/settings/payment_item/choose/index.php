<?php

# Editing by 

####################################################
#	Date:	2018-01-10  Carlos
#			Exclude already added to payment item students in selection list.
#
#	Date:	2017-08-08	Carlos
#			Added group selection.
#
#	Date: 	2015-11-26 	Carlos
#			Modified to allow select left students.
#
#	Date:	2015-10-28  Carlos
#			Added teaching staff and non-teaching staff selection.
#
#	Date:	2013-03-27	Carlos
#			Added [Subject Group] selection
#
#	Date:	2011-07-18	YatWoon
#			Fixed: Missing to check the last character is "," or not (problem: no student for selection due to mysql error)
#
####################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$MODULE_OBJ['title'] = $Lang['General']['ChooseUser'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lidb = new libdb();
$lclass = new libclass();
$subject_class_mapping = new subject_class_mapping();

# Select Target Type
$select_type_ary = array();
$select_type_ary[] = array('1',$Lang['SysMgr']['FormClassMapping']['Class']);
$select_type_ary[] = array('5',$Lang['Group']['GroupMgmt']);
$select_type_ary[] = array('2',$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']);
$select_type_ary[] = array('3',$Lang['Identity']['TeachingStaff']);
$select_type_ary[] = array('4',$Lang['Identity']['NonTeachingStaff']);

$existing_student_ary = array();
if(isset($student_list)){
	# prevent NULL record in the $student_list in the form of two comma ",,"
	$student_list = str_replace(",,", ",", $student_list);
	
	# check the last character is "," or not
	if(substr($student_list, strlen($student_list)-1, 1)==",")
	{
		$student_list = substr($student_list, 0, strlen($student_list)-1);
	}
	$existing_student_ary = explode(',',$student_list);
}

if($targetType == '') {
	$targetType = '1';
}
$select_type = $linterface->GET_SELECTION_BOX($select_type_ary,'name="targetType" onchange="this.form.action=\'\';this.form.submit();"','', $targetType);

if($targetType == '1') {
	# Target Class
	$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);
	
	if ($targetClass != "")
	{
		$name_field = getNameFieldWithClassNumberByLang("a.");
		$sql = "SELECT a.UserID, CONCAT($name_field,IF(a.RecordStatus='3','[".$Lang['Status']['Left']."]','')) FROM INTRANET_USER as a
				WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2,3) AND a.ClassName = '$targetClass' AND a.UserID ";
		# employ the "NOT IN" condition on if $student_list is not empty
		if (trim($student_list) != "")
			$sql .= " NOT IN ($student_list) ";
		$sql .= "ORDER BY a.ClassNumber+0 ";
		$list = $lclass->returnArray($sql,2);
		$select_students = getSelectByArray($list, "size=\"25\" multiple name=\"targetID[]\"");
	}
}

if($targetType == '2') {
	$subject_group_ary = $subject_class_mapping->Get_Subject_Group_Info();
	$subject_group_selection_ary = array();
	if(count($subject_group_ary)>0) {
		foreach($subject_group_ary as $subject_group_id => $subject_group_info) {
			
			$class_title_en = $subject_group_info['ClassTitleEN'];
			$class_title_b5 = $subject_group_info['ClassTitleB5'];
			$subject_desc_en = $subject_group_info['SubjectDescEN'];
			$subject_desc_b5 = $subject_group_info['SubjectDescB5'];
			
			$subject_group_title = Get_Lang_Selection($class_title_b5,$class_title_en);
			$subject_desc = Get_Lang_Selection($subject_desc_b5, $subject_desc_en);
			
			$subject_group_selection_ary[] = array($subject_group_id, $subject_group_title, $subject_desc);
		}
	}
	$select_subject_group = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($subject_group_selection_ary,'name="targetSubjectGroup" onchange="this.form.action=\'\';this.form.submit();"',$Lang['General']['PleaseSelect'],$targetSubjectGroup);
		
	if($targetSubjectGroup != "") {
		$student_ary = $subject_class_mapping->Get_Subject_Group_Student_List($targetSubjectGroup);
		$student_count = count($student_ary);
		$list = array();
		for($i=0;$i<$student_count;$i++) {
			$user_id = $student_ary[$i]['UserID'];
			$student_name = $student_ary[$i]['StudentName'];
			if(!in_array($user_id,$existing_student_ary)){
				$list[] = array($user_id,$student_name);
			}
		}
		
		$select_students = getSelectByArray($list, "size=\"25\" multiple name=\"targetID[]\"");
	}
}

if($targetType == '3' || $targetType == '4'){
	$name_field = getNameFieldByLang2("u.");
	$sql = "SELECT u.UserID, $name_field as UserName 
			FROM INTRANET_USER as u 
			WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STAFF."' ";
	if($targetType == '3'){ // teaching staff
		$sql .= " AND u.Teaching='1' ";
	}else if($targetType == '4'){
		$sql .= " AND (u.Teaching IS NULL OR u.Teaching<>'1' OR u.Teaching='0') ";
	}
	if($student_list != ''){
		$sql .= " AND u.UserID NOT IN ($student_list) ";
	}
	$sql .= " ORDER BY UserName";
	
	$staff_list = $lidb->returnArray($sql);
	
	$select_staff = getSelectByArray($staff_list, "size=\"25\" multiple=\"multiple\" name=\"targetID[]\"");
}

if($targetType == '5'){
	$libgroupcategory = new libgroupcategory();
	$libgrouping = new libgrouping();
	
	$categories = $libgroupcategory->returnAllCat();
	$category_size = count($categories);
	$group_selection = '<select name="targetGroup" id="targetGroup" onchange="document.form1.submit();">';
	$group_selection.= '<option value="">'.$Lang['General']['PleaseSelect'].'</option>';
	for($i=0;$i<$category_size;$i++){
		$groups = $libgrouping->returnCategoryGroups($categories[$i]['GroupCategoryID']);
		$group_size = count($groups);
		if($group_size > 0){
			$group_selection .= '<optgroup label="'.$categories[$i]['CategoryName'].'">';
			for($j=0;$j<$group_size;$j++){
				$group_selection .= '<option value="'.$groups[$j]['GroupID'].'" '.($targetGroup==$groups[$j]['GroupID']?'selected':'').'>'.$groups[$j]['Title'].'</option>';
			}
		}
	}
	$group_selection .= '</select>';
	
	if($targetGroup != ''){
		//$group_users = $libgrouping->returnGroupUsers($targetGroup);
		$sql = "SELECT 
					u.UserID,
					CONCAT(".getNameFieldWithClassNumberByLang("u.").",IF(u.RecordStatus='3','[".$Lang['Status']['Left']."]','')) as UserName 
				FROM INTRANET_USERGROUP as ug 
				INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
				WHERE ug.GroupID='$targetGroup' AND u.RecordStatus IN (0,1,2,3) AND u.RecordType IN (".USERTYPE_STAFF.",".USERTYPE_STUDENT.") ";
		if($student_list != ''){
			$sql .= " AND u.UserID NOT IN ($student_list) ";
		}
		$group_users = $libgrouping->returnArray($sql);
		$group_user_selection = getSelectByArray($group_users, "size=\"25\" multiple name=\"targetID[]\"");
	}
}

?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
     {
         addtext = obj.options[i].text;

         par.checkOptionAdd(parObj, addtext, obj.options[i].value);
         obj.options[i] = null;
		 i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.submitForm();
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

$(document).ready(function(){
	if(opener && opener.window.document.form1 && opener.window.document.form1.student_list){
		document.form1.student_list.value = opener.window.document.form1.student_list.value;
	}
});
</script>

<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$Lang['General']['PleaseSelect']?>:
					</td>
					<td width="80%"><?=$select_type?></td>
				</tr>
			<?php
				if($targetType == '1') { // Class Selection [BEGIN]
			?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$iDiscipline['class']?>:
					</td>
					<td width="80%"><?=$select_class?></td>
				</tr>
				<?php if($targetClass != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $iDiscipline['students']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
				
			<?php
				} // Class Selection [END]  
				else if($targetType == '2') { // Subject Group Selection [BEGIN]
			?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] ?>:
					</td>
					<td width="80%"><?=$select_subject_group?></td>
				</tr>
				
				<?php if($targetSubjectGroup != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $iDiscipline['students']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
			<?php 
				} // Subject Group Selection [END]
				// Staff Selection [BEGIN]
				else if($targetType == '3' || $targetType == '4'){
			?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?=$targetType=='3'?$Lang['Identity']['TeachingStaff']: $Lang['Identity']['NonTeachingStaff']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_staff ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<?php }else if($targetType == '5'){ // Group Selection ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $Lang['Group']['GroupMgmt']?>:</span>
					</td>
					<td width="80%">
						<?= $group_selection ?>
					</td>
				</tr>
				<?php if($targetGroup != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?=$Lang['SysMgr']['RoleManagement']['Users']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $group_user_selection ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
			<?php } // Staff Selection [END] ?>
			
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?=$fieldname?>">
<input type="hidden" name="student_list" value="<?=$student_list?>">
</form>
<br />
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>