<?php
// Editing by 
/*
 * 2019-08-07 by Carlos: If using direct pay mode, change undo to refund.
 * 2019-06-17 by Carlos: Added Notice Payment Method column if using payment gateway.
 * 2019-05-30 by Carlos: Added display column [Student Subsidy Identity].
 * 2016-07-08 by Carlos: Display Remarks(General remark and payment remark) in table column. 
 * 2015-10-28 by Carlos: Rename words about students to users.
 * 2015-08-04 by Carlos: Search keyword would also count last modified user name.
 * 2015-07-07 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added Payment Method filter
 * 2014-05-23 by Carlos: Added remark to search condition
 * 2014-03-24 by Carlos: $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
 * 2013-11-25 by Carlos: Added remark field
 * 2013-10-18 by Carlos: Modified sql to display multiple soure of subsidy in csv format
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

//$use_topup = $lpayment->isEWalletTopUpEnabled();
$use_payment_system = $lpayment->isEWalletDirectPayEnabled();


$ItemID = IntegerSafe($ItemID);



$lexport = new libexporttext();


$li = new libdb();
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
$field += 0;
//if($field > 3) $field+=1; // offset one column after remark field

$student_status_cond="";
if($StudentStatus==1){ ## Removed Student
	$student_status_cond =" AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){ ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL ";
}
else{ ## All Student
	$student_status_cond ="";
}

if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = '$RecordStatus'";
}
if ($ClassName != "")
{
    $conds .= " AND b.Classname = '$ClassName'";
}

if($sys_custom['ePayment']['PaymentMethod'] && $PaymentMethod != ''){
	$conds .= " AND a.PaymentMethod='$PaymentMethod' ";
}

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
}else {
	$Inchargearchive_namefield = "IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}
$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',
											CASE 
												WHEN e.UserID IS NULL AND f.UserID IS NULL THEN a.ProcessingAdminUser 
												WHEN e.UserID IS NOT NULL THEN ".$Inchargenamefield." 
												ELSE ".$Inchargearchive_namefield." 
											END,
											IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',
												a.ProcessingTerminalUser,
												''
											)
										)";

$target_name_field = "IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.EnglishName, b.EnglishName)";

$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query(trim($keyword));

$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName) as ClassName,
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber) as ClassNumber,
			    IF((b.UserID IS NULL AND c.UserID IS NOT NULL),
                    CONCAT('*',$archive_namefield,'</i>'), 
                    CONCAT(IF(b.UserID IS NOT NULL AND b.RecordStatus='3','#',''), $namefield)
            	) as StudentName,
			    IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.UserLogin,b.UserLogin),
                ROUND(a.Amount,2),
				a.Remark,
                ROUND(a.SubsidyAmount,2),
                IFNULL(GROUP_CONCAT(d.UnitName SEPARATOR ', '),'') as UnitName,
				i.IdentityName, ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql.=" CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
		/*
				WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' */
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,";
	$sql .= "a.ReceiptRemark,";
}
$sql.=" IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid',IF(a.RecordStatus=2,'".$Lang['ePayment']['Refunded']."','$i_Payment_PaymentStatus_Unpaid')),";
	if($use_payment_system){
		$payment_method_sql = "";
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, false);
			foreach ($service_provider_list as $k => $temp) {
				$payment_method_sql .= "WHEN potl.Sp='" . strtolower($k) . "' THEN '" . $temp . "'";
			}
		}
		$sql .= "CASE
				 WHEN a.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
				 WHEN a.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
				 $payment_method_sql
				 ELSE '' 
				END as NoticePaymentMethod, ";
	}
    $sql .= " ".$last_updated_field." as LastUpdateBy,
               IF(a.PaidTime IS NULL,'',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s'))
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID 
			 LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as s ON s.PaymentID=a.PaymentID AND s.UserID=a.StudentID 
			 LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (s.SubsidyUnitID=d.UnitID) 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s2 ON s2.StudentID=a.StudentID 
			 LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s2.IdentityID AND i.RecordStatus='1' 
             LEFT OUTER JOIN 
             INTRANET_USER as e 
             ON e.UserID = a.ProcessingAdminUser
             LEFT OUTER JOIN 
             INTRANET_ARCHIVE_USER AS f 
             ON f.UserID = a.ProcessingAdminUser 
             LEFT JOIN PAYMENT_TNG_TRANSACTION as potl ON potl.RecordID=(SELECT RecordID FROM PAYMENT_TNG_TRANSACTION WHERE PaymentID=a.PaymentID ORDER BY InputDate DESC LIMIT 1)
             
         WHERE 
              a.ItemID = '$ItemID' AND
              (b.EnglishName LIKE '%$safe_keyword%' OR
               b.ChineseName LIKE '%$safe_keyword%' OR
               b.ClassName LIKE '%$safe_keyword%' OR
               b.ClassNumber LIKE '%$safe_keyword%' OR
               c.EnglishName LIKE '%$safe_keyword%' OR
               c.ChineseName LIKE '%$safe_keyword%' OR
               c.ClassName LIKE '%$safe_keyword%' OR
               c.ClassNumber LIKE '%$safe_keyword%' OR
               d.UnitName LIKE '%$safe_keyword%' OR 
				a.Remark LIKE '%$safe_keyword%' OR 
				e.ChineseName LIKE '%$safe_keyword%' OR 
				e.EnglishName LIKE '%$safe_keyword%' 
              )
              $conds $student_status_cond 
		GROUP BY a.PaymentID";
# use for ordering the results
$field_array = array("ClassName,ClassNumber","ClassNumber","IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName");
if($sys_custom['ePayment']['PaymentMethod']){
	$field_array = array_merge($field_array, array("PaymentMethod","a.ReceiptRemark"));
	//$field_array = array("ClassName,ClassNumber","ClassNumber","IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName","PaymentMethod","a.ReceiptRemark","a.RecordStatus","LastUpdateBy","a.PaidTime");
}
//else{                        
//	$field_array = array("ClassName,ClassNumber","ClassNumber","IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.Remark","a.SubsidyAmount","d.UnitName","i.IdentityName","a.RecordStatus","LastUpdateBy","a.PaidTime");
//}
if($use_payment_system){
	$field_array = array_merge($field_array,array("a.NoticePaymentMethod"));
}
$field_array = array_merge($field_array, array("a.RecordStatus","LastUpdateBy","a.PaidTime"));

$sql .= " ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");

$result = $li->returnArray($sql,12);
//$x = "\"$i_ClassName\",\"$i_ClassNumber\",\"$i_UserName\",\"$i_UserLogin\",\"$i_Payment_Field_Amount\",\"".$Lang['General']['Remark']."\",\"$i_Payment_Subsidy_Amount\",\"$i_Payment_Subsidy_Unit\",".($sys_custom['ePayment']['PaymentMethod']?"\"".$Lang['ePayment']['PaymentMethod']."\",\"".$Lang['ePayment']['PaymentRemark']."\",":"")."\"$i_general_status\",\"$i_general_last_modified_by\",\"$i_Payment_Field_TransactionTime\"\n";
//$utf_x = $i_ClassName."\t".$i_ClassNumber."\t".$i_UserName."\t".$i_UserLogin."\t".$i_Payment_Field_Amount."\t".$Lang['General']['Remark']."\t".$i_Payment_Subsidy_Amount."\t".$i_Payment_Subsidy_Unit."\t".($sys_custom['ePayment']['PaymentMethod']?$Lang['ePayment']['PaymentMethod']."\t".$Lang['ePayment']['PaymentRemark']."\t":"").$i_general_status."\t".$i_general_last_modified_by."\t".$i_Payment_Field_TransactionTime."\r\n";
//$x = "\"Class Name\",\"Class Number\",\"Student Name\",\"User Login\",\"Amount\",\"Subsidy Amount\",\"Subsidy Unit\",\"Status\",\"Last Modified By\",\"Paid Time\"\n";
$header = array();
$header[] = $i_ClassName;
$header[] = $i_ClassNumber;
$header[] = $i_UserName;
$header[] = $i_UserLogin;
$header[] = $i_Payment_Field_Amount;
$header[] = $Lang['General']['Remark'];
$header[] = $i_Payment_Subsidy_Amount;
$header[] = $i_Payment_Subsidy_Unit;
$header[] = $Lang['ePayment']['SubsidyIdentity'];
if($sys_custom['ePayment']['PaymentMethod']){
	$header[] = $Lang['ePayment']['PaymentMethod'];
	$header[] = $Lang['ePayment']['PaymentRemark'];
}
$header[] = $i_general_status;
if($use_payment_system){
	$header[] = $Lang['eNotice']['PaymentNoticePaymentMethod'];
}
$header[] = $i_general_last_modified_by;
$header[] = $i_Payment_Field_TransactionTime;

$rows = array();
for($i=0;$i<sizeof($result);$i++){
	if($sys_custom['ePayment']['PaymentMethod']){
		list($class_name,$class_num,$student_name,$userlogin,$amount,$remark,$sub_amount,$sub_unit,$sub_identity,$payment_method,$payment_remark,$status,$lastmodifed,$paidtime)=$result[$i];
	}else{
		list($class_name,$class_num,$student_name,$userlogin,$amount,$remark,$sub_amount,$sub_unit,$sub_identity,$status,$lastmodifed,$paidtime)=$result[$i];
	}
	$col = 0;
	$class_name = $result[$i][$col++];
	$class_num = $result[$i][$col++];
	$student_name = $result[$i][$col++];
	$userlogin = $result[$i][$col++];
	$amount = $result[$i][$col++];
	$remark = $result[$i][$col++];
	$sub_amount = $result[$i][$col++];
	$sub_unit = $result[$i][$col++];
	$sub_identity = $result[$i][$col++];
	if($sys_custom['ePayment']['PaymentMethod']){
		$payment_method = $result[$i][$col++];
		$payment_remark = $result[$i][$col++];
	}
	$status = $result[$i][$col++];
	if($use_payment_system){
		$notice_payment_method = $result[$i][$col++];
	}
	$lastmodifed = $result[$i][$col++];
	$paidtime = $result[$i][$col++];
	//$x.="\"$class_name\",\"$class_num\",\"$student_name\",\"$userlogin\",\"$amount\",\"".$remark."\",\"$sub_amount\",\"$sub_unit\",".($sys_custom['ePayment']['PaymentMethod']?"\"$payment_method\",\"".$payment_remark."\",":"")."\"$status\",\"$lastmodifed\",\"$paidtime\"\n";
	//$utf_x.=$class_name."\t".$class_num."\t".$student_name."\t".$userlogin."\t".$amount."\t".$remark."\t".$sub_amount."\t".$sub_unit."\t".($sys_custom['ePayment']['PaymentMethod']?$payment_method."\t".$payment_remark."\t":"").$status."\t".$lastmodifed."\t".$paidtime."\r\n";
	$row = array();
	$row[] = $class_name;
	$row[] = $class_num;
	$row[] = $student_name;
	$row[] = $userlogin;
	$row[] = $amount;
	$row[] = $remark;
	$row[] = $sub_amount;
	$row[] = $sub_unit;
	$row[] = $sub_identity;
	if($sys_custom['ePayment']['PaymentMethod']){
		$row[] = $payment_method;
		$row[] = $payment_remark;
	}
	$row[] = $status;
	if($use_payment_system){
		$row[] = $notice_payment_method;
	}
	$row[] = $lastmodifed;
	$row[] = $paidtime;
	$rows[] = $row;
}
//$x.="\"$i_Payment_Note_StudentRemoved2\"\n";
//$utf_x.= $i_Payment_Note_StudentRemoved2."\r\n";
$row = array($i_Payment_Note_StudentRemoved2,"","","","","","","");
if($sys_custom['ePayment']['PaymentMethod']){
	$row[] = "";
	$row[] = "";
}
$row[] = "";$row[] = "";$row[] = "";
$rows[] = $row;

$row = array($i_Payment_Note_StudentLeft,"","","","","","","");
$rows[] = $row;

// Output the file to user browser
$filename = "studentlist.csv";

intranet_closedb();

//$utf_content = $utf_x;
//$content = $x;

$utf_content = $lexport->GET_EXPORT_TXT($rows, $header, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=0);

$lexport->EXPORT_FILE($filename, $utf_content);
?>