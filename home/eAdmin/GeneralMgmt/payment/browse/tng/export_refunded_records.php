<?php
// Editing by Carlos
/*
 * 2019-07-25 Ray:    Added payment notice filter
 * 2019-06-21 (Carlos): Cater top-up.
 * 2017-09-27 (Carlos): Created. Export an UTF-8 comma separated csv. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$lexport = new libexporttext();

$is_magic_quotes_on = $lpayment->isMagicQuotesOn();
if($is_magic_quotes_on){
	$keyword = stripslashes($keyword);
}
$keyword = trim($keyword);

$merchant_info = $lpayment->getTngMerchantInfo();
$merchant_name = $merchant_info['merchantName'];
$merchant_id = $merchant_info['merchantId'];

//debug_pr($merchant_info);
$sql = "SELECT 
			t.TNGNo,t.RefundStatus,t.PaymentID, s.StudentID, IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,".$lpayment->getExportAmountFormatDB("s.Amount")." as Amount  
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		WHERE t.Sp='tng' AND t.InputDate >= '$date_from 00:00:00' AND t.InputDate <= '$date_to 23:59:59' AND t.ChargeStatus='4' AND t.TNGNo IS NOT NULL AND (t.PaymentID=-1 OR (t.PaymentID<>-1 AND s.RecordStatus IN ('0','2'))) ";
if($keyword != ''){
	$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (i.Name LIKE '%$safe_keyword%' OR u1.".$name_like_field." LIKE '%$safe_keyword%' OR au1.".$name_like_field." LIKE '%$safe_keyword%' OR u1.ClassName LIKE '%$safe_keyword%') ";
}

$sql .= " ORDER BY t.InputDate ";
//debug_pr($sql);
$records = $lpayment->returnResultSet($sql);
$record_size = count($records);
//debug_pr($records);
$sep = ",";
//$header = array("#","TNG No","Mobile No","Amount","Remark","");
$rows = array();

$total_amount = 0.00;

for($i=0;$i<$record_size;$i++){
	$seq = sprintf("%d",$i+1);
	$tng_no = $records[$i]['TNGNo'];
	$mobile_no = $tng_no;
	$total_amount += $records[$i]['Amount'];
	$formatted_amount = sprintf("%d", $records[$i]['Amount'] * 100);
	$remark = "Refund";
	$refund_status = $records[$i]['RefundStatus'];
	$row = array($seq,$tng_no,$mobile_no,$formatted_amount,$remark,$refund_status);
	$rows[] = implode($sep,$row);
}

$header = array($merchant_name,$merchant_id,date("d-m-Y H:i"),count($rows),sprintf("%d",$total_amount*100)," ");
array_unshift($rows,implode($sep,$header));

intranet_closedb();

$utf8_content = implode("\r\n", $rows);
output2browser($utf8_content, "refunded_tng_records.csv");
//$utf8_content = $lexport->GET_EXPORT_TXT($rows, $header, $Delimiter=$sep, $LineBreak="\r\n", $ColumnDefDelimiter=$sep, $DataSize=0, $Quoted="11", $includeLineBreak=0);
//$lexport->EXPORT_FILE("refunded_tng_records.csv", $utf8_content);
?>