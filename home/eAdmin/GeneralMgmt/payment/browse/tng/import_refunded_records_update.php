<?php
// Editing by 
/*
 * 2019-06-21 (Carlos): Cater top-up.
 * 2017-12-22 (Carlos): Cater payment notice related transactions.
 * 2017-11-28 (Carlos): Do not change balance if undo the payment.
 * 2017-11-16 (Carlos): Undo the payment item transaction that was paid after refund. 
 * 2017-09-27 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}



$cur_sessionid = md5(session_id());

$rows_affected = 0;

$sql = "SELECT 
			r.*,
			t.RecordID as TNGRecordID,
			t.PaymentID as TNGPaymentID,
			s.ItemID,
			s.StudentID,
			s.RecordStatus as PaidStatus,
			t.NoticeID,
			t.CreditTransactionID 
		FROM PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS as r 
		LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.RecordID=r.TransactionRecordID 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=r.PaymentID 
		WHERE r.SessionID='$cur_sessionid' 
		ORDER BY r.RecordNumber ";
$records = $lpayment->returnResultSet($sql);
$record_size = count($records);
//debug_pr($sql);
$updated_by = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
$resultAry = array();
for($i=0;$i<$record_size;$i++)
{
	$notice_id = $records[$i]['NoticeID'];
	if($notice_id != ''){
		// find related TNG records with same NoticeID and StudentID
		$sql = "SELECT t.*,a.ItemID,a.StudentID FROM PAYMENT_TNG_TRANSACTION as t 
				INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON a.PaymentID=t.PaymentID 
				 WHERE t.Sp='tng' AND a.StudentID='".$records[$i]['StudentID']."' AND t.NoticeID='$notice_id' AND t.RecordID<>'".$records[$i]['TNGRecordID']."'";
		$related_payment_notice_records = $lpayment->returnResultSet($sql);
		for($j=0;$j<count($related_payment_notice_records);$j++){
			$lpayment->UnPaid_Payment_Item($related_payment_notice_records[$j]['ItemID'],$related_payment_notice_records[$j]['StudentID'],$DoNotChangeBalance=true);
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET RefundStatus='".$records[$i]['RefundStatus']."',ModifiedDate=NOW() WHERE RecordID='".$related_payment_notice_records[$j]['RecordID']."'";
			$lpayment->db_db_query($sql);
		}
	}
	$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET RefundStatus='".$records[$i]['RefundStatus']."',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
	$resultAry[$sql] = $lpayment->db_db_query($sql);
	
	if($records[$i]['PaidStatus']==1 && $records[$i]['ItemID']!='' && $records[$i]['StudentID']!=''){
		$resultAry['UnpaidPaymentItem_'.$records[$i]['ItemID'].'_'.$records[$i]['StudentID']] = $lpayment->UnPaid_Payment_Item($records[$i]['ItemID'],$records[$i]['StudentID'],$DoNotChangeBalance=true);	
	}
}

$rows_affected = count($resultAry);

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "TNGPaymentRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['ImportRefundedRecords'],"",0);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);

if(isset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']))
{
	$Msg = $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'];
	unset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>
<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$rows_affected.' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] ?>
		</td>
	</tr>
	<tr>
		<td class="dotline">
			<img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_refunded_records.php';")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>