<?php
// Editing by 
/*
 * 2019-06-21 (Carlos): Cater top-up.
 * 2017-09-27 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$limport = new libimporttext();
$lf = new libfilesystem();


$filepath = $_FILES['tngfile']['tmp_name'];
$filename = $_FILES['tngfile']['name'];

$format_array = array("#","TNG Number","Mobile Number","Amount","Remark","Refund Status");
$column_size = count($format_array);
if($filepath=="none" || $filepath == "")
{
	# import failed
    intranet_closedb();
    $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
    header("Location: import_refunded_records.php");
    exit();
}

$ext = strtoupper($lf->file_ext($filename));
if($limport->CHECK_FILE_EXT($filename))
{
  # read file into array
  # return 0 if fail, return csv array if success
  //$data = $lf->file_read_csv($filepath);
  //$data = $limport->GET_IMPORT_TXT($filepath);
  $data = array();
  $fp = fopen($filepath,"r");
  if($fp)
  {
  	while (!feof($fp))
    {
       //$line = trim(fgets($fp));
       //if($line == '') continue;
       $row = fgetcsv($fp,1000,',');
       if(count($row) != $column_size) continue;
       //$row = explode(",",$line);
       $data[] = $row;
    }
    fclose($fp);
  }
  
  if(sizeof($data)>0)
  {
  		$toprow = array_shift($data);                   # drop the title bar
  }else
  {
  		$_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	    intranet_closedb();
	    header("Location: import_refunded_records.php");
	    exit();
  }
}

if(count($toprow) != 6){
	$_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
     intranet_closedb();
     header("Location: import_refunded_records.php");
     exit();
}

$merchant_info = $lpayment->getTngMerchantInfo();
$merchant_name = $merchant_info['merchantName'];
$merchant_id = $merchant_info['merchantId'];

list($p_merchant_name,$p_merchant_id,$p_datetime,$p_total_record,$p_total_amount,$p_refund_status) = $toprow;
if($p_merchant_name != $merchant_name || $p_merchant_id != $merchant_id){
	 $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
     intranet_closedb();
     header("Location: import_refunded_records.php");
     exit();
}


$data_size = count($data);
$column_size = count($data[0]);
$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");

$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS (
			RecordID int(11) NOT NULL auto_increment,
			SessionID varchar(300) NOT NULL,
			ImportTime datetime NOT NULL,
			RecordNumber int(11),
			TNGNo varchar(255),
			MobileNumber varchar(255),
			Amount decimal(13,2),
			Remark varchar(255),
			RefundStatus varchar(255),
			TransactionRecordID int(11) DEFAULT NULL COMMENT 'PAYMENT_TNG_TRANSACTION.RecordID',
			PaymentID int(11) DEFAULT NULL, 
			PRIMARY KEY(RecordID),
			INDEX IdxSessionID(SessionID),
			INDEX IdxImportTime(ImportTime),
			INDEX IdxTNGNo(TNGNo)  
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lpayment->db_db_query($sql);

$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
$sql = "DELETE FROM PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
$lpayment->db_db_query($sql);

$insert_values = array();
for($i=0;$i<$data_size;$i++){
	
	$insert_row = "('$cur_sessionid','$cur_time'";
	for($j=0;$j<$column_size;$j++){
		$val = $data[$i][$j];
		if($j == 3){ // amount
			$val = round(str_replace(array('$', '(', ')', ',', '"'),'',$val) / 100,2);
		}
		$insert_row .= ",'".$val."'";
	}
	$insert_row .= ")";
	$insert_values[] = $insert_row;
}

$chunks = array_chunk($insert_values,500);
//$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (SessionID,ImportTime,RecordNumber,PaymentChannel,PaymentDateTime,OrderNo,TNGRefNo,PaymentType,PaymentAmount,RebateAmount,NetPaymentAmount,Remarks) VALUES ";
$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS (SessionID,ImportTime,RecordNumber,TNGNo,MobileNumber,Amount,Remark,RefundStatus) VALUES ";
for($i=0;$i<count($chunks);$i++){
	
	$insert_sql = $sql.implode(",", $chunks[$i]);
	$lpayment->db_db_query($insert_sql);
	//debug_pr($insert_sql);
}

$student_name_field = getNameFieldWithClassNumberByLang("u1.");
$archived_student_name_field = getNameFieldWithClassNumberByLang("au1.");
$top_up_student_name_field = getNameFieldWithClassNumberByLang("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");

$sql = "SELECT 
			r.*,
			t.RecordID as TNGRecordID,
			t.PaymentID as TNGPaymentID,
			IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,
			IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field)))) as StudentName   
		FROM PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS as r 
		LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.TNGNo=r.TNGNo 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		WHERE r.SessionID='$cur_sessionid' 
		ORDER BY r.RecordNumber ";
$records = $lpayment->returnArray($sql);
$record_size = count($records);

$errorsAry = array();
$import_count = 0;

$col_count = count($toprow);
$col_width = round(100 / ($col_count - 1 + 3));
$x = '<table class="common_table_list_v30" width="100%">';
$x .= '<thead>
			<tr>
				<th class="num_check sub_row_top" style="font-weight:bold;">'.$format_array[0].'</th>';
		for($i=1;$i<$col_count;$i++)
		{
			$x .= '<th class="sub_row_top" width="'.$col_width.'%" style="font-weight:bold;">'.$format_array[$i].'</th>';
		}
		$x .= '<th width="'.$col_width.'%">'.$Lang['ePayment']['PaymentItem'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['Identity']['Student'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['General']['Error'].'</th>';
	$x .= '</tr>
		</thead>';
$x .= '<tbody>';
for($i=0;$i<$record_size;$i++)
{
	$x .= '<tr>';
	for($j=3;$j<=8;$j++)
	{
		if($j == 6)
		{
			$x .= '<td>'.$lpayment->getWebDisplayAmountFormat($records[$i][$j]).'</td>';
		}else{
			$x .= '<td>'.$records[$i][$j].'</td>';
		}
	}
	$item_name = $Lang['General']['EmptySymbol'];
	$student_name = $Lang['General']['EmptySymbol'];
	$error_msg = $Lang['General']['EmptySymbol'];
	if($records[$i]['TNGRecordID'] == '' || $records[$i]['TNGPaymentID'] == ''){
		// no TNG transaction record or no payment item record
		$errorsAry[] = $records[$i];
		$error_msg = '<span style="color:red">'.$Lang['ePayment']['InvalidTNGTransactionRecordAndItemNotFound'].'</span>';
	}else{
		$item_name = $records[$i]['ItemName'];
		$student_name = $records[$i]['StudentName'];
		$import_count += 1;
		$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_REFUNDED_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
		$lpayment->db_db_query($sql);
	}
	
	$x .= '<td>'.$item_name.'</td>';
	$x .= '<td>'.$student_name.'</td>';
	$x .= '<td>'.$error_msg.'</td>';
	$x .= '</tr>';
}
$x .= '</tbody>';
$x .= '</table>';

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "TNGPaymentRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['ImportRefundedRecords'],"",0);

$PAGE_NAVIGATION[] = array($Lang['ePayment']['TNGPaymentRecords'],"index.php",0);
$PAGE_NAVIGATION[] = array($Lang['ePayment']['ImportRefundedRecords'],"",1);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

if(isset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']))
{
	$Msg = $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'];
	unset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>
<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<div align="center">
<table width="90%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['TotalRecord']?></td>
		<td class="tabletext"><?=$data_size?></td>
	</tr>
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['SuccessfulRecord']?></td>
		<td class="tabletext"><?=($data_size - sizeof($errorsAry))?></td>
	</tr>
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['FailureRecord']?></td>
		<td class="tabletext"><?=count($errorsAry)?></td>
	</tr>
</table>
<?=$x?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="dotline"><img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td align="center">
<?php
	if (sizeof($errorsAry) == 0 && $import_count >0)
		echo $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_refunded_records_update.php';").'&nbsp;';
	echo $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_refunded_records.php';");
?>
		</td>
	</tr>
</table>
</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>