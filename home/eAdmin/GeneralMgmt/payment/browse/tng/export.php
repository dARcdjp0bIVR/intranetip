<?php
// Editing by 
/*
 * 2019-09-10 Carlos: Modified to left join two PAYMENT_OVERALL_TRANSACTION_LOG tables, one for payment item, one for top-up credit, to avoid use OR condition will do full table scan which is slow.
 * 2019-07-25 Ray:    Added payment notice filter
 * 2019-07-10 (Carlos): Added display column [Post Time].
 * 2019-06-27 (Carlos): Added RefCode.
 * 2019-06-21 (Carlos): Cater top-up.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}


$lexport = new libexporttext();

$is_magic_quotes_on = $lpayment->isMagicQuotesOn();
if($is_magic_quotes_on){
	$keyword = stripslashes($keyword);
}
$keyword = trim($keyword);

$today = date('Y-m-d');
$today_ts = strtotime($today);

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d",getStartOfAcademicYear($today_ts)) : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d",getEndOfAcademicYear($today_ts)) : $date_to;

$student_name_field = getNameFieldWithClassNumberByLang("u1.");
$archived_student_name_field = getNameFieldWithClassNumberByLang("au1.");
$payer_name_field = getNameFieldByLang2("u2.");
$archived_payer_name_field = getNameFieldByLang2("au2.");
$top_up_student_name_field = getNameFieldWithClassNumberByLang("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");

$sql = "SELECT 
			IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,
			IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('*',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('*',$top_up_archived_student_name_field)))) as StudentName,
			t.SpTxNo,
			IF(t.PaymentID='-1',potl2.RefCode,potl.RefCode) as RefCode,
			".$lpayment->getWebDisplayAmountFormatDB("t.NetPaymentAmount")." as PaymentAmount,
			IF(t.PaymentStatus='1','".$Lang['ePayment']['Success']."','".$Lang['ePayment']['Fail']."') as PaymentStatus,
			CASE t.ChargeStatus 
			WHEN '1' THEN '".$Lang['ePayment']['Success']."' 
			WHEN '2' THEN '".$Lang['ePayment']['Pending']."' 
			WHEN '0' THEN '".$Lang['ePayment']['Fail']."'
			WHEN '3' THEN '".$Lang['ePayment']['Voided']."'  
			WHEN '4' THEN IF(t.RefundStatus='Success','".$Lang['ePayment']['Refunded']."','".$Lang['ePayment']['ToBeRefunded']."') 
			END as ChargeStatus,
			DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
			IF(t.PaymentID='-1',potl2.TransactionTime,potl.TransactionTime) as TransactionTime, 
			IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('*',$archived_payer_name_field)) as PayerName   
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.TransactionType='2' AND potl.RelatedTransactionID=t.PaymentID  
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl2 ON potl2.TransactionType='1' AND potl2.RelatedTransactionID=t.CreditTransactionID AND t.PaymentID='-1'
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		WHERE t.Sp='tng' AND t.InputDate >= '$date_from 00:00:00' AND t.InputDate <= '$date_to 23:59:59' ";
if($PaymentStatus != ''){
	$sql .= " AND t.PaymentStatus='$PaymentStatus' ";
}
if($ChargeStatus != ''){
	$sql .= " AND t.ChargeStatus='$ChargeStatus' ";
}
if($keyword != ''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (i.Name LIKE '%$safe_keyword%' OR u1.".$name_like_field." LIKE '%$safe_keyword%' OR au1.".$name_like_field." LIKE '%$safe_keyword%' OR u1.ClassName LIKE '%$safe_keyword%' OR t.SpTxNo LIKE '%$safe_keyword%') ";
}

if($PaymentNoticeID != '') {
	$sql .= " AND i.NoticeID='$PaymentNoticeID' ";
}
if($PaymentItemID != '') {
	$sql .= " AND i.ItemID='$PaymentItemID' ";
}

$header = array($Lang['ePayment']['PaymentItem'],$Lang['ePayment']['Student'],$Lang['ePayment']['TNGRefNo'],$i_Payment_Field_RefCode,$Lang['ePayment']['Amount'],$Lang['ePayment']['PaymentStatus'],$Lang['ePayment']['ChargeStatus'],$Lang['ePayment']['TransactionTime'],$Lang['ePayment']['PostTime'],$Lang['ePayment']['PaidBy']);
$fields = array("ItemName", "StudentName", "t.SpTxNo", "RefCode", "t.NetPaymentAmount+0","t.PaymentStatus","t.ChargeStatus","InputDate","TransactionTime","PayerName");

if($field=="" && $order=="") {
	$field = 7;
	$order = 0;
}

$sql .= " ORDER BY ".$fields[$field].($order==1?" ASC ":" DESC ");

$records = $lpayment->returnResultSet($sql);
$rows = array();

for($i=0;$i<count($records);$i++){
	$row = array();
	foreach($records[$i] as $key => $val){
		$row[] = $val;
	}
	$rows[] = $row;
}

intranet_closedb();

$utf8_content = $lexport->GET_EXPORT_TXT($rows, $header);
$lexport->EXPORT_FILE("TNG_records.csv", $utf8_content);

?>