<?php
// Editing by
/*
 * 2020-05-25 Ray: add RefundDate
 * 2020-05-19 Ray: refund checkbox only show at today
 * 2019-10-28 Carlos: Changed table column [Payment Item] to [Payment Detail]. Only display notice title if it is payment notice related transaction.
 * 					  Changed charge status to settlement status, post time to settlement time.
 * 					  Removed payment item filter.
 * 2019-09-10 Carlos: Modified to left join two PAYMENT_OVERALL_TRANSACTION_LOG tables, one for payment item, one for top-up credit, to avoid use OR condition will do full table scan which is slow.
 * 2019-08-16 Carlos: Added symbol [!] to indicate refunded payment item and hide checkbox to disallow to refund.
 * 2019-08-07 Ray:    Added export
 * 2019-07-25 Ray:    Added payment notice filter
 * 2019-07-10 Carlos: Added display column [Post Time].
 * 2019-06-27 Carlos: Added RefCode.
 * 2019-01-30 Carlos: Cater Alipay Top-up records.
 * 2018-08-02 Carlos: Created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isAlipayCNDirectPayEnabled() || $lpayment->isAlipayCNTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AlipayCNPaymentRecords";

### set cookies
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_alipaycn_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_payment_alipaycn_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_alipaycn_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_payment_alipaycn_page_number!="")
{
	$pageNo = $ck_payment_alipaycn_page_number;
}

if ($ck_payment_alipaycn_page_order!=$order && $order!="")
{
	setcookie("ck_payment_alipaycn_page_order", $order, 0, "", "", 0);
	$ck_payment_alipaycn_page_order = $order;
} else if (!isset($order) && $ck_payment_alipaycn_page_order!="")
{
	$order = $ck_payment_alipaycn_page_order;
}

if ($ck_payment_alipaycn_page_field!=$field && $field!="")
{
	setcookie("ck_payment_alipaycn_page_field", $field, 0, "", "", 0);
	$ck_payment_alipaycn_page_field = $field;
} else if (!isset($field) && $ck_payment_alipaycn_page_field!="")
{
	$field = $ck_payment_alipaycn_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$today = date('Y-m-d');
$today_ts = strtotime($today);

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d",getStartOfAcademicYear($today_ts)) : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d",getEndOfAcademicYear($today_ts)) : $date_to;

$is_magic_quotes_on = $lpayment->isMagicQuotesOn();
if($is_magic_quotes_on){
	$keyword = stripslashes($keyword);
}
$keyword = trim($keyword);

//$sp_map = $lpayment->getPaymentServiceProviderMapping();

$student_name_field = getNameFieldByLang2("u1.");
$archived_student_name_field = getNameFieldByLang2("au1.");
$payer_name_field = getNameFieldByLang2("u2.");
$archived_payer_name_field = getNameFieldByLang2("au2.");
$top_up_student_name_field = getNameFieldByLang2("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");
$epos_student_name_field = getNameFieldByLang2("epos_u1.");
$epos_archived_student_name_field = getNameFieldByLang2("epos_au1.");

$li = new libdbtable2007($field, $order, $pageNo);

$epos_sql_item_name = "''";
$epos_sql_student_name = "''";
$epos_sql_classname = "''";
$epos_sql_class_number = "''";
$epos_sql_refcode = "''";
$epos_sql_join = '';
if($plugin['ePOS']) {
	$epos_sql_item_name = "CONCAT(pos_tran.InvoiceNumber,
			         CONCAT('<a href=\"javascript:void(0);\" onclick=\"viewePOS(',pos_tran.LogID,');\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>')
			         )";
	$epos_sql_student_name = "IF(epos_u1.UserID IS NOT NULL,$epos_student_name_field,IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$epos_archived_student_name_field), ''))";
	$epos_sql_classname = "IF(epos_u1.UserID IS NOT NULL,epos_u1.ClassName,IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',epos_au1.ClassName), ''))";
	$epos_sql_class_number = "IF(epos_u1.UserID IS NOT NULL,".getClassNumberField("epos_u1.").",IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',".getClassNumberField("epos_au1.")."), ''))";
	$epos_sql_refcode = "IF(pos_potl.LogID IS NOT NULL, pos_potl.REFCode, pos_potl2.REFCode)";
	$epos_sql_join = "LEFT JOIN POS_ALIPAY_TRANSACTION as pos_alipay ON t.PaymentID='-2' AND t.ModuleName='ePOS' AND pos_alipay.TempTransactionID=t.ModuleTransactionID
		LEFT JOIN POS_TRANSACTION as pos_tran ON pos_tran.LogID=pos_alipay.TransactionID		
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as pos_potl 
						on pos_tran.LogID = pos_potl.LogID AND (pos_tran.VoidBy IS NULL AND pos_tran.VoidDate IS NULL AND pos_tran.VoidLogID IS NULL ) 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as pos_potl2 
						on pos_potl2.LogID=pos_tran.VoidLogID AND (pos_tran.VoidBy IS NOT NULL AND pos_tran.VoidDate IS NOT NULL AND pos_tran.VoidLogID IS NOT NULL) 
		LEFT JOIN INTRANET_USER as epos_u1 ON epos_u1.UserID=pos_alipay.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as epos_au1 ON epos_au1.UserID=pos_alipay.StudentID ";
}

$refund_cond = " OR DATE_FORMAT(t.InputDate,'%Y-%m-%d')!='$today' ";
if($sys_custom['ePayment']['Refund30Days']) {
	$refund_date = date("Y-m-d", strtotime("-30 days"));
	$refund_cond = " OR DATE_FORMAT(t.InputDate,'%Y-%m-%d')<'$refund_date' ";
}


$sql = "SELECT 
            CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN			    
			        $epos_sql_item_name
			    ELSE
                    CONCAT(IF(t.NoticeID IS NOT NULL,n.Title,IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name)),
                        IF(s.RecordStatus='2',' <span class=\"tabletextrequire\">!</span>',''),
                        CASE 
                            WHEN t.NoticeID IS NOT NULL AND t.NoticeID <> '' THEN CONCAT('<a title=\"".$Lang['ePayment']['ViewNotice']."\" href=\"javascript:void(0);\" onclick=\"viewNotice(',t.NoticeID,',',s.StudentID,');\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>') 
                            ELSE ''
                        END 
                    )
            END as ItemName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_student_name
			    ELSE
			    IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field))))
			END as StudentName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_classname
                ELSE		        
			        IF(u1.UserID IS NOT NULL,u1.ClassName,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',au1.ClassName), IF(u3.UserID IS NOT NULL,u3.ClassName,CONCAT('<span class=\"red\">*</span>',au3.ClassName))))
			END as ClassName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_class_number
			    ELSE
			        IF(u1.UserID IS NOT NULL,".getClassNumberField("u1.").",IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au1.")."), IF(u3.UserID IS NOT NULL,".getClassNumberField("u3.").",CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au3.")."))))
			END as ClassNumber,
			t.SpTxNo,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_refcode
			    ELSE
			        IF(t.PaymentID='-1',potl2.RefCode,potl.RefCode)
			END as RefCode,
			".$lpayment->getWebDisplayAmountFormatDB("SUM(t.NetPaymentAmount)")." as PaymentAmount,
			IF(t.PaymentStatus='1','<span style=\"color:green\">".$Lang['ePayment']['Success']."</span>','<span style=\"color:red\">".$Lang['ePayment']['Fail']."</span>') as PaymentStatus,
			CASE t.ChargeStatus 
			WHEN '1' THEN '<span style=\"color:green\">".$Lang['ePayment']['Success']."</span>' 
			WHEN '2' THEN '<span style=\"color:blue\">".$Lang['ePayment']['Pending']."</span>' 
			WHEN '4' THEN CONCAT('<span style=\"color:purple\">',IF(t.RefundStatus='Success','".$Lang['ePayment']['Refunded']."','".$Lang['ePayment']['ToBeRefunded']."'),'</span>') 
			ELSE '<span style=\"color:red\">".$Lang['ePayment']['Fail']."</span>'
			END as ChargeStatus,
			DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
			IF(t.ChargeStatus='1',t.SettlementTime, '') as SettlementTime,
            t.RefundDate,			
			IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('<span class=\"red\">*</span>',$archived_payer_name_field)) as PayerName,
			IF(s.RecordStatus='2' OR t.ChargeStatus='4' $refund_cond,'&nbsp;',CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',t.RecordID,'\" />')) as Checkbox
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=(SELECT LogID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE TransactionType='2' AND RelatedTransactionID=t.PaymentID ORDER BY TransactionTime DESC LIMIT 1)
		
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl2 ON potl2.TransactionType='1' AND potl2.RelatedTransactionID=t.CreditTransactionID AND t.PaymentID='-1' 
		
		$epos_sql_join
		
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=t.NoticeID 
		WHERE t.sp='alipaycn' AND t.InputDate >= '$date_from 00:00:00' AND t.InputDate <= '$date_to 23:59:59' ";
if($PaymentStatus != ''){
	$sql .= " AND t.PaymentStatus='$PaymentStatus' ";
}
if($ChargeStatus != ''){
	$sql .= " AND t.ChargeStatus='$ChargeStatus' ";
}
if($keyword != ''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (i.Name LIKE '%$safe_keyword%' OR u1.".$name_like_field." LIKE '%$safe_keyword%' OR au1.".$name_like_field." LIKE '%$safe_keyword%' OR u1.ClassName LIKE '%$safe_keyword%' OR t.SpTxNo LIKE '%$safe_keyword%') ";
}
if($PaymentNoticeID != '') {
	$sql .= " AND i.NoticeID='$PaymentNoticeID' ";
}
//if($PaymentItemID != '') {
//    $sql .= " AND i.ItemID='$PaymentItemID' ";
//}
$sql .= " GROUP BY t.SpTxNo ";


$li->field_array = array("ItemName", "StudentName", "ClassName", "ClassNumber", "t.SpTxNo", "RefCode", "t.NetPaymentAmount+0","t.PaymentStatus","t.ChargeStatus","InputDate","SettlementTime","RefundDate","PayerName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 9;
	$li->order = 0;
}


if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_conds = " AND (a.EndDate between '$date_from' AND '$date_to') ";
}else{
	$date_conds =" AND (
									(a.StartDate between '$date_from' AND '$date_to' 
									 or 
									 a.EndDate between '$date_from' AND '$date_to' )) ";
}

$sql = "SELECT b.NoticeID, b.Title FROM PAYMENT_PAYMENT_ITEM as a
LEFT JOIN INTRANET_NOTICE as b ON a.NoticeID=b.NoticeID
WHERE a.NoticeID IS NOT NULL
$date_conds
AND b.RecordStatus=1
GROUP BY b.NoticeID ORDER BY b.Title ASC";
$PaymentNoticeArray = $lpayment->returnArray($sql);

//$PaymentItemArray = array();
//if($PaymentNoticeID != "") {
//	$sql = "SELECT a.ItemID, a.Name FROM PAYMENT_PAYMENT_ITEM as a WHERE a.NoticeID='$PaymentNoticeID' AND a.NoticeID!='' ORDER BY a.Name ASC";
//	$PaymentItemArray = $lpayment->returnArray($sql);
//}

$export_btn = $linterface->GET_LNK_EXPORT_IP25('javascript:exportRecords();');
//$export_refunded_records_btn = $linterface->GET_LNK_EXPORT_IP25('javascript:exportRefundedRecords();', $Lang['ePayment']['ExportRefundedRecords']);
//$import_refunded_records_btn = $linterface->GET_LNK_IMPORT('import_refunded_records.php', $Lang['ePayment']['ImportRefundedRecords'], $____ParOnClick="", $____ParOthers="", $____ParClass="", $____useThickBox=1);

$date_range_filter = '';
$date_range_filter .= $Lang['General']['From']."&nbsp;".$linterface->GET_DATE_PICKER("date_from",$date_from,"","yy-mm-dd","","","","updatePaymentNoticeList();");
$date_range_filter .= $Lang['General']['To']."&nbsp;".$linterface->GET_DATE_PICKER("date_to",$date_to,"","yy-mm-dd","","","","updatePaymentNoticeList();");
$date_range_filter .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "submit", "document.form1.submit();", $ParName="btnSubmit", "", "", $Lang['Btn']['Submit']);

$payment_status_selection_ary = array(array('1',$Lang['ePayment']['Success']), array('0',$Lang['ePayment']['Fail']));
$payment_status_filter = $Lang['ePayment']['PaymentStatus'].': '.$linterface->GET_SELECTION_BOX($payment_status_selection_ary, ' name="PaymentStatus" id="PaymentStatus" onchange="document.form1.submit();" ', $Lang['General']['All'], $PaymentStatus, false);

//$charge_status_selection_ary = array(array('2',$Lang['ePayment']['Pending']) ,array('1',$Lang['ePayment']['Success']), array('0',$Lang['ePayment']['Fail']), array('3',$Lang['ePayment']['Voided']), array('4',$Lang['ePayment']['Refund']));
$charge_status_selection_ary = array(array('2',$Lang['ePayment']['Pending']), array('1',$Lang['ePayment']['Success']), array('0',$Lang['ePayment']['Fail']), array('4',$Lang['ePayment']['Refunded']));
$charge_status_filter = $Lang['ePayment']['SettlementStatus'].': '.$linterface->GET_SELECTION_BOX($charge_status_selection_ary, ' name="ChargeStatus" id="ChargeStatus" onchange="document.form1.submit();" ', $Lang['General']['All'], $ChargeStatus, false);

$payment_notice_filter = '';
$payment_notice_filter .= $Lang['eNotice']['PaymentNotice']."&nbsp;".$linterface->GET_SELECTION_BOX($PaymentNoticeArray,'id="PaymentNoticeID" name="PaymentNoticeID" onchange="onPaymentNoticeListChange();" ', $Lang['General']['All'], $PaymentNoticeID);

$payment_item_filter = '';
//$payment_item_filter .= $Lang['ePayment']['PaymentItem']."&nbsp;".$linterface->GET_SELECTION_BOX($PaymentItemArray,'id="PaymentItemID" name="PaymentItemID" onchange="document.form1.submit();" ', $Lang['General']['All'], $PaymentItemID);

$tool_buttons = array();
$tool_buttons[] = array('set',"javascript:checkEditMultiple2(document.form1,'RecordID[]','voidRecords(4);')", $Lang['ePayment']['EwalletRefund'].'<font class="tabletextrequire">#</font>');
//$tool_buttons[] = array('set',"javascript:checkEditMultiple2(document.form1,'RecordID[]','voidRecords(3);')", $Lang['ePayment']['Void']);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='13%'>".$li->column_IP25($pos++, $Lang['ePayment']['PaymentDetail'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $i_UserName)."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $i_UserClassName)."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $i_UserClassNumber)."</th>\n";
//$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['OrderNo'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['AlipayCN'].' '.$i_Payment_Field_RefCode)."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $i_Payment_Field_RefCode)."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['ePayment']['Amount'])."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['ePayment']['PaymentStatus'])."</th>\n";
$li->column_list .= "<th width='5%'>".$li->column_IP25($pos++, $Lang['ePayment']['SettlementStatus'])."</th>\n";
//$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['General']['Remark'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['TransactionTime'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['SettlementTime'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['RefundTime'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['PaidBy'])."</th>\n";
$li->column_list .= "<th width='1'></th>\n";


$TAGS_OBJ[] = array($Lang['ePayment']['AlipayCN'],"",1);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$ReturnMsg = '';
if(isset($_SESSION['PAYMENT_ALIPAYCN_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['PAYMENT_ALIPAYCN_RETURN_MSG'];
	unset($_SESSION['PAYMENT_ALIPAYCN_RETURN_MSG']);
}
//$ReturnMsg = isset($Lang['General']['ReturnMessage'][$Msg])? $Lang['General']['ReturnMessage'][$Msg] : '';
$linterface->LAYOUT_START($ReturnMsg);
?>
	<br />
	<form id="form1" name="form1" action="" method="POST">
		<div class="table_board">
			<div class="content_top_tool">
				<div class="Conntent_tool">
					<?=$export_btn?>
					<?=$export_refunded_records_btn?>
					<?=$import_refunded_records_btn?>
				</div>
				<div class="Conntent_search"><input type="text" id="keyword" name="keyword" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>" onkeyup="GoSearch(event);"></div>
				<br style="clear:both;">
			</div>
			<br style="clear:both;">
			<div class="table_filter">
				<?=$date_range_filter.' | '.$payment_status_filter.' | '.$charge_status_filter?>
				<br><br>
				<?=$payment_notice_filter?>
				<br><br>
				<?=$payment_item_filter?>
			</div>
			<br style="clear:both">
			<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
			<br style="clear:both">
			<?=$li->display();?>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td class="tabletextremark">
						<div><?=$i_Payment_Note_StudentRemoved?></div>
						<div><span class="tabletextrequire">!</span> - <?=$Lang['ePayment']['PaymentItemRefunded']?></div>
						<!--<div><span class="tabletextrequire">#</span> - <?=$Lang['ePayment']['TNGVoidTodayTransactionRemark']?></div>-->
						<?php if($sys_custom['ePayment']['Refund30Days']) { ?>
                            <div><span class="tabletextrequire">#</span> - <?=$Lang['ePayment']['PaymentItemRefund30Days']?></div>
						<?php } else { ?>
                            <div><span class="tabletextrequire">#</span> - <?=$Lang['ePayment']['PaymentItemRefundToday']?></div>
						<?php } ?>
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
		<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
		<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
		<input type="hidden" name="page_size_change" value="" />
		<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	</form>
	<br />
	<br />
	<script type="text/javascript">
        function onPaymentNoticeListChange() {
            $("#PaymentItemID option[value='']").attr('selected', true);
            document.form1.submit();
        }

        function updatePaymentNoticeList() {
            $.post(
                '../../ajax_get_payment_notice.php',
                $("form[name='form1']").serialize(),
                function(returnMsg){
                    $("#PaymentNoticeID").html(returnMsg);
                }
            );
        }

        function GoSearch(evt)
        {
            var key = evt.which || evt.charCode || evt.keyCode;

            if (key == 13) // enter
                document.form1.submit();
            else
                return false;
        }

        function voidRecords(chargeType)
        {
            var record_objs = document.getElementsByName('RecordID[]');
            var record_ids = [];
            for(var i=0;i<record_objs.length;i++){
                if(record_objs[i].checked){
                    record_ids.push(record_objs[i].value);
                }
            }

            if(record_ids.length > 1) {
                alert(globalAlertMsg1);
                return;
            }

            if(!confirm('<?=$Lang['ePayment']['ConfirmAccountEnoughBalance']?>')) {
                return;
            }

            Block_Element('form1');
            $.post(
                'void_update.php',
                {
                    'ChargeType':chargeType,
                    'RecordID[]':record_ids
                },
                function(returnData){
                    document.form1.submit();
                }
            );
        }
        /*
		function exportRefundedRecords()
		{
			var form_obj = document.form1;
			var old_action = form_obj.action;
			var old_target = form_obj.target;
			form_obj.action = 'export_refunded_records.php';
			form_obj.target = '_blank';
			form_obj.submit();
			form_obj.action = old_action;
			form_obj.target = old_target;
		}
		*/
        function exportRecords()
        {
            var form_obj = document.form1;
            var old_action = form_obj.action;
            var old_target = form_obj.target;
            form_obj.action = 'export.php';
            form_obj.target = '_blank';
            form_obj.submit();
            form_obj.action = old_action;
            form_obj.target = old_target;
        }

        function viewNotice(NoticeId,studentId)
        {
			<?
			if ($intranet_version == "2.5" || $intranet_version == "3.0") {
				//$PathToNotice = '/home/eService/notice/sign.php';
				$PathToNotice = '/home/eService/notice/eNoticePayment_sign.php';
			}
			else {
				$PathToNotice = '/home/notice/sign.php';
			}
			?>
            newWindow('<?=$PathToNotice?>?NoticeID='+NoticeId+'&StudentID='+studentId,10);
        }

        function viewePOS(LogID) {
            newWindow('/home/eAdmin/GeneralMgmt/pos/report/pos_transaction_report/detail.php?LogID='+LogID,10);
        }

	</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>