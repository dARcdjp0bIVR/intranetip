<?php
// Editing by
/*
 * 2020-02-13 Ray:    Created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isFPSDirectPayEnabled() || $lpayment->isFPSTopUpEnabled())) {
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$today = date('Y-m-d');
$today_ts = strtotime($today);

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d",getStartOfAcademicYear($today_ts)) : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d",getEndOfAcademicYear($today_ts)) : $date_to;

$is_magic_quotes_on = $lpayment->isMagicQuotesOn();
if($is_magic_quotes_on){
	$keyword = stripslashes($keyword);
}
$keyword = trim($keyword);

$student_name_field = getNameFieldByLang2("u1.");
$archived_student_name_field = getNameFieldByLang2("au1.");
$payer_name_field = getNameFieldByLang2("u2.");
$archived_payer_name_field = getNameFieldByLang2("au2.");
$top_up_student_name_field = getNameFieldByLang2("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");


$sql = "SELECT 
			CONCAT(IF(t.NoticeID IS NOT NULL,n.Title,IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name)), '') as ItemName,
			IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('*',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('*',$top_up_archived_student_name_field)))) as StudentName,
			IF(u1.UserID IS NOT NULL,u1.ClassName,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',au1.ClassName), IF(u3.UserID IS NOT NULL,u3.ClassName,CONCAT('<span class=\"red\">*</span>',au3.ClassName)))) as ClassName,
			IF(u1.UserID IS NOT NULL,".getClassNumberField("u1.").",IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au1.")."), IF(u3.UserID IS NOT NULL,".getClassNumberField("u3.").",CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au3.").")))) as ClassNumber,
			t.SpTxNo,
			IF(t.PaymentID='-1',potl2.RefCode,potl.RefCode) as RefCode,
			".$lpayment->getWebDisplayAmountFormatDB("SUM(t.NetPaymentAmount)")." as PaymentAmount,
			IF(t.PaymentStatus='1','".$Lang['ePayment']['Success']."','".$Lang['ePayment']['Fail']."') as PaymentStatus,
			CASE t.ChargeStatus 
			WHEN '2' THEN '".$Lang['ePayment']['Success']."' 
			WHEN '4' THEN IF(t.RefundStatus='Success','".$Lang['ePayment']['Refunded']."','".$Lang['ePayment']['ToBeRefunded']."') 
			ELSE '".$Lang['ePayment']['Fail']."'
			END as ChargeStatus,
			DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
			t.RefundDate,
			IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('*',$archived_payer_name_field)) as PayerName 
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=(SELECT LogID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE TransactionType='2' AND RelatedTransactionID=t.PaymentID ORDER BY TransactionTime DESC LIMIT 1)
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl2 ON potl2.TransactionType='1' AND potl2.RelatedTransactionID=t.CreditTransactionID AND t.PaymentID='-1' 
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=t.NoticeID 
		WHERE t.sp='fps' AND t.InputDate >= '$date_from 00:00:00' AND t.InputDate <= '$date_to 23:59:59' ";
if($PaymentStatus != ''){
	$sql .= " AND t.PaymentStatus='$PaymentStatus' ";
}
if($ChargeStatus != ''){
	$sql .= " AND t.ChargeStatus='$ChargeStatus' ";
}
if($keyword != ''){
	$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (i.Name LIKE '%$safe_keyword%' OR u1.".$name_like_field." LIKE '%$safe_keyword%' OR au1.".$name_like_field." LIKE '%$safe_keyword%' OR u1.ClassName LIKE '%$safe_keyword%' OR t.SpTxNo LIKE '%$safe_keyword%') ";
}
if($PaymentNoticeID != '') {
	$sql .= " AND i.NoticeID='$PaymentNoticeID' ";
}
//if($PaymentItemID != '') {
//	$sql .= " AND i.ItemID='$PaymentItemID' ";
//}
$sql .= " GROUP BY t.SpTxNo ";


$header = array($Lang['ePayment']['PaymentDetail'],$i_UserName,$i_UserClassName,$i_UserClassNumber,$Lang['ePayment']['FPS'].' '.$Lang['General']['RefNo'],$i_Payment_Field_RefCode,$Lang['ePayment']['Amount'],$Lang['ePayment']['PaymentStatus'],$Lang['ePayment']['SettlementStatus'],$Lang['ePayment']['TransactionTime'],$Lang['ePayment']['RefundTime'],$Lang['ePayment']['PaidBy']);

$fields = array("ItemName", "StudentName", "ClassName", "ClassNumber", "t.SpTxNo", "RefCode", "t.NetPaymentAmount+0","t.PaymentStatus","t.ChargeStatus","InputDate","RefundDate","PayerName");


if($field=="" && $order=="") {
	$field = 9;
	$order = 0;
}

$sql .= " ORDER BY ".$fields[$field].($order==1?" ASC ":" DESC ");

$records = $lpayment->returnResultSet($sql);

$rows = array();

for($i=0;$i<count($records);$i++){
	$row = array();
	foreach($records[$i] as $key => $val){
		$row[] = $val;
	}
	$rows[] = $row;
}

intranet_closedb();

$utf8_content = $lexport->GET_EXPORT_TXT($rows, $header);
$lexport->EXPORT_FILE("FPS_records.csv", $utf8_content);

?>