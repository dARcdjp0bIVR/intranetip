<?php
// Editing by 
/*
 * 2020-05-25 Ray: Added RefundDate
 * 2019-09-27 Carlos: Fixed void related notice payment items and transaction records with checking of same SpTxNo.
 * 2019-02-13 Carlos: Added checking on balance for refund of top-up.
 * 2019-01-30 Carlos: Cater Alipay Top-up records.
 * 2018-08-02 Carlos: Created to refund Alipay transactions.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isAlipayDirectPayEnabled() || $lpayment->isAlipayTopUpEnabled())) {
	intranet_closedb();
	$_SESSION['PAYMENT_ALIPAY_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	//header ("Location: index.php");
	exit();
}

$today = date("Y-m-d");
$ChargeStatus = $_POST['ChargeType'];
$RecordIDAry = IntegerSafe($_POST['RecordID']);
$sql = "SELECT 
			t.RecordID,t.PaymentID,a.ItemID,
			
			CASE t.PaymentID
				WHEN '-1' THEN
					c.StudentID
				WHEN '-2' THEN
					t.StudentID
				ELSE
					a.StudentID
            END as StudentID,

			t.NetPaymentAmount as Amount,t.ChargeStatus, t.OrderNo, t.SpTxNo, a.RecordStatus as PaidStatus, t.NoticeID, t.CreditTransactionID, t.ModuleName, t.ModuleTransactionID        
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON a.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
 		WHERE t.Sp='alipay' AND t.RecordID IN (".implode(",",$RecordIDAry).") AND t.ChargeStatus<>'3' AND t.ChargeStatus<>'4' ";

if($sys_custom['ePayment']['Refund30Days']) {
	$refund_date = date("Y-m-d", strtotime("-30 days"));
	$sql .= " AND DATE_FORMAT(t.InputDate,'%Y-%m-%d') >= '$refund_date' ";
} else {
	$sql .= " AND DATE_FORMAT(t.InputDate,'%Y-%m-%d')='$today' ";
}

if($ChargeStatus == '3'){ // can only void today's transactions
	//$sql .= " AND DATE_FORMAT(t.InputDate,'%Y-%m-%d')='$today' ";
}
$sql .=	" ORDER BY t.InputDate";
$records = $lpayment->returnResultSet($sql);
$record_size = count($records);

if(!in_array($ChargeType,array(3,4)) || $record_size == 0){
	$_SESSION['PAYMENT_ALIPAY_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	intranet_closedb();
	//header("Location: index.php");
	exit;
}

$lpayment->Start_Trans();

$studentIdToBalance = array();

$resultAry = array();
$erro_code = 0;
for($i=0;$i<$record_size;$i++){
	$record_id = $records[$i]['RecordID']; 
	$payment_id = $records[$i]['PaymentID'];
	$item_id = $records[$i]['ItemID'];
	$student_id = $records[$i]['StudentID'];
	$amount = $records[$i]['Amount'];
	//$balance = $records[$i]['Balance'];
	$charge_status = $records[$i]['ChargeStatus'];
	$order_no = $records[$i]['OrderNo'];
	$sptx_no = $records[$i]['SpTxNo'];
	$paid_status = $records[$i]['PaidStatus'];
	$notice_id = $records[$i]['NoticeID'];
	$credit_transaction_id = $records[$i]['CreditTransactionID'];
	$module_name = $records[$i]['ModuleName'];
	$tempTransactionID = $records[$i]['ModuleTransactionID'];

	//else if($charge_status == '0' || $charge_status == '2'){} // failed to pay or pending
	if($ChargeStatus=='4' && $order_no != '' && $sptx_no != ''){
		
		// if refund for top-up, need to check account balance enough or not to refund
		if($payment_id == -1){
			if(!isset($studentIdToBalance[$student_id])){
				$sql = "SELECT ".($lpayment->getExportAmountFormatDB("Balance"))." as Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$student_id'";
				$balance_record = $lpayment->returnResultSet($sql);
				if(count($balance_record)>0){
					$studentIdToBalance[$student_id] = $balance_record[0]['Balance'];
				}else{
					// no balance record, fail this record
					//$resultAry['NoBalance_'.$record_id] = false;
					continue;
				}
			}
			$balance = $studentIdToBalance[$student_id];
			if($balance < $amount){
				// not enough balance to refund, fail this record
				//$resultAry['NotEnoughBalance_'.$record_id] = false;
				continue;
			}
		}
		
		$refund_response = $lpayment->voidTngTransaction($order_no, $sptx_no);
		$method_result = $refund_response['MethodResult'];

		if($method_result['voidStatus'] == '1') // success
		{
			// update as voided
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='$ChargeStatus',RefundStatus='Success', RefundDate=now() WHERE RecordID='$record_id'";
			$resultAry['Update_'.$record_id] = $lpayment->db_db_query($sql);
			
			if($payment_id == -1){
				$resultAry['Undo_'.$record_id] = $lpayment->Cancel_Cash_Deposit($credit_transaction_id);
				if($resultAry['Undo_'.$record_id]){
					// refund success, substract amount from balance
					$studentIdToBalance[$student_id] -= $amount;
				}
			} else if($payment_id == -2) {
				if($module_name == 'ePOS') {
					$RefCode = '';
					$libpos = new libpos();
					$alipayTransaction = $libpos->getAlipayTransactionByID($tempTransactionID);
					if($alipayTransaction) {
						$sql = "SELECT RefCode FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE LogID = '".$alipayTransaction['TransactionID']."'";
						$temp = $lpayment->returnVector($sql);
						if($temp) {
							$RefCode = $temp[0];
						}
					}

					$balanceAfter = 0;
					$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$student_id'";
					$temp = $lpayment->returnVector($sql);
					if(count($temp) > 0) {
						$balanceAfter = $temp[0];
					}

					$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG (
								StudentID, 
								TransactionType, 
								Amount, 
								RelatedTransactionID, 
								BalanceAfter, 
								TransactionTime, 
								Details,
								InputBy,
								RefCode)
							VALUES (
								'".$student_id."', 
								6,
								'".$amount."',
								NULL,
								'".$balanceAfter."',
								NOW(),
								'".$i_Payment_action_cancel_payment." ".$lpayment->Get_Safe_Sql_Query($Lang['ePOS']['PaidByAlipay'])."',
								'$UserID',
								'$RefCode')";

					$resultAry['Insert:PAYMENT_OVERALL_TRANSACTION_LOG'] = $lpayment->db_db_query($sql);
					$logID = $lpayment->db_insert_id();
				}
			}
			else if($paid_status == '1') // paid
			{
				$resultAry['Undo_'.$record_id] = $lpayment->UnPaid_Payment_Item($item_id,$student_id,$DoNotChangeBalance=true);	
			}
			
			if($notice_id != ''){
				// find related TNG records with same NoticeID and StudentID and same SpTxNo. Must with same SpTxNo as same payment notice maybe paid more than one time
				$sql = "SELECT t.*,a.ItemID,a.StudentID FROM PAYMENT_TNG_TRANSACTION as t 
						INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON a.PaymentID=t.PaymentID 
						 WHERE t.Sp='alipay' AND a.StudentID='$student_id' AND t.NoticeID='$notice_id' AND t.RecordID<>'$record_id' AND t.SpTxNo='$sptx_no' ";
				$related_payment_notice_records = $lpayment->returnResultSet($sql);
				for($j=0;$j<count($related_payment_notice_records);$j++){
					$lpayment->UnPaid_Payment_Item($related_payment_notice_records[$j]['ItemID'],$related_payment_notice_records[$j]['StudentID'],$DoNotChangeBalance=true);
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='$ChargeStatus',RefundStatus='Success', RefundDate=now() WHERE RecordID='".$related_payment_notice_records[$j]['RecordID']."'";
					$lpayment->db_db_query($sql);
				}
			}
		} else {
			$erro_code = $method_result['errorCode'];
		}
	}
}

$success = count($resultAry)>0 && !in_array(false,$resultAry);

if(!$success){
	$lpayment->RollBack_Trans();
}else{
	$lpayment->Commit_Trans();
}

@session_start();
$_SESSION['PAYMENT_ALIPAY_RETURN_MSG'] = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess']."(".$erro_code.")";

intranet_closedb();
//header("Location: index.php");
echo $_SESSION['PAYMENT_ALIPAY_RETURN_MSG'];
//exit();
?>