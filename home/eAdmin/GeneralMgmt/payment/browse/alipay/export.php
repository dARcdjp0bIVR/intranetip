<?php
// Editing by 
/*
 * 2002-10-22 Ray: SpTxNo add '
 * 2020-05-25 Ray: add RefundDate
 * 2019-10-28 Carlos: Changed header column [Payment Item] to [Payment Detail]. Only display notice title if it is payment notice related transaction.
 * 					  Changed charge status to settlement status, post time to settlement time. 
 * 					  No filtering with payment item.
 * 2019-09-10 Carlos: Modified to left join two PAYMENT_OVERALL_TRANSACTION_LOG tables, one for payment item, one for top-up credit, to avoid use OR condition will do full table scan which is slow.
 * 2019-08-07 Ray:    Created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isAlipayDirectPayEnabled() || $lpayment->isAlipayTopUpEnabled())) {
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$today = date('Y-m-d');
$today_ts = strtotime($today);

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d",getStartOfAcademicYear($today_ts)) : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d",getEndOfAcademicYear($today_ts)) : $date_to;

$is_magic_quotes_on = $lpayment->isMagicQuotesOn();
if($is_magic_quotes_on){
	$keyword = stripslashes($keyword);
}
$keyword = trim($keyword);

$student_name_field = getNameFieldByLang2("u1.");
$archived_student_name_field = getNameFieldByLang2("au1.");
$payer_name_field = getNameFieldByLang2("u2.");
$archived_payer_name_field = getNameFieldByLang2("au2.");
$top_up_student_name_field = getNameFieldByLang2("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");
$epos_student_name_field = getNameFieldByLang2("epos_u1.");
$epos_archived_student_name_field = getNameFieldByLang2("epos_au1.");

$epos_sql_item_name = "''";
$epos_sql_student_name = "''";
$epos_sql_classname = "''";
$epos_sql_class_number = "''";
$epos_sql_refcode = "''";
$epos_sql_join = '';
if($plugin['ePOS']) {
	$epos_sql_item_name = "pos_tran.InvoiceNumber";
	$epos_sql_student_name = "IF(epos_u1.UserID IS NOT NULL,$epos_student_name_field,IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$epos_archived_student_name_field), ''))";
	$epos_sql_classname = "IF(epos_u1.UserID IS NOT NULL,epos_u1.ClassName,IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',epos_au1.ClassName), ''))";
	$epos_sql_class_number = "IF(epos_u1.UserID IS NOT NULL,".getClassNumberField("epos_u1.").",IF(epos_au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',".getClassNumberField("epos_au1.")."), ''))";
	$epos_sql_refcode = "IF(pos_potl.LogID IS NOT NULL, pos_potl.REFCode, pos_potl2.REFCode)";
	$epos_sql_join = "LEFT JOIN POS_ALIPAY_TRANSACTION as pos_alipay ON t.PaymentID='-2' AND t.ModuleName='ePOS' AND pos_alipay.TempTransactionID=t.ModuleTransactionID
		LEFT JOIN POS_TRANSACTION as pos_tran ON pos_tran.LogID=pos_alipay.TransactionID		
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as pos_potl 
						on pos_tran.LogID = pos_potl.LogID AND (pos_tran.VoidBy IS NULL AND pos_tran.VoidDate IS NULL AND pos_tran.VoidLogID IS NULL ) 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as pos_potl2 
						on pos_potl2.LogID=pos_tran.VoidLogID AND (pos_tran.VoidBy IS NOT NULL AND pos_tran.VoidDate IS NOT NULL AND pos_tran.VoidLogID IS NOT NULL) 
		LEFT JOIN INTRANET_USER as epos_u1 ON epos_u1.UserID=pos_alipay.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as epos_au1 ON epos_au1.UserID=pos_alipay.StudentID ";
}

$sql = "SELECT 
            CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN			    
			        $epos_sql_item_name
			    ELSE
                    CONCAT(IF(t.NoticeID IS NOT NULL,n.Title,IF(t.PaymentID=-1,'".$i_Payment_TransactionType_Credit."',i.Name)), '')
            END as ItemName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			       $epos_sql_student_name			       
			    ELSE
			    IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field))))
			END as StudentName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_classname
                ELSE		        
			        IF(u1.UserID IS NOT NULL,u1.ClassName,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',au1.ClassName), IF(u3.UserID IS NOT NULL,u3.ClassName,CONCAT('<span class=\"red\">*</span>',au3.ClassName))))
			END as ClassName,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			    	$epos_sql_class_number
			    ELSE
			        IF(u1.UserID IS NOT NULL,".getClassNumberField("u1.").",IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au1.")."), IF(u3.UserID IS NOT NULL,".getClassNumberField("u3.").",CONCAT('<span class=\"red\">*</span>',".getClassNumberField("au3.")."))))
			END as ClassNumber,
			t.SpTxNo as SpTxNo,
			CASE
			    WHEN t.PaymentID='-2' AND t.ModuleName='ePOS' THEN
			        $epos_sql_refcode
			    ELSE
			        IF(t.PaymentID='-1',potl2.RefCode,potl.RefCode)
			END as RefCode,
			".$lpayment->getWebDisplayAmountFormatDB("SUM(t.NetPaymentAmount)")." as PaymentAmount,
			IF(t.PaymentStatus='1','".$Lang['ePayment']['Success']."','".$Lang['ePayment']['Fail']."') as PaymentStatus,
			CASE t.ChargeStatus 
			WHEN '1' THEN '".$Lang['ePayment']['Success']."' 
			WHEN '2' THEN '".$Lang['ePayment']['Pending']."' 
			WHEN '4' THEN IF(t.RefundStatus='Success','".$Lang['ePayment']['Refunded']."','".$Lang['ePayment']['ToBeRefunded']."') 
			ELSE '".$Lang['ePayment']['Fail']."'
			END as ChargeStatus,
			DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
			IF(t.ChargeStatus='1',t.SettlementTime,'') as SettlementTime,  
			t.RefundDate,
			IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('*',$archived_payer_name_field)) as PayerName 
		FROM PAYMENT_TNG_TRANSACTION as t 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl ON potl.LogID=(SELECT LogID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE TransactionType='2' AND RelatedTransactionID=t.PaymentID ORDER BY TransactionTime DESC LIMIT 1)
		LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as potl2 ON potl2.TransactionType='1' AND potl2.RelatedTransactionID=t.CreditTransactionID AND t.PaymentID='-1' 
		
		$epos_sql_join
						
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=t.NoticeID 
		WHERE t.sp='alipay' AND t.InputDate >= '$date_from 00:00:00' AND t.InputDate <= '$date_to 23:59:59' ";
if($PaymentStatus != ''){
	$sql .= " AND t.PaymentStatus='$PaymentStatus' ";
}
if($ChargeStatus != ''){
	$sql .= " AND t.ChargeStatus='$ChargeStatus' ";
}
if($keyword != ''){
	$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (i.Name LIKE '%$safe_keyword%' OR u1.".$name_like_field." LIKE '%$safe_keyword%' OR au1.".$name_like_field." LIKE '%$safe_keyword%' OR u1.ClassName LIKE '%$safe_keyword%' OR t.SpTxNo LIKE '%$safe_keyword%') ";
}
if($PaymentNoticeID != '') {
	$sql .= " AND i.NoticeID='$PaymentNoticeID' ";
}
//if($PaymentItemID != '') {
//	$sql .= " AND i.ItemID='$PaymentItemID' ";
//}
$sql .= " GROUP BY t.SpTxNo ";


$header = array($Lang['ePayment']['PaymentDetail'],$i_UserName,$i_UserClassName,$i_UserClassNumber,$Lang['ePayment']['Alipay'].' '.$i_Payment_Field_RefCode,$i_Payment_Field_RefCode,$Lang['ePayment']['Amount'],$Lang['ePayment']['PaymentStatus'],$Lang['ePayment']['SettlementStatus'],$Lang['ePayment']['TransactionTime'],$Lang['ePayment']['SettlementTime'],$Lang['ePayment']['RefundTime'],$Lang['ePayment']['PaidBy']);

$fields = array("ItemName", "StudentName", "ClassName", "ClassNumber", "SpTxNo", "RefCode", "t.NetPaymentAmount+0","t.PaymentStatus","t.ChargeStatus","InputDate","SettlementTime","RefundDate","PayerName");


if($field=="" && $order=="") {
	$field = 9;
	$order = 0;
}

$sql .= " ORDER BY ".$fields[$field].($order==1?" ASC ":" DESC ");

$records = $lpayment->returnResultSet($sql);

$rows = array();

for($i=0;$i<count($records);$i++){
	$row = array();
	foreach($records[$i] as $key => $val){
		$row[] = $val;
	}
	$rows[] = $row;
}

intranet_closedb();


$format = "csv";

if(intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER') {
	$format = "excel";
}

if($format == "csv") {
	$utf8_content = $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("ALIPAYHK_records.csv", $utf8_content);
} else {
	$objPHPExcel = new PHPExcel();

	$objPHPExcel->setActiveSheetIndex(0);

	$ActiveSheet = $objPHPExcel->getActiveSheet();


	// Build xls header
	for ($j=0; $j<count($header); $j++) {
		$ActiveSheet->setCellValue(chr(65+$j).'1', $header[$j]);
	}

	// Set whole xls format From Cell A2 to end of sheet
	$range = 'A2:'.chr(65+count($header)-1).(count($rows)+1);

	$objPHPExcel->getActiveSheet()
		->getStyle($range)
		->getNumberFormat()
		->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );

	for ($i=0; $i<count($rows); $i++) {
		for ($j=0; $j<count($rows[$i]); $j++) {
			$ActiveSheet->setCellValueExplicit(chr(65+$j).($i+2), $rows[$i][$j], PHPExcel_Cell_DataType::TYPE_STRING);
		}
	}

	// Redirect output to a client's web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="ALIPAYHK_records.xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1201 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');

}
?>