<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_CreditTransaction";

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_credit_transaction_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_credit_transaction_page_number!="")
{
	$pageNo = $ck_data_log_browsing_credit_transaction_page_number;
}

if ($ck_data_log_browsing_credit_transaction_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_credit_transaction_page_order!="")
{
	$order = $ck_data_log_browsing_credit_transaction_page_order;
}

if ($ck_data_log_browsing_credit_transaction_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_credit_transaction_page_field!="")
{
	$field = $ck_data_log_browsing_credit_transaction_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 8;
*/

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	#$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
	$archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

	# date range
	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate' ";

switch($user_type){
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;

	default: $user_cond = "";break;
}

$sql  = "SELECT
			   
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),

               CONCAT('$', FORMAT(a.Amount, 1)) AS Amount,
               CASE a.RecordType
                    WHEN 1 THEN '$i_Payment_Credit_TypePPS'
                    WHEN 2 THEN '$i_Payment_Credit_TypeCashDeposit'
                    WHEN 3 THEN '$i_Payment_Credit_TypeAddvalueMachine'
                    ELSE '$i_Payment_Credit_TypeUnknown' END,
               a.RefCode, a.AdminInCharge, DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i'),
               CONCAT('<input type=checkbox name=TransactionID[] value=', a.TransactionID ,'>')
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID=c.UserID )
             
         WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 
              ) 
             $date_cond 
             $user_cond
                ";
                
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","Amount","a.RecordType","a.RefCode","a.AdminInCharge","a.TransactionTime","a.DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
if ($field=="" && $order=="") {
	$li->field = 8;
	$li->order = 0;
}

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Credit_Method)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_AdminInCharge)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Field_PostTime)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("TransactionID[]")."</td>\n";

## select user
$array_user_name = array($i_identity_student, $i_identity_teachstaff, $i_Payment_All);
$array_user_data = array("2", "1", "");
$select_user = getSelectByValueDiffName($array_user_data,$array_user_name,"name='user_type' onChange='submitForm(this.form);'",$user_type,0,1);

$toolbar = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export_unicode.php?FromDate=$FromDate&ToDate=$ToDate')")." ";
$toolbar .= "<a class=\"contenttool\" href=\"javascript:checkGet(document.form1,'pps_index.php')\">";
$toolbar .= "<img src=\"".$PATH_WRT_ROOT."/images/2007a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
$toolbar .= $button_export_pps."</a>";

$filterbar = $select_user;

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_CreditTransaction, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# Calculate sum
$sum_sql  = "SELECT
               SUM(a.Amount), COUNT(a.Amount)
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
             WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR 
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 

              ) 
              $date_cond 
              $user_cond  
              ";
$ldb = new libdb();
$temp = $ldb->returnArray($sum_sql,2);
list($total_amount, $total_count) = $temp[0];
$total_amount = number_format($total_amount+0,1);
$total_count += 0;

$infobar = "<tr><td colspan=\"2\" class=\"tabletext\"><font color=green><u><b>$i_Payment_ItemSummary</b></u></font></td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_TotalCountTransaction</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$total_count</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemTotal</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $total_amount</td></tr>\n";

?>
<script language="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script language="javascript">
	function checkForm(formObj){
		if(formObj==null)
			return false;
		fromV = formObj.FromDate;
		toV= formObj.ToDate;
		if(!checkDate(fromV)){
			//formObj.FromDate.focus();
			return false;
		}
		else if(!checkDate(toV)){
			//formObj.ToDate.focus();
			return false;
		}
			return true;
	}
	function checkDate(obj){
		if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>"))
			return false;
		return true;
	}
	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
	function exportPage(obj,url){
		old_url = obj.action;
		old_method = obj.method;
	    obj.action =url;
	    obj.method = "get";
	    obj.submit();
	    obj.action = old_url;
	    obj.method = old_method;
	}
</script>
<br />
<form name="form1" method="get" action="index.php">
<!-- Date Range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext"><?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?>:</td>
	</tr>
	<tr>
		<td class="tabletext">
			<?=$i_Profile_From?> <input type="text" name="FromDate" value="<?=$FromDate?>" maxlength="10" class='textboxnum'>
			<?=$linterface->GET_CALENDAR("form1", "FromDate")?> 
			<?=$i_Profile_To?> <input type="text" name="ToDate" value="<?=$ToDate?>" maxlength="10" class='textboxnum'>
			<?=$linterface->GET_CALENDAR("form1", "ToDate")?> 
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td class="tabletext">
			<?= $linterface->GET_BTN($button_submit, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
	<tr>
		<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<!-- Summary -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar?>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?=$lpayment->getResponseMsg($msg)?></td>
	</tr>
	<tr>
		<td class="tabletext">
			<?=$i_Payment_Note_StudentRemoved?>
		</td>
	</tr>
</table>
<!-- Data Table -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$filterbar?></td>
		<td align="right"><?=$searchbar?></td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
