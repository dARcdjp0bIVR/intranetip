<?php
// Editing by 
/*
 * 2020-08-11 (Ray):    Added wechat
 * 2020-07-20 (Ray):    Added visamaster
 * 2019-09-23 (Ray):    Added hour & minute for From/To time
 * 2017-02-02 (Carlos): Added CancelOption: [Within date range] / [Can exceed date range] for Hide cancelled transactions.
 * 2016-08-23 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - added credit methods [2=Cash, 6=Bank transfer, 7=Cheque deposit].
 * 						$sys_custom['ePayment']['CashDepositAccount'] - added [Account] and [ReceiptNo].
 * 2015-10-07 (Carlos): $sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display of [Login ID].
 * 2015-07-22 (Carlos): Added credit type Auto-pay.
 * 2015-02-25 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$li = new libdb();
$order = ($order == 1) ? 1 : 0;
//if ($field == "" || $field>7) $field = 7;
$field += 0;

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("d.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(e.ChineseName IS NULL,e.EnglishName,e.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(e.EnglishName IS NULL,e.ChineseName,e.EnglishName)";
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$FromTime = $WithTime? $FromTime : "00:00:00";
$ToTime = $WithTime? $ToTime : "23:59:59";

//$date_cond = " AND a.TransactionTime Between '$FromDate' AND '$ToDate' ";
if($search_by!=1)                
	$date_cond = " AND DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s') >= '$FromDate $FromTime' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s')<='$ToDate $ToTime' ";
else
  $date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i:%s') >= '$FromDate $FromTime' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i:%s')<='$ToDate $ToTime' ";


switch($user_type){
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;

	default: $user_cond = "";break;
}

switch($CreditType) {
	case "14":// alipaycn
		$CreditCond = "and a.RecordType = 14 ";
		break;
	case "13":// wechat
		$CreditCond = "and a.RecordType = 13 ";
		break;
	case "12":// VisaMaster
		$CreditCond = "and a.RecordType = 12 ";
		break;
	case "11":// TapAndGo
		$CreditCond = "and a.RecordType = 11 ";
		break;
	case "10":// FPS
		$CreditCond = "and a.RecordType = 10 ";
		break;
	case "9":// Alipay
		$CreditCond = "and a.RecordType = 9 ";
		break;
	case "8":// TNG
		$CreditCond = "and a.RecordType = 8 ";
		break;
	case "6":  // Bank transfer
		$CreditCond = " and a.RecordType=6 ";
		break;
	case "7": // Cheque deposit
		$CreditCond = " and a.RecordType=7 ";
		break;
	case "5": // Auto-pay
		$CreditCond = "and a.RecordType = 4 ";
		break;
	case "4":// Add Value Machine
		$CreditCond = "and a.RecordType = 3 ";
		break;
	case "3":// Manual Cash Deposit
		$CreditCond = "and a.RecordType = 2 ";
		break;
	case "2":// Counter Bill
		$CreditCond = "and a.RecordType = 1 and a.PPSType = 1 ";
		break;
	case "1":// IEPS
		$CreditCond = "and a.RecordType = 1 and a.PPSType = 0 ";
		break;
	default:
		$CreditCond = "";
		break;
}

if($ClassName!="")
{
	$ClassCond = " and b.ClassName='".$li->Get_Safe_Sql_Query(rawurldecode($ClassName))."'";	
}

if($HideCancelled == '1'){
	$join_cancel_trans_table = " LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.StudentID=a.StudentID AND t.TransactionType=9 AND t.RelatedTransactionID=a.TransactionID ";
	if($CancelOption == '1') $join_cancel_trans_table .= " AND (DATE_FORMAT(t.TransactionTime,'%Y-%m-%d %H:%i:%s') BETWEEN '$FromDate $FromTime' AND '$ToDate $ToTime') ";
	$cond_cancel_trans = " AND t.LogID IS NULL ";
}

if($keyword != '')
{
	$keyword = $li->Get_Safe_Sql_Like_Query(rawurldecode($keyword));
}

$sql  = "SELECT ";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$sql .= "IF(b.UserID IS NULL, c.UserLogin, b.UserLogin) as UserLogin, ";
}
     $sql .= " IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),$archive_namefield,IF(b.UserID IS NULL AND c.UserID IS NULL,'',$namefield)) as StudentName,
				IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName, IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as thisClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as thisClassNumber,
               ROUND(a.Amount,2) as Amount,
               CASE 
								WHEN a.RecordType = 1 and PPSType = 0 THEN '".$Lang['ePayment']['IEPS']."' 
								WHEN a.RecordType = 1 and PPSType = 1 THEN '".$Lang['ePayment']['CounterBill']."' 
								WHEN a.RecordType = 2 THEN '$i_Payment_Credit_TypeCashDeposit'
								WHEN a.RecordType = 3 THEN '$i_Payment_Credit_TypeAddvalueMachine' 
								WHEN a.RecordType = 4 THEN '".$Lang['ePayment']['AutoPay']."' 
								WHEN a.RecordType = 6 THEN '".$Lang['ePayment']['BankTransfer']."' 
								WHEN a.RecordType = 7 THEN '".$Lang['ePayment']['ChequeDeposit']."' 
                                WHEN a.RecordType = 8 THEN '".$Lang['ePayment']['TNG']."' 
                                WHEN a.RecordType = 9 THEN '".$Lang['ePayment']['Alipay']."' 
                                WHEN a.RecordType = 10 THEN '".$Lang['ePayment']['FPS']."' 
                                WHEN a.RecordType = 11 THEN '".$Lang['ePayment']['TapAndGo']."' 
                                WHEN a.RecordType = 12 THEN '".$Lang['ePayment']['VisaMaster']."'
                                WHEN a.RecordType = 13 THEN '".$Lang['ePayment']['WeChat']."'
                                WHEN a.RecordType = 14 THEN '".$Lang['ePayment']['AlipayCN']."'
								ELSE '$i_Payment_Credit_TypeUnknown' 
							 END as CreditType,
               a.RefCode, ";
if($sys_custom['ePayment']['CashDepositRemark']){
	$sql .= "a.Remark, ";
}
$sql .= " IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,a.AdminInCharge,$Inchargenamefield)) as DisplayAdminInCharge,
		DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),	
		DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i') "; 
if($sys_custom['ePayment']['CashDepositAccount']){
	$sql .= ",a.Account,a.ReceiptNo ";
}
$sql .= " FROM
             PAYMENT_CREDIT_TRANSACTION as a 
             LEFT OUTER JOIN 
             INTRANET_USER as b 
             ON a.StudentID = b.UserID
             LEFT OUTER JOIN 
             INTRANET_ARCHIVE_USER AS c 
             ON a.StudentID=c.UserID 
             LEFT JOIN 
             INTRANET_USER as d 
             ON (a.AdminInCharge = d.UserID) 
             LEFT JOIN 
             INTRANET_ARCHIVE_USER as e 
             ON (a.AdminInCharge = e.UserID) ";
$sql .= $join_cancel_trans_table;             
$sql .= " WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR 
               d.EnglishName LIKE '%$keyword%' OR 
               d.ChineseName LIKE '%$keyword%' OR 
               e.EnglishName LIKE '%$keyword%' OR 
               e.ChineseName LIKE '%$keyword%' 
              )
              $date_cond
              $user_cond 
              $CreditCond
              $ClassCond
              $cond_cancel_trans";
//$field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","a.Amount","CreditType","a.RefCode","a.AdminInCharge","a.TransactionTime");

$field_array = array("b.EnglishName,c.EnglishName","thisClassName","CAST(thisClassNumber AS UNSIGNED)","a.Amount","CreditType","a.RefCode");
$fieldorder2 = ",CAST(thisClassNumber AS UNSIGNED)";
if($sys_custom['ePayment']['CashDepositRemark']){
	$field_array = array_merge($field_array, array("a.Remark"));
}
$field_array = array_merge($field_array, array("a.AdminInCharge","a.TransactionTime","a.DateInput"));
if($sys_custom['ePayment']['CashDepositAccount']){
	$field_array = array_merge($field_array, array("a.Account","a.ReceiptNo"));
}
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	array_unshift($field_array,"UserLogin");
}
$sql .= " ORDER BY ".$field_array[$field].( ($order==0) ? " DESC " : " ASC ").$fieldorder2;

$result = $li->returnArray($sql);
$result_count = count($result);


# Calculate sum
$sum_sql  = "SELECT 
               SUM(".$lpayment->getExportAmountFormatDB("a.Amount")."), COUNT(a.Amount)
	         FROM 
	             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID ";
	$sum_sql.=$join_cancel_trans_table;
	$sum_sql.=" WHERE
	              (
	               b.EnglishName LIKE '%$keyword%' OR
	               b.ChineseName LIKE '%$keyword%' OR
	               b.ClassName LIKE '%$keyword%' OR
	               b.ClassNumber LIKE '%$keyword%' OR
	               a.RefCode LIKE '%$keyword%' OR
	               a.AdminInCharge LIKE '%$keyword%' OR 
	               c.EnglishName LIKE '%$keyword%' OR
	               c.ChineseName LIKE '%$keyword%' OR
	               c.ClassName LIKE '%$keyword%' OR
	               c.ClassNumber LIKE '%$keyword%' 
	
	              ) 
	              $date_cond 
	              $user_cond  
	              $CreditCond 
	              $cond_cancel_trans";
$temp = $li->returnArray($sum_sql,2);
list($total_amount, $total_count) = $temp[0];
$total_amount = number_format($total_amount+0,2);
$total_count += 0;

$infobar  = "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$infobar .= "<tr><td class=\"eSportprinttext\" colspan=\"2\">".$i_Payment_ItemSummary."</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"eSportprinttext\" width=\"30%\" align=\"left\">$i_Payment_TotalCountTransaction</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"eSportprinttext\" width=\"70%\">$total_count</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"eSportprinttext\" width=\"30%\" align=\"left\">$i_Payment_ItemTotal</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"eSportprinttext\" width=\"70%\" align=\"left\">$ $total_amount</td></tr>\n";
$infobar .= "</table>";

$column_count = 9;
if($sys_custom['ePayment']['CreditTransactionWithLoginID']) $column_count+=1;
if($sys_custom['ePayment']['CashDepositRemark']) $column_count += 1;
if($sys_custom['ePayment']['CashDepositAccount']) $column_count += 2;

$column_width = sprintf("%.2f", 100.0 / $column_count);

$display = "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">";
$display .= "<thead>";
$display .= "<tr>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"1\">#</td>";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">".$Lang['AccountMgmt']['LoginID']."</td>";
}
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_UserStudentName</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_UserClassName</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_UserClassNumber</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Field_CreditAmount</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Credit_Method</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Field_RefCode</td>";
if($sys_custom['ePayment']['CashDepositRemark']){
	$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">".$Lang['General']['Remark']."</td>";
}
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Field_AdminInCharge</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Field_TransactionTime</td>";
$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">$i_Payment_Field_PostTime</td>";
if($sys_custom['ePayment']['CashDepositAccount']){
	$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">".$Lang['ePayment']['Account']."</td>";
	$display .= "<td valign=\"middle\" class=\"eSporttdborder eSportprinttabletitle\" width=\"".$column_width."%\">".$Lang['ePayment']['ReceiptNo']."</td>";
}
$display .= "</tr>";
$display .= "</thead>\n";
$display .= "<tbody>";

if($result_count == 0){
	$colspan = 10;
	if($sys_custom['ePayment']['CashDepositRemark']){
		$colspan += 1;
	}
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$colspan += 1;
	}
	$display.="<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"$colspan\" align=\"center\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
}
for ($i=0; $i<$result_count; $i++)
{
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$login_id = array_shift($result[$i]);
	}
	if($sys_custom['ePayment']['CashDepositRemark']){
		list($studentname,$class,$classnumber,$amount,$method,$refcode,$remark,$adminInCharge,$trans_time, $post_time) = $result[$i];
	}else{
     	list($studentname,$class,$classnumber,$amount,$method,$refcode,$adminInCharge, $trans_time, $post_time) = $result[$i];
	}
	if($sys_custom['ePayment']['CashDepositAccount']){
		$account = $result[$i]['Account'];
		$receipt_no = $result[$i]['ReceiptNo'];
	}
     $css =$i%2==0?"attendancepresent":"attendanceouting";
     $display .= "<tr class=\"$css\">";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
	 	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	 		$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($login_id,1,"&nbsp;")."</td>";
	 	}
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($studentname,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($class,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($classnumber,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">$amount</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($method,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($refcode,1,"&nbsp;")."</td>";
	 	if($sys_custom['ePayment']['CashDepositRemark']){
	 		$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($remark,1,"&nbsp;")."</td>";
	 	}
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($adminInCharge,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($trans_time,1,"&nbsp;")."</td>";
	 	$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($post_time,1,"&nbsp;")."</td>";
	 	if($sys_custom['ePayment']['CashDepositAccount']){
	 		$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($account,1,"&nbsp;")."</td>";
	 		$display .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($receipt_no,1,"&nbsp;")."</td>";
	 	}
	 $display .= "</tr>\n";
}
$display .= "</tbody>";
$display .= "</table>";


include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width="90%" align="center" border="0">
	<tr>
		<td align="center" class="eSportprinttitle"><b><?=$i_Payment_Menu_Browse_CreditTransaction?></b></td>
	</tr>
</table>
<?=$infobar?>
<?=$display?>
<br />
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>