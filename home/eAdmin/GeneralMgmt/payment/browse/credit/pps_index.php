<?php
// Editing by 
/*
 * 2019-11-07 (Carlos): Added [Date Type] to choose from [By Post Time] or [By Transaction Time].
 * 2019-09-24 (Ray): Added hour & minute for From/To time & detail report
 * 2015-02-16 (Carlos): Added [Hide cancelled transactions]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_CreditTransaction";

$linterface = new interface_html();

$now = time();
$today = date('Y-m-d',$now);
$ts_monthstart = mktime(0,0,0,date('m',$now)-1,1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now),1,date('Y',$now));

$start = date('Y-m-d',$ts_monthstart);
$end = date('Y-m-d',$ts_monthend);

$FromTime = $WithTime? sprintf("%02d:%02d:00",$FromTime_hour,$FromTime_min) : "00:00:00";
$ToTime = $WithTime? sprintf("%02d:%02d:59",$ToTime_hour,$ToTime_min) : "23:59:59";

$select_1 = "<input type='checkbox' name=status1 id='status1' value='1' checked><label for='status1'> $i_Payment_PPS_Export_Choices_1</label>";
$select_2 = "<input type='checkbox' name=status2 id='status2' value='1'><label for='status2'> $i_Payment_PPS_Export_Choices_2</label>";
$select_3 = "<input type='checkbox' name=status3 id='status3' value='1'><label for='status3'> $i_Payment_PPS_Export_Choices_3</label>";


$TAGS_OBJ[] = array($i_Payment_Menu_Browse_CreditTransaction, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Payment_Menu_Browse_CreditTransaction,'index.php');

$PAGE_NAVIGATION[] = array($button_export_pps);
?>
<br />
<script language='javascript'>
function checkform(){
	obj = document.form1;
	objStatus1 = obj.status1;
	objStatus2 = obj.status2;
	objStatus3 = obj.status3;
	
	if(objStatus1.checked || objStatus2.checked || objStatus3.checked)
		obj.submit();
	else alert('<?=$i_Payment_PPS_Export_Warning?>');
	
	
}
function withTimeChecked(isChecked)
{
    if(isChecked){
        $('.time').fadeIn('fast');
    }else{
        $('.time').fadeOut('fast');
    }
}
</script>
<form name="form1" method="get" action="pps_export.php" onSubmit='return checkform()'>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['DateType']?></td>
	  	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
	    	<input type='radio' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?> onclick="hideHourMinute()">
			<?=$i_Payment_Search_By_PostTime?>&nbsp;&nbsp;
			<input type='radio' name='search_by' value='1' <?=($search_by==1?"checked":"")?> onclick="showHourMinute()">
			<?=$i_Payment_Search_By_TransactionTime?>
	    </td>
  	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<!--<input type="text" name="start" value="<?=$start?>" maxlength="10" class='textboxnum'>
			<?=$linterface->GET_CALENDAR("form1", "start")?>--><?=$linterface->GET_DATE_PICKER("start",$start)?> <span class="tabletextremark">(yyyy-mm-dd)</span>
            <span class="time"<?=!$WithTime?' style="display:none;"':''?>><?=$linterface->Get_Time_Selection("FromTime", $FromTime, $others_tab='',true)?></span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<!--<input type="text" name="end" value="<?=$end?>" maxlength="10" class='textboxnum'>
			<?=$linterface->GET_CALENDAR("form1", "end")?>--> <?=$linterface->GET_DATE_PICKER("end",$end)?><span class="tabletextremark">(yyyy-mm-dd)</span>
            <span class="time"<?=!$WithTime==1?' style="display:none;"':''?>><?=$linterface->Get_Time_Selection("ToTime", $ToTime, $others_tab='',true)?></span>
        </td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
        <td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$linterface->Get_Checkbox("WithTime", "WithTime", "1", $WithTime==1, "", $Lang['ePayment']['SearchWithTime'], "withTimeChecked(this.checked)", $Disabled='')?></td>
    </tr>
	<tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_Payment_PPS_Export_Select_Student?></td>
	<td class='tabletext'><?=$select_1?><BR><?=$select_2?><BR><?=$select_3?></td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['General']['Others']?></td>
        <td class="tabletext">
            <input type="checkbox" id="HideCancelled" name="HideCancelled" value="1" checked="checked">
            <label for="HideCancelled"><?=$Lang['ePayment']['HideCancelledTransactions']?></label>
        </td>
    </tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['StudentAttendance']['ReportType']?></td>
        <td class="tabletext">
            <input type="radio" id="report_type_0" name="report_type" value="0" checked /><label for="report_type_0"><?=$Lang['StaffAttendance']['SummaryStatistic']?></label><br/>
            <input type="radio" id="report_type_1" name="report_type" value="1" /><label for="report_type_1"><?=$Lang['StaffAttendance']['RecordDetails']?></label><br/>
        </td>
    </tr>
	<tr>
		<td colspan="2" class="tabletextremark">
			<?=$i_Payment_Note_PPSCostExport?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
