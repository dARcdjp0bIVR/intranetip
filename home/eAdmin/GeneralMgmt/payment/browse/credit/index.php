<?php
// Editing by 

###########################################################
#   Date:   2020-08-11  Ray: Added wechat
#   Date:   2020-07-20  Ray: Added visamaster
#	Date:	2019-09-23  Ray: Added hour & minute for From/To time
#	Date:	2019-01-30  Carlos: Cater Alipay and TNG top-up records.
#	Date:	2017-02-02	Carlos
#			Added CancelOption: [Within date range] / [Can exceed date range] for Hide cancelled transactions.
#
#	Date:	2016-08-23	Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] - added credit methods [2=Cash, 6=Bank transfer, 7=Cheque deposit].
#			$sys_custom['ePayment']['CashDepositAccount'] - added [Account] and [ReceiptNo].
#
#	Date:	2015-10-07  Carlos
#			$sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display of [Login ID].
#
#	Date:	2015-08-06 	Carlos
#			Get/Set date range cookies.
#
#	Date:	2015-07-22  Carlos
#			Added credit type Auto-pay.
#
#	Date:	2015-02-25	Carlos
#			Added [Print] button.
#
#	Date:	2015-02-16 	Carlos
#			Added [Hide cancelled transactions]
#
#	Date:	2014-04-02	Carlos
#			Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
#
#	Date:	2013-12-13	YatWoon
#			Add class filter
#
#	Date:	2013-11-21	Carlos
#			Format Amount with $lpayment->getExportAmountFormatDB() before SUM() 
# 
#	Date:	2013-09-26	Henry
#			added button [Export PPS Transaction Cost]
#
#	Date:	2011-09-28	YatWoon
#			update sorting order fields [Case#2011-0928-1002-42073]
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_credit_transaction_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_credit_transaction_page_number!="")
{
	$pageNo = $ck_data_log_browsing_credit_transaction_page_number;
}

if ($ck_data_log_browsing_credit_transaction_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_credit_transaction_page_order!="")
{
	$order = $ck_data_log_browsing_credit_transaction_page_order;
}

if ($ck_data_log_browsing_credit_transaction_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_credit_transaction_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_credit_transaction_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_credit_transaction_page_field!="")
{
	$field = $ck_data_log_browsing_credit_transaction_page_field;
}

$lpayment = new libpayment();
$lclass = new libclass();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_CreditTransaction";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 8;
*/

if(!isset($search_by)){
	$HideCancelled = '1';
}

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("d.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(e.ChineseName IS NULL,e.EnglishName,e.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(e.EnglishName IS NULL,e.ChineseName,e.EnglishName)";
}

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="") {
	$FromDate = $date_range_cookies['StartDate'] != '' ? $date_range_cookies['StartDate'] : date('Y-m-d', getStartOfAcademicYear($today_ts));
}
if($ToDate=="") {
	$ToDate = $date_range_cookies['EndDate'] != '' ? $date_range_cookies['EndDate'] : date('Y-m-d', getEndOfAcademicYear($today_ts));
}

$FromTime = $WithTime? sprintf("%02d:%02d:00",$FromTime_hour,$FromTime_min) : "00:00:00";
$ToTime = $WithTime? sprintf("%02d:%02d:59",$ToTime_hour,$ToTime_min) : "23:59:59";


if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

//$date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate' ";
//$date_cond = " AND DATE_FORMAT(a.DateInput,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d')<='$ToDate' ";
$search_by=$search_by==""?1:$search_by;                
                
if($search_by!=1)                
	$date_cond = " AND DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s') >= '$FromDate $FromTime' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s')<='$ToDate $ToTime' ";
else
  $date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i:%s') >= '$FromDate $FromTime' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i:%s')<='$ToDate $ToTime' ";

switch($user_type){
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;

	default: $user_cond = "";break;
}

switch($CreditType)
{
    case "14":// alipaycn
		$CreditCond = "and a.RecordType = 14 ";
		break;
	case "13":// wechat
		$CreditCond = "and a.RecordType = 13 ";
		break;
	case "12":// VisaMaster
		$CreditCond = "and a.RecordType = 12 ";
		break;
	case "11":// TapAndGo
		$CreditCond = "and a.RecordType = 11 ";
		break;
	case "10":// FPS
		$CreditCond = "and a.RecordType = 10 ";
		break;
	case "9":// Alipay
		$CreditCond = "and a.RecordType = 9 ";
		break;
	case "8":// TNG
		$CreditCond = "and a.RecordType = 8 ";
		break;
	case "6":  // Bank transfer
		$CreditCond = " and a.RecordType=6 ";
		break;
	case "7": // Cheque deposit
		$CreditCond = " and a.RecordType=7 ";
		break;
	case "5": // Auto-pay
		$CreditCond = "and a.RecordType = 4 ";
		break;
	case "4":// Add Value Machine
		$CreditCond = "and a.RecordType = 3 ";
		break;
	case "3":// Manual Cash Deposit
		$CreditCond = "and a.RecordType = 2 ";
		break;
	case "2":// Counter Bill
		$CreditCond = "and a.RecordType = 1 and a.PPSType = 1 ";
		break;
	case "1":// IEPS
		$CreditCond = "and a.RecordType = 1 and a.PPSType = 0 ";
		break;
	default:
		$CreditCond = "";
		break;
}

if($ClassName!="")
{
	$ClassCond = " and b.ClassName='". $ClassName."'";	
}



if($HideCancelled == '1'){
	$join_cancel_trans_table = " LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.StudentID=a.StudentID AND t.TransactionType=9 AND t.RelatedTransactionID=a.TransactionID ";
	if($CancelOption == '1') $join_cancel_trans_table .= " AND (DATE_FORMAT(t.TransactionTime,'%Y-%m-%d %H:%i:%s') BETWEEN '$FromDate $FromTime' AND '$ToDate $ToTime') ";
	$cond_cancel_trans = " AND t.LogID IS NULL ";
}

$sql  = "SELECT ";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$sql .= "IF(b.UserID IS NULL, c.UserLogin, b.UserLogin) as UserLogin, ";
}
    $sql .= "IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
             IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as thisClassName,
             IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as thisClassNumber,
             CONCAT('$', FORMAT(a.Amount, 2)) AS Amount,
             CASE 
							WHEN a.RecordType = 1 and PPSType = 0 THEN '".$Lang['ePayment']['IEPS']."' 
							WHEN a.RecordType = 1 and PPSType = 1 THEN '".$Lang['ePayment']['CounterBill']."' 
							WHEN a.RecordType = 2 THEN '$i_Payment_Credit_TypeCashDeposit'
							WHEN a.RecordType = 3 THEN '$i_Payment_Credit_TypeAddvalueMachine' 
							WHEN a.RecordType = 4 THEN '".$Lang['ePayment']['AutoPay']."' 
							WHEN a.RecordType = 6 THEN '".$Lang['ePayment']['BankTransfer']."' 
							WHEN a.RecordType = 7 THEN '".$Lang['ePayment']['ChequeDeposit']."' 
							WHEN a.RecordType = 8 THEN '".$Lang['ePayment']['TNG']."' 
							WHEN a.RecordType = 9 THEN '".$Lang['ePayment']['Alipay']."' 
							WHEN a.RecordType = 10 THEN '".$Lang['ePayment']['FPS']."' 
				        	WHEN a.RecordType = 11 THEN '".$Lang['ePayment']['TapAndGo']."' 
					        WHEN a.RecordType = 12 THEN '".$Lang['ePayment']['VisaMaster']."'
					        WHEN a.RecordType = 13 THEN '".$Lang['ePayment']['WeChat']."'
					        WHEN a.RecordType = 14 THEN '".$Lang['ePayment']['AlipayCN']."'
							ELSE '$i_Payment_Credit_TypeUnknown' 
						 END as CreditType,
             a.RefCode, ";
if($sys_custom['ePayment']['CashDepositRemark']){
	$sql .= "a.Remark, ";
}
$sql .= " IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,a.AdminInCharge,$Inchargenamefield)) as DisplayAdminInCharge, 
             DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
             DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i') ";
if($sys_custom['ePayment']['CashDepositAccount']){
	$sql .= ",a.Account,a.ReceiptNo ";
}
$sql .= " FROM
             PAYMENT_CREDIT_TRANSACTION as a 
             LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID) 
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID=c.UserID) 
             LEFT JOIN INTRANET_USER as d ON (a.AdminInCharge = d.UserID) 
             LEFT JOIN INTRANET_ARCHIVE_USER as e ON (a.AdminInCharge = e.UserID) ";
$sql .=	$join_cancel_trans_table;
$sql .= " WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR 
               c.ClassNumber LIKE '%$keyword%' OR 
               d.EnglishName LIKE '%$keyword%' OR 
               d.ChineseName LIKE '%$keyword%' OR 
               e.EnglishName LIKE '%$keyword%' OR 
               e.ChineseName LIKE '%$keyword%' 
              ) 
             $date_cond 
             $user_cond 
             $CreditCond 
             $ClassCond
             $cond_cancel_trans";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
// $li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","Amount","CreditType","a.RefCode","DisplayAdminInCharge","a.TransactionTime","a.DateInput");
$li->field_array = array("b.EnglishName,c.EnglishName","thisClassName","CAST(thisClassNumber AS UNSIGNED)","a.Amount","CreditType","a.RefCode");
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->field_array = array_merge($li->field_array, array("a.Remark"));
}
$li->field_array = array_merge($li->field_array, array("DisplayAdminInCharge","a.TransactionTime","a.DateInput"));
if($sys_custom['ePayment']['CashDepositAccount']){
	$li->field_array = array_merge($li->field_array, array("a.Account","a.ReceiptNo"));
}

if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	array_unshift($li->field_array,"UserLogin");
}
$li->fieldorder2 = ", CAST(thisClassNumber AS UNSIGNED)";

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
if ($field=="" && $order=="") {
	$li->field = 8;
	$li->order = 0;
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$li->field += 1;
	}
}

$column_count = 9;
if($sys_custom['ePayment']['CreditTransactionWithLoginID']) $column_count+=1;
if($sys_custom['ePayment']['CashDepositRemark']) $column_count += 1;
if($sys_custom['ePayment']['CashDepositAccount']) $column_count += 2;

$column_width = sprintf("%.2f", 100.0 / $column_count);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletoplink'>#</td>\n";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $Lang['AccountMgmt']['LoginID'])."</td>\n";
}
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Credit_Method)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $Lang['General']['Remark'])."</td>\n";
}
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Field_AdminInCharge)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $i_Payment_Field_PostTime)."</td>\n";
if($sys_custom['ePayment']['CashDepositAccount']){
	$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $Lang['ePayment']['Account'])."</td>\n";
	$li->column_list .= "<td width='".$column_width."%' class='tablebluetop'>".$li->column($pos++, $Lang['ePayment']['ReceiptNo'])."</td>\n";
}

## select user
$array_user_name = array($i_identity_student, $i_identity_teachstaff, $i_Payment_All);
$array_user_data = array("2", "1", "");
$select_user = getSelectByValueDiffName($array_user_data,$array_user_name,"name='user_type' onChange='submitForm(this.form);'",$user_type,0,1);

## select Credit Type
$CreditTypeArrayKey = array($i_Payment_All,
							$i_Payment_Credit_TypeAddvalueMachine,
							$i_Payment_Credit_TypeCashDeposit, 
							$Lang['ePayment']['CounterBill'], 
							$Lang['ePayment']['IEPS']
					 );

$CreditTypeArrayData = array("","4","3","2","1");
if($sys_custom['ePayment']['CreditMethodAutoPay']){
	$CreditTypeArrayKey[] = $Lang['ePayment']['AutoPay'];
	$CreditTypeArrayData[] = "5";
}else if($sys_custom['ePayment']['CashDepositMethod']){
	$CreditTypeArrayKey[] = $Lang['ePayment']['BankTransfer'];
	$CreditTypeArrayData[] = "6";
	$CreditTypeArrayKey[] = $Lang['ePayment']['ChequeDeposit'];
	$CreditTypeArrayData[] = "7";
}

if($sys_custom['ePayment']['TopUpEWallet']['TNG']){
	$CreditTypeArrayKey[] = $Lang['ePayment']['TNG'];
	$CreditTypeArrayData[] = "8";
}
if($sys_custom['ePayment']['TopUpEWallet']['Alipay']){
	$CreditTypeArrayKey[] = $Lang['ePayment']['Alipay'];
	$CreditTypeArrayData[] = "9";
}

$SelectCreditType = getSelectByValueDiffName($CreditTypeArrayData,$CreditTypeArrayKey,"name='CreditType' onChange='submitForm(this.form);'",$CreditType,0,1);

$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("pps_index.php",$button_export_pps,"","","",0);
/*$toolbar .= "<a class=\"contenttool\" href=\"javascript:checkGet(document.form1,'pps_index.php')\">";
$toolbar .= "<img src=\"".$PATH_WRT_ROOT."/images/2007a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
$toolbar .= $button_export_pps."</a>";*/

$filterbar = $select_user;

### Class filter
$temp = $button_select;
$button_select = $i_general_all_classes;
$select_class = $lclass->getSelectClass("name='ClassName' onChange='submitForm(this.form);'",$ClassName);
$button_select = $temp;
$filterbar .= $select_class;

$filterbar .= $SelectCreditType;

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_CreditTransaction, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# Calculate sum
$sum_sql  = "SELECT
               SUM(".$lpayment->getExportAmountFormatDB("a.Amount")."), COUNT(a.Amount)
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID ";
$sum_sql.=$join_cancel_trans_table;
$sum_sql.=" WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR 
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 

              ) 
              $date_cond 
              $user_cond  
              $CreditCond 
              $cond_cancel_trans";
$ldb = new libdb();
$temp = $ldb->returnArray($sum_sql,2);
list($total_amount, $total_count) = $temp[0];
$total_amount = number_format($total_amount+0,2);
$total_count += 0;

$infobar = "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_Payment_ItemSummary)."</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_TotalCountTransaction</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$total_count</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemTotal</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$ $total_amount</td></tr>\n";

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="javascript">
function checkForm(formObj){
	if(formObj==null)return false;
		fromV = formObj.FromDate;
		toV= formObj.ToDate;
		if(!checkDate(fromV)){
				//formObj.FromDate.focus();
				return false;
		}
		else if(!checkDate(toV)){
					//formObj.ToDate.focus();
					return false;
		}
			return true;
}
function checkDate(obj){
		if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
		return true;
}
function submitForm(obj){
	if(checkForm(obj))
		obj.submit();	
}
function exportPage(obj,url){
		old_url = obj.action;
		old_method= obj.method;
        obj.action=url;
        obj.method = "post";
        obj.submit();
        obj.action = old_url;
        obj.method = old_method;
        
}
	
function openPrintPage()
{
	newWindow("print.php?search_by=<?=$search_by?>&FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>&WithTime=<?=$WithTime?>&FromTime=<?=$FromTime?>&ToTime=<?=$ToTime?>&HideCancelled=<?=$HideCancelled?>&CancelOption=<?=$CancelOption?>&order=<?=$li->order?>&field=<?=$li->field?>&user_type=<?=$user_type?>&ClassName=<?=rawurlencode($ClassName)?>&CreditType=<?=$CreditType?>&keyword=<?=rawurlencode($keyword)?>",10);
}


function withTimeChecked(isChecked)
{
    if(isChecked){
        $('.time').fadeIn('fast');
    }else{
        $('.time').fadeOut('fast');
    }
}
</script>
<br />
<form name="form1" method="get" action="index.php">
<!-- Date Range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td colspan="2" class="tabletext"><?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?>:</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['DateType']?></td>
  	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
    	<input type='radio' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?> onclick="hideHourMinute()">
			<?=$i_Payment_Search_By_PostTime?>&nbsp;&nbsp;
			<input type='radio' name='search_by' value='1' <?=($search_by==1?"checked":"")?> onclick="showHourMinute()">
			<?=$i_Payment_Search_By_TransactionTime?>
    </td>
  </tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("FromDate", $FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
            <span class="time"<?=!$WithTime?' style="display:none;"':''?>><?=$linterface->Get_Time_Selection("FromTime", $FromTime, $others_tab='',true)?></span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
            <span class="time"<?=!$WithTime==1?' style="display:none;"':''?>><?=$linterface->Get_Time_Selection("ToTime", $ToTime, $others_tab='',true)?></span>
		</td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
        <td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$linterface->Get_Checkbox("WithTime", "WithTime", "1", $WithTime==1, "", $Lang['ePayment']['SearchWithTime'], "withTimeChecked(this.checked)", $Disabled='')?></td>
    </tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=""?></td>
  		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
    		<input type="checkbox" id="HideCancelled" name="HideCancelled" value="1" <?=($HideCancelled=='1'?"checked":"")?> onclick="document.getElementById('CancelOptionDiv').style.display=this.checked?'block':'none';" />
			<label for="HideCancelled"><?=$Lang['ePayment']['HideCancelledTransactions']?></label>
			<div id="CancelOptionDiv" <?=$HideCancelled=='1'?'':' style="display:none;"'?>>
				<?=$linterface->Get_Radio_Button("WithinDateRange", "CancelOption", "1", $CancelOption == 1, $___Class="",  $Lang['ePayment']['WithinDateRange'])?>
				<?=$linterface->Get_Radio_Button("CanExceedDateRange", "CancelOption", "2", $CancelOption=="" || $CancelOption == 2, $___Class="",  $Lang['ePayment']['CanExceedDateRange'])?>
			</div>
    	</td>
  	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
<br />
<!-- Summary -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar?>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?=$SysMsg?></td>
	</tr>
</table>
<!-- Data Table -->
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr>
		<td align="left"><?=$filterbar?><?=$searchbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<?php echo $li->displayFormat2("96%","blue"); ?>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			<?=$i_Payment_Note_StudentRemoved?>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
