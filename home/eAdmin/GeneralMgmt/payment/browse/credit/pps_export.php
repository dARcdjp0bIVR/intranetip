<?
// Editing by 
/*
 * 2020-08-11 (Ray): Added wechat
 * 2020-07-20 (Ray): Added visamaster
 * 2019-11-07 (Carlos): Added [Date Type] to choose from [By Post Time] or [By Transaction Time].
 * 2019-09-24 (Ray): Added hour & minute for From/To time & detail report
 * 2015-10-22 (Carlos): Added column Name to the csv.
 * 2015-02-16 (Carlos): Added [Hide cancelled transactions]
 * 2013-09-24 (Carlos): Modified query to sum up PPS charges
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();
$li = new libdb();
$order = ($order == 1) ? 1 : 0;
if ($field == "" || $field >2) $field = 0;
$field += 0;

$status_cond = "";

if($status1==1 || $status2==1){
	$tmp = array();
  if($status1==1){
	$tmp[] = 0;
	$tmp[] = 1;
	$tmp[] = 2;
  }
  if($status2==1){
	  $tmp[] = 3;
  }
  $status_cond = " AND b.RecordStatus IN (".implode(",",$tmp).") ";
}


$namefield = getNameFieldByLang2("b.");
$namefield_archived = getNameFieldByLang2("c.");

if( ($status1==1 || $status2==1 ) && $status3==1){ 
	# ALL
	$class_name_field ="IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName)";
	$class_num_field = "IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber)";
	$user_id_cond=" AND (b.UserID IS NOT NULL OR c.UserID IS NOT NULL)";
	$name_field = "IF(b.UserID IS NULL AND c.UserID IS NOT NULL,$namefield_archived,$namefield)";
	
}else if($status1==1 || $status2==1){ 
	# Students with status approve,pending,suspended, left
	$class_name_field = "b.ClassName";
	$class_num_field ="b.ClassNumber";
	$user_id_cond = " AND b.UserID IS NOT NULL ";
	$name_field = $namefield;
}else{ 
	#  removed student
	$class_name_field = "CONCAT('*',c.ClassName)";
	$class_num_field = "c.ClassNumber";
	$user_id_cond = " AND c.UserID IS NOT NULL ";
	$name_field = $namefield_archived;
}
/*
$sql  = "SELECT
               $class_name_field AS ClassName,
               $class_num_field AS ClassNumber,
               ROUND(SUM(IF(a.PPSType=0,".LIBPAYMENT_PPS_CHARGE.",".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL.")),2) AS TotalAmount
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID $status_cond )
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID
         WHERE
                         a.RecordType = 1  
                         $user_id_cond
                         AND a.TransactionTime >= '$start' 
                         AND a.TransactionTime <= '$end 23:59:59'
         GROUP BY a.StudentID ORDER BY $class_name_field, $class_num_field
";
*/

$transaction_time_field = $search_by == '1'? "t.TransactionTime" /* real credit transaction time */ : "a.TransactionTime" /* record input time */;

$FromTime = $WithTime? sprintf("%02d:%02d:00",$FromTime_hour,$FromTime_min) : "00:00:00";
$ToTime = $WithTime? sprintf("%02d:%02d:59",$ToTime_hour,$ToTime_min) : "23:59:59";

if($HideCancelled == '1'){
	$join_cancel_trans_table = " LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t2 ON t2.StudentID=t.StudentID AND t2.TransactionType=9 AND t2.RelatedTransactionID=t.TransactionID ";
	$cond_cancel_trans = " AND t2.LogID IS NULL ";
}

if($report_type == 0) {
	$sql = "SELECT
               $class_name_field AS ClassName,
               $class_num_field AS ClassNumber,
				$name_field as UserName,
               SUM(" . $lpayment->getExportAmountFormatDB("a.Amount") . ") AS TotalAmount
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a 
			 LEFT JOIN INTRANET_USER as b ON (a.StudentID = b.UserID $status_cond ) 
			 INNER JOIN PAYMENT_CREDIT_TRANSACTION as t On t.TransactionID=a.RelatedTransactionID AND t.StudentID=a.StudentID 
             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID ";
	$sql .= $join_cancel_trans_table;
	$sql .= " WHERE
                         a.TransactionType = 8  
                         $user_id_cond
                         AND $transaction_time_field >= '$start $FromTime' 
                         AND $transaction_time_field <= '$end $ToTime' $cond_cancel_trans
         GROUP BY a.StudentID ORDER BY $class_name_field, $class_num_field 
";
	$header = array("Class Name", "Class Number", "Name", "Amount");
	$result = $li->returnArray($sql,4);
} else {
	$sql  = "SELECT
               $class_name_field AS ClassName,
               $class_num_field AS ClassNumber,
				$name_field as UserName,
               ".$lpayment->getExportAmountFormatDB("a.Amount")." AS TotalAmount,
			   CASE 
					WHEN t.RecordType = 1 and PPSType = 0 THEN '".$Lang['ePayment']['PPSIEPS']."' 
					WHEN t.RecordType = 1 and PPSType = 1 THEN '".$Lang['ePayment']['PPSCounterBill']."' 
					WHEN t.RecordType = 2 THEN '$i_Payment_Credit_TypeCashDeposit'
					WHEN t.RecordType = 3 THEN '$i_Payment_Credit_TypeAddvalueMachine' 
					WHEN t.RecordType = 4 THEN '".$Lang['ePayment']['AutoPay']."' 
					WHEN t.RecordType = 6 THEN '".$Lang['ePayment']['BankTransfer']."' 
					WHEN t.RecordType = 7 THEN '".$Lang['ePayment']['ChequeDeposit']."' 
					WHEN t.RecordType = 8 THEN '".$Lang['ePayment']['TNG']."' 
					WHEN t.RecordType = 9 THEN '".$Lang['ePayment']['Alipay']."' 
					WHEN t.RecordType = 10 THEN '".$Lang['ePayment']['FPS']."' 
				    WHEN t.RecordType = 11 THEN '".$Lang['ePayment']['TapAndGo']."' 
				  	WHEN t.RecordType = 12 THEN '".$Lang['ePayment']['VisaMaster']."'
				  	WHEN t.RecordType = 13 THEN '".$Lang['ePayment']['WeChat']."'
				  	WHEN t.RecordType = 14 THEN '".$Lang['ePayment']['AlipayCN']."'
					ELSE '$i_Payment_Credit_TypeUnknown' 
			 	END as CreditType,
			 	t.PPSData
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a 
			 LEFT JOIN INTRANET_USER as b ON (a.StudentID = b.UserID $status_cond ) 
			 INNER JOIN PAYMENT_CREDIT_TRANSACTION as t On t.TransactionID=a.RelatedTransactionID AND t.StudentID=a.StudentID 
             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID ";
	$sql .= $join_cancel_trans_table;
	$sql .= " WHERE
                         a.TransactionType = 8  
                         $user_id_cond
                         AND $transaction_time_field >= '$start $FromTime' 
                         AND $transaction_time_field <= '$end $ToTime' $cond_cancel_trans
          ORDER BY $class_name_field, $class_num_field 
";
	$header = array("Class Name", "Class Number", "Name", "Amount","Credit Type","PPS Data");
	$result = $li->returnArray($sql,6);
}


/*
$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName,b.ClassName),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber),
               COUNT(*) AS RecordCount
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID= c.UserID
         WHERE
                         a.RecordType = 1 AND
                         a.TransactionTime >= '$start' AND
                         a.TransactionTime <= '$end 23:59:59' AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 

              )
         GROUP BY a.StudentID
";
*/

/*
$x = "Class Name,Class Number,Amount\n";
$utf_x = "Class Name \tClass Number \tAmount\r\n";*/


/*for ($i=0; $i<sizeof($result); $i++)
{
     list($class,$classnumber,$amount) = $result[$i];
  
     $x .= "\"$class\",\"$classnumber\",\"".$amount."\"\n";
     $utf_x .= "$class \t$classnumber \t".$amount."\r\n";
}
*/
// Output the file to user browser
$filename = "transaction_cost.csv";

$content = $lexport->GET_EXPORT_TXT($result,$header);
$lexport->EXPORT_FILE($filename, $content);
/*
if ($g_encoding_unicode) {
	$lexport->EXPORT_FILE($filename, $utf_x);
} else {
	header("Content-type: application/octet-stream");
	header("Content-Length: ".strlen($x) );
	header("Content-Disposition: attachment; filename=\"".$filename."\"");
	
	echo $x;
}
*/

intranet_closedb();
?>
