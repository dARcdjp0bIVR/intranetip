<?php
// Editing by 
/*
 * 2015-02-26 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$i_title = "$i_Payment_Title_Overall_SinglePurchasing";

$lpayment = new libpayment();
$lexport = new libexporttext();
$linterface = new interface_html();

if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$info = $lpayment->returnSinglePurchaseStatsByUserType($start,$end,$user_type);
$record_count = count($info);

$header = array($i_Payment_Field_SinglePurchase_Unit,$i_Payment_Field_SinglePurchase_TotalAmount,$i_Payment_Field_SinglePurchase_Count);
$rows = array();

for ($i=0; $i<$record_count; $i++)
{
     list($item, $sum, $count) = $info[$i];
     $sum = "$ ".number_format($sum,2);
     
     $item = str_replace(array('<br>','<br />','<BR>','<BR />'),' ',$item);
	 $row = array($item,$sum,$count);
	 $rows[] = $row;
}

$title = $i_Payment_Menu_Browse_PurchasingRecords."(".$start."_to_".$end.")";

$filename = $title.".csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>
