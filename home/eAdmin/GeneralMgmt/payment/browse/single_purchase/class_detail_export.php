<?php
// editing by 
/*
 * 2017-03-22 (Carlos): Deduct voided transactions. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

if($Item=="" || $FromDate=="" || $ToDate=="") header("Location: index.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libpayment();
$fcm = new form_class_manage();
$lexport = new libexporttext();

$title = $i_Payment_Menu_Browse_PurchasingRecords." - ".$Item." (".$FromDate." ".$Lang['General']['To']." ".$ToDate.")";

$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$start_ts = strtotime($FromDate." 00:00:00");
$end_ts = strtotime($ToDate." 23:59:59");

$ExportColumn = array($title,' ');
$Rows = array();
$Detail = array();

if($ClassID != ''){
	$students = $fcm->Get_Student_By_Class($ClassID);
	$libyear = new Year($YearID);
	$libclass = new year_class($ClassID);
	$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
	$class_name = Get_Lang_Selection($libclass->ClassTitleB5,$libclass->ClassTitleEN);
	
	$Detail = array($Lang['ePayment']['YearForm'].": ".$year_name,'');
	$Rows[] = $Detail;
	
	$Detail = array($Lang['StudentAttendance']['ClassName'].": ".$class_name,'');
	$Rows[] = $Detail;
	
	$Detail = array($Lang['General']['Name'],$i_Payment_Field_SinglePurchase_TotalAmount);
	$Rows[] = $Detail;
	
	$student_userid = Get_Array_By_Key($students,'UserID');
	$user_cond = " AND a.StudentID IN (".(count($student_userid)?implode(",",$student_userid):"-1").") ";
	
	$name_field = getNameFieldWithClassNumberByLang("b.");
	$name_field2 = Get_Lang_Selection("c.ChineseName","c.EnglishName");
	$name_field3 = "CONCAT(".$name_field2.",IF(c.ClassName IS NOT NULL,CONCAT(' (',c.ClassName,'-',c.ClassNumber,')'),''))";
	
	$sql  = "SELECT 
				 a.StudentID,
				 IF(b.UserID IS NOT NULL,$name_field,IF(c.UserID IS NOT NULL,$name_field3,'')) as StudentName,
	             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) as TotalAmount   
	         FROM 
	             PAYMENT_OVERALL_TRANSACTION_LOG as a 
				 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
				 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
	         WHERE 
	              a.TransactionType = 3 AND a.Details = '$Item'
	              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
	              $user_cond 
			 GROUP BY a.StudentID 
			 ORDER BY IF(b.UserID IS NOT NULL,b.ClassNumber+0,c.ClassNumber+0) ";
	//debug_r($sql);          
	$temp = $li->returnArray($sql);
	
	$sum_amount = 0;
	for($j=0;$j<count($temp);$j++){
		list($student_id,$student_name,$amount) = $temp[$j];
		
		$sum_amount += $amount + 0;
		
		
		// Student Name
		$x.="<tr class=\"".$css."\">";
		$x.="<td class=\"".$css."\">".$student_name."</td>";
		// Amount 
		$x.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$amount),2)."</td>";
		$x.="</tr>";
		
		$Detail = array($student_name,'$'.number_format(sprintf("%.2f",$amount),2));
		$Rows[] = $Detail;
	}
	
	$Detail = array($i_Payment_SchoolAccount_Total,'$'.number_format(sprintf("%.2f",$sum_amount),2));
	$Rows[] = $Detail;
}else if($YearID != ''){
	$libyear = new Year($YearID);
		
	$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
	
	$Detail = array($Lang['ePayment']['YearForm'].": ".$year_name,'');
	$Rows[] = $Detail;
	
	$Detail = array($Lang['StudentAttendance']['ClassName'],$i_Payment_Field_SinglePurchase_TotalAmount);
	$Rows[] = $Detail;
	
	$sum_amount = 0;
	
	// Get classes by YearID		
	$class_list = $fcm->Get_Class_List_By_YearID($YearID);
	for($j=0;$j<count($class_list);$j++){
		list($year_class_id, $class_title_en, $class_title_ch) = $class_list[$j];
		$class_name = trim(Get_Lang_Selection($class_title_ch,$class_title_en));
		$students = $fcm->Get_Student_By_Class($year_class_id);
		$student_userid = Get_Array_By_Key($students,'UserID');
		$user_cond = " AND a.StudentID IN (".(count($student_userid)>0?implode(",",$student_userid):"-1").")";
		
		$sql  = "SELECT
		             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) 
		         FROM 
		             PAYMENT_OVERALL_TRANSACTION_LOG as a 
					 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
					 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
		             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
		         WHERE 
		              a.TransactionType = 3 AND a.Details = '$Item'
		              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
		              $user_cond ";
		          
		$temp = $li->returnVector($sql);
		
		$amount = $temp[0];
		
		$sum_amount += $amount + 0;
		
		$Detail = array($class_name,'$'.number_format(sprintf("%.2f",$amount),2));
		$Rows[] = $Detail;
	}
	
	$Detail = array($i_Payment_SchoolAccount_Total,'$'.number_format(sprintf("%.2f",$sum_amount),2));
	$Rows[] = $Detail;
}else{
	$year_form_list = $fcm->Get_Form_List(false,1,$CurrentAcademicYearID);
	$numOfYearForm = count($year_form_list);
	
	$Detail = array($Lang['ePayment']['YearForm'],$i_Payment_Field_SinglePurchase_TotalAmount);
	$Rows[] = $Detail;
	
	$sum_amount = 0;
	for($j=0;$j<$numOfYearForm;$j++)
	{
		$year_form_id = $year_form_list[$j]['YearID'];
		
		$libyear = new Year($year_form_id);
		$students = $fcm->Get_Student_By_Form($CurrentAcademicYearID,array($year_form_id));

		$id_list = '';
		$delimiter = '';
		for($i=0;$i<count($students);$i++){
			$id_list .= $delimiter.$students[$i]['UserID'];
			$delimiter = ',';
		}
		
		$display_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
		
		$user_cond = " AND a.StudentID IN (".$id_list.") ";
		
		$sql  = "SELECT
		             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) 
		         FROM 
		             PAYMENT_OVERALL_TRANSACTION_LOG as a 
					 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
					 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
		             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
		         WHERE 
		              a.TransactionType = 3 AND a.Details = '$Item'
		              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
		              $user_cond ";
		          
		$temp = $li->returnVector($sql);
		
		$amount = $temp[0];
		
		$sum_amount += $amount + 0;
		
		$Detail = array($display_name,'$'.number_format(sprintf("%.2f",$amount),2));
		$Rows[] = $Detail;
	}
	
	$Detail = array($i_Payment_SchoolAccount_Total,'$'.number_format(sprintf("%.2f",$sum_amount),2));
	$Rows[] = $Detail;
}

$Detail = array();
$Detail[] = ' ';
$Detail[] = $i_general_report_creation_time." : ".date('Y-m-d H:i:s');
$Rows[] = $Detail;

$filename = $title.".csv";
$export_content = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);	

intranet_closedb();
?>
