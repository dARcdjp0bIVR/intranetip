<?php
// editing by 
/* 
 * 2015-08-06 (Carlos): Get/Set date range cookies.
 * 2015-02-26 (Carlos): Added [Export] button.
 * 2012-08-03 (Carlos): Added [Class Stats] icon button for user type all or student to pop up window showing Form > Class > Student total amount
 * 						Change item link to icon button [View detail records]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_SingleRecord";

$linterface = new interface_html();

$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if ($start == "") # Default current month
{
    $start = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
}
if($end == "")
{
	$end = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

if($_REQUEST['start'] != '' && $_REQUEST['end']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['start'], $_REQUEST['end']);
}

$info = $lpayment->returnSinglePurchaseStatsByUserType($start,$end,$user_type);

$x = "";
$x = "<tr class=\"tablebluetop\">
		<td class=\"tabletoplink\">$i_Payment_Field_SinglePurchase_Unit</td>
		<td class=\"tabletoplink\">$i_Payment_Field_SinglePurchase_TotalAmount</td>
		<td class=\"tabletoplink\">$i_Payment_Field_SinglePurchase_Count</td>";
	$x.= "<td class=\"tabletoplink\">&nbsp;</td>";
$x .= "</tr>\n";


for ($i=0; $i<sizeof($info); $i++)
{
     list($item, $sum, $count) = $info[$i];
     $sum = "$ ".number_format($sum,2);
     $css = $i%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";

     $x .= "<tr class=\"$css\">\n";
     //$x .= "<td class=\"tabletext\"><a class=\"tablelink\" href=\"detail.php?user_type=$user_type&item=".urlencode($item)."&start=".urlencode($start)."&end=".urlencode($end)."\">$item</a></td>\n";
     $x .= "<td class=\"tabletext\">$item</td>\n";
     $x .= "<td class=\"tabletext\">$sum</td>\n";
     $x .= "<td class=\"tabletext\">$count</td>\n";
     $x .= "<td class=\"tabletext\">";
     if($user_type=='' || $user_type==2){
		$x.= "<a href=\"javascript:void(0);\" onclick=\"newWindow('class_detail.php?Item=".urlencode($item)."&FromDate=".urlencode($start)."&ToDate=".urlencode($end)."',10);\" class=\"button_link\" title=\"$i_Payment_PresetPaymentItem_ClassStat\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_classview.gif\" border=0></a>&nbsp;";
     }
	 	$x.= "<a href=\"detail.php?user_type=$user_type&item=".urlencode($item)."&start=".urlencode($start)."&end=".urlencode($end)."\" class=\"button_link\" title=\"".$i_general_ViewDetailRecords."\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_studentview.gif\" border=\"0\"></a>";
	 $x .= "</td>\n";
     $x .= "</tr>\n";
}
if(sizeof($info)<=0){
	$x .="<tr class=\"tablebluerow2 tabletext\"><td colspan=\"3\" class=\"tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

## select user
$array_user_name = array($i_identity_student, $i_identity_teachstaff, $i_Payment_All);
$array_user_data = array("2", "1", "");
$select_user = getSelectByValueDiffName($array_user_data,$array_user_name,"name='user_type'",$user_type,0,1);

$toolbar = "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Profile_From</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletextremark\" width=\"70%\"><input class=\"textboxnum\" type=\"text\" name=\"start\" id=\"start\" value=\"$start\">
					 (yyyy-mm-dd)</td>
				</tr>
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Profile_To</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletextremark\" width=\"70%\"><input class=\"textboxnum\" type=\"text\" name=\"end\" id=\"end\" value=\"$end\">
					 (yyyy-mm-dd)</td>
				</tr>
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_UserType</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$select_user</td>
				</tr>
				<tr>
					<td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
				</tr>
				<tr>
					<td colspan=\"2\" align=\"center\">
						".$linterface->GET_ACTION_BTN($button_view, "button", "document.form1.submit()")."
					</td>
				</tr>
			</table>";

$toolbar2 = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2.= $linterface->GET_LNK_EXPORT("javascript:ExportPage(document.form1);","","","","",0);

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_PurchasingRecords, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="Javascript">
function openPrintPage()
{
	newWindow('index_print.php?user_type=<?=$user_type?>&start=<?=$start?>&end=<?=$end?>',10);
}

function ExportPage(formObj)
{
	var oldTarget = formObj.target;
	var oldAction = formObj.action;
	formObj.action = 'index_export.php';
	formObj.target = '_blank';
	formObj.submit();
	formObj.action = oldAction;
	formObj.target = oldTarget;
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#start').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#end').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" action="" method="get">
<?=$toolbar?>
<br />
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar2?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    <?=$x?>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
