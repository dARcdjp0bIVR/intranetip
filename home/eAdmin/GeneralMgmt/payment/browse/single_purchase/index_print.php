<?php
// Editing by 
/*
 * 2015-02-26 (Carlos): $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$i_title = "$i_Payment_Title_Overall_SinglePurchasing";

$lpayment = new libpayment();

$linterface = new interface_html();

if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$info = $lpayment->returnSinglePurchaseStatsByUserType($start,$end,$user_type);
$x = "";
for ($i=0; $i<sizeof($info); $i++)
{
     list($item, $sum, $count) = $info[$i];
     $sum = "$ ".number_format($sum,2);
     $css = ($type==1?"attendancepresent":"attendanceouting");

     $x .= "<tr class=\"$css\">\n";
     $x .= "<td class=\"eSporttdborder eSportprinttext\">".Get_String_Display($item,1,'&nbsp;')."</td>\n";
     $x .= "<td class=\"eSporttdborder eSportprinttext\">$sum</td>\n";
     $x .= "<td class=\"eSporttdborder eSportprinttext\">$count</td>\n";
     //$x .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>\n";
     $x .= "</tr>\n";
}

$display = "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$display .= "<thead>";
$display .= "<tr>";
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_SinglePurchase_Unit</td>";
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_SinglePurchase_TotalAmount</td>";
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_SinglePurchase_Count</td>";
//$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle' width=\"10%\">&nbsp;</td>";
$display .= "</tr>\n";
$display .= "</thead>";
$display .= "<tbody>";
$display .= $x;
$display .= "</tbody>";
$display .= "</table>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width=90% border=0 cellspacing=0 cellpadding=5 align=center><tr><Td class='eSportprinttitle tabletext'><b><?=$i_Payment_Menu_Browse_PurchasingRecords?></b></td></tr></table>

<table width='90%' align='center' border=0>
	<tr>
		<td align='left' class='eSportprinttitle'><?=$i_Profile_From?>:</td>
		<td align='left' class='eSportprinttitle' width='70%'><?=$start?></td>
	</tr>
	<tr>
		<td align='left' class='eSportprinttitle'><?=$i_Profile_To?>:</td>
		<td align='left' class='eSportprinttitle'><?=$end?></td>
	</tr>
</table>
<br />
<?=$display?>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
