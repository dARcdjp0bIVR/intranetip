<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$li = new libdb();

$list = "'".implode("','",$RecordID)."'";
$sql = "DELETE FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID IN ($list)";
$Result = $li->db_db_query($sql);

if ($Result) {
	$Msg = $Lang['ePayment']['UnknownRecordSuccess'];
}
else {
	$Msg = $Lang['ePayment']['UnknownRecordFail'];
}
header ("Location: index.php?Msg=".urlencode($Msg));
?>
