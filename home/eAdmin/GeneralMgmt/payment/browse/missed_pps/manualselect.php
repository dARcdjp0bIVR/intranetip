<?php
// Editing by 
/*
 * 2015-10-29 (Carlos): Added first level as select user type to allow input staff user.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_UnknownRecord";
$linterface = new interface_html();

if(!isset($TargetUserType) || $TargetUserType==''){
	$TargetUserType = USERTYPE_STUDENT;
}

$identities = array(array(USERTYPE_STUDENT,$Lang['Identity']['Student']),array(USERTYPE_STAFF,$Lang['Identity']['Staff']));
$select_identity = $linterface->GET_SELECTION_BOX($identities, ' name="TargetUserType" id="TargetUserType" onchange="document.form1.submit();" ', '', $TargetUserType, true);

$lclass = new libclass();
//$select_class = $lclass->getSelectClass("name=\"ClassName\" onChange=\"this.form.submit()\"",$ClassName);

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_MissedPPSBill, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Payment_action_handle);
?>
<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$Lang['General']['UserType']?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_identity?>
	    			</td>
    			</tr>
<?php if($TargetUserType == USERTYPE_STUDENT){ 
				$select_class = $lclass->getSelectClass("name=\"ClassName\" onChange=\"this.form.submit()\"",$ClassName);
?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserClassName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_class?>
	    			</td>
    			</tr>
<?php } ?>
	<?
	if ($ClassName != "" || $TargetUserType == USERTYPE_STAFF) {
		if($TargetUserType == USERTYPE_STAFF){
			$name_field = getNameFieldByLang2();
			$sql = "SELECT UserID,$name_field as UserName FROM INTRANET_USER WHERE RecordType='".USERTYPE_STAFF."' AND RecordStatus IN (0,1,2) ORDER BY UserName";
			$staff_list = $lpayment->returnArray($sql);
			$select_user = $linterface->GET_SELECTION_BOX($staff_list, ' name="TargetStudentID" id="TargetStudentID" ', '', '', true);
		}else if($ClassName != ""){
			$select_user = $lclass->getStudentSelectByClass($ClassName,"name=TargetStudentID");
		}
	?>
				<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_user?>
	    			</td>
    			</tr>
		    	<tr>
                	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
				    <td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "this.form.action='edit_update.php'") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
	<? } else { ?>
				<tr>
                	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				 <tr>
				    <td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
	<? } ?>
    		</table>
    	</td>
	</tr>
</table>
<input type="hidden" name="RecordID" value="<?=$RecordID?>">
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.ClassName");
intranet_closedb();
?>
