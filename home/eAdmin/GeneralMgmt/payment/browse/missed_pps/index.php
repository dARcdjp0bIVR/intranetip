<?php
// Editing by 
###################################################
#	2015-10-29 Carlos: Include staff user.
#	Date:	2013-03-15	YatWoon
#			remove "remove button"
#			set default filter to "Show not handled"
#
###################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_unknown_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_unknown_record_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_unknown_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_unknown_record_page_number!="")
{
	$pageNo = $ck_data_log_browsing_unknown_record_page_number;
}

if ($ck_data_log_browsing_unknown_record_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_unknown_record_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_unknown_record_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_unknown_record_page_order!="")
{
	$order = $ck_data_log_browsing_unknown_record_page_order;
}

if ($ck_data_log_browsing_unknown_record_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_unknown_record_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_unknown_record_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_unknown_record_page_field!="")
{
	$field = $ck_data_log_browsing_unknown_record_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_UnknownRecord";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$namefield = getNameFieldWithClassNumberByLang("b.");
$ArchiveNamefield = ($intranet_session_language == 'en')? 'c.EnglishName':'c.ChineseName';
$status = strlen($status)==0 ? '1' : $status;

if ($status == 1)
{
    $conds = " AND a.MappedStudentID IS NULL";
}
else if ($status == 2)
{
     $conds = " AND a.MappedStudentID IS NOT NULL";
}

$sql  = "SELECT
               a.InputTime, 
               ".$lpayment->getWebDisplayAmountFormatDB("a.Amount").", 
               CASE 
                WHEN a.PPSType=1 THEN 'Counter bill'
                ELSE 'IEPS'
               END as type,
               a.PPSAccount,
               CASE 
               	WHEN b.UserID IS NULL AND c.UserID IS NULL AND a.MappedStudentID IS NOT NULL then '<font color=red>*</font>' 
               	WHEN b.UserID IS NULL then CONCAT('<font color=red>*</font> ',$ArchiveNamefield)
               	WHEN c.UserID IS NULL then $namefield 
               	ELSE ''
               END as Name,
               CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
         FROM
             PAYMENT_MISSED_PPS_BILL as a 
             LEFT OUTER JOIN
             INTRANET_USER as b ON a.MappedStudentID = b.UserID
             LEFT OUTER JOIN 
             INTRANET_ARCHIVE_USER as c ON a.MappedStudentID = c.UserID
         WHERE
              (
               a.PPSAccount LIKE '%$keyword%' OR
               a.Amount LIKE '%$keyword%' OR
               a.InputTime LIKE '%$keyword%'
              )
              $conds
                ";
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.InputTime","a.Amount","type","a.PPSAccount","Name");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $Lang['ePayment']['RecordType'])."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Payment_Field_PPSAccountNo)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Payment_Field_MappedStudent)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

## select status
$array_status_name = array($i_ResourceSelectAllStatus, $i_Payment_ShowUnlink, $i_Payment_ShowHandled);
$array_status_data = array("0", "1", "2");
$select_status = getSelectByValueDiffName($array_status_data,$array_status_name,"name='status' onChange='this.form.submit();'",$status,0,1);

$filterbar = $select_status;

/*
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
*/
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";


$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");


if ($mapped == 1)
{
    $xmsg = "$i_Payment_con_RecordMapped";
    $SysMsg = $linterface->GET_SYS_MSG("", $i_Payment_con_RecordMapped);
}

if ($msg == 2)
	$SysMsg = $linterface->GET_SYS_MSG("update");

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_MissedPPSBill, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

?>
<br /><br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?=$SysMsg?></td>
	</tr>
	<tr class="table-action-bar">
		<td align="left"><?= $filterbar ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="left"><?=$Lang['Payment']['ArchiveUserLegend']?></td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
