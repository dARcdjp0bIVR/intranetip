<?php
// Editing by 
##########################################
##	Date: 	2020-11-11	YatWoon [Case#2020-1105-1116-03073]
##			Display "TransactionTime" as "Transaction Time" if transaction type is "PPS_Charge"
##			Origin: display "Post Time" as "Transaction Time"
##
##	Date:	2017-02-22  Carlos [ip.2.5.8.3.1] $sys_custom['ePayment']['HartsPreschool'] display Payment Method and Payment Remark.
##	Date:	2015-03-04	Carlos [ip2.5.6.3.1]
##			Display PAYMENT_OVERALL_TRANSACTION_LOG.TransactionDetails if not null(summary of purchased items), instead of PAYMENT_OVERALL_TRANSACTION_LOG.Details
##
##	Date:	2013-11-04	Carlos
##			Transaction Type display [Library Payment] or [Purchase] depends on Details
##
##	Date:	2013-10-29	Carlos
##			KIS - hide [Balance] and [Balance After]
##
##	Date:	2012-05-09	YatWoon
##			Fix: ordering "LogID" add desc/asc checking
##
##	Date:	2011-10-03	YatWoon
##			Change the table header title and default sorting [Case#2011-0822-1459-31073]
##
##	Date:	2011-05-25	YatWoon
##			Swap "Transaction Time" and "Add value Record Time" display data (#2011-0503-1130-17071)
##
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();


if($lpayment->isEWalletDirectPayEnabled()) {
	$text_i_Payment_TransactionType_Credit = $i_Payment_TransactionType_Income;
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance_DirectPay;
	$temp = $lpayment->CreditTransactionType;
	$temp[] = 7;
	$lpayment->CreditTransactionType = $temp;
	$temp = $lpayment->DebitTransactionType;
	unset($temp[3]);
	$lpayment->DebitTransactionType = $temp;
} else {
	$text_i_Payment_TransactionType_Credit = $i_Payment_TransactionType_Credit;
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance;
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($StartDate=="")
	$StartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($EndDate=="")
	$EndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_range_cond = " AND (a.TransactionTime BETWEEN '$StartDate' AND '$EndDate 23:59:59') ";

$lu = new libuser($StudentID);

if($RecordStatus != 4){
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
	$sql = "SELECT 
					a.UserID, $namefield, a.UserLogin,IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), IF(b.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("b.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID ='$StudentID'
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
} else {
	$username_field = getNameFieldByLang2("a.");
	$namefield = "CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')')))";
	
	$sql = "SELECT 
					a.UserID, $namefield, a.UserLogin,IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), IF(b.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("b.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID ='$StudentID'
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
}
$payment_account_result = $lpayment->returnArray($sql, 6);
list($u_id, $name, $login,$pps, $bal, $last_modified)=$payment_account_result[0];
$table_header = "<table width=90% border=0 cellpadding=0 cellspacing=0 align='center'>";
$table_header .= "<tr><td><b>$i_Payment_Field_Username : </b>$name</td><td><b>$i_UserLogin : </b>$login</td></tr>";
$table_header .= "<tr><td>".($isKIS?"&nbsp;":("<B>".$i_Payment_Field_Balance." : </b>".$bal))."</td><td><b>$i_Payment_Field_LastUpdated : </b>$last_modified</td></tr>";
$table_header .= "<tr><td><B>$i_Payment_Field_PPSAccountNo : </b>$pps</td><td><B>$i_Payment_Report_Print_Date : </b>".date('Y-m-d H:i:s')."</td></tr>";	
$table_header .="</table>";

$linterface = new interface_html();

$library_payment_words = explode(" / ",$Lang['ePayment']['LibraryPayment']);
$library_payment = Get_Lang_Selection($library_payment_words[0],$library_payment_words[1]);

$sql  = "SELECT
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i') as add_time,
               IF((a.TransactionType=1 or a.TransactionType=8),DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')) as trans_time,
               CASE a.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN IF(a.Details='".$Lang['ePayment']['LibraryPayment']."','$library_payment','$i_Payment_TransactionType_Purchase')
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEn '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END as trans_type,
               IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;') as credit,
               IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;') as debit,
               IF(a.TransactionDetails IS NOT NULL,a.TransactionDetails,a.Details) as Details, 
               ".$lpayment->getWebDisplayAmountFormatDB("a.BalanceAfter")." as BalanceAfter,
                a.RefCode,";
   	if($sys_custom['ePayment']['HartsPreschool']){
		$sql.=" CASE s.PaymentMethod ";
		foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
		{
			$sql .= " WHEN '$key' THEN '$val' ";	
		}
		$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
				END as PaymentMethod,";
	
	 	$sql .= " s.ReceiptRemark, ";
	}
       $sql.=" IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."), a.Amount, 0) as credit1,
               IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."), a.Amount, 0) as debit1";

if($sys_custom['ePayment']['MultiPaymentGateway']) {
    $sql .= ",UPPER(t.sp) as sp ";
    $sql .= ", 'ac_name'";
    $sql .= ", i.ItemID";
	$sql .= ",IF(ma.AccountName IS NULL, '', ma.AccountName) as credit_ac_name";
}
$sql .= "         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a
             LEFT JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID ";
   if($sys_custom['ePayment']['HartsPreschool']){
  	 $sql .= " LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.StudentID=a.StudentID AND a.TransactionType='2' AND a.RelatedTransactionID=s.PaymentID ";
   }

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$sql .= "LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON 
            (
                (a.TransactionType=1 AND t.CreditTransactionID=a.RelatedTransactionID and t.PaymentID=-1)
                OR (a.TransactionType=2 AND t.RecordID=(SELECT t2.RecordID FROM PAYMENT_TNG_TRANSACTION as t2 WHERE t2.PaymentID=a.RelatedTransactionID ORDER BY InputDate DESC LIMIT 1))
            )
    ";
	$sql .= "LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as ma ON (ma.AccountID = (SELECT AccountID FROM PAYMENT_MERCHANT_ACCOUNT WHERE ServiceProvider=t.Sp AND RecordStatus=1 limit 1) AND a.TransactionType=1)";
	$sql .= "LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS item ON item.StudentID=a.StudentID AND ((a.TransactionType=6 AND a.RelatedTransactionID=item.ItemID) OR (a.TransactionType!=6 AND a.RelatedTransactionID=item.PaymentID))
			LEFT JOIN PAYMENT_PAYMENT_ITEM AS i ON i.ItemID= item.ItemID ";
}

    $sql.=" WHERE
              a.StudentID = '$StudentID' ";
if (trim($keyword) != "") {
	$sql .= " AND
            (
             a.Details LIKE '%$keyword%'
            )";
}
if($isKIS){
	$field_array = array("add_time","trans_time","trans_type","debit1","a.Details");
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$field_array[] = "sp";
		$field_array[] = "ac_name";
	}
	$field_array[] = "a.RefCode";
}else{
	$field_array = array("add_time","trans_time","trans_type","credit1","debit1","a.Details","BalanceAfter");
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$field_array[] = "sp";
		$field_array[] = "ac_name";
	}
	$field_array[] = "a.RefCode";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$field_array = array_merge($field_array, array("PaymentMethod","s.ReceiptRemark"));
}
$qfield = $field_array[$field];
if($qfield == "") {
	$qfield = $field_array[0];
}
/*
switch($field)
{
	case 0: $qfield = "add_time";			break;
	case 1: $qfield = "trans_time";			break;
	case 2: $qfield = "trans_type";			break;
	case 3: $qfield = "credit1";			break;
	case 4: $qfield = "debit1";			break;
	case 5: $qfield = "a.Details";			break;
	case 6: $qfield = "BalanceAfter";			break;
	case 7: $qfield = "a.RefCode";			break;
			
}
*/
$qorder = $order ? "asc" : "desc";

$sql .= "     $date_range_cond
            ORDER BY $qfield $qorder, a.LogID ". (($order==0) ? " DESC" : " ASC");

$li = new libdb();
$result = $li->returnArray($sql);

$display = "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$display .= "<thead>";
$display .= "<tr>
				<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>". $Lang['ePayment']['PostTime'] ."</td>
				<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_Field_TransactionTime</td>
				<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_Field_TransactionType</td>";
if(!$isKIS){
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$text_i_Payment_TransactionType_Credit</td>";
}
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_TransactionType_Debit</td>
				<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_Field_TransactionDetail</td>";
if(!$isKIS){	
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_Field_BalanceAfterTransaction</td>";
}

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>".$Lang['ePayment']['PaymentGateway']."</td>";
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>".$Lang['ePayment']['MerchantAccountName']."</td>";
}

	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>$i_Payment_Field_RefCode</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>".$Lang['ePayment']['PaymentMethod']."</td>";
	$display.= "<td valign='middle' class='eSporttdborder eSportprinttabletitle print_header'>".$Lang['ePayment']['PaymentRemark']."</td>";
}	
	$display.="</tr>\n";
$display .= "</thead>";
$display .= "<tbody>";

for ($i=0; $i<sizeof($result); $i++)
{
     $css = ($type==1?"attendancepresent":"attendanceouting");
     list($transaction_time, $addv_file_time, $transaction_type, $credit, $debit, $details, $balance, $ref_code) = $result[$i];
     if ($ref_code == "") $ref_code = "--";
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$transaction_time</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$addv_file_time</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$transaction_type</td>";
  if(!$isKIS){   
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$credit</td>";
  }
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$debit</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$details&nbsp;</td>";
  if(!$isKIS){
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$balance</td>";
  }

	if($sys_custom['ePayment']['MultiPaymentGateway']) {
	    $sp = $result[$i]['sp'];
		$ac_name = '';
		if($result[$i]['ItemID'] != '') {
			$item_merchant_accounts = $lpayment->returnPaymentItemMultiMerchantAccountID($result[$i]['ItemID']);
			$merchant_account_in_use = $lpayment->getMerchantAccounts(array("AccountID" => $item_merchant_accounts, "ServiceProvider" => $result[$i]['sp']));
			$ac_name = $merchant_account_in_use[0]['AccountName'];
		} else {
			if ($result[$i]['credit_ac_name'] != '') {
				$ac_name = $result[$i]['credit_ac_name'];
			}
		}
		$display .= "<td class=\"eSporttdborder eSportprinttext\">$sp</td>";
		$display .= "<td class=\"eSporttdborder eSportprinttext\">$ac_name</td>";
	}

     $display .= "<td class=\"eSporttdborder eSportprinttext\">$ref_code</td>";
  if($sys_custom['ePayment']['HartsPreschool']){
  	$display .= "<td class=\"eSporttdborder eSportprinttext\">".$result[$i]['PaymentMethod']."</td>";
  	$display .= "<td class=\"eSporttdborder eSportprinttext\">".$result[$i]['ReceiptRemark']."</td>";
  }   
     $display .= "</tr>\n";
}
$display .= "</tbody>";
$display .= "</table>";

$archive_name_temp = $lu->ReturnArchiveStudentInfo($StudentID);
$student_name = trim($lu->UserNameClassNumber()) ? $lu->UserNameClassNumber() : $archive_name_temp[0];

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
thead td.print_header {border-top: 1px solid #333333;}
table.eSporttableborder {border-top: 0px}
</style>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='center' class='eSportprinttitle'>
			<strong><?=$i_Payment_Menu_DataBrowsing?><br /><?=$text_i_Payment_Menu_Browse_StudentBalance?><br />
			<?=$i_Payment_Menu_Browse_TransactionRecord." [".$student_name."]"?></strong>
		</td>
	</tr>
</table>
<?=$table_header?>
<?=$display?>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>