<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lpayment->Start_Trans();
for ($i=0; $i<sizeof($StudentID); $i++)
{
	$Result['MakeDonate'.$StudentID[$i]] = $lpayment->Make_Donation($StudentID[$i]);
}

if (in_array(false,$Result)) {
	$lpayment->RollBack_Trans();
	$Msg = $Lang['ePayment']['DonateFail'];
}
else {
	$lpayment->Commit_Trans();
	$Msg = $Lang['ePayment']['DonateSuccess'];
}


header ("Location: index.php?user_type=$user_type&ClassName=$ClassName&RecordStatus=$RecordStatus&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&donate=1&count=$count&Msg=".urlencode($Msg));
intranet_closedb();
?>
