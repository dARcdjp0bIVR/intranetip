<?php
// Editing by 
/*
 * @param array $StudentID : student user ids to check for
 *
 *  Date:   2020-03-05 (Bill)   [2020-0210-1031-11073]
 *          add flag to skip unpaid item checking when refund
 *
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();

// [2020-0210-1031-11073]
$isSkipCheck = $sys_custom['ePayment']['refundSkipUnpaidItemChecking'] && $actionTarget == 'refund.php';

$records = $lpayment->getUnsettledPaymentItemsByStudents($StudentID);
$record_count = count($records);
if($record_count > 0 && !$isSkipCheck)
{
	$studentToItems = array();
	for($i=0; $i<$record_count; $i++){
		if(!isset($studentToItems[$records[$i]['UserID']])){
			$studentToItems[$records[$i]['UserID']] = array();
		}
		$studentToItems[$records[$i]['UserID']][] = $records[$i];
	}

	$x .= '<div style="height:340px; overflow-x:hidden; overflow-y:auto;">'."\n";
	$x .= '<table width="98%" border="0" cellspacing="0" cellpadding="5">
				<tbody>
					<tr>
						<td>
							<p>'.$Lang['ePayment']['StudentsHaveUnsettledPayments'].'</p>
						</td>
					</tr>'."\n";
			 $x .= '<tr>
						<td>';
				$x .= '<table class="common_table_list_v30">';
						$x.= '<thead>';
							$x.= '<tr><th class="num_check">#</th>';
							$x.= '<th style="width:30%">'.$i_UserStudentName.'</th>';
							$x.= '<th style="width:25%">'.$i_Payment_Field_PaymentCategory.'</th>';
							$x.= '<th style="width:25%">'.$i_Payment_Field_PaymentItem.'</th>';
							$x.= '<th style="width:20%">'.$i_Payment_Field_Amount.'</th>';
							$x.= '</tr>';
						$x.= '</thead>';
						$x.= '<tbody>';
						$n = 0;
						foreach($studentToItems as $uid => $recordAry) {
							$n++;
							$rowspan = count($recordAry);
							for($j=0; $j<$rowspan; $j++) {
								$x .= '<tr>';
									if($j == 0){
										$x .= '<td rowspan="'.$rowspan.'">'.($n).'</td>';
										$x .= '<td rowspan="'.$rowspan.'">'.$recordAry[$j]['StudentName'].'</td>';
									}
									$x .= '<td>'.$recordAry[$j]['CategoryName'].'&nbsp;</td>';
									$x .= '<td>'.$recordAry[$j]['ItemName'].'&nbsp;</td>';
									$x .= '<td>'.$lpayment->getWebDisplayAmountFormat($recordAry[$j]['Amount']).'</td>';
								$x .= '</tr>'."\n";
							}
						}
						$x.= '</tbody>';
					$x.= '</table>';
				$x.='</td>
				</tr>'."\n";
		$x.='</tbody>
		</table>'."\n";
	$x .= '</div>'."\n";
	$x .= '<div class="edit_bottom_v30">
			<p class="spacer"></p>';
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Close'],"button","window.top.tb_remove();","cancel_btn")."&nbsp;";
	 $x .= '<p class="spacer"></p>';
	$x.='</div>';
	
	echo $x;	
}

intranet_closedb();
?>