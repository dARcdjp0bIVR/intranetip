<?php
// Editing by 
/*
 * 2017-02-14 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - Added STRN.
 * 2015-11-04 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - Add balance filter.
 * 2015-10-12 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - allow display negative balance.
 * 2015-10-07 (Carlos): $sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display of [Login ID].
 * 2015-02-26 (Carlos): $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
 * 2014-12-11 (Carlos): Modified to enable sorting by LastUpdatedAdmin
 * 2013-10-28 (Carlos): KIS - hide [Total balance] and [Balance]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();

if($lpayment->isEWalletDirectPayEnabled()) {
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance_DirectPay;
} else {
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance;
}


$linterface = new interface_html();


if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3 && $RecordStatus != 4) $RecordStatus = 1;

$namefield = getNameFieldByLang("b.");
$ArchiveNamefield = ($intranet_session_language == 'en')? 'b.EnglishName':'b.ChineseName';
$conds = "";
if ($ClassName != "")
{
    $conds .= " AND b.ClassName = '$ClassName' ";
}

if($sys_custom['ePayment']['AllowNegativeBalance']){
	if($BalanceStatus == '-1')
		$conds .= " AND a.Balance < 0 ";
}

$year_left_cond = "";
if($RecordStatus == 3 || $RecordStatus == 4) {
	if($YearOfLeft != "") {
		$year_left_cond = " AND b.YearOfLeft = '$YearOfLeft' ";
	}		
}

switch($user_type){
	case 1: $user_cond = " AND b.RecordType=1"; break;
	case 2: $user_cond = " AND b.RecordType=2"; break;
	default : $user_cond = " AND b.RecordType IN (1,2) "; break;
}

if($RecordStatus != 4){
	$sql = "SELECT COUNT(*),SUM(".$lpayment->getExportAmountFormatDB("a.Balance").") 
					FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.StudentID = b.UserID
	        WHERE
	              b.RecordStatus = '$RecordStatus'  
	              $user_cond 
	              $conds 
	       ";
	if ($keyword != "") {
		$sql .= "
		            AND
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              ) ";
	}
	$sql .= $year_left_cond;
} else {
	$sql = "SELECT COUNT(*),SUM(".$lpayment->getExportAmountFormatDB("a.Balance").") FROM INTRANET_ARCHIVE_USER AS b LEFT OUTER JOIN PAYMENT_ACCOUNT AS a ON a.StudentID = b.UserID
	        WHERE 
	        	1 = 1 
	         	$year_left_cond
	          $user_cond $conds ";
	if ($keyword != "") {
		$sql .= "
								AND 
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              )
	          ";
	}
}
//debug_r($sql);
$temp = $lpayment->returnArray($sql,2);
if(sizeof($temp)>0){
	$no_of_users = $temp[0][0];
	$total_balance = $temp[0][1];
}else{
	$no_of_users = 0;
	$total_balance = 0;
}
$infobar = "<span class='$css_text'><u><b>$i_Payment_ItemSummary</b></u></span><br>\n";
$infobar .= "$i_Payment_Total_Users: $no_of_users<br>\n";
if(!$isKIS){
	$infobar .= "$i_Payment_Total_Balance: ".$lpayment->getWebDisplayAmountFormat($total_balance)."<br>";
}

$Inchargenamefield = getNameFieldByLang("c.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(d.ChineseName IS NULL,d.EnglishName,d.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(d.EnglishName IS NULL,d.ChineseName,d.EnglishName)";
}

if($RecordStatus != 4)
{
	$sql  = "SELECT ";
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$sql .= " b.UserLogin, ";
	}
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= " b.STRN, ";
	}
    $sql .=  " $namefield as StudentName, b.ClassName, b.ClassNumber,";
    if($sys_custom['ePayment']['AllowNegativeBalance']){
    	$sql .= $lpayment->getWebDisplayAmountFormatDB("a.Balance")." as Balance,";
    }else{
		$sql .= "IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0").") as Balance,";
    }
	$sql .= "DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i') as LastUpdated,
				IF(
                a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
                   IF(c.UserID IS NULL AND d.UserID IS NULL,
                   	CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
                   	IF(d.UserID IS NOT NULL,
                   		CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'),
                   		$Inchargenamefield
                   		)
                   ),
                   IF(
                      a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
                      CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
                      '$i_Payment_Credit_TypeUnknown'
                   )
             ) as LastUpdatedAdmin 
         FROM 
             PAYMENT_ACCOUNT as a 
			 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID 
			 LEFT JOIN INTRANET_USER as c 
	             ON (a.LastUpdateByAdmin = c.UserID) 
	         LEFT JOIN INTRANET_ARCHIVE_USER as d 
	             ON (a.LastUpdateByAdmin = d.UserID)
         WHERE
              b.RecordStatus = '$RecordStatus'  $user_cond $conds ";
  if ($keyword != "") {
  	$sql .= "
              AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%'
              ) ";
  }
  $sql .= "
              $year_left_cond
  	      ";
}else
{
$sql  = "SELECT ";
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$sql .= " b.UserLogin, ";
	}
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= " b.STRN, ";
	}
      $sql .= " $ArchiveNamefield as StudentName, b.ClassName, b.ClassNumber,";
      if($sys_custom['ePayment']['AllowNegativeBalance']){
      	$sql .= $lpayment->getWebDisplayAmountFormatDB("a.Balance")." as Balance,";
      }else{
	  	$sql .= "IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0").") as Balance,";
      }
	  $sql .= "DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i') as LastUpdated,
				IF(a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
	                   IF(c.UserID IS NULL AND d.UserID IS NULL,
	                   	CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
	                   	IF(d.UserID IS NOT NULL,
	                   		CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'),
	                   		$Inchargenamefield
	                   		)
	                   ),
	                   IF(
	                      a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
	                      CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
	                      '$i_Payment_Credit_TypeUnknown'
	                   )
	             ) as LastUpdatedAdmin  
         FROM
             INTRANET_ARCHIVE_USER as b 
			 LEFT JOIN PAYMENT_ACCOUNT as a ON a.StudentID = b.UserID 
			 LEFT JOIN INTRANET_USER as c 
	             ON (a.LastUpdateByAdmin = c.UserID) 
	         LEFT JOIN INTRANET_ARCHIVE_USER as d 
	             ON (a.LastUpdateByAdmin = d.UserID) 
         WHERE 
         		1=1 
         		$year_left_cond 
	          $user_cond $conds ";
	if ($keyword != "") {
		$sql .= "
							AND 
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%'
              )  
         		";
  }
}
$FieldArray= array();
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$FieldArray[] = "b.UserLogin";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$FieldArray[] = "b.STRN";
}
$FieldArray = array_merge($FieldArray, array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber"));
if(!$isKIS){
	$FieldArray[] = "a.Balance";
}
$FieldArray[] = "a.LastUpdated";
$FieldArray[] = "LastUpdatedAdmin";
/*
if($isKIS){
	$FieldArray = array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber","a.LastUpdated","LastUpdatedAdmin");
}else{
	$FieldArray = array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber","a.Balance","a.LastUpdated","LastUpdatedAdmin");
}
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	array_unshift($FieldArray,"b.UserLogin");
}
*/
$sql .= 'ORDER BY 
					'.($FieldArray[$SortField]==''? $FieldArray[1]: $FieldArray[$SortField]).' '.(($SortOrder == 1)? "ASC":"DESC").'
				';
$li = new libdb();
$result = $li->returnArray($sql);

$display = "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$display .= "<thead>";
$display .= "<tr>";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$Lang['AccountMgmt']['LoginID']."</td>";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>".$i_STRN."</td>";
}
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_Username</td>";
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_UserClassName</td>";
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_UserClassNumber</td>";
if(!$isKIS){
	$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_Balance</td>";
}
$display .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_LastUpdated</td>";
$display .= "</tr>\n";
$display .= "</thead>";
$display .= "<tbody>";

for ($i=0; $i<sizeof($result); $i++)
{
     $css = ($type==1?"attendancepresent":"attendanceouting");
     if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
     	//$login_id = array_shift($result[$i]);
     	$login_id = $result[$i]['UserLogin'];
     }
     if($sys_custom['ePayment']['HartsPreschool']){
     	//$strn = array_shift($result[$i]);
     	$strn = $result[$i]['STRN'];
     }
     //list($studentname, $classname, $classnum, $balance, $lastupdated) = $result[$i];
     $studentname = $result[$i]['StudentName'];
     $classname = $result[$i]['ClassName'];
     $classnum = $result[$i]['ClassNumber'];
     $balance = $result[$i]['Balance'];
     $lastupdated = $result[$i]['LastUpdated']; 
     if ($lastupdated == "") $lastupdated = "--";
     $display .= "<tr class=\"$css\">";
     if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
     	$display .= "<td class=\"eSporttdborder eSportprinttext\">$login_id</td>";
     }
     if($sys_custom['ePayment']['HartsPreschool']){
     	$display .= "<td class=\"eSporttdborder eSportprinttext\">".$strn."</td>";
     }
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$studentname</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">".($classname==""?"&nbsp;":"$classname")."</td>";
     $display .= "<td class=\"eSporttdborder eSportprinttext\">".($classnum==""?"&nbsp;":"$classnum")."</td>";
     if(!$isKIS) {
     	$display .= "<td class=\"eSporttdborder eSportprinttext\">$balance</td>";
     }
     $display .= "<td class=\"eSporttdborder eSportprinttext\">$lastupdated</td>";
     $display .= "</tr>\n";

}
$display .= "</tbody>";
$display .= "</table>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="100%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width='100%' align='center' border=0>
	<tr>
		<td align='center' class='eSportprinttitle'><b><?=$i_Payment_Menu_DataBrowsing?><br /><?=$text_i_Payment_Menu_Browse_StudentBalance?></b></td>
	</tr>
</table>
<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>
<tr>
	<td>
		<?=$infobar?>
	</td>
</tr>
</table>
<?=$display;?>
<br />
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>