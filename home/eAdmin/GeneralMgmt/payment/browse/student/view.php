<?php
// Editing by 

##########################################
##  Date:   2020-11-17  Ray
##          Add payment gateway & merchant account
##
##	Date: 	2020-11-11	YatWoon [Case#2020-1105-1116-03073]
##			Display "TransactionTime" as "Transaction Time" if transaction type is "PPS_Charge"
##			Origin: display "Post Time" as "Transaction Time"
##
##	Date:	2017-02-22  Carlos [ip.2.5.8.3.1] $sys_custom['ePayment']['HartsPreschool'] display Payment Method and Payment Remark.
##
##	Date:	2015-03-04	Carlos [ip2.5.6.3.1]
##			 Display PAYMENT_OVERALL_TRANSACTION_LOG.TransactionDetails if not null(summary of purchased items), instead of PAYMENT_OVERALL_TRANSACTION_LOG.Details
##
##	Date:	2013-11-04	Carlos
##			Transaction Type display [Library Payment] or [Purchase] depends on Details
##
##	Date:	2013-10-29	Carlos
##			KIS - hdie [Credit] and [Balance After]
##
##	Date:	2011-12-01	YatWoon
##			Fixed: failed to display archived student name [Case#2011-1130-1828-14071]
##
##	Date:	2011-10-03	YatWoon
##			Change the table header title and default sorting [Case#2011-0822-1459-31073]
##
##	Date:	2011-05-26	YatWoon
##			Fxied: query order field setting
##
##	Date:	2011-05-25	YatWoon
##			Swap "Transaction Time" and "Add value Record Time" display data (#2011-0503-1130-17071)
##
##########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($StudentID == "")
{
    header("Location: index.php");
    exit();
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_view_user_balance_and_transaction_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_view_user_balance_and_transaction_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_view_user_balance_and_transaction_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_view_user_balance_and_transaction_page_number!="")
{
	$pageNo = $ck_data_log_browsing_view_user_balance_and_transaction_page_number;
}

if ($ck_data_log_browsing_view_user_balance_and_transaction_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_view_user_balance_and_transaction_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_view_user_balance_and_transaction_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_view_user_balance_and_transaction_page_order!="")
{
	$order = $ck_data_log_browsing_view_user_balance_and_transaction_page_order;
}

if ($ck_data_log_browsing_view_user_balance_and_transaction_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_view_user_balance_and_transaction_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_view_user_balance_and_transaction_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_view_user_balance_and_transaction_page_field!="")
{
	$field = $ck_data_log_browsing_view_user_balance_and_transaction_page_field;
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_ViewUserRecord";

if($lpayment->isEWalletDirectPayEnabled()) {
	$text_i_Payment_TransactionType_Credit = $i_Payment_TransactionType_Income;
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance_DirectPay;
	$temp = $lpayment->CreditTransactionType;
	$temp[] = 7;
	$lpayment->CreditTransactionType = $temp;
	$temp = $lpayment->DebitTransactionType;
	unset($temp[3]);
	$lpayment->DebitTransactionType = $temp;
} else {
    $text_i_Payment_TransactionType_Credit = $i_Payment_TransactionType_Credit;
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$lu = new libuser($StudentID);

# date range
$today_ts = strtotime(date('Y-m-d'));
if($TargetStartDate=="")
	$TargetStartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="")
	$TargetEndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_range_cond = " AND (a.TransactionTime BETWEEN '$TargetStartDate' AND '$TargetEndDate 23:59:59') ";

$library_payment_words = explode(" / ",$Lang['ePayment']['LibraryPayment']);
$library_payment = Get_Lang_Selection($library_payment_words[0],$library_payment_words[1]);

$sql  = "SELECT
				DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i') as add_time,
               IF((a.TransactionType=1 or a.TransactionType=8),DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')) as trans_time,
               CASE a.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN IF(a.Details='".$Lang['ePayment']['LibraryPayment']."','$library_payment','$i_Payment_TransactionType_Purchase')
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEn '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END as trans_type,";            
  if(!$isKIS){    
      $sql .= "IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;') as credit,";
  }
	  $sql .= "IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("a.Amount").",'&nbsp;') as debit,
              IF(a.TransactionDetails IS NOT NULL,a.TransactionDetails,a.Details) as Details,";
  if(!$isKIS) {
	  if($lpayment->isEWalletDirectPayEnabled() == false) {
		  $sql .= $lpayment->getWebDisplayAmountFormatDB("a.BalanceAfter") . " as BalanceAfter,";
	  }
  }

if($sys_custom['ePayment']['MultiPaymentGateway']) {
    $sql .= "UPPER(t.sp) as sp, ";
    $sql .= "'ac_name',";
}

	  $sql .= "a.RefCode,";
 if($sys_custom['ePayment']['HartsPreschool']){
	$sql.=" CASE s.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,";

 	$sql .= " s.ReceiptRemark, ";
 }
      $sql .= " IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."), a.Amount, 0) as credit1,
               IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."), a.Amount, 0) as debit1";

if($sys_custom['ePayment']['MultiPaymentGateway']) {
    $sql .= ", i.ItemID";
	$sql .= ",IF(ma.AccountName IS NULL, '', ma.AccountName) as credit_ac_name";
}

$sql .= " FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a
             LEFT JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID ";
  if($sys_custom['ePayment']['HartsPreschool']){
  	$sql .= " LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.StudentID=a.StudentID AND a.TransactionType='2' AND a.RelatedTransactionID=s.PaymentID ";
  }

if($sys_custom['ePayment']['MultiPaymentGateway']) {
    $sql .= "LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON 
            (
                (a.TransactionType=1 AND t.CreditTransactionID=a.RelatedTransactionID and t.PaymentID=-1)
                OR (a.TransactionType=2 AND t.RecordID=(SELECT t2.RecordID FROM PAYMENT_TNG_TRANSACTION as t2 WHERE t2.PaymentID=a.RelatedTransactionID ORDER BY InputDate DESC LIMIT 1))
            )
    ";
   $sql .= "LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as ma ON (ma.AccountID = (SELECT AccountID FROM PAYMENT_MERCHANT_ACCOUNT WHERE ServiceProvider=t.Sp AND RecordStatus=1 limit 1) AND a.TransactionType=1)";
   $sql .= "LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS item ON item.StudentID=a.StudentID AND ((a.TransactionType=6 AND a.RelatedTransactionID=item.ItemID) OR (a.TransactionType!=6 AND a.RelatedTransactionID=item.PaymentID))
			LEFT JOIN PAYMENT_PAYMENT_ITEM AS i ON i.ItemID= item.ItemID ";
}

 $sql.=" WHERE
              a.StudentID = '$StudentID' ";
if (trim($keyword) != "") {
	$sql .= " AND
            (
             a.Details LIKE '%$keyword%'
            )";
}
$sql .= "     $date_range_cond
                ";
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$field_array = array();
if($isKIS) {
	$field_array = array("add_time","trans_time","trans_type","debit1","a.Details");
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$field_array[] = "sp";
		$field_array[] = "ac_name";
	}
	$field_array[] = "a.RefCode";
}else if($lpayment->isEWalletDirectPayEnabled()) {
	$field_array = array("add_time","trans_time","trans_type","credit1","debit1","a.Details");
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$field_array[] = "sp";
		$field_array[] = "ac_name";
	}
	$field_array[] = "a.RefCode";
} else {
	$field_array = array("add_time","trans_time","trans_type","credit1","debit1","a.Details","BalanceAfter");
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$field_array[] = "sp";
		$field_array[] = "ac_name";
	}
	$field_array[] = "a.RefCode";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$field_array = array_merge($field_array, array("PaymentMethod","s.ReceiptRemark"));
}

$li->field_array = $field_array;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
$li->fieldorder2 = " , a.LogID " . (($li->order==0) ? " DESC" : " ASC");


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletoplink'>#</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $Lang['ePayment']['PostTime'])."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionType)."</td>\n";
if(!$isKIS){
	$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $text_i_Payment_TransactionType_Credit)."</td>\n";
}
$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_TransactionType_Debit)."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionDetail)."</td>\n";
if(!$isKIS){
	if($lpayment->isEWalletDirectPayEnabled() == false) {
		$li->column_list .= "<td class='tablebluetop tabletoplink'>" . $li->column($pos++, $i_Payment_Field_BalanceAfterTransaction) . "</td>\n";
	}
}

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$li->column_list .= "<td class='tablebluetop tabletoplink'>".$Lang['ePayment']['PaymentGateway']."</td>\n";
	$li->column_list .= "<td class='tablebluetop tabletoplink'>".$Lang['ePayment']['MerchantAccountName']."</td>\n";
}

$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
if($sys_custom['ePayment']['HartsPreschool']){
	$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $Lang['ePayment']['PaymentMethod'])."</td>\n";
	$li->column_list .= "<td class='tablebluetop tabletoplink'>".$li->column($pos++, $Lang['ePayment']['PaymentRemark'])."</td>\n";
}

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$archive_name_temp = $lu->ReturnArchiveStudentInfo($StudentID);
$student_name = trim($lu->UserNameClassNumber()) ? $lu->UserNameClassNumber() : $archive_name_temp[0];

$PAGE_NAVIGATION[] = array($i_Payment_Menu_Browse_TransactionRecord." [".$student_name."]");

$TAGS_OBJ[] = array($text_i_Payment_Menu_Browse_StudentBalance, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="Javascript">
function openPrintPage()
{
	newWindow('view_print.php?StudentID=<?=$StudentID?>&keyword=<?=$keyword?>&StartDate=<?=$TargetStartDate?>&EndDate=<?=$TargetEndDate?>&RecordStatus=<?=$RecordStatus?>&order=<?=$li->order?>&field=<?=$li->field?>',10);
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#TargetStartDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#TargetEndDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>	

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
	<td valign="top" class="tabletext" width="70%">
		<input type="text" name="TargetStartDate" id="TargetStartDate" value="<?=$TargetStartDate?>" maxlength="10" class='textboxnum'>
		<span class="tabletextremark">(yyyy-mm-dd)</span>
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
	<td valign="top" class="tabletext" width="70%">
		<input type="text" name="TargetEndDate" id="TargetEndDate" value="<?=$TargetEndDate?>" maxlength="10" class='textboxnum'>
		<span class="tabletextremark">(yyyy-mm-dd)</span>
	</td>
</tr>
<tr>
	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td colspan="2" align="center" class="tabletext">
		<?= $linterface->GET_ACTION_BTN($button_view, "button", "document.form1.submit();","submit2") ?>
	</td>
</tr>
</table>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
	</tr>
	<tr>
		<td align="left"><?=$searchbar?></td>
	</tr>
</table>
<?php echo displayFormat2($li, "95%", "blue"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="RecordStatus" value="<?=$RecordStatus?>">
</form>
<br />
<?

function displayFormat2($li, $width="", $tablecolor=""){
	global $image_path, $lpayment;
	$i = 0;
	$DisplayIndex = ($li->HideIndex) ? false : true;
	$TotalColumn = ($DisplayIndex) ? $li->no_col : $li->no_col-1;

	$li->rs = $li->db_db_query($li->built_sql());
	$width = ($width!="") ? $width : "100%";

	$n_start = $li->n_start;
	$x .= "<table width='{$width}' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
	$x .= $li->displayColumn();
	if ($li->db_num_rows()==0)
	{
		$x .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='".$TotalColumn."'>".$li->no_msg."</td></tr>\n";
	} else
	{
		while ($row = $li->db_fetch_array())
		{
			$i++;
			$css = ($i%2) ? "1" : "2";
			if ($i>$li->page_size)
			{
				break;
			}
			$x .= "<tr class='table{$tablecolor}row{$css}'>\n";
			if ($DisplayIndex)
			{
				$x .= "<td class='tabletext'>".($n_start+$i)."</td>\n";
			}

			for ($j=0; $j<$li->no_col-1; $j++)
			{
			    if($row[$j] == 'ac_name') {
			        if($row['ItemID'] != '') {
						$item_merchant_accounts = $lpayment->returnPaymentItemMultiMerchantAccountID($row['ItemID']);
						$merchant_account_in_use = $lpayment->getMerchantAccounts(array("AccountID" => $item_merchant_accounts, "ServiceProvider" => $row['sp']));
						$row[$j] = $merchant_account_in_use[0]['AccountName'];
					} else {
			            if($row['credit_ac_name'] != '') {
							$row[$j] = $row['credit_ac_name'];
						} else {
							$row[$j] = '';
						}
					}
				}
				$x .= $li->displayCell($j, $row[$j]);
			}
			$x .= "</tr>\n";
		}
	}
	if($li->db_num_rows()<>0 && $li->with_navigation) $x .= "<tr><td colspan='".$TotalColumn."' class='table{$tablecolor}bottom'>".$li->navigation()."</td></tr>";
	$x .= "</table>\n";
	$li->db_free_result();

	return $x;
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>