<?php
// Editing by 
/*
 * 2019-06-24 (Carlos): Cater top-up.
 * 2017-01-13 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "ImportTNGTransactionRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['ImportTNGTransactionRecords'],"",0);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

if(isset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']))
{
	$Msg = $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'];
	unset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>
<br />
<form name="form1" method="post" action="import_confirm.php" enctype="multipart/form-data" onsubmit="checkForm(this);return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan=2" class="tabletextremark">
						<?=$Lang['ePayment']['ImportTNGTransactionRecordsInstruction']?><br>
					</td>
				</tr>
		    	<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	                	<?=$i_select_file?>
	                </td>
	                <td class="tabletext" width="70%">
	    				<input class="file" type="file" name="tngfile">
	    			</td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "") ?>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript" language="JavaScript">
function checkForm(obj)
{
	var tngfile = $('#tngfile').val();
	
	if(tngfile == '' || Get_File_Ext(tngfile) != 'csv'){
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		return false;
	}
	
	obj.submit();
}
</script>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.tngfile");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>