<?php
// Editing by 
/*
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();

$today_ts = strtotime(date('Y-m-d'));
$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

# Class Menu #
//$select_class = $lclass->getSelectClass("name=\"ClassName\" onchange=removeStudent();this.form.action='';this.form.target='';this.form.submit()",($data_format == 2?$ClassName:""));
$select_class = $lclass->getSelectClassID(' name="ClassID[]" id="ClassID" onchange="classSelectionChanged(this);" multiple="multiple" size="10" ', $__selected="", $__DisplaySelect=2, $__AcademicYearID='',$__optionFirst='', $__DisplayClassIDArr='', $__TeachingOnly=0);

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "ElectronicBilling";

$linterface = new interface_html();


$TAGS_OBJ[] = array($Lang['ePayment']['ElectronicBilling'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>
<style type="text/css">
table.common_table_list_v30 tr td {background-color: transparent;}
table.common_table_list_v30 tr:hover {background-color: #FFFFCC;}
.billing_report {padding-bottom:1em;}
</style>
<script type="text/javascript" language="JavaScript">
function loadTable()
{
	Block_Element('TableLayer');
	$.post(
		'get_student_list.php',
		$('#form1').serialize(),
		function(data){
			$('#TableLayer').html(data);
			UnBlock_Element('TableLayer');
		}
	);
}

function submitForm(formObj)
{
	var from_date_error = $('#DPWL-FromDate').html() != '';
	var to_date_error = $('#DPWL-ToDate').html() != '';
	var class_id_error = $('#ClassID').length > 0 && $('#ClassID option:selected').length == 0;
	var student_id_error = $('#StudentID').length > 0 && $('#StudentID option:selected').length == 0;
	var record_type_error = $('input[name="RecordType[]"]:checked').length == 0;
	
	if(student_id_error){
		$('#StudentSelectionError').show();
		$('#StudentID').focus();
	}else{
		$('#StudentSelectionError').hide();
	}
	
	if(class_id_error){
		$('#ClassSelectionError').show();
		$('#ClassID').focus();
	}else{
		$('#ClassSelectionError').hide();
	}
	
	if(record_type_error){
		$('#RecordTypeError').show();
		$('#RecordType1').focus();
	}else{
		$('#RecordTypeError').hide();
	}
	
	if(from_date_error || to_date_error || class_id_error || student_id_error || record_type_error)
		return false;
	
	loadTable();
}

function classSelectionChanged(obj)
{
	var selected_vals = [];
	
	for(var i=0;i<obj.options.length;i++){
		if(obj.options[i].selected){
			selected_vals.push(obj.options[i].value);
		}
	}
	
	if(selected_vals.length == 0){
		$('#StudentSelectionRow').hide();
		$('#StudentSelectionLayer').html('');
		return;
	}
	
	$.post(
		'get_student_selection.php',
		{
			'ClassID[]': selected_vals
		},
		function(data){
			$('#StudentSelectionLayer').html(data);
			$('#StudentSelectionRow').show();
		}
	);
}

function SelectAll(obj)
{
	 for (i=0; i<obj.length; i++)
	 {
	      obj.options[i].selected = true;
	 }
}

function generateBilling(studentId, fromDate, toDate)
{
	var return_message = {'1':'<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>','0':'<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>'};
	Block_Document();
	$.post(
		'generate.php',
		{
			'StudentID[]': studentId,
			'FromDate': fromDate,
			'ToDate': toDate,
			'RecordType[]': [1]
		},
		function(returnData){
			var msg = (return_message[returnData])? return_message[returnData]: return_message['0'];
			Get_Return_Message(msg);
			loadTable();
			UnBlock_Document();
		}
	);
}

function generateSelectedBilling(fromDate, toDate)
{
	var studentIdAry = [];
	var studentIdObjs = document.getElementsByName('SelectedStudentID[]');
	for(var i=0;i<studentIdObjs.length;i++){
		if(studentIdObjs[i].checked){
			studentIdAry.push(studentIdObjs[i].value);
		}
	}
	if(studentIdAry.length == 0){
		jAlert(globalAlertMsg2);
		return;
	}
	generateBilling(studentIdAry, fromDate, toDate);
}

function deleteBilling(recordId)
{
	var return_message = {'1':'<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>','0':'<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>'};
	jConfirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>','<?=$Lang['Btn']['Confirm']?>',function(choice){
		if(!choice) return;
		Block_Document();
		$.post(
			'delete.php',
			{
				'RecordID[]': recordId
			},
			function(returnData){
				var msg = (return_message[returnData])? return_message[returnData]: return_message['0'];
				Get_Return_Message(msg);
				loadTable();
				UnBlock_Document();
			}
		);
	});
}

function deleteSelectedBilling()
{
	var recordIdAry = [];
	var studentIdObjs = document.getElementsByName('SelectedStudentID[]');
	for(var i=0;i<studentIdObjs.length;i++){
		if(studentIdObjs[i].checked){
			var tmp_obj = $(studentIdObjs[i]);
			var billIdObjs = tmp_obj.closest('tr').find('input[name="BillingID[]"]');
			for(var j=0;j<billIdObjs.length;j++){
				recordIdAry.push(billIdObjs[j].value);
			}
		}
	}
	if(recordIdAry.length == 0)
	{
		jAlert(globalAlertMsg2);
		return;
	}
	deleteBilling(recordIdAry);
}

function printBilling(recordId, format)
{
	var popup = window.open ('', 'intranet_popup', 'menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700');
	var f = $(document.form_print);
	var recordIdAry = [];
	if(typeof(recordId) == 'string'){
		recordIdAry.push(recordId);
	}else{
		recordIdAry = recordId;
	}
	f.find('input').remove();
	for(var i=0;i<recordIdAry.length;i++)
	{
		f.append('<input name="RecordID[]" type="hidden" value="'+recordIdAry[i]+'" />');
	}
	f.append('<input name="Format" type="hidden" value="'+format+'" />');
	var oldTarget = f.attr('target');
	f.attr('target', 'intranet_popup');
	f.submit();
	f.attr('target',oldTarget);
	f.find('input').remove();
	//newWindow('print.php?RecordID=' + recordId + '&Format=' + format, 24);
}

function printSelectedBilling(format)
{
	var recordIdAry = [];
	var studentIdObjs = document.getElementsByName('SelectedStudentID[]');
	for(var i=0;i<studentIdObjs.length;i++){
		if(studentIdObjs[i].checked){
			var tmp_obj = $(studentIdObjs[i]);
			var billIdObjs = tmp_obj.closest('tr').find('input[name="BillingID[]"]');
			for(var j=0;j<billIdObjs.length;j++){
				recordIdAry.push(billIdObjs[j].value);
			}
		}
	}
	if(recordIdAry.length == 0)
	{
		jAlert(globalAlertMsg2);
		return;
	}
	printBilling(recordIdAry, format);
}
</script>
<br />
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
<table class="form_table_v30">
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title"><?=$i_Payment_Menu_PrintPage_AddValueReport_Date?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$i_UserClassName?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php 
				$select_all_btn = '&nbsp;'.$linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('ClassID'));classSelectionChanged(document.getElementById('ClassID'));");
				echo $linterface->Get_MultipleSelection_And_SelectAll_Div($select_class, $select_all_btn, 'ClassSelectionLayer'); 
			?> 
			<div id="ClassSelectionError" style="display:none;color:red;"><?=$Lang['General']['JS_warning']['SelectClasses']?></div>
		</td>
	</tr>
	<tr id="StudentSelectionRow" style="display:none;" valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$i_UserStudentName?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php
				$select_all_btn = $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('StudentID'));");
				echo $linterface->Get_MultipleSelection_And_SelectAll_Div('', $select_all_btn, 'StudentSelectionLayer'); 
			?>
			<div id="StudentSelectionError" style="display:none;color:red;"><?=$Lang['General']['JS_warning']['SelectStudent']?></div>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['ePayment']['RecordType']?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php
				echo $linterface->Get_Checkbox('RecordType1', 'RecordType[]', '1', 1, $__Class='', $__Display=$Lang['ePayment']['NegativeBalanceRecords'], $__Onclick='', $__Disabled='');
				echo '&nbsp;'.$linterface->Get_Checkbox('RecordType2', 'RecordType[]', '2', 0, $__Class='', $__Display=$Lang['ePayment']['GeneratedRecords'], $__Onclick='', $__Disabled='');
			?>
			<div id="RecordTypeError" style="display:none;color:red;"><?=$Lang['General']['PleaseSelect'].'&nbsp;'.$Lang['ePayment']['RecordType']?></div>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm(document.form1);", "submit_btn").'&nbsp;'?>
		</td>
	</tr>
</table>
</form>
<form name="form_print" method="post" id="form_print" action="print.php">
</form>
<br />
<div id="TableLayer"></div>
<?
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>