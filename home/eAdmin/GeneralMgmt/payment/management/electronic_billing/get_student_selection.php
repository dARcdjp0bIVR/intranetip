<?php
// Editing by 
/*
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$ClassIdAry = (array)$_POST['ClassID'];
$students = $lclass->getStudentListByClassID($ClassIdAry);

$student_selection = getSelectByArray($students, ' id="StudentID" name="StudentID[]" multiple="multiple" size="10" ', '', $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
echo $student_selection;

intranet_closedb();
?>