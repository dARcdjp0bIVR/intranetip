<?php
// Editing by 
/*
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lpayment = new libpayment();

$from_date = date("Y-m-d", strtotime($_POST['FromDate']));
$to_date = date("Y-m-d", strtotime($_POST['ToDate']));

$result = $lpayment->getNegativeBalanceRecordsByDateRange($_POST);
$result_count = count($result);

$checkbox_all = '<input type="checkbox" id="checkmaster" name="checkmaster" onclick="Set_Checkbox_Value(\'SelectedStudentID[]\',this.checked)" />';
//$header_generate_button = $linterface->GET_SMALL_BTN('Generate', 'button', $ParOnClick="", $ParName="GenerateSelectedBtn", $ParOtherAttribute="", $OtherClass="", $ParTitle='');
//$header_print_pdf_button = $linterface->GET_SMALL_BTN('Print PDF', 'button', $ParOnClick="", $ParName="PrintSelectedPDFBtn", $ParOtherAttribute="", $OtherClass="", $ParTitle='');
//$header_print_html_button = $linterface->GET_SMALL_BTN('Print HTML', 'button', $ParOnClick="", $ParName="PrintSelectedHTMLBtn", $ParOtherAttribute="", $OtherClass="", $ParTitle='');

$x = '';

$btn_ary = array();
$btn_ary[] = array('other','javascript:generateSelectedBilling(\''.$from_date.'\',\''.$to_date.'\');',$Lang['Btn']['Generate'],'GenerateTool','');
$btn_ary[] = array('other','javascript:printSelectedBilling(\'pdf\');',$Lang['Btn']['Print'].' PDF','PrintPDFTool','');
$btn_ary[] = array('other','javascript:printSelectedBilling(\'html\');',$Lang['Btn']['Print'].' HTML','PrintHTMLTool','');
$btn_ary[] = array('delete','javascript:deleteSelectedBilling();',$Lang['Btn']['Delete'],'DeleteTool','');

$table_tool = $linterface->Get_DBTable_Action_Button_IP25($btn_ary);
$x .= $table_tool;

$x.= '<table class="common_table_list_v30">'."\n";
		$x.='<thead>'."\n";
		$x.= '<tr>';
			$x.='<th class="num_check">#</th>';
			$x.='<th style="width:10%;">'.$Lang['ePayment']['StudentNumber'].'</th>';
			$x.='<th style="width:10%;">'.$i_general_class.'</th>';
			$x.='<th style="width:5%;">'.$Lang['General']['ClassNumber'].'</th>';
			$x.='<th style="width:15%;">'.$Lang['General']['ChineseName'].'</th>';
			$x.='<th style="width:15%;">'.$Lang['General']['EnglishName'].'</th>';
			$x.='<th style="width:5%;">'.$i_Payment_Field_Balance.'</th>';
			$x.='<th style="width:40%;">'.$Lang['ePayment']['ElectronicBilling'].'</th>';
			$x.='<th class="num_check">'.$checkbox_all.'</th>';
		$x.= '</tr>'."\n";
		$x.='</thead>'."\n";
		$x.='<tbody>'."\n";


	if($result_count > 0)
	{
		$n = 0;
		foreach($result as $sid => $trans_ary){
			$n += 1;
			
			$last_record = count($trans_ary['Transactions'])>0? $trans_ary['Transactions'][count($trans_ary['Transactions'])-1] : $trans_ary['Billings'][count($trans_ary['Billings'])-1];
			$checkbox = $linterface->Get_Checkbox("SelectedStudentID[".$sid."]", "SelectedStudentID[]", $sid, 0, $__Class='', $__Display='', $__Onclick='', $__Disabled='');
			$generate_btn = count($trans_ary['Transactions'])>0? $linterface->GET_SMALL_BTN($Lang['Btn']['Generate'], 'button', $ParOnClick="generateBilling('$sid','$from_date','$to_date')", $ParName="GenerateBtn".$sid, $ParOtherAttribute="", $OtherClass="", $ParTitle='') : '';
			//$view_btn = $linterface->GET_SMALL_BTN('View', 'button', $ParOnClick="", $ParName="ViewBtn".$sid, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
			
			$billing_records = $trans_ary['Billings'];
			$billing_count = count($billing_records);
			if($billing_count == 0){
				//$y = 'No billing report'.'&nbsp;';
				$y = $generate_btn;
			}else{
				$y = '';
				for($j=0;$j<$billing_count;$j++){
					$billing_id = $billing_records[$j]['RecordID'];
					$delete_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['Delete'], 'button', $ParOnClick="deleteBilling('$billing_id')", $ParName="DeleteBtn".$billing_id, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
					$print_pdf_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['Print'].' PDF', 'button', $ParOnClick="printBilling('$billing_id','pdf')", $ParName="PrintPDFBtn".$sid, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
					$print_html_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['Print'].' HTML', 'button', $ParOnClick="printBilling('$billing_id','html')", $ParName="PrintHTMLBtn".$sid, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
					$hidden_billing_id = '<input name="BillingID[]" type="hidden" value="'.$billing_id.'" />';
					$last_updated = str_replace('<!--USERNAME-->',$billing_records[$j]['GeneratedUser'],str_replace('<!--DATETIME-->',$billing_records[$j]['GeneratedDate'],$Lang['ePayment']['DatetimeByUsername']));
					$y .= '<div class="billing_report">['.$billing_records[$j]['BillingNumber'].']&nbsp;'.'('.$billing_records[$j]['FromDate'].' '.$Lang['General']['To'].' '.$billing_records[$j]['ToDate'].')'.'('.$last_updated.')<br />'.$delete_btn.'&nbsp;'.$print_pdf_btn.'&nbsp;'.$print_html_btn.$hidden_billing_id.'</div>';
				}
				$y .= $generate_btn;
			}
			
			$x .= '<tr>';
				$x .= '<td>'.$n.'</td>';
				$x .= '<td>'.Get_String_Display($last_record['RegNo'],1).'</td>';
				$x .= '<td>'.Get_String_Display($last_record['ClassName'],1).'</td>';
				$x .= '<td>'.Get_String_Display($last_record['ClassNumber'],1).'</td>';
				$x .= '<td>'.Get_String_Display($last_record['ChineseName'],1).'</td>';
				$x .= '<td>'.Get_String_Display($last_record['EnglishName'],1).'</td>';
				$x .= '<td>'.(isset($last_record['BalanceAfter'])? $lpayment->getWebDisplayAmountFormat($trans_ary['Balance']) : $lpayment->getWebDisplayAmountFormat($last_record['BalanceWhenGenerated'])).'</td>';
				$x .= '<td>'.$y.'</td>';
				$x .= '<td>'.$checkbox.'</td>';
			$x .= '</tr>'."\n";
		}
	}else{
		$x.='<tr>';
			$x.='<td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x.='</tr>'."\n";
	}
$x.= '</tbody>'."\n";
$x.= '</table>'."\n";

echo $x;

intranet_closedb();
?>