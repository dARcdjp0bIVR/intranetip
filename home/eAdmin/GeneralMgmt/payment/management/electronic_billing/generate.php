<?php
// Editing by 
/*
 * !!!!! Use UTF-8 to edit this page. !!!!!
 * 2016-04-21 (Carlos): Get school info from settings.
 * 2015-12-02 (Carlos): Changed billing number format. 
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lpayment = new libpayment();

$format = strtolower($_POST['format']);
$from_date = $_POST['FromDate'];
$to_date = $_POST['ToDate'];
$studentIdAry = (array)$_POST['StudentID'];

$today_ts = strtotime(date('Y-m-d'));
$FromYear = date('Y',getStartOfAcademicYear($today_ts));
$ToYear = date('Y',getEndOfAcademicYear($today_ts));

function digitToChinese($text)
{
	$map = array('0'=>'零','1'=>'一','2'=>'二','3'=>'三','4'=>'四','5'=>'五','6'=>'六','7'=>'七','8'=>'八','9'=>'九');
	$map_keys = array_keys($map);
	$out = '';
	$length = strlen($text);
	for($i=0;$i<$length;$i++)
	{
		if(in_array($text[$i],$map_keys)){
			$out .= $map[$text[$i]];
		}else{
			$out .= $text[$i];
		}
	}
	return $out;
}

$display_from_year = digitToChinese($FromYear);
$display_to_year = digitToChinese($ToYear);

$records = $lpayment->getNegativeBalanceRecordsByDateRange($_POST);
$record_count = count($records);

if($record_count == 0)
{
	echo '0';
	intranet_closedb();
	exit;
}

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = trim($school_data[0]);
$school_org = trim($school_data[1]);
$school_phone = trim($school_data[2]);
$school_address = '';
for($i=3; $i<count($school_data); $i++){
	$school_address .= $school_data[$i]."\n";
}

$expense_types = array(2,3,4,7,8,9,10);
$income_types = array(1,5,6,11);

$generated_date = date("j / n / Y");

foreach($records as $sid => $ary)
{
	$transaction_ary = $ary['Transactions'];
	$transaction_count = count($transaction_ary);
	
	$reg_no = $transaction_ary[$transaction_count-1]['RegNo'];
	$class_name = $transaction_ary[$transaction_count-1]['ClassName'];
	$class_number = $transaction_ary[$transaction_count-1]['ClassNumber'];
	$chinese_name = $transaction_ary[$transaction_count-1]['ChineseName'];
	$english_name = $transaction_ary[$transaction_count-1]['EnglishName'];
	$last_balance = $transaction_ary[$transaction_count-1]['BalanceAfter'];
	
	$billing_number = $lpayment->getNextElectronicBillingNumber();
	$record_id = $lpayment->insertElectronicBillingRecord($sid, $from_date, $to_date, $billing_number, $last_balance);
	//$billing_number = $lpayment->getFormattedElectronicBillingNumber($record_id);
	
	$x = '';
	$x .= '<style type="text/css">
			html, body, table, div, p {margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;} 
			.container {font-family:新細明體,sans-serif;font-size:1em;}
			table {width:100%;border-collapse:collapse;border:0px none;margin:0;padding:0;}
			table.plain td {padding:0.2em 0.5em 0.2em 0em;}
			table th {font-weight:bold}
			table.plain tr, table.plain th, table.plain td {border:0px none;}
			table.plain td.left {width:20%;}
			table.plain td.right {width:20%; text-align:right;}
			table.plain td.center {width:60%;text-align:center;}
			table.detail td, table.detail th {border: 1px solid #000000;padding:0.2em;}
			table.detail th {text-align:center;}
			div, p{width:100%;margin:0;padding:0;}
			.underlined {text-decoration:underline;}
			.bold {font-weight:bold;}
			img {margin:1em;}
			</style>';
	
	$x .= '<div class="container">';
	$x .= '<table class="plain">';
		$x .= '<tbody>';
		$x .= '<tr>
					<td class="left"></td>
					<td class="center bold">'.$school_name.'<br />'.$school_address.'<br />電話：'.$school_phone.'</td>
					<td class="right bold">報表編號<br />'.Get_String_Display($billing_number).'</td>
				</tr>';
		$x .= '<tr>
					<td class="left"></td>
					<td class="center underlined bold">'.Get_String_Display($display_from_year).'至'.Get_String_Display($display_to_year).'年度<br />電子繳費項目支出及課外活動資助總表</td>
					<td class="right"></td>
				</tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	$x .= '<table class="plain" style="margin:1em 0em;">';
		$x .= '<tbody>';
		$x .= '<tr>';
			$x .= '<td class="underlined bold">學生證號碼</td><td class="underlined bold">班別</td><td class="underlined bold">學號</td><td class="underlined bold">中文姓名</td><td class="underlined bold">English Name</td>';
		$x .= '</tr>';
		$x .= '<tr>';
			$x .= '<td>'.Get_String_Display($reg_no).'</td><td>'.Get_String_Display($class_name).'</td><td>'.Get_String_Display($class_number).'</td><td>'.Get_String_Display($chinese_name).'</td><td>'.Get_String_Display($english_name).'</td>';
		$x .= '</tr>';
		$x .= '<tbody>';
	$x .= '</table>';
	
	
	// transaction items
	//$x .= '<p class="bold">電子繳費存入及支出記錄：</p>';
	$x .= '<table class="plain">';
		$x .= '<tbody>';
			$x .= '<tr><td class="bold">電子繳費存入及支出記錄：</td></tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	
	$x .= '<table class="detail">';
		$x .= '<thead>
					<tr>
						<th style="width:20%">交易日期</th><th colspan="2" style="width:40%">項目</th><th style="width:20%">支出 (HK$)</th><th style="width:20%">存入 (HK$)</th>
					</tr>
				</thead>';
		$x .= '<tbody>';
		
		$sum_expense = 0;
		$sum_income = 0;
			
			// the 1st transaction is the remaining balance since start date
			$date = date("j / n / Y", strtotime($transaction_ary[0]['TransactionTime']));
			$detail = '承上期結餘';
			$amount = $transaction_ary[0]['Amount'];
			$display_expense = '&nbsp;';
			$display_income = number_format($amount,1);
			$sum_income += $amount;
			
			
			$x .= '<tr>';
				$x .= '<td>'.$date.'</td>';
				$x .= '<td colspan="2">'.Get_String_Display($detail).'</td>';
				$x .= '<td align="right">'.$display_expense.'</td>';
				$x .= '<td align="right">'.$display_income.'</td>';
			$x .= '</tr>';
			
			for($i=1;$i<$transaction_count;$i++)
			{
				$date = date("j / n / Y", strtotime($transaction_ary[$i]['TransactionTime']));
				$transaction_type = $transaction_ary[$i]['TransactionType'];
				$detail = $transaction_ary[$i]['Details'];
				$amount = $transaction_ary[$i]['Amount'];
				$display_expense = in_array($transaction_type,$expense_types)? number_format($amount,1) : '&nbsp;';
				$display_income = in_array($transaction_type, $income_types)? number_format($amount,1) : '&nbsp;';
				if(in_array($transaction_type,$expense_types)){
					$sum_expense += $amount;
				}else{
					$sum_income += $amount;
				}
				
				$x .= '<tr>';
					$x .= '<td>'.$date.'</td>';
					$x .= '<td colspan="2">'.Get_String_Display($detail).'</td>';
					$x .= '<td align="right">'.$display_expense.'</td>';
					$x .= '<td align="right">'.$display_income.'</td>';
				$x .= '</tr>';
				
			}
			
		$total = $sum_income - $sum_expense;	
		$display_total = $total < 0 ? '('.str_replace('-','',number_format($total,1)).')' : number_format($total,1);
			
		$x .= '<tr>';
			$x .= '<td>'.$generated_date.'</td>';
			$x .= '<td colspan="2" align="right">總數</td>';
			$x .= '<td align="right">'.number_format($sum_expense,1).'</td>';
			$x .= '<td align="right">'.number_format($sum_income,1).'</td>';
		$x .= '</tr>';
		$x .= '<tr>';
			$x .= '<td style="border-right:0px none;">&nbsp;</td><td class="bold" style="border-left:0px none;border-right:0px none;">電子繳費戶口結餘(結欠)</td><td class="bold" align="right" style="border-left:0px none;">(a)</td><td colspan="2" align="center" class="bold">'.$display_total.'</td>';
		$x .= '</tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	
	$subsidy_items = $ary['SubsidyItems'];
	$subsidy_item_count = count($subsidy_items);
	// subsidy items
	$subsidy_total = 0.0;
	
	$x .= '<table class="plain" style="margin-top:1em;">';
		$x .= '<tbody>';
			$x .= '<tr><td class="bold">課外活動資助記錄：</td></tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	
	$x .= '<table class="detail">';
		$x .= '<thead>
					<tr>
						<th style="width:20%">資助日期</th><th colspan="2" style="width:40%">項目</th><th style="width:40%">資助金額 (HK$)</th>
					</tr>
				</thead>';
		$x .= '<tbody>';
		
		for($i=0;$i<$subsidy_item_count;$i++){
			$date = date("j / n / Y", strtotime( $subsidy_items[$i]['TransactionTime']));
			$item_name = $subsidy_items[$i]['ItemName'];
			$amount = $subsidy_items[$i]['SubsidyAmount'];
			$subsidy_total += $amount;
			
			$x .= '<tr>';
					$x .= '<td>'.$date.'</td>';
					$x .= '<td colspan="2">'.Get_String_Display($item_name).'</td>';
					$x .= '<td align="right">'.number_format($amount,1).'</td>';
			$x .= '</tr>';
		}
		
		$display_subsidy_total = number_format($subsidy_total,1);
		$x .= '<tr>';
			$x .= '<td style="border-right:0px none;">&nbsp;</td><td class="bold" style="border-left:0px none;border-right:0px none;">活動資助總額</td><td class="bold" align="right" style="border-left:0px none;">(b)</td><td align="center" class="bold">'.$display_subsidy_total.'</td>';
		$x .= '</tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	
	$net_total = $total + $subsidy_total;
	$display_net = $net_total < 0 ? '('.str_replace('-','',number_format($net_total,1)).')' : number_format($net_total,1);
	
	$x .= '<table class="detail" style="margin-top:1em;font-size:1.2em;">';
		$x .= '<thead>
					<tr>
						<th style="width:70%">電子繳費戶口結餘(結欠)及活動資助總額合計[(a) - (b)]</th><th align="center" style="width:30%">'.$display_net.'</th>
					</tr>
				</thead>';
	$x .= '</table>';
	
	$eps_barcode_number = $lpayment->getEPSBarcodeNumber($reg_no);
	$barcode_url = $lpayment->getEPSBarcodeUrl($eps_barcode_number);
	
	$x .= '<div style="text-align:center;"><img src="'.$barcode_url.'" /></div>';
	
	$x .= '<ol>';
		$x .= '<li class="bold">如同學電子繳費戶口給餘及課外活動資助總額為結欠(括號內數值)，請於一星期內利用繳費靈(PPS)或到 OK 便利店 / VanGO 便利店進行繳費，並保留有關繳費記錄。<br />繳費方法請參閱網頁 http://www.pps.com.hk。</li>';
		$x .= '<li>同學電子繳費戶口內之結餘將會轉至下一學年。</li>';
		$x .= '<li>*每個學期校方將支付首一次繳費靈手續費，費用已存入同學戶口內。</li>';
		$x .= '<li>如有查詢，請與本校校務處聯絡。</li>';
	$x .= '</ol>';
	
	$x .= '<p style="text-align:right;">校印：<span style="letter-spacing:-1px;">'.str_repeat('_',20).'</span></p>';
	/*
	$x .= '<table class="plain">';
		$x .= '<tbody>';
			$x .= '<tr><td align="right">校印：</td><td align="right"><u>'.str_repeat('&nbsp;',40).'</u></td></tr>';
		$x .= '</tbody>';
	$x .= '</table>';
	*/
	$x .= '</div>';
	//$x .= '<div style="page-break-after:always"></div>';
	
	$lpayment->updateElectronicBillingRecord($record_id, array('BillingContent' => $x, 'BalanceWhenGenerated'=>$net_total));
}

echo '1';

intranet_closedb();
?>