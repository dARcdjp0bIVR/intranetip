<?php
// Editing by 
/*
 * !!!!! Print PDF requires mPDF library !!!!!
 * !!!!! Use UTF-8 to edit !!!!!
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
ini_set('memory_limit','128M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

function strip_style_tag($html)
{
	$start_pos = strpos($html,'</style>')+strlen('</style>');
	return substr($html,$start_pos);
}

$linterface = new interface_html();
$lpayment = new libpayment();

$recordIdAry = (array)$_POST['RecordID'];
$format = strtolower($_POST['Format']);

$records = $lpayment->getElectronicBillingRecordsByRecordId($recordIdAry);
$record_count = count($records);

if($format == 'pdf'){
	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	
	$school_name = GET_SCHOOL_NAME();
	
	/*
	$x = '';
	for($i=0;$i<$record_count;$i++){
		if($i > 0){
			$x .= '<div style="page-break-after:always"></div>';
			$x .= strip_style_tag($records[$i]['BillingContent']);
		}else{
			$x .= $records[$i]['BillingContent'];
		}
	}
	*/
	
	$margin= 12.7; // mm
	$margin_top = $margin;
	$margin_bottom = $margin;
	$margin_left = $margin;
	$margin_right = $margin;
	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	
	//pdf metadata setting
	$pdf->SetTitle($school_name.' 電子繳費單');
	$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
	$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
	$pdf->SetSubject($school_name.' 電子繳費單');
	$pdf->SetKeywords($school_name, 'eClass, ePayment, 電子繳費單');
	
	// Chinese use mingliu 
	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;
	
	
	$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">第 {PAGENO} 頁，共 {nbpg} 頁</div>');
	$pdf->SetHTMLFooterByName('htmlFooter');
	//$pdf->setFooter('<div style="text-align:center;">第 {PAGENO} 頁，共 {nbpg} 頁</div>');
	
	for($i=0;$i<$record_count;$i++){
		$x = '';
		if($i > 0){
			//$x .= '<div style="page-break-after:always"></div>';
			$x .= strip_style_tag($records[$i]['BillingContent']);
		}else{
			$x .= $records[$i]['BillingContent'];
		}
		
		$pdf->WriteHTML($x);
		if($i < $record_count-1)
			$pdf->WriteHTML('<pagebreak resetpagenum="1" pagenumstyle="1" suppress="off" />');
	}
	
	//$pdf->WriteHTML($x);
	//$pdf->DefHTMLHeaderByName('NoHeader','');
	
	$pdf->Output($school_name.'電子繳費單.pdf', 'I');
	
}else{
	//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	$x = '<style type="text/css" media="print">.print_hide {display:none;}</style>
		  <style type="text/css">.print_hide {text-align:right;}</style>';
	$x .= '<div class="print_hide"><input type="button" name="print_btn" value="'.$Lang['Btn']['Print'].'" onclick="window.print();" /></div>';
	
	for($i=0;$i<$record_count;$i++){
		if($i > 0){
			$x .= '<div style="page-break-after:always"></div>';
			$x .= strip_style_tag($records[$i]['BillingContent']);
		}else{
			$x .= $records[$i]['BillingContent'];
		}
	}
	
	echo $x;
	
	//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
}


intranet_closedb();
?>