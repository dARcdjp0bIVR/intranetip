<?php 
// Editing by  
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT.'home/library_sys/management/circulation/User.php');
include_once($PATH_WRT_ROOT.'home/library_sys/management/circulation/BookManager.php');
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']
 || !$sys_custom['eLibraryPlus']['ePaymentOverduePayment'] || !$plugin['library_management_system']) {
	intranet_closedb();
	exit();
}

$liblms = new liblms();
$lpayment = new libpayment();

$uID = isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])? $_SESSION['LIBMS']['CirculationManagemnt']['UID'] : $_SESSION['UserID'];
$User = new User($uID, $liblms);

$OverDueLogID = $_REQUEST['OverDueLogID'];

$sql = "SELECT 
			lu.ClassName, 
			lu.ClassNumber, 
			lu.EnglishName, 
			lu.ChineseName,
			lu.UserID,
			ol.OverDueLogID,
			ol.Payment as Fine,
			ol.PaymentReceived as Paid, 
			if(ol.DaysCount is NULL, 'LOST', ol.DaysCount) As LostOrDays,
			b.BookTitle 
		FROM LIBMS_OVERDUE_LOG as ol 
		INNER JOIN LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
		INNER JOIN LIBMS_BOOK as b ON b.BookID=bl.BookID 
		INNER JOIN LIBMS_USER as lu ON lu.UserID=bl.UserID 
		WHERE ol.RecordStatus='SETTLED' AND ol.IsWaived=0 AND ol.PaymentLogID IS NOT NULL 
			 AND ol.OverDueLogID IN ('".implode("','",(array)$OverDueLogID)."') 
		ORDER BY lu.ClassName,lu.ClassNumber+0";
$records = $liblms->returnArray($sql);
$record_count = count($records);

$result = array();
for($i=0;$i<$record_count;$i++){
	$overdue_log_id = $records[$i]['OverDueLogID'];
	$pay_amount = $records[$i]['Paid'];
	
	$result[$overdue_log_id]=$User->cancel_overdue($pay_amount, $overdue_log_id, true);
}

if(!in_array(false,$result)){
	echo '1';
}else{
	echo '0';
}

intranet_closedb();
?>