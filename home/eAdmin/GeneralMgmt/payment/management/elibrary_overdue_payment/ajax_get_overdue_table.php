<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']
 || !$sys_custom['eLibraryPlus']['ePaymentOverduePayment'] || !$plugin['library_management_system']) {
 	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lclass = new libclass();
$liblms = new liblms();
$lpayment = new libpayment();

$TargetType = $_REQUEST['TargetType'];
$TargetID = $_REQUEST['TargetID'];

$StudentIdAry = array();
if($TargetType=='student'){
	$StudentIdAry = $TargetID;
}else if($TargetType == 'class'){
	$ClassIdAry = $TargetID;
	$StudentAry = $lclass->getStudentListByClassID($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}else if($TargetType == 'form'){
	$FormIdAry = $TargetID;
	$ClassIdAry = array();
	
	for($i=0;$i<sizeof($FormIdAry);$i++)
	{
		$ClassList = $lclass->returnClassListByLevel($FormIdAry[$i]);
		for($j=0;$j<sizeof($ClassList);$j++) {
			$ClassIdAry[] = $ClassList[$j][0];
		}
	}
	
	$StudentAry = $lclass->getStudentListByClassID($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}

$sort_field_ary = array('lu.ClassName','lu.ClassNumber',Get_Lang_Selection('lu.ChineseName','lu.EnglishName'),'b.BookTitle','LostOrDays','BookFine','FineDate');
$sortorder = $sortorder == 1? 0:1;
$sort_order = $sortorder == 1? 'ASC': 'DESC';
$sort_css = $sortorder == 1? 'sort_asc' : 'sort_dec';
$sort_field = $sort_field_ary[$sortfield];
if($sort_field == '' || $sort_field == $sort_field_ary[0]) {
	$sortfield = 0;
	$sort_cond = $sort_field_ary[0]." $sort_order,lu.ClassNumber ASC";
}else{
	$sort_cond = "$sort_field $sort_order";
}


$sql = "SELECT 
			lu.ClassName, 
			lu.ClassNumber, 
			lu.EnglishName, 
			lu.ChineseName,
			(lu.balance*-1) as Fine,
			lu.UserID,
			ol.OverDueLogID,
			if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as BookFine, 
			if(ol.DaysCount is NULL, 'LOST', ol.DaysCount) As LostOrDays,
			b.BookTitle,
			ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO,
			ol.DateCreated as FineDate 
		FROM LIBMS_OVERDUE_LOG as ol 
		INNER JOIN LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
		INNER JOIN LIBMS_BOOK as b ON b.BookID=bl.BookID 
		INNER JOIN LIBMS_USER as lu ON lu.UserID=bl.UserID 
		LEFT JOIN LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
		WHERE lu.balance<0 AND ol.RecordStatus='OUTSTANDING' AND ol.IsWaived=0	
			 AND bl.UserID IN ('".implode("','",$StudentIdAry)."') 
		ORDER BY $sort_cond";

$records = $liblms->returnArray($sql);
$record_count = count($records);


$x = '';
$x.= '<table class="common_table_list_v30">'."\n";
	$x.='<thead>';
	$x.= '<tr>';
		$x.='<th class="num_check">#</th>';
		$css = $sortfield == 0? $sort_css : 'tabletoplink';
		$x.='<th style="width:10%;"><a class="'.$css.'" href="javascript:loadOverdueTable(0,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['libms']['bookmanagement']['ClassName'].'</a></th>';
		$css = $sortfield == 1? $sort_css : 'tabletoplink';
		$x.='<th style="width:10%;"><a class="'.$css.'" href="javascript:loadOverdueTable(1,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['libms']['bookmanagement']['ClassNumber'].'</a></th>';
		$css = $sortfield == 2? $sort_css : 'tabletoplink';
		$x.='<th style="width:20%;"><a class="'.$css.'" href="javascript:loadOverdueTable(2,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang["libms"]["book"]["user_name"].'</a></th>';
		$css = $sortfield == 3? $sort_css : 'tabletoplink';
		$x.='<th style="width:30%;"><a class="'.$css.'" href="javascript:loadOverdueTable(3,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['libms']['bookmanagement']['bookTitle'].'</a></th>';
		$css = $sortfield == 4? $sort_css : 'tabletoplink';
		$x.='<th style="width:10%;"><a class="'.$css.'" href="javascript:loadOverdueTable(4,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['libms']['CirculationManagement']['overdue_type'] .'</a></th>';
		$css = $sortfield == 5? $sort_css : 'tabletoplink';
		$x.='<th style="width:10%;"><a class="'.$css.'" href="javascript:loadOverdueTable(5,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang["libms"]["CirculationManagement"]["payment"].'</a></th>';
		$css = $sortfield == 6? $sort_css : 'tabletoplink';
		$x.='<th style="width:10%;"><a class="'.$css.'" href="javascript:loadOverdueTable(6,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang["libms"]["CirculationManagement"]["FineDate"].'</a></th>';
		
		$x.='<th class="num_check"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="Set_Checkbox_Value(\'OverDueLogID[]\',this.checked)" /></th>';
	$x.= '</tr>'."\n";
	$x.='</thead>';
	$x.='<tbody>';
	
if($record_count == 0){
	$x.='<tr>';
		$x.='<td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
	$x.='</tr>'."\n";
}

for($i=0;$i<$record_count;$i++) {
	
	$display_name = Get_Lang_Selection($records[$i]['ChineseName'],$records[$i]['EnglishName']);
	$overdue_type = $records[$i]['LostOrDays'] == 'LOST'? $Lang["libms"]["book_status"]["LOST"] : str_replace('!','',$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"]).'&nbsp;'.$records[$i]['LostOrDays'].'&nbsp;'.$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"];
	$fine = $lpayment->getWebDisplayAmountFormat($records[$i]['BookFine']);
	$ACNO = $records[$i]['ACNO'];
	
	$x .= '<tr>';
		$x.='<td>'.($i+1).'</td>';
		$x.='<td>'.$records[$i]['ClassName'].'</td>';
		$x.='<td>'.$records[$i]['ClassNumber'].'</td>';
		$x.='<td>'.$display_name.'</td>';
		$x.='<td>'.$records[$i]['BookTitle'].($ACNO!=''?' ['.$ACNO.']':'').'</td>';
		$x.='<td>'.$overdue_type.'</td>';
		$x.='<td>'.$fine.'</td>';
		$x.='<td>'.$records[$i]['FineDate'].'</td>';
		$x.='<td><input type="checkbox" id="OverDueLogID['.$i.']" name="OverDueLogID[]" value="'.$records[$i]['OverDueLogID'].'" onclick="unset_checkall(this, document.getElementById(\'MainForm\'))" /></td>';
	$x .= '</tr>'."\n";
}

$x .= '</tbody>';
$x .= '</table>'."\n";

$x.='<div id="DivPayButton" class="edit_bottom_v30">'."\n";
	$x.= '<p class="spacer"></p>'."\n";
	if($record_count > 0){
		$x.= $linterface->GET_ACTION_BTN($Lang['eNotice']['Pay'], "button", "paySubmitForm();", "payBtn");
	}
	$x.= '<p class="spacer"></p>'."\n";
$x.='</div>'."\n";

echo $x;

intranet_closedb();
?>