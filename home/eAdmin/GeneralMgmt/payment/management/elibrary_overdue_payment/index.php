<?php
// Editing by 
/*
 * 2014-04-25 (Carlos): Changed UI and added print & csv function
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']
 || !$sys_custom['eLibraryPlus']['ePaymentOverduePayment'] || !$plugin['library_management_system']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$lpayment = new libpayment();
$lclass = new libclass();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "eLibraryPlusOverduePayment";

$linterface = new interface_html();

if(!isset($sortfield)){
	$sortfield = 0;
}

if(!isset($sortorder)){
	$sortorder = 0;
}

if(!isset($TargetRecordType)) {
	$TargetRecordType = 1; // 1: Outstanding ; 2: Payment settled 
}

$TAGS_OBJ[] = array($Lang['ePayment']['eLibPlusOverduePayment'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

?>
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'class'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'ajax_load_target.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else if(selectedTargetType == 'student'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'ajax_load_target.php',
			{
				'target':'student',
				'academicYearId':academicYearId,
				'fieldId':'studentTargetID',
				'fieldName':'studentTargetID[]',
				'onchange':'getStudentSelection()',
				'divStudentSelection':'DivStudentSelection'
			},
			function(data){
				$('#selectAllTargetBtn').hide();
				$('#DivSelectAllRemark').show();
				getStudentSelection();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function getStudentSelection()
{
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById('studentTargetID');
	for(var i=0;i<yearClassObj.options.length;i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#DivStudentSelection').html(loadingImg);
	$.post(
		'ajax_load_target.php',
		{
			'target':'student2ndLayer',
			'academicYearId':academicYearId,
			'fieldId':'studentTargetID',
			'fieldName':'studentTargetID[]',
			'studentFieldName':'TargetID[]',
			'studentFieldId':'TargetID',
			'YearClassID[]':selectedYearClassId 
		},
		function(data){
			$('#DivStudentSelection').html(data);
		}
	);
}
/*
function loadOverdueTable(sortfield,sortorder)
{
	$('#DivMain').html(loadingImg);
	
	$.post(
		'ajax_get_overdue_table.php',
		{
			'TargetType':$('#TargetType').val(),
			'TargetID[]':$('#TargetID').val(),
			'sortfield':sortfield,
			'sortorder':sortorder
		},
		function(ReturnData){
			$('#DivMain').html(ReturnData);
			$('#sortfield').val(sortfield);
			$('#sortorder').val(sortorder);
		}
	);
}

function loadPaidOverdueTable(sortfield,sortorder)
{
	$('#DivMain').html(loadingImg);
	
	$.post(
		'ajax_get_paid_overdue_table.php',
		{
			'TargetType':$('#TargetType').val(),
			'TargetID[]':$('#TargetID').val(),
			'sortfield':sortfield,
			'sortorder':sortorder
		},
		function(ReturnData){
			$('#DivMain').html(ReturnData);
			$('#sortfield').val(sortfield);
			$('#sortorder').val(sortorder);
		}
	);
}
*/
function loadTable(sortfield,sortorder)
{
	$('#DivMain').html(loadingImg);
	
	$.post(
		'get_records.php',
		{
			'StartDate':$('#StartDate').val(),
			'EndDate':$('#EndDate').val(),
			'PenaltyType':$('input[name=PenaltyType]:checked').val(),
			'PaymentStatus':$('input[name=PaymentStatus]:checked').val(),
			'PaymentMethod':$('input[name=PaymentMethod]:checked').val(),
			'TargetType':$('#TargetType').val(),
			'TargetID[]':$('#TargetID').val(),
			'sortfield':sortfield,
			'sortorder':sortorder
		},
		function(ReturnData){
			$('#DivMain').html(ReturnData);
			$('#sortfield').val(sortfield);
			$('#sortorder').val(sortorder);
		}
	);
}

function checkForm(format)
{
	var StartDateObj = document.getElementById('StartDate');
	var EndDateObj = document.getElementById('EndDate');
	var TargetIDObj = $('#TargetID');
	var IsValid = true;
	
	if(!check_date_without_return_msg(StartDateObj)){
		$('#WarnDateRange span').html('<?=$Lang['General']['InvalidDateFormat']?>');
		$('#WarnDateRange').show();
		StartDateObj.focus();
		IsValid = false;
	}else{
		$('#WarnDateRange').hide();
	}
	
	if(IsValid){
		if(!check_date_without_return_msg(EndDateObj)){
			$('#WarnDateRange span').html('<?=$Lang['General']['InvalidDateFormat']?>');
			$('#WarnDateRange').show();
			EndDateObj.focus();
			IsValid = false;
		}else{
			$('#WarnDateRange').hide();
		}
	}
	
	if(IsValid){
		if(StartDateObj.value > EndDateObj.value){
			$('#WarnDateRange span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			$('#WarnDateRange').show();
			IsValid = false;
		}else{
			$('#WarnDateRange').hide();
		}
	}
	
	$('#WarnTargetID').hide();
	if(TargetIDObj.length == 0){
		$('#WarnTargetID').show();
		$('#TargetType').focus();
		IsValid = false;
	}
	if(!TargetIDObj.val() || TargetIDObj.val().length == 0){
		$('#WarnTargetID').show();
		$('#TargetType').focus();
		IsValid = false
	}
	
	if(IsValid){
		if(format == 'print' || format=='csv'){
			var formObj = $('#form1');
			$('input#format').val(format);
			formObj.attr('method','POST');
			formObj.attr('action','get_records.php');
			formObj.attr('target',format=='print'?'_blank':'');
			formObj.submit();
		}else{
			loadTable($('#sortfield').val(),$('#sortorder').val());
		}
	}
}

function resetForm()
{
	$('#TargetType').val('');
	$('input[name=PenaltyType]').val('');
	$('input[name=PaymentStatus]').val('');
	$('input[name=PaymentMethod]').val('');
	targetTypeChanged(document.getElementById('TargetType'));
	$('#DivMain').html('');
}

function paySubmitForm()
{
	var penaltyTypeAry = $('input[name="HiddenPenaltyType[]"]');
	var overdueLogAry = $('input[name="OverDueLogID[]"]');
	var overdueLogIdAry = [];
	
	for(var i=0;i<overdueLogAry.length;i++)
	{
		if(overdueLogAry.get(i).checked && penaltyTypeAry.get(i).value == 'OUTSTANDING'){
			overdueLogIdAry.push(overdueLogAry.get(i).value);
		}
	}
	
	if(overdueLogIdAry.length == 0){
		alert('<?=$Lang['ePayment']['LibraryRequestSelectOutstandingRecords']?>');
		return false;
	}
	
	Block_Element('DivMain');
	$('#DivMain').load(
		'ajax_get_confirm_overdue_table.php',
		{
			'OverDueLogID[]':overdueLogIdAry
		},
		function(ReturnData){
			UnBlock_Element('DivMain');
		}
	);
}

function undoSubmitForm()
{
	var penaltyTypeAry = $('input[name="HiddenPenaltyType[]"]');
	var overdueLogAry = $('input[name="OverDueLogID[]"]');
	var overdueLogIdAry = [];
	
	for(var i=0;i<overdueLogAry.length;i++)
	{
		if(overdueLogAry.get(i).checked && penaltyTypeAry.get(i).value == 'SETTLED'){
			overdueLogIdAry.push(overdueLogAry.get(i).value);
		}
	}
	
	if(overdueLogIdAry.length == 0){
		alert('<?=$Lang['ePayment']['LibraryRequestSelectSettledRecords']?>');
		return false;
	}
	
	if(confirm('<?=$i_Payment_alert_undopay?>')){
		Block_Document();
		$.post(
			'ajax_undo_payment.php',
			{
				'OverDueLogID[]':overdueLogIdAry
			},
			function(ReturnMsg){
				if(ReturnMsg == '1'){
					Get_Return_Message('<?=$Lang['ePayment']['PayUndoSuccess']?>');
				}else{
					Get_Return_Message('<?=$Lang['ePayment']['PayUndoUnsuccess']?>');
				}
				UnBlock_Document();
				loadTable($('#sortfield').val(),$('#sortorder').val());
			}
		);
	}
}

function payConfirmSubmit()
{
	var overdueLogAry = $('input[name="OverDueLogID[]"]');
	var overdueLogIdAry = [];
	
	if(confirm('<?=$i_Payment_alert_forcepayall?>')){
		Block_Document();
		for(var i=0;i<overdueLogAry.length;i++)
		{
			overdueLogIdAry.push(overdueLogAry[i].value);
		}
		$.post(
			'ajax_pay_overdue.php',
			{
				'OverDueLogID[]':overdueLogIdAry
			},
			function(ReturnMsg){
				if(ReturnMsg == '1'){
					Get_Return_Message('<?=$Lang['ePayment']['PayProcessSuccess']?>');
				}else{
					Get_Return_Message('<?=$Lang['ePayment']['PayProcessUnsuccess']?>');
				}
				UnBlock_Document();
				loadTable($('#sortfield').val(),$('#sortorder').val());
			}
		);
	}
}

function cancelPayment()
{
	loadTable($('#sortfield').val(),$('#sortorder').val());
}

</script>
<form id="form1" name="form1" method="post" action="" onsubmit="return false;">
<table width="100%">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td>
						<table  class="form_table_v30">
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['ePayment']['LibraryFineDate']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->GET_DATE_PICKER("StartDate",date("Y-m-d"))." ~ ".$linterface->GET_DATE_PICKER("EndDate",date("Y-m-d"))?>
									<?=$linterface->Get_Form_Warning_Msg('WarnDateRange', '', 'WarnDateRange', false);?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['CommonChoose']['SelectTarget']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table border="0">
										<tr>
											<td valign="top">
												<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
													<option value="" selected="selected">-- <?=$Lang['General']['PleaseSelect']?> --</option>
													<option value="form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></option>
													<option value="class"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></option>
													<option value="student"><?=$Lang['Identity']['Student']?></option>
												</select>
											</td>
											<td>
												<span id='DivRankTargetDetail'></span>
												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
											</td>
										</tr>
									</table>
									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID', false);?>
									<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['ePayment']['LibraryPenaltyType']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->Get_Radio_Button("PenaltyTypeAll", "PenaltyType", "", 1, "", $Lang['General']['All'], "",0)?>
									<?=$linterface->Get_Radio_Button("PenaltyTypeY", "PenaltyType", "OVERDUE", 0, "", $Lang['ePayment']['LibraryOverdueFine'], "",0)?>
									<?=$linterface->Get_Radio_Button("PenaltyTypeN", "PenaltyType", "LOST", 0, "", $Lang['ePayment']['LibraryLostBook'], "",0)?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['ePayment']['LibraryPaymentStatus']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<!--
									<select name="TargetRecordType" id="TargetRecordType">
										<option value="1" selected="selected"><?=$Lang['ePayment']['LibraryOutstanding']?></option>
										<option value="2"><?=$Lang['ePayment']['LibraryPaymentSettled']?></option>
									</select>
									-->
									<?=$linterface->Get_Radio_Button("PaymentStatusAll", "PaymentStatus", "", 1, "", $Lang['General']['All'], "",0)?>
									<?=$linterface->Get_Radio_Button("PaymentStatusOutstanding", "PaymentStatus", "OUTSTANDING", 0, "", $Lang['ePayment']['LibraryOutstanding'], "",0)?>
									<?=$linterface->Get_Radio_Button("PaymentStatusSettled", "PaymentStatus", "SETTLED", 0, "", $Lang['ePayment']['LibraryPaymentSettled'], "",0)?>
									<?=$linterface->Get_Radio_Button("PaymentStatusWaived", "PaymentStatus", "WAIVED", 0, "", $Lang['ePayment']['LibraryWaived'], "",0)?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['ePayment']['LibraryPaymentMethod']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->Get_Radio_Button("PaymentMethodAll", "PaymentMethod", "", 1, "", $Lang['General']['All'], "",0)?>
									<?=$linterface->Get_Radio_Button("PaymentMethodePayment", "PaymentMethod", "ePayment", 0, "", $Lang['ePayment']['LibraryByePayment'], "",0)?>
									<?=$linterface->Get_Radio_Button("PaymentMethodeLibraryPlus", "PaymentMethod", "eLibraryPlus", 0, "", $Lang['ePayment']['LibraryByeLibraryPlus'], "",0)?>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
							<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm('web');", "submitBtn").'&nbsp;'?>
									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button", "resetForm();", "resetBtn")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="format" id="format" value="" />
<input type="hidden" name="sortfield" id="sortfield" value="<?=$sortfield?>" />
<input type="hidden" name="sortorder" id="sortorder" value="<?=$sortorder?>" />
</form>
<br />
<form name="MainForm" id="MainForm" action="" method="post">
<div id="DivMain"></div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>