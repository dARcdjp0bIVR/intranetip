<?php
// Editing by 
/*
 * 2018-12-13 (Cameron): add filter LIBMS_OVERDUE_LOG.RecordStatus<>'DELETE' [case #L154330]
 *  
 * 2014-04-25 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']
 || !$sys_custom['eLibraryPlus']['ePaymentOverduePayment'] || !$plugin['library_management_system']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();
$lclass = new libclass();
$liblms = new liblms();

$format = $_REQUEST['format'];

$TargetType = $_REQUEST['TargetType'];
$TargetID = $_REQUEST['TargetID'];

$StudentIdAry = array();
if($TargetType=='student'){
	$StudentIdAry = $TargetID;
}else if($TargetType == 'class'){
	$ClassIdAry = $TargetID;
	//$StudentAry = $lclass->getStudentListByClassID($ClassIdAry);
	$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}else if($TargetType == 'form'){
	$FormIdAry = $TargetID;
	$ClassIdAry = array();
	
	for($i=0;$i<sizeof($FormIdAry);$i++)
	{
		$ClassList = $lclass->returnClassListByLevel($FormIdAry[$i]);
		for($j=0;$j<sizeof($ClassList);$j++) {
			$ClassIdAry[] = $ClassList[$j][0];
		}
	}
	
	//$StudentAry = $lclass->getStudentListByClassID($ClassIdAry);
	$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
}

$sort_field_ary = array('lu.ClassName','lu.ClassNumber',Get_Lang_Selection('lu.ChineseName','lu.EnglishName'),'b.BookTitle','LostOrDays','BookFine','OverdueDays','FineDate','PaymentStatus','PaymentReceived','PaymentTime');
$sortorder = $sortorder == 1? 0:1;
$sort_order = $sortorder == 1? 'ASC': 'DESC';
$sort_css = $sortorder == 1? 'sort_asc' : 'sort_dec';
$sort_field = $sort_field_ary[$sortfield];
if($sort_field == '' || $sort_field == $sort_field_ary[0]) {
	$sortfield = 0;
	$sort_cond = $sort_field_ary[0]." $sort_order,lu.ClassNumber ASC";
}else{
	$sort_cond = "$sort_field $sort_order";
}

$date_cond = " AND ol.DateCreated >= '$StartDate 00:00:00' AND ol.DateCreated <= '$EndDate 23:59:59' ";

if($PenaltyType == "OVERDUE"){
	$penalty_type_cond = " AND ol.DaysCount IS NOT NULL ";
}else if($PenaltyType == "LOST"){
	$penalty_type_cond = " AND ol.DaysCount IS NULL ";
}else{
	$penalty_type_cond = "";
}

if($PaymentStatus == "OUTSTANDING"){
	$payment_status_cond = " AND ol.RecordStatus='OUTSTANDING' ";
}else if($PaymentStatus == "SETTLED"){
	$payment_status_cond = " AND ol.RecordStatus='SETTLED' ";
}else if($PaymentStatus == "WAIVED"){
	$payment_status_cond = " AND ol.RecordStatus='WAIVED' ";
}else {
	$payment_status_cond = " AND ol.RecordStatus<>'DELETE' ";
}

if($PaymentMethod == "ePayment"){
	$payment_method_cond = " AND ol.PaymentMethod='ePayment' ";
}else if($PaymentMethod == "eLibraryPlus"){
	$payment_method_cond = " AND (ol.PaymentMethod<>'ePayment' OR ol.PaymentMethod IS NULL) ";
}else{
	$payment_method_cond = "";
}

$sql = "SELECT 
			lu.ClassName, 
			lu.ClassNumber, 
			lu.EnglishName, 
			lu.ChineseName,
			(lu.balance*-1) as Fine,
			lu.UserID,
			ol.OverDueLogID,
			if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as BookFine, 
			if(ol.DaysCount is NULL, 'LOST', ol.DaysCount) As LostOrDays,
			b.BookTitle,
			ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO,
			ol.DateCreated as FineDate,
			if(ol.DaysCount is NULL, '', ol.DaysCount) as OverdueDays,
			ol.RecordStatus as PaymentStatus,
			if(ol.PaymentReceived is null,0,ol.PaymentReceived) as PaymentReceived,
			IF(ol.RecordStatus='OUTSTANDING','',ol.DateModified) as PaymentTime,
			ol.IsWaived 
		FROM LIBMS_OVERDUE_LOG as ol 
		INNER JOIN LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
		INNER JOIN LIBMS_BOOK as b ON b.BookID=bl.BookID 
		INNER JOIN LIBMS_USER as lu ON lu.UserID=bl.UserID 
		LEFT JOIN LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
		WHERE /*lu.balance<0 AND*/ bl.UserID IN ('".implode("','",$StudentIdAry)."') 
			$date_cond $penalty_type_cond $payment_status_cond $payment_method_cond
		ORDER BY $sort_cond";

$records = $liblms->returnArray($sql);
$record_count = count($records);

if($format == 'csv'){
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	$header = array(
				'#',
				$Lang['General']['Class'],
				$Lang['General']['ClassNumber'],
				$Lang['ePayment']['LibraryName'],
				$Lang['ePayment']['LibraryBookTitle'],
				$Lang['ePayment']['LibraryPenaltyType'],
				$Lang['ePayment']['LibraryFine'],
				$Lang['ePayment']['LibraryOverdueDays'],
				$Lang['ePayment']['LibraryFineDate'],
				$Lang['ePayment']['LibraryPaymentStatus'],
				$Lang['ePayment']['LibraryPaid'],
				$Lang['ePayment']['LibraryPaidOn']
			);
	$rows = array();
	
	if($record_count == 0){
		$row = array($i_no_record_searched_msg);
		for($j=1;$j<12;$j++){
			$row[] = '';
		}
		$rows[] = $row;
	}
}


$x = '';

if($format != 'print' && $format != 'csv'){
	$x .= '<div class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:checkForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
				<a href="javascript:checkForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
			</div>
			<br style="clear:both" />
		  </div>';
}

if($format == 'print'){
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
			</tr>
			</table>';
			
	$x .= '<table width="100%" align="center" border="0">
			<tr>
				<td align="center"><h2>'.$Lang['ePayment']['eLibPlusOverduePayment'].'</h2></td>
			</tr>
			</table>';
}

if($format != 'csv')
{
	$x.='<table width="100%" align="center" border="0">
			<tr>
				<td align="right">'.$Lang['General']['Date'].': '.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</td>
			</tr>
		</table>';
	
	$x.= '<table class="common_table_list_v30'.($format=='print'?' view_table_list_v30':'').'">'."\n";
		$x.='<thead>';
		$x.= '<tr>';
			$x.='<th class="num_check">#</th>';
			$css = $sortfield == 0? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['General']['Class']:'<a class="'.$css.'" href="javascript:loadTable(0,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['General']['Class'].'</a>').'</th>';
			$css = $sortfield == 1? $sort_css : 'tabletoplink';
			$x.='<th style="width:5%;">'.($format=='print'?$Lang['General']['ClassNumber']:'<a class="'.$css.'" href="javascript:loadTable(1,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['General']['ClassNumber'].'</a>').'</th>';
			$css = $sortfield == 2? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryName']:'<a class="'.$css.'" href="javascript:loadTable(2,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryName'].'</a>').'</th>';
			$css = $sortfield == 3? $sort_css : 'tabletoplink';
			$x.='<th style="width:20%;">'.($format=='print'?$Lang['ePayment']['LibraryBookTitle']:'<a class="'.$css.'" href="javascript:loadTable(3,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryBookTitle'].'</a>').'</th>';
			$css = $sortfield == 4? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryPenaltyType']:'<a class="'.$css.'" href="javascript:loadTable(4,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryPenaltyType'] .'</a>').'</th>';
			$css = $sortfield == 5? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryFine']:'<a class="'.$css.'" href="javascript:loadTable(5,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryFine'].'</a>').'</th>';
			$css = $sortfield == 6? $sort_css : 'tabletoplink';
			$x.='<th style="width:5%;">'.($format=='print'?$Lang['ePayment']['LibraryOverdueDays']:'<a class="'.$css.'" href="javascript:loadTable(6,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryOverdueDays'].'</a>').'</th>';
			$css = $sortfield == 7? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryFineDate']:'<a class="'.$css.'" href="javascript:loadTable(7,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryFineDate'].'</a>').'</th>';
			$css = $sortfield == 8? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryPaymentStatus']:'<a class="'.$css.'" href="javascript:loadTable(8,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryPaymentStatus'].'</a>').'</th>';
			$css = $sortfield == 9? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryPaid']:'<a class="'.$css.'" href="javascript:loadTable(9,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryPaid'].'</a>').'</th>';
			$css = $sortfield == 10? $sort_css : 'tabletoplink';
			$x.='<th style="width:10%;">'.($format=='print'?$Lang['ePayment']['LibraryPaidOn']:'<a class="'.$css.'" href="javascript:loadTable(10,'.$sortorder.');" onmouseover="window.status=\'\';return true;" onmouseout="window.status=\'\';return true;">'.$Lang['ePayment']['LibraryPaidOn'].'</a>').'</th>';
		if($format != 'print'){	
			$x.='<th class="num_check"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="Set_Checkbox_Value(\'OverDueLogID[]\',this.checked)" /></th>';
		}
		$x.= '</tr>'."\n";
		$x.='</thead>';
		$x.='<tbody>';
		
	if($record_count == 0){
		$x.='<tr>';
			$x.='<td colspan="13" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x.='</tr>'."\n";
	}
}

$outstanding_count = 0;
$settled_count = 0;
for($i=0;$i<$record_count;$i++) {
	
	$display_name = Get_Lang_Selection($records[$i]['ChineseName'],$records[$i]['EnglishName']);
	$overdue_type = $records[$i]['LostOrDays'] == 'LOST'? $Lang['ePayment']['LibraryLost'] : $Lang['ePayment']['LibraryOverdue'].' '.$records[$i]['LostOrDays'].' '.$Lang['ePayment']['LibraryOverdueDay'];
	$fine = $lpayment->getWebDisplayAmountFormat($records[$i]['BookFine']);
	$ACNO = $records[$i]['ACNO'];
	
	if($records[$i]['PaymentStatus'] == 'WAIVED'){
		$payment_status = $Lang['ePayment']['LibraryWaived'];
	}else if($records[$i]['PaymentStatus']=='OUTSTANDING'){
		$outstanding_count++;
		$payment_status = $Lang['ePayment']['LibraryOutstanding'];
	}else if($records[$i]['PaymentStatus']=='SETTLED'){
		$settled_count++;
		$payment_status = $Lang['ePayment']['LibraryPaymentSettled'];
	}else{
		$payment_status = '';
	}
	
	if($format != 'csv')
	{
		$x .= '<tr>';
			$x.='<td>'.($i+1).'</td>';
			$x.='<td>'.$records[$i]['ClassName'].'</td>';
			$x.='<td>'.$records[$i]['ClassNumber'].'</td>';
			$x.='<td>'.$display_name.'</td>';
			$x.='<td>'.$records[$i]['BookTitle'].($ACNO!=''?' ['.$ACNO.']':'').'</td>';
			$x.='<td>'.$overdue_type.'</td>';
			$x.='<td>'.$fine.'</td>';
			$x.='<td>'.Get_String_Display($records[$i]['OverdueDays'],1).'</td>';
			$x.='<td>'.$records[$i]['FineDate'].'</td>';
			$x.='<td>'.$payment_status.'</td>';
			$x.='<td>'.$lpayment->getWebDisplayAmountFormat($records[$i]['PaymentReceived']).'</td>';
			$x.='<td>'.Get_String_Display($records[$i]['PaymentTime'],1).'</td>';
		if($format != 'print'){	
			$x.='<td>';
				$x.= '<input type="hidden" id="HiddenPenaltyType['.$i.']" name="HiddenPenaltyType[]" value="'.$records[$i]['PaymentStatus'].'" />';
				if($records[$i]['IsWaived']=='1'){
					$x .= '&nbsp;';
				}else{
					$x.='<input type="checkbox" id="OverDueLogID['.$i.']" name="OverDueLogID[]" value="'.$records[$i]['OverDueLogID'].'" onclick="unset_checkall(this, document.getElementById(\'MainForm\'))" />';
				}
			$x.='</td>';
		}
		$x .= '</tr>'."\n";
	}
	
	if($format == 'csv'){
		$row = array(
				($i+1),
				$records[$i]['ClassName'],
				$records[$i]['ClassNumber'],
				$display_name,
				$records[$i]['BookTitle'].($ACNO!=''?' ['.$ACNO.']':''),
				$overdue_type,
				$fine,
				Get_String_Display($records[$i]['OverdueDays'],1),
				$records[$i]['FineDate'],
				$payment_status,
				$lpayment->getWebDisplayAmountFormat($records[$i]['PaymentReceived']),
				Get_String_Display($records[$i]['PaymentTime'],1)
				);
		$rows[] = $row;
	}
}

if($format != 'csv'){
	$x .= '</tbody>';
	$x .= '</table>'."\n";
}

if($format == 'print'){
	$x .= '<br />'."\n";
}

if($format != 'print' && $format != 'csv'){
	$x.='<div id="DivPayButton" class="edit_bottom_v30">'."\n";
		$x.= '<p class="spacer"></p>'."\n";
		if($outstanding_count > 0){
			$x.= $linterface->GET_ACTION_BTN($Lang['eNotice']['Pay'], "button", "paySubmitForm();", "payBtn").'&nbsp;';
		}
		if($settled_count > 0){
			$x.= $linterface->GET_ACTION_BTN($button_undo, "button", "undoSubmitForm();", "undoBtn").'&nbsp;';
		}
		$x.= '<p class="spacer"></p>'."\n";
	$x.='</div>'."\n";
}

if($format == 'csv'){
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE(strip_tags($Lang['ePayment']['eLibPlusOverduePayment']).".csv", $exportContent);
}else{
	if($format=='print'){
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	}
	echo $x;
	if($format == 'print'){
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
}

intranet_closedb();
?>