<?php 
// Editing by  
/*
 * 2014-04-25 (Carlos): Added fields [OverdueDays], [PaymentStatus], [PaymentReceived], [PaymentTime] 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']
 || !$sys_custom['eLibraryPlus']['ePaymentOverduePayment'] || !$plugin['library_management_system']) {
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$liblms = new liblms();
$lpayment = new libpayment();

$OverDueLogID = $_REQUEST['OverDueLogID'];

$sql = "SELECT 
			lu.ClassName, 
			lu.ClassNumber, 
			lu.EnglishName, 
			lu.ChineseName,
			(lu.balance*-1) as Fine,
			lu.UserID,
			ol.OverDueLogID,
			if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as BookFine, 
			if(ol.DaysCount is NULL, 'LOST', ol.DaysCount) As LostOrDays,
			b.BookTitle,
			ifnull(bu.ACNO, bu.ACNO_BAK) AS ACNO,
			ol.DateCreated as FineDate,
			if(ol.DaysCount is NULL, '', ol.DaysCount) as OverdueDays,
			ol.RecordStatus as PaymentStatus,
			if(ol.PaymentReceived is null,0,ol.PaymentReceived) as PaymentReceived,
			IF(ol.RecordStatus='OUTSTANDING','',ol.DateModified) as PaymentTime,
			ol.IsWaived   
		FROM LIBMS_OVERDUE_LOG as ol 
		INNER JOIN LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
		INNER JOIN LIBMS_BOOK as b ON b.BookID=bl.BookID 
		INNER JOIN LIBMS_USER as lu ON lu.UserID=bl.UserID 
		LEFT JOIN LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
		WHERE /*lu.balance<0 AND*/ ol.RecordStatus='OUTSTANDING' AND ol.IsWaived=0	
			 AND ol.OverDueLogID IN ('".implode("','",$OverDueLogID)."') 
		ORDER BY lu.ClassName,lu.ClassNumber+0";
$records = $liblms->returnArray($sql);
$record_count = count($records);

$userIdAry = Get_Array_By_Key($records,'UserID');
$userIdAry = array_values(array_unique($userIdAry));

$sql = "SELECT 
			StudentID, Balance
		FROM PAYMENT_ACCOUNT 
		WHERE StudentID IN ('".implode("','",$userIdAry)."')";
$balanceRecords = $lpayment->returnResultSet($sql);
$balance_record_count = count($balanceRecords);
$userIdToBalanceAry = array();

for($i=0;$i<$balance_record_count;$i++) {
	$userIdToBalanceAry[$balanceRecords[$i]['StudentID']] = $balanceRecords[$i]['Balance'];
}

$x = '';
$x.= '<table class="common_table_list_v30">'."\n";
	$x.='<thead>';
	$x.= '<tr>';
		$x.='<th class="num_check">#</th>';
		$x.='<th style="width:10%;">'.$Lang['libms']['bookmanagement']['ClassName'].'</th>';
		$x.='<th style="width:5%;">'.$Lang['libms']['bookmanagement']['ClassNumber'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryName'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryBookTitle'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryPenaltyType'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryFine'].'</th>';
		$x.='<th style="width:5%;">'.$Lang['ePayment']['LibraryOverdueDays'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryFineDate'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryPaymentStatus'].'</th>';
		$x.='<th style="width:10%;">'.$Lang['ePayment']['LibraryPaid'].'</th>';
		$x.='<th style="width:5%;">'.$i_Payment_Field_CurrentBalance.'</th>';
		$x.='<th style="width:5%;">'.$i_Payment_Field_BalanceAfterPay.'</th>';
	$x.= '</tr>'."\n";
	$x.='</thead>';
	$x.='<tbody>';
	
if($record_count == 0){
	$x.='<tr>';
		$x.='<td colspan="13" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
	$x.='</tr>'."\n";
}

$can_pay_count = 0;
for($i=0;$i<$record_count;$i++) {
	
	$current_balance = $userIdToBalanceAry[$records[$i]['UserID']];
	
	$display_name = Get_Lang_Selection($records[$i]['ChineseName'],$records[$i]['EnglishName']);
	$overdue_type = $records[$i]['LostOrDays'] == 'LOST'? $Lang["libms"]["book_status"]["LOST"] : $Lang['ePayment']['LibraryOverdue'].'&nbsp;'.$records[$i]['LostOrDays'].'&nbsp;'.$Lang['ePayment']['LibraryOverdueDay'];
	$fine = $lpayment->getWebDisplayAmountFormat($records[$i]['BookFine']);
	$ACNO = $records[$i]['ACNO'];
	
	if($records[$i]['PaymentStatus'] == 'WAIVED'){
		$payment_status = $Lang['ePayment']['LibraryWaived'];
	}else if($records[$i]['PaymentStatus']=='OUTSTANDING'){
		$outstanding_count++;
		$payment_status = $Lang['ePayment']['LibraryOutstanding'];
	}else if($records[$i]['PaymentStatus']=='SETTLED'){
		$settled_count++;
		$payment_status = $Lang['ePayment']['LibraryPaymentSettled'];
	}else{
		$payment_status = '';
	}
	
	$can_pay = true;
	$row_css = '';
	$balance_after = $current_balance - $records[$i]['BookFine'];
	if($balance_after < 0) {
		$can_pay = false;
		$row_css = ' style="color:red;"';
	}else{
		$userIdToBalanceAry[$records[$i]['UserID']] = $balance_after;
	}
	
	$x .= '<tr'.$row_css.'>';
		$x.='<td>'.($i+1).'</td>';
		$x.='<td>'.$records[$i]['ClassName'].'</td>';
		$x.='<td>'.$records[$i]['ClassNumber'].'</td>';
		$x.='<td>'.$display_name.'</td>';
		$x.='<td>'.$records[$i]['BookTitle'].($ACNO!=''?' ['.$ACNO.']':'').'</td>';
		$x.='<td>'.$overdue_type.'</td>';
		$x.='<td>'.$fine.'</td>';
		$x.='<td>'.Get_String_Display($records[$i]['OverdueDays'],1).'</td>';
		$x.='<td>'.$records[$i]['FineDate'].'</td>';
		$x.='<td>'.$payment_status.'</td>';
		$x.='<td>'.$lpayment->getWebDisplayAmountFormat($records[$i]['PaymentReceived']).'</td>';
		$x.='<td>'.$lpayment->getWebDisplayAmountFormat($current_balance).'</td>';
		$x.='<td>';
			$x.=$lpayment->getWebDisplayAmountFormat($balance_after);
			if($can_pay){
				$x.='<input type="hidden" name="OverDueLogID[]" value="'.$records[$i]['OverDueLogID'].'" />';
				$can_pay_count+=1;
			}
		$x.='</td>';
	$x .= '</tr>'."\n";
}

$x .= '</tbody>';
$x .= '</table>'."\n";

$x .= '<br />';
$x .= '<div class="tabletextremark">';
$x .= '<span class="tabletextrequire">*</span>&nbsp;';
$x .= $i_Payment_PaymentItem_Pay_Warning;
$x .= '</div>'."\n";

$x.='<div id="DivPayButton" class="edit_bottom_v30">'."\n";
	$x.= '<p class="spacer"></p>'."\n";
	if($can_pay_count > 0){
		$x.= $linterface->GET_ACTION_BTN($i_Payment_action_proceed_payment, "button", "payConfirmSubmit();", "payConfirmBtn").'&nbsp;';
	}
	$x.= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "cancelPayment();", "cancelPaymentBtn");
	$x.= '<p class="spacer"></p>'."\n";
$x.='</div>'."\n";

echo $x;

intranet_closedb();
?>