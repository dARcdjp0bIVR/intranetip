<?php
// Editing by 
/*
 * 2017-02-21 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - report for checking paid/unpaid payment items for students.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['HartsPreschool']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lclass = new libclass();

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="")
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

# Class Menu #
$select_class = $lclass->getSelectClassID(' name="ClassID[]" id="ClassID" onchange="classSelectionChanged(this);" multiple="multiple" size="10" ', $__selected="", $__DisplaySelect=2, $__AcademicYearID='',$__optionFirst='', $__DisplayClassIDArr='', $__TeachingOnly=0);


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_PaymentItemStatusReport";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemStatusReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';

function loadReport()
{
	$('#TableLayer').html(loadingImg);
	$.post(
		'report.php',
		$('#form1').serialize(),
		function(data){
			$('#TableLayer').html(data);
		}
	);
}

function submitForm(formObj, format)
{
	var from_date_error = $('#DPWL-FromDate').html() != '';
	var to_date_error = $('#DPWL-ToDate').html() != '';
	var class_id_error = $('#ClassID').length > 0 && $('#ClassID option:selected').length == 0;
	var student_id_error = $('#StudentID').length > 0 && $('#StudentID option:selected').length == 0;
	var record_type_error = $('input[name="RecordType[]"]:checked').length == 0;
	
	if(student_id_error){
		$('#StudentSelectionError').show();
		$('#StudentID').focus();
	}else{
		$('#StudentSelectionError').hide();
	}
	
	if(class_id_error){
		$('#ClassSelectionError').show();
		$('#ClassID').focus();
	}else{
		$('#ClassSelectionError').hide();
	}
	
	if(record_type_error){
		$('#RecordTypeError').show();
		$('#RecordType1').focus();
	}else{
		$('#RecordTypeError').hide();
	}
	
	if(from_date_error || to_date_error || class_id_error || student_id_error || record_type_error)
		return false;
	
	$('#format').val(format);
	if(format == 'print' || format == 'csv')
	{
		var formObj = $('#form1');
		$('input#format').val(format);
		formObj.attr('method','POST');
		formObj.attr('action','report.php');
		formObj.attr('target',format=='print'?'_blank':'');
		formObj.submit();
	}else{
		loadReport();
	}
}

function classSelectionChanged(obj)
{
	var selected_vals = [];
	
	for(var i=0;i<obj.options.length;i++){
		if(obj.options[i].selected){
			selected_vals.push(obj.options[i].value);
		}
	}
	
	if(selected_vals.length == 0){
		$('#StudentSelectionRow').hide();
		$('#StudentSelectionLayer').html('');
		return;
	}
	
	$.post(
		'get_student_selection.php',
		{
			'ClassID[]': selected_vals
		},
		function(data){
			$('#StudentSelectionLayer').html(data);
			$('#StudentSelectionRow').show();
		}
	);
}

function SelectAll(obj)
{
	 for (i=0; i<obj.length; i++)
	 {
	      obj.options[i].selected = true;
	 }
}

</script>
<br />
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
<table class="form_table_v30">
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title"><?=$i_Payment_Field_PaymentItem.(Get_Lang_Selection(' ','')).$Lang['General']['StartDate']?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$i_UserClassName?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php 
				$select_all_btn = '&nbsp;'.$linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('ClassID'));classSelectionChanged(document.getElementById('ClassID'));");
				echo $linterface->Get_MultipleSelection_And_SelectAll_Div($select_class, $select_all_btn, 'ClassSelectionLayer'); 
			?> 
			<div id="ClassSelectionError" style="display:none;color:red;"><?=$Lang['General']['JS_warning']['SelectClasses']?></div>
		</td>
	</tr>
	<tr id="StudentSelectionRow" style="display:none;" valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$i_UserStudentName?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php
				$select_all_btn = $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('StudentID'));");
				echo $linterface->Get_MultipleSelection_And_SelectAll_Div('', $select_all_btn, 'StudentSelectionLayer'); 
			?>
			<div id="StudentSelectionError" style="display:none;color:red;"><?=$Lang['General']['JS_warning']['SelectStudent']?></div>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['ePayment']['RecordType']?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<?php
				echo $linterface->Get_Checkbox('RecordType0', 'RecordType[]', '0', 1, $__Class='', $__Display=$Lang['ePayment']['UnpaidPaymentItems'], $__Onclick='', $__Disabled='');
				echo '&nbsp;'.$linterface->Get_Checkbox('RecordType1', 'RecordType[]', '1', 1, $__Class='', $__Display=$Lang['ePayment']['PaidPaymentItems'], $__Onclick='', $__Disabled='');
			?>
			<div id="RecordTypeError" style="display:none;color:red;"><?=$Lang['General']['PleaseSelect'].'&nbsp;'.$Lang['ePayment']['RecordType']?></div>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm(document.form1,'');", "submit_btn").'&nbsp;'?>
		</td>
	</tr>
</table>
<input type="hidden" name="format" id="format" value="" />
</form>
<br />
<div id="TableLayer"></div>
<?
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>