<?php
// Editing by 
/*
 * 2017-02-21 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - report for checking paid/unpaid payment items for students.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['HartsPreschool']) {
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();

$format = $_POST['format'];
if(!in_array($format,array('','web','print','csv'))) $format = '';
$from_date = date("Y-m-d", strtotime($_POST['FromDate']));
$to_date = date("Y-m-d", strtotime($_POST['ToDate']));
$recordTypeAry = $_POST['RecordType'];
$recordTypeAry = array_values(array_intersect($recordTypeAry,array('1','0')));
$studentIdAry = IntegerSafe( $_POST['StudentID'] );
$student_size = count($studentIdAry);

$lpayment->Set_Report_Date_Range_Cookies($from_date, $to_date);

$name_field = getNameFieldByLang2("u.");
$sql = "SELECT 
			u.UserID,u.UserLogin,u.STRN,u.ClassName,u.ClassNumber,$name_field as StudentName 
		FROM INTRANET_USER as u 
		WHERE u.UserID IN (".implode(",",$studentIdAry).") 
		ORDER BY u.ClassName,u.ClassNumber+0";
$studentsAry = $lpayment->returnResultSet($sql);

$sidToRecords = array();
for($i=0;$i<count($studentsAry);$i++){
	$sidToRecords[$studentsAry[$i]['UserID']] = array('StudentInfo'=>$studentsAry[$i],'PaidItems'=>array(),'UnpaidItems'=>array());
}

$sql = "SELECT 
			s.StudentID,
			s.PaymentID,
			s.ItemID,
			c.Name as CategoryName,
			i.Name as ItemName,
			s.Amount,
			s.SubsidyAmount,
			i.StartDate,
			i.EndDate,
			s.RecordStatus,
			s.PaidTime,
			s.Remark,
			CASE s.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val){	
		$sql.= " WHEN '$key' THEN '$val' "; 
	}
	$sql .=	" ELSE '' 
			END as PaymentMethod  
		FROM PAYMENT_PAYMENT_ITEMSTUDENT as s  
		INNER JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		INNER JOIN PAYMENT_PAYMENT_CATEGORY as c ON c.CatID=i.CatID 
		WHERE s.StudentID IN (".implode(",",$studentIdAry).") AND (i.StartDate BETWEEN '$from_date' AND '$to_date') ";
if(!in_array('1',$recordTypeAry)){ // paid is not checked
	$sql .= " AND s.PaidTime is null AND (s.RecordStatus=0 OR s.RecordStatus IS NULL) ";
}
if(!in_array('0',$recordTypeAry)){ // unpaid is not checked
	$sql .= " AND s.PaidTime IS NOT NULL AND s.RecordStatus='1' ";
}
$sql.=" ORDER BY s.StudentID,i.StartDate ";
$records = $lpayment->returnResultSet($sql);
for($i=0;$i<count($records);$i++){
	if($records[$i]['RecordStatus'] == 1){
		$sidToRecords[$records[$i]['StudentID']]['PaidItems'][] = $records[$i];
	}else
	{
		$sidToRecords[$records[$i]['StudentID']]['UnpaidItems'][] = $records[$i];
	}
}


intranet_closedb();

if($format == 'csv'){
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	$header = array();
	$rows = array();
	
	foreach($studentIdAry as $student_id)
	{
		if(!isset($sidToRecords[$student_id])) continue;
		
		$row = array_fill(0,8,'');
		$row[0] = (Get_String_Display($sidToRecords[$student_id]['StudentInfo']['ClassName'],1).'('.$sidToRecords[$student_id]['StudentInfo']['ClassNumber'].') '.Get_String_Display($sidToRecords[$student_id]['StudentInfo']['StudentName'],1));
		$rows[] = $row;
		
		if(in_array('1',$recordTypeAry))
		{
			$paid_ary = $sidToRecords[$student_id]['PaidItems'];
			$row = array_fill(0,8,'');
			$row[0] = $Lang['ePayment']['PaidPaymentItems'];
			
			$row = array(
				'#',
				$i_Payment_Field_PaymentCategory,
				$i_Payment_Field_PaymentItem,
				$Lang['ePayment']['Amount'],
				$i_Payment_Subsidy_Amount,
				$Lang['ePayment']['LibraryPaidOn'],
				$Lang['ePayment']['PaymentRemark'],
				$Lang['ePayment']['PaymentMethod']
			);
			$rows[] = $row;
			
			$total = 0;
			$subsidy_total = 0;
			for($i=0;$i<count($paid_ary);$i++){
				$total += $paid_ary[$i]['Amount'];
				$subsidy_total += $paid_ary[$i]['SubsidyAmount'];
				$row = array(
					$i+1,
					$paid_ary[$i]['CategoryName'],
					$paid_ary[$i]['ItemName'],
					$paid_ary[$i]['Amount'],
					$paid_ary[$i]['SubsidyAmount'],
					$paid_ary[$i]['PaidTime'],
					$paid_ary[$i]['Remark'],
					$paid_ary[$i]['PaymentMethod']
				);
				$rows[] = $row;
			}
			
			if(count($paid_ary) == 0){
				
			}else{
				$row = array(
					'','',$Lang['General']['Total'],$total,$subsidy_total,'','',''
				);
				$rows[] = $row;
			}
			
			$rows[] = array_fill(0,8,'');
		}
		
		if(in_array('0',$recordTypeAry))
		{
			$unpaid_ary = $sidToRecords[$student_id]['UnpaidItems'];
			
			$row = array_fill(0,8,'');
			$row[0] = $Lang['ePayment']['UnpaidPaymentItems'];
			$rows[] = $row;
			$row = array(
				'#',
				$i_Payment_Field_PaymentCategory,
				$i_Payment_Field_PaymentItem,
				$Lang['ePayment']['Amount'],
				$i_Payment_Subsidy_Amount,
				$i_general_startdate,
				$i_general_enddate,
				''
			);
			$rows[] = $row;
			
			$total = 0;
			$subsidy_total += 0;
			for($i=0;$i<count($unpaid_ary);$i++){
				$total += $unpaid_ary[$i]['Amount'];
				$subsidy_total += $unpaid_ary[$i]['SubsidyAmount'];
				$row = array(
					($i+1),
					$unpaid_ary[$i]['CategoryName'],
					$unpaid_ary[$i]['ItemName'],
					$unpaid_ary[$i]['Amount'],
					$unpaid_ary[$i]['SubsidyAmount'],
					$unpaid_ary[$i]['StartDate'],
					$unpaid_ary[$i]['EndDate'],
					''
				);
				$rows[] = $row;
			}
			
			if(count($unpaid_ary) == 0){
				
			}else{
				$row = array(
					'','',$Lang['General']['Total'],$total,$subsidy_total,'','',''
				);
				$rows[] = $row;
			}
			
			$rows[] = array_fill(0,8,'');
		}
		
	}
	
	
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $header, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=0);
	$lexport->EXPORT_FILE(strip_tags($Lang['ePayment']['PaymentItemStatusReport']).".csv", $exportContent);
}else{
	if($format=='print'){
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	}
	
	$x = '';
	
	if($format != 'print' && $format != 'csv'){
		$x .= '<div class="content_top_tool">
				<div class="Conntent_tool">
					<a href="javascript:submitForm(document.form1,\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
					<a href="javascript:submitForm(document.form1,\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
				</div>
				<br style="clear:both" />
			  </div>';
	}
	
	if($format == 'print'){
		$x .= '<style type="text/css" media="print"> .print-style { page-break-inside: avoid; page-break-after:always;}</style>';
		$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
				</tr>
				</table>';
				
		$x .= '<table width="100%" align="center" border="0">
				<tr>
					<td align="center"><h2>'.$Lang['ePayment']['PaymentItemStatusReport'].'</h2></td>
				</tr>
				</table>';
	}
	
	foreach($studentIdAry as $student_id)
	{
		if(!isset($sidToRecords[$student_id])) continue;
		
		$x.='<div class="print-style">';
		$x.='<table width="100%" align="center" border="0">
				<tr>
					<td align="center"><h3>'.(Get_String_Display($sidToRecords[$student_id]['StudentInfo']['ClassName'],1).'('.$sidToRecords[$student_id]['StudentInfo']['ClassNumber'].') '.Get_String_Display($sidToRecords[$student_id]['StudentInfo']['StudentName'],1)).'</h3></td>
				</tr>
			</table>';
		
		if(in_array('1',$recordTypeAry))
		{
			$paid_ary = $sidToRecords[$student_id]['PaidItems'];
			$x.= '<table class="common_table_list_v30'.($format=='print'?' view_table_list_v30':'').'">'."\n";
			$x .= '<caption style="text-align:left;">'.$Lang['ePayment']['PaidPaymentItems'].'</caption>';
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th class="num_check">#</th>';
				$x.='<th width="20%">'.$i_Payment_Field_PaymentCategory.'</th>';
				$x.='<th width="20%">'.$i_Payment_Field_PaymentItem.'</th>';
				$x.='<th width="10%">'.$Lang['ePayment']['Amount'].'</th>';
				$x.='<th width="10%">'.$i_Payment_Subsidy_Amount.'</th>';
				$x.='<th width="15%">'.$Lang['ePayment']['LibraryPaidOn'].'</th>';
				$x.='<th width="15%">'.$Lang['ePayment']['PaymentRemark'].'</th>';
				$x.='<th width="10%">'.$Lang['ePayment']['PaymentMethod'].'</th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
			$total = 0;
			$subsidy_total = 0;
			for($i=0;$i<count($paid_ary);$i++){
				$total += $paid_ary[$i]['Amount'];
				$subsidy_total += $paid_ary[$i]['SubsidyAmount'];
				$x.='<tr>';
					$x.='<td>'.($i+1).'</td>';
					$x.='<td>'.Get_String_Display($paid_ary[$i]['CategoryName'],1).'</td>';
					$x.='<td>'.Get_String_Display($paid_ary[$i]['ItemName'],1).'</td>';
					$x.='<td>'.$lpayment->getWebDisplayAmountFormat($paid_ary[$i]['Amount']).'</td>';
					$x.='<td>'.($paid_ary[$i]['SubsidyAmount']!=''?$lpayment->getWebDisplayAmountFormat($paid_ary[$i]['SubsidyAmount']):Get_String_Display($paid_ary[$i]['SubsidyAmount'],1)).'</td>';
					$x.='<td>'.$paid_ary[$i]['PaidTime'].'</td>';
					$x.='<td>'.Get_String_Display($paid_ary[$i]['Remark'],1).'</td>';
					$x.='<td>'.Get_String_Display($paid_ary[$i]['PaymentMethod'],1).'</td>';
				$x.='</tr>';
			}
			
			if(count($paid_ary) == 0){
				$x.='<tr>';
					$x.='<td colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
				$x.='</tr>'."\n";
			}else{
				$x.='<tr class="total_row">';
					$x.='<th colspan="3" style="text-align:right">'.$Lang['General']['Total'].'</th><td>'.$lpayment->getWebDisplayAmountFormat($total).'</td><td colspan="4">'.$lpayment->getWebDisplayAmountFormat($subsidy_total).'</td>';
				$x.='</tr>';
			}
			
			$x .= '</tbody>';
			$x .= '</table>'."\n";
			$x .= '<br />'."\n";
		}
		
		if(in_array('0',$recordTypeAry))
		{
			$unpaid_ary = $sidToRecords[$student_id]['UnpaidItems'];
			$x.= '<table class="common_table_list_v30'.($format=='print'?' view_table_list_v30':'').'">'."\n";
			$x .= '<caption style="text-align:left;">'.$Lang['ePayment']['UnpaidPaymentItems'].'</caption>';
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th class="num_check">#</th>';
				$x.='<th width="20%">'.$i_Payment_Field_PaymentCategory.'</th>';
				$x.='<th width="20%">'.$i_Payment_Field_PaymentItem.'</th>';
				$x.='<th width="10%">'.$Lang['ePayment']['Amount'].'</th>';
				$x.='<th width="10%">'.$i_Payment_Subsidy_Amount.'</th>';
				$x.='<th width="20%">'.$i_general_startdate.'</th>';
				$x.='<th width="20%">'.$i_general_enddate.'</th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
			$total = 0;
			$subsidy_total += 0;
			for($i=0;$i<count($unpaid_ary);$i++){
				$total += $unpaid_ary[$i]['Amount'];
				$subsidy_total += $unpaid_ary[$i]['SubsidyAmount'];
				$x.='<tr>';
					$x.='<td>'.($i+1).'</td>';
					$x.='<td>'.Get_String_Display($unpaid_ary[$i]['CategoryName'],1).'</td>';
					$x.='<td>'.Get_String_Display($unpaid_ary[$i]['ItemName'],1).'</td>';
					$x.='<td>'.$lpayment->getWebDisplayAmountFormat($unpaid_ary[$i]['Amount']).'</td>';
					$x.='<td>'.($unpaid_ary[$i]['SubsidyAmount']!=''?$lpayment->getWebDisplayAmountFormat($unpaid_ary[$i]['SubsidyAmount']):Get_String_Display($unpaid_ary[$i]['SubsidyAmount'],1)).'</td>';
					$x.='<td>'.$unpaid_ary[$i]['StartDate'].'</td>';
					$x.='<td>'.$unpaid_ary[$i]['EndDate'].'</td>';
				$x.='</tr>';
			}
			
			if(count($unpaid_ary) == 0){
				$x.='<tr>';
					$x.='<td colspan="7" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
				$x.='</tr>'."\n";
			}else{
				$x.='<tr class="total_row">';
					$x.='<th colspan="3" style="text-align:right">'.$Lang['General']['Total'].'</th><td>'.$lpayment->getWebDisplayAmountFormat($total).'</td><td colspan="3">'.$lpayment->getWebDisplayAmountFormat($subsidy_total).'</td>';
				$x.='</tr>';
			}
			
			$x .= '</tbody>';
			$x .= '</table>'."\n";
			$x .= '<br />'."\n";
		}
		
		$x.='<br />'."\n";
		$x.='</div>';
	}
	
	echo $x;
	if($format == 'print'){
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
}
?>