<?php
// using by 
########################################### Change Log ###################################################
#
# 2017-02-14 by Carlos  - $sys_custom['ePayment']['HartsPreschool'] Added "STRN".
# 2017-01-27 by Carlos : Added displayed column "Login ID".
# 2016-05-25 by Carlos : Fixed sorting of class number by numeric value.
# 2015-02-26 by Carlos : $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
# 2011-11-09 by YatWoon: Fixed: Update wording "Credit Amount" to "Amount" [Case#2011-0928-1640-14073]
# 2011-10-26 by	YatWoon: Fixed: Failed to display archived student
# 2010-02-10 by Carlos: Add border line to table
#
##########################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$linterface = new interface_html();

if($lpayment->isEWalletDirectPayEnabled() == false) {
	$transaction_time = $Lang['ePayment']['RefundTime'];
	$refund_report = $Lang['ePayment']['LeaveSchoolRefundReport'];
	$table_col_name_1 = $i_Profile_DataLeftYear;
} else {
	$transaction_time = $Lang['ePayment']['RefundTime'];
	$refund_report = $Lang['ePayment']['RefundReport'];
	$table_col_name_1 = $Lang['eNotice']['PaymentItemName'];
}

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == "" || !in_array($field,array(0,1,2,3,4,5,6,7,8))){
	 $field = 7;
	 if($sys_custom['ePayment']['HartsPreschool']) $field += 1;
}
	 

// $namefield = getNameFieldWithClassNumberByLang("b.");
$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d')<='$ToDate' ";

if ($YearOfLeft != "") {
	$YearOfLeftCond = " AND (b.YearOfLeft = '".$YearOfLeft."' OR c.YearOfLeft = '".$YearOfLeft."') ";
}

if (trim($ClassName) != "") 
	$user_cond .= " AND (b.ClassName like '%".$ClassName."%') ";
if (trim($_REQUEST['UserID']) != "")
 	$user_cond .= " AND (b.UserID = '".$_REQUEST['UserID']."') ";
/*
$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.YearOfLeft,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.YearOfLeft)) as YearOfLeft,
               ".$lpayment->getWebDisplayAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode as RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
         WHERE
         		 potl.TransactionType=7 
         		 and 
         			(
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               potl.RefCode LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'
              )
             $date_cond
             $user_cond 
             $YearOfLeftCond 
                ";
*/
$sql  = "SELECT 
			   IF(b.UserID IS NOT NULL,b.UserLogin,IF(c.UserLogin IS NOT NULL,c.UserLogin,'".$Lang['General']['EmptySymbol']."')) as UserLogin,";
if($sys_custom['ePayment']['HartsPreschool']){
	$sql .= "IF(b.UserID IS NOT NULL,b.STRN,c.STRN) as STRN,";
}
       $sql.=" IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
               IF( (c.ClassName IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (c.ClassNumber IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,";
if($lpayment->isEWalletDirectPayEnabled() == false) {
	$sql .= "IF( (c.YearOfLeft IS NOT NULL),c.YearOfLeft,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.YearOfLeft)) as YearOfLeft,";
} else {
	$sql .= "e.Name as YearOfLeft,";
}
	$sql .= $lpayment->getWebDisplayAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode as RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime,
			   IF( (c.ClassName IS NOT NULL),c.ClassName,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassNameForSort,
			   IF( c.ClassNumber IS NOT NULL,c.ClassNumber,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber))+0 as ClassNumberForSort  
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
            LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS d ON d.PaymentID=potl.RelatedTransactionID
            LEFT JOIN PAYMENT_PAYMENT_ITEM AS e ON e.ItemID=d.ItemID
         WHERE
         		 potl.TransactionType=7 
         		 and 
         			(
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               potl.RefCode LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'
              )
             $date_cond
             $user_cond 
             $YearOfLeftCond 
                ";
$fields = array("UserLogin");
if($sys_custom['ePayment']['HartsPreschool']){
	$fields[] = "STRN";
}
$fields = array_merge($fields, array("Name","ClassName","ClassNameForSort,ClassNumberForSort","YearOfLeft","potl.Amount","RefCode","TransactionTime"));            
$sql .= " ORDER BY ".$fields[$field]." ".($order==1?" ASC ": " DESC ");
                        
$Result = $lpayment->returnArray($sql);

$x = "<table width=90% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x .= '<tr>';
$x .= '<td width="10%"><b>'.$i_ClassName.'</b></td>';
$x .= '<td><b>: '.(($ClassName != "")? $ClassName:$i_general_all).'</b></td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td width="10%"><b>'.$i_Profile_DataLeftYear.'</b></td>';
$x .= '<td><b>: '.(($YearOfLeft != "")?$YearOfLeft:$i_general_all).'</b></td>';
$x .= '</tr>';
//$x .= '<tr>';
//$x .= '<td><b>'.$Lang['Payment']['Keyword'].'</b></td>';
//$x .= '<td><b>:'.$Keyword.'</b></td>';
//$x .= '</tr>';
$x .= '</table>';
//$x .= '<hr>';
$x .= "<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='eSporttableborder'>";
$x .= "<thead>";
$x .= "<tr class='eSporttdborder eSportprinttabletitle'>";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$Lang['AccountMgmt']['LoginID']."</td>\n";
if($sys_custom['ePayment']['HartsPreschool']){
	$x .= "<td width=5% class='eSporttdborder eSportprinttabletitle'>".$i_STRN."</td>\n";
}
$x .= "<td width=15% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_Username."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassName."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassNumber."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$table_col_name_1."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['Amount']."</td>\n";
$x .= "<td width=12% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_RefCode."</td>\n";
$x .= "<td width=18% class='eSporttdborder eSportprinttabletitle'>".$transaction_time."</td>\n";
$x .= "</tr>";
$x .= "</thead>";
$x .= "<tbody>";

for ($i=0; $i< sizeof($Result); $i++) {
	$x .= '<tr>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['UserLogin'];
	$x .= '</td>';
	if($sys_custom['ePayment']['HartsPreschool']){
		$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
		$x .= $Result[$i]['STRN'];
		$x .= '</td>';
	}
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['Name'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassName'])=='')?'--':$Result[$i]['ClassName'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassNumber'])=='')?'--':$Result[$i]['ClassNumber'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['YearOfLeft'])=='')?'--':$Result[$i]['YearOfLeft'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['Amount'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['RefCode'])=='')?'--':$Result[$i]['RefCode'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['TransactionTime'];
	$x .= '</td>';
	$x .= '</tr>';
}
$x.="</tbody>";
$x.="</table>";

include_once($PATH_WRT_ROOT."templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submitPrint")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	echo '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
}
?>
<table border=0 width=90% cellpadding=2 align=center>
<tr><td class='<?=$css_title?>'><b><?=$refund_report?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<?=$x?>
<br>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>
