<?php
// Editing by 
#####################################################
#	
#	2017-02-14 Carlos : Added STRN.
#	2017-01-27 Carlos : Added displayed column "Login ID".
#	2015-08-06 Carlos : Get/Set date range cookies.
#	2011-11-09 YatWoon : Fixed Update wording "Credit Amount" to "Amount" [Case#2011-0928-1640-14073]
#
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_Donation";

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 7;
	 if($sys_custom['ePayment']['HartsPreschool']) $field += 1;
}
	 
$namefield = getNameFieldWithClassNumberByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="")
        $FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

$date_cond = " AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d') >= '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";

if ($YearOfLeft != "") {
	$YearOfLeftCond = " AND (b.YearOfLeft = '".$lpayment->Get_Safe_Sql_Query($YearOfLeft)."' OR c.YearOfLeft = '".$lpayment->Get_Safe_Sql_Query($YearOfLeft)."') ";
}

if (trim($ClassName) != "") 
	$user_cond .= " AND (b.ClassName like '%".$lpayment->Get_Safe_Sql_Like_Query($ClassName)."%') ";
if (trim($_REQUEST['UserID']) != "")
 	$user_cond .= " AND (b.UserID = '".$lpayment->Get_Safe_Sql_Query($_REQUEST['UserID'])."') ";

$escaped_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);

$sql  = "SELECT 
			  IF(b.UserID IS NOT NULL,b.UserLogin,IF(c.UserLogin IS NOT NULL,c.UserLogin,'".$Lang['General']['EmptySymbol']."')) as UserLogin,";
if($sys_custom['ePayment']['HartsPreschool']){
	$sql .= "IF(b.UserID IS NOT NULL,b.STRN,c.STRN) as STRN,";
}    
    $sql .= " IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.YearOfLeft,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.YearOfLeft)) as YearOfLeft,
               ".$lpayment->getWebDisplayAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
         WHERE
         		 potl.TransactionType=10 
         		 and 
         			(
               b.EnglishName LIKE '%$escaped_keyword%' OR
               b.ChineseName LIKE '%$escaped_keyword%' OR
               b.ClassName LIKE '%$escaped_keyword%' OR
               b.ClassNumber LIKE '%$escaped_keyword%' OR
               potl.RefCode LIKE '%$escaped_keyword%' OR
               c.EnglishName LIKE '%$escaped_keyword%' OR
               c.ChineseName LIKE '%$escaped_keyword%' OR
               c.ClassName LIKE '%$escaped_keyword%' OR
               c.ClassNumber LIKE '%$escaped_keyword%'
              )
             $date_cond
             $user_cond 
             $YearOfLeftCond 
                ";
//debug_r($sql); 
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("UserLogin");
if($sys_custom['ePayment']['HartsPreschool']){
	$li->field_array[] = "STRN";
}
$li->field_array = array_merge($li->field_array, array("Name","ClassName","ClassNumber","YearOfLeft","Amount","RefCode","TransactionTime"));
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['AccountMgmt']['LoginID'])."</td>\n";
if($sys_custom['ePayment']['HartsPreschool']){
	$li->column_list .= "<td width=5% class=tableTitle>".$li->column($pos++, $i_STRN)."</td>\n";
}
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Profile_DataLeftYear)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['Amount'])."</td>\n";
$li->column_list .= "<td width=12% class=tableTitle>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";


$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=ClassName onChange=\"document.form1.submit();\"",''.$_REQUEST['ClassName'].'');

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);

// Year OF Left filter
$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
$YearOfLeftArr = $lclass->returnVector($sql);
$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_ARCHIVE_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
$YearOfLeftArr = array_unique(array_merge($YearOfLeftArr,$lclass->returnVector($sql)));
sort($YearOfLeftArr);
$select_year_left .= "<SELECT name=\"YearOfLeft\" onChange=\"this.form.submit()\">\n";
$empty_selected = ($YearOfLeft == '')? "SELECTED":"";
$select_year_left .= "<OPTION value='' $empty_selected>-- $i_Profile_DataLeftYear --</OPTION>\n";
if (sizeof($YearOfLeftArr) > 0) {
	foreach($YearOfLeftArr as $Key => $name) {
		$sel_str = ($YearOfLeft == $name? "SELECTED":"");
		$select_year_left .= "<OPTION value='$name' $sel_str> $name </OPTION>\n";
	}
}
$select_year_left .= "</SELECT>\n";

$searchbar .= '<span id="ClassLayer">'.$select_class.'</span>';
$searchbar .= $select_year_left;
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".escape_double_quotes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($Lang['Payment']['DonationReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="javascript">
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it

function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
}

function Show_Class_Layer(UserType) {
	if (UserType != 1) 
		document.getElementById("ClassLayer").style.display = '';
	else 
		document.getElementById("ClassLayer").style.display = 'none';
}

function Show_Student_Layer(ClassName) {
	if (Trim(ClassName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_student_selection.php';
	  var postContent = 'ClassName='+encodeURIComponent(ClassName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
			  document.getElementById("StudentLayer").innerHTML = ResponseText;
			  document.getElementById("StudentLayer").style.display = "";
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		document.getElementById("StudentLayer").innerHTML = "";
		document.getElementById("StudentLayer").style.display = "none";
	}
}

function openPrintPage(){
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action="print.php";
  FormObj.target="_blank";
  FormObj.submit();
  FormObj.action = old_url;
  FormObj.target= old_target;
}

function exportPage(obj,url){
  old_url = obj.action;
  obj.action=url;
  obj.submit();
  obj.action = old_url;
}
</script>
<form name="form1" id="form1" method="get" action='index.php'>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Payment_Menu_Browse_CreditTransaction_DateRange ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$i_Profile_To?>  
			<?=$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1;","submit2") ?>
		</td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
	<tr>
		<td align="left"><?=$searchbar ?></td>
	</tr>
</table>
<?= $li->display() ?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr><td><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>