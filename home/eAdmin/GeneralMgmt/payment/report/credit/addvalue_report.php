<?php
// editing by 

#################################################
#	Date:	2019-01-31 Carlos
#			Support Alipay and TNG top-up records.
#	Date: 	2017-02-02 Carlos
#			Added CancelOption: [Within date range] / [Can exceed date range] for Hide cancelled transactions.
# 	Date:	2016-08-24 Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
# 	Date:	2015-08-06 Carlos
#			Set date range cookies.
#	Date:	2015-07-22 Carlos
#			$sys_custom['ePayment']['CreditMethodAutoPay'] - added credit type Auto-pay.
# 	Date:	2015-07-07 Carlos
#			Flip the logic of [Hide cancelled transactions] to [Display cancelled transactions].
#	Date:	2015-02-26	Carlos
#			$sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
#	Date:	2015-02-16 	Carlos
#			Added [Hide cancelled transactions]	
#	Date:	2013-11-21	Carlos
#			Format Amount by $lpayment->getExportAmountFormatDB() before doing MySql SUM() as it lose floating point precision
#	Date:	2011-10-24	YatWoon
#			Fixed: missing "Print" button for web format [Case#2011-1010-1212-23071]
#			
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

$MODULE_OBJ['title'] = $i_Payment_Menu_PrintPage_AddValueReport;

$linterface = new interface_html("popup.html");


# date range 
# default : FromDate = 1st of current month 
#			ToDate = last day of current month
$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

if($FromDate != '' && $ToDate!=''){
	$lpayment->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
}

if($search_by!=1){
	$search_time ="a.DateInput";
	$date_cond = " AND (DATE_FORMAT(a.DateInput,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d')<='$ToDate') ";
}else{ 
	$search_time="a.TransactionTime";
	$date_cond = " AND (DATE_FORMAT(a.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate') ";
}

if($valueadd_type=='')
	$conds = " a.RecordType IN (1,2,3".($sys_custom['ePayment']['CreditMethodAutoPay']?",4":"").($sys_custom['ePayment']['CashDepositMethod']?",6,7":"").",8,9)";
else $conds = " a.RecordType= '$valueadd_type'";

if($DisplayCancelled != '1'){
	$join_cancel_trans_table = " LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.StudentID=a.StudentID AND t.TransactionType=9 AND t.RelatedTransactionID=a.TransactionID ";
	if($CancelOption == '1') $join_cancel_trans_table .= " AND (DATE_FORMAT(t.TransactionTime,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') ";
	$cond_cancel_trans = " AND t.LogID IS NULL ";
}

$sql = "SELECT 
		DATE_FORMAT($search_time,'%Y/%m/%d') AS TransactionDay,
		SUM(".$lpayment->getExportAmountFormatDB("a.Amount").") AS TotalAmount,
		COUNT(a.TransactionID) AS TransactionsCount 
		FROM PAYMENT_CREDIT_TRANSACTION as a $join_cancel_trans_table 
		WHERE $conds $date_cond $cond_cancel_trans
		GROUP BY DATE_FORMAT($search_time,'%Y-%m-%d') 
		";
		
$li = new libpayment();
$temp = $li->returnArray($sql,3);
$total_amount = 0;
$total_count  = 0;
for($i=0;$i<sizeof($temp);$i++){
	list($date,$amount,$count)=$temp[$i];
	$amount = $amount+0;
	$result[$date]['amount']=$amount;
	$result[$date]['count'] =$count;
	$total_amount+=$amount;
	$total_count +=$count;
}

switch($valueadd_type){
	case '1' : $value_add_type_name=$i_Payment_Credit_TypePPS;break;
	case '2' : $value_add_type_name=$i_Payment_Credit_TypeCashDeposit;break;
	case '3' : $value_add_type_name=$i_Payment_Credit_TypeAddvalueMachine;break;
	case '4' : $value_add_type_name=$Lang['ePayment']['AutoPay'];break;
	case '6' : $value_add_type_name=$Lang['ePayment']['BankTransfer'];break;
	case '7' : $value_add_type_name=$Lang['ePayment']['ChequeDeposit'];break;
	case '8' : $value_add_type_name=$Lang['ePayment']['TNG'];break;
	case '9' : $value_add_type_name=$Lang['ePayment']['Alipay'];break;
	case '10' : $value_add_type_name=$Lang['ePayment']['FPS'];break;
	case '11' : $value_add_type_name=$Lang['ePayment']['TapAndGo'];break;
	case '12' : $value_add_type_name=$Lang['ePayment']['VisaMaster'];break;
	case '13' : $value_add_type_name=$Lang['ePayment']['WeChat'];break;
	case '14' : $value_add_type_name=$Lang['ePayment']['AlipayCN'];break;
	default :  $value_add_type_name=$i_status_all;
}

$display="<table width=90% border=0 cellpadding=1 cellspacing=0 align=center>";
$display.="<tr><td class='$css_text'><B>".($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime).":</b> $FromDate $i_Profile_To $ToDate</td></tr>";
$display.="<tr><td class='$css_text'><B>".$i_Payment_Credit_Method.":</b> ".$value_add_type_name."</td></tr>";
$display.="</table>";

$csv="\"".($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime)."\",\"$FromDate $i_Profile_To $ToDate\"\n";
$csv.="\"".$i_Payment_Credit_Method."\",\"$value_add_type_name\"\n\n";
$csv_utf=($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime)."\t$FromDate $i_Profile_To $ToDate\r\n";
$csv_utf.="$i_Payment_Credit_Method\t$value_add_type_name\r\n";

$display.="<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display.="<thead>";
$display.="<tr>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"30%\">$i_Payment_Menu_PrintPage_AddValueReport_Date</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"30%\">$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"30%\">$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount</td>";
$display.="</tr>";
$display.="</thead>";
$display.="<tbody>";

$csv .= "\"$i_Payment_Menu_PrintPage_AddValueReport_Date\",\"$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal\",\"$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount\"\n";
$csv_utf .= "$i_Payment_Menu_PrintPage_AddValueReport_Date\t$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal\t$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount\r\n";

		
$start_ts =strtotime($FromDate);
$end_ts = strtotime($ToDate);
$i=0;

while($start_ts<=$end_ts) {
	$target_date = date('Y/m/d',$start_ts);
	$target_amount = $result[$target_date]['amount']==""?"--":$result[$target_date]['amount'];
	$target_amount_csv = $li->getExportAmountFormat($target_amount);
	$target_amount = $target_amount=="--"?$target_amount:$li->getWebDisplayAmountFormat($target_amount);
	$target_count = $result[$target_date]['count']==""?"--":$result[$target_date]['count'];
	$start_ts += 60*60*24;
	$i++;
	
	$display.="<tr class=\"tablebluerow". ( $i%2 + 1 )."\">\n";
	$display.="<td class=\"tabletext\">$target_date</td>";
	$display.="<td class=\"tabletext\">$target_amount</td>";
	$display.="<td class=\"tabletext\">$target_count</td>";
	$display.="</tr>";
	
	$csv.="\"$target_date\",\"$target_amount_csv\",\"$target_count\"\n";
	$csv_utf.="$target_date\t$target_amount_csv\t$target_count\r\n";
}
if($i==0){
	$display.="<tr><td colspan=\"3\" class=\"tabletext\" align=\"center\">$i_StaffAttendance_Status_NoRecord</td></tr>";
}else{
	$display.="<tr class=\"tableorangebottom\"><td class=\"tabletext\">$list_total</td><td class=\"tabletext\">".$li->getWebDisplayAmountFormat($total_amount)."</td><td class=\"tabletext\">$total_count</td></tr>";
	$csv.="\"$list_total\",\"".$li->getExportAmountFormat($total_amount)."\",\"$total_count\"\n";
	$csv_utf.="$list_total\t".$li->getExportAmountFormat($total_amount)."\t$total_count\r\n";
}
$display.="</tbody>";
$display.="</table>";


if ($format == 1)     # Excel
{
  # Get template
	$output_filename = "addvalue.csv";
	$lexport->EXPORT_FILE($output_filename, $csv_utf);
}
else{
	$linterface->LAYOUT_START();

?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<form name="form1" method="get" action='addvalue_report.php'>
<?=$display?>
</form>

<?
$linterface->LAYOUT_STOP();
}
intranet_closedb();
?>