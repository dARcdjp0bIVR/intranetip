<?php
// Editing by 
/*
 * 2019-01-31 (Carlos): support Alipay and TNG top-up records.
 * 2017-02-02 (Carlos): Added CancelOption: [Within date range] / [Can exceed date range] for Hide cancelled transactions.
 * 2016-08-24 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
 * 2015-08-13 (Carlos): Default check on [Display cancelled transactions].
 * 2015-08-06 (Carlos): Get date range cookies.
 * 2015-07-22 (Carlos): $sys_custom['ePayment']['CreditMethodAutoPay'] added credit type Auto-pay
 * 2015-07-07 (Carlos): Change [Hide cancelled transactions] to [Display cancelled transactions].
 * 2015-02-16 (Carlos): Added [Hide cancelled transactions]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_CreditTransaction";

$linterface = new interface_html();

# date range ( 1st of current month till the last day of current month)
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate == ""){
	$FromDate = $date_range_cookies['StartDate'] != ''? $date_range_cookies['StartDate'] : ((date('Y-m'))."-01");
}
//$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
if($ToDate == ""){
	$ToDate = $date_range_cookies['EndDate'] != ''? $date_range_cookies['EndDate'] : (date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y"))));
}
//$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

$firstname_filter = "$i_status_all";
$array_filter_name = array($i_Payment_Credit_TypePPS, $i_Payment_Credit_TypeCashDeposit, $i_Payment_Credit_TypeAddvalueMachine);
$array_filter_data = array("1", "2", "3");
if($sys_custom['ePayment']['CreditMethodAutoPay']){
	$array_filter_name[] = $Lang['ePayment']['AutoPay'];
	$array_filter_data[] = "4";
}else if($sys_custom['ePayment']['CashDepositMethod']){
	$array_filter_name[] = $Lang['ePayment']['BankTransfer'];
	$array_filter_data[] = "6";
	$array_filter_name[] = $Lang['ePayment']['ChequeDeposit'];
	$array_filter_data[] = "7";
}
if($sys_custom['ePayment']['TopUpEWallet']['TNG']){
	$array_filter_name[] = $Lang['ePayment']['TNG'];
	$array_filter_data[] = "8";
}
if($sys_custom['ePayment']['TopUpEWallet']['Alipay']){
	$array_filter_name[] = $Lang['ePayment']['Alipay'];
	$array_filter_data[] = "9";
}
if($sys_custom['ePayment']['TopUpEWallet']['FPS']){
	$array_filter_name[] = $Lang['ePayment']['FPS'];
	$array_filter_data[] = "10";
}
if($sys_custom['ePayment']['TopUpEWallet']['TAPANDGO']){
	$array_filter_name[] = $Lang['ePayment']['TapAndGo'];
	$array_filter_data[] = "11";
}
if($sys_custom['ePayment']['TopUpEWallet']['VisaMaster']){
	$array_filter_name[] = $Lang['ePayment']['VisaMaster'];
	$array_filter_data[] = "12";
}
if($sys_custom['ePayment']['TopUpEWallet']['WeChat']){
	$array_filter_name[] = $Lang['ePayment']['WeChat'];
	$array_filter_data[] = "13";
}
if($sys_custom['ePayment']['TopUpEWallet']['AlipayCN']){
	$array_filter_name[] = $Lang['ePayment']['AlipayCN'];
	$array_filter_data[] = "14";
}
$select_filter = getSelectByValueDiffName($array_filter_data,$array_filter_name,"name='valueadd_type'","",1,0, $firstname_filter);

// Default check on [Display cancelled transactions]
if(!isset($_REQUEST['FromDate'])){
	$DisplayCancelled = 1;
}
/*
$select_filter = "<SELECT name='valueadd_type'>";
$select_filter.="<OPTION value=''>$i_status_all</OPTION>";
$select_filter.="<OPTION value='1'>$i_Payment_Credit_TypePPS</OPTION>";
$select_filter.="<OPTION value='2'>$i_Payment_Credit_TypeCashDeposit</OPTION>";
$select_filter.="<OPTION value='3'>$i_Payment_Credit_TypeAddvalueMachine</OPTION>";
$select_filter.="</SELECT>";
*/

$format_array = array(
                      array(0,"Web"),
                      array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);

$TAGS_OBJ[] = array($i_Payment_Menu_PrintPage_AddValueReport, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
function checkForm(formObj){
        if(formObj==null) return false;
	    fromV = formObj.FromDate;
	    toV= formObj.ToDate;
	    if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
	    }
	    else if(!checkDate(toV)){
            //formObj.ToDate.focus();
            return false;
	    }
	    return true;
}

function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
}

function submitForm(obj){
        if(checkForm(obj)) {
	        if(obj.format.value!=1){
        		obj.target='intranet_popup10';
        		newWindow('about:blank', 10);
        	}else{
	        	obj.target='';
	        }
			obj.submit();
		}
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get" action="addvalue_report.php">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['DateType']?></td>
  	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
    	<input type='radio' id='search_by_posttime' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?>>
			<label for="search_by_posttime"><?=$i_Payment_Search_By_PostTime?></label>&nbsp;&nbsp;
			<input type='radio' id="search_by_transactiontime" name='search_by' value='1' <?=($search_by==1?"checked":"")?>>
			<label for="search_by_transactiontime"><?=$i_Payment_Search_By_TransactionTime?></label>
    </td>
  </tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input class="textboxnum" type="text" name="FromDate" id="FromDate" value="<?=$FromDate?>">
			 <?=$i_Profile_To?> 
			 <input class="textboxnum" type="text" name="ToDate" id="ToDate" value="<?=$ToDate?>">
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Payment_Credit_Method?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$select_filter?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_Format?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$select_format?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['General']['Others']?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input type="checkbox" id="DisplayCancelled" name="DisplayCancelled" value="1" <?=$DisplayCancelled==1?'checked="checked"':''?> onclick="document.getElementById('CancelOptionTr').style.display=this.checked?'none':'';"  />
			<label for="DisplayCancelled"><?=$Lang['ePayment']['DisplayCancelledTransactions']?></label>
		</td>
	</tr>
	<tr id="CancelOptionTr" <?=$DisplayCancelled=='1'?' style="display:none;"':''?>>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['ePayment']['HideCancelledTransactions']?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<div>
				<?=$linterface->Get_Radio_Button("WithinDateRange", "CancelOption", "1", $CancelOption == 1, $___Class="",  $Lang['ePayment']['WithinDateRange'])?>
				<?=$linterface->Get_Radio_Button("CanExceedDateRange", "CancelOption", "2", $CancelOption=="" || $CancelOption == 2, $___Class="",  $Lang['ePayment']['CanExceedDateRange'])?>
			</div>
		</td>
	</tr>
	<tr>
    	<td colspan="2">
		    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td colspan="2" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:submitForm(document.form1)") ?>
		</td>
	</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.FromDate");
intranet_closedb();
?>
