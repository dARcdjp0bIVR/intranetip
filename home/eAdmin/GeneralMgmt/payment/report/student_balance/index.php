<?php
// Editing by 
/*
 * 2015-08-06 (Carlos): Get and use date range cookies.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lclass = new libclass();
$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_ExportStudentBalance";

$TAGS_OBJ[] = array($i_Payment_Menu_Report_Export_StudentBalance, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$TargetClassLevel = trim(stripslashes(urldecode($_REQUEST['TargetClassLevel'])));

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($TargetStartDate=="" || !intranet_validateDate($TargetStartDate))
	$TargetStartDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="" || !intranet_validateDate($TargetEndDate))
	$TargetEndDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

$select_default_value = "";
// [2014-1127-1747-49207] - add All to drop down list
if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
	if($TargetClassLevel == ""){
		$TargetClassID = "";
	}
	
	$select_default_value = $i_general_all;
}

// [2014-1127-1747-49207]
//$class_level_selection = $lclass->getSelectLevel("name=\"TargetClassLevel\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" ", $TargetClassLevel, true);
$class_level_selection = $lclass->getSelectLevel("name=\"TargetClassLevel\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" ", $TargetClassLevel, true, $select_default_value);

if($TargetClassLevel != "") {
	
	//$level_id = $lclass->getLevelID($TargetClassLevel);
	
	$arr_class_list = $lclass->returnClassListByLevel($TargetClassLevel);
	
	if(sizeof($arr_class_list)>0) {
		//$class_name_selection = getSelectByArray($arr_class_list," name=\"TargetClassID[]\" MULTIPLE size=\"5\" onClick=\"document.form1.flag.value=0; this.form.submit();\" ",$TargetClassID,0);
		
		$class_name_selection = "<SELECT name=\"TargetClassID[]\" MULTIPLE size=\"5\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" >";
		for($i=0; $i<sizeof($arr_class_list); $i++) {
			if((sizeof($TargetClassID)>0) && (in_array($arr_class_list[$i]['YearClassID'],$TargetClassID))) {
				$x .= "<OPTION value='".$arr_class_list[$i]['YearClassID']."' SELECTED>".Get_Lang_Selection($arr_class_list[$i]['ClassTitleB5'],$arr_class_list[$i]['ClassTitleEN'])."</OPTION>\n";
			} else {
				$x .= "<OPTION value='".$arr_class_list[$i]['YearClassID']."'>".Get_Lang_Selection($arr_class_list[$i]['ClassTitleB5'],$arr_class_list[$i]['ClassTitleEN'])."</OPTION>\n";
			}
		} 
		
		$class_name_selection .= $x;
		$class_name_selection .= "</SELECT>";
	} else {
		$class_name_selection = "<SELECT name=\"TargetClassID[]\" MULTIPLE size=\"5\" onChange=\"document.form1.flag.value=0; document.form1.action=''; this.form.submit();\" >";
		$class_name_selection .= "</SELECT>";
	}
	
	$btn_select_all_class = $linterface->GET_SMALL_BTN($button_select_all,"button","selectAll('TargetClassID[]');","");
	$class_name_selection .= "&nbsp;".$btn_select_all_class;
}

if($TargetClassID != "") {

	$TargetClassID = IntegerSafe($TargetClassID);
	$class_list = "'".implode("','",$TargetClassID)."'";
	
	$namefield = getNameFieldByLang2("u.");
	$sql = "SELECT 
						ycu.UserID, 
						CONCAT($namefield,' (',u.ClassName,'-',u.ClassNumber,')') 
					FROM 
						YEAR_CLASS_USER as ycu 
						inner join 
						YEAR_CLASS as yc 
						on ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						inner join 
						INTRANET_USER as u 
						on ycu.UserID = u.UserID and ycu.YearClassID in (".$class_list.") 
					ORDER BY 
						yc.sequence, ycu.ClassNumber";
	//debug_r($sql); die;
	$arr_student = $lpayment->returnArray($sql,2);
	
	if(sizeof($arr_student)>0){
		//$student_selection = getSelectByArray($arr_student_selected, " name=\"TargetStudentID\" ");
		
		$student_selection = "<SELECT name=\"TargetStudentID[]\" MULTIPLE size=\"5\" >";
		for($i=0; $i<sizeof($arr_student); $i++) {
			list($student_id, $student_name) = $arr_student[$i];
			if((sizeof($TargetStudentID)>0) && (in_array($student_id,$TargetStudentID))) {
				$y .= "<OPTION value='$student_id' SELECTED>$student_name</OPTION>\n";
			} else {
				$y .= "<OPTION value='$student_id'>$student_name</OPTION>\n";
			}
		}
		$student_selection .= $y;
		$student_selection .= "</SELECT>";
	} else {
		$student_selection = "<SELECT name=\"TargetStudentID[]\" MULTIPLE size=\"5\" >";
		$student_selection .= "</SELECT>";
	}
	
	//$btn_select_all_student = "<a href=\"javascript:selectAll('TargetStudentID[]')\"><img src='$image_path/admin/button/s_btn_select_all_$intranet_session_language.gif' border='0'></a>";
	$btn_select_all_student = $linterface->GET_SMALL_BTN($button_select_all,"button","selectAll('TargetStudentID[]');","");
	$student_selection .= "&nbsp;".$btn_select_all_student;
}
?>
<script language="javascript">
	function selectAll(element)
	{
		var obj = document.form1;
		var s = obj.elements[element];
		
		for(i=0; i<s.length; i++){
			s[i].selected = true;
		}
		obj.flag.value = 0;
		obj.submit();
	}

	function check_multi_select(obj, element)
	{
		var s = obj.elements[element];
		var select_element = 0;
		
		for(i=0; i<s.length; i++){
			if(s[i].selected){
				select_element++;
			}
		}
		
		if(select_element>0){
			return true;
		} else {
			return false;
		}
	}
		
	function checkForm()
	{
		var obj = document.form1;
		var passed = 0;
	
		if(obj.flag.value == 1){
			if(obj.TargetClassLevel.value != "") {
				if(check_multi_select(obj, 'TargetClassID[]')) {
					if(check_multi_select(obj, 'TargetStudentID[]')) {
						passed = 1;
					} else {
						alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert3;?>");
					}
				} else {
					alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert2;?>");
				}
			} else {
				if(<?=$sys_custom['ePayment']['ReportWithAllTransactionRecords']?"true":"false"?>){
					passed = 1;
				} else {
					alert("<?=$i_Payment_Menu_Report_Export_StudentBalance_Alert1;?>");
				}
			}
		} else {
			passed = 0;
		}
		
		if(passed == 1) {
			obj.action = "export_student_balance.php";
			return true;
		} else {
			obj.action = "";
			return false;
		}
			
	}
</script>

<form name="form1" method="POST" onSubmit="return checkForm();">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_startdate ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->Get_Date_Picker("TargetStartDate",$TargetStartDate)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_enddate ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->Get_Date_Picker("TargetEndDate",$TargetEndDate)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_ClassLevel ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$class_level_selection?>
		</td>
	</tr>
	<? if($TargetClassLevel != "") { ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">* <?=$i_ClassName;?></td>
		<td  valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$class_name_selection;?></td>
	</tr>
	<? } ?>
	<? if($TargetClassID != "") { ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">* <?=$i_general_choose_student;?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$student_selection;?></td>
	</tr>
	<? } ?>
	<? if(($TargetClassLevel != "") || ($TargetClassID != "")) { ?>
	<tr>
		<td>&nbsp;</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><span class="extraInfo">* <?=$i_Payment_Menu_Report_Export_StudentBalance_MultiSelect;?></span></td></tr>
	<? } ?>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1;","submit2") ?>
		</td>
	</tr>
</table>

<input type="hidden" name="flag" value="0">

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>