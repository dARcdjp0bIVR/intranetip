<?php
// Editing by 
/*
 * 2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
 * 2015-07-15 (Carlos): Added data sorting.
 * 2015-02-09 (Carlos): $sys_custom['ePayment']['PaymentItemDateRange'] - record date range are bound by StartDate or EndDate
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

if(!isset($field) || $field == '' || !in_array($field, array(0,1,2,3,4,5,6,7,8,9))){
	$field = 3;  
}
if(!isset($order) || $order == '' || !in_array($order,array(0,1))){
	$order = 0;
}
$field_offset = 0;
$sort_fields = array('name','catname','startdate','enddate','status','total','paid');
if($sys_custom['ePayment']['TNG']){
	$sort_fields[] = 'paid_tng';
	$sort_fields[] = 'paid_manual';
	$field_offset += 4;
}
$sort_fields[] = 'unpaid';
/*
$x.="\"$i_Payment_Field_PaymentItem\",";
$x.="\"$i_Payment_Field_PaymentCategory\",";
$x.="\"$i_general_startdate\",";
$x.="\"$i_general_enddate\",";
$x.="\"$i_general_status\",";
$x.="\"$i_Payment_Field_TotalPaidCount\",";
$x.="\"$i_Payment_PresetPaymentItem_PaidStudentCount\",";
$x.="\"$i_Payment_PresetPaymentItem_PaidAmount\",";

$x.="\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\",";
$x.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\"";
$x.="\n";
*/
$exportColumn = array($i_Payment_Field_PaymentItem, 
						$i_Payment_Field_PaymentCategory, 
						$i_general_startdate, 
						$i_general_enddate, 
						$i_general_status, 
						$i_Payment_Field_TotalPaidCount, 
						$i_Payment_PresetPaymentItem_PaidStudentCount, 
						$i_Payment_PresetPaymentItem_PaidAmount);
if($sys_custom['ePayment']['TNG']){
	$exportColumn[] = $Lang['ePayment']['TNGPaidCount'];
	$exportColumn[] = $Lang['ePayment']['TNGAmountPaid'];
	$exportColumn[] = $Lang['ePayment']['ManualPaidCount'];
	$exportColumn[] = $Lang['ePayment']['ManualAmountPaid'];
}	 
$exportColumn[] = $i_Payment_PresetPaymentItem_UnpaidStudentCount; 
$exportColumn[] = $i_Payment_PresetPaymentItem_UnpaidAmount;


# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$today = date('Y-m-d');

if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_cond =" (DATE_FORMAT(a.StartDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') AND (DATE_FORMAT(a.EndDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') ";
}else{
	$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";		
}

$cat_cond = $CatID==""?"":" AND a.CatID='$CatID' ";

$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond $cat_cond AND a.Name LIKE '%$keyword%' ";

$temp = $lpayment->returnVector($sql);
if(sizeof($temp)>0){
	$item_list = implode(",",$temp);
	$sql="SELECT a.ItemID, 
				a.Name,
				b.Name,
				a.StartDate,
				a.EndDate,
				IF('$today'<DATE_FORMAT(a.StartDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_NotStarted',
				IF('$today'>DATE_FORMAT(a.EndDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_Ended','$i_Payment_PresetPaymentItem_Progress')) AS Status
				FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
				WHERE a.ItemID IN($item_list) ORDER BY a.EndDate DESC";
	$temp = $lpayment->returnArray($sql,6);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name,$cat_name,$start_date,$end_date,$status)=$temp[$i];
		$result[$item_id]['itemid']     = $item_id;
		$result[$item_id]['name']		= $item_name;
		$result[$item_id]['catname']	= $cat_name;
		$result[$item_id]['startdate']	= $start_date;
		$result[$item_id]['enddate']	= $end_date;
		$result[$item_id]['status']		= $status;
		$result[$item_id]['paid']	= 0;
		$result[$item_id]['unpaid']= 0;
		$result[$item_id]['paidcount']	= 0;
		$result[$item_id]['unpaidcount']= 0;
		if($sys_custom['ePayment']['TNG']){
			$result[$item_id]['paid_tng']= 0;
			$result[$item_id]['paidcount_tng'] = 0;
			$result[$item_id]['paid_manual']= 0;
			$result[$item_id]['paidcount_manual'] = 0;
		}
		
	}
	$sql="SELECT b.ItemID,
				b.RecordStatus,
				b.Amount ";
	if($sys_custom['ePayment']['TNG']){
		$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus,t.ChargeStatus ";
	}
	$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS b ";
	if($sys_custom['ePayment']['TNG']){
		$sql .= " LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=b.PaymentID ";
	}
	$sql.=" WHERE b.ItemID IN ($item_list) 
			ORDER BY b.ItemID,b.RecordStatus";
	$temp = $lpayment->returnArray($sql);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$record_status,$amount) = $temp[$i];
		if($record_status==1){ # Paid
			$result[$item_id]['paid']+=($amount>0?$amount:0);
			$result[$item_id]['paidcount']++;
			if($sys_custom['ePayment']['TNG']){
				if($temp[$i]['TNGRecordID']!='' && $temp[$i]['PaymentStatus']==1 && $temp[$i]['ChargeStatus']==1){
					$result[$item_id]['paid_tng']+=($amount>0?$amount:0);
					$result[$item_id]['paidcount_tng']++;
				}else{
					$result[$item_id]['paid_manual']+=($amount>0?$amount:0);
					$result[$item_id]['paidcount_manual']++;
				}
			}  
		}else{
			$result[$item_id]['unpaid']+=($amount>0?$amount:0);
			$result[$item_id]['unpaidcount']++; 
		}
	}
	
	if(sizeof($result)>0){
		// makeup the 'total' field
		foreach($result as $item_id =>$values){
			$result[$item_id]['total'] = $values['paidcount'] + $values['unpaidcount'];
		}
		//use usort to sort the array
		sortByColumn2($result,$sort_fields[$field],$order);
		
		$j=0;
		foreach($result as $__item_id =>$values){
			$css = $j%2==0?"tableContent":"tableContent2";
			$j++;
			$item_id   = $values['itemid'];
			$item_name = $values['name'];
			$cat_name  = $values['catname'];
			$start_date= $values['startdate'];
			$end_date  = $values['enddate'];
			$status    = $values['status'];
			$paid	   = $values['paid'];
			$unpaid	   = $values['unpaid'];
			$paidcount = $values['paidcount'];
			$unpaidcount= $values['unpaidcount'];
			//$total  = $paidcount + $unpaidcount;
			$total = $values['total'];
			/*
			$x.="";
			$x.="\"$item_name\",";
			$x.="\"$cat_name\",";
			$x.="\"$start_date\",";
			$x.="\"$end_date\",";
			$x.="\"$status\",";
			$x.="\"$total\",";
			$x.="\"$paidcount\",";
			$x.="\"".round($paid,2)."\",";
			$x.="\"$unpaidcount\",";
			$x.="\"".round($unpaid,2)."\"";
			$x.="\n";
			*/
			$row = array($item_name, $cat_name, $start_date, $end_date, $status, $total, $paidcount, round($paid,2));
			if($sys_custom['ePayment']['TNG']){
				$row[] = $values['paidcount_tng'];
				$row[] = round($values['paid_tng'],2);
				$row[] = $values['paidcount_manual'];
				$row[] = round($values['paid_manual'],2);
			}
			$row[] = $unpaidcount;
			$row[] = round($unpaid,2);
			$rows[] = $row;
			//$rows[] = array($item_name, $cat_name, $start_date, $end_date, $status, $total,
			//				$paidcount, round($paid,2), $unpaidcount, round($unpaid,2)
			//				);
		}
	}
	
	
}else{ # no record
	//$x.="\"$i_no_record_exists_msg\"\n";
	$rows[] = $i_no_record_exists_msg;
}

//$x.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));

$display="\"$i_Payment_Menu_Report_PresetItem ($FromDate $i_Profile_To $ToDate)\"\n";
$utf_display="$i_Payment_Menu_Report_PresetItem ($FromDate $i_Profile_To $ToDate)\r\n";

//$display.=$x;

intranet_closedb();

$filename = "payment_item_report.csv";
//output2browser($display,$filename);

$export_content = $display.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>