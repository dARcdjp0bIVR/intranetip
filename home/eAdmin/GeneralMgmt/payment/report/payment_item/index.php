<?php
// Editing by 
/*
 * 2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
 * 2015-08-06 (Carlos): Get/Set date range cookies.
 * 2015-07-15 (Carlos): Added data sorting.
 * 2015-02-09 (Carlos): $sys_custom['ePayment']['PaymentItemDateRange'] - record date range are bound by StartDate or EndDate
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_PaymentItem";

$linterface = new interface_html();

$lb = new libdbtable("","","");

if(!isset($field) || $field == '' || !in_array($field, array(0,1,2,3,4,5,6,7,8,9))){
	$field = 3;  
}
if(!isset($order) || $order == '' || !in_array($order,array(0,1))){
	$order = 0;
}
$reverse_order = $order == 1? 0 : 1;
$sort_img = '<img name="sort_icon" id="sort_icon" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_'.($order==0?'a':'d').'_off.gif" align="absmiddle" border="0">';
$mouse_events = ' onmouseover="window.status=\'\';MM_swapImage(\'sort_icon\',\'\',\''.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_'.($order==0?'a':'d').'_on.gif\',1);return true;" onmouseout="window.status=\'\';MM_swapImgRestore();return true;" ';

$sort_fields = array('name','catname','startdate','enddate','status','total','paid');
if($sys_custom['ePayment']['TNG']){
	$sort_fields[] = 'paid_tng';
	$sort_fields[] = 'paid_manual';
}
$sort_fields[] = 'unpaid';

$field_offset = 0;
if($sys_custom['ePayment']['TNG']){
	$tng_header = $Lang['ePayment']['TNGPaidCount'].' ('.$Lang['ePayment']['TNGAmountPaid'].')';
	$field_offset += 1;
	$manual_pay_header = $Lang['ePayment']['ManualPaidCount'].' ('.$Lang['ePayment']['ManualAmountPaid'].')';
	$field_offset += 1;
}

$x="<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x.="<tr class=\"tablebluetop\">";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 0? $reverse_order : $order).",0,document.form1);\" ".($field==0?$mouse_events:"").">$i_Payment_Field_PaymentItem".($field==0?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 1? $reverse_order : $order).",1,document.form1);\" ".($field==1?$mouse_events:"").">$i_Payment_Field_PaymentCategory".($field==1?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 2? $reverse_order : $order).",2,document.form1);\" ".($field==2?$mouse_events:"").">$i_general_startdate".($field==2?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 3? $reverse_order : $order).",3,document.form1);\" ".($field==3?$mouse_events:"").">$i_general_enddate".($field==3?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 4? $reverse_order : $order).",4,document.form1);\" ".($field==4?$mouse_events:"").">$i_general_status".($field==4?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 5? $reverse_order : $order).",5,document.form1);\" ".($field==5?$mouse_events:"").">$i_Payment_Field_TotalPaidCount".($field==5?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 6? $reverse_order : $order).",6,document.form1);\" ".($field==6?$mouse_events:"").">$i_Payment_PresetPaymentItem_PaidStudentCount ($i_Payment_PresetPaymentItem_PaidAmount)".($field==6?$sort_img:"")."</a></td>";
if($sys_custom['ePayment']['TNG']){
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 7? $reverse_order : $order).",7,document.form1);\" ".($field==7?$mouse_events:"").">$tng_header".($field==7?$sort_img:"")."</a></td>";
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 8? $reverse_order : $order).",8,document.form1);\" ".($field==8?$mouse_events:"").">$manual_pay_header".($field==8?$sort_img:"")."</a></td>";
}
$x.="<td class=\"tabletoplink\"><a class=\"tabletoplink\" href=\"javascript:sortPage(".($field == 7+$field_offset? $reverse_order : $order).",".(7+$field_offset).",document.form1);\" ".($field==7+$field_offset?$mouse_events:"").">$i_Payment_PresetPaymentItem_UnpaidStudentCount ($i_Payment_PresetPaymentItem_UnpaidAmount)".($field==7+$field_offset?$sort_img:"")."</a></td>";
$x.="<td>&nbsp;</td>";
$x.="</tr>";


# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="" || !intranet_validateDate($FromDate))
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="" || !intranet_validateDate($ToDate))
	$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

$today = date('Y-m-d');

if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_cond =" (DATE_FORMAT(a.StartDate,'%Y-%m-%d') BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)."') AND (DATE_FORMAT(a.EndDate,'%Y-%m-%d') BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";
}else{
	$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."'";		
}

$cat_cond = $CatID==""?"":" AND a.CatID='".$lpayment->Get_Safe_Sql_Query($CatID)."' ";

$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond $cat_cond AND a.Name LIKE '%".$lpayment->Get_Safe_Sql_Like_Query($keyword)."%'";
$temp = $lpayment->returnVector($sql);
if(sizeof($temp)>0){
	$item_list = implode(",",$temp);
	$sql="SELECT a.ItemID, 
				a.Name,
				b.Name,
				a.StartDate,
				a.EndDate,
				IF('$today'<DATE_FORMAT(a.StartDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_NotStarted',
				IF('$today'>DATE_FORMAT(a.EndDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_Ended','$i_Payment_PresetPaymentItem_Progress')) AS Status
				FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
				WHERE a.ItemID IN($item_list) ORDER BY a.EndDate DESC";
	$temp = $lpayment->returnArray($sql,6);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name,$cat_name,$start_date,$end_date,$status)=$temp[$i];
		$result[$item_id]['itemid']     = $item_id;
		$result[$item_id]['name']		= $item_name;
		$result[$item_id]['catname']	= $cat_name;
		$result[$item_id]['startdate']	= $start_date;
		$result[$item_id]['enddate']	= $end_date;
		$result[$item_id]['status']		= $status;
		$result[$item_id]['paid']	= 0;
		$result[$item_id]['unpaid']= 0;
		$result[$item_id]['paidcount']	= 0;
		$result[$item_id]['unpaidcount']= 0;
		if($sys_custom['ePayment']['TNG']){
			$result[$item_id]['paid_tng']= 0;
			$result[$item_id]['paidcount_tng'] = 0;
			$result[$item_id]['paid_manual']= 0;
			$result[$item_id]['paidcount_manual'] = 0;
		}
	}
	$sql="SELECT b.ItemID,
				b.RecordStatus,
				b.Amount ";
	if($sys_custom['ePayment']['TNG']){
		$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus,t.ChargeStatus ";
	}
	$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS b ";
	if($sys_custom['ePayment']['TNG']){
		$sql .= " LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=b.PaymentID ";
	}
	$sql.=" WHERE b.ItemID IN ($item_list) 
			ORDER BY b.ItemID,b.RecordStatus";	
	$temp = $lpayment->returnArray($sql);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$record_status,$amount) = $temp[$i];
		if($record_status==1){ # Paid
			$result[$item_id]['paid']+=($amount>0?$amount:0);
			$result[$item_id]['paidcount']++;
			if($sys_custom['ePayment']['TNG']){
				if($temp[$i]['TNGRecordID']!='' && $temp[$i]['PaymentStatus']==1 && $temp[$i]['ChargeStatus']==1){
					$result[$item_id]['paid_tng']+=($amount>0?$amount:0);
					$result[$item_id]['paidcount_tng']++;
				}else{
					$result[$item_id]['paid_manual']+=($amount>0?$amount:0);
					$result[$item_id]['paidcount_manual']++;
				}
			}
		}else{
			$result[$item_id]['unpaid']+=($amount>0?$amount:0);
			$result[$item_id]['unpaidcount']++; 
		}
	}
	
	if(sizeof($result)>0){
		// makeup the 'total' field
		foreach($result as $item_id =>$values){
			$result[$item_id]['total'] = $values['paidcount'] + $values['unpaidcount'];
		}
		//use usort to sort the array
		sortByColumn2($result,$sort_fields[$field],$order);
		
		$j=0;
		foreach($result as $__item_id =>$values){
			$css =$j%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			$j++;
			$item_id   = $values['itemid'];
			$item_name = $values['name'];
			$cat_name  = $values['catname'];
			$start_date= $values['startdate'];
			$end_date  = $values['enddate'];
			$status    = $values['status'];
			$paid	   = $values['paid'];
			$unpaid	   = $values['unpaid'];
			$paidcount = $values['paidcount'];
			$unpaidcount= $values['unpaidcount'];
			//$total  = $paidcount + $unpaidcount;
			
			$total = $values['total'];
			
			$x.="<tr class=\"$css\">";
			$x.="<td class=\"tabletext\">$item_name</td>";
			$x.="<td class=\"tabletext\">$cat_name</td>";
			$x.="<td class=\"tabletext\">$start_date</td>";
			$x.="<td class=\"tabletext\">$end_date</td>";
			$x.="<td class=\"tabletext\">$status</td>";
			$x.="<td class=\"tabletext\">$total</td>";
			$x.="<td class=\"tabletext\">$paidcount ($".number_format($paid,2).")</td>";
			if($sys_custom['ePayment']['TNG']){
				$paid_tng = $lpayment->getWebDisplayAmountFormat($values['paid_tng']);
				$paidcount_tng = $values['paidcount_tng'];
				$tng_info = $paidcount_tng.' ('.$paid_tng.')';
				$x.="<td class=\"tabletext\">$tng_info</td>";
				
				$paid_manual = $lpayment->getWebDisplayAmountFormat($values['paid_manual']);
				$paidcount_manual = $values['paidcount_manual'];
				$manual_pay_info = ($paidcount_manual).' ('.$paid_manual.')';
				$x.="<td class=\"tabletext\">$manual_pay_info</td>";
			}
			$x.="<td class=\"tabletext\">$unpaidcount ($".number_format($unpaid,2).")</td>";
			$x.="<td class=\"tabletext\" nowrap>";
			$x.="<a href=\"class_stat.php?ItemID=$item_id\" class=\"button_link\" title=\"$i_Payment_PresetPaymentItem_ClassStat\">";
			$x.="<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_classview.gif\" border=0></a>&nbsp;";
			$x.="<a href=\"personal_report.php?ItemID=$item_id\" class=\"button_link\" title=\"$i_Payment_PresetPaymentItem_Personal\">";
			$x.="<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_studentview.gif\" border=\"0\"></a></td>";
			$x.="</tr>";
		}
	}
	
}else{ # no record
	$x.="<tr class=\"tablebluerow2 tabletext\"><td class=\"tabletext\" colspan=\"".(9+$field_offset)."\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg<td></tr>";
}


$x.="</table>";

$display=$x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);

$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID ",$CatID,1);

$TAGS_OBJ[] = array($i_Payment_Menu_Report_PresetItem, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
    function checkForm(formObj){
        if(formObj==null) return false;
        fromV = formObj.FromDate;
        toV= formObj.ToDate;
        if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
        }
        else if(!checkDate(toV)){
            //formObj.ToDate.focus();
            return false;
        }
        return true;
    }
    
    function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
    }
    
    function submitForm(obj){
        if(checkForm(obj))
        	obj.submit();
    }
    
    function openPrintPage()
	{
		newWindow("print.php?FromDate=<?=urlencode($FromDate)?>&ToDate=<?=urlencode($ToDate)?>&CatID=<?=urlencode($CatID)?>&keyword=<?=urlencode($keyword)?>&field=<?=urlencode($field)?>&order=<?=urlencode($order)?>",10);
	}
	
	function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;
	        
	}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get" action=''>
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
		<?=$i_Payment_Field_PaymentCategory?>:
		</td>
		<Td><?=$select_cat?></td></tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
		<?=$i_Payment_Field_PaymentItem?>:
		</td>
		<td><input type="text" name="keyword" value="<?=escape_double_quotes($keyword)?>"></td></tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="FromDate" id="FromDate" value="<?=escape_double_quotes($FromDate)?>" maxlength="10" class="textboxnum">
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="ToDate" id="ToDate" value="<?=escape_double_quotes($ToDate)?>" maxlength="10" class='textboxnum'>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="field" id="field" value="<?=escape_double_quotes($field)?>" />
<input type="hidden" name="order" id="order" value="<?=escape_double_quotes($order)?>" />
<input type="hidden" name="pageNo" id="pageNo" value="1" />
</form>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
</table>
<?=$display?>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<br />
<?php
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>
