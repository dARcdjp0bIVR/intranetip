<?php
// Editing by 
/*
 * 2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
 * 2017-07-04 (Carlos): Added display column [Transaction Time] as paid time.
 * 2017-02-10 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - display STRN, Payment Method, Receipt Remark.
 * 2015-12-29 (Carlos): $sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display column [Login ID].
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

if($ItemID=="") return;

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$lpayment = new libpayment();

$paid_cond ="";

$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";

$sql=" SELECT a.StudentID, 
				a.Amount, 
				a.RecordStatus, 
				a.PaidTime,
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, $archive_namefield, $namefield), 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName, b.ClassName),
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassNumber,b.ClassNumber) ";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$sql.= " ,IF(b.UserID IS NOT NULL,b.UserLogin,c.UserLogin) as UserLogin ";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$sql.= " ,IF(b.UserID IS NOT NULL,b.STRN,c.STRN) as STRN ";
	$sql.=" ,CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod ";
	$sql .= " ,a.ReceiptRemark ";
}
if($sys_custom['ePayment']['TNG']){
	$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus as TNGPaymentStatus,t.ChargeStatus as TNGChargeStatus ";	
}
$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
}
$sql.=" LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber+0,c.ClassName,c.ClassNumber";


/*
$sql=" SELECT a.StudentID, a.Amount, a.RecordStatus, $namefield, b.ClassName,b.ClassNumber FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber";
*/
		
$temp = $lpayment->returnArray($sql);
/*
$y="\"$i_ClassName\","; 
$y.="\"$i_ClassNumber\",";
$y.="\"$i_UserName\","; 
$y.="\"$i_Payment_Field_Amount\","; 
$y.="\"$i_general_status\""; 
$y.="\n";
*/
$colspan = 6;

$exportColumn = array();
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$exportColumn[] = $Lang['AccountMgmt']['LoginID'];
	$colspan++;
}
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_ClassNumber;
if($sys_custom['ePayment']['HartsPreschool']){
	$exportColumn[] = $i_STRN;
	$colspan++;
}
$exportColumn[] = $i_UserName;
$exportColumn[] = $i_Payment_Field_Amount;
$exportColumn[] = $i_general_status;
$exportColumn[] = $i_Payment_Field_TransactionTime;
if($sys_custom['ePayment']['HartsPreschool']){
	$exportColumn[] = $Lang['ePayment']['PaymentMethod'];
	$exportColumn[] = $Lang['ePayment']['PaymentRemark'];
	$colspan+=2;
}


$paidcount =0;
$unpaidcount =0;
$paid=0;
$unpaid=0;
$noRecord=false;
if($sys_custom['ePayment']['TNG']){
	$tng_paidcount = 0;
	$tng_paid = 0;
}

if(sizeof($temp)>0){

	for($i=0;$i<sizeof($temp);$i++){
		list($sid,$amount,$isPaid,$paid_time,$sname,$class_name,$class_num) = $temp[$i];
		if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
			$user_login = $temp[$i]['UserLogin'];	
		}
		if($isPaid){
			$paidcount++;
			$paid+=$amount+0;
			if($filter_paid=="" || $filter_paid==1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_PaidCount;
				$result[$sid]['paidtime'] = $paid_time;
				if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
					$result[$sid]['userlogin'] = $user_login;
				}
				if($sys_custom['ePayment']['HartsPreschool']){
					$result[$sid]['strn'] = $temp[$i]['STRN'];
					$result[$sid]['paymentmethod'] = $temp[$i]['PaymentMethod'];
					$result[$sid]['receiptremark'] = $temp[$i]['ReceiptRemark'];
				}
				if($sys_custom['ePayment']['TNG'] && $temp[$i]['TNGRecordID']!='' && $temp[$i]['TNGPaymentStatus']==1 && $temp[$i]['TNGChargeStatus']==1){
					$result[$sid]['paid_by_tng'] = true;
					$tng_paidcount++;
					$tng_paid += $amount+0;
				}
			}
		}else{
			$unpaidcount++;
			$unpaid+=$amount+0;
			if($filter_paid!=1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_UnpaidCount;
				$result[$sid]['paidtime'] = $paid_time;
				if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
					$result[$sid]['userlogin'] = $user_login;
				}
				if($sys_custom['ePayment']['HartsPreschool']){
					$result[$sid]['strn'] = $temp[$i]['STRN'];
					$result[$sid]['paymentmethod'] = $temp[$i]['PaymentMethod'];
					$result[$sid]['receiptremark'] = $temp[$i]['ReceiptRemark'];
				}
			}
		}
	}
	if(sizeof($result)>0){
		foreach($result as $sid => $values){
			$r_name 		= $values['name'];
			$r_class 		= $values['class'];
			$r_classnum	= $values['classnumber'];
			$r_amount 	= $values['amount'];
			$r_status 	= $values['status'];
			$r_paid_time = $values['paidtime'];
			if($sys_custom['ePayment']['TNG'] && $values['paid_by_tng']){
				$r_status .= ' (TNG)';
			}
			/*
			$y.="\"$r_class\",";
			$y.="\"$r_classnum\",";
			$y.="\"$r_name\",";
			$y.="\"".round($r_amount,2)."\",";
			$y.="\"$r_status\"";
			$y.="\n";
			*/
			$tmp_row = array();
			if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
				$tmp_row[] = $values['userlogin'];
			}
			$tmp_row[] = $r_class;
			$tmp_row[] = $r_classnum;
			if($sys_custom['ePayment']['HartsPreschool']){
				$tmp_row[] = $values['strn'];
			}
			$tmp_row[] = $r_name;
			$tmp_row[] = round($r_amount,2);
			$tmp_row[] = $r_status;
			$tmp_row[] = $r_paid_time;
			if($sys_custom['ePayment']['HartsPreschool']){
				$tmp_row[] = $values['paymentmethod'];
				$tmp_row[] = $values['receiptremark'];
			}
			
			$rows[] = $tmp_row;
		}
	}else{ # no record
		$noRecord = true;
	}
}else $noRecord = true;
if($noRecord){
		//$y.="\"$i_no_record_exists_msg\"\n";
		$rows[] = $i_no_record_exists_msg;
}


# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];
/*
$x="\"$i_Payment_PresetPaymentItem_Personal\"\n";
$x.="\"$i_Payment_Field_PaymentItem\",\"$item_name\"\n";
$x.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaymentPeriod\",\"$start_date $i_To $end_date\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaidAmount\",\"".round($paid,2)."\",\"$i_Payment_PresetPaymentItem_PaidStudentCount\",\"$paidcount\"\n";
$x.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\",\"".round($unpaid,2)."\",\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\",\"$unpaidcount\"\n";
*/
$preheader=$i_Payment_PresetPaymentItem_Personal."\r\n";
$preheader.=$i_Payment_Field_PaymentItem."\t".$item_name."\r\n";
$preheader.=$i_Payment_Field_PaymentCategory."\t".$cat_name."\r\n";
$preheader.=$i_Payment_PresetPaymentItem_PaymentPeriod."\t".$start_date." ".$i_To." ".$end_date."\r\n";
$preheader.=$i_Payment_PresetPaymentItem_PaidAmount."\t".round($paid,2)."\t".$i_Payment_PresetPaymentItem_PaidStudentCount."\t".$paidcount."\r\n";
if($sys_custom['ePayment']['TNG']){
	$preheader.=$Lang['ePayment']['TNGAmountPaid']."\t".round($tng_paid,2)."\t".$Lang['ePayment']['TNGPaidCount']."\t".$tng_paidcount."\r\n";
	$preheader.=$Lang['ePayment']['ManualAmountPaid']."\t".round($paid - $tng_paid,2)."\t".$Lang['ePayment']['ManualPaidCount']."\t".($paidcount - $tng_paidcount)."\r\n";	
}
$preheader.=$i_Payment_PresetPaymentItem_UnpaidAmount."\t".round($unpaid,2)."\t".$i_Payment_PresetPaymentItem_UnpaidStudentCount."\t".$unpaidcount."\r\n";

//$display=$x.$y;

//$display.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));

$filename="personal_report.csv";

//output2browser($display,$filename);

$export_content = $preheader.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>