<?php
// Editing by 
######### Change Log [Start] #########
#
#	2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
#	2010-03-24 YatWoon : update the query don't order by "b.ClassName,c.ClassName"
#
######### Change Log [End] #########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$linterface = new interface_html();


# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x="<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\">";
$x.="<tr><td colspan=\"2\" class=\"eSportprinttext\"><b>$i_Payment_PresetPaymentItem_ClassStat</b></td></tr>";
$x.="<tr><td class=\"eSportprinttext\">$i_Payment_Field_PaymentItem:</td><td class=\"eSportprinttext\" width=\"70%\">$item_name</td></tr>";
$x.="<tr><td class=\"eSportprinttext\">$i_Payment_Field_PaymentCategory:</td><td class=\"eSportprinttext\" width=\"70%\">$cat_name</td></tr>";
$x.="<tr><td class=\"eSportprinttext\">$i_Payment_PresetPaymentItem_PaymentPeriod:</td><td class=\"eSportprinttext\" width=\"70%\">$start_date $i_To $end_date</td></tr>";
$x.="</table><br />";


$sql=" SELECT a.Amount, 
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) as ClassNameTmp ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus as TNGPaymentStatus,t.ChargeStatus as TNGChargeStatus ";	
}
$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
}
$sql.=" LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY ClassNameTmp";
		
$temp = $lpayment->returnArray($sql);

$colspan = 0;
if($sys_custom['ePayment']['TNG']){
	$colspan += 4;
}

$y="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$y.="<tr>";
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_ClassName</td>"; 
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_TotalPaidCount</td>"; 
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PresetPaymentItem_PaidStudentCount</td>";
if($sys_custom['ePayment']['TNG']){
	$y.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['TNGPaidCount']."</td>";
	$y.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['ManualPaidCount']."</td>";
} 
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PresetPaymentItem_UnpaidStudentCount</td>";  
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PresetPaymentItem_PaidAmount</td>";
if($sys_custom['ePayment']['TNG']){
	$y.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['TNGAmountPaid']."</td>";
	$y.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['ManualAmountPaid']."</td>";
} 
$y.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PresetPaymentItem_UnpaidAmount</td>"; 
$y.="</tr>";

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
			if($sys_custom['ePayment']['TNG'] && $temp[$i]['TNGRecordID']!='' && $temp[$i]['TNGPaymentStatus']==1 && $temp[$i]['TNGChargeStatus']==1){
				$result["$class_name"]['tng_paidcount']++;
				$result["$class_name"]['tng_paid']+=$amount+0;
			}
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
	}
	
	$overall_total =0;
	$overall_paid  =0;
	$overall_paidcount=0;
	$overall_unpaid =0;
	$overall_unpaidcount=0;
	if($sys_custom['ePayment']['TNG'])
	{
		$overall_tng_paid = 0;
		$overall_tng_paidcount = 0;
	}
	
	$j=0;
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'];
		$unpaid_count = $values['unpaidcount'];
		$paid = $values['paid'];
		$unpaid=$values['unpaid'];

		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		
		$css = $j%2==0?"attendancepresent":"attendanceouting";
		$y.="<tr class=\"$css\">";
		$y.="<td class=\"eSporttdborder eSportprinttext\">$class &nbsp;</td>";
		$y.="<td class=\"eSporttdborder eSportprinttext\">$total</td>";
		$y.="<td class=\"eSporttdborder eSportprinttext\">".($paid_count==""?0:$paid_count)."</td>";
		if($sys_custom['ePayment']['TNG']){
			$overall_tng_paid += $values['tng_paid'] + 0;
			$overall_tng_paidcount += $values['tng_paidcount'] + 0;
			$y.="<td class=\"eSporttdborder eSportprinttext\">".($values['tng_paidcount']+0)."</td>";
			$y.="<td class=\"eSporttdborder eSportprinttext\">".($paid_count-$values['tng_paidcount'])."</td>";
		}
		$y.="<td class=\"eSporttdborder eSportprinttext\">".($unpaid_count==""?0:$unpaid_count)."</td>";
		$y.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($paid,2)."</td>";
		if($sys_custom['ePayment']['TNG']){
			$y.="<td class=\"eSporttdborder eSportprinttext\">".$lpayment->getWebDisplayAmountFormat($values['tng_paid'])."</td>";
			$y.="<td class=\"eSporttdborder eSportprinttext\">".$lpayment->getWebDisplayAmountFormat($paid-$values['tng_paid'])."</td>";
		}
		$y.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($unpaid,2)."</td>";
		$y.="</tr>";
		$j++;
	}
	$y.="<tr>";
	$y.="<td class=\"eSporttdborder eSportprinttext\">$i_Payment_SchoolAccount_Total</td>";
	$y.="<td class=\"eSporttdborder eSportprinttext\">$overall_total</td>";
	$y.="<td class=\"eSporttdborder eSportprinttext\">$overall_paidcount</td>";
	if($sys_custom['ePayment']['TNG']){
		$y.="<td class=\"eSporttdborder eSportprinttext\">$overall_tng_paidcount</td>";
		$y.="<td class=\"eSporttdborder eSportprinttext\">".($overall_paidcount-$overall_tng_paidcount)."</td>";
	}
	$y.="<td class=\"eSporttdborder eSportprinttext\">$overall_unpaidcount</td>";
	$y.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($overall_paid,2)."</td>";
	if($sys_custom['ePayment']['TNG']){
		$y.="<td class=\"eSporttdborder eSportprinttext\">".$lpayment->getWebDisplayAmountFormat($overall_tng_paid)."</td>";
		$y.="<td class=\"eSporttdborder eSportprinttext\">".$lpayment->getWebDisplayAmountFormat($overall_paid-$overall_tng_paid)."</td>";
	}
	$y.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($overall_unpaid,2)."</td>";
	$y.="</tr>";
	
}else{ # no record
	$y.="<tr><td colspan=\"".(6+$colspan)."\" align=\"center\" class=\"eSporttdborder eSportprinttext\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg<td></tr>";
}


$y.="</table>\n";

$display=$y;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?= $x ?>
<?= $display ?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
