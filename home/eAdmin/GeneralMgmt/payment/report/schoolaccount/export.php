<?php
# using: 
########################################################
#	Date:	2020-04-24 Ray
#			Add last_modify order
#
#	Date:	2019-08-07  Carlos
#			Addd more credit types - top-up of TNG, AlipayHK, FPS and tap & Go.
#
#	Date:	2016-12-12 	Carlos
#			Added transaction date type option.
#
#	Date:	2016-11-03	Carlos
#			Improved payment item type transaction to first priority use PAYMENT_PAYMENT_ITEM.Name as transaction name, 
#			in order to avoid same payment item displayed as several items if payment item has ever changed its name, 
#			and some students paid before and some students paid after name changing. 
#
#	Date:	2016-08-24  Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
#
#	Date:	2013-10-29	Carlos
#			KIS - hide [Income] and [Total Income]
#
#	Date:	2011-11-18	YatWoon
#			Improved: display notice title in the payment item [Case#2011-1115-1426-21072]
#
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lexport = new libexporttext();

$lpayment = new libpayment();


# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

if($SearchBy == 2){ // by transaction time
	$date_cond = " IF(a.TransactionType=1 AND c.TransactionID IS NOT NULL,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')<='$ToDate',DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate') ";
}else{ // by input time
	$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate' ";
}


$records_field = $field;
$records_order = $order;

$sql="SELECT a.Amount,
                if(f.NoticeID is null, IF(a.TransactionType=2 AND e.ItemID IS NOT NULL,e.Name,a.Details), concat(f.Title,' - ',a.Details)),
                a.TransactionType,
                c.RecordType,
				a.TransactionTime                
      FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a 
        LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID 
        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID 
		left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID 
		left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID 
      WHERE $date_cond ORDER BY a.TransactionTime ASC";

$li = new libpayment();
$temp = $li->returnArray($sql,4);


$ary_types = array(1,2,3,6,7,8,9,10,11);

$x="\"$i_Payment_SchoolAccount_Detailed_Transaction\",";
if(!$isKIS){
	$x.="\"$i_Payment_SchoolAccount_Deposit\",";
}
$x.="\"\",";
$x.="\"$i_Payment_SchoolAccount_Expenses\",\"$i_Form_LastModified\"\n";
if($isKIS){
	$x.="\"\",";
}else{
	$x.="\"\",\"\",";
}
$x.="\"$i_Payment_SchoolAccount_PresetItem\",\"$i_Payment_SchoolAccount_SinglePurchase\",\"$i_Payment_SchoolAccount_Other\"\n";

$utf_x=$i_Payment_SchoolAccount_Detailed_Transaction."\t";
if(!$isKIS){
	$utf_x.=$i_Payment_SchoolAccount_Deposit."\t";
}
$utf_x.="\t";
$utf_x.=$i_Payment_SchoolAccount_Expenses."\t".$i_Form_LastModified."\t\r\n";
if($isKIS){
	$utf_x.="\t";
}else{
	$utf_x.="\t\t";
}
$utf_x.=$i_Payment_SchoolAccount_PresetItem."\t".$i_Payment_SchoolAccount_SinglePurchase."\t".$i_Payment_SchoolAccount_Other."\r\n";



$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;

$result = array();
for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type, $credit_transaction_type, $transaction_time) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type]['amount'] += $amount;
                                       $result[$type][$credit_transaction_type]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                        
                        case 9        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                                       
                        case 11        :
                                       $result[$type]["all"]['amount'] += $amount+0;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;
                                       
                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{
	$list_data = array();
	if(!$isKIS){
        # deposit
        $list_item = $result[1];
        if(sizeof($list_item)>0){
                foreach($list_item as $credit_type => $temp){
						$t_total = $temp['amount'];
                        switch ($credit_type)
                        {
                                case 1: $name = $i_Payment_Credit_TypePPS; break;
                                case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
                                case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
                                case 4: $name = $Lang['ePayment']['AutoPay'];break;
	                        	case 6: $name = $Lang['ePayment']['BankTransfer'];break;
	                        	case 7: $name = $Lang['ePayment']['ChequeDeposit'];break;
	                        	case 8: $name = $Lang['ePayment']['TNGTopUp'];break;
		                        case 9: $name = $Lang['ePayment']['AlipayTopUp'];break;
		                        case 10:$name = $Lang['ePayment']['FPSTopUp'];break;
		                        case 11:$name = $Lang['ePayment']['TapAndGoTopUp'];break;
								case 12:$name = $Lang['ePayment']['VisaMasterTopUp'];break;
								case 13:$name = $Lang['ePayment']['WeChatTopUp'];break;
								case 14:$name = $Lang['ePayment']['AlipayCNTopUp'];break;
                                default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";

                        }
                        $name = intranet_undo_htmlspecialchars($name);
						if(!$isKIS) {
							$list_data[] = array($name, round($t_total, 2), "", "", "", $temp['last_modify']);
						} else {
							$list_data[] = array($name, "", "", "", $temp['last_modify']);
						}
                        //if($isKIS){
                        //	$x.="\"$name\"\n";
                        //	$utf_x.=$name."\r\n";
                        //}else{
                        //	$x.="\"$name\",\"".round($t_total,2)."\"\n";
                        //	$utf_x.=$name."\t".round($t_total,2)."\r\n";
                        //}
                }
        }
	}
        # payment cancellation
        $list_item = $result[6];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                	$t_total = $temp['amount'];
                    $name = intranet_undo_htmlspecialchars($name);
					if(!$isKIS) {
						$list_data[] = array($name, number_format($t_total, 2), "", "", "", $temp['last_modify']);
					} else {
						$list_data[] = array($name, "", "", "", $temp['last_modify']);
					}
                    //if($isKIS){
                    //	$x.="\"$name\"\n";
                    //    $utf_x.=$name."\r\n";
                    //}else{
                    //    $x.="\"$name\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t".round($t_total,2)."\r\n";
                    //}
                }
        }
				
				# POS Void Transaction Refund
				$list_item = $result[11];
				if(sizeof($list_item)>0){
					$item_name = $Lang['ePOS']['ReportVoidTitle'];
					$s_total = $list_item['all']['amount'];
					if(!$isKIS) {
						$list_data[] = array($item_name, $lpayment->getExportAmountFormat($s_total), "", "", "", $list_item['all']['last_modify']);
					} else {
						$list_data[] = array($item_name, "", "", "", $list_item['all']['last_modify']);
					}
					//if($isKIS){
					//	$x.="\"$item_name\",\"\",\"\",\"\"\n";
         			// 	$utf_x.=$item_name."\t\t\t\r\n";
					//}else{
					//	$x.="\"$item_name\",\"".$lpayment->getExportAmountFormat($s_total)."\",\"\",\"\",\"\"\n";
         			// 	$utf_x.=$item_name."\t".$lpayment->getExportAmountFormat($s_total)."\t\t\t\r\n";
					//}
				}
				
        # payment item
        $list_item = $result[2];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                	$t_total = $temp['amount'];
                    $name = intranet_undo_htmlspecialchars($name);
					if(!$isKIS) {
						$list_data[] = array($name, "", round($t_total, 2), "", "", $temp['last_modify']);
					} else {
						$list_data[] = array($name, round($t_total, 2), "", "", $temp['last_modify']);
					}
                    //if($isKIS){
                    //	$x.="\"$name\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t".round($t_total,2)."\r\n";
                    //}else{
                    //    $x.="\"$name\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t\t".round($t_total,2)."\r\n";
                    //}
                }
        }

        # single purchase
        $list_item = $result[3];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                	$t_total = $temp['amount'];
                    $name = intranet_undo_htmlspecialchars($name);
					if(!$isKIS) {
						$list_data[] = array($name, "", "", round($t_total, 2), "", $temp['last_modify']);
					} else {
						$list_data[] = array($name, "", round($t_total, 2), "", $temp['last_modify']);
					}
                    //if($isKIS){
                    //	$x.="\"$name\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t\t".round($t_total,2)."\r\n";
                    //}else{
                    //    $x.="\"$name\",\"\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t\t\t".round($t_total,2)."\r\n";
                    //}
                }
        }

        # refund
        $list_item = $result[7];
        if(sizeof($list_item)>0){
                $item_name = $i_Payment_action_refund;
                foreach($list_item as $name => $temp){
                	$t_total = $temp['amount'];
                    $item_name = intranet_undo_htmlspecialchars($item_name);
					if(!$isKIS) {
						$list_data[] = array($item_name, "", "", "", round($t_total, 2), $temp['last_modify']);
					} else {
						$list_data[] = array($item_name, "", "", round($t_total, 2), $temp['last_modify']);
					}
                    //if($isKIS){
                    //	$x.="\"$item_name\",\"\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$item_name."\t\t\t".round($t_total,2)."\r\n";
                    //}else{
                    //    $x.="\"$item_name\",\"\",\"\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$item_name."\t\t\t\t".round($t_total,2)."\r\n";
                    //}
                }
        }

        # pps charges
        $list_item = $result[8];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                	$t_total = $temp['amount'];
                    $name = intranet_undo_htmlspecialchars($i_Payment_PPS_Charge);
					if(!$isKIS) {
						$list_data[] = array($name, "", "", "", round($t_total, 2), $temp['last_modify']);
					} else {
						$list_data[] = array($name, "", "", round($t_total, 2), $temp['last_modify']);
					}
                    //if($isKIS){
                    //	$x.="\"$name\",\"\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t\t\t".round($t_total,2)."\r\n";
                    //}else{
                    //    $x.="\"$name\",\"\",\"\",\"\",\"".round($t_total,2)."\"\n";
                    //    $utf_x.=$name."\t\t\t\t".round($t_total,2)."\r\n";
                   // }
                }
        }

				# Cancel Deposit Charge
        $list_item = $result[9];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['CancelDepositDescription'];
          $s_total =0;
          $last_modify = "";
          foreach($list_item as $name => $temp){
          	  $s_total+=$temp['amount'];
			  $last_modify = $temp['last_modify'];
          }
          
          $item_name = intranet_undo_htmlspecialchars($item_name);
          if(!$isKIS) {
			  $list_data[] = array($item_name, "", "", "", $lpayment->getExportAmountFormat($s_total), $last_modify);
		  } else {
			  $list_data[] = array($item_name, "", "", $lpayment->getExportAmountFormat($s_total), $last_modify);
		  }
          //if($isKIS){
          //	$x.="\"$item_name\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          //	$utf_x.=$item_name."\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
          //}else{
          //	$x.="\"$item_name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          //	$utf_x.=$item_name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
          //}
        }
        
        # Donation to school
        $list_item = $result[10];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['DonateBalanceDescription'];
          $s_total =0;
          $last_modify = "";
          foreach($list_item as $name => $temp){
                  $s_total+=$temp['amount'];
			  $last_modify = $temp['last_modify'];
          }
          
          $item_name = intranet_undo_htmlspecialchars($item_name);
          if(!$isKIS) {
			  $list_data[] = array($item_name, "", "", "", $lpayment->getExportAmountFormat($s_total), $last_modify);
		  } else {
			  $list_data[] = array($item_name, "", "", $lpayment->getExportAmountFormat($s_total), $last_modify);
		  }
          //if($isKIS){
          //	$x.="\"$item_name\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          //	$utf_x.=$item_name."\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
          //}else{
          //	$x.="\"$item_name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          //	$utf_x.=$item_name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
          //}
        }

	usort($list_data, "sort_list_data");
	foreach($list_data as $temp) {
		$x.="\"".$temp[0]."\",\"".$temp[1]."\",\"".$temp[2]."\",\"".$temp[3]."\",\"".$temp[4]."\"";
		$utf_x.=$temp[0]."\t".$temp[1]."\t".$temp[2]."\t".$temp[3]."\t".$temp[4];
		if(!$isKIS) {
			$x .= ",\"".$temp[5]."\"\n";
			$utf_x .= "\t".$temp[5]."\r\n";
		} else {
			$x .= "\n";
			$utf_x .= "\r\n";
		}

	}

        # Total
        $total_income = $income+0;
		
		if($isKIS){
			$x.="\"$i_Payment_SchoolAccount_Total\",\"".round($expense_item,2)."\",\"".round($expense_single,2)."\",\"".round($expense_other,2)."\"\n";
        	$utf_x.=$i_Payment_SchoolAccount_Total."\t".round($expense_item,2)."\t".round($expense_single,2)."\t".round($expense_other,2)."\r\n";
		}else{
        	$x.="\"$i_Payment_SchoolAccount_Total\",\"".round($total_income,2)."\",\"".round($expense_item,2)."\",\"".round($expense_single,2)."\",\"".round($expense_other,2)."\"\n";
        	$utf_x.=$i_Payment_SchoolAccount_Total."\t".round($total_income,2)."\t".round($expense_item,2)."\t".round($expense_single,2)."\t".round($expense_other,2)."\r\n";
		}
        
        # Summary
        $total_expense = $expense_item + $expense_single + $expense_other+0;
        $net_income = $total_income - $total_expense;
        $net_income_str = round($net_income,2);
        $y = "";
        if(!$isKIS){
        	$y .="\"$i_Payment_SchoolAccount_TotalIncome\",\"".round($total_income,2)."\"\n";
        }
        $y.="\"$i_Payment_SchoolAccount_TotalExpense\",\"".round($total_expense,2)."\"\n";
        $y.="\"$i_Payment_SchoolAccount_NetIncomeExpense\",\"$net_income_str\"\n\n";

		$utf_y = "";
		if(!$isKIS){
        	$utf_y.=$i_Payment_SchoolAccount_TotalIncome."\t".round($total_income,2)."\r\n";
		}
        $utf_y.=$i_Payment_SchoolAccount_TotalExpense."\t".round($total_expense,2)."\r\n";
        $utf_y.=$i_Payment_SchoolAccount_NetIncomeExpense."\t".$net_income_str."\r\n\r\n";
}


$content = "\"$i_Payment_Menu_Report_SchoolAccount ( $FromDate $i_Profile_To $ToDate )\"\n\n";
$content.=$y.$x;
$content.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";


$utf_content = $i_Payment_Menu_Report_SchoolAccount." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\r\n\r\n";

$utf_content.=$utf_y.$utf_x;
$utf_content.=$i_general_report_creation_time."\t".date('Y-m-d H:i:s')."\r\n";

function sort_list_data($a, $b)
{
	global $records_order, $records_field, $isKIS;
	if($records_field == 0) {
		$ret = strnatcasecmp($a[0], $b[0]);
		if($records_order == '1') {
			return $ret;
		} else {
			return $ret * -1;
		}
	}

	if($records_field == 1) {
		$a_t = str_replace(array('$',','),'', $a[1]);
		$b_t = str_replace(array('$',','),'', $b[1]);
		if($a_t == $b_t) {
			return 0;
		}
		if($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
	}

	if($records_field == 2) {
		$i = ($isKIS) ? 4 : 5;
		$a_t = strtotime($a[$i]);
		$b_t = strtotime($b[$i]);
		if ($a_t == $b_t) {
			return 0;
		}
		if ($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
	}
}
intranet_closedb();

$filename = "school_account.csv";
$lexport->EXPORT_FILE($filename, $utf_content);
?>
