<?php
// Editing by 
/*
 * 2013-08-28 (Carlos): Created
 * 2013-10-29 (Carlos): Hide [Add value Record Time], [Credit], [Balance After]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$lclass = new libclass();
$lexport = new libexporttext();

if(($_REQUEST['UserType']=='')||($_REQUEST['UserType']==0))
	$cond1 = "RecordType IN (1,2)";
if($_REQUEST['UserType']==1)
	$cond1 = "RecordType IN (1)";
if($_REQUEST['UserType']==2) {
	$cond1 = "RecordType IN (2)";
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($TargetStartDate=="")
	$TargetStartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="")
	$TargetEndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

if ($TargetClass != "") 
	$ClassName = $lclass->getClassName($TargetClass);

$payment_account_result = array();
if (sizeof($UserStatus)>0) {
	
	$BalanceField = $lpayment->getExportAmountFormatDB("b.Balance");
	
	$cond2 = " AND RecordStatus IN (".implode(",",$UserStatus).")";
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT 
					a.UserID, 
					$namefield as UserName, 
					IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo) as PPSAccountNo, 
					".$BalanceField." as Balance, 
					if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated) as LastUpdated, 
					CASE 
						WHEN a.RecordStatus = 0 THEN '".$i_status_suspended."' 
						WHEN a.RecordStatus = 1 THEN '".$i_status_approved."' 
						WHEN a.RecordStatus = 2 THEN '".$i_status_pendinguser."' 
						WHEN a.RecordStatus = 3 THEN '".$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp."' 
						ELSE '' 
					END as Status
			FROM 
					INTRANET_USER AS a 
					LEFT JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE 
					".$cond1." 
					".$cond2." 
					".(($TargetClass != "")? "AND ClassName ='".$ClassName."'":"")." 
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
	$payment_account_result = $lpayment->returnArray($sql,6);
}

if ($GetArchive) {
	$ArchiveNamefield = ($intranet_session_language == 'en')? 'au.EnglishName':'au.ChineseName';
	
	$BalanceField = $lpayment->getExportAmountFormatDB("0.0");
	
	$sql = "select 
						au.UserID,
						".$ArchiveNamefield." as UserName, 
						'--' as PPSAccountNo,
						'0.0' as Balance,
						'--' as LastUpdated, 
						'".$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived."' as Status 
					from 
						INTRANET_ARCHIVE_USER as au 
					where 
						".$cond1." 
						".(($TargetClass != "")? "AND ClassName ='".$ClassName."'":"")." 
					ORDER BY
						au.ClassName, au.ClassNumber, au.EnglishName";
	
	$ArchiveUser_Result = $lpayment->returnArray($sql,6);
	$payment_account_result = array_merge($payment_account_result,$ArchiveUser_Result);
}

$ExportRow = array();
if($isKIS){
	$HeaderRow = array($i_Payment_Field_Username, $i_Payment_Field_TransactionTime,$i_Payment_Field_TransactionType,$i_Payment_TransactionType_Debit,$i_Payment_Field_TransactionDetail,$i_Payment_Field_RefCode);
}else{
	$HeaderRow = array($i_Payment_Field_Username, /* $i_general_status,$i_Payment_Field_Balance,$i_Payment_Field_LastUpdated,$i_Payment_Field_PPSAccountNo,*/ $i_Payment_Field_TransactionTime,$i_Payment_Field_TransactionFileTime,$i_Payment_Field_TransactionType,$i_Payment_TransactionType_Credit,$i_Payment_TransactionType_Debit,$i_Payment_Field_TransactionDetail,$i_Payment_Field_BalanceAfterTransaction,$i_Payment_Field_RefCode);
}

if(sizeof($payment_account_result)>0)
{
	$lineRemain = $defaultNumOfLine - $page_header_linefeed;
	
	for($i=0; $i<sizeof($payment_account_result); $i++)
	{
		list($u_id, $name, $pps, $bal, $last_modified, $Status) = $payment_account_result[$i];

		$AmountField = $lpayment->getExportAmountFormatDB("a.Amount");
		$BalanceAfterField = $lpayment->getExportAmountFormatDB("a.BalanceAfter");
		
		$sql2  = "SELECT
								DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
								IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
								CASE a.TransactionType
								    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
								    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
								    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
								    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
								    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
								    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
								    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
								    WHEN 8 THEN '$i_Payment_PPS_Charge'
								    ELSE '$i_Payment_TransactionType_Other' END,
								IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$AmountField.",' -- '),
								IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$AmountField.",' -- '),
								a.Details, ".$BalanceAfterField.",
								IF(a.RefCode IS NULL, ' -- ', a.RefCode)
							FROM
								PAYMENT_OVERALL_TRANSACTION_LOG as a
								LEFT OUTER JOIN 
								PAYMENT_CREDIT_TRANSACTION as b 
								ON a.RelatedTransactionID = b.TransactionID
							WHERE
								a.StudentID =$u_id 
								AND 
								a.TransactionTime between '".$TargetStartDate."' AND '".$TargetEndDate." 23:59:59'";
		
    	$payment_detail_result = $lpayment->returnArray($sql2,7);
	
		if(sizeof($payment_detail_result)>0)
		{
			for($j=0; $j<sizeof($payment_detail_result); $j++)
			{
				list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
				if($isKIS) {
					$ExportRow[] = array($name,$transaction_time,$transaction_type,$debit_amount,$transaction_detail,$ref_no);
				}else{
					$ExportRow[] = array($name,/*$Status,$bal,$last_modified,$pps,*/ $transaction_time,$credit_transaction_time,$transaction_type,$credit_amount,$debit_amount,$transaction_detail,$balance_after,$ref_no);
				}
			}
		}
	}
}

if(count($ExportRow)==0){
	$ExportRow[] = array($i_no_record_exists_msg);
}

if(($_REQUEST['UserType']=='')||($_REQUEST['UserType']==0))
	$filename = "All_Users";
if($_REQUEST['UserType']==1)
	$filename = "Teachers";
if($_REQUEST['UserType']==2)
	$filename = "Students";

$filename = "Detail_Transaction_Log($filename)".date('Y-m-d').".csv";
$export_content_final .= $lexport->GET_EXPORT_TXT($ExportRow,$HeaderRow);
$lexport->EXPORT_FILE($filename, $export_content_final);

intranet_closedb();
?>