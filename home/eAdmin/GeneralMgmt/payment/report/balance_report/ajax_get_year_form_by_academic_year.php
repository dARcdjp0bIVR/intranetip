<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$fcm = new form_class_manage();

$AcademicYearID = $_REQUEST['AcademicYearID'];

# get year form id list
$year_form_list = $fcm->Get_Form_List(false,1,$AcademicYearID);
$form_selection = '<select name="YearFormID[]" multiple="multiple" size="10">'."\n";
$form_selection .= '<optgroup label="'.$Lang['ePayment']['Identity'].'">'."\n";
$form_selection .= '<option value="-1">'.$Lang['ePayment']['Staff'].'</option>'."\n";
//$form_selection .= '<option value="-2">'.$Lang['ePayment']['Student'].'</option>'."\n";
$form_selection .= '</optgroup>'."\n";
$form_selection .= '<optgroup label="'.$Lang['ePayment']['YearForm'].'">'."\n";
for($i=0;$i<count($year_form_list);$i++){
	$form_selection.='<option value="'.$year_form_list[$i]['YearID'].'">'.htmlspecialchars($year_form_list[$i]['YearName'],ENT_QUOTES).'</option>'."\n";
}
$form_selection .= '</optgroup>'."\n";
$form_selection.= '</select>'."\n";

echo $form_selection;

intranet_closedb();
?>