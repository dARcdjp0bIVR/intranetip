<?php
# editing by 
/*
 * 2104-10-14 (Carlos): [ip2.5.5.10.1] Restrict date range to selected academic year start and end date.
 * 2013-10-10 (Carlos): Added academic year selection
 * Created on 2012-07-20
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_BalanceReport";

$linterface = new interface_html();

$fcm = new form_class_manage();
$fcm_ui = new form_class_manage_ui();

$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$academic_years = $fcm->Get_All_Academic_Year();
$js = 'var academicYearDateRange = {};'."\n";
for($i=0;$i<count($academic_years);$i++){
	$js .= 'academicYearDateRange['.$academic_years[$i]['AcademicYearID'].'] = {"YearStart":"'.date("Y-m-d",strtotime($academic_years[$i]['AcademicYearStart'])).'","YearEnd":"'.date("Y-m-d",strtotime($academic_years[$i]['AcademicYearEnd'])).'"}'."\n";
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
	
$YearFormID = isset($YearFormID) && count($YearFormID)>0? $YearFormID : array();	

$academicYearSelection = getSelectAcademicYear("AcademicYearID", 'onchange="academicYearChanged(this.value)";', 1, 0, $CurrentAcademicYearID);

# get year form id list
$year_form_list = $fcm->Get_Form_List(false,1,$CurrentAcademicYearID);
$form_selection = '<select name="YearFormID[]" multiple="multiple" size="10">'."\n";
$form_selection .= '<optgroup label="'.$Lang['ePayment']['Identity'].'">'."\n";
$form_selection .= '<option value="-1">'.$Lang['ePayment']['Staff'].'</option>'."\n";
//$form_selection .= '<option value="-2">'.$Lang['ePayment']['Student'].'</option>'."\n";
$form_selection .= '</optgroup>'."\n";
$form_selection .= '<optgroup label="'.$Lang['ePayment']['YearForm'].'">'."\n";
for($i=0;$i<count($year_form_list);$i++){
	$form_selection.='<option value="'.$year_form_list[$i]['YearID'].'" '.(in_array($year_form_list[$i]['YearID'],$YearFormID)?'selected':'').'>'.htmlspecialchars($year_form_list[$i]['YearName'],ENT_QUOTES).'</option>'."\n";
}
$form_selection .= '</optgroup>'."\n";
$form_selection.= '</select>'."\n";

$select_all_btn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "selectAll('YearFormID[]');return false;");

$format_array = array(array(0,"Web"),array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);

// required transaciton types
//$ary_types = array(1,2,3,6,7,8,9,10,11);

$TAGS_OBJ[] = array($Lang['ePayment']['BalanceReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script type="text/javascript" language="javascript">
<?=$js?>

function selectAll(element)
{
	var obj = document.form1;
	var s = obj.elements[element];
	
	for(var i=0; i<s.length; i++){
		s[i].selected = true;
	}
}

function check_multi_select(obj, element)
{
	var s = obj.elements[element];
	var select_element = 0;
	
	for(i=0; i<s.length; i++){
		if(s[i].selected){
			select_element++;
		}
	}
	
	if(select_element>0){
		return true;
	} else {
		return false;
	}
}

function submitForm(obj)
{
	var from_obj = document.getElementById('FromDate');
	var to_obj = document.getElementById('ToDate');
	
	if(!check_date_without_return_msg(from_obj)){
		$('input#FromDate').focus();
		return false;
	}
	if(!check_date_without_return_msg(to_obj)){
		$('input#ToDate').focus();
		return false;
	}
	
	if(from_obj.value > to_obj.value){
		$('div#ToDateWarningDiv').show();
		return false;
	}else{
		$('div#ToDateWarningDiv').hide();
	}
	
	if(check_multi_select(obj,'YearFormID[]')){
		$('div#ClassLevelWarningDiv').hide();

		obj.target='intranet_popup10';
		newWindow('about:blank', 10);
		
		obj.submit();
	}else{
		$('div#ClassLevelWarningDiv').show();
		$('select[name=YearFormID[]]').focus();
		return false;
	}
}

function academicYearChanged(selectedAcademicYearId)
{
	document.getElementById('FromDate').value = academicYearDateRange[selectedAcademicYearId]['YearStart'];
	document.getElementById('ToDate').value = academicYearDateRange[selectedAcademicYearId]['YearEnd'];
	$('#FormSelectionLayer').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.get(
		"ajax_get_year_form_by_academic_year.php?AcademicYearID=" + selectedAcademicYearId,
		{},
		function(returnData){
			$('#FormSelectionLayer').html(returnData);
		}
	);
}

function startDateChanged(obj)
{
	var d = $.trim(obj.value);
	if(d.match(/^\d\d\d\d\-\d\d\-\d\d$/)){
		var academic_year_id = $('#AcademicYearID').val();
		if(academicYearDateRange[academic_year_id]){
			var year_start = academicYearDateRange[academic_year_id]['YearStart'];
			var year_end = academicYearDateRange[academic_year_id]['YearEnd'];
			if(d < year_start){
				obj.value = year_start;
			}else if(d > year_end){
				obj.value = year_end;
			}
		}
	}
}

function endDateChanged(obj)
{
	var d = $.trim(obj.value);
	if(d.match(/^\d\d\d\d\-\d\d\-\d\d$/)){
		var academic_year_id = $('#AcademicYearID').val();
		if(academicYearDateRange[academic_year_id]){
			var year_start = academicYearDateRange[academic_year_id]['YearStart'];
			var year_end = academicYearDateRange[academic_year_id]['YearEnd'];
			if(d > year_end){
				obj.value = year_end;
			}else if(d < year_start){
				obj.value = year_start;
			}
		}
	}
}
</script>	

<br />
<form name="form1" method="post" action="balance_report.php">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['General']['AcademicYear']?></td>
		<td valign="top" class="tabletext" width="70%"><?=$academicYearSelection?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("FromDate",$FromDate,$OtherMember_=' onchange="startDateChanged(this);" ',$DateFormat_="yy-mm-dd",$MaskFunction_="",$ExtWarningLayerID_="",$ExtWarningLayerContainer_="",$OnDatePickSelectedFunction_=' startDateChanged(this); ')?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("ToDate",$ToDate,$OtherMember_=' onchange="endDateChanged(this);" ',$DateFormat_="yy-mm-dd",$MaskFunction_="",$ExtWarningLayerID_="",$ExtWarningLayerContainer_="",$OnDatePickSelectedFunction_=' endDateChanged(this); ')?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$linterface->Get_Thickbox_Warning_Msg_Div("ToDateWarningDiv",$Lang['ePayment']['WarningMsg']['PleaseInputValidDateRange'])?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['ePayment']['IdentityOrYearForm']?></td>
		<td valign="top" class="tabletext" width="70%">
			<?='<span id="FormSelectionLayer">'.$form_selection.'</span>&nbsp;'.$select_all_btn?>
			<?=$linterface->Get_Thickbox_Warning_Msg_Div("ClassLevelWarningDiv",$Lang['ePayment']['WarningMsg']['PleaseSelectIdentityOrYearForm'])?>
		</td>
	</tr>
<!--
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_Format ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$select_format?></td>
	</tr>
-->
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","submit1") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

