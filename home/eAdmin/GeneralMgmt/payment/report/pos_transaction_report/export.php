<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();
$lexport = new libexporttext();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

$namefield = getNameFieldWithClassNumberByLang("u.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="au.ChineseName";
}else  $archive_namefield ="au.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59' ";

if (trim($ClassName) != "") 
	$user_cond .= " AND (u.ClassName like '%".$ClassName."%') ";

$sql  = "SELECT
						CASE 
							WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
							ELSE '".$Lang['ePayment']['NoInvoiceNumber']."' 
						END as InvoiceNumber,
						IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					  ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),2) AS GrandTotal,
					  potl.RefCode,
					  ppdr.DateInput
					from
					  PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					  inner join
					  PAYMENT_OVERALL_TRANSACTION_LOG as potl
					  on ppdr.TransactionLogID = potl.LogID
					  LEFT JOIN
					  INTRANET_USER as u
					  on potl.StudentID = u.UserID
					  LEFT JOIN
					  INTRANET_ARCHIVE_USER as au
					  on potl.StudentID = au.UserID
         WHERE
						(
						 u.EnglishName LIKE '%$keyword%' OR
						 u.ChineseName LIKE '%$keyword%' OR
						 u.ClassName LIKE '%$keyword%' OR
						 u.ClassNumber LIKE '%$keyword%' OR
						 au.EnglishName LIKE '%$keyword%' OR
						 au.ChineseName LIKE '%$keyword%' OR
						 au.ClassName LIKE '%$keyword%' OR
						 au.ClassNumber LIKE '%$keyword%' OR 
						 potl.RefCode LIKE '%$keyword%' OR 
						 ppdr.InvoiceNumber LIKE '%$keyword%'  
						)
						$date_cond
						$user_cond 
				Group By
					ppdr.InvoiceNumber, potl.RefCode
					";
$Result = $lpayment->returnArray($sql);

$UtfContent = $Lang['ePayment']['POSTransactionReport']." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\r\n\r\n";
$UtfContent .= $i_ClassName."\t".(($ClassName != "")? $ClassName:$i_general_all)."\r\n";
$UtfContent .= $Lang['Payment']['Keyword']."\t".$Keyword."\r\n";
$UtfContent .= "\r\n";

$ExportColumn = array($Lang['ePayment']['InvoiceNumber'],$i_Payment_Field_Username,$i_UserClassName,$i_UserClassNumber,$Lang['ePayment']['GrandTotal'],$i_Payment_Field_RefCode,$i_Payment_Field_TransactionTime);

for ($i=0; $i< sizeof($Result); $i++) {
	unset($Detail);
	$Detail[] = $Result[$i]['InvoiceNumber'];
	$Detail[] = $Result[$i]['Name'];
	$Detail[] = $Result[$i]['ClassName'];
	$Detail[] = $Result[$i]['ClassNumber'];
	$Detail[] = $lpayment->getExportAmountFormat($Result[$i]['GrandTotal']);
	$Detail[] = $Result[$i]['RefCode'];
	$Detail[] = $Result[$i]['DateInput'];
	
	$Rows[] = $Detail;
}

intranet_closedb();

$filename = "cancel_deposit_report.csv";
$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
$lexport->EXPORT_FILE($filename, $ExportContent);
?>
