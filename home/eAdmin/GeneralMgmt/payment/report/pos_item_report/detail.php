<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader.php");
intranet_opendb();

$lpayment = new libpayment();

$ItemName = trim(urldecode($_REQUEST['ItemName']));
$FromDate = trim(urldecode($_REQUEST['FromDate']));
$ToDate = trim(urldecode($_REQUEST['ToDate']));

$namefield = getNameFieldWithClassNumberByLang("u.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="au.ChineseName";
}else  $archive_namefield ="au.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

$sql  = "SELECT
						CASE 
							WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
							ELSE '".$Lang['Payment']['NoInvoiceNumber']."' 
						END as InvoiceNumber,
						IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					  ppdr.ItemQty,
					  ppdr.ItemSubTotal,
					 	ROUND(ppdr.ItemSubTotal*ppdr.ItemQty,2) as GrandTotal,
					  potl.RefCode,
					  ppdr.DateInput
					from
					  PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					  inner join
					  PAYMENT_OVERALL_TRANSACTION_LOG as potl
					  on ppdr.ItemName = '".str_replace("'", "\\'", $ItemName)."' 
					  	and 
					  	ppdr.TransactionLogID = potl.LogID 
					  	and 
					  	ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59' 
					  LEFT JOIN
					  INTRANET_USER as u
					  on potl.StudentID = u.UserID
					  LEFT JOIN
					  INTRANET_ARCHIVE_USER as au
					  on potl.StudentID = au.UserID
					Order By 
						ppdr.DateInput
					";
                //echo $sql; die;
$Result = $lpayment->returnArray($sql);

$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
$table_content .= "<tr>
										<td class='$css_text'><b>".$Lang['ePayment']['ItemName']." : </b>".$ItemName."</td>
									</tr>";
$table_content .= "<tr>
										<td class='$css_text'><B>".$i_Payment_Field_TransactionTime." : </b>".$FromDate." ".$i_To." ".$ToDate."</td>
									</tr>";
$table_content .="</table><br>";
$table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$table_content .= "<tr>
										<td class='$css_text'><b>".$i_Payment_Field_TransactionTime."</b></td>
										<td class='$css_text'><b>".$Lang['ePayment']['InvoiceNumber']."</b></td>
										<td class='$css_text'><b>".$i_UserName."</b></td>";
$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
$table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
$table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td></tr>";

for ($i=0; $i< sizeof($Result);$i++) {
	$css =$i%2==0?$css_table_content:$css_table_content."2";
	
	$table_content .= "<tr>
											<td class='$css'>".$Result[$i]['DateInput']."</td>
											<td class='$css'>".$Result[$i]['InvoiceNumber']."</td>
											<td class='$css'>".$Result[$i]['Name']."</td>
											<td class='$css'>".$Result[$i]['ItemQty']."</td>
											<td class='$css'>".$Result[$i]['ItemSubTotal']."</td>
											<td class='$css' align=right>".$Result[$i]['GrandTotal']."</td>
										</tr>";
	$Total += $Result[$i]['GrandTotal'];
}
$table_content .= "<tr><td colspan='6' align=right>".$Total."</td></tr>";

$table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=\"center\">
									<tr><td>$i_Payment_Note_StudentRemoved</td></tr>
									</table>";

echo $table_content;

intranet_closedb();
?>
