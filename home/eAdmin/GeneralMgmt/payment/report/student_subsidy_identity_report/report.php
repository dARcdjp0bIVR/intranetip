<?php
// editing by

#################################################
#   Date:   2020-05-06 Ray
#           use NewEffectiveStartDate & NewEffectiveEndDate
#	Date:	2019-08-28 Ray
#			Created
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

$MODULE_OBJ['title'] = $Lang['ePayment']['StudentSubsidyIdentityReport'];

$linterface = new interface_html("popup.html");


# date range
# default : FromDate = 1st of current month
#			ToDate = last day of current month
$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

if($FromDate != '' && $ToDate!=''){
	$lpayment->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
}

$modifier_name_field = getNameFieldByLang2("u.");
$intranet_user_name = Get_Lang_Selection("u1.ChineseName","u1.EnglishName");
$intranet_archive_user_name = Get_Lang_Selection("u2.ChineseName","u2.EnglishName");

$cond = '';
if($TargetType == 'subsidy') {
    if(count($TargetID) > 0) {
		$cond = "s.NewIdentityID IN (" . implode(',', IntegerSafe($TargetID)) . ")";
	}
} else {
	$lclass = new libclass();
    $StudentAry = $lclass->getStudentByClassId(IntegerSafe($TargetID));
    $StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
	if(count($StudentIdAry) > 0) {
		$cond = "s.StudentID IN (" . implode(',', $StudentIdAry) . ")";
	}
}

//$date_cond = " AND (DATE_FORMAT(s.InputDate,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(s.InputDate,'%Y-%m-%d')<='$ToDate') ";

$date_cond = " AND 
(
    (s.NewEffectiveStartDate IS NULL AND s.NewEffectiveEndDate IS NULL) OR
    (s.NewEffectiveStartDate IS NOT NULL AND 
        (
            (s.NewEffectiveEndDate IS NULL AND DATE_FORMAT(s.NewEffectiveStartDate,'%Y-%m-%d') <= '$ToDate')
            OR
            (s.NewEffectiveEndDate IS NOT NULL AND DATE_FORMAT(s.NewEffectiveStartDate,'%Y-%m-%d') <= '$ToDate'
                AND DATE_FORMAT(s.NewEffectiveEndDate,'%Y-%m-%d') >= '$ToDate')
            OR
            (s.NewEffectiveEndDate IS NOT NULL AND DATE_FORMAT(s.NewEffectiveStartDate,'%Y-%m-%d') <= '$FromDate'
                AND DATE_FORMAT(s.NewEffectiveEndDate,'%Y-%m-%d') >= '$FromDate')
            OR
            (s.NewEffectiveEndDate IS NOT NULL AND DATE_FORMAT(s.NewEffectiveStartDate,'%Y-%m-%d') >= '$FromDate'
                AND DATE_FORMAT(s.NewEffectiveEndDate,'%Y-%m-%d') <= '$ToDate')
            OR
            (s.NewEffectiveEndDate IS NOT NULL AND DATE_FORMAT(s.NewEffectiveStartDate,'%Y-%m-%d') <= '$FromDate'
                AND DATE_FORMAT(s.NewEffectiveEndDate,'%Y-%m-%d') >= '$ToDate')
        )
     )
)";


$result = array();
if($cond != '') {
	$sql = "SELECT 
			IF(u1.UserID IS NOT NULL,u1.ClassName,u2.ClassName) as ClassName,
			IF(u1.UserID IS NOT NULL,u1.ClassNumber,u2.ClassNumber) as ClassNumber,
			IF(u1.UserID IS NOT NULL,$intranet_user_name,$intranet_archive_user_name) as StudentName,
			IF(s.NewEffectiveStartDate IS NOT NULL AND s.NewEffectiveEndDate IS NOT NULL,CONCAT(s.NewEffectiveStartDate,' " . $Lang['General']['To'] . " ',s.NewEffectiveEndDate),IF(s.NewEffectiveStartDate IS NOT NULL AND s.NewEffectiveEndDate IS NULL, s.NewEffectiveStartDate,'".$Lang['General']['EmptySymbol']."')) as EffectivePeriod,
			s.InputDate,
			$modifier_name_field as InputUserName,
			si.IdentityName
		FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT_CHANGE_LOG as s
		LEFT JOIN INTRANET_USER as u1 ON s.StudentID=u1.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON s.StudentID=u2.UserID 
		LEFT JOIN INTRANET_USER as u ON u.UserID=s.InputBy 
		LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as si ON si.IdentityID=s.NewIdentityID
		WHERE $cond $date_cond ORDER BY InputDate ASC";

	$result = $lpayment->returnArray($sql, 7);
}

$display="<table width=90% border=0 cellpadding=1 cellspacing=0 align=center>";
$display.="<tr><td class='$css_text'><B>".$Lang['eEnrolment']['AvailiableDate'].":</b> $FromDate $i_Profile_To $ToDate</td></tr>";
$display.="</table>";

$csv="\"".$Lang['eEnrolment']['AvailiableDate']."\",\"$FromDate $i_Profile_To $ToDate\"\r\n";
$csv_utf=$Lang['eEnrolment']['AvailiableDate']."\t$FromDate $i_Profile_To $ToDate\r\n";


$display.="<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display.="<thead>";
$display.="<tr>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"14%\">".$Lang['SysMgr']['FormClassMapping']['ClassTitle']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"14%\">".$Lang['SysMgr']['FormClassMapping']['ClassNo']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"15%\">".$Lang['SysMgr']['FormClassMapping']['StudentName']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"15%\">".$Lang['ePayment']['SubsidyIdentity']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"14%\">".$Lang['eEnrolment']['AvailiableDate']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"14%\">".$i_Files_Date."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"14%\">".$Lang['General']['InputBy']."</td>";
$display.="</tr>";
$display.="</thead>";
$display.="<tbody>";

$header = array($Lang['SysMgr']['FormClassMapping']['ClassTitle'],$Lang['SysMgr']['FormClassMapping']['ClassNo'],$Lang['SysMgr']['FormClassMapping']['StudentName'],$Lang['ePayment']['SubsidyIdentity'],$Lang['eEnrolment']['AvailiableDate'],$i_Files_Date,$Lang['General']['InputBy']);

$csv .= '"'.implode('","', $header).'"'."\r\n";
$csv_utf .= implode("\t", $header)."\r\n";


$i = 0;
foreach($result as $temp) {
    $i++;
	$display.="<tr class=\"tablebluerow". ( $i%2 + 1 )."\">\n";
	$display.="<td class=\"tabletext\">".$temp['ClassName']."</td>";
	$display.="<td class=\"tabletext\">".$temp['ClassNumber']."</td>";
	$display.="<td class=\"tabletext\">".$temp['StudentName']."</td>";
	$display.="<td class=\"tabletext\">".$temp['IdentityName']."</td>";
    $display.="<td class=\"tabletext\">".$temp['EffectivePeriod']."</td>";
    $display.="<td class=\"tabletext\">".$temp['InputDate']."</td>";
    $display.="<td class=\"tabletext\">".$temp['InputUserName']."</td>";
	$display.="</tr>";

	$data = array($temp['ClassName'],$temp['ClassNumber'],$temp['StudentName'], $temp['IdentityName'], $temp['EffectivePeriod'], $temp['InputDate'], $temp['InputUserName']);
	$csv .= '"'.implode('","', $data).'"'."\r\n";
	$csv_utf .= implode("\t", $data)."\r\n";

}

if($i==0) {
	$display .= "<tr><td colspan=\"7\" class=\"tabletext\" align=\"center\">$i_StaffAttendance_Status_NoRecord</td></tr>";
}

$display.="</tbody>";
$display.="</table>";

if ($format == 1)     # Excel
{
	# Get template
	$output_filename = "student_subsidy_identity_report.csv";
	$lexport->EXPORT_FILE($output_filename, $csv_utf);
}
else{
	$linterface->LAYOUT_START();

	?>
    <style type="text/css" media="print">
        thead {display: table-header-group;}
    </style>
    <table width="90%" align="center" class="print_hide" border="0">
        <tr>
            <td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
        </tr>
    </table>
	<?php
	if($sys_custom['ePayment']['ReportWithSchoolName']){
		$school_name = GET_SCHOOL_NAME();
		$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
		echo $x;
	}
	?>
    <form name="form1" method="get" action='report.php'>
		<?=$display?>
    </form>

	<?
	$linterface->LAYOUT_STOP();
}
intranet_closedb();

?>