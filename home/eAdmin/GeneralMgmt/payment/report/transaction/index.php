<?php
// Editing by 
/*
 * 2013-10-29 (Carlos): hide [Add Value Records], [Cancel Payment], [Cancelled POS Transaction]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_TodayTransaction";

$linterface = new interface_html();

# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase
# 4 - Transfer FROM
# 5 - Transfer TO
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
# 11 - POS void transaction (refund)
$target_type=array(1,2,3,6,7,8,9,10,11);

if(!$isKIS){
	$target_type_name[1]=$i_Payment_ValueAddedRecord;
}
$target_type_name[2]=$i_Payment_SchoolAccount_PresetItem;
$target_type_name[3]=$i_Payment_SchoolAccount_SinglePurchase;
if(!$isKIS){
	$target_type_name[6]=$i_Payment_TransactionType_CancelPayment;
	if ($_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'])
		$target_type_name[11]=$Lang['ePOS']['ReportVoidTitle'];
}
$target_type_name[7]=$i_Payment_SchoolAccount_Other;


$type_cond = " TransactionType IN ('".implode("','",$target_type)."')";

$today = date('Y-m-d');
$FromDate = $FromDate=="" || !intranet_validateDate($FromDate)?$today:$FromDate;
	
$date_cond =" DATE_FORMAT(TransactionTime,'%Y-%m-%d')='".$lpayment->Get_Safe_Sql_Query($FromDate)."'";		

$sql =	"SELECT 
					IF(TransactionType=8 OR TransactionType=9 OR TransactionType=10,7,TransactionType),
					Amount,
					Details 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG 
				WHERE 
					$date_cond AND $type_cond 
				ORDER BY TransactionType,Details";
$temp = $lpayment->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($tran_type,$amount,$detail)=$temp[$i];
	$result[$tran_type]["$detail"]+=$amount+0;
}

$x="";
foreach($target_type_name as $type => $name){
	$x.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	$x.="<tr><td colspan=\"2\">".$linterface->GET_NAVIGATION2($name)."</td></tr>";
	$x.="</table>";
	$x.="<table width=\"93%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	$x.="<tr class=\"tablebluetop\">";
	$x.="<td class=\"tabletoplink\" height=\"20\" style=\"vertical-align:middle\" width='80%'>&nbsp;</td>";
	$x.="<td class=\"tabletoplink\" height=\"20\" style=\"vertical-align:middle\" width='20%'>$i_Payment_Field_Amount</td>";
	$x.="</tr>";
	$values = $result[$type];
	if(sizeof($values)>0){
		$j=0;
		$total=0;
		foreach($values as $detail => $amount){
			$css =$j%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			$total+=$amount+0;
			$x.="<tr class=\"$css\">";
			$x.="<td class=\"tabletext\">$detail&nbsp;</td>";
			$x.="<td class=\"tabletext\">$".number_format($amount,2)."</td>";
			$x.="</tr>";
			$j++;
	 	}
		$css ='tablebluebottom';
	 	$x.="<tr class=\"$css\"><td class=\"tabletext\" align=\"right\">$i_Payment_SchoolAccount_Total:&nbsp;</td>";
	 	$x.="<td class=\"tabletext\">$".number_format($total,2)."</td></tr>";
	}
	else
		$x.="<tr><td align=\"center\" class=\"tabletext\" height=\"40\" style=\"vertical-align:middle\" colspan=\"2\">$i_no_record_exists_msg</td></tr>";
	$x.="<tr><td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
	$x.="<tr><td colspan=\"2\">&nbsp;</td></tr>";
	$x.="</table>";
}
$display=$x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."')","","","","",0);

$TAGS_OBJ[] = array($i_Payment_Menu_Report_TodayTransaction, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
    function checkForm(formObj){
        if(formObj==null)return false;
        fromV = formObj.FromDate;
        toV= formObj.ToDate;
        if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
        }
        return true;
    }
    
    function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
    }
    
    function submitForm(obj){
        if(checkForm(obj))
            obj.submit();
    }
    
    function openPrintPage()
	{
        newWindow("print.php?FromDate=<?=urlencode($FromDate)?>",10);
	}
	
	function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;        
	}
	
$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get" action=''>
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_Payment_Menu_PrintPage_AddValueReport_Date?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="FromDate" id="FromDate" value="<?=escape_double_quotes($FromDate)?>" maxlength="10" class="textboxnum">
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
</form>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
</table>
<?=$display?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<br />
<?php
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>
