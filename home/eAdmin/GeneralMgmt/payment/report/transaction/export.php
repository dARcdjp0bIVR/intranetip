<?php
// Editing by 
/*
 * 2013-10-29 (Carlos): hide [Add Value Records], [Cancel Payment], [Cancelled POS Transaction]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lexport = new libexporttext();

$lpayment = new libpayment();

# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase
# 4 - Transfer FROM
# 5 - Transfer TO
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
# 11 - POS void transaction (refund)
$target_type=array(1,2,3,6,7,8,9,10,11);
if(!$isKIS){
	$target_type_name[1]=$i_Payment_ValueAddedRecord;
}
$target_type_name[2]=$i_Payment_SchoolAccount_PresetItem;
$target_type_name[3]=$i_Payment_SchoolAccount_SinglePurchase;
if(!$isKIS){
	$target_type_name[6]=$i_Payment_TransactionType_CancelPayment;
	if ($_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'])
		$target_type_name[11]=$Lang['ePOS']['ReportVoidTitle'];
}
$target_type_name[7]=$i_Payment_SchoolAccount_Other;

//$target_type_name[8]=$i_Payment_PPS_Charge;

$type_cond = " TransactionType IN ('".implode("','",$target_type)."')";

$today = date('Y-m-d');
$FromDate = $FromDate==""?$today:$FromDate;
	
$date_cond =" DATE_FORMAT(TransactionTime,'%Y-%m-%d')='$FromDate'";		

$sql =	"SELECT 
					IF(TransactionType=8 OR TransactionType=9 OR TransactionType=10,7,TransactionType),
					Amount,
					Details 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG 
				WHERE 
					$date_cond AND $type_cond 
				ORDER BY TransactionType,Details";
$temp = $lpayment->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($tran_type,$amount,$detail)=$temp[$i];
	$result[$tran_type]["$detail"]+=$amount+0;
}
$x="\n";
$utf_x="\r\n";
foreach($target_type_name as $type => $name){
	$x.="\"$name\",\"$i_Payment_Field_Amount\"\n";
	$utf_x .= $name."\t".$i_Payment_Field_Amount."\r\n";
	$values = $result[$type];
	if(sizeof($values)>0){
		$total=0;
		foreach($values as $detail => $amount){
			$total+=$amount+0;
			$x.="\"$detail\",\"".round($amount,2)."\"\n";
			$utf_x.=$detail."\t".round($amount,2)."\r\n";
	 	}
	 	$x.="\"$i_Payment_SchoolAccount_Total\",\"".round($total,2)."\"\n\n";
	 	$utf_x.=$i_Payment_SchoolAccount_Total."\t".round($total,2)."\r\n\r\n";
	}
	else
	{
		$x.="\"$i_no_record_exists_msg\"\n\n";
		$utf_x.=$i_no_record_exists_msg."\r\n\r\n";
	}
}
$x.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$utf_x.=$i_general_report_creation_time."\t".date('Y-m-d H:i:s')."\r\n";

$display="\"$i_Payment_Menu_Report_TodayTransaction ($FromDate)\"\n".$x;
$utf_display = $i_Payment_Menu_Report_TodayTransaction." (".$FromDate.")\r\n".$utf_x;

intranet_closedb();
$filename="transaction.csv";

$lexport->EXPORT_FILE($filename, $utf_display);
?>
