<?php
// Editing by 
/*
 * 2015-02-26 (Carlos): $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
 * 2013-10-29 (Carlos): hide [Add Value Records], [Cancel Payment], [Cancelled POS Transaction]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();

$linterface = new interface_html();

# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase
# 4 - Transfer FROM
# 5 - Transfer TO
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
# 11 - POS void transaction (refund)
$target_type=array(1,2,3,6,7,8,9,10,11);
if(!$isKIS){
	$target_type_name[1]=$i_Payment_ValueAddedRecord;
}
$target_type_name[2]=$i_Payment_SchoolAccount_PresetItem;
$target_type_name[3]=$i_Payment_SchoolAccount_SinglePurchase;
if(!$isKIS){
	$target_type_name[6]=$i_Payment_TransactionType_CancelPayment;
	if ($_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'])
		$target_type_name[11]=$Lang['ePOS']['ReportVoidTitle'];
}
$target_type_name[7]=$i_Payment_SchoolAccount_Other;


$type_cond = " TransactionType IN ('".implode("','",$target_type)."')";

$today = date('Y-m-d');
$FromDate = $FromDate==""?$today:$FromDate;
	
$date_cond =" DATE_FORMAT(TransactionTime,'%Y-%m-%d')='$FromDate'";		

$sql =	"SELECT 
					IF(TransactionType=8 OR TransactionType=9 OR TransactionType=10,7,TransactionType),
					Amount,
					Details 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG 
				WHERE 
					$date_cond AND $type_cond 
				ORDER BY TransactionType,Details";
$temp = $lpayment->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($tran_type,$amount,$detail)=$temp[$i];
	$result[$tran_type]["$detail"]+=$amount+0;
}

$x="";
$x.="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
foreach($target_type_name as $type => $name){
	$x.="<tr><td class=\"eSporttdborder eSportprinttabletitle\" height=\"20\" style=\"vertical-align:middle\" width=\"80%\"><u>$name</u></td>";
	$x.="<td class='eSporttdborder eSportprinttabletitle' height=\"20\" style=\"vertical-align:middle\" width=\"20%\">$i_Payment_Field_Amount</td></tr>";
	$values = $result[$type];
	if(sizeof($values)>0){
		$j=0;
		$total=0;
		foreach($values as $detail => $amount){
			$css =$j%2==0?"attendancepresent":"attendanceouting";
			$total+=$amount+0;
			$x.="<tr class=\"$css\">";
			$x.="<td class=\"eSporttdborder eSportprinttext\">$detail&nbsp;</td>";
			$x.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($amount,2)."</td>";
			$x.="</tr>";
			$j++;
	 	}
		$css ='eSportprinttext';
	 	$x.="<tr><td class=\"eSporttdborder eSportprinttext\" align=\"right\">$i_Payment_SchoolAccount_Total:&nbsp;</td><td class=\"eSporttdborder eSportprinttext\">$".number_format($total,2)."</td></tr>";
	}
	else $x.="<tr><td align=\"center\" class=\"eSporttdborder eSportprinttext\" height=\"40\" style=\"vertical-align:middle\" colspan=\"2\">$i_no_record_exists_msg</td></tr>";
}
$x.="</table>";
$display=$x;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<!-- date range -->
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="left"><b><?= $i_Payment_Menu_Report_TodayTransaction ?> (<?="$FromDate"?>)</b></td>
	</tr>
</table>
<?=$display?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
