<?php
// editing by

#################################################
#	Date:	2019-09-03 Ray
#			Created
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

$MODULE_OBJ['title'] = $Lang['ePayment']['StudentSubsidyReport'];

$linterface = new interface_html("popup.html");


# date range
# default : FromDate = 1st of current month
#			ToDate = last day of current month
$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

if($FromDate != '' && $ToDate!=''){
	$lpayment->Set_Report_Date_Range_Cookies($FromDate, $ToDate);
}

$modifier_name_field = getNameFieldByLang2("u.");
$intranet_user_name = Get_Lang_Selection("u1.ChineseName","u1.EnglishName");
$intranet_archive_user_name = Get_Lang_Selection("u2.ChineseName","u2.EnglishName");

$cond = '';
if($TargetType_subsidy == 'subsidy') {
	if (count($TargetID_subsidy) > 0) {
		$cond .= " AND s.SubsidyIdentityID IN (" . implode(',', IntegerSafe($TargetID_subsidy)) . ")";
	}
}

if($TargetType_class == 'class') {
	$lclass = new libclass();
	$StudentAry = $lclass->getStudentByClassId(IntegerSafe($TargetID_class));
	$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
	if(count($StudentIdAry) > 0) {
		$cond .= " AND s.StudentID IN (" . implode(',', $StudentIdAry) . ")";
	}
}
$date_cond = " AND (DATE_FORMAT(s.PaidTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(s.PaidTime,'%Y-%m-%d')<='$ToDate') ";

$result = array();
if($cond != '') {
    $sql = "SELECT
                IF(u1.UserID IS NOT NULL,u1.ClassName,u2.ClassName) as ClassName,
			    IF(u1.UserID IS NOT NULL,u1.ClassNumber,u2.ClassNumber) as ClassNumber,
		    	IF(u1.UserID IS NOT NULL,$intranet_user_name,$intranet_archive_user_name) as StudentName,
                s.StudentID, 
                pn.Title,
                pn.NoticeNumber,
                pc.Name as CatName, 
                si.IdentityName,
                s.Amount, 
                s.OriginalAmount,
                (s.OriginalAmount - s.Amount) as SubsidizedAmount
            FROM PAYMENT_PAYMENT_ITEMSTUDENT s
            LEFT JOIN INTRANET_USER as u1 ON s.StudentID=u1.UserID 
            LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON s.StudentID=u2.UserID
            LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as si ON si.IdentityID=s.SubsidyIdentityID
            LEFT JOIN PAYMENT_PAYMENT_ITEM as pi ON s.ItemID=pi.ItemID
            LEFT JOIN INTRANET_NOTICE as pn ON pi.NoticeID=pn.NoticeID
            LEFT JOIN PAYMENT_PAYMENT_CATEGORY as pc ON pi.CatID = pc.CatID
            WHERE s.SubsidyIdentityID IS NOT NULL AND s.RecordStatus=1 $cond $date_cond ORDER BY PaidTime ASC";

	$result = $lpayment->returnArray($sql, 11);
}

$display="<table width=90% border=0 cellpadding=1 cellspacing=0 align=center>";
$display.="<tr><td class='$css_text'><B>".$i_Payment_Menu_PrintPage_AddValueReport_Date.":</b> $FromDate $i_Profile_To $ToDate</td></tr>";
$display.="</table>";

$csv="\"".$i_Payment_Menu_PrintPage_AddValueReport_Date."\",\"$FromDate $i_Profile_To $ToDate\"\r\n";
$csv_utf=$i_Payment_Menu_PrintPage_AddValueReport_Date."\t$FromDate $i_Profile_To $ToDate\r\n";


$display.="<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display.="<thead>";
$display.="<tr>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"8%\">".$i_UserClassName."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"8%\">".$i_UserClassNumber."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"8%\">".$i_UserStudentName."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$Lang['ePayment']['SubsidyIdentity']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$i_Notice_NoticeNumber."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"12%\">".$Lang['eNotice']['NoticeTitle']."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$i_Payment_Field_PaymentItem."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$i_Payment_Field_PaymentDefaultAmount."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$i_Payment_Subsidy_Amount."</td>";
$display.="<td class=\"tablebluetop tabletopnolink\" width=\"10%\">".$i_Payment_Field_Chargeable_Amount."</td>";
$display.="</tr>";
$display.="</thead>";
$display.="<tbody>";

$header = array($i_UserClassName, $i_UserClassNumber, $i_UserStudentName, $Lang['ePayment']['SubsidyIdentity'], $i_Notice_NoticeNumber, $Lang['eNotice']['NoticeTitle'], $i_Payment_Field_PaymentItem, $i_Payment_Field_PaymentDefaultAmount, $i_Payment_Subsidy_Amount, $i_Payment_Field_Chargeable_Amount);

$csv .= '"'.implode('","', $header).'"'."\r\n";
$csv_utf .= implode("\t", $header)."\r\n";



$i = 0;
$total_OriginalAmount = 0;
$total_Amount = 0;
$total_SubsidizedAmount = 0;
foreach($result as $temp) {
	$i++;

	$display.="<tr class=\"tablebluerow". ( $i%2 + 1 )."\">\n";
	$display.="<td class=\"tabletext\">".$temp['ClassName']."</td>";
	$display.="<td class=\"tabletext\">".$temp['ClassNumber']."</td>";
	$display.="<td class=\"tabletext\">".$temp['StudentName']."</td>";
	$display.="<td class=\"tabletext\">".$temp['IdentityName']."</td>";
	$display.="<td class=\"tabletext\">".$temp['NoticeNumber']."</td>";
	$display.="<td class=\"tabletext\">".$temp['Title']."</td>";
	$display.="<td class=\"tabletext\">".$temp['CatName']."</td>";
	$display.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($temp['OriginalAmount'])."</td>";
	$display.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($temp['Amount'])."</td>";
	$display.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($temp['SubsidizedAmount'])."</td>";
	$display.="</tr>";

	$total_OriginalAmount += $temp['OriginalAmount'];
	$total_Amount += $temp['Amount'];
	$total_SubsidizedAmount += $temp['SubsidizedAmount'];

	$data = array($temp['ClassName'], $temp['ClassNumber'], $temp['StudentName'], $temp['IdentityName'], $temp['NoticeNumber'], $temp['Title'], $temp['CatName'], $lpayment->getExportAmountFormat($temp['OriginalAmount']), $lpayment->getExportAmountFormat($temp['Amount']),$lpayment->getExportAmountFormat($temp['SubsidizedAmount']));

	$csv .= '"'.implode('","', $data).'"'."\r\n";
	$csv_utf .= implode("\t", $data)."\r\n";

}

if($i==0) {
	$display .= "<tr><td colspan=\"10\" class=\"tabletext\" align=\"center\">$i_StaffAttendance_Status_NoRecord</td></tr>";
} else {
	$data = array_fill(0, 6, '');
	$data[] = $i_Payment_ItemTotal;
	$data[] = $lpayment->getExportAmountFormat($total_OriginalAmount);
	$data[] = $lpayment->getExportAmountFormat($total_Amount);
	$data[] = $lpayment->getExportAmountFormat($total_SubsidizedAmount);
	$csv .= '"'.implode('","', $data).'"'."\r\n";
	$csv_utf .= implode("\t", $data)."\r\n";
	$display .= "<tr><td colspan=\"7\" class=\"tablebluetop\" align=\"right\">".$i_Payment_ItemTotal." : </td>";
	$display .= "<td class=\"tablebluetop\">".$lpayment->getWebDisplayAmountFormat($total_OriginalAmount)."</td>";
	$display .= "<td class=\"tablebluetop\">".$lpayment->getWebDisplayAmountFormat($total_Amount)."</td>";
	$display .= "<td class=\"tablebluetop\">".$lpayment->getWebDisplayAmountFormat($total_SubsidizedAmount)."</td>";
	$display .= "</tr>";
}

$display.="</tbody>";
$display.="</table>";

if ($format == 1)     # Excel
{
	# Get template
	$output_filename = "student_subsidy_report.csv";
	$lexport->EXPORT_FILE($output_filename, $csv_utf);
}
else{
	$linterface->LAYOUT_START();

	?>
	<style type="text/css" media="print">
		thead {display: table-header-group;}
	</style>
	<table width="90%" align="center" class="print_hide" border="0">
		<tr>
			<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		</tr>
	</table>
	<?php
	if($sys_custom['ePayment']['ReportWithSchoolName']){
		$school_name = GET_SCHOOL_NAME();
		$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
		echo $x;
	}
	?>
	<form name="form1" method="get" action='report.php'>
		<?=$display?>
	</form>

	<?
	$linterface->LAYOUT_STOP();
}
intranet_closedb();

?>