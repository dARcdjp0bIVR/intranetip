<?php
if(!isset($lpayment)) {
	return;
}

$_FromDate = $lpayment->Get_Safe_Sql_Query($FromDate);
$_ToDate = $lpayment->Get_Safe_Sql_Query($ToDate);

//same PaymentID duplicate
$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE
        (StartDate BETWEEN '$_FromDate' AND '$_ToDate') OR (EndDate BETWEEN '$_FromDate' AND '$_ToDate') OR
        ('$_FromDate' BETWEEN StartDate AND EndDate) OR ('$_ToDate' BETWEEN StartDate AND EndDate)";
$paymentItem_data = $lpayment->returnArray($sql);
$itemIds = array();
foreach($paymentItem_data as $data) {
	$itemIds[] = $data['ItemID'];
}

if(count($itemIds) > 0) {
	$sql = "SELECT 
				DATE_FORMAT(a.InputDate,'%Y-%m-%d %H:%i') as InputDate,
                i.StartDate,
                i.EndDate,
                IF(u.UserID IS NULL, au.ClassName, u.ClassName) as ClassName,
                IF(u.UserID IS NULL, au.ClassNumber, u.ClassNumber) as ClassNumber,
                IF(u.UserID IS NULL, ".getNameFieldByLang("au.").", ".getNameFieldByLang("u.").") as StudentName,
                IF(u.UserID IS NULL, au.UserLogin, u.UserLogin) as UserLogin,
                i.Name as ItemName,
                c.Name as CatName,
                a.PaymentID,
                pi.ItemID,
                pi.StudentID
                FROM PAYMENT_TNG_TRANSACTION as a
                LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as pi ON pi.PaymentID=a.PaymentID
                LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=pi.ItemID
                LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON (i.CatID = c.CatID) 
                LEFT JOIN INTRANET_USER as u ON u.UserID=pi.StudentID 
                LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=pi.StudentID
                WHERE a.NoticeID IS NULL AND a.PaymentID > 0 AND (RefundStatus IS NULL OR RefundStatus='')
                AND i.ItemID IN (" . implode(',', $itemIds) . ")
                GROUP BY a.PaymentID HAVING count(*) > 1
                ORDER BY i.StartDate ASC
                ";

	$rs = $lpayment->returnArray($sql);
	$j = 0;
	$x = '';
	if($format == 'csv') {
		$x = array();
	}
	foreach ($rs as $data) {
		if($format == 'web') {
			$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
			$j++;
			$x .= "<tr class=\"$css\">";
			$x .= "<td class=\"tabletext\">" . $j . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['StartDate'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['EndDate'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['ClassName'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['ClassNumber'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['StudentName'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['UserLogin'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['CatName'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['ItemName'] . "</td>";
			$x .= "<td class=\"tabletext\">";
			$x .= "<a href=\"detail.php?StudentID=" . $data['StudentID'] . "&PaymentID=" . $data['PaymentID'] . "&ItemID=" . $data['ItemID'] . "&type=1\">" . $Lang['eInventory']['Details'] . "</a>";
			$x .= "</td>";
			$x .= "</tr>";
		} else if($format == 'print') {
			$css = $j%2==0?"attendancepresent":"attendanceouting";
			$j++;
			$x .= "<tr class=\"$css\">";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $j . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['InputDate'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['StartDate'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['EndDate'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassName'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassNumber'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['StudentName'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['UserLogin'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['CatName'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ItemName'] . "</td>";
			$x .= "</tr>";
		} else if($format == 'csv') {
			$x[] = array($data['InputDate'],
						$data['StartDate'],
						$data['EndDate'],
						$data['ClassName'],
						$data['ClassNumber'],
						$data['StudentName'],
						$data['UserLogin'],
						$data['CatName'],
						$data['ItemName']);

		}
	}
	$display_1 = $x;
}

//same NoticeID,StudentID duplicate
$sql = "SELECT NoticeID FROM INTRANET_NOTICE WHERE
        (DateStart BETWEEN '$_FromDate' AND '$_ToDate') OR (DateEnd BETWEEN '$_FromDate' AND '$_ToDate') OR
        ('$_FromDate' BETWEEN DateStart AND DateEnd) OR ('$_ToDate' BETWEEN DateStart AND DateEnd)";
$noticeItem_data = $lpayment->returnArray($sql);
$noticeIds = array();
foreach($noticeItem_data as $data) {
	$noticeIds[] = $data['NoticeID'];
}

if(count($noticeIds) > 0) {
	$sql = "SELECT
            *
            FROM (
                SELECT NoticeID, StudentID, SUM(count) as count, InputDate  FROM (
                    SELECT SpTxNo,NoticeID,bb.StudentID, 1 as count, aa.InputDate FROM PAYMENT_TNG_TRANSACTION aa
                    LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as bb ON bb.PaymentID=aa.PaymentID
                    WHERE NoticeID IS NOT NULL AND (RefundStatus IS NULL OR RefundStatus='') AND bb.StudentID IS NOT NULL
                    GROUP BY SpTxNo,NoticeID,StudentID
                ) a 
                GROUP BY NoticeID, StudentID
            ) b
            WHERE count > 1 AND NoticeID IN (".implode(',', $noticeIds).")
            ORDER BY NoticeID";
	$rs = $lpayment->returnArray($sql);

	$temp_table_sql = "CREATE TEMPORARY TABLE TEMP_DOUBLE_PAID_NOTICE_SUMMARY (
                    NoticeID int,
                    StudentID int,
                    InputDate datetime)";
	$lpayment->db_db_query($temp_table_sql);
	$pValue = '';
	for ($i=0; $i<count($rs); $i++){
		if ($i>0) {
			$pValue .= ",";
		}
		$pValue .=	"(".$rs[$i]['NoticeID'].",'".$rs[$i]['StudentID']."','".$rs[$i]['InputDate']."')";
	}

	$insert_temp_sql = "INSERT INTO TEMP_DOUBLE_PAID_NOTICE_SUMMARY (NoticeID, StudentID, InputDate) VALUES $pValue";
	$lpayment->db_db_query($insert_temp_sql);

	$sql = "SELECT
			DATE_FORMAT(a.InputDate,'%Y-%m-%d %H:%i') as InputDate,
	        n.DateStart,
	        n.DateEnd,
	        n.Title,
	        n.NoticeNumber,
            IF(u.UserID IS NULL, au.ClassName, u.ClassName) as ClassName,
            IF(u.UserID IS NULL, au.ClassNumber, u.ClassNumber) as ClassNumber,
            IF(u.UserID IS NULL, ".getNameFieldByLang("au.").", ".getNameFieldByLang("u.").") as StudentName,
            IF(u.UserID IS NULL, au.UserLogin, u.UserLogin) as UserLogin,
	        a.NoticeID,
	        a.StudentID
	        FROM 
	        TEMP_DOUBLE_PAID_NOTICE_SUMMARY a
	        LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=a.NoticeID
	        LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
	        LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=a.StudentID
	        ORDER BY n.DateStart ASC
	        ";
	$rs = $lpayment->returnArray($sql);
	$j = 0;
	$x = '';
	if($format == 'csv') {
		$x = array();
	}
	foreach ($rs as $data) {
		if($format == 'web') {
			$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
			$j++;
			$x .= "<tr class=\"$css\">";
			$x .= "<td class=\"tabletext\">" . $j . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['DateStart'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['DateEnd'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['ClassName'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['ClassNumber'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['StudentName'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['UserLogin'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['NoticeNumber'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['Title'] . "</td>";
			$x .= "<td class=\"tabletext\">";
			$x .= "<a href=\"detail.php?StudentID=" . $data['StudentID'] . "&NoticeID=" . $data['NoticeID'] . "&type=2\">" . $Lang['eInventory']['Details'] . "</a>";
			$x .= "</td>";
			$x .= "</tr>";
		} else if($format == 'print') {
			$css = $j%2==0?"attendancepresent":"attendanceouting";
			$j++;
			$x .= "<tr class=\"$css\">";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $j . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['InputDate'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['DateStart'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['DateEnd'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassName'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassNumber'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['StudentName'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['UserLogin'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['NoticeNumber'] . "</td>";
			$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['Title'] . "</td>";
			$x .= "</tr>";
		} else if($format == 'csv') {
			$x[] = array($data['InputDate'],
										$data['DateStart'],
										$data['DateEnd'],
										$data['ClassName'],
										$data['ClassNumber'],
										$data['StudentName'],
										$data['UserLogin'],
										$data['NoticeNumber'],
										$data['Title']);

		}
	}
	$display_2 = $x;
}

//PaymentID not found
$sql = "SELECT
        a.InputDate,
        a.Sp,
        a.SpTxNo,
        IF(u.UserID IS NULL, au.ClassName, u.ClassName) as ClassName,
        IF(u.UserID IS NULL, au.ClassNumber, u.ClassNumber) as ClassNumber,
        IF(u.UserID IS NULL, ".getNameFieldByLang("au.").", ".getNameFieldByLang("u.").") as StudentName,
        IF(u.UserID IS NULL, au.UserLogin, u.UserLogin) as UserLogin,
        a.StudentID,
        n.NoticeNumber,
        n.Title
        FROM PAYMENT_TNG_TRANSACTION as a 
        LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON b.PaymentID=a.PaymentID
        LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID
        LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=a.StudentID
        LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=a.NoticeID
        WHERE b.ItemID IS NULL AND a.PaymentID > 0 AND (a.RefundStatus IS NULL OR a.RefundStatus='')
        AND a.InputDate BETWEEN '$_FromDate' AND '$_ToDate'
        GROUP BY a.SpTxNo
        ORDER BY a.InputDate ASC
        ";
$rs = $lpayment->returnArray($sql);
$j = 0;
$x = '';
if($format == 'csv') {
	$x = array();
}
foreach ($rs as $data) {
	if($format == 'web') {
		$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
		$j++;
		$x .= "<tr class=\"$css\">";
		$x .= "<td class=\"tabletext\">" . $j . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['ClassName'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['ClassNumber'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['StudentName'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['UserLogin'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['SpTxNo'] . "</td>";
		$x .= "<td class=\"tabletext\">";
		$x .= "<a href=\"detail.php?SpTxNo=" . $data['SpTxNo'] . "&StudentID=".$data['StudentID']."&type=3\">" . $Lang['eInventory']['Details'] . "</a>";
		$x .= "</td>";
		$x .= "</tr>";
	} else if($format == 'print') {
		$css = $j%2==0?"attendancepresent":"attendanceouting";
		$j++;
		$x .= "<tr class=\"$css\">";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $j . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['InputDate'] . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassName'] . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['ClassNumber'] . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['StudentName'] . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['UserLogin'] . "</td>";
		$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $data['SpTxNo'] . "</td>";
		$x .= "</tr>";
	} else if($format == 'csv') {
		$x[] = array($data['InputDate'],
					$data['ClassName'],
					$data['ClassNumber'],
					$data['StudentName'],
					$data['UserLogin'],
					$data['SpTxNo']);

	}
}
$display_3 = $x;
