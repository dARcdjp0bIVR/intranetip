<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['DoublePaidPaymentReport']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_DoublePaidPaymentReport";

$linterface = new interface_html();

$overview_detail = array();

if($type == '1') {
	$StudentID = IntegerSafe($StudentID);
	$ItemID = IntegerSafe($ItemID);
	$PaymentID = IntegerSafe($PaymentID);

	$item_detail = $lpayment->returnPaymentItemInfo($ItemID);
    $item_cat_detail = $lpayment->returnPaymentCatName($item_detail['CatID']);

    if($StudentID != '') {
		$lu = new libuser($StudentID);
		if (empty($lu->UserID)) {
			$archive_user = $lu->ReturnArchiveStudentInfo($StudentID);
			$overview_detail[] = array($i_UserClassName, $archive_user['ClassName']);
			$overview_detail[] = array($i_ClassNumber, $archive_user['ClassNumber']);
			$overview_detail[] = array($i_UserStudentName, Get_Lang_Selection($archive_user['ChineseName'], $archive_user['EnglishName']));
			$overview_detail[] = array($i_UserLogin, $archive_user['UserLogin']);
		} else {
			$overview_detail[] = array($i_UserClassName, $lu->ClassName);
			$overview_detail[] = array($i_ClassNumber, $lu->ClassNumber);
			$overview_detail[] = array($i_UserStudentName, $lu->UserNameLang(0));
			$overview_detail[] = array($i_UserLogin, $lu->UserLogin);
		}
	}
	$overview_detail[] = array($i_Payment_Field_PaymentCategory, $item_cat_detail);
	$overview_detail[] = array($i_Payment_Field_PaymentItem, $item_detail['Name']);


	$sql = "SELECT
	            a.InputDate,
	            a.Sp,
	            a.SpTxNo,
	            a.PaymentAmount,
	            a.RefundStatus
	        FROM PAYMENT_TNG_TRANSACTION as a
	        LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as pi ON pi.PaymentID=a.PaymentID
            WHERE a.PaymentID='$PaymentID' AND pi.StudentID='$StudentID'
	        ORDER BY a.InputDate ASC
	        ";

	$transaction_detail = $lpayment->returnArray($sql);
	$x = '';

	$x .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"96%\" align=\"center\">
                <tr><td class=\"tabletext\"><img src=\"$image_path/$LAYOUT_SKIN/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">
                        <span class=\"sectiontitle\">".$item_detail['Name']."</span>
                    </td></tr>
            </table>";

	$x .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
                <tr class=\"tablebluetop\">
                    <td class=\"tabletoplink\">".$Lang['ePayment']['TransactionTime']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['PaymentMethod']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['Refunded']."</td>
                    <td class=\"tabletoplink\">".$i_Payment_Field_RefCode."</td>
                    <td class=\"tabletoplink\">".$i_Payment_Field_Amount."</td>                    
                </tr>";

	$j = 0;
	$total = 0;
	foreach($transaction_detail as $itemID=>$data) {
		$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
		$total += ($data['RefundStatus'] == '') ? $data['PaymentAmount'] : -$data['PaymentAmount'];
		$j++;
		$x .= "<tr class=\"$css\">";
		$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
		$x .= "<td class=\"tabletext\">" . strtoupper($data['Sp']) . "</td>";
		$x .= "<td class=\"tabletext\">" . (($data['RefundStatus'] != '') ? $Lang['ePayment']['Refunded'] : '') . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['SpTxNo'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $lpayment->getWebDisplayAmountFormat($data['PaymentAmount']) . "</td>";
		$x .= "</tr>";
	}

	$x .= "<tr class=\"tablebluebottom\"><td class=\"tabletext\" colspan=\"4\" align=\"right\">".$Lang['General']['Total'].":</td><td style=\"font-weight: bold;\">".$lpayment->getWebDisplayAmountFormat($total)."</td></tr>";
	$x .= "</table>";
    $x .= "<br/>";

	$display = $x;
}

if($type == '2') {
	$StudentID = IntegerSafe($StudentID);
	$NoticeID = IntegerSafe($NoticeID);

	$sql = "SELECT
	            a.PaymentID,
	            pi.itemID,
	            n.Title,
	            i.Name,
	            a.InputDate,
	            a.Sp,
	            a.SpTxNo,
	            a.PaymentAmount,
	            a.RefundStatus
	        FROM PAYMENT_TNG_TRANSACTION as a
	        LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as pi ON pi.PaymentID=a.PaymentID
            LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=pi.ItemID 
            LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=a.NoticeID
            WHERE a.NoticeID='$NoticeID' AND pi.StudentID='$StudentID'
	        ORDER BY a.InputDate ASC
	        ";
	$rs = $lpayment->returnArray($sql);
	$transaction_detail = array();
	foreach($rs as $temp) {
		$transaction_detail[$temp['itemID']][] = $temp;
	}

	$lnotice = new libnotice($NoticeID);
	if($StudentID != '') {
		$lu = new libuser($StudentID);
		if (empty($lu->UserID)) {
			$archive_user = $lu->ReturnArchiveStudentInfo($StudentID);
			$overview_detail[] = array($i_UserClassName, $archive_user['ClassName']);
			$overview_detail[] = array($i_ClassNumber, $archive_user['ClassNumber']);
			$overview_detail[] = array($i_UserStudentName, Get_Lang_Selection($archive_user['ChineseName'], $archive_user['EnglishName']));
			$overview_detail[] = array($i_UserLogin, $archive_user['UserLogin']);
		} else {
			$overview_detail[] = array($i_UserClassName, $lu->ClassName);
			$overview_detail[] = array($i_ClassNumber, $lu->ClassNumber);
			$overview_detail[] = array($i_UserStudentName, $lu->UserNameLang(0));
			$overview_detail[] = array($i_UserLogin, $lu->UserLogin);
		}
	}
	$overview_detail[] = array($i_Notice_NoticeNumber, $lnotice->NoticeNumber);
	$overview_detail[] = array($i_Notice_Title, $lnotice->Title);

	$x = '';
	foreach($transaction_detail as $itemID=>$temp) {

	    $x .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"96%\" align=\"center\">
                <tr><td class=\"tabletext\"><img src=\"$image_path/$LAYOUT_SKIN/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">
                        <span class=\"sectiontitle\">".$temp[0]['Title'].' - '.$temp[0]['Name']."</span>
                    </td></tr>
            </table>";


	    $x .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
                <tr class=\"tablebluetop\">
                    <td class=\"tabletoplink\">".$Lang['ePayment']['TransactionTime']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['PaymentMethod']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['Refunded']."</td>
                    <td class=\"tabletoplink\">".$i_Payment_Field_RefCode."</td>
                    <td class=\"tabletoplink\">".$i_Payment_Field_PaymentItem.$i_Payment_Field_Amount."</td>                    
                </tr>";

		$j = 0;
		$total = 0;
		foreach($temp as $data) {
			$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
			$total += ($data['RefundStatus'] == '') ? $data['PaymentAmount'] : -$data['PaymentAmount'];
			$j++;
			$x .= "<tr class=\"$css\">";
			$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
			$x .= "<td class=\"tabletext\">" . strtoupper($data['Sp']) . "</td>";
			$x .= "<td class=\"tabletext\">" . (($data['RefundStatus'] != '') ? $Lang['ePayment']['Refunded'] : '') . "</td>";
			$x .= "<td class=\"tabletext\">" . $data['SpTxNo'] . "</td>";
			$x .= "<td class=\"tabletext\">" . $lpayment->getWebDisplayAmountFormat($data['PaymentAmount']) . "</td>";
			$x .= "</tr>";
		}
        $x .= "<tr class=\"tablebluebottom\"><td class=\"tabletext\" colspan=\"4\" align=\"right\">".$Lang['General']['Total'].":</td><td style=\"font-weight: bold;\">".$lpayment->getWebDisplayAmountFormat($total)."</td></tr>";
		$x .= "</table>";
		$x .= "<br/>";
	}

	$display = $x;
}

if($type == '3') {
	$StudentID = IntegerSafe($StudentID);

	if($StudentID != '') {
		$lu = new libuser($StudentID);
		if (empty($lu->UserID)) {
			$archive_user = $lu->ReturnArchiveStudentInfo($StudentID);
			$overview_detail[] = array($i_UserClassName, $archive_user['ClassName']);
			$overview_detail[] = array($i_ClassNumber, $archive_user['ClassNumber']);
			$overview_detail[] = array($i_UserStudentName, Get_Lang_Selection($archive_user['ChineseName'], $archive_user['EnglishName']));
			$overview_detail[] = array($i_UserLogin, $archive_user['UserLogin']);
		} else {
			$overview_detail[] = array($i_UserClassName, $lu->ClassName);
			$overview_detail[] = array($i_ClassNumber, $lu->ClassNumber);
			$overview_detail[] = array($i_UserStudentName, $lu->UserNameLang(0));
			$overview_detail[] = array($i_UserLogin, $lu->UserLogin);
		}
	}

	$rs = array();
	if($SpTxNo != '') {
		$sql = "SELECT
	            a.PaymentID,
	            pi.itemID,
	            n.Title,
	            i.Name,
	            a.InputDate,
	            a.Sp,
	            a.SpTxNo,
	            a.PaymentAmount,
	            a.RefundStatus
	        FROM PAYMENT_TNG_TRANSACTION as a
	        LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as pi ON pi.PaymentID=a.PaymentID
            LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=pi.ItemID 
            LEFT JOIN INTRANET_NOTICE as n ON n.NoticeID=a.NoticeID
            WHERE a.SpTxNo='" . $lpayment->Get_Safe_Sql_Query($SpTxNo) . "'
	        ORDER BY a.InputDate ASC
	        ";
		$rs = $lpayment->returnArray($sql);
	}

	$x = '';

	$x .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
                <tr class=\"tablebluetop\">
                    <td class=\"tabletoplink\">".$Lang['ePayment']['TransactionTime']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['PaymentMethod']."</td>
                    <td class=\"tabletoplink\">".$Lang['ePayment']['Refunded']."</td>
                    <td class=\"tabletoplink\">".$i_Payment_Field_RefCode."</td>   
                    <td class=\"tabletoplink\">".$i_Payment_Field_Amount."</td>                   
                </tr>";

	$j = 0;
	$total = 0;
	foreach($rs as $data) {
		$css = $j % 2 == 0 ? "tablebluerow1 tabletext" : "tablebluerow2 tabletext";
		$total += ($data['RefundStatus'] == '') ? $data['PaymentAmount'] : -$data['PaymentAmount'];
		$j++;
		$x .= "<tr class=\"$css\">";
		$x .= "<td class=\"tabletext\">" . $data['InputDate'] . "</td>";
		$x .= "<td class=\"tabletext\">" . strtoupper($data['Sp']) . "</td>";
		$x .= "<td class=\"tabletext\">" . (($data['RefundStatus'] != '') ? $Lang['ePayment']['Refunded'] : '') . "</td>";
		$x .= "<td class=\"tabletext\">" . $data['SpTxNo'] . "</td>";
		$x .= "<td class=\"tabletext\">" . $lpayment->getWebDisplayAmountFormat($data['PaymentAmount']) . "</td>";
		$x .= "</tr>";

	}

	$x .= "<tr class=\"tablebluebottom\"><td class=\"tabletext\" colspan=\"4\" align=\"right\">".$Lang['General']['Total'].":</td><td style=\"font-weight: bold;\">".$lpayment->getWebDisplayAmountFormat($total)."</td></tr>";
	$x .= "</table>";
	$display = $x;
}

$TAGS_OBJ[] = array($Lang['ePayment']['DoublePaidPaymentReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
    <tr>
        <td>
            <table class="form_table_v30">
				<?php foreach($overview_detail as $data) { ?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$data[0]?>:</td>
                        <td>
                            <?=intranet_htmlspecialchars($data[1])?>
                        </td>
                    </tr>
				<?php } ?>
            </table>

        </td>
    </tr>
    <tr>
        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>

<br/>

<?=$display?>

<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
</div>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>