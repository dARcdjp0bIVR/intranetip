<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['DoublePaidPaymentReport']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

$display_1 = array();
$display_2 = array();
$display_3 = array();

$format = 'csv';
include('double_paid.php');


$csv_utf = "";

if(count($display_1) > 0) {
	$csv_utf .= $i_Payment_Field_PaymentItem."\r\n";
	$data_col = array($Lang['ePayment']['TransactionTime'],$i_general_startdate,$i_general_enddate,$i_UserClassName,$i_ClassNumber,$i_UserStudentName,$i_UserLogin,$i_Payment_Field_PaymentCategory,$i_Payment_Field_PaymentItem);
	$csv_utf .= implode("\t", $data_col)."\r\n";
	foreach($display_1 as $temp) {
		$csv_utf .= implode("\t", $temp) . "\r\n";
	}
	$csv_utf .= "\r\n";
}

if(count($display_2) > 0) {
	$csv_utf .= $Lang['eNotice']['PaymentNotice'] . "\r\n";
	$data_col = array($Lang['ePayment']['TransactionTime'],$i_Notice_DateStart, $i_Notice_DateEnd, $i_UserClassName, $i_ClassNumber, $i_UserStudentName, $i_UserLogin, $i_Notice_NoticeNumber, $i_Notice_Title);
	$csv_utf .= implode("\t", $data_col) . "\r\n";
	foreach($display_2 as $temp) {
		$csv_utf .= implode("\t", $temp) . "\r\n";
	}
	$csv_utf .= "\r\n";
}

if(count($display_3) > 0) {
	$csv_utf .= $i_Payment_Menu_Browse_MissedPPSBill . "\r\n";
	$data_col = array($Lang['ePayment']['TransactionTime'], $i_UserClassName, $i_ClassNumber, $i_UserStudentName, $i_UserLogin, $i_Payment_Field_RefCode);
	$csv_utf .= implode("\t", $data_col) . "\r\n";
	foreach($display_3 as $temp) {
		$csv_utf .= implode("\t", $temp) . "\r\n";
	}
	$csv_utf .= "\r\n";
}


$filename = "double_paid_payment.csv";
$lexport->EXPORT_FILE($filename, $csv_utf);

intranet_closedb();