<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['DoublePaidPaymentReport']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_DoublePaidPaymentReport";

$linterface = new interface_html();

$today = date('Y-m-d');
$today_ts = strtotime($today);

$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="" || !intranet_validateDate($FromDate)){
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d', getStartOfAcademicYear($today_ts));
}
if($ToDate=="" || !intranet_validateDate($ToDate)){
	$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d', getEndOfAcademicYear($today_ts));
}
if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

$display_1 = '';
$display_2 = '';
$display_3 = '';

$format = 'web';
include('double_paid.php');




$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);

$TAGS_OBJ[] = array($Lang['ePayment']['DoublePaidPaymentReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
	<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
	<script>
        function checkDate(obj){
            if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
            return true;
        }

        function checkForm(formObj){
            if(formObj==null)return false;
            fromV = formObj.FromDate;
            if(!checkDate(fromV)){
                return false;
            }
            if(!checkDate(formObj.ToDate)){
                return false;
            }
            if(formObj.FromDate.value > formObj.ToDate.value){
                alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
                return false;
            }

            return true;
        }

        function submitForm(obj){
            if(checkForm(obj))
                obj.submit();
        }

        function openPrintPage()
        {
            newWindow("print.php?FromDate=<?=urlencode($FromDate)?>&ToDate=<?=urlencode($ToDate)?>",10);
        }

        function exportPage(obj,url){
            old_url = obj.action;
            obj.action=url;
            obj.submit();
            obj.action = old_url;

        }

	</script>
	<br />
	<form name="form1" action="" method="get">
		<!-- date range -->
		<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_Payment_Menu_PrintPage_AddValueReport_Date?></td>
				<td valign="top" class="tabletext" width="70%">
					<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
					<span class="tabletextremark">(yyyy-mm-dd)</span>
					<?=$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
					<span class="tabletextremark">(yyyy-mm-dd)</span>
				</td>
			</tr>

			<tr>
				<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td colspan="2" class="tabletext" align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","submit2") ?>
				</td>
			</tr>
		</table>
	</form>

	<br />

	<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
		</tr>
	</table>

    <table border="0" cellpadding="2" cellspacing="0" width="96%" align="center">
        <tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
                <span class="sectiontitle"><?=$i_Payment_Field_PaymentItem?></span>
            </td></tr>
    </table>

    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr class="tablebluetop">
            <td class="tabletoplink">#</td>
            <td class="tabletoplink"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="tabletoplink"><?=$i_general_startdate?></td>
            <td class="tabletoplink"><?=$i_general_enddate?></td>
            <td class="tabletoplink"><?=$i_UserClassName?></td>
            <td class="tabletoplink"><?=$i_ClassNumber?></td>
            <td class="tabletoplink"><?=$i_UserStudentName?></td>
            <td class="tabletoplink"><?=$i_UserLogin?></td>
            <td class="tabletoplink"><?=$i_Payment_Field_PaymentCategory?></td>
            <td class="tabletoplink"><?=$i_Payment_Field_PaymentItem?></td>
            <td class="tabletoplink"></td>
        </tr>

        <?=$display_1?>
        <tr class="tablebluebottom"><td class="tabletext" colspan="11">&nbsp;</td></tr>
    </table>

	<br />

    <table border="0" cellpadding="2" cellspacing="0" width="96%" align="center">
        <tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
                <span class="sectiontitle"><?=$Lang['eNotice']['PaymentNotice']?></span>
            </td></tr>
    </table>
    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr class="tablebluetop">
            <td class="tabletoplink">#</td>
            <td class="tabletoplink"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="tabletoplink"><?=$i_Notice_DateStart?></td>
            <td class="tabletoplink"><?=$i_Notice_DateEnd?></td>
            <td class="tabletoplink"><?=$i_UserClassName?></td>
            <td class="tabletoplink"><?=$i_ClassNumber?></td>
            <td class="tabletoplink"><?=$i_UserStudentName?></td>
            <td class="tabletoplink"><?=$i_UserLogin?></td>
            <td class="tabletoplink"><?=$i_Notice_NoticeNumber?></td>
            <td class="tabletoplink"><?=$i_Notice_Title?></td>
            <td class="tabletoplink"></td>
        </tr>

		<?=$display_2?>
        <tr class="tablebluebottom"><td class="tabletext" colspan="11">&nbsp;</td></tr>
    </table>

    <br />
    <table border="0" cellpadding="2" cellspacing="0" width="96%" align="center">
        <tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
                <span class="sectiontitle"><?=$i_Payment_Menu_Browse_MissedPPSBill?></span>
            </td></tr>
    </table>
    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr class="tablebluetop">
            <td class="tabletoplink">#</td>
            <td class="tabletoplink"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="tabletoplink"><?=$i_UserClassName?></td>
            <td class="tabletoplink"><?=$i_ClassNumber?></td>
            <td class="tabletoplink"><?=$i_UserStudentName?></td>
            <td class="tabletoplink"><?=$i_UserLogin?></td>
            <td class="tabletoplink"><?=$i_Payment_Field_RefCode?></td>
            <td class="tabletoplink"></td>
        </tr>

		<?=$display_3?>
        <tr class="tablebluebottom"><td class="tabletext" colspan="8">&nbsp;</td></tr>
    </table>

    <br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>