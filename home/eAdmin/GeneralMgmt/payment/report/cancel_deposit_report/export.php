<?php
// 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}
	 

$namefield = getNameFieldByLang("b.");
$input_namefield = getNameFieldByLang("input_b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
	$archive_input_namefield = "input_c.ChineseName";
}else {
	$archive_namefield = "c.EnglishName";
	$archive_input_namefield = "input_c.EnglishName";
}

# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d')<='$ToDate' ";

if (trim($ClassName) != "") 
	$user_cond .= " AND (b.ClassName like '%".$ClassName."%') ";
if (trim($_REQUEST['UserID']) != "")
 	$user_cond .= " AND (b.UserID = '".$_REQUEST['UserID']."') ";

$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',$archive_namefield), IF(b.UserID IS NULL AND c.UserID IS NULL,'*',$namefield)) as Name,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,
               ".$lpayment->getExportAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime,
               IF( (input_b.UserID IS NULL AND input_c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_input_namefield,'</i>'), IF(input_b.UserID IS NULL AND input_c.UserID IS NULL,'<font color=red>*</font>',$input_namefield)) as InputName
             
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
            LEFT JOIN
            INTRANET_USER as input_b 
         		ON potl.InputBy = input_b.UserID 
         	LEFT JOIN 
            INTRANET_ARCHIVE_USER AS input_c 
            ON potl.InputBy=input_c.UserID
         WHERE
         		 potl.TransactionType=9 
         		 and 
         			(
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               potl.RefCode LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'
              )
             $date_cond
             $user_cond
                ";
$Result = $lpayment->returnArray($sql);

$Content .= "\"".$Lang['Payment']['CancelDepositReport']." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\"\n\n";
$Content .= "\"".$i_ClassName."\",\"".(($ClassName != "")? $ClassName:$i_general_all)."\"\n";
$Content .= "\"".$Lang['Payment']['Keyword']."\",\"".$Keyword."\"\n";
$Content .= "\n";

$UtfContent = $Lang['Payment']['CancelDepositReport']." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\r\n\r\n";
$UtfContent .= $i_ClassName."\t".(($ClassName != "")? $ClassName:$i_general_all)."\r\n";
$UtfContent .= $Lang['Payment']['Keyword']."\t".$Keyword."\r\n";
$UtfContent .= "\r\n";

$Content .= '"'.$i_Payment_Field_Username.'",';
$Content .= '"'.$i_UserClassName.'",';
$Content .= '"'.$i_UserClassNumber.'",';
$Content .= '"'.$i_Payment_Field_CreditAmount.'",';
$Content .= '"'.$i_Payment_Field_RefCode.'",';
$Content .= '"'.$i_Payment_Field_CancelDepositTime.'",';
$Content .= "\"".$i_general_last_modified_by."\"\n";

$ExportColumn = array($i_Payment_Field_Username,$i_UserClassName,$i_UserClassNumber,$i_Payment_Field_CreditAmount,$i_Payment_Field_RefCode,$i_Payment_Field_CancelDepositTime,$i_general_last_modified_by);

for ($i=0; $i< sizeof($Result); $i++) {
	unset($Detail);
	$Detail[] = $Result[$i]['Name'];
	$Detail[] = $Result[$i]['ClassName'];
	$Detail[] = $Result[$i]['ClassNumber'];
	$Detail[] = $lpayment->getExportAmountFormat($Result[$i]['Amount']);
	$Detail[] = $Result[$i]['RefCode'];
	$Detail[] = $Result[$i]['TransactionTime'];
	$Detail[] = $Result[$i]['InputName'];
	
	$Rows[] = $Detail;
	
	$Content .= '"'.$Result[$i]['Name'].'",';
	$Content .= '"'.$Result[$i]['ClassName'].'",';
	$Content .= '"'.$Result[$i]['ClassNumber'].'",';
	$Content .= '"'.$lpayment->getExportAmountFormat($Result[$i]['Amount']).'",';
	$Content .= '"'.$Result[$i]['RefCode'].'",';
	$Content .= '"'.$Result[$i]['TransactionTime'].'",';
	$Content .= "\"".$Result[$i]['InputName']."\"\n";
}

intranet_closedb();

$filename = "cancel_deposit_report.csv";
$export_content = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn);
$lexport->EXPORT_FILE($filename, $export_content);
?>
