<?php
$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ldbsms 	= new libsmsv2();
$msg 		= $Message;
$temp = explode(";",$sms_info);
$sms_ary = array();

for($i=0;$i<sizeof($temp);$i++)
{
	list($student_id, $sms_phone, $gname)	= explode(",", $temp[$i]);
	if($student_id=="")	continue;
	
	# get msg content
	$this_msg = $ldbsms->replace_content($student_id, $msg);
	$recipientData[] = array($sms_phone, $student_id, $gname, $this_msg);
}

	# send sms
    $sms_message = $msg;
	$targetType = 3;  
	$picType = 2;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule= "SMS_FR_MODULE_PAYMENT";
	$deliveryTime = "";
	$isIndividualMessage=true;
	$ldbsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC, $frModule, $deliveryTime,$isIndividualMessage);
	
	intranet_closedb();
	
	header("Location: index.php?sent=1");

?>