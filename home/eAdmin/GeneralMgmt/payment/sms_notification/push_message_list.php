<?php
//using by: 

##################################
# change log:
# 2016-11-21 Carlos: added checkbox to the can send list for sending push notification to selected students. default checked all can send students.
# 2014-10-10 Roy: file created, to let user confirm sending push message about ePayment
##################################

$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# classes:
$linterface = new interface_html();
$LIDB = new libdb();
$luser = new libuser();
$lclass = new libclass();
$lp = new libpayment();

$cannotSendArray = array();
$canSendArray = array();
$can_send = true;
$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

# build data:
$title = $_POST['PushMessageTitle'];
$message = $_POST['Message'];
## Student info
$stu_ary = array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$this_class = $ClassID[$i];
	$stu_ary = array_merge($stu_ary, $lclass->getClassStudentList($this_class));
	if($CcToClassTeacher)
		$temp_class_teacher_ary = array_merge($temp_class_teacher_ary, $lclass->returnClassTeacherID($this_class));
}
$stu_ary = array_unique ($stu_ary);
 

## Check balance less than
if($check_bal){
	 
	$TargetUsers = array();
	for($i=0;$i<sizeof($stu_ary);$i++){
		$this_bal = $lp->checkBalance($stu_ary[$i]);
		if($this_bal[0] >= $bal_less_than) continue;
		$TargetUsers[] = $stu_ary[$i];
	}
}else{
	$TargetUsers = $stu_ary;
}

for ($i = 0; $i < sizeOf($TargetUsers); $i++) {
	$studentId = $TargetUsers[$i];
	$lu = new libuser($studentId);
	$appParentIdAry = $lu->getParentUsingParentApp($studentId);
	if (in_array($studentId, $studentWithParentUsingAppAry)) {
		$canSendArray[$i]['name'] = $lu->UserName();
		$canSendArray[$i]['classname'] = $lu->ClassName;
		$canSendArray[$i]['classnumber'] = $lu->ClassNumber;
//		$canSendArray[$i]['parentname'] = implode("<br/>", BuildMultiKeyAssoc($appParentAry, "ParentID", array("OutputName"), 1, 0));
		$parentName = "";
		foreach ($appParentIdAry as $appParentId) {
			$lu_parent = new libuser($appParentId);
			if ($parentName != "") {
				$parentName .= "<br/>";
			}
			$parentName .= $lu_parent->UserName();
		}
		$canSendArray[$i]['parentname'] = $parentName;
		$canSendArray[$i]['studentId'] = $studentId;
		$studentIds[] = $studentId;
	} else {
		$cannotSendArray[$i]['name'] = $lu->UserName();
		$cannotSendArray[$i]['classname'] = $lu->ClassName;
		$cannotSendArray[$i]['classnumber']	= $lu->ClassNumber;
		$cannotSendArray[$i]['reason'] = $Lang['AppNotifyMessage']['ePayment']['ParentNotUsingEClassApp'];
	}
	
	
}

# cant sent table content:
$invalid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
$invalid_table.= "<td>";
$invalid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"><font color='red'> {$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
$invalid_table.= "</td></tr></table>";

$cannot_send_table_content = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
<td class=\"tabletoplink\" width=\"55%\">$i_Attendance_Reason</td>
</tr>\n";

$css = "tablerow1";   
foreach($cannotSendArray as $this_ary) {
	if(trim($this_ary['name'])=="")	continue;
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['reason'] ."</td>";
	$x .= "</tr>\n";
}
$x .= "</table>\n";
$cannot_send_table_content = $invalid_table.$x;


# can sent table content:
$invalid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
$invalid_table.= "<td>";
$invalid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
$invalid_table.= "</td></tr></table>";

$can_send_table_content = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
<td class=\"tabletoplink\" width=\"55%\">".$Lang['General']['ParentUsingEClassApp']."</td>
<td class=\"tabletoplink\" width=\"1\" ><input type=\"checkbox\" name=\"checkmaster\" onclick=\"(this.checked)?setChecked(1,this.form,'studentIds[]'):setChecked(0,this.form,'studentIds[]')\" checked=\"checked\" /></td></tr>\n";

$css = "tablerow1";   
foreach($canSendArray as $this_ary) {
	if(trim($this_ary['name'])=="")	continue;
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['parentname'] ."</td>";
	$x .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"studentIds[]\" value=\"".$this_ary['studentId']."\" checked=\"checked\" /></td>";
	$x .= "</tr>\n";
}
$x .= "</table>\n";
$can_send_table_content = $invalid_table.$x;


# UI components:
$back_button = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php';");
$send_button = $linterface->GET_ACTION_BTN($button_send, "submit");

# layout start:
$TAGS_OBJ[] = array($this_page_title, "", 0);
$MODULE_OBJ = $lp->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
function checkForm() {
	if($('input[name="studentIds\\[\\]"]:checked').length == 0)
	{
		alert('<?=$i_SMS_no_student_select?>');
		return false;
	}
	return true;	
}
</script>
<br />
<form name="form1" method="post" action="send_push_message_update.php" onsubmit="return checkForm();">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Title']?>
						</td>
						<td class="tabletext" width="70%">
			  				<?=$title?>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Description']?>
						</td>
						<td class="tabletext" width="70%">
			  				<?=$message?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
				  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				  </tr>
					<tr>
					  <td align="center" colspan="2">
							<?= $send_button."&nbsp;".$back_button ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><?=$cannot_send_table_content?></td>
		</tr>
		<tr>
			<td><?=$can_send_table_content?></td>
		</tr>
	</table>
	<input type="hidden" name="Title" value="<?=$title?>">
	<input type="hidden" name="Content" value="<?=$message?>">
	<input type="hidden" name="this_page" value="<?=$this_page?>">
	<input type="hidden" name="sms_info" value="<?=$sms_info?>">
	<input type="hidden" name="TargetDate" id="TargetDate" value="<?=$TargetDate?>">
	<input type="hidden" name="DayType" id="DayType" value="<?=$DayType?>">
	<input type="hidden" name="order_by_time" value="<?=$order_by_time?>">
	<!-- <input type="hidden" name="studentIds" value="<?=implode(",", $studentIds)?>"> -->
	
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>