<?php
//using by: 

##################################
# change log:
# 2019-09-23 Carlos: Removed auto fill items ($Payment_Item), ($Payment_StartDate) and ($Payment_EndDate) from the message as it is not applicable.
# 2017-07-19 Ivan: Fixed iOS cannot display merged content problem
# 2017-07-17 Carlos: Added payment account balance to push message.
# 2016-11-21 Carlos: if $_POST['studentIds'] is array, use it directly, otherwise splits it into array. 
# 2014-10-09 Roy: file created, send push message about ePayment
##################################

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$luser = new libuser();
$libeClassApp = new libeClassApp();
$ldbsms 	= new libsmsv2();
$lpayment = new libpayment();

$studentIds = is_array($_POST['studentIds'])? $_POST['studentIds'] : split(",",standardizeFormPostValue($_POST['studentIds']));
$isPublic = "N";
//debug_pr($luser->getParentStudentMappingInfo($studentIds));
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentIds), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

$studentIdToBalance = $lpayment->getBalanceInAssoc();

//debug_pr($luser->getParentStudentMappingInfo($studentIds));
//debug_pr($parentStudentAssoAry);
$messageTitle = standardizeFormPostValue($_POST['Title']);
$messageContent = standardizeFormPostValue($_POST['Content']);
$appType = $eclassAppConfig['appType']['Parent'];
$sendTimeMode = "";
$sendTimeString = "";
$individualMessageInfoAry = array();
$i = 0;
foreach ($studentIds as $studentId) {
	$thisStudent = new libuser($studentId);
	$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
//	debug_pr($appParentIdAry);
	$_individualMessageInfoAry = array();
	foreach ($appParentIdAry as $parentId) {
		$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
	}
	$_individualMessageInfoAry['messageTitle'] = $messageTitle;
	$_individualMessageInfoAry['messageContent'] = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
	$_individualMessageInfoAry['messageContent'] = str_replace('($payment_balance)',$studentIdToBalance[$studentId],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace(array('($Payment_Item)','($Payment_StartDate)','($Payment_EndDate)'),'',$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $_individualMessageInfoAry['messageContent'], '');

	$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
//debug_pr($_individualMessageInfoAry);
	$i++;
}

//debug_pr($recipientUserIdAry);
//debug_pr($parentStudentAssoAry);
//debug_pr($individualMessageInfoAry);
### send message
//$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);

//$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

$sent =  ($notifyMessageId > 0) ? 1 : 0;
intranet_closedb();

header("Location: index.php?sent=".$sent);
?>