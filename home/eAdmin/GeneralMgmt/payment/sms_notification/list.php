<?php

# using: 

#########################################################
#	Date: 	2014-10-10 Roy 
#			add exclude eClass app parent feature
#
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
#	Date:	2012-08-09	YatWoon
#			Improved: Add remark "School will be charged for each SMS message"
#
#	Date:	2012-03-13	YatWoon
#			Improved: disabled the button once client click on "Submit", prevent client double click [Case#2012-0216-1852-14071]
#
#########################################################

$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
} 

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lp = new libpayment();

$linterface = new interface_html();

$lclass = new libclass();
$ldb 	= new libdb();
$lsms 	= new libsmsv2();
$luser = new libuser();

$stu_ary = array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$this_class = $ClassID[$i];
	$stu_ary = array_merge($stu_ary, $lclass->getClassStudentList($this_class));
}

$stu_ary = array_unique ($stu_ary);

$sms_ary	= array();
$no_sms_ary = array();
$sms_info 	= "";
$sms_no		= 0;
$no_sms_no	= 0;
$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
$namefield2 		= $lsms->getSMSGuardianUserNameField("");
$includeAppParent = $_POST['includeAppParent'];

$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

for($i=0;$i<sizeof($stu_ary);$i++)
{
	$student_id	= $stu_ary[$i];
	if($student_id=="")	continue;
	
	if (!$includeAppParent) {
		if (in_array($student_id, $studentWithParentUsingAppAry)) {
			continue;
		}
	}
	
	//Need Check Balance or not?
	if($check_bal)
	{
		$this_bal = $lp->checkBalance($student_id);
		if($this_bal[0] >= $bal_less_than) continue;
	}
	
		$lu = new libuser($student_id);
		
		$sql = "
			SELECT 
					$main_content_field,
					Relation,
					Phone, 
					EmPhone,
					IsMain,
					$namefield2
			FROM
					$eclass_db.GUARDIAN_STUDENT
			WHERE 
					UserID = '$student_id' and 
					isSMS = 1
			ORDER BY 
					IsMain DESC, Relation ASC
		";
		$temp = $ldb->returnArray($sql);
	
		// guardian info
		for($j=0;$j<sizeof($temp);$j++)
		{
			# phone / emergency Phone 
			$temp[$j][2] = $lsms->parsePhoneNumber($temp[$j][2]);
			$temp[$j][3] = $lsms->parsePhoneNumber($temp[$j][3]);
			$p  		= $lsms->isValidPhoneNumber($temp[$j][2]) ? $temp[$j][2] : "";
			$e  		= $lsms->isValidPhoneNumber($temp[$j][3]) ? $temp[$j][3] : "";
			$sms_phone 	= ($p!="") ? $p : $e;
			
			if(!$sms_phone)		
			{	
				$no_sms_ary[$i]['reason'] = $i_SMS_Error_NovalidPhone;
				$mobile 	= ($temp[$j][2]!="") ? $temp[$j][2] : $temp[$j][3];
				$guardianName	= "";
			}
			else
			{
				$no_sms_ary[$i]['reason'] = "";
				$mobile 	= ($p!="") ? $p : $e;
				$guardianName	= $temp[$j][5];
				break;
			}
		}
		
		//Check Guardian
		if(sizeof($temp)==0)	
		{
			$no_sms_ary[$i]['reason'] = $i_SMS_Error_No_Guardian;
			$mobile					= "";
		}
		
		if($no_sms_ary[$i]['reason'])	# in $no_sms_ary
			{
					$no_sms_ary[$i]['name'] 		= $lu->UserName();
					$no_sms_ary[$i]['classname'] 	= $lu->ClassName;
					$no_sms_ary[$i]['classnumber'] 	= $lu->ClassNumber;
					$no_sms_ary[$i]['mobile'] 		= $mobile;
					$no_sms_no++;
			}
			else
			{
					$sms_ary[$i]['guardian'] 	= $temp[$j][0];
					$sms_ary[$i]['relation'] 	= $temp[$j][1];	
					$sms_ary[$i]['name'] 		= $lu->UserName();
					$sms_ary[$i]['classname'] 	= $lu->ClassName;
					$sms_ary[$i]['classnumber']	= $lu->ClassNumber;
					$sms_ary[$i]['mobile'] 		= $mobile;
					$sms_ary[$i]['guardianName']= $guardianName;
					
					$sms_info .= $student_id .",".$mobile.",".$guardianName.";";
					$sms_no++;
			}
}

############################################################
## no_sms_ary
############################################################

$css ='tabletext';

$invalid_table = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$invalid_table.= "<tr ><td>".$linterface->GET_NAVIGATION2($i_SMS_Warning_Cannot_Send_To_Users)."</td></tr>";
$invalid_table.= "</table>";
$table_content = "";
$x = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class='tabletop tabletoplink'>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_Attendance_Reason</td></tr>\n";

$no_sms_ary_no = 0;
foreach($no_sms_ary as $this_ary)
{
	if(trim($this_ary['name'])=="")	continue;
	$x .= "<tr class='$css'>";
	$x .= "<td class='$css'>". $this_ary['name'] ."</td>";
	$x .= "<td class='$css'>". $this_ary['classname'] ."</td>";
	$x .= "<td class='$css'>". $this_ary['classnumber'] ."</td>";
	$x .= "<td class='$css'>". $this_ary['mobile'] ."</td>";
	$x .= "<td class='$css'>". $this_ary['reason'] ."</td>";
	$x .= "</tr>\n";
	$no_sms_ary_no++;
}
$x .= "</table>\n";
$table_content = $invalid_table.$x;

############################################################
## sms_ary
############################################################
$valid_table = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$valid_table.= "<tr><td>".$linterface->GET_NAVIGATION2($i_SMS_Notice_Send_To_Users)."</td></tr></table>";
$table_content1 = "";
$x = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class='tabletop tabletoplink'>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_StudentGuardian_GuardianName</td>
</tr>\n";

$sms_ary_no = 0;
foreach($sms_ary as $this_ary)
{
	if(trim($this_ary['name'])=="")	continue;
	$x .= "<tr class=$css>";
	$x .= "<td>". $this_ary['name'] ."</td>";
	$x .= "<td>". $this_ary['classname'] ."</td>";
	$x .= "<td>". $this_ary['classnumber'] ."</td>";
	$x .= "<td>". $this_ary['mobile'] ."</td>";
	$x .= "<td>". $this_ary['guardianName'] ."</td>";
	$x .= "</tr>\n";
	$sms_ary_no++;
}
$x .= "</table>\n";
$table_content1 = $valid_table.$x;

//$back_button = "<a href=\"javascript:history.back();\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
//$send_button = "<a href=\"javascript:document.form1.submit();\"><img src='$image_path/admin/button/s_btn_send_$intranet_session_language.gif' border='0' align='absmiddle'></a>";


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SmsNotification";
$TAGS_OBJ[] = array($i_SMS_Notification, "", 0);
$MODULE_OBJ = $lp->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


### Warning
$WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Note'].'</font>', $Lang['SMS']['RemarkChargeForSMS'], $others="");
?>

<script language="javascript">
<!--
function click_submit()
{
	document.form1.submitbtn.disabled = true;
	document.form1.submitbtn.value = "<?=$Lang['Btn']['Sending']?>";
	document.form1.cancelbtn.disabled = true;
	document.form1.submit(); 
}
//-->
</script>

<BR>
<form name="form1" method="post" action="send_sms.php" >

<?=$WarningBox?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<? if($sms_ary_no>0) {?>
	<tr><td colspan='2'>
	
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="tabletext">
	    				<?=$i_SMS_MessageContent?>
	    			</td>
	    			<td class="tabletext" width="70%"><?=$Message?><BR>
	    			</td>
    			</tr>
    			<?php if($check_bal){?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SMS_Balance_Lass_Than?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    			$ <?=$bal_less_than?>
	    			
	    			</td>
    			</tr>	
    			<?php } ?>
		</table>
	</td></tr>
    <tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
				</tr>
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>	
    <tr>
	    <td align="center" colspan="2">
			<?//= $linterface->GET_ACTION_BTN($button_send, "submit", "javascript:document.form1.submit()", "submitbtn1") ?>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "click_submit();", "submitbtn") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn") ?>
		</td>
	</tr>	
<? } ?>
<? if($no_sms_ary_no!=0) {?>
<br/>
<tr><td colspan='2'>
<?=$table_content?>
</td></tr>
<? } ?>

<? if($sms_ary_no!=0) {?>
<br />
<tr><td colspan='2'>
<?=$table_content1?>
</td></tr>
<? } ?>


<? if($sms_ary_no==0) {?>
<br />
<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
</tr>
<? } ?>
</table>
<br>
<input type="hidden" name="sms_info" value="<?=$sms_info?>">
<input type="hidden" name="Message" value="<?=$Message?>">
</form>
<BR>

<?



$linterface->LAYOUT_STOP();
intranet_closedb();
?>
