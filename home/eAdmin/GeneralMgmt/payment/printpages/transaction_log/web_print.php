<?php
// editing by 
########################################### Change Log ###################################################
# 2018-07-10 by Carlos: fixed display columns [Post Time] and [Transaction Time].
# 2018-01-02 by Carlos: Modified to filter student by user id.
# 2017-12-11 by Carlos: [ip.2.5.9.1.1] $sys_custom['ePayment']['DetailTransactionLogReportTitle'] added report title.
# 2017-02-22 by Carlos: [ip.2.5.8.3.1] $sys_custom['ePayment']['HartsPreschool'] display Payment Method and Payment Remark.
# 2015-08-06 by Carlos: Set date range cookies.
# 2015-08-05 by Carlos: Added data column [Remark].
# 2014-08-15 by Carlos: order result set by transaction time
# 2013-10-29 by Carlos: KIS - hide [Add value Record Time], [Credit], [Balance After]
#
# 2013-02-08 by YatWoon: no need to call layout_start() / layout_stop(), 
#						 remove line break after the print button table
#						 page break for each student, not count by line#
#
# 2010-02-10 by Carlos: Add border line to table
#
##########################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$lclass = new libclass();

if($_REQUEST['TargetStartDate'] != '' && $_REQUEST['TargetEndDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['TargetStartDate'], $_REQUEST['TargetEndDate']);
}

if ($Format == "Print") {
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	$linterface = new interface_html();

	$MODULE_OBJ['title'] = $i_Payment_Menu_Browse_TransactionRecord;
// 	$linterface = new interface_html("popup.html");
// 	$linterface->LAYOUT_START();
	
	/*
	# Parameters of page
	//Default value of number of line in each page
	$defaultNumOfLine = 45;
	
	//Default width of a specific field
	$defaultFieldWidth1 = 53;
	
	//current number of line remaining
	$lineRemain = $defaultNumOfLine;
	
	//how many line need
	$fieldLineNeed = 1;
	
	# Page layout details
	$page_breaker = "<P CLASS='breakhere'>";
	$page_header_linefeed = 8;
	$intermediate_pageheader = "<P CLASS='breakhere'>";
	$intermediate_linefeed = 5;
	*/
	?>
	
	<STYLE TYPE="text/css">
	     P.breakhere {page-break-before: always}
	</STYLE>
	<?
}
else {
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
}
//if(sizeof($UserType)>0)
//	$temp_UserType = implode(",",$UserType);

if(($_REQUEST['UserType']=='')||($_REQUEST['UserType']==0))
	$cond1 = "RecordType IN (1,2)";
if($_REQUEST['UserType']==1)
	$cond1 = "RecordType IN (1)";
if($_REQUEST['UserType']==2) {
	$cond1 = "RecordType IN (2)";
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($TargetStartDate=="")
	$TargetStartDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="")
	$TargetEndDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

if ($TargetClass != "") 
	$ClassName = $lclass->getClassName($TargetClass);

$payment_account_result = array();
if (sizeof($UserStatus)>0) {
	if ($Format == "Print") {
		$BalanceField = $lpayment->getWebDisplayAmountFormatDB("b.Balance");
	}
	else {
		$BalanceField = $lpayment->getExportAmountFormatDB("b.Balance");
	}
	
	$cond2 = " AND RecordStatus IN ('".implode("','",$UserStatus)."')";
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT 
					a.UserID, 
					$namefield as UserName, 
					IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo) as PPSAccountNo, 
					".$BalanceField." as Balance, 
					if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated) as LastUpdated, 
					CASE 
						WHEN a.RecordStatus = 0 THEN '".$i_status_suspended."' 
						WHEN a.RecordStatus = 1 THEN '".$i_status_approved."' 
						WHEN a.RecordStatus = 2 THEN '".$i_status_pendinguser."' 
						WHEN a.RecordStatus = 3 THEN '".$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp."' 
						ELSE '' 
					END as Status
			FROM 
					INTRANET_USER AS a 
					LEFT JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE 
					".$cond1." 
					".$cond2." 
					".(($TargetClass != "")? "AND ClassName ='".$ClassName."'":"")." 
					".(isset($TargetStudent) && count($TargetStudent)>0 ? " AND a.UserID IN (".implode(",",(array)IntegerSafe($TargetStudent)).") ":"")."
			ORDER BY
					a.ClassName, a.ClassNumber+0, a.EnglishName";
	$payment_account_result = $lpayment->returnArray($sql,6);
}

if ($GetArchive) {
	$ArchiveNamefield = ($intranet_session_language == 'en')? 'au.EnglishName':'au.ChineseName';
	
	if ($Format == "Print") {
		$BalanceField = $lpayment->getWebDisplayAmountFormatDB("0.0");
	}
	else {
		$BalanceField = $lpayment->getExportAmountFormatDB("0.0");
	}
	$sql = "select 
						au.UserID,
						".$ArchiveNamefield." as UserName, 
						'--' as PPSAccountNo,
						'0.0' as Balance,
						'--' as LastUpdated, 
						'".$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived."' as Status 
					from 
						INTRANET_ARCHIVE_USER as au 
					where 
						".$cond1." 
						".(($TargetClass != "")? "AND ClassName ='".$ClassName."'":"")." 
					ORDER BY
						au.ClassName, au.ClassNumber+0, au.EnglishName";
	//debug_r($sql);
	$ArchiveUser_Result = $lpayment->returnArray($sql,6);
	//debug_r($payment_account_result);
	//debug_r($ArchiveUser_Result);
	$payment_account_result = array_merge($payment_account_result,$ArchiveUser_Result);
}

if(sizeof($payment_account_result)>0)
{
	$lineRemain = $defaultNumOfLine - $page_header_linefeed;
	
	for($i=0; $i<sizeof($payment_account_result); $i++)
	{
		list($u_id, $name, $pps, $bal, $last_modified, $Status) = $payment_account_result[$i];

		if ($Format == "Print") {
			$AmountField = $lpayment->getWebDisplayAmountFormatDB("a.Amount");
			$BalanceAfterField = $lpayment->getWebDisplayAmountFormatDB("a.BalanceAfter");
		}
		else {
			$AmountField = $lpayment->getExportAmountFormatDB("a.Amount");
			$BalanceAfterField = $lpayment->getExportAmountFormatDB("a.BalanceAfter");
		}
		$sql2  = "SELECT
								DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
								IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
								CASE a.TransactionType
								    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
								    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
								    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
								    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
								    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
								    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
								    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
								    WHEN 8 THEN '$i_Payment_PPS_Charge'
								    ELSE '$i_Payment_TransactionType_Other' END,
								IF(a.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$AmountField.",' -- '),
								IF(a.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$AmountField.",' -- '),
								a.Details, ".$BalanceAfterField.",
								IF(a.RefCode IS NULL, ' -- ', a.RefCode),
								IF(a.TransactionType=1 AND b.TransactionID IS NOT NULL, b.Remark, IF(a.TransactionType=2 AND c.PaymentID IS NOT NULL,c.Remark,'')) as Remark ";
			if($sys_custom['ePayment']['HartsPreschool']){
				$sql2.=" ,CASE c.PaymentMethod ";
				foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
				{
					$sql2 .= " WHEN '$key' THEN '$val' ";	
				}
				$sql2 .= " ELSE '".$Lang['ePayment']['NA']."' 
						END as PaymentMethod ";
			
			 	$sql2 .= " ,c.ReceiptRemark ";
			 }
				$sql2.=" FROM
								PAYMENT_OVERALL_TRANSACTION_LOG as a
								LEFT JOIN 
								PAYMENT_CREDIT_TRANSACTION as b 
								ON a.RelatedTransactionID = b.TransactionID 
								LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c ON c.PaymentID=a.RelatedTransactionID 
							WHERE
								a.StudentID ='$u_id' 
								AND 
								a.TransactionTime between '".$TargetStartDate."' AND '".$TargetEndDate." 23:59:59' 
							ORDER BY a.TransactionTime,a.LogID ";
		//debug_r($sql2);
    $payment_detail_result = $lpayment->returnArray($sql2,7);

    if ($Format == "Print") {
// 			$lineRemain -= 8;
// 			if ($lineRemain <0)
// 			{
// 	      $table_content .= "$intermediate_pageheader";
// 	      $lineRemain = $defaultNumOfLine - 8;
// 			}

		if($sys_custom['ePayment']['DetailTransactionLogReportTitle']){
			$breakStyle = '';
			$table_content .= '<div style="width:95%;text-align:center;'.(($i>0)?"page-break-before:always;":"").'">'.$sys_custom['ePayment']['DetailTransactionLogReportTitle'].'</div>';
		}else{
			$breakStyle = ($i<sizeof($payment_account_result)-1)?"style='page-break-after:always'":"";
		}
	    $table_content .= "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"eSporttableborder\" align='center' $breakStyle>\n";
	    $table_content .= "<tr><td>";
	    $table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" >\n";
			$table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\"><b>$i_Payment_Field_Username:</b> $name <br>";
			$table_content .= "<b>".$i_general_status.":</b> ".$Status."<br>";
			$table_content .= "<b>$i_Payment_Field_Balance:</b> $bal<br>";
			$table_content .= "<b>$i_Payment_Field_LastUpdated:</b> $last_modified<br>";
			$table_content .= "<b>$i_Payment_Field_PPSAccountNo:</b> $pps</td></tr>";
			$table_content .= "</table></td></tr>";
			$table_content .= "<tr><td><table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" >";
			$table_content .= "<tr class=\"eSporttdborder eSportprinttabletitle\"><td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PostTime']."</td>";
			if(!$isKIS){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_TransactionTime</td>";
			}
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_TransactionType</td>";
			if(!$isKIS){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_TransactionType_Credit</td>";
			}
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_TransactionType_Debit</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_TransactionDetail</td>";
			if(!$isKIS){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_BalanceAfterTransaction</td>";
			}
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_RefCode</td>
								<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Remark</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentMethod']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentRemark']."</td>";
			}
			$table_content .= "</tr>";
		}
		else {
			$ExportRow[] = array($i_Payment_Field_Username,$name);
			$ExportRow[] = array($i_general_status,$Status);
			$ExportRow[] = array($i_Payment_Field_Balance,$bal);
			$ExportRow[] = array($i_Payment_Field_LastUpdated,$last_modified);
			$ExportRow[] = array($i_Payment_Field_PPSAccountNo,$pps);
			if($isKIS){
				$ExportRow[] = array($Lang['ePayment']['PostTime'],$i_Payment_Field_TransactionType,$i_Payment_TransactionType_Debit,$i_Payment_Field_TransactionDetail,$i_Payment_Field_RefCode,$i_Payment_Receipt_Remark);
			}else{
				$ExportRow[] = array($Lang['ePayment']['PostTime'],$i_Payment_Field_TransactionTime,$i_Payment_Field_TransactionType,$i_Payment_TransactionType_Credit,$i_Payment_TransactionType_Debit,$i_Payment_Field_TransactionDetail,$i_Payment_Field_BalanceAfterTransaction,$i_Payment_Field_RefCode,$i_Payment_Receipt_Remark);
			}
			if($sys_custom['ePayment']['HartsPreschool']){
				$ExportRow[count($ExportRow)-1][] = $Lang['ePayment']['PaymentMethod'];
				$ExportRow[count($ExportRow)-1][] = $Lang['ePayment']['PaymentRemark'];
			}
		}
		
		if(sizeof($payment_detail_result)>0)
		{
			if ($Format == "Print") {
				for ($z=0; $z<sizeof($payment_detail_result); $z++)
				{	
		          list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no, $remark) = $payment_detail_result[$z];
		//           $currentLineNeed = ceil ( strlen($transaction_time.$credit_transaction_time.$transaction_type.$credit_amount.$debit_amount.$transaction_detail.$balance_after.$ref_no) / $defaultFieldWidth1 );
		// 		
		//           if ($fieldLineNeed < $currentLineNeed)
		//           	$fieldLineNeed = $currentLineNeed;
		          	
		          $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$transaction_time</td>";
		          		if(!$isKIS){
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$credit_transaction_time</td>";
		          		}
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$transaction_type</td>";
						if(!$isKIS){
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$credit_amount</td>";
						}
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$debit_amount</td>";
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$transaction_detail</td>";
						if(!$isKIS){
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$balance_after</td>";
						}
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$ref_no</td>
											   <td class=\"eSporttdborder eSportprinttext\">$remark</td>";
					 if($sys_custom['ePayment']['HartsPreschool']){
						$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$payment_detail_result[$z]['PaymentMethod']."&nbsp;</td>
									   <td class=\"eSporttdborder eSportprinttext\">".$payment_detail_result[$z]['ReceiptRemark']."&nbsp;</td>";
					}
					$table_content .= "</tr>";
				}
				
// 				$lineRemain = $lineRemain - $fieldLineNeed * sizeof($payment_detail_result) ;
			}
			else {
				for($j=0; $j<sizeof($payment_detail_result); $j++)
				{
					list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no, $remark) = $payment_detail_result[$j];
					if($isKIS){
						$ExportRow[] = array($transaction_time,$transaction_type,$debit_amount,$transaction_detail,$ref_no,$remark);
					}else{
						$ExportRow[] = array($transaction_time,$credit_transaction_time,$transaction_type,$credit_amount,$debit_amount,$transaction_detail,$balance_after,$ref_no,$remark);
					}
					if($sys_custom['ePayment']['HartsPreschool']){
						$ExportRow[count($ExportRow)-1][] = $payment_detail_result[$j]['PaymentMethod'];
						$ExportRow[count($ExportRow)-1][] = $payment_detail_result[$j]['ReceiptRemark'];
					}
				}
			}
		}
		else
		{
			if ($Format == "Print") {
// 				$lineRemain = $lineRemain - 1;
				$table_content .= "<tr><td colspan=11 align=center class='eSporttdborder eSportprinttext'>$i_no_record_exists_msg</td></tr>";
			}
			else {
				$ExportRow[] = array($i_no_record_exists_msg);
			}
		}
		
		if ($Format == "Print") {
			$table_content .= "</table></td></tr>";
			$table_content .= "</table><br>";
// 			$table_content .= "<table border=0>";
// 			$table_content .= "<tr><td height=20px></td></tr>";
// 			$table_content .= "</table>";
// 			$table_content .="<P CLASS='breakhere'></P>";
		}
		else {
			$ExportRow[] = array("");
		}
	}
}

if ($Format == "Print") {
	$print_btn_table ="<table width=\"95%\" align=\"center\" class=\"print_hide\" border=\"0\"><tr><td align=\"right\">".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td></tr></table>";
// 	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	?>
	<?=$print_btn_table?>
	<?=$table_content?>
	<?
// 	$linterface->LAYOUT_STOP();
}
else {
	if(($_REQUEST['UserType']=='')||($_REQUEST['UserType']==0))
		$filename = "All_Users";
	if($_REQUEST['UserType']==1)
		$filename = "Teachers";
	if($_REQUEST['UserType']==2)
		$filename = "Students";
		
	$filename = "Detail_Transaction_Log($filename)".date('Y-m-d').".csv";
	$export_content_final .= $lexport->GET_EXPORT_TXT($ExportRow,"", $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=0);
	$lexport->EXPORT_FILE($filename, $export_content_final);
}
intranet_closedb();
?>