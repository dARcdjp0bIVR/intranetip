<?php
// Editing by 
/*
 * 2018-01-02 (Carlos): Added student selection.
 * 2015-08-06 (Carlos): Get and use date range cookies.
 * 2013-08-28 (Carlos): Added format [CSV (Data Set)]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PrintPage_DetailTransactionLog";

$linterface = new interface_html();

$lclass = new libclass();

$usertype = array(
array(1,$i_identity_teachstaff),
array(2,$i_identity_student));

$format_array = array(
array(0,"Web"),
array(1,"CSV"),
array(2,$Lang['ePayment']['CsvDataSet']));

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($TargetStartDate=="")
	$TargetStartDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($TargetEndDate=="")
	$TargetEndDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

$UserType_Selection = getSelectByArray($usertype,"name=UserType onChange=\"this.form.flag.value=0; javascript:checkForm();\"",$_REQUEST['UserType'],1,0,0);

if($_REQUEST['UserType']==2)
{
	$class_list = $lclass->getClassList();
	$Class_Selection = getSelectByArray($class_list,' id="TargetClass" name="TargetClass" onchange="loadStudentSelection(this.options[this.selectedIndex].value,\'StudentRow\',\'StudentSelectionContainer\');" ',0,1,0,0);
}

if($_REQUEST['UserType']==1) {
	$UserStatus_Selection = "
													<input type=checkbox name=UserStatus[] id=\"SuspendStaff\" value=0><label for=\"SuspendStaff\">$i_status_suspended</label> 
													<input type=checkbox name=UserStatus[] id=\"ApproveStaff\" value=1><label for=\"ApproveStaff\">$i_status_approved </label>
													<input type=checkbox name=UserStatus[] id=\"PendingStaff\" value=2><label for=\"PendingStaff\">$i_status_pendinguser</label>";
}
else {
	$UserStatus_Selection = "
													<input type=checkbox name=UserStatus[] id=\"SuspendStudent\" value=0><label for=\"SuspendStudent\">$i_status_suspended</label> 
													<input type=checkbox name=UserStatus[] id=\"ApproveStudent\" value=1><label for=\"ApproveStudent\">$i_status_approved</label> 
													<input type=checkbox name=UserStatus[] id=\"PendingStudent\" value=2><label for=\"PendingStudent\">$i_status_pendinguser</label> 
													<input type=checkbox name=UserStatus[] id=\"LeftStudent\" value=3><label for=\"LeftStudent\">$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp</label>
													<input type=checkbox name=GetArchive id=\"ArchiveStudent\" value=1><label for=\"ArchiveStudent\">$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived</label>";
}
$File_Type = getSelectByArray($format_array,"name=FileFormat",0,0,1);

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_TransactionRecord, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


?>

<SCRIPT Language="JavaScript">
function checkForm()
{
	var obj1 = document.form1.UserType;
	var obj2 = document.form1.elements["UserStatus[]"];
	var obj3 = document.form1.TargetClass;
	var checking1 = 0;
	
	for(j=0; j<obj2.length; j++) {
		if(obj2[j].checked)
			checking1 = 1;
	}
	
	if (document.getElementById('ArchiveStudent') && document.getElementById('ArchiveStudent').checked) {
		checking1 = 1;
	}
	
	if($('#TargetClass').length > 0 && $('#TargetClass').val() != '' && $('#TargetStudent option:selected').length==0){
		alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
		return false;
	}
	
	if(document.form1.flag.value == 2)
	{
		if(checking1 == 1)
		{
			//document.form1.target = "_blank";
			if(document.form1.FileFormat.value==2){
				document.form1.action='csv_dataset.php';
				document.form1.submit();
			}else if(document.form1.FileFormat.value==1)
			{
				document.form1.action='web_print.php?Format=CSV';
				//return true;
				document.form1.submit();

			}
			else
			{
				document.form1.action='web_print.php?Format=Print';
				document.form1.target="_blank";
				document.form1.submit();
			}
			//document.form1.submit();
		}
		else
		{
			alert("<?=$i_Payment_PrintPage_Browse_UserStatusWarning?>");
			return false;
		}
	}
	if((document.form1.flag.value == 0)||(document.form1.flag.value == 1))
	{
		document.form1.target = '';
		document.form1.action='';
		document.form1.submit();
		return false;
	}
}

function loadStudentSelection(selectedClassId, rowId, containerId)
{
	if(selectedClassId == ''){
		$('#'+rowId).hide();
		$('#'+containerId).html('');
		return;
	}
	var data = {'task':'getStudentSelectionByClassId','TargetClass':selectedClassId,'IsMultiple':1};
	var selected_options = $('#TargetStudent option:selected');
	var selected_student_ids = [];
	for(var i=0;i<selected_options.length;i++){
		selected_student_ids.push(selected_options[i].value);
	}
	data['TargetStudent'] = selected_student_ids;
	$('#'+rowId).show();
	$('#'+containerId).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		'ajax_task.php',
		data,
		function(returnHtml)
		{
			$('#'+containerId).html(returnHtml);
		}
	);
}
</SCRIPT>
<BR><BR>
<form name=form1 id=form1 action="" method="post" >

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_startdate ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->Get_Date_Picker("TargetStartDate",$TargetStartDate)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_enddate ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->Get_Date_Picker("TargetEndDate",$TargetEndDate)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_identity ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$UserType_Selection?>
		</td>
	</tr>
	<?php if($_REQUEST['UserType']==2){?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_ClassName?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$Class_Selection?>
		</td>
	</tr>
	<tr id="StudentRow" style="display:none">
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['ePayment']['Student']?></td>
		<td valign="top" class="tabletext" width="70%">
			<span id="StudentSelectionContainer"></span>
			<?= $linterface->GET_BTN($button_select_all, "button", "Select_All_Options('TargetStudent',true);"); ?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_general_status?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$UserStatus_Selection?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_general_Format?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$File_Type?></td>
	</tr>	
	<Tr><td colspan='2'>&nbsp;</td></tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:document.form1.flag.value=2;checkForm()","submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","resetbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type=hidden name=flag value=0>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>