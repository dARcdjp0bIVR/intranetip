<?php
//Modifying by: 
/*
 * 2014-08-19 (Carlos): Added PaymentID[] filter condition
 */
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$page_breaker = "<P CLASS='breakhere'>";

#print_r($student_itemlist);
include_once("../../../../templates/fileheader.php");
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));


/*
if (is_file("$intranet_root/templates/reportheader/paymentlist_footer.php"))
{
    $content = get_file_content("$intranet_root/templates/reportheader/paymentlist_footer.php");
    $pagefooter = $content;
}
*/
$table_header = "<table width=95% border=0 cellspacing=0 cellpadding=0 class='$css_table'><tr><td class='$css_table_title'>$i_Payment_Receipt_Payment_Category</td><td class='$css_table_title'>$i_Payment_Receipt_Payment_Item</td><td class='$css_table_title'>$i_Payment_Receipt_Payment_Description</td><td class='$css_table_title'>$i_Payment_Receipt_Payment_Amount</td></tr>";


$display = "";

$lclass = new libclass();
if ($StudentID == "")
{
    if ($ClassName == "")
    {
        # All students
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
    else
    {
        # All students in $ClassName
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
}
else
{
  if (is_array($StudentID)) {
		$target_students = $StudentID;
	}
	else {
    $target_students = array($StudentID);
  }
}

$date_field = "a.PaidTime";
$conds = "";
if ($date_from != "")
{
    $conds .= " AND $date_field >= '$date_from'";
}
if ($date_to != "")
{
    $conds .= " AND $date_field <= '$date_to 23:59:59'";
}

if($IndividualItems == 1){
	$conds .= " AND a.PaymentID IN ('".implode("','",(array)$PaymentID)."') ";
}

# Get Latest Balance
$lpayment = new libpayment();
$list = implode(",", $target_students);
$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
$temp = $lpayment->returnArray($sql,2);
$balance_array = build_assoc_array($temp);


for ($j=0; $j<sizeof($target_students); $j++)
{
     $target_id = $target_students[$j];
$lu = new libuser($target_id);
$studentname = $lu->UserNameClassNumber();
$i_title = $studentname;
$curr_balance = $balance_array[$target_id];

$sql = "SELECT c.Name, b.Name,b.Description, a.Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
                LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID
        WHERE a.StudentID = $target_id AND a.RecordStatus = 1 $conds
                ORDER BY c.DisplayOrder, b.DisplayOrder
                ";
$student_itemlist = $lu->returnArray($sql,4);

# start student
$total_amount = 0;
$list_table = "$table_header";
for ($i=0; $i<sizeof($student_itemlist); $i++)
{
	
	 $css =$i%2==0?$css_table_content:$css_table_content."2";
     list($cat_name, $item_name, $item_desp, $item_amount) = $student_itemlist[$i];
     $total_amount += $item_amount;
     $item_amount = $lpayment->getWebDisplayAmountFormat($item_amount);
     $list_table .= "<tr><td class='$css'>$cat_name&nbsp;</td><td class='$css'>$item_name&nbsp;</td><td class='$css'>$item_desp&nbsp;</td><td align=center class='$css'>$item_amount</td></tr>\n";
}
if(sizeof($student_itemlist)<=0){
	$list_table.="<tr><td class='$css_table_content' colspan='4'>&nbsp;</td></tr>";
}
$total_amount = $lpayment->getWebDisplayAmountFormat($total_amount);

$list_table.="<tr><td colspan='4' class='$css_table_content'><img src='$image_path/space.gif' border=0 height=1></td></tr>";
$list_table .= "<tr><td  class='$css_table_content' colspan='3' align=right><B>$i_Payment_Receipt_Payment_Total</b></td><td align=center class='$css_table_content'>$total_amount</td></tr>\n";
$list_table .= "</table>";
$name_string = "$i_Payment_Receipt_ReceivedFrom $studentname $i_Payment_Receipt_PaidAmount$total_amount $i_Payment_Receipt_ForItems";

if ($j!=0)
{
    echo $page_breaker;
}

echo "<table style='height:10cm;width:24.2cm;' border=0 cellspacing=0 cellpadding=0><tr><td>\n";
echo "<table width=95% border=0 cellspacing=0 cellpadding=0 style='height:4cm;width:20cm;margin-left:2.1cm;margin-top:3cm;'><tr><td>";
echo "<table width=95% border=0 cellspacing=0 cellpadding=0><tr><td class='$csv_text'>$name_string</td></tr></table>\n";
echo "<br>";
echo $list_table;
echo "</tr></td></table>\n";
echo "<img src='$image_path/leeshaukee/school_chop_HD.gif' style='height:2.5cm;width:4.2cm;margin-left:17.9cm;'>";
echo "</td></tr></table>\n";

# End of 1 student
}

intranet_closedb();
include_once("../../../../templates/filefooter.php");
?>
