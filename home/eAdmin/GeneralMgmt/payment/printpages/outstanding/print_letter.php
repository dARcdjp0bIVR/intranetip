<?php
## Using By : 
#################################################################################################
## Modification Log
## 2017-12-22 Carlos
## - Order records by student then by start date.
##
## 2016-11-21 Carlos
## - Added [Exclude Date] option for print letter.
##
## 2014-11-10 Carlos [ip2.5.5.12.1]:
## - Add sorting field DisplayOrder
##
## 2014-10-24 Carlos [ip2.5.5.10.1]: 
## - Add [All Payment Items] type
##
## 2014-09-30 Bill:
## - hide issue date when exclude parent signature
##
## 2014-09-02 Carlos: 
## - Sort by ClassName, ClassNumber, StudentName, RecordType
## 
## 2014-07-16 Carlos: 
## - Sort by casting ClassNumber to number ClassNumber+0 
## - Add ItemType for filtering StartDate and EndDate conditions
##
## 2014-04-24 Carlos: 
## - Added eLibrary Plus Overdue Fine records
##
## 2013-12-23: Carlos
## - Display students without classes
##
## 2013-10-29: Carlos
## KIS - Hide [PPS Account No.], [Account Balance], [Inadequate Account Balance]
##
## 2011-08-02: YatWoon
## - Payment Letter template is changed to HTML-editor, so need to update coding to display HTML-editor data
##
## 2010-02-02: Carlos
## - Add option to allow user to choose whether to show the parent signature field or not 
##
## 2009-12-14: Max (200912101448)
## - Add option to include payment item which is not yet started
#################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$include_library_overdue_fine = $plugin['library_management_system'] && $sys_custom['ePayment']['OutstandingLibraryOverdueFine'] && $IncludeLibraryOverdueFine == '1';
if($include_library_overdue_fine){
	include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
	include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	
	$liblms = new liblms();
}

include_once($PATH_WRT_ROOT."templates/fileheader.php");

$lpayment = new libpayment();

$linterface = new interface_html();

$GeneralSetting = new libgeneralsettings();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

# get school data
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];

# get payment letter header & footer
$SettingList[] = "'PaymentLetterHeader'";
$SettingList[] = "'PaymentLetterHeader1'";
$SettingList[] = "'PaymentLetterFooter'";
$LetterSetting = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$letter_head = intranet_undo_htmlspecialchars($LetterSetting['PaymentLetterHeader']);
$letter_head2 = intranet_undo_htmlspecialchars($LetterSetting['PaymentLetterHeader1']);
$letter_foot = intranet_undo_htmlspecialchars($LetterSetting['PaymentLetterFooter']);

// $letter_head = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_head)));
// $letter_head2 = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_head2)));
// $letter_foot = nl2br(str_replace(" ","&nbsp;",stripslashes($letter_foot)));

$ExcludeEmpty = (isset($_REQUEST['ExcludeEmpty']) && $_REQUEST['ExcludeEmpty'] == 1) ? 1 : 0;
$ExcludeSignature = (isset($_REQUEST['ExcludeSignature']) && $_REQUEST['ExcludeSignature']==1)?1:0;
$page_breaker = "<P CLASS='breakhere'>";
$namefield = getNameFieldWithClassNumberByLang("c.");

# Get Account Balance
$student_balance = $lpayment->getBalanceInAssoc();
/*
$IncludeNotStarted = $_POST['IncludeNotStarted'];
if ($IncludeNonOverDue == 1) {
	if ($IncludeNotStarted == 1) {
		
	}
	else {
		$cond = " AND b.StartDate <=CURDATE()";
	}
} else {
	$cond = " AND b.EndDate < CURDATE() ";
}
*/
if($ItemType == '1'){ // Overdue Items Only
	$cond = " AND b.EndDate < CURDATE() ";
}else if($ItemType == '2'){ // Non-Overdue Items Only
	$cond = " AND b.EndDate >= CURDATE() ";
}else if($ItemType == '4'){ // Non-Started Items Only
	$cond = " AND b.StartDate > CURDATE() ";
}else if($ItemType == '3'){ // Started, Overdue and Non-Overdue Items
	$cond = " AND b.StartDate <= CURDATE() ";
}// else all payment items

$sql = "SELECT
          $namefield as StudentName,
          b.Name,
          a.Amount,
          IF(c.ClassName IS NULL OR c.ClassName='','".$Lang['AccountMgmt']['StudentNotInClass']."', c.ClassName) as ClassName,
          a.StudentID,
          d.PPSAccountNo,
          DATE_FORMAT(b.StartDate,'%Y/%m/%d') as StartDate,
          DATE_FORMAT(b.EndDate,'%Y/%m/%d') as EndDate,
		  c.ClassNumber,
		  'ePayment Payment Item' as RecordType,
		  b.DisplayOrder as DisplayOrder   
        FROM
          PAYMENT_PAYMENT_ITEMSTUDENT AS a
          INNER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
          LEFT JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
          LEFT JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
        WHERE 
          a.RecordStatus <> 1
          $cond ";
if($_REQUEST['ClassName'] == '-2'){
	$sql .= " AND (c.ClassName IS NULL OR c.ClassName='') ";
}else{          
	$sql .= (!empty($_REQUEST['ClassName'])) ? " AND c.ClassName = '".$_REQUEST['ClassName']."'" : "";
}

if($include_library_overdue_fine){
	$student_name_field = Get_Lang_Selection("lu.ChineseName","lu.EnglishName");
	$sql .= "UNION 
			 SELECT 
				$student_name_field as StudentName,
				CONCAT(b.BookTitle,if(TRIM(bu.ACNO)<>'',CONCAT('[',bu.ACNO,']'),IF(TRIM(bu.ACNO_BAK)<>'',CONCAT('[',bu.ACNO_BAK,']'),'')),
					IF(ol.DaysCount is NULL,' (".$Lang["libms"]["book_status"]["LOST"].")',
						CONCAT(' (".str_replace('!','',$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"])." ',ol.DaysCount,' ".$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"].")'))
				) as Name,
				if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as Amount,
				IF(lu.ClassName IS NULL OR lu.ClassName='','-',lu.ClassName) as ClassName, 
				lu.UserID as StudentID,
				pa.PPSAccountNo,
				DATE_FORMAT(ol.DateCreated,'%Y/%m/%d') as StartDate,
				DATE_FORMAT(CURDATE(),'%Y/%m/%d') as EndDate,
				lu.ClassNumber,
				'eLibrary Plus Overdue Fine' as RecordType,
				ol.OverDueLogID as DisplayOrder  
			FROM ".$liblms->db.".LIBMS_OVERDUE_LOG as ol 
			INNER JOIN ".$liblms->db.".LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
			INNER JOIN ".$liblms->db.".LIBMS_BOOK as b ON b.BookID=bl.BookID 
			INNER JOIN ".$liblms->db.".LIBMS_USER as lu ON lu.UserID=bl.UserID 
			LEFT JOIN ".$liblms->db.".LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
			LEFT JOIN PAYMENT_ACCOUNT as pa ON pa.StudentID=lu.UserID 
			WHERE ol.RecordStatus='OUTSTANDING' AND ol.IsWaived=0 ";
	if($_REQUEST['ClassName'] == '-2'){
		$sql .= " AND (lu.ClassName IS NULL OR lu.ClassName='') ";
	}else{
		$sql .= (!empty($_REQUEST['ClassName'])) ? " AND lu.ClassName = '".$_REQUEST['ClassName']."'" : "";
	}
}

$sql .= " ORDER BY ClassName, ClassNumber+0, StudentName, StartDate, RecordType DESC, DisplayOrder ";
$data = $lpayment->returnArray($sql, 8);

for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $itemname, $amount, $classname, $sid, $s_pps_no,$start_date,$due_date) = $data[$i];
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        $TotalAmount[$sid] += $amount;
        $Payment[$classname][] = array($sname, $itemname, $amount, $sid, $s_pps_no,$start_date,$due_date);
}

$classes = array();
if (empty($_REQUEST['ClassName']))
{
	if ($intranet_version == "2.5" || $intranet_version == "3.0") {
	  $sql = "SELECT 
	  					yc.ClassTitleEN
	  				FROM
	            YEAR y
	            inner join
	  					YEAR_CLASS yc
	            on y.YearID = yc.YearID
	  				WHERE
	  					AcademicYearID = '".Get_Current_Academic_Year_ID()."'
	  				ORDER BY y.sequence, yc.sequence";
	}
	else {
		$sql = "SELECT 
	  					ClassName
	  				FROM
	  					INTRANET_CLASS  
	  				Where 
	  					RecordStatus = '1' 
	  				Order by 
	  					ClassName";
	}
	$classes = $lpayment->returnVector($sql);
}
else
{
	if($_REQUEST['ClassName'] != '-2'){
  		$classes[] = $_REQUEST['ClassName'];
	}
}

if($_REQUEST['ClassName'] == '' || $_REQUEST['ClassName'] == '-2'){
	$classes[] = $Lang['AccountMgmt']['StudentNotInClass'];
}

function printLine($num){
	for($i=0;$i<$num;$i++){
		$x.="&nbsp;";
	}
	$x = "<u>$x</u>";
	return $x;
}

# header table
$header_table = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
$header_table.="<br /><div align=\"center\"><font size=\"+2\">$school_name</font></div><br />";
$header_table.="<div><font size=\"+2\">$letter_head</font></div><br />";
$header_table.="</td></tr>";
$header_table.="<tr><td><div>$letter_head2</div></td></tr>";
$header_table.="</table><br />";

# footer table
$footer_table = "<br /><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
$footer_table.="<br /><div align=\"left\">$letter_foot</div><br /></td></tr>";
$footer_table.="<tr><td>";
$footer_table.="<table align=\"right\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">";
if($ExcludeSignature!=1) {
	$footer_table.="<tr><td align=\"right\" nowrap>$i_Payment_PrintPage_PaymentLetter_ParentSign : ".printLine(40)."<br />&nbsp;</td></tr>";
}
if($ExcludeDate != 1){
	$footer_table.="<tr><td align=\"right\" nowrap>$i_Payment_PrintPage_PaymentLetter_Date : ".printLine(40)."<br /></td></tr>";
}
$footer_table.="</table><br />";
$footer_table.="</td></tr></table><br /><br />";


$display = "";
$display .="<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=center><tr><td>\n";
if(sizeof($TotalRecord) == 0)
{
	$content .= "<tr><td colspan=9><hr></td></tr>\n";
	$content .= "<tr><td>$i_no_record_exists_msg</td></tr>\n";
}else
for ($i=0; $i<sizeof($classes); $i++){
	$classname = $classes[$i];
	if($TotalRecord[$classname] <= 0)
		continue;
    $payment_record = $Payment[$classname];
    $curr_student = "";
    
    for($j=0; $j<sizeof($payment_record); $j++){
       list($StudentName, $ItemName, $Amount, $StudentID, $s_pps_no,$start_date,$due_date) = $payment_record[$j];
       if($curr_student!=$StudentID){
		   if($curr_student!=""){
			   		# add item list table
			   		$item_table.="<tr><td align=\"right\" colspan=\"3\">$i_Payment_PrintPage_Outstanding_Total :&nbsp;</td><td align=\"right\">$".number_format($TotalAmount[$curr_student],2)."</td></tr>";
			   		$item_table.="</table><br />";
			   		
			   		$content.=$item_table;
			   		
			   		# add amount due table		
			   		$t_balance = $student_balance[$curr_student] + 0;
                    $t_diff = $TotalAmount[$curr_student] - $t_balance;
                    if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                    {
                        $t_diff = 0;
                    }
                    $diff_str= $t_diff>0?"$".number_format(abs($t_diff),2):"--";
				    $amount_table = "<table border=\"1\" width=\"80%\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">";
				    $amount_table.="<tr><td>$i_Payment_PrintPage_Outstanding_LeftAmount:</td><td width=\"20%\">$diff_str</td></tr>";
				    $amount_table.="</table>";
					$content .=$amount_table;

			   		# add footer table
       				$content.=$footer_table.$page_breaker;
       	   }
	       
	       $content.=$header_table; # add header table
	       
       	   # add account summary table
		   $ac_table = "<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">";
		   $ac_table.= "<tr><td width=\"20%\">$i_UserStudentName:</td><td>$StudentName</td>";
		   if(!$isKIS){
		   	$ac_table.= "<td>$i_Payment_Field_PPSAccountNo:</td><td width=\"20%\">$s_pps_no</td>";
		   }
		   $ac_table.= "</tr>";
	   	   $ac_table.= "<tr><td width=\"20%\">$i_Payment_PrintPage_PaymentLetter_Date:</td><td>".date('Y/m/d')."</td>";
	   	   if(!$isKIS){
		   	$ac_table.= "<td>$i_Payment_PrintPage_Outstanding_AccountBalance:</td><td width=\"20%\">$".number_format($student_balance[$StudentID],2)."</td>";
	   	   }
		   $ac_table.= "</tr>";
		   $ac_table.= "</table><br />";
		   
		   $content.=$ac_table;
		   
		   $item_table = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\" width=\"100%\">";
		   $item_table .= "<tr><td>$i_Payment_PrintPage_PaymentLetter_UnpaidItem</td><td align=\"center\" width=\"15%\">$i_Payment_PrintPage_PaymentLetter_Date ($i_From)</td><td align=\"center\" width=\"15%\">$i_Payment_PrintPage_PaymentLetter_Date ($i_To)</td><td align=\"center\" width=\"15%\">$i_Payment_Field_Amount</td></tr>";

	   }
	   
	   
   	   # build item list table
	   $item_table .= "<tr><td>$ItemName</td><td align=\"center\">$start_date</td><td align=\"center\">$due_date</td><Td align=\"right\">$".number_format($Amount,2)."</td></tr>";
	   $curr_student = $StudentID;
	}
	
	if($curr_student!=""){
   		# add item list table
   		$item_table.="<tr><td align=\"right\" colspan=\"3\">$i_Payment_PrintPage_Outstanding_Total :&nbsp;</td><td align=\"right\">$".number_format($TotalAmount[$curr_student],2)."</td></tr>";
   		$item_table.="</table><br />";
   		
   		$content.=$item_table;
   		
   		# add amount due table		
   		$t_balance = $student_balance[$curr_student] + 0;
        $t_diff = $TotalAmount[$curr_student] - $t_balance;
        if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
        {
            $t_diff = 0;
        }
        $diff_str= $t_diff>0? "$".number_format(abs($t_diff),2):"--";
        if(!$isKIS){
		    $amount_table = "<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">";
		    $amount_table.="<tr><td>$i_Payment_PrintPage_Outstanding_LeftAmount:</td><td width=\"20%\">$diff_str</td></tr>";
		    $amount_table.="</table>";
			$content .=$amount_table;
        }

   		# add footer table
		$content.=$footer_table.$page_breaker;
   }
}




$display.=$content;
$display .="</td></tr></table>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>

<style type="text/css">
     P.breakhere {page-break-before: always}
     td {vertical-align:top}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?=$display?>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/filefooter.php");
?>
