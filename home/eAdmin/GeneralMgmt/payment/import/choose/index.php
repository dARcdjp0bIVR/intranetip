<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
    
################################################################
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.form1.flag.value = 0;
     par.generalFormSubmitCheck(par.form1);
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $iDiscipline['class']?>:</span></td>
			<td width="80%"><?php echo $select_class; ?></td>
		</tr>
		<tr><td colspan="2" height="5"></td></tr>		
		<?php if($targetClass != "")
		{ ?>
		<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td colspan="2" height="5"></td></tr>
		<tr>
			<td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $iDiscipline['students']?>: </span></td>
			<td width="80%" style="align: left">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
					<tr> 
						<td>
		       				<?php echo $select_students; ?>
		       			</td>
		       			<td style="vertical-align:bottom">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
									</td>
								</tr>
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       	</table>
			</td>
		</tr>
		<?php
		} ?>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>