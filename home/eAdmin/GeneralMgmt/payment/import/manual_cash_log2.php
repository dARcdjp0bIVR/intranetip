<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();


$lpayment = new libpayment();
$linterface = new interface_html();
$lclass = new libclass();


if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();

$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}

if($TargetDate=="")
	$TargetDate = date('Y-m-d');
	
if(sizeof($student)>0)
{
	$student_list = implode(",",$student);
	$namefield = getNameFieldWithClassNumberByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN ($student_list) ORDER BY ClassName, ClassNumber, EnglishName";
	$result = $lclass->returnArray($sql,2);
	
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			$css ="tabletext";
			list($uid, $name) = $result[$i];
			$table_content .= "<tr class='$css'>";
			$table_content .= "<input type=hidden name=student[] value=$uid>";
			$table_content .= "<td>$name</td>";
			$table_content .= "<td><input type=text name=amount_$uid size=8></td>";
			$table_content .= "<td><input type=text name=ref_$uid size=8></td>";
			$table_content .= "<td><input type=text name=TargetDate_$uid size=10>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span></td>";
			$curr_hour = date("H");
			$curr_min = date("i");
			$table_content .= "<td>".getSelectByValue($hours,"name=hour_$uid",0,0,1).":".getSelectByValue($minutes,"name=min_$uid",0,0,1)."</td>";
			$table_content .= "</tr>";
		}
	}
}



$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_DepositManual";


$TAGS_OBJ[] = array($i_Payment_Menu_ManualCashDeposit, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$STEPS_OBJ[] = array($i_Discipline_System_Select_Student, 0);
$STEPS_OBJ[] = array($i_Discipline_System_Add_Record, 1);

$linterface->LAYOUT_START();
?>



<SCRIPT Language="JavaScript">
function setAllDate(checking)
{
	if(checking == 1)
	{
		temp_value = document.form1.TargetDate_SetAll.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = temp_value;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = '';
		<?
		}
		?>
	}
}
function setAllTime(checking)
{
	if(checking == 1)
	{
		selected_hour = document.form1.all_hour.value;
		selected_min = document.form1.all_min.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = selected_hour;
			temp_min.value = selected_min;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = '00';
			temp_min.value = '00';
		<?
		}
		?>
	}
}
function checkForm()
{
	var checking=0;
	
	<?
	for($i=0; $i<sizeof($result); $i++)
	{
		list($u_id, $u_name) = $result[$i];
	?>
		temp_date 	=	eval("document.form1.TargetDate_"+<?=$u_id?>);
		temp_hour 	= 	eval("document.form1.hour_"+<?=$u_id?>);
		temp_min 	= 	eval("document.form1.min_"+<?=$u_id?>);
		temp_amount = 	eval("document.form1.amount_"+<?=$u_id?>);
		temp_ref 	=	eval("document.form1.ref_"+<?=$u_id?>);
		year		=	temp_date.value.substr(0,4);
		month		=	temp_date.value.substr(5,2);
		day			=	temp_date.value.substr(8,2);
		
		if(temp_amount.value!='')
		{
			if(!isNaN(temp_amount.value))
			{
				if(temp_amount.value > 0)
				{
					if(temp_ref.value!='')
					{
						if(temp_date.value!='')
						{
							if((!isNaN(year))&&(!isNaN(month))&&(!isNaN(day)))
							{
								if(year%4==0)
								{
									switch(month)
									{
										case '01':
										case '03':
										case '05':
										case '07':
										case '08':
										case '10':
										case '12':
											if(day<=31)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										
										case '04':
										case '06':
										case '09':
										case '11':
											if(day<=30)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										case '02':
											if(day<=29)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										default:
											break;
									}
								}
								else
								{
									switch(month)
									{
										case '01':
										case '03':
										case '05':
										case '07':
										case '08':
										case '10':
										case '12':
											if(day<=31)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										
										case '04':
										case '06':
										case '09':
										case '11':
											if(day<=30)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										case '02':
											if(day<=28)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										default:
											break;
									}
								}
							}
						}
						else
						{
							alert("<?=$i_Payment_DepositDateWarning?>");
							return false;
						}
					}
					else
					{
						alert("<?=$i_Payment_RefCodeWarning?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_Payment_DepositAmountWarning?>");
					return false;
				}
			}
			else
			{
				alert("<?=$i_Payment_DepositAmountWarning?>");
				return false;
			}
		}
		else
		{
			alert("<?=$i_Payment_AmountWarning?>");
			return false;
		}
	<?
	}
	?>
if(checking == 1)
{
	return true;
}	
}
</SCRIPT>

<form name="form1" method="POST" action="manual_cash_log_update.php" onSubmit="return checkForm()">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	</td>
</tr>
<tr>
	<td><BR><BR>

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center"><tr class="tabletop tabletoplink">
	<td width=10% style="vertical-align:bottom"><?=$i_UserName?></td>
	<td style="vertical-align:bottom"><?=$i_Payment_Field_Amount?></td>
	<td style="vertical-align:bottom"><?=$i_Payment_Field_RefCode?></td>
	<td style="vertical-align:bottom">
		<?=$i_Payment_CashDepositDate?><br>
		<input type=text name=TargetDate_SetAll size=10 value=<?=$TargetDate?>>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;<input type=checkbox name=SetAllDate onClick="this.checked?setAllDate(1):setAllDate(0)"><br>
	</td>
	<td style="vertical-align:bottom">
		<?=$i_Payment_CashDepositTime?><br>
		<?=getSelectByValue($hours,"name=all_hour",$curr_hour,0,1)?>:<?=getSelectByValue($minutes,"name=all_min",$curr_min,0,1)?><input type=checkbox name=SetAllTime onClick="this.checked?setAllTime(1):setAllTime(0)">
	</td>
</tr>
<?=$table_content?>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<!--<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>-->
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:location.href='manual_cash_log.php'") ?>
			</td>
		</tr>
	</table>

	</td>
</tr>
</table>

<br />
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="type" value="<?=$type?>" />
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
