<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();

$list = implode(",",$StudentID);
$sql = "UPDATE PAYMENT_ACCOUNT SET PPSAccountNo = '' WHERE StudentID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: pps_account_list.php?msg=2");
?>
