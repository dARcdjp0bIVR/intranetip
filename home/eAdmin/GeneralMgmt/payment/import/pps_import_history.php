<?php
// Editing by 
/*
 * 2018-03-19 (Carlos): $sys_custom['ePayment']['PPSFileLogCheckFileHash'] - added the file hash value column as ID.
 * 2015-06-30 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

$linterface = new interface_html();
$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLog";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 0;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$pps_type_ary = array(
					array('0', 'IEPS'),
					array('1', 'Counter Bill')
				);
$pps_type_selection = getSelectByArray($pps_type_ary, ' id="FileType" name="FileType" onchange="document.form1.submit();" ', $FileType, 1, 0);

$currentAcademicYearId = Get_Current_Academic_Year_ID();
$today_ts = time();

if($FromDate == ''){
	$FromDate = empty($currentAcademicYearId)? date('Y-m-d') : date('Y-m-d',getStartOfAcademicYear($today_ts));
}
if($ToDate == ''){
	$ToDate = empty($currentAcademicYearId)? date('Y-m-d') : date('Y-m-d',getEndOfAcademicYear($today_ts));
}

$date_range_filter = '&nbsp;|&nbsp;'.$linterface->Get_Checkbox("UseDateRange", "UseDateRange", "1", $UseDateRange==1, '', '', 'onUseDateRangeChecked(this.checked);', '');
$date_range_filter.= $Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("FromDate",$FromDate,"","yy-mm-dd","","","","","",0,0, $UseDateRange!=1).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("ToDate",$ToDate,"","yy-mm-dd","","","","","",0,0, $UseDateRange!=1);
$date_range_filter.= $linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], 'button', "submitDateRangeValue();", "submit_btn", /*$UseDateRange!=1? 'disabled':*/ '', "", '');

$sql = "SELECT 
			CONCAT(SUBSTRING(FileDate,1,4),'-',SUBSTRING(FileDate,5,2),'-',SUBSTRING(FileDate,7,2)) as FileDate,
			CASE FileType 
			WHEN 0 THEN 'IEPS' 
			WHEN 1 THEN 'Counter Bill' 
			END as FileType ";
if($sys_custom['ePayment']['PPSFileLogCheckFileHash']){
	$sql .= ",FileHashValue ";
}
$sql.=" FROM PAYMENT_CREDIT_FILE_LOG 
		WHERE 1 ";
if($FileType != ''){
	$sql .= " AND FileType='$FileType' ";
}
if($UseDateRange==1 && $FromDate != '' && $ToDate != ''){
	$sql .= " AND (CONCAT(SUBSTRING(FileDate,1,4),'-',SUBSTRING(FileDate,5,2),'-',SUBSTRING(FileDate,7,2)) BETWEEN '$FromDate' AND '$ToDate') ";
}

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("FileDate", "FileType");
if($sys_custom['ePayment']['PPSFileLogCheckFileHash']){
	$li->field_array = array_merge($li->field_array,array("FileHashValue"));
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array_fill(0,sizeof($li->field_array),0);
$li->wrap_array = array_fill(0,sizeof($li->field_array),0);
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

// TABLE COLUMN
$cols_ratio = array(1,1);
if($sys_custom['ePayment']['PPSFileLogCheckFileHash']){
	$cols_ratio[] = 1;
}
$cols_total_ratio = array_sum($cols_ratio);

$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='".sprintf("%.2f",$cols_ratio[$pos]/$cols_total_ratio*100)."%'>".$li->column_IP25($pos++, $Lang['General']['Date'])."</th>\n";
$li->column_list .= "<th width='".sprintf("%.2f",$cols_ratio[$pos]/$cols_total_ratio*100)."%'>".$li->column_IP25($pos++, $Lang['ePayment']['RecordType'])."</th>\n";
if($sys_custom['ePayment']['PPSFileLogCheckFileHash']){
	$li->column_list .= "<th width='".sprintf("%.2f",$cols_ratio[$pos]/$cols_total_ratio*100)."%'>".$li->column_IP25($pos++, 'ID')."</th>\n";
}

$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLog,"pps_log.php",0);
$TAGS_OBJ[] = array($Lang['ePayment']['PPSFileImportLog'],"pps_import_history.php",1);

//$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLog, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form id="form1" name="form1" action="<?=$_SERVER['SCRIPT_NAME']?>" method="POST">
<div class="table_board">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td valign="bottom">
					<div class="table_filter">
						<?=$pps_type_selection.$date_range_filter?>
				    </div>
				</td>
				<td valign="bottom">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2"><?php echo $li->display("96%", "common_table_list_v30"); ?></td>
			</tr>
		</tbody>
	</table>
</div>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<br />
<script type="text/javascript" language="JavaScript">
function onUseDateRangeChecked(checked)
{
	$('#FromDate').attr('disabled',!checked);
	$('#ToDate').attr('disabled',!checked);
	//$('#submit_btn').attr('disabled', !checked);
}

function submitDateRangeValue()
{
	if($('#UseDateRange').is(':checked'))
	{
		if($('#DPWL-FromDate').html() != '')
		{
			$('#FromDate').focus();
			return;
		}
		if($('#DPWL-ToDate').html() != ''){
			$('#ToDate').focus();
			return;
		}
	}
	document.getElementById('form1').submit();
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>