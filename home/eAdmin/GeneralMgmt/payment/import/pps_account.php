<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLinkage";
$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLink, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_import);

if ($failed == 2) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
?>
<br/ >
<form name="form1" method="post" action="pps_account_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"><?= $SysMsg ?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td class="tabletext"><input class="file" type="file" name="userfile"><br>
					<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_general_Format ?>
					</td>
					<td class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" value="1" id="format1" checked></td>
								<td class="tabletext">
									<label for="format1"><?=$i_Payment_Import_Format_PPSLink?></label><br />
									<a class="tablelink" href="<?= GET_CSV("pps_account_sample.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" id="format2" value="2"></td>
								<td class="tabletext">
									<label for="format2"><?=$i_Payment_Import_Format_PPSLink2?></label><br />
									<a class="tablelink" href="<?= GET_CSV("pps_account_sample2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['formClassMapping']['Reference']?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='pps_account_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.userfile");
intranet_closedb();
?>
