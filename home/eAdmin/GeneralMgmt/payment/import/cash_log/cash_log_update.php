<?php
// Editing by 
/*
 * 2019-10-25 (Carlos): Check CSRF token to avoid duplicated processing.
 * 2016-08-26 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] added approval logic.
 * 2016-08-23 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - added deposit method 1=Cash,2=Bank transfer,3=Cheque deposit.
 * 						$sys_custom['ePayment']['CashDepositAccount'] - added [Account] and [ReceiptNo].
 * 2015-11-27 (Carlos): Modified allow import for left student and staff.
 * 2015-07-22 (Carlos): $sys_custom['ePayment']['CreditMethodAutoPay'] - added deposit method, 1=Cash, 2=Auto-pay
 * 2014-04-02 (Carlos): Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
 * 2013-11-05 (Carlos): Checked if no payment account, create one for the use, prevent fail to update balance issue
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!verifyCsrfToken($csrf_token, $csrf_token_key)){
	header("Location: cash_log.php?clear=1&error=1");
	exit;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$lpayment = new libpayment();



if($format=="")
	header("Location: cash_log.php");

if ($confirm == 1)
{
	/*
	 * PAYMENT_CREDIT_TRANSACTION.RecordStatus=1 is temp impoty record
	 * PAYMENT_CREDIT_TRANSACTION.RecordStatus=0 is active/valid record
	 */
	$need_approval = false;
	$credit_transaction_table = 'PAYMENT_CREDIT_TRANSACTION';
	if($sys_custom['ePayment']['CashDepositApproval'] && $lpayment->isCashDepositNeedApproval()){
		$need_approval = true;
		$credit_transaction_table = 'PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD';
	}
	
    $sql = "LOCK TABLES
                 $credit_transaction_table WRITE
                 ,TEMP_CASH_DEPOSIT WRITE
                 ,TEMP_CASH_DEPOSIT as a WRITE
                 ,INTRANET_USER as b READ
                 ,PAYMENT_ACCOUNT as WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG as WRITE";
    $li->db_db_query($sql);

    if($format==1){
    	$sql = "INSERT INTO $credit_transaction_table (
    						StudentID, 
    						Amount,
    						RecordType, 
    						RecordStatus, 
    						TransactionTime, 
    						RefCode,
    						AdminInCharge ";
    	if($sys_custom['ePayment']['CashDepositRemark']){
    		$sql .= ",Remark ";
    	}
    	if($sys_custom['ePayment']['CashDepositAccount']){
    		$sql .= ",Account,ReceiptNo ";
    	}
		$sql.= ")
             	SELECT 
             		b.UserID, 
             		a.Amount,
             		".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"a.RecordType":"2").", 
             		1,
             		a.InputTime, 
             		a.RefCode,
             		'".$_SESSION['UserID']."' ";
        if($sys_custom['ePayment']['CashDepositRemark']){
        	$sql.=",a.Remark ";	
        }
        if($sys_custom['ePayment']['CashDepositAccount']){
        	$sql .= ",a.Account,a.ReceiptNo ";
        }
		$sql.="FROM 
               	TEMP_CASH_DEPOSIT as a 
               	LEFT OUTER JOIN 
               	INTRANET_USER as b 
               	ON 
               		a.ClassName = b.ClassName 
               		AND a.ClassNumber = b.ClassNumber 
               		AND b.RecordType = 2 
               		AND b.RecordStatus IN (0,1,2,3) 
               WHERE b.UserID IS NOT NULL";
		}else if($format==2){
	    $sql = "INSERT INTO $credit_transaction_table (
	    					StudentID, 
	    					Amount,
	    					RecordType, 
	    					RecordStatus, 
	    					TransactionTime, 
	    					RefCode,
    						AdminInCharge ";
    	if($sys_custom['ePayment']['CashDepositRemark']){
        	$sql.=",Remark ";	
        }
        if($sys_custom['ePayment']['CashDepositAccount']){
    		$sql .= ",Account,ReceiptNo ";
    	}
		$sql.=")
	            SELECT 
	            	b.UserID, 
	            	a.Amount,
	            	".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"a.RecordType":"2").", 
	            	1,
	            	a.InputTime, 
	            	a.RefCode,
	            	'".$_SESSION['UserID']."' ";
	    if($sys_custom['ePayment']['CashDepositRemark']){
        	$sql.=",a.Remark ";	
        }
        if($sys_custom['ePayment']['CashDepositAccount']){
        	$sql .= ",a.Account,a.ReceiptNo ";
        }
		$sql.=" FROM 
	            	TEMP_CASH_DEPOSIT as a 
	            	LEFT OUTER JOIN 
	            	INTRANET_USER as b 
	            	ON a.UserLogin = b.UserLogin 
	            		AND b.RecordType IN (1,2) 
	            		AND b.RecordStatus IN (0,1,2,3) 
	           	WHERE b.UserID IS NOT NULL";
    }
    $li->db_db_query($sql);

	$transaction_detail_map = array(2=>$i_Payment_TransactionDetailCashDeposit,4=>$Lang['ePayment']['TransactionDetailAutoPay'],6=>$Lang['ePayment']['TransactionDetailBankTransfer'],7=>$Lang['ePayment']['TransactionDetailChequeDeposit']);

    # Update Ref Code
    $sql = "UPDATE $credit_transaction_table SET
               RefCode = CONCAT('CSH',TransactionID) 
               WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2")." AND (RefCode IS NULL OR RefCode = '')";
              
    $li->db_db_query($sql);

	if(!$need_approval)
	{
	    $sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),RefCode ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?",RecordType ":""); 
	    $sql.= " FROM $credit_transaction_table WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2")." ORDER BY TransactionTime";
	    $transactions = $li->returnArray($sql);
		
	    $values = "";
	    $delim = "";
	    for ($i=0; $i<sizeof($transactions); $i++)
	    {
	         list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
	         $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$sid'";
	         $temp = $li->returnVector($sql);
	         if(count($temp)==0){
	         	$sql_create_account = "INSERT INTO PAYMENT_ACCOUNT (StudentID,Balance) VALUES ('$sid',0)";
	         	$li->db_db_query($sql_create_account);
	         }
	         
	         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid'";
	         $li->db_db_query($sql);
	         $balanceAfter = $temp[0]+$amount;
	         if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
	         	//$transaction_detail = $transactions[$i]['RecordType'] == 4? $Lang['ePayment']['TransactionDetailAutoPay'] : $i_Payment_TransactionDetailCashDeposit;
	         	$transaction_detail = $transaction_detail_map[$transactions[$i]['RecordType']];
	         }else{
	         	$transaction_detail = $i_Payment_TransactionDetailCashDeposit;
	         }
	         # Change transaction time on overall trans to now()
	         #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailCashDeposit','$refCode')";
	         $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$transaction_detail','".$li->Get_Safe_Sql_Query($refCode)."')";
	         $delim = ",";
	    }
	    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
	                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
	                   TransactionTime,Details,RefCode) VALUES $values";
	    $li->db_db_query($sql);
		
	    # Update in progress to finished status
	    $sql = "UPDATE $credit_transaction_table SET
	               RecordStatus = 0,
	               DateInput = now() WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2");
	    $li->db_db_query($sql);
	
	}
	
    $sql = "DELETE FROM TEMP_CASH_DEPOSIT";
    $li->db_db_query($sql);
	
    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);
    $msg = 2;
}
else
{
    $msg = "";
}

$Msg = $Lang['ePayment']['CashDepositImportSuccess'];

intranet_closedb();
header("Location: cash_log.php?Msg=".urlencode($Msg));
?>
