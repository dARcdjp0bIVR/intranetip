<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$is_approval_admin || count($_POST['TransactionID'])==0) {
	intranet_closedb();
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'] = $_POST['RecordStatus'];
	header ("Location:cash_deposit_approval.php");
	exit();
}

$TransactionID = IntegerSafe($_POST['TransactionID']);

$sql = "DELETE FROM PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD WHERE TransactionID IN (".implode(",",$TransactionID).")";
$success = $lpayment->db_db_query($sql);

$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'] = $_POST['RecordStatus'];
$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];

intranet_closedb();
header("Location:cash_deposit_approval.php");
exit;
?>