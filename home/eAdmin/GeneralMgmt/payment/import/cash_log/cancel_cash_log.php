<?php
// Editing by 
/*
 * 2019-06-26 (Carlos): Does not allow to undo eWallet credit records here. Can void the eWallet credit at the corresponding eWallet(TNG/AlipayHK) transaction list.
 * 2017-10-10 (Carlos): Fixed the sorting of Amount and Balance as numeric values.
 * 2016-08-26 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] added [Cash Deposit Approval] page tab.
 * 2014-04-02 (Carlos): Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";
$linterface = new interface_html();
### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",1);
if($sys_custom['ePayment']['CashDepositApproval']){
	$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();
	if($is_approval_admin){
		$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",0);
	}
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

// resturn message
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$StudentID = $_REQUEST['StudentID'];

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}
	 

$namefield = getNameFieldWithClassNumberByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
  $archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
  //$archive_namefield="c.ChineseName";
}else  {
	//$archive_namefield ="c.EnglishName";
	$archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("f.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(g.ChineseName IS NULL,g.EnglishName,g.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(g.EnglishName IS NULL,g.ChineseName,g.EnglishName)";
}

# date range
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$search_by=$search_by==""?1:$search_by;                
                
if($search_by!=1)                
	$date_cond = " AND DATE_FORMAT(a.DateInput,'%Y-%m-%d') >= '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";
else
  $date_cond = " AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d') >= '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";

switch($user_type){
	case 2: 
		$user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; 
		$user_cond .= " AND (b.ClassName like '%".$lpayment->Get_Safe_Sql_Like_Query($ClassName)."%') ";
		if ($StudentID != "" && $ClassName != "") 
			$user_cond .= " AND (b.UserID = '".$lpayment->Get_Safe_Sql_Query($StudentID)."') ";
		break;
	case 1: 
		$user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; 
		break;
	//case 1: $user_cond = " AND (b.RecordType=1 )"; break;
	default: 
		$user_cond .= " AND ((b.RecordType=2 AND b.ClassName like '%".$lpayment->Get_Safe_Sql_Like_Query($ClassName)."%') OR b.RecordType=1) ";
		if ($StudentID != "" && $ClassName != "") 
			$user_cond .= " AND (b.UserID = '".$lpayment->Get_Safe_Sql_Query($StudentID)."') ";
		break;
}

$escaped_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);

$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),
               ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
               ".$lpayment->getWebDisplayAmountFormatDB("d.Balance")." AS Balance,
               a.RefCode, ";
if($sys_custom['ePayment']['CashDepositRemark']){
	$sql .= "a.Remark,";
}
	$sql .= " IF((f.UserID IS NULL AND g.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(f.UserID IS NULL AND g.UserID IS NULL,a.AdminInCharge,$Inchargenamefield)) as DisplayAdminInCharge, 
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i'),
               IF(
               	(d.Balance >= a.Amount AND b.UserID IS NOT NULL AND t.RecordID IS NULL),
               	CONCAT(
               		'<input onclick=\"Check_Balance(\'',
               		b.UserLogin,
               		'\',',
               		a.Amount,
               		',this)\" type=checkbox name=TransactionID[] value=', 
               		a.TransactionID ,
               		'>',
               		'<script>',
               		b.UserLogin,
               		'=',
               		d.Balance,
               		';</script>',
               		'<input type=hidden name=UserLogin[] value=',
               		b.UserLogin,
               		'>',
               		'<input type=hidden name=Amount[] value=',
               		a.Amount,
               		'>'
               	),
               	'&nbsp;'
               ) as TransactionID   
         FROM
             PAYMENT_CREDIT_TRANSACTION as a 
             LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID=c.UserID )
             LEFT OUTER JOIN PAYMENT_ACCOUNT as d ON (a.StudentID = d.StudentID) 
             LEFT OUTER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as e ON (a.TransactionID = e.RelatedTransactionID and e.TransactionType=9) 
			 LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=-1 AND t.CreditTransactionID=a.TransactionID  
             LEFT JOIN 
             INTRANET_USER as f 
             ON (a.AdminInCharge = f.UserID) 
             LEFT JOIN 
             INTRANET_ARCHIVE_USER as g 
             ON (a.AdminInCharge = g.UserID) 
         WHERE
         			a.RecordType not in (1) 
         			AND 
         			e.LogID is NULL 
         			AND 
              (
               b.EnglishName LIKE '%$escaped_keyword%' OR
               b.ChineseName LIKE '%$escaped_keyword%' OR
               b.ClassName LIKE '%$escaped_keyword%' OR
               b.ClassNumber LIKE '%$escaped_keyword%' OR
               a.RefCode LIKE '%$escaped_keyword%' OR
               a.AdminInCharge LIKE '%$escaped_keyword%' OR
               c.EnglishName LIKE '%$escaped_keyword%' OR
               c.ChineseName LIKE '%$escaped_keyword%' OR
               c.ClassName LIKE '%$escaped_keyword%' OR
               c.ClassNumber LIKE '%$escaped_keyword%' OR 
               f.EnglishName LIKE '%$escaped_keyword%' OR 
               f.ChineseName LIKE '%$escaped_keyword%' OR 
               g.EnglishName LIKE '%$escaped_keyword%' OR 
               g.ChineseName LIKE '%$escaped_keyword%' 
              )
             $date_cond
             $user_cond
                ";
//echo '<pre>'.$sql.'</pre>'; die;
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","a.Amount+0","d.Balance+0","a.RefCode","a.Remark","DisplayAdminInCharge","a.TransactionTime","a.DateInput","TransactionID");
}else{
	$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","a.Amount+0","d.Balance+0","a.RefCode","DisplayAdminInCharge","a.TransactionTime","a.DateInput","TransactionID");
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
$li->column_array[sizeof($li->field_array)-1] = 10;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tabletoplink>#</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_Payment_Field_Balance)."</td>\n";
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->column_list .= "<td width=10%>".$li->column($pos++, $Lang['General']['Remark'])."</td>\n";
}
$li->column_list .= "<td width=10%>".$li->column($pos++, $i_Payment_Field_AdminInCharge)."</td>\n";
$li->column_list .= "<td width=15%>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width=15%>".$li->column($pos++, $i_Payment_Field_PostTime)."</td>\n";
$li->column_list .= "<td width=1><input type=checkbox id=CheckAll onclick=\"Check_All_Deposit(this.checked);\"></td>\n";


$select_user = "<SELECT name='user_type' onChange='Show_Class_Layer(this.value);'>";
$select_user.= "<OPTION value='2'".($user_type==2?"SELECTED":"").">$i_identity_student</OPTION>";
$select_user.= "<OPTION value='1' ".($user_type==1?"SELECTED":"").">$i_identity_teachstaff</OPTION>";
$select_user.= "<OPTION value='' ".($user_type==""?"SELECTED":"").">$i_Payment_All</OPTION>";
$select_user.= "</SELECT>&nbsp;&nbsp;";

$lclass = new libclass();
if ($_REQUEST['user_type'] != 1) {
	$select_class = $lclass->getSelectClass("name=ClassName onChange=\"document.form1.submit();\"",''.$_REQUEST['ClassName'].'');
}
else {
	$ClassDisplay = 'style="display:none;"';
}

if ($_REQUEST['ClassName']) {
	$select_student = $lclass->getStudentSelectByClass($_REQUEST['ClassName'],"name=StudentID onChange=\"document.form1.submit();\"",$StudentID);
}
else {
	$StudentDisplay = 'style="display:none;"';
}
//$toolbar = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate&search_by=$search_by')>".exportIcon()."$button_export</a>";
//$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'pps_index.php')>".exportIcon()."$button_export_pps</a>";
#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$functionbar .= "<a class=\"tabletool\" href=\"javascript:checkAlert(document.form1,'TransactionID[]','cancel_cash_log_process.php','".$Lang['Payment']['CancelDepositWarning']."')\">
									<img src='".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_cancel_deposit.gif' border='0' align='absmiddle'>
									".$Lang['Btn']['Cancel']."
								</a>&nbsp;\n";

$searchbar .= $select_user;
$searchbar .= '<span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'</span>';
$searchbar .= '<span id="StudentLayer" '.$StudentDisplay.'>'.$select_student.'</span>';
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".escape_double_quotes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "document.form1.submit();")."&nbsp;\n";

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function Check_Balance(UserLogin,Amount,obj,Silent) {
	var Silent = Silent || false;
	if (obj.checked) {
		if ((eval(UserLogin + "-" + Amount)) < 0) {
			if (!Silent)
				alert('<?=$Lang['Payment']['CancelDepositInvalidWarning']?>');
			obj.checked = false;
			return false;
		}
		else {
			eval(UserLogin + "-=" + Amount);
			return true;
		}
	}
	else {
		eval(UserLogin + "+=" + Amount);
		return true;
	}
}

function Check_All_Deposit(ObjChecked) {
	var DepositCheck = document.getElementsByName('TransactionID[]');
	var UserLoginCheck = document.getElementsByName('UserLogin[]');
	var AmountCheck = document.getElementsByName('Amount[]');
	var Flag = true;
	
	for (var i=0; i< DepositCheck.length; i++) {
		if (ObjChecked) {
			if (!DepositCheck[i].checked) {
				DepositCheck[i].checked = ObjChecked;
				if (!Check_Balance(UserLoginCheck[i].value,AmountCheck[i].value,DepositCheck[i],true)) {
					Flag = false;
				}
			}
		}
		else {
			if (DepositCheck[i].checked) {
				DepositCheck[i].checked = ObjChecked;
				if (!Check_Balance(UserLoginCheck[i].value,AmountCheck[i].value,DepositCheck[i],true)) {
					Flag = false;
				}
			}
		}
	}
	
	if (!Flag) {
		alert("Some of the cash deposit item cannot be checked/ cancelled, because their balance is not enough.");
	}
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
}

function Show_Class_Layer(UserType) {
	if (UserType != 1) 
		document.getElementById("ClassLayer").style.display = '';
	else 
		document.getElementById("ClassLayer").style.display = 'none';
}

function Show_Student_Layer(ClassName) {
	if (Trim(ClassName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_student_selection.php';
	  var postContent = 'ClassName='+encodeURIComponent(ClassName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
			  document.getElementById("StudentLayer").innerHTML = ResponseText;
			  document.getElementById("StudentLayer").style.display = "";
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		document.getElementById("StudentLayer").innerHTML = "";
		document.getElementById("StudentLayer").style.display = "none";
	}
}


$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<form name="form1" method="post" action='cancel_cash_log.php'>
<!-- date range -->
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletext" align="right"><?=($dup=="" && $no_trans=="")?$lpayment->getResponseMsg($msg):$xmsg?></td>
				</tr>
				<tr>
					<td colspan=2" class="tabletextremark"><?= $i_Payment_Note_StudentRemoved ?></td>
				</tr>
	    	<tr>
	    		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['DateType']?></td>
		    	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
          	<input type='radio' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?>>
						<?=$i_Payment_Search_By_PostTime?>&nbsp;&nbsp;
						<input type='radio' name='search_by' value='1' <?=($search_by==1?"checked":"")?>>
						<?=$i_Payment_Search_By_TransactionTime?>
          </td>
        </tr>
        <tr>
		    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?></td>
          <td class="tabletext" width="70%">
	          <input type=text name=FromDate id="FromDate" value="<?=escape_double_quotes($FromDate)?>" size=10>&nbsp;
						<?=$i_Profile_To?>
						<input type=text name=ToDate id="ToDate" value="<?=$ToDate?>" size=10>&nbsp;
						<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;&nbsp;
						<?= $linterface->GET_SMALL_BTN($button_submit, "button", "submitForm(document.form1);") ?>
	   			</td>
        </tr>
		    </table>
	    </td>
	</tr>
	<tr>
		<td>
	    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
	    </table>
	  </td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr><td><?=$functionbar?></td></tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
