<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_DepositCSV";

if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

$format=$format==1?1:2;
if($format==1)
	$file_format = array ("ClassName","ClassNumber","Amount","Time","RefCode");
if($format==2)
	$file_format = array ("UserLogin","Amount","Time","RefCode");

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: cash_log.php");
        exit();
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           //$col_name = array_shift($data);                   # drop the title bar
           $data = $limport->GET_IMPORT_TXT($filepath);
		   $col_name = array_shift($data);
           
           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: cash_log.php?error=1");
               exit();
           }
           
           
      	   # build student array
    	   $t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber,RecordType FROM INTRANET_USER WHERE (RecordType=2 OR RecordType=1) AND RecordStatus IN(0,1,2)";
       	   $t_result = $li->returnArray($t_sql,5);
       	   for($i=0;$i<sizeof($t_result);$i++){
	       	   list($u_id,$u_login,$u_class,$u_classnum,$type) = $t_result[$i];
	       	   if($format==1){
		       	   $students[$u_class][$u_classnum]['uid']=$u_id;
		       	   $students[$u_class][$u_classnum]['login']=$u_login;
		       }
	       	   else if($format==2){
		       	   $students[$u_login]['uid'] = $u_id;
		       	   $students[$u_login]['class'] = $u_class;
		       	   $students[$u_login]['classnumber']=$u_classnum;
		       	   $students[$u_login]['type']=$type;

		       }
	       }
          
           $error = false;
           
           # Update TEMP_CASH_DEPOSIT
           $values = "";
           $delim = "";
           for ($i=0; $i<sizeof($data); $i++)
           {
						# check for empty line
						$test = trim(implode("",$data[$i])); 
						if($test=="") continue;
				
	           if($format==1){ # Format 1
                	list($class, $classnum, $amount, $tran_time, $refCode) = $data[$i];
                	$class = trim($class);
                	$classnum = trim($classnum);
                	if ($class != "" && $classnum != "" && $amount > 0)
                	{
	                	if($students[$class][$classnum]['uid']==""){ # no match student
	                		$error = true;
	                    $error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>","","$amount","<i>$tran_time</i>","<i>$refCode</i>");
	                  }else{
		                  $user_login = $students[$class][$classnum]['login'];
		                  $user_type=2;
		                }
		            	}
                }
                else if($format==2){ # Format 2

                	list($user_login, $amount, $tran_time, $refCode) = $data[$i];
                	
                	if($students[$user_login]['uid']==""){ # no matched student
                		$error = true;
                    	$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>");
                	}
	               	else if($user_login!=""){
	                  	$user_type = $students[$user_login]['type'];
	                   	$class = $user_type==1?"-" :$students[$user_login]['class'];
	                   	$classnum = $user_type==1?"-" :$students[$user_login]['classnumber'];
                   	}
	            }
	            if (!$error && $amount>0){
		            if($user_type==1 ||  ($user_type==2 && $class != "" && $classnum != "")){
	                    if ($tran_time != "")
	                        $time_str = "'$tran_time'";
	                    else $time_str = "now()";
	                    $values .= "$delim ('$user_login','$class','$classnum','$amount',$time_str,'$refCode')";
	                    $delim = ",";
                	}
                }
              
           }
           if(!$error){
           	  $sql = "INSERT INTO TEMP_CASH_DEPOSIT (UserLogin,ClassName,ClassNumber,Amount,InputTime,RefCode) VALUES $values";
          		$li->db_db_query($sql);
        	}
        }
        else
        {
            header ("Location: cash_log.php?error=1");
            exit();
        }
}

if(!$error){
	$namefield = getNameFieldByLang("b.");
	if($format==1){
		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin, a.Amount, a.InputTime, a.RefCode
        	FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b
            	 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)";
    }else if($format ==2){
   		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin, a.Amount, a.InputTime, a.RefCode
        	FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b
            	 ON a.UserLogin = b.UserLogin AND b.RecordType IN(1,2) AND b.RecordStatus IN (0,1,2)";
	}
		$result = $li->returnArray($sql,7);
}else{
	$result = $error_entries;
}

$display = "<tr class=\"tabletop\">
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_Field_Username</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_UserClassName</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_UserClassNumber</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_UserLogin</td>
				<td class=\"tabletoplink\" width=\"10%\">$i_Payment_Field_CreditAmount</td>
				<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Field_TransactionTime</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_Field_RefCode</td>
			</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list($name,$class,$classnum,$login,$amount,$tran_time,$refcode) = $result[$i];
     $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"tabletext\">$name</td><td class=\"tabletext\">$class</td><td class=\"tabletext\">$classnum</td><td class=\"tabletext\">$login</td>";
     $display .= "<td class=\"tabletext\">$".number_format($amount, 1)."</td><td class=\"tabletext\">$tran_time</td><td class=\"tabletext\">$refcode&nbsp;</td>";
     $display .= "</tr>\n";
}

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_CashDeposit, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="get" action="cash_log_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletextremark">
						<?=($error?($format==1?$i_Payment_Import_NoMatch_Entry:$i_Payment_Import_NoMatch_Entry2):$i_Payment_Import_Confirm)?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<?=$display?>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
	    <?php if(!$error){?>
			<?= $linterface->GET_ACTION_BTN($button_confirm, "submit", "") ?>
		<?php } ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='cash_log.php?clear=1'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="confirm" value="1">
<input type="hidden" name="format" value="<?=$format?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
