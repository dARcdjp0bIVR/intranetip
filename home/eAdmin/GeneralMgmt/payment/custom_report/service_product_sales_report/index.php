<?php
# using:
#####################################
#	Date: 	2019-09-27 Ray
#			Created
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_custom_report']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "CustomReport_ServiceProductSales";

$linterface = new interface_html();


# date range
if($FromDate=="") {
	$FromDate = date('Y-m-d', getStartOfAcademicYear());
}
if($ToDate=="") {
	$ToDate = date('Y-m-d', getEndOfAcademicYear());
}


$format_array = array(array(0,"PDF"),array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);


$TAGS_OBJ[] = array($Lang['ePayment']['ServiceProductSales'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
	<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
	<script>
        function checkDate(obj){
            if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
            return true;
        }

        function checkForm(formObj){
            if(formObj==null)return false;
            fromV = formObj.FromDate;
            toV= formObj.ToDate;
            if(!checkDate(fromV)){
                return false;
            }
            else if(!checkDate(toV)) {
                return false;
            }
            if(formObj.FromDate.value > formObj.ToDate.value){
                alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
                return false;
            }
            return true;
        }

        function submitForm(obj){
            if(checkForm(obj)) {
                obj.submit();
            }
        }

        $.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
        $(document).ready(function(){
            $('input#FromDate').datepick({
                dateFormat: 'yy-mm-dd',
                dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                changeFirstDay: false,
                firstDay: 0
            });
            $('input#ToDate').datepick({
                dateFormat: 'yy-mm-dd',
                dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                changeFirstDay: false,
                firstDay: 0
            });
        });
	</script>
	<br />
	<form name="form1" action="report.php" method="get" target="_blank">
		<!-- date range -->
		<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">

			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_From ?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
					<input class="textboxnum" type="text" name="FromDate" id="FromDate" value="<?=$FromDate?>">
					<span class="tabletextremark">(yyyy-mm-dd)</span>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_To ?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
					<input class="textboxnum" type="text" name="ToDate" id="ToDate" value="<?=$ToDate?>">
					<span class="tabletextremark">(yyyy-mm-dd)</span>
				</td>
			</tr>

			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_Format ?></td>
				<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$select_format?></td>
			</tr>


			<tr>
				<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td colspan="2" class="tabletext" align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","submit2") ?>
				</td>
			</tr>
		</table>
	</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>