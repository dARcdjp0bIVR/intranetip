<?php
ini_set('memory_limit','128M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_custom_report']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$intranet_session_language = 'b5';

$linterface = new interface_html();
$lpayment = new libpayment();
$POS = new libpos();

require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$margin= 10; // mm
$margin_top = $margin;
$margin_bottom = $margin;
$margin_left = $margin;
$margin_right = $margin;
$margin_top = 45;



$ttmss_payment_receipt_data = $sys_custom['ttmss_payment_receipt_data'][$sys_custom['ttmss_payment_receipt_code']];
//$school_name = GET_SCHOOL_NAME();
$school_name = $ttmss_payment_receipt_data['school_name'];


$record_data = array();


$amountConds = " IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
$voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";

$conds = "";
if ($FromDate != "" && $ToDate != "") {
	$conds .= " AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59'";
}

$sql  = "SELECT
					pt.LogID 
				from 
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
						$conds
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN 
					INTRANET_USER as u
					on potl.StudentID = u.UserID or potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID or potl2.StudentID = au.UserID 
				WHERE
					$amountConds
                    $voidConds
				Group By 
					pt.LogID
					";

$logid_array = $POS->returnVector($sql);

$total_amount = 0;
$temp_result = array();
$temp_record_data = array();
foreach($logid_array as $log_id) {
	$namefield = getNameFieldWithClassNumberByLang("u.");
	$archive_namefield = "au.ChineseName";
	$amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
	$voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";
	$sql = "SELECT
					ppdr.InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					pi.ItemName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $POS->DecimalPlace . ") as ItemSubTotal,
					ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $POS->DecimalPlace . ") as GrandTotal,
					IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as RefCode,
					ppdr.DateInput,
					c.CategoryName,
					ppdr.ReceiveAmount as ReceiveAmount,
					pt.LogID as LogID,
					pi.Barcode,
					c.CategoryCode
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber  
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					pt.LogID='$log_id' 
                    $amountConds
                    $voidConds ";


	$result = $POS->returnArray($sql);
	
	foreach($result as $data) {
		//group with same item & unit price
		$name_with_item_sub_total = $data['ItemName'].'_'.$data['ItemSubTotal'];
		$temp = array();
		$temp['ItemCode'] = $data['Barcode'];
		$temp['Name'] = $data['ItemName'];
		$temp['Qty'] = $data['ItemQty'];
		$temp['SubTotal'] = $data['GrandTotal'];
		$temp['CatCode'] = $data['CategoryCode'];
		$temp['CatName'] = $data['CategoryName'];
		$type = $data['CategoryCode'].' - '.$data['CategoryName'];
		$temp_record_data[$type][$name_with_item_sub_total][] = $temp;
	}
}


foreach($temp_record_data as $type=>$items) {
	foreach ($items as $name_with_item_sub_total=> $data) {
		$temp = array();
		$temp = $data[0];
		$item_qty = 0;
		$item_subtotal = 0;
		foreach ($data as $item) {
			$item_qty += $item['Qty'];
			$item_subtotal += $item['SubTotal'];
		}
		$temp['Qty'] = $item_qty;
		$temp['SubTotal'] = $item_subtotal;
		$record_data[$type][] = $temp;
	}
}


$date_field = "a.PaidTime";
$conds = "";
if ($FromDate != "")
{
	$conds .= " AND $date_field >= '$FromDate'";
}
if ($ToDate != "")
{
	$conds .= " AND $date_field <= '$ToDate 23:59:59'";
}

$sql = "SELECT 
						b.ItemCode,
						c.Name as cName, 
						c.Description as cDescription,
						b.Name,
						b.Description, 
						a.PaidTime,
						a.Amount, 
						d.RefCode,
						d.RelatedTransactionID, 
						c.Description,
						count(*) as qty,
						SUM(a.Amount) as sub_total,
						c.CatCode
			FROM 
				PAYMENT_PAYMENT_ITEMSTUDENT as a
			  	inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID)
	         	LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          	LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        WHERE 
	        	 a.RecordStatus = 1 AND d.TransactionType=2 
	        	AND	a.PaymentID Not IN (
					SELECT 
							a.PaymentID
					FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG d
					INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON (d.TransactionType=6 and d.RelatedTransactionID=a.PaymentID)
					WHERE 
						d.TransactionType=6 
				)
				$conds
	    		GROUP By a.ItemID";

	$payment_itemlist = $lpayment->returnArray($sql);

	foreach($payment_itemlist as $data) {
		$temp = array();
		$temp['ItemCode'] = $data['ItemCode'];
		$temp['Name'] = $data['Name'];
		$temp['Qty'] = $data['qty'];
		$temp['SubTotal'] = $data['sub_total'];
		$temp['CatCode'] = $data['CatCode'];
		$temp['CatName'] = $data['cDescription'];
		$type = $data['CatCode'].' - '.$data['cName'];
		$record_data[$type][] = $temp;
	}

ksort($record_data);

if($format == 0) { //pdf
	$pdf = new mPDF('', 'A4', 0, '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);
	$pdf->shrink_tables_to_fit = 1;

	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;

	$pdf->DefHTMLHeaderByName(
		'header',
		'<div style="margin-top: 10;">
		<div style="text-align: center; font-size: 18;">' . $school_name . '</div>
		<div style="text-align: center; font-size: 18;">服務/產品銷售排行表</div>
		<table width="100%" style="font-weight: bold; font-size: 12; margin-bottom: 10;">
			<tr>
				<td style="text-align: right;" width="80">從收據日期:</td>
				<td style="text-align: left;">'.$FromDate.'</td>
				<td style="text-align: right;" width="150">日期:</td>
				<td style="text-align: left;" width="100">'.date("Y-m-d").'</td>
			</tr>
			<tr>
				<td style="text-align: right;" width="80">至收據日期:</td>
				<td style="text-align: left;">'.$ToDate.'</td>
				<td style="text-align: right;" width="150">時間:</td>
				<td style="text-align: left;" width="100">'.date("H:i:s").'</td>
			</tr>
			<tr>
				<td style="text-align: right;" width="80"></td>
				<td style="text-align: left;"></td>
				<td style="text-align: right;" width="150">頁次:</td>
				<td style="text-align: left;" width="100">{PAGENO}</td>
			</tr>
		</table>
		
		<table style="width: 100%; font-weight: bold; font-size: 12;">
			<tr>
					<td width="120" style="text-align: left;">編號</td>
					<td style="text-align: left;">服務/產品名稱</td>
					<td width="200" style="text-align: left;">類別</td>					
					<td width="70" style="text-align: right;">數量</td>
					<td width="100" style="text-align: right;">金額</td>
			</tr>
		</table>
		<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>
	</div>
');

	$pdf->SetHTMLHeaderByName('header');


	$all_sub_total = 0;
	$all_qty_total = 0;
	foreach ($record_data as $type => $items) {
		$x = '<div style="font-weight: bold; font-size: 18;">類別: ' . $type . '</div>';
		$pdf->WriteHTML($x);

		$type_sub_total = 0;
		$type_qty_total = 0;
		uasort($items, 'sort_by_item_code');
		foreach ($items as $item) {
			$type_sub_total += $item['SubTotal'];
			$type_qty_total += $item['Qty'];
			$x = '<table width="100%" style="font-size: 12;">
				<tr>
					<td width="120" style="text-align: left;" valign="top">' . $item['ItemCode'] . '</td>
					<td style="text-align: left;" valign="top">' . $item['Name'] . '</td>
					<td width="200" style="text-align: left;" valign="top">' . $type . '</td>					
					<td width="70" style="text-align: right;" valign="top">' . $item['Qty'] . '</td>
					<td width="100" style="text-align: right;" valign="top">' . number_format($item['SubTotal'], 2) . '</td>
				</tr>
			</table>';
			$pdf->WriteHTML($x);
		}
		$all_sub_total += $type_sub_total;
		$all_qty_total += $type_qty_total;

		$x = '<table width="100%" style="font-size: 12; font-weight: bold; page-break-inside: avoid;">
				<tr>
					<td> </td>
					<td colspan="2" width="200" style="border-bottom:2px solid black;"> </td>
				</tr>
				<tr>
					<td style="text-align: right;">類別總計:</td>
					<td width="100" style="text-align: right;">' . $type_qty_total . '</td>
					<td width="100" style="text-align: right;">' . number_format($type_sub_total, 2) . '</td>
				</tr>
				<tr>
					<td> </td>
					<td colspan="2" width="200" style="border-bottom:2px solid black; border-top:2px solid black;"> </td>
				</tr>
			</table>';

		$pdf->WriteHTML($x);
	}

	$x = '<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>';
	$pdf->WriteHTML($x);

	$x = '<table width="100%" style="font-weight: bold; font-size: 12;">
				<tr>
					<td style="text-align: right;">總計:</td>
					<td width="100" style="text-align: right;">' . $all_qty_total . '</td>
					<td width="100" style="text-align: right;">' . number_format($all_sub_total, 2) . '</td>
				</tr>
			</table>
			<table width="100%" style="font-size: 12;">
				<tr>
					<td> </td>
					<td width="200" style="border-bottom:2px solid black; border-top:2px solid black;"> </td>
				</tr>
			</table>';
	$pdf->WriteHTML($x);

	$x = '<table width="100%" style="font-size: 12; font-weight: bold; page-break-inside: avoid;">
				<tr>
					<td style="text-align: left;" valign="bottom" height="150">負責人簽署/日期: _______________________________________</td>
					<td style="text-align: right;" valign="bottom">校長簽署/日期: _______________________________________</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<table width="100%" style="font-size: 12;">
							<tr>
								<td width="300">
									<table width="100%" style="font-size: 12;">
										<tr>
											<td width="100%" style="border-bottom:2px solid black;"> </td>
										</tr>
									</table>																
								</td>
								<td style="font-size: 18; font-weight: bold;">報表完</td>
								<td width="300">
									<table width="100%" style="font-size: 12;">
										<tr>
											<td width="100%" style="border-bottom:2px solid black;"> </td>
										</tr>
									</table>																
								</td>
							</tr>
						</table>
					</td>					
				</tr>
			</table>';

	$pdf->WriteHTML($x);

	$pdf->Output('service_product_sales_report_' . $FromDate . '_' . $ToDate . '.pdf', 'D');

} else { //csv
	$csv_data = array();
	$csv_data[] = array($school_name);
	$csv_data[] = array('服務/產品銷售排行表');
	$csv_data[] = array();
	$csv_data[] = array('從收據日期:', $FromDate, '日期:', date("Y-m-d"));
	$csv_data[] = array('至收據日期:', $ToDate, '時間:', date("H:i:s"));
	$csv_data[] = array();
	$csv_data[] = array('編號','服務/產品名稱','數量','金額');

	$all_sub_total = 0;
	$all_qty_total = 0;
	foreach ($record_data as $type => $items) {
		$csv_data[] = array("類別: ".$type);

		$type_sub_total = 0;
		$type_qty_total = 0;
		uasort($items, 'sort_by_item_code');
		foreach ($items as $item) {
			$type_sub_total += $item['SubTotal'];
			$type_qty_total += $item['Qty'];

			$temp = array();
			$temp[] = $item['ItemCode'];
			$temp[] = $item['Name'];
			$temp[] = $item['Qty'];
			$temp[] = number_format($item['SubTotal'], 2);
			$csv_data[] = $temp;

		}
		$all_sub_total += $type_sub_total;
		$all_qty_total += $type_qty_total;

		$temp = array();
		$temp[] = '';
		$temp[] = '類別總計:';
		$temp[] = $type_qty_total;
		$temp[] = number_format($type_sub_total, 2);
		$csv_data[] = $temp;
	}

	$temp = array();
	$temp[] = '';
	$temp[] = '總計:';
	$temp[] = $all_qty_total;
	$temp[] = number_format($all_sub_total, 2);
	$csv_data[] = $temp;

	$csv_data[] = array();
	$csv_data[] = array();

	$csv_data[] = array('負責人簽署/日期:','','校長簽署/日期:');
	$csv_data[] = array('---------------------- 報表完 ----------------------');

	$lexport = new libexporttext();
	$filename = 'service_product_sales_report_' . $FromDate . '_' . $ToDate . '.csv';
	$export_content = $lexport->GET_EXPORT_TXT($csv_data, array());
	$lexport->EXPORT_FILE($filename, $export_content);


}

intranet_closedb();

function sort_by_item_code($a, $b) {
	if ($a['ItemCode'] == $b['ItemCode']) {
		return 0;
	}
	return ($a['ItemCode'] < $b['ItemCode']) ? -1 : 1;
}
