<?php
# using: 

#################################
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	$linterface = new interface_html("popup.html");
	
	
	# remove files if given in POST
	if (isset($RemoveFiles) && is_array($RemoveFiles))
	{
		$limc->removeEmailAttachment($MassMailingID, $RemoveFiles);
		$MsgResult = $Lang['General']['ReturnMessage']['DeleteSuccess'];
	}
	
	
	$attachment_list = $limc->ListAttachmentEdit($MassMailingID);
	$AttachmentTotal = $limc->AttachmentTotal;
	
	if ($attachment_list!="")
	{
		$button_remove_html = "<tr><td colspan='4' align='right'>".$linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], "button", "removeSelectedAttachment()") ."</td></tr>";
	}
	
	# Start layout
	$linterface->LAYOUT_START($MsgResult);
?>

<script language="javascript">
function removeSelectedAttachment()
{
	var obj = document.formAJ;
	element = "RemoveFiles[]"
	if(countChecked(obj,element)==0)
	{
		alert(globalAlertMsg2);
	} else
	{
		if(confirm("<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>"))
		{
			obj.submit();
		}
	}
}


parent.jsUpdateAttachmentTotal("<?=$AttachmentTotal?>");
	
	
</script>

<form name="formAJ" method="post" action="ajax_list_attachment.php" >

<table width="100%" border="0" align="center">
<tr>
	<td>
		<table align="center" class="common_table_list">
		<!--
		<thead>
			<th nowrap='nowrap'><?=$Lang['Btn']['View']?></th>
			<th width='9%'><?=$Lang['SysMgr']['RoleManagement']['Class']?></th>
			<th width='5%'><?=$Lang['SysMgr']['RoleManagement']['ClassNumber']?></th>
			<th width='35%'><?=$Lang['MassMailing']['RecipientEmail']?></th>
			<th width='30%'><?=$Lang['Gamma']['SendersMailAddress']?></th>
			<th width='20%'><?=$Lang['MassMailing']['SendTime']?></th>
		</thead>
		-->
		<tbody>
			<?=$attachment_list .  $button_remove_html ?>
		</tbody>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="MassMailingID" value="<?=$MassMailingID?>" />
</form>
<?
	
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>