<?php
# using: 

#################################
#	Date:	2014-04-10	Yuen
#			handle EJ and IP encoding issue!!

#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	intranet_auth();
	intranet_opendb();
	
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	
	$MassMailingID = (isset($MassMailingID_e) && $MassMailingID_e!="") ? getDecryptedText($MassMailingID_e): $MassMailingID;
	
	$sql = "SELECT * FROM INTRANET_MASS_MAILING WHERE MassMailingID='$MassMailingID'";
	$rows = $limc->returnArray($sql);
	
	
	$MassMailingObj = $rows[0];
	
	$EmailTitle = $MassMailingObj["EmailTitle"];
	
	/*
	$RowHeader = $csv_data[0];
	for ($i=0; $i<sizeof($RowHeader); $i++)
	{
		$HeaderArr[] = strtoupper(trim($RowHeader[$i]));
	}
	
	for ($i=1; $i<sizeof($csv_data); $i++)
	{
		$RowArr = $csv_data[$i];
		for ($j=0; $j<sizeof($RowArr); $j++)
		{
			$CSVDataArr[$i-1][$HeaderArr[$j]] = $RowArr[$j];
		}
	}
	*/
	
	
	if ($file=="CSV")
	{
		$CSVFileObj = $limc->GetCSVFile($MassMailingID, $MassMailingObj["CSVFile"]);
		$csv_data = $CSVFileObj["data"];
		for ($i=0; $i<sizeof($csv_data); $i++)
		{
			$RowArr = $csv_data[$i];
			if (sizeof($RowArr)>0)
			{
				
				if (isset($junior_mck))
				{
					# EJ uses CSV
					$file_content .= "\"".implode("\",\"", $RowArr) . "\"" .  "\r\n";
				} else
				{
					# IP uses unicode textfile
					$file_content .= implode("\t", $RowArr) .  "\r\n";
				}
			}
		}
		//ord($charset[1]) == 255 && ord($charset[2]) == 254)
		if (isset($junior_mck))
		{
			# EJ uses big5
			//$file_content = "\xFF\xFE".iconv("BIG5", "UTF-16LE//IGNORE", $file_content);	
		} else
		{
			$file_content = "\xFF\xFE".iconv("UTF-8", "UTF-16LE//IGNORE", $file_content);
		}
		
		$file_name = "mail_recipients.csv";
	}
	else if ($file=="HTML")
	{
		$file_content = $MassMailingObj["HTMLFile"];
		$file_name = "mail_html_template.htm";
	}
	
	
	# header to output
	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: text/plain");
	header("Content-Length: ".strlen($file_content));
	header("Content-Disposition: attachment; filename=\"".$file_name."\"");
	
	echo $file_content;
			
	
?>