<?php
# using: 

#################################
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	$lfs = new libfilesystem();
	
	
	# remove CSV file if exists
	$CSVFileObj = $limc->GetCSVFile($MassMailingID);
	$csv_file_path = $CSVFileObj["filepath"];
	if ($csv_file_path!="")
		$lfs->file_remove($intranet_root.$csv_file_path);
	
	
	$result = $limc->deleteRecord($MassMailingID);
	
	
	intranet_closedb();
	
	
	if ($result)
	{
		header("location: index.php?msg=DeleteSuccess");
	} else
	{
		header("location: index.php?msg=DeleteUnsuccess");
	}
	
?>