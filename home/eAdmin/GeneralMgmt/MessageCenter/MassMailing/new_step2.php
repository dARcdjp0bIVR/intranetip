<?php
# using: 

#################################
#
#	TBD: check access right
#
#	Date:	2016-02-03
#			added Resend to Unsent Email and Send Single Email Buttons and functions
#
#	Date:	2011-06-23	Yuen
#			first version
#################################



	# for testing
	//$SetAsCampusMail = true;


	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	
	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	$liwm = new libwebmail();
	$libimport = new libimporttext();
	
	
	# Create a new interface instance
	$linterface = new interface_html();
	
	$HTML_template = "";
	if ($MassMailingID>0)
	{		
		# load from database
		$sql = "SELECT * FROM INTRANET_MASS_MAILING WHERE MassMailingID='$MassMailingID'";
		$rows = $limc->returnArray($sql,null,1);
		
		if (sizeof($rows)==0)
		{
			$MassMailingID = "";
		}
		
		$MassMailingObj = $rows[0];
		
		$ShowContent = $MassMailingObj["ShowContentIn"];
		$EmailBody = $MassMailingObj["EmailBody"];
		$RecipientStudent = $MassMailingObj["RecipientStudent"];
		$RecipientParent = $MassMailingObj["RecipientParent"];
		$RecipientAddress = $MassMailingObj["RecipientAddress"];
		$RecipientLoginID = $MassMailingObj["RecipientLoginID"];
		$SendAsAdmin = $MassMailingObj["SendAsAdmin"];
		$EmailTitle = $MassMailingObj["EmailTitle"];
		$LastModified = $MassMailingObj["ModifyDate"];
		$BodyFrom = ($MassMailingObj["BodyFrom"]=="") ? 2 : $MassMailingObj["BodyFrom"];
		
		$CSVFormat = $MassMailingObj["CSVFormat"];
		
		$CSVFileObj = $limc->GetCSVFile($MassMailingID, $MassMailingObj["CSVFile"]);
		$csv_data = $CSVFileObj["data"];
		
		$CSV_WARNING = $libimport->DISPLAY_FORMAT_WARNING($csv_data);

		$RowHeader = $csv_data[0];
		for ($i=0; $i<sizeof($RowHeader); $i++)
		{
			// ClassNo = ClassNumber
			if (strtoupper(trim($RowHeader[$i]))=="CLASSNO")
			{
				$HeaderArr[] = "CLASSNUMBER";
			} else
			{
				$HeaderArr[] = strtoupper(trim($RowHeader[$i]));
			}
		}
		
		for ($i=1; $i<sizeof($csv_data); $i++)
		{
			$RowArr = $csv_data[$i];
			for ($j=0; $j<sizeof($RowArr); $j++)
			{
				$CSVDataArr[$i-1][$HeaderArr[$j]] = $RowArr[$j];
			}
		}
		
		# Format 2
		if ($CSVFormat==2)
		{
			//consolidate data from multiple rows
			// TYPE, Award_Event or CONTENT
			
			$row_index = 0;
			for ($i=0; $i<sizeof($CSVDataArr); $i++)
			{	
				if ($RecipientAddress)
				{
					$iClass = $CSVDataArr[$i]['EMAIL'];
					$iClassNo = "-";
				} elseif ($RecipientLoginID)
				{
					$iClass = $CSVDataArr[$i]['USERLOGIN'];
					$iClassNo = "-";
				} else
				{
					$iClass = $CSVDataArr[$i]['CLASS'];
					$iClassNo = $CSVDataArr[$i]['CLASSNUMBER'];					
				}
				//debug($iClass, $iClassNo);
				$iType = $CSVDataArr[$i]['TYPE'];
				$iContent = $CSVDataArr[$i]['CONTENT'];
				if ($sys_custom["ttca_mailmerge"])
				{
					if ($CSVDataArr[$i]['AWARD_EVENT']!="" && $CSVDataArr[$i]['AWARD_EVENT']!="-" && $CSVDataArr[$i]['AWARD_EVENT']!="--" && $CSVDataArr[$i]['AWARD_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['AWARD_EVENT']; 
					} elseif ($CSVDataArr[$i]['PUNISHMENT_EVENT']!="" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="-" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="--" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['PUNISHMENT_EVENT'];
					}
				} 
				//$iContent = ($CSVDataArr[$i]['AWARD_EVENT']!="") ? $CSVDataArr[$i]['AWARD_EVENT'] : $CSVDataArr[$i]['CONTENT'];
				$DataAsso[$iClass][$iClassNo][$iType] .= ($DataAsso[$iClass][$iClassNo][$iType]!="") ? "<br />".$iContent : $iContent;
				if ($sys_custom["ttca_mailmerge"])
				{
					$DataAsso[$iClass][$iClassNo]["NAME"] = "({$iClass}".str_pad($iClassNo, 2, "0", STR_PAD_LEFT).") ".$CSVDataArr[$i]['CHNAME'];
				} else
				{
					if ($CSVDataArr[$i]['NAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["NAME"] = $CSVDataArr[$i]['NAME'];
					}
					if ($CSVDataArr[$i]['STUDENTNAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["STUDENTNAME"] = $CSVDataArr[$i]['STUDENTNAME'];
					}					
				}
			}
			
			if (is_array($DataAsso) && sizeof($DataAsso)>0)
			{
				unset($CSVDataArr);
				unset($HeaderArr);	
				$Arr_index = 0;
				foreach($DataAsso as $iClass=>$DataStudents)
				{
					if ($Arr_index==0)
					{
						if ($RecipientAddress)
						{
							$HeaderArr[] = "EMAIL";
						} elseif ($RecipientLoginID)
						{
							$HeaderArr[] = "USERLOGIN";
						} else
						{
							$HeaderArr[] = "CLASS";		
						}
						
					}
					if (is_array($DataStudents) && sizeof($DataStudents)>0)
					{
						foreach($DataStudents as $iClassNo=>$DataOneStudent)
						{
							if ($Arr_index==0 && !$RecipientAddress && !$RecipientLoginID)
							{
								$HeaderArr[] = "CLASSNUMBER";
								$HeaderArr[] = "CLASSNO";
							}
							
							if ($RecipientAddress)
							{
								$CSVDataArr[$Arr_index]["EMAIL"] = $iClass;
							} elseif ($RecipientLoginID)
							{
								$CSVDataArr[$Arr_index]["USERLOGIN"] = $iClass;
							} else
							{
								$CSVDataArr[$Arr_index]["CLASS"] = $iClass;
								$CSVDataArr[$Arr_index]["CLASSNUMBER"] = $iClassNo;
							}
							if (is_array($DataOneStudent) && sizeof($DataOneStudent)>0)
							{
								foreach($DataOneStudent as $iType=>$iContent)
								{
									$CSVDataArr[$Arr_index][$iType] = $iContent;
									if (!in_array($iType, $HeaderArr))
									{
										$HeaderArr[] = $iType;
									}
								}
							}
							$Arr_index++;
						}
					}
				}
				if ($sys_custom["ttca_mailmerge"])
				{
					if (!in_array("NAME", $HeaderArr))
						$HeaderArr[] = "NAME";
					if (!in_array("D_ASS", $HeaderArr))
						$HeaderArr[] = "D_ASS";
					if (!in_array("PLA", $HeaderArr))
						$HeaderArr[] = "PLA";
					if (!in_array("WLA", $HeaderArr))
						$HeaderArr[] = "WLA";
					if (!in_array("MLA", $HeaderArr))
						$HeaderArr[] = "MLA";
					if (!in_array("DLA", $HeaderArr))
						$HeaderArr[] = "DLA";
					if (!in_array("P", $HeaderArr))
						$HeaderArr[] = "P";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "W";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "M";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "D";
				}
			}
		}
		
		//debug_r($HeaderArr);
		//debug_r($CSVDataArr);
		
		/*
		if ($CSVFormat==2)
		{
			//consolidate data from multiple rows
			// TYPE, Award_Event or CONTENT
			
			$row_index = 0;
			for ($i=0; $i<sizeof($CSVDataArr); $i++)
			{
				$iClass = $CSVDataArr[$i]['CLASS'];
				$iClassNo = $CSVDataArr[$i]['CLASSNUMBER'];
				$iType = $CSVDataArr[$i]['TYPE'];
				$iContent = $CSVDataArr[$i]['CONTENT'];
				if ($sys_custom["ttca_mailmerge"])
				{
					if ($CSVDataArr[$i]['AWARD_EVENT']!="" && $CSVDataArr[$i]['AWARD_EVENT']!="-" && $CSVDataArr[$i]['AWARD_EVENT']!="--" && $CSVDataArr[$i]['AWARD_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['AWARD_EVENT']; 
					} elseif ($CSVDataArr[$i]['PUNISHMENT_EVENT']!="" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="-" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="--" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['PUNISHMENT_EVENT'];
					}
				} 
				//$iContent = ($CSVDataArr[$i]['AWARD_EVENT']!="") ? $CSVDataArr[$i]['AWARD_EVENT'] : $CSVDataArr[$i]['CONTENT'];
				$DataAsso[$iClass][$iClassNo][$iType] .= ($DataAsso[$iClass][$iClassNo][$iType]!="") ? "<br />".$iContent : $iContent;
				if ($sys_custom["ttca_mailmerge"])
				{
					$DataAsso[$iClass][$iClassNo]["NAME"] = "({$iClass}".str_pad($iClassNo, 2, "0", STR_PAD_LEFT).") ".$CSVDataArr[$i]['CHNAME'];
				} else
				{
					if ($CSVDataArr[$i]['NAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["NAME"] = $CSVDataArr[$i]['NAME'];
					}
					if ($CSVDataArr[$i]['STUDENTNAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["STUDENTNAME"] = $CSVDataArr[$i]['STUDENTNAME'];
					}					
				}
			}
			
			if (is_array($DataAsso) && sizeof($DataAsso)>0)
			{
				unset($CSVDataArr);
				unset($HeaderArr);	
				$Arr_index = 0;
				foreach($DataAsso as $iClass=>$DataStudents)
				{
					if ($Arr_index==0)
						$HeaderArr[] = "CLASS";
					if (is_array($DataStudents) && sizeof($DataStudents)>0)
					{
						foreach($DataStudents as $iClassNo=>$DataOneStudent)
						{
							if ($Arr_index==0)
							{
								$HeaderArr[] = "CLASSNUMBER";
								$HeaderArr[] = "CLASSNO";
							}
							$CSVDataArr[$Arr_index]["CLASS"] = $iClass;
							$CSVDataArr[$Arr_index]["CLASSNUMBER"] = $iClassNo;
							if (is_array($DataOneStudent) && sizeof($DataOneStudent)>0)
							{
								foreach($DataOneStudent as $iType=>$iContent)
								{
									$CSVDataArr[$Arr_index][$iType] = $iContent;
									if (!in_array($iType, $HeaderArr))
									{
										$HeaderArr[] = $iType;
									}
								}
							}
							$Arr_index++;
						}
					}
				}
				if ($sys_custom["ttca_mailmerge"])
				{
					if (!in_array("NAME", $HeaderArr))
						$HeaderArr[] = "NAME";
					if (!in_array("D_ASS", $HeaderArr))
						$HeaderArr[] = "D_ASS";
					if (!in_array("PLA", $HeaderArr))
						$HeaderArr[] = "PLA";
					if (!in_array("WLA", $HeaderArr))
						$HeaderArr[] = "WLA";
					if (!in_array("MLA", $HeaderArr))
						$HeaderArr[] = "MLA";
					if (!in_array("DLA", $HeaderArr))
						$HeaderArr[] = "DLA";
					if (!in_array("P", $HeaderArr))
						$HeaderArr[] = "P";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "W";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "M";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "D";
				}
			}
		}
		*/
		
		$HTML_template = $MassMailingObj["HTMLFile"];
		
		//debug($htmlsize_org, strlen($HTML_template));
	}
	
	# parse the tags using regular expression to remove unnecessary tag inside [=..=]
	$HTML_template = $limc->ParseTemplateTags($HTML_template,$HeaderArr);

	$EmailSender = $limc->getSenderEmail($SendAsAdmin, $UserID);
	

	$ForPreview = true;
	$MergeHTML = $limc->MergeContent($HTML_template, $HeaderArr, $CSVDataArr[0], $ForPreview);
	$MergeHTML .= $limc->CheckEmailAttachment($MassMailingID, $CSVDataArr[0]["EMAILATTACHMENT"]);
	$ToEmail = $CSVDataArr[0]["EMAIL"];

	if (sizeof($CSVDataArr)>1)
	{
		if ($IsPreview && $PreviewIndex>0)
		{
			$MergeHTML = $limc->MergeContent($HTML_template, $HeaderArr, $CSVDataArr[$PreviewIndex-1], $ForPreview);
			echo $MergeHTML . $limc->CheckEmailAttachment($MassMailingID, $CSVDataArr[$PreviewIndex-1]["EMAILATTACHMENT"]);
			die();
		}
		for ($k=0; $k<sizeof($CSVDataArr); $k++)
		{
			$ChoiceTitle = "".($k+1).". ";
			if (!$RecipientLoginID && !$RecipientAddress)
			{
				if ($CSVDataArr[$k]['CLASSNAME']!="" && $CSVDataArr[$k]['CLASSNUMBER']!="")
				{
					$ChoiceTitle .= " ". $CSVDataArr[$k]['CLASSNAME'] ."-".$CSVDataArr[$k]['CLASSNUMBER'];
				} elseif ($CSVDataArr[$k]['CLASS']!="" && $CSVDataArr[$k]['CLASSNUMBER']!="")
				{
					$ChoiceTitle .= " ". $CSVDataArr[$k]['CLASS'] ."-".$CSVDataArr[$k]['CLASSNUMBER'];
				}
			}
			if ($CSVDataArr[$k]['STUDENTNAME']!="")
			{
				$ChoiceTitle .= " ". $CSVDataArr[$k]['STUDENTNAME'] ;
			} elseif ($CSVDataArr[$k]['USERNAME']!="")
			{
				$ChoiceTitle .= " ". $CSVDataArr[$k]['USERNAME'] ;
			} elseif ($CSVDataArr[$k]['NAME']!="")
			{
				$ChoiceTitle .= " ". $CSVDataArr[$k]['NAME'] ;
			} elseif ($CSVDataArr[$k]['EMAIL']!="")
			{
				$ChoiceTitle .= " (". $CSVDataArr[$k]['EMAIL'].")" ;
			}
			if ($CSVDataArr[$k]['USERLOGIN']!="")
			{
				$ChoiceTitle .= " [UserLogin: ". $CSVDataArr[$k]['USERLOGIN']."]" ;
			} 
			$PreviewChoice[] = array($k+1, $ChoiceTitle);
		}
		if (sizeof($PreviewChoice)>0)
		{
			$button_previous_shown = $linterface->GET_BTN(" &lt;&lt;", "button", "changePreivewTo(-1)", "ViewPrevious", "disabled='disabled' title='".$Lang['MassMailing']['PreviousRecord']."'");

			if (sizeof($PreviewChoice)>1)
			{
				$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "changePreivewTo(1)", "ViewNext", "title='".$Lang['MassMailing']['NextRecord']."'");
			} else
			{
				$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "", "ViewNext", "disabled='disabled' title='".$Lang['MassMailing']['NextRecord']."'");
			}
			$PreviewSelections = $button_previous_shown ." " .$linterface->GET_SELECTION_BOX($PreviewChoice, " onChange='changePreivew(this.value)' name='PreviewOptions' id='PreviewOptions' ", "") . " " . $button_next_shown;
			$PreviewSelections .= " <span id='PreviewStatus'></span>";
		}
	}
	
	
	# get emails from iMail plus, iMail 1.2, alternative emails
	if (!$SetAsCampusMail && $plugin['imail_gamma'])
	{
		$sql = "SELECT UserID, ImapUserEmail, ClassName, ClassNumber FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>''";
	} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
	{
		$sql = "SELECT a.UserID, a.UserLogin, a.ClassName, a.ClassNumber, b.ACL FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE  a.RecordType='2' AND RecordStatus='1' AND a.ClassName<>'' AND a.ClassNumber<>'' order by a.ClassName, a.ClassNumber";
	} else
	{
		# campusmail
		$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>''";
		$UseCampusMail = true;
	}
	$rows = $limc->returnArray($sql,null,1);
	
	
	if (!$RecipientAddress && $SendAsAdmin==0)
	{
		$limc->ChangeEmailMethod($UseCampusMail);
	}
	
	if (trim($EmailSender)=="" && !$limc->UseCampusMail)
	{
		$EmailSender = "<font color='red'>No email address found!</font>";
		$ErrorNoEmailSender = true;
	}
	
	for ($i=0; $i<sizeof($rows); $i++)
	{
		# user email for iMail 1.2
		if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
		{
			if ($rows[$i]["ACL"]==1 || $rows[$i]["ACL"]==3)
			{
				$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
			}
		}
		$CurUser = $rows[$i];
		if ($CurUser["ClassName"]!="" && $CurUser["ClassNumber"]!="")
		{
			$StudentsArr[$CurUser["ClassName"]][$CurUser["ClassNumber"]] = ($UseCampusMail) ? array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>"[CAMPUSMAIL]"): array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>$CurUser["ImapUserEmail"]);
			$StudentsArrByID[$CurUser["UserID"]] = array("ClassName"=>$CurUser["ClassName"], "ClassNumber"=>$CurUser["ClassNumber"]);
		}
	}

	# check emails for students
	$CountStudents = 0;
	$CountAddress = 0;
	for ($i=0; $i<sizeof($CSVDataArr); $i++)
	{
		$ToEmail = $CSVDataArr[$i]["EMAIL"];
		$StdClass = $CSVDataArr[$i]["CLASS"];
		if (trim($StdClass)=="")
		{
			$StdClass = $CSVDataArr[$i]["CLASSNAME"];
		}
		$StdClassNumber = $CSVDataArr[$i]["CLASSNUMBER"];
		if ($RecipientStudent || $RecipientAddress)
		{
			if (!isset($StudentsArr[$StdClass][(int)$StdClassNumber]) && $ToEmail=="")
			{
				$ErrorNoStudent[] =  (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$StdClass . " - " . $StdClassNumber;
			} elseif (trim($StudentsArr[$StdClass][(int)$StdClassNumber]["ImapUserEmail"])=="" && $ToEmail=="")
			{
				$ErrorNoEmailStudent[] = (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$StdClass . " - " . $StdClassNumber;
			} else
			{
				if ($RecipientAddress)
					$CountAddress ++;
				else
					$CountStudents ++;
			}
		}
		
		# for mapping to parents
		if ($StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"]!="" && $StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"]>0)
		{
			$StudentIDs[] = $StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"];	
		}
	}

	//debug("Email can be sent to {$CountStudents} students.");
	if ($CountStudents>0)
	{
		$RecipientSummary = $CountStudents.$Lang['MassMailing']['NoOfStudents'];
	}
	
	if ($CountAddress>0)
	{
		$RecipientSummary = $CountAddress.$Lang['MassMailing']['NoOfAddress'];
	}
	
	# display no email for below students
	//debug_r($ErrorNoEmailStudent);
	
	# check emails for parents
	if ($RecipientParent)
	{
		if (sizeof($StudentIDs)>0)
		{
			$StdIDsStr = implode(",", $StudentIDs);
			
			if (!$SetAsCampusMail && $plugin['imail_gamma'])
			{
				$sql = "SELECT ip.ParentID, ip.StudentID, iu.ImapUserEmail FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND iu.RecordStatus=1 ";
			} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
			{
				$sql = "SELECT DISTINCT ip.ParentID, ip.StudentID, iu.UserLogin FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu, INTRANET_SYSTEM_ACCESS AS b where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=b.UserID AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND b.ACL IN (1,3) AND iu.RecordStatus=1  ";
			} else
			{
				# campusmail
				$sql = "SELECT ip.ParentID, ip.StudentID FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND iu.RecordStatus=1 ";
				$UseCampusMail = true;
			}
			$rows = $limc->returnArray($sql,null,1);
	
			for ($i=0; $i<sizeof($rows); $i++)
			{
				# user email for iMail 1.2
				if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
				{
					$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
				$CurParent = $rows[$i];				
				$StdClass = $StudentsArrByID[$CurParent["StudentID"]]["ClassName"];
				$StdClassNumber = $StudentsArrByID[$CurParent["StudentID"]]["ClassNumber"];
				$ParentsArr[$StdClass][(int)$StdClassNumber] = ($UseCampusMail) ? array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>"[CAMPUSMAIL]"): array("UserID"=>$CurParent["ParentID"], "ImapUserEmail"=>$CurParent["ImapUserEmail"]);
			}
		} else
		{			
			$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['NoStudentImported'], array());
		}

		
		# check parent existence by students
		$CountParents = 0;
		for ($i=0; $i<sizeof($CSVDataArr); $i++)
		{
			$ToEmail = $CSVDataArr[$i]["EMAIL"];
			$StdClass = $CSVDataArr[$i]["CLASS"];
			if (trim($StdClass)=="")
			{
				$StdClass = $CSVDataArr[$i]["CLASSNAME"];
			}
			$StdClassNumber = $CSVDataArr[$i]["CLASSNUMBER"];
			if (trim($ParentsArr[$StdClass][(int)$StdClassNumber]["ImapUserEmail"])!="")
			{
				$CountParents ++;
			} elseif (is_array($ParentsArr[$StdClass][(int)$StdClassNumber]) && isset($ParentsArr[$StdClass][(int)$StdClassNumber]))
			{
				$ErrorNoEmailParent[] = (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$StdClass . " - " . $StdClassNumber;
			} elseif (isset($StudentsArr[$StdClass][(int)$StdClassNumber]))
			{
				$ErrorNoParent[] =  (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$StdClass . " - " . $StdClassNumber;
			} else
			{
				$ErrorNoStudent[] =  (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$StdClass . " - " . $StdClassNumber;
			}
		}
		//debug($CountParents." parents are okay:");
		//debug_r($ParentsArr);
	}
	# summary
	
	if ($CountParents>0)
	{
		$RecipientSummary .= (($RecipientSummary!="") ? $Lang['MassMailing']['And'] : ""). $CountParents.$Lang['MassMailing']['NoOfParents'];
	}
	
	$ShowContentWay = ($ShowContent==1) ? $Lang['MassMailing']['InMailBody'] : $Lang['MassMailing']['AsAttachment'];
	
	if ($RecipientLoginID)
	{
		# get emails from iMail plus, iMail 1.2, alternative emails
		if (!$SetAsCampusMail && $plugin['imail_gamma'])
		{
			$sql = "SELECT UserID, ImapUserEmail, UserLogin FROM INTRANET_USER where RecordStatus=1";
		} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
		{
			$sql = "SELECT a.UserID, a.UserLogin, b.ACL FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE RecordStatus='1' order by a.UserLogin";
		} else
		{
			$sql = "SELECT UserID, UserLogin FROM INTRANET_USER where RecordStatus=1";
		}
		$rows = $limc->returnArray($sql,null,1);
		
		# prepare array with emails
		for ($i=0; $i<sizeof($rows); $i++)
		{
			# user email for iMail 1.2
			if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
			{
				if ($rows[$i]["ACL"]==1 || $rows[$i]["ACL"]==3)
				{
					$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
			}
			$CurUser = $rows[$i];
			if ($CurUser["UserLogin"]!="")
			{
				$AllUsers[$CurUser["UserLogin"]] = ($UseCampusMail) ? array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>"[CAMPUSMAIL]"): array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>$CurUser["ImapUserEmail"]);
			}
		}
		
		# check and count valid email address according to CSV
		
		# check emails for students
		$CountAddress = 0;
		for ($i=0; $i<sizeof($CSVDataArr); $i++)
		{
			$UserLogin = $CSVDataArr[$i]["USERLOGIN"];
			if (trim($AllUsers[$UserLogin]["ImapUserEmail"])!="")
			{
				$CountAddress ++;
			} else
			{
				$ErrorNoUserLogin[] =  (($CSVFormat==2) ? "" :"[row ".($i+2)."] " ).$UserLogin;
			}
		}
		
		if ($CountAddress>0)
		{
			$RecipientSummary = $CountAddress.$Lang['MassMailing']['NoOfAddress'];
		}
	}


	if (sizeof($ErrorNoEmailParent)>0)
	{
		$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['ParentNoEmail'], $ErrorNoEmailParent);
	}
	if (sizeof($ErrorNoStudent)>0)
	{
		$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['NoUser'], $ErrorNoStudent);
	}
	if (sizeof($ErrorNoParent)>0)
	{
		$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['NoParentRelation'], $ErrorNoParent);
	}
	if (sizeof($ErrorNoUserLogin)>0)
	{
		$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['NoUserLogin'], $ErrorNoUserLogin);
	}
	
	if (sizeof($ErrorNoEmailStudent)>0)
	{
		$RecipientErrors .= $limc->ShowError($Lang['MassMailing']['StudentNoEmail'], $ErrorNoEmailStudent);
	}
	
	
	
	$EmailAttachment = $limc->GetAttachmentSummary($MassMailingID);
	
	
	# Page title
	$CurrentPageArr['eAdmineMassMailing'] = 1;
	$CurrentPage = "PageMassMailing";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['MassMailing']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['MassMailing'], "javascript:location='index.php';");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New']. " (".$Lang['MassMailing']['NewStep2'].")");
	
	# Start layout
	$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
	
	if ($err!="")
	{
		if (strstr($err, "NoCSV"))
		{
			$ErrorArr[] = $Lang['MassMailing']['ProblemNoCSV'];
		}
		if (strstr($err, "NoHTML"))
		{
			$ErrorArr[] = $Lang['MassMailing']['ProblemNoHTML'];
		}
		$ErrorMessage = "<li>".implode("<li>", $ErrorArr);
	}
	
	# get the number of send out before
	$sql = "SELECT COUNT(*) FROM INTRANET_MASS_MAILING_LOG WHERE MassMailingID='{$MassMailingID}'";
	$row = $limc->returnVector($sql);	
	$SendTotal = (int) $row[0];
	
	if ($SendTotal>0)
	{
		$sql = "SELECT InputDate FROM INTRANET_MASS_MAILING_LOG WHERE MassMailingID='{$MassMailingID}' order by UNIX_TIMESTAMP(InputDate) DESC LIMIT 1";
		$row = $limc->returnVector($sql);	
		$LastSendTime = $row[0];
	}
	
	if (trim($RecipientSummary)=="")
	{
		$RecipientSummary = "<font color='red'>".$Lang['MassMailing']['NoValidRecipient']."</font>";
	}
	if ($CSV_WARNING!="")
	{
		$RecipientSummary .= "<br /><font color='red'>".$CSV_WARNING."</font>";
	}
	
	if (!$ErrorNoEmailSender && ($CountStudents>0 || $CountParents>0 || $CountAddress>0))
	{
		$RowNumber = $limc->getLastMailRowNumber($MassMailingID);
		$totalNumberOfRows = count($CSVDataArr);
		if($totalNumberOfRows>$RowNumber&&!is_null($RowNumber)){
		$ButtonsAvaiable .=  ($SendTotal>0) ? $linterface->GET_ACTION_BTN($Lang['MessageCenter']['ResendUnsentEmail'][0].($RowNumber+1).$Lang['MessageCenter']['ResendUnsentEmail'][1], "button", "confrimToResend()")."&nbsp;":'';
		}
		$ButtonsAvaiable .= ($SendTotal>0) ? $linterface->GET_ACTION_BTN($Lang['MassMailing']['BtnReSend'], "button", "confirmToReSend()") : $linterface->GET_ACTION_BTN($button_send, "button", "confirmToSend()")."&nbsp;";
	}
	
	if ($SendTotal<1)
	{
		$ButtonsAvaiable .= $linterface->GET_ACTION_BTN($Lang['Button']['Edit'] , "button", "jsEdit(this.form)")."&nbsp;";
	}
	
	if ($MassMailingID>0 && $MassMailingID!="")
	{
		$ButtonsAvaiable .= $linterface->GET_ACTION_BTN($Lang['Button']['Delete'], "button", "jsDelete(this.form)")."&nbsp;";
	}
	
?>


<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script> 

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 


<script language="javascript">

function checkform2(obj)
{
	
        if(confirm("<?=$Lang['MassMailing']['ConfirmToSend']?>"))
        {
			return true;
		} else
		{
			return false;
		}
		
}

function jsEdit(obj)
{
	obj.action = "new.php";
	obj.submit();
}

function jsDelete(obj)
{
        if(confirm("<?=$Lang['MassMailing']['ConfirmToDelete']?>"))
        {
			obj.action = "delete.php";
			obj.submit();
		}
		
}

function confrimToResend(){
	if(confirm("<?=$Lang['MassMailing']['ConfirmToReSend']?>"))
    {
		$('#resend_by_ajax').click();
    }
}

function confirmToSend()
{
	
    <?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
	alert("Sorry, this action is not allowed in trial site!");
	return false;
	<?php } ?>
	
	if(confirm("<?=$Lang['MassMailing']['ConfirmToSend']?>"))
    {
		$('#send_by_ajax').click();
    }
    
    
}


function confirmToReSend()
{
	if(confirm("<?=$Lang['MassMailing']['ConfirmToReSend']?>"))
    {
		$('#send_by_ajax').click();
    }
}
function sendSingleEmail(){
	var n =$('#PreviewOptions').val();
	if(!n){
		n=1;
	}
	var href = "ajax_send.php?height=200&width=500&MassMailingID=<?=$MassMailingID?>&modal=true&sendSingleMail=true";
	$('#send_single_mail_by_ajax').attr("href",href+"&sendSingleRowNumber="+n);
	if(confirm("<?=$Lang['MassMailing']['ConfirmToReSend']?>"))
    {
		$('#send_single_mail_by_ajax').click();
    }	
}

function js_ShowProgress()
{
	tb_show("new","new.php","images/cart.gif");
	
}


function changePreivew(PreviewIndex, e)
{
	 updateNextPreviousButtons(PreviewIndex);
	 $('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax({
	    url: 'new_step2.php?MassMailingID=<?=$MassMailingID?>&IsPreview=1&PreviewIndex='+PreviewIndex,
	    error: function(xhr) {
			alert('request error');
	    },
	    success: function(response) {
	      	$('#PreviewMerged').html(response);
	      	 $('#PreviewStatus').html('');
	    }
	});
}




function changePreivewTo(IsNext)
{
	var OptionObj = document.form1.PreviewOptions;
	var OptionTotal = OptionObj.length;
	var OptionNow = parseInt(OptionObj.value);
	if (IsNext==1)
	{
		NextIndex = OptionNow + 1;
		if (OptionNow < OptionTotal)
		{
			changePreivew(NextIndex);
			$("#PreviewOptions option[value='"+OptionNow+"']").attr('selected', '');
			$("#PreviewOptions option[value='"+NextIndex+"']").attr('selected', 'selected');
		}
	} else if (IsNext==-1)
	{
		NextIndex = OptionNow - 1;
		if (OptionNow > 1)
		{
			changePreivew(NextIndex);
			$("#PreviewOptions option[value='"+OptionNow+"']").attr('selected', '');
			$("#PreviewOptions option[value='"+NextIndex+"']").attr('selected', 'selected');
		}		
	}
}


function updateNextPreviousButtons(PreviewIndex)
{
	var OptionObj = document.form1.PreviewOptions;
	var OptionTotal = OptionObj.length;
	var ViewNextObj = document.form1.ViewNext;
	var ViewPreviousObj = document.form1.ViewPrevious;
	
	if (PreviewIndex >= OptionTotal)
	{
		ViewNextObj.disabled = "disabled";
	} else
	{
		ViewNextObj.disabled = "";
	}
	
	if (1 >= PreviewIndex)
	{
		ViewPreviousObj.disabled = "disabled";
	} else
	{
		ViewPreviousObj.disabled = "";
	}
		
}
</script>


<form name="form1" action="new_step3.php" method="post" onSubmit="return checkform(this);">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
<?php
if ($err=="")
{
?>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
		   <td class="tabletext formfieldtitle" colspan="2">
		   
		    <fieldset class="instruction_box"><legend><?=$Lang['General']['Instruction']?></legend>
		    <a style="display:none" id="send_by_ajax" name="send_by_ajax" href="ajax_send.php?height=200&width=500&MassMailingID=<?=$MassMailingID?>&modal=true" class="thickbox">Thickbox</a>
			 <a style="display:none" id="resend_by_ajax" name="resend_by_ajax" href="ajax_send.php?height=200&width=500&MassMailingID=<?=$MassMailingID?>&modal=true&resendFromUnSendEmail=true&fromRow=<?=$RowNumber?>" class="thickbox">Thickbox</a>
			<a style="display:none" id="send_single_mail_by_ajax" name="send_single_mail_by_ajax" href="ajax_send.php?height=200&width=500&MassMailingID=<?=$MassMailingID?>&modal=true&sendSingleMail=true" class="thickbox">Thickbox</a>
			<?=$Lang['MassMailing']['Instruction']?>
			</fieldset>
		   
			</td>
		</tr>
		
		<!-- Title -->
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['Subject']?> </td>
			<td align="left" valign="top">
			  <?=stripslashes($EmailTitle)?>
			</td>
		</tr>
		
		
		<!-- show in  -->
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['ShowContent']?> </td>
			<td align="left" valign="top">
			  <?= $ShowContentWay ?>
			</td>
		</tr>
		
		<!-- attachments  -->
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['Attachments']?> </td>
			<td align="left" valign="top">
			  <?= $EmailAttachment ?>
			</td>
		</tr>
		
		<!-- Description -->
		<tr style="display:none" id="EmailBodyTr">
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['Message']?></td>
			<td align="left" valign="top"><?=$linterface->GET_TEXTAREA("EmailBody", $EmailBody, "86",10)?></td>
		</tr>		
		
		<tr>
			<td class="tabletext formfieldtitle" valign="top" nowrap='nowrap'><?=$Lang['Gamma']['Recipient']?> </td>
			<td align="left" valign="top">
			  <?= $RecipientSummary ?>
			  <?= $RecipientErrors ?>
			</td>
		</tr>
		

<?php
if ($EmailSender!="")
{
?>		
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['SendersMailAddress']?> </td>
			<td align="left" valign="top">
			  <?= $EmailSender ?>
			</td>
		</tr>

<?php
}


if ($LastModified!="")
{
?>
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['General']['LastModified'] ?> </td>
			<td align="left" valign="top">
			  <?= $LastModified ?>
			</td>
		</tr>
<?php
}
?>

<?php
if ($SendTotal>0)
{
?>
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['NoOfDone']?> </td>
			<td align="left" valign="top">
			   <a href="ajax_send_record.php?MassMailingID=<?=$MassMailingID?>&width=740" class="thickbox" title="<?= "[".$Lang['MassMailing']['SendRecord'] . "] " . str_replace('"', "&quot;", $MassMailingObj["EmailTitle"]);?>" ><?= $SendTotal ?></a>
			</td>
		</tr>
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['LastSendTime']?> </td>
			<td align="left" valign="top">
			  <?= $LastSendTime ?>
			</td>
		</tr>
<?php
}
?>
		
	</table>
		
		
		
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $ButtonsAvaiable ?><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "location='index.php'")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<br /><br />
	<!--- preview -->
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td colspan="3"><div class="table_board"><span class="sectiontitle_v30"><?=$Lang['Btn']['Preview'] ." ". $PreviewSelections ?></span></div></td></tr>
		<tr>
			<td nowrap="nowrap">&nbsp; &nbsp;</td>
			<td width="95%" valign="top" style="border:dotted #555555 1px;">
			  <div id="PreviewMerged"><?=$MergeHTML?></div>
			</td>
			<td nowrap="nowrap">&nbsp; &nbsp;</td>			
		</tr>
		<tr>
			<td colspan="3" align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['MessageCenter']['SendSingleEmail'], "button", "sendSingleEmail();")?>
			</td>
		</tr>
		<tr>
			<td colspan="3" >
			<p><?=$Lang['AccountMgmt']['Remarks']?> : <span style='background:#88FF88;'> &nbsp &nbsp;&nbsp;</span> <?=$Lang['MassMailing']['PreviewMergedData']?> &nbsp; &nbsp; &nbsp; <span style='background:#FF6666;'> &nbsp &nbsp;&nbsp;</span> <?=$Lang['MassMailing']['PreviewMissingData']?></p>
			</td>
		</tr>
	</table>
<?php
} else
{
?>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$Lang['SysMgr']['FormClassMapping']['MissingData']?> </td>
			<td align="left" valign="top" style="border:dotted #555555 1px;">
			  <?=$ErrorMessage?>
			</td>
		</tr>	
	</table>
		
		
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "location='index.php'")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php
}
?>

	<input type="hidden" name="MassMailingID" value="<?= $MassMailingID ?>" />
		
</form>
<?

	
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>