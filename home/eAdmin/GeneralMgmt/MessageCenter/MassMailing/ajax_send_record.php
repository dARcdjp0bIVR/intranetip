<?php
# using: 

#################################
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html("popup_no_layout.html");
	$limc = new libmessagecenter();
	
	
	# for EJ only!
	$limc->CaterBigForAJAX();
	
	
	$sql = "SELECT * " .
			"FROM INTRANET_MASS_MAILING_LOG " .
			"WHERE MassMailingID='{$MassMailingID}' " .
			"ORDER BY InputDate ASC ";
	$rows = $limc->returnArray($sql);
	for ($i=0; $i<sizeof($rows); $i++)
	{
		$rowObj = $rows[$i];
		$ClassName = ($rowObj["Class"]!="") ? $rowObj["Class"] : "--";
		$ClassNumber = ($rowObj["ClassNumber"]!="" && $rowObj["ClassNumber"]>0) ? str_pad($rowObj["ClassNumber"], 2, "0", STR_PAD_LEFT) : "--";
		$row_style = ($i%2==0) ? "" : "class='row_avaliable'";
		$ParentIcon = ($rowObj["IsParent"]) ? "(".$Lang['Identity']['Parent'].") " : "";
		if ($rowObj["RecipientEmail"]=="[CAMPUSMAIL]")
		{
			$rowObj["RecipientEmail"] = "<span class='tabletextremark'>".$i_adminmenu_sc_campusmail."</span>";
			$UseCampusMail = true;
		}
		if ($UseCampusMail)
		{
			$sent_records .= "<tr {$row_style}><td align='center'><a href=\"view_record.php?MassMailingID=".$MassMailingID."&MassMailingLogID=".$rowObj["MassMailingLogID"]."\" target=\"ViewSentOut\"><img title=\"".$Lang['Btn']['View']."\" src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_resultview.gif\" border='0' alignmiddle='abs'/></a></td><td nowrap='nowrap'>".$ClassName."</td><td nowrap='nowrap'>".$ClassNumber."</td><td>".$ParentIcon.$rowObj["RecipientName"]."</td><td nowrap='nowrap'>". $rowObj["InputDate"]."</td></tr>\n";
		} else
		{
			$sent_records .= "<tr {$row_style}><td align='center'><a href=\"view_record.php?MassMailingID=".$MassMailingID."&MassMailingLogID=".$rowObj["MassMailingLogID"]."\" target=\"ViewSentOut\"><img title=\"".$Lang['Btn']['View']."\" src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_resultview.gif\" border='0' alignmiddle='abs'/></a></td><td nowrap='nowrap'>".$ClassName."</td><td nowrap='nowrap'>".$ClassNumber."</td><td>".$ParentIcon.$rowObj["RecipientEmail"]."</td><td>".$rowObj["SenderEmail"]."</td><td nowrap='nowrap'>". $rowObj["InputDate"]."</td></tr>\n";
		}
	}
	
	$print_all = $linterface->GET_BTN($Lang['Btn']['PrintAll'], "button", "jsPrintAll()", "PrintAll", "");
?>

<script language="JavaScript">
function jsPrintAll()
{
	newWindow("view_record.php?MassMailingID=<?=$MassMailingID?>&PrintAll=1", 37);
}
</script>


<form name="formAJ">

<table width="96%" border="0" align="center">
<tr>
	<td>
	<?=$print_all?>
		<table align="center" class="common_table_list">
<?php if ($UseCampusMail) {?>
		<thead>
			<th nowrap='nowrap'><?=$Lang['Btn']['View']?></th>
			<th width='10%'><?=$Lang['SysMgr']['RoleManagement']['Class']?></th>
			<th width='10%'><?=$Lang['SysMgr']['RoleManagement']['ClassNumber']?></th>
			<th width='60%'><?=$Lang['Gamma']['InternalRecipient']?></th>
			<th width='20%'><?=$Lang['MassMailing']['SendTime']?></th>
		</thead>
<?php } else { ?>
		<thead>
			<th nowrap='nowrap'><?=$Lang['Btn']['View']?></th>
			<th width='9%'><?=$Lang['SysMgr']['RoleManagement']['Class']?></th>
			<th width='5%'><?=$Lang['SysMgr']['RoleManagement']['ClassNumber']?></th>
			<th width='35%'><?=$Lang['MassMailing']['RecipientEmail']?></th>
			<th width='30%'><?=$Lang['Gamma']['SendersMailAddress']?></th>
			<th width='20%'><?=$Lang['MassMailing']['SendTime']?></th>
		</thead>
<?php } ?>
		<tbody>
			<?=$sent_records?>
		</tbody>
		</table>
	</td>
</tr>
</table>

</form>
<?
	
	intranet_closedb();
?>