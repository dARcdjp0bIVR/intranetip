<?php
//Modifying by: Isaac

############ Change Log Start ###############
#   Date:	2018-03-14 (Isaac) [DM#3390]
#           fixed $keyword for search sql, added intranet_htmlspecialchars() to $keyword.
#
#   Date:	2017-01-17 (Isaac)
#	        Improved: added EnglishName & ChineseName to keyword search's sql
#                     added Get_Safe_Sql_Like_Query to keyword search's sql
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}


$CurrentPageArr['eAdmineMassMailing'] = 1;
$CurrentPage = "PageMassMailing";

$linterface = new interface_html();
$limc = new libmessagecenter();

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == "") ? 0 : $order;
$field = ($field == "") ? 4 : $field;

if ($keyword!="")
{
    $keyword = intranet_htmlspecialchars(trim($keyword));
    $conditions = " AND ((imm.EmailTitle LIKE '%".$keyword."%') OR (iu.EnglishName like '%".$keyword."%') OR
				(iu.ChineseName like '%".$keyword."%')) ";
}

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("iu.");
$sql = "
		SELECT 
			CONCAT('<a href=\"new_step2.php?MassMailingID=', imm.MassMailingID,'\">', if(imm.RecordStatus<>2, '<b>', ''), imm.EmailTitle, if(imm.RecordStatus<>2, '</b>', ''), '</a>'), 
			CASE imm.RecordStatus WHEN 2 THEN '".$Lang['MassMailing']['StatusSent']."' WHEN 1 THEN '".$Lang['MassMailing']['StatusSending']."' ELSE '".$Lang['MassMailing']['StatusDraft']."' END, 
			if (COUNT(imml.MassMailingLogID)>0, 
					CONCAT('<a href=\"ajax_send_record.php?MassMailingID=', imm.MassMailingID,'&width=780\" class=\"thickbox\" title=\"[".$Lang['MassMailing']['SendRecord']."] ', REPLACE(imm.EmailTitle, '\"', '&quot;'), '\" >', COUNT(imml.MassMailingLogID), '</a>'),
			   0) AS TotalSent, 
			{$name_field} AS StaffName, imm.InputDate,  
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" id=\"RecordID[]\" value=\"', imm.MassMailingID ,'\" />')
		FROM 
			INTRANET_MASS_MAILING AS imm LEFT JOIN INTRANET_USER as iu On iu.UserID=imm.InputBy LEFT JOIN INTRANET_MASS_MAILING_LOG AS imml on imml.MassMailingID=imm.MassMailingID
		WHERE
			imm.MassMailingID>0
			$conditions
		GROUP BY
			imm.MassMailingID
		";
//echo $sql;	
$li->sql = $sql;
$li->field_array = array("imm.EmailTitle", "imm.RecordStatus", "TotalSent", "StaffName", "InputDate");
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='40%' >".$li->column($pos++, $Lang['Gamma']['Subject'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_general_status)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['MassMailing']['NoOfDone'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['General']['CreatedBy'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['IP_FILE']['InputDate'])."</th>\n";
//$li->column_list .= "<th width='5'>".$li->check("RecordID[]")."</th>\n";

$toolbar = $linterface->GET_LNK_ADD("new.php",$button_new,"","","",0);

### Title ###
$TAGS_OBJ[] = array($Lang['Header']['Menu']['MassMailing']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

## search
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"".stripslashes($keyword)."\" onkeyup=\"Check_Go_Search(event);\" />";

?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script> 

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 

<script language="javascript">
<!--

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}


-->
</script>

<form name="form1" id="form1" method="POST" action="index.php" onSubmit="return checkForm()">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchTag?></div>     
	<br style="clear:both" />
</div>


<?=$li->display();?>


<br />
<input type="hidden" name="completeFlag" id="completeFlag" value="<?=$completeFlag?>" />
<input type="hidden" name="requestFlag" id="requestFlag" value="<?=$requestFlag?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>