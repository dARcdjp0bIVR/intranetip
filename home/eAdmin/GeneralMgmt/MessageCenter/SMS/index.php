<?php //

/********************************************************
 *    Date:	2018-03-14 (Isaac) [DM#3390]
 *              -fixed $keyword for search sql, added intranet_htmlspecialchars() to $keyword.
 * 
 *   2017-01-17 (Isaac)
 *              - added LEFT JOIN INTRANET_USER, EnglishName & ChineseName to keyword search's sql
 *              - added Get_Safe_Sql_Like_Query to keyword search's sql
 *
 *	 2015-02-03 (Omas) X74829 
 *				Improve performance
 * 				- default order by SourceID (Pri Key)
 * 				- column of Sorting by DateInput -> Sorting by Pri Key 
 *   2013-03-20	(Ivan) [2012-0928-1438-14066]
 * 				- added logic to show scheduled sms send time
 *	2012-11-07	(Rita)
 * 			  	- add Status filter, Cookies for filter
 * 
 ********************************************************/
 
//if ($page_size_change == 1)
//{
//    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
//    $ck_page_size = $numPerPage;
//}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_opendb();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$arrCookies = array(); 

if ($page_size_change == 1)
{
  	$arrCookies[] = array("ck_page_size", "numPerPage"); 	
}

if ($sms_vendor == 'TACTUS'){
	# Cookie for maintaining filter status after save / cancel	
	$arrCookies[] = array("ck_status_option", "statusOptionArrSting"); 

	if ($sms_vendor == 'TACTUS' && isset($_GET['StatusOption'])){
		$statusOptionArr = $_GET['StatusOption'];
		$statusOptionArrSting='';
		if(count($statusOptionArr)>0){
			$statusOptionArrSting = implode(',', $statusOptionArr);
		}
	}		
}

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}else{
	updateGetCookies($arrCookies);	
}

$CurrentPage = "PageSMS";
$linterface = new interface_html();
$limc = new libmessagecenter();

### Refresh SMS status
$libsms = new libsmsv2();
$isRefresh = $_GET['isRefresh'];
if ($isRefresh)
{
	$libsms->Refresh_SMS_Status();
	$xmsg = "UpdateSuccess";
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = intranet_htmlspecialchars(trim($keyword));
if ($field=="") $field = 2;
if ($order=="") $order = 0;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 5; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;

$namefield = getNameFieldByLang("b.");

#$content_field = " IF(a.IsIndividualMessage <>1, a.Content, IF(a.Content IS NOT NULL AND TRIM(a.Content)<>'', a.Content, IF(a.RecipientCount=1, c.Message, '<i>[$i_SMS_MultipleMessage]</i>')))";
$content_field = " IF(a.IsIndividualMessage <>1, a.Content, IF(a.Content IS NOT NULL AND TRIM(a.Content)<>'', a.Content, '<i>[$i_SMS_MultipleMessage]</i>'))";

$StatusField = '';
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
{
	$StatusField = $libsms->Get_SMS_Status_Field('ismr');
	$StatusFieldSQL = " IF (a.RecipientCount > 1 And (ismr.ReferenceID is Not Null Or ismr.ReferenceID != ''), 
							'<i>[".$i_SMS['MultipleStatus']."]</i>', 
							$StatusField) 
						as RecordStatus, ";
}

if ($sms_vendor == 'TACTUS'){
	if(!isset($statusOptionArr) && isset($ck_status_option)){
		$statusOptionArr = explode(',',$ck_status_option); 
	}
	$numOfStatusOptionArr = count($statusOptionArr);
	if($numOfStatusOptionArr>0){
		$status_cond = "AND  IF (a.RecipientCount > 1 And (ismr.ReferenceID is Not Null Or ismr.ReferenceID != ''), 
							'MultipleStatus', 
							IF (ismr.ReferenceID Is Null,'4',ismr.RecordStatus)) 
							 IN ('".implode("','", (array)$statusOptionArr)."')";
	}else{
		$status_cond = '';
	}
	
	$statusFilter = $libsms->getSMSStatusMultiSelection(true, $statusOptionArr);	
	
}
else{
		$status_cond = '';
}

if ($sms_vendor != "TACTUS") {
	$scheduleDateTimeField = " If (a.ScheduleDateTime is null OR a.ScheduleDateTime = '0000-00-00 00:00:00', '".$Lang['General']['EmptySymbol']."', a.ScheduleDateTime) as ScheduleDateTime, ";
}

if(empty($keyword)){
	$search_conds = '';
}
else{
    $search_conds = " AND (($content_field LIKE '%".$limc->Get_Safe_Sql_Query($keyword)."%') OR (iu.EnglishName like '%".$limc->Get_Safe_Sql_Query($keyword)."%') OR
				(iu.ChineseName like '%".$limc->Get_Safe_Sql_Query($keyword)."%') ) ";
}

$sql = "SELECT        		
                concat('<a href=detail.php?SourceID=', a.SourceID,'>',$content_field,'</a>'),
                concat('<a href=detail.php?SourceID=', a.SourceID,'>',a.RecipientCount,'</a>'),
                a.UserPIC,
                a.DateInput,
				$scheduleDateTimeField
				$StatusFieldSQL
				a.PICType,
				a.AdminPIC
        FROM 
        		INTRANET_SMS2_SOURCE_MESSAGE as a
                LEFT JOIN INTRANET_USER as iu On iu.UserID=a.UserPIC
        		INNER JOIN INTRANET_SMS2_MESSAGE_RECORD as ismr ON (a.SourceID = ismr.SourceMessageID)
        WHERE 
             	1
				$search_conds
				$status_cond            
        Group By
               a.SourceID
";
#             LEFT OUTER JOIN INTRANET_SMS2_MESSAGE_RECORD as c ON (a.SourceID = c.SourceMessageID)


# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
//if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
//	$li->field_array = array("a.Content", "a.RecipientCount", "PIC", "a.DateInput", "RecordStatus");
//else
//	$li->field_array = array("a.Content", "a.RecipientCount", "PIC", "a.DateInput");
	
$li->field_array = array("a.Content", "a.RecipientCount", "a.SourceID");
if ($sms_vendor != "TACTUS") {
	$li->field_array[] = 'ScheduleDateTime';
}
if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS') {
	$li->field_array[] = 'RecordStatus';
}

$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = "";
$li->column_array = array(5,0,0,0);
$li->wrap_array = array(25,0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = 'MessageCenter_SmsIndex';

// TABLE COLUMN
$col = 0;
$li->column_list .= "<th width=1 class=num_check>#</th>\n";
$li->column_list .= "<th class=tableTitle>".$li->column($col++, $i_SMS_MessageContent)."</th>\n";
$li->column_list .= "<th width=10% class=tableTitle>".$li->column($col++, $i_SMS_RecipientCount)."</th>\n";
$li->column_list .= "<th width=15% class=tableTitle>".$Lang['General']['CreatedBy']."</th>\n";
//$li->column_list .= "<th width=20% class=tableTitle nowrap>".$li->column(3, $i_SMS_TargetSendTime)."</th>\n";
$li->column_list .= "<th width=15% class=tableTitle nowrap>".$li->column($col++, $Lang['SMS']['SmsCreationTime'])."</th>\n";
if ($sms_vendor != "TACTUS") {
	$li->column_list .= "<th width=15% class=tableTitle nowrap>".$li->column($col++, $Lang['SMS']['ScheduleDateTime'])."</th>\n";
}
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS')) {
	$li->column_list .= "<th width=15% class=tableTitle>".$li->column($col++, $i_general_status)."</th>\n";
}


//$toolbar  .= "<br><a class=iconLink href=javascript:checkPost(document.form1,'export.php')>".exportIcon()."$button_export</a>".toolBarSpacer();
$toolbar = $linterface->GET_LNK_ADD("new.php",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkPost(document.form1,'export.php')",$button_export,"","","",0);
# Yuen: to support
//$toolbar .= "<div class='Conntent_tool'><a class='clear_record' href=javascript:checkPost(document.form1,'clear.php')>$i_SMS_ClearRecords</a></div>";
$toolbar .= " <div class='Conntent_tool'><a href='javascript:refresh();' class='refresh_status'>". $i_SMS['RefreshAllStatus'] ."</a></div> \n";
# Yuen: to support
//$toolbar .= "<div class='Conntent_tool''><a href='javascript:refresh();' class='blank_content' ><img src='/images/admin/icon_refresh.gif' border='0' align='absmiddle'>". $i_SMS['RefreshAllStatus'] ."</a></div>";

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
//$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


$Instruction = $libsms->Get_SMS_Fee_Notes();

$sentSql = "SELECT COUNT(*) FROM INTRANET_SMS_LOG WHERE RecordStatus = 2 OR RecordStatus = 0 OR RecordStatus IS NULL";
$result = $li->returnVector($sentSql);
$sent = $result[0]+0;

$quota = get_file_content("$intranet_root/admin/sms/limit.txt");
$removed = get_file_content("$intranet_root/admin/sms/removed.txt");
$removed += 0;
$sent += $removed;

$left = $quota - $sent;

$left = ($left<0? "<font color=red>$left</font>":"<font color=green>$left</font>");

$infobar = "$i_SMS_MessageSentSuccessfully: $sent<br>$i_SMS_MessageQuotaLeft: $left";

### Title ###

$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);


?>

<script language="javascript">
<!--
        function view_detail(sid)
        {
                with(document.form1)
                {
                        SourceID.value = sid;
                        submit();
                }
        }
        
        function refresh()
		{
			document.form1.isRefresh.value = 1;
			document.form1.submit();
		}
		
		
//-->
<? if ($sms_vendor == 'TACTUS')
{	
?>

	function checkboxCheck(flag) {	
			if(flag==true){		
				$('#Pending').attr('checked', true);
				$('#Sending').attr('checked', true);
				$('#Failed').attr('checked', true);
				$('#Delivered').attr('checked', true);	
				$('#MultipleStatus').attr('checked', true);	
				$('#FailToSendSMS').attr('checked', true);					
				
			}else{
				$('#Pending').attr('checked', false);
				$('#Sending').attr('checked', false);
				$('#Failed').attr('checked', false);
				$('#Delivered').attr('checked', false);
				$('#MultipleStatus').attr('checked', false);
				$('#FailToSendSMS').attr('checked', false);		
			}
		
		}
<?}?>
</script>

<form name="form1" id="form1" method="get" action="index.php" >

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchbar?></div>     
	<br style="clear:both" />
</div>

<?php if ($Instruction!="") {?>
<center>
<fieldset class="instruction_box">
	<legend><?=$Lang['Header']['Menu']['SMS']?></legend>
	<?= $Instruction ?>
</fieldset>
</center>
<?php } ?>

<?=$statusFilter;?>

<?=$li->display();?>


<br />



<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="SourceID" value="">
<input type="hidden" name="isRefresh" value="">



</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>