<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php"); 
include_once($PATH_WRT_ROOT."includes/libexporttext.php");


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_opendb();

$li = new libdb();
$list = implode(",",$TemplateID);

$table = ($TemplateType==1) ? "INTRANET_SMS_TEMPLATE" : "INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";

$sql = "DELETE FROM $table WHERE TemplateID IN ($list)";
$li->db_db_query($sql);


intranet_closedb();
header("Location: template.php?TemplateType=$TemplateType&xmsg=DeleteSuccess");
?>