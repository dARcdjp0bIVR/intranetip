<?php

# using: 


###################### Change Log Start ########################
#	Date:	2015-11-17	Ivan [P88986] [ip.2.5.7.1.1]
#			- trim the message before sending to service provider now
#
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");

intranet_auth();
intranet_opendb();

# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

$libsms = new libsmsv2();

if ($_SESSION["SSV_PLUGINS"]["SMS"]["tactus_from"]!="")
{
	$sms_tactus_from = $_SESSION["SSV_PLUGINS"]["SMS"]["tactus_from"];
}

$message = trim($Message);

$sms_message = sms_substr(intranet_htmlspecialchars(trim($message)));
if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";

if($recipient!=""){
	$temp_numbers = explode(";",$recipient);
	//$temp_numbers = array_unique($temp_numbers);  // remove duplicated phone number
	$temp_numbers = array_keys(array_flip($temp_numbers));		// remove duplicated phone number with continous index
	
	$numbers=array();
	$valid_numbers= array();
	$invalid_numbers=array();
	for ($i=0; $i<sizeof($temp_numbers); $i++)
	{
		 if(trim($temp_numbers[$i])=="") continue;
	     $numbers[$i] = $libsms->parsePhoneNumber($temp_numbers[$i]);
	     
	     if($libsms->isValidPhoneNumber($numbers[$i])){
	     	$valid_numbers[] = $numbers[$i];
	 	 }else if($numbers[$i]!=""){
		 	$invalid_numbers[] = $numbers[$i];
	 	 }
	}
	
	// CREATE RECIPIENT DATA
	for($i=0;$i<sizeof($valid_numbers);$i++){
		$phone = $valid_numbers[$i];
		$recipientData[] = array($phone,0,"","");
	}
	
		
	if(sizeof($recipientData)>0 && $send==1){ // user clicked send button
		$sms_message = $message;
		$targetType = 1;
		$picType = 2;
		$adminPIC =$PHP_AUTH_USER;
		$userPIC = $UserID;
		$frModule= "";
		$deliveryTime = $time_send;
		$isIndividualMessage=false;

		$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
		$isSent = 1;
	}
		
	if($isSent && ($returnCode > 0 && $returnCode==true))
		header("Location: index.php?xmsg=SMSsubmitted");
	else{
	
		# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPage = "PageSMS";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "index.php");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New']. " (".$Lang['MassMailing']['NewStep2'].")");
	
	
	# Start layout
	$linterface->LAYOUT_START();
		
		$xmsg = '';
		if ($isSent && $returnCode < 0)
			$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
			
		
		if ($isSent)
	    	$btn_to_send = $Lang['SMS']['ReSend'];
	    else
	    	$btn_to_send = $Lang['SMS']['Send'];
			
		# ------------- invalid ------------ #
	    $reason =$i_SMS_Error_NovalidPhone;
	
		if (sizeof($invalid_numbers)>0)
		{
			$invalids = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
			$invalids.= "<table class=\"common_table_list view_table_list\">";
			$invalids.= "<thead><tr><th width=30%>$i_SMS_MobileNumber</th><th width=70%>$i_Attendance_Reason</th></tr></thead>";
		    for($i=0;$i<sizeof($invalid_numbers);$i++)
		    {
		    	        $css = $i%2==0?"row_suspend":"";
			    		$mobile = $invalid_numbers[$i];
	    				$invalids.="<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
		    }
		    $invalids.= "</table>";
			$invalids.="</div>";
		}
	    
	    
	    # ---------------------- valid list ---------- #
	    if (sizeof($valid_numbers)>0)
	    {
			$valids = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
			$valids.= "<table class=\"common_table_list view_table_list\">";
			$valids.= "<thead><tr><th width='1%'>&nbsp;</th><th width='99%'>$i_SMS_MobileNumber</th></tr></thead>";
		    for($i=0;$i<sizeof($valid_numbers);$i++){
			    $mobile = $valid_numbers[$i];
		    
		        $css = $i%2==0?"row_avaliable":"";
	    		$valids.="<tr class='$css'><td>".($i+1)."</td><td>".($mobile==""?"-":$mobile)."</td></tr>";
		    }
		    $valids.= "</table>";
			$valids.="</div>";
			
		    $valids.="<Br />";
	    }
			
		
		}
		
		?>
		<script language='javascript'>
		function sendSMS(){

<?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
			alert("Sorry, this action is not allowed in trial site!");
			return false;
<?php } ?>
			
			obj = document.form1;
			if(document.form1==null) return;
			
			sendObj = obj.send;
			if(sendObj==null) return;
			sendObj.value = 1;
			
			$('div#actionBtnDiv').hide();
			$('div#sendingSmsDiv').show();
			
			document.form1.submit();
			
		}
		</script>
		<form name=form1 action='send_mobile_update.php' method=post>
		<input type=hidden name="scdate" value="<?=$scdate?>">
		<input type=hidden name="sctime" value="<?=$sctime?>">
		<input type=hidden name="Message" value="<?=intranet_htmlspecialchars(stripslashes($message))?>">
		<input type=hidden name="recipient" value="<?=$recipient?>">
		<input type=hidden name="send" value="">
		
		
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><table width="90%" border="0" cellspacing="5" cellpadding="15" align="center">	
				<tr>
					<td nowrap='nowrap'><?=$Lang['SMS']['MessageToBeSent']?></td><td bgcolor='#DEEAFF' style='border: 1px dotted #4466AA;' width='95%'><?=stripslashes($sms_message)?></td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">	
	<tr>
		<td>
		<?=$invalids?>
		<Br />		
		<?=$valids?>
		</td>
	</tr>
	</table>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="actionBtnDiv">
								<?php if (sizeof($valid_numbers)>0) { ?>
									<?= $linterface->GET_ACTION_BTN($btn_to_send, "button", "sendSMS()")?>&nbsp;
								<?php } ?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php'")?>
							</div>
							<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
								<?=$Lang['SMS']['SendingSms']?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		
	</form>


    <?php
    intranet_closedb();
	$linterface->LAYOUT_STOP();

}
                    

?>