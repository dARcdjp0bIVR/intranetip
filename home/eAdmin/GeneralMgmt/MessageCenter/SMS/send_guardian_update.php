<?php


# using: yuen


###################### Change Log Start ########################
#	Date: 	2013-04-19 yuen 
#			fixed the bug of saving parent as the SMS sender
#
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");

intranet_auth();
intranet_opendb();

# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}



$libsms = new libsmsv2();

$message = str_replace("\n", "", trim($Message));
$sms_message = intranet_htmlspecialchars(trim($message));

##---- get user list ----------###

$Recipient = array_unique($RecipientG);
$Recipient = array_values($RecipientG);

$group_list = array();
$user_list = array();


for($i=0;$i<sizeof($Recipient);$i++){
	$user = $Recipient[$i];
	if(substr($user,0,1)=="G"){
		$group_list[] = substr($user,1);
	}
	else if(substr($user,0,1)=="U"){
		$user_list[] =  substr($user,1);
	}
}


if(sizeof($group_list)>0){
	$list = implode(",",$group_list);
	$sql = "Select
					iu.UserID
			From
					INTRANET_USER as iu
					Inner Join
					YEAR_CLASS_USER as ycu On (iu.UserID = ycu.UserID)
					Inner Join
					YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
			Where
					yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					And
					yc.YearClassID in ($list)
					And
					iu.RecordType = 2
					And
					iu.RecordStatus in (0,1,2)
			";
	$temp= $libsms->returnVector($sql);
	if(sizeof($temp)>0)
		$user_list = array_merge($user_list,$temp);
}

$user_list = array_unique($user_list);


if (sizeof($user_list)==0)
{
    header("Location: index.php?nouser=1");
    exit();
}


if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";

$list = implode("','",$user_list);

# ---- end get user list ---- #



## ----- get guardian data ---- ##
$namefield  = $libsms->getSMSIntranetUserNameField("b.");
$namefield2 = $libsms->getSMSGuardianUserNameField("a.");

$sql="SELECT a.UserID, $namefield2, a.Emphone, a.Phone, a.IsMain, a.IsSMS, b.UserID, $namefield, b.ClassName, b.ClassNumber  FROM $eclass_db.GUARDIAN_STUDENT AS a RIGHT OUTER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.UserID IN ('$list') AND b.RecordType=2 AND b.RecordStatus IN (0,1,2) ORDER BY b.UserID";
$temp = $libsms->returnArray($sql,10);

for($i=0;$i<sizeof($temp);$i++){
	
	list($a_userid,$name,$emphone,$phone, $is_main,$is_sms, $b_userid, $student,$class,$classnumber) = $temp[$i];
	$emphone = $libsms->parsePhoneNumber($emphone);
	$phone = $libsms->parsePhoneNumber($phone);

	$target_phone = "";
	if($libsms->isValidPhoneNumber($emphone))
		$target_phone = $emphone;
	else if($libsms->isValidPhoneNumber($phone))
		$target_phone = $phone;
	
	$guardian_data[$b_userid][] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	
	if($a_userid=="")	continue;
	
	
	if($is_sms)
		$sms[$b_userid] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	else if($is_main)
		$main[$b_userid] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
	else $others[$b_userid][] = array($target_phone,$a_userid,$name,$student,$class,$classnumber);
}

/*
1. IsSMS
2. IsMain
3. Neither IsSMS nor IsMain
*/
$valid=array();
$error_list=array();
for($i=0;$i<sizeof($user_list);$i++){
	
	$user_id = $user_list[$i];
	
	$hasGuardian = ($sms[$user_id]!="" || $main[$user_id]!="" || $others[$user_id]!="")? true : false;
	
	if($sms[$user_id]!="" && $sms[$user_id][0]!=""){ // has IsSMS AND IsSMS phone valid
				$recipientData[] = array($sms[$user_id][0],$sms[$user_id][1],$sms[$user_id][2],"");
				$valid[$user_id] = $sms[$user_id];
				continue;
	}
	else if( $main[$user_id]!="" && $main[$user_id][0]!=""){ 
				## ( No IsSMS OR IsSMS phone invalid ) AND  has IsMain AND IsMain phone valid
				$recipientData[] = array($main[$user_id][0],$main[$user_id][1],$main[$user_id][2],"");
				$valid[$user_id] = $main[$user_id];
				continue;
	}else if($others[$user_id]!=""){
			# ( No IsSMS OR IsSMS phone invalid ) AND ( No IsMain OR IsMain phone invalid )
			$other = $others[$user_id];
			for($j=0;$j<sizeof($other);$j++){
				$phone = $other[$j][0];
				if($phone!=""){ 
					$recipientData[] = array($other[$j][0],$other[$j][1],$other[$j][2],"");
					$valid[$user_id] = $other[$j];
					break;
				}
			}
	}
	if($valid[$user_id]==""){
		if($hasGuardian){ 
			// has guardian but no valid phone num
			$reason[$user_id] = $i_SMS_Error_NovalidPhone;
		}
		else{ 
			// no guardian
		   $reason[$user_id] = $i_SMS_Error_No_Guardian;
		}
		$error_list[] = $user_id;
	}		
}	

if(sizeof($recipientData)>0 && $send==1){
	$targetType = 4;
	$picType = 2;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=false;
	$sms_message = $message;
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;

}

if($isSent && ($returnCode > 0 && $returnCode==true))
{
	intranet_closedb();
	header("Location: index.php?sent=".$isSent);
}
else
{
	## ---------  valid list----------------##
	$hasValidNumber = sizeof($valid)>0?true:false;
	if ($hasValidNumber)
	{
		$valid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
		$valid_table.= "<table class=\"common_table_list view_table_list\">";
		$valid_table.= "<thead><tr><th width='1'>&nbsp;</th><th width=25%>$i_UserName</th><th width=13%>$i_ClassName</th><th width=12%>$i_ClassNumber</th><th width=25%>$i_StudentGuardian_GuardianName</th><th width=25%>$i_SMS_MobileNumber</th></tr></thead>";
		$i=0;
		foreach($valid as $user_id =>$values){
			list($mobile,$user_id,$name,$student,$class,$classnumber) = $values;
			$css = $i%2==0?"row_avaliable":"";
		    $valid_table.="<tr class='$css'><td>".($i+1)."</td><td>".($student==""?"-":$student)."</td><td>".($class==""?"-":$class)."</td><Td width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td></tr>";
			$i++;
		}	
		$valid_table.="</table>";
		$valid_table.="</div>";
	}

	#### --------  invalid list ------ #
	if (sizeof($error_list)>0)
	{
		$invalid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
		$invalid_table.= "<table class=\"common_table_list view_table_list\">";
		$invalid_table.= "<thead><tr><th width=20%>$i_UserName</th><th width=10%>$i_ClassName</th><th width=10%>$i_ClassNumber</th><th  width=15%>$i_StudentGuardian_GuardianName</th><th  width=15%>$i_SMS_MobileNumber</th><th width=30%>$i_Attendance_Reason</th></tr></thead>";
        for($i=0;$i<sizeof($error_list);$i++)
        {
   	         $css = $i%2==0?"row_suspend":"";
	        $user_id = $error_list[$i];
	        $error_reason = $reason[$user_id];
	        $data = $guardian_data[$user_id];
	        if(sizeof($data)==1){
   		        list($mobile,$id,$name,$student,$class,$classnumber)= $data[0];
	    		$invalid_table.="<tr class='$css'><td>".($student==""?"-":$student)."</td><td>".($class==""?"-":$class)."</td><Td width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
			}else{
				$rowspan=sizeof($data);
  		        list($mobile,$id,$name,$student,$class,$classnumber)= $data[0];
	    		$invalid_table.="<tr class='$css'><td rowspan='$rowspan'>".($student==""?"-":$student)."</td><td rowspan='$rowspan'>".($class==""?"-":$class)."</td><Td rowspan='$rowspan' width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
		        for($j=1;$j<sizeof($data);$j++){
			        list($mobile,$id,$name,$student,$class,$classnumber)= $data[$j];
		    		$invalid_table.="<tr class='$css'><td>".($name==""?"-":$name)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($error_reason==""?"-":$error_reason)."</td></tr>";
			    }
		    }
	    }	
		$invalid_table.="</table>";
		$invalid_table.="</div>";
	}
	
	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPage = "PageSMS";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "index.php");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New']. " (".$Lang['MassMailing']['NewStep2'].")");
	
	
	# Start layout
	$linterface->LAYOUT_START();
	
	$xmsg = '';
	if ($isSent && $returnCode < 0)
		$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
	
	if ($isSent)
    	$btn_to_send = $Lang['SMS']['ReSend'];
    else
    	$btn_to_send = $Lang['SMS']['Send'];
	
	?>
	<script language='javascript'>
		function sendSMS(){
			
<?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
			alert("Sorry, this action is not allowed in trial site!");
			return false;
<?php } ?>


			obj = document.form1;
			if(document.form1==null) return;
			
			sendObj = obj.send;
			if(sendObj==null) return;
			sendObj.value = 1;
			
			$('div#actionBtnDiv').hide();
			$('div#sendingSmsDiv').show();
			
			document.form1.submit();
			
		}
	</script>
	<form name=form1 action='send_guardian_update.php' method=post>
	<input type=hidden name="scdate" value="<?=$scdate?>">
	<input type=hidden name="sctime" value="<?=$sctime?>">
	<input type=hidden name="Message" value="<?=intranet_htmlspecialchars(stripslashes($sms_message))?>">

	<?php 
		for($i=0;$i<sizeof($Recipient);$i++)
			echo "<input type=hidden name='RecipientG[]' value='".$Recipient[$i]."'>\n";
		for($i=0;$i<sizeof($usertype);$i++)
			echo "<input type=hidden name='usertype[]' value='".$usertype[$i]."'>\n";

	?>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><table width="90%" border="0" cellspacing="5" cellpadding="15" align="center">	
				<tr>
					<td nowrap='nowrap'><?=$Lang['SMS']['MessageToBeSent']?></td><td bgcolor='#DEEAFF' style='border: 1px dotted #4466AA;' width='95%'><?=stripslashes($sms_message)?></td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	
	
	<input type="hidden" name="send" value="">
	
	<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">	
	<tr>
		<td>
		<?=$invalid_table?>
		<Br />
		<?=$valid_table?>
		</td>
	</tr>
	</table>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="actionBtnDiv">
								<?php if ($hasValidNumber) {?>
									<?= $linterface->GET_ACTION_BTN($btn_to_send, "button", "sendSMS()")?>&nbsp;
								<?php } ?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php'")?>
							</div>
							<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
								<?=$Lang['SMS']['SendingSms']?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

    </form>
    <?php
    intranet_closedb();
	$linterface->LAYOUT_STOP();

}
                    

?>