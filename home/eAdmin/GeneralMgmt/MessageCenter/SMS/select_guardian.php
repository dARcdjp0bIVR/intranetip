<?php
# using: 

#################################
#	Date:	2015-03-27	Roy
#			- fix: should check "RecordStatus IN (0,1,2)" and "RecordType = 2" in SQL statement when getting student
#
#	Date:	2011-10-17	Yuen
#			support multiple data in multiple rows for a person
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$MODULE_OBJ['title'] = $Lang['CommonChoose']['PageTitle'][$page_title];
	$linterface = new interface_html("popup.html");
	
	
	$limc = new libmessagecenter();
	
	$li = new libclass();
	
	# Start layout
	$linterface->LAYOUT_START();

	$curAcademicYearID = Get_Current_Academic_Year_ID();
$sql="
		SELECT 
			DISTINCT a.ClassTitleEn as ClassName, a.YearClassID as ClassID
		FROM 
			YEAR_CLASS AS a, 
			INTRANET_USER AS b, 
			$eclass_db.GUARDIAN_STUDENT AS c 
		WHERE 
			a.ClassTitleEn = b.ClassName AND
			a.AcademicYearID = '$curAcademicYearID' AND
			b.UserID = c.UserID AND
			b.RecordType=2 AND 
			b.RecordStatus IN (0,1,2) 
		ORDER BY a.ClassTitleEn
	";
$result = $li->returnArray($sql,2);

$classlist = "<SELECT multiple name='ClassID[]' style='height=100px'>\n";
for ($i=0; $i < sizeof($result); $i++)
{
     $name = $result[$i][0];
     $classid  = $result[$i][1];
     $selected_str = (is_array($ClassID) && in_array($classid,$ClassID))?" SELECTED " : "";
     $classlist .= "<OPTION value='$classid' $selected_str>$name</OPTION>\n";
}
$classlist .= "</SELECT>\n";

if(sizeof($ClassID)>0){
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$list = implode(",",$ClassID);

	$sql="
			SELECT 
				DISTINCT a.UserID, $namefield 
			FROM 
				INTRANET_USER AS a , 
				YEAR_CLASS AS b, 
				$eclass_db.GUARDIAN_STUDENT AS c  
			WHERE 
				a.ClassName = b.ClassTitleEn AND 
				b.YearClassID IN ($list) AND 
				c.UserID = a.UserID AND
				a.RecordType=2 AND 
				a.RecordStatus IN (0,1,2) 
			ORDER BY 
				a.ClassName,a.ClassNumber
		";
		
	$students = $li->returnArray($sql,2);
	$select_students ="<SELECT name='StudentID[]' multiple style='height=150px'>\n";
	for($i=0;$i<sizeof($students);$i++){
		list($userid,$name) = $students[$i];
		$select_students.="<OPTION value='$userid'>$name</OPTION>\N";
	}
	$select_students.="</SELECT>";
}
?>


<script language="javascript">
function Trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     x = (obj.name == "ClassID[]") ? "G" : "U";
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
 
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text+"<?=$i_general_targetGuardian?>", x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}
function SelectAll()
{
        var obj = document.form1.elements['StudentID[]'];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}
</script>

<form name="form1" action="select_guardian.php" method="post">

<div class="form_table">
<table class="form_table_v30">
<tr>
	<td nowrap='nowrap' class="field_title"><?=$i_UserParentLink_SelectClass?></td>
	<td nowrap='nowrap' ><?=$classlist?></td>
	<td width="70%">
		<?= $linterface->GET_BTN($button_add, "button","checkOption(document.form1.elements['ClassID[]']);AddOptions(document.form1.elements['ClassID[]'])") . "<br>"; ?>
		<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(document.form1.elements['ClassID[]'])") . "<br>"; ?>
	</td>
</tr>
<?php if(sizeof($ClassID)>0) { ?>
<tr>
	<td class="field_title"><?=$i_general_select_guardian_from_student?><br /><span class="tabletextremark">(<?=$i_SMS_Notice_Show_Student_With_Guardian_Only?>)</span></td>
	<td ><?=$select_students?></td>
	<td>
		<?= $linterface->GET_BTN($button_select_all, "button","SelectAll()") . "<br>"; ?>
		<?= $linterface->GET_BTN($button_add, "submit","checkOption(document.form1.elements['StudentID[]']);AddOptions(document.form1.elements['StudentID[]'])") . "<br>"; ?>
	</td>
</tr>
<?php } ?>
</table>


<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
</div>

<input type=hidden name=fieldname value="<?php echo $fieldname; ?>" />
<input type=hidden name=page_title value="<?php echo $page_title; ?>" />
</form>

<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>