<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php"); 
include_once($PATH_WRT_ROOT."includes/libexporttext.php");


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_opendb();

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$Content = intranet_htmlspecialchars(trim($Content));

if (isset($TemplateID) && $TemplateID>0)
{
	if($TemplateType==1)
	{
		$table		= "INTRANET_SMS_TEMPLATE";	
		$Title 		= intranet_htmlspecialchars(trim($Title));
		
		$fieldname .= "Title = '$Title', ";
	}
	else
	{
		$table			= "INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";	
		$TemplateCode	= intranet_htmlspecialchars(trim($TemplateCode));
		
		$fieldname .= "RecordStatus = '$RecordStatus', ";
	}
	
	$Content = intranet_htmlspecialchars(trim($Content));
	
	$fieldname .= "Content = '$Content', ";
	$fieldname .= "DateModified = now()";
	$sql = "UPDATE $table SET $fieldname WHERE TemplateID = $TemplateID";
	$result = $li->db_db_query($sql);
	
	$msg = ($result) ? "UpdateSuccess" : "UpdateUnsuccess";
} else
{
	if($TemplateType==1) 	# Normal Template
	{
		$tableDB	= "INTRANET_SMS_TEMPLATE";	
		$fieldname 	= "Title,Content,DateInput,DateModified";
		$fieldvalue = "'$Title','$Content',now(),now()";
	}
	else					# System Template
	{
		$tableDB	= "INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";	
		$fieldname 	= "TemplateCode,Content,DateInput,DateModified,RecordStatus";
		$fieldvalue = "'$TemplateCode','$Content',now(),now(),'$RecordStatus'";
	}	
	
	$sql = "INSERT INTO $tableDB ($fieldname) VALUES ($fieldvalue)";
	$result = $li->db_db_query($sql);
	
	$msg = ($result) ? "AddSuccess" : "AddUnsuccess";
}

intranet_closedb();
header("Location: template.php?xmsg={$msg}&TemplateType=$TemplateType");
?>