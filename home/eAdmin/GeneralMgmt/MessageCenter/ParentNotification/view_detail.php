<?php
# editing by:

/***************************************************************************
 * 	modification log:
 *
 *  2020-07-28  Ray
 *  - add display returnMessage
 *
 *  2020-03-03  Bill    [2020-0228-0904-26206]
 *  - use full file path to fix display problem of attach image in KIS
 *
 * 	2019/12/17  Philips
 * 	- use relative path instead of curl (Image Path)
 *
 *  2019/05/29	Ronald
 *  - add display image attachment
 *
 * 	2016/10/12	Villa [L105447]
 * 	- add export btn
 *
 * 	2016/05/18	Ivan [L95978] [ip.2.5.7.7.1]
 * 	- added status for user without push message registered
 *
 * 	2016/03/11 Ivan [ip.2.5.7.4.1]
 * 	- added student app logic
 *
 * 	2015/09/17 Ivan [ip.2.5.6.10.1]
 * 	- modified count the message priority as follows now: 1) has read, 2) sent success, 3) waiting to send, 4) send failed
 *
 * 	2013/12/18	Ivan
 * 	- added new eClass App logic to show message related user and message status
 *
 * 	2013/12/13	Roy
 * 	- fix list cannot display all items problem due to GROUP_CONCAT limit
 *
 *  2013/10/10	Roy
 * 	improve $recipient_html display:
 * 		- show parent, children and msg content for multiple messages
 * 		- show parent and children for single message
 *
***************************************************************************/

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
//{
//	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	$laccessright = new libaccessright();
//	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
//}

intranet_auth();
intranet_opendb();

# Create interface instance
$linterface = new interface_html();

$limc = new libmessagecenter();
$leClassApp = new libeClassApp();

$appType = ($_POST['appType']) ? $_POST['appType'] : $appType;

$messageStatus = isset($_POST['messageStatus']) ? $_POST['messageStatus'] : -1;
$returnMsgKey = ($_POST['returnMsgKey']) ? $_POST['returnMsgKey'] : $_GET['returnMsgKey'];

$limc->checkNotifyMessageAccessRight($appType);

$NotifyMessageID = ($_POST['targetNotifyMessageID']) ? $_POST['targetNotifyMessageID'] : $NotifyMessageID;
if($NotifyMessageID == '')
{
	intranet_closedb();
	header("Location: index.php?filter=$filter&order=$order&field=$field&appType=$appType");
	exit;
}

if ($appType == $eclassAppConfig['appType']['Parent'])
{
	$CurrentPage = "PageParentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['ParentAppsNotify'], "javascript:history.back();");
	$publicLang = $Lang['AppNotifyMessage']['Public'];
	
	$orderByName = false;
	$orderByClassNameClassNumber = false;
}
else if ($appType == $eclassAppConfig['appType']['Teacher'])
{
	$CurrentPage = "PageTeacherNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['TeacherAppsNotify'], "javascript:history.back();");
	$publicLang = $Lang['AppNotifyMessage']['Public(Teacher)'];
	
	$orderByName = true;
	$orderByClassNameClassNumber = false;
}
else if ($appType == $eclassAppConfig['appType']['Student'])
{
	$CurrentPage = "PageStudentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['StudentAppsNotify'], "javascript:history.back();");
	$publicLang = $Lang['AppNotifyMessage']['Public(Student)'];
	
	$orderByName = true;
	$orderByClassNameClassNumber = true;
}

$li = new libdb();

$name_field = getNameFieldByLang("iu.");
$sql = "SELECT 
			DATE_FORMAT(m.NotifyDateTime, '%Y-%m-%d %H:%i:%s') as NotifyDateTime,m.MessageTitle,m.MessageContent,m.NotifyFor,
			IF(m.NotifyUserID is not null AND m.NotifyUserID > 0, $name_field, m.NotifyUser) as NotifyUser,
			m.IsPublic,t.TargetID as TargetID 
		FROM
		    INTRANET_APP_NOTIFY_MESSAGE as m 
            LEFT JOIN INTRANET_APP_NOTIFY_MESSAGE_TARGET as t ON m.NotifyMessageID = t.NotifyMessageID
            LEFT JOIN INTRANET_USER as iu ON (m.NotifyUserID = iu.UserID)
		WHERE
		    m.NotifyMessageID = '$NotifyMessageID' ";
$temp_result = $li->returnArray($sql);
$NotifyMessage = $temp_result[0];

if ($NotifyMessage['MessageContent'] == "MULTIPLE MESSAGES")
{
	# multiple messages case
	$sql = "SELECT * FROM INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageID = '$NotifyMessageID' ORDER BY TargetID";
	$log_rows = $li->returnResultSet($sql);
	for ($i=0; $i<sizeof($log_rows); $i++)
	{
		$log_obj = $log_rows[$i];
		$MessageLogByParent[$log_obj["TargetID"]] = array("MessageTitle"=>$log_obj["MessageTitle"], "MessageContent"=>$log_obj["MessageContent"]);
		$MessageLogByMessageTargetID[$log_obj["NotifyMessageTargetID"]] = array("MessageTitle"=>$log_obj["MessageTitle"], "MessageContent"=>$log_obj["MessageContent"]);
	}
}

# check push message attachment if exist
$attachmentPath = "";

$sql = "SELECT AttachmentPath FROM INTRANET_APP_NOTIFY_MESSAGE_ATTACHMENT WHERE NotifyMessageID = '$NotifyMessageID'";
$messageAttachment = $li->returnResultSet($sql);

if (sizeof($messageAttachment) > 0)
{
    $attachmentPath = $eclassAppConfig['urlFilePath'].$messageAttachment[0]["AttachmentPath"];

    // [2020-0228-0904-26206] use full path
    //$attachmentPath = str_replace($intranet_root.'/', '', $attachmentPath);

    ### use relative path instead of curl
    //$attachmentPath = $PATH_WRT_ROOT . $attachmentPath;
    //$attachmentPath = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $attachmentPath);
    //$attachmentPath = curPageURL($withQueryString=false, $withPageSuffix=false).'/'.$attachmentPath;
}

foreach ($temp_result as $result_row) {
	$target_ids[] = $result_row['TargetID'];
}
$target_ids = implode(',', $target_ids);
// $target_ids = $NotifyMessage['TargetIDS'];

$messageTargetAry = $limc->getPushMessageTargetInfo($NotifyMessageID, $orderByName, $orderByClassNameClassNumber);
$messageRelatedToAry = $limc->getPushMessageRelatedToInfo($NotifyMessageID);
$messageRelatedToAssoAry = BuildMultiKeyAssoc($messageRelatedToAry, array('NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$messageReferenceAssoAry = BuildMultiKeyAssoc($limc->getPushMessageReferenceInfo($NotifyMessageID), array('NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);

$ExpBtn = $linterface->Get_Content_Tool_v30('export','javascript: Export();');

if ($target_ids != NULL)
{
	$recipient_html = '';
	if (count($messageRelatedToAssoAry) > 0)
	{
		### new logic: record the push message related to student in DB
		
		// status selection
		$statusSelection = $limc->getPushMessageStatusSelection('messageStatusSel', 'messageStatus', $messageStatus, "changedStatusSelection(this.value);", $NotifyMessageID);
		
		// pre-load user data
		$allUserIdAry = array_values(array_unique(array_merge(Get_Array_By_Key($messageTargetAry, 'TargetID'), Get_Array_By_Key($messageRelatedToAry, 'UserID'))));
		$objUser = new libuser('', '', $allUserIdAry);
		
		// get user last app login info
		$appLoginAssoAry = BuildMultiKeyAssoc($leClassApp->getUserLoginAppInfo($allUserIdAry), 'UserID', 'LastLoginTime');
		
		### DB table action buttons
		$btnAry = array();
		$btnAry[] = array('other', 'javascript: clickedResend();', $Lang['AppNotifyMessage']['Resend']);
		$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);
		
// 		$recipient_html .= $statusSelection."\n";
// 		$recipient_html .= "<br>\n";
		$recipient_html .= "<table ><tr>";
		$recipient_html .= "<td style=' border-bottom: 0px solid #ddd;'><div class='Conntent_tool'>".$statusSelection."</td></div>";
		$recipient_html .= "<td style=' border-bottom: 0px solid #ddd;'><div class='Conntent_tool'>";
		$recipient_html .= $ExpBtn."</div></td></tr></table>";
		$recipient_html .= $htmlAry['dbTableActionBtn'];
		$recipient_html .= '<table class="common_table_list view_table_list">'."\n";
			$recipient_html .= '<tr>'."\n";
				$recipient_html .= '<th style="width:3%;">#</th>'."\n";
				if ($appType == $eclassAppConfig['appType']['Parent']) {
					$recipient_html .= '<th style="width:20%;">'.$Lang['General']['Parent'].'</th>'."\n";
					$recipient_html .= '<th>'.$Lang['General']['Children'].'</th>'."\n";
				}
				else if ($appType == $eclassAppConfig['appType']['Teacher']) {
					$recipient_html .= '<th>'.$i_identity_teachstaff.'</th>'."\n";
				}
				else if ($appType == $eclassAppConfig['appType']['Student']) {
					$recipient_html .= '<th>'.$Lang['General']['Class'].'</th>'."\n";
					$recipient_html .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
					$recipient_html .= '<th>'.$Lang['Identity']['Student'].'</th>'."\n";
				}
				$recipient_html .= '<th style="width:15%;">'.$Lang['General']['Status'].'</th>'."\n";
				if (is_array($MessageLogByMessageTargetID) && count($MessageLogByMessageTargetID)>0) {
					$recipient_html .= "<th>".$Lang['AppNotifyMessage']['Description']."</th>";
				}
				$recipient_html .= '<th style="width:20%;">'.$Lang['AppNotifyMessage']['AppLastLoginTime'].'</th>';
				$recipient_html .= "<th width='1'>".$linterface->Get_Checkbox('messageTargetIdChk_global', 'messageTargetIdChk_global', $Value, $isChecked=0, $Class='', $Display='', $Onclick='Check_All_Options_By_Class(\'messageTargetIdChk\', this.checked);', $Disabled='')."</th>";
			$recipient_html .= '</tr>'."\n";
			
			$numOfTarget = count($messageTargetAry);
			$numOfDisplayRow = 0;
			for ($i=0; $i<$numOfTarget; $i++)
			{
				$_messageTargetId = $messageTargetAry[$i]['NotifyMessageTargetID'];
				$_targetUserId = $messageTargetAry[$i]['TargetID'];

				// get target user info
				$objUser->LoadUserData($_targetUserId);
				if ($objUser->UserID > 0) {
					$_englishName = $objUser->EnglishName;
					$_chineseName = $objUser->ChineseName;
					$_className = $objUser->ClassName;
					$_classNumber = $objUser->ClassNumber;
				}
				else {
					$sql = "SELECT EnglishName, ChineseName, ClassName, ClassNumber FROM INTRANET_ARCHIVE_USER WHERE UserID = '".$_targetUserId."'";
					$_userAry = $objUser->returnResultSet($sql);
					
					$_englishName = $_userAry[0]['EnglishName'];
					$_chineseName = $_userAry[0]['ChineseName'];
					$_className = $_userAry[0]['ClassName'];
					$_classNumber = $_userAry[0]['ClassNumber'];
				}
				//$_targetUserName = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
				$_targetUserName = Get_Lang_Selection($_chineseName, $_englishName);
				if ($appType == $eclassAppConfig['appType']['Student']) {
					//$_classNameDisplay = ($objUser->ClassName)? $objUser->ClassName : $Lang['General']['EmptySymbol'];
					//$_classNumberDisplay = ($objUser->ClassNumber)? $objUser->ClassNumber : $Lang['General']['EmptySymbol'];
					$_classNameDisplay = $_className? $_className: $Lang['General']['EmptySymbol'];
					$_classNumberDisplay = $_classNumber? $_classNumber: $Lang['General']['EmptySymbol'];
				}
				
				// get related user info
				$_relatedToAry = $messageRelatedToAssoAry[$_messageTargetId];
				$_numOfRelatedUser = count((array)$_relatedToAry);
				$_relatedUserDisplayAry = array();
				for ($j=0; $j<$_numOfRelatedUser; $j++)
				{
					$__relatedUserId = $_relatedToAry[$j]['UserID'];
					
					$objUser->LoadUserData($__relatedUserId);
					$__relatedUserDisplay = '';
					$__relatedUserDisplay .= Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
					if ($objUser->ClassName != '' && $objUser->ClassNumber != '') {
						$__relatedUserDisplay .= ' ('.$objUser->ClassName.' - '.$objUser->ClassNumber.')';
					}
					$_relatedUserDisplayAry[] = $__relatedUserDisplay;  
				}
				$_relatedUserDisplay = implode('<br>', $_relatedUserDisplayAry);
				
				// get message status
				$_messageReferenceAry = $messageReferenceAssoAry[$_messageTargetId];
				//if (is_array($_messageReferenceAry)) {
					$_messageReferenceAry = $messageReferenceAssoAry[$_messageTargetId];
					$_messageStatusAry = Get_Array_By_Key($_messageReferenceAry, 'MessageStatus');
					$_messageErrorCodeAry = Get_Array_By_Key($_messageReferenceAry, 'ErrorCode');
				    $_messageReturnMessageAry = Get_Array_By_Key($_messageReferenceAry, 'ReturnMessage');
				    $_messageReturnMessageAry = array_unique($_messageReturnMessageAry);
				    $_messageReturnMessageAry = array_filter($_messageReturnMessageAry, 'strlen');

					$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
					$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
					if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'], (array)$_messageStatusAry)) {
						$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'];
						$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'];
					}
					else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'], (array)$_messageStatusAry)) {
						// do nth
					}
					else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'], (array)$_messageStatusAry)) {
						$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'];
						$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'];
					}
					else {
						$_isAllFailed = true;
						$_numOfMessage = count($_messageStatusAry);
						for ($j=0; $j<$_numOfMessage; $j++) {
							$__status = $_messageStatusAry[$j];
							
							if ($__status != $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
								$_isAllFailed = false;
								break;
							}
						}
						
						if ($_isAllFailed) {
							if (in_array($eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'], $_messageErrorCodeAry) || in_array($eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer'], $_messageErrorCodeAry)) {
								$_messageStatus = $eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'];
							}
							else {
								$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
							}
							$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
						}
						else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'], (array)$_messageStatusAry)) {
							$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'];
							$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'];
						}
					}
					
					if ($messageStatus != -1 && $_filterStatus != $messageStatus) {
						continue;
					}
					$_messageStatusDisplay = $limc->getPushMessageStatusDisplay($_messageStatus);

					if(count($_messageReturnMessageAry) > 0) {
						//$_messageStatusDisplay .= ' ('.implode(',', $_messageReturnMessageAry).')';
					}
				//}
				
				// app last login time
				$_appLastLoginTime = $appLoginAssoAry[$_targetUserId]['LastLoginTime'];
				$_appLastLoginTime = ($_appLastLoginTime)? $_appLastLoginTime : $Lang['General']['EmptySymbol'];

				$recipient_html .= '<tr>'."\n";
					$recipient_html .= '<td>'.($numOfDisplayRow + 1).'</td>'."\n";
					if ($appType == $eclassAppConfig['appType']['Student']) {
						$recipient_html .= '<td>'.$_classNameDisplay.'</td>'."\n";
						$recipient_html .= '<td>'.$_classNumberDisplay.'</td>'."\n";
					}
					$recipient_html .= '<td>'.$_targetUserName.'</td>'."\n";
					if ($appType == $eclassAppConfig['appType']['Parent']) {
						$recipient_html .= '<td>'.$_relatedUserDisplay.'</td>'."\n";
					}
					//if (is_array($_messageReferenceAry)) {
						$recipient_html .= '<td>'.$_messageStatusDisplay.'</td>'."\n";
					//}
					
					if (isset($MessageLogByMessageTargetID[$_messageTargetId])) {
						$_messageTitle = $MessageLogByMessageTargetID[$_messageTargetId]['MessageTitle'];
						$_messageContent = $MessageLogByMessageTargetID[$_messageTargetId]['MessageContent'];
						
						$recipient_html .= '<td><b>'.$_messageTitle.'</b><br />'.nl2br($_messageContent).'</td>'."\n";
					}
					$recipient_html .= '<td>'.$_appLastLoginTime.'</td>'."\n";

					if ($_filterStatus == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
						$_checkbox = $linterface->Get_Checkbox('messageTargetIdChk_'.$_targetUserId, 'messageTargetIdAry[]', $_messageTargetId, $isChecked=0, $Class='messageTargetIdChk', $Display='', $Onclick='Uncheck_SelectAll(\'messageTargetIdChk_global\', this.checked);', $Disabled='');
					}
					else {
						$_checkbox = $Lang['General']['EmptySymbol'];
					}
					$recipient_html .= "<td width='1'>".$_checkbox."</td>";
				$recipient_html .= '</tr>'."\n";
				
				$numOfDisplayRow++;
			}
			
			if ($numOfDisplayRow == 0) {
				$recipient_html .= '<tr><td colspan="5" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			
		$recipient_html .= '</table>'."\n";
	}
	else
    {
		### old logic: on-the-fly find the parent student relationship
		$NameField = getNameFieldByLang("u.");
		$sql = "SELECT
		            DISTINCT u.UserLogin, $NameField as ParentName , u.UserID
				FROM 
				    INTRANET_USER as u 
				    INNER JOIN INTRANET_PARENTRELATION as p ON p.ParentID = u.UserID 
				    INNER JOIN INTRANET_USER as su ON su.UserID = p.StudentID AND su.RecordStatus = 1 AND su.RecordType = '".USERTYPE_STUDENT."' 
				WHERE
				    u.RecordStatus = 1 AND u.RecordType = '".USERTYPE_PARENT."' AND u.UserID IN ($target_ids) 
				GROUP BY u.UserID 
				ORDER BY ParentName ASC ";
		$rows = $li->returnArray($sql, null, true);
		if (sizeof($rows))
		{
			$recipient_html = "<table class=\"common_table_list view_table_list\" >";
			$recipient_html .= "<tr><th>".$Lang['General']['Parent']."</th><th>".$Lang['General']['Children']."</th>";
			if (is_array($MessageLogByParent) && sizeof($MessageLogByParent) > 0) {
				$recipient_html .= "<th width='60%'>".$Lang['AppNotifyMessage']['Description']."</th></tr>";
			}
		}

		// if (is_array($MessageLogByParent) && sizeof($MessageLogByParent)>0)
		// {
			for ($i=0; $i<sizeof($rows); $i++)
			{
				$parent_obj = $rows[$i];
				$recipient_html .= "<tr><td>".$parent_obj["ParentName"]."</td>";
				
				# get children info
				$thisParentID = $parent_obj["UserID"];
				$sql = "SELECT
							$NameField as ChildrenName,
							u.ClassName,
							u.ClassNumber
						FROM
							INTRANET_PARENTRELATION as p
						    INNER JOIN INTRANET_USER as u ON p.StudentID = u.UserID
						WHERE
							ParentID = $thisParentID
						ORDER BY
							ClassName, ClassNumber";
				$children = $li->returnArray($sql, null, true);
				
				$recipient_html .= "<td>";
				for ($j=0; $j<sizeof($children); $j++)
				{
					$thisChildrenName = $children[$j]["ChildrenName"];
					$thisClassName = $children[$j]["ClassName"];
					$thisClassNumber = $children[$j]["ClassNumber"];
					
					$recipient_html .= $thisClassName."(".$thisClassNumber.")&nbsp;".$thisChildrenName;
					if ($j < (sizeof($children) - 1)) {
						$recipient_html .= "</br>";
					}
				}
				$recipient_html .= "</td>";
				
				if (is_array($MessageLogByParent) && sizeof($MessageLogByParent) > 0) {
					$recipient_html .= "<td><b>".$MessageLogByParent[$parent_obj["UserID"]]["MessageTitle"]."</b><br />".$MessageLogByParent[$parent_obj["UserID"]]["MessageContent"]."</td></tr>\n";
				}
			}
		// } else
		// {
			// for ($i=0; $i<sizeof($rows); $i++)
			// {
				// $cell1 = $rows[$i]["ParentName"];
				// if ($rows[$i+1]!=null)
				// {
					// $i++;
					// $cell2 = $rows[$i]["ParentName"];
				// } else
				// {
					// $cell2 = "&nbsp;";
				// }
				// $recipient_html .= "<tr><td  width='40%'>{$cell1}</td><td  width='60%'>{$cell2}</td></tr>\n";
			// }
		// }
		
		if (sizeof($rows)) {
			$recipient_html .= "</table>";
		}
	}
}

if ($NotifyMessage['IsPublic'] == 'Y' && $recipient_html == "")
{
	$recipient_html = $publicLang;
}

# Page title
//$CurrentPage = "PageParentNotification";

//$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

//$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['ParentAppsNotify'], "javascript:history.back();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View']);

# Start layout
$returnMsg = $Lang['AppNotifyMessage']['ReturnMsg'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

function data_uri($file, $mime)
{
    $contents = file_get_contents($file);
    $base64 = base64_encode($contents);
    return ('data:' . $mime . ';base64,' . $base64);
}
?>

<script type="text/javascript">
function changedStatusSelection(targetStatus) {
	$('form#form1').attr('action', 'view_detail.php').submit();
}

function goBack() {
	$('form#form1').attr('action', 'index.php').submit();
}

function clickedResend() {
	var canSubmit = true;
	
	if ($('input#isSending').val() == 1) {
		alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['ProcessingMessageAlready']?>');
		canSubmit = false;
	}
	
	if ($('input.messageTargetIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
		canSubmit = false;
	}
	
	if (canSubmit && !confirm('<?=$Lang['AppNotifyMessage']['WarningMsg']['ResendPushMessage']?>')) {
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('input#isSending').val(1);
		$('form#form1').attr('action', 'resend_message.php').submit();
	}
}

function Export(){
	$('form#form1').attr('action', 'export.php').submit();
}
</script>

<form name="form1" id="form1" method="post" action="view_detail.php">
	<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
	<p class="spacer"></p>
	
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title" style="width:20%;"><?=$Lang['AppNotifyMessage']['SendTime']?></td>
				<td><?=$NotifyMessage['NotifyDateTime']?></td>
			</tr>
			<tr>
				<td class="field_title" style="width:20%;"><?=$Lang['AppNotifyMessage']['Title']?></td>
				<td><?=htmlspecialchars($NotifyMessage['MessageTitle'])?></td>
			</tr>
			<tr>
				<td class="field_title" style="width:20%;"><?=$Lang['AppNotifyMessage']['Description']?></td>
				<td><?=nl2br(htmlspecialchars($NotifyMessage['MessageContent']))?></td>
			</tr>
			<tr>
				<td class="field_title" style="width:20%;"><?=$Lang['General']['CreatedBy']?></td>
				<td><?=$NotifyMessage['NotifyUser']?></td>
			</tr>
            <? if ($attachmentPath != "") { ?>
                <tr>
                    <td class="field_title" style="width:20%;"><?=$Lang['AppNotifyMessage']['AttachImage']?></td>
                    <td><img src="<?=data_uri($attachmentPath, 'image/png')?>" alt="" /></td>
                </tr>
            <? } ?>
			<tr>
				<td class="field_title" style="width:20%;"><?=$Lang['AppNotifyMessage']['Recipients']?></td>
				<td><?= $recipient_html ?></td>
			</tr>
		</table>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()")?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<input type=hidden name=NotifyMessageID value="<?=$NotifyMessageID?>">
	<input type=hidden name=appType value="<?=$appType?>">
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
	<input type="hidden" name="order" id="order" value="<?=$order?>" />
	<input type="hidden" name="field" id="field" value="<?=$field?>" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$page_size?>" />
	<input type="hidden" name="isSending" id="isSending" value="" />
</form>

<?php
intranet_closedb();

$linterface->LAYOUT_STOP();
?>