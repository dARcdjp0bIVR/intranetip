<?php
# using: 

#################################
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT.'includes/liblog.php');
	
//	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
//	{
//		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//		$laccessright = new libaccessright();
//		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//		exit;
//	}

	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($intranet_root.'/includes/json.php');
	
	
	intranet_auth();
	intranet_opendb();
	
	$appType = $_POST['appType'];
	
	$limc = new libmessagecenter();
	$limc->checkNotifyMessageAccessRight($appType);
	
	

$li = new libdb();
$jsonObj = new JSON_obj();
$list =implode(",", $NotifyMessageID);

// delete schedule push message in central server
$sql = "Select NotifyMessageID, ToServer From INTRANET_APP_NOTIFY_MESSAGE WHERE NotifyMessageID IN ($list) And SendTimeMode = '".$eclassAppConfig['pushMessageSendMode']['scheduled']."'";
$scheduledMessageAssoAry = BuildMultiKeyAssoc($li->returnResultSet($sql), 'ToServer', array('NotifyMessageID'), $SingleValue=1, $BuildNumericArray=1);


$canDelete = true;
foreach ((array)$scheduledMessageAssoAry as $_toServer => $_notifyMessageIdAry) {
	$postParamAry = array();
	$postParamAry['RequestMethod'] = 'cancelScheduledPushMessage';
	$postParamAry['SchoolCode'] = $config_school_code;
	$postParamAry['NotifyMessageIDList'] = implode(',', $_notifyMessageIdAry);
	$postJsonString = $jsonObj->encode($postParamAry);
	
	
	$headers = array('Content-Type: application/json');
	
	session_write_close();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_URL, $_toServer);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
	$responseJson = curl_exec($ch);
	curl_close($ch);
	
	$responseJson = standardizeFormPostValue($responseJson);
	$responseAry = $jsonObj->decode($responseJson);
	
	$SuccessArr['DeleteFromCentralServer'] = ($responseAry['MethodResult']['response']=='ACK')? true : false;
	$canDelete = $SuccessArr['DeleteFromCentralServer'];
}



if ($canDelete) {
	//$sql = "DELETE FROM INTRANET_APP_NOTIFY_MESSAGE WHERE NotifyMessageID IN ($list)";
	$sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE SET RecordStatus = 0, ShowInApp = 0 WHERE NotifyMessageID IN ($list)";
	$success = $li->db_db_query($sql);
	
	# insert delete log
	$liblog = new liblog();
	$tmpAry = array();
	$tmpAry['NotifyMessageID'] = $list;
	$SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('MessageCenter', 'Delete_Parent_Push_Notification', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_APP_NOTIFY_MESSAGE');
}



//$sql = "DELETE FROM INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageID IN ($list)";
//$target_success = $li->db_db_query($sql);

//$msg = ($success && $target_success)?$Lang['AppNotifyMessage']['ReturnMsg']['RemoveSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['RemoveFail'];
$msg = ($success )?$Lang['AppNotifyMessage']['ReturnMsg']['RemoveSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['RemoveFail'];

intranet_closedb();
header("Location: index.php?appType=$appType&filter=$filter&order=$order&field=$field&msg=$msg");
	
?>