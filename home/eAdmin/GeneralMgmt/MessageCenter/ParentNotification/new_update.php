<?php
# using: 

#################################
#
#	to show parents using parent apps only
#	Date:	2019-09-24 	(Philips) [2019-0917-1549-28207]
#			added attach image file checking
#
#	Date:	2016-10-06 Villa[K104806]
#			Return the msg in the header with the form of english in stead of b5
#	Date:	2013-12-13 	Roy
#			fix bug in $ASLParam of IsPublic==Y case
#	Date:	2013-10-09 	Roy
#			get parent login name data for IsPublic = Y
#	Date:	2013-05-10	Yuen
#			support CSV import
#	Date:	2012-03-21	Carlos
#			set SchoolID with $asl_parent_app['SchoolID'](which is school server domain, because IP address does not work)
#	Date:	2011-12-28	Carlos
#			join table INTRANET_API_REQUEST_LOG for group selection to select parents that have purchased parent app
#
#	Date:	2011-09-02	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
	include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	
	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	
	intranet_auth();
	intranet_opendb();
	
	
$li = new libdb();
$libimport = new libimporttext();

$libuser = new libuser($UserID);
$notifyuser = $libuser->UserNameLang();

$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);


$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
$MsgNotifyDateTime = date("Y-m-d H:i:s");

### check image file format
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
$lfs = new libfilesystem();
$filename = $_FILES["attachedImage"]["name"];
$tmpfilepath = $_FILES["attachedImage"]["tmp_name"];
$fileext = $lfs->file_ext($filename);
if($_FILES["attachedImage"]["name"] && !(strtolower($fileext) == ".jpg" || strtolower($fileext) == ".gif" || strtolower($fileext) == ".png")){
	header('Location: index.php?appType='.$appType.'&msg=imageFail');
	die();
}

if ($IsCSV)
{
	$MsgTitle = intranet_htmlspecialchars(trim($MsgTitle));
	$MsgTitle = str_replace(array('<', '>', ';', 'javascript:', '"'),'',$MsgTitle);
	$notifyfor = ""; //"Parents";

	$sql = "SELECT * FROM TEMPSTORE_PUSHMESSAGE_FILE_USER where UserID='$UserID'";
	$push_messages = $li->returnResultSet($sql);
	
	$NotifyMessageID = -1;
	
	for ($i=0; $i<sizeof($push_messages); $i++)
	{
		$msg_row = $push_messages[$i];
		$msg_row_title = intranet_htmlspecialchars(trim(addslashes($msg_row["MessageTitle"])));
		$msg_row_message = intranet_htmlspecialchars(trim(addslashes($msg_row["Message"])));

		$ASLParam = array(
					"SchID" => $SchoolID,
					"MsgNotifyDateTime" => $MsgNotifyDateTime,
					"MessageTitle" => $msg_row_title,
					"MessageContent" => $msg_row_message,
					"NotifyFor" => $notifyfor,
					"IsPublic" => "N",
					"NotifyUser" => $notifyuser,
					"TargetUsers" => array(
						"TargetUser" => array($msg_row["UserLogin"])
					)
		);
		//debug_r($ASLParam);
		$ReturnMessage = "";
		$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);

		$debug_log = $sys_custom['ASLParentAppDebugLog'];
		if ($debug_log)
		{
			$log_file = $file_path."/file/asl_app_test.txt";
			$file_handle = fopen($log_file,"a+");
			fwrite($file_handle,$asl->postData."\n".$Result."\n".$ReturnMessage."\n\n");
			fclose($file_handle);
		}
		//$Result = 1;
		
		if ($Result == 1 && $NotifyMessageID==-1)
		{
			$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
					VALUES ('$MsgNotifyDateTime','".$MsgTitle."','MULTIPLE MESSAGES','".$notifyfor."','N','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
		
			$success = $li->db_db_query($sql);
			$NotifyMessageID = $li->db_insert_id();
		}
			
		if ($Result == 1 && $NotifyMessageID>0)
		{
				
			$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,MessageTitle,MessageContent,DateInput)
					VALUES ('$NotifyMessageID','U','".$msg_row["ParentID"]."','{$msg_row_title}','{$msg_row_message}',NOW())";
			$target_success = $li->db_db_query($sql);
		}

	}
	
	$sql = "DELETE FROM TEMPSTORE_PUSHMESSAGE_FILE_USER where UserID='$UserID'";
	$li->db_db_query($sql);
} else
{

# send for the parents selected using UI
	$Title = $MsgTitle;
	$title = intranet_htmlspecialchars(trim($Title));
	$message = intranet_htmlspecialchars(trim($Message));
	$notifyfor = intranet_htmlspecialchars(trim($NotifyFor));
	// store current user
	
	
	$titleUTF8 = $title;
	$messageUTF8 = $message;
	$notifyforUTF8 = $notifyfor;
	$notifyuserUTF8 = $notifyuser;
	
	$recipient_group_id = array();
	$recipient_parent_id = array();
	$merge_recipient_parent_id = array();
	
	//$SchoolID = $_SERVER['SERVER_NAME'];
	//$SchoolID = "junior20-b5-demo2.eclass.com.hk";
	
	if ($IsPublic == 'N')
	{
		for($i=0;$i<sizeof($Recipient);++$i)
		{
			if(trim(str_replace('&nbsp;','',$Recipient[$i])) == '') continue;
			$RecipientType = substr($Recipient[$i],0,1);
		    $RecipientID = substr($Recipient[$i],1);
		    if($RecipientID == 0) continue;
		    if($RecipientType == 'G'){
		    	$recipient_group_id[] = $RecipientID;
		    }elseif($RecipientType == 'U'){
		    	$recipient_parent_id[] = $RecipientID;
		    }
		}
		//debug_pr(array('recipient_group_id',$recipient_group_id));
		//debug_pr(array('recipient_parent_id',$recipient_parent_id));
		if(count($recipient_group_id)>0){
			$group_id_csv = "'".implode("','",$recipient_group_id)."'";
			//$sql = "SELECT DISTINCT u.UserID FROM INTRANET_USER as u INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID = u.UserID WHERE ug.GroupID IN ($group_id_csv) AND u.RecordStatus = 1 AND u.RecordType = '".USERTYPE_PARENT."' ";
			$sql = "SELECT DISTINCT u.UserID 
					 FROM INTRANET_USER as u 
					 INNER JOIN INTRANET_PARENTRELATION as p ON p.ParentID = u.UserID 
					 INNER JOIN INTRANET_USER as su ON su.UserID = p.StudentID AND su.RecordStatus = 1 AND su.RecordType = '".USERTYPE_STUDENT."' 
					 INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID = p.StudentID 
					 INNER JOIN INTRANET_API_REQUEST_LOG as r ON (r.UserID = p.ParentID OR r.UserID = p.StudentID) 
					 WHERE u.RecordStatus = 1 AND u.RecordType = '".USERTYPE_PARENT."' AND ug.GroupID IN ($group_id_csv) 
					 ";
			$group_userid_arr = $li->returnVector($sql);
			//debug_pr(array('group_userid_arr',$group_userid_arr));
			for($i=0;$i<sizeof($group_userid_arr);++$i){
				$merge_recipient_parent_id[] = $group_userid_arr[$i];
			}
		}
		if(count($recipient_parent_id)>0){
			for($i=0;$i<sizeof($recipient_parent_id);++$i){
				$merge_recipient_parent_id[] = $recipient_parent_id[$i];
			}
		}
		//debug_pr(array('merge_recipient_parent_id',$merge_recipient_parent_id));
		$merge_recipient_parent_id = array_values(array_unique($merge_recipient_parent_id));
		
		if(count($merge_recipient_parent_id)==0){
			intranet_closedb();
			header("Location: ./index.php?msg=".$Lang['AppNotifyMessage']['ReturnMsg']['SendFail']);
			exit;
		}
		
		$sql = "SELECT DISTINCT u.UserLogin FROM INTRANET_USER as u WHERE u.UserID IN ('".implode("','",$merge_recipient_parent_id)."')";
		$recipient_parent_userlogin = $li->returnVector($sql);
		//debug_pr(array('recipient_parent_userlogin',$recipient_parent_userlogin));
		$TargetUsers = array();
		for($i=0;$i<sizeof($recipient_parent_userlogin);++$i){
			$TargetUsers[] = $recipient_parent_userlogin[$i];
		}
		//debug_pr(array('TargetUsers',$TargetUsers));
		//AWS_NotifyMessage
		$ASLParam = array(
						"SchID" => $SchoolID,
						"MsgNotifyDateTime" => $MsgNotifyDateTime,
						"MessageTitle" => $titleUTF8,
						"MessageContent" => $messageUTF8,
						"NotifyFor" => $notifyforUTF8,
						"IsPublic" => "N",
						"NotifyUser" => $notifyuserUTF8,
						"TargetUsers" => array(
							"TargetUser" => $TargetUsers
						)
		);
		// debug_pr($ASLParam);
	} else
	{ // IsPublic == Y
		//AWS_NotifyMessage
		
		# get all parents login name
		$ParentArr = $libuser->getParent($StudentIDStr="", $sortByClass=1, $filterAppParent=1, $UserRecordStatus=1, $getAllParent=1);
		// debug_pr($ParentArr);	
		$thisParentLogin = array();
		foreach ((array)$ParentArr as $ParentInfo) {
			$thisParentID = $ParentInfo['ParentID'];
			$thisParentLogin[] = $libuser->user_login($thisParentID);
		}
		$thisParentLogin = array_values(array_unique($thisParentLogin));
		// debug_pr($thisParentLogin);
		$ASLParam = array(
						"SchID" => $SchoolID,
						"MsgNotifyDateTime" => $MsgNotifyDateTime,
						"MessageTitle" => $titleUTF8,
						"MessageContent" => $messageUTF8,
						"NotifyFor" => $notifyforUTF8,
						"IsPublic" => "Y",
						"NotifyUser" => $notifyuserUTF8,
						"TargetUsers" => array(
							"TargetUser" => $thisParentLogin
						)
		);
		// debug_pr($ASLParam);
	}
	$ReturnMessage = "";
	$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
	
	$debug_log = $sys_custom['ASLParentAppDebugLog'];
	if ($debug_log)
	{
		$log_file = $file_path."/file/asl_app_test.txt";
		$file_handle = fopen($log_file,"a+");
		fwrite($file_handle,$asl->postData."\n".$Result."\n".$ReturnMessage."\n\n");
		fclose($file_handle);
	}
	//debug_r($ASLParam);
	
	if ($Result == 1)
	{
		$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
				VALUES ('$MsgNotifyDateTime','".$Title."','".$Message."','".$NotifyFor."','$IsPublic','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
	
		$success = $li->db_db_query($sql);
		
		if($success && $IsPublic == 'N' && count($merge_recipient_parent_id)>0)
		{
			$NotifyMessageID = $li->db_insert_id();
			$values = array();
			//for($i=0;$i<count($recipient_group_id);++$i){
			//	$values[] = "('$NotifyMessageID','G','".$recipient_group_id[$i]."',NOW())";
			//}
			for($i=0;$i<count($merge_recipient_parent_id);++$i){
				$values[] = "('$NotifyMessageID','U','".$merge_recipient_parent_id[$i]."',NOW())";
			}
			
			$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,DateInput)
					VALUES ".implode(",",$values);
			$target_success = $li->db_db_query($sql);
		}
	}
		
}


// $msg = ($Result==1)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
//Villa [K104806] 2016-10-06 Avoid Display problem of B5 in the hyper link
$msg = ($Result==1)? 'success':'fail';

	
intranet_closedb();


header("Location: ./index.php?msg=$msg");
?>