<?php

/////////////////// SQL Safe START ///////////////////
$groupID = IntegerSafe($GroupCheckBox);
/////////////////// SQL Safe END ///////////////////


//////////////////// Access Right START ////////////////////
/**/
if($groupID){
	foreach((array)$groupID as $gid){
		if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight($gid)){
			header('Location: /');
			exit;
		}
	}
}else{
	if(!libMsschPrintingAccessRight::isSuperAdmin()){
		header('Location: /');
		exit;
	}
}
/** /
if(!libMsschPrintingAccessRight::isSuperAdmin()){
	header('Location: /');
	exit;
}
/**/
//////////////////// Access Right END ////////////////////


/////////////////// INIT START ///////////////////
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
if(empty($groupID)){
	$isValid = false;
	$result[] = false;
}
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if($isValid){
	foreach($groupID as $gid){
		$objPRINTING_GROUP = new PRINTING_GROUP($gid);
		$result[] = $objPRINTING_GROUP->delete();
//		$result[] = PRINTING_GROUP_MEMBER::deleteAllPic($gid); # Don't delete PIC, just for debugging.
	}
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];}
header('Location: ?t=setting.group&Msg='.$Msg);
exit;
?>