<?php

/////////////////// SQL Safe START ///////////////////
$subjectGroupID = IntegerSafe($subjectGroupID);
/////////////////// SQL Safe END ///////////////////


//////////////////// Access Right START ////////////////////
//	if(!libMsschPrintingAccessRight::isSuperAdmin()){
if (!libMsschPrintingAccessRight::checkGroupSettingAccessRight()) {
    header('Location: /');
    exit;
}
//////////////////// Access Right END ////////////////////


/////////////////// INIT START ///////////////////
$allMfpCode = $objPrinting->getAllGroupMfpCode($AcademicYearID = Get_Current_Academic_Year_ID());
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if ($isValid) {
    foreach ($subjectGroupID as $sgId) {
        $stc = new subject_term_class($sgId, true, true);

        $mfpCode = $stc->ClassCode;
        $groupName = Get_Lang_Selection($stc->ClassTitleB5, $stc->ClassTitleEN);
        $PIC = Get_Array_By_Key($stc->ClassTeacherList, 'UserID');
        $Member = Get_Array_By_Key($stc->ClassStudentList, 'UserID');

        #### Check MFP Code duplicate START ####
        $index = 2;
        $_groupName = $groupName;
        $_mfpCode = $mfpCode;
        while (in_array($_mfpCode, $allMfpCode)) {
            $_mfpCode = "{$mfpCode}_{$index}";
            $_groupName = "{$groupName}_{$index}";
            $index++;
        }
        $mfpCode = $_mfpCode;
        $groupName = $_groupName;
        #### Check MFP Code duplicate END ####

        $objPRINTING_GROUP = new PRINTING_GROUP(0);
        $objPRINTING_GROUP->setAcademicYearID(Get_Current_Academic_Year_ID());
        $objPRINTING_GROUP->setMFP_CODE($mfpCode);
        $objPRINTING_GROUP->setNAME($groupName);
        $objPRINTING_GROUP->setSUBJECT_GROUP_ID($sgId);
        $result[] = $objPRINTING_GROUP->save();

        $groupID = $objPRINTING_GROUP->getGROUP_ID();
        $result[] = PRINTING_GROUP_MEMBER::insertMember($groupID, $PIC, $cfg_msschPrint['group']['memberType']['PIC']);
        $result[] = PRINTING_GROUP_MEMBER::insertMember($groupID, $Member, $cfg_msschPrint['group']['memberType']['Member']);
    }
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if (in_array(false, $result)) {
    $Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
} else {
    $Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
}
header('Location: ?t=setting.group&Msg=' . $Msg);
exit;
