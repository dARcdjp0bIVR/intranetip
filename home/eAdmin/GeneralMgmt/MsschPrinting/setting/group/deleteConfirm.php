<?php

///////////// SQL Safe START /////////////
$AcademicYearFilter = IntegerSafe($AcademicYearFilter);
$GroupCheckBox = IntegerSafe($GroupCheckBox);
///////////// SQL Safe END /////////////


//////////////////// Access Right START ////////////////////
/**/
if($GroupCheckBox){
	foreach((array)$GroupCheckBox as $gid){
		if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight($gid)){
			header('Location: /');
			exit;
		}
	}
}else{
	if(!libMsschPrintingAccessRight::isSuperAdmin()){
		header('Location: /');
		exit;
	}
}
/**/
/*if(!libMsschPrintingAccessRight::isSuperAdmin()){
	header('Location: /');
	exit;
}*/
//////////////////// Access Right END ////////////////////


///////////// Init START /////////////
$deletePage = '?t=setting.group.delete';
$backPage = '?t=setting.group';

$GroupCheckBoxSQL = implode("','", (array)$GroupCheckBox);
///////////// Init END /////////////


///////////// Get printing record group START /////////////
$sql = "SELECT 
	PG.MFP_CODE,
	PG.GROUP_ID,
	PG.NAME,
	SUM(TOTAL_AMOUNT) AS AMOUNT,
	COUNT(PGM.GROUP_MEMBER_ID) AS MEMBER_COUNT
FROM 
	PRINTING_GROUP PG
LEFT JOIN
	PRINTING_GROUP_MEMBER PGM
ON 
	PG.GROUP_ID = PGM.GROUP_ID
AND
	PGM.MEMBER_TYPE = '{$cfg_msschPrint['group']['memberType']['Member']}'
LEFT JOIN
	PRINTING_GROUP_USAGE PGU
ON
	PG.GROUP_ID = PGU.GROUP_ID
GROUP BY
	PG.GROUP_ID
HAVING
	PG.GROUP_ID IN ('{$GroupCheckBoxSQL}')
ORDER BY
	PG.MFP_CODE";
$groupInfo = $objDB->returnResultSet($sql);

$cannotDeleteGroupID = array();
foreach($groupInfo as $group){
	if($group['AMOUNT']){
		$cannotDeleteGroupID[] = $group['GROUP_ID'];
	}
}
///////////// Get printing record group END /////////////


///////////// UI START /////////////
$CurrentPage = "SettingGroup";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['group'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>

<form name="form1" action="<?=$deletePage?>" method="POST">

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext">
			<?php if(count($cannotDeleteGroupID)){ ?>
				<span><?=$Lang['MsschPrint']['setting']['group']['deleteHint']['groupContainsData']?></span>
			<?php }else{ ?>
				<span><?=$Lang['MsschPrint']['setting']['group']['deleteHint']['groupNoContainsData']?></span>
			<?php } ?>
		</td>
	</tr>
</table>

<table width="96%" cellspacing="0" cellpadding="4" border="0" align="center">
	<tr class="tabletop">
		<td class="tabletoplink"><?=$Lang['MsschPrint']['setting']['group']['tableHeader']['mfpCode']?></td>
		<td class="tabletoplink"><?=$Lang['MsschPrint']['setting']['group']['tableHeader']['name']?></td>
		<td class="tabletoplink"><?=$Lang['MsschPrint']['setting']['group']['tableHeader']['countMember']?></td>
		<td class="tabletoplink"><?=$Lang['MsschPrint']['setting']['group']['delete']['printingAmount']?></td>
	</tr>
	<?php foreach($groupInfo as $index=>$group){ ?>
		<tr class="tablerow<?= $index % 2 + 1 ?> tabletext">
			<td class"tabletext"><?=$group['MFP_CODE']?></td>
			<td class"tabletext"><?=$group['NAME']?></td>
			<td class"tabletext"><?=$group['MEMBER_COUNT']?></td>
			<?php if($group['AMOUNT']){ ?>
				<td class"tabletext" style="color:red"><?=$group['AMOUNT']?></td>
			<?php }else{ ?>
				<td class"tabletext">0.00</td>
			<?php }?>
		</tr>
	<?php } ?>
</table>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
    	<td align="center">
    		<br />
			<?php 
			if(count($cannotDeleteGroupID) == 0){ 
				echo $linterface->GET_ACTION_BTN($button_remove, "button", "document.form1.submit()") . '&nbsp;';
			}
			echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='{$backPage}'");
			?>
    	</td>
    </tr>
</table>
<?php foreach($GroupCheckBox as $groupID){ ?>
	<input type="hidden" name="GroupCheckBox[]" value="<?=$groupID?>" />
<?php } ?>
</form>

<?php $linterface->LAYOUT_STOP(); ?>