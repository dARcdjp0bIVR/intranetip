<?php
/**
 *  Change Log:
 *  2019-07-02 Pun
 *   - File created
 */

//////////////////// SQL Safe START ////////////////////
$groupID = IntegerSafe($GroupCheckBox[0]);
//////////////////// SQL Safe END ////////////////////


//////////////////// Access Right START ////////////////////
if ($groupID) { // Edit Group
    if (!libMsschPrintingAccessRight::checkGroupSettingAccessRight($groupID)) {
        header('Location: /');
        exit;
    }
} else { // New Group
//	if(!libMsschPrintingAccessRight::isSuperAdmin()){
    if (!libMsschPrintingAccessRight::checkGroupSettingAccessRight()) {
        header('Location: /');
        exit;
    }
}
//////////////////// Access Right END ////////////////////


//////////////////// INIT START ////////////////////
$parentPagePath = 'setting.group';
$savePagePath = 'setting.group.addFromSubjectGroupSave';
//////////////////// INIT END ////////////////////

//////////////////// UI Field START ////////////////////
$academicYearHTML = getSelectAcademicYear('AcademicYearID', '', 1, 0);

//////////////////// UI Field END ////////////////////

//////////////////// UI START ////////////////////
$CurrentPage = "SettingGroup";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['group'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['MsschPrint']['menu']['group'], "?t={$parentPagePath}");
$PAGE_NAVIGATION[] = array(($groupID) ? $Lang['MsschPrint']['setting']['group']['editGroup'] : $Lang['MsschPrint']['setting']['group']['newGroup'], '');
$linterface->LAYOUT_START($Msg);
$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
    <style type="text/css">
        .subjectName {
            margin-top: 1rem;
            font-weight: bold;
        }

        .subjectGroup {
            display: inline-block;
            min-width: 200px;
        }
    </style>


    <div class="table_board">
        <?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

        <form id="form1" name="form1" method="post" action="index.php">
            <table class="form_table_v30">

                <tr>
                    <td class="field_title"><?= $linterface->RequiredSymbol() ?><?= $Lang['General']['AcademicYear'] ?></td>
                    <td>
                        <?= $academicYearHTML ?>
                    </td>
                </tr>

                <tr>
                    <td class="field_title"><?= $linterface->RequiredSymbol() ?><?= $Lang['SysMgr']['SubjectClassMapping']['Term'] ?></td>
                    <td>
                        <div id="loadingTerm">
                            <?= $linterface->Get_Ajax_Loading_Image(); ?>
                        </div>
                        <div id="termResult"></div>
                    </td>
                </tr>

                <tr>
                    <td class="field_title"><?= $linterface->RequiredSymbol() ?><?= $Lang['SysMgr']['SubjectClassMapping']['Group'] ?></td>
                    <td>
                        <div id="loadingSubjectGroup">
                            <?= $linterface->Get_Ajax_Loading_Image(); ?>
                        </div>
                        <div id="subjectGroupResult"></div>
                    </td>
                </tr>

            </table>

            <?= $linterface->MandatoryField(); ?>

            <div class="edit_bottom_v30">
                <p class="spacer"></p>
                <?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "submitForm") ?>
                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?t={$parentPagePath}'", "cancelbtn") ?>
                <p class="spacer"></p>
            </div>

            <input type="hidden" name="groupID" value="<?= $groupID ?>"/>
        </form>

    </div>


    <!-------------- Script START -------------->
    <script>
        $('#AcademicYearID').change(function () {
            var academicYearId = $(this).val();

            $('#termResult, #subjectGroupResult').html('');
            $('#loadingTerm, #loadingSubjectGroup').show();

            $.get('index.php', {
                't': 'setting.group.ajax.getAcademicYearTermSelection',
                'AcademicYearID': academicYearId
            }, function (res) {
                $('#termResult').html(res);
                $('#loadingTerm').hide();

                $('#termResult').find('#TermID').change(function () {
                    getSubjectGroupHTML($(this).val());
                }).change();
            });
        }).change();

        function getSubjectGroupHTML(termId) {
            $('#subjectGroupResult').html('');
            $('#loadingSubjectGroup').show();

            $.get('index.php', {
                't': 'setting.group.ajax.getSubjectGroupCheckbox',
                'TermID': termId
            }, function (res) {
                $('#subjectGroupResult').html(res);
                $('#loadingSubjectGroup').hide();

                $('#subjectGroupResult').find('.checkAllSubject').click(function () {
                    checkAllSubjectGroup($(this).attr('data-target'), $(this).attr('checked'));
                });
            });
        }

        function checkAllSubjectGroup(targetId, checked) {
            if (checked) {
                $('#subjectGroupResult').find(targetId).find('input[type="checkbox"]').attr('checked', true);
            } else {
                $('#subjectGroupResult').find(targetId).find('input[type="checkbox"]').removeAttr('checked');
            }
        }


        var isSubmitting = false;
        $('#submitForm').click(function() {
            if (isSubmitting) {
                return false;
            }

            if($('[name="subjectGroupID[]"]:checked').length == 0){
                alert('<?=$Lang['General']['WarningArr']['PleaseSelectAtLeastOneSubjectGroup']?>');
                return false;
            }

            isSubmitting = true;
            $('#form1').attr('action','?t=<?=$savePagePath?>').submit();
        });
    </script>
    <!-------------- Script END -------------->


<?php $linterface->LAYOUT_STOP(); ?>