<?php

//////////////////// SQL Safe START ////////////////////
$TermID = IntegerSafe($TermID);
//////////////////// SQL Safe END ////////////////////

//////////////////// INIT START ////////////////////
$subject_class_mapping = new subject_class_mapping();
$subjectArr = $subject_class_mapping->Get_Subject_List();
$rs = $subject_class_mapping->Get_Subject_Group_List($TermID, '', false);
$subjectGroupArr = BuildMultiKeyAssoc($rs, array('SubjectID', 'SubjectGroupID'), array('ClassTitleB5', 'ClassTitleEN'));
//////////////////// INIT END ////////////////////

//////////////////// Access Right START ////////////////////
if (!libMsschPrintingAccessRight::checkGroupSettingAccessRight()) {
    header('Location: /');
    exit;
}
//////////////////// Access Right END ////////////////////


//debug_rt($subjectArr);
//debug_r(($subjectGroupArr[8]));

foreach ($subjectArr as $subjectInfo) {
    $subjectId = $subjectInfo['SubjectID'];
    $subjectName = Get_Lang_Selection($subjectInfo['SubjectDescB5'], $subjectInfo['SubjectDescEN']);

    if (empty($subjectGroupArr[$subjectId])) {
        continue;
    }

    ?>
    <div class="subjectName">
        <input
                type="checkbox"
                id="checkAllSubject_<?= $subjectId ?>"
                class="checkAllSubject"
                data-target="#subjectGroups_<?= $subjectId ?>"
        />
        <label for="checkAllSubject_<?= $subjectId ?>"><?= $subjectName ?></label>
    </div>

    <div id="subjectGroups_<?= $subjectId ?>">
        <?php
        foreach ($subjectGroupArr[$subjectId] as $subjectGroupId => $subjectGroupInfo) {
            $subjectGroupName = Get_Lang_Selection($subjectGroupInfo['ClassTitleB5'], $subjectGroupInfo['ClassTitleEN']);
            ?>
            <div class="subjectGroup">
                <input
                        type="checkbox"
                        id="sg_<?= $subjectGroupId ?>"
                        name="subjectGroupID[]"
                        value="<?= $subjectGroupId ?>"
                />
                <label for="sg_<?= $subjectGroupId ?>"><?= $subjectGroupName ?></label>
            </div>
            <?php
        }
        ?>

    </div>

    <?php
}