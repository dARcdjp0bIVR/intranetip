<?php

//////////////////// SQL Safe START ////////////////////
$groupID = IntegerSafe($groupID);
$mfpCode = trim( htmlentities( $mfpCode , ENT_QUOTES, 'UTF-8'));
//////////////////// SQL Safe END ////////////////////

//////////////////// INIT START ////////////////////
$objPrintingGroup = new PRINTING_GROUP($groupID);
$oldMfpCode = $objPrintingGroup->getMFP_CODE();
//////////////////// INIT END ////////////////////

//////////////////// Access Right START ////////////////////
if($groupID){ // Edit Group
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight($groupID)){
		header('Location: /');
		exit;
	}
}else{ // New Group
//	if(!libMsschPrintingAccessRight::isSuperAdmin()){
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight()){
		header('Location: /');
		exit;
	}
}
//////////////////// Access Right END ////////////////////



//////////////////// Checking START ////////////////////
$allMfpCode = $objPrinting->getAllGroupMfpCode($AcademicYearID = Get_Current_Academic_Year_ID());
if( 
	$mfpCode != '' &&
	$mfpCode != $oldMfpCode &&
	in_array($mfpCode, $allMfpCode)
){
	echo '0';
}else{
	echo '1';
}
?>