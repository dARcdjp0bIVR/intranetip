<?php

/**
 * Input{
 * 	SelectionID: The HTML name of selection box
 * 	SelectedUserType:
 * 		TeachingStaff
 * 		NonTeachingStaff
 * 		Student
 * 	YearClassIDArr(optional): The YearClassID array for UserType='Student'
 * }
 */


///////////////// Access Right START /////////////////
///////////////// Access Right END /////////////////


///////////// SQL Safe START /////////////
$YearClassIDArr = IntegerSafe($YearClassIDArr);
///////////// SQL Safe END /////////////


///////////// Validate START /////////////
///////////// Validate END /////////////


///////////// Init START /////////////
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
$form_class_manage_ui = new form_class_manage_ui();

if(!is_array($YearClassIDArr)){
	$YearClassIDArr = (array)$YearClassIDArr;
}
///////////// Init END /////////////

///////////// UI START /////////////
switch($SelectedUserType){
	case 'TeachingStaff':
		echo $form_class_manage_ui->Get_Staff_Selection($SelectionID, $isTeachingStaff=1, $SelectedUserID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $ParID='', $ParClass='');
		break;
	case 'NonTeachingStaff':
		echo $form_class_manage_ui->Get_Staff_Selection($SelectionID, $isTeachingStaff=0, $SelectedUserID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $ParID='', $ParClass='');
		break;
	case 'Student':
		echo $form_class_manage_ui->Get_Student_Selection($SelectionID, (array)$YearClassIDArr, $SelectedStudentID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $ExcludedStudentIDArr='', $IncludeStudentIDArr='');
		break;
}


?>