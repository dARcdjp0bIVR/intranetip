<?php

/**
 * Input{
 * 		SelectionID: The HTML name of selection box
 * 		AcademicYearID(optional): The AcademicYearID of the class, default current year.
 * 		FormID(optional): The formID of the class
 * }
 */


///////////////// Access Right START /////////////////
///////////////// Access Right END /////////////////


///////////// SQL Safe START /////////////
$AcademicYearID = IntegerSafe($AcademicYearID);
$FormID = IntegerSafe($FormID);
///////////// SQL Safe END /////////////


///////////// Validate START /////////////
///////////// Validate END /////////////


///////////// Init START /////////////
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
$form_class_manage_ui = new form_class_manage_ui();

if($AcademicYearID == 0){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}
///////////// Init END /////////////


///////////// UI START /////////////
echo $form_class_manage_ui->Get_Class_Selection($AcademicYearID, $FormID, $SelectionID, $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $TeachingOnly=0);


?>