<?php
// modify :
/*
 * 2020-08-26 (Pun) [193753]
 *  - Added support for import last year (hidden function)
 * Date:    2020-06-09  (Bill)  [2020-0609-0918-44073]
 *          fixed: cannot import printing data if INTRANET_USER_PERSONAL_SETTINGS - QuitTime = '0000-00-00 00:00:00'
 */

include_once($PATH_WRT_ROOT . "includes/cust/MSSCH_printing/libMsschPrintingDataImport_v2.php");


################### Access Right START ###################
if (!libMsschPrintingAccessRight::checkPrintImportAccessRight()) {
    header('Location: /');
    exit;
}
################### Access Right END ###################


################### Init START ###################
$selectAcademicYear = IntegerSafe($selectAcademicYear);

$error = array();
$result = array();
$studentCostArr = array();
$objImport = new libMsschPrintingDataImport();
$data = $objImport->ReadImportData($TargetFilePath, $selectAcademicYear);

$nrSuccess = 0;    // number of successfully import record

$parentPath = 'management.print';
################### Init END ###################

################### Get group START ###################
$rs = $objPrinting->getAllGroupMfpCodeArr($selectAcademicYear);
$groupIdMapping = array_flip($rs);
################### Get group END ###################

################### Get cost START ###################
$rs = $objPrinting->getCostByAcademicYearId($selectAcademicYear);
$costArr = BuildMultiKeyAssoc($rs, array('YearMonth', 'Type'), 'Cost', true);
$costArr = $costArr[$_POST['selectMonth']];
################### Get cost END ###################

################### Get QuitTime START ###################
$mfpCodeArr = array();
foreach ($data["Data"] as $r) {
    $mfpCodeArr[] = $r['rawData']['MfpCode'];
}
$quitTimeArr = $objPrinting->getAllGroupUserQuitTimeArr($selectAcademicYear, $mfpCodeArr);
################### Get QuitTime END ###################

################### Import START ###################
if ($data != false) {
    if ($data["HeaderError"] == true) {
        $error[] = "Wrong Header";
    } else if ($data["NumOfData"] == 0) {
        $error[] = "No record to import";
    } else    // Import Data exists
    {
        $rs = $data["Data"];

        #### Delete old record START ####
        $sql = "DELETE FROM PRINTING_MEMBER_USAGE_V2 WHERE RECORD_DATE LIKE'{$_POST['selectMonth']}%'";
        $result[] = $_result = $objPrinting->db_db_query($sql);
        #### Delete old record END ####

        if (!$_result) {
            $error[] = "Cannot delete data";
        } else {
            #### Calculate cost START ####
            foreach($rs as $r) {
                if ($r["pass"]) {
                    $crs = $r["rawData"];

                    $date = $crs['Date'];
                    $mfpCode = $crs['MfpCode'];
                    $groupId = $groupIdMapping[$mfpCode];
                    $colorMode = strtoupper($crs['ColorMode']);
                    $pages = $crs['Pages'];
                    $sheet = $crs['Sheet'];
                    $paperSize = strtoupper($crs['PaperSize']);


                    ######## Calculate print cost START ########
                    #### Color START ####
                    if ($colorMode == 'FULL COLOR') {
                        $colorCost = $costArr['colorFc'] * $pages;
                    } else {
                        $colorCost = $costArr['colorBW'] * $pages;
                    }
                    if ($paperSize == 'OTHER') {
                        $colorCost *= 2;
                    }
                    #### Color END ####

                    #### Page START ####
                    if ($paperSize == 'A4') {
                        $paperCost = $costArr['a4Price'] * $sheet;
                    } elseif ($paperSize == 'A3') {
                        $paperCost = $costArr['a3Price'] * $sheet;
                    } else {
                        $paperCost = 0;
                    }
                    #### Page END ####

                    $totalCost = $colorCost + $paperCost;
                    ######## Calculate print cost END ########

                    ######## Calculate total member START ########
                    $totalMember = 0;
                    foreach ($quitTimeArr[$mfpCode] as $studentId => $quitTime) {
                        // [2020-0609-0918-44073] fixed cannot import printing data if $quitTime = '0000-00-00 00:00:00'
                        //if (!is_null($quitTime) && $quitTime < $date) {
                        if (!is_null($quitTime) && !is_date_empty($quitTime) && $quitTime < $date) {
                            continue;
                        }
                        $totalMember++;
                    }
                    ######## Calculate total member END ########

                    ######## Calculate member cost START ########
                    $memberCost = $totalCost / $totalMember;
                    foreach ($quitTimeArr[$mfpCode] as $studentId => $quitTime) {
                        // [2020-0609-0918-44073] fixed cannot import printing data if $quitTime = '0000-00-00 00:00:00'
                        //if (!is_null($quitTime) && $quitTime < $date) {
                        if (!is_null($quitTime) && !is_date_empty($quitTime) && $quitTime < $date) {
                            continue;
                        }
                        $studentCostArr[] = array(
                            'studentId' => $studentId,
                            'groupId' => $groupId,
                            'date' => $date,
                            'cost' => $memberCost,
                            'academicYearId' => $selectAcademicYear,
                        );
                    }

                    ######## Calculate member cost END ########
                }
            } // End For
            #### Calculate cost END ####

            #### Save to DB START ####
	        $studentCostArrChunk = array_chunk($studentCostArr, 2);
            foreach($studentCostArrChunk as $studentCostArr){
	            $valueSql = '';
	            foreach ($studentCostArr as $costInfo) {
		            $valueSql .= "(
                    '{$costInfo['groupId']}',
                    '{$costInfo['academicYearId']}',
                    '{$costInfo['studentId']}',
                    '{$costInfo['date']}',
                    '{$costInfo['cost']}',
                    NOW(),
                    '{$_SESSION['UserID']}'
                ),";
	            }
	            $valueSql = trim($valueSql, ',');

	            $sql = "INSERT INTO 
                    PRINTING_MEMBER_USAGE_V2
                (
                    GROUP_ID, 
                    AcademicYearID, 
                    UserID, 
                    RECORD_DATE, 
                    AMOUNT,
                    DATE_INPUT, 
                    INPUT_BY
                ) VALUES 
                    {$valueSql}";
	            $result[] = $_result = $objPrinting->db_db_query($sql);

	            if ($_result) {
		            $nrSuccess += count($studentCostArr);
	            }
            }
            #### Save to DB END ####
        }
    }
} else {
    $error[] = "False csv data";
}


if (in_array(false, $result)) {
    $Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
} else {
    $Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
}


#################### UI START ####################
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep = 3, $CustStepArr = '');
$Title = $Lang['MsschPrint']['general']['ImportData'];
$PAGE_NAVIGATION[] = array($Title, "");
$CurrentPage = "ManagementPrint";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['print'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>

    <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td align='right'><?= $linterface->GET_SYS_MSG("", $xmsg); ?></td>
        </tr>
        <tr>
            <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
        </tr>
        <tr>
            <td><?= $h_stepUI; ?></td>
        </tr>
    </table>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr height="20">
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align='center'>
                <?= $nrSuccess . "&nbsp;" . $Lang['General']['ImportArr']['RecordsImportedSuccessfully'] ?>
            </td>
        </tr>
        <tr height="20">
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"/></td>
        </tr>

        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td align="center">
                            <br/>
                            <?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t={$parentPath}'", "back_btn", " class='formbutton' "); ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


<?php
$linterface->LAYOUT_STOP();
?>
