<?php



//////////// SQL Safe START ////////////
$batchID = IntegerSafe($batchID);
//////////// SQL Safe END ////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkDepositAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


//////////// Init START //////////// 
$onBackPress = 'history.back()';
$objDB = new libDB();
//////////// Init END //////////// 


//////////// Get User List START ////////////
$depositData = $objPrinting->getBatchDepositDataByBatchID($batchID);
$academic_year = new academic_year($depositData['AcademicYearID']);
$academicYearHTML = $academic_year->Get_Academic_Year_Name();
$dateHTML = $depositData['DATE_INPUT'];
$titleHTML = $depositData['TITLE'];
$totalAmountHTML = $depositData['AMOUNT'];


$userIds = PRINTING_USER_DEPOSIT::getUserIDsByBatchID($batchID);
$userIdSQL = implode("','", $userIds);

if($orderDesc){
	$orderSql = 'ClassName, ClassNumber DESC';
}else{
	$orderSql = 'ClassName, ClassNumber';
}
$sql = "SELECT 
	UserID,
	EnglishName,
	ChineseName,
	ClassName,
	ClassNumber
FROM 
	INTRANET_USER
WHERE 
	UserID IN ('{$userIdSQL}')
ORDER BY
	{$orderSql}";
$rs = $objDB->returnResultSet($sql);
$allClassArr = array();
$userNameArr = array();
foreach($rs as $r){
	$classNameNumber = "{$r['ClassName']}({$r['ClassNumber']}) ";
	$userName = Get_Lang_Selection($r['ChineseName'], $r['EnglishName']);
	$userNameArr[$r['ClassName']][] = $classNameNumber . $userName;//[$r['ClassNumber']]
	$allClassArr[] = $r['ClassName'];
}
$allClassArr = array_values(array_unique($allClassArr));
//$userNameArr = getNameByUserID($userIds);
//////////// Get User List END ////////////

//////////// Get Max student in class START ////////////
$sql = "SELECT 
	COUNT(ClassNumber) AS TOTAL_USER
FROM 
	INTRANET_USER
WHERE 
	UserID IN ('{$userIdSQL}')
GROUP BY
	ClassName";
$totalRow = $objDB->returnVector($sql);
$totalRow = max($totalRow);
//////////// Get Max student in class END ////////////


//////////// UI START ////////////
if($orderDesc){
	$sortingImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif' hspace='2' border='0' />";
}else{
	$sortingImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif' hspace='2' border='0' />";
}


 
$CurrentPage = "ManagementDeposit";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['deposit'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Msg);
?>



<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['General']['AcademicYear']?></td>
		<td class="tabletext" width="70%"><?=$academicYearHTML?></td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['management']['deposit']['tableHeader']['date']?></td>
		<td class="tabletext" width="70%"><?=$dateHTML?></td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['management']['deposit']['tableHeader']['title']?></td>
		<td class="tabletext" width="70%"><?=$titleHTML?></td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['management']['deposit']['tableHeader']['amount']?></td>
		<td class="tabletext" width="70%"><?=number_format($totalAmountHTML, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep'])?></td>
	</tr>
	
	<tr>
		<td height="1" class="dotline" colspan="<?=count($monthArr)+2?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>

	
	<tr>
		<td colspan="2">
		<table class="common_table_list" border="0" cellspacing="0" cellpadding="5" width="100%">	
			<thead>	
				<tr>
					<?php for($i=0,$iMax=count($allClassArr);$i<$iMax;$i++){ ?>
					<th>
						<?php if($i == 0){ ?>
							<a href="?t=management.deposit.showDepositUser&batchID=<?=$batchID?>&orderDesc=<?=($orderDesc+1)%2?>">
								<?=$allClassArr[$i]?>&nbsp;
								<?=$sortingImg?>
							</a>
						<?php }else{ ?>
							<?=$allClassArr[$i]?>
						<?php } ?>
					</th>
					<?php } ?>
					<!--th width="50%">
						<a href="?t=management.deposit.showDepositUser&batchID=<?=$batchID?>&orderDesc=<?=($orderDesc+1)%2?>">
							<?=$Lang['MsschPrint']['report']['printingSummary']['result']['userName']?>
							<?=$sortingImg?>
						</a>
					</th>
					<th>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['userName']?>
					</th-->
				</tr>
			</thead>
			
<!-- -------------------- Display User List START -------------------- -->
			<?php 
			$index = 0;
			for($i=0;$i<$totalRow;$i++){ 
			?>
				<tr>
					<?php foreach((array)$allClassArr as $className){ ?>
					<td>
						<?=$userNameArr[$className][$i]?>
					</td>
					<?php } ?>
				</tr>
			<?php } ?>
<!-- -------------------- Display User List END -------------------- -->

		</table>
		<br />
		</td>
	</tr>
			<tr>
				<td height="1" class="dotline" colspan="<?=count($monthArr)+2?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
			</tr>

			<tr>
				<td height="1" colspan="2" align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button',$ParOnClick=$onBackPress, $ParName="back")?>&nbsp;</td>
			</tr>
</table>


		
	
<?php $linterface->LAYOUT_STOP(); ?>