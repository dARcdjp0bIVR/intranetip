<?php


/////////////////// SQL Safe START ///////////////////
$BatchIdCheckBox = IntegerSafe($BatchIdCheckBox);
/////////////////// SQL Safe END ///////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkDepositAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


/////////////////// INIT START ///////////////////
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////


/////////////////// Validate START ///////////////////
if(count($BatchIdCheckBox) == 0){
	$isValid = false;
	$result[] = false;
}
/////////////////// Validate END ///////////////////


/////////////////// Save START ///////////////////
if($isValid){
	$result[] = PRINTING_BATCH_DEPOSIT::deleteDeposit($BatchIdCheckBox);
	$result[] = PRINTING_USER_DEPOSIT::deleteDepositByBatchID($BatchIdCheckBox);
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}
header('Location: ?t=management.deposit&Msg='.$Msg);
exit;
?>