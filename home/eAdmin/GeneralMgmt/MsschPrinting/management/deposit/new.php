<?php


//////////////////// SQL Safe START ////////////////////
//////////////////// SQL Safe END ////////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkDepositAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


//////////////////// INIT START ////////////////////
$parentPagePath = 'management.deposit&clearCoo=1';
$savePagePath = 'management.deposit.newSave';

//////////////////// INIT END ////////////////////

//////////////////// Access Right START ////////////////////
if(!libMsschPrintingAccessRight::checkDepositAccessRight()){
	header('Location: /');
	exit;
}
//////////////////// Access Right END ////////////////////

//////////////////// UI Field START ////////////////////
$thickboxPar = 'fieldname=r_group_user[]&page_title=SelectMembers&permitted_type=1,2&excluded_type=4&from_thickbox=true&KeepThis=true&TB_iframe=true&height=500&width=600';
$memberHTML = '<span style="float:left;">';
$memberHTML .= $linterface->GET_SELECTION_BOX(array(), "name='r_group_user[]' id='r_group_user[]' class='select_studentlist' size='7' style='width:400px' multiple='multiple'", "");
$memberHTML .= '</span>';
$memberHTML .= $linterface->GET_BTN($button_select, "button", "javascript: $('#thickboxTest').click();");
$memberHTML .= '<br/>';
$memberHTML .= $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('r_group_user[]'))");
$memberHTML .= '<br/>';
$memberHTML .= '<a style="display:none;" id="thickboxTest" class="thickbox" href="/home/common_choose/index.php?'.$thickboxPar.'">test</a>';
$memberHTML .= '<span class="form_sep_title"><br style="clear:both;" />'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>';			
//////////////////// UI Field END ////////////////////


//////////////////// UI START ////////////////////
$CurrentPage = "ManagementDeposit";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['deposit'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['MsschPrint']['menu']['deposit'], "?t={$parentPagePath}");
$PAGE_NAVIGATION[] = array( $Lang['MsschPrint']['management']['deposit']['newDeposit'] , '');
$linterface->LAYOUT_START($Msg);
$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
	.errorMSG{ color: red; }
</style>



<div class="table_board">
	<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
	
<form name="form1" id="form1" method="post" action="?t=<?=$savePagePath?>">
	<table class="form_table_v30">
	
		<tr>
			<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['MsschPrint']['management']['deposit']['tableHeader']['title']?></td>
			<td>
				<input name="title" id="title" class="textboxtext"/><br>
				<span id='errTitleEmpty' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['management']['deposit']['newErr']['titleEmpty']?></span>
			</td>
		</tr>
	
		<tr>
			<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['MsschPrint']['management']['deposit']['new']['amount']?></td>
			<td>
				<input name="amount" id="amount" class="textboxtext"/><br>
				<span id='errAmountEmpty' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['management']['deposit']['newErr']['amountEmpty']?></span>
				<span id='errAmountFormat' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['management']['deposit']['newErr']['amountFormat']?></span>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['MsschPrint']['management']['deposit']['new']['user']?></td>
			<td>
				<?=$memberHTML?>
				<span id='errUserEmpty' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['management']['deposit']['newErr']['userEmpty']?></span>
			</td>
		</tr>
		
	</table>
	
	<?=$linterface->MandatoryField();?>

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?> 
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "document.form1.reset();","reset2") ?> 
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?t={$parentPagePath}'","cancelbtn") ?>
		<p class="spacer"></p>
	</div>
</form>

</div>


<!-------------- Script START -------------->
<script>
	$('#form1').submit(function(){
		$('.errorMSG').hide();
		
		var isValid = true;
		
		var title = $.trim( $('#title').val() );
		if( title == ''){
			$('#errTitleEmpty').show();
			isValid = false;
		}
		
		var amount = $.trim( $('#amount').val() );
		if( amount == ''){
			$('#errAmountEmpty').show();
			isValid = false;
		}
		
		if( !( /^\d*(\.\d{0,2})?$/.test(amount) ) ){
			$('#errAmountFormat').show();
			isValid = false;
		}

		if( $('#r_group_user\\[\\] > option').length == 0 ){
			$('#errUserEmpty').show();
			isValid = false;
		}

		/*if(amount.indexOf('.') == 0){
			$('#amount').val('0' + amount);
		}*/
		$('#r_group_user\\[\\] > option').attr("selected", "selected");
		return isValid;
	});
</script>
<!-------------- Script END -------------->


<?php $linterface->LAYOUT_STOP(); ?>