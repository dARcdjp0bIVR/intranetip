<?php

/////////////////// SQL Safe START ///////////////////
$title = trim( htmlentities( $title , ENT_QUOTES, 'UTF-8'));
$amount = trim( htmlentities( $amount , ENT_QUOTES, 'UTF-8'));
$Member = array();
foreach((array)$r_group_user as $m){
	$Member[] = IntegerSafe(trim($m,'U'));
}
/////////////////// SQL Safe END ///////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkDepositAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


/////////////////// INIT START ///////////////////
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////


/////////////////// Validate START ///////////////////
if($title == ''){
	$isValid = false;
	$result[] = false;
}
if(!preg_match("/^[0-9]*(?:\.[0-9]{1,2})?$/", $amount)){
	$isValid = false;
	$result[] = false;
}
/////////////////// Validate END ///////////////////


/////////////////// Save START ///////////////////
if($isValid){
	$DEPOSIT_ID = PRINTING_BATCH_DEPOSIT::insertDeposit($title, $amount);
	if($DEPOSIT_ID == 0){
		$result[] = false;
	}else{
		$result[] = PRINTING_USER_DEPOSIT::insertDeposit($DEPOSIT_ID, $amount, $Member);
	}
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	$Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
}
header('Location: ?t=management.deposit&Msg='.$Msg);
exit;
?>