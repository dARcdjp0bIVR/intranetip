<?php
// modify : 
include_once($PATH_WRT_ROOT."includes/cust/MSSCH_printing/libMsschPrintingDataImport.php");

// error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
/////////////////// Access Right START ///////////////////
if(!libMsschPrintingAccessRight::checkPrintImportAccessRight()){
	header('Location: /');
	exit;
}
/////////////////// Access Right END ///////////////////


/////////////////// Init START ///////////////////
$libimport = new libimporttext();
$libfs = new libfilesystem();
$objImport = new libMsschPrintingDataImport();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($libfs->file_ext($name));

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$nextStepPath = 'management.printImport3';
$lastStepPath = 'management.printImport1';
/////////////////// Init END ///////////////////


/////////////////// Validate START ///////////////////
if(!($ext == ".CSV") && !($ext == ".TXT"))
{
	header('Location: /');
	exit();
}
/////////////////// Validate END ///////////////////


/////////////////// Import START ///////////////////
#######################################################
### move to temp folder first for others validation ###
#######################################################
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder($cfg_msschPrint['importTempFolder'], $csvfile, $name);


##################################
### Validate file and Read data ##
##################################
$data = $objImport->ReadImportData($TargetFilePath);

$passCount = 0;
if($data === NULL){ // Files contains blank lines.
	
}else{
	foreach($data['Data'] as $d)
	{
		if($d['pass'])
		{
			$passCount++;
		}
	}
}
$failCount = $data['NumOfData'] - $passCount;
/////////////////// Import END ///////////////////


/////////////////// Get group name START ///////////////////
$mfpCodeArr = array();
for($i = 0, $iMax = $data['NumOfData']; $i < $iMax; $i++) {
	$mfpCode = trim( htmlentities( $data['Data'][$i]['rawData']['MfpCode'] , ENT_QUOTES, 'UTF-8'));
	$mfpCodeArr[] = $mfpCode;
}
$mfpCodeList = implode("','", $mfpCodeArr);
$sql = "SELECT
	MFP_CODE,
	NAME
FROM 
	PRINTING_GROUP
WHERE
	MFP_CODE IN ('{$mfpCodeList}')
AND
	AcademicYearID = '{$currentAcademicYearID}'";
$rs = $objDB->returnResultSet($sql);
$nameArr = BuildMultiKeyAssoc($rs, array('MFP_CODE'), $IncludedDBField=array('NAME'), $SingleValue=1);
/////////////////// Get group name END ///////////////////


//////////////////// UI START ////////////////////
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');
$Title = $Lang['MsschPrint']['general']['ImportData'];
$PAGE_NAVIGATION[] = array($Title,"");
$CurrentPage = "ManagementPrint";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['print'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>

<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
		</tr>
		<tr>
			<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td><?=$h_stepUI;?></td>
		</tr>
		<?php if($data !== NULL){ ?>
			<tr>
				<td>	
					<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class='formfieldtitle'><?=$Lang['MsschPrint']['management']['print']['importMonth']?>:</td>
						<td class='tabletext'><?=$_POST['selectMonth']?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['Btn']['Import']?>:</td>
						<td class='tabletext'><?=$Lang['MsschPrint']['management']['print']['importRecord']?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?>:</td>
						<td class='tabletext'><?=$passCount?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?>:</td>
						<td class='tabletext <?php if($failCount>0) print('red');?>'><?=$failCount?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
						<tr>
							<td class='tablebluetop tabletopnolink' width="1%">#</th>
							
<!-- --------------------- Table Header START --------------------- -->
							<td class="tablebluetop tabletopnolink"><?=$Lang['MsschPrint']['management']['print']['importRemarks']['mfpCode']?></th>
							<td class="tablebluetop tabletopnolink"><?=$Lang['MsschPrint']['management']['print']['tableHeader']['groupName']?></th>
							<td class="tablebluetop tabletopnolink"><?=$Lang['MsschPrint']['management']['print']['importRemarks']['amount']?></th>
<!-- --------------------- Table Header END --------------------- -->

							<td class="tablebluetop tabletopnolink"><?=$Lang['MsschPrint']['general']['import']['dataChecking']?></th>
							<?php if($failCount>0) {?>
								<td class="tablebluetop tabletopnolink"><?=$Lang['MsschPrint']['general']['import']['reason']?></th>
							<?php }?>
						</tr>
						
						<?php for($i = 0, $iMax = $data['NumOfData']; $i < $iMax; $i++) {?>
						<tr class='tablebluerow<?=(($i+1)%2+1)?>'>
							<td class='tabletext' valign='top'><?=($i+1)?></td>
							
<!-- --------------------- Table Content START --------------------- -->
							<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['MfpCode']?></td>
							<td class='tabletext' valign='top'><?=$nameArr[ $data['Data'][$i]['rawData']['MfpCode'] ]?></td>
							<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Amount']?></td>
<!-- --------------------- Table Content END --------------------- -->
							 
							<td class='tabletext' valign='top'><?=($data['Data'][$i]['pass'])?$linterface->Get_Tick_Image():'<span class="table_row_tool"><a class="delete"/></span>'?></td>
							<?php if($failCount>0) {?>
								<td class='tabletext' valign='top'><?=$data['Data'][$i]["rawData"]["Error"]?></td>
							<?php }?>
						</tr>
						<?php } //end foreach?>
					</table>
				</td>
			</tr>
		<?php 
		}else{
			echo '<tr><td><p align="center">'.$Lang['General']['NoRecordFound'].'</p></td></tr>';
		}
		?>
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<input type="hidden" name="t" value="<?=$nextStepPath?>"/>
				<input type="hidden" name="TargetFilePath" value="<?=$TargetFilePath?>"/>	
				<?=($failCount == 0) ? $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") :"" ?>
				&nbsp;
				<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t={$lastStepPath}'","back_btn"," class='formbutton' ") ?>
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="selectMonth" value="<?=$_POST['selectMonth']?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
?>