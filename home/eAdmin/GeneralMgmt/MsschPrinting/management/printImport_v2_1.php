<?php
// modify : Pun
/**
 * Change Log:
 * 2020-08-26 (Pun) [193753]
 *  - Added support for import last year (hidden function)
 * 2019-07-08 (Pun)
 *  - File created
 */

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintImportAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////

$sample_file= $PATH_WRT_ROOT.$cfg_msschPrint['path'].'?t=tools.downloadCSV&file=printing_import_sample2.csv&path='.$cfg_msschPrint['path'].'/management/print/';
$csvFile = "<a class='tablelink' href='". $sample_file ."' target='_blank'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

$csv_format = '';
$importRemarks = array_values($Lang['MsschPrint']['management']['print']['importRemarks2']);
$delim = '';
$requiredField = array(0,3,4,8,9,10,11);
for($i=0, $iMax = count($importRemarks); $i < $iMax; $i++)
{
	$csv_format .= $delim . $Lang['SysMgr']['Homework']['Column'] . ' ' . ($i+1) . ' : ';
	if(in_array($i,$requiredField))
	{
		$csv_format .= $linterface->RequiredSymbol();
	}
	$csv_format .= $importRemarks[$i];
	$delim = '<br />';
}
$ajaxSearchYearMonthFilterPath = '?t=generalAjax.getYearMonthSelection';
///////////////// Init END /////////////////


//////////////////// UI Field START ////////////////////
/**** Import year-month in this academic year START **** /
$startAcademicYear = getStartOfAcademicYear();
//$endAcademicYear = getEndOfAcademicYear();
$endAcademicYear = time(); // Only allow select from start month of current academicYear to current month
$_date = $startAcademicYear;

$selectMonthHTML = '<select id="selectMonth" name="selectMonth">';
do {
    $selectMonthHTML .= '<option>'.(date('Y-m', $_date)).'</option>';
}while (($_date = strtotime("+1 MONTH", $_date)) <= $endAcademicYear);
$selectMonthHTML .= '</select>';
/**** Import year-month in this academic year END ****/


/**** Import year-month "start from August end from July" START ****/

$academicYearFilterHTML = getSelectAcademicYear('selectAcademicYear', $tag = '', $noFirst = 1, $noPastYear = 0, $targetYearID = Get_Current_Academic_Year_ID(), $displayAll = 0, $pastAndCurrentYearOnly = 1, $OrderBySequence = 1, $excludeCurrentYear = 0, $excludeYearIDArr = array());
$selectMonthHTML = $objPrinting->getYearMonthSelection();
/**** Import year-month "start from August end from July" END ****/
//////////////////// UI Field END ////////////////////


//////////////////// UI START ////////////////////
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');
$Title = $Lang['MsschPrint']['general']['ImportData'];
$PAGE_NAVIGATION[] = array($Title,"");
$CurrentPage = "ManagementPrint";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['print'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Msg);
?>
<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?=$h_stepUI;?></td>
</tr>

<tr>
	<td>
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr> 
            	<td><br />


					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr style="display:none;">
							<td class="formfieldtitle" align="left"><?=$Lang['General']['AcademicYear']?></td>
							<td class="tabletext">
								<?=$academicYearFilterHTML?>
							</td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['MsschPrint']['management']['print']['importMonth']?></td>
							<td class="tabletext">
								<?=$selectMonthHTML?>
							</td>
						</tr>
						
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "csvfile" name="csvfile" accept=".csv,.txt"></td>
						</tr>
<!--						
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
-->						
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
					<br/><br/>

				</td></tr>
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		        </tr>
            <tr>
				<td align="center">
				<br/>
				<input type="hidden" name="t" value="management.printImport_v2_2"/>
					<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='?t=management.print'","back_btn"," class='formbutton' ") ?>				
				</td>
			</tr>
		</table>
	</td>
</tr>

</table>


</form>

<script>

  $('#selectAcademicYear').change(function(){
    $.get('<?=$ajaxSearchYearMonthFilterPath?>', {
      'AcademicYearID': $(this).val(),
      'SelectionID': 'selectMonth'
    }, function(res){
      $('#selectMonthContainer').html(res);
    });
  });

function CheckForm()
{
	var filename = $("#csvfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}
</script>
<?php

$linterface->LAYOUT_STOP();
?>
