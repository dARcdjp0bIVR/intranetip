<?php

/////////////////// SQL Safe START ///////////////////
$RecordIdCheckBox = IntegerSafe($RecordIdCheckBox);
/////////////////// SQL Safe END ///////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintImportAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


/////////////////// INIT START ///////////////////
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
if(empty($RecordIdCheckBox)){
	$isValid = false;
	$result[] = false;
}
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if($isValid){
	$result[] = PRINTING_GROUP_USAGE::deleteAllGroupUsage($RecordIdCheckBox);
	$result[] = PRINTING_MEMBER_USAGE::deleteAllMemberUsage($RecordIdCheckBox);
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];}
header('Location: ?t=management.print&Msg='.$Msg);
exit;
?>