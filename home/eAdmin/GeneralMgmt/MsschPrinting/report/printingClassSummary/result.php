<?php
/**
 * Change Log:
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 * 2016-01-18 (Pun)
 *  - Change the start month from "start month of current academic year" to "start - 1 month of current academic year"
 */

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// SQL Safe START /////////////////
$classID = IntegerSafe($classID);
///////////////// SQL Safe END /////////////////


///////////////// Init START /////////////////
$currentAcademicYear = Get_Current_Academic_Year_ID();
$form_class_manage = new form_class_manage();

$academicYearHTML = getCurrentAcademicYear();

$printViewLink = '?t=report.printingClassSummary.result&printView=1&'.http_build_query(array('classID' => $classID));
$exportViewLink = '?t=report.printingClassSummary.result&exportView=1&'.http_build_query(array('classID' => $classID));
///////////////// Init END /////////////////


///////////////// Validate START /////////////////
///////////////// Validate END /////////////////


//// Get all month in current academic year START ////
$period = getPeriodOfAcademicYear($currentAcademicYear);
$_startDate = $period['StartDate'];
$_startDate = (date('Y-m-d H:i:s', strtotime('-1 month', strtotime($_startDate))));
$_endDate = $period['EndDate'];
$_endDate = (date('Y-m-d H:i:s', strtotime('-1 month', strtotime($_endDate))));
$monthArr = returnListOfYearMonth($_startDate, $_endDate);
//// Get all month in current academic year END ////

if(is_array($classID)){
    include('getAllClassDisplay.php');
}else{
    include('getSingleClassDisplay.php');
}
if(empty($classID)){
    $data = array();
    $rowTitle = array();
}

//////////////////// Calculate Total Amount START ////////////////////
$totalAmount = 0;
foreach($data as $d){
    $money = array_values($d);
    foreach($money as $m){
        $totalAmount += $m;
    }
}
//////////////////// Calculate Total Amount END ////////////////////

//////////////////// UI START ////////////////////

if($printView){
    include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
    
    $printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
    
    $printHTML = <<<HTML
    
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td>
	<div class="print_hide" align="right">
	<br />
	$printButton
	<br />
	<br />
	</div>
HTML;
    echo $printHTML;
    $tableStyle = 'common_table_list_v30';
}else if($exportView){
    include('export.php');
    exit;
}else{
    $CurrentPage = "ReportPrintClassSummary";
    $TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingClassSummary'], "", 0);
    $MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
    
    $linterface->LAYOUT_START($Msg);
    $tableStyle = 'displayData';
}
?>


<table width="100%" border="0" cellspacing="0" cellpadding="5">
<?php if(!$printView){ ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['year']?></td>
					<td class="tabletext" width="70%"><?=$academicYearHTML?></td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['class']?></td>
					<td class="tabletext" width="70%"><?=$className?></td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount']?></td>
					<td class="tabletext" width="70%"><?=number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep'])?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="<?=count($monthArr)+2?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<!--tr>
					<td><?=$linterface->GET_NAVIGATION2($Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'])?></td>
				</tr-->
				
				<tr>
					<td>
						<div class="Conntent_tool">
							<a href="<?=$printViewLink?>" target="_blank" class="print tablelink" style="float:none;display:inline;"><?=$Lang['Btn']['Print']?></a>&nbsp;
							<a href="<?=$exportViewLink?>" target="_blank" class="export tablelink" style="float:none;display:inline;"><?=$Lang['Btn']['Export']?></a>
						</div>
					</td>
				</tr>
<?php }else{ ?>
	<tr>
		<td colspan="2">
			<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">
				<?=$className . ' ' . $Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord']?> (<?=$academicYearHTML?>)
			</div>
		</td>
	</tr>
<?php } ?>
	<tr>
		<td colspan="2">

<!-- -------------------- Report All Class START -------------------- -->
			<table class="<?=$tableStyle?>" border="0" cellspacing="0" cellpadding="5" width="100%">	
			<thead>	
				<tr>
					<th class="tablebluetop tabletopnolink" style="text-align:center">
						<?=(is_array($classID))? $Lang['MsschPrint']['report']['printingSummary']['result']['class'] : $Lang['MsschPrint']['report']['printingSummary']['result']['userName']?>
					</th>
					<!--th class="tablebluetop tabletopnolink" style="text-align:center">
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance']?>
					</th-->
					
					<?php 
					foreach($monthArr as $month){ 
						$m = (int)substr($month, 5,2);
					?>
						<th width="5%" class="tablebluetop tabletopnolink" style="background-color:#6BB357;text-align:center" nowrap><?=$Lang['General']['month'][$m]?></th>
					<?php 
					} 
					?>

					<th class="tablebluetop tabletopnolink" style="text-align:center">
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount']?>
					</th>
					
					<!--th class="tablebluetop tabletopnolink" style="text-align:center" nowrap>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance']?>
					</td-->
				</tr>
			</thead>
				
				<!-- -------- Data Display START -------- -->
				<?php 
				$index = 0;
				$monthAmount = array();
				$totalOldBalance = array_sum($oldBalance);
				
				foreach((array)$rowTitle as $id => $title){ 
				?>
					<tr class="tablebluerow<?=($index++ % 2 + 1)?>">
						<td valign="top" class="tabletext"><?=$title?></td>
						<!--td valign="top" class="tabletext" style="text-align:center"><?= ($oldBalance[$id])? $oldBalance[$id] : '0' ?></td-->
						<?php 
						$rowTotalAmount = 0;
						foreach($monthArr as $month){ 
							$day = str_replace('_','-',$month) . '-01';
							$_amount = $data[$id][$day];
							$rowTotalAmount += $_amount;
							$monthAmount[$month] += $_amount;
							
						?>
							<td class="tabletext" style="text-align:center"><?= ($_amount)? number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
						<?php 
						}
						?>
						<td class="tabletext" style="text-align:center"><?= ($rowTotalAmount)? number_format($rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
						<!--td class="tabletext" style="text-align:center"><?= ($oldBalance[$id] - $rowTotalAmount)? number_format( ($oldBalance[$id] - $rowTotalAmount) , $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td-->
					</tr>
				<?php 
				} 
				?>
				<!-- -------- Data Display END -------- -->
				
				<tr class="tablebluebottom">
					<td class="tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount']?></td>
					<!--td class="tabletext"><?= ($totalOldBalance)? number_format($totalOldBalance, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td-->
					<?php 
					foreach($monthArr as $month){ 
						$_amount = $monthAmount[$month];
					?>
						<td class="tabletext"><?= ($_amount)? number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
					<?php 
					} 
					$yearAmount = array_sum($monthAmount);
					$currentBalance = $totalOldBalance - $yearAmount;
					?>
					<td class="tabletext"><?= ($yearAmount)? number_format($yearAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
					<!--td class="tabletext"><?= ($currentBalance)? number_format($currentBalance, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td-->
				</tr>
			</table>
			<br />
			<?php if(!is_array($classID)){ ?>
			<tr>
				<td><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></td>
			</tr>
			<?php } ?>
<!-- -------------------- Report All Class END -------------------- -->
			<?php if(!$printView){ ?>
				<tr>
					<td height="1" class="dotline" colspan="<?=count($monthArr)+2?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
	
				<tr>
					<td height="1" colspan="2" align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button',$ParOnClick=$onBackPress, $ParName="back")?>&nbsp;</td>
				</tr>
			<?php } ?>
		</td>
	</tr>
</table>


<script type="text/javascript" src="/templates/jquery/jquery.fixedheader.js"></script>
<script type="text/javascript">
	$('.displayData').each(function(){
		new FixedHeader( this );
	});
</script>

<?php 
if(!$printView){ 
	$linterface->LAYOUT_STOP();
}
?>