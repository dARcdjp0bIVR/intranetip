<?php
/**
 * Change Log:
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 * 2016-01-18 (Pun)
 *  - Change the start month from "start month of current academic year" to "start - 1 month of current academic year"
 */

///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// SQL Safe START /////////////////
$classID = IntegerSafe($classID);
///////////////// SQL Safe END /////////////////


///////////////// Init START /////////////////
$currentAcademicYear = Get_Current_Academic_Year_ID();
$form_class_manage = new form_class_manage();

$academicYearHTML = getCurrentAcademicYear();

$printViewLink = '?t=report.printingClassSummary.result_v2&printView=1&' . http_build_query(array('classID' => $classID));
$exportViewLink = '?t=report.printingClassSummary.result_v2&exportView=1&' . http_build_query(array('classID' => $classID));
///////////////// Init END /////////////////


///////////////// Validate START /////////////////
///////////////// Validate END /////////////////


//// Get all month in current academic year START ////
$yearMonthArr = $objPrinting->getYearMonthArr();
//// Get all month in current academic year END ////

if (is_array($classID)) {
    include('getAllClassDisplay_v2.php');
} else {
    include('getSingleClassDisplay_v2.php');
}
if (empty($classID)) {
    $data = array();
    $rowTitle = array();
}

//////////////////// Calculate Total Amount START ////////////////////
$totalAmount = array_sum($monthTotalUsage);
//////////////////// Calculate Total Amount END ////////////////////


//////////////////// UI START ////////////////////

if ($printView) {
    include_once($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");

    $printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

    $printHTML = <<<HTML
    
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td>
	<div class="print_hide" align="right">
	<br />
	$printButton
	<br />
	<br />
	</div>
HTML;
    echo $printHTML;
    $tableStyle = 'common_table_list_v30';
} else if ($exportView) {
    include('export_v2.php');
    exit;
} else {
    $CurrentPage = "ReportPrintClassSummary";
    $TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingClassSummary'], "", 0);
    $MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

    $linterface->LAYOUT_START($Msg);
    $tableStyle = 'displayData';
}
?>
    <style>
        .displayData td.tabletext {
            border-right: 1px solid lightblue;
        }
    </style>

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <?php if (!$printView) { ?>
            <tr>
                <td valign="top" nowrap="nowrap"
                    class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['year'] ?></td>
                <td class="tabletext" width="70%"><?= $academicYearHTML ?></td>
            </tr>

            <tr>
                <td valign="top" nowrap="nowrap"
                    class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['class'] ?></td>
                <td class="tabletext" width="70%"><?= $className ?></td>
            </tr>

            <tr>
                <td valign="top" nowrap="nowrap"
                    class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?></td>
                <td class="tabletext"
                    width="70%"><?= number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
            </tr>

            <tr>
                <td height="1" class="dotline" colspan="<?= count($monthArr) + 2 ?>"><img
                            src="<?= $image_path ?>/<?= $LAYOUT_SKIN ?>/10x10.gif" width="10" height="1"></td>
            </tr>
            <!--tr>
					<td><?= $linterface->GET_NAVIGATION2($Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord']) ?></td>
				</tr-->

            <tr>
                <td>
                    <div class="Conntent_tool">
                        <a href="<?= $printViewLink ?>" target="_blank" class="print tablelink"
                           style="float:none;display:inline;"><?= $Lang['Btn']['Print'] ?></a>&nbsp;
                        <a href="<?= $exportViewLink ?>" target="_blank" class="export tablelink"
                           style="float:none;display:inline;"><?= $Lang['Btn']['Export'] ?></a>
                    </div>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="2">
                    <div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">
                        <?= $className . ' ' . $Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'] ?>
                        (<?= $academicYearHTML ?>)
                    </div>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td colspan="2">

                <!-- -------------------- Report All Class START -------------------- -->
                <table class="<?= $tableStyle ?>" border="0" cellspacing="0" cellpadding="5" width="100%">
                    <thead>
                    <tr>
                        <th class="tablebluetop tabletopnolink" style="text-align:center;width:150px;">
                            <?= (is_array($classID)) ? $Lang['MsschPrint']['report']['printingSummary']['result']['class'] : $Lang['MsschPrint']['report']['printingSummary']['result']['userName'] ?>
                        </th>
                        <!--th class="tablebluetop tabletopnolink" style="text-align:center">
						<?= $Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'] ?>
					</th-->

                        <?php
                        foreach ($yearMonthArr as $yearMonth) {
                            $m = (int)substr($yearMonth, 5, 2);
                            ?>
                            <th class="tablebluetop tabletopnolink"
                                style="background-color:#6BB357;text-align:center;width: 100px;"
                                nowrap><?= $Lang['General']['month'][$m] ?></th>
                            <?php
                        }
                        ?>

                        <th class="tablebluetop tabletopnolink" style="text-align:center;width: 100px;">
                            <?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?>
                        </th>

                        <!--th class="tablebluetop tabletopnolink" style="text-align:center" nowrap>
						<?= $Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'] ?>
					</td-->
                    </tr>
                    </thead>

                    <!-- -------- Data Display START -------- -->
                    <tbody>
                    <?php
                    $index = 0;
                    foreach ((array)$rowData as $d1) {
                        ?>
                        <tr class="tablebluerow<?= ($index++ % 2 + 1) ?>">
                            <?php
                            foreach ($d1 as $i => $d):
                                $style = ($i) ? 'text-align:center' : '';
                                ?>
                                <td valign="top" class="tabletext" style="<?= $style ?>">
                                    <?= $d ?>
                                </td>
                            <?php
                            endforeach;
                            ?>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                    <!-- -------- Data Display END -------- -->

                    <tfoot>
                    <tr class="tablebluebottom">
                        <td class="tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?></td>
                        <?php
                        foreach ($yearMonthArr as $i => $yearMonth):
                            $style = ($i) ? 'text-align:center' : '';
                            ?>
                            <td valign="top" class="tabletext" style="<?= $style ?>">
                                <?= ($monthTotalUsage[$yearMonth] == 0) ? '&nbsp;' : number_format($monthTotalUsage[$yearMonth], $cfg_msschPrint['moneyFormat']['decimals']) ?>
                            </td>
                        <?php
                        endforeach;
                        ?>
                        <td valign="top" class="tabletext" style="text-align:center;">
                            <?= ($totalAmount == 0) ? '&nbsp;' : number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals']) ?>
                        </td>
                    </tr>
                    </tfoot>
                </table>
                <br/>
                <?php if (!is_array($classID)){ ?>
        <tr>
            <td><?= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] ?></td>
        </tr>
    <?php } ?>
        <!-- -------------------- Report All Class END -------------------- -->
        <?php if (!$printView) { ?>
            <tr>
                <td height="1" class="dotline" colspan="<?= count($monthArr) + 2 ?>"><img
                            src="<?= $image_path ?>/<?= $LAYOUT_SKIN ?>/10x10.gif" width="10" height="1"></td>
            </tr>

            <tr>
                <td height="1" colspan="2"
                    align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', $ParOnClick = $onBackPress, $ParName = "back") ?>
                    &nbsp;
                </td>
            </tr>
        <?php } ?>
        </td>
        </tr>
    </table>


    <script type="text/javascript" src="/templates/jquery/jquery.fixedheader.js"></script>
    <script type="text/javascript">
        $('.displayData').each(function () {
            new FixedHeader(this);
        });
    </script>

<?php
if (!$printView) {
    $linterface->LAYOUT_STOP();
}
?>