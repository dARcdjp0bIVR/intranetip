<?php
/**
 * Change Log:
 * 2016-01-18 (Pun)
 *  - Hide deposit and total balance
 */

///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// SQL Safe START /////////////////
$AcademicYearFrom = IntegerSafe($AcademicYearFrom);
$AcademicYearTo = IntegerSafe($AcademicYearTo);
$selectedUserID = IntegerSafe($selectedUserID);
///////////////// SQL Safe END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT . "includes/form_class_manage.php");

$backPath = '?t=report.printingUserSummary';

$yearFrom = new academic_year($AcademicYearFrom);
$yearTo = new academic_year($AcademicYearTo);
$user = new libuser($selectedUserID);

$yearIdList = getAcademicYearIdList($AcademicYearFrom, $AcademicYearTo); # cust/common_function.php
$yearNameList = getAcademicYearNameList($yearIdList); # cust/common_function.php
$groupNameList = $objPrinting->getAllGroupNameArr($yearIdList);
$groupMfpList = $objPrinting->getAllGroupMfpCodeArr($yearIdList);

$printViewLink = "?t=report.printingUserSummary.result_v2&printView=1&AcademicYearFrom={$AcademicYearFrom}&AcademicYearTo={$AcademicYearTo}&selectedUserID={$selectedUserID}";
$exportViewLink = "?t=report.printingUserSummary.result_v2&exportView=1&AcademicYearFrom={$AcademicYearFrom}&AcademicYearTo={$AcademicYearTo}&selectedUserID={$selectedUserID}";
///////////////// Init END /////////////////


///////////////// Helper START /////////////////
///////////////// Helper END /////////////////


///////////////// Report Summary START /////////////////
$yearFromName = $yearFrom->Get_Academic_Year_Name();
$yearToName = $yearTo->Get_Academic_Year_Name();
if ($yearFromName == $yearToName) {
    $yearHTML = $yearFromName;
} else {
    $yearHTML = "{$yearFromName}&nbsp;{$Lang['General']['To']}&nbsp;{$yearToName}";
}
$userName = $user->UserNameLang();

$totalDeposit = number_format($objPrinting->getPrintingDeposit($selectedUserID, $yearIdList), $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
$totalPrintingAmount = number_format($objPrinting->getPrintingUsage($selectedUserID, $yearIdList), $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
$totalBalance = number_format($totalDeposit - $totalPrintingAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
///////////////// Report Summary END /////////////////


///////////////// Deposit Report START /////////////////
$depositList = PRINTING_USER_DEPOSIT::getUserDeposit($selectedUserID, $yearIdList);
///////////////// Deposit Report END /////////////////


///////////////// Printing Report START /////////////////
$printingList = PRINTING_MEMBER_USAGE::getUserPrintingRecord($selectedUserID, $yearIdList);
$printingList = array_reverse($printingList, true);
///////////////// Printing Report END /////////////////


///////////////// Printing Report v2 START /////////////////
$rs = $objPrinting->getSingleUserReportData_v2($selectedUserID);


///////////////// Calculate student group usage START /////////////////
$v2ReportAcademicYearIdArr = array();
$yearMonthGroupV2 = array();
$studentGroupUsageV2 = array();
$studentUsageV2 = array();
$studentYearMonthTotalV2 = array();
$studentYearTotalV2 = array();
foreach ($rs as $r) {
    $yearMonth = substr($r['RECORD_DATE'], 0, 7);
    $yearMonthGroupV2[$r['AcademicYearID']][$r['GROUP_ID']] = $r['GROUP_NAME'];
    $studentUsageV2[$yearMonth][$r['GROUP_ID']] += $r['AMOUNT'];
    $studentGroupUsageV2[$r['GROUP_ID']] += $r['AMOUNT'];
    $v2ReportAcademicYearIdArr[] = $r['AcademicYearID'];

    $studentYearMonthTotalV2[$yearMonth] += $r['AMOUNT'];
    $studentYearTotalV2[$r['AcademicYearID']] += $r['AMOUNT'];
    $totalPrintingAmount += $r['AMOUNT'];
}
uksort($studentUsageV2, 'strcmp');
///////////////// Calculate student group usage END /////////////////
///////////////// Printing Report v2 END /////////////////


//////////////////// UI START ////////////////////
if ($printView) {
    include_once($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");

    $printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

    $printHTML = <<<HTML

	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td>
	<div class="print_hide" align="right">
	<br />
	$printButton
	<br />
	<br />
	</div>
HTML;
    echo $printHTML;
    $tableStyle = 'common_table_list_v30';
} else if ($exportView) {
    include('export_v2.php');
    exit;
} else {
    $CurrentPage = "ReportPrintUserSummary";
    $TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingUserSummary'], "", 0);
    $MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

    $linterface->LAYOUT_START($Msg);
    $tableStyle = 'displayData';
}
?>

    <style>
        .displayData td.tabletext {
            border-right: 1px solid lightblue;
        }
    </style>

    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td>

                <!-- -------------------- Report Summary START -------------------- -->
                <table border="0" cellspacing="0" cellpadding="5" width="100%">

                    <!-- -------- Form Element START -------- -->
                    <tr>
                        <td valign="top" nowrap="nowrap"
                            class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['year'] ?></td>
                        <td class="tabletext" width="70%"><?= $yearHTML ?></td>
                    </tr>

                    <tr>
                        <td valign="top" nowrap="nowrap"
                            class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['userName'] ?></td>
                        <td class="tabletext" width="70%"><?= $userName ?></td>
                    </tr>

                    <!--tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalDeposit'] ?></td>
					<td class="tabletext" width="70%">$ <?= $totalDeposit ?></td>
				</tr-->

                    <tr>
                        <td valign="top" nowrap="nowrap"
                            class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalPrinting'] ?></td>
                        <td class="tabletext" width="70%">$ <span
                                    id="totalAmount"><?= number_format($totalPrintingAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></span>
                        </td>
                    </tr>

                    <!--tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['Balance'] ?></td>
					<td class="tabletext" width="70%">$ <?= $totalBalance ?></td>
				</tr-->
                    <!-- -------- Form Element END -------- -->

                    <tr>
                        <td height="1" class="dotline" colspan="2"><img
                                    src="<?= $image_path ?>/<?= $LAYOUT_SKIN ?>/10x10.gif" width="10" height="1"></td>
                    </tr>
                </table>
                <br/>
                <!-- -------------------- Report Summary END -------------------- -->

            </td>
        </tr>

        <?php if (!$printView) { ?>
            <tr>
                <td>
                    <div class="Conntent_tool">
                        <a href="<?= $printViewLink ?>" target="_blank" class="print tablelink"
                           style="float:none;display:inline;"><?= $Lang['Btn']['Print'] ?></a>&nbsp;
                        <a href="<?= $exportViewLink ?>" target="_blank" class="export tablelink"
                           style="float:none;display:inline;"><?= $Lang['Btn']['Export'] ?></a>
                    </div>
                </td>
            </tr>
        <?php } ?>

        <!-- -------------------- Deposit Report START -------------------- -- >
				<tr>
					<td><?= $linterface->GET_NAVIGATION2($Lang['MsschPrint']['report']['printingSummary']['result']['depositRecord']) ?></td>
				</tr>
	<tr>
		<td>

			<table class="<?= $tableStyle ?>" border="0" cellspacing="0" cellpadding="5" width="100%">
				<thead>
					<tr>
						<th width="33%" class="tablebluetop tabletopnolink">
							<?= $Lang['MsschPrint']['report']['printingSummary']['result']['dateInput'] ?>
						</th>
						<th width="33%" class="tablebluetop tabletopnolink">
							<?= $Lang['MsschPrint']['report']['printingSummary']['result']['amount'] ?>
						</th>
						<th width="34%" class="tablebluetop tabletopnolink">
							<?= $Lang['MsschPrint']['report']['printingSummary']['result']['inputBy'] ?>
						</th>
					</tr>
				</thead>
				
				<!-- -------- Data Display START -------- -- >
				<?php foreach ($depositList as $index => $deposit) { ?>
					<tr class="tablebluerow<?= ($index % 2 + 1) ?>">
						<td valign="top" class="tabletext"><?= $deposit['DATE_INPUT'] ?></td>
						<td valign="top" class="tabletext"><?= number_format($deposit['AMOUNT'], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
						<td valign="top" class="tabletext"><?= $deposit['INPUT_BY'] ?></td>
					</tr>
				<?php } ?>
				<!-- -------- Data Display END -------- -- >
				
				<tr class="tablebluebottom">
					<td class="tabletext"><?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?></td>
					<td colspan="2" class="tabletext"><?= $totalDeposit ?></td>
				</tr>
			</table>
			<br />

		</td>
	</tr>
				<tr>
					<td height="1" class="dotline" colspan="3"><img src="<?= $image_path ?>/<?= $LAYOUT_SKIN ?>/10x10.gif" width="10" height="1"></td>
				</tr>
<!-- -------------------- Deposit Report END -------------------- -->

        <tr>
            <td><?= $linterface->GET_NAVIGATION2($Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord']) ?></td>
        </tr>

        <tr>
            <td>

                <!-- -------------------- Printing Report START -------------------- -->
                <?php
                foreach ($yearIdList as $academicYearId => $yearID) {
                    if (in_array($yearID, $v2ReportAcademicYearIdArr)):
                        ####################### V2 report START #######################
                        ?>
                        <span><?= $yearNameList[$yearID] ?></span>
                        <table class="<?= $tableStyle ?>" border="0" cellspacing="0" cellpadding="5" width="100%">
                            <thead>
                            <tr>
                                <th class="tablebluetop tabletopnolink" style="white-space: nowrap; width: 300px;">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['month'] ?>
                                </th>
                                <?php
                                foreach ($yearMonthGroupV2[$yearID] as $groupID => $groupName) {
                                    ?>
                                    <th class="tablebluetop tabletopnolink"
                                        style="text-align:center;white-space: nowrap; width: 300px;">
                                        <?= $groupNameList[$groupID] ?><br/>
                                        (<?= $groupMfpList[$groupID] ?>)
                                    </th>
                                    <?php
                                }
                                ?>
                                <th class="tablebluetop tabletopnolink"
                                    style="text-align:center;white-space: nowrap; width: 300px;">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?>
                                </th>
                            </tr>
                            </thead>

                            <!-- -------- Data Display START -------- -->
                            <tbody>
                            <?php
                            $css = 0;
                            foreach ($studentUsageV2 as $yearMonth => $d1) {
                                $m = (int)substr($yearMonth, 5, 2);
                                ?>
                                <tr class="tablebluerow<?= ($css++ % 2 + 1) ?>">
                                    <td class="tabletext"><?= $Lang['General']['month'][$m] ?></td>
                                    <?php
                                    foreach ($yearMonthGroupV2[$yearID] as $groupID => $groupName) {
                                        $amount = $d1[$groupID];
                                        if($amount == 0){
                                            $amount = '&nbsp;';
                                        }else{
                                            $amount = number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
                                        }
                                        ?>
                                        <td class="tabletext"
                                            style="text-align:center;"><?= $amount ?></td>
                                        <?php
                                    }
                                    ?>
                                    <td class="tabletext"
                                        style="text-align:center;"><?= number_format($studentYearMonthTotalV2[$yearMonth], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <!-- -------- Data Display END -------- -->

                            <!-- -------- Footer Display START -------- -->
                            <tfoot>
                            <tr class="tablebluebottom">
                                <td class="tabletext">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?>
                                </td>
                                <?php
                                foreach ($yearMonthGroupV2[$yearID] as $groupID => $groupName) {
                                    ?>
                                    <td style="text-align:center;"><?= number_format($studentGroupUsageV2[$groupID], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                                    <?php
                                }
                                ?>
                                <td class="yearTotal"
                                    style="text-align:center;"><?= number_format($studentYearTotalV2[$yearID], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                            </tr>
                            </tfoot>
                            <!-- -------- Footer Display END -------- -->

                        </table>
                    <?php
                    ####################### V2 report END #######################
                    else:
                        ####################### V1 report START #######################
                        $printingRecord = (array)$printingList[$yearID];
//			foreach($printingList as $yearID=>$printingRecord){ 
                        $totalGroupAmount = array();
                        ?>
                        <span><?= $yearNameList[$yearID] ?></span>
                        <table class="<?= $tableStyle ?>" border="0" cellspacing="0" cellpadding="5" width="100%">
                            <thead>
                            <tr>
                                <th class="tablebluetop tabletopnolink" style="white-space: nowrap; width: 300px;">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['month'] ?>
                                </th>
                                <?php
                                $groups = array();
                                foreach ($printingRecord as $groupRecord) {
                                    ksort($groupRecord);
                                    foreach ($groupRecord as $groupName => $amount) {
                                        $groups[$groupName] = 1;
                                    }
                                }
                                $groups = array_keys($groups);
                                foreach ($groups as $groupID) {
                                    ?>
                                    <th class="tablebluetop tabletopnolink"
                                        style="text-align:center;white-space: nowrap; width: 300px;">
                                        <?= $groupNameList[$groupID] ?><br/>
                                        (<?= $groupMfpList[$groupID] ?>)
                                    </th>
                                    <?php
                                }
                                ?>
                                <th class="tablebluetop tabletopnolink"
                                    style="text-align:center;white-space: nowrap; width: 300px;">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?>
                                </th>
                            </tr>
                            </thead>

                            <!-- -------- Data Display START -------- -->
                            <tbody>
                            <?php
                            $css = 0;
                            foreach ($printingRecord as $month => $groupRecord) {
                                ?>
                                <tr class="tablebluerow<?= ($css++ % 2 + 1) ?>">
                                    <td class="tabletext"><?= $Lang['General']['month'][$month] ?></td>
                                    <?php
                                    $totalMonthAmount = 0;
                                    foreach ($groups as $group) {
                                        $amount = $groupRecord[$group];
                                        $totalMonthAmount += $amount;
                                        $totalGroupAmount[$group] += $amount;
                                        ?>
                                        <td class="tabletext"
                                            style="text-align:center;"><?= number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                                        <?php
                                    }
                                    ?>
                                    <td style="text-align:center;"><?= number_format($totalMonthAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <!-- -------- Data Display END -------- -->

                            <!-- -------- Footer Display START -------- -->
                            <tfoot>
                            <tr class="tablebluebottom">
                                <td class="tabletext">
                                    <?= $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'] ?>
                                </td>
                                <?php
                                $totalYearAmount = 0;
                                foreach ($groups as $group) {
                                    $amount = $totalGroupAmount[$group];
                                    $totalYearAmount += $amount;
                                    ?>
                                    <td class="tabletext"
                                        style="text-align:center;"><?= number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                                    <?php
                                }
                                ?>
                                <td class="yearTotal"
                                    style="text-align:center;"><?= number_format($totalYearAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) ?></td>
                            </tr>
                            </tfoot>
                            <!-- -------- Footer Display END -------- -->

                        </table>

                        <br/>
                    <?php
                        ####################### V1 report END #######################
                    endif;
                }
                ?>
                <!-- -------------------- Printing Report END -------------------- -->

                <?php if (!$printView){ ?>
                <table border="0" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <td height="1" class="dotline"><img src="<?= $image_path ?>/<?= $LAYOUT_SKIN ?>/10x10.gif"
                                                            width="10" height="1"></td>
                    </tr>
                </table>
        <tr>
            <td height="1"
                align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', $ParOnClick = "window.location.href = '{$backPath}'", $ParName = "back") ?>
                &nbsp;
            </td>
        </tr>
    <?php } ?>
        </td>
        </tr>
    </table>


    <script type="text/javascript" src="/templates/jquery/jquery.fixedheader.js"></script>
    <script type="text/javascript">
        $('.displayData').each(function () {
            new FixedHeader(this);
        });
    </script>

<?php
if (!$printView) {
    $linterface->LAYOUT_STOP();
}
?>