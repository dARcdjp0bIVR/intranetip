<?php
/**
 * Change Log:
 * 2016-01-18 (Pun)
 *  - Hide deposit and total balance
 */


///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT . "includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();
///////////////// Init END /////////////////


///////////////// Export START /////////////////


//// Report Summary START ////
$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['year'];
$_cell[] = $yearHTML;
$Rows[] = $_cell;

$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['userName'];
$_cell[] = $userName;
$Rows[] = $_cell;

// $_cell = array();
// $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalDeposit'];
// $_cell[] = $totalDeposit;
// $Rows[] = $_cell;

$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalPrinting'];
$_cell[] = $totalPrintingAmount;
$Rows[] = $_cell;

// $_cell = array();
// $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['Balance'];
// $_cell[] = $totalBalance;
// $Rows[] = $_cell;
//// Report Summary END ////

// $Rows[] = '';

//// Deposit Report START ////
/*
$Rows[] = '';
$Rows[] = array($Lang['MsschPrint']['report']['printingSummary']['result']['depositRecord']);
$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['dateInput'];
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['amount'];
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['inputBy'];
$Rows[] = $_cell;

foreach($depositList as $index => $deposit){
	$_cell = array();
	$_cell[] = $deposit['DATE_INPUT'];
	$_cell[] = number_format($deposit['AMOUNT'], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
	$_cell[] = $deposit['INPUT_BY'];
	$Rows[] = $_cell;
}
$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
$_cell[] = $totalDeposit;
$Rows[] = $_cell;
*/
//// Deposit Report END ////

$Rows[] = '';

//// Printing Report START ////
foreach ($yearIdList as $yearID) {
    $Rows[] = '';
    $Rows[] = array($Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'] . ' ' . $yearNameList[$yearID]);

    $_cell = array();
    $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['month'];
    if (in_array($yearID, $v2ReportAcademicYearIdArr)) {
        ####################### V2 report START #######################
        foreach ($yearMonthGroupV2[$yearID] as $groupID => $groupName) {
            $_cell[] = $groupNameList[$groupID] . ' (' . $groupMfpList[$groupID] . ')';
        }
        $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
        $Rows[] = $_cell;


        foreach ($studentUsageV2 as $yearMonth => $d1) {
            $m = (int)substr($yearMonth, 5, 2);
            $_cell = array();

            $_cell[] = $Lang['General']['month'][$m];
            foreach ($d1 as $groupId => $amount) {
                $_cell[] = number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
            }
            $_cell[] = number_format($studentYearMonthTotalV2[$yearMonth], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);

            $Rows[] = $_cell;
        }

        $_cell = array();
        $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
        foreach ($yearMonthGroupV2[$yearID] as $groupID => $groupName) {
            $_cell[] = number_format($studentYearMonthTotalV2[$yearMonth], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
        }
        $_cell[] = number_format($studentYearTotalV2[$yearID], $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
        $Rows[] = $_cell;
        ####################### V2 report END #######################
    } else {
        ####################### V1 report START #######################
        $printingRecord = (array)$printingList[$yearID];
        $totalGroupAmount = array();
        $groups = array();
        foreach ($printingRecord as $groupRecord) {
            ksort($groupRecord);
            foreach ($groupRecord as $groupName => $amount) {
                $groups[$groupName] = 1;
            }
        }
        $groups = array_keys($groups);
        foreach ($groups as $groupID) {
            $_cell[] = $groupNameList[$groupID] . ' (' . $groupMfpList[$groupID] . ')';
        }
        $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
        $Rows[] = $_cell;

        foreach ($printingRecord as $month => $groupRecord) {
            $_cell = array();
            $_cell[] = $Lang['General']['month'][$month];
            $totalMonthAmount = 0;
            foreach ($groups as $group) {
                $amount = $groupRecord[$group];
                $totalMonthAmount += $amount;
                $totalGroupAmount[$group] += $amount;
                $_cell[] = number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
            }
            $_cell[] = number_format($totalMonthAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
            $Rows[] = $_cell;
        }

        $_cell = array();
        $_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
        $totalYearAmount = 0;
        foreach ($groups as $group) {
            $amount = $totalGroupAmount[$group];
            $totalYearAmount += $amount;
            $_cell[] = number_format($amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
        }
        $_cell[] = number_format($totalYearAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
        $Rows[] = $_cell;
        ####################### V1 report START #######################
    } // End if
} // End foreach
//// Printing Report END ////


///////////////// Export END /////////////////


///////////////// Output START /////////////////
if (!empty($Rows)) {
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
} else {
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>