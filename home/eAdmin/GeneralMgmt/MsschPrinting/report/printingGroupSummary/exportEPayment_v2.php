<?php

///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT . "includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();

$startDate = $_POST['fromMonth'];
$endDate = $_POST['toMonth'];
$formID = $_POST['formID'];
///////////////// Init END /////////////////

///////////////// Helper START /////////////////
function filterYearMonth($r)
{
    global $startDate, $endDate;
    return (
        "{$startDate}-00" <= $r['RECORD_DATE'] &&
        $r['RECORD_DATE'] <= "{$endDate}-32"
    );
}

///////////////// Helper END /////////////////


///////////////// Get Group Report Data START /////////////////
$rs = $objPrinting->getGroupReportData_v2($formID);
$rs = array_filter($rs, 'filterYearMonth');

$groups = BuildMultiKeyAssoc($rs, 'GROUP_ID', 'GROUP_NAME', 1);
$students = BuildMultiKeyAssoc($rs, 'UserID', array('StudentName'));
///////////////// Get Group Report Data END /////////////////

///////////////// Get student login START /////////////////
$studentIdArr = array_keys($students);
$studentIdSql = implode("','", $studentIdArr);
$sql = "SELECT
    UserID, 
    UserLogin
FROM
    INTRANET_USER IU
WHERE
    UserID IN ('{$studentIdSql}')";
$rs2 = $objPrinting->returnResultSet($sql);

foreach ($rs2 as $r) {
    $students[$r['UserID']]['UserLogin'] = $r['UserLogin'];
}
///////////////// Get student login END /////////////////

///////////////// Calculate student group usage START /////////////////
$studentUsage = array();
foreach ($rs as $r) {
    $studentUsage[$r['UserID']][$r['GROUP_ID']] += $r['AMOUNT'];
}

$studentTotal = array();
foreach ($studentUsage as $studentId => $amounts) {
    $studentTotal[$studentId] = array_sum($amounts);
}
///////////////// Calculate student group usage END /////////////////


///////////////// Export START /////////////////
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'];
$Rows[] = $Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'];

if (!$sys_custom['ePayment']['PaymentMethod']) {
    unset($exportColumn[0][count($exportColumn[0]) - 1]);
    unset($Rows[0][count($Rows[0]) - 1]);
}

//// Export Data START ////
foreach ($students as $studentId => $studentInfo) {
    $dataRow = array(
        $studentInfo["UserLogin"],
        $studentInfo["StudentName"],
        $studentTotal[$studentId],
        '',
    );
    if ($sys_custom['ePayment']['PaymentMethod']) {
        $dataRow[] = '';
    }
    $Rows[] = $dataRow;
}


///////////////// Output START /////////////////
if (!empty($Rows)) {
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
} else {
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>