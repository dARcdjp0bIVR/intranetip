<?php
/*
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 */

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();
///////////////// Init END /////////////////


///////////////// Export START /////////////////
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['userName'];
$exportColumn = array_merge($exportColumn, array_values($groups));
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['amount'];

$Rows = array_merge($Rows, $data);


$Rows[] = '';
$Rows[] = array( str_replace('<font style="color:red;">*</font>', '*', $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']) );
///////////////// Export END /////////////////


///////////////// Output START /////////////////
if(!empty($Rows)){
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
}else{
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>