<?php
/*
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 */

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();
///////////////// Init END /////////////////


///////////////// Export START /////////////////
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['userName'];
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'];
foreach($groupIdArr as $groupID){
    $exportColumn[] = $allGroupName[$groupID];
}
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['amount'];
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'];


foreach((array)$allStudentArr as $student){
    $_cell = array();
    
    $studentName = "{$student['ClassName']}({$student['ClassNumber']}) {$student['StudentName']}";
    $_cell[] = str_replace('<span class="tabletextrequire">*</span>', '*', $studentName);
    // 	$_cell[] = ($totalBalanceArr[ $student['UserID'] ])? $totalBalanceArr[ $student['UserID'] ] : '0' ;
    
    $rowTotalAmount = 0;
    foreach($groupIdArr as $groupID){
        $_amount = $currentYearUsage[ $groupID ][ $student['UserID'] ];
        $rowTotalAmount += $_amount;
        $_cell[] = ($_amount)? number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;';
    }
    $_cell[] = ($rowTotalAmount)? number_format($rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;';
    // 	$_cell[] = ($totalBalanceArr[ $student['UserID'] ] - $rowTotalAmount)? number_format( $totalBalanceArr[ $student['UserID'] ] - $rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;';
    
    $Rows[] = $_cell;
}

$Rows[] = '';
$Rows[] = array( str_replace('<font style="color:red;">*</font>', '*', $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']) );
///////////////// Export END /////////////////


///////////////// Output START /////////////////
if(!empty($Rows)){
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
}else{
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>