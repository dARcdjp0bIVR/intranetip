<?php
/**
 * Change Log:
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 * 2016-01-18 (Pun)
 *  - Added export for ePrinting
 */

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// SQL Safe START /////////////////
$formID = IntegerSafe($formID);
///////////////// SQL Safe END /////////////////


///////////////// Init START /////////////////
$currentAcademicYear = Get_Current_Academic_Year_ID();
$form_class_manage = new form_class_manage();

$academicYearHTML = getCurrentAcademicYear();
$Year = new Year($formID);
$formName = $Year->Get_Year_Name();

$allGroupName = $objPrinting->getAllGroupNameArr($currentAcademicYear);
$onBackPress = 'history.back()';
$printViewLink = '?t=report.printingGroupSummary.result&printView=1&formID='.$formID;
$exportViewLink = '?t=report.printingGroupSummary.result&exportView=1&formID='.$formID;
///////////////// Init END /////////////////


///////////////// Validate START /////////////////
///////////////// Validate END /////////////////


///////////////// Get Group Report Data START /////////////////
$data = $objPrinting->getGroupReportData($formID);

$groupIdArr = array_keys($data['CurrentYearUsage']);
$allStudentArr = $data['StudentList'];
$totalBalanceArr = $data['TotalBalance'];
$currentYearUsage = $data['CurrentYearUsage'];
///////////////// Get Group Report Data END /////////////////


//////////////////// UI START ////////////////////
if($printView){
    include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
    
    $printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
    
    $printHTML = <<<HTML
    
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td>
	<div class="print_hide" align="right">
	<br />
	$printButton
	<br />
	<br />
	</div>
HTML;
    echo $printHTML;
    $tableStyle = 'common_table_list_v30';
}else if($exportView){
    include('export.php');
    exit;
}else{
    $CurrentPage = "ReportPrintGroupSummary";
    $TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingGroupSummary'], "", 0);
    $MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
    
    $linterface->LAYOUT_START($Msg);
    $tableStyle = 'displayData';
    
    $c_ui = new common_ui();
    $js_css = $c_ui->Include_JS_CSS();
    echo $js_css;
    
    ######## Export to EPayment selection START ########
    $fromMonthHTML = $objPrinting->getYearMonthSelection('fromMonth');
    
    $endYear = date('Y');
    $endMonth = (date('m') == 8)? 7 : date('m');
    $endMonth = str_pad($endMonth,2,'0',STR_PAD_LEFT);
    $defaultSelected = "{$endYear}-{$endMonth}";
    $toMonthHTML = $objPrinting->getYearMonthSelection($htmlName = 'toMonth',$startMonth='', $endMonth='', $defaultSelected);
    ######## Export to EPayment selection END ########
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
<?php if(!$printView){ ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['year']?></td>
					<td class="tabletext" width="70%"><?=$academicYearHTML?></td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['form']?></td>
					<td class="tabletext" width="70%"><?=$formName?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="<?=count($groupIdArr)+4?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				
				<tr>
					<td>
						<div class="Conntent_tool">
							<a href="<?=$printViewLink?>" target="_blank" class="print tablelink" style="float:none;display:inline;"><?=$Lang['Btn']['Print']?></a>&nbsp;
							<a href="<?=$exportViewLink?>" target="_blank" class="export tablelink" style="float:none;display:inline;"><?=$Lang['Btn']['Export']?></a>&nbsp;
							<a href="javascript:showExportToEPaymentThickBox();" class="export tablelink" style="float:none;display:inline;"><?=$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['title']?></a>&nbsp;
						</div>
					</td>
				</tr>
<?php }else{ ?>
	<tr>
		<td colspan="2">
			<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">
				<?=$formName . ' ' . $Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord']?> (<?=$academicYearHTML?>)
			</div>
		</td>
	</tr>
<?php } ?>
				<!--tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount']?></td>
					<td class="tabletext" width="70%"><?=number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep'])?></td>
				</tr>
				<tr>
					<td><?=$linterface->GET_NAVIGATION2($Lang['MsschPrint']['report']['printingSummary']['result']['printingRecord'])?></td>
				</tr-->
	<tr>
		<td colspan="2">

<!-- -------------------- Report All Class START -------------------- -->
			<table class="<?=$tableStyle?>" border="0" cellspacing="0" cellpadding="5" width="100%">	
							
<thead>	
				<tr>
					<th class="tablebluetop tabletopnolink" nowrap>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['userName']?>
					</td>

					<!--th class="tablebluetop tabletopnolink" style="text-align:center" nowrap>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance']?>
					</td-->

					<?php foreach($groupIdArr as $groupID){ ?>
						<th class="tablebluetop tabletopnolink" style="background-color:#6BB357;text-align:center" nowrap><?=$allGroupName[$groupID]?></td>
					<?php } ?>

					<th class="tablebluetop tabletopnolink" style="text-align:center" nowrap>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['amount']?>
					</td>

					<!--th class="tablebluetop tabletopnolink" style="text-align:center" nowrap>
						<?=$Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance']?>
					</td-->
				</tr>
</thead>
				<!-- -------- Data Display START -------- -->
				<?php 
				$index = 0;
				foreach((array)$allStudentArr as $student){ 
				?>
					<tr class="tablebluerow<?=($index++ % 2 + 1)?>">
						<td valign="top" class="tabletext"><?=$student['ClassName']?>(<?=$student['ClassNumber']?>) <?=$student['StudentName']?></td>
						<!--td valign="top" class="tabletext" style="text-align:center"><?= ($totalBalanceArr[ $student['UserID'] ])? $totalBalanceArr[ $student['UserID'] ] : '0' ?></td-->
						<?php 
						$rowTotalAmount = 0;
						foreach($groupIdArr as $groupID){ 
							$_amount = $currentYearUsage[ $groupID ][ $student['UserID'] ];
							$rowTotalAmount += $_amount;
						?>
							<td class="tabletext" style="text-align:center"><?= ($_amount)? number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
						<?php 
						}
						?>
						<td class="tabletext" style="text-align:center"><?= ($rowTotalAmount)? number_format($rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?></td>
						<!--td valign="top" class="tabletext" style="text-align:center">
							<?= ($totalBalanceArr[ $student['UserID'] ] - $rowTotalAmount)? number_format( $totalBalanceArr[ $student['UserID'] ] - $rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']) : '&nbsp;' ?>
						</td-->
					</tr>
				<?php 
				} 
				?>
				<!-- -------- Data Display END -------- -->

			</table>
			<br />
				<tr>
					<td><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></td>
				</tr>
<!-- -------------------- Report All Class END -------------------- -->
			<?php if(!$printView){ ?>
				<tr>
					<td height="1" class="dotline" colspan="<?=count($groupIdArr)+4?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
	
				<tr>
					<td height="1" colspan="2" align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button',$ParOnClick=$onBackPress, $ParName="back")?>&nbsp;</td>
				</tr>
			<?php } ?>

		</td>
	</tr>
</table>

<div id="exportToEPayment" style="display: none;">
	<div class="edit_pop_board edit_pop_board_reorder" style="height: auto;">
		<br />
		<form class="ePaymentForm" action="index.php" target="_blank" method="POST">
			<input type="hidden" name="t" value="report.printingGroupSummary.exportEPayment" />
			<input type="hidden" name="formID" value="<?=$formID ?>" />
			
			<table class="form_table">
				<tr>
					<td><?=$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['startMonth'] ?></td>
					<td>
						<?=$fromMonthHTML ?>
					</td>
				</tr>
				<tr>
					<td><?=$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['endMonth'] ?></td>
					<td>
						<?=$toMonthHTML ?>
					</td>
				</tr>
			</table>
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td valign="top" nowrap="nowrap" class="tabletextremark">
						<?=$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['importHint'] ?>
					</td>
					<td width="80%">&nbsp;</td>
				</tr>
			</table>
		</form>
		<div class="edit_bottom" style="margin-top: 10px;">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", 
						$onclick="Check_EPayment_Form()", $id="Btn_Save") ?>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
						$onclick="js_Hide_ThickBox()", $id="Btn_Cancel") ?>
		</div>
	</div>
</div>


<script type="text/javascript" src="/templates/jquery/jquery.fixedheader.js"></script>
<script type="text/javascript">
	$('.displayData').each(function(){
		new FixedHeader( this );
	});


	function showExportToEPaymentThickBox(){
		js_Show_ThickBox('<?=$Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['title'] ?>',180, 380);
		$('#TB_ajaxContent').html( $('#exportToEPayment').html() );
	}
	function Check_EPayment_Form(){
		$('#TB_ajaxContent').find('.ePaymentForm').submit();
		js_Hide_ThickBox();
	}
	
</script>

<?php 
if(!$printView){ 
	$linterface->LAYOUT_STOP();
}
?>