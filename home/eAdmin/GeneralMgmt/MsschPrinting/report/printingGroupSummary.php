<?php


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
$resultPath = 'report.printingGroupSummary.result_v2';
$form_class_manage_ui = new form_class_manage_ui();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
///////////////// Init END /////////////////


///////////////// Validate START /////////////////
///////////////// Validate END /////////////////


///////////////// UI Field START /////////////////
$academicYearHTML = getCurrentAcademicYear();

$formHTML = $form_class_manage_ui->Get_Form_Selection($SelectionID = 'formID', $SelectedYearID='', $Onchange='', $noFirst=0, $isAll=0, $isMultiple=0);
///////////////// UI Field END /////////////////


//////////////////// UI START ////////////////////
$CurrentPage = "ReportPrintGroupSummary";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingGroupSummary'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Msg);
?>


<form id="form1" method="POST" action="index.php">
	<input type="hidden" name="t" value="<?=$resultPath?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="1000px">
			
<!-- -------------------- Form Element START -------------------- -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['academicYear']?></td>
					<td class="tabletext" width="70%"><?=$academicYearHTML?></td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['form']?></td>
					<td class="tabletext" width="70%"><?=$formHTML?></td>
				</tr>
<!-- -------------------- Form Element END -------------------- -->

				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'submit')?>&nbsp;</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</form>

<script type="text/javascript">
$('#form1').submit(function(e){
	if( $('#formID').val() == ''){
		alert('<?=$Lang['MsschPrint']['report']['printingSummary']['searchErr']['emptyForm']?>');
		e.preventDefault();
	}
});
</script>


<?php
$linterface->LAYOUT_STOP();
?>