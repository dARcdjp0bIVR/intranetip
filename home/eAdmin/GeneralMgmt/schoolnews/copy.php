<?php
# Using :

############ Change Log [Start] ############
#
#   2020-01-17 Bill	    [2020-0113-1406-25226]
#   fixed: DHL PICs cannot access
#
#   2019-04-29 Anna
#   $AnnouncementID[0] avoid sql injection
#
#   2018-12-27 Anna
#   added access right for $sys_custom['DHL'] PIC
#
# 	2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
############ Change Log [End] ############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

// [2020-0113-1406-25226]
// if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) ||  ($isPIC && $sys_custom['DHL'])){
if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$li = new libdb();

$copy_from = IntegerSafe($AnnouncementID[0]);
$la = new libannounce($copy_from);
$lf = new libfilesystem();

# Copy INTRANET_ANNOUNCEMENT
$sql = "insert into INTRANET_ANNOUNCEMENT 
        (Title, Description, AnnouncementDate, EndDate, UserID, PosterName, RecordType, RecordStatus, OwnerGroupID, Internal, SystemAdminLogin, PublicStatus, onTop)
        select Title, Description, AnnouncementDate, EndDate, UserID, PosterName, RecordType, RecordStatus, OwnerGroupID, Internal, SystemAdminLogin, PublicStatus, onTop from INTRANET_ANNOUNCEMENT where AnnouncementID=$copy_from";
$li->db_db_query($sql) or die(mysql_error());
$AnnouncementID = $li->db_insert_id();

# Update Date Input/Modified
$sql = "update INTRANET_ANNOUNCEMENT set DateInput=now(), DateModified=now() WHERE AnnouncementID = '{$AnnouncementID}'";
$li->db_db_query($sql);

$folder = session_id()."_a";
$path = "$file_path/file/announcement/$folder$AnnouncementID";
$lf->folder_new ($path);

# Copy attachment and powerVoice files
if(!empty($la->Attachment) || !empty($la->VoiceFile))
{
    $old_path = "$file_path/file/announcement/$la->Attachment$copy_from";
    $copy_files = $lf->folder_content_copy($old_path, $path);

    # Attachment
    if(!empty($la->Attachment))
    {
        $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '". $folder ."' WHERE AnnouncementID = '{$AnnouncementID}'";
        $li->db_db_query($sql);
    }

    # PowerVoice
    if(!empty($la->VoiceFile))
    {
        $loc = $la->VoiceFile;

        # retrieve VoiceFile filename
        $pos = strrpos($loc, '/', 1);
        $voiceFile = substr($loc, $pos+1, strlen($loc));
        $des = $path."/".$voiceFile;

        $sql = 	"UPDATE INTRANET_ANNOUNCEMENT SET VoiceFile = '".addslashes($des)."' WHERE AnnouncementID = '{$AnnouncementID}'";
        $li->db_db_query($sql);
    }
}

# Copy INTRANET_GROUPANNOUNCEMENT
$sql = "select GroupID from INTRANET_GROUPANNOUNCEMENT where AnnouncementID=$copy_from";
$GroupAry = $li->returnVector($sql);
if(!empty($GroupAry))
{
    foreach($GroupAry as $k=>$d)
    {
        $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$d.", $AnnouncementID)";
        $li->db_db_query($sql);
    }
}

intranet_closedb();
header("Location: edit.php?AnnouncementID=$AnnouncementID&copied=1");
?>