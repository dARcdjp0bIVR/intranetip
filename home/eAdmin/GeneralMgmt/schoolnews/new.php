<?php
# using: 

############ Change Log [Start] ############
#   2020-03-05 (Ray)
#   added website code
#
#   2019-09-10 (Tommy)
#   added display on top checkbox for "display on top" function
#
#   2019-01-09 (Anna)
#   Added $sys_custom['SchoolNews']['DisplayEngTitleAndContent']
#
#   2018-12-27 Anna
#   added access right for $sys_custom['DHL'] PIC
#
#   2018-12-18 Anna
#   $sys_custom['DHL']  = hide Target_allUser 
#
#   2018-08-21 Anna
#   Remove title max length
#
#   2018-05-03 Anna
#   add when default target is whole school,change to specific group, default checked push_message_alert after setting sendPushMsg
#
#	2017-09-26	Bill	[2017-0904-1103-25073]
#	add option "Send eClass App push message to student's guardian as well"
#
#	2017-07-03	Carlos
#	$sys_custom['DHL'] - Changed group target selection to common choose.
#
#	2017-04-25	Carlos
#	$sys_custom['DHL'] - Changed group target selection to DHL department groups.
#
#	2016-12-13	Villa
#	- add $sys_custom['schoolNews']['defaultSpecificTarget'] to control default setting target
#
#	2016-10-31	Villa
#	- fixing bug about Plugin
#
#	2016-10-17	Villa
#	UI Improvemnt
#		Change the position and rename the public announcement
#
#	2016-09-23	Villa 
#	UI improvement 
#		add block to push msg setting
#		add to module setting
#		"default using eclass App to notify"
#
#	2016-09-06	Bill	[2016-0906-1555-34206]
#	Fixed: js error related to date picker if without eClassApp
#
#	2016-03-03	Kenneth
#	add schedule push message 
#
# - 2015-12-20	(Kenneth)	[2015-1113-1510-52071]
#	- Move "Pending" below Notification option
#	- js to toggle disable/enable of checkbox
#
# - 2015-11-13 (Ivan)
#	improved: show send push message by batches push message remarks if enabled this cust
#
# - 2014-10-14 (Roy)
#	enhanced: add option to send push message to user
#
# - 2013-11-06 (YatWoon)
#	enhanced: add flag $special_feature['announcement_hide_email_alert'] [Case#2013-1023-1213-39177]
#
# - 2013-06-18 (YatWoon)
#	impvoed: disabled the submit button once client submit the form [Case#2013-0611-1047-19066]
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
# - 2010-11-03 YatWoon
#	end date according to the setting value
#	update to IP25 UI standard
#
# - 2010-09-08 Marcus
#	modified displayAnnouncementGroups to show ECA Groups
#
# - 2010-06-15 YatWoon
#	remove flag checking $special_announce_public_allowed, no need for this checking and assume this feature is as a general
#
# - 2010-04-13 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
# - 2010-01-04 YatWoon
#	Fixed: Cannot upload more than 5 files due to attachment_size always pass "5" to the next page
#
############ Change Log [End] ############

$CurrentPageArr['schoolNews'] = 1;

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){
    $isAdmin = true;
}

$lfilesystem = new libfilesystem();
$leClassApp = new libeClassApp();

# Temp assign memory of this page
ini_set("memory_limit", "150M");

# Create a new interface Instance
$linterface = new interface_html();

# Create a new homework Instance
$lschoolnews = new libschoolnews();

# Select Current Page
$CurrentPage = "SchoolNews";

# Page Title
$PAGE_TITLE = $Lang['SysMgr']['SchoolNews']['SchoolNews'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "");

$MODULE_OBJ = $lschoolnews->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SchoolNews']['NewRecord'], "");

# Date Setting
$today = date('Y-m-d');
$temp = strtotime($lschoolnews->defaultNumDays.' day', strtotime($today));
$preStartDate = $today;
$preEndDate = date('Y-m-d',$temp);

$lo = new libgrouping();

if ($t != "")
	echo "<p> $i_con_msg_date_wrong </p>\n";
$no_file = 1;

if ($plugin['power_voice'])
{
	$RecBtn = "<input type='button' name='pv_rec_btn' value='".$iPowerVoice['powervoice']."' onclick='editPVoice()' />";
	$ClearBtn = "<input type='button' name='pv_clear_btn' value='".$button_clear."' onclick='clearPVoice()' />";
	$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" ><input type=\"hidden\" name=\"voicePath\" >";
	$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"1\" >";
	$VoiceClip = " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif' border='0' align='absmiddle' /> ";
			
	$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\" >";
	$voiceContent .= "<tr>";
	$voiceContent .= "	<td><span id=\"voiceDiv\" ></span></td>";
	$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
	$voiceContent .= "	<td><span id=\"clearDiv\" style=\"display:none\" >{$ClearBtn}</span></td>";
	$voiceContent .= "</tr>";
	$voiceContent .= "</table>";
	
	$PVHtml = "<tr>";
	$PVHtml .= "<td class=\"field_title\">".$iPowerVoice['sound_recording']."</td>";
	$PVHtml .= "<td>".$voiceContent."</td>";
	$PVHtml .= "</tr>";
}

$htmlAry['sendInBatchRemarks'] = '';
if ($leClassApp->isEnabledSendBulkPushMessageInBatches()) {
	$htmlAry['sendInBatchRemarks'] = '<span class="tabletextremark">('.$Lang['AppNotifyMessage']['SendInBatchesRemarks'].')</span>';
}

$nowChecked = false;
$scheduledChecked = true;
$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
	$sendTime = strtotime($sendTimeString);
	if ($sendTime > $nowTime) {
		$scheduledMessageChecked = "checked";
		$nowChecked = false;
		$scheduledChecked = true;
	}
	else {
		$scheduledMessageChecked = "";
		$sendTimeString = "";
	}
}
else {
	$sendTimeString = "";
}

$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
$nextTimeslotHour = date('H', $rounded_seconds);
$nextTimeslotMinute = date('i', $rounded_seconds);
if ($sendTimeString == "") {
	$selectionBoxDate = date('Y-m-d', $nowTime);
	$selectionBoxHour = $nextTimeslotHour;
	$selectionBoxMinute = $nextTimeslotMinute;
}
else {
	$selectionBoxDate = date('Y-m-d', $sendTime);
	$selectionBoxHour = date('H', $sendTime);
	$selectionBoxMinute = date('i', $sendTime);
}	

$htmlAry['sendTimeNowRadio'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $nowChecked=true, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
$htmlAry['sendTimeScheduledRadio'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");

// [2016-0906-1555-34206] Define Date Picker for Start Date and End Date first
// Ensure datepicker.js included even client without eClassApp
$htmlAry['startTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("AnnouncementDate", ($ad==""?$preStartDate:$ad));
$htmlAry['endTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("EndDate", ($ed==""?$preEndDate:$ed));

$x = '';
$x .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$x .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
$x .= " : ";
$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
$x .= "&nbsp;&nbsp;";
$x .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
$x .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
$x .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
$htmlAry['sendTimeDateTimeDisplay'] = '<div id="specificSendTimeDiv" style='.($scheduledChecked? "":"display:none").'>';
$htmlAry['sendTimeDateTimeDisplay'] .= $x;
$htmlAry['sendTimeDateTimeDisplay'] .= '</div>';

// [2017-0904-1103-25073] Send eClass App push message to student's guardian as well
if(!$sys_custom['schoolNews']['disableSendToParentAlert']) {
	$htmlAry['sendToParentAlert'] = '<span id="stu_parent_alert">
										<br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="stu_parent_alert" id="stu_parent_alert" value="1"> <label for="stu_parent_alert">'.$Lang['SysMgr']['SchoolNews']['StudentParentAlert'].'</label>
									</span>';
}

// Send to Public checkbox
$x = '';
if ($sys_custom['SchoolNews']['SendToPublicOnly']) {
	$x .= '<input type="hidden" name="publicdisplay" id="publicdisplay" value="1">';
}
else {
	$x = '<input type="checkbox" name="publicdisplay" id="publicdisplay" onClick="groupSelection();" value="1" CHECKED> <label for="publicdisplay">'.$Lang['SysMgr']['SchoolNews']['PublicAnnouncement'].'</label>';
}
$htmlAry['publicCheckbox'] = $x;

$checked = $lschoolnews->sendPushMsg==1? "checked":""; 

### Default Sending Target Setting ###
// Default All Users
$Target_specificUser_check = "";
$Target_allUser_check = "checked";
if($sys_custom['schoolNews']['defaultSpecificTarget']){
	$Target_specificUser_check = "checked";
	$Target_allUser_check = "";
}

# Start layout
$linterface->LAYOUT_START();
echo $linterface->Include_JS_CSS();
?>

<script language="javascript">
$(document).ready(function(){
	// 2016-09-23 Villa
	<? if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
		if('<?=$lschoolnews->sendPushMsg?>'=='1'){
			$('div#PushMessageSetting').show();
			$('div#specificSendTimeDiv').show();
		}
		else{
			$('div#PushMessageSetting').hide();
		}
	 	<?php } ?>
	 	
	 	$('input:radio[name="RecordStatus"]').click(
	 		function(){
				if(this.value==0) {
					<?php if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
		 				$('input:checkbox[name="push_message_alert"]').removeAttr('checked');
		 				$('input:checkbox[name="push_message_alert"]').attr('disabled','true');
		 				$('div#specificSendTimeDiv').hide();
		 				$('div#PushMessageSetting').hide();
		 				$('input:radio[name="sendTimeMode"]').removeAttr('checked');
		 				$('input:radio[name="sendTimeMode"]').attr('disabled','true');
		 				
						if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
							$('input#stu_parent_alert').removeAttr('checked');
							$('input#stu_parent_alert').attr('disabled', 'true');
							$('span#stu_parent_alert').hide();
						}
	 				<?php } ?>
	 				
	 				$('input:checkbox[name="email_alert"]').removeAttr('checked');
	 				$('input:checkbox[name="email_alert"]').attr('disabled','true');
	 			}
	 			else if(this.value==1){
	 				var Target_allUser = document.getElementById("Target_allUser");
 					<?php if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
	 	 				$('input:checkbox[name="push_message_alert"]').removeAttr('disabled');
	 	 				$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
	 					
						if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
							$('input#stu_parent_alert').removeAttr('disabled');
							$('span#stu_parent_alert').show();
						}
					<?php } ?>
					
 	 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
//  				$('input:checkbox[name="push_message_alert"]').removeAttr('disabled');
//  				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
//  				$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
//  				$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
//  				$('div#PushMessageSetting').hide();
	 			}
	 		}
	 	);

	 	if('<?=$Target_allUser_check?>'=='checked'){
	 		$('#Target_allUser').click();
	 	}
	 	else{
	 		$('#Target_specificUser').click();
	 	}

	 	<? if ($sys_custom['DHL']){ ?>
	 	$('#Target_specificUser').click();
	 	<? }?>
	 	
});

var no_of_upload_file = <? echo $no_file==""?5:$no_file;?>;

function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
			obj.options[i] = null;
		}
	}
}

// function removeAllOption(from, to){
// 	checkOption1(from);
// 	checkOption1(to);
// 	for(i=0; i<from.length; i++){
// 		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
// 	}
// 	checkOptionClear(from);
// 	text1 = "";
// 	for(i=0; i<20; i++)
// 		text1 += " ";
// 	checkOptionAdd(from, text1, "")
// }

function checkform(obj)
{
        if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
        if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
        if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$i_EndDate; ?>.")) return false;
 		if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>.")) return false;
		if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
		if(compareDate(obj.EndDate.value,obj.AnnouncementDate.value)<0) 
		{
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		
		var today = new Date();
		var todayStr = today.getFullYear()+"-"+((today.getMonth()+1)<10?"0":"")+(today.getMonth()+1)+"-"+today.getDate();
		if(compareDate(obj.AnnouncementDate.value,todayStr)<0) 
		{
			alert ("<?=$Lang['Group']['WarnStratDayPassed']?>"); 
			return false;
		}
		
// 		var PublicDisplay = document.getElementById("publicdisplay");
// 		if(PublicDisplay != null)
// 		{
// 			var Group = document.getElementById("GroupID");
// 			if(PublicDisplay.checked == false){
// 				if(Group.length==0){
					//alert
// 					return false;
// 				}
// 			}
// 		}
		
		var Group = document.getElementById("GroupID");
		var PublicDisplay = document.getElementById("Target_specificUser");
		if(PublicDisplay.checked == true){
			if(Group.length==0){
				alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		var scheduleString = '';
   		if ($('input#sendTimeRadio_scheduled').attr('checked')) {
   	 		scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    	}
    	$('input#sendTimeString').val(scheduleString);
    	
    	var dateNow = new Date();
    	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
        if($('#push_message_alert').attr('checked')){    	
    		if ($('input#sendTimeRadio_scheduled').attr('checked') && scheduleString <= dateNowString) {
    			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
    			$('input#sendTime_date').focus();
    			return false;
    		}     
        }
    	// check if the schedule day is greater than 31days after the issue day 
    	if($('#push_message_alert').attr('checked')){
    		var maxDate = new Date();
    		maxDate.setDate(dateNow.getDate() + 31);
    		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
    		if ($('input#sendTimeRadio_scheduled').attr('checked')&& scheduleString > maxDateString) {
    			$('input#sendTime_date').focus();
    			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
    			return false;
    		}
    	}
    	
		document.getElementById('submit_btn').disabled  = true; 
		document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
		
		checkOptionAll(obj.elements["GroupID[]"]);
		checkOption1(document.getElementById("GroupID"));
        return Big5FileUploadHandler();
}

function Big5FileUploadHandler() {
	 for(i=0;i<=no_of_upload_file;i++)
	 {
		 	objFile = eval('document.form1.filea'+i);
		 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
		 	if(objFile!=null && objFile.value!='' && objUserFile!=null)
		 	{
			 	var Ary = objFile.value.split('\\');
			 	objUserFile.value = Ary[Ary.length-1];
			}
	 }
	 return true;
}

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x= '<input class="file" type="file" name="filea'+no_of_upload_file+'" size="40">';
	x+='<input type="hidden" name="hidden_userfile_name'+no_of_upload_file+'">';
	
	cell.innerHTML = x;
	no_of_upload_file++;
	
	document.form1.attachment_size.value = no_of_upload_file;
}

function back(){
  window.location="index.php";        
}

function groupSelection(){
	var obj = document.getElementById("publicdisplay");
	var group = document.getElementById("groupSelectionBox");
	if(obj.checked == false){
		checkOption1(document.getElementById("GroupID"));
		group.style.display = "block";
	}
	else{
		group.style.display = "none";
// 		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
	}
}

<?php
	if ($plugin['power_voice'])
	{ 
?>
		var NewRecordBefore = false;
		
		function listenPVoice2(fileName)
		{
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
		}
		
		function editPVoice()
		{
			var fileName = document.form1.voiceFile.value;
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
		}
		
		function clearPVoice(num)
		{
			var voiceObj = document.getElementById("voiceDiv");
			var clearObj = document.getElementById("clearDiv");
			
			voiceObj.innerHTML = "";
			document.form1.voiceFile.value = "";
			clearObj.style.display = "none";
		}
		
		function file_exists()
		{
			alert("<?=$classfiles_alertMsgFile12?>");
		}
		
		function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
		{
			var voiceObj = document.getElementById("voiceDiv");
			var clearObj = document.getElementById("clearDiv");
			
			if (fileName != "")
			{
				if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
				else
					voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
				
				if (fileEmpty=="1")
				{
					document.form1.is_empty_voice.value = 1;
				}
				else
				{
					NewRecordBefore = true;
					document.form1.is_empty_voice.value = "";
				}
				document.form1.voiceFile.value = fileName;
				document.form1.voicePath.value = filePath;
				clearObj.style.display = "block";
			}
		}
<?php
	} 
?>

<? if( $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
function clickedSendTimeRadio(targetType) {
	if (targetType == 'now') {
		$('div#specificSendTimeDiv').hide();
	}
	else {
		$('div#specificSendTimeDiv').show();
	}
}

function checkedSendPushMessage(checkBox) {
	if (checkBox.checked == true) {
		$('div#sendTimeSettingDiv').show();
		$('div#PushMessageSetting').show();
		
		if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
			$('input#stu_parent_alert').removeAttr('disabled');
			$('span#stu_parent_alert').show();
		}
	}
	else {
		$('div#sendTimeSettingDiv').hide();
		$('div#PushMessageSetting').hide();
		
		if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
			$('input#stu_parent_alert').removeAttr('checked');
			$('input#stu_parent_alert').attr('disabled', 'true');
			$('span#stu_parent_alert').hide();
		}
	}
	
	if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
		if(document.getElementById('Target_allUser').checked) {
			$('input#stu_parent_alert').removeAttr('checked');
			$('input#stu_parent_alert').attr('disabled', 'true');
			$('span#stu_parent_alert').hide();
		}
	}
}

function syncIssueTimeToPushMsg(){
	var issueDate = $('#AnnouncementDate').val();
	$('#sendTime_date').val(issueDate);
}
<?php }?>

	// villa 2016-10-17
	function checkingStatus(radioBtn){
		if(radioBtn.checked==true){
			<? if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
				if($("input#push_message_alert").attr("checked")) {
					$('div#sendTimeSettingDiv').show();
					$('div#PushMessageSetting').show();
				}
				
				if('<?=$sys_custom['schoolNews']['enableSendingPushMsgToAllUser']?>'!='1'){
					$('input#push_message_alert').removeAttr('checked');
					$('span#push_message_alert').hide();
					$('div#sendTimeSettingDiv').hide();
					$('div#PushMessageSetting').hide();
				}
				
				// for Target : Specific Group only
				if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
					$('input#stu_parent_alert').removeAttr('checked');
					$('input#stu_parent_alert').attr('disabled', true);
					$('span#stu_parent_alert').hide();
				}
			<?php } ?>
			
			if('<?=$sys_custom['schoolNews']['enableSendingEmailToAllUser']?>'!='1'){
// 				$('#email_alert').attr('disabled',true);
				$('input#email_alert').removeAttr('checked');
				$('span#email_alert').hide();
			}
			
			$('#groupSelectionBox').hide();
// 			removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
		}
		else{
// 			$('#email_alert').removeAttr('disabled');
// 			$('#push_message_alert').removeAttr('disabled');
			<? if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
    			if('<?= $lschoolnews->sendPushMsg ?>'=='1'){
    				$("input#push_message_alert").attr("checked",true)		
    			}
				$('span#push_message_alert').show();
				if($("input#push_message_alert").attr("checked")){
					$('div#sendTimeSettingDiv').show();
					$('div#PushMessageSetting').show();
				}
				
				if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
					$('input#stu_parent_alert').removeAttr('disabled');
					$('span#stu_parent_alert').show();
				}
			<?php } ?>
			
			$('span#email_alert').show();
			$('#groupSelectionBox').show();
			checkOption1(document.getElementById("GroupID"));
		}
	}
</script>

<style> 
select option {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 400px;
}
/*
 * Villa Prevent the name of option in the selection box break the layout
*/
</style>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?> 

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
	<table class="form_table_v30">
		<!-- Start Date -->
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['StartDate']?></td>
			<td><?=$htmlAry['startTimeDateTimeDisplay']?></td>
		</tr>
		
		<!-- End Date -->
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['EndDate']?></td>
			<td><?=$htmlAry['endTimeDateTimeDisplay']?></td>
		</tr>

		<?php if($sys_custom['SchoolNews']['ShowWebsiteCode']) { ?>
		<tr>
			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['WebsiteCode']?></td>
			<td>
				<input type="text" name="WebsiteCode" value="<?=$WebsiteCode?>" size="200"  class="textboxtext">
			</td>
		</tr>
		<?php } ?>

		<!-- Title -->
		<tr>
		   	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['Title']?></td>
			<td>
				<input type="text" name="Title" value="<?=$t?>" size="30"  class="textboxtext">
			</td>
		</tr>
		
		<?php if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){ ?>	
		<!-- English Title -->
		<tr>
		  	<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['TitleEng']?> </td>
			<td>
				<input type="text" name="TitleEng" value="<?=$enTitle?>" size="30" class="textboxtext">
			</td>
		</tr>
		<?php }?>
		
		
		<!-- Description -->
		<tr>
			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['Description']?></td>
			<td align="left" valign="top">
			<?
			  	if($publishEclassNews) {	# from eClass new feature bubble
			  		//$url = "http://192.168.0.146:31002/home/abc.htm";
// 			  		$Content = file_get_contents($url);
					//if(file_exists('http://support.broadlearning.com/doc/help/central/bubble_news/latest_news_zhtw.html')) {
					if($intranet_session_language=="en") {
			  			$Content = file_get_contents('http://support.broadlearning.com/doc/help/central/bubble_news/latest_news_en.html');
					}
					else {
			  			$Content = file_get_contents('http://support.broadlearning.com/doc/help/central/bubble_news/latest_news_zhtw.html');  			
			  		}
			  		//}
			  	}
			  	
				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
				//$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash");
				$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", intranet_undo_htmlspecialchars($Content));
				$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
				$objHtmlEditor->Create();
			?>
			</td>
		</tr>
		
		
		<?php if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){ ?>	

    			<!-- Description English-->
    		<tr>
    			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['DescriptionEng']?></td>
    			<td align="left" valign="top">
    			<? /* ?><textarea name="Description" cols="60" rows=10><?=$cDescription?></textarea><? */ ?>
    			<?
    				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
    				$objHtmlEditor = new FCKeditor ( 'DescriptionEng' , "100%", "320", "", "Basic2_withInsertImageFlash", $enDescription);
    				$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
    				$objHtmlEditor->Create();
    			?>
    			</td>
    		</tr>
		<?php } ?>
		
		<!-- Sound Recorder -->
		<?=$PVHtml?>
		
		<!-- Attachment -->
		<tr>
			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['Attachment']?></td>
			<td>
				<? /* <input type="hidden" name="attachment_size" value="0"> */ ?>
				<table id="upload_file_list" border="0" cellpadding="0" cellpadding="0">
					<script language="javascript">
						for(i=0;i<no_of_upload_file;i++)
						{
							document.writeln('<tr><td><input class="file" type="file" name="filea'+i+'" size="40">');
							document.writeln('<input type="hidden" name="hidden_userfile_name'+i+'"></td></tr>');
						}
					</script>
				</table>
				<input type="button" value=" + " onClick="add_field()">
			</td>
		</tr>
		<!-- End of Attachment -->
		
		<!-- Target -->
		<tr>
			<td class="field_title" ><?=$Lang['SchoolNews']['target']?></td>
			<td>
				<table class="form_table_v30">
					<tr>
					
						<td colspan='3'>
						<?php if(!$sys_custom['DHL'] || ($sys_custom['DHL'] && $isAdmin) ){?>
						    <input type="radio" name="Target_user" id="Target_allUser" value="1" <?=$Target_allUser_check?> onClick="javascript:checkingStatus(this);"><label for="Target_allUser"><?=$Lang['eSurvey']['public']?></label>
						
						<?php  } ?>
						<input type="radio" name="Target_user" id="Target_specificUser" value="0" <?=$Target_specificUser_check?> onClick="javascript:checkingStatus($('#Target_allUser'));"><label for="Target_specificUser"><?=$Lang['SchoolNews']['SpecificGroup']?></label>
						</td>
					</tr>
					<tr id='groupSelectionBox'>
						<td class="field_title"	valign="top">
							<?=$i_admintitle_group?>
						</td>
						<td>
						<?php 
						if($sys_custom['DHL']){
							include_once($intranet_root."/includes/DHL/libdhl.php");
							$libdhl = new libdhl();
							//$group_selection = $libdhl->getIntranetGroupSelection("AvailableGroupID", "AvailableGroupID[]", "", $___isMultiple=true, $____hasFirst=false, $____firstText='', $____tags=' size="10" ', $____valuePrefix='');
							
							$x = "<table width=100% border=0 cellpadding=5 cellspacing=0 class=\"inside_form_table\">\n";
                        	$x .= "<tr>\n";
                        		$x .= "<td class=tableContent width=50%>\n";
                        			$x .= $libdhl->getIntranetGroupSelection("GroupID", "GroupID[]", array(0), $i___sMultiple=true, $____hasFirst=false, $____firstText='', $____tags=' size="10" style="min-width:18em;" " ', $____valuePrefix='');
                        			$x .= "<div>\n";
                        			$x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=GroupID[]&SelectGroupOnly=1&page_title=SelectAudience&DisplayGroupCategory=1&isAdmin=$isAdmin',16);");
                        			$x .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "remove_selection_option('GroupID',0);");
                        			$x .= "</div>\n";
                        		$x .= "</td>\n";
                        	/*	
                        	$x .= "<td class=tableContent align=center>\n";
                        		$x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "submit", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['GroupID[]']);return false;", "submit2") . "<br /><br />";
                        		$x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "submit", "checkOptionTransfer(this.form.elements['GroupID[]'],this.form.elements['AvailableGroupID[]']);return false;", "submit23");
                        	$x .= "</td>\n";
                        	$x .= "<td class=tableContent width=50%>\n";
                        		$x .= $group_selection;
                        	$x .= "</td>\n";
                        	*/
                        	$x .= "</tr>\n";
                        	$x .= "</table>\n";
                        	echo $x;
						}
						else{
						?>
							<?=$lo->displayAnnouncementGroups(0, 1)?>
						<?php } ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- Target End	-->
		
		<!-- Record Status -->
		<tr>
			<td class="field_title" rowspan="2"><?=$Lang['SysMgr']['SchoolNews']['Status']?></td>
			<td>
				<input type="radio" name="RecordStatus" id="RecordStatus0" value="0"> <label for="RecordStatus0"><?=$Lang['SysMgr']['SchoolNews']['StatusPending']?></label>
				<input type="radio" name="RecordStatus" id="RecordStatus1" value="1" CHECKED> <label for="RecordStatus1"><?=$Lang['SysMgr']['SchoolNews']['StatusPublish']?></label>
			  	
			  	<? if(!$special_feature['announcement_hide_email_alert'] || $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
			  	</br>
					<? if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
						<span id='push_message_alert' style='margin-left:20px;'>
							<input type="checkbox" name="push_message_alert" id="push_message_alert" value="1" onclick="checkedSendPushMessage(this);" <?= $checked?>> <label for="push_message_alert"><?=$Lang['SysMgr']['SchoolNews']['PushMessageAlert']?> <?=$htmlAry['sendInBatchRemarks']?></label>
							<div id= 'PushMessageSetting'>
								<?php echo $linterface->Get_Warning_Message_Box(
									$Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'],
									'<div id="sendTimeSettingDiv">'.
											$htmlAry['sendTimeNowRadio'].'<br>'.
											$htmlAry['sendTimeScheduledRadio'].$htmlAry['sendTimeDateTimeDisplay'].
											$htmlAry['sendToParentAlert']
								);?>
							</div>
							</span><br>
					<!--	<div id="sendTimeSettingDiv" style="display:none">  -->
					<!--		//$htmlAry['sendTimeNowRadio'] -->
					<!--	 	<br/> -->
					<!--	 	//$htmlAry['sendTimeScheduledRadio'] -->
					<!--	  	//$htmlAry['sendTimeDateTimeDisplay'] -->
					<!--	</div> -->
					<!--	</span> -->
					<!--	<br /> -->
					<? }?>
					
					<? if(!$special_feature['announcement_hide_email_alert']) {?>
						<span id='email_alert' style='margin-left:20px;'>
							<input type="checkbox" name="email_alert" id="email_alert" value="1"> <label for="email_alert"><?=$Lang['SysMgr']['SchoolNews']['EmailAlert']?></label>
						</span>
						<br />
						<span id='email_alert' style='margin-left:20px;'>(<?=$Lang['eNotice']['NotifyRemark']?>)</span>
					<? }?>
				<? } ?>
			</td>
		</tr>
		<!-- Record Status End -->
		
		<!-- Display On Top -->
		<tr></tr>
		<tr>
			<td class="field_title" rowspan="2"><?=$Lang['SysMgr']['SchoolNews']['DisplayOnTop']?> </td>
			<td class="tabletext">
				<input type="checkbox" name="onTop" value="1">
			</td>
		</tr>
		
		<!-- Display On Top End -->
	</table>
	
	<!-- Groups --> 
	
	<?=$linterface->MandatoryField();?>
	<div class="edit_bottom_v30">
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit_btn")?> &nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back()")?>
	</div>
	
	<input type="hidden" name='attachment_size' value='<? echo $no_file==""?5:$no_file;?>'>
	<input type="hidden" name="is_empty_voice" value='' />
</form>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>