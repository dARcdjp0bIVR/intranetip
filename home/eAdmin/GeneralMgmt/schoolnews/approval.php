<?php
# using: henry chow

############ Change Log [Start] ############
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
############# Change Log [End] #############

	$PATH_WRT_ROOT = "../../../../";
	$CurrentPageArr['schoolNews'] = 1;
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

	intranet_auth();
	intranet_opendb();
	
	if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])){
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
	}
	
	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	# Create a new interface instance
	$linterface = new interface_html();

	# Create a new homework instance
	$lschoolnews = new libschoolnews();
	
	if($type==1){
		$sql = "UPDATE INTRANET_ANNOUNCEMENT_APPROVAL SET RecordStatus = 1 WHERE AnnouncementID = ".$announcementID;
		$result = $lschoolnews->db_db_query($sql);
		if($result){
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET RecordStatus = 1 WHERE AnnouncementID = ".$announcementID;
			$result = $lschoolnews->db_db_query($sql);
		}
	}
	
	else if($type==3){
		$sql = "UPDATE INTRANET_ANNOUNCEMENT_APPROVAL SET RecordStatus = 3 WHERE AnnouncementID = ".$announcementID;
		$result = $lschoolnews->db_db_query($sql);
		if($result){
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET RecordStatus = 3 WHERE AnnouncementID = ".$announcementID;
			$result = $lschoolnews->db_db_query($sql);
		}
		
	}
	intranet_opendb();
	
	echo $result;
?>