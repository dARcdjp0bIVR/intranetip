<?php
# using: 

############ Change Log [Start] ############
#   2020-03-05 (Ray)
#   added website code
#
#   2019-09-10 (Tommy)
#   added display on top checkbox for "display on top" function
#
#   2019-01-09 (Anna)
#   Added $sys_custom['SchoolNews']['DisplayEngTitleAndContent']
#
#   2018-12-27 (Anna)
#   added access right for $sys_custom['DHL'] PIC
#
#	2017-09-26	Bill	[2017-0904-1103-25073]
#	add option "Send eClass App push message to student's guardian as well"
#
#	2017-07-03	Carlos
#	$sys_custom['DHL'] - Changed group target selection to common choose.
#
#	2017-04-25	Carlos
#	$sys_custom['DHL'] - Changed group target selection to DHL department groups.
#
#	2016-10-31	Villa
#	- fixing bug about Plugin
#
#	2016-10-17	Villa
#	UI Improvemnt
#		Change the position and rename the public announcement
#
#	2016-09-23	Villa 
#	UI improvement 
#	add block to push msg setting
#
#	2016-09-06	Bill	[2016-0906-1555-34206]
#	Fixed: js error related to date picker if without eClassApp
#
#	2016-03-03	Kenneth
#	add schedule push message 
#
# - 2015-12-20	(Kenneth)	[2015-1113-1510-52071]
#	- Move "Pending" below Notification option
#	- js to toggle disable/enable of checkbox
#
# - 2015-11-13 (Ivan)
#	improved: show send push message by batches push message remarks if enabled this cust
#
# - 2014-10-14 (Roy)
#	enhanced: add option to send push message to user
#
# - 2014-03-24 (YatWoon)
#	improved: change date selection to date_picker [Case#J59973]
#
# - 2013-06-18 (YatWoon)
#	impvoed: disabled the submit button once client submit the form [Case#2013-0611-1047-19066]
#
# - 2012-03-01 (YatWooon)
#	update attachment coding, need to support by Chrome and FF
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
# - 2010-11-03 YatWoon
#	update to IP25 UI standard
#
# - 2010-06-15 YatWoon
#	remove flag checking $special_announce_public_allowed, no need for this checking and assume this feature is as a general
#
# - 2010-04-13 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
############ Change Log [End] ############

$CurrentPageArr['schoolNews'] = 1;

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){
    $isAdmin = true;
}

$lfilesystem = new libfilesystem();
$leClassApp = new libeClassApp();

# Temp assign memory of this page
ini_set("memory_limit", "150M");

# Create a new interface Instance
$linterface = new interface_html();

# Create a new homework Instance
$lschoolnews = new libschoolnews();

# Select Current Page
$CurrentPage = "SchoolNews";

# Page Title
$PAGE_TITLE = $Lang['SysMgr']['SchoolNews']['SchoolNews'];
$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "");

$MODULE_OBJ = $lschoolnews->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SchoolNews']['EditRecord'], "");

# Load Announcment Settings
$AnnouncementID = (is_array($AnnouncementID)? $AnnouncementID[0] : $AnnouncementID);
$li = new libannounce($AnnouncementID);

$lo = new libgrouping();
$number_of_groups = sizeof($lo->returnGroups());

$RecordStatus0 = ($li->RecordStatus==0) ? "CHECKED" : "";
$RecordStatus1 = ($li->RecordStatus==1) ? "CHECKED" : "";

$displayAttachment = $i_AnnouncementNoAttachment;
if ($li->Attachment != "") {
	$displayAttachment = $li->displayAttachmentEdit("file2delete[]");
}

if ($invalid==1) {
	echo "<p> $i_con_msg_date_wrong </p>\n";
}

if ($ea==1)   // Editing Attachment
{
	$cTitle = stripslashes($Title);
	$cDescription = stripslashes($Description);
	$cStartDate = $AnnouncementDate;
	$cEndDate = $EndDate;
}
else
{
	$cTitle = $li->Title;
	$cDescription = htmlspecialchars_decode($li->Description);
	if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
	    $enTitle = $li->TitleEng;
	    $enDescription = htmlspecialchars_decode($li->DescriptionEng);
	}
	if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
		$WebsiteCode = $li->WebsiteCode;
	}
	$cStartDate = $li->AnnouncementDate;
	$cEndDate = $li->EndDate;
}

if ($li->RecordType == 1)
{
	$public_checked = "CHECKED";
	$specific_checked = "";
}
else
{
	$public_checked = "";
	$specific_checked = "CHECKED";
}

if($li->onTop == 1){
    $onTop_checked = "CHECKED";
}else{
    $onTop_checked = "";
}

if ($plugin['power_voice'])
{
	$RecBtn = "<input type='button' name='pv_rec_btn' value='".$iPowerVoice['powervoice']."' onclick='editPVoice()' />";
	$ClearBtn = "<input type='button' name='pv_clear_btn' value='".$button_clear."' onclick='clearPVoice()' />";
	$VoiceClip = " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif' border='0' align='absmiddle' /> ";
			
	if ($li->VoiceFile != "")
	{
		$VoiceFileName = get_file_basename($li->VoiceFile);
		//$VoiceFileDir = substr($li->VoiceFile,0,strlen($VoiceFileName));
		$VoiceFileDir = dirname($li->VoiceFile);
		
		//$VoiceFileLink = "<a href='".str_replace($intranet_root,"",$li->VoiceFile)."' target='_blank' />".$VoiceFileName."</a>";
		$VoiceFileLink = "<a href=\"javascript:listenPVoice('".str_replace($intranet_root,"",$li->VoiceFile)."')\"'  />".$VoiceClip.$VoiceFileName."</a>";
		$CurVoiceFileLink = str_replace($intranet_root,"",$li->VoiceFile);
		
		$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" value=\"{$VoiceFileName}\" ><input type=\"hidden\" name=\"voicePath\" value=\"{$VoiceFileDir}\" >";
		$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"\" >";
		
		$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\">";
		$voiceContent .= "<tr>";
		$voiceContent .= "	<td><span id=\"voiceDiv\" >".$VoiceFileLink."</span>&nbsp;&nbsp;</td>";
		$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
		$voiceContent .= "	<td><span id=\"clearDiv\" >{$ClearBtn}</span></td>";
		$voiceContent .= "</tr>";
		$voiceContent .= "</table>";
	}
	else
	{
		$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile\" ><input type=\"hidden\" name=\"voicePath\" >";
		$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere\" value=\"1\" >";	
		
		$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\">";
		$voiceContent .= "<tr>";
		$voiceContent .= "	<td><span id=\"voiceDiv\" ></span></td>";
		$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
		$voiceContent .= "	<td><span id=\"clearDiv\" style=\"display:none\" >{$ClearBtn}</span></td>";
		$voiceContent .= "</tr>";
		$voiceContent .= "</table>";
	}
	
	$PVHtml = "<tr>";
	$PVHtml .= "<td class=\"field_title\">".$iPowerVoice['sound_recording']."</td>";
	$PVHtml .= "<td>".$voiceContent."</td>";
	$PVHtml .= "</tr>";
}

$htmlAry['sendInBatchRemarks'] = '';
if ($leClassApp->isEnabledSendBulkPushMessageInBatches()) {
	$htmlAry['sendInBatchRemarks'] = '<span class="tabletextremark">('.$Lang['AppNotifyMessage']['SendInBatchesRemarks'].')</span>';
}

$libdb = new libdb();
$sql = "Select NotifyDateTime, SendTimeMode from INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr
		inner join INTRANET_APP_NOTIFY_MESSAGE as ianm On ianmmr.NotifyMessageID = ianm.NotifyMessageID
		Where ianmmr.ModuleName = 'SchoolNews' And ianmmr.ModuleRecordID = $AnnouncementID And ianm.RecordStatus = '1' order by ianm.NotifyMessageID Desc Limit 1";
$resultAry = $libdb->returnResultSet($sql);
if (count($resultAry) > 0) {
	$sendTimeMode = $resultAry[0]["SendTimeMode"];
	$sendTimeString = $resultAry[0]["NotifyDateTime"];
}
else {
	$sendTimeMode = "";
	$sendTimeString = "";
}

$nowChecked = false;
$scheduledChecked = true;
$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
	$sendTime = strtotime($sendTimeString);
	if ($sendTime > $nowTime) {
		$scheduledMessageChecked = "checked";
		$nowChecked = false;
		$scheduledChecked = true;
		$passScheduled = true;
	}
	else {
		$passScheduled = false;
		$scheduledMessageChecked = "";
		$sendTimeString = "";
	}
}
else {
	$sendTimeString = "";
}

$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
$nextTimeslotHour = date('H', $rounded_seconds);
$nextTimeslotMinute = date('i', $rounded_seconds);
if ($sendTimeString == "") {
	$selectionBoxDate = date('Y-m-d', $nowTime);
	$selectionBoxHour = $nextTimeslotHour;
	$selectionBoxMinute = $nextTimeslotMinute;
}
else {
	$selectionBoxDate = date('Y-m-d', $sendTime);
	$selectionBoxHour = date('H', $sendTime);
	$selectionBoxMinute = date('i', $sendTime);
}	

$htmlAry['sendTimeNowRadio'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $nowChecked, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
$htmlAry['sendTimeScheduledRadio'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");

// [2016-0906-1555-34206] Define Date Picker for Start Date and End Date first
// Ensure datepicker.js included even client without eClassApp
$htmlAry['startTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("AnnouncementDate", $cStartDate);
$htmlAry['endTimeDateTimeDisplay'] = $linterface->GET_DATE_PICKER("EndDate", $cEndDate);

$x = '';
$x .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$x .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
$x .= " : ";
$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
$x .= "&nbsp;&nbsp;";
$x .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
$x .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
$x .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
$TimeDivDisplay = ($scheduledChecked)?'':'style="display:none"';
$htmlAry['sendTimeDateTimeDisplay'] = '<div id="specificSendTimeDiv" '.$TimeDivDisplay.'>';
$htmlAry['sendTimeDateTimeDisplay'] .=$x;
$htmlAry['sendTimeDateTimeDisplay'] .='</div>';

// [2017-0904-1103-25073] Send eClass App push message to student's guardian as well
if(!$sys_custom['schoolNews']['disableSendToParentAlert']) {
	$htmlAry['sendToParentAlert'] = '<span id="stu_parent_alert">
										<br>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="stu_parent_alert" id="stu_parent_alert" value="1"> <label for="stu_parent_alert">'.$Lang['SysMgr']['SchoolNews']['StudentParentAlert'].'</label>
									</span>';
}

$sendPushNotiInputCheck = ($passScheduled)?'checked':'';

if($copied)
	$xmsg = $Lang['SysMgr']['SchoolNews']['Copied'];

# Start layout
$linterface->LAYOUT_START($xmsg);
echo $linterface->Include_JS_CSS();
?>

<script language="javascript">
$(document).ready(function(){
	<? if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
		$('input#sendNewSelect').hide();
		$('label#sendNewSelect').hide();
		if( '<?=$passScheduled?>'!='1'){
			$('div#PushMessageSetting').hide();
		}
		else{
			$('div#PushMessageSetting').show();
			$('#sendTimeSettingDiv').show();
		}
 	<?php } ?>
	
 	$('input:radio[name="RecordStatus"]').click(
 		function(){
			if(this.value==0) {
				<?php if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
	 				$('input:checkbox[name="push_message_alert"]').removeAttr('checked');
	 				$('input:checkbox[name="push_message_alert"]').attr('disabled','true');
	 				$('div#specificSendTimeDiv').hide();
	 				$('div#PushMessageSetting').hide();
	 				$('input:radio[name="sendTimeMode"]').removeAttr('checked');
	 				$('input:radio[name="sendTimeMode"]').attr('disabled','true');
		 				
					if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
						$('input#stu_parent_alert').removeAttr('checked');
						$('input#stu_parent_alert').attr('disabled', 'true');
						$('span#stu_parent_alert').hide();
					}
 				<?php } ?>
 				
 				$('input:checkbox[name="email_alert"]').removeAttr('checked');
 				$('input:checkbox[name="email_alert"]').attr('disabled','true');
 			}
 			else if(this.value==1){
				<?php if($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
 	 				$('input:checkbox[name="push_message_alert"]').removeAttr('disabled');
 	 				$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
	 				
					if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
						$('input#stu_parent_alert').removeAttr('disabled');
						$('span#stu_parent_alert').show();
					}
 				<?php } ?>
 				
 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
//  			$('input:checkbox[name="push_message_alert"]').removeAttr('disabled');
//  			$('input:checkbox[name="email_alert"]').removeAttr('disabled');
//  			$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
//  			$('input:radio[name="sendTimeMode"]').removeAttr('disabled');
 			}
 		}
 	);
 	
 	if('<?=$public_checked?>'=="CHECKED"){
 		$('#Target_allUser').click();
 	 }
 	else{
 		$('#Target_specificUser').click();
 	}
});

function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
			obj.options[i] = null;
		}
	}
}

function removeAllOption(from, to){
	checkOption1(from);
	checkOption1(to);
	for(i=0; i<from.length; i++){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
	}
	checkOptionClear(from);
	text1 = "";
	for(i=0; i<20; i++)
		text1 += " ";
	checkOptionAdd(from, text1, "")
}

function checkform(obj)
{
        if(!check_text(obj.AnnouncementDate, "<?php echo $i_alert_pleasefillin.$i_AnnouncementDate; ?>.")) return false;
        if(!check_date(obj.AnnouncementDate, "<?php echo $i_invalid_date; ?>.")) return false;
        if(!check_text(obj.EndDate, "<?php echo $i_alert_pleasefillin.$i_EndDate; ?>.")) return false;
 		if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>.")) return false;
		if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;
        if(compareDate(obj.EndDate.value,obj.AnnouncementDate.value)<0) 
		{
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		
// 		var PublicDisplay = document.getElementById("publicdisplay");
		var Group = document.getElementById("GroupID");
		var PublicDisplay = document.getElementById("Target_specificUser");
		if(PublicDisplay.checked == true){
			if(Group.length==0){
				alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		document.getElementById('submit_btn').disabled  = true; 
		document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
		
		checkOptionAll(obj.elements["GroupID[]"]);
		checkOption1(document.getElementById("GroupID"));
		
        Big5FileUploadHandler();
        
        var scheduleString = '';
   		if ($('input#sendTimeRadio_scheduled').attr('checked')) {
   	 		scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    	}
    	$('input#sendTimeString').val(scheduleString);
		
    	var dateNow = new Date();
    	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
        if($('#push_message_alert').attr('checked')){    	
    		if ($('input#sendTimeRadio_scheduled').attr('checked') && scheduleString <= dateNowString) {
    			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
    			$('input#sendTime_date').focus();
    			return false;
    		}     
        }
    	// check if the schedule day is greater than 31days after the issue day 
    	if($('#push_message_alert').attr('checked')){
    		var maxDate = new Date();
    		maxDate.setDate(dateNow.getDate() + 31);
    		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
    		if ($('input#sendTimeRadio_scheduled').attr('checked')&& scheduleString > maxDateString) {
    			$('input#sendTime_date').focus();
    			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
    			return false;
    		}
    	}
        
        // create a list of files to be deleted
        objDelFiles = document.getElementsByName('file2delete[]');
        files = obj.deleted_files;
        if(objDelFiles==null)return true;
        x="";
        for(i=0;i<objDelFiles.length;i++){
	        if(objDelFiles[i].checked==true){
		 		x+=objDelFiles[i].value+":";
		    }
	    }
	    files.value = x.substr(0,x.length-1);
	    return true;
}

function Big5FileUploadHandler() {
	 new_attach = parseInt(document.form1.attachment_size.value);
	 for(i=0;i<new_attach;i++)
	 {
		 	objFile = eval('document.form1.filea'+i);
		 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
		 	if(typeof(objFile)!='undefined' && objFile.value!='' && typeof(objUserFile)!='undefined'){
			 	Ary = objFile.value.split('\\');
			 	if(Ary!=null){
				 	objUserFile.value = Ary[Ary.length-1];
				}
			}
	 }
	 return true;
}

function fileAttach(obj){
     aWin=newWindow("",1);
     temp = obj.target;
     obj.target="intranet_popup1";
     obj.action="more_attach.php";
     obj.submit();
     
     obj.target=temp;
     obj.action="edit_update.php";
}

function setRemoveFile(index,v){
	obj = document.getElementById('a_'+index);
	obj.style.textDecorationLineThrough = !v;
}

//villa 2016-10-17
function checkingStatus(radioBtn){
	if(radioBtn.checked==true){
		<? if( $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
			if('<?=$sys_custom['schoolNews']['enableSendingPushMsgToAllUser']?>'!='1'){
				$('#push_message_alert').removeAttr('checked');
				$('div#sendTimeSettingDiv').hide();
				$('div#PushMessageSetting').hide();
// 				$('#push_message_alert').attr('disabled', true);
				$('input#push_message_alert').removeAttr("checked");
				$('#push_message_alert').hide();
			}
			
			// for Target : Specific Group only
			if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
				$('input#stu_parent_alert').removeAttr('checked');
				$('input#stu_parent_alert').attr('disabled', true);
				$('span#stu_parent_alert').hide();
			}
		<?php }?>
		
		if('<?=$sys_custom['schoolNews']['enableSendingEmailToAllUser']?>'!='1'){
// 			$('#email_alert').attr('disabled',true);
			$('span#email_alert').hide();
			$('input#email_alert').removeAttr("checked");
		}

		$('#groupSelectionBox').hide();
// 		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
	}
	else{
		<? if( $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
			$('span#push_message_alert').show();
			
			if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
				$('input#stu_parent_alert').removeAttr('disabled');
				$('span#stu_parent_alert').show();
			}
		<?php }?>
		
		$('#groupSelectionBox').show();
		$('span#email_alert').show();
		
		checkOption1(document.getElementById("GroupID"));
	}
}

/*
function add_field(formObj)
{
	var table = document.getElementById("file_list");
	var new_attachment = parseInt(formObj.attachment_size.value);
	var row = table.insertRow(new_attachment);
	if (document.all)
	{
		var cell = row.insertCell(0);
		new_attachment++;
		x= '<input class="file" type="file" name="filea'+new_attachment+'" size="40">';
		x+='<input type="hidden" name="hidden_userfile_name'+new_attachment+'">';

		cell.innerHTML = x;
		document.form1.attachment_size.value = new_attachment;
	}	
}
*/

var no_of_upload_file = <? echo $no_file==""?1:$no_file;?>;

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x= '<input class="file" type="file" name="filea'+no_of_upload_file+'" size="40">';
	x+='<input type="hidden" name="hidden_userfile_name'+no_of_upload_file+'">';
	
	cell.innerHTML = x;
	no_of_upload_file++;
	
	document.form1.attachment_size.value = no_of_upload_file;
}

function back(){
  window.location="index.php";        
}

function setRemoveFile(index,v){
	obj = document.getElementById('a_'+index);
	obj.style.textDecoration = v?"line-through":"";
}

<?php
if ($plugin['power_voice'])
{ 
?>
	var NewRecordBefore = false;
	
	function listenPVoice(fileName)
	{
		newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location="+fileName, 18);
	}
	
	function listenPVoice2(fileName)
	{
		newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
	}
	
	function editPVoice()
	{
		var fileName = document.form1.voiceFile.value;
		newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
	}
	
	function clearPVoice(num)
	{
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
		
		voiceObj.innerHTML = "";
		document.form1.voiceFile.value = "";
		clearObj.style.display = "none";
	}

	function file_exists()
	{
		alert("<?=$classfiles_alertMsgFile12?>");
	}
	
	function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
	{
		var voiceObj = document.getElementById("voiceDiv");
		var clearObj = document.getElementById("clearDiv");
		
		if (fileName != "")
		{
			if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
			{
				if ((fileEmpty=="1") && (!NewRecordBefore))
				{
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice('<?=$CurVoiceFileLink?>')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
					document.form1.is_empty_voice.value = "1";
				}
				else
				{
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
					document.form1.voicePath.value = filePath;
					if (fileEmpty=="1") 
						document.form1.is_empty_voice.value = "1";
					else
						document.form1.is_empty_voice.value = "";
					NewRecordBefore = true;
				}
			}
			else {
				voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
			}
			
			document.form1.voiceFile.value = fileName;
			document.form1.voiceFileFromWhere.value = 1;
			clearObj.style.display = "block";
		}
	}
<?php
} 
?>

<? if( $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
		}
		else {
			$('div#specificSendTimeDiv').show();
		}
	}
	
	function checkedSendPushMessage(checkBox) {
		if (checkBox.checked == true) {
			$('div#sendTimeSettingDiv').show();
			$('div#PushMessageSetting').show();
			
			if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
				$('input#stu_parent_alert').removeAttr('disabled');
				$('span#stu_parent_alert').show();
			}
		}
		else {
			$('div#sendTimeSettingDiv').hide();
			$('div#PushMessageSetting').hide();
			
			if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
				$('input#stu_parent_alert').removeAttr('checked');
				$('input#stu_parent_alert').attr('disabled', 'true');
				$('span#stu_parent_alert').hide();
			}
		}
	
		if('<?=$sys_custom['schoolNews']['disableSendToParentAlert']?>'!='1') {
			if(document.getElementById('Target_allUser').checked) {
				$('input#stu_parent_alert').removeAttr('checked');
				$('input#stu_parent_alert').attr('disabled', 'true');
				$('span#stu_parent_alert').hide();
			}
		}
	}
	
	function syncIssueTimeToPushMsg(){
		var issueDate = $('#AnnouncementDate').val();
		$('#sendTime_date').val(issueDate);
	}
<?php }?>
</script>

<style> 
select option {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 400px;
}
/*
 * Villa Prevent the name of option in the selection box break the layout
*/
</style>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" action="edit_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">

	<table class="form_table_v30">
		<!-- Start Date -->
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['StartDate']?> </td>
			<td><?=$htmlAry['startTimeDateTimeDisplay']?></td>
		</tr>
		
		<!-- End Date -->
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['EndDate']?> </td>
			<td><?=$htmlAry['endTimeDateTimeDisplay']?></td>
		</tr>

		<?php if($sys_custom['SchoolNews']['ShowWebsiteCode']) { ?>
            <tr>
                <td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['WebsiteCode']?></td>
                <td>
                    <input type="text" name="WebsiteCode" value="<?=$WebsiteCode?>" size="200"  class="textboxtext">
                </td>
            </tr>
		<?php } ?>

		<!-- Title -->
		<tr>
		  	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['SchoolNews']['Title']?> </td>
			<td>
				<input type="text" name="Title" value="<?=$cTitle?>" size="30" class="textboxtext">
			</td>
		</tr>
		<?php if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){ ?>	
		<!-- English Title -->
		<tr>
		  	<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['TitleEng']?> </td>
			<td>
				<input type="text" name="TitleEng" value="<?=$enTitle?>" size="30" class="textboxtext">
			</td>
		</tr>
		<?php }?>
		
		<!-- Description -->
		<tr>
			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['Description']?></td>
			<td align="left" valign="top">
			<? /* ?><textarea name="Description" cols="60" rows=10><?=$cDescription?></textarea><? */ ?>
			<?
				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
				$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", $cDescription);
				$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
				$objHtmlEditor->Create();
			?>
			</td>
		</tr>
		<?php if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){ ?>	

    			<!-- Description English-->
    		<tr>
    			<td class="field_title"><?=$Lang['SysMgr']['SchoolNews']['DescriptionEng']?></td>
    			<td align="left" valign="top">
    			<? /* ?><textarea name="Description" cols="60" rows=10><?=$cDescription?></textarea><? */ ?>
    			<?
    				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
    				$objHtmlEditor = new FCKeditor ( 'DescriptionEng' , "100%", "320", "", "Basic2_withInsertImageFlash", $enDescription);
    				$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
    				$objHtmlEditor->Create();
    			?>
    			</td>
    		</tr>
		<?php } ?>
		
		<!-- Sound Recorder -->
		<?=$PVHtml?>
		
		<!-- Attachment -->
		<tr>
			<td class="field_title" rowspan="2"><?=$Lang['SysMgr']['SchoolNews']['CurrentAttachment']?></td>
			<td>
				<table><?=$displayAttachment?></table>
			</td>
		</tr>
		
		<tr>
			<td>
			<? /* ?>
				<table width=80% border=0 id='file_list' cellpadding=0 cellspacing=0>
					<tr>
						<td>
							<input class="file" type="file" name="filea1" size="40">
							<input type="hidden" name="hidden_userfile_name1">
						</td>
					</tr>
				</table>
			<? */ ?>
			
				<table id="upload_file_list" border="0" cellpadding="0" cellpadding="0">
					<script language="javascript">
						for(i=0;i<no_of_upload_file;i++)
						{
							document.writeln('<tr><td><input class="file" type="file" name="filea'+i+'" size="40">');
							document.writeln('<input type="hidden" name="hidden_userfile_name'+i+'"></td></tr>');
						}
					</script>
				</table>
				<input type=button value=" + " onClick="add_field()">
			</td>
		</tr>
		<!-- End of Attachment -->
		
		<!-- Target -->
		<tr>
			<td class="field_title"><?=$Lang['SchoolNews']['target']?></td>
			<td>
				<table class="form_table_v30">
					<tr>
						<td colspan="3"><input type="radio" name="Target_user" id="Target_allUser" value="1" onClick="javascript:checkingStatus(this);" <?=$public_checked?>><label for="Target_allUser"><?=$Lang['eSurvey']['public']?></label>
						<input type="radio" name="Target_user" id="Target_specificUser" value="0" onClick="javascript:checkingStatus($('#Target_allUser'));" <?=$specific_checked?>><label for="Target_specificUser"><?=$Lang['SchoolNews']['SpecificGroup']?></label></td>
					</tr>
					<tr id='groupSelectionBox'>
						<td class="field_title"	valign="top">
							<?=$i_admintitle_group?>
						</td>
						<td>
						<?php 
						if($sys_custom['DHL']){
							include_once($intranet_root."/includes/DHL/libdhl.php");
							$libdhl = new libdhl();
							$target_group_id_ary = $li->returnTargetGroupsID();
						
							$x = "<table width=100% border=0 cellpadding=5 cellspacing=0 class=\"inside_form_table\">\n";
                        	$x .= "<tr>\n";
                        		$x .= "<td class=tableContent width=50%>\n";
                        		$x .= $libdhl->getIntranetGroupSelection("GroupID", "GroupID[]", count($target_group_id_ary)>0?$target_group_id_ary: array(0), $i___sMultiple=true, $____hasFirst=false, $____firstText='', $____tags=' size="10" style="width:100%;" ', $____valuePrefix='',$____display_prefix_code_in_group_name=false,$fromSchoolInfo=false,$fromSchoolNews=true);
                        			$x .= "<div>\n";
                        			$x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=GroupID[]&SelectGroupOnly=1&page_title=SelectAudience&DisplayGroupCategory=1&isAdmin=$isAdmin',16);");
                        			$x .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "remove_selection_option('GroupID',0);");
                        			$x .= "</div>\n";
                        		$x .= "</td>\n";
                        	/*	
                        	$x .= "<td class=tableContent align=center>\n";
                        		$x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "submit", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['GroupID[]']);return false;", "submit2") . "<br /><br />";
                        		$x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "submit", "checkOptionTransfer(this.form.elements['GroupID[]'],this.form.elements['AvailableGroupID[]']);return false;", "submit23");
                        	$x .= "</td>\n";
                        	$x .= "<td class=tableContent width=50%>\n";
                        		$x .= $libdhl->getIntranetGroupSelection("AvailableGroupID", "AvailableGroupID[]", "", $___isMultiple=true, $____hasFirst=false, $____firstText='', $____tags=' size="10" ', $____valuePrefix='');
                        	$x .= "</td>\n";
                        	*/
                        	$x .= "</tr>\n";
                        	$x .= "</table>\n";
                        	echo $x;
						}
						else{
						?>
							<?=$lo->displayAnnouncementGroups($li->AnnouncementID, 1)?>
						<?php } ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<!-- Record Status -->
		<tr>
			<td class="field_title" valign="top" rowspan="3"><?=$Lang['SysMgr']['SchoolNews']['Status']?></td>
			<td>
			 	<input type="radio" name="RecordStatus" id="RecordStatus0" value="0" <?=$RecordStatus0?>> <label for="RecordStatus0"><?=$Lang['SysMgr']['SchoolNews']['StatusPending']?></label>
			 	<input type="radio" name="RecordStatus" id="RecordStatus1" value="1" <?=$RecordStatus1?>> <label for="RecordStatus1"><?=$Lang['SysMgr']['SchoolNews']['StatusPublish']?></label>
			 	
			 	<? if(!$special_feature['announcement_hide_email_alert'] || $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {?>
					</br>
				 	<? if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
						<span id="push_message_alert" style='margin-left:20px;'>
							<input type="checkbox" name="push_message_alert" id="push_message_alert" value="1" onclick="checkedSendPushMessage(this);" <?= $sendPushNotiInputCheck ?>> <label for="push_message_alert"><?=$Lang['SysMgr']['SchoolNews']['PushMessageAlert']?> <?=$htmlAry['sendInBatchRemarks']?></label>
							<div id= 'PushMessageSetting' <?= $TimeDivDisplay ?>>
								<?php echo $linterface->Get_Warning_Message_Box(
									$Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'],
									'<div id="sendTimeSettingDiv" style="display:none">'.
											$htmlAry['sendTimeNowRadio'].'<br>'.
											$htmlAry['sendTimeScheduledRadio'].$htmlAry['sendTimeDateTimeDisplay'].
											$htmlAry['sendToParentAlert']
								);?>
							</div>
							</span><br>
					<? }?>
					
					<? if(!$special_feature['announcement_hide_email_alert']) {?>
						<span id="email_alert" style='margin-left:20px;'>
							<input type="checkbox" name="email_alert" id="email_alert" value="1"> <label for="email_alert"><?=$Lang['SysMgr']['SchoolNews']['EmailAlert']?></label>
						</span>
						<br />
						<span id='email_alert' style='margin-left:20px;'>(<?=$Lang['eNotice']['NotifyRemark']?>)</span>
					<? }?>
				<? } ?>
			</td>
		</tr>
		<!-- Record Status End -->
		
		<!-- Display On Top -->
		<tr></tr>
		<tr></tr>
		<tr>
			<td class="field_title" rowspan="2"><?=$Lang['SysMgr']['SchoolNews']['DisplayOnTop']?> </td>
			<td class="tabletext">
				<input type="checkbox" name="onTop" value="1" <?=$onTop_checked?>>
			</td>
		</tr>
		
		<!-- Display On Top End -->
	</table>
	
	<?=$linterface->MandatoryField();?>
	<div class="edit_bottom_v30">
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit_btn")?> &nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back()")?>
	</div>
	
	<input type="hidden" name="AnnouncementID" value="<?=$li->AnnouncementID?>">
	<input type="hidden" name='attachment_size' value='<? echo $no_file==""?1:$no_file;?>'>
	<input type="hidden" name="deleted_files" value='' />
	<input type="hidden" name="is_empty_voice" value='' />
</form>

<script language="javascript">
	if ('<?=$li->RecordType?>'!="1"){
			var group = document.getElementById("groupSelectionBox");
			group.style.display = "block";
	}
	
// 	$(document).ready(function() {
//  		$('input:radio[name="RecordStatus"]').click(
//  			function(){
//  				if(this.value==0){
// 					$('input:checkbox[name="push_message_alert"]').removeAttr('checked');
// 					$('input:checkbox[name="push_message_alert"]').attr('disabled','true');
// 					$('input:checkbox[name="email_alert"]').removeAttr('checked');
// 					$('input:checkbox[name="email_alert"]').attr('disabled','true');
//  				}
//  				else if(this.value==1){
// 	 				$('input:checkbox[name="push_message_alert"]').removeAttr('disabled');
// 	 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
//  				}
//  			}
//  		);
// 	});
	
	if(<?=$li->RecordStatus==0?"true":"false"?>){
		$('input:checkbox[name="push_message_alert"]').attr("disabled",true);
		$('input:checkbox[name="email_alert"]').attr("disabled",true);
	}
</script>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>