<?php
# using: anna

############ Change Log [Start] ############
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
# - 2010-04-13 YatWoon
#	add coding: remove images which user upload via fck editor 
#
############ Change Log [End] ############

	$PATH_WRT_ROOT = "../../../../";
	$CurrentPageArr['schoolNews'] = 1;
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/liblog.php");
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

	intranet_auth();
	intranet_opendb();
	
	if($sys_custom['DHL']){
	    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	    $libdhl = new libdhl();
	    $isPIC = $libdhl->isPIC();
	}
	
	if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))){
				include_once($PATH_WRT_ROOT."includes/libaccessright.php");
				$laccessright = new libaccessright();
				$laccessright->NO_ACCESS_RIGHT_REDIRECT();
				exit;
	}

	$li = new libdb();
	$lf = new libfilesystem();
	$leClassApp = new libeClassApp();
	$list =implode(",", $AnnouncementID);
	
	$status = IntegerSafe($status);
	$sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID IN ($list)";
	$li->db_db_query($sql);

	# Get attachment path
	$sql = "SELECT AnnouncementID, Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
	$array = $li->returnArray($sql,2);
	
	# Delete attachment
	for ($i=0; $i<sizeof($array); $i++)
	{
		if ($array[$i][1]==null || $array[$i][1]=="")
		{
			
		}
		else {
			$dir2del = "$file_path/file/announcement/".$array[$i][1].$array[$i][0];
			$t = $lf->lfs_remove($dir2del);
		}
		
		# delete unsent scheduled push message
		$_announcementId = $array[$i][0];
		$leClassApp->overrideExistingScheduledPushMessageFromModule("SchoolNews", $_announcementId, $newNotifyMessageId='');
	}
	
	
	# Add deletion log
	$lg = new liblog();
	$sql = "select * FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
	$result = $li->returnArray($sql);
	foreach($result as $k=>$d)
	{
		$this_announcement_id = $d['AnnouncementID'];
		
		$RecordDetailAry = array();
		$RecordDetailAry['Record_Date'] = substr($d['AnnouncementDate'],0,10);
		$RecordDetailAry['Title'] = $d['Title'];
		$RecordDetailAry['PosterName'] = $d['PosterName'];
		
		$lg->INSERT_LOG('SchoolNews', 'SchoolNews', $RecordDetailAry, 'INTRANET_ANNOUNCEMENT', $this_announcement_id);
	}
	
	$sql = "DELETE FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
	$li->db_db_query($sql);
	
	# remove user uploaded files which via fckeditor
	foreach($AnnouncementID as $k=>$v)
	{
		$currentFolder = $lf->returnRelativePathFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $v);
		$lf->deleteDirectory($intranet_root."/".$currentFolder);
	}
	
	intranet_closedb();
	$msg = "delete";
	header("Location: index.php?status=$status&order=$order&field=$field&msg=$msg");
?>